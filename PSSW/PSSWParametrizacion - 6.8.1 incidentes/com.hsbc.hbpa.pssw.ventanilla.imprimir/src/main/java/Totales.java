import java.util.*;

public class Totales {

	public int imprimeTotales(Vector vDatos, int iDriver) {
		ToolsPrinter tpImpresora = new ToolsPrinter();
		String strOut;
		int intStatus = tpImpresora.verificaStatus();
		if ((intStatus == 95) || (intStatus == 88)) {
			intStatus = 0;
			tpImpresora.activateControl("CERTIFICACION",iDriver);
			for (int intControl = 1;
				intControl < vDatos.size();
				intControl++) {
				strOut = (String) vDatos.elementAt(intControl);
				tpImpresora.enviaCadena(strOut);
			}
			if (iDriver == 0) {
				tpImpresora.InserteDocumento();
				tpImpresora.activateControl("SELECCIONASLIP",iDriver);
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.imprimeCadena1();
				tpImpresora.activateControl("SALTOHOJA",iDriver);
				tpImpresora.activateControl("SELECCIONAJOURNAL",iDriver);
			} else
				tpImpresora.activateControl("SALTOHOJA",iDriver);
		}
		return intStatus;
	}
}