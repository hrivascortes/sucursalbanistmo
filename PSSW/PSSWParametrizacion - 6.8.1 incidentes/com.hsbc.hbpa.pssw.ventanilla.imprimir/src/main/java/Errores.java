import java.util.*;

public class Errores {
	public int imprimeError(String strCadena, int iDriver) {
		Vector vCampos = new Vector();
		String strTmp = new String();
		ToolsPrinter tpImpresora = new ToolsPrinter();

		int intStatus = tpImpresora.verificaStatus();
		if ((intStatus == 95) || (intStatus == 88)) {
			intStatus = 0;
			vCampos = tpImpresora.Campos(strCadena, "~");
			int intSize = vCampos.size();
			tpImpresora.activateControl("CERTIFICACION",iDriver);
			for (int i = 1; i < intSize; i++) {
				strTmp = (String) vCampos.elementAt(i);
				if (iDriver != 1)
					strTmp = "                    " + strTmp;
				tpImpresora.enviaCadena(strTmp);
			}
			if (iDriver == 0) {
				tpImpresora.InserteDocumento();
				tpImpresora.activateControl("SELECCIONASLIP",iDriver);
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.imprimeCadena1();
				tpImpresora.activateControl("SALTOHOJA",iDriver);
				tpImpresora.activateControl("SELECCIONAJOURNAL",iDriver);
			} else {
				tpImpresora.activateControl("SALTOHOJA",iDriver);
			}
		}
		return intStatus;
	}
}