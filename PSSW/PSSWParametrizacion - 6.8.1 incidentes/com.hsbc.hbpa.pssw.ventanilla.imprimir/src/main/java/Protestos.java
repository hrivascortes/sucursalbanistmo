import java.util.*;

public class Protestos {
	public int imprimeProtesto1(
		String strLeyenda,
		String strDatosSuc,
		String strDate,
		String strTime,
		int iDriver,
		String strIdentidad) {
		String strJustificacion = "";					
		ToolsPrinter tpImpresora = new ToolsPrinter();
		if (iDriver != 1)
			strJustificacion = "";		
		int intStatus = tpImpresora.verificaStatus();
		if ((intStatus == 95) || (intStatus == 88)) {
			String strOutLine = new String("");
			String strFiller =
				new String("                                                                                ");
			tpImpresora.activateControl("CERTIFICACION",iDriver);

			Vector vDatosSuc = tpImpresora.Campos(strDatosSuc, "@");
			String strNomSuc = (String) vDatosSuc.elementAt(0);
			strNomSuc = strNomSuc.substring(7, strNomSuc.length());
			String strNumSuc = (String) vDatosSuc.elementAt(1);
			strNumSuc = strNumSuc.substring(7, strNumSuc.length());
			String strDomSuc = (String) vDatosSuc.elementAt(2);
			strDomSuc = strDomSuc.substring(7, strDomSuc.length());
			String strNomPza = (String) vDatosSuc.elementAt(4);
			strNomPza = strNomPza.substring(7, strNomPza.length());
			tpImpresora.enviaCadena(
				strJustificacion + "                               "+strIdentidad+" PANAMA                               ");
			tpImpresora.enviaCadena(
				strJustificacion + "Certifica que este cheque fue presentado para su pago en ventanilla dentro del");
			tpImpresora.enviaCadena(
				strJustificacion + "plazo fijado en el Art�culo 181 de la Ley General  de T�tulos y Operaciones de");
			tpImpresora.enviaCadena(
				strJustificacion + "Credito y que el librado rehus� el pago por la siguiente causa :");
			tpImpresora.enviaCadena(strJustificacion + strLeyenda);
			String strFecha =
				strDate.substring(7, 9)
					+ "/"
					+ strDate.substring(5, 7)
					+ "/"
					+ strDate.substring(1, 5);
			String strHora =
				strTime.substring(0, 2) + ":" + strTime.substring(2, 4);
			if (strNomPza.length() > 25)
				strNomPza = strNomPza.substring(0, 25);
			else
				strNomPza =
					strNomPza + strFiller.substring(0, 25 - strNomPza.length());
			strOutLine =
				"|"
					+ strNomPza
					+ strFecha
					+ "     "
					+ strHora
					+ "| |                            |";
			tpImpresora.enviaCadena(
				strJustificacion + " _____________________________________________   ____________________________");
			tpImpresora.enviaCadena(strJustificacion + strOutLine);
			strOutLine = "| Sucursal " + strNumSuc + " " + strNomSuc;
			if (strOutLine.length() > 46)
				strOutLine = strOutLine.substring(0, 46);
			else
				strOutLine =
					strOutLine
						+ strFiller.substring(0, 46 - strOutLine.length());
			strOutLine = strOutLine + "| |                            |";
			tpImpresora.enviaCadena(strJustificacion + strOutLine);
			tpImpresora.enviaCadena(
				strJustificacion + "|_____________________________________________| |____________________________|");
			if (iDriver == 0) {
				tpImpresora.InserteDocumento();
				tpImpresora.activateControl("SELECCIONASLIP",iDriver);
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.imprimeCadena1();
				tpImpresora.activateControl("SALTOHOJA",iDriver);
				tpImpresora.activateControl("SELECCIONAJOURNAL",iDriver);
			} else {
				tpImpresora.activateControl("SALTOHOJA",iDriver);
			}
		}
		System.gc();
		return intStatus;
	}

	public int imprimeProtesto2(
		String strLeyenda,
		String strDate,
		int iDriver,
		String strIdentidad) {
		String strJustificacion = "";					
		if (iDriver != 1)
			strJustificacion = "";					
		ToolsPrinter tpImpresora = new ToolsPrinter();
		int intStatus = tpImpresora.verificaStatus();
		if ((intStatus == 95) || (intStatus == 88)) {
//			String strOutLine = new String("");
//			String strFiller =
				new String("                                                                                ");
			tpImpresora.activateControl("CERTIFICACION",iDriver);
			tpImpresora.enviaCadena(
				strJustificacion + "                           "+strIdentidad+" PANAMA                               ");
			tpImpresora.enviaCadena(
				strJustificacion + "Este cheque fue rechazado a su presentaci�n en ventanilla por no reunir los");
			tpImpresora.enviaCadena(
				strJustificacion + "requisitos para su proceso en la C�mara de Compensaci�n , por la(s) causa(s)");
			tpImpresora.enviaCadena(strJustificacion + "abajo se�alada(s) : ");
			tpImpresora.enviaCadena("");
			tpImpresora.enviaCadena(strJustificacion + strLeyenda);
			tpImpresora.enviaCadena("");
			String strFecha =
				strDate.substring(7, 9)
					+ "/"
					+ strDate.substring(5, 7)
					+ "/"
					+ strDate.substring(1, 5);
			tpImpresora.enviaCadena(
				strJustificacion + "                               Fecha : " + strFecha);
			tpImpresora.enviaCadena(
				strJustificacion + "                          No hace las veces de protesto");
			if (iDriver == 0) {
				tpImpresora.InserteDocumento();
				tpImpresora.activateControl("SELECCIONASLIP",iDriver);
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.imprimeCadena1();
				tpImpresora.activateControl("SALTOHOJA",iDriver);
				tpImpresora.activateControl("SELECCIONAJOURNAL",iDriver);
			} else {
				tpImpresora.activateControl("SALTOHOJA",iDriver);
			}
		}
		System.gc();
		return intStatus;
	}
}