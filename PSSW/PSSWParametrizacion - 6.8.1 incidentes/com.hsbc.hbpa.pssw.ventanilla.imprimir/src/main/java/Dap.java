import java.util.*;

public class Dap {
	Currency cNumaLet = new Currency();
	ToolsPrinter tpImpresora = new ToolsPrinter();
	int intStatus;
	String strFiller = "                                                  ";
	String strOutLine = "";
	Vector vDatosSuc = new Vector();

	public int imprimeDap(
		Vector vDatos,
		String strDatosSuc,
		String strCajero,
		String strDate,
		String strTime,
		int iDriver,
		String strIdentidad) {
		intStatus = tpImpresora.verificaStatus();
		if ((intStatus == 95) || (intStatus == 88)) {
			intStatus = 0;
			tpImpresora.activateControl("CERTIFICACION",iDriver);
			String strNombre1 = (String) vDatos.elementAt(1);
			if (strNombre1.equals(""))
				strNombre1 = " ";
			String strServicio = (String) vDatos.elementAt(2);
			if (strServicio.equals(""))
				strServicio = " ";
			String strImporte = (String) vDatos.elementAt(3);
			if (strImporte.equals(""))
				strImporte = "0.00";
			String strNombre2 = (String) vDatos.elementAt(4);
			if (strNombre2.equals(""))
				strNombre2 = " ";
			String strReferencia = (String) vDatos.elementAt(5);
			if (strReferencia.equals(""))
				strReferencia = " ";
			String strCuenta = (String) vDatos.elementAt(6);
			if (strCuenta.equals(""))
				strCuenta = " ";
			vDatosSuc = tpImpresora.Campos(strDatosSuc, "@");
			String strNumSuc = (String) vDatosSuc.elementAt(1);
			strNumSuc = strNumSuc.substring(7, strNumSuc.length());
			tpImpresora.enviaCadena(
				"             Comprobante de pago por Dispersión");
			tpImpresora.enviaCadena(
				"___________________________________________________________");
			strOutLine =
				"Sucursal : "
					+ strNumSuc
					+ "               "
					+ strTime.substring(0, 2)
					+ ":"
					+ strTime.substring(2, 4)
					+ ":"
					+ strTime.substring(4, 6)
					+ "     "
					+ strDate.substring(7, 9)
					+ "/"
					+ strDate.substring(5, 7)
					+ "/"
					+ strDate.substring(1, 5)
					+ "\n";
			tpImpresora.enviaCadena(strOutLine);
			strOutLine = "Cajero   : " + strCajero + "\n";
			tpImpresora.enviaCadena(strOutLine);
			strOutLine = "           " + strNombre1 + "\n";
			tpImpresora.enviaCadena(strOutLine);
			strOutLine =
				"Servicio : "
					+ strServicio
					+ "               Pago de : "
					+ strImporte
					+ "\n";
			tpImpresora.enviaCadena(strOutLine);
			if (strCuenta.equals(" "))
				strOutLine = "       A : " + strNombre2 + "\n";
			else
				strOutLine =
					"       A : "
						+ strNombre2
						+ "               Cuenta  : "
						+ strCuenta
						+ "\n";
			tpImpresora.enviaCadena(strOutLine);
			strOutLine =
				"Referencia:"
					+ strReferencia
					+ "               Firma : _______________ "
					+ "\n";
			tpImpresora.enviaCadena(strOutLine);
			tpImpresora.enviaCadena(
				"ESTA FICHA CONSTITUYE UN COMPROBANTE DE LIQUIDACION. LA FIRMA DEL");
			tpImpresora.enviaCadena(
				"BENEFICIARIO IMPLICA LA ACEPTACION DE QUE LA CANTIDAD ENTREGADA ES");
			tpImpresora.enviaCadena(
				"CORRECTA. "+strIdentidad+"  NO TENDRA NINGUNA RESPONSABILIDAD YA QUE OBEDECE ");
			tpImpresora.enviaCadena("A INSTRUCCIONES PRECISAS DEL DISPERSOR.");
			if (iDriver == 0) {
				tpImpresora.InserteDocumento();
				tpImpresora.activateControl("SELECCIONASLIP",iDriver);
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.imprimeCadena1();
				tpImpresora.activateControl("SALTOHOJA",iDriver);
				tpImpresora.activateControl("SELECCIONAJOURNAL",iDriver);
			} else
				tpImpresora.activateControl("SALTOHOJA",iDriver);
		}
		return intStatus;
	}
}