import java.util.*;

public class Ficha {
	ToolsPrinter tpImpresora = new ToolsPrinter();
	int intStatus = 0;
	int intPagina = 1;
	String strFiller =
		"                                                                      ";
	String strOut = "";
	String strDivisa = " ";
	String strCuenta = " ";
	String strSerial = " ";
	String strSucursal = " ";

	public int imprimeFichaCobro(
		String strRespuesta,
		String strCajero,
		String strTitular,
		String strDate,
		int iDriver) {
		int intSize = 0;
		int intDoctos = 0;
		int intContador = 0;
		int intQuerys = 0;
		long longMontoAcum = 0;
		long longMontoTot = 0;
		Vector vCampos = new Vector();
		Vector vFicha = new Vector();
		Vector vBancos = new Vector();
		Vector vTmp = new Vector();
		String strTmp = new String();

		intStatus = tpImpresora.verificaStatus();
		if ((intStatus == 95) || (intStatus == 88)) {
			intStatus = 0;
			intDoctos = 5;
			vCampos = tpImpresora.Campos(strRespuesta, "^");
			intSize = vCampos.size();
			for (int i = 0; i < intSize; i++) {
				strTmp = (String) vCampos.elementAt(i);
				if (strTmp.startsWith("DATOS_FICHA")) {
					strTmp = (String) vCampos.elementAt(i);
					vTmp = tpImpresora.Campos(strTmp, "@");
					strTmp = (String) vTmp.elementAt(1);
					vFicha = tpImpresora.Campos(strTmp, "~");
				}
				if (strTmp.startsWith("DATOS_BANCO")) {
					vTmp = tpImpresora.Campos(strTmp, "@");
					strTmp = (String) vTmp.elementAt(1);
					strTmp = strTmp.substring(3, strTmp.length() - 2);
					vBancos.add(strTmp);
				}
			}
			intQuerys = (vFicha.size() - 1) / 8;
			tpImpresora.activateControl("CERTIFICACION",iDriver);
			intContador = 1;
			String strSigno = "$";
			for (int i = 0; i < intQuerys; i++) {
				String strMontoFormat = "";
				String strBanco = "";
				String strConsecutivo = (String) vFicha.elementAt(2 + (8 * i));
				String strDescripcion = (String) vFicha.elementAt(3 + (8 * i));
				String strMonto = (String) vFicha.elementAt(4 + (8 * i));
				strCuenta = (String) vFicha.elementAt(1 + (8 * i));
				if (strDescripcion.length() > 36)
					strSerial = strDescripcion.substring(30, 37);
				else
					strSerial = "       ";
				strSucursal = (String) vFicha.elementAt(5 + (8 * i));
				if (i < vBancos.size())
					strBanco = (String) vBancos.elementAt(i);
				else
					strBanco = " ";
				if (i == 0) {
					strDivisa = (String) vFicha.elementAt(0);
					if (strDivisa.length() > 0)
						strDivisa = strDivisa.trim();
					if ((strDivisa.equals("N$"))
						|| (strDivisa.equals("&N$"))) {
						strDivisa = "MONEDA NACIONAL";
						strSigno = "$";
					} else {
						strDivisa = "DOLARES";
						strSigno = "$";
					}
				}
				if (intContador == 1) {
					strOut =
						"                  FICHA DE DEPOSITO DE COBRO INMEDIATO           PAG. "
							+ intPagina;
					tpImpresora.enviaCadena(strOut);
					strOut =
						"                                                          "
							+ strDivisa;
					tpImpresora.enviaCadena(strOut);
					strOut =
						"                                                       FECHA : "
							+ strDate.substring(7, 9)
							+ "/"
							+ strDate.substring(5, 7)
							+ "/"
							+ strDate.substring(1, 5);
					tpImpresora.enviaCadena(strOut);
					strOut =
						"TITULAR : "
							+ strTitular
							+ strFiller.substring(0, 30 - strTitular.length())
							+ "              CUENTA : "
							+ strCuenta;
					tpImpresora.enviaCadena(strOut);
					tpImpresora.enviaCadena(
						"_________________________________________________________________________");
					tpImpresora.enviaCadena(
						"    HORA           BANCO                      SERIAL          MONTO      ");
					tpImpresora.enviaCadena(" ");
					longMontoAcum = 0;
				}
				longMontoTot = longMontoTot + Long.parseLong(strMonto);
				longMontoAcum = longMontoAcum + Long.parseLong(strMonto);
				strMontoFormat = tpImpresora.formatMonto(strMonto);
				if (strBanco.length() > 0)
					strBanco = strBanco.replace('�', 'e');
				strOut =
					"  "
						+ strConsecutivo
						+ "      "
						+ strBanco
						+ strFiller.substring(0, 30 - strBanco.length())
						+ strSerial
						+ " "
						+ strSigno
						+ strFiller.substring(0, 17 - strMontoFormat.length())
						+ strMontoFormat;
				tpImpresora.enviaCadena(strOut);
				String strTotalLetra;
				if (i < (intQuerys - 1)) {
					if (intContador == intDoctos) {
						strTotalLetra =
							tpImpresora.formatMonto(
								new Long(longMontoAcum).toString());
						tpImpresora.enviaCadena(
							"                                            --------    -----------------   ");
						strOut =
							"                                               "
								+ intContador
								+ "     "
								+ strSigno
								+ strFiller.substring(
									0,
									17 - strTotalLetra.length())
								+ strTotalLetra;
						tpImpresora.enviaCadena(strOut);
						longMontoAcum = 0;
						strOut =
							" SUCURSAL: "
								+ strSucursal
								+ "   CAJERO: "
								+ strCajero;
						tpImpresora.enviaCadena(strOut);
						tpImpresora.enviaCadena(
							"_________________________________________________________________________");
						tpImpresora.enviaCadena(
							"  La recepci�n de documentos a cargo de otras instituciones, para abono  ");
						tpImpresora.enviaCadena(
							"  en cuenta, se sujeta a que re�nan los requisitos para su presentaci�n  ");
						tpImpresora.enviaCadena(
							"  en la C�mara de Compensaci�n Electr�nica.                              ");
						tpImpresora.enviaCadena(
							"  Este recibo  s�lo sera  v�lido cuando  figure en el, la certificaci�n ");
						tpImpresora.enviaCadena(
							"  de nuestro sistema, sello y firma del cajero (los cheques se reciben  ");
						tpImpresora.enviaCadena(
							"  salvo buen cobro).                                         ");
						tpImpresora.enviaCadena(
							"  Con la  recepci�n de este comprobante el  depositante acepta  que los  ");
						tpImpresora.enviaCadena(
							"  datos aqui contenidos son correctos                                    ");
						if (iDriver == 0) {
							tpImpresora.InserteDocumento();
							tpImpresora.activateControl("SELECCIONASLIP",iDriver);
							tpImpresora.enviaCadena("");
							tpImpresora.enviaCadena("");
							tpImpresora.enviaCadena("");
							tpImpresora.enviaCadena("");
							tpImpresora.enviaCadena("");
							tpImpresora.imprimeCadena1();
							tpImpresora.activateControl("SALTOHOJA",iDriver);
							tpImpresora.activateControl("SELECCIONAJOURNAL",iDriver);
						} else {
							tpImpresora.activateControl("SALTOHOJA",iDriver);
							tpImpresora.muestraVentana();
						}
						intContador = 0;
						intPagina++;
					}
					intContador++;
				} else {
					strTotalLetra =
						tpImpresora.formatMonto(
							new Long(longMontoAcum).toString());
					if (intQuerys > 1) {
						tpImpresora.enviaCadena(
							"                                            --------    -----------------   ");
						strOut =
							"                                               "
								+ intContador
								+ "     "
								+ strSigno
								+ strFiller.substring(
									0,
									17 - strTotalLetra.length())
								+ strTotalLetra;
						tpImpresora.enviaCadena(strOut);
						longMontoAcum = 0;
					}
					strTotalLetra =
						tpImpresora.formatMonto(
							new Long(longMontoTot).toString());
					tpImpresora.enviaCadena(
						"                                            ========    =================   ");
					String strEsp = "     ";
					if (intQuerys > 10)
						strEsp = "    ";
					strOut =
						" TOTAL DEPOSITADO A LA CUENTA DE CHEQUES :     "
							+ intQuerys
							+ strEsp
							+ strSigno
							+ strFiller.substring(0, 17 - strTotalLetra.length())
							+ strTotalLetra;
					tpImpresora.enviaCadena(strOut);
					strOut =
						" SUCURSAL: " + strSucursal + "   CAJERO: " + strCajero;
					tpImpresora.enviaCadena(strOut);
					tpImpresora.enviaCadena(
						"_________________________________________________________________________");
					tpImpresora.enviaCadena(
						"  La recepci�n de documentos a cargo de otras instituciones, para abono  ");
					tpImpresora.enviaCadena(
						"  en cuenta, se sujeta a que re�nan los requisitos para su presentaci�n  ");
					tpImpresora.enviaCadena(
						"  en la C�mara de Compensaci�n Electr�nica.                              ");
					tpImpresora.enviaCadena(
						"  Este recibo  s�lo sera  v�lido cuando  figure en el, la certificaci�n ");
					tpImpresora.enviaCadena(
						"  de nuestro sistema, sello y firma del cajero (los cheques se reciben  ");
					tpImpresora.enviaCadena(
						"  salvo buen cobro).                                         ");
					tpImpresora.enviaCadena(
						"  Con la  recepci�n de este comprobante el  depositante acepta  que los  ");
					tpImpresora.enviaCadena(
						"  datos aqui contenidos son correctos                                    ");
					if (iDriver == 0) {
						tpImpresora.InserteDocumento();
						tpImpresora.activateControl("SELECCIONASLIP",iDriver);
						tpImpresora.enviaCadena("");
						tpImpresora.enviaCadena("");
						tpImpresora.enviaCadena("");
						tpImpresora.enviaCadena("");
						tpImpresora.enviaCadena("");
						tpImpresora.imprimeCadena1();
						tpImpresora.activateControl("SALTOHOJA",iDriver);
						tpImpresora.activateControl("SELECCIONAJOURNAL",iDriver);
					} else
						tpImpresora.activateControl("SALTOHOJA",iDriver);
				}
			}
		}
		return intStatus;
	}

	public int imprimeFichaRemesa(
		String strRespuesta,
		String strCajero,
		String strTitular,
		String strDate,
		int iDriver) {
		int intSize = 0;
		int intDoctos = 0;
		int intContador = 0;
		int intQuerys = 0;
		long longMontoAcum = 0;
		long longMontoTot = 0;
		Vector vCampos = new Vector();
		Vector vFicha = new Vector();
		Vector vBancos = new Vector();
		Vector vTmp = new Vector();
		String strTmp = new String();

		intStatus = tpImpresora.verificaStatus();
		if ((intStatus == 95) || (intStatus == 88)) {
			intStatus = 0;
			intDoctos = 8;
			vCampos = tpImpresora.Campos(strRespuesta, "^");
			intSize = vCampos.size();
			for (int i = 0; i < intSize; i++) {
				strTmp = (String) vCampos.elementAt(i);
				if (strTmp.startsWith("DATOS_REMESA")) {
					strTmp = (String) vCampos.elementAt(i);
					vTmp = tpImpresora.Campos(strTmp, "@");
					strTmp = (String) vTmp.elementAt(1);
					vFicha = tpImpresora.Campos(strTmp, "~");
					//System.out.println("vFicha" + vFicha + "size " + vFicha.size());
				}
				if (strTmp.startsWith("DATOS_BANCO")) {
					vTmp = tpImpresora.Campos(strTmp, "@");
					strTmp = (String) vTmp.elementAt(1);
					vTmp = tpImpresora.Campos(strTmp, "~");
					strTmp = (String) vTmp.elementAt(0);
					strTmp = strTmp.substring(3, strTmp.length());
					vBancos.add(strTmp);
				}
			}
			intQuerys = (vFicha.size() - 1) / 8;
			tpImpresora.activateControl("CERTIFICACION",iDriver);
			intContador = 1;
			for (int i = 0; i < intQuerys; i++) {
				String strMontoFormat = "";
				String strBanco = "";
				String strConsecutivo = (String) vFicha.elementAt(2 + (8 * i));
				//String strDescripcion = (String) vFicha.elementAt(3 + (8 * i));
				String strMonto = (String) vFicha.elementAt(4 + (8 * i));
				strDivisa = (String) vFicha.elementAt(0 + (8 * i));
				strDivisa = strDivisa.trim();
				strCuenta = (String) vFicha.elementAt(1 + (8 * i));
				strSerial = (String) vFicha.elementAt(6 + (8 * i));
				strSucursal = (String) vFicha.elementAt(5 + (8 * i));
				if (vBancos.size() > i)
					strBanco = (String) vBancos.elementAt(i);
				else
					strBanco = " ";
				if ((strDivisa.equals("N$")) || (strDivisa.equals("&N$")))
					strDivisa = "MONEDA NACIONAL";
				else
					strDivisa = "DOLARES";
				if (intContador == 1) {
					strOut =
						"             FICHA DE DEPOSITO DE CHEQUES EXTRANJEROS             PAG. "
							+ intPagina;
					tpImpresora.enviaCadena(strOut);
					strOut =
						"                                                          "
							+ strDivisa;
					tpImpresora.enviaCadena(strOut);
					strOut =
						"                                                       FECHA : "
							+ strDate.substring(7, 9)
							+ "/"
							+ strDate.substring(5, 7)
							+ "/"
							+ strDate.substring(1, 5);
					tpImpresora.enviaCadena(strOut);
					strOut =
						"TITULAR:  "
							+ strTitular
							+ strFiller.substring(0, 30 - strTitular.length())
							+ "             CUENTA:  "
							+ strCuenta;
					tpImpresora.enviaCadena(strOut);
					tpImpresora.enviaCadena(
						"_________________________________________________________________________");
					tpImpresora.enviaCadena(
						"    HORA           BANCO                      SERIAL          MONTO      ");
					tpImpresora.enviaCadena(" ");
					longMontoAcum = 0;
				}
				longMontoTot = longMontoTot + Long.parseLong(strMonto);
				longMontoAcum = longMontoAcum + Long.parseLong(strMonto);
				strMontoFormat = tpImpresora.formatMonto(strMonto);
				if (strBanco.length() > 30)
					strBanco = strBanco.substring(0, 30);
				if (strMontoFormat.length() < 18)
					strOut =
						"  "
							+ strConsecutivo
							+ "      "
							+ strBanco
							+ strFiller.substring(0, 30 - strBanco.length())
							+ strSerial
							+ " "
							+ strFiller.substring(
								0,
								17 - strMontoFormat.length())
							+ strMontoFormat;
				else
					strOut =
						"  "
							+ strConsecutivo
							+ "      "
							+ strBanco
							+ strFiller.substring(0, 30 - strBanco.length())
							+ strSerial
							+ " "
							+ strMontoFormat;
				tpImpresora.enviaCadena(strOut);
				intContador++;
				if (intContador == intDoctos) {
					String strTotalLetra;
					strTotalLetra =
						tpImpresora.formatMonto(
							new Long(longMontoAcum).toString());
					tpImpresora.enviaCadena(
						"                                                          ---------------   ");
					strOut =
						"                                                       "
							+ strFiller.substring(0, 18 - strTotalLetra.length())
							+ strTotalLetra;
					tpImpresora.enviaCadena(strOut);
					longMontoAcum = 0;
					strOut =
						" SUCURSAL: " + strSucursal + "   CAJERO: " + strCajero;
					tpImpresora.enviaCadena(strOut);
					tpImpresora.enviaCadena(
						"_________________________________________________________________________");
					/*tpImpresora.enviaCadena(
						"  Los importes de COMISION e IVA cargados por su dep�sito aparecer�n en  ");
					tpImpresora.enviaCadena(
						"  su pr�ximo Estado de Cuenta.                                           ");
					tpImpresora.enviaCadena(
						"  En caso de devoluci�n de cheques en d�lares americanos pagaderos en los");
					tpImpresora.enviaCadena(
						"  E.U.A. , se  entregar�  copia   del   documento  original ,  seg�n  las");
					tpImpresora.enviaCadena(
						"  disposiciones legales del mencionado pa�s (Ley Cheque 21).");*/
					if (iDriver == 0) {
						tpImpresora.InserteDocumento();
						tpImpresora.activateControl("SELECCIONASLIP",iDriver);
						tpImpresora.enviaCadena("");
						tpImpresora.enviaCadena("");
						tpImpresora.enviaCadena("");
						tpImpresora.enviaCadena("");
						tpImpresora.enviaCadena("");
						tpImpresora.imprimeCadena1();
						tpImpresora.activateControl("SALTOHOJA",iDriver);
						tpImpresora.activateControl("SELECCIONAJOURNAL",iDriver);
					} else {
						tpImpresora.activateControl("SALTOHOJA",iDriver);
						tpImpresora.muestraVentana();
					}
					intContador = 1;
					intPagina++;
				}
			}
			if (intContador == 1) {
				strOut =
					"                   FICHA DE DEPOSITO DE REMESAS                    PAG. "
						+ intPagina;
				tpImpresora.enviaCadena(strOut);
				strOut =
					"                                                          "
						+ strDivisa;
				tpImpresora.enviaCadena(strOut);
				strOut =
					"                                                       FECHA:  "
						+ strDate.substring(7, 9)
						+ "/"
						+ strDate.substring(5, 7)
						+ "/"
						+ strDate.substring(1, 5);
				tpImpresora.enviaCadena(strOut);
				strOut =
					"TITULAR:  "
						+ strTitular
						+ strFiller.substring(0, 30 - strTitular.length())
						+ "              CUENTA:  "
						+ strCuenta;
				tpImpresora.enviaCadena(strOut);
				tpImpresora.enviaCadena(
					"_________________________________________________________________________");
				tpImpresora.enviaCadena(
					"    HORA           BANCO                      SERIAL              MONTO      ");
				tpImpresora.enviaCadena(" ");
				longMontoAcum = 0;
			}
			if (intContador != intDoctos) {
				String totalletra;
				totalletra =
					tpImpresora.formatMonto(new Long(longMontoAcum).toString());
				tpImpresora.enviaCadena(
					"                                                          ---------------   ");
				strOut =
					"                                                       "
						+ strFiller.substring(0, 18 - totalletra.length())
						+ totalletra;
				longMontoAcum = 0;
				tpImpresora.enviaCadena(strOut);
				totalletra =
					tpImpresora.formatMonto(new Long(longMontoTot).toString());
				tpImpresora.enviaCadena(
					"                                                          ===============   ");
				strOut =
					" TOTAL DEPOSITADO A LA CUENTA DE CHEQUES:              "
						+ strFiller.substring(0, 18 - totalletra.length())
						+ totalletra;
				tpImpresora.enviaCadena(strOut);
				strOut =
					" SUCURSAL: " + strSucursal + "   CAJERO: " + strCajero;
				tpImpresora.enviaCadena(strOut);
				tpImpresora.enviaCadena(
					"_________________________________________________________________________");
				/*tpImpresora.enviaCadena(
					"  Los importes de COMISION e IVA cargados por su dep�sito aparecer�n en  ");
				tpImpresora.enviaCadena(
					"  su pr�ximo Estado de Cuenta.                                           ");
				tpImpresora.enviaCadena(
					"  En caso de devoluci�n de cheques en d�lares americanos pagaderos en los");
				tpImpresora.enviaCadena(
					"  E.U.A. , se  entregar�  copia   del   documento  original ,  seg�n  las");
				tpImpresora.enviaCadena(
					"  disposiciones legales del mencionado pa�s (Ley Cheque 21).");*/
				if (iDriver == 0) {
				tpImpresora.InserteDocumento();
				tpImpresora.activateControl("SELECCIONASLIP",iDriver);
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.imprimeCadena1();
				tpImpresora.activateControl("SALTOHOJA",iDriver);
				tpImpresora.activateControl("SELECCIONAJOURNAL",iDriver);
				} else
					tpImpresora.activateControl("SALTOHOJA",iDriver);
			}
		}
		return intStatus;
	}

	public int imprimeFichaCV(
		String strCadena,
		String strRespuesta,
		String strCajero,
		String strDate,
		int iDriver) {
		Vector vCampos = new Vector();
		Vector vTmp = new Vector();
		String strTmp = new String();
		String strBanco = new String();

		intStatus = tpImpresora.verificaStatus();
		if ((intStatus == 95) || (intStatus == 88)) {
			vCampos = tpImpresora.Campos(strRespuesta, "^");
			int intSize = vCampos.size();
			for (int i = 0; i < intSize; i++) {
				strTmp = (String) vCampos.elementAt(i);
				if (strTmp.startsWith("DATOS_BANCO")) {
					vTmp = tpImpresora.Campos(strTmp, "@");
					strTmp = (String) vTmp.elementAt(1);
					//System.out.println("strTmp "  + strTmp);																												
					strBanco = strTmp.substring(3, strTmp.length() - 2);
				}
			}
			vCampos = tpImpresora.Campos(strCadena, "~");
			tpImpresora.activateControl("CERTIFICACION",iDriver);
			//String strFormato = (String) vCampos.elementAt(0);
			String strTitular = (String) vCampos.elementAt(1);
			String strCuenta = (String) vCampos.elementAt(3);
			String strMoneda = (String) vCampos.elementAt(2);
			String strConsecutivo = (String) vCampos.elementAt(4);
			String strSerial = (String) vCampos.elementAt(5);
			String strMonto1 = "";
			String strMonto2 = "";
			if (strMoneda.equals("01")) {
				strMonto1 = (String) vCampos.elementAt(6);
				strMonto2 = (String) vCampos.elementAt(7);
			} else {
				strMonto1 = (String) vCampos.elementAt(7);
				strMonto2 = (String) vCampos.elementAt(6);
			}
			String strTipocambio = (String) vCampos.elementAt(8);
			String strDivisa = new String("");
			String strTipo1 = new String("");
			String strTipo2 = new String("");
			if (strMoneda.equals("01")) {
				strDivisa = "MONEDA NACIONAL";
				strTipo1 = "$";
				strTipo2 = "USD";
			} else {
				strDivisa = "MONEDA EN DOLARES";
				strTipo1 = "USD";
				strTipo2 = "$";
			}
			tpImpresora.enviaCadena(
				"                 FICHA DE DEVOLUCION DE COBRO INMEDIATO                  ");
			strOut =
				"                                                          "
					+ strDivisa;
			tpImpresora.enviaCadena(strOut);
			strOut =
				"                                                          "
					+ strDate.substring(7, 9)
					+ "/"
					+ strDate.substring(5, 7)
					+ "/"
					+ strDate.substring(1, 5);
			tpImpresora.enviaCadena(strOut);
			strOut =
				"TITULAR : "
					+ strTitular
					+ strFiller.substring(0, 40 - strTitular.length())
					+ " CUENTA : "
					+ strCuenta;
			tpImpresora.enviaCadena(strOut);
			tpImpresora.enviaCadena(
				"_________________________________________________________________________");
			tpImpresora.enviaCadena(
				"    HORA                                      SERIAL          MONTO      ");
			tpImpresora.enviaCadena(" ");
			strOut =
				"  "
					+ strConsecutivo
					+ "      "
					+ strBanco
					+ strFiller.substring(0, 30 - strBanco.length())
					+ strSerial
					+ strFiller.substring(0, 15 - strMonto1.length())
					+ strTipo1
					+ " "
					+ strMonto1;
			tpImpresora.enviaCadena(strOut);
			tpImpresora.enviaCadena(
				"                                            ==========      =============");
			strOut =
				"                                                1  "
					+ strFiller.substring(0, 18 - strMonto1.length())
					+ strTipo1
					+ " "
					+ strMonto1;
			tpImpresora.enviaCadena(strOut);
			strOut =
				"              TOTAL DEPOSITADO A LA CUENTA DE CHEQUES : "
					+ strTipo2
					+ " "
					+ strMonto2;
			tpImpresora.enviaCadena(strOut);
			strOut =
				" SUCURSAL: "
					+ strCajero.substring(0, 4)
					+ "   CAJERO: "
					+ strCajero
					+ "  TIPO DE CAMBIO "
					+ strTipocambio;
			tpImpresora.enviaCadena(strOut);
			tpImpresora.enviaCadena(
				"_________________________________________________________________________");
			if (iDriver == 0) {
				tpImpresora.InserteDocumento();
				tpImpresora.activateControl("SELECCIONASLIP",iDriver);
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.imprimeCadena1();
				tpImpresora.activateControl("SALTOHOJA",iDriver);
				tpImpresora.activateControl("SELECCIONAJOURNAL",iDriver);
			} else
				tpImpresora.activateControl("SALTOHOJA",iDriver);
		}
		return intStatus;
	}

	public int imprimeFichaRem(
		String strCadena,
		String strRespuesta,
		String strCajero,
		String strDate,
		int iDriver) {
		Vector vCampos = new Vector();
		Vector vTmp = new Vector();
		String strTmp = new String();
		String strBanco = new String();

		intStatus = tpImpresora.verificaStatus();
		if ((intStatus == 95) || (intStatus == 88)) {
			vCampos = tpImpresora.Campos(strRespuesta, "^");
			int intSize = vCampos.size();
			for (int i = 0; i < intSize; i++) {
				strTmp = (String) vCampos.elementAt(i);
				if (strTmp.startsWith("DATOS_BANCO")) {
					vTmp = tpImpresora.Campos(strTmp, "@");
					strTmp = (String) vTmp.elementAt(1);
					strBanco = strTmp.substring(3, strTmp.length() - 2);
				}
			}
			vCampos = tpImpresora.Campos(strCadena, "~");
			tpImpresora.activateControl("CERTIFICACION",iDriver);
			String strTitular = (String) vCampos.elementAt(2);
			String strCuenta = (String) vCampos.elementAt(3);
			String strMoneda = (String) vCampos.elementAt(4);
			String strConsecutivo = (String) vCampos.elementAt(5);
			String strSerial = (String) vCampos.elementAt(6);
			String strMonto1 = "";
			String strMonto2 = "";
			if (strMoneda.equals("01")) {
				strMonto1 = (String) vCampos.elementAt(7);
				strMonto2 = (String) vCampos.elementAt(8);
			} else {
				strMonto1 = (String) vCampos.elementAt(8);
				strMonto2 = (String) vCampos.elementAt(7);
			}
			String strTipocambio = (String) vCampos.elementAt(9);
			String strDivisa = new String("");
			String strTipo1 = new String("");
			String strTipo2 = new String("");
			if (strMoneda.equals("01")) {
				strDivisa = "MONEDA NACIONAL";
				strTipo1 = "$";
				strTipo2 = "USD";
			} else {
				strDivisa = "MONEDA  DOLARES";
				strTipo1 = "USD";
				strTipo2 = "$";
			}
			strOut =
				strFiller.substring(0, 22) + "FICHA DE DEPOSITO DE REMESAS";
			tpImpresora.enviaCadena(strOut);
			strOut = strFiller.substring(0, 57) + strDivisa;
			tpImpresora.enviaCadena(strOut);
			strOut =
				strFiller.substring(0, 62)
					+ strDate.substring(7, 9)
					+ "/"
					+ strDate.substring(5, 7)
					+ "/"
					+ strDate.substring(1, 5);
			tpImpresora.enviaCadena(strOut);
			strOut = "  TITULAR : " + strTitular;
			tpImpresora.enviaCadena(strOut);
			strOut = "  CUENTA  : " + strCuenta;
			tpImpresora.enviaCadena(strOut);
			tpImpresora.enviaCadena(
				"________________________________________________________________________");
			tpImpresora.enviaCadena(
				"   HORA               BANCO               SERIAL            MONTO      ");
			tpImpresora.enviaCadena("  ");
			strOut =
				" "
					+ strConsecutivo
					+ "  "
					+ strBanco
					+ strFiller.substring(0, 31 - strBanco.length())
					+ " "
					+ strSerial
					+ " "
					+ strFiller.substring(0, 16 - strMonto1.length())
					+ strTipo1
					+ " "
					+ strMonto1;
			tpImpresora.enviaCadena(strOut);
			tpImpresora.enviaCadena(
				"                                         ==========    ================");
			strOut =
				"                                             1      "
					+ strFiller.substring(0, 16 - strMonto1.length())
					+ strTipo1
					+ " "
					+ strMonto1;
			tpImpresora.enviaCadena(strOut);
			strOut =
				"  TOTAL DEPOSITADO A LA CUENTA DE CHEQUES: "
					+ strTipo2
					+ " "
					+ strMonto2;
			tpImpresora.enviaCadena(strOut);
			strOut =
				"  SUCURSAL: "
					+ strCajero.substring(0, 4)
					+ "   CAJERO: "
					+ strCajero
					+ "  TIPO DE CAMBIO: "
					+ strTipocambio;
			tpImpresora.enviaCadena(strOut);
			tpImpresora.enviaCadena(
				"________________________________________________________________________");
			/*tpImpresora.enviaCadena(
				"  Los importes de  COMISION  e  IVA  cargados por su dep�sito aparecer�n");
			tpImpresora.enviaCadena(
				"  en su pr�ximo Estado de Cuenta.  Las devoluciones de  documentos ser�n");
			tpImpresora.enviaCadena(
				"  cargados a su cuenta al tipo de cambio a la venta vigente el dia de la");
			tpImpresora.enviaCadena(
				"  devoluci�n. En  caso de  devoluci�n de  cheques en  d�lares americanos");
			tpImpresora.enviaCadena(
				"  pagaderos en los  E. U. A. se entregar�  copia del documento original,");
			tpImpresora.enviaCadena(
				"  seg�n las disposiciones legales del mencionado pa�s ( Ley Cheque 21 )");*/
			if (iDriver == 0) {
				tpImpresora.InserteDocumento();
				tpImpresora.activateControl("SELECCIONASLIP",iDriver);
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.imprimeCadena1();
				tpImpresora.activateControl("SALTOHOJA",iDriver);
				tpImpresora.activateControl("SELECCIONAJOURNAL",iDriver);
			} else
				tpImpresora.activateControl("SALTOHOJA",iDriver);
		}
		return intStatus;
	}

	public int imprimeFicha4009(
		String strCadena,
		String strCajero,
		String strDate,
		int iDriver) {
		Vector vCampos = new Vector();

		intStatus = tpImpresora.verificaStatus();
		if ((intStatus == 95) || (intStatus == 88)) {
			intStatus = 0;
			vCampos = tpImpresora.Campos(strCadena, "~");
			tpImpresora.activateControl("CERTIFICACION",iDriver);
			String strMoneda = (String) vCampos.elementAt(1);
			String strTitular = (String) vCampos.elementAt(2);
			if (!strTitular.equals(""))
				strTitular = strTitular.trim();
			String strCuenta = (String) vCampos.elementAt(3);
			String strConsecutivo = (String) vCampos.elementAt(4);
			String strDoctos = (String) vCampos.elementAt(5);
			String strMonto = (String) vCampos.elementAt(6);
			strMonto = tpImpresora.formatMonto(strMonto);
			tpImpresora.enviaCadena(
				"                FICHA DE DEPOSITO DE COBRO INMEDIATO               PAG. 1\n");
			if (strMoneda.equals("01")) {
				tpImpresora.enviaCadena(
					"                                                        (MONEDA NACIONAL)\n");
				strMoneda = "$";
			} else {
				tpImpresora.enviaCadena(
					"                                                                (DOLARES)\n");
				strMoneda = "USD";
			}
			strOut =
				"                                                               "
					+ strDate.substring(7, 9)
					+ "/"
					+ strDate.substring(5, 7)
					+ "/"
					+ strDate.substring(1, 5);
			tpImpresora.enviaCadena(strOut);
			strOut =
				"TITULAR : "
					+ strTitular
					+ strFiller.substring(0, 41 - strTitular.length())
					+ "   CUENTA : "
					+ strCuenta;
			tpImpresora.enviaCadena(strOut);
			tpImpresora.enviaCadena(
				"_________________________________________________________________________");
			tpImpresora.enviaCadena(
				"    HORA            B A N C O              DOCTOS              MONTO   \n");
			strOut =
				"  "
					+ strConsecutivo
					+ "      DEPOSITO GLOBAL C.I.          "
					+ strFiller.substring(0, 4 - strDoctos.length())
					+ strDoctos
					+ "    "
					+ strMoneda
					+ strFiller.substring(0, 18 - strMonto.length())
					+ strMonto
					+ "\n";
			tpImpresora.enviaCadena(strOut);
			strOut =
				strFiller.substring(0, 45)
					+ "---- "
					+ "    "
					+ "--------------------"
					+ "\n";
			tpImpresora.enviaCadena(strOut);
			strOut =
				" TOTAL DEPOSITADO A LA CUENTA DE CHEQUES :  "
					+ strFiller.substring(0, 4 - strDoctos.length())
					+ strDoctos
					+ "   "
					+ strMoneda
					+ strFiller.substring(0, 18 - strMonto.length())
					+ strMonto;
			tpImpresora.enviaCadena(strOut);
			strOut =
				" SUCURSAL: "
					+ strCajero.substring(0, 4)
					+ "   CAJERO: "
					+ strCajero;
			tpImpresora.enviaCadena(strOut);
			tpImpresora.enviaCadena(
				"_________________________________________________________________________");
			tpImpresora.enviaCadena(
				"  La recepci�n de documentos a cargo de otras instituciones,para abono en");
			tpImpresora.enviaCadena(
				"  cuenta se sujeta a que reunan los requisitos para su presentaci�n en la");
			tpImpresora.enviaCadena(
				"  C�mara de Compensaci�n Electr�nica.                                    ");
			tpImpresora.enviaCadena(
				"  El importe del Dep�sito SBC  que describe este recibo, corresponde a la");
			tpImpresora.enviaCadena(
				"  suma  de  los  documentos  presentados  para  su dep�sito.  En caso  de");
			tpImpresora.enviaCadena(
				"  devoluci�n  de alguno(s)  documento(s),  se cargar�  a  la  cuenta  del");
			tpImpresora.enviaCadena(
				"  cliente el monto de cada documento devuelto.                           ");
			tpImpresora.enviaCadena(
				"  Con  la  recepci�n de  este comprobante  el  depositante acepta que los");
			tpImpresora.enviaCadena(
				"  datos aqui contenidos son correctos                                    ");
			tpImpresora.enviaCadena(
				"  Este  recibo s�lo ser� v�lido  cuando figure en el, la certificaci�n de");
			tpImpresora.enviaCadena(
				" nuestro sistema, sello y firma del cajero ( los cheques se reciben salvo");
			tpImpresora.enviaCadena("  buen cobro).");
			if (iDriver == 0) {
				tpImpresora.InserteDocumento();
				tpImpresora.activateControl("SELECCIONASLIP",iDriver);
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.imprimeCadena1();
				tpImpresora.activateControl("SALTOHOJA",iDriver);
				tpImpresora.activateControl("SELECCIONAJOURNAL",iDriver);
			} else
				tpImpresora.activateControl("SALTOHOJA",iDriver);
		}
		return intStatus;
	}

	public int imprimeWesterUnion(
		Vector vDatos,
		String strDate,
		String strDatosSuc,
		int iDriver,
		String strIdentidad) {
		String strFiller =
			"                                                                                ";
		intStatus = tpImpresora.verificaStatus();
		if ((intStatus == 95) || (intStatus == 88)) {
			intStatus = 0;
			tpImpresora.activateControl("CERTIFICACION",iDriver);
			Vector vDatosSuc = tpImpresora.Campos(strDatosSuc, "@");
			String strNumSuc = (String) vDatosSuc.elementAt(1);
			strNumSuc = strNumSuc.substring(7, strNumSuc.length());
			String strNomSuc = (String) vDatosSuc.elementAt(0);
			strNomSuc = strNomSuc.substring(7, strNomSuc.length());
			String strDomSuc = (String) vDatosSuc.elementAt(2);
			strDomSuc = strDomSuc.substring(7, strDomSuc.length());
			String strNomPza = (String) vDatosSuc.elementAt(4);
			strNomPza = strNomPza.substring(7, strNomPza.length());
			String strNombre = (String) vDatos.elementAt(1);
			if (strNombre.equals(""))
				strNombre = " ";
			String strImporte = (String) vDatos.elementAt(2);
			if (strImporte.equals(""))
				strImporte = "000";
			strImporte = tpImpresora.formatMonto(strImporte.trim());
			String strCuenta = (String) vDatos.elementAt(3);
			if (strCuenta.equals(""))
				strCuenta = " ";
			String strOutline = new String("");
			tpImpresora.enviaCadena("");
			strOutline = strFiller.substring(0, 28) + "DINEROS EN MINUTOS";
			tpImpresora.enviaCadena(strOutline);
			if (strCuenta.equals(" ")) {
				strOutline =
					strFiller.substring(0, 29) + "PAGO EN EFECTIVO" + "\n\n";
				tpImpresora.enviaCadena(strOutline);
				strOutline =
					"FECHA : "
						+ strDate.substring(7, 9)
						+ "/"
						+ strDate.substring(5, 7)
						+ "/"
						+ strDate.substring(1, 5);
			} else {
				strOutline =
					strFiller.substring(0, 29) + " ABONO A CUENTA" + "\n\n";
				tpImpresora.enviaCadena(strOutline);
				strOutline =
					"  FECHA : "
						+ strDate.substring(7, 9)
						+ "/"
						+ strDate.substring(5, 7)
						+ "/"
						+ strDate.substring(1, 5)
						+ strFiller.substring(0, 20)
						+ "No. CUENTA :"
						+ strCuenta;
			}
			tpImpresora.enviaCadena(strOutline);
			strOutline = "  NOMBRE DEL BENEFICIARIO : " + strNombre;
			tpImpresora.enviaCadena(strOutline);
			if (strCuenta.equals(" "))
				strOutline = " MONTO DEL EFECTIVO PAGADO : " + strImporte;
			else
				strOutline = "  MONTO DEL EFECTIVO ABONADO : " + strImporte;
			tpImpresora.enviaCadena(strOutline);
			strOutline =
				"  MONTO EN LETRA : " + Currency.convert(strImporte, 1);
			tpImpresora.enviaCadena(strOutline);
			strOutline =
				"  SUCURSAL : " + strNumSuc + "  " + strNomSuc + "\n\n\n";
			tpImpresora.enviaCadena(strOutline);
			strOutline =
				"                    BENEFICIARIO                          "+strIdentidad+" PANAMA "
					+ "\n\n\n\n";
			tpImpresora.enviaCadena(strOutline);
			strOutline =
				"                _____________________               ______________________";
			tpImpresora.enviaCadena(strOutline);
			strOutline =
				"                RECIBI DE CONFORMIDAD               AUTORIZO SELLO Y FIRMA";
			tpImpresora.enviaCadena(strOutline);
			if (iDriver == 0) {
				tpImpresora.InserteDocumento();
				tpImpresora.activateControl("SELECCIONASLIP",iDriver);
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.imprimeCadena1();
				tpImpresora.activateControl("SALTOHOJA",iDriver);
				tpImpresora.activateControl("SELECCIONAJOURNAL",iDriver);
			} else {
				tpImpresora.activateControl("SALTOHOJA",iDriver);
			}
		}
		return intStatus;
	}
}
