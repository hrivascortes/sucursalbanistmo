import java.util.*;

public class Nomina {
	String strFiller = "                                                  ";

	public int imprimeNomina(
		Vector vDatos,
		String strRespuesta,
		String strDate,
		String strTime,
		int iDriver,
		String strIdentidad) {
		ToolsPrinter tpImpresora = new ToolsPrinter();
		int intStatus = tpImpresora.verificaStatus();
		int i = 0;
		int intSize = 0;

		if ((intStatus == 95) || (intStatus == 88)) {
			intStatus = 0;
			String strOut = "";
			String strTmp = "";
			String strMontoCte = "";
			String strCuentaCte = "";
			String strStatusCte = "";
			String strCajero = (String) vDatos.elementAt(1);
			String strContrato = (String) vDatos.elementAt(2);
			String strEmpresa = (String) vDatos.elementAt(3);
			String strCuenta = "";
			String strMonto = "";
			String strEstatus = "";
			String strConsecutivo = "";
			Vector vCampos = new Vector();
			Vector vCamposT = new Vector();
			Vector vCamposTT = new Vector();
			Vector vParametros = new Vector();
			Vector vCertificacion = new Vector();
			int intControl = 0, intNoParametros = 4, intNoCamposCert = 6;
			int intPagina = 0, intDocacep = 0, intDocrech = 0;
			long longTotal = 0, longRechazado = 0, longAceptado = 0;
			tpImpresora.activateControl("CERTIFICACION",iDriver);
			vCampos = tpImpresora.Campos(strRespuesta, "^");
			intSize = vCampos.size();
			for (i = 0; i < intSize; i++) {
				strTmp = (String) vCampos.elementAt(i);
				if (strTmp.startsWith("MOVIMIENTOS_NOMINA")) {
					strTmp = (String) vCampos.elementAt(i);
					vCamposT = tpImpresora.Campos(strTmp, "@");
					strTmp = (String) vCamposT.elementAt(1);
					vCamposT = tpImpresora.Campos(strTmp, "|");
					for (int j = 0; j < vCamposT.size(); j++) {
						strTmp = (String) vCamposT.elementAt(j);
						vCamposTT = tpImpresora.Campos(strTmp, "~");
						for (int k = 0; k < vCamposTT.size(); k++) {
							strTmp = (String) vCamposTT.elementAt(k);
							vParametros.addElement(strTmp);
						}
					}
				}
				if (strTmp.startsWith("CERTIFICACION_NOMINA")) {
					strTmp = (String) vCampos.elementAt(i);
					vCamposT = tpImpresora.Campos(strTmp, "@");
					strTmp = (String) vCamposT.elementAt(1);
					vCamposT = tpImpresora.Campos(strTmp, "|");
					for (int j = 0; j < vCamposT.size(); j++) {
						strTmp = (String) vCamposT.elementAt(j);
						vCamposTT = tpImpresora.Campos(strTmp, "~");
						for (int k = 0; k < vCamposTT.size(); k++) {
							strTmp = (String) vCamposTT.elementAt(k);
							vCertificacion.addElement(strTmp);
						}
					}
				}
			}
			intSize = vParametros.size() / intNoParametros;
			strEstatus = (String) vParametros.elementAt(2);
			if (!strEstatus.equals("R")
				&& !strContrato.equals("0")
				&& !strEmpresa.equals("0")) {
				for (int ii = 0; ii < intSize; ii++) {
					strCuenta =
						(String) vParametros.elementAt(
							0 + (ii * intNoParametros));
					strMonto =
						(String) vParametros.elementAt(
							1 + (ii * intNoParametros));
					strEstatus =
						(String) vParametros.elementAt(
							2 + (ii * intNoParametros));
					strConsecutivo =
						(String) vParametros.elementAt(
							3 + (ii * intNoParametros));
					if (intControl == 0) {
						if (intPagina == 0) {
							longTotal = Long.parseLong(strMonto.trim());
							strMontoCte = strMonto;
							strCuentaCte = strCuenta;
							if (strEstatus.equals("R"))
								strStatusCte = "RECHAZADO";
							else
								strStatusCte = "ACEPTADO";
						}
						intPagina++;
						strOut = "                                      "+strIdentidad;
						tpImpresora.enviaCadena(strOut);
						tpImpresora.enviaCadena(" ");
						strOut =
							"                                                            No. PAGINA "
								+ intPagina;
						tpImpresora.enviaCadena(strOut);
						tpImpresora.enviaCadena(" ");
						strOut =
							"            Hora      : "
								+ strTime.substring(0, 2)
								+ ":"
								+ strTime.substring(2, 4)
								+ ":"
								+ strTime.substring(4, 6);
						tpImpresora.enviaCadena(strOut);
						strOut =
							"            Fecha     : "
								+ strDate.substring(7, 9)
								+ "-"
								+ strDate.substring(5, 7)
								+ "-"
								+ strDate.substring(1, 5)
								+ "                     Cajero : "
								+ strCajero;
						tpImpresora.enviaCadena(strOut);
						strOut =
							"            Sucursal  : "
								+ strCajero.substring(0, 4)
								+ "                 No. de Contrato  : "
								+ strContrato;
						tpImpresora.enviaCadena(strOut);
						tpImpresora.enviaCadena(" ");
						strOut = " Nombre de la Empresa : " + strEmpresa;
						tpImpresora.enviaCadena(strOut);
						strOut =
							"     Cuenta del cargo : " + strCuentaCte.trim();
						tpImpresora.enviaCadena(strOut);
						strOut =
							"      Monto del cargo : "
								+ tpImpresora.formatMonto(strMontoCte.trim());
						tpImpresora.enviaCadena(strOut);
						strOut = "    Estatus del cargo : " + strStatusCte;
						tpImpresora.enviaCadena(strOut);
						tpImpresora.enviaCadena(" ");
						tpImpresora.enviaCadena(" ");
						strOut =
							"      Cuenta de Abono          Monto Abonado          Estatus del Abono";
						tpImpresora.enviaCadena(strOut);
						tpImpresora.enviaCadena(
							"___________________________________________________________________________");
						intControl++;
					}
					if (ii > 0) {
						longAceptado = Long.parseLong(strMonto.trim());
						strMonto = tpImpresora.formatMonto(strMonto.trim());
						for (int j = strMonto.length(); j < 13; j++)
							strMonto = " " + strMonto;
						if (strEstatus.equals("R")) {
							++intDocrech;
							longRechazado = longRechazado + longAceptado;
							strOut =
								"        "
									+ strCuenta
									+ "             "
									+ strMonto
									+ "              RECHAZADO";
						} else {
							++intDocacep;
							strOut =
								"        "
									+ strCuenta
									+ "             "
									+ strMonto
									+ "              ACEPTADO";
						}
						tpImpresora.enviaCadena(strOut);
						/*System.out.println(
							" intDocacep ***"
								+ intDocacep
								+ "*** intDocrech ***"
								+ intDocrech
								+ "*** ii ***"
								+ ii
								+ "*** strOut "
								+ strOut);*/
						intControl++;
					}
					if (intControl == 41) {
						if (iDriver == 0) {
							tpImpresora.InserteDocumento();
							tpImpresora.activateControl("SELECCIONASLIP",iDriver);
							tpImpresora.enviaCadena("");
							tpImpresora.enviaCadena("");
							tpImpresora.enviaCadena("");
							tpImpresora.enviaCadena("");
							tpImpresora.enviaCadena("");
							tpImpresora.imprimeCadena1();
							tpImpresora.activateControl("SALTOHOJA",iDriver);
							tpImpresora.activateControl("SELECCIONAJOURNAL",iDriver);
						} else {
							tpImpresora.activateControl("SALTOHOJA",iDriver);
							tpImpresora.muestraVentana();
						}
						intControl = 0;
					}
				}
				longAceptado = longTotal - longRechazado;
				tpImpresora.enviaCadena(
					"___________________________________________________________________________\n");
				strOut = tpImpresora.formatMonto(String.valueOf(longAceptado));
				strOut =
					" Total Abonado         : "
						+ strOut
						+ strFiller.substring(0, 23 - strOut.length())
						+ "No. Doctos Aceptados : "
						+ String.valueOf(intDocacep);
				tpImpresora.enviaCadena(strOut);
				strOut = tpImpresora.formatMonto(String.valueOf(longRechazado));
				strOut =
					" Total de Devoluciones : "
						+ strOut
						+ strFiller.substring(0, 23 - strOut.length())
						+ "No. Doctos Rechazados: "
						+ String.valueOf(intDocrech);
				tpImpresora.enviaCadena(strOut);
				strOut =
					" Total                 : "
						+ tpImpresora.formatMonto(String.valueOf(longTotal));
				tpImpresora.enviaCadena(strOut);
				if (iDriver == 0) {
					tpImpresora.InserteDocumento();
					tpImpresora.activateControl("SELECCIONASLIP",iDriver);
					tpImpresora.enviaCadena("");
					tpImpresora.enviaCadena("");
					tpImpresora.enviaCadena("");
					tpImpresora.enviaCadena("");
					tpImpresora.enviaCadena("");
					tpImpresora.imprimeCadena1();
					tpImpresora.activateControl("SALTOHOJA",iDriver);
					tpImpresora.activateControl("SELECCIONAJOURNAL",iDriver);
				} else {
					tpImpresora.activateControl("SALTOHOJA",iDriver);
				}
			}
			tpImpresora.activateControl("CERTIFICACION",iDriver);
			intSize = vCertificacion.size() / intNoCamposCert;
			String strCtaant = null;
			int intSalto = 1;
			for (i = 0; i < intSize; i++) {
				String strCertificacion = "";
				String strSucursal = strCajero.substring(0, 4);
				String strDivisa = "$";
				strCuenta =
					(String) vCertificacion.elementAt(
						0 + (i * intNoCamposCert));
				strConsecutivo =
					(String) vCertificacion.elementAt(
						1 + (i * intNoCamposCert));
				strMonto =
					(String) vCertificacion.elementAt(
						2 + (i * intNoCamposCert));
				String strTxn =
					(String) vCertificacion.elementAt(
						3 + (i * intNoCamposCert));
				String strLine =
					(String) vCertificacion.elementAt(
						4 + (i * intNoCamposCert));
				String strResp =
					(String) vCertificacion.elementAt(
						5 + (i * intNoCamposCert));
				int intNumLineas = Integer.parseInt(strLine);
				strCertificacion =
					strConsecutivo
						+ " "
						+ strDate.substring(7, 9)
						+ strDate.substring(5, 7)
						+ strDate.substring(1, 5)
						+ " "
						+ strTxn
						+ " "
						+ strCajero
						+ " ";
				if (strCuenta != null)
					strCertificacion = strCertificacion + strCuenta + " ";
				if (strMonto != null)
					strCertificacion =
						strCertificacion
							+ strDivisa
							+ tpImpresora.formatMonto(strMonto)
							+ " ";
				strCertificacion = strCertificacion + strSucursal;
				if (!strCuenta.equals(strCtaant)) {
					if (intSalto == 0) {
						if (iDriver == 0) {
							tpImpresora.InserteDocumento();
							tpImpresora.activateControl("SELECCIONASLIP",iDriver);
							tpImpresora.enviaCadena("");
							tpImpresora.enviaCadena("");
							tpImpresora.enviaCadena("");
							tpImpresora.enviaCadena("");
							tpImpresora.enviaCadena("");
							tpImpresora.imprimeCadena1();
							tpImpresora.activateControl("SALTOHOJA",iDriver);
							tpImpresora.activateControl("SELECCIONAJOURNAL",iDriver);
						} else {
							tpImpresora.activateControl("SALTOHOJA",iDriver);
							tpImpresora.muestraVentana();
						}
					} else
						intSalto = 0;
				}
				strCtaant = strCuenta;
				if (intNumLineas == 1)
					tpImpresora.enviaCadena(strCertificacion);
				tpImpresora.enviaCadena(strResp);
			}
			if (iDriver == 0) {
				tpImpresora.InserteDocumento();
				tpImpresora.activateControl("SELECCIONASLIP",iDriver);
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.imprimeCadena1();
				tpImpresora.activateControl("SALTOHOJA",iDriver);
				tpImpresora.activateControl("SELECCIONAJOURNAL",iDriver);
			} else {
				tpImpresora.activateControl("SALTOHOJA",iDriver);
			}
		}
		return intStatus;
	}
}
