package ventanilla.com.bital.util;

public final class NSTokenizer extends java.util.StringTokenizer
{
  private int     count = 0;
  private boolean start = true;
  private String  delim = null;

  private NSTokenizer(String str)
  {
    super(str);
  }
  
  private NSTokenizer(String str, String delim, boolean returnDelims)
  {
    super(str, delim, returnDelims);
  }
  
  public NSTokenizer(String str, String delim)
  {
    super(str, delim, true);
    this.delim = new String(delim);
  }
  
  
  public String nextToken()
  {
    String retval = null;
    String token = super.nextToken();

    //if( token.charAt(0) == delim.charAt(0) )
    boolean isToken = false;
    for(int i=0; i<delim.length(); i++)
      if( token.charAt(0) == delim.charAt(i) )
        isToken = true;
    
    if( isToken )
    {
      if( start )
        ++count;
      
      ++count;
      if( count > 1 )
      {
        count = 1;
        retval = new String("");
      }
    }
    else
    {
      count = 0;
      retval = token;
    }
    
    start = false;
    return retval;
  }
}
