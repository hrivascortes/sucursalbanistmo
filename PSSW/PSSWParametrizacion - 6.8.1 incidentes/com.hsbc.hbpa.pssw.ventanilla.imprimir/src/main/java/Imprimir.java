import netscape.javascript.*;
import java.awt.*;
import java.util.*;
import java.awt.event.*;
import java.applet.Applet;

class Imprime {

	private int iImprimio = 0;

	public int imprimeCadena(
		
		String strCadena,
		String strRespuesta,
		String strDatosSuc,
		String strCajero,
		String strConsecutivo,
		String strDate,
		String strTime,
		int iDriver,
		String strIdentidad,
		String strIdentidad2) {
		
		Vector vDatos = new Vector();
		String strNombre = new String();
		ToolsPrinter tpImpresora = new ToolsPrinter();
		int intStatus = 0;
		


		intStatus = tpImpresora.verificaStatus();
		if ((intStatus == 95) || (intStatus == 88)) {
			intStatus = 0;
			vDatos = tpImpresora.Campos(strCadena, "~");
			strNombre = (String) vDatos.elementAt(0);
			if (strNombre.equals("NOMINA")) {
				Nomina Nom = new Nomina();
				Nom.imprimeNomina(
					vDatos,
					strRespuesta,
					strDate,
					strTime,
					iDriver,
					strIdentidad);
			} else if (strNombre.equals("RAP")) {
				RAP Rap = new RAP();
				Rap.imprimeRap(
					vDatos,
					strRespuesta,
					strDatosSuc,
					strCajero,
					strDate,
					strTime,
					iDriver,
					strIdentidad,
					strIdentidad2);
			} else if (strNombre.equals("PROTESTO1")) {
				Protestos Prot = new Protestos();
				Prot.imprimeProtesto1(
					(String) vDatos.elementAt(1),
					strDatosSuc,
					strDate,
					strTime,
					iDriver,
					strIdentidad);
			} else if (strNombre.equals("PROTESTO2")) {
				Protestos Prot = new Protestos();
				Prot.imprimeProtesto2(
					(String) vDatos.elementAt(1),
					strDate,
					iDriver,
					strIdentidad);
			} else if (
				(strNombre.equals("COMPCOM"))
					|| (strNombre.equals("TRANSFERENCIA"))
					|| (strNombre.equals("CFISCAL"))) {
				Comprobantes Comp = new Comprobantes();
				if (strNombre.equals("TRANSFERENCIA")){
					Comp.imprimeTransferencia(
						(String) vDatos.elementAt(1),
						(String) vDatos.elementAt(2),
						(String) vDatos.elementAt(3),
						iDriver);
				}
				else{
					Comp.imprimeCFiscal(strDatosSuc, vDatos, strDate, iDriver);
				}
			} else if (
			             (strNombre.equals("CHQCAJA"))
				      || (strNombre.equals("CHQCERT"))
				      || (strNombre.equals("CHQGER"))
				      || (strNombre.equals("CHQGERE"))
				      ) 
			{
				Cheques Chqs = new Cheques();
				if (strNombre.equals("CHQCAJA"))
					{
					Chqs.imprimeChequeCaja(
						vDatos,
						strDatosSuc,
						strDate,
						iDriver);
					}
				if (strNombre.equals("CHQCERT")){
					Chqs.imprimeChequeCertif(
						(String) vDatos.elementAt(1),
						(String) vDatos.elementAt(2),
						(String) vDatos.elementAt(3),
						strConsecutivo,
						strDate,
						iDriver,
						strIdentidad);
				}
				if (strNombre.equals("CHQGER")){
				    Chqs.imprimeChequeGer(
						vDatos,
						strDatosSuc,
						strDate,
						iDriver);	
				}
				if (strNombre.equals("CHQGERE")){										
				    Chqs.imprimeChequeGerDes(
						vDatos,
						strDatosSuc,
						strDate,
						iDriver);			
				}
			} else if (strNombre.equals("RELCHQCJA")) {
				Cheques Chqs = new Cheques();
				Chqs.imprimeRelacion(vDatos, strCajero, strDate, iDriver, strIdentidad);
			} else if (strNombre.equals("RELENMA")) {
				Cheques Chqs = new Cheques();
				Chqs.imprimeRelacionEnma(vDatos, strCajero, strDate, iDriver,strIdentidad);
			} else if (strNombre.equals("FICDEPCOB")) {
				Ficha Fic = new Ficha();
				Fic.imprimeFichaCobro(
					strRespuesta,
					strCajero,
					(String) vDatos.elementAt(1),
					strDate,
					iDriver);
			} else if (strNombre.equals("FICDECOB4009")) {
				Ficha Fic = new Ficha();
				Fic.imprimeFicha4009(strCadena, strCajero, strDate, iDriver);
			} else if (strNombre.equals("COMPRAVENTA")) {
				CompraVenta cvFormato = new CompraVenta();
				String strFormato = (String) vDatos.elementAt(1);
				if (strFormato.equals("0726-0106")
					|| strFormato.equals("0726-4111")
					|| strFormato.equals("4043-0106")
					|| strFormato.equals("4043-4111")
					|| strFormato.equals("0128-4101")
					|| strFormato.equals("0130-4101")
					|| strFormato.equals("0112-4111")
					|| strFormato.equals("0114-4111")){
					//|| strFormato.equals("0108-4117"))*/
				cvFormato.imprimeCompraVentaNueva(
					vDatos,
					strRespuesta,
					strDatosSuc,
					strDate,
					strCajero,
					iDriver,
					strIdentidad,
					strIdentidad2);
				}
				else
					cvFormato.imprimeCompraVenta(
						vDatos,
						strDatosSuc,
						strDate,
						strTime,
						strCajero,
						iDriver,
						strIdentidad);
			} else if (strNombre.equals("COMPTDC")) {
				Comprobantes Comp = new Comprobantes();
				Comp.imprimeCTDC(
					vDatos,
					strDatosSuc,
					strCajero,
					strDate,
					strTime,
					iDriver,
					strIdentidad);
			} else if (strNombre.equals("EXPORDUSD")) {
				CompraVenta cvFormato = new CompraVenta();
				cvFormato.imprime4043_0786(
					vDatos,
					strDatosSuc,
					strCajero,
					strDate,
					strTime,
					iDriver);
			} else if (strNombre.equals("GIROCITI1")) {
				Giros Giro = new Giros();
				Giro.imprimeGiro1(
					vDatos,
					strDatosSuc,
					strCajero,
					strDate,
					strTime,
					iDriver);
			} else if (strNombre.equals("GIROCITI2")) {
				Giros Giro = new Giros();
				Giro.imprimeGiro2(
					vDatos,
					strDatosSuc,
					iDriver);
			} else if (strNombre.equals("GIROCITI3")) {
				Giros Giro = new Giros();
				Giro.imprimeGiro3(
					vDatos,
					strDatosSuc,
					strRespuesta,
					strDate,
					iDriver);					
			} else if (strNombre.equals("RETIRO")) {
				Inversiones Inv = new Inversiones();
				Inv.imprimeFichaRetiro(
					vDatos,
					strRespuesta,
					strDate,
					strDatosSuc,
					strCajero,
					strConsecutivo,
					iDriver,
					strIdentidad,
					strIdentidad2);
			} else if (strNombre.equals("REINVERSION")) {
				Inversiones Inv = new Inversiones();
				Inv.imprimeFichaReinversion(
					vDatos,
					strRespuesta,
					strDate,
					strDatosSuc,
					strCajero,
					strConsecutivo,
					iDriver,
					strIdentidad,
					strIdentidad2);
			} else if (strNombre.equals("REMESA")) {
				Ficha Fic = new Ficha();
				Fic.imprimeFichaRemesa(
					strRespuesta,
					strCajero,
					(String) vDatos.elementAt(1),
					strDate,
					iDriver);
			} else if (strNombre.equals("DIARIO")) {
				Diario Dia = new Diario();
				Dia.imprimeDiario(strCadena, iDriver, strIdentidad);
			} else if (strNombre.equals("LAHORRO")) {
				LAhorro ahorro = new LAhorro();
				ahorro.imprimeMovimientos(strCadena, iDriver);
			} else if (strNombre.equals("REPORTELOG")) {
				Diario Dia = new Diario();
				Dia.imprimeReporteAdmon(strCadena, iDriver, strIdentidad);
			} else if (
				(strNombre.equals("AUTORIZACION1"))
					|| (strNombre.equals("AUTORIZACION2"))
					|| (strNombre.equals("REVERSO1"))
					|| (strNombre.equals("REVERSO2"))) {
				Diario Dia = new Diario();
				Dia.imprimeReporteGte(strCadena, iDriver, strIdentidad);
			} else if (strNombre.equals("TOTALES")) {
				Totales Tot = new Totales();
				Tot.imprimeTotales(vDatos, iDriver);
				/*} else if (strNombre.equals("RELACION0818")) {
					Cheques Chqs = new Cheques();
					Chqs.imprimeRelacion0818(vDatos, strCajero, iDriver);*/
			} else if(strNombre.equals("SIB"))
				{
					FormatoSIB SIB = new FormatoSIB();
					SIB.imprimeFormatoSIB(vDatos,iDriver); 
			} else if (
				(strNombre.equals("REVERSOORDENPAGO"))
					|| (strNombre.equals("CARGO"))
					|| (strNombre.equals("EFECTIVO"))
					//|| (strNombre.equals("ABONO"))
					|| (strNombre.equals("SUSPENSION"))
					|| (strNombre.equals("LIQUIDACION"))
					|| (strNombre.equals("CANCELACION"))
					|| (strNombre.equals("RETRANSMISION"))) {
				OrdenesPago OrdPag = new OrdenesPago();
				OrdPag.imprimeOrdenNueva(
					vDatos,
					strRespuesta,
					strDatosSuc,
					strCajero,
					strDate,
					strTime,
					iDriver,
					strIdentidad,
					strIdentidad2);
			} else if (strNombre.equals("OrdenesPagoMasivas")) {
				OrdenesPago OrdPag = new OrdenesPago();
				OrdPag.imprimeOrdenMasiva(
					vDatos,
					strDatosSuc,
					strCajero,
					strDate,
					iDriver,
					strIdentidad);
			} /*else if (strNombre.equals("5217")
					|| strNombre.equals("5219")){
				ChqBanLoc CheqLoc = new ChqBanLoc();
				CheqLoc.imprimeChqBanLoc(
					vDatos,
					strRespuesta,
					strDatosSuc,
					strCajero,
					(String) vDatos.elementAt(1),
					strDate,
					strTime,
					iDriver);
			}*/ else if (strNombre.equals("PAGOVOLUNTARIO")) {
			PagoVoluntario PVoluntario = new PagoVoluntario();
			PVoluntario.imprimePV(
				vDatos,
				strDatosSuc,
				strCajero,
				strDate,
				iDriver, strIdentidad);
			} else if (strNombre.equals("G062")) {
					Relaciones rRelacion = new Relaciones();
					rRelacion.imprimeRelGiro(vDatos, strDatosSuc, strDate, iDriver, strIdentidad);
			} else if (strNombre.equals("9630")) {
				Relaciones rRelacion = new Relaciones();
				rRelacion.imprime9630(
					vDatos,
					strCajero.substring(0, 4),
					strDatosSuc,
					strDate,
					iDriver,
					strIdentidad);
			} else if (strNombre.equals("9503")) {
				Relaciones rRelacion = new Relaciones();
				rRelacion.imprime9503(
					vDatos,
					strCajero.substring(0, 4),
					strDatosSuc,
					strDate,
					iDriver,
					strIdentidad);
			} else if (strNombre.equals("9642")) {
				Relaciones rRelacion = new Relaciones();
				rRelacion.imprime9642(
					vDatos,
					strCajero.substring(0, 4),
					strDatosSuc,
					strDate,
					iDriver,
					strIdentidad);
			} else if (strNombre.equals("REMESACV")) {
				Ficha Fic = new Ficha();
				Fic.imprimeFichaRem(
					strCadena,
					strRespuesta,
					strCajero,
					strDate,
					iDriver);
			} else if (strNombre.equals("DEVCOBRO")) {
				Ficha Fic = new Ficha();
				Fic.imprimeFichaCV(
					strCadena,
					strRespuesta,
					strCajero,
					strDate,
					iDriver);
			} else if (strNombre.equals("RELORDENPAGO")) {
				Relaciones rRelacion = new Relaciones();
				rRelacion.imprimeRelOP(strRespuesta, strDate, iDriver, strIdentidad);
			} else if (strNombre.equals("9730")) {
				Relaciones rRelacion = new Relaciones();
				rRelacion.imprime9730(
					vDatos,
					strCajero.substring(0, 4),
					strDatosSuc,
					strDate,
					iDriver,
					strIdentidad);
			} else if (strNombre.equals("9732")) {
				Relaciones rRelacion = new Relaciones();
				rRelacion.imprime9732(
					vDatos,
					strCajero.substring(0, 4),
					strDatosSuc,
					strDate,
					iDriver,
					strIdentidad);
			} else if (strNombre.equals("9251")) {
				Relaciones rRelacion = new Relaciones();
				rRelacion.imprime9251(
					vDatos,
					strCajero.substring(0, 4),
					strDatosSuc,
					strDate,
					iDriver,
					strIdentidad);
			} else if (strNombre.equals("9821")) {
				Relaciones rRelacion = new Relaciones();
				rRelacion.imprime9821(vDatos, strDatosSuc, strDate, iDriver,strIdentidad);
			} else if (strNombre.equals("9640")) {
				Relaciones rRelacion = new Relaciones();
				rRelacion.imprime9640(vDatos, strDatosSuc, strDate, iDriver,strIdentidad);
			} else if (strNombre.equals("9009")) {
				Relaciones rRelacion = new Relaciones();
				rRelacion.imprimeReporte(vDatos, strDatosSuc, strDate, iDriver,strIdentidad);
			} else if (strNombre.equals("0250")) {
				Relaciones rRelacion = new Relaciones();
				rRelacion.imprime0250(
					vDatos,
					strDate,
					strTime,
					strCajero,
					iDriver,
					strIdentidad);
			} else if (strNombre.equals("0604") || strNombre.equals("4019")) {
				Relaciones rRelacion = new Relaciones();
				rRelacion.imprimeReporte(
				vDatos,
				strDatosSuc,
				strDate,
				iDriver,
				strIdentidad);
			}else if (strNombre.equals("ERRORMONTOLIM")) {
				Errores eError = new Errores();
				eError.imprimeError(strCadena, iDriver);
			}else if (strNombre.equals("DEBITO") || strNombre.equals("CREDITO")){
				ChqBanLoc cChqBanLoc = new ChqBanLoc();
				cChqBanLoc.imprimeChqBanLoc(
				vDatos,
				strRespuesta,
				strDatosSuc,
				strCajero,
				strDate,
				strTime,
				iDriver);
			}else if (strNombre.equals("ITBMS")){
				ITBMS itbms = new ITBMS();
				itbms.comprobanteITBMS(
					vDatos,
					strRespuesta,
					strDatosSuc,
					strDate,
					strCajero,
					strConsecutivo,
					iDriver);
			}
			iImprimio = 1;
		}
		return intStatus;
	}

	public int getStatus() {
		return iImprimio;
	}
}

public class Imprimir extends Applet implements ActionListener{
	private String strLabel1 = "   Certificacion   ";
	private String strLabel2 = "Documentacion";
	private String strLabel3 = "   Cancelacion   ";
	private String strCadena = "";
	private String strCajero = "";
	private String strDate = "";
	private String CFESP = "";
	private String strTime = "";
	private String strReverso = "N";
	private String strTransaccion = "";
	private String strDatosSuc = "";
	private String strConsecutivo = " ";
	private String strRespuesta = "";
	private String strFlujo = "N";
	private String strFlujoAux = "";
	private String strFlujoCAPR = ""; //JGA
	private String strCRM = "";
	private String strTmp = "";
	private String strFicha = "";
	private String strTellerPerfil = "";
	private String strDatosFicha = "";
	private String strMontoChqDev1003 = "";
	private Button bImp_;
	private Button bCon_;
	private Button bCan_;
	private Label lStatus;
	private int intCont;
	private int intStatus;
	private int intSize;
	private int intBanderaClose = 0;
	public TextField tfKey;
	public String RemoteIP = "";
	public String strRuta = "";
	private String strIdentidad = "";
	private String strIdentidad2 = "";
	int Port = 80;
	int intWindows = 1;
	int iDriver = 1;

	public int getDriver() {
		ParallelPort lpt1 = new ParallelPort();
		iDriver = lpt1.getNameDriver();
		
		return iDriver;
	}

	public void EvaluaSalida() {
		System.gc();
		intCont = 0;
		JSObject win = null;
		try {
			win = JSObject.getWindow(this);
		} catch (Exception e) {
			System.out.println(" e " + e);
		}
		if  ( (strRespuesta.indexOf("CERTIFICACION") > -1) && (strRespuesta.indexOf("~A~") > 0) && strTransaccion.equals("txn1003"))
			win.eval("submite1003()");		
		else if (strCadena.startsWith("TOTALES"))
			win.eval("CloseWindows()");
		else {
			if (strTransaccion.equals("txn5361"))
				win.eval("submiteRapCheqDev()");
			else if (strFlujo.equals("N")) {
				if(strFlujoCAPR != null && strFlujoCAPR.equals("1")){
					win.eval("submiteRAPL()");
				}else				
				if (strFlujoAux != null) {
					if (strFlujoAux.equals("1"))
						win.eval("submiteRap()");
					else if (strFlujoAux.equals("10"))
						win.eval("submite0818()");
					else if (strCRM.equals("SIN-DATOS"))
						if (strReverso.equals("N"))
							win.eval("submiteForma()");
						else
							win.eval("submiteDiario()");
					else
						win.eval("submiteCRM()");
				} else {
					if (strCRM.equals("SIN-DATOS"))
						if (strReverso.equals("N"))
							win.eval("submiteForma()");
						else
							win.eval("submiteDiario()");
					else
						win.eval("submiteCRM()");
				}
			} else
				win.eval("handleOK(this)");
		}
	}

	public void init() {

		try {

			iDriver = getDriver();
			ToolsPrinter tpImpresora = new ToolsPrinter();
			Vector vCampos;
			setBackground(java.awt.Color.white);
			bImp_ = new Button(strLabel1);
			bImp_.setBackground(java.awt.Color.white);
			bImp_.setFont(new Font("Arial", Font.BOLD, 12));
			bImp_.addActionListener(this);
			add(bImp_);
			bImp_.setEnabled(false);
			bImp_.setVisible(true);
			bCon_ = new Button(strLabel2);
			bCon_.setBackground(java.awt.Color.white);
			bCon_.setFont(new Font("Arial", Font.BOLD, 12));
			bCon_.addActionListener(this);
			bCon_.setEnabled(false);
			bCon_.setVisible(true);
			add(bCon_);
			bCan_ = new Button(strLabel3);
			bCan_.setBackground(java.awt.Color.white);
			bCan_.setFont(new Font("Arial", Font.BOLD, 12));
			bCan_.addActionListener(this);
			bCan_.setEnabled(false);
			bCan_.setVisible(true);
			add(bCan_);
			lStatus =
				new Label("                                                                                        ");
			add(lStatus);
			strCadena = getParameter("Cadena");
			strCadena = java.net.URLDecoder.decode(strCadena);
			strDate = getParameter("strDate");
			CFESP = getParameter("CFESP");
			strTime = getParameter("strTime");
			strDatosSuc = getParameter("DatosSucursal");
			strFlujoAux = getParameter("Flujo");
			strFlujoCAPR = getParameter("FlujoCAPR");//JGA
			strTransaccion = getParameter("strTransaccion");
			strCajero = getParameter("strCajero");
			strTellerPerfil = getParameter("strTellerPerfil");
			strFicha = getParameter("strFicha");
			strDatosFicha = getParameter("strValorFicha");
			strCRM = getParameter("crmInfo");
			strConsecutivo = getParameter("Consecutivo");
			strRespuesta = getParameter("Respuesta");
			strRespuesta = java.net.URLDecoder.decode(strRespuesta);
			strMontoChqDev1003 = getParameter("strMontoChqDev1003");
			RemoteIP = getParameter("RemoteIP");
			Port = Integer.parseInt(getParameter("RemotePort"));
			strRuta = getParameter("Ruta");
			strIdentidad = getParameter("strIdentidad");
			strIdentidad = java.net.URLDecoder.decode(strIdentidad);
			strIdentidad2 = getParameter("strIdentidad2");
			strIdentidad2 = java.net.URLDecoder.decode(strIdentidad2);
			
			
			if (getParameter("Reverso") != null)
				strReverso = getParameter("Reverso");
			intCont = 0;
			if (!strCadena.equals("")) {
				if (strCadena.substring(0, 2).equals("1~")
					|| strCadena.substring(0, 2).equals("0~")
					|| strCadena.substring(0, 2).equals("T~")){
					strCadena = "";
				}
			}
			vCampos = tpImpresora.Campos(strRespuesta, "^");
			System.out.println("cadenaJAR--"+strRespuesta);
			intSize = vCampos.size();
			for (int i = 0; i < intSize; i++) {
				strTmp = (String) vCampos.elementAt(i);
				if (strTmp.startsWith("MAS_TXN")) {
					strTmp = (String) vCampos.elementAt(i);
					vCampos = tpImpresora.Campos(strTmp, "@");
					strFlujo = (String) vCampos.elementAt(1);
					i = intSize + 2;
				}
			}
			Certificacion cCertificar = new Certificacion();
			intStatus = 0;
			if (strCadena.endsWith("NOIMPRIMIR")){
				intStatus = 0;
			}
			else {
				intWindows = 0;
				if (strCadena.length() > 5) {
					Imprime iImp = new Imprime();
					String strAmbos = strCadena.substring(0, 1);
					if (!strCadena.startsWith("~COMPRAVENTA~0726-0106")
						&& !strCadena.startsWith("~COMPRAVENTA~0726-4111")
						&& !strCadena.startsWith("~COMPRAVENTA~4043-0106")
						&& !strCadena.startsWith("~COMPRAVENTA~4043-4111")
						&& !strCadena.startsWith("~COMPRAVENTA~0128-4101")
						&& !strCadena.startsWith("~COMPRAVENTA~0130-4101")
						&& !strCadena.startsWith("~COMPRAVENTA~0112-4111")
						&& !strCadena.startsWith("~COMPRAVENTA~0114-4111") //&& !strCadena.startsWith("~COMPRAVENTA~0108-4117")
						&& !strCadena.startsWith("~REINVERSION")
						&& !strCadena.startsWith("~RETIRO")						
						&& !strCadena.startsWith("~CARGO~N$")
						&& !strCadena.startsWith("~CARGO~US$")
						//&& !strCadena.startsWith("~TIP")
						&& !strCadena.startsWith("~EFECTIVO~N$")
						&& !strCadena.startsWith("~ABONO~N$")
						&& !strCadena.startsWith("~LIQUIDACION~N$")
						&& !strCadena.startsWith("~CANCELACION~N$")	
						&& !strCadena.startsWith("~ITBMS")
						&& strAmbos.equals("~")) {
						intCont = 2;
						bImp_.setEnabled(true);
						strCadena = strCadena.substring(1, strCadena.length());
						intStatus =
						cCertificar.certifica(
								CFESP,
								strRespuesta,
								strCadena,
								strDate,
								strDatosSuc,
								iDriver,
								strFicha,
								strDatosFicha,
								strTellerPerfil,
								strMontoChqDev1003,
								strIdentidad,
								strIdentidad2);								
						if (intStatus == 0) {
							intBanderaClose = 1;
							bImp_.setEnabled(false);
							intStatus = 103;
							intCont = 1;
							bCan_.setEnabled(true);
							bCon_.setEnabled(true);
						}
					} else {
						intBanderaClose = 1;
						if (strCadena.startsWith("~COMPRAVENTA~0726-0106")
							|| strCadena.startsWith("~COMPRAVENTA~0726-4111")
							|| strCadena.startsWith("~COMPRAVENTA~4043-0106")
							|| strCadena.startsWith("~COMPRAVENTA~4043-4111")
							|| strCadena.startsWith("~COMPRAVENTA~0128-4101")
							|| strCadena.startsWith("~COMPRAVENTA~0130-4101")
							|| strCadena.startsWith("~COMPRAVENTA~0112-4111")
							|| strCadena.startsWith("~COMPRAVENTA~0114-4111")
							|| strCadena.startsWith("~RETIRO")							
							|| strCadena.startsWith("~REINVERSION")
							|| strCadena.startsWith("~CARGO~N$")
							|| strCadena.startsWith("~CARGO~US$")
							|| strCadena.startsWith("~EFECTIVO~N$")
							|| strCadena.startsWith("~ABONO~N$")
							|| strCadena.startsWith("~LIQUIDACION~N$")
							|| strCadena.startsWith("~CANCELACION~N$")
							|| strCadena.startsWith("~ITBMS")){
							strCadena =
								strCadena.substring(1, strCadena.length());
						}
						if (strCadena.startsWith("TOTALES")
							|| strCadena.startsWith("DIARIO")
							|| strCadena.startsWith("RAP")
							|| strCadena.startsWith("LAHORRO"))
							intBanderaClose = 0;
						bImp_.setEnabled(false);
						bCan_.setEnabled(true);
						bCon_.setEnabled(true);
						intStatus =
							iImp.imprimeCadena(
								strCadena,
								strRespuesta,
								strDatosSuc,
								strCajero,
								strConsecutivo,
								strDate,
								strTime,
								iDriver,
								strIdentidad,
								strIdentidad2);
						intCont = 1;
					}
					int iCambioEtiqueta = iImp.getStatus();
					if (iCambioEtiqueta == 1)
						bCon_.setLabel("Documentacion");
				} else {
					bImp_.setEnabled(true);
					intStatus =
						cCertificar.certifica(
								CFESP,
								strRespuesta,
								strCadena,
								strDate,
								strDatosSuc,
								iDriver,
								strFicha,
								strDatosFicha,
								strTellerPerfil,
								strMontoChqDev1003,
								strIdentidad,
								strIdentidad2);								
				}
			}
			if (intStatus != 0){
				showStatus(intStatus);
				}
			else if (intBanderaClose == 0){
				EvaluaSalida();
				}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void showStatus(int intStatus) {
		if ((intStatus == 71) || (intStatus == 72) || (intStatus == 79)){
			lStatus.setText("       Impresora no preparada");
		}
		else if (intStatus == 1){
			lStatus.setText("                             ");
		}
		else if (intStatus == 112){
			lStatus.setText("Verifique Rollo en Certificadora");
			}
		else if ((intStatus == 96) || (intStatus == 103)){
			lStatus.setText("  Falta Papel en la impresora");
		}
		else if (intStatus == 0){
			lStatus.setText(strCadena);
		}
		else if (intStatus == 10){
			lStatus.setText("NO Existe la certificacion(sql)");
		}
		else if (intStatus == 120){
			lStatus.setText("          Impresora Apagada");
		}
		else{
			lStatus.setText("        Estatus no Reconocido");
		}
	}

	public void ejecutaAccion() {
		Imprime iImp = new Imprime();
		int iCambioEtiqueta = iImp.getStatus();
		if (iCambioEtiqueta == 1)
			bCon_.setLabel("Documentacion");
		Certificacion cCertificar = new Certificacion();
		lStatus.setText(
			"                                                            ");
		if (intCont == 1)
			intStatus =
				iImp.imprimeCadena(
					strCadena,
					strRespuesta,
					strDatosSuc,
					strCajero,
					strConsecutivo,
					strDate,
					strTime,
					iDriver,
					strIdentidad,
					strIdentidad2);
		else if (strCadena.length() > 5) {
			intBanderaClose = 1;
			if (strCadena.startsWith("TOTALES")
				|| strCadena.startsWith("DIARIO")
				|| strCadena.startsWith("LAHORRO"))
				intBanderaClose = 0;
			if (intCont == 2) {
				intStatus =
				cCertificar.certifica(
						CFESP,
						strRespuesta,
						strCadena,
						strDate,
						strDatosSuc,
						iDriver,
						strFicha,
						strDatosFicha,
						strTellerPerfil,
						strMontoChqDev1003,
						strIdentidad,
						strIdentidad2);								
			}
			if (intStatus == 0) {
				intCont = 1;
				intStatus = 103;
			}
		} else {
			intBanderaClose = 0;
			intStatus =
			cCertificar.certifica(
					CFESP,
					strRespuesta,
					strCadena,
					strDate,
					strDatosSuc,
					iDriver,
					strFicha,
					strDatosFicha,
					strTellerPerfil,
					strMontoChqDev1003,
					strIdentidad,strIdentidad2);								
		}
		if (intStatus != 0) {
			if (intCont == 1) {
				bCon_.setEnabled(true);
				bCan_.setEnabled(true);
			} else {
				bImp_.setEnabled(true);
			}
			showStatus(intStatus);
		} else {
			if (intBanderaClose == 0) {
				bImp_.setEnabled(false);
				EvaluaSalida();
			} else {
				bCon_.setEnabled(true);
				bCan_.setEnabled(true);
			}
		}
	}

	public void actionPerformed(ActionEvent ae) {
		repaint();
		Button de = (Button) ae.getSource();

		if (de.getLabel().equals("   Certificacion   ")) {
			bImp_.setEnabled(false);
			bCan_.setEnabled(false);
			bCon_.setEnabled(false);
			ejecutaAccion();
		}
		if (de.getLabel().equals("Documentacion")) {
			bCan_.setEnabled(true);
			bCon_.setEnabled(false);
			bImp_.setEnabled(false);
			ejecutaAccion();
		}
		if (de.getLabel().equals("   Cancelacion   "))
			EvaluaSalida();
		System.gc();
	}
	
}