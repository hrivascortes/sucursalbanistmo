//*************************************************************************************************
//	   Funcion: Clase que certifica Pago Voluntario
//	  Elemento: PagoVoluntario.java
//	Creado por: Guillermo Rodrigo Escand�n Soto
//*************************************************************************************************
//CCN - 4620027	- 24/10/2007 - Se actualiza "WWW..COM.MX" por "WWW..COM.PA"
//*************************************************************************************************

import java.util.*;

public class PagoVoluntario{
	ToolsPrinter tpImpresora = new ToolsPrinter();
	int intStatus = 0;
	String strFiller =
		"                                                                                                                                  ";
	String strOutLine = "";
	Vector vDatosSuc = new Vector();
	int intMoneda = 0;
	
	public int imprimePV(
	Vector vDatos,
	String strDatosSuc,
	String strCajero,
	String strDate,
	int iDriver,
	String strIdentidad){
	
	intStatus = tpImpresora.verificaStatus();
	if ((intStatus == 95) || (intStatus == 88)) {
		intStatus = 0;
		String strTxn = "";
		String strCliente = (String) vDatos.elementAt(1);
		String strCredito = (String) vDatos.elementAt(2);
		String strCapVig = (String) vDatos.elementAt(3);
		//strCapVig = tpImpresora.formatMonto(strCapVig);
		String strIntVig = (String) vDatos.elementAt(4);
		//strIntVig = tpImpresora.formatMonto(strIntVig);
		String strFECIVig = (String) vDatos.elementAt(5);
		//strFECIVig = tpImpresora.formatMonto(strFECIVig);
		String strGasVig = (String) vDatos.elementAt(6);
		//strGasVig = tpImpresora.formatMonto(strGasVig);
		String strSegVig = (String) vDatos.elementAt(7);
		//strSegVig = tpImpresora.formatMonto(strSegVig);
		String strComVig = (String) vDatos.elementAt(8);
		//strComVig = tpImpresora.formatMonto(strComVig);
		String strTotalVig = (String) vDatos.elementAt(9);
		//strTotalVig = tpImpresora.formatMonto(strTotalVig);
		String strCapVen = (String) vDatos.elementAt(10);
		//strCapVen = tpImpresora.formatMonto(strCapVen);
		String strIntVen = (String) vDatos.elementAt(11);
		//strIntVen = tpImpresora.formatMonto(strIntVen);
		String strFECIVen = (String) vDatos.elementAt(12);
		//strFECIVen = tpImpresora.formatMonto(strFECIVen);
		String strGasVen = (String) vDatos.elementAt(13);
		//strGasVen = tpImpresora.formatMonto(strGasVen);
		String strSegVen = (String) vDatos.elementAt(14);
		//strSegVen = tpImpresora.formatMonto(strSegVen);
		String strComVen = (String) vDatos.elementAt(15);
		//strComVen = tpImpresora.formatMonto(strComVen);
		String strMoratorios = (String) vDatos.elementAt(16);
		//strMoratorios = tpImpresora.formatMonto(strMoratorios);
		String strTotalVen = (String) vDatos.elementAt(17);
		//strTotalVen = tpImpresora.formatMonto(strTotalVen);
		String strTotalPag = (String) vDatos.elementAt(18);
		//strTotalPag = tpImpresora.formatMonto(strTotalPag);
		String strTotalAde = (String) vDatos.elementAt(19);
		//strTotalAde = tpImpresora.formatMonto(strTotalAde);
		String strAmorSig = (String) vDatos.elementAt(20);
		
		//ITBMS
		String strItbmVig = (String) vDatos.elementAt(21);
		String strItbmVen = (String) vDatos.elementAt(22);
		
		
		
		tpImpresora.activateControl("CERTIFICACION",iDriver);
		vDatosSuc = tpImpresora.Campos(strDatosSuc, "@");
		String strNomSuc = (String) vDatosSuc.elementAt(0);
		strNomSuc = strNomSuc.substring(7, strNomSuc.length());
		String strNumSuc = (String) vDatosSuc.elementAt(1);
		strNumSuc = strNumSuc.substring(7, strNumSuc.length());
		strOutLine =
			strFiller.substring(0, 39)
				+ strIdentidad+" PANAMA"
				+ "\n";
		tpImpresora.enviaCadena(strOutLine);
		//Imprime Sucursal
		strOutLine = "SUCURSAL : " + strNumSuc + "-" + strNomSuc;
		tpImpresora.enviaCadena(strOutLine);
			
		//Imprime fecha y cajero
		strOutLine =
					strDate.substring(5, 7)
					+ "-"
					+ strDate.substring(7, 9)
					+ "-"
					+ strDate.substring(1, 5)
					+ "    "
					+ strCajero;
		tpImpresora.enviaCadena(strOutLine);
		//tpImpresora.enviaCadena("");
		
		strOutLine = "CLIENTE:   " + strCliente + "\nNUMERO DE CREDITO:   " +  strCredito;
		tpImpresora.enviaCadena(strOutLine);
		tpImpresora.enviaCadena("");
		
		strOutLine = strFiller.substring(0, 20) 
						+ "SALDOS VIGENTES" 
						+ strFiller.substring(0, 24) //56
						+ "SALDOS VENCIDOS";
		tpImpresora.enviaCadena(strOutLine);
		strOutLine = strFiller.substring(0, 14)
						+ "CAPITAL       $"
						+ strFiller.substring(0, 12 - strCapVig.length())
						+ strCapVig
						+ strFiller.substring(0, 12)
						+ "CAPITAL       $"
						+ strFiller.substring(0, 12 - strCapVen.length())
						+ strCapVen;
		tpImpresora.enviaCadena(strOutLine);
		strOutLine = strFiller.substring(0, 14)
						+ "INTERES       $"
						+ strFiller.substring(0, 12 - strIntVig.length())
						+ strIntVig
						+ strFiller.substring(0, 12)
						+ "INTERES       $"
						+ strFiller.substring(0, 12 - strIntVen.length())
						+ strIntVen;
		tpImpresora.enviaCadena(strOutLine);
		strOutLine = strFiller.substring(0, 14)
						+ "FECI          $"
						+ strFiller.substring(0, 12 - strFECIVig.length())
						+ strFECIVig
						+ strFiller.substring(0, 12)
						+ "FECI          $"
						+ strFiller.substring(0, 12 - strFECIVen.length())
						+ strFECIVen;
		tpImpresora.enviaCadena(strOutLine);
		strOutLine = strFiller.substring(0, 14)
						+ "GASTOS        $"
						+ strFiller.substring(0, 12 - strGasVig.length())
						+ strGasVig
						+ strFiller.substring(0, 12)
						+ "GASTOS        $"
						+ strFiller.substring(0, 12 - strGasVen.length())
						+ strGasVen;
		tpImpresora.enviaCadena(strOutLine);
		strOutLine = strFiller.substring(0, 14)
						+ "SEGUROS       $"
						+ strFiller.substring(0, 12 - strSegVig.length())
						+ strSegVig
						+ strFiller.substring(0, 12)
						+ "SEGUROS       $"
						+ strFiller.substring(0, 12 - strSegVen.length())
						+ strSegVen;
		tpImpresora.enviaCadena(strOutLine);
		strOutLine = strFiller.substring(0, 14)
						+ "COMISIONES    $"
						+ strFiller.substring(0, 12 - strComVig.length())
						+ strComVig
						+ strFiller.substring(0, 12)
						+ "COMISIONES    $"
						+ strFiller.substring(0, 12 - strComVen.length())
						+ strComVen;
		tpImpresora.enviaCadena(strOutLine);
		//ITBMS
		strOutLine = strFiller.substring(0, 14)
		+ "ITBMS         $"
		+ strFiller.substring(0, 12 - strItbmVig.length())
		+ strItbmVig
		+ strFiller.substring(0, 12)
		+ "ITBMS         $"
		+ strFiller.substring(0, 12 - strItbmVen.length())
		+ strItbmVen;
		tpImpresora.enviaCadena(strOutLine);
		
		strOutLine = strFiller.substring(0, 53)
						+ "MORATORIOS    $"
						+ strFiller.substring(0, 12 - strMoratorios.length())
						+ strMoratorios;				
		tpImpresora.enviaCadena(strOutLine);
		tpImpresora.enviaCadena("");
		strOutLine = strFiller.substring(0, 14)
						+ "TOTAL VIGENTE $"
						+ strFiller.substring(0, 12 - strTotalVig.length())
						+ strTotalVig
						+ strFiller.substring(0, 12)
						+ "TOTAL VENCIDO $"
						+ strFiller.substring(0, 12 - strTotalVen.length())
						+ strTotalVen;
		tpImpresora.enviaCadena(strOutLine);
		//tpImpresora.enviaCadena("");
		
		strOutLine = strFiller.substring(0, 14)
						+ "CARGO POR APLICAR MES CORRIENTE $"
						+ strFiller.substring(0, 12 - strAmorSig.length())
						+ strAmorSig;
		tpImpresora.enviaCadena(strOutLine);
		//tpImpresora.enviaCadena("");
		
		strOutLine = strFiller.substring(0, 30)
						+ "___________________________________";
		tpImpresora.enviaCadena(strOutLine);
		strOutLine = strFiller.substring(0, 30)
						+ "TOTAL PAGADO        $"
						+ strFiller.substring(0, 14 - strTotalPag.length())
						+ strTotalPag;
		tpImpresora.enviaCadena(strOutLine);
		strOutLine = strFiller.substring(0, 30)
						+ "TOTAL ADEUDADO      $"
						+ strFiller.substring(0, 14 - strTotalAde.length())
						+ strTotalAde;
		tpImpresora.enviaCadena(strOutLine);
		strOutLine = strFiller.substring(0, 30)
						+ "___________________________________";
		tpImpresora.enviaCadena(strOutLine);
		//tpImpresora.enviaCadena("");
		strOutLine = strFiller.substring(0, 23)
						+ "CON LA RECEPCION DE ESTE COMPROBANTE EL CLIENTE\n"
						+ strFiller.substring(0, 23)
						+ "ACEPTA QUE LOS DATOS AQUI CONTENIDOS SON CORRECTOS.";
		tpImpresora.enviaCadena(strOutLine);
		tpImpresora.enviaCadena("\n");
		tpImpresora.enviaCadena(strFiller.substring(0, 33) 
									+ "_____________________");
		tpImpresora.enviaCadena(strFiller.substring(0, 41) 
									+ "FIRMA");
		
		if (iDriver == 0) {
		tpImpresora.InserteDocumento();
			tpImpresora.activateControl("SELECCIONASLIP",iDriver);
			tpImpresora.enviaCadena("");
			tpImpresora.enviaCadena("");
			tpImpresora.enviaCadena("");
			tpImpresora.enviaCadena("");
			tpImpresora.enviaCadena("");
			tpImpresora.imprimeCadena1();
			tpImpresora.activateControl("SALTOHOJA",iDriver);
			tpImpresora.activateControl("SELECCIONAJOURNAL",iDriver);
		} else {
			tpImpresora.activateControl("SALTOHOJA",iDriver);
			tpImpresora.muestraVentana();
		}			
	}
	return intStatus;
	}
}





/*import java.util.*;

public class PagoVoluntario{
	ToolsPrinter tpImpresora = new ToolsPrinter();
	int intStatus = 0;
	String strFiller =
		"                                                                                                                                  ";
	String strOutLine = "";
	Vector vDatosSuc = new Vector();
	int intMoneda = 0;
	
	public int imprimePV(
	Vector vDatos,
	String strDatosSuc,
	String strCajero,
	String strDate,
	int iDriver){
	
	intStatus = tpImpresora.verificaStatus();
	if ((intStatus == 95) || (intStatus == 88)) {
		intStatus = 0;
		String strCliente = (String) vDatos.elementAt(1);
		String strCredito = (String) vDatos.elementAt(2);
		String strCapVig = (String) vDatos.elementAt(3);
		String strIntVig = (String) vDatos.elementAt(4);
		String strFECIVig = (String) vDatos.elementAt(5);
		String strGasVig = (String) vDatos.elementAt(6);
		String strSegVig = (String) vDatos.elementAt(7);
		String strComVig = (String) vDatos.elementAt(8);
		String strTotalVig = (String) vDatos.elementAt(9);
		String strCapVen = (String) vDatos.elementAt(10);
		String strIntVen = (String) vDatos.elementAt(11);
		String strFECIVen = (String) vDatos.elementAt(12);
		String strGasVen = (String) vDatos.elementAt(13);
		String strSegVen = (String) vDatos.elementAt(14);
		String strComVen = (String) vDatos.elementAt(15);
		String strMoratorios = (String) vDatos.elementAt(16);
		String strTotalVen = (String) vDatos.elementAt(17);
		String strTotalPag = (String) vDatos.elementAt(18);
		String strTotalAde = (String) vDatos.elementAt(19);

		
		tpImpresora.activateControl("CERTIFICACION",iDriver);
		vDatosSuc = tpImpresora.Campos(strDatosSuc, "@");
		String strNomSuc = (String) vDatosSuc.elementAt(0);
		strNomSuc = strNomSuc.substring(7, strNomSuc.length());
		String strNumSuc = (String) vDatosSuc.elementAt(1);
		strNumSuc = strNumSuc.substring(7, strNumSuc.length());
		strOutLine =
					strFiller.substring(0, 30)
					+ " PANAMA"
					+ "\n";
		tpImpresora.enviaCadena(strOutLine);
		//Imprime Sucursal
		strOutLine = "  SUCURSAL : " + strNumSuc + "-" + strNomSuc;
		tpImpresora.enviaCadena(strOutLine);
		tpImpresora.enviaCadena(" ");
			
		//Imprime fecha y cajero
		strOutLine = 
					"  "
					+ strDate.substring(5, 7)
					+ "-"
					+ strDate.substring(7, 9)
					+ "-"
					+ strDate.substring(1, 5)
					+ "    "
					+ strCajero
					+ "\n";
		tpImpresora.enviaCadena(strOutLine);
		
		strOutLine = "  CLIENTE: " + strCliente; 
		tpImpresora.enviaCadena(strOutLine);
		strOutLine = "  NUMERO DE CREDITO:   " +  strCredito;
		tpImpresora.enviaCadena(strOutLine);
		tpImpresora.enviaCadena("");
		tpImpresora.enviaCadena("");
		
		strOutLine = strFiller.substring(0, 13) 
						+ "SALDOS VIGENTES";

		tpImpresora.enviaCadena(strOutLine);
		strOutLine = "  CAPITAL             $"
						+ strFiller.substring(0, 14 - strCapVig.length())
						+ strCapVig;
		tpImpresora.enviaCadena(strOutLine);
		strOutLine = "  INTERES             $"
						+ strFiller.substring(0, 14 - strIntVig.length())
						+ strIntVig;
		tpImpresora.enviaCadena(strOutLine);
		strOutLine = "  FECI                $"
						+ strFiller.substring(0, 14 - strFECIVig.length())
						+ strFECIVig;
		tpImpresora.enviaCadena(strOutLine);
		strOutLine = "  GASTOS              $"
						+ strFiller.substring(0, 14 - strGasVig.length())
						+ strGasVig;
		tpImpresora.enviaCadena(strOutLine);
		strOutLine = "  SEGUROS             $"
						+ strFiller.substring(0, 14 - strSegVig.length())
						+ strSegVig;
		tpImpresora.enviaCadena(strOutLine);
		strOutLine = "  COMISIONES          $"
						+ strFiller.substring(0, 14 - strComVig.length())
						+ strComVig;
		tpImpresora.enviaCadena(strOutLine);
		tpImpresora.enviaCadena("");
		strOutLine = "  TOTAL VIGENTE       $"
						+ strFiller.substring(0, 14 - strTotalVig.length())
						+ strTotalVig;
		tpImpresora.enviaCadena(strOutLine);
		tpImpresora.enviaCadena("");
		
		strOutLine = strFiller.substring(0, 13) 
						+ "SALDOS VENCIDOS";
		tpImpresora.enviaCadena(strOutLine);
		strOutLine = "  CAPITAL             $"
						+ strFiller.substring(0, 14 - strCapVen.length())
						+ strCapVen;
		tpImpresora.enviaCadena(strOutLine);
		strOutLine = "  INTERES             $"
						+ strFiller.substring(0, 14 - strIntVen.length())
						+ strIntVen;
		tpImpresora.enviaCadena(strOutLine);
		strOutLine = "  FECI                $"
						+ strFiller.substring(0, 14 - strFECIVen.length())
						+ strFECIVen;
		tpImpresora.enviaCadena(strOutLine);
		strOutLine = "  GASTOS              $"
						+ strFiller.substring(0, 14 - strGasVen.length())
						+ strGasVen;
		tpImpresora.enviaCadena(strOutLine);
		strOutLine = "  SEGUROS             $"
						+ strFiller.substring(0, 14 - strSegVen.length())
						+ strSegVen;
		tpImpresora.enviaCadena(strOutLine);
		strOutLine = "  COMISIONES          $"
						+ strFiller.substring(0, 14 - strComVen.length())
						+ strComVen;
		tpImpresora.enviaCadena(strOutLine);
		strOutLine = "  MORATORIOS          $"
						+ strFiller.substring(0, 14 - strMoratorios.length())
						+ strMoratorios;				
		tpImpresora.enviaCadena(strOutLine);
		tpImpresora.enviaCadena("");
		strOutLine = "  TOTAL VENCIDO       $"
						+ strFiller.substring(0, 14 - strTotalVen.length())
						+ strTotalVen;
		tpImpresora.enviaCadena(strOutLine);
		
		tpImpresora.enviaCadena("");
		strOutLine = strFiller.substring(0, 5)
						+ "___________________________________";
		tpImpresora.enviaCadena(strOutLine);
		strOutLine = strFiller.substring(0, 5)
						+ "TOTAL PAGADO        $"
						+ strFiller.substring(0, 14 - strTotalPag.length())
						+ strTotalPag;
		tpImpresora.enviaCadena(strOutLine);
		strOutLine = strFiller.substring(0, 5)
						+ "TOTAL ADEUDADO      $"
						+ strFiller.substring(0, 14 - strTotalAde.length())
						+ strTotalAde;
		tpImpresora.enviaCadena(strOutLine);
		strOutLine = strFiller.substring(0, 5)
						+ "___________________________________";
		tpImpresora.enviaCadena(strOutLine);
		
		if (iDriver == 0) {
		tpImpresora.InserteDocumento();
		tpImpresora.enviaCadena("");
		tpImpresora.enviaCadena("");
		tpImpresora.enviaCadena("");
		tpImpresora.enviaCadena("");
		tpImpresora.enviaCadena("");
		tpImpresora.enviaCadena("");
		tpImpresora.enviaCadena("");
		tpImpresora.enviaCadena("");
		tpImpresora.enviaCadena("");
		tpImpresora.enviaCadena("");
		tpImpresora.enviaCadena("");
		tpImpresora.enviaCadena("");
		tpImpresora.enviaCadena("");
		tpImpresora.enviaCadena("");
		tpImpresora.enviaCadena("");
		tpImpresora.enviaCadena("");		
			tpImpresora.imprimeCadena1();
			tpImpresora.activateControl("CORTAPAPEL",iDriver);
		} else {
			tpImpresora.activateControl("SALTOHOJA",iDriver);
			tpImpresora.muestraVentana();
		}
			
	}
	return intStatus;
	}
}*/