//*************************************************************************************************
//	   Funcion: Clase que certifica RAP
//	  Elemento: RAP.java
//	Creado por: Juan Carlos Gaona Marcial
//Modificado por: Rodrigo Escand�n Soto
//*************************************************************************************************
//CCN - 4620037	- 21/11/2007 - Se actualiza formato de ticket
//CCN - 		- 25/11/2007 - Se quita la leyenda "Con la recepci�n de este documento..."
//*************************************************************************************************

import java.util.*;

public class RAP {
	public int imprimeRap(Vector vDatos, String strRespuesta,
			String strDatosSuc, String strCajero, String strDate,
			String strTime,
			// String strMontoRecibido,
			int iDriver, String strIdentidad, String strIdentidad2) {
		String strIva = "";
		String strComision = "";
		String strNombre = "";
		String strDomicilio = "";
		String strRfc = "";
		String strFiller = "                                                                      ";
		String strJustificacion = "                                        ";
		String strTmp = "";
		String strSucursal = "";
		String strPlaza = "";
		String strDivisa = "";
		String strEfectivo = new String("");
		String strMonto = new String("");
		String strEfectivoAux = new String("");
		String strMontoAux = new String("");
		String strTxn = "";
		// String strTxnPos = "";
		String strMoneda = "";
		String strConsecutivo = "";
		String strReferencia1 = "";
		String strReferencia1Aux = "";
		String strReferencia2 = "";
		String strReferencia3 = "";
		String strServicio = "";
		String strServicioAnt = "";
		String strCertificacion = "";
		String strNomServ = "";
		Vector vCamposT = new Vector();
		Vector vCamposTT = new Vector();
		Vector vCampos = new Vector();
		Vector vParametros = new Vector();
		Vector vDatosSuc = new Vector();
		Vector vCamposF = new Vector();//JGA impresion
		String strFolio = ""; //JGA impresion
		//JGA


		int i = 0;
		int intSize = 0;
		int intVeces = 0;
		int intNoCampos = 10;
		int intBeneficiario = 0;
		ToolsPrinter tpImpresora = new ToolsPrinter();

		int intStatus = tpImpresora.verificaStatus();
		if ((intStatus == 95) || (intStatus == 88)) {
			intStatus = 0;
			vDatosSuc = tpImpresora.Campos(strDatosSuc, "@");
			vCampos = tpImpresora.Campos(strRespuesta, "^");
			intSize = vCampos.size();
			for (i = 0; i < intSize; i++) {
				strTmp = (String) vCampos.elementAt(i);
				if (strTmp.startsWith("MOVIMIENTOS_RAP")) {
					strTmp = (String) vCampos.elementAt(i);
					vCamposT = tpImpresora.Campos(strTmp, "@");
					strTmp = (String) vCamposT.elementAt(1);
					vCamposTT = tpImpresora.Campos(strTmp, "~");
					for (int j = 0; j < vCamposTT.size() - 1; j++) {
						strTmp = (String) vCamposTT.elementAt(j);
						if ((strTmp.startsWith("|")) && (strTmp.length() > 1)){
							strTmp = strTmp.substring(1, strTmp.length());
						}
						else if (strTmp.startsWith("|")){
							strTmp = " ";
						}
						vParametros.addElement(strTmp);
					}
				}
				
				if (strTmp.startsWith("FOLIO")) {
					vCamposF = tpImpresora.Campos(strTmp, "~");
					strFolio = (String)vCamposF.elementAt(1);
					strFolio = strFolio.trim();	
					
				}
			}
			intVeces = vParametros.size() / intNoCampos;
			String strTipoDoc = new String("");
			String strStatus = "";
			int intStatusR = 0;
			String strComp = new String("");
			String strNomSuc = (String) vDatosSuc.elementAt(0);
			strNomSuc = strNomSuc.substring(7, strNomSuc.length());
			strSucursal = (String) vDatosSuc.elementAt(1);
			strSucursal = strSucursal.substring(7, 11);
			strPlaza = (String) vDatosSuc.elementAt(3);
			strPlaza = strPlaza.substring(7, strPlaza.length());
			String strDomSuc = (String) vDatosSuc.elementAt(2);
			strDomSuc = strDomSuc.substring(7, strDomSuc.length());
			tpImpresora.activateControl("CERTIFICACION", iDriver);
			for (i = 0; i < intVeces;) {
				String valorAnt = (String) vParametros
						.elementAt(3 + (i * intNoCampos));
				String valorAct = (String) vParametros
						.elementAt(3 + (i * intNoCampos));
				while (valorAnt.equals(valorAct)) {
					strTmp = (String) vParametros
							.elementAt(4 + (i * intNoCampos));
					if (strTmp.equals("1")) {
						/*
						 * if (i != 0) { if (iDriver == 0) {
						 * tpImpresora.InserteDocumento();
						 * tpImpresora.activateControl("SELECCIONASLIP",iDriver);
						 * tpImpresora.enviaCadena("");
						 * tpImpresora.enviaCadena("");
						 * tpImpresora.enviaCadena("");
						 * tpImpresora.enviaCadena("");
						 * tpImpresora.enviaCadena("");
						 * tpImpresora.enviaCadena("");
						 * tpImpresora.enviaCadena("");
						 * tpImpresora.imprimeCadena1();
						 * tpImpresora.activateControl("SALTOHOJA",iDriver);
						 * tpImpresora.activateControl("SELECCIONAJOURNAL",iDriver); }
						 * else {
						 * tpImpresora.activateControl("SALTOHOJA",iDriver);
						 * tpImpresora.muestraVentana(); } }
						 */
						strDivisa = (String) vParametros
								.elementAt(9 + (i * intNoCampos));
						strDivisa = strDivisa.trim();
						if (strDivisa.equals("N$")) {
							strDivisa = "   $";
							strMoneda = "1";
						} else
							strMoneda = "2";
						strEfectivo = (String) vParametros
								.elementAt(8 + (i * intNoCampos));
						strMonto = (String) vParametros
								.elementAt(1 + (i * intNoCampos));
						strTxn = (String) vParametros
								.elementAt(2 + (i * intNoCampos));
						strConsecutivo = (String) vParametros
								.elementAt(6 + (i * intNoCampos));
						strServicio = (String) vParametros
								.elementAt(0 + (i * intNoCampos));
						if (strServicio.length() > 0) {
							strStatus = strServicio.substring(0, 1);
							strServicio = strServicio.substring(1, strServicio
									.length());
						}
						strNomServ = (String) vCamposTT
								.elementAt(5 + (intNoCampos * (intVeces - 1)));

						if (strTxn.equals("5503")) {
							strRfc = "";
							if (!strServicioAnt.equals(strServicio)){
								if ((++intBeneficiario) > 4){
									intBeneficiario = 1;
								}
							}
							strComp = (String) vDatos
									.elementAt(5 + intBeneficiario);
							strServicioAnt = strServicio;
							strTmp = (String) vParametros
									.elementAt(7 + (i * intNoCampos));
							vCamposT = tpImpresora.Campos(strTmp, "&");
							if (vCamposT.size() > 2) {
								strReferencia1 = (String) vCamposT.elementAt(0);
								strReferencia2 = (String) vCamposT.elementAt(1);
								strReferencia3 = (String) vCamposT.elementAt(2);
							} else {
								strReferencia1 = " ";
								strReferencia2 = " ";
								strReferencia3 = " ";
							}
							if (strStatus.equals("R")) {
								intStatusR = 1;
								tpImpresora.muestraRechazo();
							}
							strTipoDoc = strServicio;
							strEfectivoAux = strEfectivo;
							strMontoAux = strMonto;
							strReferencia1Aux = strReferencia1;
						}

						if (strTxn.equals("0736") || strTxn.equals("0792")) {
							strTmp = (String) vParametros
									.elementAt(7 + (i * intNoCampos));
							vCamposT = tpImpresora.Campos(strTmp, "&");

							if (vCamposT.size() > 2) {
								strNombre = (String) vCamposT.elementAt(0);
								strDomicilio = (String) vCamposT.elementAt(1);
								strRfc = (String) vCamposT.elementAt(2);
							}

							else {
								strNombre = " ";
								strDomicilio = " ";
								strRfc = "";
							}
							strComision = strMonto;
						}
						if (strTxn.equals("0372")){
							strIva = strMonto;
						}
						
						strCertificacion = strConsecutivo + " "
								+ strDate.substring(7, 9)
								+ strDate.substring(5, 7)
								+ strDate.substring(1, 5) + " " + strTxn + " "
								+ strCajero + " ";
						strCertificacion = strCertificacion + strServicio + " ";
						if (!strEfectivo.equals(""))
							strCertificacion = strCertificacion
									+ strDivisa
									+ tpImpresora.formatMonto(strEfectivo
											.trim()) + " ";
						if (!strMonto.equals("")){
							strCertificacion = strCertificacion + "   "
									+ strDivisa
									+ tpImpresora.formatMonto(strMonto.trim())
									+ " ";
						}
						strCertificacion = strCertificacion + strSucursal;

						if (strServicio.equals("4254") && intStatusR != 0) {
							tpImpresora
									.enviaCadena("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
							tpImpresora
									.enviaCadena("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
							tpImpresora.enviaCadena("\n\n\n\n\n\n\n\n\n");
							tpImpresora.enviaCadena("          "
									+ strCertificacion);
						} else if (intStatusR != 0){
							tpImpresora.enviaCadena(strJustificacion
									+ strCertificacion);
						}
						strCertificacion = new String("");
						strCertificacion = strCertificacion + strReferencia1
								+ " ";
						strCertificacion = strCertificacion + strReferencia2
								+ " ";
						strCertificacion = strCertificacion + strReferencia3;
						if (!strCertificacion.trim().equals("")
								&& intStatusR != 0) {
							if (strServicio.equals("4254"))
								tpImpresora.enviaCadena("          "
										+ strCertificacion.trim());
							else
								tpImpresora.enviaCadena(strJustificacion
										+ strCertificacion.trim());
						}
						strCertificacion = (String) vParametros
								.elementAt(5 + (i * intNoCampos));

						if (strServicio.equals("4254") && intStatusR != 0)
							tpImpresora.enviaCadena("          "
									+ strCertificacion);
						else if (intStatusR != 0)
							tpImpresora.enviaCadena(strJustificacion
									+ strCertificacion);
					} else {
						strTmp = (String) vParametros
								.elementAt(5 + (i * intNoCampos));
						if (strServicio.equals("4254") && intStatusR != 0)
							tpImpresora.enviaCadena("          " + strTmp);
						else if (intStatusR != 0)
							tpImpresora.enviaCadena(strJustificacion + strTmp);
					}

					i++;
					if (i < intVeces)
						valorAct = (String) vParametros
								.elementAt(3 + (i * intNoCampos));
					else
						valorAct = null;
				}

				if (iDriver == 0 && intStatusR != 0) {
					tpImpresora.InserteDocumento();
					tpImpresora.activateControl("SELECCIONASLIP", iDriver);
					tpImpresora.enviaCadena("");
					tpImpresora.enviaCadena("");
					tpImpresora.enviaCadena("");
					tpImpresora.enviaCadena("");
					tpImpresora.enviaCadena("");
					tpImpresora.imprimeCadena1();
					tpImpresora.activateControl("SALTOHOJA", iDriver);
					tpImpresora.activateControl("SELECCIONAJOURNAL", iDriver);
				} else {
					tpImpresora.activateControl("SALTOHOJA", iDriver);
				}

				if (intStatusR == 0) {

					if (strTxn.equals("5503")) {

						// Imprime Sucursal y fecha
						strCertificacion = "SUCURSAL: " + strSucursal + "-"
								+ strNomSuc;
						tpImpresora.enviaCadena(strCertificacion);
						strCertificacion = strDate.substring(7, 9) + "-"
								+ strDate.substring(5, 7) + "-"
								+ strDate.substring(1, 5) + "    "
								+ strSucursal + strCajero + " \n";
						tpImpresora.enviaCadena(strCertificacion);

						// Imprime T�tulo
						tpImpresora.enviaCadena("   AGENTE RECEPTOR DE PAGOS");
						tpImpresora
								.enviaCadena("   DEPOSITO EFECTIVO / MIXTO\n");

						// Imprime flujo de tansacciones
						strCertificacion = "   TXN    CONSEC";
						tpImpresora.enviaCadena(strCertificacion);
						strCertificacion = "   " + strTxn + "   "
								+ strConsecutivo;
						tpImpresora.enviaCadena(strCertificacion);
						tpImpresora.enviaCadena("");

						// Imprime clave y nombre del servicio
						strCertificacion = "CLAVE DE SERVICIO:    "
								+ strServicio;
						tpImpresora.enviaCadena(strCertificacion);
						strCertificacion = "NOMBRE DEL SERVICIO:  "
								+ strNomServ;
						tpImpresora.enviaCadena(strCertificacion);

						if (!strTipoDoc.equals("60")
								&& !strTipoDoc.equals("670")
								&& !strTipoDoc.equals("669")) {
							/*
							 * strTmp = (String)
							 * vDatos.elementAt(intBeneficiario);
							 * strCertificacion = " Pago de : " + strTmp;
							 * tpImpresora.enviaCadena(strCertificacion);
							 * strCertificacion = " Servicio : " + strTipoDoc +
							 * "\n"; tpImpresora.enviaCadena(strCertificacion);
							 */

							if (!strReferencia3.equals("")) {
								strCertificacion = "REF 1: " + strReferencia1;
								tpImpresora.enviaCadena(strCertificacion);
								strCertificacion = "REF 2: " + strReferencia2;
								tpImpresora.enviaCadena(strCertificacion);
								strCertificacion = "REF 3: " + strReferencia3;
								tpImpresora.enviaCadena(strCertificacion);
							} else if (!strReferencia2.equals("")) {
								strCertificacion = "REF 1: " + strReferencia1;
								tpImpresora.enviaCadena(strCertificacion);
								strCertificacion = "REF 2: " + strReferencia2;
								tpImpresora.enviaCadena(strCertificacion);
							} else {
								strCertificacion = "REF 1: " + strReferencia1;
								tpImpresora.enviaCadena(strCertificacion);
							}
						} else {
							if (strTipoDoc.equals("60")) {
								strCertificacion = "   PAGO DE TARJETA DE CREDITO \n";
								tpImpresora.enviaCadena(strCertificacion);
							} else if (strTipoDoc.equals("669")
									|| strTipoDoc.equals("670")) {
								strCertificacion = "PAGO DE AMERICAN EXPRESS";
								tpImpresora.enviaCadena(strCertificacion);
								strCertificacion = "RFC : AEC-810901-298\n";
								tpImpresora.enviaCadena(strCertificacion);
							}
							strCertificacion = "  Numero de Tarjeta : "
									+ strReferencia1Aux;
							tpImpresora.enviaCadena(strCertificacion);
						}

						strTmp = tpImpresora.formatMonto(strEfectivoAux);
						//
						
						///FOLIO JGA
						
						/////////////////////////JGA
						if( strTxn.equals("5503") ){
							if (!strFolio.equals("")){
								tpImpresora.enviaCadena("FOLIO:"+strFolio);
							}
							
						}
						////////////////
						///
						
						strCertificacion = tpImpresora
								.limpiaEspacio(strCertificacion);
						strDivisa = tpImpresora.limpiaEspacio(strDivisa);
						strCertificacion = "\n   EFECTIVO   : " + strDivisa
						// + strFiller.substring(0, 18 - strTmp.length())
								+ strTmp;
						/*strCertificacion = strFiller.substring(0,
								(38 - strCertificacion.length()) / 2)
								+ strCertificacion;*/
						tpImpresora.enviaCadena(strCertificacion);
						/*
						 * hgrc long longDocumentos =
						 * Long.parseLong(strMontoAux) -
						 * Long.parseLong(strEfectivoAux); String strDoc = "" +
						 * longDocumentos; strTmp =
						 * tpImpresora.formatMonto(strDoc);
						 * 
						 * 
						 */

						// strCertificacion =
						// tpImpresora.limpiaEspacio(strCertificacion);
						/*
						 * strCertificacion = " DOCUMENTO : " + strDivisa //+
						 * strFiller.substring(0, 18 - strTmp.length()) +
						 * strTmp; strCertificacion = strFiller.substring(0, (38 -
						 * strCertificacion.length())/2) + strCertificacion;
						 * tpImpresora.enviaCadena(strCertificacion);
						 * 
						 * fin HGRC
						 */

						String strRecargo = (String) vDatos.elementAt(10);
						String strMontoRecargo = (String) vDatos.elementAt(11);
						String strDescuento = (String) vDatos.elementAt(12);
						String strMontoDescuento = (String) vDatos
								.elementAt(13);
						String strMontoA = (String) vDatos.elementAt(14);
						String strMontoAPagar = (String) vDatos.elementAt(15);
						if (strRecargo.equals("R")) {
							strTmp = tpImpresora.formatMonto(strMontoRecargo);
							strCertificacion = "  Recargo     : "
									+ strDivisa
									+ strFiller.substring(0, 18 - strTmp
											.length()) + strTmp;
							tpImpresora.enviaCadena(strCertificacion);
						}
						if (strDescuento.equals("D")) {
							strTmp = tpImpresora.formatMonto(strMontoDescuento);
							strCertificacion = "  Descuento   : "
									+ strDivisa
									+ strFiller.substring(0, 18 - strTmp
											.length()) + strTmp;
							tpImpresora.enviaCadena(strCertificacion);
						}
						if (strMontoA.equals("T")) {
							strTmp = tpImpresora.formatMonto(strMontoAPagar);
							strCertificacion = "  Monto a Pagar "
									+ strDivisa
									+ strFiller.substring(0, 18 - strTmp
											.length()) + strTmp;
							tpImpresora.enviaCadena(strCertificacion);
							strMontoAux = strMontoAPagar;
						}

						/*
						 * else { strTmp = tpImpresora.formatMonto(strMontoAux);
						 * strCertificacion = " Monto : " + strDivisa +
						 * strFiller.substring(0, 18 - strTmp.length()) +
						 * strTmp; tpImpresora.enviaCadena(strCertificacion); }
						 * strCertificacion = " Cantidad : " + Currency.convert(
						 * tpImpresora.formatMonto(strMontoAux),
						 * Integer.parseInt(strMoneda)) + " \n";
						 * tpImpresora.enviaCadena(strCertificacion);
						 */

						
						//
						/*
						 * strTmp = tpImpresora.limpiaEspacio(strTmp); strDivisa =
						 * tpImpresora.limpiaEspacio(strDivisa);
						 * strCertificacion = " TOTAL DOCUMENTO : " + strDivisa
						 * //+ strFiller.substring(0, 18 - strTmp.length()) +
						 * strTmp; tpImpresora.enviaCadena(strCertificacion);
						 * 
						 * strTmp = tpImpresora.formatMonto(strMonto); // strTmp =
						 * tpImpresora.limpiaEspacio(strTmp); strDivisa =
						 * tpImpresora.limpiaEspacio(strDivisa);
						 * strCertificacion = " TOTAL DEPOSITO : " + strDivisa
						 * //+ strFiller.substring(0, 18 - strTmp.length()) +
						 * strTmp;
						 */
						// hgrc Cambio para Cheque Devuelto +
						//CQACEP
						String strCQAceptado="";
						if (strRespuesta.contains("/#/")) {
							String[] strChqAcep = strRespuesta.split("/#/");
							for (int iAp = 1; iAp < strChqAcep.length - 1; iAp++) {
								strCQAceptado = strChqAcep[iAp].substring(0,strChqAcep[iAp].indexOf("!"));
								strCQAceptado = tpImpresora.formatMonto(strCQAceptado);
								tpImpresora.enviaCadena("   DOCUMENTO "+iAp+" : "+ strDivisa + strCQAceptado);
								tpImpresora.enviaCadena("   SERIAL "+iAp+"    :"+strChqAcep[iAp].substring(strChqAcep[iAp].lastIndexOf("!")+1));
								strDivisa = tpImpresora.limpiaEspacio(strDivisa);
							}
						}
						
						//CQDEV
						String strAceptados, strChqdev = "";
						int punto;
						if (strRespuesta.contains("|^=")) {
							String[] strChqDev = strRespuesta.split("=");
							double intChqdev = 0;
							for (int iCq = 1; iCq < strChqDev.length - 1; iCq++) {
								if(strChqDev[iCq].contains(",")){
								punto = strChqDev[iCq].lastIndexOf(',');
								strChqDev[iCq]= strChqDev[iCq].substring(0,strChqDev[iCq].indexOf(',')) + strChqDev[iCq].substring(punto + 1);
								}
								punto = strChqDev[iCq].lastIndexOf('.');
								strChqDev[iCq]= strChqDev[iCq].substring(0,strChqDev[iCq].indexOf('.')) + strChqDev[iCq].substring(punto + 1);
								intChqdev = Double.valueOf(strChqDev[iCq]).doubleValue()+ intChqdev;
							}
							strChqdev = String.valueOf(intChqdev);
							strAceptados = String.valueOf(Double.valueOf(strMonto).doubleValue()
											- Double.valueOf(strEfectivoAux).doubleValue()
											- Double.valueOf(strChqdev).doubleValue());
							strChqdev = tpImpresora.formatMonto(strChqdev.substring(0,strChqdev.indexOf('.')));
						} else {
							strAceptados = String.valueOf(Double.valueOf(strMonto).doubleValue()
											- Double.valueOf(strEfectivoAux).doubleValue());
							strChqdev = "0.00";
						}
						tpImpresora.enviaCadena("");
						tpImpresora.enviaCadena("");
						strMonto = tpImpresora.formatMonto(strMonto);
						strAceptados = tpImpresora.formatMonto(strAceptados.substring(0,strAceptados.indexOf('.')));				

						strCertificacion = " TOTAL DOCS ACEPTADOS : "
								+ strDivisa + strAceptados;
						tpImpresora.enviaCadena(strCertificacion);
						strDivisa = tpImpresora.limpiaEspacio(strDivisa);
						
						strCertificacion = " TOTAL DOCS DEVUELTOS : "
								+ strDivisa + strChqdev;
						tpImpresora.enviaCadena(strCertificacion);
						strDivisa = tpImpresora.limpiaEspacio(strDivisa);
						
						strCertificacion = "       TOTAL DEPOSITO : "
								+ strDivisa + strMonto;
						tpImpresora.enviaCadena(strCertificacion);
						// FIN hgrc Cambio para Cheque Devuelto */

						tpImpresora.enviaCadena("\n\n\n\n          FIRMA DEL CLIENTE\n\n\n");
						tpImpresora.enviaCadena("\n       SELLO Y FIRMA DEL CAJERO");
						// tpImpresora.enviaCadena("\n\n \"Con la recepci�n de
						// este comprobante el depositante acepta");
						// tpImpresora.enviaCadena(" que los datos aqu�
						// contenidos son correctos\"");
						tpImpresora
								.enviaCadena("\n\n\n               "+strIdentidad2+"    ");

						if (iDriver == 0) {
							tpImpresora.enviaCadena("");
							tpImpresora.enviaCadena("");
							tpImpresora.enviaCadena("");
							tpImpresora.enviaCadena("");
							tpImpresora.enviaCadena("");
							tpImpresora.enviaCadena("");
							tpImpresora.enviaCadena("");
							tpImpresora.enviaCadena("");
							tpImpresora.enviaCadena("");
							tpImpresora.enviaCadena("");
							tpImpresora.enviaCadena("");
							tpImpresora.enviaCadena("");
							tpImpresora.enviaCadena("");
							tpImpresora.enviaCadena("");
							tpImpresora.enviaCadena("");
							tpImpresora.enviaCadena("");
							tpImpresora.imprimeCadena1();
							tpImpresora.activateControl("CORTAPAPEL", iDriver);
						} else
							tpImpresora.activateControl("SALTOHOJA", iDriver);

						/*
						 * if (strTipoDoc.equals("669") ||
						 * strTipoDoc.equals("670")) strCertificacion = " ESTE
						 * ES UN RECIBO DE PAGO, NO ES UN COMPROBANTE FISCAL ";
						 * else strCertificacion = " ESTE DOCUMENTO NO ES UN
						 * COMPROBANTE FISCAL ";
						 * tpImpresora.enviaCadena(strCertificacion); if
						 * (iDriver == 0) { tpImpresora.InserteDocumento();
						 * tpImpresora.activateControl("SELECCIONASLIP",iDriver);
						 * tpImpresora.enviaCadena("");
						 * tpImpresora.enviaCadena("");
						 * tpImpresora.enviaCadena("");
						 * tpImpresora.enviaCadena("");
						 * tpImpresora.enviaCadena("");
						 * tpImpresora.imprimeCadena1();
						 * tpImpresora.activateControl("SALTOHOJA",iDriver);
						 * tpImpresora.activateControl("SELECCIONAJOURNAL",iDriver); }
						 * else {
						 * tpImpresora.activateControl("SALTOHOJA",iDriver); }
						 */
					}

					if (strComp.equals("S") || strTipoDoc.equals("60")
							|| strTipoDoc.equals("670")
							|| strTipoDoc.equals("669")) {
						if (iDriver == 1)
							tpImpresora.muestraVentana();
						strCertificacion = "                      "+strIdentidad+"     P A N A M A \n";
						tpImpresora.enviaCadena(strCertificacion);
						strCertificacion = "  Plaza    : "
								+ strPlaza
								+ strFiller
										.substring(0, 40 - strPlaza.length())
								+ "  Sucursal : " + strSucursal;
						tpImpresora.enviaCadena(strCertificacion);
						strCertificacion = "  Cajero   : "
								+ strCajero
								+ strFiller.substring(0, 22 - strCajero
										.length()) + "Fecha: "
								+ strDate.substring(7, 9) + "/"
								+ strDate.substring(5, 7) + "/"
								+ strDate.substring(1, 5) + "       Hora : "
								+ strTime.substring(0, 2) + ":"
								+ strTime.substring(2, 4) + ":"
								+ strTime.substring(4, 6) + " \n";
						tpImpresora.enviaCadena(strCertificacion);
						if (!strTipoDoc.equals("60")
								&& !strTipoDoc.equals("670")
								&& !strTipoDoc.equals("669")) {
							strTmp = (String) vDatos.elementAt(intBeneficiario);
							strCertificacion = "         Pago de  : " + strTmp;
							tpImpresora.enviaCadena(strCertificacion);
							strCertificacion = "         Servicio : "
									+ strTipoDoc + "\n";
							tpImpresora.enviaCadena(strCertificacion);
							strCertificacion = "  Referencia 1: "
									+ strReferencia1;
							tpImpresora.enviaCadena(strCertificacion);
							strCertificacion = "  Referencia 2: "
									+ strReferencia2;
							tpImpresora.enviaCadena(strCertificacion);
							strCertificacion = "  Referencia 3: "
									+ strReferencia3;
							tpImpresora.enviaCadena(strCertificacion);
						} else {
							if (strTipoDoc.equals("60")) {
								strCertificacion = "                 PAGO DE TARJETA DE CREDITO \n";
								tpImpresora.enviaCadena(strCertificacion);
							} else if (strTipoDoc.equals("669")
									|| strTipoDoc.equals("670")) {
								strCertificacion = "PAGO DE AMERICAN";
								tpImpresora.enviaCadena(strCertificacion);
								strCertificacion = "RFC : AEC-810901-298\n";
								tpImpresora.enviaCadena(strCertificacion);
							}
							strCertificacion = "  Numero de Tarjeta : "
									+ strReferencia1Aux;
							tpImpresora.enviaCadena(strCertificacion);
						}
						strTmp = tpImpresora.formatMonto(strEfectivoAux);
						strCertificacion = "  Efectivo    : " + strDivisa
								+ strFiller.substring(0, 18 - strTmp.length())
								+ strTmp;
						tpImpresora.enviaCadena(strCertificacion);
						long longDocumentos = Long.parseLong(strMontoAux)
								- Long.parseLong(strEfectivoAux);
						String strDoc = "" + longDocumentos;
						strTmp = tpImpresora.formatMonto(strDoc);
						strCertificacion = "  Documentos  : " + strDivisa
								+ strFiller.substring(0, 18 - strTmp.length())
								+ strTmp;
						tpImpresora.enviaCadena(strCertificacion);
						String strRecargo = (String) vDatos.elementAt(10);
						String strMontoRecargo = (String) vDatos.elementAt(11);
						String strDescuento = (String) vDatos.elementAt(12);
						String strMontoDescuento = (String) vDatos
								.elementAt(13);
						String strMontoA = (String) vDatos.elementAt(14);
						String strMontoAPagar = (String) vDatos.elementAt(15);
						if (strRecargo.equals("R")) {
							strTmp = tpImpresora.formatMonto(strMontoRecargo);
							strCertificacion = "  Recargo     : "
									+ strDivisa
									+ strFiller.substring(0, 18 - strTmp
											.length()) + strTmp;
							tpImpresora.enviaCadena(strCertificacion);
						}
						if (strDescuento.equals("D")) {
							strTmp = tpImpresora.formatMonto(strMontoDescuento);
							strCertificacion = "  Descuento   : "
									+ strDivisa
									+ strFiller.substring(0, 18 - strTmp
											.length()) + strTmp;
							tpImpresora.enviaCadena(strCertificacion);
						}
						if (strMontoA.equals("T")) {
							strTmp = tpImpresora.formatMonto(strMontoAPagar);
							strCertificacion = "  Monto a Pagar "
									+ strDivisa
									+ strFiller.substring(0, 18 - strTmp
											.length()) + strTmp;
							tpImpresora.enviaCadena(strCertificacion);
							strMontoAux = strMontoAPagar;
						} else {
							strTmp = tpImpresora.formatMonto(strMontoAux);
							strCertificacion = "  Monto       : "
									+ strDivisa
									+ strFiller.substring(0, 18 - strTmp
											.length()) + strTmp;
							tpImpresora.enviaCadena(strCertificacion);
						}
						strCertificacion = "  Cantidad    : "
								+ Currency.convert(tpImpresora
										.formatMonto(strMontoAux), Integer
										.parseInt(strMoneda)) + " \n";
						tpImpresora.enviaCadena(strCertificacion);
						if (strTipoDoc.equals("669")
								|| strTipoDoc.equals("670"))
							strCertificacion = "  ESTE ES UN RECIBO DE PAGO, NO ES UN COMPROBANTE FISCAL ";
						else
							strCertificacion = "  ESTE DOCUMENTO NO ES UN COMPROBANTE FISCAL ";
						tpImpresora.enviaCadena(strCertificacion);
						if (iDriver == 0) {
							tpImpresora.InserteDocumento();
							tpImpresora.activateControl("SELECCIONASLIP",
									iDriver);
							tpImpresora.enviaCadena("");
							tpImpresora.enviaCadena("");
							tpImpresora.enviaCadena("");
							tpImpresora.enviaCadena("");
							tpImpresora.enviaCadena("");
							tpImpresora.imprimeCadena1();
							tpImpresora.activateControl("SALTOHOJA", iDriver);
							tpImpresora.activateControl("SELECCIONAJOURNAL",
									iDriver);
						} else {
							tpImpresora.activateControl("SALTOHOJA", iDriver);
						}
					}
					if (!strRfc.equals("")) {
						if (iDriver == 1){
							tpImpresora.muestraVentana();
						}
						tpImpresora.enviaCadena("\n\n\n\n\n\n");
						strCertificacion = "            " + strPlaza
								+ "          " + strDate.substring(7, 9) + "/"
								+ strDate.substring(5, 7) + "/"
								+ strDate.substring(1, 5) + "\n";
						tpImpresora.enviaCadena(strCertificacion);
						strCertificacion = "                         "
								+ strDomSuc;
						tpImpresora.enviaCadena(strCertificacion);
						if (strNombre.length() > 40){
							strNombre = strNombre.substring(0, 40);
							}
						strCertificacion = "         "
								+ strNombre
								+ strFiller.substring(0, 40 - strNombre
										.length()) + strRfc;
						tpImpresora.enviaCadena(strCertificacion);
						strCertificacion = "            " + strDomicilio
								+ "\n\n";
						tpImpresora.enviaCadena(strCertificacion);
						tpImpresora
								.enviaCadena("          SERVICIOS DE CONVENIENCIA\n");
						tpImpresora.enviaCadena("          PAGO DE SERVICIOS");
						strIva = strIva.trim();
						strComision = strComision.trim();
						long longTotal = Long.parseLong(strComision)
								+ Long.parseLong(strIva);
						strIva = tpImpresora.formatMonto(strIva);
						strComision = tpImpresora.formatMonto(strComision);
						strDivisa = strDivisa.trim();
						strTmp = strFiller.substring(0, 28)
								+ strDivisa
								+ strFiller.substring(0, 18 - strComision
										.length()) + strComision;
						tpImpresora.enviaCadena(strTmp);
						strTmp = strFiller.substring(0, 28) + strDivisa
								+ strFiller.substring(0, 18 - strIva.length())
								+ strIva;
						tpImpresora.enviaCadena(strTmp);
						String strTotalT = new String("");
						strTotalT = String.valueOf(longTotal);
						strTotalT = tpImpresora.formatMonto(strTotalT);
						strTmp = strFiller.substring(0, 28)
								+ strDivisa
								+ strFiller.substring(0, 18 - strTotalT
										.length()) + strTotalT;
						tpImpresora.enviaCadena(strTmp);
						tpImpresora.enviaCadena("          "
								+ Currency.convert(strTotalT, Integer
										.parseInt(strMoneda)));
						if (iDriver == 0) {
							tpImpresora.InserteDocumento();
							tpImpresora.activateControl("SELECCIONASLIP",
									iDriver);
							tpImpresora.enviaCadena("");
							tpImpresora.enviaCadena("");
							tpImpresora.enviaCadena("");
							tpImpresora.enviaCadena("");
							tpImpresora.enviaCadena("");
							tpImpresora.imprimeCadena1();
							tpImpresora.activateControl("SALTOHOJA", iDriver);
							tpImpresora.activateControl("SELECCIONAJOURNAL",
									iDriver);
						} else {
							tpImpresora.activateControl("SALTOHOJA", iDriver);
						}
					}
				}
			}
		}
		return intStatus;
	}
}