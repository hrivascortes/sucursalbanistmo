import java.awt.*;
import java.awt.event.*;

public class SimpleDialog extends Dialog implements ActionListener {
	Button b;

	SimpleDialog(Frame dw, String title) {
		super(dw, "  Ventana de Estatus  ", true);

		Panel p2 = new Panel();
		p2.setBackground(java.awt.Color.white);
		p2.setLayout(new GridLayout(2, 1));
		Label label1 = new Label(title, Label.CENTER);
		p2.add("South", label1);
		Button b = new Button("Continuar");
		p2.add(b);
		b.addActionListener(this);
		b.setBackground(java.awt.Color.white);
		add("Center", p2);
		if (title.indexOf("Rechazada") > 0) {
			label1.setForeground(java.awt.Color.red);
			label1.setFont(new Font("Arial", Font.BOLD, 14));
		}
		if (title.indexOf("Inserte") > 0) {
			label1.setForeground(java.awt.Color.green);
			label1.setFont(new Font("Arial", Font.BOLD, 14));
		}
		pack();
	}

	public void actionPerformed(ActionEvent ae) {
		Button de = (Button) ae.getSource();

		if (de.getLabel().equals("Continuar") == true)
			System.gc();
		hide();
	}
}