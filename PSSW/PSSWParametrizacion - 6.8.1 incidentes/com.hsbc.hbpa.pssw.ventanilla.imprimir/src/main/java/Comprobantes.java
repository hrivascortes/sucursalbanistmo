import java.util.*;

public class Comprobantes {
	ToolsPrinter tpImpresora = new ToolsPrinter();

	public int imprimeCFiscal(
		String strDatosSuc,
		Vector datos,
		String strDate,
		int iDriver) {
		String strCertificacion = "";
		String strFiller =
			"                                                                                          ";
		String nombre = (String) datos.elementAt(1);
		String rfc = (String) datos.elementAt(2);
		String servicio = (String) datos.elementAt(3);
		String concepto = (String) datos.elementAt(4);
		String domicilio = (String) datos.elementAt(5);
		String comision = (String) datos.elementAt(6);
		String iva = (String) datos.elementAt(7);
		String total = (String) datos.elementAt(8);
		String direccion2 = "";
		String strMoneda = "01";
		String strSigno = new String("$");
		if (datos.size() > 10)
			strMoneda = (String) datos.elementAt(10);
		Vector vDatosSuc = new Vector();
		int intStatus = tpImpresora.verificaStatus();
		if ((intStatus == 95) || (intStatus == 88)) {
			if (comision
				.substring(comision.length() - 2, comision.length() - 1)
				.equals("."))
				comision = comision + "0";
			if (iva.substring(iva.length() - 2, iva.length() - 1).equals("."))
				iva = iva + "0";
			if (total
				.substring(total.length() - 2, total.length() - 1)
				.equals("."))
				total = total + "0";
			vDatosSuc = tpImpresora.Campos(strDatosSuc, "@");
			String strNumSuc = (String) vDatosSuc.elementAt(1);
			strNumSuc = strNumSuc.substring(7, strNumSuc.length());
			String strNomSuc = (String) vDatosSuc.elementAt(0);
			strNomSuc = strNomSuc.substring(7, strNomSuc.length());
			String strDomSuc = (String) vDatosSuc.elementAt(2);
			strDomSuc = strDomSuc.substring(7, strDomSuc.length());
			String strNomPza = (String) vDatosSuc.elementAt(4);
			strNomPza = strNomPza.substring(7, strNomPza.length());
			intStatus = 0;
			tpImpresora.activateControl("CFISCAL",iDriver);
			if (datos.size() > 9)
				direccion2 = (String) datos.elementAt(9);
			else
				direccion2 = strDomSuc;
			if (!total.equals("0")) {
				if (!strMoneda.equals("01"))
					strSigno = "$";
				tpImpresora.enviaCadena("\n\n\n\n\n\n");
				strCertificacion =
					"            "
						+ strNomPza
						+ "          "
						+ strDate.substring(1, 5)
						+ "/"
						+ strDate.substring(5, 7)
						+ "/"
						+ strDate.substring(7, 9);
				tpImpresora.enviaCadena(strCertificacion);
				strCertificacion = "                         " + direccion2;
				tpImpresora.enviaCadena(strCertificacion);
				strCertificacion =
					"         "
						+ nombre
						+ strFiller.substring(0, 40 - nombre.length())
						+ rfc;
				tpImpresora.enviaCadena(strCertificacion);
				strCertificacion = "            " + domicilio + "\n\n";
				tpImpresora.enviaCadena(strCertificacion);
				strCertificacion = "          " + servicio + "\n";
				tpImpresora.enviaCadena(strCertificacion);
				strCertificacion = "          " + concepto;
				tpImpresora.enviaCadena(strCertificacion);
				strCertificacion =
					strFiller.substring(0, 28)
						+ strSigno
						+ strFiller.substring(0, 18 - comision.length())
						+ comision;
				tpImpresora.enviaCadena(strCertificacion);
				strCertificacion =
					strFiller.substring(0, 28)
						+ strSigno
						+ strFiller.substring(0, 18 - iva.length())
						+ iva;
				tpImpresora.enviaCadena(strCertificacion);
				strCertificacion =
					strFiller.substring(0, 28)
						+ strSigno
						+ strFiller.substring(0, 18 - total.length())
						+ total;
				tpImpresora.enviaCadena(strCertificacion);
				if (strMoneda.equals("01"))
					strCertificacion =
						"          " + Currency.convert(total, 1);
				else
					strCertificacion =
						"          " + Currency.convert(total, 2);
				tpImpresora.enviaCadena(strCertificacion);
			} else
				tpImpresora.enviaCadena("PROBLEMAS DE COMUNICACION CON MERVA");
			if (iDriver == 0) {
				tpImpresora.InserteDocumento();
				tpImpresora.activateControl("SELECCIONASLIP",iDriver);
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.imprimeCadena1();
				tpImpresora.activateControl("SALTOHOJA",iDriver);
				tpImpresora.activateControl("SELECCIONAJOURNAL",iDriver);
			} else {
				tpImpresora.activateControl("SALTOHOJA",iDriver);
			}
		}
		return intStatus;
	}

	public int imprimeTransferencia(
		String tasa,
		String udi,
		String total,
		int iDriver) {
		int intStatus = tpImpresora.verificaStatus();
		if ((intStatus == 95) || (intStatus == 88)) {
			tpImpresora.activateControl("TRANSFERENCIA",iDriver);
			intStatus = 0;
			for (int noline = 0; noline < 18; noline++)
				tpImpresora.enviaCadena(" ");
			String outline =
				"     "
					+ tasa
					+ "                 "
					+ udi
					+ "                    "
					+ total;
			tpImpresora.enviaCadena(outline);
			if (iDriver == 0) {
				tpImpresora.InserteDocumento();
				tpImpresora.activateControl("SELECCIONASLIP",iDriver);
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.imprimeCadena1();
				tpImpresora.activateControl("SALTOHOJA",iDriver);
				tpImpresora.activateControl("SELECCIONAJOURNAL",iDriver);
			} else {
				tpImpresora.activateControl("SALTOHOJA",iDriver);
			}
		}
		return intStatus;
	}

	public int imprimeCTDC(
		Vector vDatos,
		String strDatosSuc,
		String strCajero,
		String strDate,
		String strTime,
		int iDriver,
		String strIdentidad) {
		int intStatus = tpImpresora.verificaStatus();
		if ((intStatus == 95) || (intStatus == 88)) {
			String strOutLine = new String("");
			String strFiller =
				new String("                                                  ");
			tpImpresora.activateControl("CERTIFICACION",iDriver);
			//tpImpresora.activateControl("NOMINA");

			String strTarjeta = (String) vDatos.elementAt(1);
			if (strTarjeta.equals(""))
				strTarjeta = " ";
			String strImporte = (String) vDatos.elementAt(2);
			if (strImporte.equals(""))
				strImporte = "0.00";
			String strComision = (String) vDatos.elementAt(3);
			if (strComision.equals(""))
				strComision = "0.00";
			String strIva = (String) vDatos.elementAt(4);
			if (strIva.equals(""))
				strIva = "0.00";
			String strTotal = (String) vDatos.elementAt(5);
			if (strTotal.equals(""))
				strTotal = "0.00";
			Vector vDatosSuc = tpImpresora.Campos(strDatosSuc, "@");
			String strNumSuc = (String) vDatosSuc.elementAt(1);
			strNumSuc = strNumSuc.substring(7, strNumSuc.length());
			String strNomSuc = (String) vDatosSuc.elementAt(0);
			strNomSuc = strNomSuc.substring(7, strNomSuc.length());
			String strDomSuc = (String) vDatosSuc.elementAt(2);
			strDomSuc = strDomSuc.substring(7, strDomSuc.length());
			String strNomPza = (String) vDatosSuc.elementAt(4);
			strNomPza = strNomPza.substring(7, strNomPza.length());
			tpImpresora.enviaCadena(
				"                                      "+strIdentidad+"\n");
			strOutLine =
				"      HORA     : "
					+ strTime.substring(0, 2)
					+ ":"
					+ strTime.substring(2, 4)
					+ ":"
					+ strTime.substring(4, 6);
			tpImpresora.enviaCadena(strOutLine);
			strOutLine =
				"      FECHA    : "
					+ strDate.substring(7, 9)
					+ "-"
					+ strDate.substring(5, 7)
					+ "-"
					+ strDate.substring(1, 5)
					+ "    CAJERO : "
					+ strCajero;
			tpImpresora.enviaCadena(strOutLine);
			strOutLine = "      SUCURSAL : " + strNumSuc;
			tpImpresora.enviaCadena(strOutLine);
			tpImpresora.enviaCadena(
				"________________________________________________________________________");
			tpImpresora.enviaCadena(
				"   TRANSACCION : 0476 DISPOSICION DE EFECTIVO CON TARJETA DE CREDITO.   \n\n");
			strOutLine =
				" NUMERO DE TARJETA  : "
					+ strTarjeta
					+ strFiller.substring(0, 20 - strTarjeta.length())
					+ "      MONEDA : DOLARES ";
			tpImpresora.enviaCadena(strOutLine);
			strOutLine =
				" IMPORTE SOLICITADO : "
					+ strFiller.substring(0, 15 - strImporte.length())
					+ strImporte;
			tpImpresora.enviaCadena(strOutLine);
			strOutLine =
				" COMISION           : "
					+ strFiller.substring(0, 15 - strComision.length())
					+ strComision;
			tpImpresora.enviaCadena(strOutLine);
			strOutLine =
				" I.V.A.             : "
					+ strFiller.substring(0, 15 - strIva.length())
					+ strIva
					+ "\n";
			tpImpresora.enviaCadena(strOutLine);
			strOutLine =
				" IMPORTE ENTREGADO  : "
					+ strFiller.substring(0, 15 - strTotal.length())
					+ strTotal
					+ "\n";
			tpImpresora.enviaCadena(strOutLine);
			tpImpresora.enviaCadena(
				"                                      ________________________");
			tpImpresora.enviaCadena(
				"                                        FIRMA DE CONFORMIDAD  ");
			if (iDriver == 0) {
				tpImpresora.InserteDocumento();
				tpImpresora.activateControl("SELECCIONASLIP",iDriver);
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.imprimeCadena1();
				tpImpresora.activateControl("SALTOHOJA",iDriver);
				tpImpresora.activateControl("SELECCIONAJOURNAL",iDriver);
			} else {
				tpImpresora.activateControl("SALTOHOJA",iDriver);
			}
			if (vDatos.size() > 6) {
				for (int i = 0; i < 6; i++)
					vDatos.remove(0);
				if (iDriver == 1)
					tpImpresora.muestraVentana();
				Comprobantes Comp = new Comprobantes();
				Comp.imprimeCFiscal(strDatosSuc, vDatos, strDate, iDriver);
			}
		}
		return intStatus;
	}

/*	public int imprimeTip(
		Vector vDatos,
		String strDatosSuc,
		String strCajero,
		String strDate,
		String strTime,
		String strRespuesta,
		int iDriver) {
		int intStatus = tpImpresora.verificaStatus();
		if ((intStatus == 95) || (intStatus == 88)) {
			String strOutLine = new String("");
			String strFiller =
				new String("                                                                                ");
			Vector vDatosTxn = new Vector();

			tpImpresora.activateControl("CERTIFICACION");
			vDatosTxn = tpImpresora.Campos(strRespuesta, "~");
			tpImpresora.enviaCadena("");
			Vector vDatosSuc = tpImpresora.Campos(strDatosSuc, "@");
			String strNomSuc = (String) vDatosSuc.elementAt(0);
			strNomSuc = strNomSuc.substring(7, strNomSuc.length());
			String strNumSuc = (String) vDatosSuc.elementAt(1);
			strNumSuc = strNumSuc.substring(7, strNumSuc.length());
			String strDomSuc = (String) vDatosSuc.elementAt(2);
			strDomSuc = strDomSuc.substring(7, strDomSuc.length());
			String strNomPza = (String) vDatosSuc.elementAt(4);
			strNomPza = strNomPza.substring(7, strNomPza.length());

			strOutLine = "SUCURSAL : " + strNumSuc + "-" + strNomSuc;
			tpImpresora.enviaCadena(strOutLine);
			tpImpresora.enviaCadena(" ");
			strOutLine = "   TXN    CONSEC";
			tpImpresora.enviaCadena(strOutLine);
			for (int veces = 0; veces < (vDatosTxn.size() / 2); veces++) {
				strOutLine =
					"   "
						+ (String) vDatosTxn.elementAt(veces * 2)
						+ "   "
						+ (String) vDatosTxn.elementAt(veces * 2 + 1);
				tpImpresora.enviaCadena(strOutLine);
			}

			String strServicio = (String) vDatos.elementAt(1);
			if (strServicio.equals(""))
				strServicio = " ";
			String strReferencia = (String) vDatos.elementAt(2);
			if (strReferencia.equals(""))
				strReferencia = "0.00";
			String strBeneficiario = (String) vDatos.elementAt(3);
			if (strBeneficiario.equals(""))
				strBeneficiario = "0.00";
			String strMonto = (String) vDatos.elementAt(4);
			if (strMonto.equals(""))
				strMonto = "0.00";
			String strCuenta = (String) vDatos.elementAt(5);
			String strMoneda = (String) vDatos.elementAt(6);
			String strSigno = "";
			if (strMoneda.equals("") || strMoneda.equals("1")) {
				strMoneda = "1";
				strSigno = "$";
			}
			if (strCuenta.equals(""))
				tpImpresora.enviaCadena(
					"                        COMPROBANTE DE PAGO EN EFECTIVO");
			else
				tpImpresora.enviaCadena(
					"                     COMPROBANTE DE PAGO CON ABONO A CUENTA");
			tpImpresora.enviaCadena(
				"                     TRANSFERENCIA INMEDIATA DE PAGOS \n");
			strOutLine =
				strFiller.substring(0, 55)
					+ "FECHA : "
					+ strDate.substring(7, 9)
					+ "/"
					+ strDate.substring(5, 7)
					+ "/"
					+ strDate.substring(1, 5);
			tpImpresora.enviaCadena(strOutLine);
			strOutLine =
				strFiller.substring(0, 55)
					+ "HORA  : "
					+ strTime.substring(0, 2)
					+ ":"
					+ strTime.substring(2, 4)
					+ ":"
					+ strTime.substring(4, 6);
			tpImpresora.enviaCadena(strOutLine);
			strOutLine = "SERVICIO     : " + strServicio;
			tpImpresora.enviaCadena(strOutLine);
			strOutLine = "REFERENCIA   : " + strReferencia;
			tpImpresora.enviaCadena(strOutLine);
			strOutLine = "BENEFICIARIO : " + strBeneficiario;
			tpImpresora.enviaCadena(strOutLine);
			if (!strMoneda.equals("1"))
				strSigno = "US$";
			strOutLine = "MONTO        : " + strSigno + " " + strMonto;
			tpImpresora.enviaCadena(strOutLine);
			if (strMoneda.equals("1"))
				strOutLine = "               " + Currency.convert(strMonto, 1);
			else
				strOutLine = "               " + Currency.convert(strMonto, 2);
			tpImpresora.enviaCadena(strOutLine);
			if (!strCuenta.equals("")) {
				strOutLine = "CUENTA DE ABONO : " + strCuenta;
				tpImpresora.enviaCadena(strOutLine);
			}
			tpImpresora.enviaCadena(" ");
			tpImpresora.enviaCadena("");
			tpImpresora.enviaCadena("");
			tpImpresora.enviaCadena("");
			tpImpresora.enviaCadena("");
			strOutLine =
				strFiller.substring(0, 23) + "FIRMA : ________________________";
			tpImpresora.enviaCadena(strOutLine);
			tpImpresora.enviaCadena("");
			tpImpresora.enviaCadena("");
			tpImpresora.enviaCadena(
				"ESTA FICHA CONSTITUYE UN FORMATO DE LIQUIDACION, LA FIRMA DEL BENEFICIARIO");
			tpImpresora.enviaCadena(
				"IMPLICA LA ACEPTACION DE QUE LA CANTIDAD ENTREGADA ES CORRECTA.   NO");
			tpImpresora.enviaCadena(
				"TENDRA NINGUNA RESPONSABILIDAD YA QUE OBEDECE A INSTRUCCIONES PRECISAS DE");
			tpImpresora.enviaCadena(
				"LA EMPRESA QUE REALIZA LA TRANSFERENCIA DE PAGO.");
			if (iDriver == 0) {
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.imprimeCadena1();
				tpImpresora.activateControl("CORTAPAPEL");
			} else
				tpImpresora.activateControl("SALTOHOJA");
		}
		return intStatus;
	}
*/
	public int imprimeTip(
		Vector vDatos,
		String strDatosSuc,
		String strDate,
		String strTime,
		int iDriver,
		String strIdentidad) {
		int intStatus = tpImpresora.verificaStatus();
		if ((intStatus == 95) || (intStatus == 88)) {
			String strOutLine = new String("");
			String strFiller =
				new String("                                                                                ");
			tpImpresora.activateControl("CERTIFICACION",iDriver);	
			String strServicio = (String) vDatos.elementAt(1);
			if (strServicio.equals(""))
				strServicio = " ";
			String strReferencia = (String) vDatos.elementAt(2);
			if (strReferencia.equals(""))
				strReferencia = "0.00";
			String strBeneficiario = (String) vDatos.elementAt(3);
			if (strBeneficiario.equals(""))
				strBeneficiario = "0.00";
			String strMonto = (String) vDatos.elementAt(4);
			if (strMonto.equals(""))
				strMonto = "0.00";
			String strCuenta = (String) vDatos.elementAt(5);
			String strMoneda = (String) vDatos.elementAt(6);
			String strSigno = "";
			if (strMoneda.equals("") || strMoneda.equals("1")) {
				strMoneda = "1";
				strSigno = "$";
			}
			Vector vDatosSuc = tpImpresora.Campos(strDatosSuc, "@");
			String strNomSuc = (String) vDatosSuc.elementAt(0);
			strNomSuc = strNomSuc.substring(7, strNomSuc.length());
			String strNumSuc = (String) vDatosSuc.elementAt(1);
			strNumSuc = strNumSuc.substring(7, strNumSuc.length());
			String strDomSuc = (String) vDatosSuc.elementAt(2);
			strDomSuc = strDomSuc.substring(7, strDomSuc.length());
			String strNomPza = (String) vDatosSuc.elementAt(4);
			strNomPza = strNomPza.substring(7, strNomPza.length());
			if (strCuenta.equals(""))
				tpImpresora.enviaCadena(
					"                        COMPROBANTE DE PAGO EN EFECTIVO");
			else
				tpImpresora.enviaCadena(
					"                     COMPROBANTE DE PAGO CON ABONO A CUENTA");
			tpImpresora.enviaCadena(
				"                     TRANSFERENCIA INMEDIATA DE PAGOS "+strIdentidad+"\n");
			//IMAGEN
			strOutLine = "SUCURSAL : " + strNumSuc + "  " + strNomPza;
			tpImpresora.enviaCadena(strOutLine);
			strOutLine =
				strFiller.substring(0, 55)
					+ "FECHA : "
					+ strDate.substring(7, 9)
					+ "/"
					+ strDate.substring(5, 7)
					+ "/"
					+ strDate.substring(1, 5);
			tpImpresora.enviaCadena(strOutLine);
			strOutLine =
				strFiller.substring(0, 55)
					+ "HORA  : "
					+ strTime.substring(0, 2)
					+ ":"
					+ strTime.substring(2, 4)
					+ ":"
					+ strTime.substring(4, 6);
			tpImpresora.enviaCadena(strOutLine);
			strOutLine = "SERVICIO     : " + strServicio;
			tpImpresora.enviaCadena(strOutLine);
			strOutLine = "REFERENCIA   : " + strReferencia;
			tpImpresora.enviaCadena(strOutLine);
			strOutLine = "BENEFICIARIO : " + strBeneficiario;
			tpImpresora.enviaCadena(strOutLine);
			if (!strMoneda.equals("1"))
				strSigno = "$";
			strOutLine = "MONTO        : " + strSigno + " " + strMonto;
			tpImpresora.enviaCadena(strOutLine);
			if (strMoneda.equals("1"))
				strOutLine = "               " + Currency.convert(strMonto, 1);
			else
				strOutLine = "               " + Currency.convert(strMonto, 2);
			tpImpresora.enviaCadena(strOutLine);
			if (!strCuenta.equals("")) {
				strOutLine = "CUENTA DE ABONO : " + strCuenta;
				tpImpresora.enviaCadena(strOutLine);
			}
			tpImpresora.enviaCadena(" ");
			strOutLine =
				strFiller.substring(0, 23) + "FIRMA : ________________________";
			tpImpresora.enviaCadena(strOutLine);
			tpImpresora.enviaCadena(
				"ESTA FICHA CONSTITUYE UN FORMATO DE LIQUIDACION, LA FIRMA DEL BENEFICIARIO");
			tpImpresora.enviaCadena(
				"IMPLICA LA ACEPTACION DE QUE LA CANTIDAD ENTREGADA ES CORRECTA. "+strIdentidad+"  NO  ");
			tpImpresora.enviaCadena(
				"TENDRA NINGUNA RESPONSABILIDAD YA QUE OBEDECE A INSTRUCCIONES PRECISAS DE  ");
			tpImpresora.enviaCadena(
				"LA EMPRESA QUE REALIZA LA TRANSFERENCIA DE PAGO.");
			if (iDriver == 0) {
				tpImpresora.InserteDocumento();
				tpImpresora.activateControl("SELECCIONASLIP",iDriver);
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.imprimeCadena1();
				tpImpresora.activateControl("SALTOHOJA",iDriver);
				tpImpresora.activateControl("SELECCIONAJOURNAL",iDriver);
			} else
				tpImpresora.activateControl("SALTOHOJA",iDriver);
		}
		return intStatus;
	}

}