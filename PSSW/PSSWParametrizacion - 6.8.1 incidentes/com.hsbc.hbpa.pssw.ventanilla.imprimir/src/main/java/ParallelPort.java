import java.io.File;

public class ParallelPort {

	private int portBase = 0x3BC;

	public ParallelPort() {
	}

	public ParallelPort(int portBase) {
		this.portBase = portBase;
	}

	public int read() {
		return ParallelPort.readOneByte(this.portBase + 1);
	}

	public boolean init() {
		return ParallelPort.initPrinter();
	}

	public boolean print(String value, int timeoutSeconds) {
		return ParallelPort.printString(value, timeoutSeconds);
	}

	public void abort() {
		ParallelPort.abortPrinter();
	}

	public void open() {
		ParallelPort.openPort();
	}

	public void close() {
		ParallelPort.closePort();
	}

	public int getNameDriver() {
		int nDriver = 1; // 1 ML320 , 0 OKIPOS,  2 USBOKIPOS
		String sArchivo = "c:\\Windows\\parport.dll";
		File fArchivo = new File(sArchivo);
		if (fArchivo.exists())
		{
		   try {
			   nDriver = ParallelPort.gDriverName();
		   } catch (UnsatisfiedLinkError e) {
			   System.out.println("DLL ANTERIOR");		
			   nDriver = 1;
		   }
		}
		else
		{
			nDriver = 0;				
		}
		return nDriver;
	}

	public static native int gDriverName();
	public static native int readOneByte(int portStatus);
	public static native void openPort();
	public static native void closePort();
	public static native boolean initPrinter();
	public static native boolean printString(String value, int timeoutSeconds);
	public static native void abortPrinter();

	static {
		String sArchivo = "c:\\Windows\\parport.dll";
		File fArchivo = new File(sArchivo);
		if (fArchivo.exists())
		   System.loadLibrary("parport");
	}
}