//*************************************************************************************************
//Funcion: Clase que combierte la moneda de n�mero a letra
//Elemento: OrdenesPago.java
//Creado por: Juan Carlos Gaona Marcial
//Modificado por: Rodrigo Escand�n Soto
//*************************************************************************************************
//CCN - 4620027 - 24/10/2007 - Se actualiza "WWW..COM.MX" por "WWW..COM.PA"
//CCN - 4620027 - 25/10/2007 - Se quita la validaci�n de perfil de cajero 01
//CCN - 4620038 - 22/11/2007 - Se cambia el orden de las monedas en el arreglo currency
//*************************************************************************************************


public class Currency {
	public static String[][] currency = { {"DOLAR","DOLARES"}, {
		"PESO", "PESOS"}, {
		"UDI", "UDIS"}, {
		" "," "}
	};
	public static String removeBlanks(String str) {
		if (str == null)
			return null;

		int length = str.length();
		StringBuffer buffer = new StringBuffer(length);

		int index = 0;
		for (; index < length; index++)
			if (str.charAt(index) != ' ')
				break;

		boolean space = false;
		for (; index < length; index++) {
			char ch = str.charAt(index);

			if (ch == ' ') {
				if (space == false)
					buffer.append(ch);
				space = true;
			} else
				space = false;

			if (space)
				continue;

			buffer.append(ch);
		}

		return new String(buffer);
	}

	public static int[] toArray(long value, int reps) {
		int index = 0;
		long mask = 1;
		int[] vector = new int[10];

		for (int i = 0; i < reps; i++)
			mask *= 10;

		do {
			long milles = value % mask;
			vector[index++] = (int) milles;
			value = value / mask;
		} while (value > 0);

		if (index % 2 == 1)
			++index;

		int[] tmp = new int[index];
		System.arraycopy(vector, 0, tmp, 0, index);
		return tmp;
	}

	public static String process(long valor, int moneda) {
		String label1[] = { "", " MIL ", "" };
		String label21[] = { "", " MILLON ", " BILLON ", "", "", "" };
		String label22[] = { "", " MILLONES ", " BILLONES ", "", "", "" };

		int[] first = toArray(valor, 6);
		String cadena = "";
		for (int i = 0; i < first.length; i++) {
			String tmp = "";
			if (first[i] == 0) {
				if (i == 0)
					cadena = "DE ";
				continue;
			}

			int second[] = toArray(first[i], 3);
			for (int j = second.length - 1; j >= 0; j--) {
				if (second[j] == 0)
					continue;

				tmp = tmp + currencyToString(second[j]) + label1[j];
			}

			if (first[i] == 1)
				cadena = tmp + label21[i] + cadena;
			else
				cadena = tmp + label22[i] + cadena;
		}

		if (valor == 1)
			cadena += (" " + currency[moneda - 1][0]);
		else
			cadena += (" " + currency[moneda - 1][1]);
		return cadena;
	}

	public static String currencyToString(int valor) {
		String[] centenas =
			{
				"",
				"CIENTO",
				"DOSCIENTOS",
				"TRESCIENTOS",
				"CUATROCIENTOS",
				"QUINIENTOS",
				"SEISCIENTOS",
				"SETECIENTOS",
				"OCHOCIENTOS",
				"NOVECIENTOS" };

		String[] decenas =
			{
				"",
				"DIEZ",
				"VEINTI",
				"TREINTA",
				"CUARENTA",
				"CINCUENTA",
				"SESENTA",
				"SETENTA",
				"OCHENTA",
				"NOVENTA" };

		String[] unidades =
			{
				"",
				"UN",
				"DOS",
				"TRES",
				"CUATRO",
				"CINCO",
				"SEIS",
				"SIETE",
				"OCHO",
				"NUEVE",
				"DIEZ",
				"ONCE",
				"DOCE",
				"TRECE",
				"CATORCE",
				"QUINCE" };

		if (valor > 999)
			return "ERROR";

		int dos = valor % 100;

		int centena = valor / 100;
		int decena = (valor / 10) % 10;
		int unidad = valor % 10;

		StringBuffer temp = new StringBuffer(100);
		if (dos < unidades.length)
			temp.append(unidades[dos]);
		else {
			if (decena == 2 && unidad == 0)
				temp.append("VEINTE");
			else
				temp.append(decenas[decena]);

			if (decena != 2 && decena != 0 && unidad != 0)
				temp.append(" Y ");
			temp.append(unidades[unidad]);
		}

		String result = "";
		if (centena == 1 && temp.length() == 0)
			result = "CIEN";
		else {
			result = centenas[centena];
			if (result.length() > 0 && temp.length() > 0)
				result = result + " ";
			result += temp.toString();
		}

		return result;
	}

	public static String convert(String str, int moneda) {
		if (str == null)
			return null;

		if (moneda < 1 || moneda > 4)
			return null;

		// Eliminar comas del formato
		StringBuffer buffer = new StringBuffer(str.length());
		for (int i = 0; i < str.length(); i++)
			if (str.charAt(i) == ',')
				continue;
			else
				buffer.append(str.charAt(i));
		str = buffer.toString();

		int position = str.indexOf('.');
		String integer = null;
		String decimal = null;

		if (position > 0) {
			integer = str.substring(0, position);
			decimal = str.substring(position + 1);
		} else {
			integer = str;
			decimal = "00";
		}

		//System.out.println(integer + " - " + decimal);
		long value = Long.parseLong(integer);

		return removeBlanks(process(value, moneda)) + " (" + decimal + "/100)";
	}

	/*public static void main(String[] args)
	{
	  System.out.println(Currency.convert("10000,000,000.95", 3));
	}*/
}