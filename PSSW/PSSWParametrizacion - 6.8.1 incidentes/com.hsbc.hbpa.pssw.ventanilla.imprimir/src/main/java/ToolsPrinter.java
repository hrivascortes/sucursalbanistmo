import java.util.*;
import java.text.*;
import java.awt.*;
import java.io.*;

import javax.print.Doc;
import javax.print.DocFlavor;
import javax.print.DocPrintJob;
import javax.print.PrintException;
import javax.print.PrintService;
import javax.print.PrintServiceLookup;
import javax.print.SimpleDoc;

import ventanilla.com.bital.util.NSTokenizer;

public class ToolsPrinter extends Frame {
	Formato formattmp = new Formato();
	int Windows98 = 1;
	String strBuffer = new String("");
	int Termino = 0;

	public Vector Campos(String cadena, String delimitador) {
		NSTokenizer parser = new NSTokenizer(cadena, delimitador);
		Vector campos = new Vector();
		while (parser.hasMoreTokens()) {
			String token = parser.nextToken();
			if (token == null)
				continue;
			campos.addElement(token);
		}
		return campos;
	}

	public String formatMonto(String s) {
		s = s.trim();
		if (s.equals(""))
			return s;
		int len = s.length();
		if (len == 1)
			s = "0.0" + s;
		else if (len == 2)
			s = "0." + s;
		else {
			int n = (len - 3) / 3;
			s = s.substring(0, len - 2) + "." + s.substring(len - 2, len);
			for (int i = 0; i < n; i++) {
				len = s.length();
				s =
					s.substring(0, (len - (i * 3 + 6 + i)))
						+ ","
						+ s.substring((len - (i * 3 + 6 + i)), len);
			}
		}
		return s;
	}

	public String unFormat(String strCantidad) {
		String strTmp = new String("");
		if (strCantidad != null) {
			int intCiclo = (strCantidad.length() / 4) + 1;
			int intSubindice1 = -2;
			for (int i = 0; i < intCiclo; i++) {
				intSubindice1 += 4;
				if ((i + 1) == intCiclo)
					intSubindice1 = strCantidad.length();
				int intSubindice2 = (3 * i) + i - 1;
				if (intSubindice2 < 0)
					intSubindice2 = 0;
				strTmp =
					strCantidad.substring(
						strCantidad.length() - intSubindice1,
						strCantidad.length() - intSubindice2)
						+ strTmp;
			}
		}
		return strTmp;
	}

	public String getDate(String parametro) {
		java.util.Date fecha = new java.util.Date();
		SimpleDateFormat formatof = new SimpleDateFormat(parametro);
		String fech = formatof.format(fecha);
		return fech;
	}

/*	public int verificaStatus() {
		int status = 0;
		ParallelPort lpt1 = new ParallelPort(0x3BC);
		lpt1.open();
		status = lpt1.read();
		status = status & 127;
		lpt1.close();
		return status;
	}
*/
	
	public int verificaStatus() {
		String sArchivo = "c:\\Windows\\parport.dll";
		File fArchivo = new File(sArchivo);
		int status = 0;		
		if (fArchivo.exists())
		{			
			ParallelPort lpt1 = new ParallelPort(0x3BC);
			lpt1.open();
			status = lpt1.read();
			status = status & 127;
			lpt1.close();
		}
			else
				status = 95;
		return status;
	}
	
	
	public void outControl() {
		String sArchivo = "c:\\Windows\\parport.dll";
		File fArchivo = new File(sArchivo);
		
		if (fArchivo.exists())
		{
		    ParallelPort lpt1 = new ParallelPort(0x3BC);
		    if (lpt1.init()) {
			   /*for(int i = 0; i < formattmp.out.length; i++)
				System.out.println("i = " + i + " " + (int)formattmp.out[i]);*/
			   String strC = new String(formattmp.out);
			   boolean bResp = lpt1.print(strC, 5);
			   /*if (Termino == 1)
				 //System.out.println("************************************************");				*/
			   if (!bResp)
				  lpt1.abort();
		     } else
			     System.out.println("1 Error en Inicializacion de Impresora");
		}
		else
		{
			PrintService printService = PrintServiceLookup.lookupDefaultPrintService();
			DocFlavor flavor = DocFlavor.BYTE_ARRAY.AUTOSENSE;
			DocPrintJob docPrintJob = printService.createPrintJob();
			String strC = new String(formattmp.out);			
			Doc doc=new SimpleDoc(strC.getBytes(),flavor,null); 
			try 
			{
				docPrintJob.print(doc, null);
			}
			catch (PrintException e) 
			{
				System.out.println("Error al imprimir: "+e.getMessage());
			} 
		}
	}

	/*public void outControl1() {
		try {
			ParallelPort lpt1 = new ParallelPort(0x3BC);   
			lpt1.init();
			FileOutputStream os = new FileOutputStream("LPT1:");
			PrintStream ps = new PrintStream(os);
			ps.print(formattmp.out);
			ps.close();
		} catch (Exception e) {
			System.out.println("Error: " + e.getMessage());
		}
	}*/
	public String limpiaEspacio(String linea){
		linea = linea.trim();
		return linea;
	}

	public void enviaCadena(String linea) {
		linea = linea + "\r\n";
		char caracterN = (char) 165;
		char caractern = (char) 164;
		char caractero = (char) 162;
		char caracteru = (char) 163;
		char caractere = (char) 130;
		char caractera = (char) 160;
		char caracteri = (char) 161;
		linea = linea.replace('�', caracterN);
		linea = linea.replace('�', caractern);
		linea = linea.replace('�', caractero);
		linea = linea.replace('�', caracteri);
		linea = linea.replace('�', caracteru);
		linea = linea.replace('�', caractere);
		linea = linea.replace('�', caractera);
		strBuffer += linea;
	}

	public void imprimeCodigo(String strCadenaTmp) {
		try {
			char[] confcode3 = new char[12];
			char[] confcode1 = new char[4];
			String strTmp = new String("");
			confcode3[0] = (char) 27;
			confcode3[1] = (char) 16;
			confcode3[2] = (char) 65;
			confcode3[3] = (char) 8;
			confcode3[4] = (char) 3;
			confcode3[5] = (char) 0;
			confcode3[6] = (char) 0;
			confcode3[7] = (char) 3;
			confcode3[8] = (char) 1;
			confcode3[9] = (char) 1;
			confcode3[10] = (char) 1;
			confcode3[11] = (char) 0;
			confcode1[0] = (char) 27;
			confcode1[1] = (char) 16;
			confcode1[2] = (char) 66;
			confcode1[3] = (char) 20;
			PrintService printService = PrintServiceLookup.lookupDefaultPrintService();
			DocFlavor flavor = DocFlavor.BYTE_ARRAY.AUTOSENSE;
			DocPrintJob docPrintJob = printService.createPrintJob();
			strTmp = new String(confcode3);
			Doc doc=new SimpleDoc(strTmp.getBytes(),flavor,null);
			docPrintJob.print(doc, null);			
			strTmp = new String(confcode1);
			doc=new SimpleDoc(strTmp.getBytes(),flavor,null);
			docPrintJob.print(doc, null);
			doc=new SimpleDoc(strCadenaTmp.getBytes(),flavor,null);			
			docPrintJob.print(doc, null);
			/*FileOutputStream os = new FileOutputStream("LPT1:");
			PrintStream ps = new PrintStream(os);
			ps.print(confcode3);
			ps.print(confcode1);
			ps.println(strCadenaTmp);
			ps.close();*/
		} catch (Exception e) {
			System.out.println("Error: " + e.getMessage());
		}
	}

/*	public void imprimeCadena1() {
		try {
			ParallelPort lpt1 = new ParallelPort(0x3BC);			
			lpt1.init();			
			char[] confcode3 = new char[2];
			char[] confcode1 = new char[2];
			confcode3[0] = (char) 27;
			confcode3[1] = (char) 15;
			confcode1[0] = (char) 27;
			confcode1[1] = (char) 118;
			FileOutputStream os = new FileOutputStream("LPT1:");
			PrintStream ps = new PrintStream(os);	
			ps.print(confcode3);
			for(int i = 0; i < confcode3.length; i++)
				System.out.println("i = " + i + " " + (int)confcode3[i]);			
			ps.println(strBuffer);			
			System.out.println(strBuffer);					
			ps.print(confcode1);
			for(int i = 0; i < confcode1.length; i++)
				System.out.println("i = " + i + " " + (int)confcode1[i]);							
			ps.close();
			strBuffer = new String("");
		} catch (Exception e) {
			System.out.println("Error: " + e.getMessage());
		}
	}
*/
	public void imprimeCadena() {
		String sArchivo = "c:\\Windows\\parport.dll";
		File fArchivo = new File(sArchivo);
		if (fArchivo.exists())
		{		
		   ParallelPort lpt1 = new ParallelPort(0x3BC);
		   if (lpt1.init()) {
			   boolean bResp = lpt1.print(strBuffer, 15);
			   //System.out.println(strBuffer);
			   if (!bResp)
				   lpt1.abort();
			   strBuffer = new String("");
		   } else
			   System.out.println("2 Error en Inicializacion de Impresora");
		}
		else
		{
			PrintService printService = PrintServiceLookup.lookupDefaultPrintService();
			DocFlavor flavor = DocFlavor.BYTE_ARRAY.AUTOSENSE;
			DocPrintJob docPrintJob = printService.createPrintJob();
			Doc doc=new SimpleDoc(strBuffer.getBytes(),flavor,null); 
			try 
			{
				docPrintJob.print(doc, null);
				strBuffer = new String("");				
			}
			catch (PrintException e) 
			{
				System.out.println("Error al imprimir: "+e.getMessage());
			} 
		}
	}

	public void imprimeCadena1() {
		String sArchivo = "c:\\Windows\\parport.dll";
		File fArchivo = new File(sArchivo);
		if (fArchivo.exists())
		{	
		   ParallelPort lpt1 = new ParallelPort(0x3BC);
		   if (lpt1.init()) {
			   boolean bResp = lpt1.print(strBuffer, 15);
			   //System.out.println(strBuffer);
			   if (!bResp)
				   lpt1.abort();
			   strBuffer = new String("");
		   } else
			   System.out.println("2 Error en Inicializacion de Impresora");
		}
		else
		{
			PrintService printService = PrintServiceLookup.lookupDefaultPrintService();
			DocFlavor flavor = DocFlavor.BYTE_ARRAY.AUTOSENSE;
			DocPrintJob docPrintJob = printService.createPrintJob();
			Doc doc=new SimpleDoc(strBuffer.getBytes(),flavor,null); 
			try 
			{
				docPrintJob.print(doc, null);
				strBuffer = new String("");				
			}
			catch (PrintException e) 
			{
				System.out.println("Error al imprimir: "+e.getMessage());
			} 
		}
	}

	public void muestraVentana() {
		SimpleDialog sDialog;
		int continua = 103;
		while ((continua != 95) && (continua != 88)) {
			if ((continua == 103) || (continua == 96))
				sDialog = new SimpleDialog(this, "     Falta  Papel     ");
			else
				sDialog = new SimpleDialog(this, "Impresora No Preparada");
			sDialog.setSize(200, 180);
			sDialog.setLocation(310, 220);
			sDialog.setVisible(true);
			//sDialog.show();
			continua = 0;
			continua = verificaStatus();
		}
	}

	public void muestraRechazo() {
		SimpleDialog sDialog;
		sDialog = new SimpleDialog(this, "Transaccion  Rechazada");
		sDialog.setSize(200, 180);
		sDialog.setLocation(310, 220);
		sDialog.setVisible(true);
		//sDialog.show();
	}

	public void InserteDocumento() {
		SimpleDialog sDialog;
		sDialog = new SimpleDialog(this, "Inserte Documento");
		sDialog.setSize(200, 180);
		sDialog.setLocation(310, 220);
		sDialog.setVisible(true);
		//sDialog.show();
	}

	public void activateControl(String Nombre, int intDriver) {
		Termino =0;
		if (Nombre.equals("SALTOHOJA") == true) {
			if (intDriver == 0)
				imprimeCadena1();
			else
				imprimeCadena();
			formattmp.NoLineas = 80;
			formattmp.noOut = 1;
			formattmp.out = new char[formattmp.noOut];
			formattmp.out[0] = (char) 12;
			Termino = 1;
		}else if (Nombre.equals("CERTIFICACION") == true) {
			formattmp.NoLineas = 20;
			formattmp.noOut = 4;
			formattmp.out = new char[formattmp.noOut];
			formattmp.out[0] = (char) 15;
			formattmp.out[1] = (char) 27;
			formattmp.out[2] = (char) 87;
			formattmp.out[3] = (char) 1;
			String strTmp = new String(formattmp.out);
		}else if (Nombre.equals("CERTIFICACION2") == true) {
				formattmp.NoLineas = 66;
				formattmp.noOut = 2;
				formattmp.out = new char[formattmp.noOut];
				formattmp.out[0] = (char) 18;
				formattmp.out[1] = (char) 15;
				String strTmp = new String(formattmp.out);
		}else if (Nombre.equals("OPECONANC") == true) {						
					formattmp.NoLineas = 20;
					formattmp.noOut = 2;
					formattmp.out = new char[formattmp.noOut];
					formattmp.out[0] = (char) 27;
					formattmp.out[1] = (char) 64;
					//formattmp.out[2] = (char) 1;
		} else if (Nombre.equals("CLOCONANC") == true) {
					formattmp.NoLineas = 20;
					formattmp.noOut = 3;
					formattmp.out = new char[formattmp.noOut];
					formattmp.out[0] = (char) 27;
					formattmp.out[1] = (char) 87;
					formattmp.out[2] = (char) 0;
				

		} else if (Nombre.equals("SELECCIONASLIP") == true) {
			formattmp.NoLineas = 4;
			formattmp.noOut = 4;
			formattmp.out = new char[formattmp.noOut];
			formattmp.out[0] = (char) 27;
			formattmp.out[1] = (char) 99;
			formattmp.out[2] = (char) 48;
			formattmp.out[3] = (char) 4;
		} else if (Nombre.equals("SELECCIONAJOURNAL") == true) {
			formattmp.NoLineas = 4;
			formattmp.noOut = 4;
			formattmp.out = new char[formattmp.noOut];
			formattmp.out[0] = (char) 27;
			formattmp.out[1] = (char) 99;
			formattmp.out[2] = (char) 48;
			formattmp.out[3] = (char) 3;
			String strTmp = new String(formattmp.out);
		} else if (Nombre.equals("CORTAPAPEL") == true) {
			formattmp.NoLineas = 66;
			formattmp.noOut = 2;
			formattmp.out = new char[formattmp.noOut];
			formattmp.out[0] = (char) 27;
			formattmp.out[1] = (char) 105;
			Termino = 1;
			String strTmp = new String(formattmp.out);		
		} else if (
			(Nombre.equals("NOMINA") == true)
				|| (Nombre.equals("CFISCAL") == true)) {
			formattmp.NoLineas = 20;
			formattmp.noOut = 3;
			formattmp.out = new char[formattmp.noOut];
			formattmp.out[0] = (char) 27;
			formattmp.out[1] = (char) 80;
			formattmp.out[2] = (char) 14;
		} else if (
			(Nombre.equals("TOTALES") == true)
				|| (Nombre.equals("TRANSFERENCIA") == true)
				|| (Nombre.equals("DIARIO") == true)
				|| (Nombre.equals("FICDEPCOB") == true)
				|| (Nombre.equals("CHQCERTIF") == true)) {
			formattmp.NoLineas = 66;
			formattmp.noOut = 7;
			formattmp.out = new char[formattmp.noOut];
			formattmp.out[0] = (char) 27;
			formattmp.out[1] = (char) 91;
			formattmp.out[2] = (char) 73;
			formattmp.out[3] = (char) 2;
			formattmp.out[4] = (char) 0;
			formattmp.out[5] = (char) 0;
			formattmp.out[6] = (char) 11;
		}
		/*if (intDriver == 0)
			outControl1();
		else*/
			outControl();		
	}
}