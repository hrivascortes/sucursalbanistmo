//*************************************************************************************************
//		 Funcion: Clase que certifica Ordenes de Pago
//		Elemento: OrdenesPago.java
//	  Creado por: Juan Carlos Gaona Marcial
//Modificado por: Rodrigo Escand�n Soto
//*************************************************************************************************
//CCN - 4620027	- 24/10/2007 - Se actualiza "WWW..COM.MX" por "WWW..COM.PA"
//CCN - 4620032 - 08/11/2007 - Se actualiza impresion de tickets con formato para Panam�
//*************************************************************************************************

import java.util.*;

public class OrdenesPago {
	ToolsPrinter tpImpresora = new ToolsPrinter();
	int intStatus = 0;
	String strFiller =
		"                                                                                ";
	String strOutLine = "";
	Vector vDatosSuc = new Vector();
	int intMoneda = 0;

	public int imprimeOrdenNueva(
		Vector vDatos,
		String strRespuesta,
		String strDatosSuc,
		String strCajero,
		String strDate,
		String strTime,
		int iDriver,
		String strIdentidad,
		String strIdentidad2){
		intStatus = tpImpresora.verificaStatus();
		String strTipoCambio = new String("");
		String strMonedaUSD = new String("");
		int intExiste = 0;
		Vector vDatosTxn = new Vector();
		if ((intStatus == 95) || (intStatus == 88)) {
			intStatus = 0;
			String strFormato = (String) vDatos.elementAt(0);
			if (strFormato.equals(""))
				strFormato = " ";
			String strMoneda = (String) vDatos.elementAt(1);
			if (strMoneda.equals("") || strMoneda.equals("N$")) {
				strMoneda = "$";
				intMoneda = 1;
			} else {
				strMoneda = "EUR";
				intMoneda = 2;				
			}
			/*if ((vDatos.size() > 12) && (vDatos.size() <15))
			{
				intExiste =2;
				strTipoCambio = (String) vDatos.elementAt(12);
				if (strTipoCambio.equals(""))
					strTipoCambio = "0.00";
				strMonedaUSD = (String) vDatos.elementAt(13);
				if (strMonedaUSD.equals(""))
					strMonedaUSD = "000";
				strMonedaUSD = tpImpresora.formatMonto(strMonedaUSD);
			}*/							
			String strOrden = (String) vDatos.elementAt(2);
			if (strOrden.equals(""))
				strOrden = " ";
			String strImporte = (String) vDatos.elementAt(3);
			if (strImporte.equals(""))
				strImporte = "000";
			String strComision = (String) vDatos.elementAt(4);
			if (strComision.equals(""))
				strComision = "000";
			String strIva = (String) vDatos.elementAt(5);
			if (strIva.equals(""))
				strIva = "000";
			String strTotal = (String) vDatos.elementAt(6);
			if (strTotal.equals(""))
				strTotal = "000";
			String strBeneficiario = (String) vDatos.elementAt(7);
			if (strBeneficiario.equals(""))
				strBeneficiario = " ";
			String strOrdenante = (String) vDatos.elementAt(8);
			if (strOrdenante.equals(""))
				strOrdenante = " ";
			String strCuenta = (String) vDatos.elementAt(9);
			if (strCuenta.equals(""))
				strCuenta = " ";
			String strNombreCorto = (String) vDatos.elementAt(10);
			if (strNombreCorto.equals(""))
				strNombreCorto = " ";
			String strBanco = (String) vDatos.elementAt(11);
			if (strBanco.equals(""))
				strBanco = strIdentidad;
			String strReferencia = (String) vDatos.elementAt(12);
			if (strReferencia.equals(""))
				strReferencia = " ";
			vDatosTxn = tpImpresora.Campos(strRespuesta, "~");
			vDatosSuc = tpImpresora.Campos(strDatosSuc, "@");
			String strNomSuc = (String) vDatosSuc.elementAt(0);
			strNomSuc = strNomSuc.substring(7, strNomSuc.length());
			String strNumSuc = (String) vDatosSuc.elementAt(1);
			strNumSuc = strNumSuc.substring(7, strNumSuc.length());
			tpImpresora.enviaCadena("");
			
			//Imprime Sucursal
			strOutLine = "SUCURSAL : " + strNumSuc + "-" + strNomSuc;
			tpImpresora.enviaCadena(strOutLine);
			tpImpresora.enviaCadena(" ");
			
			//Imprime fecha y cajero
			strOutLine =
						strDate.substring(5, 7)
						+ "-"
						+ strDate.substring(7, 9)
						+ "-"
						+ strDate.substring(1, 5)
						+ "    "
						+ strNumSuc
						+ strCajero
						+ "\n";
			tpImpresora.enviaCadena(strOutLine);
			
			tpImpresora.activateControl("CERTIFICACION",iDriver);
			if (strFormato.equals("REVERSOORDENPAGO"))
				strOutLine =
					"0"
						+ strTime.substring(0, 2)
						+ strTime.substring(2, 4)
						+ strTime.substring(4, 6)
						+ " "
						+ strDate.substring(7, 9)
						+ strDate.substring(5, 7)
						+ strDate.substring(1, 5)
						+ " REV "
						+ strCajero
						+ " "
						+ strCajero.substring(0, 4)
						+ "  $  "
						+ strMoneda
						+ tpImpresora.formatMonto(strImporte);
			
			if ((!strFormato.equals("SUSPENSION"))
				&& (!strFormato.equals("REVERSOORDENPAGO"))
				&& (!strFormato.equals("RETRANSMISION"))) {
				
				/*//Imprime flujo de transacciones
				strOutLine = "   TXN    CONSEC";
				tpImpresora.enviaCadena(strOutLine);
				for (int veces = 0; veces < (vDatosTxn.size() / 2); veces++) {
					strOutLine =
						"   "
							+ (String) vDatosTxn.elementAt(veces * 2)
							+ "   "
							+ (String) vDatosTxn.elementAt(veces * 2 + 1);
					tpImpresora.enviaCadena(strOutLine);
				}
				tpImpresora.enviaCadena("");*/
				
				//Imprime T�tulo
				if (strFormato.equals("EFECTIVO")) {
					strOutLine =
							//"EMISION DE TRANSFERENCIA NACIONAL EN EFECTIVO S/" + strBanco;
							"  EMISION DE TRANSFERENCIA NACIONAL\n           EN EFECTIVO\n";
					//strOutLine =
						//strFiller.substring(0, (68 - strOutLine.length()) / 2)
							//+ strOutLine
						//	+ "\n";
					//strOutLine = tpImpresora.limpiaEspacio(strOutLine);
					tpImpresora.enviaCadena(strOutLine);
				} else if (strFormato.equals("CANCELACION")) {
					if (strCuenta.equals(" "))
						tpImpresora.enviaCadena(
							"   CANCELACION TRANSFERENCIA NACIONAL\n             EN EFECTIVO\n");
					else
						tpImpresora.enviaCadena(
							"   CANCELACION TRANSFERENCIA NACIONAL\n         CON CREDITO A CUENTA\n");
				} else if (strFormato.equals("LIQUIDACION")) {					
					if (strCuenta.equals("") || strCuenta.equals(" "))
						tpImpresora.enviaCadena(
							"    PAGO DE TRANSFERENCIA NACIONAL\n");					
						/*tpImpresora.enviaCadena(
							"          LIQUIDACION EN EFECTIVO ORDEN DE PAGO SUSPENDIDA\n");*/
					else
						/*tpImpresora.enviaCadena(
							"        LIQUIDACION ABONO A CUENTA ORDEN DE PAGO SUSPENDIDA\n");*/
						tpImpresora.enviaCadena(
							"      LIQUIDACION ABONO A CUENTA\n            ORDEN DE PAGO\n");
				} else if (strFormato.equals("CARGO")) {
					strOutLine = "  DEBITO POR EMISION DE TRANSFERENCIA\n            NACIONAL " + strBanco+"\n";
					//strOutLine =
						//strFiller.substring(0, (68 - strOutLine.length()) / 2)
							//+ strOutLine
							//+ "\n";
					//strOutLine = tpImpresora.limpiaEspacio(strOutLine);
					tpImpresora.enviaCadena(strOutLine);
				} else if (strFormato.equals("ABONO")) {
					if (strCuenta.equals("") || strCuenta.equals(" "))
						tpImpresora.enviaCadena(
							"               LIQUIDACION EN EFECTIVO ORDEN DE PAGO\n");
					else
						tpImpresora.enviaCadena(
							"                      ABONO POR ORDEN DE PAGO\n");
				}
				
				//Imprime flujo de transacciones
				  strOutLine = "   TXN    CONSEC";
				  tpImpresora.enviaCadena(strOutLine);
				  for (int veces = 0; veces < (vDatosTxn.size() / 2); veces++) {
					  strOutLine =
								  "   "
								  + (String) vDatosTxn.elementAt(veces * 2)
								  + "   "
								  + (String) vDatosTxn.elementAt(veces * 2 + 1);
				 tpImpresora.enviaCadena(strOutLine);
				 }
				 tpImpresora.enviaCadena("");
				
				/*//Imprime cajero, fecha y hora
				strOutLine =
					strNumSuc
						+ "    "
						+ strCajero
						+ "    "
						+ strDate.substring(7, 9)
						+ "/"
						+ strDate.substring(5, 7)
						+ "/"
						+ strDate.substring(1, 5)
						+ "     "
						+ strTime.substring(0, 2)
						+ ":"
						+ strTime.substring(2, 4)
						+ "\n";
				tpImpresora.enviaCadena(strOutLine);*/
				
				//Imprime Orden, Ordenante y Beneficiario
				/*if (strFormato.equals("CARGO"))
					strOutLine =
						"Orden    :  "
							+ strOrden
							+ strFiller.substring(0, 18 - strOrden.length())
							+ "Ordenante    : "
							+ strOrdenante;
				else
					strOutLine = "Orden    :  " + strOrden;*/
				strOutLine =
							"Orden       : "
							+ strOrden
							+ strFiller.substring(0, 18 - strOrden.length());
				tpImpresora.enviaCadena(strOutLine);
				strOutLine = "Ordenante   : " + strOrdenante;
				tpImpresora.enviaCadena(strOutLine);
				strOutLine = "Beneficiario: " + strBeneficiario;
				tpImpresora.enviaCadena(strOutLine);
				String strTmp2;
				
				//Imprime datos de la Transferencia
				if (strFormato.equals("CARGO"))
				{
					strOutLine = "Cuenta      : " + strCuenta;
					tpImpresora.enviaCadena(strOutLine);
					strTmp2 = tpImpresora.formatMonto(strImporte);
					strOutLine =
								"Importe  : "
								+ strMoneda
								+ strFiller.substring(0, 15 - strTmp2.length())
								+ strTmp2;
					tpImpresora.enviaCadena(strOutLine);
					strTmp2 = tpImpresora.formatMonto(strComision);
					strOutLine =
								"Comisi�n : "
								+ strMoneda
								+ strFiller.substring(0, 15 - strTmp2.length())
								+ strTmp2;
					tpImpresora.enviaCadena(strOutLine);
					strTmp2 = tpImpresora.formatMonto(strIva);
					strOutLine =
								"ITBMS :    "
								+ strMoneda
								+ strFiller.substring(0, 15 - strTmp2.length())
								+ strTmp2;
					tpImpresora.enviaCadena(strOutLine);
					strTmp2 = tpImpresora.formatMonto(strTotal);
					strOutLine =
								"TOTAL    : "
								+ strMoneda
								+ strFiller.substring(0, 15 - strTmp2.length())
								+ strTmp2
								+ "\n";
					tpImpresora.enviaCadena(strOutLine);
								
					/*if ((vDatos.size() > 12) && (vDatos.size() <15))
					{
						strOutLine =
							"Importe  : "
							+ "USD"
							+ strFiller.substring(0, 14 - strMonedaUSD.length())
							+ strMonedaUSD
							+ " Tipo de Cambio: "
							+ strTipoCambio;
						tpImpresora.enviaCadena(strOutLine);
					}
					strOutLine =
						"Importe  : "
							+ strMoneda
							+ strFiller.substring(0, 15 - strTmp2.length())
							+ strTmp2
							+ "  Cuenta       : "
							+ strCuenta;*/
				}
				
				else if(strFormato.equals("EFECTIVO")){
					strOutLine ="Referencia  : " + strReferencia;
					tpImpresora.enviaCadena(strOutLine);
					strOutLine =
								strTmp2 = tpImpresora.formatMonto(strImporte);
								strOutLine =
								"Importe  : "
								+ strMoneda
								+ strFiller.substring(0, 15 - strTmp2.length())
								+ strTmp2;
					tpImpresora.enviaCadena(strOutLine);
					strTmp2 = tpImpresora.formatMonto(strComision);
					strOutLine =
								"Comisi�n : "
								+ strMoneda
								+ strFiller.substring(0, 15 - strTmp2.length())
								+ strTmp2;
					tpImpresora.enviaCadena(strOutLine);
					strTmp2 = tpImpresora.formatMonto(strIva);
					strOutLine =
								"ITBMS :    "
								+ strMoneda
								+ strFiller.substring(0, 15 - strTmp2.length())
								+ strTmp2;
					tpImpresora.enviaCadena(strOutLine);
					strTmp2 = tpImpresora.formatMonto(strTotal);
					strOutLine =
								"TOTAL    : "
								+ strMoneda
								+ strFiller.substring(0, 15 - strTmp2.length())
								+ strTmp2
								+ "\n";
								tpImpresora.enviaCadena(strOutLine);
										
					/*strOutLine =
						"Importe  : "
							+ strMoneda
							+ strFiller.substring(0, 15 - strTmp2.length())
							+ strTmp2;
				tpImpresora.enviaCadena(strOutLine);*/
				}
				
				else if (strFormato.equals("CANCELACION")){
					if (strCuenta.equals(" ")){
						strTmp2 = tpImpresora.formatMonto(strImporte);
						strOutLine =
									"Importe  : "
									+ strMoneda
									+ strFiller.substring(0, 15 - strTmp2.length())
									+ strTmp2;
						tpImpresora.enviaCadena(strOutLine);
					}else{
						strOutLine = "Cuenta      : " + strCuenta;
						tpImpresora.enviaCadena(strOutLine);
						strTmp2 = tpImpresora.formatMonto(strImporte);
						strOutLine =
									"Importe  : "
									+ strMoneda
									+ strFiller.substring(0, 15 - strTmp2.length())
									+ strTmp2;
						tpImpresora.enviaCadena(strOutLine);
					}
				
				/*	if (strFormato.equals("ABONO")
					|| strFormato.equals("CANCELACION")
					|| strFormato.equals("LIQUIDACION")) 
					strOutLine = "Ordenante: " + strOrdenante;
					tpImpresora.enviaCadena(strOutLine);
					strOutLine = "Beneficiario: " + strBeneficiario;
					tpImpresora.enviaCadena(strOutLine);
					if (!strCuenta.equals(" ")) {
						strOutLine = "Cuenta   : " + strCuenta;
						tpImpresora.enviaCadena(strOutLine);
					}
					if (!strNombreCorto.equals(" ")) {
						strOutLine = "Nombre Corto: " + strNombreCorto + "\n";
						tpImpresora.enviaCadena(strOutLine);
					}*/
				}
				
				else {
						strTmp2 = tpImpresora.formatMonto(strImporte);
						strOutLine =
									"Importe  : "
									+ strMoneda
									+ strFiller.substring(0, 15 - strTmp2.length())
									+ strTmp2;
						tpImpresora.enviaCadena(strOutLine);
				}
				
				/*else {
					strTmp2 = tpImpresora.formatMonto(strComision);
					strOutLine =
						"Comisi�n : "
							+ strMoneda
							+ strFiller.substring(0, 15 - strTmp2.length())
							+ strTmp2
							+ "  Beneficiario : "
							+ strBeneficiario.trim();
					tpImpresora.enviaCadena(strOutLine);
					strTmp2 = tpImpresora.formatMonto(strIva);
					strOutLine =
						"I.V.A.   : "
							+ strMoneda
							+ strFiller.substring(0, 15 - strTmp2.length())
							+ strTmp2;
					tpImpresora.enviaCadena(strOutLine);
					strTmp2 = tpImpresora.formatMonto(strTotal);
					strOutLine =
						"TOTAL    : "
							+ strMoneda
							+ strFiller.substring(0, 15 - strTmp2.length())
							+ strTmp2
							+ "\n";
					tpImpresora.enviaCadena(strOutLine);
				}*/
				/*if (strFormato.equals("LIQUIDACION")
					|| strFormato.equals("CANCELACION")
					|| strFormato.equals("ABONO")) {
					strOutLine =
						"Paguese a la orden de : "
							+ strBeneficiario
							+ ", la cantidad de :";
					strTmp2 = tpImpresora.formatMonto(strImporte.trim());
					tpImpresora.enviaCadena(strOutLine);
					tpImpresora.enviaCadena(
						Currency.convert(strTmp2, intMoneda));
				}*/
				
				if ((strFormato.equals("EFECTIVO")
					|| strFormato.equals("CARGO")) && intMoneda != 2){
					tpImpresora.enviaCadena("   ESTA TRANSFERENCIA SERA PAGADA AL");
					tpImpresora.enviaCadena("BENEFICIARIO AL INFORMAR EL NUMERO DE");
					tpImpresora.enviaCadena("  ORDEN CON QUE FUE EMITIDA, ADEMAS ");
					tpImpresora.enviaCadena("   DE PRESENTAR CEDULA O PASAPORTE");
					tpImpresora.enviaCadena("\n          FIRMA DEL ORDENANTE\n\n\n");
					tpImpresora.enviaCadena("     ____________________________");
					tpImpresora.enviaCadena("               ACEPTO");
				}
				
				else if(strFormato.equals("LIQUIDACION")){
					tpImpresora.enviaCadena("\n              ACEPTO\n\n\n");
					tpImpresora.enviaCadena("     ____________________________");
					tpImpresora.enviaCadena("       FIRMA DEL BENEFICIARIO");
					
					/*if (intMoneda != 2) {
						tpImpresora.enviaCadena(
							"ESTA ORDEN SERA LIQUIDADA  AL BENEFICIARIO AL INFORMAR EL NUMERO DE");
						tpImpresora.enviaCadena(
							"ORDEN CON QUE FUE EXPEDIDA , ADEMAS DE PRESENTAR UNA IDENTIFICACION");
						tpImpresora.enviaCadena(
							"OFICIAL CON FOTOGRAFIA, PASAPORTE, CARTILLA O CREDENCIAL DE ELECTOR");
					}*/
				}
				
				else {
					tpImpresora.enviaCadena("\n          FIRMA DEL ORDENANTE\n\n\n");
					tpImpresora.enviaCadena("     _____________________________");
					
						strOutLine = strOrdenante;
						strOutLine =
									strFiller.substring(0, (38 - strOutLine.length()) / 2)
									+ strOutLine;
						//strOutLine = tpImpresora.limpiaEspacio(strOutLine);
						tpImpresora.enviaCadena(strOutLine);
					
									/*if (intMoneda != 2) {
										tpImpresora.enviaCadena(
											"ESTA ORDEN SERA LIQUIDADA  AL BENEFICIARIO AL INFORMAR EL NUMERO DE");
										tpImpresora.enviaCadena(
											"ORDEN CON QUE FUE EXPEDIDA , ADEMAS DE PRESENTAR UNA IDENTIFICACION");
										tpImpresora.enviaCadena(
											"OFICIAL CON FOTOGRAFIA, PASAPORTE, CARTILLA O CREDENCIAL DE ELECTOR");
									}*/
						}
				
				tpImpresora.enviaCadena("\n");
				/*if (strFormato.equals("ABONO") || strFormato.equals("LIQUIDACION"))
					tpImpresora.enviaCadena(
						"                            Beneficiario\n\n\n");
				else
					tpImpresora.enviaCadena(
						"                             Ordenante\n\n\n");
				tpImpresora.enviaCadena(
					"                    ____________________________");
				if (strFormato.equals("ABONO") || strFormato.equals("LIQUIDACION"))				
					strOutLine = strBeneficiario;
				else
					strOutLine = strOrdenante;
				strOutLine =
					strFiller.substring(0, (68 - strOutLine.length()) / 2)
						+ strOutLine;
				tpImpresora.enviaCadena(strOutLine);*/
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena(
					"                "+strIdentidad2+"    ");
				if (iDriver == 0) {
					//tpImpresora.enviaCadena("");
					tpImpresora.enviaCadena("");
					tpImpresora.enviaCadena("");
					tpImpresora.enviaCadena("");
					tpImpresora.enviaCadena("");
					tpImpresora.enviaCadena("");
					tpImpresora.enviaCadena("");
					tpImpresora.enviaCadena("");
					tpImpresora.enviaCadena("");
					tpImpresora.enviaCadena("");
					tpImpresora.enviaCadena("");
					tpImpresora.enviaCadena("");
					tpImpresora.enviaCadena("");
					tpImpresora.enviaCadena("");
					tpImpresora.enviaCadena("");
					tpImpresora.enviaCadena("");
					tpImpresora.enviaCadena("");					
					tpImpresora.imprimeCadena1();
					tpImpresora.activateControl("CORTAPAPEL",iDriver);
				} else
					tpImpresora.activateControl("SALTOHOJA",iDriver);
			}
			
			/*se agrego el comprobante fiscal */
			/*if (vDatos.size() > 15) {				
				for (int i = 0; i < (12 + intExiste); i++)
					vDatos.remove(0);
				//System.out.println(" vDatos " + vDatos);
				tpImpresora.muestraVentana();
				Comprobantes Comp = new Comprobantes();
				Comp.imprimeCFiscal(strDatosSuc, vDatos, strDate, iDriver);
			}*/
		}
		return intStatus;
	}

	public int imprimeOrdenMasiva(
		Vector vDatos,
		String strDatosSuc,
		String strCajero,
		String strDate,
		int iDriver,
		String strIdentidad) {
		intStatus = tpImpresora.verificaStatus();
		if ((intStatus == 95) || (intStatus == 88)) {
			intStatus = 0;
			Vector vConsecutivos = new Vector();
			Vector vNombres = new Vector();
			Vector vApellidos = new Vector();
			Vector vMontos = new Vector();
			Vector vStatus = new Vector();
			Vector vDatosSuc = new Vector();
			vDatosSuc = tpImpresora.Campos(strDatosSuc, "@");
			String strNumSuc = (String) vDatosSuc.elementAt(1);
			strNumSuc = strNumSuc.substring(7, strNumSuc.length());
			String strNomSuc = (String) vDatosSuc.elementAt(0);
			strNomSuc = strNomSuc.substring(7, strNomSuc.length());
			String strDomSuc = (String) vDatosSuc.elementAt(2);
			strDomSuc = strDomSuc.substring(7, strDomSuc.length());
			String strNomPza = (String) vDatosSuc.elementAt(4);
			strNomPza = strNomPza.substring(7, strNomPza.length());
			String strCuenta = (String) vDatos.elementAt(1);
			if (strCuenta.equals(""))
				strCuenta = " ";
			String strMonto = (String) vDatos.elementAt(2);
			if (strMonto.equals(""))
				strMonto = " ";
			String strNombre = (String) vDatos.elementAt(3);
			if (strNombre.equals(""))
				strNombre = " ";
			String strConsecutivos = (String) vDatos.elementAt(4);
			String strNombres = (String) vDatos.elementAt(5);
			String strApellidos = (String) vDatos.elementAt(6);
			String strMontos = (String) vDatos.elementAt(7);
			String strStatus = (String) vDatos.elementAt(8);
			vConsecutivos = tpImpresora.Campos(strConsecutivos, "*");
			vNombres = tpImpresora.Campos(strNombres, "*");
			vApellidos = tpImpresora.Campos(strApellidos, "*");
			vMontos = tpImpresora.Campos(strMontos, "*");
			vStatus = tpImpresora.Campos(strStatus, "*");
			tpImpresora.activateControl("CERTIFICACION",iDriver);
			int intControl = 0;
			int intPagina = 1;
			for (int i = 0; i < vConsecutivos.size(); i++) {
				strConsecutivos = (String) vConsecutivos.elementAt(i);
				strNombres = (String) vNombres.elementAt(i);
				strApellidos = (String) vApellidos.elementAt(i);
				strMontos = (String) vMontos.elementAt(i);
				strMontos = tpImpresora.formatMonto(strMontos);
				strStatus = (String) vStatus.elementAt(i);
				if (intControl == 0) {
					strOutLine =
						"   "+strIdentidad+" ( "+strIdentidad+" DE PANAMA )"
							+ strFiller.substring(0, 29)
							+ "PAGINA : "
							+ intPagina++;
					tpImpresora.enviaCadena(strOutLine);
					tpImpresora.enviaCadena("PLAZA " + strNomPza);
					strOutLine =
						"SUCURSAL : "
							+ strCajero.substring(0, 4)
							+ strFiller.substring(0, 44)
							+ " FECHA : "
							+ strDate.substring(7, 9)
							+ "/"
							+ strDate.substring(5, 7)
							+ "/"
							+ strDate.substring(1, 5);
					tpImpresora.enviaCadena(strOutLine);
					tpImpresora.enviaCadena(
						"RELACION DE ORDENES DE PAGO POR NOMINA");
					tpImpresora.enviaCadena(
						"------------------------------------------------------------------------------");
					tpImpresora.enviaCadena(
						"     NOMBRE DE LA EMPRESA                          NUMERO DE LA CUENTA ");
					strOutLine =
						"     "
							+ strNombre
							+ strFiller.substring(0, 51 - strNombre.length())
							+ strCuenta;
					tpImpresora.enviaCadena(strOutLine);
					tpImpresora.enviaCadena(
						"------------------------------------------------------------------------------");
					tpImpresora.enviaCadena(
						" No. ORDEN        NOMBRE DEL BENEFICIARIO                    MONTO      STATUS ");
				}
				strNombres = strNombres + " " + strApellidos;
				strOutLine =
					"  "
						+ strConsecutivos
						+ strFiller.substring(0, 13 - strConsecutivos.length())
						+ strNombres
						+ strFiller.substring(0, 39 - strNombres.length())
						+ " "
						+ strFiller.substring(0, 16 - strMontos.length())
						+ strMontos
						+ "    "
						+ strStatus;
				tpImpresora.enviaCadena(strOutLine);
				intControl++;
				if ((intControl == 45) && (i != (vConsecutivos.size() - 1))) {
					intControl = 0;
					if (iDriver == 0) {
						tpImpresora.InserteDocumento();
						tpImpresora.activateControl("SELECCIONASLIP",iDriver);
						tpImpresora.enviaCadena("");
						tpImpresora.enviaCadena("");
						tpImpresora.enviaCadena("");
						tpImpresora.enviaCadena("");
						tpImpresora.enviaCadena("");
						tpImpresora.imprimeCadena1();
						tpImpresora.activateControl("SALTOHOJA",iDriver);
						tpImpresora.activateControl("SELECCIONAJOURNAL",iDriver);
					} else {
						tpImpresora.activateControl("SALTOHOJA",iDriver);
						tpImpresora.muestraVentana();
					}
				}
			}
			strOutLine = strFiller.substring(0, 55) + "________________ ";
			tpImpresora.enviaCadena(strOutLine);
			String strTmp = tpImpresora.formatMonto(strMonto);
			strOutLine =
				strFiller.substring(0, 39)
					+ "IMPORTE TOTAL : "
					+ strFiller.substring(0, 16 - strTmp.length())
					+ strTmp;
			tpImpresora.enviaCadena(strOutLine);
			strOutLine =
				strFiller.substring(0, 33)
					+ "TOTAL DE DOCUMENTOS : "
					+ vConsecutivos.size();
			tpImpresora.enviaCadena(strOutLine);
			if (iDriver == 0) {
				tpImpresora.InserteDocumento();
				tpImpresora.activateControl("SELECCIONASLIP",iDriver);
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.imprimeCadena1();
				tpImpresora.activateControl("SALTOHOJA",iDriver);
				tpImpresora.activateControl("SELECCIONAJOURNAL",iDriver);
			} else {
				tpImpresora.activateControl("SALTOHOJA",iDriver);
			}
		}
		return intStatus;
	}
}