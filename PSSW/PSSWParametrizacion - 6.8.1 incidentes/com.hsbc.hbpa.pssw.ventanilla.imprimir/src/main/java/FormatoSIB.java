//*************************************************************************************************
//			 Funcion: Clase que certifica el formato SIB
//			Elemento: FormatoSIB.java
//		  Creado por: Juan Carlos Gaona Marcial
//	  Modificado por: Rodrigo Escand�n Soto
//*************************************************************************************************
//CCN - 4620035 - 13/11/2007 - Se modifica formato de impresi�n para formato SIB de Panam�
//*************************************************************************************************

import java.util.*;

public class FormatoSIB {
	ToolsPrinter tpImpresora = new ToolsPrinter();
	int intStatus = 0;
	String strFiller =
		"                                                                                                        ";
	String strOutLine = "";
	Vector vDatosSuc = new Vector();
	int intMoneda = 0;
	
	public int imprimeFormatoSIB(
	Vector vDatos,
	int iDriver){
		intStatus = tpImpresora.verificaStatus();
		int intExiste = 0;
		Vector vDatosTxn = new Vector();
		intStatus = 0;
		
		String strSuc = vDatos.get(13).toString().trim();
		String strFecha = vDatos.get(14).toString().trim();
		String strOperacion = (String)vDatos.get(1);
		String strRubro = (String)vDatos.get(2);
		String strUbicacion = (String)vDatos.get(3);
		String strMonto = (String)vDatos.get(4);
		String strNomDep = (String)vDatos.get(5);
		String strNomFun = (String)vDatos.get(11);
		String strCedDep = (String)vDatos.get(7);
		String strCargo = (String)vDatos.get(12);
		String strNacDep = (String)vDatos.get(6);
		String strNomCli = (String)vDatos.get(8);
		String strCedCli = (String)vDatos.get(9);
		String strNacCli = (String)vDatos.get(10);

		tpImpresora.activateControl("CERTIFICACION",iDriver);
		strOutLine = " ";
		tpImpresora.enviaCadena(strOutLine);
		strOutLine = " ";
		tpImpresora.enviaCadena(strOutLine);
		strOutLine = " ";
		tpImpresora.enviaCadena(strOutLine);
		strOutLine = "                   " + strSuc + "                                                 " + strFecha.substring(0,2) + "/" + strFecha.substring(2,4) + "/" + strFecha.substring(4,8);
		tpImpresora.enviaCadena(strOutLine);
		strOutLine = "\n ";
		tpImpresora.enviaCadena(strOutLine);
		/*strOutLine = " ";
		tpImpresora.enviaCadena(strOutLine);*/
		strOutLine =  "      " + strOperacion + "                " + strRubro  + strFiller.substring(0, (48 - strUbicacion.length())/2) + strUbicacion + strFiller.substring(0, (40 - strUbicacion.length())/2) +  strMonto;
		tpImpresora.enviaCadena(strOutLine);
		strOutLine = " ";
		tpImpresora.enviaCadena(strOutLine);
		strOutLine = " ";
		tpImpresora.enviaCadena(strOutLine);
		strOutLine = " ";
		tpImpresora.enviaCadena(strOutLine);
		//strOutLine =  strFiller.substring(0, ((40 - strNomDep.length())/2) - strNomDep.length()/4) + strNomDep + strFiller.substring(0, (40 - strNomDep.length())/2) //+ strFiller.substring(0, (48 - strNomFun.length())/2) + strFiller.substring(0, (40 - strNomFun.length())/2) + strNomFun;		
		if(strNomDep.length() > 25 && strNomFun.length() > 25){
			strOutLine =  strFiller.substring(0, (40 - strNomDep.length())/2) + strNomDep + strFiller.substring(0, (40 - strNomDep.length())/2) + strFiller.substring(0, (48 - strNomFun.length())/2) + strFiller.substring(0, (40 - strNomFun.length())/2) + strNomFun;
		}
		else if(strNomDep.length() < 25 && strNomFun.length() > 25){
			strOutLine =  strFiller.substring(0, (25 - strNomDep.length())/2) + strNomDep + strFiller.substring(0, (20 - strNomDep.length())/2) + strFiller.substring(0, (84 - strNomFun.length())/2) + strFiller.substring(0, (40 - strNomFun.length())/2) + strNomFun;		
			}
		else if(strNomDep.length() > 25 && strNomFun.length() < 25){
			strOutLine =  strFiller.substring(0, (40 - strNomDep.length())/2) + strNomDep + strFiller.substring(0, (40 - strNomDep.length())/2) + "    " + strFiller.substring(0, (40 - strNomFun.length())/2) + strNomFun;
		}
		else{
			strOutLine =  strFiller.substring(0, (25 - strNomDep.length())/2)+ strNomDep + strFiller.substring(0, (25 - strNomDep.length())/2) + "                            " + strFiller.substring(0, (25 - strNomFun.length())/2) + strNomFun;			
		}
		
		/*if(strNomDep.length() > 25 && strNomFun.length() > 25){
			strOutLine =  strFiller.substring(0, (40 - strNomDep.length())/2) + strNomDep + strFiller.substring(0, (40 - strNomDep.length())/2) + strFiller.substring(0, (48 - strNomFun.length())/2) + strFiller.substring(0, (40 - strNomFun.length())/2) + strNomFun;
		}else{
			if(strNomDep.length() < 25){
				strOutLine = strFiller.substring(0, (25 - strNomDep.length())/2) + strNomDep;
			}else{
				strOutLine = strFiller.substring(0, (25 - strNomDep.length())/2) + strNomDep;
			}
			if(strNomFun.length() < 25){
				strOutLine += strFiller.substring(0, (40 - strNomDep.length())/2) + strFiller.substring(0, (48 - strNomFun.length())/2) + strFiller.substring(0, (40 - strNomFun.length())/2) + strNomFun;  
			}else{
				strOutLine += strFiller.substring(0, (40 - strNomDep.length())/2) + strFiller.substring(0, (48 - strNomFun.length())/2) + strFiller.substring(0, (40 - strNomFun.length())/2) + strNomFun;
			}
		}*/

		tpImpresora.enviaCadena(strOutLine);
		strOutLine = " ";
		tpImpresora.enviaCadena(strOutLine);
		if(strCargo.length()<12){
		strOutLine =   "      " + strFiller.substring(0, (25 - strCedDep.length())/2) + strCedDep + strFiller.substring(0, (20 - strCedDep.length())/2) +   "                                        " + strFiller.substring(0, (20 - strCedDep.length())/2) + strCargo;
		tpImpresora.enviaCadena(strOutLine);
		}
		else{
			strOutLine =   "      " + strFiller.substring(0, (25 - strCedDep.length())/2) + strCedDep + strFiller.substring(0, (20 - strCedDep.length())/2) +   "                              " + strFiller.substring(0, (20 - strCedDep.length())/2) + strCargo;
			tpImpresora.enviaCadena(strOutLine);
			}
				strOutLine = " ";
		tpImpresora.enviaCadena(strOutLine);
		if(strNacDep.length()>25)
			strOutLine =  "" + strFiller.substring(0, (25 - strNacDep.substring(0, 21).length())/2) + strNacDep;
		else
			strOutLine =   "" + strFiller.substring(0, (25 - strNacDep.length())/2) + strNacDep;
		//strOutLine = strNacDep.substring(0,21);
		tpImpresora.enviaCadena(strOutLine);
		strOutLine = " ";
		tpImpresora.enviaCadena(strOutLine);
		strOutLine = " ";
		tpImpresora.enviaCadena(strOutLine);
		strOutLine =   "" + strFiller.substring(0, (40 - strNomCli.length())/2) + strNomCli;
		tpImpresora.enviaCadena(strOutLine);
		strOutLine = " ";
		tpImpresora.enviaCadena(strOutLine);
		strOutLine =   "      " + strFiller.substring(0, (25 - strCedCli.length())/2) + strCedCli;
		tpImpresora.enviaCadena(strOutLine);
		strOutLine = " ";
		tpImpresora.enviaCadena(strOutLine);
		if(strNacCli.length()>25)
			strOutLine =  "" + strFiller.substring(0, (25 - strNacCli.substring(0, 21).length())/2) + strNacCli;
		else
			strOutLine =   "" + strFiller.substring(0, (25 - strNacCli.length())/2) + strNacCli;
		//strOutLine = strNacCli.substring(0,21);
		tpImpresora.enviaCadena(strOutLine);
		
		if (iDriver == 0) {				
			tpImpresora.InserteDocumento();
			tpImpresora.activateControl("SELECCIONASLIP",iDriver);
			tpImpresora.enviaCadena("");
			tpImpresora.enviaCadena("");
			tpImpresora.enviaCadena("");
			tpImpresora.enviaCadena("");
			tpImpresora.enviaCadena("");
			tpImpresora.imprimeCadena1();
			tpImpresora.activateControl("SALTOHOJA",iDriver);
			tpImpresora.activateControl("SELECCIONAJOURNAL",iDriver);
		} else
			tpImpresora.activateControl("SALTOHOJA",iDriver);
		return intStatus;
	}
}