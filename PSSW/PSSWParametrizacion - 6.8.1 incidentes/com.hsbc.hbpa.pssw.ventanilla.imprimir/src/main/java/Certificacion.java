//*************************************************************************************************
//		   Funcion: Clase que certifica Ordenes de Pago
//		  Elemento: OrdenesPago.java
//		Creado por: Juan Carlos Gaona Marcial
//	Modificado por: Rodrigo Escand�n Soto
//*************************************************************************************************
//CCN - 4620027	- 24/10/2007 - Se actualiza "WWW..COM.MX" por "WWW..COM.PA"
//CCN -	4620027	- 25/10/2007 - Se quita la validaci�n de perfil de cajero 01
//*************************************************************************************************

import java.util.*;
import java.math.BigInteger;

public class Certificacion {

	public int certifica(
		String CFESP,
		String strRespuesta,
		String strCadena,
		String strDate,
		String strDatosSuc,
		int iDriver,
		String strFicha,
		String strDatosFicha,
		String strTellerPerfil,
		String strMontoChqDev1003,
		String strIdentidad,
		String strIdentidad2) {
		int intStatus = 0;
		int intVeces = 0;
		int intCompF = 0;
		int intSize = 0;
		int intCert = 0;

		String strCertificacion = "";
		String strTmp = "";
		String strJustificacion = "";
		String strFiller =
			"                                                                                                    ";
		ToolsPrinter tpImpresora = new ToolsPrinter();
		Vector vCampos = new Vector();
		Vector vDatosSuc = new Vector();
		Vector vCamposT = new Vector();
		Vector vParam = new Vector();
		Vector vDatosChequera = new Vector();
		Vector vDatosKroner = new Vector();
		Vector vDatosMonto = new Vector();
		String strSucursal = "";
		String strCuenta = "";
		String strReferencia = "";
		String strReferencia1 = "";
		String strReferencia2 = "";
		String strReferencia3 = "";
		String strDescripcion = "";
		String strEfectivo = "";
		String strEstatus = "";
		String strMonto = "";
		String strSerial = "";
		String strDivisa = "";
		String strCajero = "";
		String strTxn = "";
		String strProveedor = "";
		String strConsecutivo = "";
		String strComprobante = "";
		String strTitular = "";
		String strTipChqr = "";
		String strSucDest = "";
		String strCtaKroner = "";

		intStatus = tpImpresora.verificaStatus();
		if ((intStatus != 95) && (intStatus != 88)) {
			System.gc();
			return intStatus;
		} else {
			intStatus = 0;
			intVeces = -1;
			intCompF = 0;
			vCampos = tpImpresora.Campos(strRespuesta, "^");
			intSize = vCampos.size();
			for (int i = 0; i < intSize; i++) {
				strTmp = (String) vCampos.elementAt(i);
				if (strTmp.startsWith("CERTIFICACION"))
					intVeces++;
				if (strTmp.startsWith("COMPROBANTE_FISCAL")) {
					strTmp = (String) vCampos.elementAt(i);
					vCamposT = tpImpresora.Campos(strTmp, "@");
					strTmp = (String) vCamposT.elementAt(1);
					if (strTmp.equals("1") || strTmp.equals("S"))
						intCompF = 1;
				}
				if (strTmp.startsWith("PROVEEDOR")) {
					strTmp = (String) vCampos.elementAt(i);
					vCamposT = tpImpresora.Campos(strTmp, "@");
					strProveedor = (String) vCamposT.elementAt(1);
				}
				if (strTmp.startsWith("ERRORCOMUNICACION"))
					intVeces++;
			}
			for (int i = 0; i < intSize; i++) {
				strTmp = (String) vCampos.elementAt(i);
				if (strTmp.startsWith("CERTIFICACION")) {
					strTmp = (String) vCampos.elementAt(i);
					vCamposT = tpImpresora.Campos(strTmp, "@");
					strTmp = (String) vCamposT.elementAt(1);
					vCamposT = tpImpresora.Campos(strTmp, "|");
					strTmp = (String) vCamposT.elementAt(0);
					vParam = tpImpresora.Campos(strTmp, "~");
					intCert = 1;
				}
				if (vParam.size() > 13)
				{
					strTxn = (String) vParam.elementAt(13);
					strEstatus = (String) vParam.elementAt(10);				
				}
				if (strTxn.equals("0101") && CFESP != null){
					vDatosChequera = tpImpresora.Campos(CFESP, "~");
					strTipChqr = vDatosChequera.elementAt(1).toString();
					if (strTipChqr.equals("01") || strTipChqr.equals("1"))
						strTipChqr = "CON COPIA";
					else if (strTipChqr.equals("02") || strTipChqr.equals("2"))
						strTipChqr = "SIN COPIA";
					else if (strTipChqr.equals("03") || strTipChqr.equals("3"))
						strTipChqr = "ECOLOGICA";
					else if (strTipChqr.equals("04") || strTipChqr.equals("4"))
						strTipChqr = strIdentidad +" PREFERENCIAL CON COPIA";
					else if (strTipChqr.equals("05") || strTipChqr.equals("5"))
						strTipChqr = strIdentidad +" PREFERENCIAL SIN COPIA";
					else if (strTipChqr.equals("08") || strTipChqr.equals("8"))
						strTipChqr = strIdentidad +" PLUS CON COPIA";
				    else if (strTipChqr.equals("09") || strTipChqr.equals("9"))
				        strTipChqr = strIdentidad +" PLUS SIN COPIA";
					else if (strTipChqr.equals("51"))
				        strTipChqr = strIdentidad +" 3 - 1";
					else if (strTipChqr.equals("52"))
				        strTipChqr = strIdentidad +" PRE-ELABORADAS CON CLISE ";
					else if (strTipChqr.equals("53"))
				        strTipChqr = strIdentidad +"PRE-ELABORADAS SIN CLISE ";
					else if (strTipChqr.equals("54"))
				        strTipChqr = strIdentidad +" ESPECIAL VOUCHER 2 PARTES";
					else if (strTipChqr.equals("55"))
				        strTipChqr = strIdentidad +" ESPECIAL VOUCHER 3 PARTES";
					else if (strTipChqr.equals("56"))
				        strTipChqr = strIdentidad +" ESPECIAL VOUCHER 4 PARTES";
					else if (strTipChqr.equals("57"))
				        strTipChqr = strIdentidad +" LASER";						
					
					else
						strTipChqr = "COMERCIAL";							
					strSucDest = vDatosChequera.elementAt(2).toString();
					if (strSucDest.equals("00001"))
						strSucDest = "TORRE " + strIdentidad;
					else if (strSucDest.equals("00002"))
						strSucDest = "EL DORADO MALL";
					else if (strSucDest.equals("00003"))
						strSucDest = "OBARRIO";
					else if (strSucDest.equals("00004"))
						strSucDest = "VIA ESPA�A";
					else if (strSucDest.equals("00005"))
						strSucDest = "EL DORADO " + strIdentidad;
					else if (strSucDest.equals("00006"))
						strSucDest = "MULTIMAX " + strIdentidad;
					else if (strSucDest.equals("00007"))
						strSucDest = "BETHANIA";
					else if (strSucDest.equals("00008"))
						strSucDest = "PARQUE LEFREVE";
					else if (strSucDest.equals("00009"))
						strSucDest = "P.A. TRANSISTMICA";
					else if (strSucDest.equals("00010"))
						strSucDest = "LOS PUEBLOS";
					else if (strSucDest.equals("00011"))
						strSucDest = "PUNTA PAITILLA";
					else if (strSucDest.equals("00012"))
						strSucDest = "CINCO DE MAYO " +strIdentidad;
					else if (strSucDest.equals("00013"))
						strSucDest = "BETHANIA "+strIdentidad;
					else if (strSucDest.equals("00014"))
						strSucDest = "PLAZA CAROLINA";
					else if (strSucDest.equals("00015"))
						strSucDest = "CAMILO PORRAS";
					else if (strSucDest.equals("00016"))
						strSucDest = "LOS PUEBLOS "+strIdentidad;
					else if (strSucDest.equals("00017"))
						strSucDest = "ALBROOK";
					else if (strSucDest.equals("00018"))
						strSucDest = "C/P ALBROOS";
					else if (strSucDest.equals("00019"))
						strSucDest = "VILLA LUCRE BDI";
					else if (strSucDest.equals("00020"))
						strSucDest = "VILLA LUCRE "+strIdentidad;
					else if (strSucDest.equals("00021"))
						strSucDest = "RIO ABAJO";
					else if (strSucDest.equals("00022"))
						strSucDest = "C/P LA DO�A";
					else if (strSucDest.equals("00023"))
						strSucDest = "PLAZA "+strIdentidad;
					else if (strSucDest.equals("00024"))
						strSucDest = "CALLE 50";
					else if (strSucDest.equals("00025"))
						strSucDest = "JUSTO AROSAMENA";
					else if (strSucDest.equals("00026"))
						strSucDest = "WORLD TRADE CENTER";
					else if (strSucDest.equals("00027"))
						strSucDest = "SAN FRANCISCO";
					else if (strSucDest.equals("00028"))
						strSucDest = "ZONA LIBRE "+strIdentidad;
					else if (strSucDest.equals("00029"))
						strSucDest = "ZONA LIBRE BDI";
					else if (strSucDest.equals("00030"))
						strSucDest = "VIA ARGENTINA";
					else if (strSucDest.equals("00031"))
						strSucDest = "COLON "+strIdentidad;
					else if (strSucDest.equals("00032"))
						strSucDest = "COLON BDI";
					else if (strSucDest.equals("00033"))
						strSucDest = "C.M. PAITILLA";
					else if (strSucDest.equals("00034"))
						strSucDest = "RIO ABAJO "+strIdentidad;
					else if (strSucDest.equals("00035"))
						strSucDest = "BALBOA "+strIdentidad;
					else if (strSucDest.equals("00036"))
						strSucDest = "BALBOA BDI";
					else if (strSucDest.equals("00037"))
						strSucDest = "CHANIS";
					else if (strSucDest.equals("00038"))
						strSucDest = "LOS ANDES";
					else if (strSucDest.equals("00039"))
						strSucDest = "PLAZA BELEM";
					else if (strSucDest.equals("00040"))
						strSucDest = "MULTIPLAZA PACIFIC "+strIdentidad;
					else if (strSucDest.equals("00041"))
						strSucDest = "PLAZA PACIFICA "+strIdentidad;
					else if (strSucDest.equals("00042"))
						strSucDest = "MULTICENTRO";
					else if (strSucDest.equals("00043"))
						strSucDest = "C/P AVE. PERU";
					else if (strSucDest.equals("00044"))
						strSucDest = "CHITRE "+strIdentidad;
					else if (strSucDest.equals("00045"))
						strSucDest = "CHITRE BDI";
					else if (strSucDest.equals("00046"))
						strSucDest = "SANTIAGO";
					else if (strSucDest.equals("00047"))
						strSucDest = "DAVID BDI";
					else if (strSucDest.equals("00048"))
						strSucDest = "DAVID "+strIdentidad;
					else if (strSucDest.equals("00049"))
						strSucDest = "LA CHORRERA";
					else if (strSucDest.equals("00050"))
						strSucDest = "LAS TABLAS";
					else if (strSucDest.equals("00051"))
						strSucDest = "PENONOME";
					else if (strSucDest.equals("00052"))
						strSucDest = "AGUADULCE " + strIdentidad ;
					else if (strSucDest.equals("00053"))
						strSucDest = "AGUADULCE BDI";
					else if (strSucDest.equals("00054"))
						strSucDest = "PUERTO ARMUELLES";
					else if (strSucDest.equals("00055"))
						strSucDest = "VOLCAN";
					else if (strSucDest.equals("00056"))
						strSucDest = "CHANGUINOLA";
					else if (strSucDest.equals("00057"))
						strSucDest = "BUGABA";
					else if (strSucDest.equals("00058"))
						strSucDest = "BOQUETE BDI";
					else if (strSucDest.equals("00059"))
						strSucDest = "BOQUETE "+strIdentidad;
					else if (strSucDest.equals("00060"))
						strSucDest = "DOLEGUITA";
					else if (strSucDest.equals("00061"))
						strSucDest = "CHEPO";

				}
				else if (strTxn.equals("4019") && CFESP != null){
					vDatosKroner = tpImpresora.Campos(CFESP, "~");
					strCtaKroner = vDatosKroner.elementAt(1).toString();				
				}
				else
					vDatosChequera = null;
// if (!(strFicha.equals("S") && ( strTxn.equals("1001") ||
// strTxn.equals("1003") || strTxn.equals("4541"))) ||
// strTellerPerfil.equals("01") || strEstatus.equals("R") ||
// strEstatus.equals("X") || strEstatus.equals("C") || intCert == 0)
				if (!(strFicha.equals("S") && ( strTxn.equals("1001") || strTxn.equals("1003") || strTxn.equals("4541"))) || strEstatus.equals("R") ||  strEstatus.equals("X") || strEstatus.equals("C") || intCert == 0)
				{
					if (intCert == 1) {
						tpImpresora.activateControl("CERTIFICACION",iDriver);
						intCert = 0;
						strComprobante = "";
						strSucursal = (String) vParam.elementAt(0);
						strCuenta = (String) vParam.elementAt(1);
						strDivisa = (String) vParam.elementAt(2);
						if ((strDivisa.equals("N$")) || (strDivisa.equals("N$ ")))
							strDivisa = "$";
						strSerial = (String) vParam.elementAt(3);
						strReferencia = ((String) vParam.elementAt(4)).trim();
						strReferencia1 = ((String) vParam.elementAt(5)).trim();
						strReferencia2 = ((String) vParam.elementAt(6)).trim();
						strReferencia3 = ((String) vParam.elementAt(7)).trim();
						strDescripcion = (String) vParam.elementAt(8);
						strEfectivo = (String) vParam.elementAt(9);
						if (strEstatus.equals("R")) {
							tpImpresora.muestraRechazo();
						}
						strMonto = (String) vParam.elementAt(11);
						strConsecutivo = (String) vParam.elementAt(14);
						strCajero = (String) vParam.elementAt(15);
						
						String tmpStrCertificacion=""; 
						if(strTxn.equals("5177") || strTxn.equals("5183")|| strTxn.equals("0598")){
							tmpStrCertificacion = "\n\n\n";
						}					
						
						strCertificacion = 
							tmpStrCertificacion 
							 	+ strConsecutivo
								+ " "
								+ strDate.substring(7, 9)
								+ strDate.substring(5, 7)
								+ strDate.substring(1, 5)
								+ " "
								+ strTxn
								+ " "
								+ strCajero
								+ " ";
						if (strTxn.equals("0460"))
							strCertificacion = strCertificacion + " " + strTxn;
						else if (strTxn.equals("4019") && CFESP != null)
							strCertificacion = strCertificacion + " " + strCtaKroner + " ";
						else
							strCertificacion = strCertificacion + strCuenta + " ";
						if (!strEfectivo.equals(""))
							strCertificacion =
								strCertificacion
									+ strDivisa
									+ tpImpresora.formatMonto(strEfectivo)
									+ " ";
						if (!strMonto.equals(""))
							strCertificacion =
								strCertificacion
									+ "   "
									+ strDivisa
									+ tpImpresora.formatMonto(strMonto.trim())
									+ " ";
						strCertificacion = strCertificacion + strSucursal;
						tpImpresora.enviaCadena(" ");
						// Primera Linea de Certificacion
						if (iDriver == 0)
							strCertificacion = strJustificacion + strCertificacion;
						tpImpresora.enviaCadena(strCertificacion);
						if (strDescripcion.length() > 38)
							strComprobante =
								"R.F.C. "
									+ strDescripcion.substring(5, 24)
									+ "     I.V.A. "
									+ strDivisa
									+ strDescripcion.substring(25, 39);
						if (CFESP != null) {
							if ((strTxn.equals("4545") || strTxn.equals("4089"))
								&& (CFESP.length() > 5)) {
								intCompF = 1;
								int indice = CFESP.indexOf("~");
								strComprobante =
									"R.F.C. "
										+ CFESP.substring(0, indice)
										+ "     I.V.A. "
										+ CFESP.substring(indice + 1);
							}
						}
						if (strTxn.equals("0542")
							|| strTxn.equals("1183")
							|| strTxn.equals("0604")
							|| strTxn.equals("1009")
							|| strTxn.equals("4009")
							|| strTxn.equals("0528")
							|| strTxn.equals("0580")
							|| strTxn.equals("0460")) {
							strDescripcion = "";
							strReferencia = "";
							strReferencia1 = "";
							strReferencia2 = "";
							strReferencia3 = "";
						}
						strCertificacion = strDescripcion + " ";
						if (strTxn.equals("4117"))
							strCertificacion =
								strReferencia
									+ " "
									+ strIdentidad
									+ " "
									+ strCertificacion
									+ strReferencia3
									+ " ";
						else
							strCertificacion = strCertificacion + strSerial + " ";
						if ((strTxn.equals("4303")) || (strTxn.equals("0544")))
							strCertificacion =
								strCertificacion + " " + strReferencia3 + " ";
						strCertificacion = strCertificacion + strReferencia + " ";
						strCertificacion = strCertificacion + strReferencia1 + " ";
						strCertificacion = strCertificacion + strReferencia2 + " ";
						strCertificacion = strCertificacion + strReferencia3;
						if (strTxn.equals("0460"))
							strCertificacion = strReferencia + strReferencia1;
						if (strTxn.equals("0108")) {
							strCertificacion = strReferencia;
							Vector vDatos = tpImpresora.Campos(strCadena, "~");
							if (vDatos.size() > 0) {
								String strNombre = (String) vDatos.elementAt(0);
								if (strNombre.equals("COMPRAVENTA") == true) {
									String strTipoCambio =
										(String) vDatos.elementAt(4);
									if (strTipoCambio.equals(""))
										strTipoCambio = "0.00";
									strCertificacion =
										strCertificacion
											+ " TIPCAM "
											+ strTipoCambio;
								}
							}
						}
						if (strTxn.equals("0572"))
							strCertificacion =
								strSerial
									+ " "
									+ strReferencia
									+ "-"
									+ strReferencia1
									+ "/021000089";
						if (!strCertificacion.trim().equals("")) {
							if (strCertificacion.indexOf("REV") < 0)
								if (strEstatus.equals("C")
									|| strEstatus.equals("X"))
									strCertificacion =
										"REVERSO " + strCertificacion;
							// Segunda Linea de Certificacion
							if (iDriver == 0)
								strCertificacion =
									strJustificacion + strCertificacion;
							tpImpresora.enviaCadena(strCertificacion);
						} else {
							if (strEstatus.equals("C") || strEstatus.equals("X")) {
								strCertificacion = "REVERSO ";
								if (iDriver == 0)
									strCertificacion =
										strJustificacion + strCertificacion;
								tpImpresora.enviaCadena(strCertificacion);
							}
						}
						vDatosSuc = tpImpresora.Campos(strDatosSuc, "@");
						String strNumSuc = (String) vDatosSuc.elementAt(1);
						strNumSuc = strNumSuc.substring(7,strNumSuc.length());         
						String strNomSuc = (String) vDatosSuc.elementAt(0);
						strNomSuc = strNomSuc.substring(7,strNomSuc.length());
						tpImpresora.enviaCadena(strJustificacion + "SUCURSAL: " + strNumSuc + "  " + strNomSuc); 
						strTmp = (String) vParam.elementAt(12);
						if (strTxn.equals("0101") && CFESP != null){
							tpImpresora.enviaCadena(strJustificacion + "TIPO DE CHEQUERA: " + strTipChqr);
							tpImpresora.enviaCadena(strJustificacion + "SUCURSAL DESTINO: " + strSucDest);
						}
						int nlineas = strTmp.length() / 78;
						if (strTmp.length() % 78 > 0)
							nlineas++;
						if (strTxn.equals("0836") && (!strEstatus.equals("R")))
							nlineas--;
						for (int j = 0; j < nlineas; j++) {
							String strTmpL = "";
							if (((j * 78) + 78) < strTmp.length())
								if(strTxn.equals("5357") && j == 1)
									strTmpL = "NOMBRE: " + strTmp.substring(78, 156);
								else
									strTmpL = strTmp.substring((j * 78), (j * 78) + 78);
							else
								strTmpL = strTmp.substring(j * 78, strTmp.length());
							if (iDriver != 1)
								strTmpL = strJustificacion + strTmpL;
							tpImpresora.enviaCadena(strTmpL);
						}
						/*
						 * if (strTxn.equals("4019") && CFESP != null){
						 * tpImpresora.enviaCadena(""); strCertificacion =
						 * strJustificacion + "CON LA RECEPCION DE ESTE
						 * COMPROBANTE EL CLIENTE ACEPTA QUE LOS DATOS";
						 * tpImpresora.enviaCadena(strCertificacion);
						 * strCertificacion = strJustificacion + "AQUI
						 * CONTENIDOS SON CORRECTOS.";
						 * tpImpresora.enviaCadena(strCertificacion);
						 * tpImpresora.enviaCadena("\n\n\n");
						 * tpImpresora.enviaCadena(strFiller.substring(0, 68) +
						 * "_____________________");
						 * tpImpresora.enviaCadena(strFiller.substring(0, 76) +
						 * "FIRMA"); }
						 */
						if (!strProveedor.equals("") && strTxn.equals("1053"))						
							if (iDriver != 1)
								strProveedor = strJustificacion + strProveedor;												
							tpImpresora.enviaCadena(strProveedor);
						if (intCompF == 1) {
							if (iDriver != 1)
								strComprobante = strJustificacion + strComprobante;
							tpImpresora.enviaCadena(strComprobante);
						}
						if (strTxn.equals("0051")
							&& (!strEstatus.equals("R"))
							&& (!strEstatus.equals("C"))
							&& (!strEstatus.equals("X"))) {
							vDatosSuc = tpImpresora.Campos(strDatosSuc, "@");
							String strPlaza = (String) vDatosSuc.elementAt(5);
							strPlaza = strPlaza.substring(7, strPlaza.length());
							strPlaza = strPlaza.trim();
							tpImpresora.enviaCadena("\n\n\n");
							strTmp = strDate.substring(5, 7);
							if (strTmp.equals("01"))
								strTmp = "ENERO";
							else if (strTmp.equals("02"))
								strTmp = "FEBRERO";
							else if (strTmp.equals("03"))
								strTmp = "MARZO";
							else if (strTmp.equals("04"))
								strTmp = "ABRIL";
							else if (strTmp.equals("05"))
								strTmp = "MAYO";
							else if (strTmp.equals("06"))
								strTmp = "JUNIO";
							else if (strTmp.equals("07"))
								strTmp = "JULIO";
							else if (strTmp.equals("08"))
								strTmp = "AGOSTO";
							else if (strTmp.equals("09"))
								strTmp = "SEPTIEMBRE";
							else if (strTmp.equals("10"))
								strTmp = "OCTUBRE";
							else if (strTmp.equals("11"))
								strTmp = "NOVIEMBRE";
							else if (strTmp.equals("12"))
								strTmp = "DICIEMBRE";
							strTmp =
								strPlaza
									+ ",        "
									+ strDate.substring(7, 9)
									+ " "
									+ strTmp
									+ " "
									+ strDate.substring(1, 5);
							strCertificacion =
								strTmp
									+ strFiller.substring(0, 80 - strTmp.length())
									+ strCuenta;
							if (iDriver != 1)
								strCertificacion =
									strJustificacion + strCertificacion;
							tpImpresora.enviaCadena(strCertificacion);
						}
						if (iDriver == 0) {
							tpImpresora.InserteDocumento();
							tpImpresora.activateControl("SELECCIONASLIP",iDriver);
							tpImpresora.enviaCadena("");
							tpImpresora.enviaCadena("");
							tpImpresora.enviaCadena("");
							tpImpresora.enviaCadena("");
							tpImpresora.enviaCadena("");
							tpImpresora.imprimeCadena1();
							tpImpresora.activateControl("SALTOHOJA",iDriver);
							tpImpresora.activateControl("SELECCIONAJOURNAL",iDriver);
						} else {
							tpImpresora.activateControl("SALTOHOJA",iDriver);
						}
						if (intVeces > 0) {
							intVeces--;
							if (iDriver == 1)
								tpImpresora.muestraVentana();
						}
						} else if (
						(strTmp.startsWith("ERRORCOMUNICACION"))
							|| (intVeces == -1)) {
						tpImpresora.enviaCadena(
							"                              TRANSACCION RECHAZADA!!!");
						tpImpresora.enviaCadena(
							"                              VERIFIQUE ESTATUS EN EL PROCESADOR CENTRAL");
						if (iDriver == 0) {
							tpImpresora.InserteDocumento();
							tpImpresora.activateControl("SELECCIONASLIP",iDriver);
							tpImpresora.enviaCadena("");
							tpImpresora.enviaCadena("");
							tpImpresora.enviaCadena("");
							tpImpresora.enviaCadena("");
							tpImpresora.enviaCadena("");
							tpImpresora.imprimeCadena1();
							tpImpresora.activateControl("SALTOHOJA",iDriver);
							tpImpresora.activateControl("SELECCIONAJOURNAL",iDriver);
						} else {
							tpImpresora.activateControl("SALTOHOJA",iDriver);
						}
						intVeces--;
						if (intVeces > 0) {
							intVeces--;
							if (iDriver == 1)
								tpImpresora.muestraVentana();
						}
					}
					}
				else				
				if ((strFicha.equals("S") &&  ( strTxn.equals("1001") || strTxn.equals("1003") || strTxn.equals("4541")) && intCert == 1))
				{
					tpImpresora.activateControl("CERTIFICACION",iDriver);
					intCert = 0;					
					vDatosSuc = tpImpresora.Campos(strDatosSuc, "@");
					String strNumSuc = (String) vDatosSuc.elementAt(1);
					strNumSuc = strNumSuc.substring(7,strNumSuc.length());         
					String strNomSuc = (String) vDatosSuc.elementAt(0);
					strNomSuc = strNomSuc.substring(7,strNomSuc.length());         
					strCuenta = (String) vParam.elementAt(1);
					strDivisa = (String) vParam.elementAt(2);
					strSerial = (String) vParam.elementAt(3);					
					strCajero = (String) vParam.elementAt(15);					
					int iDivisa = 2;
					if ((strDivisa.equals("N$")) || (strDivisa.equals("N$ ")))
					{
						strDivisa = "$";
						iDivisa = 1;
					}
					strEfectivo = (String) vParam.elementAt(9);
					strMonto = (String) vParam.elementAt(11);
					strConsecutivo = (String) vParam.elementAt(14);					
					tpImpresora.enviaCadena("");
					strCertificacion = "SUCURSAL : " + strNumSuc + "-" + strNomSuc;
					tpImpresora.enviaCadena(strCertificacion);
					tpImpresora.enviaCadena(" ");
					strCertificacion = strDate.substring(7, 9) + "-" + strDate.substring(5, 7) + "-" + strDate.substring(1, 5) + " " + strCajero;			
					tpImpresora.enviaCadena(strCertificacion);
					tpImpresora.enviaCadena(" ");					
					if (strTxn.equals("1003"))
						tpImpresora.enviaCadena("           DEPOSITO MIXTO");
					else
						if (strTxn.equals("1001"))					 					
							tpImpresora.enviaCadena("     DEPOSITO CUENTA NUEVA");					
						else					
							tpImpresora.enviaCadena("    DEPOSITO CHEQUE CERTIFICADO");
					tpImpresora.enviaCadena(" ");
					tpImpresora.enviaCadena("   TXN    CONSEC");
					tpImpresora.enviaCadena(" ");
					strCertificacion = "  " + strTxn + "    " + strConsecutivo;
					tpImpresora.enviaCadena(strCertificacion);
					tpImpresora.enviaCadena(" ");
					//
					strCuenta = tpImpresora.limpiaEspacio(strCuenta);
					strCertificacion = "   CUENTA DE DEPOSITO : " + strCuenta;
					tpImpresora.enviaCadena(strCertificacion);
					String strTmpL = "";						
					String strImporte = new String("");					
					String strStatus = new String("");					
					String strCertificacionTmp = new String("");
					if (!(strTxn.equals("4541")))
					{
						strTmp = (String) vParam.elementAt(12);
						int nlineas = strTmp.length() / 78;
						if (strTmp.length() % 78 > 0)
							nlineas++;				
						for (int j = 0; j < nlineas; j++) {
							if (((j * 78) + 78) < strTmp.length())
								strTmpL = strTmp.substring((j * 78), (j * 78) + 78);
							else
								strTmpL = strTmp.substring(j * 78, strTmp.length());
							if (j == 1 )
								j = nlineas + 2;
						}					
						strCertificacion = "   NOMBRE : " + strTmpL;
						tpImpresora.enviaCadena(strCertificacion);						
						strCertificacionTmp = strDivisa + tpImpresora.formatMonto(strEfectivo);
						strCertificacion = strFiller.substring(0,21-strCertificacionTmp.length()) + strCertificacionTmp;
						//
						strCertificacion = tpImpresora.limpiaEspacio(strCertificacion);
						strCertificacion = "   EFECTIVO : " + strCertificacion;
						tpImpresora.enviaCadena(strCertificacion);										
						if (!(strDatosFicha.equals("N")))
						{
							Vector vDatosFicha = tpImpresora.Campos(strDatosFicha, "~");
							int intRepetir = (vDatosFicha.size() / 3);
							for (int veces = 0; veces < intRepetir; veces++) {
								strImporte = (String) vDatosFicha.elementAt(veces * 3);								
								strStatus = (String) vDatosFicha.elementAt(veces * 3 + 2);
								if (strStatus.equals("A"))
								{
									strImporte = tpImpresora.formatMonto(strImporte);
									strImporte = strFiller.substring(0,20-strImporte.length()) + strImporte;
									//
									strImporte = tpImpresora.limpiaEspacio(strImporte);
									//
									strDivisa = tpImpresora.limpiaEspacio(strDivisa);
									strCertificacion = "   DOCUMENTO " + (veces + 1) + " : " + strDivisa 
										+ strImporte;
									tpImpresora.enviaCadena(strCertificacion);
									strCertificacion = "   SERIAL :"
										+ (String) vDatosFicha.elementAt(veces * 3 + 1);								
										tpImpresora.enviaCadena(strCertificacion);
								}
							}						
							for (int veces = (vDatosFicha.size() / 3) -1 ; veces < 6; veces++) 
								tpImpresora.enviaCadena(" ");													
						}
						else
						{
							for (int veces =0  ; veces < 6; veces++) 
								tpImpresora.enviaCadena(" ");													
						}													
					}
					else
					{
						tpImpresora.enviaCadena(" ");												
						strCertificacionTmp = strDivisa + tpImpresora.formatMonto(strEfectivo);
						strCertificacion = strFiller.substring(0,21-strCertificacionTmp.length()) + strCertificacionTmp;
						//
						strCertificacion = tpImpresora.limpiaEspacio(strCertificacion);
						strCertificacion = "      EFECTIVO : " + strCertificacion;
						tpImpresora.enviaCadena(strCertificacion);											
						strImporte = tpImpresora.formatMonto(strMonto);
						strImporte = strFiller.substring(0,20-strImporte.length()) + strImporte;
						//
						strImporte = tpImpresora.limpiaEspacio(strImporte);
						//
						strDivisa = tpImpresora.limpiaEspacio(strDivisa);
						
						strCertificacion = "   DOCUMENTO 1 : " + strDivisa 
							+ strImporte
							+ "\n   SERIAL :"
							+ strSerial;								
							tpImpresora.enviaCadena(strCertificacion);						
						for (int veces =0  ; veces < 5; veces++) 
							tpImpresora.enviaCadena(" ");																			
					}					
					// BigInteger bDocumentos = new BigInteger("0");
					BigInteger bCantidad1 = new BigInteger("10000000");
					BigInteger bCantidad2 = new BigInteger("850000");
					// BigInteger bDeposito = new BigInteger("0");
					BigInteger bDocumentos = new BigInteger(strMonto);
					BigInteger bDeposito = new BigInteger(strMonto);
					// System.out.println("bDeposito " + bDeposito.toString());
					bDocumentos = bDocumentos.subtract(new BigInteger(strEfectivo));
					bDocumentos = bDocumentos.subtract(new BigInteger(strMontoChqDev1003));
					String strDocumentos = bDocumentos.toString();										
					strCertificacionTmp = strDivisa + tpImpresora.formatMonto(strDocumentos);
					strCertificacion = strFiller.substring(0,21-strCertificacionTmp.length()) + strCertificacionTmp;
					//
					strCertificacion = tpImpresora.limpiaEspacio(strCertificacion);
					strCertificacion = "   TOTAL DOCS ACEPTADOS : " + strCertificacion;
					tpImpresora.enviaCadena(strCertificacion);
					strCertificacionTmp = strDivisa + tpImpresora.formatMonto(strMontoChqDev1003);
					strCertificacion = strFiller.substring(0,21-strCertificacionTmp.length()) + strCertificacionTmp;
					//
					strCertificacion = tpImpresora.limpiaEspacio(strCertificacion);
					strCertificacion = "   TOTAL DOCS DEVUELTOS : " + strCertificacion;
					tpImpresora.enviaCadena(strCertificacion);
					strCertificacionTmp = strDivisa + tpImpresora.formatMonto(strMonto);
					strCertificacion = strFiller.substring(0,21-strCertificacionTmp.length()) + strCertificacionTmp;
					//
					strCertificacion = tpImpresora.limpiaEspacio(strCertificacion);
					strCertificacion = "         TOTAL DEPOSITO : " + strCertificacion;
					tpImpresora.enviaCadena(strCertificacion);						
					tpImpresora.enviaCadena("");
					tpImpresora.enviaCadena("");
					tpImpresora.enviaCadena("");
					tpImpresora.enviaCadena("");
					tpImpresora.enviaCadena("");					
					if (iDivisa == 1)
					{	
						int cr = bDeposito.compareTo(bCantidad1);
						// System.out.println("cr " + cr);
						if (cr < 0)
						{
							tpImpresora.enviaCadena("");
							tpImpresora.enviaCadena("");
							tpImpresora.enviaCadena("");						
						}
						else
						{
							tpImpresora.enviaCadena("DEPOSITANTE");							
							tpImpresora.enviaCadena("");
							tpImpresora.enviaCadena("RECIBI DE CONFORMIDAD");							
						}					
					}
					else
					{
						int cr = bDeposito.compareTo(bCantidad2);
						if (cr < 0)
						{
							tpImpresora.enviaCadena("");
							tpImpresora.enviaCadena("");
							tpImpresora.enviaCadena("");						
						}
						else
						{
							tpImpresora.enviaCadena("DEPOSITANTE");							
							tpImpresora.enviaCadena("");
							tpImpresora.enviaCadena("RECIBI DE CONFORMIDAD");							
						}											
					}
					tpImpresora.enviaCadena("");
					tpImpresora.enviaCadena("");
					tpImpresora.enviaCadena("");
					tpImpresora.enviaCadena("");
					tpImpresora.enviaCadena("");
					tpImpresora.enviaCadena("");
					tpImpresora.enviaCadena("             SELLO Y FIRMA");							
					tpImpresora.enviaCadena("");
					tpImpresora.enviaCadena(" CON LA RECEPCION DE ESTE COMPROBANTE EL DEPOSITANTE ACEPTA QUE LOS");
					tpImpresora.enviaCadena(" DATOS AQUI CONTENIDOS SON CORRECTOS.                              ");
					tpImpresora.enviaCadena("");
					tpImpresora.enviaCadena("               "+strIdentidad2+"    ");												
					tpImpresora.enviaCadena("");
					tpImpresora.enviaCadena("");
					tpImpresora.enviaCadena("");
					if (iDriver == 0) {
						tpImpresora.enviaCadena("");
						tpImpresora.enviaCadena("");
						tpImpresora.enviaCadena("");
						tpImpresora.enviaCadena("");
						tpImpresora.enviaCadena("");
						tpImpresora.enviaCadena("");
						tpImpresora.enviaCadena("");
						tpImpresora.enviaCadena("");
						tpImpresora.enviaCadena("");
						tpImpresora.enviaCadena("");
						tpImpresora.imprimeCadena1();
						tpImpresora.activateControl("CORTAPAPEL",iDriver);
					} else 
						tpImpresora.activateControl("SALTOHOJA",iDriver);
				}				
				
			}
		}
		System.gc();
		return intStatus;
	}
}