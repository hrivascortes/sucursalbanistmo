import java.util.Vector;

public class LAhorro
{
  public int imprimeMovimientos(String strRespuesta, int iDriver)
  {
    String strFiller = 
      "                                                                                                                                                                ";

    String strFiller1 = "****************************************************************************************************";
    String strFiller2 = "012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789";

    Vector vParametros = new Vector();
    String renglon = "";
    String nombre_xml = "";
    String cta_xml = "";
    String sucursal_xml = "";
    String fecha_oper = "";
    String num_cheq = "";
    String tran_id = "";
    String monto = "";
    String tipTran = "";
    String saldo = "";
    String rastrId = "";
    String descripcion = "";
    String tipo = "";
    String strCer = "";
    int lineaImpDisp = 0;
    int intNoCampos = 12;
    int k = 0;
    int maxRenglones = 0;
    int x = 1;
    int z = 1;

    ToolsPrinter tpImpresora = new ToolsPrinter();
    int intStatus = tpImpresora.verificaStatus();

    if ((intStatus == 95) || (intStatus == 88)) {
      strCer = "";
      intStatus = 0;
      tpImpresora.activateControl("OPECONANC", iDriver);
		tpImpresora.activateControl("CERTIFICACION",iDriver);

      vParametros = tpImpresora.Campos(strRespuesta, "~");
      lineaImpDisp = (vParametros.size() - 1) / 12;

      tipo = (String)vParametros.elementAt(0);
      renglon = (String)vParametros.elementAt(1);
      nombre_xml = (String)vParametros.elementAt(2);
      cta_xml = (String)vParametros.elementAt(3);
      sucursal_xml = (String)vParametros.elementAt(4);

      if (iDriver == 0)
      {
        int i;
        if (renglon.equals("0"))
        {
          maxRenglones = 25 - Integer.parseInt(renglon) + 1;
          strCer = "\n" + strFiller.substring(0, 36) + 
            nombre_xml + 
            "\n";
          tpImpresora.enviaCadena(strCer);

          strCer = 
            "\n" + 
            strFiller.substring(0, 36) + 
            cta_xml + 
            strFiller.substring(0, 26) + sucursal_xml;
          tpImpresora.enviaCadena(strCer);

          strCer = "\n\n";

          tpImpresora.enviaCadena(strCer);
        }
        else
        {
          if (Integer.parseInt(renglon) > 25) renglon = "25";
          maxRenglones = 25 - Integer.parseInt(renglon) + 1;
          k = 25 - maxRenglones + 1;
          strCer = "\n\n\n\n\n\n";

          while (x < k)
          {
            strCer = strCer + "\n\n";
            ++x;
          }

          tpImpresora.enviaCadena(strCer);
        }

        if (lineaImpDisp < maxRenglones + 1) {
          for (i = 0; i < lineaImpDisp; ++i)
          {
            fecha_oper = (String)vParametros.elementAt(i * 12 + 5);
            num_cheq = (String)vParametros.elementAt(i * 12 + 6);
            tran_id = (String)vParametros.elementAt(i * 12 + 7);
            monto = (String)vParametros.elementAt(i * 12 + 8);
            tipTran = (String)vParametros.elementAt(i * 12 + 9);
            saldo = (String)vParametros.elementAt(i * 12 + 10);
            rastrId = (String)vParametros.elementAt(i * 12 + 11);
            descripcion = (String)vParametros.elementAt(i * 12 + 12);

            strCer = strFiller.substring(0, 19) + fecha_oper;

            if (tipTran.equals("D"))
            {
              strCer = strCer + 
                strFiller.substring(0, 20) + monto;
              strCer = strCer + 
                strFiller.substring(0, 15 - monto.length()) + saldo;
              strCer = strCer + 
                strFiller.substring(0, 17 - saldo.length()) + tran_id;
            }

            if (tipTran.equals("C"))
            {
              strCer = strCer + 
                strFiller.substring(0, 6) + monto;

              strCer = strCer + 
                strFiller.substring(0, 25 - monto.length()) + saldo;
              strCer = strCer + 
                strFiller.substring(0, 17 - saldo.length()) + tran_id;
            }

            strCer = strCer + "\n";

            tpImpresora.enviaCadena(strCer);
          }
        }
        else
        {
          if (renglon.equals("0")) {
            --maxRenglones;
          }
          for (i = 0; i < maxRenglones; ++i)
          {
            fecha_oper = (String)vParametros.elementAt(i * 12 + 5);
            num_cheq = (String)vParametros.elementAt(i * 12 + 6);
            tran_id = (String)vParametros.elementAt(i * 12 + 7);
            monto = (String)vParametros.elementAt(i * 12 + 8);
            tipTran = (String)vParametros.elementAt(i * 12 + 9);
            saldo = (String)vParametros.elementAt(i * 12 + 10);
            rastrId = (String)vParametros.elementAt(i * 12 + 11);
            descripcion = (String)vParametros.elementAt(i * 12 + 12);

            strCer = strFiller.substring(0, 19) + fecha_oper;

            if (tipTran.equals("D"))
            {
              strCer = strCer + 
                strFiller.substring(0, 21) + monto;
              strCer = strCer + 
                strFiller.substring(0, 13 - monto.length()) + saldo;
              strCer = strCer + 
                strFiller.substring(0, 17 - saldo.length()) + tran_id;
            }

            if (tipTran.equals("C"))
            {
              strCer = strCer + 
                strFiller.substring(0, 6) + monto;

              strCer = strCer + 
                strFiller.substring(0, 27 - monto.length()) + saldo;
              strCer = strCer + 
                strFiller.substring(0, 17 - saldo.length()) + tran_id;
            }

            strCer = strCer + "\n";
            tpImpresora.enviaCadena(strCer);
          }

        }

      }

      if (iDriver == 0) {
        tpImpresora.InserteDocumento();
        tpImpresora.activateControl("SELECCIONASLIP", iDriver);
        tpImpresora.enviaCadena("");
        tpImpresora.enviaCadena("");
        tpImpresora.enviaCadena("");
        tpImpresora.enviaCadena("");
        tpImpresora.enviaCadena("");
        tpImpresora.imprimeCadena1();
        tpImpresora.activateControl("SALTOHOJA", iDriver);
        tpImpresora.activateControl("SELECCIONAJOURNAL", iDriver);
      } else {
        tpImpresora.activateControl("SALTOHOJA", iDriver);
      }
    }

    return intStatus;
  }
}