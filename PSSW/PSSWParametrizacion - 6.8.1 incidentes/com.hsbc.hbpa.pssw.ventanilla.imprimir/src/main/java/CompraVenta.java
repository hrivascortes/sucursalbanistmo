//*************************************************************************************************
//		 Funcion: Clase que certifica Ordenes de Pago
//		Elemento: OrdenesPago.java
//	  Creado por: Juan Carlos Gaona Marcial
//Modificado por: Rodrigo Escand�n Soto
//*************************************************************************************************
//CCN - 4620027	- 24/10/2007 - Se actualiza "WWW.HSBC.COM.MX" por "WWW.HSBC.COM.PA"
//*************************************************************************************************

import java.util.*;

public class CompraVenta {
	ToolsPrinter tpImpresora = new ToolsPrinter();
	int intStatus;
	String strFiller = "                                                  ";
	String strOutLine = "";
	Vector vDatosSuc = new Vector();

	public int imprime4043_0786(
		Vector vDatos,
		String strDatosSuc,
		String strCajero,
		String strDate,
		String strTime,
		int iDriver) {
		intStatus = tpImpresora.verificaStatus();
		if ((intStatus == 95) || (intStatus == 88)) {
			intStatus = 0;
			tpImpresora.activateControl("CERTIFICACION",iDriver);
			String strOrden = (String) vDatos.elementAt(1);
			if (strOrden.equals(""))
				strOrden = " ";
			String strOrdenante = (String) vDatos.elementAt(2);
			if (strOrdenante.equals(""))
				strOrdenante = " ";
			String strImporte = (String) vDatos.elementAt(3);
			if (strImporte.equals(""))
				strImporte = "0.0";
			String strCuenta = (String) vDatos.elementAt(4);
			if (strCuenta.equals(""))
				strCuenta = " ";
			String strBeneficiario = (String) vDatos.elementAt(5);
			if (strBeneficiario.equals(""))
				strBeneficiario = " ";
			vDatosSuc = tpImpresora.Campos(strDatosSuc, "@");
			String strNumSuc = (String) vDatosSuc.elementAt(1);
			strNumSuc = strNumSuc.substring(7, strNumSuc.length());
			String strNomSuc = (String) vDatosSuc.elementAt(0);
			strNomSuc = strNomSuc.substring(7, strNomSuc.length());
			String strDomSuc = (String) vDatosSuc.elementAt(2);
			strDomSuc = strDomSuc.substring(7, strDomSuc.length());
			String strNomPza = (String) vDatosSuc.elementAt(4);
			strNomPza = strNomPza.substring(7, strNomPza.length());
			tpImpresora.enviaCadena(
				"       CARGO EXP. ORDEN DE PAGO S/BCOS. EXTRANJEROS\n");
			strOutLine =
				strNumSuc
					+ "   "
					+ strCajero
					+ "                             "
					+ strDate.substring(7, 9)
					+ "/"
					+ strDate.substring(5, 7)
					+ "/"
					+ strDate.substring(1, 5)
					+ "              "
					+ strTime.substring(0, 2)
					+ ":"
					+ strTime.substring(2, 4)
					+ "\n";
			tpImpresora.enviaCadena(strOutLine);
			strOutLine =
				"Orden   :    "
					+ strFiller.substring(0, 15 - strOrden.length())
					+ strOrden
					+ " Ordenante : "
					+ strOrdenante;
			tpImpresora.enviaCadena(strOutLine);
			strOutLine =
				"Importe :   $"
					+ strFiller.substring(0, 15 - strImporte.length())
					+ strImporte
					+ " Cuenta : "
					+ strCuenta;
			tpImpresora.enviaCadena(strOutLine);
			strOutLine =
				strFiller.substring(0, 28)
					+ " Beneficiario : "
					+ strBeneficiario;
			tpImpresora.enviaCadena(strOutLine);
			strOutLine =
				"Total   :   $"
					+ strFiller.substring(0, 15 - strImporte.length())
					+ strImporte
					+ "\n";
			tpImpresora.enviaCadena(strOutLine);
			/*tpImpresora.enviaCadena(
				"  LA COMISION E IVA APARECERAN EN SU PROXIMO ESTADO DE CUENTA.");
			tpImpresora.enviaCadena(
				"  ESTA ORDEN  SERA  LIQUIDADA AL  BENEFICIARIO AL  INFORMAR EL");
			tpImpresora.enviaCadena(
				"  NUMERO DE ORDEN CON QUE FUE EXPEDIDA, ADEMAS DEBE PRESENTAR ");
			tpImpresora.enviaCadena(
				"  UNA IDENTIFICACION OFICIAL CON FOTOGRAFIA : PASAPORTE, ");
			tpImpresora.enviaCadena(
				"  CARTILLA O CREDENCIAL DE  ELECTOR.\n");*/
			tpImpresora.enviaCadena(
				"                              Ordenante\n\n");
			tpImpresora.enviaCadena(
				"                         ____________________");
			strOutLine =
				strFiller.substring(0, 35 - (strOrdenante.length() / 2))
					+ strOrdenante;
			tpImpresora.enviaCadena(strOutLine);
			if (iDriver == 0) {
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");				
				tpImpresora.imprimeCadena1();
				tpImpresora.activateControl("CORTAPAPEL",iDriver);
			} else
				tpImpresora.activateControl("SALTOHOJA",iDriver);
		}
		return intStatus;
	}

	public int imprimeCompraVentaNueva(
		Vector vDatos,
		String strRespuesta,
		String strDatosSuc,
		String strDate,
		String strCajero,
		int iDriver,
		String strIdentidad,
		String strIdentidad2) {
		intStatus = tpImpresora.verificaStatus();
		Vector vDatosTxn = new Vector();
		if ((intStatus == 95) || (intStatus == 88)) {
			intStatus = 0;
			vDatosSuc = tpImpresora.Campos(strDatosSuc, "@");
			String strNumSuc = (String) vDatosSuc.elementAt(1);
			strNumSuc = strNumSuc.substring(7, strNumSuc.length());
			String strNomSuc = (String) vDatosSuc.elementAt(0);
			strNomSuc = strNomSuc.substring(7, strNomSuc.length());
			String strFormato = (String) vDatos.elementAt(1);
			if (strFormato.startsWith("0108-4117"))
				strRespuesta =
					strRespuesta.substring(
						strRespuesta.indexOf("~4117~") + 1,
						strRespuesta.length());
			vDatosTxn = tpImpresora.Campos(strRespuesta, "~");
			/*if (strFormato.equals("0726-4111")
				|| strFormato.equals("0112-4111")
				|| strFormato.equals("0114-4111")) {
				strNombre = (String) vDatos.elementAt(27);
				for (int i = 0; i < (strNombre.length() / 78); i++) {
					String line =
						strNombre.substring(
							i * 78,
							Math.min(i * 78 + 77, strNombre.length()));
					if (i == 1)
						strNombre = line.trim();
					if (Math.min(i * 78 + 77, strNombre.length())
						== strNombre.length())
						break;
				}
			}*/
			String strCliente = (String) vDatos.elementAt(2);
			if (strCliente.equals(""))
				strCliente = " ";
			String strTipo = (String) vDatos.elementAt(14);
			if (strTipo.equals(""))
				strTipo = "1";
			String strDiv = (String) vDatos.elementAt(15);
			if (strDiv.equals("N$"))
				strDiv = "$";
			String strDiv2 = (String) vDatos.elementAt(16);
			String strImporte1 = "";
			String strImporte2 = "";
			if (strTipo.equals("01")) {
				strImporte1 = (String) vDatos.elementAt(3);
				if (strImporte1.equals(""))
					strImporte1 = "000";
				strImporte2 = (String) vDatos.elementAt(5);
				if (strImporte2.equals(""))
					strImporte2 = "000";
			} else {
				strImporte1 = (String) vDatos.elementAt(5);
				if (strImporte1.equals(""))
					strImporte1 = "000";
				strImporte2 = (String) vDatos.elementAt(3);
				if (strImporte2.equals(""))
					strImporte2 = "000";
			}
			tpImpresora.activateControl("CERTIFICACION",iDriver);
			int intTmp = Integer.parseInt(strImporte1);
			strImporte1 = String.valueOf(intTmp);
			strImporte1 = tpImpresora.formatMonto(strImporte1);
			intTmp = Integer.parseInt(strImporte2);
			strImporte2 = String.valueOf(intTmp);
			strImporte2 = tpImpresora.formatMonto(strImporte2);
			strImporte1 = strImporte1.trim();
			strImporte2 = strImporte2.trim();
			String strTipoCambio = (String) vDatos.elementAt(4);
			if (strTipoCambio.equals(""))
				strTipoCambio = "0.00";
			String strSerial = (String) vDatos.elementAt(6);
			if (strSerial.equals(""))
				strSerial = " ";
			String strCuenta1 = (String) vDatos.elementAt(7);
			if (strCuenta1.equals(""))
				strCuenta1 = " ";
			String strCuenta2 = (String) vDatos.elementAt(8);
			if (strCuenta2.equals(""))
				strCuenta2 = " ";
			String strBanco = (String) vDatos.elementAt(9);
			if (strBanco.equals(""))
				strBanco = " ";
			String strBeneficiario = (String) vDatos.elementAt(10);
			if (strBeneficiario.equals(""))
				strBeneficiario = " ";
			String strComision = (String) vDatos.elementAt(11);
			if (strComision.equals(""))
				strComision = "000";
			String strIva = (String) vDatos.elementAt(12);
			if (strIva.equals(""))
				strIva = "000";
			String strTotal = (String) vDatos.elementAt(13);
			if (strTotal.equals(""))
				strTotal = "000";
			/*if (vDatos.size() > 15) {
				strComision = (String) vDatos.elementAt(15);
				if (strComision.equals(""))
					strComision = "000";
				strIva = (String) vDatos.elementAt(16);
				if (strIva.equals(""))
					strIva = "000";
				strTotal = (String) vDatos.elementAt(17);
				if (strTotal.equals(""))
					strTotal = "000";
			}*/
			tpImpresora.enviaCadena("");
			strOutLine = "SUCURSAL : " + strNumSuc + "-" + strNomSuc;
			tpImpresora.enviaCadena(strOutLine);
			tpImpresora.enviaCadena(" ");
			strOutLine =
				strDate.substring(7, 9)
					+ "-"
					+ strDate.substring(5, 7)
					+ "-"
					+ strDate.substring(1, 5)
					+ " "
					+ strCajero;
			tpImpresora.enviaCadena(strOutLine);
			tpImpresora.enviaCadena(" ");
			if (strFormato.equals(""))
				strFormato = " ";
			/*if (strFormato.equals("0726-0786"))
			{
				imprime0726_0786(vDatos, strDatosSuc, strDate, strTime, strCajero, intWindows);
				tpImpresora.muestraVentana();				
			}*/
			if (strTipo.equals("01"))
				tpImpresora.enviaCadena(
					"           VENTA DE DIVISAS");
			else
				tpImpresora.enviaCadena(
					"           COMPRA DE DIVISAS");
			if (strFormato.equals("0108-4117"))
				tpImpresora.enviaCadena(
					"                COBRO INMEDIATO  --  ABONO A CUENTA");
			else if (strFormato.equals("0112-4111")|| strFormato.equals("0128-4101"))
				tpImpresora.enviaCadena(
					"            LIQUIDACION ORDEN DE PAGO --  ABONO A CUENTA");
			else if (strFormato.equals("0114-4111")||strFormato.equals("0130-4101"))
				tpImpresora.enviaCadena(
					"            CANCELACION ORDEN DE PAGO --  ABONO A CUENTA");
			else if (strFormato.equals("0726-0106"))
				tpImpresora.enviaCadena(
					"         EFECTIVO -- EFECTIVO");
			/*else if (strFormato.equals("0726-0786"))
				tpImpresora.enviaCadena(" EFECTIVO RECIBIDO PARA EXP. DE ORDEN DE PAGO" + strTipoMov);*/
			else if (strFormato.equals("0726-4111"))
				tpImpresora.enviaCadena(
					"      EFECTIVO -- ABONO A CUENTA");
			else if (strFormato.equals("4043-0106"))
				tpImpresora.enviaCadena(
					"      CARGO A CUENTA -- EFECTIVO");
			else if (strFormato.equals("4043-4111"))
				tpImpresora.enviaCadena(
					"    CARGO A CUENTA -- ABONO A CUENTA");
			/*else if (strFormato.equals("4057-0106"))
				tpImpresora.enviaCadena("                     CHEQUE DE CAJA -- EFECTIVO");
			else if (strFormato.equals("4057-4111"))
				tpImpresora.enviaCadena("                  CHEQUE DE CAJA -- ABONO A CUENTA");
			else if (strFormato.equals("4063-4111"))
				tpImpresora.enviaCadena("            CHEQUE DE CAJA CANCELADO --  ABONO A CUENTA");
			else if (strFormato.equals("4063-0106"))
				tpImpresora.enviaCadena("               CHEQUE DE CAJA CANCELADO --  EFECTIVO");*/
			tpImpresora.enviaCadena("");
			strOutLine = "   TXN    CONSEC";
			tpImpresora.enviaCadena(strOutLine);
			for (int veces = 0; veces < (vDatosTxn.size() / 2); veces++) {
				strOutLine =
					"   "
						+ (String) vDatosTxn.elementAt(veces * 2)
						+ "   "
						+ (String) vDatosTxn.elementAt(veces * 2 + 1);
				tpImpresora.enviaCadena(strOutLine);
			}
			/*String strSigno1 = new String("");
			String strSigno2 = new String("");
			if (strTipo.equals("01")) {
				strSigno1 = "MN ";
				strSigno2 = "USD";
			} else {

				strSigno1 = "USD";
				strSigno2 = "MN ";
			}*/
			tpImpresora.enviaCadena("");
			strOutLine =
				"            DIVISA : " + strDiv + "  ---  " + strDiv2;
			tpImpresora.enviaCadena(strOutLine);
			if (!strFormato.startsWith("0726")) {
				strOutLine = "    NOMBRE CLIENTE : " + strCliente;
				tpImpresora.enviaCadena(strOutLine);
			}
			/*if(strFormato.startsWith("0108"))
			{
				strOutLine = "            SERIAL : " + strSerial;
				tpImpresora.enviaCadena(strOutLine);
				strOutLine = "             BANCO : " + strBanco;
				tpImpresora.enviaCadena(strOutLine);
			}*/
			else if (strFormato.startsWith("0112"))
				tpImpresora.enviaCadena("     ORDEN DE PAGO : " + strSerial);
			else if (strFormato.startsWith("0114"))
				tpImpresora.enviaCadena("     ORDEN DE PAGO : " + strSerial);
			/*else if(strFormato.startsWith("4057"))
				tpImpresora.enviaCadena("    CHEQUE DE CAJA : " + strSerial);
			else if(strFormato.startsWith("4059"))
				tpImpresora.enviaCadena("SERIAL DEL CHEQUE : " + strSerial);
			else if(strFormato.startsWith("4063"))
				tpImpresora.enviaCadena("CHQ CAJA CANCELADO : " + strSerial);*/
			strOutLine =
				"    MONTO RECIBIDO : "
					+ strDiv
					//+ strFiller.substring(0, 18 - strImporte1.length())
					+"  "
					+ strImporte1;
			tpImpresora.enviaCadena(strOutLine);
			if (strFormato.startsWith("4043")) {
				strOutLine = "   CUENTA DE CARGO : " + strCuenta1;
				tpImpresora.enviaCadena(strOutLine);
			}
			strOutLine =
				"   MONTO ENTREGADO : "
					+ strDiv2
					//+ strFiller.substring(0, 18 - strImporte2.length())
					+"  "
					+ strImporte2;
			tpImpresora.enviaCadena(strOutLine);
			if (strFormato.equals("0108-4117")
				|| strFormato.equals("4057-4111")
				|| strFormato.equals("4043-4113")
				|| strFormato.equals("0112-4111")
				|| strFormato.equals("0128-4101")
				|| strFormato.equals("4057-4113")
				|| strFormato.equals("4059-4113")
				|| strFormato.equals("4063-4111")
				|| strFormato.equals("4059-4111")
				|| strFormato.equals("0114-4111")
				|| strFormato.equals("0130-4101")
				|| strFormato.equals("4043-4111")
				|| strFormato.equals("0726-4111")) {

				/*if (!strFormato.equals("0108-4117")) {
					strOutLine = "            NOMBRE : " + strNombre;
					tpImpresora.enviaCadena(strOutLine);
				}*/
				strOutLine = "   CUENTA DE ABONO : " + strCuenta2;
				tpImpresora.enviaCadena(strOutLine);
			}
			strOutLine = "    TIPO DE CAMBIO : " + strTipoCambio;
			tpImpresora.enviaCadena(strOutLine);
			if (!strFormato.startsWith("0726")) {
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena(
					"               TITULAR");
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena(
					"         RECIBI DE CONFORMIDAD");
			}
			tpImpresora.enviaCadena("");
			tpImpresora.enviaCadena("");
			tpImpresora.enviaCadena("");
			tpImpresora.enviaCadena("");
			tpImpresora.enviaCadena("");
			tpImpresora.enviaCadena("");
			tpImpresora.enviaCadena(
				"            SELLO Y FIRMA");
			strOutLine = "            CAJERO " + strCajero;
			tpImpresora.enviaCadena(strOutLine);
			tpImpresora.enviaCadena("");
			tpImpresora.enviaCadena("");
			tpImpresora.enviaCadena("");
			tpImpresora.enviaCadena(
				"               "+strIdentidad2+"    ");
			if (iDriver == 0) {
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");				
				tpImpresora.imprimeCadena1();
				tpImpresora.activateControl("CORTAPAPEL",iDriver);
			} else
				tpImpresora.activateControl("SALTOHOJA",iDriver);
		}
		return intStatus;
	}

	public int imprimeCompraVenta     (Vector vDatos, String strDatosSuc, String strDate, String strTime, String strCajero, int iDriver, String strIdentidad)	
   {
		intStatus = tpImpresora.verificaStatus();
		if ((intStatus == 95) || (intStatus == 88))
		{
			intStatus = 0;
         String strFormato = (String)vDatos.elementAt(1);
         if (strFormato.equals(""))
         	strFormato = " ";
			if (strFormato.equals("0726-0786"))
			{
				imprime0726_0786(vDatos, strDatosSuc, strDate, strTime, strCajero, iDriver);
				tpImpresora.muestraVentana();				
			}			         	
			tpImpresora.activateControl("CERTIFICACION",iDriver);			
         String strCliente = (String)vDatos.elementAt(2);
         if (strCliente.equals(""))
         	strCliente = " ";
         String strTipo = (String)vDatos.elementAt(14);
         if (strTipo.equals(""))
         	strTipo = "1";         
         String strImporte1 = "";
			String strImporte2 = "";
         if (strTipo.equals("01"))         
         {
         	strImporte1 = (String)vDatos.elementAt(3);
         	if (strImporte1.equals(""))
         		strImporte1 = "000";
	         strImporte2 = (String)vDatos.elementAt(5);
   	      if (strImporte2.equals(""))
      	   	strImporte2 = "000";         	
         }
         else
         {
         	strImporte1 = (String)vDatos.elementAt(5);
         	if (strImporte1.equals(""))
         		strImporte1 = "000";
	         strImporte2 = (String)vDatos.elementAt(3);
   	      if (strImporte2.equals(""))
      	   	strImporte2 = "000";         	
         }
         int intTmp = Integer.parseInt(strImporte1);
	      strImporte1 = String.valueOf(intTmp);
			strImporte1 = tpImpresora.formatMonto(strImporte1);                           
         intTmp = Integer.parseInt(strImporte2);
	      strImporte2 = String.valueOf(intTmp);
			strImporte2 = tpImpresora.formatMonto(strImporte2);         		         
         String strTipoCambio = (String)vDatos.elementAt(4);
         if (strTipoCambio.equals(""))
         	strTipoCambio = "0.00";
         String strSerial = (String)vDatos.elementAt(6);
         if (strSerial.equals(""))
         	strSerial = " ";
         String strCuenta1 = (String)vDatos.elementAt(7);
         if (strCuenta1.equals(""))
         	strCuenta1 = " ";
         String strCuenta2 = (String)vDatos.elementAt(8);
         if (strCuenta2.equals(""))
         	strCuenta2 = " ";
         String strBanco = (String)vDatos.elementAt(9);
         if (strBanco.equals(""))
         	strBanco = " ";         
         String strBeneficiario = (String)vDatos.elementAt(10);
         if (strBeneficiario.equals(""))
         	strBeneficiario = " ";         
         String strComision = (String)vDatos.elementAt(11);
         if (strComision.equals(""))
         	strComision = "000";         
         String strIva = (String)vDatos.elementAt(12);
         if (strIva.equals(""))
         	strIva = "000";         
         String strTotal = (String)vDatos.elementAt(13);
         if (strTotal.equals(""))
         	strTotal = "000";         
			if (vDatos.size() > 15)         
			{
	         strComision = (String)vDatos.elementAt(15);
   	      if (strComision.equals(""))
      	   	strComision = "000";         				
         	strIva = (String)vDatos.elementAt(16);
         	if (strIva.equals(""))
         		strIva = "000";         
         	strTotal = (String)vDatos.elementAt(17);
         	if (strTotal.equals(""))
         		strTotal = "000";         				
			}
			vDatosSuc = tpImpresora.Campos(strDatosSuc, "@");
			String strNumSuc = (String) vDatosSuc.elementAt(1);
			strNumSuc = strNumSuc.substring(7,strNumSuc.length());         
			String strNomSuc = (String) vDatosSuc.elementAt(0);
			strNomSuc = strNomSuc.substring(7,strNomSuc.length());         
			String strDomSuc = (String) vDatosSuc.elementAt(2);
			strDomSuc = strDomSuc.substring(7,strDomSuc.length());
			String strNomPza = (String) vDatosSuc.elementAt(4);
			strNomPza = strNomPza.substring(7,strNomPza.length());
			if (strFormato.equals("0108-4117"))
				tpImpresora.enviaCadena("     COBRO INMEDIATO PARA ABONO A CUENTA DE CHEQUES POR COMPRA/VENTA");							
			else if (strFormato.equals("0112-4111")||strFormato.equals("0128-4101"))
				tpImpresora.enviaCadena("       ORDEN DE PAGO P/ABONO A CUENTA DE CHEQUES POR COMPRA/VENTA");								
			else if (strFormato.equals("0114-4111")||strFormato.equals("0130-4101"))
				tpImpresora.enviaCadena("     CANC. ORD. DE PAGO P/ABONO A CTA. DE CHEQUES POR COMPRA/VENTA");				
			else if (strFormato.equals("0726-0106"))
				tpImpresora.enviaCadena("        EFECTIVO RECIBIDO P/ENTREGA DE EFECTIVO POR COMPRA/VENTA");			
			else if (strFormato.equals("0726-0786"))
				tpImpresora.enviaCadena("       EFECTIVO RECIBIDO P/EXP. DE ORDEN DE PAGO POR COMPRA/VENTA");
         else if (strFormato.equals("0726-4111"))
				tpImpresora.enviaCadena("     EFECTIVO RECIBIDO P/ABONO A CUENTA DE CHEQUES POR COMPRA/VENTA");
			else if (strFormato.equals("0726-4113"))
				tpImpresora.enviaCadena("    EFECTIVO RECIBIDO P/ABONO A CUENTA DE INVERSIONES POR COMPRA/VENTA");				
			else if (strFormato.equals("0726-4115"))
				tpImpresora.enviaCadena("       EFECTIVO RECIBIDO P/EXP DE CHEQUE DE CAJA POR COMPRA/VENTA");				
			else if (strFormato.equals("4043-0106"))
				tpImpresora.enviaCadena("            CARGO PARA ENTREGA DE EFECTIVO POR COMPRA/VENTA");
			else if (strFormato.equals("4043-4111"))
				tpImpresora.enviaCadena("          CARGO PARA ABONO A CUENTA DE CHEQUES POR COMPRA/VENTA");
			else if (strFormato.equals("4043-4113"))
				tpImpresora.enviaCadena("        CARGO PARA ABONO A CUENTA DE INVERSIONES POR COMPRA/VENTA");
			else if (strFormato.equals("4043-4115"))
				tpImpresora.enviaCadena("         CARGO POR EXPEDICION DE CHEQUE DE CAJA POR COMPRA/VENTA");
			else if (strFormato.equals("4057-0106"))
				tpImpresora.enviaCadena("         CHEQUE DE CAJA PARA ENTREGA DE EFECTIVO POR COMPRA/VENTA");
			else if (strFormato.equals("4057-4111"))
				tpImpresora.enviaCadena("         CHQ. DE CAJA P/ABONO A CTA. DE CHEQUES POR COMPRA/VENTA");
			else if (strFormato.equals("4057-4113"))
				tpImpresora.enviaCadena("       CHQ. DE CAJA P/ABONO A CTA. DE INVERSIONES POR COMPRA/VENTA");
			else if (strFormato.equals("4059-0106"))
				tpImpresora.enviaCadena("        CHQ. PERSONAL PARA ENTREGA DE EFECTIVO POR COMPRA/VENTA");
			else if (strFormato.equals("4059-4111"))
				tpImpresora.enviaCadena("        CHQ. PERSONAL P/ABONO A CTA. DE CHEQUES POR COMPRA/VENTA");
			else if (strFormato.equals("4059-4113"))
				tpImpresora.enviaCadena("      CHQ. PERSONAL P/ABONO A CTA. DE INVERSIONES POR COMPRA/VENTA");
			else if (strFormato.equals("4063-4111"))
				tpImpresora.enviaCadena("      CANC. CHQ.DE CAJA P/ABONO A CTA. DE CHEQUES POR COMPRA/VENTA");
			else if (strFormato.equals("4063-4113"))
				tpImpresora.enviaCadena("    CANC. CHQ.DE CAJA P/ABONO A CTA. DE INVERSIONES POR COMPRA/VENTA");
			else if (strFormato.equals("4063-0106"))
				tpImpresora.enviaCadena("      CANC. CHQ.DE CAJA PARA ENTREGA DE EFECTIVO POR COMPRA/VENTA");
			tpImpresora.enviaCadena("\n");
			if(strFormato.startsWith("4043"))
			{
				tpImpresora.enviaCadena("CIUDAD Y FECHA                           NUMERO DE CUENTA DE CARGO");
				strOutLine = strNomPza + "  " +  strDate.substring(7,9) + "-" + strDate.substring(5,7) + "-" + strDate.substring(1,5);
				strOutLine = strOutLine + strFiller.substring(0,50-strOutLine.length()) + strCuenta1;
			}
			else
			{
				tpImpresora.enviaCadena("CIUDAD Y FECHA");
				strOutLine = strNomPza + "  " +  strDate.substring(7,9) + "-" + strDate.substring(5,7) + "-" + strDate.substring(1,5);
			}
			tpImpresora.enviaCadena(strOutLine);
			if(strFormato.startsWith("0108"))
				tpImpresora.enviaCadena("NOMBRE DEL CLIENTE                       MONTO DEL DOCUMENTO");			
			else if(strFormato.startsWith("0112"))
				tpImpresora.enviaCadena("NOMBRE DEL CLIENTE                       MONTO DE LA ORDEN DE PAGO");			
			else if(strFormato.startsWith("0114"))
				tpImpresora.enviaCadena("NOMBRE DEL CLIENTE                       MONTO DE ORDEN DE PAGO CANC.");								
			else if(strFormato.startsWith("0726"))
				tpImpresora.enviaCadena("NOMBRE DEL CLIENTE                       MONTO DEL EFECTIVO RECIBIDO");
			else if(strFormato.startsWith("4043"))
				tpImpresora.enviaCadena("NOMBRE DEL CLIENTE                       MONTO DEL CARGO A CUENTA");
			else if(strFormato.startsWith("4057"))
				tpImpresora.enviaCadena("NOMBRE DEL CLIENTE                       MONTO DEL CHEQUE DE CAJA");
			else if(strFormato.startsWith("4059"))
				tpImpresora.enviaCadena("NOMBRE DEL CLIENTE                       MONTO DEL CHEQUE");
			else if(strFormato.startsWith("4063"))
				tpImpresora.enviaCadena("NOMBRE DEL CLIENTE                       MONTO DEL CHQ. CAJA CANCELADO");
			if (strTipo.equals("01"))
				strOutLine = strCliente + strFiller.substring(0,40-strCliente.length()) + " $" + strFiller.substring(0,20-strImporte1.length()) + strImporte1;
			else
				strOutLine = strCliente + strFiller.substring(0,40-strCliente.length()) + " $" + strFiller.substring(0,20-strImporte1.length()) + strImporte1;
			tpImpresora.enviaCadena(strOutLine);			
			tpImpresora.enviaCadena("MONTO EN LETRA");			
			if (strTipo.equals("01"))
				strOutLine = Currency.convert(strImporte1.trim(),1);
			else
				strOutLine = Currency.convert(strImporte1.trim(),2);
			tpImpresora.enviaCadena(strOutLine);			
			if (strFormato.equals("0726-4115") ||strFormato.equals("0726-0786") )			
			{
				strComision = tpImpresora.formatMonto(strComision);
				strIva = tpImpresora.formatMonto(strIva);
				strTotal = tpImpresora.formatMonto(strTotal);
				tpImpresora.enviaCadena("   COMISION        I.V.A.        TOTAL DEL EFECTIVO RECIBIDO");			
				strOutLine = "$" + strFiller.substring(0,15-strComision.length())  + strComision + "  $" + strFiller.substring(0,15-strIva.length()) + strIva + "  $" + strFiller.substring(0,20-strTotal.length()) + strTotal;
				tpImpresora.enviaCadena(strOutLine);			
			}			
			if(strFormato.startsWith("0108"))
				tpImpresora.enviaCadena("SERIAL : " + strSerial + "                         BANCO " + strBanco);			
			else if(strFormato.startsWith("0112"))
				tpImpresora.enviaCadena("SERIAL DE ORDEN DE PAGO : " + strSerial);
			else if(strFormato.startsWith("0114"))
				tpImpresora.enviaCadena("SERIAL DE ORDEN DE PAGO CANC. : " + strSerial);				
			else if(strFormato.startsWith("4057"))
				tpImpresora.enviaCadena("SERIAL DEL CHEQUE DE CAJA : " + strSerial);
			else if(strFormato.startsWith("4059"))
				tpImpresora.enviaCadena("SERIAL DEL CHEQUE : " + strSerial);
			else if(strFormato.startsWith("4063"))
				tpImpresora.enviaCadena("SERIAL DEL CHQ CAJA CANCELADO: " + strSerial);
			if ((strFormato.equals("4043-0106")) || (strFormato.equals("0726-0106"))|| (strFormato.equals("4063-0106"))|| 
			    (strFormato.equals("4057-0106"))|| (strFormato.equals("4059-0106")))
				tpImpresora.enviaCadena("TIPO DE CAMBIO                           MONTO DEL EFECTIVO ENTREGADO");
			else if (strFormato.equals("4043-4115"))
					tpImpresora.enviaCadena("TIPO DE CAMBIO                      MONTO DEL CHEQUE DE CAJA EXP.");			
			else if (strFormato.equals("0726-0786"))
					tpImpresora.enviaCadena("TIPO DE CAMBIO                      MONTO DE LA ORDEN DE PAGO EXP.");			
			else if (strFormato.equals("0726-4115"))
					tpImpresora.enviaCadena("TIPO DE CAMBIO                        MONTO DEL CHEQUE DE CAJA ");			
			else
				tpImpresora.enviaCadena("TIPO DE CAMBIO                           MONTO DEL ABONO A CUENTA");			
			if (strTipo.equals("01"))
				strOutLine = "  " + strTipoCambio + strFiller.substring(0,39-strTipoCambio.length()) + "  $" + strFiller.substring(0,20-strImporte2.length()) + strImporte2;
			else
				strOutLine = "  " + strTipoCambio + strFiller.substring(0,39-strTipoCambio.length()) + "  $" + strFiller.substring(0,20-strImporte2.length()) + strImporte2;
			tpImpresora.enviaCadena(strOutLine);			
			tpImpresora.enviaCadena("MONTO EN LETRA");			
			if (strTipo.equals("01"))
				strOutLine = Currency.convert(strImporte2.trim(),2);
			else
				strOutLine = Currency.convert(strImporte2.trim(),1);			
			tpImpresora.enviaCadena(strOutLine);
			if ( strFormato.equals("0108-4117") || strFormato.equals("4057-4111") || strFormato.equals("4043-4113") || strFormato.equals("0112-4111")|| strFormato.equals("0128-4101") ||
			     strFormato.equals("4057-4113") || strFormato.equals("4059-4113") || strFormato.equals("4063-4111") ||
				  strFormato.equals("4059-4111") || strFormato.equals("0114-4111") || strFormato.equals("0130-4101") || strFormato.equals("4043-4111") || strFormato.equals("0726-4111"))				 
			{
				strOutLine = "NUMERO DE CUENTA DE ABONO : " + strCuenta2;
				tpImpresora.enviaCadena(strOutLine);
			}			
			if (strFormato.equals("4043-4115") || strFormato.equals("0726-4115")|| strFormato.equals("0726-0786"))
			{
				if (strFormato.equals("0726-0786"))
					strOutLine = "SERIAL DE LA ORDEN DE PAGO : " + strSerial;							
				else
					strOutLine = "SERIAL DEL CHEQUE DE CAJA : " + strSerial;			
				tpImpresora.enviaCadena( strOutLine );							
				strOutLine = "BENEFICIARIO : " + strBeneficiario;			
				tpImpresora.enviaCadena( strOutLine );											
			}
         intTmp = Integer.parseInt(strNumSuc);         
			strOutLine = "SUCURSAL  " + String.valueOf(intTmp) + "  " + strNomSuc;
			tpImpresora.enviaCadena(strOutLine);
			tpImpresora.enviaCadena("\n");
			tpImpresora.enviaCadena("          TITULAR                                "+strIdentidad+" PANAMA        \n\n");			//IMAGEN
			tpImpresora.enviaCadena("   _____________________                  _______________________   ");			
			tpImpresora.enviaCadena("   RECIBI DE CONFORMIDAD                  AUTORIZO NOMBRE Y FIRMA   ");			
			tpImpresora.activateControl("SALTOHOJA",iDriver);
		}
		return intStatus;
	}
	
	public int imprime0726_0786(
		Vector vDatos,
		String strDatosSuc,
		String strDate,
		String strTime,
		String strCajero,
		int iDriver) {
		intStatus = tpImpresora.verificaStatus();
		if ((intStatus == 95) || (intStatus == 88)) {
			intStatus = 0;
			tpImpresora.activateControl("CERTIFICACION",iDriver);
			String strOrden = (String) vDatos.elementAt(6);
			if (strOrden.equals(""))
				strOrden = "0000000000";
			String strImporte1 = (String) vDatos.elementAt(5);
			if (strImporte1.equals(""))
				strImporte1 = "000";
			strImporte1 = tpImpresora.formatMonto(strImporte1);
			String strComision1 = (String) vDatos.elementAt(11);
			if (strComision1.equals(""))
				strComision1 = "000";
			strComision1 = tpImpresora.formatMonto(strComision1);
			String strIva1 = (String) vDatos.elementAt(12);
			if (strIva1.equals(""))
				strIva1 = "0.00";
			strIva1 = tpImpresora.formatMonto(strIva1);
			String strTotal1 = (String) vDatos.elementAt(13);
			if (strTotal1.equals(""))
				strTotal1 = "000";
			strTotal1 = tpImpresora.formatMonto(strTotal1);
			String strOrdenante = (String) vDatos.elementAt(2);
			if (strOrdenante.equals(""))
				strOrdenante = " ";
			else
				strOrdenante = strOrdenante.trim();
			String strBeneficiario = (String) vDatos.elementAt(10);
			if (strBeneficiario.equals(""))
				strBeneficiario = " ";
			vDatosSuc = tpImpresora.Campos(strDatosSuc, "@");
			String strNumSuc = (String) vDatosSuc.elementAt(1);
			strNumSuc = strNumSuc.substring(7, strNumSuc.length());
			String strNomSuc = (String) vDatosSuc.elementAt(0);
			strNomSuc = strNomSuc.substring(7, strNomSuc.length());
			String strDomSuc = (String) vDatosSuc.elementAt(2);
			strDomSuc = strDomSuc.substring(7, strDomSuc.length());
			String strNomPza = (String) vDatosSuc.elementAt(4);
			strNomPza = strNomPza.substring(7, strNomPza.length());
			tpImpresora.enviaCadena(
				"     EFECTIVO EXP. ORDEN DE PAGO S/BCOS. EXTRANJEROS\n");
			strOutLine =
				strNumSuc
					+ "   "
					+ strCajero
					+ "                             "
					+ strDate.substring(7, 9)
					+ "/"
					+ strDate.substring(5, 7)
					+ "/"
					+ strDate.substring(1, 5)
					+ "              "
					+ strTime.substring(0, 2)
					+ ":"
					+ strTime.substring(2, 4)
					+ "\n";
			tpImpresora.enviaCadena(strOutLine);
			if (strOrdenante.length() > 25) {
				strOutLine =
					"Orden    : "
						+ strOrden
						+ "             Ordenante : "
						+ strOrdenante.substring(0, 25);
				tpImpresora.enviaCadena(strOutLine);
				strOutLine =
					strFiller.substring(0, 46)
						+ strOrdenante.substring(25, strOrdenante.length());
				tpImpresora.enviaCadena(strOutLine);
			} else {
				strOutLine =
					"Orden    : "
						+ strOrden
						+ "             Ordenante : "
						+ strOrdenante;
				tpImpresora.enviaCadena(strOutLine);
			}
			strOutLine =
				"Importe  :    $"
					+ strFiller.substring(0, 15 - strImporte1.length())
					+ strImporte1;
			tpImpresora.enviaCadena(strOutLine);
			if (strBeneficiario.length() > 25) {
				strOutLine =
					"Comision :    $"
						+ strFiller.substring(0, 15 - strComision1.length())
						+ strComision1
						+ " Beneficiario : "
						+ strBeneficiario.substring(0, 25);
				tpImpresora.enviaCadena(strOutLine);
				strOutLine =
					strFiller.substring(0, 46)
						+ strBeneficiario.substring(25, strBeneficiario.length());
				tpImpresora.enviaCadena(strOutLine);
			} else {
				strOutLine =
					"Comision :    $"
						+ strFiller.substring(0, 15 - strComision1.length())
						+ strComision1
						+ " Beneficiario : "
						+ strBeneficiario;
				tpImpresora.enviaCadena(strOutLine);
			}
			strOutLine =
				"Iva      :    $"
					+ strFiller.substring(0, 15 - strIva1.length())
					+ strIva1;
			tpImpresora.enviaCadena(strOutLine);
			strOutLine =
				"Total    :    $"
					+ strFiller.substring(0, 15 - strTotal1.length())
					+ strTotal1
					+ "\n";
			tpImpresora.enviaCadena(strOutLine);
			/*tpImpresora.enviaCadena(
				"ESTA ORDEN SERA LIQUIDADA  AL BENEFICIARIO AL INFORMAR EL NUMERO DE ORDEN");
			tpImpresora.enviaCadena(
				"CON QUE FUE EXPEDIDA,  ADEMAS DE PRESENTAR UNA IDENTIFICACION OFICIAL CON");
			tpImpresora.enviaCadena(
				"FOTOGRAFIA:  PASAPORTE, CARTILLA O CREDENCIAL DE ELECTOR (ESTA LEYENDA NO");
			tpImpresora.enviaCadena(
				"APLICA PARA ORDENES DE PAGO AL EXTRANJERO).\n");*/
			tpImpresora.enviaCadena(
				"                              Ordenante\n\n");
			tpImpresora.enviaCadena(
				"                         ____________________");
			strOutLine =
				strFiller.substring(0, 35 - (strOrdenante.length() / 2))
					+ strOrdenante;
			tpImpresora.enviaCadena(strOutLine);
			if (iDriver == 0) {
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");				
				tpImpresora.imprimeCadena1();
				tpImpresora.activateControl("CORTAPAPEL",iDriver);
			} else
				tpImpresora.activateControl("SALTOHOJA",iDriver);
		}
		return intStatus;
	}
}