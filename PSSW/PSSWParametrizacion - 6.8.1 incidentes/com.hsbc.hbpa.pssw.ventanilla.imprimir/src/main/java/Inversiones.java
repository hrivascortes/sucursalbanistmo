//*************************************************************************************************
//		 Funcion: Clase que certifica Ordenes de Pago
//		Elemento: OrdenesPago.java
//	  Creado por: Juan Carlos Gaona Marcial
//Modificado por: Rodrigo Escand�n Soto
//*************************************************************************************************
//CCN - 4620027	- 24/10/2007 - Se actualiza "WWW..COM.MX" por "WWW..COM.PA"
//*************************************************************************************************

import java.util.*;
import java.io.*;

public class Inversiones {
	String strFiller = "                                                  ";
	String strOut = "";
	ToolsPrinter tpImpresora = new ToolsPrinter();
	int intStatus = 0;

	private void CodigoBarras2de5320(String sDato) {
		try {
			char[] confcode = new char[16];
			confcode[0] = (char) 27; //ESC
			confcode[1] = (char) 16; //DLE
			confcode[2] = (char) 65; //A
			confcode[3] = (char) 8; //m
			confcode[4] = (char) 3; //n1
			confcode[5] = (char) 0; //n2
			confcode[6] = (char) 0; //n3
			confcode[7] = (char) 3; //n4
			confcode[8] = (char) 1; //n5
			confcode[9] = (char) 1; //n6
			confcode[10] = (char) 1; //n7
			confcode[11] = (char) 0; //n8
			confcode[12] = (char) 27; //ESC
			confcode[13] = (char) 16; //DLE
			confcode[14] = (char) 66; //B
			confcode[15] = (char) 24; //m 34			
			FileOutputStream os = new FileOutputStream("LPT1:");
			PrintStream ps = new PrintStream(os);
			ps.print(confcode);
			ps.println(sDato);
			ps.close();
		} catch (Exception e) {
			System.out.println("Error: " + e.getMessage());
		}
	}

	/*public int imprimeFichaRetiroAnterior(Vector vDatos, String strDate) {
		intStatus = tpImpresora.verificaStatus();
		if ((intStatus == 95) || (intStatus == 88)) {
			String strNombre = (String) vDatos.elementAt(1);
			String strInversion = (String) vDatos.elementAt(2);
			String strVencimiento =
				strDate.substring(7, 9)
					+ strDate.substring(5, 7)
					+ strDate.substring(1, 5);
			String strMonto = (String) vDatos.elementAt(3);
			strMonto = tpImpresora.formatMonto(strMonto);
			String strMoneda = (String) vDatos.elementAt(4);
			tpImpresora.activateControl("NOMINA");
			intStatus = 0;
			strOut = "                     FICHA DE RETIRO DE INVERSION\n";
			tpImpresora.enviaCadena(strOut);
			strOut =
				"Nombre : "
					+ strNombre
					+ strFiller.substring(0, 35 - strNombre.length())
					+ "No. INVERSION: "
					+ strInversion
					+ "\n";
			tpImpresora.enviaCadena(strOut);
			strOut =
				strFiller.substring(0, 44)
					+ "FECHA DE OPERACION: "
					+ strDate.substring(7, 9)
					+ strDate.substring(5, 7)
					+ strDate.substring(1, 5)
					+ "\n";
			tpImpresora.enviaCadena(strOut);
			strOut =
				"FECHA DE VENCIMIENTO : "
					+ strVencimiento
					+ "             MONTO A RETIRAR : "
					+ strMonto
					+ "\n\n";
			tpImpresora.enviaCadena(strOut);
			strOut =
				"MONTO EN LETRA : "
					+ Currency.convert(strMonto, Integer.parseInt(strMoneda))
					+ "\n\n";
			tpImpresora.enviaCadena(strOut);
			tpImpresora.enviaCadena(
				"          ____________________                  ____________________          ");
			tpImpresora.enviaCadena(
				"          TITULAR DE LA CUENTA                  FIRMA DE AUTORIZACION         ");
			tpImpresora.activateControl("SALTOHOJA");
		}
		return intStatus;
	}*/

	public int imprimeFichaRetiro(
		Vector vDatos,
		String strRespuesta,
		String strDate,
		String strDatosSuc,
		String strCajero,
		String strConsecutivo,
		int iDriver,
		String strIdentidad,
		String strIdentidad2) {
		intStatus = tpImpresora.verificaStatus();
		if ((intStatus == 95) || (intStatus == 88)) {
			tpImpresora.activateControl("CERTIFICACION",iDriver);
			Vector vDatosTxn = tpImpresora.Campos(strRespuesta, "~");
			Vector vDatosSuc = tpImpresora.Campos(strDatosSuc, "@");
			String strNumSuc = (String) vDatosSuc.elementAt(1);
			strNumSuc = strNumSuc.substring(7, strNumSuc.length());
			String strNomSuc = (String) vDatosSuc.elementAt(0);
			strNomSuc = strNomSuc.substring(7, strNomSuc.length());
			String strOutLine = "SUCURSAL : " + strNumSuc + "-" + strNomSuc;
			tpImpresora.enviaCadena(strOutLine);
			tpImpresora.enviaCadena(" ");
			strOutLine =
				strDate.substring(7, 9)
					+ "-"
					+ strDate.substring(5, 7)
					+ "-"
					+ strDate.substring(1, 5)
					+ " "
					+ strCajero;
			tpImpresora.enviaCadena(strOutLine);
			tpImpresora.enviaCadena(" ");
			String strNombre = (String) vDatos.elementAt(1);
			String strInversion = (String) vDatos.elementAt(2);
			String strVencimiento =
				strDate.substring(7, 9)
					+ strDate.substring(5, 7)
					+ strDate.substring(1, 5);
			String strMonto = (String) vDatos.elementAt(3);
			strMonto = tpImpresora.formatMonto(strMonto);
			intStatus = 0;
			strOutLine = "     FICHA DE RETIRO DE INVERSION";
			tpImpresora.enviaCadena(strOutLine);
			tpImpresora.enviaCadena("");
			strOutLine = "   TXN    CONSEC";
			tpImpresora.enviaCadena(strOutLine);
			for (int veces = 0; veces < (vDatosTxn.size() / 2); veces++) {
				strOutLine =
					"   "
						+ (String) vDatosTxn.elementAt(veces * 2)
						+ "   "
						+ (String) vDatosTxn.elementAt(veces * 2 + 1);
				tpImpresora.enviaCadena(strOutLine);
			}
			tpImpresora.enviaCadena("");
			if(strNombre.length()>13){
				strOutLine = "   NOMBRE : " + strNombre;
				tpImpresora.enviaCadena(strOutLine);						
			}
			else{
				strOutLine = "             NOMBRE : " + strNombre;
				tpImpresora.enviaCadena(strOutLine);			
			}
			strOutLine = "          INVERSION : " + strInversion;
			tpImpresora.enviaCadena(strOutLine);
			strOutLine = "              MONTO : " + strMonto;
			tpImpresora.enviaCadena(strOutLine);
			strOutLine =
				" FECHA DE OPERACION : "
					+ strDate.substring(7, 9)
					+ strDate.substring(5, 7)
					+ strDate.substring(1, 5);
			tpImpresora.enviaCadena(strOutLine);
			strOutLine = "FECHA DE VENCIMIENTO: " + strVencimiento;
			tpImpresora.enviaCadena(strOutLine);
			tpImpresora.enviaCadena("");
			tpImpresora.enviaCadena("");
			tpImpresora.enviaCadena("");
			tpImpresora.enviaCadena("");
			tpImpresora.enviaCadena("");
			tpImpresora.enviaCadena(
				"         ____________________");
			tpImpresora.enviaCadena(
				"         TITULAR DE LA CUENTA");
			tpImpresora.enviaCadena("");
			tpImpresora.enviaCadena("");
			tpImpresora.enviaCadena("");
			tpImpresora.enviaCadena("");
			tpImpresora.enviaCadena("");
			tpImpresora.enviaCadena("");
			tpImpresora.enviaCadena("");
			tpImpresora.enviaCadena(
				"         _____________________");
			tpImpresora.enviaCadena(
				"         FIRMA DE AUTORIZACION");
			tpImpresora.enviaCadena("");
			tpImpresora.enviaCadena("");
			tpImpresora.enviaCadena("");
			tpImpresora.enviaCadena(
				"                "+strIdentidad2+"    \n\n\n");
			if (strConsecutivo.length() == 5)
				strConsecutivo = "0" + strConsecutivo;
			else if (strConsecutivo.length() == 7)
				strConsecutivo = strConsecutivo.substring(1, 7);
			strOut = "3314" +
				strConsecutivo
					+ strCajero
					+ strDate.substring(1, 5)
					+ strDate.substring(5, 7)
					+ strDate.substring(7, 9);
			if (iDriver == 0) 					
				tpImpresora.imprimeCadena1();
			else
				tpImpresora.imprimeCadena();			
			CodigoBarras2de5320(strOut);
			if (iDriver == 0) {
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.imprimeCadena1();
				tpImpresora.activateControl("CORTAPAPEL",iDriver);
			} else
				tpImpresora.activateControl("SALTOHOJA",iDriver);
		}
		return intStatus;
	}

	public int imprimeFichaReinversion(
			Vector vDatos,
			String strRespuesta,
			String strDate,
			String strDatosSuc,
			String strCajero,
			String strConsecutivo,
			int iDriver,
			String strIdentidad,
			String strIdentidad2) {
			intStatus = tpImpresora.verificaStatus();
			if ((intStatus == 95) || (intStatus == 88)) {
				tpImpresora.activateControl("CERTIFICACION",iDriver);
				Vector vDatosTxn = tpImpresora.Campos(strRespuesta, "~");
				Vector vDatosSuc = tpImpresora.Campos(strDatosSuc, "@");
				String strNumSuc = (String) vDatosSuc.elementAt(1);
				strNumSuc = strNumSuc.substring(7, strNumSuc.length());
				String strNomSuc = (String) vDatosSuc.elementAt(0);
				strNomSuc = strNomSuc.substring(7, strNomSuc.length());
				String strOutLine = "SUCURSAL : " + strNumSuc + "-" + strNomSuc+"\n";
				tpImpresora.enviaCadena(strOutLine);
				tpImpresora.enviaCadena(" ");
				strOutLine =
					strDate.substring(7, 9)
						+ "-"
						+ strDate.substring(5, 7)
						+ "-"
						+ strDate.substring(1, 5)
						+ " "
						+ strCajero;
				tpImpresora.enviaCadena(strOutLine);
				String strNombre = (String) vDatos.elementAt(1);
				String strInversion = (String) vDatos.elementAt(2);
				String strMonto = (String) vDatos.elementAt(5);
				strMonto = tpImpresora.formatMonto(strMonto);
				String strTasa = (String) vDatos.elementAt(6);
				String strVencimiento = (String) vDatos.elementAt(8);
				intStatus = 0;
				strOutLine = "\n         FICHA DE REINVERSION";
				tpImpresora.enviaCadena(strOutLine);
				tpImpresora.enviaCadena("");
				strOutLine = "   TXN    CONSEC";
				tpImpresora.enviaCadena(strOutLine);
				for (int veces = 0; veces < (vDatosTxn.size() / 2); veces++) {
					strOutLine =
						"   "
							+ (String) vDatosTxn.elementAt(veces * 2)
							+ "   "
							+ (String) vDatosTxn.elementAt(veces * 2 + 1);
					tpImpresora.enviaCadena(strOutLine);
				}
				tpImpresora.enviaCadena("");
				if(strNombre.length()>13){
					strOutLine = "   NOMBRE : " + strNombre;
					tpImpresora.enviaCadena(strOutLine);						
				}
				else{
					strOutLine = "               NOMBRE : " + strNombre;
					tpImpresora.enviaCadena(strOutLine);			
				}
				strOutLine = "            INVERSION : " + strInversion;
				tpImpresora.enviaCadena(strOutLine);
				strOutLine = "                MONTO : " + strMonto;
				tpImpresora.enviaCadena(strOutLine);
				strOutLine = "                 TASA : " + strTasa;
				tpImpresora.enviaCadena(strOutLine);
				strOutLine = "                PLAZO : ";
				tpImpresora.enviaCadena(strOutLine);
				strOutLine =
					"   FECHA DE OPERACION : "
						+ strDate.substring(7, 9)
						+ strDate.substring(5, 7)
						+ strDate.substring(1, 5);
				tpImpresora.enviaCadena(strOutLine);
				strOutLine = " FECHA DE VENCIMIENTO : " + strVencimiento;
				tpImpresora.enviaCadena(strOutLine);
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena(
					"        ____________________");
				tpImpresora.enviaCadena(
						"        TITULAR DE LA CUENTA");
				tpImpresora.enviaCadena(
					"\n\n\n        _____________________");
				tpImpresora.enviaCadena(
						"        FIRMA DE AUTORIZACION\n\n");
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena(
				"LAS INVERSIONES  SERAN ADMINISTRADAS SEGUN   EL   CONTRATO   DE  DEPOSITO");
			tpImpresora.enviaCadena(
				"BANCARIO  DE  TITULOS DE VALOR  Y  DE\nDINERO EN ADMINISTRACION  DE COMISION");
			tpImpresora.enviaCadena(
				"MERCANTIL  CELEBRADO  Y  CUYO NUMERO APARECERA   EN  ESTE   DOCUMENTO   EL");
			tpImpresora.enviaCadena(
				"DEPOSITANTE    Y    LA    INSTITUCIONCONVIENEN   EN    QUE    ESTA  ULTIMA");
			tpImpresora.enviaCadena(
				"RECIBIRA LOS  DOCUMENTOS  SALVO BUEN COBRO.  LOS  DOCUMENTOS QUE  NO  SEAN");
			tpImpresora.enviaCadena(
				"PAGADOS SE CARGARAN EN  SU INVERSION SIN  PRESIO  AVISO  EN  LA  FECHA DE ");
			tpImpresora.enviaCadena(
				"VENCIMIENTO, SE RENOVARA  AUTOMATICA�");
			tpImpresora.enviaCadena(
				"MENTE CON LAS MISMAS CARACTERISTICAS");
			tpImpresora.enviaCadena(
				"Y A LAS TASAS VIGENTES, A  MENOS  QUE");
			tpImpresora.enviaCadena(
				"EL DEPOSITANTE  NOS DE  INSTRUCCIONES");
			tpImpresora.enviaCadena(
				"EN CONTRARIO AL MOMENTO DEL  DEPOSITO");
			tpImpresora.enviaCadena(
				"O EN LA FECHA DE RENOVACION. (EN CASO");
			tpImpresora.enviaCadena(
				"DE  CERTIFICADOS DE  DEPOSITO,  SERAN");
			tpImpresora.enviaCadena(
				"ABONADOS  A  LA  CUENTA  A  LA  VISTA");
			tpImpresora.enviaCadena(
				"DESIGANADA  POR EL  CLIENTE) LOS IM�-");
			tpImpresora.enviaCadena(
				"PUESTOS   CORRESPONDIENTES   A   LOS");
			tpImpresora.enviaCadena(
				"INTERESES GENERADOS EN UNA INVERSION,");
			tpImpresora.enviaCadena(
				"SERAN  A   CARGO   DEL   DEPOSITANTE;");
			if(strIdentidad.equals("HSBC")){
			tpImpresora.enviaCadena(
					strIdentidad+"  PANAMA,  S.A.   INSTITUCION  DE");
			tpImpresora.enviaCadena(
				"BANCA MULTIPLE, GRUPO FINANCIERO "+strIdentidad);
			tpImpresora.enviaCadena(
				"LOS RETENDRA Y ENTERARA A LA  SECRE�");
			tpImpresora.enviaCadena(
				"TARIA DE  HACIENDA Y CREDITO  PUBLICO");}
			else {
				strIdentidad.contains("hsbc");
				StringBuffer sDato ;
			}
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena(
					"             "+strIdentidad2+"    \n\n\n\n");
				if (iDriver == 0) 					
					tpImpresora.imprimeCadena1();
				else
					tpImpresora.imprimeCadena();			
				if (strConsecutivo.length() == 5)
					strConsecutivo = "0" + strConsecutivo;
				else if (strConsecutivo.length() == 7)
					strConsecutivo = strConsecutivo.substring(1, 7);
				strOut = "3009" +
					strConsecutivo
						+ strCajero
						+ strDate.substring(1, 5)
						+ strDate.substring(5, 7)
						+ strDate.substring(7, 9);
				CodigoBarras2de5320(strOut);
				if (iDriver == 0) {
					tpImpresora.enviaCadena("");
					tpImpresora.enviaCadena("");
					tpImpresora.enviaCadena("");
					tpImpresora.enviaCadena("");
					tpImpresora.enviaCadena("");
					tpImpresora.enviaCadena("");
					tpImpresora.enviaCadena("");
					tpImpresora.enviaCadena("");
					tpImpresora.enviaCadena("");
					tpImpresora.imprimeCadena1();
					tpImpresora.activateControl("CORTAPAPEL",iDriver);
				} else
					tpImpresora.activateControl("SALTOHOJA",iDriver);
			}
			return intStatus;
		}

	/*public int imprimeFichaReinversionAnterior(Vector vDatos, String strDate) {
		intStatus = tpImpresora.verificaStatus();
		if ((intStatus == 95) || (intStatus == 88)) {
			String strNombre = (String) vDatos.elementAt(1);
			String strInversion = (String) vDatos.elementAt(2);
			String strInstrumento = (String) vDatos.elementAt(3);
			String strPlazo = (String) vDatos.elementAt(4);
			String strMonto = (String) vDatos.elementAt(5);
			strMonto = tpImpresora.formatMonto(strMonto);
			String strTasa = (String) vDatos.elementAt(6);
			String strMoneda = (String) vDatos.elementAt(7);
			tpImpresora.activateControl("NOMINA",iDriver);
			String strVencimiento = (String) vDatos.elementAt(8);
			intStatus = 0;
			strOut = "                              FICHA DE REINVERSION  ";
			tpImpresora.enviaCadena(strOut);
			if (strNombre.length() > 35)
				strOut =
					" NOMBRE : "
						+ strNombre
						+ strFiller.substring(0, 35 - strNombre.length())
						+ " No. INVERSION : "
						+ strInversion
						+ "\n";
			else
				strOut =
					" NOMBRE : "
						+ strNombre
						+ " No. INVERSION : "
						+ strInversion
						+ "\n";
			tpImpresora.enviaCadena(strOut);
			strOut =
				" TIPO DE INSTRUMENTO: "
					+ strInstrumento
					+ strFiller.substring(0, 11 - strInstrumento.length())
					+ " PLAZO: "
					+ strPlazo
					+ strFiller.substring(0, 12 - strPlazo.length())
					+ " FECHA OPERACION:"
					+ strDate.substring(7, 9)
					+ strDate.substring(5, 7)
					+ strDate.substring(1, 5)
					+ "\n";
			tpImpresora.enviaCadena(strOut);
			strOut =
				" FECHA DE VENCIMIENTO : "
					+ strVencimiento
					+ " MONTO REINVERTIDO  : "
					+ strMonto
					+ "\n";
			tpImpresora.enviaCadena(strOut);
			strOut =
				" TASA DE INTERES APLICADA A LA REINVERSION : "
					+ strTasa
					+ "\n";
			tpImpresora.enviaCadena(strOut);
			strOut =
				" MONTO EN LETRA : "
					+ Currency.convert(strMonto, Integer.parseInt(strMoneda))
					+ "\n\n\n";
			tpImpresora.enviaCadena(strOut);
			tpImpresora.enviaCadena(
				"          ____________________                    ____________________          ");
			tpImpresora.enviaCadena(
				"          TITULAR DE LA CUENTA                    FIRMA DE AUTORIZACION         ");
			tpImpresora.activateControl("SALTOHOJA",iDriver);
		}
		return intStatus;
	}*/
}