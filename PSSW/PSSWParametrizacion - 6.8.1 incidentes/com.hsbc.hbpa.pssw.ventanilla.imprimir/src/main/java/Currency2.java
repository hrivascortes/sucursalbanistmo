public class Currency2 {
	public static String unFormat(String strCantidad) {
		String strTmp = new String("");
		if (strCantidad != null) {
			int intCiclo = (strCantidad.length() / 4) + 1;
			int intSubindice1 = -2;
			for (int i = 0; i < intCiclo; i++) {
				intSubindice1 += 4;
				if ((i + 1) == intCiclo)
					intSubindice1 = strCantidad.length();
				int intSubindice2 = (3 * i) + i - 1;
				if (intSubindice2 < 0)
					intSubindice2 = 0;
				strTmp =
					strCantidad.substring(
						strCantidad.length() - intSubindice1,
						strCantidad.length() - intSubindice2)
						+ strTmp;
			}
		}
		return strTmp;
	}

	public static String unidades(String strUnidades) {
		String strRespuesta = new String("");
		if (strUnidades.equals("0"))
			strRespuesta = "";
		else if (strUnidades.equals("1"))
			strRespuesta = "ONE ";
		else if (strUnidades.equals("2"))
			strRespuesta = "TWO ";
		else if (strUnidades.equals("3"))
			strRespuesta = "THREE ";
		else if (strUnidades.equals("4"))
			strRespuesta = "FOUR ";
		else if (strUnidades.equals("5"))
			strRespuesta = "FIVE ";
		else if (strUnidades.equals("6"))
			strRespuesta = "SIX ";
		else if (strUnidades.equals("7"))
			strRespuesta = "SEVEN ";
		else if (strUnidades.equals("8"))
			strRespuesta = "EIGHT ";
		else if (strUnidades.equals("9"))
			strRespuesta = "NINE ";
		return strRespuesta;
	}

	public static String decena(String strDecenas, String strUnidades) {
		String strRespuesta = new String("");
		if (strDecenas.equals("0"))
			strRespuesta = "";
		else if (strDecenas.equals("1"))
			strRespuesta = "TEN ";
		else if (strDecenas.equals("2"))
			strRespuesta = "TWENTY ";
		else if (strDecenas.equals("3"))
			strRespuesta = "THIRTY ";
		else if (strDecenas.equals("4"))
			strRespuesta = "FOURTY ";
		else if (strDecenas.equals("5"))
			strRespuesta = "FIFTY ";
		else if (strDecenas.equals("6"))
			strRespuesta = "SIXTY ";
		else if (strDecenas.equals("7"))
			strRespuesta = "SEVENTY ";
		else if (strDecenas.equals("8"))
			strRespuesta = "EIGHTY ";
		else if (strDecenas.equals("9"))
			strRespuesta = "NINETY ";
		strRespuesta += unidades(strUnidades);
		return strRespuesta;
	}

	public static String decenas(String strDecenas, String strUnidades) {
		String strRespuesta = new String("");
		int intValor = Integer.parseInt(strDecenas);
		if ((intValor < 20) && (intValor > 9)) {
			if (strUnidades.equals("0"))
				strRespuesta = "TEN ";
			else if (strUnidades.equals("1"))
				strRespuesta = "ELEVEN ";
			else if (strUnidades.equals("2"))
				strRespuesta = "TWELVE ";
			else if (strUnidades.equals("3"))
				strRespuesta = "THIRTEEN ";
			else if (strUnidades.equals("4"))
				strRespuesta = "FOURTEEN ";
			else if (strUnidades.equals("5"))
				strRespuesta = "FIFTEEN ";
			else if (strUnidades.equals("6"))
				strRespuesta = "SIXTEEN ";
			else if (strUnidades.equals("7"))
				strRespuesta = "SEVENTEEN ";
			else if (strUnidades.equals("8"))
				strRespuesta = "EIGHTEEN ";
			else if (strUnidades.equals("9"))
				strRespuesta = "NINETEEN ";
		} else
			strRespuesta = decena(strDecenas.substring(0, 1), strUnidades);
		return strRespuesta;
	}

	public static String centenas(
		String strCentena,
		String strDecenas,
		String strUnidades) {
		String strRespuesta = new String("");
		if (strCentena.equals("0"))
			strRespuesta = "";
		else if (strCentena.equals("1"))
			strRespuesta = "ONE HUNDRED ";
		else if (strCentena.equals("2"))
			strRespuesta = "TWO HUNDRED ";
		else if (strCentena.equals("3"))
			strRespuesta = "THREE HUNDRED ";
		else if (strCentena.equals("4"))
			strRespuesta = "FOUR HUNDRED ";
		else if (strCentena.equals("5"))
			strRespuesta = "FIVE HUNDRED ";
		else if (strCentena.equals("6"))
			strRespuesta = "SIX HUNDRED ";
		else if (strCentena.equals("7"))
			strRespuesta = "SEVEN HUNDRED ";
		else if (strCentena.equals("8"))
			strRespuesta = "EIGHT HUNDRED ";
		else if (strCentena.equals("9"))
			strRespuesta = "NINE HUNDRED ";
		strRespuesta += decenas(strDecenas, strUnidades);
		return strRespuesta;
	}

	public static String convert(String strCantidad) {
		String strCentavos = new String("");
		String strEnteros = new String("");
		String strConversion = new String("");
		String strLetra = new String("");
		String strLetraComplemento = new String("");
		String strCeros = new String("000000000");
        String strTmp = unFormat(strCantidad);
		strCentavos = strTmp.substring(strTmp.length() - 2, strTmp.length());
		strEnteros = strTmp.substring(0, strTmp.length() - 2);
		if (strEnteros.length() > 10)
			strLetra = "Longitud del monto No permitida";
		else {
			int intVeces = strEnteros.length() / 3;
			if (strEnteros.length() == 10)
				strLetraComplemento =
					unidades(strEnteros.substring(0, 1)) + "THOUSAND ";
			if (((intVeces * 3) < strEnteros.length())
				&& (strEnteros.length() != 10))
				intVeces++;
			if (strEnteros.length() != 10)
				strConversion =
					strCeros.substring(0, (intVeces * 3) - strEnteros.length())
						+ strEnteros;
			else
				strConversion = strEnteros;				
			for (int intCiclo = 0; intCiclo < intVeces; intCiclo++) {
				int intIndice = strConversion.length() - (intCiclo * 3);
				//System.out.println("intIndice " + intIndice);
				String strCentenas =
					strConversion.substring(intIndice - 3, intIndice - 2);
				String strDecenas =
					strConversion.substring(intIndice - 2, intIndice);
				String strUnidades =
					strConversion.substring(intIndice - 1, intIndice);
				strTmp = centenas(strCentenas, strDecenas, strUnidades);
				if (intCiclo == 1){
                	if(strCentenas.equals("0")&&strDecenas.equals("00")&&strUnidades.equals("0"))
                    	strTmp = "";
                    else
                        strTmp = strTmp + "THOUSAND ";
                }
				if (intCiclo == 2)
					if (strUnidades.equals("1"))
						strTmp += "MILLION ";
					else
						strTmp += "MILLIONS ";
				strLetra = strTmp + strLetra;
			}
			strLetra =
				strLetraComplemento
					+ strLetra
					+ "US. DLLS. "
					+ "("
					+ strCentavos
					+ "/100)";
		}
		return strLetra;
	}
}