import java.util.*;
import java.math.BigInteger;

public class Relaciones {
	ToolsPrinter tpImpresora = new ToolsPrinter();
	int intStatus = 0;
	String strFiller =
		"                                                                                                                                      ";
	String strOutLine = new String("");
	String strTmp = "";
	Vector vDatosSuc = new Vector();
	String strNumSuc = "";
	String strNomSuc = "";
	String strDomSuc = "";
	String strNomPza = "";

	public void datosSucursal(String strDatosSuc) {
		vDatosSuc = tpImpresora.Campos(strDatosSuc, "@");
		strNumSuc = (String) vDatosSuc.elementAt(1);
		strNumSuc = strNumSuc.substring(7, strNumSuc.length());
		strNomSuc = (String) vDatosSuc.elementAt(0);
		strNomSuc = strNomSuc.substring(7, strNomSuc.length());
		strDomSuc = (String) vDatosSuc.elementAt(2);
		strDomSuc = strDomSuc.substring(7, strDomSuc.length());
		strNomPza = (String) vDatosSuc.elementAt(4);
		strNomPza = strNomPza.substring(7, strNomPza.length());
	}

	public int imprime9642(
		Vector vDatos,
		String strSucursal,
		String strDatosSuc,
		String strDate,
		int iDriver,
		String strIdentidad) {
		intStatus = tpImpresora.verificaStatus();
		if ((intStatus == 95) || (intStatus == 88)) {
			intStatus = 0;
			int intControl = 0;
			int intPagina = 1;
			long intAcumMonto = 0;

			tpImpresora.activateControl("CERTIFICACION",iDriver);
			datosSucursal(strDatosSuc);
			String strMoneda = (String) vDatos.elementAt(1);
			String strTituloMon = new String("");
			if (strMoneda.equals("01"))
				strTituloMon = "   MONEDA NACIONAL";
			else
				strTituloMon = "   MONEDA EN DOLARES";
			int intSize = (vDatos.size() - 2) / 5;
			for (int i = 0; i < intSize; i++) {
				String strConsecutivo = (String) vDatos.elementAt((i * 5) + 2);
				String strHora = " ";
				if (strConsecutivo.equals(""))
					strConsecutivo = " ";
				else
					strHora =
						strConsecutivo.substring(1, 3)
							+ ":"
							+ strConsecutivo.substring(3, 5);
				String strCuenta = (String) vDatos.elementAt((i * 5) + 3);
				if (strCuenta.equals(""))
					strCuenta = " ";
				String strSerial = (String) vDatos.elementAt((i * 5) + 4);
				if (strSerial.equals(""))
					strSerial = " ";
				String strMonto = (String) vDatos.elementAt((i * 5) + 5);
				if (strMonto.equals(""))
					strMonto = "000";
				intAcumMonto += Long.parseLong(strMonto);
				strMonto = tpImpresora.formatMonto(strMonto);
				String strOperador = (String) vDatos.elementAt((i * 5) + 6);
				if (strOperador.equals(""))
					strOperador = "      ";
				if (intControl == 0) {
					strOutLine =
						"         "+strIdentidad+"             "
							+ strFiller.substring(0, 29)
							+ "PAGINA : "
							+ intPagina++;
					//IMAGEN
					tpImpresora.enviaCadena(strOutLine);
					tpImpresora.enviaCadena("PLAZA " + strNomPza);
					strOutLine = "SUCURSAL : " + strSucursal + "  " + strNomSuc;
					tpImpresora.enviaCadena(strOutLine);
					strOutLine =
						"RELACION DE CHEQUES CERTIFICADOS DEL DIA "
							+ strDate.substring(7, 9)
							+ "/"
							+ strDate.substring(5, 7)
							+ "/"
							+ strDate.substring(1, 5)
							+ "    "
							+ strTituloMon;
					tpImpresora.enviaCadena(strOutLine);
					tpImpresora.enviaCadena(
						"------------------------------------------------------------------------------");
					tpImpresora.enviaCadena(
						"   CONSECUTIVO   No. CUENTA     No.CHEQUE       IMPORTE       CAJERO   HORA ");
					tpImpresora.enviaCadena(
						"------------------------------------------------------------------------------");
				}
				strOutLine =
					"     "
						+ strConsecutivo
						+ "    "
						+ strFiller.substring(0, 11 - strCuenta.length())
						+ strCuenta
						+ "   "
						+ strFiller.substring(0, 11 - strSerial.length())
						+ strSerial
						+ "  "
						+ strFiller.substring(0, 16 - strMonto.length())
						+ strMonto
						+ "   "
						+ strOperador
						+ "   "
						+ strHora;
				tpImpresora.enviaCadena(strOutLine);
				intControl++;
				if ((intControl == 45) && (i != (intSize - 1))) {
					intControl = 0;
					if (iDriver == 0) {
						tpImpresora.InserteDocumento();
						tpImpresora.activateControl("SELECCIONASLIP",iDriver);
						tpImpresora.enviaCadena("");
						tpImpresora.enviaCadena("");
						tpImpresora.enviaCadena("");
						tpImpresora.enviaCadena("");
						tpImpresora.enviaCadena("");
						tpImpresora.imprimeCadena1();
						tpImpresora.activateControl("SALTOHOJA",iDriver);
						tpImpresora.activateControl("SELECCIONAJOURNAL",iDriver);
					} else {
						tpImpresora.activateControl("SALTOHOJA",iDriver);
						tpImpresora.muestraVentana();
					}
				}
			}
			String strMontoTotal = String.valueOf(intAcumMonto);
			strMontoTotal = tpImpresora.formatMonto(strMontoTotal);
			tpImpresora.enviaCadena(
				"------------------------------------------------------------------------------");
			strOutLine =
				"                    SUMA TOTAL :           "
					+ strFiller.substring(0, 16 - strMontoTotal.length())
					+ strMontoTotal;
			tpImpresora.enviaCadena(strOutLine);
			strOutLine =
				"           TOTAL DE DOCUMENTOS :           " + intSize;
			tpImpresora.enviaCadena(strOutLine);
			if (iDriver == 0) {
				tpImpresora.InserteDocumento();
				tpImpresora.activateControl("SELECCIONASLIP",iDriver);
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.imprimeCadena1();
				tpImpresora.activateControl("SALTOHOJA",iDriver);
				tpImpresora.activateControl("SELECCIONAJOURNAL",iDriver);
			} else {
				tpImpresora.activateControl("SALTOHOJA",iDriver);
			}
		}
		return intStatus;
	}

	public int imprime9630(
		Vector vDatos,
		String strSucursal,
		String strDatosSuc,
		String strDate,
		int iDriver,
		String strIdentidad) {
		long intAcumMonto = 0;
		intStatus = tpImpresora.verificaStatus();
		if ((intStatus == 95) || (intStatus == 88)) {
			intStatus = 0;
			int intControl = 0;
			int intPagina = 1;
			tpImpresora.activateControl("CERTIFICACION",iDriver);
			datosSucursal(strDatosSuc);
			String strMoneda = (String) vDatos.elementAt(1);
			if (strMoneda.equals("01"))
				strMoneda = "   MONEDA NACIONAL";
			else
				strMoneda = "   MONEDA EN DOLARES";
			String strServicio = (String) vDatos.elementAt(2);
			int intSize = (vDatos.size() - 3) / 4;
			for (int i = 0; i < intSize; i++) {
				String strConsecutivo = (String) vDatos.elementAt((i * 4) + 3);
				String strHora = " ";
				if (strConsecutivo.equals(""))
					strConsecutivo = " ";
				else
					strHora =
						strConsecutivo.substring(1, 3)
							+ ":"
							+ strConsecutivo.substring(3, 5);
				String strReferencia = (String) vDatos.elementAt((i * 4) + 4);
				if (strReferencia.equals(""))
					strReferencia = " ";
				String strMonto = (String) vDatos.elementAt((i * 4) + 5);
				if (strMonto.equals(""))
					strMonto = "000";
				intAcumMonto += Long.parseLong(strMonto);
				strMonto = tpImpresora.formatMonto(strMonto);
				String strOperador = (String) vDatos.elementAt((i * 4) + 6);
				if (strOperador.equals(""))
					strOperador = "      ";
				if (intControl == 0) {
					strOutLine =
						"         "+strIdentidad+"             "
							+ strFiller.substring(0, 29)
							+ "PAGINA : "
							+ intPagina++;
					//IMAGEN
					tpImpresora.enviaCadena(strOutLine);
					tpImpresora.enviaCadena("PLAZA " + strNomPza);
					strOutLine = "SUCURSAL : " + strSucursal + "  " + strNomSuc;
					tpImpresora.enviaCadena(strOutLine);
					if (!strServicio.equals("0"))
						strOutLine =
							"RELACION DE SERVICIOS  - EGRESOS (TXN = "
								+ strServicio
								+ ") - DEL DIA "
								+ strDate.substring(7, 9)
								+ "/"
								+ strDate.substring(5, 7)
								+ "/"
								+ strDate.substring(1, 5)
								+ "    "
								+ strMoneda;
					else
						strOutLine =
							"RELACION DE SERVICIOS - EGRESOS -  DEL DIA "
								+ strDate.substring(7, 9)
								+ "/"
								+ strDate.substring(5, 7)
								+ "/"
								+ strDate.substring(1, 5)
								+ "    "
								+ strMoneda;
					tpImpresora.enviaCadena(strOutLine);
					tpImpresora.enviaCadena(
						"------------------------------------------------------------------------------");
					tpImpresora.enviaCadena(
						"  CONSECUTIVO          REFERENCIA        IMPORTE              CAJERO   HORA ");
					tpImpresora.enviaCadena(
						"------------------------------------------------------------------------------");
				}
				strOutLine =
					"    "
						+ strConsecutivo
						+ "     "
						+ strFiller.substring(0, 21 - strReferencia.length())
						+ strReferencia
						+ "     "
						+ strFiller.substring(0, 16 - strMonto.length())
						+ strMonto
						+ "   "
						+ strOperador
						+ "   "
						+ strHora;
				tpImpresora.enviaCadena(strOutLine);
				intControl++;
				if ((intControl == 45) && (i != (intSize - 1))) {
					intControl = 0;
					if (iDriver == 0) {
						tpImpresora.InserteDocumento();
						tpImpresora.activateControl("SELECCIONASLIP",iDriver);
						tpImpresora.enviaCadena("");
						tpImpresora.enviaCadena("");
						tpImpresora.enviaCadena("");
						tpImpresora.enviaCadena("");
						tpImpresora.enviaCadena("");
						tpImpresora.imprimeCadena1();
						tpImpresora.activateControl("SALTOHOJA",iDriver);
						tpImpresora.activateControl("SELECCIONAJOURNAL",iDriver);
					} else {
						tpImpresora.activateControl("SALTOHOJA",iDriver);
						tpImpresora.muestraVentana();
					}
				}
			}
			String strMontoTotal = String.valueOf(intAcumMonto);
			strMontoTotal = tpImpresora.formatMonto(strMontoTotal);
			tpImpresora.enviaCadena(
				"------------------------------------------------------------------------------");
			strOutLine =
				"                    SUMA TOTAL :          "
					+ strFiller.substring(0, 16 - strMontoTotal.length())
					+ strMontoTotal;
			tpImpresora.enviaCadena(strOutLine);
			strOutLine = "           TOTAL DE DOCUMENTOS :          " + intSize;
			tpImpresora.enviaCadena(strOutLine);
			if (iDriver == 0) {
				tpImpresora.InserteDocumento();
				tpImpresora.activateControl("SELECCIONASLIP",iDriver);
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.imprimeCadena1();
				tpImpresora.activateControl("SALTOHOJA",iDriver);
				tpImpresora.activateControl("SELECCIONAJOURNAL",iDriver);
			} else {
				tpImpresora.activateControl("SALTOHOJA",iDriver);
			}
		}
		return intStatus;
	}

	public int imprime9732(
		Vector vDatos,
		String strSucursal,
		String strDatosSuc,
		String strDate,
		int iDriver,
		String strIdentidad) {
		long intAcumMonto = 0;
		long intAcumEfectivo = 0;
		intStatus = tpImpresora.verificaStatus();
		if ((intStatus == 95) || (intStatus == 88)) {
			intStatus = 0;
			int intControl = 0;
			int intPagina = 1;
			tpImpresora.activateControl("CERTIFICACION",iDriver);
			datosSucursal(strDatosSuc);
			String strMoneda = (String) vDatos.elementAt(1);
			if (strMoneda.equals("01"))
				strMoneda = "   MONEDA NACIONAL";
			else
				strMoneda = "   MONEDA EN DOLARES";
			String strServicio = (String) vDatos.elementAt(2);
			int intSize = (vDatos.size() - 3) / 5;
			for (int i = 0; i < intSize; i++) {
				String strConsecutivo = (String) vDatos.elementAt((i * 5) + 3);
				String strReferencia = (String) vDatos.elementAt((i * 5) + 4);
				if (strReferencia.equals(""))
					strReferencia = " ";
				String strEfectivo = (String) vDatos.elementAt((i * 5) + 5);
				if (strEfectivo.equals(""))
					strEfectivo = "000";
				intAcumEfectivo += Long.parseLong(strEfectivo);
				strEfectivo = tpImpresora.formatMonto(strEfectivo);
				String strMonto = (String) vDatos.elementAt((i * 5) + 6);
				if (strMonto.equals(""))
					strMonto = "000";
				intAcumMonto += Long.parseLong(strMonto);
				strMonto = tpImpresora.formatMonto(strMonto);
				String strOperador = (String) vDatos.elementAt((i * 5) + 7);
				if (strOperador.equals(""))
					strOperador = "      ";
				if (intControl == 0) {
					strOutLine =
						"         "+strIdentidad+"             "
							+ strFiller.substring(0, 29)
							+ "PAGINA : "
							+ intPagina++;
					tpImpresora.enviaCadena(strOutLine);
					tpImpresora.enviaCadena("PLAZA " + strNomPza);
					strOutLine = "SUCURSAL : " + strSucursal + "  " + strNomSuc;
					tpImpresora.enviaCadena(strOutLine);
					if (!strServicio.equals("0"))
						strOutLine =
							"RELACION DE SERVICIOS  - INGRESOS (TXN = "
								+ strServicio
								+ ") - DEL DIA "
								+ strDate.substring(7, 9)
								+ "/"
								+ strDate.substring(5, 7)
								+ "/"
								+ strDate.substring(1, 5)
								+ "    "
								+ strMoneda;
					else
						strOutLine =
							"RELACION DE SERVICIOS - INGRESOS -  DEL DIA "
								+ strDate.substring(7, 9)
								+ "/"
								+ strDate.substring(5, 7)
								+ "/"
								+ strDate.substring(1, 5)
								+ "    "
								+ strMoneda;
					tpImpresora.enviaCadena(strOutLine);
					tpImpresora.enviaCadena(
						"------------------------------------------------------------------------------");
					tpImpresora.enviaCadena(
						"CONSECUTIVO        REFERENCIA       EFECTIVO          MONTO          CAJERO   ");
					tpImpresora.enviaCadena(
						"------------------------------------------------------------------------------");
				}
				strOutLine =
					"  "
						+ strConsecutivo
						+ " "
						+ strFiller.substring(0, 21 - strReferencia.length())
						+ strReferencia
						+ "  "
						+ strFiller.substring(0, 16 - strEfectivo.length())
						+ strEfectivo
						+ "  "
						+ strFiller.substring(0, 16 - strMonto.length())
						+ strMonto
						+ "  "
						+ strOperador;
				tpImpresora.enviaCadena(strOutLine);
				intControl++;
				if ((intControl == 45) && (i != (intSize - 1))) {
					intControl = 0;
					if (iDriver == 0) {
						tpImpresora.InserteDocumento();
						tpImpresora.activateControl("SELECCIONASLIP",iDriver);
						tpImpresora.enviaCadena("");
						tpImpresora.enviaCadena("");
						tpImpresora.enviaCadena("");
						tpImpresora.enviaCadena("");
						tpImpresora.enviaCadena("");
						tpImpresora.imprimeCadena1();
						tpImpresora.activateControl("SALTOHOJA",iDriver);
						tpImpresora.activateControl("SELECCIONAJOURNAL",iDriver);
					} else {
						tpImpresora.activateControl("SALTOHOJA",iDriver);
						tpImpresora.muestraVentana();
					}
				}
			}
			String strMontoTotal = String.valueOf(intAcumMonto);
			strMontoTotal = tpImpresora.formatMonto(strMontoTotal);
			String strEfectivoTotal = String.valueOf(intAcumEfectivo);
			tpImpresora.enviaCadena(
				"------------------------------------------------------------------------------");
			strEfectivoTotal = tpImpresora.formatMonto(strEfectivoTotal);
			strOutLine =
				"                    SUMA TOTAL : "
					+ strFiller.substring(0, 16 - strEfectivoTotal.length())
					+ strEfectivoTotal
					+ "  "
					+ strFiller.substring(0, 16 - strMontoTotal.length())
					+ strMontoTotal;
			tpImpresora.enviaCadena(strOutLine);
			strOutLine = "           TOTAL DE DOCUMENTOS : " + intSize;
			tpImpresora.enviaCadena(strOutLine);
			if (iDriver == 0) {
				tpImpresora.InserteDocumento();
				tpImpresora.activateControl("SELECCIONASLIP",iDriver);
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.imprimeCadena1();
				tpImpresora.activateControl("SALTOHOJA",iDriver);
				tpImpresora.activateControl("SELECCIONAJOURNAL",iDriver);
			} else {
				tpImpresora.activateControl("SALTOHOJA",iDriver);
			}
		}
		return intStatus;
	}

	public int imprime9503(
		Vector vDatos,
		String strSucursal,
		String strDatosSuc,
		String strDate,
		int iDriver,
		String strIdentidad) {
		intStatus = tpImpresora.verificaStatus();
		if ((intStatus == 95) || (intStatus == 88)) {
			intStatus = 0;
			int intControl = 0;
			int intPagina = 1;
			long intAcumMonto = 0;
			long intAcumEfectivo = 0;
			tpImpresora.activateControl("CERTIFICACION",iDriver);
			datosSucursal(strDatosSuc);
			String strOpcion1 = (String) vDatos.elementAt(1);
			String strOpcion2 = (String) vDatos.elementAt(2);
			String strTitulo1 =
				new String("RELACION DE  DECLARACIONES TESOFE DEL DIA ");
			String strTitulo2 = new String("EFECTIVO Y CHEQUE "+strIdentidad);
			if (strOpcion1.equals("2"))
				strTitulo1 = "RELACION PAGOS COMISION NAL. AGUA DEL DIA ";
			if (strOpcion1.equals("3"))
				strTitulo1 =
					"DECLARACIONES 1% SOBRE EROGACIONES Y 2% SOBRE HOSPEDAJE ";
			if (strOpcion1.equals("4"))
				strTitulo1 = "DECLARACIONES SF-2 ";
			if (strOpcion1.equals("5"))
				strTitulo1 = "DECLARACIONES SF-3 ";
			if (strOpcion1.equals("6"))
				strTitulo1 = "DECLARACIONES TENENCIAS 2005 ESTADO DE OAXACA ";
			if (strOpcion1.equals("7"))
				strTitulo1 = "DECLARACIONES TENENCIAS 2005 ESTADO DE PUEBLA ";

			if (strOpcion2.equals("1"))
				strTitulo2 = "EN CEROS";
			if (strOpcion2.equals("3"))
				strTitulo2 = "EN DOCUMENTOS";
			int intSize = (vDatos.size() - 3) / 7;
			for (int i = 0; i < intSize; i++) {
				String strConsecutivo = (String) vDatos.elementAt((i * 7) + 3);
				String strCuenta = (String) vDatos.elementAt((i * 7) + 4);
				String strReferencia = (String) vDatos.elementAt((i * 7) + 5);
				if (strReferencia.equals(""))
					strReferencia = " ";
				String strEfectivo = (String) vDatos.elementAt((i * 7) + 6);
				if (strEfectivo.equals(""))
					strEfectivo = "000";
				intAcumEfectivo += Long.parseLong(strEfectivo);
				strEfectivo = tpImpresora.formatMonto(strEfectivo);
				String strMonto = (String) vDatos.elementAt((i * 7) + 7);
				if (strMonto.equals(""))
					strMonto = "000";
				intAcumMonto += Long.parseLong(strMonto);
				strMonto = tpImpresora.formatMonto(strMonto);
				String strOperador = (String) vDatos.elementAt((i * 7) + 8);
				String strMsgEnvio = (String) vDatos.elementAt((i * 7) + 9);
				int iPIndex = strMsgEnvio.indexOf(strReferencia);
				String strRefMod = strMsgEnvio.substring(iPIndex, iPIndex + 20);
				//System.out.println("strRefMod: " + strRefMod);
				strReferencia = "";
				int intAst = 0;
				if (strTitulo1.equals("DECLARACIONES SF-2 ")
					|| strTitulo1.equals("DECLARACIONES SF-3 ")) {
					for (int inicioCadena = 0; intAst < 3; inicioCadena++) {
						if (inicioCadena == 20)
							break;
						if (strRefMod.charAt(inicioCadena) != '*')
							strReferencia =
								strReferencia + strRefMod.charAt(inicioCadena);
						else {
							intAst++;
							break;
						}
					}
				} else {
					for (int inicioCadena = 0; intAst < 3; inicioCadena++) {
						//System.out.println("inicioCadena:" + inicioCadena);
						if (inicioCadena == 20)
							break;

						if (strRefMod.charAt(inicioCadena) != '*')
							strReferencia =
								strReferencia + strRefMod.charAt(inicioCadena);
						else
							intAst++;

					}
				}
				if (strOperador.equals(""))
					strOperador = "      ";
				if (intControl == 0) {
					strOutLine =
						"         "+strIdentidad+"             "
							+ strFiller.substring(0, 29)
							+ "PAGINA : "
							+ intPagina++;
					tpImpresora.enviaCadena(strOutLine);
					strOutLine =
						"PLAZA "
							+ strNomPza
							+ strFiller.substring(0, 42 - strNomPza.length())
							+ strFiller.substring(0, 24 - strTitulo2.length())
							+ strTitulo2;
					tpImpresora.enviaCadena(strOutLine);
					strOutLine =
						"SUCURSAL : "
							+ strSucursal
							+ "  "
							+ strNomSuc
							+ "(MONEDA NACIONAL)";
					tpImpresora.enviaCadena(strOutLine);
					String strOutLine1 =
						strTitulo1
							+ strDate.substring(7, 9)
							+ "/"
							+ strDate.substring(5, 7)
							+ "/"
							+ strDate.substring(1, 5);
					strOutLine = strOutLine1;
					tpImpresora.enviaCadena(strOutLine);
					strOutLine = " CUENTA COMPA�IA " + strCuenta;
					tpImpresora.enviaCadena(strOutLine);
					tpImpresora.enviaCadena(
						"------------------------------------------------------------------------------");
					tpImpresora.enviaCadena(
						"CONSECUTIVO        REFERENCIA       EFECTIVO          MONTO          CAJERO   ");
					tpImpresora.enviaCadena(
						"------------------------------------------------------------------------------");
				}
				strOutLine =
					"  "
						+ strConsecutivo
						+ " "
						+ strFiller.substring(0, 21 - strReferencia.length())
						+ strReferencia
						+ "  "
						+ strFiller.substring(0, 16 - strEfectivo.length())
						+ strEfectivo
						+ "  "
						+ strFiller.substring(0, 16 - strMonto.length())
						+ strMonto
						+ "  "
						+ strOperador;
				tpImpresora.enviaCadena(strOutLine);
				intControl++;
				if ((intControl == 45) && (i != (intSize - 1))) {
					intControl = 0;
					if (iDriver == 0) {
						tpImpresora.InserteDocumento();
						tpImpresora.activateControl("SELECCIONASLIP",iDriver);
						tpImpresora.enviaCadena("");
						tpImpresora.enviaCadena("");
						tpImpresora.enviaCadena("");
						tpImpresora.enviaCadena("");
						tpImpresora.enviaCadena("");
						tpImpresora.imprimeCadena1();
						tpImpresora.activateControl("SALTOHOJA",iDriver);
						tpImpresora.activateControl("SELECCIONAJOURNAL",iDriver);
					} else {
						tpImpresora.activateControl("SALTOHOJA",iDriver);
						tpImpresora.muestraVentana();
					}
				}
			}
			String strMontoTotal = String.valueOf(intAcumMonto);
			strMontoTotal = tpImpresora.formatMonto(strMontoTotal);
			String strEfectivoTotal = String.valueOf(intAcumEfectivo);
			tpImpresora.enviaCadena(
				"------------------------------------------------------------------------------");
			strEfectivoTotal = tpImpresora.formatMonto(strEfectivoTotal);
			strOutLine =
				"                    SUMA TOTAL : "
					+ strFiller.substring(0, 16 - strEfectivoTotal.length())
					+ strEfectivoTotal
					+ "  "
					+ strFiller.substring(0, 16 - strMontoTotal.length())
					+ strMontoTotal;
			tpImpresora.enviaCadena(strOutLine);
			strOutLine = "           TOTAL DE DOCUMENTOS : " + intSize;
			tpImpresora.enviaCadena(strOutLine);
			if (iDriver == 0) {
				tpImpresora.InserteDocumento();
				tpImpresora.activateControl("SELECCIONASLIP",iDriver);
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.imprimeCadena1();
				tpImpresora.activateControl("SALTOHOJA",iDriver);
				tpImpresora.activateControl("SELECCIONAJOURNAL",iDriver);
			} else {
				tpImpresora.activateControl("SALTOHOJA",iDriver);
			}
		}
		return intStatus;
	}

	public int imprime9730(
		Vector vDatos,
		String strSucursal,
		String strDatosSuc,
		String strDate,
		int iDriver,
		String strIdentidad) {
		//int intDoctos = 0;
		intStatus = tpImpresora.verificaStatus();
		if ((intStatus == 95) || (intStatus == 88)) {
			intStatus = 0;
			int intControl = 0;
			int intPagina = 1;
			long intAcumMonto = 0;
			long intAcumEfectivo = 0;
			tpImpresora.activateControl("CERTIFICACION",iDriver);
			datosSucursal(strDatosSuc);
			String strMoneda = (String) vDatos.elementAt(1);
			if (strMoneda.equals("01"))
				strMoneda = "   MONEDA NACIONAL";
			else
				strMoneda = "   MONEDA EN DOLARES";
			int intSize = (vDatos.size() - 3) / 6;
			for (int i = 0; i < intSize; i++) {
				String strConsecutivo = (String) vDatos.elementAt((i * 6) + 3);
				if (strConsecutivo.equals(""))
					strConsecutivo = " ";
				String strCuenta = (String) vDatos.elementAt((i * 6) + 4);
				String strReferencia = (String) vDatos.elementAt((i * 6) + 5);
				if (strReferencia.equals(""))
					strReferencia = " ";
				String strEfectivo = (String) vDatos.elementAt((i * 6) + 6);
				if (strEfectivo.equals(""))
					strEfectivo = "000";
				intAcumEfectivo += Long.parseLong(strEfectivo);
				strEfectivo = tpImpresora.formatMonto(strEfectivo);
				String strMonto = (String) vDatos.elementAt((i * 6) + 7);
				if (strMonto.equals(""))
					strMonto = "000";
				intAcumMonto += Long.parseLong(strMonto);
				strMonto = tpImpresora.formatMonto(strMonto);
				String strOperador = (String) vDatos.elementAt((i * 6) + 8);
				if (strOperador.equals(""))
					strOperador = "      ";
				if (intControl == 0) {
					strOutLine =
						"         "+strIdentidad+"             "
							+ strFiller.substring(0, 29)
							+ "PAGINA : "
							+ intPagina++;
					tpImpresora.enviaCadena(strOutLine);
					tpImpresora.enviaCadena("PLAZA " + strNomPza);
					strOutLine = "SUCURSAL : " + strSucursal + "  " + strNomSuc;
					tpImpresora.enviaCadena(strOutLine);
					strOutLine =
						"RELACION DE SERVICIOS ESPECIALES DEL DIA "
							+ strDate.substring(7, 9)
							+ "/"
							+ strDate.substring(5, 7)
							+ "/"
							+ strDate.substring(1, 5)
							+ "    "
							+ strMoneda;
					tpImpresora.enviaCadena(strOutLine);
					tpImpresora.enviaCadena(
						"---------------------------------------------------------------------------------------------------------");
					tpImpresora.enviaCadena(
						"  CONSECUTIVO       CUENTA               REFERENCIA              EFECTIVO          MONTO         CAJERO  ");
					tpImpresora.enviaCadena(
						"---------------------------------------------------------------------------------------------------------");
				}
				strOutLine =
					"    "
						+ strConsecutivo
						+ "    "
						+ strFiller.substring(0, 16 - strCuenta.length())
						+ strCuenta
						+ "  "
						+ strFiller.substring(0, 26 - strReferencia.length())
						+ strReferencia
						+ "  "
						+ strFiller.substring(0, 16 - strEfectivo.length())
						+ strEfectivo
						+ "  "
						+ strFiller.substring(0, 16 - strMonto.length())
						+ strMonto
						+ "  "
						+ strOperador;
				tpImpresora.enviaCadena(strOutLine);
				intControl++;
				if ((intControl == 45) && (i != (intSize - 1))) {
					intControl = 0;
					if (iDriver == 0) {
						tpImpresora.InserteDocumento();
						tpImpresora.activateControl("SELECCIONASLIP",iDriver);
						tpImpresora.enviaCadena("");
						tpImpresora.enviaCadena("");
						tpImpresora.enviaCadena("");
						tpImpresora.enviaCadena("");
						tpImpresora.enviaCadena("");
						tpImpresora.imprimeCadena1();
						tpImpresora.activateControl("SALTOHOJA",iDriver);
						tpImpresora.activateControl("SELECCIONAJOURNAL",iDriver);
					} else {
						tpImpresora.activateControl("SALTOHOJA",iDriver);
						tpImpresora.muestraVentana();
					}
				}
			}
			String strMontoTotal = String.valueOf(intAcumMonto);
			strMontoTotal = tpImpresora.formatMonto(strMontoTotal);
			String strEfectivoTotal = String.valueOf(intAcumEfectivo);
			tpImpresora.enviaCadena(
				"---------------------------------------------------------------------------------------------------------");
			strEfectivoTotal = tpImpresora.formatMonto(strEfectivoTotal);
			strOutLine =
				"                    SUMA TOTAL :                             "
					+ strFiller.substring(0, 16 - strEfectivoTotal.length())
					+ strEfectivoTotal
					+ "  "
					+ strFiller.substring(0, 16 - strMontoTotal.length())
					+ strMontoTotal;
			tpImpresora.enviaCadena(strOutLine);
			strOutLine =
				"           TOTAL DE DOCUMENTOS :                    "
					+ intSize;
			tpImpresora.enviaCadena(strOutLine);
			if (iDriver == 0) {
				tpImpresora.InserteDocumento();
				tpImpresora.activateControl("SELECCIONASLIP",iDriver);
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.imprimeCadena1();
				tpImpresora.activateControl("SALTOHOJA",iDriver);
				tpImpresora.activateControl("SELECCIONAJOURNAL",iDriver);
			} else {
				tpImpresora.activateControl("SALTOHOJA",iDriver);
			}
		}
		return intStatus;
	}

	public int imprime9251(
		Vector vDatos,
		String strSucursal,
		String strDatosSuc,
		String strDate,
		int iDriver,
		String strIdentidad) {
		long intAcumMonto = 0;
		intStatus = tpImpresora.verificaStatus();
		if ((intStatus == 95) || (intStatus == 88)) {
			intStatus = 0;
			int intControl = 0;
			int intPagina = 1;
			tpImpresora.activateControl("CERTIFICACION",iDriver);
			datosSucursal(strDatosSuc);
			String strMoneda = (String) vDatos.elementAt(1);
			String strOpcion1 = (String) vDatos.elementAt(3);
			if (strMoneda.equals("01"))
				strMoneda = "   MONEDA NACIONAL";
			else
				strMoneda = "   MONEDA EN DOLARES";
			if (strOpcion1.equals("1"))
				strOpcion1 = "LOCALES";
			else
				strOpcion1 = "FORANEOS";
			int intSize = (vDatos.size() - 4) / 6;
			for (int i = 0; i < intSize; i++) {
				String strCodBanco = (String) vDatos.elementAt((i * 6) + 4);
				String strConsecutivo = (String) vDatos.elementAt((i * 6) + 5);
				String strCuenta = (String) vDatos.elementAt((i * 6) + 6);
				if (strCuenta.equals(""))
					strCuenta = " ";
				String strSerial = (String) vDatos.elementAt((i * 6) + 7);
				if (strSerial.equals(""))
					strSerial = " ";
				String strMonto = (String) vDatos.elementAt((i * 6) + 8);
				if (strMonto.equals(""))
					strMonto = "000";
				intAcumMonto += Long.parseLong(strMonto);
				strMonto = tpImpresora.formatMonto(strMonto);
				String strOperador = (String) vDatos.elementAt((i * 6) + 9);
				if (strOperador.equals(""))
					strOperador = "      ";
				if (intControl == 0) {
					strOutLine =
						"         "+strIdentidad+"             "
							+ strFiller.substring(0, 29)
							+ "PAGINA : "
							+ intPagina++;
					tpImpresora.enviaCadena(strOutLine);
					tpImpresora.enviaCadena("PLAZA " + strNomPza);
					strOutLine = "SUCURSAL : " + strSucursal + "  " + strNomSuc;
					tpImpresora.enviaCadena(strOutLine);
					strOutLine =
						"RELACION DE CHEQUES RECIBIDOS DE OTROS BANCOS "
							+ strOpcion1
							+ " DEL DIA "
							+ strDate.substring(7, 9)
							+ "/"
							+ strDate.substring(5, 7)
							+ "/"
							+ strDate.substring(1, 5);
					tpImpresora.enviaCadena(strOutLine);
					strOutLine = strMoneda;
					tpImpresora.enviaCadena(strOutLine);
					tpImpresora.enviaCadena(
						"------------------------------------------------------------------------------");
					tpImpresora.enviaCadena(
						"   COD BANCO  CONSECUTIVO  No. CUENTA      No.CHEQUE       IMPORTE      CAJERO");
					tpImpresora.enviaCadena(
						"------------------------------------------------------------------------------");
				}
				strOutLine =
					"   "
						+ strCodBanco
						+ strFiller.substring(0, 12 - strCodBanco.length())
						+ strConsecutivo
						+ "    "
						+ strFiller.substring(0, 11 - strCuenta.length())
						+ strCuenta
						+ "   "
						+ strFiller.substring(0, 11 - strSerial.length())
						+ strSerial
						+ "  "
						+ strFiller.substring(0, 16 - strMonto.length())
						+ strMonto
						+ "   "
						+ strOperador;
				tpImpresora.enviaCadena(strOutLine);
				intControl++;
				if ((intControl == 45) && (i != (intSize - 1))) {
					intControl = 0;
					if (iDriver == 0) {
						tpImpresora.InserteDocumento();
						tpImpresora.activateControl("SELECCIONASLIP",iDriver);
						tpImpresora.enviaCadena("");
						tpImpresora.enviaCadena("");
						tpImpresora.enviaCadena("");
						tpImpresora.enviaCadena("");
						tpImpresora.enviaCadena("");
						tpImpresora.imprimeCadena1();
						tpImpresora.activateControl("SALTOHOJA",iDriver);
						tpImpresora.activateControl("SELECCIONAJOURNAL",iDriver);
					} else {
						tpImpresora.activateControl("SALTOHOJA",iDriver);
						tpImpresora.muestraVentana();
					}
				}
			}
			String strMontoTotal = String.valueOf(intAcumMonto);
			strMontoTotal = tpImpresora.formatMonto(strMontoTotal);
			tpImpresora.enviaCadena(
				"------------------------------------------------------------------------------");
			strOutLine =
				"                    SUMA TOTAL :                     "
					+ strFiller.substring(0, 16 - strMontoTotal.length())
					+ strMontoTotal;
			tpImpresora.enviaCadena(strOutLine);
			strOutLine =
				"           TOTAL DE DOCUMENTOS :                                 "
					+ intSize;
			tpImpresora.enviaCadena(strOutLine);
			if (iDriver == 0) {
				tpImpresora.InserteDocumento();
				tpImpresora.activateControl("SELECCIONASLIP",iDriver);
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.imprimeCadena1();
				tpImpresora.activateControl("SALTOHOJA",iDriver);
				tpImpresora.activateControl("SELECCIONAJOURNAL",iDriver);
			} else {
				tpImpresora.activateControl("SALTOHOJA",iDriver);
			}
		}
		return intStatus;
	}

	public int imprimeRelOP(String strRespuesta, String strDate, int iDriver, String strIdentidad) {
		Vector vCampos = new Vector();
		String strBanco = new String("   ");
		Vector vBancos = new Vector();
		Vector vTmp = new Vector();
		Vector vTTmp = new Vector();
		String strTmp = new String("   ");
		String strCodigo = new String("   ");
		int intControl = 0;
		int intPagina = 1;
		intStatus = tpImpresora.verificaStatus();
		if ((intStatus == 95) || (intStatus == 88)) {
			intStatus = 0;
			int intOpcion = 0;
			int i = 0;
			vTTmp = tpImpresora.Campos(strRespuesta, "^");
			int intSize = vTTmp.size();
			for (i = 0; i < intSize; i++) {
				strTmp = (String) vTTmp.elementAt(i);
				if (strTmp.startsWith("MOVIMIENTOS_OP1")) {
					strTmp = (String) vTTmp.elementAt(i);
					vTmp = tpImpresora.Campos(strTmp, "@");
					strTmp = (String) vTmp.elementAt(1);
					vCampos = tpImpresora.Campos(strTmp, "~");
					intOpcion = 1;
				}
				if (strTmp.startsWith("MOVIMIENTOS_OP2")) {
					strTmp = (String) vTTmp.elementAt(i);
					vTmp = tpImpresora.Campos(strTmp, "@");
					strTmp = (String) vTmp.elementAt(1);
					vCampos = tpImpresora.Campos(strTmp, "~");
					//System.out.println("vCampos"  + vCampos + "size " + vCampos.size());
					intOpcion = 2;
				}
				if (strTmp.startsWith("DATOS_BANCO")) {
					strTmp = (String) vTTmp.elementAt(i);
					vTmp = tpImpresora.Campos(strTmp, "@");
					strTmp = (String) vTmp.elementAt(1);
					//System.out.println("strTmp "  + strTmp);																												
					strTmp = strTmp.substring(3, strTmp.length() - 2);
					//System.out.println("strTmp "  + strTmp);				
					if (strTmp.equals(""))
						vBancos.add(" ");
					else
						vBancos.add(strTmp);
				}
			}
			tpImpresora.activateControl("CERTIFICACION",iDriver);
			if (intOpcion == 1) {
				intControl = 0;
				intSize = vCampos.size() / 2;
				int intBanco = 0;
				String strMonto = new String("000");
				String strBancoAnt = new String("000");
				String strMontoAnt = new String("000");
				int intMonto = 0;
				int intMontoAnt = 0;
				int intEncabezado = 0;
				int intTotalOrd = 0;
				int intMontoAcum = 0;
				String strCodigoAnt = new String("");
				strCodigoAnt = (String) vCampos.elementAt(0);
				for (int intCont = 0; intCont < intSize; intCont++) {
					strBanco = (String) vBancos.elementAt(intCont);
					if (strBanco.length() > 0)
						strBanco = strBanco.replace('�', 'e');
					strCodigo = (String) vCampos.elementAt(intCont * 2);
					if (strCodigo.startsWith("|"))
						strCodigo = strCodigo.substring(1, strCodigo.length());
					if ((intControl == 0) && (intEncabezado == 0)) {
						intEncabezado = 1;
						tpImpresora.enviaCadena(
							"                    TRANSMISION DE ORDENES DE PAGO\n");
						strOutLine =
							"      "+strIdentidad+"          "
								+ strFiller.substring(0, 28)
								+ "     PAGINA : "
								+ intPagina++;
						tpImpresora.enviaCadena(strOutLine);
						strOutLine =
							"DEL DIA  : "
								+ strDate.substring(7, 9)
								+ "/"
								+ strDate.substring(5, 7)
								+ "/"
								+ strDate.substring(1, 5);
						tpImpresora.enviaCadena(strOutLine);
						tpImpresora.enviaCadena(
							"------------------------------------------------------------------------------");
						tpImpresora.enviaCadena(
							"   NUMERO           NOMBRE               NUM. ORDENES          MONTO          ");
						tpImpresora.enviaCadena(
							"------------------------------------------------------------------------------");
					}
					strMonto = (String) vCampos.elementAt(intCont * 2 + 1);
					if (strMonto.equals(""))
						strMonto = "000";
					intMonto += Integer.parseInt(strMonto);
					intMontoAcum += Integer.parseInt(strMonto);
					if (!strCodigo.equals(strCodigoAnt)) {
						intMontoAnt = intMonto - Integer.parseInt(strMonto);
						strMontoAnt = String.valueOf(intMontoAnt);
						strMontoAnt = tpImpresora.formatMonto(strMontoAnt);
						intMontoAnt = Integer.parseInt(strMonto);
						intMonto = Integer.parseInt(strMonto);
						if (strBancoAnt.length() > 30)
							strBancoAnt = strBancoAnt.substring(0, 30);
						strOutLine =
							"   "
								+ strCodigoAnt
								+ "     "
								+ strBancoAnt
								+ strFiller.substring(
									0,
									30 - strBancoAnt.length())
								+ "      "
								+ intBanco
								+ "           "
								+ strFiller.substring(
									0,
									16 - strMontoAnt.length())
								+ strMontoAnt;
						tpImpresora.enviaCadena(strOutLine);
						intControl++;
						if (intControl > 45) {
							if (iDriver == 0) {
								tpImpresora.InserteDocumento();
								tpImpresora.activateControl("SELECCIONASLIP",iDriver);
								tpImpresora.enviaCadena("");
								tpImpresora.enviaCadena("");
								tpImpresora.enviaCadena("");
								tpImpresora.enviaCadena("");
								tpImpresora.enviaCadena("");
								tpImpresora.imprimeCadena1();
								tpImpresora.activateControl("SALTOHOJA",iDriver);
								tpImpresora.activateControl(
									"SELECCIONAJOURNAL",iDriver);
							} else {
								tpImpresora.activateControl("SALTOHOJA",iDriver);
								tpImpresora.muestraVentana();
							}
							intControl = 0;
							intEncabezado = 0;
						}
						strBancoAnt = strBanco;
						strCodigoAnt = strCodigo;
						intTotalOrd += intBanco;
						intBanco = 1;
					} else {
						intMontoAnt = intMonto;
						strBancoAnt = strBanco;
						strCodigoAnt = strCodigo;
						intBanco++;
					}
				}
				intTotalOrd += intBanco;
				strMontoAnt = String.valueOf(intMontoAnt);
				strMontoAnt = tpImpresora.formatMonto(strMontoAnt);
				if (strBancoAnt.length() > 30)
					strBancoAnt = strBancoAnt.substring(0, 30);
				strOutLine =
					"   "
						+ strCodigoAnt
						+ "     "
						+ strBancoAnt
						+ strFiller.substring(0, 30 - strBancoAnt.length())
						+ "      "
						+ intBanco
						+ "           "
						+ strFiller.substring(0, 16 - strMontoAnt.length())
						+ strMontoAnt;
				intControl++;
				tpImpresora.enviaCadena(strOutLine);
				strMontoAnt = String.valueOf(intMontoAcum);
				strMontoAnt = tpImpresora.formatMonto(strMontoAnt);
				tpImpresora.enviaCadena("\n");
				strOutLine =
					strFiller.substring(0, 40)
						+ "TOTAL DE ORDENES : "
						+ intTotalOrd;
				tpImpresora.enviaCadena(strOutLine);
				strOutLine =
					strFiller.substring(0, 45)
						+ "MONTO TOTAL : "
						+ strFiller.substring(0, 16 - strMontoAnt.length())
						+ strMontoAnt;
				tpImpresora.enviaCadena(strOutLine);
				if (intControl != 0)
					if (iDriver == 0) {
						tpImpresora.InserteDocumento();
						tpImpresora.activateControl("SELECCIONASLIP",iDriver);
						tpImpresora.enviaCadena("");
						tpImpresora.enviaCadena("");
						tpImpresora.enviaCadena("");
						tpImpresora.enviaCadena("");
						tpImpresora.enviaCadena("");
						tpImpresora.imprimeCadena1();
						tpImpresora.activateControl("SALTOHOJA",iDriver);
						tpImpresora.activateControl("SELECCIONAJOURNAL",iDriver);
					} else {
						tpImpresora.activateControl("SALTOHOJA",iDriver);
					}
			} else {
				if (intOpcion == 2) {
					intSize = vCampos.size() / 8;
					int intContador = 1;
					intPagina = 1;
					int intDoctos = 7;
					long intAcumMonto = 0;
					String strMonto = new String("");
					for (i = 0; i < intSize; i++) {
						strCodigo = (String) vCampos.elementAt((8 * i) + 0);
						String strFecha =
							(String) vCampos.elementAt((8 * i) + 1);
						String strNumOr =
							(String) vCampos.elementAt((8 * i) + 2);
						String strOrdte =
							(String) vCampos.elementAt((8 * i) + 3);
						String strBenef =
							(String) vCampos.elementAt((8 * i) + 4);
						String strCdad =
							(String) vCampos.elementAt((8 * i) + 5);
						String strInstr =
							(String) vCampos.elementAt((8 * i) + 6);
						strMonto = (String) vCampos.elementAt((8 * i) + 7);
						if (vBancos.size() > i)
							strBanco = (String) vBancos.elementAt(i);
						else
							strBanco = " ";
						if (strBanco.length() > 0)
							strBanco = strBanco.replace('�', 'e');
						if (intContador == 1) {
							tpImpresora.enviaCadena(
								"                   TRANSMISION DE ORDENES DE PAGO                  ");
							strOutLine =
								"      "+strIdentidad+"                                                    PAGINA "
									+ intPagina++;
							tpImpresora.enviaCadena(strOutLine);
						}
						if (strCodigo.startsWith("|"))
							strCodigo =
								strCodigo.substring(1, strCodigo.length());
						strOutLine =
							strCodigo
								+ strBanco
								+ strFiller.substring(0, 40 - strBanco.length())
								+ "DEL DIA "
								+ strFecha.substring(6, 8)
								+ "/"
								+ strFecha.substring(4, 6)
								+ "/"
								+ strFecha.substring(0, 4)
								+ "\n\n";
						tpImpresora.enviaCadena(strOutLine);
						intAcumMonto += Long.parseLong(strMonto);
						strOutLine =
							"Num. Orden    : "
								+ strNumOr
								+ strFiller.substring(0, 30 - strNumOr.length())
								+ "Monto : $"
								+ tpImpresora.formatMonto(strMonto);
						tpImpresora.enviaCadena(strOutLine);
						strOutLine = "Ordenante     : " + strOrdte;
						tpImpresora.enviaCadena(strOutLine);
						strOutLine = "Beneficiario  : " + strBenef;
						tpImpresora.enviaCadena(strOutLine);
						strOutLine = "Ciudad        : " + strCdad;
						tpImpresora.enviaCadena(strOutLine);
						strOutLine = "Instrucciones : " + strInstr + "\n";
						tpImpresora.enviaCadena(strOutLine);
						intContador++;
						if (intContador == intDoctos) {
							if (iDriver == 0) {
								tpImpresora.InserteDocumento();
								tpImpresora.activateControl("SELECCIONASLIP",iDriver);
								tpImpresora.enviaCadena("");
								tpImpresora.enviaCadena("");
								tpImpresora.enviaCadena("");
								tpImpresora.enviaCadena("");
								tpImpresora.enviaCadena("");
								tpImpresora.imprimeCadena1();
								tpImpresora.activateControl("SALTOHOJA",iDriver);
								tpImpresora.activateControl(
									"SELECCIONAJOURNAL",iDriver);
							} else {
								tpImpresora.activateControl("SALTOHOJA",iDriver);
								tpImpresora.muestraVentana();
							}
							intContador = 1;
							intPagina++;
						}
					}
					if (intContador == 1) {
						tpImpresora.enviaCadena(
							"                   TRANSMISION DE ORDENES DE PAGO                  ");
						strOutLine =
							"      "+strIdentidad+"                                                    PAGINA "
								+ intPagina++;
						tpImpresora.enviaCadena(strOutLine);
					}
					strOutLine =
						"                                        Total de Ordenes : "
							+ i;
					tpImpresora.enviaCadena(strOutLine);
					strMonto = String.valueOf(intAcumMonto);
					strOutLine =
						"                                        Monto Total      : "
							+ tpImpresora.formatMonto(strMonto);
					tpImpresora.enviaCadena(strOutLine);
					if (iDriver == 0) {
						tpImpresora.InserteDocumento();
						tpImpresora.activateControl("SELECCIONASLIP",iDriver);
						tpImpresora.enviaCadena("");
						tpImpresora.enviaCadena("");
						tpImpresora.enviaCadena("");
						tpImpresora.enviaCadena("");
						tpImpresora.enviaCadena("");
						tpImpresora.imprimeCadena1();
						tpImpresora.activateControl("SALTOHOJA",iDriver);
						tpImpresora.activateControl("SELECCIONAJOURNAL",iDriver);
					} else {
						tpImpresora.activateControl("SALTOHOJA",iDriver);
					}
				} else {
					tpImpresora.enviaCadena(
						"                   TRANSMISION DE ORDENES DE PAGO                  ");
					tpImpresora.enviaCadena(
						"                 NO SE HAN REALIZADO TRANSACCIONES                 ");
					if (iDriver == 0) {
						tpImpresora.InserteDocumento();
						tpImpresora.activateControl("SELECCIONASLIP",iDriver);
						tpImpresora.enviaCadena("");
						tpImpresora.enviaCadena("");
						tpImpresora.enviaCadena("");
						tpImpresora.enviaCadena("");
						tpImpresora.enviaCadena("");
						tpImpresora.imprimeCadena1();
						tpImpresora.activateControl("SALTOHOJA",iDriver);
						tpImpresora.activateControl("SELECCIONAJOURNAL",iDriver);
					} else {
						tpImpresora.activateControl("SALTOHOJA",iDriver);
					}
				}
			}
		}
		return intStatus;
	}

	public int imprime9821(
		Vector vDatos,
		String strDatosSuc,
		String strDate,
		int iDriver,
		String strIdentidad) {
		intStatus = tpImpresora.verificaStatus();
		if ((intStatus == 95) || (intStatus == 88)) {
			intStatus = 0;
			int intControl = 0;
			int intPagina = 1;
			tpImpresora.activateControl("CERTIFICACION",iDriver);
			datosSucursal(strDatosSuc);
			String strMoneda = (String) vDatos.elementAt(1);
			String strTxn = (String) vDatos.elementAt(2);
			if (strMoneda.equals("01"))
				strMoneda = "   MONEDA NACIONAL";
			else
				strMoneda = "   MONEDA EN DOLARES";
			int intSize = (vDatos.size() - 3) / 6;
			for (int i = 0; i < intSize; i++) {
				String strConsecutivo = (String) vDatos.elementAt((i * 6) + 3);
				String strCuenta = (String) vDatos.elementAt((i * 6) + 4);
				if (strCuenta.equals(""))
					strCuenta = " ";
				String strReferencia = (String) vDatos.elementAt((i * 6) + 5);
				if (strReferencia.equals(""))
					strReferencia = " ";
				String strDescripcion = (String) vDatos.elementAt((i * 6) + 6);
				if (strDescripcion.equals(""))
					strDescripcion = " ";
				String strMonto = (String) vDatos.elementAt((i * 6) + 7);
				if (strMonto.equals(""))
					strMonto = "000";
				String strCajero = (String) vDatos.elementAt((i * 6) + 8);
				if (strCajero.equals(""))
					strMonto = "000";
				strMonto = tpImpresora.formatMonto(strMonto);
				if (intControl == 0) {
					strOutLine =
						"         "+strIdentidad+"             "
							+ strFiller.substring(0, 29)
							+ "  PAGINA : "
							+ intPagina++;
					tpImpresora.enviaCadena(strOutLine);
					tpImpresora.enviaCadena("PLAZA " + strNomPza);
					strOutLine =
						"SUCURSAL :  " + strNumSuc + "    " + strNomSuc;
					tpImpresora.enviaCadena(strOutLine);
					strOutLine =
						"RELACION DE TRANSACCION "
							+ strTxn
							+ " DEL DIA "
							+ strDate.substring(7, 9)
							+ "/"
							+ strDate.substring(5, 7)
							+ "/"
							+ strDate.substring(1, 5)
							+ "           "
							+ strMoneda;
					tpImpresora.enviaCadena(strOutLine);
					tpImpresora.enviaCadena(
						"------------------------------------------------------------------------------");
					tpImpresora.enviaCadena(
						"CONSECUTIVO   CUENTA     REFERENCIA      DESCRIPCION        MONTO       CAJERO");
					tpImpresora.enviaCadena(
						"------------------------------------------------------------------------------");
				}
				if (strReferencia.length() > 14)
					strReferencia = strReferencia.substring(0, 14);
				if (strDescripcion.length() > 17)
					strDescripcion = strDescripcion.substring(0, 17);
				strOutLine =
					"  "
						+ strConsecutivo
						+ " "
						+ strCuenta
						+ strFiller.substring(0, 10 - strCuenta.length())
						+ " "
						+ strReferencia
						+ strFiller.substring(0, 14 - strReferencia.length())
						+ " "
						+ strDescripcion
						+ strFiller.substring(0, 17 - strDescripcion.length())
						+ strFiller.substring(0, 16 - strMonto.length())
						+ strMonto
						+ " "
						+ strCajero;
				tpImpresora.enviaCadena(strOutLine);
				intControl++;
				if ((intControl == 45) && (i != (intSize - 1))) {
					intControl = 0;
					if (iDriver == 0) {
						tpImpresora.InserteDocumento();
						tpImpresora.activateControl("SELECCIONASLIP",iDriver);
						tpImpresora.enviaCadena("");
						tpImpresora.enviaCadena("");
						tpImpresora.enviaCadena("");
						tpImpresora.enviaCadena("");
						tpImpresora.enviaCadena("");
						tpImpresora.imprimeCadena1();
						tpImpresora.activateControl("SALTOHOJA",iDriver);
						tpImpresora.activateControl("SELECCIONAJOURNAL",iDriver);
					} else {
						tpImpresora.activateControl("SALTOHOJA",iDriver);
						tpImpresora.muestraVentana();
					}
				}
			}
			if (iDriver == 0) {
				tpImpresora.InserteDocumento();
				tpImpresora.activateControl("SELECCIONASLIP",iDriver);
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.imprimeCadena1();
				tpImpresora.activateControl("SALTOHOJA",iDriver);
				tpImpresora.activateControl("SELECCIONAJOURNAL",iDriver);
			} else {
				tpImpresora.activateControl("SALTOHOJA",iDriver);
			}
		}
		return intStatus;
	}

	public int imprime9640(
		Vector vDatos,
		String strDatosSuc,
		String strDate,
		int iDriver,
		String strIdentidad) {
		intStatus = tpImpresora.verificaStatus();
		BigInteger bAcum = new BigInteger("0");
		BigInteger bAcumCE = new BigInteger("0");
		BigInteger bAcumCA = new BigInteger("0");
		BigInteger bAcumR = new BigInteger("0");
		String strMonto = new String("");
		String strCuentaAnt = new String("");
		int intbTotales = 0;
		int intR = 0;
		int intT = 0;
		int intCA = 0;
		int intCE = 0;
		if ((intStatus == 95) || (intStatus == 88)) {
			intStatus = 0;
			int intControl = 0;
			int intPagina = 1;
			String strTxn = new String("");
			tpImpresora.activateControl("CERTIFICACION",iDriver);
			datosSucursal(strDatosSuc);
			String strMoneda = (String) vDatos.elementAt(1);
			String strOpcion1 = (String) vDatos.elementAt(2);
			String strOpcion2 = (String) vDatos.elementAt(3);
			int intOpcion = Integer.parseInt(strOpcion2);
			String strHora = (String) vDatos.elementAt(4);
			if ((strHora == null) || strHora.equals("null"))
				strHora = "0150000";
			int intHora = Integer.parseInt(strHora);
			int intSize = (vDatos.size() - 5) / 10;
			if (strMoneda.equals("01") && strOpcion1.equals("1"))
				strTxn = "REMESAS          ";
			else if (strMoneda.equals("01") && strOpcion1.equals("2"))
				strTxn = "TOTALES          ";
			else if (strMoneda.equals("02") && strOpcion1.equals("1"))
				strTxn = "A.E. TRAVELER EF.";
			else if (strMoneda.equals("02") && strOpcion1.equals("2"))
				strTxn = "A.E. TRAVELER AB. CTA";
			else if (strMoneda.equals("02") && strOpcion1.equals("3"))
				strTxn = "REMESAS          ";
			else if (strMoneda.equals("02") && strOpcion1.equals("4"))
				strTxn = "TOTALES          ";
			String strTituloMon = new String("");
			if (strMoneda.equals("01"))
				strTituloMon = "   MONEDA NACIONAL";
			else
				strTituloMon = "   MONEDA EN DOLARES";
			int intOk = 0;
			int intImprime = 0;
			for (int i = 0; i < intSize; i++) {
				String strCodigo = (String) vDatos.elementAt((i * 10) + 5);
				String strConsecutivo = (String) vDatos.elementAt((i * 10) + 6);
				int intConsecutivo = Integer.parseInt(strConsecutivo);
				String strCuenta = (String) vDatos.elementAt((i * 10) + 7);
				if (strCuenta.equals(""))
					strCuenta = " ";
				String strSerial = (String) vDatos.elementAt((i * 10) + 8);
				if (strSerial.equals(""))
					strSerial = " ";
				strMonto = (String) vDatos.elementAt((i * 10) + 9);
				if (strMonto.equals(""))
					strMonto = "000";
				String strCajero = (String) vDatos.elementAt((i * 10) + 10);
				String strRefer1 = (String) vDatos.elementAt((i * 10) + 11);
				String strRefer2 = (String) vDatos.elementAt((i * 10) + 12);
				String strDesc = (String) vDatos.elementAt((i * 10) + 13);
				String strTxnD = (String) vDatos.elementAt((i * 10) + 14);
				if (strCajero.equals(""))
					strCajero = " ";
				if ((intControl == 0) && (intOk == 0)) {
					intOk = 1;
					strOutLine =
						"         "+strIdentidad+"             "
							+ strFiller.substring(0, 29)
							+ "PAGINA : "
							+ intPagina++;
					tpImpresora.enviaCadena(strOutLine);
					String strTipoCorte = "  ANTES DEL CORTE";
					if (intOpcion == 2)
						strTipoCorte = "  DESPUES DEL CORTE";
					strOutLine =
						"PLAZA "
							+ strNomPza
							+ strFiller.substring(0, 47 - strNomPza.length())
							+ strTipoCorte;
					tpImpresora.enviaCadena(strOutLine);
					strOutLine =
						"SUCURSAL :  " + strNumSuc + "    " + strNomSuc;
					tpImpresora.enviaCadena(strOutLine);
					strOutLine =
						"RELACION DE "
							+ strTxn
							+ " DEL DIA "
							+ strDate.substring(7, 9)
							+ "/"
							+ strDate.substring(5, 7)
							+ "/"
							+ strDate.substring(1, 5)
							+ "    "
							+ strTituloMon;
					tpImpresora.enviaCadena(strOutLine);
					tpImpresora.enviaCadena(
						"------------------------------------------------------------------------------");
					if ((strMoneda.equals("01") && strOpcion1.equals("2"))
						|| (strMoneda.equals("02") && strOpcion1.equals("4"))) {
						intbTotales = 1;
						tpImpresora.enviaCadena(
							"              RUBRO               NUMERO DE DOCUMENTOS           IMPORTE   ");
					} else
						tpImpresora.enviaCadena(
							" CODIGO BANCO   CONSECUTIVO   CUENTA     No. CHEQUE      IMPORTE     CAJERO");
					tpImpresora.enviaCadena(
						"------------------------------------------------------------------------------");
				}
				if (strTxnD.equals("0510")) {
					strCodigo = strSerial;
					strConsecutivo = strRefer1;
					strCuenta = strRefer2;
					strSerial = strDesc;
				}
				if (strTxnD.equals("4303")) {
					strCuentaAnt = strCuenta;
					intImprime = 3;
				} else if (
					((intOpcion == 1) && (intConsecutivo < intHora))
						|| ((intOpcion == 2) && (intConsecutivo >= intHora))) {
					if (strTxnD.equals("0602")) {
						strCuenta = " ";
						strCuentaAnt = strCuenta;
						intImprime = 1;
						bAcumCE = bAcumCE.add(new BigInteger(strMonto));
						intCE++;
					} else if (strCodigo.startsWith("6")) {
						intImprime = 1;
						bAcumCA = bAcumCA.add(new BigInteger(strMonto));
						intCA++;
					} else {
						intImprime = 2;
						bAcumR = bAcumR.add(new BigInteger(strMonto));
						intR++;
					}
					bAcum = bAcum.add(new BigInteger(strMonto));
					intT++;
				}
				strMonto = tpImpresora.formatMonto(strMonto);
				if ((strMoneda.equals("01")
					&& strOpcion1.equals("1")
					&& (intImprime == 2))
					|| (strMoneda.equals("02")
						&& strOpcion1.equals("3")
						&& (intImprime == 2)))
					intImprime = 1;
				if (intbTotales == 0) {
					if (intImprime == 1) {
						intImprime = 0;
						if ((strCuenta.length() < 5)
							&& (!strCuentaAnt.equals("")))
							strCuenta = strCuentaAnt;
						strOutLine =
							"    "
								+ strFiller.substring(0, 4 - strCodigo.length())
								+ strCodigo
								+ "         "
								+ strFiller.substring(
									0,
									8 - strConsecutivo.length())
								+ strConsecutivo
								+ " "
								+ strFiller.substring(0, 12 - strCuenta.length())
								+ strCuenta
								+ " "
								+ strFiller.substring(0, 12 - strSerial.length())
								+ strSerial
								+ " "
								+ strFiller.substring(0, 16 - strMonto.length())
								+ strMonto
								+ "  "
								+ strCajero;
						if (((intOpcion == 1) && (intConsecutivo < intHora))
							|| ((intOpcion == 2)
								&& (intConsecutivo >= intHora))) {
							tpImpresora.enviaCadena(strOutLine);
							intControl++;
							if ((intControl == 45) && (i != (intSize - 1))) {
								intOk = 0;
								intControl = 0;
								if (iDriver == 0) {
									tpImpresora.InserteDocumento();
									tpImpresora.activateControl(
										"SELECCIONASLIP",iDriver);
									tpImpresora.enviaCadena("");
									tpImpresora.enviaCadena("");
									tpImpresora.enviaCadena("");
									tpImpresora.enviaCadena("");
									tpImpresora.enviaCadena("");
									tpImpresora.imprimeCadena1();
									tpImpresora.activateControl("SALTOHOJA",iDriver);
									tpImpresora.activateControl(
										"SELECCIONAJOURNAL",iDriver);
								} else {
									tpImpresora.activateControl("SALTOHOJA",iDriver);
									tpImpresora.muestraVentana();
								}
							}
						}
					}
				}
			}
			if ((strMoneda.equals("01") && strOpcion1.equals("1"))
				|| (strMoneda.equals("02") && strOpcion1.equals("1"))
				|| (strMoneda.equals("02") && strOpcion1.equals("2"))
				|| (strMoneda.equals("02") && strOpcion1.equals("3"))) {
				if ((strMoneda.equals("01") && strOpcion1.equals("1")))
					strMonto = bAcumR.toString();
				else if (strMoneda.equals("02") && strOpcion1.equals("1"))
					strMonto = bAcumCE.toString();
				else if (strMoneda.equals("02") && strOpcion1.equals("2"))
					strMonto = bAcumCA.toString();
				else if (strMoneda.equals("02") && strOpcion1.equals("3"))
					strMonto = bAcumR.toString();
				strMonto = tpImpresora.formatMonto(strMonto);
				if (intSize < 9)
					strOutLine =
						"\n\n               TOTAL DE DOCUMENTOS  "
							+ intT
							+ "    TOTAL  : $ "
							+ strFiller.substring(0, 16 - strMonto.length())
							+ strMonto;
				else
					strOutLine =
						"\n\n               TOTAL DE DOCUMENTOS  "
							+ intT
							+ "   TOTAL  : $ "
							+ strFiller.substring(0, 16 - strMonto.length())
							+ strMonto;
				tpImpresora.enviaCadena(strOutLine);
			} else {
				if (strMoneda.equals("02") && strOpcion1.equals("4")) {
					strMonto = bAcumCE.toString();
					strMonto = tpImpresora.formatMonto(strMonto);
					if (intCE < 9)
						strOutLine =
							" 1. A.E. TRAVELER  EN EFECTIVO "
								+ intCE
								+ "               "
								+ strFiller.substring(0, 16 - strMonto.length())
								+ strMonto;
					else
						strOutLine =
							" 1. A.E. TRAVELER  EN EFECTIVO "
								+ intCE
								+ "              "
								+ strFiller.substring(0, 16 - strMonto.length())
								+ strMonto;
					tpImpresora.enviaCadena(strOutLine);
					strMonto = bAcumCA.toString();
					strMonto = tpImpresora.formatMonto(strMonto);
					if (intCA < 9)
						strOutLine =
							" 2. A.E. TRAVELER  ABONO CTA   "
								+ intCA
								+ "               "
								+ strFiller.substring(0, 16 - strMonto.length())
								+ strMonto;
					else
						strOutLine =
							" 2. A.E. TRAVELER  ABONO CTA   "
								+ intCA
								+ "              "
								+ strFiller.substring(0, 16 - strMonto.length())
								+ strMonto;
					tpImpresora.enviaCadena(strOutLine);
					strMonto = bAcumR.toString();
					strMonto = tpImpresora.formatMonto(strMonto);
					if (intR < 9)
						strOutLine =
							" 3. REMESAS                    "
								+ intR
								+ "               "
								+ strFiller.substring(0, 16 - strMonto.length())
								+ strMonto;
					else
						strOutLine =
							" 3. REMESAS                    "
								+ intR
								+ "              "
								+ strFiller.substring(0, 16 - strMonto.length())
								+ strMonto;
					tpImpresora.enviaCadena(strOutLine);
					strMonto = bAcumCE.toString();
					bAcumR = bAcumR.add(new BigInteger(strMonto));
					strMonto = bAcumCA.toString();
					bAcumR = bAcumR.add(new BigInteger(strMonto));
					strMonto = bAcumR.toString();
					strMonto = tpImpresora.formatMonto(strMonto);
					if (intSize < 9)
						strOutLine =
							"\n\n                      TOTAL DE DOCUMENTOS  "
								+ intT
								+ " TOTAL : $ "
								+ strFiller.substring(0, 16 - strMonto.length())
								+ strMonto;
					else
						strOutLine =
							"\n\n                      TOTAL DE DOCUMENTOS  "
								+ intT
								+ " TOTAL : $"
								+ strFiller.substring(0, 16 - strMonto.length())
								+ strMonto;
					tpImpresora.enviaCadena(strOutLine);
				}
				if (strMoneda.equals("01") && strOpcion1.equals("2")) {
					strMonto = bAcumR.toString();
					strMonto = tpImpresora.formatMonto(strMonto);
					tpImpresora.enviaCadena(
						" 1. A. E. TRAVELER EN EFECTIVO             0                       0.00            ");
					tpImpresora.enviaCadena(
						" 2. A. E. TRAVELER ABONO A CUENTA          0                       0.00            ");
					if (intT < 9)
						strOutLine =
							" 3. REMESAS                                "
								+ intT
								+ "           "
								+ strFiller.substring(0, 16 - strMonto.length())
								+ strMonto;
					else
						strOutLine =
							" 3. REMESAS                                "
								+ intT
								+ "          "
								+ strFiller.substring(0, 16 - strMonto.length())
								+ strMonto;
					tpImpresora.enviaCadena(strOutLine);
					if (intSize < 9)
						strOutLine =
							"\n\n                      TOTAL DE DOCUMENTOS  "
								+ intT
								+ " TOTAL : $ "
								+ strFiller.substring(0, 16 - strMonto.length())
								+ strMonto;
					else
						strOutLine =
							"\n\n                      TOTAL DE DOCUMENTOS  "
								+ intT
								+ " TOTAL : $"
								+ strFiller.substring(0, 16 - strMonto.length())
								+ strMonto;
					tpImpresora.enviaCadena(strOutLine);
				}
			}
			if (iDriver == 0) {
				tpImpresora.InserteDocumento();
				tpImpresora.activateControl("SELECCIONASLIP",iDriver);
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.imprimeCadena1();
				tpImpresora.activateControl("SALTOHOJA",iDriver);
				tpImpresora.activateControl("SELECCIONAJOURNAL",iDriver);
			} else {
				tpImpresora.activateControl("SALTOHOJA",iDriver);
			}
		}
		return intStatus;
	}

	public int imprime0250(
		Vector vDatos,
		String strDate,
		String strTime,
		String strCajero,
		int iDriver,
		String strIdentidad) {
		intStatus = tpImpresora.verificaStatus();
		String strJustificacion = "";
		if ((intStatus == 95) || (intStatus == 88)) {
			intStatus = 0;
			tpImpresora.activateControl("CERTIFICACION",iDriver);
			strOutLine = (String) vDatos.elementAt(3);
			strOutLine = strOutLine + " " + strDate + " " + strTime + " ";
			strOutLine =
				strJustificacion
					+ strOutLine
					+ (String) vDatos.elementAt(0)
					+ " "
					+ strCajero
					+ " "
					+ strCajero.substring(0, 4)
					+ "\n";
			tpImpresora.enviaCadena(strOutLine);
			strTmp = (String) vDatos.elementAt(2);
			int lines = Integer.parseInt(strTmp);
			String szResp = (String) vDatos.elementAt(4);
			StringBuffer RespSB = new StringBuffer(szResp);
			for (int y = 0; y < RespSB.length(); y++)
				if (RespSB.charAt(y) == 0x00)
					RespSB.replace(y, y + 1, " ");
			szResp = RespSB.toString();
			for (int i = 0; i < lines; i++) {
				strOutLine =
					strJustificacion
						+ szResp.substring(
							i * 78,
							Math.min(i * 78 + 77, szResp.length()));
				tpImpresora.enviaCadena(strOutLine);
				if (Math.min(i * 78 + 77, szResp.length()) == szResp.length())
					break;
			}
			if (iDriver == 0) {
				tpImpresora.InserteDocumento();
				tpImpresora.activateControl("SELECCIONASLIP",iDriver);
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.imprimeCadena1();
				tpImpresora.activateControl("SALTOHOJA",iDriver);
				tpImpresora.activateControl("SELECCIONAJOURNAL",iDriver);
			} else {
				tpImpresora.activateControl("SALTOHOJA",iDriver);
			}
		}
		return intStatus;
	}

	public int imprimeRelGiro(
		Vector vDatos,
		String strDate,
		String strTime,
		int iDriver,
		String strIdentidad) {
		intStatus = tpImpresora.verificaStatus();

		if ((intStatus == 95) || (intStatus == 88)) {
			intStatus = 0;
			int intControl = 0;
			int intPagina = 1;
			String strTituloMon = "";
			//String strTxn = new String("");
			tpImpresora.activateControl("CERTIFICACION",iDriver);
			String strMoneda = (String) vDatos.elementAt(1);
			String strTotal = (String) vDatos.elementAt(2);
			String strDivisa = "$   ";
			int intSize = (vDatos.size() - 3) / 6;
			if (strMoneda.equals("01"))
				strTituloMon = "MONEDA NACIONAL";
			else {
				strTituloMon = "MONEDA EN DOLARES";
				strDivisa = "USD ";

			}
			for (int i = 0; i < intSize; i++) {
				String strGiro = (String) vDatos.elementAt((i * 6) + 3);
				String strBeneficiario = (String) vDatos.elementAt((i * 6) + 4);
				String strImporte = (String) vDatos.elementAt((i * 6) + 5);
				String strBanco = (String) vDatos.elementAt((i * 6) + 6);
				String strStatus = (String) vDatos.elementAt((i * 6) + 7);
				String strPlaza = (String) vDatos.elementAt((i * 6) + 8);
				if (intControl == 0) {
					intControl = 1;
					strOutLine =
						strFiller.substring(0, 54)
							+ strIdentidad+"       "
							+ strFiller.substring(0, 54)
							+ "PAGINA : "
							+ intPagina++;
					tpImpresora.enviaCadena(strOutLine);
					strOutLine = "PLAZA " + strNomPza;
					tpImpresora.enviaCadena(strOutLine);
					strOutLine =
						"SUCURSAL :  " + strNumSuc + "    " + strNomSuc;
					tpImpresora.enviaCadena(strOutLine);
					strOutLine =
						"RELACION DE GIROS EN "
							+ strTituloMon
							+ " DEL DIA "
							+ strDate.substring(7, 9)
							+ "/"
							+ strDate.substring(5, 7)
							+ "/"
							+ strDate.substring(1, 5);
					tpImpresora.enviaCadena(strOutLine);
					tpImpresora.enviaCadena(
						"__________________________________________________________________________________________________________________________________");
					tpImpresora.enviaCadena(
						"    GIRO               BENEFICIARIO                      IMPORTE            BANCO CORRESPONSAL       ST           PLAZA           ");
					tpImpresora.enviaCadena(
						"__________________________________________________________________________________________________________________________________");
				}
				if (strBanco.length() > 30)
					strBanco = strBanco.substring(0, 30);
				if (strBeneficiario.length() > 35)
					strBeneficiario = strBeneficiario.substring(0, 35);
				strImporte = tpImpresora.formatMonto(strImporte);
				strOutLine =
					" "
						+ strGiro
						+ strFiller.substring(0, 10 - strGiro.length())
						+ " "
						+ strBeneficiario
						+ strFiller.substring(0, 35 - strBeneficiario.length())
						+ " "
						+ strDivisa
						+ strFiller.substring(0, 17 - strImporte.length())
						+ strImporte
						+ " "
						+ strBanco
						+ strFiller.substring(0, 30 - strBanco.length())
						+ " "
						+ strStatus
						+ "  "
						+ strPlaza;
				tpImpresora.enviaCadena(strOutLine);
				intControl++;
				if ((intControl == 45) && (i != (intSize - 1))) {
					intControl = 0;
					if (iDriver == 0) {
						tpImpresora.InserteDocumento();
						tpImpresora.activateControl("SELECCIONASLIP",iDriver);
						tpImpresora.enviaCadena("");
						tpImpresora.enviaCadena("");
						tpImpresora.enviaCadena("");
						tpImpresora.enviaCadena("");
						tpImpresora.enviaCadena("");
						tpImpresora.imprimeCadena1();
						tpImpresora.activateControl("SALTOHOJA",iDriver);
						tpImpresora.activateControl("SELECCIONAJOURNAL",iDriver);
					} else {
						tpImpresora.activateControl("SALTOHOJA",iDriver);
						tpImpresora.muestraVentana();
					}
				}
			}
			tpImpresora.enviaCadena(
				"__________________________________________________________________________________________________________________________________");
			strTotal = tpImpresora.formatMonto(strTotal);
			strOutLine =
				strFiller.substring(0, 40)
					+ "TOTAL : "
					+ strDivisa
					+ strFiller.substring(0, 17 - strTotal.length())
					+ strTotal;
			tpImpresora.enviaCadena(strOutLine);
			if (iDriver == 0) {
				tpImpresora.InserteDocumento();
				tpImpresora.activateControl("SELECCIONASLIP",iDriver);
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.imprimeCadena1();
				tpImpresora.activateControl("SALTOHOJA",iDriver);
				tpImpresora.activateControl("SELECCIONAJOURNAL",iDriver);
			} else {
				tpImpresora.activateControl("SALTOHOJA",iDriver);
			}
		}
		return intStatus;
		
		
	}
	
	public int imprimeReporte(
		Vector vDatos,
		String strDatosSuc,
		String strDate,
		int iDriver,
		String strIdentidad) {
		intStatus = tpImpresora.verificaStatus();
		
		if ((intStatus == 95) || (intStatus == 88)) {
			intStatus = 0;
			int intPagina = 1;
			int intSize = (vDatos.size() - 2) / 8;
			int intControl = 0;
			int intTotal = 0;
			double douTotal = 0.00;
			String strSucursal = "";
			String strCajero = "";
			String strTxn = "";
			String strFecha = "";
			String strMonto1 = "";
			String strMonto2 = "";
			String strConsec = "";
			String strCtaDep = "";
			String strChqDep = "";
			String strTotal = "";
			tpImpresora.activateControl("CERTIFICACION",iDriver);
			strOutLine =
				strFiller.substring(0, 78)
					+ strIdentidad+"       "
					+ strFiller.substring(0, 68)
					+ "PAGINA : "
					+ intPagina++
					+ "\n";
			tpImpresora.enviaCadena(strOutLine);
			this.datosSucursal(strDatosSuc);
			strOutLine =
				"SUCURSAL :  " + this.strNumSuc + "    " + this.strNomSuc + "\n";
			tpImpresora.enviaCadena(strOutLine);
			strOutLine =
				"RELACION DE CHEQUES LOCALES EN DEL DIA "
				+ strDate.substring(7, 9)
				+ "/"
				+ strDate.substring(5, 7)
				+ "/"
				+ strDate.substring(1, 5)
				+ "    "
				+ strCajero
				+ "\n";
			strOutLine = strFiller.substring(0, (178 - strOutLine.length())/2) + strOutLine; 
			tpImpresora.enviaCadena(strOutLine);
			tpImpresora.enviaCadena(
				"_______________________________________________________________________________________________________________________________________________________________________");
			tpImpresora.enviaCadena(
				"       SUCURSAL       CAJERO       TRANSACCION       FECHA       CUENTA DEL DEPOSITO       CONSECUTIVO       CHEQUES DEPOSITADOS       MONTO GLOBAL DEL DEPOSITO       ");
			tpImpresora.enviaCadena(
				"_______________________________________________________________________________________________________________________________________________________________________");

			for (int i = 0; i < intSize; i++) {
				strSucursal = (String) vDatos.elementAt((i * 8) + 2);
				if(strSucursal.startsWith("&"))
					strSucursal = strSucursal.substring(1);
				strCajero = (String) vDatos.elementAt((i * 8) + 3);
				strTxn = (String) vDatos.elementAt((i * 8) + 4);
				strFecha = (String) vDatos.elementAt((i * 8) + 5);
				strFecha = strDate.substring(7, 9)
							+ "/"
							+ strDate.substring(5, 7)
							+ "/"
							+ strDate.substring(1, 5);
				strMonto1 = (String) vDatos.elementAt((i * 8) + 6);
				strMonto2 = tpImpresora.formatMonto(strMonto1);
				strConsec = (String) vDatos.elementAt((i * 8) + 7);
				strCtaDep = (String) vDatos.elementAt((i * 8) + 8);
				if(strCtaDep.equals(""))
					strCtaDep = "-";
				strChqDep = (String) vDatos.elementAt((i * 8) + 9);
				if(strChqDep.equals(""))
					strChqDep = "-";
				douTotal = Double.valueOf(strMonto1).doubleValue()/100 + douTotal;
				
				strOutLine = 
					"       " 
					+ strFiller.substring(0, 8 - strSucursal.length())
					+ strSucursal
					+ "       "
					+ strFiller.substring(0, 6 - strCajero.length())
					+ strCajero
					+ "       "
					+ strFiller.substring(0, 11 - strTxn.length())
					+ strTxn
					+ "     "
					+ strFecha
					+ "    "
					+ strFiller.substring(0, 19 - strCtaDep.length())
					+ strCtaDep
					+ "       "
					+ strFiller.substring(0, 11 - strConsec.length())
					+ strConsec
					+ "       "
					+ strFiller.substring(0, 19 - strChqDep.length())
					+ strChqDep
					+ "          $"
					+ strFiller.substring(0, 21 - strMonto2.length())
					+ strMonto2;
				tpImpresora.enviaCadena(strOutLine);
				
				intControl++;
				if ((intControl == 45) && (i != (intSize - 1))) {
					intControl = 0;
					if (iDriver == 0) {
						tpImpresora.InserteDocumento();
						tpImpresora.activateControl("SELECCIONASLIP",iDriver);
						tpImpresora.enviaCadena("");
						tpImpresora.enviaCadena("");
						tpImpresora.enviaCadena("");
						tpImpresora.enviaCadena("");
						tpImpresora.enviaCadena("");
						tpImpresora.imprimeCadena1();
						tpImpresora.activateControl("SALTOHOJA",iDriver);
						tpImpresora.activateControl("SELECCIONAJOURNAL",iDriver);
					} else {
						tpImpresora.activateControl("SALTOHOJA",iDriver);
						tpImpresora.muestraVentana();
					}
				}
			}
			tpImpresora.enviaCadena(
				"_______________________________________________________________________________________________________________________________________________________________________");

			douTotal = douTotal * 100;
			intTotal = (int) douTotal;
			strTotal = Integer.toString(intTotal);
			strTotal = tpImpresora.formatMonto(strTotal);	

			strOutLine =
				strFiller.substring(0, 133)
					+ "TOTAL :    $"
					+ strFiller.substring(0, 15 - strTotal.length())
					+ strTotal;
			tpImpresora.enviaCadena(strOutLine);
					
			if (iDriver == 0) {
			tpImpresora.InserteDocumento();
				tpImpresora.activateControl("SELECCIONASLIP",iDriver);
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.imprimeCadena1();
				tpImpresora.activateControl("SALTOHOJA",iDriver);
				tpImpresora.activateControl("SELECCIONAJOURNAL",iDriver);
			} else {
				tpImpresora.activateControl("SALTOHOJA",iDriver);
				tpImpresora.muestraVentana();
			}
		}
		return intStatus;
	}
}
