import java.util.*;

public class LOCH {
	public int imprimeLoch(
		Vector vDatos,
		String strRespuesta,
		String strDatosSuc,
		String strCajero,
		String strDate,
		String strTime,
		int iDriver) {
		String strIva = "";
		String strComision = "";
		String strNombre = "";
		String strDomicilio = "";
		String strRfc = "";
		String strFiller = "                                                  ";
		String strTmp = "";
		String strSucursal = "";
		String strPlaza = "";
		String strDivisa = "";
		String strEfectivo = new String("");
		String strMonto = new String("");
		String strEfectivoAux = new String("");
		String strMontoAux = new String("");
		String strTxn = "", strMoneda = "";
		String strConsecutivo = "";
		String strReferencia1 = "";
		String strReferencia1Aux = "";
		String strReferencia2 = "";
		String strReferencia3 = "";
		String strReferencia = "";
		String strServicio = "";
		String strServicioAnt = "";
		String strCertificacion = "";
		Vector vCamposT = new Vector();
		Vector vCamposTT = new Vector();
		Vector vCampos = new Vector();
		Vector vParametros = new Vector();
		Vector vDatosSuc = new Vector();
		Vector vDatos5503 = new Vector();
		int i = 0;
		int intSize = 0;
		int intVeces = 0;
		int intNoCampos = 12;
		int intBeneficiario = 0;
		int Posicion = 0;
		int Posicion1 = 0;
		int Posicion2 = 0;
		int Posicion3 = 0;
		int intLoch = 0;
		ToolsPrinter tpImpresora = new ToolsPrinter();
		int intStatus = tpImpresora.verificaStatus();
		if ((intStatus == 95) || (intStatus == 88)) {
			intStatus = 0;
			vDatosSuc = tpImpresora.Campos(strDatosSuc, "@");
			vCampos = tpImpresora.Campos(strRespuesta, "^");
			intSize = vCampos.size();
			for (i = 0; i < intSize; i++) {
				strTmp = (String) vCampos.elementAt(i);
				if (strTmp.startsWith("MOVIMIENTOS_LOCH")) {
					strTmp = (String) vCampos.elementAt(i);
					vCamposT = tpImpresora.Campos(strTmp, "@");
					strTmp = (String) vCamposT.elementAt(1);
					vCamposTT = tpImpresora.Campos(strTmp, "~");
					for (int j = 0; j < vCamposTT.size() - 1; j++) {
						strTmp = (String) vCamposTT.elementAt(j);
						if ((strTmp.startsWith("|")) && (strTmp.length() > 1))
							strTmp = strTmp.substring(1, strTmp.length());
						else if (strTmp.startsWith("|"))
							strTmp = " ";
						vParametros.addElement(strTmp);
					}
				}
			}
			for (i = 0; i < vDatos.size(); i++) {
				strTmp = (String) vDatos.elementAt(i);
				if (strTmp.startsWith("LOCH")) {
					intLoch++;
					if (intLoch == 1)
						Posicion1 = i;
					if (intLoch == 2)
						Posicion2 = i;
					if (intLoch == 3)
						Posicion3 = i;
				}
			}
			intLoch = 0;
			intVeces = vParametros.size() / intNoCampos;
			String strTipoDoc = new String("");
			String strStatus = "";
			int intStatusR = 0;
			String strComp = new String("");
			strSucursal = (String) vDatosSuc.elementAt(1);
			strSucursal = strSucursal.substring(7, 11);
			strPlaza = (String) vDatosSuc.elementAt(3);
			strPlaza = strPlaza.substring(7, strPlaza.length());
			String strDomSuc = (String) vDatosSuc.elementAt(2);
			strDomSuc = strDomSuc.substring(7, strDomSuc.length());
			//			tpImpresora.activateControl("INICIALIZA");								
			tpImpresora.activateControl("CERTIFICACION",iDriver);
			int iRap = 0;
			for (i = 0;
				i < intVeces;
				i++) { //D_CUENTA, D_MONTO, D_TXN, D_RESPUESTA, D_REFER, D_REFER1, D_REFER2, D_REFER3, D_EFECTIVO, D_DIVISA, D_CONSECUTIVO, D_ESTATUS
				strDivisa =
					(String) vParametros.elementAt(9 + (i * intNoCampos));
				strDivisa = strDivisa.trim();
				if (strDivisa.equals("N$")) {
					strDivisa = "$";
					strMoneda = "1";
				} else
					strMoneda = "2";
				strEfectivo =
					(String) vParametros.elementAt(8 + (i * intNoCampos));
				strMonto =
					(String) vParametros.elementAt(1 + (i * intNoCampos));
				strTxn = (String) vParametros.elementAt(2 + (i * intNoCampos));
				strConsecutivo =
					(String) vParametros.elementAt(10 + (i * intNoCampos));
				//obtenerlo
				strServicio =
					(String) vParametros.elementAt(0 + (i * intNoCampos));
				strReferencia =
					(String) vParametros.elementAt(4 + (i * intNoCampos));
				strReferencia1 =
					(String) vParametros.elementAt(5 + (i * intNoCampos));
				strReferencia2 =
					(String) vParametros.elementAt(6 + (i * intNoCampos));
				strReferencia3 =
					(String) vParametros.elementAt(7 + (i * intNoCampos));
				if (strServicio.length() > 0)
					strServicio = strServicio.trim();
				if (strTxn.equals("5503")) {
					intLoch++;
					if (intLoch == 1)
						Posicion = Posicion1;
					else if (intLoch == 2)
						Posicion = Posicion2;
					else
						Posicion = Posicion3;
					strRfc = "";
					if (!strServicioAnt.equals(strServicio))
						if ((++intBeneficiario) > 4)
							intBeneficiario = 1;
					strComp =
						(String) vDatos.elementAt(
							Posicion + 5 + intBeneficiario);
					strServicioAnt = strServicio;
					strReferencia =
						(String) vParametros.elementAt(4 + (i * intNoCampos));
					strReferencia1 =
						(String) vParametros.elementAt(5 + (i * intNoCampos));
					strReferencia2 =
						(String) vParametros.elementAt(6 + (i * intNoCampos));
					strReferencia3 =
						(String) vParametros.elementAt(7 + (i * intNoCampos));
					strStatus =
						(String) vParametros.elementAt(11 + (i * intNoCampos));
					if (strStatus.equals("R")) {
						intStatusR = 1;
						tpImpresora.muestraRechazo();
						vDatos5503.addElement("1");
					} else
						vDatos5503.addElement("0");
					strTipoDoc = strServicio;
					strEfectivoAux = strEfectivo;
					strMontoAux = strMonto;
					strReferencia1Aux = strReferencia;
					vDatos5503.addElement(strComp);
					vDatos5503.addElement(strTipoDoc);
					vDatos5503.addElement(strReferencia1);
					vDatos5503.addElement(strReferencia2);
					vDatos5503.addElement(strReferencia3);
					vDatos5503.addElement(strReferencia1Aux);
					vDatos5503.addElement(strEfectivoAux);
					vDatos5503.addElement(strDivisa);
					vDatos5503.addElement(strMontoAux);
					vDatos5503.addElement(strMoneda);
					/***************************/
					vDatos5503.addElement(strNombre);
					vDatos5503.addElement(strRfc);
					vDatos5503.addElement(strDomicilio);
					vDatos5503.addElement(strComision);
					vDatos5503.addElement(strIva);
					/***************************/
					iRap = 1;
				}
				if (strTxn.equals("0736") || strTxn.equals("0792")) {
					if (vDatos.size() > (11 + Posicion)) {
						strRfc = (String) vDatos.elementAt(Posicion + 10);
						strNombre = (String) vDatos.elementAt(Posicion + 11);
						strDomicilio = (String) vDatos.elementAt(Posicion + 12);
					} else {
						strRfc = "";
						strNombre = "";
						strDomicilio = "";
					}
					strReferencia1 = strReferencia1Aux;
					strReferencia2 = "";
					strReferencia3 = "";
					strReferencia = "";
					strComision = strMonto;
					vDatos5503.setElementAt(
						strNombre,
						(16 * (intLoch - 1)) + 11);
					vDatos5503.setElementAt(strRfc, (16 * (intLoch - 1)) + 12);
					vDatos5503.setElementAt(
						strDomicilio,
						(16 * (intLoch - 1)) + 13);
					vDatos5503.setElementAt(
						strComision,
						(16 * (intLoch - 1)) + 14);
					/*vDatos5503.addElement(strNombre);
					vDatos5503.addElement(strRfc);
					vDatos5503.addElement(strDomicilio);
					vDatos5503.addElement(strComision);	*/
				}
				if (strTxn.equals("0372")) {
					strIva = strMonto;
					if (iRap == 1) {
						strReferencia1 = strReferencia1Aux;
						strReferencia2 = "";
						strReferencia3 = "";
						strReferencia = "";
						vDatos5503.setElementAt(
							strIva,
							(16 * (intLoch - 1)) + 15);
						//vDatos5503.addElement(strIva);
						iRap = 0;
					}
				}
				strCertificacion =
					strConsecutivo
						+ " "
						+ strDate.substring(7, 9)
						+ strDate.substring(5, 7)
						+ strDate.substring(1, 5)
						+ " "
						+ strTxn
						+ " "
						+ strCajero
						+ " ";
				strCertificacion = strCertificacion + strServicio + " ";
				if (!strEfectivo.equals(""))
					strCertificacion =
						strCertificacion
							+ strDivisa
							+ tpImpresora.formatMonto(strEfectivo.trim())
							+ " ";
				if (!strMonto.equals(""))
					strCertificacion =
						strCertificacion
							+ "   "
							+ strDivisa
							+ tpImpresora.formatMonto(strMonto.trim())
							+ " ";
				strCertificacion = strCertificacion + strSucursal;
				if (strServicio.equals("4254")) {
					tpImpresora.enviaCadena(
						"\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
					tpImpresora.enviaCadena(
						"\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
					tpImpresora.enviaCadena("\n\n\n\n\n\n\n\n\n");
					tpImpresora.enviaCadena("          " + strCertificacion);
				} else
					tpImpresora.enviaCadena(strCertificacion);
				strCertificacion = new String("");
				strCertificacion = strCertificacion + strReferencia + " ";
				strCertificacion = strCertificacion + strReferencia1 + " ";
				strCertificacion = strCertificacion + strReferencia2 + " ";
				strCertificacion = strCertificacion + strReferencia3;
				if (!strCertificacion.trim().equals("")) {
					if (strServicio.equals("4254"))
						tpImpresora.enviaCadena(
							"          " + strCertificacion.trim());
					else
						tpImpresora.enviaCadena(strCertificacion.trim());
				}
				strCertificacion =
					(String) vParametros.elementAt(3 + (i * intNoCampos));
				if (strServicio.equals("4254"))
					tpImpresora.enviaCadena("          " + strCertificacion);
				else {
					int nlineas = strCertificacion.length() / 78;
					if (strCertificacion.length() % 78 > 0)
						nlineas++;
					if (!strStatus.equals("R"))
						nlineas--;
					for (int j = 0; j < nlineas; j++) {
						String strTmpL = "";
						if (((j * 78) + 78) < strCertificacion.length())
							strTmpL =
								strCertificacion.substring(
									(j * 78),
									(j * 78) + 78);
						else
							strTmpL =
								strCertificacion.substring(
									j * 78,
									strCertificacion.length());
						tpImpresora.enviaCadena(strTmpL);
					}
				}
				if (iDriver == 0) {
					tpImpresora.InserteDocumento();
					tpImpresora.activateControl("SELECCIONASLIP",iDriver);
					tpImpresora.enviaCadena("");
					tpImpresora.enviaCadena("");
					tpImpresora.enviaCadena("");
					tpImpresora.enviaCadena("");
					tpImpresora.enviaCadena("");
					tpImpresora.imprimeCadena1();
					tpImpresora.activateControl("SALTOHOJA",iDriver);
					tpImpresora.activateControl("SELECCIONAJOURNAL",iDriver);
				} else {
					tpImpresora.activateControl("SALTOHOJA",iDriver);
				}
			}
			for (i = 0; i < intLoch; i++) {
				if (i == 1)
					Posicion = Posicion1;
				else if (i == 2)
					Posicion = Posicion2;
				else
					Posicion = Posicion3;
				strTmp = (String) vDatos5503.elementAt(16 * i + 0);
				intStatusR = Integer.parseInt(strTmp);
				strComp = (String) vDatos5503.elementAt(16 * i + 1);
				strTipoDoc = (String) vDatos5503.elementAt(16 * i + 2);
				strReferencia1 = (String) vDatos5503.elementAt(16 * i + 3);
				strReferencia2 = (String) vDatos5503.elementAt(16 * i + 4);
				strReferencia3 = (String) vDatos5503.elementAt(16 * i + 5);
				strReferencia1Aux = (String) vDatos5503.elementAt(16 * i + 6);
				strEfectivoAux = (String) vDatos5503.elementAt(16 * i + 7);
				strDivisa = (String) vDatos5503.elementAt(16 * i + 8);
				strMontoAux = (String) vDatos5503.elementAt(16 * i + 9);
				strMoneda = (String) vDatos5503.elementAt(16 * i + 10);
				strNombre = (String) vDatos5503.elementAt(16 * i + 11);
				strRfc = (String) vDatos5503.elementAt(16 * i + 12);
				strDomicilio = (String) vDatos5503.elementAt(16 * i + 13);
				strComision = (String) vDatos5503.elementAt(16 * i + 14);
				strIva = (String) vDatos5503.elementAt(16 * i + 15);
				if (intStatusR == 0) {
					tpImpresora.activateControl("CERTIFICACION",iDriver);
					if (strComp.equals("S")
						|| strTipoDoc.equals("60")
						|| strTipoDoc.equals("670")
						|| strTipoDoc.equals("669")) {
						if (iDriver == 1)
							tpImpresora.muestraVentana();
						strCertificacion =
							"                      H S B C     P A N A M A \n";
						tpImpresora.enviaCadena(strCertificacion);
						strCertificacion =
							"  Plaza    : "
								+ strPlaza
								+ strFiller.substring(0, 40 - strPlaza.length())
								+ "  Sucursal : "
								+ strSucursal;
						tpImpresora.enviaCadena(strCertificacion);
						strCertificacion =
							"  Cajero   : "
								+ strCajero
								+ strFiller.substring(0, 22 - strCajero.length())
								+ "Fecha: "
								+ strDate.substring(7, 9)
								+ "/"
								+ strDate.substring(5, 7)
								+ "/"
								+ strDate.substring(1, 5)
								+ "       Hora : "
								+ strTime.substring(0, 2)
								+ ":"
								+ strTime.substring(2, 4)
								+ ":"
								+ strTime.substring(4, 6)
								+ " \n";
						tpImpresora.enviaCadena(strCertificacion);
						if (!strTipoDoc.equals("60")
							&& !strTipoDoc.equals("670")
							&& !strTipoDoc.equals("669")) {
							strTmp =
								(String) vDatos.elementAt(
									Posicion + intBeneficiario);
							strCertificacion = "         Pago de  : " + strTmp;
							tpImpresora.enviaCadena(strCertificacion);
							strCertificacion =
								"         Servicio : " + strTipoDoc + "\n";
							tpImpresora.enviaCadena(strCertificacion);
							strCertificacion =
								"  Referencia 1: " + strReferencia1;
							tpImpresora.enviaCadena(strCertificacion);
							strCertificacion =
								"  Referencia 2: " + strReferencia2;
							tpImpresora.enviaCadena(strCertificacion);
							strCertificacion =
								"  Referencia 3: " + strReferencia3;
							tpImpresora.enviaCadena(strCertificacion);
						} else {
							if (strTipoDoc.equals("60")) {
								strCertificacion =
									"                 PAGO DE TARJETA DE CREDITO \n";
								tpImpresora.enviaCadena(strCertificacion);
							} else if (
								strTipoDoc.equals("669")
									|| strTipoDoc.equals("670")) {
								strCertificacion =
									//"PAGO DE AMERICAN EXPRESS COMPANY ( MEXICO ) S.A. DE C.V.";
									"PAGO DE AMERICAN EXPRESS";
								tpImpresora.enviaCadena(strCertificacion);
								strCertificacion = "RFC : AEC-810901-298\n";
								tpImpresora.enviaCadena(strCertificacion);
							}
							strCertificacion =
								"  Numero de Tarjeta : " + strReferencia1Aux;
							tpImpresora.enviaCadena(strCertificacion);
						}
						strTmp = tpImpresora.formatMonto(strEfectivoAux);
						strCertificacion =
							"  Efectivo    : "
								+ strDivisa
								+ strFiller.substring(0, 18 - strTmp.length())
								+ strTmp;
						tpImpresora.enviaCadena(strCertificacion);
						long longDocumentos =
							Long.parseLong(strMontoAux)
								- Long.parseLong(strEfectivoAux);
						String strDoc = "" + longDocumentos;
						strTmp = tpImpresora.formatMonto(strDoc);
						strCertificacion =
							"  Documentos  : "
								+ strDivisa
								+ strFiller.substring(0, 18 - strTmp.length())
								+ strTmp;
						tpImpresora.enviaCadena(strCertificacion);
						strTmp = tpImpresora.formatMonto(strMontoAux);
						strCertificacion =
							"  Monto       : "
								+ strDivisa
								+ strFiller.substring(0, 18 - strTmp.length())
								+ strTmp;
						tpImpresora.enviaCadena(strCertificacion);
						strCertificacion =
							"  Cantidad    : "
								+ Currency.convert(
									tpImpresora.formatMonto(strMontoAux),
									Integer.parseInt(strMoneda))
								+ " \n";
						tpImpresora.enviaCadena(strCertificacion);
						if (strTipoDoc.equals("669")
							|| strTipoDoc.equals("670"))
							strCertificacion =
								"  ESTE ES UN RECIBO DE PAGO, NO ES UN COMPROBANTE FISCAL ";
						else
							strCertificacion =
								"  ESTE DOCUMENTO NO ES UN COMPROBANTE FISCAL ";
						tpImpresora.enviaCadena(strCertificacion);
						if (iDriver == 0) {
							tpImpresora.InserteDocumento();
							tpImpresora.activateControl("SELECCIONASLIP",iDriver);
							tpImpresora.enviaCadena("");
							tpImpresora.enviaCadena("");
							tpImpresora.enviaCadena("");
							tpImpresora.enviaCadena("");
							tpImpresora.enviaCadena("");
							tpImpresora.imprimeCadena1();
							tpImpresora.activateControl("SALTOHOJA",iDriver);
							tpImpresora.activateControl("SELECCIONAJOURNAL",iDriver);
						} else {
							tpImpresora.activateControl("SALTOHOJA",iDriver);
						}
					}
					if (!strRfc.equals("")) {
						if (iDriver == 1)
							tpImpresora.muestraVentana();
						tpImpresora.enviaCadena("\n\n\n\n\n\n");
						//String strSigno = "US$";
						//if (!strMoneda.equals("2"))
						//	strSigno = "$";
						strCertificacion =
							"            "
								+ strPlaza
								+ "          "
								+ strDate.substring(7, 9)
								+ "/"
								+ strDate.substring(5, 7)
								+ "/"
								+ strDate.substring(1, 5)
								+ "\n";
						tpImpresora.enviaCadena(strCertificacion);
						strCertificacion =
							"                         " + strDomSuc;
						// (String)vDatos.elementAt(5);
						tpImpresora.enviaCadena(strCertificacion);
						if (strNombre.length() > 40)
							strNombre = strNombre.substring(0, 40);
						strCertificacion =
							"         "
								+ strNombre
								+ strFiller.substring(0, 40 - strNombre.length())
								+ strRfc;
						tpImpresora.enviaCadena(strCertificacion);
						strCertificacion =
							"            " + strDomicilio + "\n\n";
						tpImpresora.enviaCadena(strCertificacion);
						tpImpresora.enviaCadena(
							"          SERVICIOS DE CONVENIENCIA\n");
						tpImpresora.enviaCadena("          PAGO DE SERVICIOS");
						strIva = strIva.trim();
						strComision = strComision.trim();
						long longTotal =
							Long.parseLong(strComision)
								+ Long.parseLong(strIva);
						strIva = tpImpresora.formatMonto(strIva);
						strComision = tpImpresora.formatMonto(strComision);
						strDivisa = strDivisa.trim();
						strTmp =
							strFiller.substring(0, 28)
								+ strDivisa
								+ strFiller.substring(
									0,
									18 - strComision.length())
								+ strComision;
						tpImpresora.enviaCadena(strTmp);
						strTmp =
							strFiller.substring(0, 28)
								+ strDivisa
								+ strFiller.substring(0, 18 - strIva.length())
								+ strIva;
						tpImpresora.enviaCadena(strTmp);
						String strTotalT = new String("");
						strTotalT = String.valueOf(longTotal);
						strTotalT = tpImpresora.formatMonto(strTotalT);
						strTmp =
							strFiller.substring(0, 28)
								+ strDivisa
								+ strFiller.substring(0, 18 - strTotalT.length())
								+ strTotalT;
						tpImpresora.enviaCadena(strTmp);
						tpImpresora.enviaCadena(
							"          "
								+ Currency.convert(
									strTotalT,
									Integer.parseInt(strMoneda)));
						if (iDriver == 0) {
							tpImpresora.InserteDocumento();
							tpImpresora.activateControl("SELECCIONASLIP",iDriver);
							tpImpresora.enviaCadena("");
							tpImpresora.enviaCadena("");
							tpImpresora.enviaCadena("");
							tpImpresora.enviaCadena("");
							tpImpresora.enviaCadena("");
							tpImpresora.imprimeCadena1();
							tpImpresora.activateControl("SALTOHOJA",iDriver);
							tpImpresora.activateControl("SELECCIONAJOURNAL",iDriver);
						} else {
							tpImpresora.activateControl("SALTOHOJA",iDriver);
						}
					}
				}
			}
			intLoch = 0;
			for (i = 0; i < vDatos.size(); i++) {
				strTmp = (String) vDatos.elementAt(i);
				if (strTmp.startsWith("LCFISCAL")) {
					intLoch++;
					if (intLoch == 1)
						Posicion1 = i;
					if (intLoch == 2)
						Posicion2 = i;
					if (intLoch == 3)
						Posicion3 = i;
				}
			}
			for (i = 0; i < intLoch; i++) {
				if (i == 0)
					Posicion = Posicion1;
				if (i == 1)
					Posicion = Posicion2 - Posicion1;
				if (i == 2)
					Posicion = Posicion3 - Posicion2 - Posicion1;
				for (int j = 0; j < Posicion; j++)
					vDatos.remove(0);
				if (iDriver == 1)
					tpImpresora.muestraVentana();
				Comprobantes Comp = new Comprobantes();
				Comp.imprimeCFiscal(strDatosSuc, vDatos, strDate, iDriver);
			}
		}
		return intStatus;
	}
}
