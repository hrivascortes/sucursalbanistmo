import java.util.*;

public class Diario {
	public int imprimeDiario(String strRespuesta, int iDriver, String strIdentidad) {
		String strFiller =
			"                                                                                ";
		Vector vParametros = new Vector();
		String strTxn = "";
		String strSt = "";
		String strCon = "";
		String strFec = "";
		String strCta = "";
		String strMto = new String("");
		String strEfe = new String("");
		String strTC = "";
		String strTA = "";
		String strSer = "";
		String strRef = "";
		String strCer = "";
		String strCajero = "";
		String strTmp = "";
		long longEfe = 0;
		long longMto = 0;
		int intVeces = 0;
		int intLineas = 0;
		int intNoCampos = 11;
		int intPagina = 1;
		ToolsPrinter tpImpresora = new ToolsPrinter();
		int intStatus = tpImpresora.verificaStatus();
		if ((intStatus == 95) || (intStatus == 88)) {
			intStatus = 0;
			tpImpresora.activateControl("CERTIFICACION",iDriver);
			vParametros = tpImpresora.Campos(strRespuesta, "~");
			intVeces = (vParametros.size() - 1) / intNoCampos;
			strCajero = (String) vParametros.elementAt(1);
			for (int i = 0; i < intVeces; i++) {
				strCer = "";
				strTxn = (String) vParametros.elementAt(2 + (i * intNoCampos));
				strSt = (String) vParametros.elementAt(3 + (i * intNoCampos));
				strCon = (String) vParametros.elementAt(4 + (i * intNoCampos));
				strFec = (String) vParametros.elementAt(5 + (i * intNoCampos));
				strCta = (String) vParametros.elementAt(6 + (i * intNoCampos));
				strMto = (String) vParametros.elementAt(7 + (i * intNoCampos));
				strEfe = (String) vParametros.elementAt(8 + (i * intNoCampos));
				if (!strEfe.equals("")) {
					strTmp = tpImpresora.unFormat(strEfe);
					longEfe += Long.parseLong(strTmp);
				}
				if (!strMto.equals("")) {
					strTmp = tpImpresora.unFormat(strMto);
					longMto += Long.parseLong(strTmp);
				}
				strTC = (String) vParametros.elementAt(9 + (i * intNoCampos));
				strTA = (String) vParametros.elementAt(10 + (i * intNoCampos));
				strSer = (String) vParametros.elementAt(11 + (i * intNoCampos));
				strRef = (String) vParametros.elementAt(12 + (i * intNoCampos));
				if (intLineas == 0) {
					tpImpresora.enviaCadena(
						"                                                             "+strIdentidad);
					// IMAGEN
					strCer =
						"DIARIO ELECTRONICO "
							+ strFiller.substring(0, 70)
							+ "   CAJERO : "
							+ strCajero
							+ strFiller.substring(0, 8 - strCajero.length())
							+ " No. PAGINA : "
							+ intPagina;
					tpImpresora.enviaCadena(strCer);
					tpImpresora.enviaCadena(
						"TXN  ST  CONSEC.    FECHA         CUENTA             MONTO            EFECTIVO      TOTAL  TOTAL    SERIAL    REFERENCIA   ");
					tpImpresora.enviaCadena(
						"                                                                                    CARGO  ABONO                         ");
					tpImpresora.enviaCadena(
						"----------------------------------------------------------------------------------------------------------------------------------");
				}
				intLineas++;
				strCer = strTxn + " ";
				strCer = strCer + strSt + "  ";
				strCer =
					strCer
						+ strCon
						+ strFiller.substring(0, 9 - strCon.length());
				strCer = strCer + strFec + "  ";
				strCer =
					strCer
						+ strCta
						+ strFiller.substring(0, 17 - strCta.length());
				strCer =
					strCer
						+ strFiller.substring(0, 17 - strMto.length())
						+ strMto
						+ "  ";
				strCer =
					strCer
						+ strFiller.substring(0, 17 - strEfe.length())
						+ strEfe
						+ "   ";
				strCer =
					strCer + strTC + strFiller.substring(0, 7 - strTC.length());
				strCer =
					strCer + strTA + strFiller.substring(0, 6 - strTA.length());
				strCer =
					strCer
						+ strSer
						+ strFiller.substring(0, 10 - strSer.length())
						+ "  ";
				if (strRef.length() > 20)
					strCer = strCer + strRef.substring(0, 20);
				else
					strCer = strCer + strRef;
				tpImpresora.enviaCadena(strCer);
				if (intLineas > 49) {
					if (i == (intVeces - 1)) {
						tpImpresora.enviaCadena(
							"==================================================================================================================================");
						strMto =
							tpImpresora.formatMonto(
								new Long(longMto).toString());
						strEfe =
							tpImpresora.formatMonto(
								new Long(longEfe).toString());
						strCer =
							"TOTALES"
								+ strFiller.substring(0, 56 - strMto.length())
								+ strMto
								+ "  ";
						strCer =
							strCer
								+ strFiller.substring(0, 17 - strEfe.length())
								+ strEfe
								+ "   ";
						tpImpresora.enviaCadena(strCer);
					}
					intPagina++;
					if (iDriver == 0) {
						tpImpresora.InserteDocumento();
						tpImpresora.activateControl("SELECCIONASLIP",iDriver);
						tpImpresora.enviaCadena("");
						tpImpresora.enviaCadena("");
						tpImpresora.enviaCadena("");
						tpImpresora.enviaCadena("");
						tpImpresora.enviaCadena("");
						tpImpresora.imprimeCadena1();
						tpImpresora.activateControl("SALTOHOJA",iDriver);
						tpImpresora.activateControl("SELECCIONAJOURNAL",iDriver);
					} else {
						if (iDriver == 1)
							tpImpresora.muestraVentana();
						tpImpresora.activateControl("SALTOHOJA",iDriver);
					}
					intLineas = 0;
				}
			}
			if (intLineas != 0) {
				tpImpresora.enviaCadena(
					"----------------------------------------------------------------------------------------------------------------------------------");
				strMto = tpImpresora.formatMonto(new Long(longMto).toString());
				strEfe = tpImpresora.formatMonto(new Long(longEfe).toString());
				strCer =
					"TOTALES"
						+ strFiller.substring(0, 56 - strMto.length())
						+ strMto
						+ "  ";
				strCer =
					strCer
						+ strFiller.substring(0, 17 - strEfe.length())
						+ strEfe
						+ "   ";
				tpImpresora.enviaCadena(strCer);
				tpImpresora.enviaCadena(
					"==================================================================================================================================");
				if (iDriver == 0) {
					tpImpresora.InserteDocumento();
					tpImpresora.activateControl("SELECCIONASLIP",iDriver);
					tpImpresora.enviaCadena("");
					tpImpresora.enviaCadena("");
					tpImpresora.enviaCadena("");
					tpImpresora.enviaCadena("");
					tpImpresora.enviaCadena("");
					tpImpresora.imprimeCadena1();
					tpImpresora.activateControl("SALTOHOJA",iDriver);
					tpImpresora.activateControl("SELECCIONAJOURNAL",iDriver);
				} else {
					tpImpresora.activateControl("SALTOHOJA",iDriver);
				}
			}
		}
		return intStatus;
	}

	public int imprimeReporteGte(String strRespuesta, int iDriver, String strIdentidad) {
		String strFiller =
			"                                                                                                    ";
		Vector vParametros = new Vector();
		String strNombre = "";
		String strSucursal = "";
		String strCajero1 = "";
		String strHora = "";
		String strRACF1 = "";
		String strDI1 = "";
		String strCajero2 = "";
		String strRACF2 = "";
		String strDI2 = "";
		String strTxn = "";
		String strFecha = "";
		String strMonto = "";
		String strDivisa = "";
		String strTmp = "";
		String strCodigo = "";
		int intVeces = 0;
		int intLineas = 0;
		int intDesfaze = 0;
		int intNoCampos = 0;
		int intPagina = 1;

		ToolsPrinter tpImpresora = new ToolsPrinter();
		int intStatus = tpImpresora.verificaStatus();
		if ((intStatus == 95) || (intStatus == 88)) {
			intStatus = 0;
			tpImpresora.activateControl("CERTIFICACION",iDriver);
			vParametros = tpImpresora.Campos(strRespuesta, "~");
			strNombre = (String) vParametros.elementAt(0);

			if (strNombre.startsWith("AUTORIZ")) {
				if (strNombre.endsWith("1")) {
					intDesfaze = 0;
					intNoCampos = 10;
					strCajero1 = (String) vParametros.elementAt(1);
				} else {
					intDesfaze = 1;
					intNoCampos = 11;
					strSucursal = (String) vParametros.elementAt(1);
				}
			} else {
				if (strNombre.endsWith("1")) {
					intDesfaze = 1;
					intNoCampos = 11;
					strCajero1 = (String) vParametros.elementAt(1);
				} else {
					intDesfaze = 2;
					intNoCampos = 12;
					strSucursal = (String) vParametros.elementAt(1);
				}
			}
			intVeces = (vParametros.size() - 2) / intNoCampos;
			for (int i = 0; i < intVeces; i++) {
				if (intDesfaze == 1) {
					if (strNombre.startsWith("AUTORIZ"))
						strCajero1 =
							(String) vParametros.elementAt(
								1 + intDesfaze + (i * intNoCampos));
					else
						strCodigo =
							(String) vParametros.elementAt(
								1 + intDesfaze + (i * intNoCampos));
				}
				if (intDesfaze == 2) {
					strCajero1 =
						(String) vParametros.elementAt(
							0 + intDesfaze + (i * intNoCampos));
					strCodigo =
						(String) vParametros.elementAt(
							1 + intDesfaze + (i * intNoCampos));
				}
				strHora =
					(String) vParametros.elementAt(
						2 + intDesfaze + (i * intNoCampos));
				strRACF1 =
					(String) vParametros.elementAt(
						3 + intDesfaze + (i * intNoCampos));
				strDI1 =
					(String) vParametros.elementAt(
						4 + intDesfaze + (i * intNoCampos));
				strCajero2 =
					(String) vParametros.elementAt(
						5 + intDesfaze + (i * intNoCampos));
				strRACF2 =
					(String) vParametros.elementAt(
						6 + intDesfaze + (i * intNoCampos));
				strDI2 =
					(String) vParametros.elementAt(
						7 + intDesfaze + (i * intNoCampos));
				strTxn =
					(String) vParametros.elementAt(
						8 + intDesfaze + (i * intNoCampos));
				strFecha =
					(String) vParametros.elementAt(
						9 + intDesfaze + (i * intNoCampos));
				strMonto =
					(String) vParametros.elementAt(
						10 + intDesfaze + (i * intNoCampos));
				strDivisa =
					(String) vParametros.elementAt(
						11 + intDesfaze + (i * intNoCampos));
				if (intLineas == 0) {
					tpImpresora.enviaCadena(
						"                                                             "+strIdentidad+"\n");
					if (strNombre.startsWith("AUTORIZ"))
						if (strNombre.startsWith("AUTORIZACION1"))
							strTmp =
								"REPORTE DE AUTORIZACIONES DEL CAJERO "
									+ strCajero1
									+ strFiller.substring(0, 72)
									+ " No. PAGINA : "
									+ intPagina
									+ "\n";
						else
							strTmp =
								"REPORTE DE AUTORIZACIONES DE LA SUCURSAL "
									+ strSucursal
									+ strFiller.substring(0, 70)
									+ " No. PAGINA : "
									+ intPagina
									+ "\n";
					else if (strNombre.startsWith("REVERSO1"))
						strTmp =
							"REPORTE DE REVERSOS DEL CAJERO "
								+ strCajero1
								+ strFiller.substring(0, 78)
								+ " No. PAGINA : "
								+ intPagina
								+ "\n";
					else
						strTmp =
							"REPORTE DE REVERSOS DE LA SUCURSAL "
								+ strSucursal
								+ strFiller.substring(0, 76)
								+ " No. PAGINA : "
								+ intPagina
								+ "\n";
					tpImpresora.enviaCadena(strTmp);
					if (strNombre.startsWith("AUTORIZACION1")
						|| strNombre.startsWith("AUTORIZACION2")) {
						if (intDesfaze == 0) {
							tpImpresora.enviaCadena(
								" AUTORIZACION      E J E C U T I V O             A U T O R I Z A D O R           T R A N S A C C I O N   O R I G I N A L \n");
							tpImpresora.enviaCadena(
								"     HORA        USUARIO     DIRECCION      CAJERO  USUARIO      DIRECCION     TXN     FECHA          MONTO         DIVISA");
							tpImpresora.enviaCadena(
								"                   RACF          IP                   RACF           IP       \n");
						} else {
							tpImpresora.enviaCadena(
								"         AUTORIZACION      E J E C U T I V O             A U T O R I Z A D O R           T R A N S A C C I O N   O R I G I N A L \n");
							tpImpresora.enviaCadena(
								" CAJERO      HORA        USUARIO     DIRECCION      CAJERO  USUARIO      DIRECCION     TXN     FECHA          MONTO         DIVISA");
							tpImpresora.enviaCadena(
								"                           RACF          IP                   RACF           IP       \n");
						}
					} else {
						if (intDesfaze == 1) {
							tpImpresora.enviaCadena(
								"R E V E R S O      E J E C U T I V O             A U T O R I Z A D O R           T R A N S A C C I O N   O R I G I N A L \n");
							tpImpresora.enviaCadena(
								" TXN    HORA     USUARIO     DIRECCION      CAJERO  USUARIO      DIRECCION     TXN     FECHA          MONTO         DIVISA");
							tpImpresora.enviaCadena(
								"                   RACF          IP                   RACF           IP       \n");
						} else {
							tpImpresora.enviaCadena(
								"       R E V E R S O       E J E C U T I V O             A U T O R I Z A D O R           T R A N S A C C I O N   O R I G I N A L \n");
							tpImpresora.enviaCadena(
								"CAJERO  TXN    HORA      USUARIO     DIRECCION      CAJERO  USUARIO      DIRECCION     TXN     FECHA          MONTO         DIVISA");
							tpImpresora.enviaCadena(
								"                           RACF          IP                   RACF           IP       \n");
						}
					}
				}
				intLineas++;
				if (strNombre.startsWith("AUTORIZACION1")
					|| strNombre.startsWith("AUTORIZACION2")) {
					if (intDesfaze == 1)
						strTmp =
							" "
								+ strCajero1
								+ "     "
								+ strHora
								+ "     "
								+ strRACF1
								+ "  "
								+ strDI1
								+ strFiller.substring(0, 15 - strDI1.length())
								+ "  "
								+ strCajero2
								+ "  "
								+ strRACF2
								+ "   "
								+ strDI2
								+ strFiller.substring(0, 15 - strDI2.length())
								+ " "
								+ strTxn
								+ "  "
								+ strFecha
								+ " "
								+ strFiller.substring(0, 18 - strMonto.length())
								+ strMonto
								+ "   "
								+ strDivisa;
					else
						strTmp =
							"   "
								+ strHora
								+ "      "
								+ strRACF1
								+ "  "
								+ strDI1
								+ strFiller.substring(0, 15 - strDI1.length())
								+ "   "
								+ strCajero2
								+ " "
								+ strRACF2
								+ "   "
								+ strDI2
								+ strFiller.substring(0, 15 - strDI2.length())
								+ " "
								+ strTxn
								+ "  "
								+ strFecha
								+ " "
								+ strFiller.substring(0, 18 - strMonto.length())
								+ strMonto
								+ "   "
								+ strDivisa;
				} else {
					if (intDesfaze == 2)
						strTmp =
							strCajero1
								+ " "
								+ strCodigo
								+ "  "
								+ strHora
								+ "   "
								+ strRACF1
								+ "  "
								+ strDI1
								+ strFiller.substring(0, 15 - strDI1.length())
								+ "   "
								+ strCajero2
								+ "  "
								+ strRACF2
								+ "   "
								+ strDI2
								+ strFiller.substring(0, 15 - strDI2.length())
								+ " "
								+ strTxn
								+ "  "
								+ strFecha
								+ " "
								+ strFiller.substring(0, 18 - strMonto.length())
								+ strMonto
								+ "   "
								+ strDivisa;
					else
						strTmp =
							strCodigo
								+ "  "
								+ strHora
								+ "   "
								+ strRACF1
								+ "  "
								+ strDI1
								+ strFiller.substring(0, 15 - strDI1.length())
								+ "  "
								+ strCajero2
								+ "  "
								+ strRACF2
								+ "   "
								+ strDI2
								+ strFiller.substring(0, 15 - strDI2.length())
								+ " "
								+ strTxn
								+ "  "
								+ strFecha
								+ " "
								+ strFiller.substring(0, 18 - strMonto.length())
								+ strMonto
								+ "   "
								+ strDivisa;
				}
				tpImpresora.enviaCadena(strTmp);
				if (intLineas > 49) {
					intPagina++;
					if (iDriver == 0) {
						tpImpresora.InserteDocumento();
						tpImpresora.activateControl("SELECCIONASLIP",iDriver);
						tpImpresora.enviaCadena("");
						tpImpresora.enviaCadena("");
						tpImpresora.enviaCadena("");
						tpImpresora.enviaCadena("");
						tpImpresora.enviaCadena("");
						tpImpresora.imprimeCadena1();
						tpImpresora.activateControl("SALTOHOJA",iDriver);
						tpImpresora.activateControl("SELECCIONAJOURNAL",iDriver);
					} else {
						if (iDriver == 1)
							tpImpresora.muestraVentana();
						tpImpresora.activateControl("SALTOHOJA",iDriver);
					}
					intLineas = 0;
				}
			}
			if (intLineas != 0)
				if (iDriver == 0) {
					tpImpresora.InserteDocumento();
					tpImpresora.activateControl("SELECCIONASLIP",iDriver);
					tpImpresora.enviaCadena("");
					tpImpresora.enviaCadena("");
					tpImpresora.enviaCadena("");
					tpImpresora.enviaCadena("");
					tpImpresora.enviaCadena("");
					tpImpresora.imprimeCadena1();
					tpImpresora.activateControl("SALTOHOJA",iDriver);
					tpImpresora.activateControl("SELECCIONAJOURNAL",iDriver);
				} else {
					tpImpresora.activateControl("SALTOHOJA",iDriver);
				}
		}
		return intStatus;
	}
	
	public int imprimeReporteAdmon(String strRespuesta, int iDriver, String strIdentidad) 
	{
		Vector vParametros = new Vector();
		String strGerente = "";
		String strMovimiento = "";
		String strCajero = "";		
		String strDatoAnterior = "";		
		String strDatoActual = "";		
		String strHora = "";
		String strTmp = "";
		int intVeces = 0;
		int intLineas = 0;
		int intPagina = 1;
		int intNoCampos = 6;

		//System.out.println(" strCadena " + strRespuesta);		
		ToolsPrinter tpImpresora = new ToolsPrinter();
		int intStatus = tpImpresora.verificaStatus();
		if ((intStatus == 95) || (intStatus == 88)) {
			//System.out.println(" Entro " + strRespuesta);					
			intStatus = 0;
			tpImpresora.activateControl("CERTIFICACION",iDriver);
			vParametros = tpImpresora.Campos(strRespuesta, "~");
			intVeces = (vParametros.size() - 3) / intNoCampos;
			for (int i = 0; i < intVeces; i++) {		
				strGerente = (String) vParametros.elementAt(3 + (i * intNoCampos));
				strMovimiento = (String) vParametros.elementAt(4 + (i * intNoCampos));
				strCajero = (String) vParametros.elementAt(5 + (i * intNoCampos));
				strDatoAnterior = (String) vParametros.elementAt(6 + (i * intNoCampos));
				strDatoActual = (String) vParametros.elementAt(7 + (i * intNoCampos));
				strHora = (String) vParametros.elementAt(8 + (i * intNoCampos));
				if (intLineas == 0) {
					tpImpresora.enviaCadena("                                                             "+strIdentidad+"n");
					strTmp ="                                                      MOVIMIENTOS DE USUARIOS                                           PAGINA : " + intPagina + "\n";
					tpImpresora.enviaCadena(strTmp);
				}
				intLineas+=5;
				strTmp = "----------------------------------------------------------------------------------------------------------------------------------";;
				tpImpresora.enviaCadena(strTmp);
				strTmp = " Gerente/Supervisor : " + strGerente + "               Cajero: " + strCajero + "               Hora :"+  strHora;
				tpImpresora.enviaCadena(strTmp);
				strTmp = " Movimiento         : " + strMovimiento;				
				tpImpresora.enviaCadena(strTmp);
				strTmp = " Dato Anterior      : " + strDatoAnterior;
				tpImpresora.enviaCadena(strTmp);				
				strTmp = " Dato Actual        : " + strDatoActual;
				tpImpresora.enviaCadena(strTmp);
				if (intLineas > 54) {
					intPagina++;
					if (iDriver == 0) {
						tpImpresora.InserteDocumento();
						tpImpresora.activateControl("SELECCIONASLIP",iDriver);
						tpImpresora.enviaCadena("");
						tpImpresora.enviaCadena("");
						tpImpresora.enviaCadena("");
						tpImpresora.enviaCadena("");
						tpImpresora.enviaCadena("");
						tpImpresora.imprimeCadena1();
						tpImpresora.activateControl("SALTOHOJA",iDriver);
						tpImpresora.activateControl("SELECCIONAJOURNAL",iDriver);
					} else {
						if (iDriver == 1)
							tpImpresora.muestraVentana();
						tpImpresora.activateControl("SALTOHOJA",iDriver);
					}
					intLineas = 0;
				}
			}
			if (intLineas != 0)
			{
				if (iDriver == 0) {
					tpImpresora.InserteDocumento();
					tpImpresora.activateControl("SELECCIONASLIP",iDriver);
					tpImpresora.enviaCadena("");
					tpImpresora.enviaCadena("");
					tpImpresora.enviaCadena("");
					tpImpresora.enviaCadena("");
					tpImpresora.enviaCadena("");
					tpImpresora.imprimeCadena1();
					tpImpresora.activateControl("SALTOHOJA",iDriver);
					tpImpresora.activateControl("SELECCIONAJOURNAL",iDriver);
				} else {
					tpImpresora.activateControl("SALTOHOJA",iDriver);
				}				
			}
			
		}
		return intStatus;
	}	
}