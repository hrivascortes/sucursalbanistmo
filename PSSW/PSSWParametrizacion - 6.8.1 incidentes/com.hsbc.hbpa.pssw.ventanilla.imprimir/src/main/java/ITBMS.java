import java.util.*;
public class ITBMS {
	public int comprobanteITBMS(
		Vector vDatos,
		String strRespuesta,
		String strDatosSuc,
		String strDate,
		String strCajero,
		String strConsecutivo,
		int iDriver) {
		ToolsPrinter tpImpresora = new ToolsPrinter();
		int intStatus = 0;
		String strFiller = "                                                                                ";
		String strOutLine = "";
		String strJustificacion = "                                        ";
		Vector vDatosSuc = new Vector();
		Vector vDatosTxn = new Vector();
		intStatus = tpImpresora.verificaStatus();
		if ((intStatus == 95) || (intStatus == 88)) {
			intStatus = 0;
			//~ITBMS~Txn~Mto Comision~Mto ITBMS~Mto Total~titular~Descripcion~ -> 1695 y 0360
			//~ITBMS~Txn~Monto~Mto Comision~Mto ITBMS~Mto Total~Serial~ -> 4155
			tpImpresora.activateControl("CERTIFICACION", iDriver);
			iDriver =0;
			//tpImpresora.activateControl("OPECONANC", iDriver);
			String strTxn = (String) vDatos.elementAt(1);
			if(strTxn.indexOf("*")!= -1){
				strTxn = strTxn.substring(0,strTxn.indexOf("*"))
						 + "  "
						 + strTxn.substring(strTxn.indexOf("*")+1);
			}
			
			String strMonto = "";
			String strComision = "";
			String strITBMS = "";
			String strTotal = "";
			String strDescrip = "";
			String strTitular = "";
			String strBenefi = "";
			String strCta = "";
			String strReferencia="";
			String strReferencia1695="";
			
			if(strTxn.equals("4155") || strTxn.equals("4545")){
				strMonto = (String) vDatos.elementAt(2);
				strComision = (String) vDatos.elementAt(3);
				strITBMS = (String) vDatos.elementAt(4);
				strTotal = (String) vDatos.elementAt(5);
				strDescrip = (String) vDatos.elementAt(6);
				if(strTxn.equals("4545"))
				{
					strCta = (String) vDatos.elementAt(7);
					strBenefi = (String) vDatos.elementAt(8);;
				}
				
			}else{
				strComision = (String) vDatos.elementAt(2);
				strITBMS = (String) vDatos.elementAt(3);
				strTotal = (String) vDatos.elementAt(4);
				if(strTxn.equals("1695")){
				strTitular = (String) vDatos.elementAt(5);
				strDescrip = (String) vDatos.elementAt(6);
				strReferencia1695=(String) vDatos.elementAt(7);
				}
				else
				{
					strDescrip = (String) vDatos.elementAt(5);
					strReferencia = (String) vDatos.elementAt(6);
				}
			}
			
			if (strMonto.equals(""))
				strMonto = "000";
			if (strComision.equals(""))
				strComision = "000";
			if (strITBMS.equals(""))
				strITBMS = "000";
			if (strTotal.equals(""))
				strTotal = "000";
			
			strMonto = tpImpresora.formatMonto(strMonto);
			strComision = tpImpresora.formatMonto(strComision);
			strITBMS = tpImpresora.formatMonto(strITBMS);
			strTotal = tpImpresora.formatMonto(strTotal);
			
			vDatosSuc = tpImpresora.Campos(strDatosSuc, "@");
			System.out.println("vDatosSuc: " + vDatosSuc.toString());
			String strNomSuc = (String) vDatosSuc.elementAt(0);
			strNomSuc = strNomSuc.substring(7, strNomSuc.length());
			String strNumSuc = (String) vDatosSuc.elementAt(1);
			strNumSuc = strNumSuc.substring(7, strNumSuc.length());
			//vDatosTxn = tpImpresora.Campos(strRespuesta, "~");
			tpImpresora.enviaCadena("");
			tpImpresora.enviaCadena("");
			
			if(!strTxn.equals("4155") && !strTxn.equals("4545")){
				strOutLine = strConsecutivo
							 + "    "
							 + strDate.substring(7, 9) + "-"
							 + strDate.substring(5, 7)
							 + "-"
							 + strDate.substring(1, 5)
							 + "    "
							 + strTxn
							 + "    "
							 + strCajero;
				tpImpresora.enviaCadena(strOutLine);
				strOutLine = strNumSuc + "-" + strNomSuc;
				tpImpresora.enviaCadena(strOutLine);
				strOutLine = strDescrip;
				if(strTxn.equals("1695")){
			    tpImpresora.enviaCadena(strOutLine);
				strOutLine = strTitular;
				tpImpresora.enviaCadena(strOutLine);
				strOutLine = strReferencia1695;
				}
				else{
				    tpImpresora.enviaCadena(strOutLine);
					strOutLine = strReferencia;					
				}
				tpImpresora.enviaCadena(strOutLine);
				strOutLine = "COMISION: " + strComision + "  ITBMS: " + strITBMS + "  TOTAL: " + strTotal;
				tpImpresora.enviaCadena(strOutLine);
			}else{
				String strCertificacion = strConsecutivo
										  + " "
										  + strDate.substring(7, 9)
										  + strDate.substring(5, 7)
										  + strDate.substring(1, 5)
										  + " "
										  + strTxn
										  + " "
										  + strCajero
										  + " ";
				if (strTxn.equals("4545")){
					strCertificacion =strCertificacion + " "+strCta;}
										  
				tpImpresora.enviaCadena(strCertificacion);
				strOutLine = "MONTO: " + strMonto + "  COMISION: " + strComision + "  ITBMS: " + strITBMS + "  TOTAL: " + strTotal;
				tpImpresora.enviaCadena(strOutLine);
				if(strTxn.equals("4155")){
					strOutLine = "EXPED. CHEQUE DE GERENCIA " + strDescrip;
					tpImpresora.enviaCadena(strOutLine);
				}else{
					strOutLine = strBenefi +"  " +strDescrip;
					tpImpresora.enviaCadena(strOutLine);
				}
				strOutLine = "SUCURSAL: " + strNumSuc + "  " + strNomSuc;
				tpImpresora.enviaCadena(strOutLine);
				strOutLine = "CONSECUTIVO: " + strConsecutivo;
				tpImpresora.enviaCadena(strOutLine);
				
			}
			tpImpresora.enviaCadena("");
			tpImpresora.enviaCadena("");
			if (iDriver == 0) {
				tpImpresora.InserteDocumento();
				tpImpresora.activateControl("SELECCIONASLIP", iDriver);
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.imprimeCadena1();
				tpImpresora.activateControl("SALTOHOJA", iDriver);
				tpImpresora.activateControl("SELECCIONAJOURNAL", iDriver);
				//tpImpresora.activateControl("CLOCONANC", iDriver);
			} else
				tpImpresora.activateControl("SALTOHOJA", iDriver);
			    //tpImpresora.activateControl("CLOCONANC", iDriver);
		}
		return intStatus;
	}
}