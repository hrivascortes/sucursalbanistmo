import java.util.*;

public class Giros {
	String strMes[] =
		{
			"Enero ",
			"Febrero ",
			"Marzo ",
			"Abril ",
			"Mayo ",
			"Junio ",
			"Julio ",
			"Agosto ",
			"Septiembre ",
			"Octubre ",
			"Noviembre ",
			"Diciembre " };
	ToolsPrinter tpImpresora = new ToolsPrinter();
	int intStatus;
	String strFiller =
		"                                                                                                              ";
	String strOut = "";
	Vector vDatosSuc = new Vector();

	public int imprimeGiro1(
		Vector vDatos,
		String strDatosSuc,
		String strCajero,
		String strDate,
		String strTime,
		int iDriver) {
		intStatus = tpImpresora.verificaStatus();
		if ((intStatus == 95) || (intStatus == 88)) {
			tpImpresora.activateControl("CERTIFICACION",iDriver);
			tpImpresora.enviaCadena(String.valueOf(intStatus));
			tpImpresora.enviaCadena("+++++++11+");

			intStatus = 0;
			String strTipo = (String) vDatos.elementAt(1);
			if (strTipo.equals(""))
				strTipo = " ";
			String strGiro = (String) vDatos.elementAt(2);
			if (strGiro.equals(""))
				strGiro = " ";
			String strImporte = (String) vDatos.elementAt(3);
			if (strImporte.equals(""))
				strImporte = "000";
			strImporte = tpImpresora.formatMonto(strImporte);
			String strComision = (String) vDatos.elementAt(4);
			if (strComision.equals(""))
				strComision = "000";
			strComision = tpImpresora.formatMonto(strComision);
			String strIva = (String) vDatos.elementAt(5);
			if (strIva.equals(""))
				strIva = "000";
			strIva = tpImpresora.formatMonto(strIva);
			String strTotal = (String) vDatos.elementAt(6);
			if (strTotal.equals(""))
				strTotal = "000";
			strTotal = tpImpresora.formatMonto(strTotal);
			String strBeneficiario = (String) vDatos.elementAt(8);
			if (strBeneficiario.equals(""))
				strBeneficiario = " ";
			String strOrdenante = (String) vDatos.elementAt(7);
			if (strOrdenante.equals(""))
				strOrdenante = " ";
			String strCuenta = (String) vDatos.elementAt(9);
			if (strCuenta.equals(""))
				strCuenta = " ";
			vDatosSuc = tpImpresora.Campos(strDatosSuc, "@");
			String strPlaza = (String) vDatosSuc.elementAt(0);
			strPlaza = strPlaza.substring(7, strPlaza.length());
			if (!strTipo.equals("CAN")) {
				tpImpresora.enviaCadena("\n\n\n\n\n");
				int intMes = Integer.parseInt(strDate.substring(5, 7)) - 1;
				strOut =
					strFiller.substring(0, 92)
						+ strMes[intMes]
						+ strDate.substring(7, 9)
						+ ", "
						+ strDate.substring(1, 5);
				tpImpresora.enviaCadena(strOut);
				strOut = "           " + strGiro;
				tpImpresora.enviaCadena(strOut);
				strOut = strFiller.substring(0, 80) + strImporte + "\n\n";
				tpImpresora.enviaCadena(strOut);
				strOut = "          " + strBeneficiario + "\n";
				tpImpresora.enviaCadena(strOut);
				strOut =
					"                         "
						+ Currency2.convert(strImporte)
						+ "\n\n\n\n\n\n\n";
				tpImpresora.enviaCadena(strOut);
			}
			if (strTipo.equals("CAN")) {
				if (strCuenta.equals("0000000000"))
					tpImpresora.enviaCadena(
						"         CANCELACION DE GIRO EN  DOLARES EN EFECTIVO\n");
				else
					tpImpresora.enviaCadena(
						"      CANCELACION DE GIRO EN DOLARES CON ABONO A CUENTA\n");
			} else if (strTipo.equals("EXP")) {
				if (strCuenta.equals("0000000000"))
					tpImpresora.enviaCadena(
						"           EXPEDICION GIROS EN DOLARES EN EFECTIVO\n");
				else
					tpImpresora.enviaCadena(
						"       EXPEDICION GIROS EN DOLARES CON CARGO A CUENTA\n");
			} else
				tpImpresora.enviaCadena(
					"           CARGO POR EXPEDICION DE GIRO EN DOLARES\n");
			strOut =
				"    "
					+ strCajero.substring(0, 4)
					+ "     "
					+ strCajero
					+ strFiller.substring(0, 20)
					+ strDate.substring(7, 9)
					+ "-"
					+ strDate.substring(5, 7)
					+ "-"
					+ strDate.substring(1, 5)
					+ "          "
					+ strTime.substring(0, 2)
					+ ":"
					+ strTime.substring(2, 4);
			tpImpresora.enviaCadena(strOut);
			if (strTipo.equals("EXP") || strTipo.equals("CAN")) {
				if (strCuenta.equals("0000000000"))
					strOut = " N�m. Giro: " + strGiro;
				else if (strTipo.equals("CAN"))
					strOut =
						" N�m. Giro: "
							+ strGiro
							+ strFiller.substring(0, 19 - strGiro.length())
							+ "  Cuenta      : "
							+ strCuenta;
				else
					strOut =
						" N�m. Giro: "
							+ strGiro
							+ strFiller.substring(0, 19 - strGiro.length())
							+ "  Cuenta Cargo: "
							+ strCuenta;
			} else
				strOut =
					" N�m. Giro: "
						+ strGiro
						+ strFiller.substring(0, 19 - strGiro.length())
						+ "  Cuenta      : "
						+ strCuenta;
			tpImpresora.enviaCadena(strOut);
			if (!strTipo.equals("CAN")) {
				if (strOrdenante.length() < 30) {
					strOut =
						" Importe  :   $ "
							+ strFiller.substring(0, 15 - strImporte.length())
							+ strImporte
							+ "  Ordenante   : "
							+ strOrdenante;
					tpImpresora.enviaCadena(strOut);
				} else {
					strOut =
						" Importe  :   $ "
							+ strFiller.substring(0, 15 - strImporte.length())
							+ strImporte
							+ "  Ordenante   : "
							+ strOrdenante.substring(0, 30);
					tpImpresora.enviaCadena(strOut);
					strOut =
						strFiller.substring(0, 47)
							+ strOrdenante.substring(30, strOrdenante.length());
					tpImpresora.enviaCadena(strOut);
				}
				if (strBeneficiario.length() < 30) {
					strOut =
						" Comisi�n :   $ "
							+ strFiller.substring(0, 15 - strComision.length())
							+ strComision
							+ "  Beneficiario: "
							+ strBeneficiario;
					tpImpresora.enviaCadena(strOut);
				} else {
					strOut =
						" Comisi�n :   $ "
							+ strFiller.substring(0, 15 - strComision.length())
							+ strComision
							+ "  Beneficiario: "
							+ strBeneficiario;
					tpImpresora.enviaCadena(strOut);
				}
				strOut =
					" I.V.A.   :   $ "
						+ strFiller.substring(0, 15 - strIva.length())
						+ strIva
						+ "  Plaza        : "
						+ strPlaza;
				tpImpresora.enviaCadena(strOut);
				strOut =
					" Total    :   $ "
						+ strFiller.substring(0, 15 - strTotal.length())
						+ strTotal;
				tpImpresora.enviaCadena(strOut);
			} else {
				strOut = " Ordenante   : " + strOrdenante;
				tpImpresora.enviaCadena(strOut);
				strOut = " Beneficiario: " + strBeneficiario + "\n\n";
				tpImpresora.enviaCadena(strOut);
				strOut =
					" Importe  :   $ "
						+ strFiller.substring(0, 15 - strImporte.length())
						+ strImporte;
				tpImpresora.enviaCadena(strOut);
				strTotal = strImporte;
			}
			strOut =
				" Monto en Letra :     "
					+ Currency.convert(strTotal.trim(), 2)
					+ "\n";
			tpImpresora.enviaCadena(strOut);
			tpImpresora.enviaCadena(
				"                               Ordenante\n\n");
			tpImpresora.enviaCadena(
				"                          __________________ ");
			int intCentro = (18 - strOrdenante.trim().length()) / 2;
			strOut =
				strFiller.substring(0, 26 + intCentro) + strOrdenante.trim();
			tpImpresora.enviaCadena(strOut);
			if (!strCuenta.equals("0000000000"))
				if (!strTipo.equals("CAN"))
					tpImpresora.enviaCadena(
						"              - AUTORIZO CARGO EN MI CUENTA DE CHEQUES -");
			if (iDriver == 0) {
				tpImpresora.InserteDocumento();
				tpImpresora.activateControl("SELECCIONASLIP",iDriver);
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.imprimeCadena1();
				tpImpresora.activateControl("SALTOHOJA",iDriver);
				tpImpresora.activateControl("SELECCIONAJOURNAL",iDriver);
			} else {
				tpImpresora.activateControl("SALTOHOJA",iDriver);
			}
		}
		return intStatus;
	}

	public int imprimeGiro2(
		Vector vDatos,
		String strDatosSuc,
		int iDriver) {
		intStatus = tpImpresora.verificaStatus();
		if ((intStatus == 95) || (intStatus == 88)) {
			tpImpresora.activateControl("CERTIFICACION",iDriver);
			tpImpresora.enviaCadena(String.valueOf(intStatus));
			tpImpresora.enviaCadena("++++++++");
			intStatus = 0;
			String strTipo = (String) vDatos.elementAt(1);
			if (strTipo.equals(""))
				strTipo = " ";
			String strGiro = (String) vDatos.elementAt(2);
			if (strGiro.equals(""))
				strGiro = " ";
			String strImporte = (String) vDatos.elementAt(3);
			if (strImporte.equals(""))
				strImporte = "000";
			strImporte = tpImpresora.formatMonto(strImporte);
			String strBeneficiario = (String) vDatos.elementAt(4);
			if (strBeneficiario.equals(""))
				strBeneficiario = " ";
			String strOrdenante = (String) vDatos.elementAt(5);
			if (strOrdenante.equals(""))
				strOrdenante = " ";
			String strMoneda1 = (String) vDatos.elementAt(6);
			if (strMoneda1.equals(""))
				strMoneda1 = " ";
			String strMoneda2 = (String) vDatos.elementAt(7);
			if (strMoneda2.equals(""))
				strMoneda2 = " ";
			String strLinea1 = (String) vDatos.elementAt(8);
			if (strLinea1.equals(""))
				strLinea1 = " ";
			String strLinea2 = (String) vDatos.elementAt(9);
			if (strLinea2.equals(""))
				strLinea2 = " ";
			String strLinea3 = (String) vDatos.elementAt(10);
			if (strLinea3.equals(""))
				strLinea3 = " ";
			vDatosSuc = tpImpresora.Campos(strDatosSuc, "@");
			String strPlaza = (String) vDatosSuc.elementAt(0);
			strPlaza = strPlaza.substring(7, strPlaza.length());
			tpImpresora.enviaCadena("\n\n\n");
			strOut =
				strFiller.substring(0, 54)
					+ tpImpresora.getDate(" MMM d, yyyy ");
			tpImpresora.enviaCadena(strOut);
			tpImpresora.enviaCadena(" ");
			strOut = strFiller.substring(0, 55) + strImporte + "\n\n";
			tpImpresora.enviaCadena(strOut);
			strOut = "            " + strBeneficiario + "\n";
			tpImpresora.enviaCadena(strOut);
			String strOut1 = "          " + Currency2.convert(strImporte);
			strOut = strOut1.substring(0, strOut1.length() - 18);
			String strOut2 =
				strOut1.substring(strOut1.length() - 8, strOut1.length());
			strOut = strOut + strMoneda2 + strOut2 + "\n\n";
			strOut2 = strOut;
			tpImpresora.enviaCadena(strOut);
			tpImpresora.enviaCadena("\n");
			tpImpresora.enviaCadena(strLinea1);
			tpImpresora.enviaCadena(strLinea2);
			tpImpresora.enviaCadena(strLinea3);
			tpImpresora.enviaCadena("\n\n\n\n");
			tpImpresora.enviaCadena(
				"                 EXPEDICION GIROS EN DIVISAS\n");
			strOut =
				strFiller.substring(0, 50)
					+ tpImpresora.getDate(" MMM d, yyyy ");
			tpImpresora.enviaCadena(strOut);
			strOut = " NUMERO DE GIRO : " + strGiro;
			tpImpresora.enviaCadena(strOut);
			strOut = " IMPORTE        : " + strImporte + " " + strMoneda1;
			tpImpresora.enviaCadena(strOut);
			strOut = " BENEFICIARIO   : " + strBeneficiario;
			tpImpresora.enviaCadena(strOut);
			strOut = " ORDENANTE      : " + strOrdenante;
			tpImpresora.enviaCadena(strOut);
			strOut = " IMPORTE EN LETRA :     " + strOut2;
			tpImpresora.enviaCadena(strOut);
			strOut = " PAIS           : " + strLinea1;
			tpImpresora.enviaCadena(strOut);
			strOut = "                  " + strLinea2;
			tpImpresora.enviaCadena(strOut);
			strOut = "                  " + strLinea3 + "\n";
			tpImpresora.enviaCadena(strOut);
			strOut = " PLAZA          : " + strPlaza;
			tpImpresora.enviaCadena(strOut);
			if (iDriver == 0) {
				tpImpresora.InserteDocumento();
				tpImpresora.activateControl("SELECCIONASLIP",iDriver);
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.imprimeCadena1();
				tpImpresora.activateControl("SALTOHOJA",iDriver);
				tpImpresora.activateControl("SELECCIONAJOURNAL",iDriver);
			} else {
				tpImpresora.activateControl("SALTOHOJA",iDriver);
			}
		}
		return intStatus;
	}
	
	public int imprimeGiro3(
		Vector vDatos,
		String strDatosSuc,
		String strRespuesta,
		String strDate,
		int iDriver) {
		intStatus = tpImpresora.verificaStatus();
		if ((intStatus == 95) || (intStatus == 88)) {
			intStatus = 0;
			tpImpresora.activateControl("CERTIFICACION",iDriver);
			Vector vCampos = tpImpresora.Campos(strRespuesta, "^");
			Vector vCamposT = new Vector();
			int intSize = vCampos.size();
			String strTmp = new String("");
			for (int i = 0; i < intSize; i++) {
				strTmp = (String) vCampos.elementAt(i);
				if (strTmp.startsWith("COMPROBANTE_GIRO")) {
					strTmp = (String) vCampos.elementAt(i);
					vCamposT = tpImpresora.Campos(strTmp, "@");
					strTmp = (String) vCamposT.elementAt(1);
					vCamposT = tpImpresora.Campos(strTmp, "|");
					strTmp = (String) vCamposT.elementAt(0);
					vCamposT = tpImpresora.Campos(strTmp, "~");
				}
			}
			tpImpresora.activateControl("CERTIFICACION",iDriver);
			intStatus = 0;
			String strTipo = (String) vDatos.elementAt(1);
			if (strTipo.equals(""))
				strTipo = " ";
			String strGiro = (String) vDatos.elementAt(2);
			if (strGiro.equals(""))
				strGiro = " ";
			String strMoneda = (String) vCamposT.elementAt(0);
			if (strMoneda.equals(""))
				strMoneda = "";
			String strBeneficiario = (String) vCamposT.elementAt(1);
			if (strBeneficiario.equals(""))
				strBeneficiario = " ";
			String strImporte = (String) vCamposT.elementAt(2);
			if (strImporte.equals(""))
				strImporte = "000";
			strImporte = tpImpresora.formatMonto(strImporte);
			String strBanco = (String) vCamposT.elementAt(3);
			if (strBanco.equals(""))
				strBanco = " ";
			String strPlaza = (String) vCamposT.elementAt(4);
			if (strPlaza.equals(""))
				strPlaza = " ";							
			tpImpresora.enviaCadena("\n\n\n\n\n");
			int intMes = Integer.parseInt(strDate.substring(5, 7)) - 1;
			vDatosSuc = tpImpresora.Campos(strDatosSuc, "@");
			String strNumSuc = (String) vDatosSuc.elementAt(1);
			strNumSuc = strNumSuc.substring(7, strNumSuc.length());			
			strOut = strFiller.substring(0, 70)
					+ " SUC. " + strNumSuc 
					+ strFiller.substring(0, 12)
					+ strMes[intMes]
					+ strDate.substring(7, 9)
					+ ", "
					+ strDate.substring(1, 5);
			tpImpresora.enviaCadena(strOut);
			//strOut = "           " + strGiro;
			//tpImpresora.enviaCadena(strOut);
			tpImpresora.enviaCadena("  ");
			strOut = strFiller.substring(0, 10) 
					+ strBeneficiario 
					+ strFiller.substring(0, 70-strBeneficiario.length()) + strImporte + "\n\n\n";
			tpImpresora.enviaCadena(strOut);
			//strOut = "          " + strBeneficiario + "\n";
			//tpImpresora.enviaCadena(strOut);
			if (strMoneda.equals("02"))
			{
			strOut =
				"                         "
					+ Currency2.convert(strImporte)
					+ "\n\n\n";
			}
			else
			{
			strOut =
				"                         "
					+ Currency.convert(strImporte,1)
					+ "\n\n\n";
			}			
			tpImpresora.enviaCadena(strOut);
			strOut = strFiller.substring(0, 10) + "A : "
					+ strBanco ;
			tpImpresora.enviaCadena(strOut);			
			strOut = strFiller.substring(0, 10) + "    "
					+ strPlaza ;
			tpImpresora.enviaCadena(strOut);			
			if (iDriver == 0) {
				tpImpresora.InserteDocumento();
				tpImpresora.activateControl("SELECCIONASLIP",iDriver);
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.imprimeCadena1();
				tpImpresora.activateControl("SALTOHOJA",iDriver);
				tpImpresora.activateControl("SELECCIONAJOURNAL",iDriver);
			} else {
				tpImpresora.activateControl("SALTOHOJA",iDriver);
			}
		}
		return intStatus;
	}	
}