//*************************************************************************************************
//	Funcion: Clase que imprime cheques
//	Elemento: OrdenesPago.java
//	Creado por: Juan Carlos Gaona Marcial
//	Modificado por: Rodrigo Escand�n Soto
//*************************************************************************************************
// CCN - 4620038 - 22/11/2007 - Se ambienta el formato de impresi�n
//*************************************************************************************************


import java.util.*;

public class Cheques {
	ToolsPrinter tpImpresora = new ToolsPrinter();
	int intStatus;
	String strFiller =
		"                                                                                                                                                                ";
	String strFiller1 = "****************************************************************************************************";
	String strFiller2 = "012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789";

	String strOut = "";
	String strAst = "";
	Vector vDatosSuc = new Vector();

	public String agrega_espacios(String str, int longitud) {
		while (str.length() <= longitud)
			str = str + " ";
		return str;
	}

	public String agrega_asteriscos(String str, int longitud) {
		while (str.length() <= longitud)
			str = str + "*";
		return str;
	}

	public int imprimeChequeGer(
			Vector vDatos,
			String strDatosSuc,
			String strDate,
			int iDriver) {
			intStatus = tpImpresora.verificaStatus();
			if ((intStatus == 95) || (intStatus == 88)) {
				tpImpresora.activateControl("OPECONANC", iDriver);
				tpImpresora.activateControl("CERTIFICACION",iDriver);
				vDatosSuc = tpImpresora.Campos(strDatosSuc, "@");
				intStatus = 0;

				/*String strPlaza 	= (String) vDatosSuc.elementAt(5);
				strPlaza 			= strPlaza.substring(7, strPlaza.length());
				strPlaza 			= strPlaza.trim();*/

				String strNomSuc	= (String) vDatosSuc.elementAt(0);
				strNomSuc 			= strNomSuc.substring(7, strNomSuc.length());
				strNomSuc 			= strNomSuc.trim();

				String strBeneficiario = (String) vDatos.elementAt(1);
				String strMoneda 	= (String) vDatos.elementAt(2);
				String strMonto 	= (String) vDatos.elementAt(3);
				String strNoCheque 	= (String) vDatos.elementAt(4);
				String strSucursal 	= (String) vDatos.elementAt(5);
				String strCuenta 	= (String) vDatos.elementAt(6);

				String strDetalle 	= (String) vDatos.elementAt(7);
			    String strAdress 	= (String) vDatos.elementAt(8);

				String strPAutoriz 	= (String) vDatos.elementAt(9);
				String strCedula 	= (String) vDatos.elementAt(10);

				if(strDetalle.equals(""))
				strDetalle = "NA";
				if(strAdress.equals(""))
				strAdress = "NA";

				if(strPAutoriz.equals(""))
				strPAutoriz = "NA";
				if(strCedula.equals(""))
				strCedula = "NA";


				//tpImpresora.enviaCadena(strFiller2);
				tpImpresora.enviaCadena("\n\n");
				if (iDriver == 0) {

					strOut =
							  strFiller.substring(0, 141)
							//+ strFiller.substring(0, 15)
							+ strDate.substring(7, 8)
							+ "  "
							+ strDate.substring(8, 9)
							+ "  "
							+ strDate.substring(5, 6)
							+ "  "
							+ strDate.substring(6, 7)
							+ "  "
							+ strDate.substring(1, 2)
							+ "  "
							+ strDate.substring(2, 3)
							+ "  "
							+ strDate.substring(3, 4)
							+ "  "
							+ strDate.substring(4, 5);
							//+ "\n";
					tpImpresora.enviaCadena(strOut);

					if (strBeneficiario.length() <= 56) {
						strOut = "\n\n\n";
					} else if (strBeneficiario.length() <= 112) {
						strOut = "\n\n";
					} else {
						strOut = "\n";
					}

					int posStrBenef = 0;
					// Longitud del Beneficiario menor a 56
					while ((strBeneficiario.length()-posStrBenef) > 56) {
						int nroSubstFin = ((strBeneficiario.length()-posStrBenef) > 112)? 80 : 56;
						strOut += strBeneficiario.substring(posStrBenef, posStrBenef+=nroSubstFin);
						tpImpresora.enviaCadena(strOut);
						strOut = "";
					}
					if ((strBeneficiario.length()-posStrBenef) <= 56) {
						String strTmpBenf = strBeneficiario.substring(posStrBenef, strBeneficiario.length());
						strOut += strTmpBenf
								+ strFiller1.substring(0, 56 - strTmpBenf.length())
								+ "     "+ "USD*"+tpImpresora.formatMonto(strMonto)+"***";
						tpImpresora.enviaCadena(strOut);
					}

					String strMontoDesc = Currency.convert(tpImpresora.formatMonto(strMonto), Integer.parseInt(strMoneda));
					if (strMontoDesc.length() > 72) {
						strOut = strMontoDesc.substring(0, 54);
						tpImpresora.enviaCadena(strOut);

						strOut = strMontoDesc.substring(54, strMontoDesc.length()) + "\n";
						tpImpresora.enviaCadena(strOut);
					}
					else {
						strOut = "\n" + strMontoDesc + "\n";
						tpImpresora.enviaCadena(strOut);
					}
				}
				else
				{
				   //System.out.println("uno");
	               strOut =
						   strFiller.substring(0, 93)
						   + "\n";
					tpImpresora.enviaCadena(strOut);

					strOut =
							  strFiller.substring(0, 78)
							+ strFiller.substring(0, 15)
							+ strDate.substring(7, 9)
							+ "/"
							+ strDate.substring(5, 7)
							+ "/"
							+ strDate.substring(1, 5)
							+ "\n";
					tpImpresora.enviaCadena(strOut);
					strOut =
							  strFiller.substring(0, 20)
							+ strBeneficiario
							+ strFiller.substring(0, 40 - strBeneficiario.length())
							+ strFiller1.substring(0, 33)
							+ "  "+ "USD*"+tpImpresora.formatMonto(strMonto)+"***"
							+ "\n";
					tpImpresora.enviaCadena(strOut);
					strOut =
						"                 "
							//+ Currency.convert(strMonto, Integer.parseInt(strMoneda))
							+ Currency.convert(tpImpresora.formatMonto(strMonto), Integer.parseInt(strMoneda))
							+ "\n\n\n\n\n";
					tpImpresora.enviaCadena(strOut);

				}

				//modificacion
				//tpImpresora.enviaCadena("\n\n\n\n\n");

				//tpImpresora.enviaCadena("\n\n");

				strOut= strFiller.substring(0, 25) + strNoCheque;
				tpImpresora.enviaCadena(strOut);

				strOut= strFiller.substring(0, 25) + "SUC. "+ strSucursal +" "+ strNomSuc;
				tpImpresora.enviaCadena(strOut);


				tpImpresora.enviaCadena("\n\n\n\n\n\n\n");


				if (strCuenta.equals(""))
					strCuenta = "Efectivo Recibido";

//	modificacion
				//strOut = "        Debito a Cuenta  : " + strCuenta;

			strOut =
					"Debito a Cuenta  : "
					+ strCuenta + strFiller.substring(0, 25 - strCuenta.length());

				if (strDetalle.length() > 20) {
					tpImpresora.enviaCadena(strOut);

					strOut = "Detalle de Pago  : " + strDetalle;
				} else {
					strOut += "Detalle de Pago : " + strDetalle;
				}

				tpImpresora.enviaCadena(strOut);

//	modificacion
			//	strOut = "        Credito a Cuenta : Cheque de Gerencia";

	  			strOut = "Credito a Cuenta : Cheque de Gerencia"
						 + strFiller.substring(0, 7);

	  			if (strAdress.length() > 15) {
					tpImpresora.enviaCadena(strOut);

					strOut = "Direccion Beneficiario: " + strAdress;
	  			} else {
	  				strOut += "Direccion Beneficiario : " + strAdress;
	  			}

				tpImpresora.enviaCadena(strOut);

				strOut = "Beneficiario     : ";

				int posStrBenef = 0;
				while ((strBeneficiario.length()-posStrBenef) > 62) {
					strOut += strBeneficiario.substring(posStrBenef, posStrBenef+=62);
					tpImpresora.enviaCadena(strOut);
					strOut = "                   ";
				}
				if ((strBeneficiario.length()-posStrBenef) <= 62) {
					strOut += strBeneficiario.substring(posStrBenef, strBeneficiario.length());
					tpImpresora.enviaCadena(strOut);
				}

//	modificacion

				/*strOut = "        Importe          : USD$ " + tpImpresora.formatMonto(strMonto) + " USD.";
				tpImpresora.enviaCadena(strOut);

				strOut =
					"        No. Documento    : "
						+ strNoCheque;
				tpImpresora.enviaCadena(strOut);*/
				String strMontoFormat =  tpImpresora.formatMonto(strMonto);
				strOut = "Importe          : USD$ " + strMontoFormat + " USD."
						 + strFiller.substring(0, 15 - strMontoFormat.length());

				if (strPAutoriz.length() > 19) {
					tpImpresora.enviaCadena(strOut);

					strOut = "Persona Autorizada : " + strPAutoriz;
	  			} else {
	  				strOut += "Persona Autorizada : " + strPAutoriz;
	  			}

				tpImpresora.enviaCadena(strOut);

				strOut =
						 "No. Documento    : "
						 + strNoCheque
						 + strFiller.substring(0, 25 - strNoCheque.length())
						 + "Cedula             : " + strCedula;
				tpImpresora.enviaCadena(strOut);


				strOut =
						"                   "
						//+ strNomSuc
						//+ " "
						+ strDate.substring(7, 9)
						+ "/"
						+ strDate.substring(5, 7)
						+ "/"
						+ strDate.substring(1, 5);
				String strLinea = "____________________________________";
				//strOut = agrega_espacios(strOut, 20);
				strOut += strFiller.substring(0, 15) + strLinea;
				tpImpresora.enviaCadena(strOut);

				strOut =
						"                   "
						+ strSucursal
						+ " "
						+ strNomSuc
						+ strFiller.substring(0, 12 - (strSucursal.length()) + strNomSuc.length())
						+ "Firma del Cliente ";
				tpImpresora.enviaCadena(strOut);


			/*	strOut =
					"        Persona Autorizada : "
						+ strPAutoriz;
				tpImpresora.enviaCadena(strOut);

				strOut =
					"        Cedula           : "
						+ strCedula;
				tpImpresora.enviaCadena(strOut);	*/


				if (iDriver == 0) {
					tpImpresora.InserteDocumento();
					tpImpresora.activateControl("SELECCIONASLIP",iDriver);
					tpImpresora.enviaCadena("");
					tpImpresora.enviaCadena("");
					tpImpresora.enviaCadena("");
					tpImpresora.enviaCadena("");
					tpImpresora.enviaCadena("");
					tpImpresora.imprimeCadena1();
					tpImpresora.activateControl("SALTOHOJA",iDriver);
					tpImpresora.activateControl("SELECCIONAJOURNAL",iDriver);
				} else {
					tpImpresora.activateControl("SALTOHOJA",iDriver);
				}
			}
			return intStatus;
		}

		public int imprimeChequeGerDes(
		    Vector vDatos,
			String strDatosSuc,
			String strDate,
			int iDriver) {
			intStatus = tpImpresora.verificaStatus();
			if ((intStatus == 95) || (intStatus == 88)) {

				tpImpresora.activateControl("OPECONANC", iDriver);
					tpImpresora.activateControl("CERTIFICACION2",iDriver);

				vDatosSuc = tpImpresora.Campos(strDatosSuc, "@");
				intStatus = 0;

				/*String strPlaza 	= (String) vDatosSuc.elementAt(5);
				strPlaza 			= strPlaza.substring(7, strPlaza.length());
				strPlaza 			= strPlaza.trim();*/

				String strNomSuc 	= (String) vDatosSuc.elementAt(5);
				strNomSuc 			= strNomSuc.substring(7, strNomSuc.length());
				strNomSuc 			= strNomSuc.trim();

				String strBeneficiario = (String) vDatos.elementAt(1);
				String strMoneda 	= (String) vDatos.elementAt(2);
				String strMonto 	= (String) vDatos.elementAt(3);
				String strNoCheque 	= (String) vDatos.elementAt(4);
				String strSucursal 	= (String) vDatos.elementAt(5);
				String strCuenta 	= (String) vDatos.elementAt(6);

				String strDetalle 	= (String) vDatos.elementAt(7);
			    String strAdress 	= (String) vDatos.elementAt(8);


				if(strDetalle.equals(""))
				strDetalle = "NA";
				if(strAdress.equals(""))
				strAdress = "NA";


				//tpImpresora.enviaCadena(strFiller2);
				tpImpresora.enviaCadena("\n\n");
				if (iDriver == 0) {

					strOut =
							  strFiller.substring(0, 145)
							//+ strFiller.substring(0, 15)
							+ strDate.substring(7, 9)
							+ "/"
							+ strDate.substring(5, 7)
							+ "/"
							+ strDate.substring(1, 5);
							//+ "\n";
					tpImpresora.enviaCadena(strOut);

					//Longitud del Beneficiario menor a 101
					if(strBeneficiario.length()<101){
						//tpImpresora.enviaCadena("\n");

						strOut =
								  "\n"
								+ strFiller.substring(0, 33)
								+ strBeneficiario
								+ strFiller1.substring(0, 100 - strBeneficiario.length())
								+ strFiller1.substring(0, 14)
								+ "    "+ "USD*"+tpImpresora.formatMonto(strMonto)+"***"
								+ "\n";
						tpImpresora.enviaCadena(strOut);

						strOut =
							"                             "
								+ Currency.convert(tpImpresora.formatMonto(strMonto), Integer.parseInt(strMoneda))
								+ "\n\n\n\n\n\n\n";
						tpImpresora.enviaCadena(strOut);
					}

					//Longitud del Beneficiario mayor a 101
					if(strBeneficiario.length()>101){
						strAst= strBeneficiario.substring(79,strBeneficiario.length());

						strOut =    strFiller.substring(0, 33) +
									strBeneficiario.substring(0,79);
					    tpImpresora.enviaCadena(strOut);

						strOut =  strFiller.substring(0, 33) +
								  strBeneficiario.substring(79,strBeneficiario.length())
								+ strFiller1.substring(0, 101-strAst.length())
								+ strFiller1.substring(0, 16)
								+ "    "+ "USD*"+tpImpresora.formatMonto(strMonto)+"***"
								+ "\n";
						tpImpresora.enviaCadena(strOut);

						strOut =
							"                             "
								+ Currency.convert(tpImpresora.formatMonto(strMonto), Integer.parseInt(strMoneda))
								+ "\n\n\n\n\n\n\n";
						tpImpresora.enviaCadena(strOut);
					}
				}
				else
				{
				   //System.out.println("uno");
	               strOut =
						   strFiller.substring(0, 93)
						   + "\n";
					tpImpresora.enviaCadena(strOut);

					strOut =
							  strFiller.substring(0, 78)
							+ strFiller.substring(0, 15)
							+ strDate.substring(7, 9)
							+ "/"
							+ strDate.substring(5, 7)
							+ "/"
							+ strDate.substring(1, 5)
							+ "\n";
					tpImpresora.enviaCadena(strOut);
					strOut =
							  strFiller.substring(0, 20)
							+ strBeneficiario
							+ strFiller.substring(0, 40 - strBeneficiario.length())
							+ strFiller1.substring(0, 33)
							+ "  "+ "USD*"+tpImpresora.formatMonto(strMonto)+"***"
							+ "\n";
					tpImpresora.enviaCadena(strOut);
					strOut =
						"                 "
							+ Currency.convert(strMonto, Integer.parseInt(strMoneda))
							+ "\n\n\n\n\n\n\n";
					tpImpresora.enviaCadena(strOut);

				}

				//modificacion
				//tpImpresora.enviaCadena("\n\n\n\n\n");

				//tpImpresora.enviaCadena("\n\n");

				strOut= strFiller.substring(0, 85) + strNoCheque;
				tpImpresora.enviaCadena(strOut);

				strOut= strFiller.substring(0, 85) + "SUC. "+ strSucursal +" "+ strNomSuc;
				tpImpresora.enviaCadena(strOut);


				tpImpresora.enviaCadena("\n\n\n");




				if (strCuenta.equals(""))
					strCuenta = "Efectivo Recibido";

//	modificacion
				//strOut = "        Debito a Cuenta  : " + strCuenta;

			strOut =
					"        Debito a Cuenta  : "
	  				 + strCuenta + strFiller.substring(0, 54 - strCuenta.length())
					 + " Detalle de Pago : "
					 + strDetalle;

				tpImpresora.enviaCadena(strOut);

//	modificacion
			//	strOut = "        Credito a Cuenta : Cheque de Gerencia";

	  			strOut = "        Credito a Cuenta : Cheque de Gerencia"
	 					 + 	strFiller.substring(0, 36)
	  					 + " Direcci�n Beneficiario : "
	  					 + strAdress;


				tpImpresora.enviaCadena(strOut);




				if(strBeneficiario.length()<140)
				{
					strOut = "        Beneficiario     : " + strBeneficiario;
					tpImpresora.enviaCadena(strOut);
				}

				if(strBeneficiario.length()>140)
				{
					strOut = "        Beneficiario     : " + strBeneficiario.substring(0,120);
					tpImpresora.enviaCadena(strOut);

					strOut = "                           " + strBeneficiario.substring(120,strBeneficiario.length());
					tpImpresora.enviaCadena(strOut);
				}

//	modificacion

				/*strOut = "        Importe          : USD$ " + tpImpresora.formatMonto(strMonto) + " USD.";
				tpImpresora.enviaCadena(strOut);

				strOut =
					"        No. Documento    : "
						+ strNoCheque;
				tpImpresora.enviaCadena(strOut);*/

				strOut = "        Importe          : USD$ " + tpImpresora.formatMonto(strMonto) + " USD.";
				tpImpresora.enviaCadena(strOut);

				strOut =
					     "        No. Documento    : "
						 + strNoCheque;
				tpImpresora.enviaCadena(strOut);


				strOut =
					"                           "
						//+ strNomSuc
						//+ " "
						+ strDate.substring(7, 9)
						+ "/"
						+ strDate.substring(5, 7)
						+ "/"
						+ strDate.substring(1, 5);
				String strLinea = "____________________________________";
				strOut = agrega_espacios(strOut, 100);
				strOut = strOut + strLinea;
				tpImpresora.enviaCadena(strOut);

				strOut =
					"                           "
						+ strSucursal
						+ " "
						+ strNomSuc
						+ strFiller.substring(0, 45 - (strSucursal.length()) + strNomSuc.length())
						+ "                           Firma del Cliente         ";
				tpImpresora.enviaCadena(strOut);


			/*	strOut =
					"        Persona Autorizada : "
						+ strPAutoriz;
				tpImpresora.enviaCadena(strOut);

				strOut =
					"        Cedula           : "
						+ strCedula;
				tpImpresora.enviaCadena(strOut);	*/


				if (iDriver == 0) {
					tpImpresora.InserteDocumento();
					tpImpresora.activateControl("SELECCIONASLIP",iDriver);
					tpImpresora.enviaCadena("");
					tpImpresora.enviaCadena("");
					tpImpresora.enviaCadena("");
					tpImpresora.enviaCadena("");
					tpImpresora.enviaCadena("");
					tpImpresora.imprimeCadena1();
					tpImpresora.activateControl("SALTOHOJA",iDriver);
					tpImpresora.activateControl("SELECCIONAJOURNAL",iDriver);
				} else {
					tpImpresora.activateControl("SALTOHOJA",iDriver);
				}
			}
			return intStatus;

		}


	public int imprimeChequeCaja(
		Vector vDatos,
		String strDatosSuc,
		String strDate,
		int iDriver) {
		intStatus = tpImpresora.verificaStatus();
		if ((intStatus == 95) || (intStatus == 88)) {
			tpImpresora.activateControl("CERTIFICACION",iDriver);
			vDatosSuc = tpImpresora.Campos(strDatosSuc, "@");
			//System.out.println("vDatosSuc: " + vDatosSuc);
			intStatus = 0;
			//String strPlaza = (String) vDatosSuc.elementAt(5);
			//strPlaza = strPlaza.substring(7, strPlaza.length());
			//strPlaza = strPlaza.trim();
			String strBeneficiario = (String) vDatos.elementAt(1);
			String strMoneda = (String) vDatos.elementAt(2);
			String strMonto = (String) vDatos.elementAt(3);
			String strNoCheque = (String) vDatos.elementAt(4);
			String strSucursal = (String) vDatosSuc.elementAt(1);
			strSucursal = strSucursal.substring(7, strSucursal.length());
			strSucursal = strSucursal.trim();
			String strNomSuc 	= (String) vDatosSuc.elementAt(0);
			strNomSuc 			= strNomSuc.substring(7, strNomSuc.length());
			strNomSuc 			= strNomSuc.trim();
			String strCuenta = (String) vDatos.elementAt(6);
			String strPlaza = (String) vDatos.elementAt(7);
			tpImpresora.enviaCadena("\n\n");
			if (iDriver == 0) {
				strOut =
						strFiller.substring(0, 141)
						+ strDate.substring(7, 8)
							+ "  "
							+ strDate.substring(8, 9)
							+ "  "
							+ strDate.substring(5, 6)
							+ "  "
							+ strDate.substring(6, 7)
							+ "  "
							+ strDate.substring(1, 2)
							+ "  "
							+ strDate.substring(2, 3)
							+ "  "
							+ strDate.substring(3, 4)
							+ "  "
							+ strDate.substring(4, 5);
				tpImpresora.enviaCadena(strOut);

				if (strBeneficiario.length() <= 56) {
					strOut = "\n\n\n";
				} else if (strBeneficiario.length() <= 112) {
					strOut = "\n\n";
				} else {
					strOut = "\n";
				}

				int posStrBenef = 0;
				// Longitud del Beneficiario menor a 56
				while ((strBeneficiario.length()-posStrBenef) > 56) {
					int nroSubstFin = ((strBeneficiario.length()-posStrBenef) > 112)? 80 : 56;
					strOut += strBeneficiario.substring(posStrBenef, posStrBenef+=nroSubstFin);
					tpImpresora.enviaCadena(strOut);
					strOut = "";
				}
				if ((strBeneficiario.length()-posStrBenef) <= 56) {
					String strTmpBenf = strBeneficiario.substring(posStrBenef, strBeneficiario.length());
					strOut += strTmpBenf
							+ strFiller1.substring(0, 56 - strTmpBenf.length())
							+ "     "+ "USD*"+strMonto+"***";
					tpImpresora.enviaCadena(strOut);
				}

				String strMontoDesc = Currency.convert(strMonto, Integer.parseInt(strMoneda));
				if (strMontoDesc.length() > 72) {
					strOut = strMontoDesc.substring(0, 54);
					tpImpresora.enviaCadena(strOut);

					strOut = strMontoDesc.substring(54, strMontoDesc.length()) + "\n";
					tpImpresora.enviaCadena(strOut);
				}
				else {
					strOut = "\n" + strMontoDesc + "\n";
					tpImpresora.enviaCadena(strOut);
				}
			}
			else
			{
				strOut =
						strFiller.substring(0, 93)
						+ "\n";
				 tpImpresora.enviaCadena(strOut);

				 strOut =
						   strFiller.substring(0, 78)
						 + strFiller.substring(0, 15)
						 + strDate.substring(7, 9)
						 + "/"
						 + strDate.substring(5, 7)
						 + "/"
						 + strDate.substring(1, 5)
						 + "\n";
				 tpImpresora.enviaCadena(strOut);
				 strOut =
						   strFiller.substring(0, 20)
						 + strBeneficiario
						 + strFiller.substring(0, 40 - strBeneficiario.length())
						 + strFiller1.substring(0, 33)
						 + "  "+ "USD*" + strMonto + "***"
						 + "\n";
				 tpImpresora.enviaCadena(strOut);
				 strOut =
					 "                 "
						 + Currency.convert(strMonto, Integer.parseInt(strMoneda))
						 + "\n\n\n\n\n\n";
				 tpImpresora.enviaCadena(strOut);
			}

			strOut= strFiller.substring(0, 25) + strNoCheque;
			tpImpresora.enviaCadena(strOut);

			strOut= strFiller.substring(0, 25) + "SUC. "+ strSucursal +" "+ strNomSuc;
			tpImpresora.enviaCadena(strOut);
			tpImpresora.enviaCadena("\n\n\n\n\n\n\n");

			if (strCuenta.equals(""))
				strCuenta = "Efectivo Recibido";

			strOut =
					"Debito a Cuenta  : "
					 + strCuenta + strFiller.substring(0, 54 - strCuenta.length());
			tpImpresora.enviaCadena(strOut);

			strOut = "Credito a Cuenta : Cheque de Gerencia";
			tpImpresora.enviaCadena(strOut);

			strOut = "Beneficiario     : ";

			int posStrBenef = 0;
			while ((strBeneficiario.length()-posStrBenef) > 62) {
				strOut += strBeneficiario.substring(posStrBenef, posStrBenef+=62);
				tpImpresora.enviaCadena(strOut);
				strOut = "                   ";
			}
			if ((strBeneficiario.length()-posStrBenef) <= 62) {
				strOut += strBeneficiario.substring(posStrBenef, strBeneficiario.length());
				tpImpresora.enviaCadena(strOut);
			}

			strOut = "Importe          : USD$ " + strMonto + " USD.";
			tpImpresora.enviaCadena(strOut);

			strOut =
					 "No. Documento    : "
					 + strNoCheque;
			tpImpresora.enviaCadena(strOut);

			strOut =
				"                   "
					//+ strNomSuc
					//+ " "
					+ strDate.substring(7, 9)
					+ "/"
					+ strDate.substring(5, 7)
					+ "/"
					+ strDate.substring(1, 5);
			String strLinea = "          ____________________________________";
			strOut = agrega_espacios(strOut, 20);
			strOut = strOut + strLinea;
			tpImpresora.enviaCadena(strOut);

			strOut =
				"               "
					+ strSucursal
					+ " "
					+ strNomSuc
					+ strFiller.substring(0, 12 - (strSucursal.length()) + strNomSuc.length())
					+ "Firma del Cliente         ";
			tpImpresora.enviaCadena(strOut);
			if (iDriver == 0) {
				tpImpresora.InserteDocumento();
				tpImpresora.activateControl("SELECCIONASLIP",iDriver);
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.imprimeCadena1();
				tpImpresora.activateControl("SALTOHOJA",iDriver);
				tpImpresora.activateControl("SELECCIONAJOURNAL",iDriver);
			} else {
				tpImpresora.activateControl("SALTOHOJA",iDriver);
			}
		}
		return intStatus;
	}


	public int imprimeChequeNafinsa(
		String strDatosSuc,
		String strRespuesta,
		String strDate,
		int iDriver) {
		intStatus = tpImpresora.verificaStatus();
		if ((intStatus == 95) || (intStatus == 88)) {

			intStatus = 0;
			tpImpresora.activateControl("CERTIFICACION",iDriver);
			Vector vCampos = tpImpresora.Campos(strRespuesta, "^");
			Vector vCamposT = new Vector();
			int intSize = vCampos.size();
			String strTmp = new String("");
			for (int i = 0; i < intSize; i++) {
				strTmp = (String) vCampos.elementAt(i);
				if (strTmp.startsWith("COMPROBANTE_NAFINSA")) {
					strTmp = (String) vCampos.elementAt(i);
					vCamposT = tpImpresora.Campos(strTmp, "@");
					strTmp = (String) vCamposT.elementAt(1);
					vCamposT = tpImpresora.Campos(strTmp, "|");
					strTmp = (String) vCamposT.elementAt(0);
					vCamposT = tpImpresora.Campos(strTmp, "~");
				}
			}
			String strBillete = (String) vCamposT.elementAt(1);
			String strDepositante = (String) vCamposT.elementAt(2);
			String strClave = (String) vCamposT.elementAt(3);
			String strPlaza = (String) vCamposT.elementAt(4);
			String strMonto = (String) vCamposT.elementAt(5);
			String strConcepto = (String) vCamposT.elementAt(6);
			String strAutoridad = (String) vCamposT.elementAt(7);
			String strNumSuc = (String) vCamposT.elementAt(8);
			String strNomSuc = (String) vCamposT.elementAt(9);
			String strMoneda = (String) vCamposT.elementAt(10);
			strMoneda = strMoneda.trim();
			tpImpresora.enviaCadena("\n\n\n\n\n\n");
			strOut = strFiller.substring(0, 89) + strBillete.trim();
			tpImpresora.enviaCadena(strOut);
			strOut = strFiller.substring(0, 94) + strClave;
			tpImpresora.enviaCadena("\n");
			tpImpresora.enviaCadena(strOut);
			strOut =
				"       "
					+ strDepositante
					+ strFiller.substring(0, 87 - strDepositante.length())
					+ strPlaza;
			tpImpresora.enviaCadena(strOut);
			strOut =
				strFiller.substring(0, 94)
					+ strDate.substring(7, 9)
					+ "-"
					+ strDate.substring(5, 7)
					+ "-"
					+ strDate.substring(1, 5);
			tpImpresora.enviaCadena(strOut);
			tpImpresora.enviaCadena("\n\n");
			if (strMoneda.equals("01"))
				strOut =
					"       "
						+ Currency.convert(tpImpresora.formatMonto(strMonto), 1);
			else
				strOut =
					"       "
						+ Currency.convert(tpImpresora.formatMonto(strMonto), 2);
			strOut =
				strOut
					+ strFiller.substring(0, 100 - strOut.length())
					+ "$"
					+ tpImpresora.formatMonto(strMonto.trim());
			tpImpresora.enviaCadena(strOut);
			tpImpresora.enviaCadena("\n\n\n\n");
			if (strConcepto.length() > 68) {
				strOut = "       " + strConcepto.substring(0, 68);
				tpImpresora.enviaCadena(strOut);
				strOut =
					"       " + strConcepto.substring(68, strConcepto.length());
				tpImpresora.enviaCadena(strOut);
			} else {
				strOut = "       " + strConcepto;
				tpImpresora.enviaCadena(strOut);
			}
			tpImpresora.enviaCadena("\n\n\n");
			if (strAutoridad.length() > 68) {
				strOut = "       " + strAutoridad.substring(0, 68);
				tpImpresora.enviaCadena(strOut);
				strOut =
					"       "
						+ strAutoridad.substring(68, strAutoridad.length());
				tpImpresora.enviaCadena(strOut);
			} else {
				strOut = "       " + strAutoridad;
				tpImpresora.enviaCadena(strOut);
			}
			tpImpresora.enviaCadena("\n\n");
			vDatosSuc = tpImpresora.Campos(strDatosSuc, "@");
			strNumSuc = (String) vDatosSuc.elementAt(1);
			strNumSuc = strNumSuc.substring(7, strNumSuc.length());
			strNomSuc = (String) vDatosSuc.elementAt(0);
			strNomSuc = strNomSuc.substring(7, strNomSuc.length());
			strOut = strFiller.substring(0, 90) + strNumSuc;
			tpImpresora.enviaCadena(strOut);
			strOut = strFiller.substring(0, 90) + strNomSuc;
			tpImpresora.enviaCadena(strOut);
			if (iDriver == 0) {
				tpImpresora.InserteDocumento();
				tpImpresora.activateControl("SELECCIONASLIP",iDriver);
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.imprimeCadena1();
				tpImpresora.activateControl("SALTOHOJA",iDriver);
				tpImpresora.activateControl("SELECCIONAJOURNAL",iDriver);
			} else {
				tpImpresora.activateControl("SALTOHOJA",iDriver);
			}
		}
		return intStatus;
	}

	public int imprimeChequeMasivo1(
		Vector vDatos,
		String strDatosSuc,
		String strDate,
		int iDriver) {
		intStatus = tpImpresora.verificaStatus();
		if ((intStatus == 95) || (intStatus == 88)) {
			tpImpresora.activateControl("CERTIFICACION",iDriver);
			vDatosSuc = tpImpresora.Campos(strDatosSuc, "@");
			intStatus = 0;
			String strPlaza = (String) vDatosSuc.elementAt(0);
			strPlaza = strPlaza.substring(7, strPlaza.length());
			if (strPlaza.length() > 30)
				strPlaza = strPlaza.substring(0, 30);
			int intSize = vDatos.size() - 1;
			intSize = intSize / 5;
			for (int i = 0; i < intSize; i++) {
				String strNoCheque = (String) vDatos.elementAt(1 + (i * 5));
				String strBeneficiario = (String) vDatos.elementAt(2 + (i * 5));
				String strMoneda = (String) vDatos.elementAt(3 + (i * 5));
				String strMonto = (String) vDatos.elementAt(4 + (i * 5));
				int intMonto = Integer.parseInt(strMonto);
				strMonto = "" + intMonto;
				strMonto = tpImpresora.formatMonto(strMonto);
				String strCuenta = (String) vDatos.elementAt(5 + (i * 5));
				String strDivisa = " $  ";
				if (!strMoneda.equals("01"))
					strDivisa = "USD ";
				tpImpresora.enviaCadena("");
				strOut = strFiller.substring(0, 61) + strNoCheque + "\n\n";
				tpImpresora.enviaCadena(strOut);
				if (iDriver == 0) {
					strOut =
						"                         "
							+ strFiller.substring(0, 30 - strPlaza.length())
							+ strPlaza
							+ strFiller.substring(0, 91)
							+ strDate.substring(7, 9)
							+ "/"
							+ strDate.substring(5, 7)
							+ "/"
							+ strDate.substring(1, 5)
							+ "\n";
					tpImpresora.enviaCadena(strOut);
					strOut =
						"          "
							+ strBeneficiario
							+ strFiller.substring(0, 40 - strBeneficiario.length())
							+ strFiller.substring(0, 91)
							+ strDivisa
							+ strMonto
							+ "\n";
					tpImpresora.enviaCadena(strOut);
				}
				else
				{
					strOut =
						"                         "
							+ strFiller.substring(0, 30 - strPlaza.length())
							+ strPlaza
							+ strFiller.substring(0, 51)
							+ strDate.substring(7, 9)
							+ "/"
							+ strDate.substring(5, 7)
							+ "/"
							+ strDate.substring(1, 5)
							+ "\n";
					tpImpresora.enviaCadena(strOut);
					strOut =
						"               "
							+ strBeneficiario
							+ strFiller.substring(0, 40 - strBeneficiario.length())
							+ strFiller.substring(0, 51)
							+ strDivisa
							+ strMonto
							+ "\n";
					tpImpresora.enviaCadena(strOut);
				}
				strOut =
					"               "
						+ Currency.convert(strMonto, Integer.parseInt(strMoneda))
						+ "\n\n";
				tpImpresora.enviaCadena(strOut);
				tpImpresora.enviaCadena("\n\n\n\n\n\n\n\n");
				strOut = " CUENTA POR LIQUIDAR : " + strCuenta + "\n";
				tpImpresora.enviaCadena(strOut);
				strOut = " No. DE CHEQUE       : " + strNoCheque + "\n";
				tpImpresora.enviaCadena(strOut);
				strOut =
					" FECHA               : "
						+ strDate.substring(7, 9)
						+ "/"
						+ strDate.substring(5, 7)
						+ "/"
						+ strDate.substring(1, 5)
						+ "\n";
				tpImpresora.enviaCadena(strOut);
				strOut =
					" BENEFICIARIO        : "
						+ strBeneficiario
						+ strFiller.substring(0, 50 - strBeneficiario.length())
						+ "     _____________________"
						+ "\n";
				tpImpresora.enviaCadena(strOut);
				strOut =
					" IMPORTE             : "
						+ strDivisa
						+ strMonto
						+ strFiller.substring(0, 50 - strMonto.length())
						+ "   FIRMA BENEFICIARIO"
						+ "\n";
				tpImpresora.enviaCadena(strOut);
				if (iDriver == 0) {
					tpImpresora.InserteDocumento();
					tpImpresora.activateControl("SELECCIONASLIP",iDriver);
					tpImpresora.enviaCadena("");
					tpImpresora.enviaCadena("");
					tpImpresora.enviaCadena("");
					tpImpresora.enviaCadena("");
					tpImpresora.enviaCadena("");
					tpImpresora.imprimeCadena1();
					tpImpresora.activateControl("SALTOHOJA",iDriver);
					tpImpresora.activateControl("SELECCIONAJOURNAL",iDriver);
				} else {
					tpImpresora.activateControl("SALTOHOJA",iDriver);
				}
				if (i < intSize - 1)
					if (iDriver == 1)
						tpImpresora.muestraVentana();
			}
		}
		return intStatus;
	}

	public int imprimeChequeMasivo2(
		Vector vDatos,
		String strDatosSuc,
		String strDate,
		int iDriver) {
		intStatus = tpImpresora.verificaStatus();
		if ((intStatus == 95) || (intStatus == 88)) {
			tpImpresora.activateControl("CERTIFICACION",iDriver);
			vDatosSuc = tpImpresora.Campos(strDatosSuc, "@");
			intStatus = 0;
			String strPlaza = (String) vDatosSuc.elementAt(0);
			strPlaza = strPlaza.substring(7, strPlaza.length());
			if (strPlaza.length() > 30)
				strPlaza = strPlaza.substring(0, 30);
			int intSize = vDatos.size() - 1;
			intSize = intSize / 5;
			for (int i = 0; i < intSize; i++) {
				String strNoCheque = (String) vDatos.elementAt(1 + (i * 5));
				String strBeneficiario = (String) vDatos.elementAt(2 + (i * 5));
				String strMoneda = (String) vDatos.elementAt(3 + (i * 5));
				String strMonto = (String) vDatos.elementAt(4 + (i * 5));
				int intMonto = Integer.parseInt(strMonto);
				strMonto = "" + intMonto;
				strMonto = tpImpresora.formatMonto(strMonto);
				String strCuenta = (String) vDatos.elementAt(5 + (i * 5));
				String strDivisa = " $  ";
				if (!strMoneda.equals("01"))
					strDivisa = "USD ";
				tpImpresora.enviaCadena("");
				strOut = strFiller.substring(0, 61) + strNoCheque + "\n\n";
				tpImpresora.enviaCadena(strOut);
				if (iDriver == 0) {
					strOut =
						"                         "
							+ strFiller.substring(0, 30 - strPlaza.length())
							+ strPlaza
							+ strFiller.substring(0, 91)
							+ strDate.substring(7, 9)
							+ "/"
							+ strDate.substring(5, 7)
							+ "/"
							+ strDate.substring(1, 5)
							+ "\n";
					tpImpresora.enviaCadena(strOut);
					strOut =
						"          "
							+ strBeneficiario
							+ strFiller.substring(0, 40 - strBeneficiario.length())
							+ strFiller.substring(0, 91)
							+ strDivisa
							+ strMonto
							+ "\n";
					tpImpresora.enviaCadena(strOut);
				}
				else
				{
					strOut =
						"                         "
							+ strFiller.substring(0, 30 - strPlaza.length())
							+ strPlaza
							+ strFiller.substring(0, 51)
							+ strDate.substring(7, 9)
							+ "/"
							+ strDate.substring(5, 7)
							+ "/"
							+ strDate.substring(1, 5)
							+ "\n";
					tpImpresora.enviaCadena(strOut);
					strOut =
						"               "
							+ strBeneficiario
							+ strFiller.substring(0, 40 - strBeneficiario.length())
							+ strFiller.substring(0, 51)
							+ strDivisa
							+ strMonto
							+ "\n";
					tpImpresora.enviaCadena(strOut);

				}
				strOut =
					"               "
						+ Currency.convert(strMonto, Integer.parseInt(strMoneda))
						+ "\n\n";
				tpImpresora.enviaCadena(strOut);
				tpImpresora.enviaCadena("\n\n\n\n\n\n\n\n\n\n");
				strOut = " CONCEPTO : " + strCuenta;
				tpImpresora.enviaCadena(strOut);
				strOut =
					" BENEFICIARIO        : "
						+ strBeneficiario
						+ strFiller.substring(0, 50 - strBeneficiario.length())
						+ "     _____________________";
				tpImpresora.enviaCadena(strOut);
				strOut =
					" IMPORTE             : "
						+ strDivisa
						+ strMonto
						+ strFiller.substring(0, 50 - strMonto.length())
						+ "  PERSONAS AUTORIZADAS";
				tpImpresora.enviaCadena(strOut);
				strOut = " No. DE CHEQUE       : " + strNoCheque;
				tpImpresora.enviaCadena(strOut);
				strOut =
					" FECHA               : "
						+ strDate.substring(7, 9)
						+ "/"
						+ strDate.substring(5, 7)
						+ "/"
						+ strDate.substring(1, 5)
						+ "\n";
				tpImpresora.enviaCadena(strOut);
				if (iDriver == 0) {
					tpImpresora.InserteDocumento();
					tpImpresora.activateControl("SELECCIONASLIP",iDriver);
					tpImpresora.enviaCadena("");
					tpImpresora.enviaCadena("");
					tpImpresora.enviaCadena("");
					tpImpresora.enviaCadena("");
					tpImpresora.enviaCadena("");
					tpImpresora.imprimeCadena1();
					tpImpresora.activateControl("SALTOHOJA",iDriver);
					tpImpresora.activateControl("SELECCIONAJOURNAL",iDriver);
				} else {
					tpImpresora.activateControl("SALTOHOJA",iDriver);
				}
				if (i < intSize - 1)
					if (iDriver == 1)
						tpImpresora.muestraVentana();
			}
		}
		return intStatus;
	}


	public int imprimeRelacion(
		Vector vDatos,
		String strCajero,
		String strDate,
		int iDriver,
		String strIdentidad) {
		intStatus = tpImpresora.verificaStatus();
		if ((intStatus == 95) || (intStatus == 88)) {
			tpImpresora.activateControl("CERTIFICACION",iDriver);
			intStatus = 0;
			int intSize = vDatos.size() - 2;
			intSize = intSize / 6;
			tpImpresora.enviaCadena(strOut);
			int intPag = 1;
			String strTotal = (String) vDatos.elementAt(1);
			int intTotal = Integer.parseInt(strTotal);
			strTotal = "" + intTotal;
			strTotal = tpImpresora.formatMonto(strTotal);
			for (int i = 0; i < intSize; i++) {
				String strNoRenglon = (String) vDatos.elementAt(2 + (i * 6));
				String strNoControl = (String) vDatos.elementAt(3 + (i * 6));
				String strCuenta = (String) vDatos.elementAt(4 + (i * 6));
				String strNoCheque = (String) vDatos.elementAt(5 + (i * 6));
				String strMonto = (String) vDatos.elementAt(6 + (i * 6));
				int intMonto = Integer.parseInt(strMonto);
				strMonto = "" + intMonto;
				strMonto = tpImpresora.formatMonto(strMonto);
				String strBeneficiario = (String) vDatos.elementAt(7 + (i * 6));
				if ((i % 50) == 0) {
					if (intPag > 1) {
						if (iDriver == 0) {
							tpImpresora.InserteDocumento();
							tpImpresora.activateControl("SELECCIONASLIP",iDriver);
							tpImpresora.enviaCadena("");
							tpImpresora.enviaCadena("");
							tpImpresora.enviaCadena("");
							tpImpresora.enviaCadena("");
							tpImpresora.enviaCadena("");
							tpImpresora.imprimeCadena1();
							tpImpresora.activateControl("SALTOHOJA",iDriver);
							tpImpresora.activateControl("SELECCIONAJOURNAL",iDriver);
						} else {
							tpImpresora.activateControl("SALTOHOJA",iDriver);
						}
						if (i < intSize - 1)
							if (iDriver == 1)
								tpImpresora.muestraVentana();
					}
					strOut =
						strFiller.substring(0, 49)
							+ "GRUPO FINANCIERO "+strIdentidad+"\n\n";
					tpImpresora.enviaCadena(strOut);
					strOut =
						strFiller.substring(0, 26)
							+ "SUCURSAL "
							+ strCajero.substring(0, 4)
							+ strFiller.substring(0, 42)
							+ "CAJERO "
							+ strCajero
							+ "\n\n";
					tpImpresora.enviaCadena(strOut);
					strOut =
						"   FECHA : "
							+ strDate.substring(7, 9)
							+ "/"
							+ strDate.substring(5, 7)
							+ "/"
							+ strDate.substring(1, 5)
							+ strFiller.substring(0, 80)
							+ "PAGINA "
							+ intPag++
							+ "\n";
					tpImpresora.enviaCadena(strOut);
					tpImpresora.enviaCadena(
						"________________________________________________________________________________________________________________________");
					tpImpresora.enviaCadena(
						"  No.     No. CONTROL       CUENTA/LIQ.       No. DE CHEQUE         IMPORTE($)               BENEFICIARIO");
					tpImpresora.enviaCadena(
						"________________________________________________________________________________________________________________________");
				}
				strOut =
					"  "
						+ strNoRenglon
						+ strFiller.substring(0, 4 - strNoRenglon.length())
						+ "    "
						+ strNoControl
						+ "        "
						+ strCuenta
						+ "          "
						+ strNoCheque
						+ "    "
						+ strFiller.substring(0, 19 - strMonto.length())
						+ strMonto
						+ "        "
						+ strBeneficiario;
				tpImpresora.enviaCadena(strOut);
			}
			tpImpresora.enviaCadena(
				"________________________________________________________________________________________________________________________\n");
			strOut =
				strFiller.substring(0, 45)
					+ " IMPORTE TOTAL : "
					+ strFiller.substring(0, 19 - strTotal.length())
					+ strTotal;
			tpImpresora.enviaCadena(strOut);
			if (iDriver == 0) {
				tpImpresora.InserteDocumento();
				tpImpresora.activateControl("SELECCIONASLIP",iDriver);
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.imprimeCadena1();
				tpImpresora.activateControl("SALTOHOJA",iDriver);
				tpImpresora.activateControl("SELECCIONAJOURNAL",iDriver);
			} else {
				tpImpresora.activateControl("SALTOHOJA",iDriver);
				tpImpresora.muestraVentana();
			}
		}
		return intStatus;
	}
	public int imprimeRelacionEnma(
		Vector vDatos,
		String strCajero,
		String strDate,
		int iDriver,
		String strIdentidad) {
		intStatus = tpImpresora.verificaStatus();
		if ((intStatus == 95) || (intStatus == 88)) {
			tpImpresora.activateControl("CERTIFICACION",iDriver);
			intStatus = 0;
			int intSize = vDatos.size() - 2;
			intSize = intSize / 5;
			tpImpresora.enviaCadena(strOut);
			int intPag = 1;
			String strTotal = (String) vDatos.elementAt(1);
			int intTotal = Integer.parseInt(strTotal);
			strTotal = "" + intTotal;
			strTotal = tpImpresora.formatMonto(strTotal);
			for (int i = 0; i < intSize; i++) {
				String strNoRenglon = (String) vDatos.elementAt(2 + (i * 5));
				String strNoCheque = (String) vDatos.elementAt(3 + (i * 5));
				String strMonto = (String) vDatos.elementAt(4 + (i * 5));
				int intMonto = Integer.parseInt(strMonto);
				strMonto = "" + intMonto;
				strMonto = tpImpresora.formatMonto(strMonto);
				String strBeneficiario = (String) vDatos.elementAt(5 + (i * 5));
				String strEstatus = (String) vDatos.elementAt(6 + (i * 5));
				if ((i % 50) == 0) {
					if (intPag > 1) {
						if (iDriver == 0) {
							tpImpresora.InserteDocumento();
							tpImpresora.activateControl("SELECCIONASLIP",iDriver);
							tpImpresora.enviaCadena("");
							tpImpresora.enviaCadena("");
							tpImpresora.enviaCadena("");
							tpImpresora.enviaCadena("");
							tpImpresora.enviaCadena("");
							tpImpresora.imprimeCadena1();
							tpImpresora.activateControl("SALTOHOJA",iDriver);
							tpImpresora.activateControl("SELECCIONAJOURNAL",iDriver);
						} else {
							tpImpresora.activateControl("SALTOHOJA",iDriver);
						}
						if (i < intSize - 1)
							if (iDriver == 1)
								tpImpresora.muestraVentana();
					}
					strOut =
						strFiller.substring(0, 49)
							+ "GRUPO FINANCIERO "+strIdentidad+"\n\n";
					tpImpresora.enviaCadena(strOut);
					strOut =
						strFiller.substring(0, 26)
							+ "SUCURSAL "
							+ strCajero.substring(0, 4)
							+ strFiller.substring(0, 42)
							+ "CAJERO "
							+ strCajero
							+ "\n\n";
					tpImpresora.enviaCadena(strOut);
					strOut =
						"   FECHA : "
							+ strDate.substring(7, 9)
							+ "/"
							+ strDate.substring(5, 7)
							+ "/"
							+ strDate.substring(1, 5)
							+ strFiller.substring(0, 80)
							+ "PAGINA "
							+ intPag++
							+ "\n";
					tpImpresora.enviaCadena(strOut);
					tpImpresora.enviaCadena(
						"________________________________________________________________________________________________________________________");
					tpImpresora.enviaCadena(
						"  No.          No. DE CHEQUE       IMPORTE($)                                 BENEFICIARIO                       ESTATUS");
					tpImpresora.enviaCadena(
						"________________________________________________________________________________________________________________________");
				}
				strOut =
					"  "
						+ strNoRenglon
						+ strFiller.substring(0, 4 - strNoRenglon.length())
						+ "          "
						+ strNoCheque
						+ "    "
						+ strFiller.substring(0, 19 - strMonto.length())
						+ strMonto
						+ "        "
						+ strBeneficiario
						+ strFiller.substring(0, 50 - strBeneficiario.length())
						+ "           "
						+ strEstatus;
				tpImpresora.enviaCadena(strOut);
			}
			tpImpresora.enviaCadena(
				"________________________________________________________________________________________________________________________\n");
			strOut =
				strFiller.substring(0, 45)
					+ " IMPORTE TOTAL : "
					+ strFiller.substring(0, 19 - strTotal.length())
					+ strTotal;
			tpImpresora.enviaCadena(strOut);
			if (iDriver == 0) {
				tpImpresora.InserteDocumento();
				tpImpresora.activateControl("SELECCIONASLIP",iDriver);
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.imprimeCadena1();
				tpImpresora.activateControl("SALTOHOJA",iDriver);
				tpImpresora.activateControl("SELECCIONAJOURNAL",iDriver);
			} else {
				tpImpresora.activateControl("SALTOHOJA",iDriver);
				tpImpresora.muestraVentana();
			}
		}
		return intStatus;
	}

	public int imprimeChequeCertif(
		String strMoneda,
		String importe,
		String strSucursal,
		String folio,
		String strDate,
		int iDriver,
		String strIdentidad) {
		intStatus = tpImpresora.verificaStatus();
		if ((intStatus == 95) || (intStatus == 88)) {
			tpImpresora.activateControl("CERTIFICACION",iDriver);
			intStatus = 0;
			String strOut = "";
			String strJustificacion = "";
			if (iDriver != 1)
				strJustificacion = "                              ";
			strOut =
				strJustificacion
					+ "              "+strIdentidad+"    P A N A M A  ";
			tpImpresora.enviaCadena(strOut);
			strOut =
				strJustificacion
					+ "           Certifica que este documento";
			tpImpresora.enviaCadena(strOut);
			strOut =
				strJustificacion
					+ "          es bueno por la suma de: ";
			tpImpresora.enviaCadena(strOut);
			if (strMoneda.equals("1"))
				strOut =
					strJustificacion + "               $" + importe;
			else
				strOut =
					strJustificacion + "              USD" + importe;
			tpImpresora.enviaCadena(strOut);
			strOut = strJustificacion + "" + strSucursal;
			tpImpresora.enviaCadena(strOut);
			strOut =
				strJustificacion
					+ "     "
					+ strDate.substring(7, 9)
					+ "/"
					+ strDate.substring(5, 7)
					+ "/"
					+ strDate.substring(1, 5)
					+ "      "
					+ folio;
			tpImpresora.enviaCadena(strOut);
			tpImpresora.enviaCadena("\n\n");
			strOut =
				strJustificacion
					+ "                __________________ ";
			tpImpresora.enviaCadena(strOut);
			strOut =
				strJustificacion
					+ "                FIRMAS AUTORIZADAS ";
			tpImpresora.enviaCadena(strOut);
			/*strOut =
				strJustificacion
					+ "                       NO DESTRUYA ESTE CHEQUE ";
			tpImpresora.enviaCadena(strOut);
			strOut =
				strJustificacion + "                            NO NEGOCIABLE ";
			tpImpresora.enviaCadena(strOut);*/
			if (iDriver == 0) {
				tpImpresora.InserteDocumento();
				tpImpresora.activateControl("SELECCIONASLIP",iDriver);
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.imprimeCadena1();
				tpImpresora.activateControl("SALTOHOJA",iDriver);
				tpImpresora.activateControl("SELECCIONAJOURNAL",iDriver);
			} else {
				tpImpresora.activateControl("SALTOHOJA",iDriver);
			}
		}
		return intStatus;
	}
}