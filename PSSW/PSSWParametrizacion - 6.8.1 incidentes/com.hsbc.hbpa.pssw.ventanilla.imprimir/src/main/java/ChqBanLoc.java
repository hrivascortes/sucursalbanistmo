//*************************************************************************************************
//	Funcion:	Clase que certifica el Dep�sito de cheques de bancos locales y extranjeros
//	Elemento:	ChqBanLoc.java
//	Creado por: Guillermo Rodrigo Escand�n Soto
//*************************************************************************************************
// CCN -  -  - 
//*************************************************************************************************


import java.util.*;

public class ChqBanLoc {
	ToolsPrinter tpImpresora = new ToolsPrinter();
	int intStatus = 0;
	String strFiller =
		"                                                                                ";
	String strFiller2 =
		"--------------------------------------------------------------------";
	String strOutLine = "";
	Vector vDatosSuc = new Vector();
	int intMoneda = 0;

	public int imprimeChqBanLoc(
	Vector vDatos,
	String strRespuesta,
	String strDatosSuc,
	String strCajero,
	String strDate,
	String strTime,
	int iDriver){
		intStatus = tpImpresora.verificaStatus();
		String strTipoCambio = new String("");
		String strMonedaUSD = new String("");
		int intExiste = 0;
		Vector vDatosTxn = new Vector();
		if ((intStatus == 95) || (intStatus == 88)) {	
			tpImpresora.activateControl("CERTIFICACION",iDriver);
			String strFormato = (String) vDatos.elementAt(0);
			String strMoneda = (String) vDatos.elementAt(1);
			String strCuenta = (String) vDatos.elementAt(2);
			String strMonto = (String) vDatos.elementAt(3);
			strMonto = tpImpresora.formatMonto(strMonto);
			String strOperacion = (String) vDatos.elementAt(4);
			String strDivisa = "";
			String strConsec = (String) vDatos.elementAt(5);
			strTime = strTime.substring(0, 2) + ":" + strTime.substring(2, 4) + ":" + strTime.substring(4, 6);
			if (strMoneda.equals("01")){
				strDivisa = "USD$";
			}
			
			vDatosTxn = tpImpresora.Campos(strRespuesta, "~");		
			vDatosSuc = tpImpresora.Campos(strDatosSuc, "@");
			String strNomSuc = (String) vDatosSuc.elementAt(0);
			strNomSuc = strNomSuc.substring(7, strNomSuc.length());
			String strNumSuc = (String) vDatosSuc.elementAt(1);
			strNumSuc = strNumSuc.substring(7, strNumSuc.length());
			tpImpresora.enviaCadena("");
			
			//Imprime Sucursal
			strOutLine = "SUCURSAL : " + strNumSuc + "-" + strNomSuc;
			tpImpresora.enviaCadena(strOutLine);
			tpImpresora.enviaCadena(" ");
			
			//Imprime fecha y cajero
			strOutLine =
						strDate.substring(5, 7)
						+ "-"
						+ strDate.substring(7, 9)
						+ "-"
						+ strDate.substring(1, 5)
						+ "    "
						+ strCajero
						+ "\n";
			tpImpresora.enviaCadena(strOutLine);
			
			//Imprime T�tulo
			if (strFormato.equals("DEBITO")) {
				strOutLine =
						"CARGO POR CORRECCION ABONO B.R.\n";
				strOutLine =
					strFiller.substring(0, (68 - strOutLine.length()) / 2)
						+ strOutLine
						+ "\n";
				tpImpresora.enviaCadena(strOutLine);
			} else if (strFormato.equals("CREDITO")) {
				if (strOperacion.substring(1, 3).equals("01")){
					strOutLine =
						"ABONO POR CORRECCI�N DE CARGO\n";
					strOutLine =
						strFiller.substring(0, (68 - strOutLine.length()) / 2)
							+ strOutLine
							+ "\n";
					tpImpresora.enviaCadena(strOutLine);
				}
				else {
					strOutLine =
						"ABONO POR CORRECCI�N DE CARGO B.R.\n";
					strOutLine =
						strFiller.substring(0, (68 - strOutLine.length()) / 2)
							+ strOutLine
							+ "\n";
					tpImpresora.enviaCadena(strOutLine);
					}
			}
				
				//Imprime flujo de transacciones
				  strOutLine = "   TXN    CONSEC";
				  tpImpresora.enviaCadena(strOutLine);
				  if (strFormato.equals("DEBITO")){
				  	strOutLine = "  5217    " + strConsec;
					tpImpresora.enviaCadena(strOutLine);
				  }
				  else{
					strOutLine = "  5219    " + strConsec;
					tpImpresora.enviaCadena(strOutLine);
				  }
				 tpImpresora.enviaCadena("");
				
				strOutLine = "MONEDA";
				strOutLine =
					strFiller.substring(0, 58)
						+ strOutLine;
				tpImpresora.enviaCadena(strOutLine);
				
				String strOutLnAux = "CUENTA: " + strCuenta;
				strOutLine = 
					"TITULAR: " 
					+ strFiller.substring(0, (59 - strOutLnAux.length())) 
					+ strOutLnAux;
				tpImpresora.enviaCadena(strOutLine);
				tpImpresora.enviaCadena("");
				
				tpImpresora.enviaCadena(strFiller2);
				
				strOutLine = "   HORA         B.R                    SERIAL          MONTO\n";
				tpImpresora.enviaCadena(strOutLine);
				strOutLine = 
							 " "
							 + strTime
							 + "                                     "
							 + strDivisa 
							 + strFiller.substring(50, (68 - strMonto.length())) 
							 + strMonto;
				tpImpresora.enviaCadena(strOutLine);
				strOutLine = "                                     ********    *******************";
				tpImpresora.enviaCadena(strOutLine);
				strOutLine = 
							 "TOTAL DEPOSITADO A LA CUENTA DE CHEQUES:  1   "
							 + strDivisa
							 + strFiller.substring(50, (68 - strMonto.length()))
							 + strMonto;
				tpImpresora.enviaCadena(strOutLine);
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena(strFiller2);
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
			if (iDriver == 0) {
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");
				tpImpresora.enviaCadena("");					
				tpImpresora.imprimeCadena1();
				tpImpresora.activateControl("CORTAPAPEL",iDriver);
			} else
				tpImpresora.activateControl("SALTOHOJA",iDriver);
			}
			
			
	return intStatus;
	}

	
}