<!--
//*************************************************************************************************
//             Funcion: JSP para despliege la consulta de solicitud de cheques expirados
//            Elemento: PageBuilderRECG.jsp
//      Modificado por: YGX
//*************************************************************************************************
-->
<%@ page session="true" import="java.util.*,ventanilla.GenericClasses"%>
<%
response.setHeader("Cache-Control","no-store"); //HTTP 1.1
response.setHeader("Pragma","no-cache"); //HTTP 1.0
response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
%>

<%!
 
  private String formatMonto(String s)
	{
		int len = s.length();
 		if (len < 3)
    		s = "0." + s;
    	else
    	{
      	int n = (len - 3 ) / 3;
    		s = s.substring(0, len - 2) + "." + s.substring(len - 2 , len);
      	for (int i = 0; i < n; i++)
      	{
      		len = s.length();
    			s = s.substring(0, (len -(i*3+6+i))) + "," + s.substring((len -(i*3+6+i)) , len);
    		}
    	}
      for (int i = s.length(); i < 16 ; i++)
    		s = " " + s;
    	return s;
  	}

  private String delCeros(String s)
  {
    int len = 0;
    int space = 0;
    for(len = 0 ; len < s.length(); len++)
	  if(s.charAt(len) == '0')
	   space ++;
	  else
	   break;
    return s.substring(space, s.length());
  }
  
	private String formatFecha(String s)
	{
		String fechaAux;
		
		fechaAux = "/" + s.substring(0,4);
		fechaAux = "/" + s.substring(4,6) + fechaAux;
		fechaAux = s.substring(6,8) + fechaAux;	    		
		
    	return fechaAux;
  	}
  
  private Hashtable getInfo(String sucursal)
    
    {  
        String statementPS = com.bital.util.ConnectionPoolingManager.getStatementPostFix();
        java.sql.Connection pooledConnection = com.bital.util.ConnectionPoolingManager.getPooledConnection( );
        java.sql.Statement select = null;
        java.sql.ResultSet Rselect = null;
   	
    	Hashtable hs = new Hashtable();
    	String sql="";    	
    	int key = 1; 
    			
		try
		{
       
         if(pooledConnection != null)
           {
             select=pooledConnection.createStatement();
           	 sql = "SELECT D_BENEFICIARIO,D_MONTO,C_FECHA_SOL, D_DETALLE, D_DIRECCION, D_AUTORIZADO, D_CEDULA, D_CONNUREG, D_SUC_EXP, D_CUENTA ";
           	 sql = sql + "FROM TW_CHQGER WHERE D_STATUS='4'";
           	 //sql = sql + "FROM TW_CHQGER WHERE D_SUC_EXP='";
           	 //sql = sql + sucursal+"' AND D_STATUS='4'";           
           	 //System.out.println("SQL ["+sql+"]");
             Rselect = select.executeQuery(sql); 	   
             while(Rselect.next())
                {  
                  StringBuffer st=new StringBuffer();
                  String a=(String)Rselect.getString("D_AUTORIZADO");
                  if (a.equals(""))
                  {
                    a=". ";
                  }
                  
                  String b=(String)Rselect.getString("D_CEDULA");
                  if (b.equals(""))
                  {
                    b=". ";
                  }
                  
                  String c=(String)Rselect.getString("D_DIRECCION");
                  if (c.equals(""))
                  {
                    c=". ";
                  }
                  
                  String d = (String)Rselect.getString("D_BENEFICIARIO");
                  d = d.replace('\'', ' ');
                  d = d.replace('*',' ');
                  d = d.replace('"',' ');
                  //if(d.length() > 40)
						//d = d.substring(0, 40);
                  hs.put("key"+key,d+"~"+(String)Rselect.getString("D_MONTO")+"~"+(String)Rselect.getString("C_FECHA_SOL")+"~"+(String)Rselect.getString("D_DETALLE")+"~"+c+"~"+a+"~"+b+"~"+(String)Rselect.getString("D_CONNUREG")+"~"+(String)Rselect.getString("D_SUC_EXP")+"~"+(String)Rselect.getString("D_CUENTA")); 
                  key++;   
                  //System.out.println("Entr al query");                             
    			}     
           }
        }
      catch(java.sql.SQLException sqlException){
	        //System.out.println("TW_CHQGER::SelectLog [" + sqlException + "]");                    
		}        
        finally {
	    	try {
	    		if(Rselect != null) {
		           Rselect.close();
		           Rselect = null;
		        }
        		if(select != null) {
	            	select.close();
    	            select = null;
                }
	            if(pooledConnection != null) {
    	            pooledConnection.close();
        	        pooledConnection = null;
            	}
            }
	        catch(java.sql.SQLException sqlException){
    	        //System.out.println("Error FINALLY TW_CHQ::SelectLog [" + sqlException + "]");                
           	}
	    } 
  		return  hs;
  		
  	} 
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<%

String MontoPro = new String("");
Vector listaCampos = (Vector)session.getAttribute("page.listcampos");
Vector listaContenidos = (Vector)session.getAttribute("page.listcont");
String txnAuthLevel = (String)session.getAttribute("page.txnAuthLevel");
String txnIMGAutoriz = (String)session.getAttribute("page.txnIMGAutoriz");
Vector v = (Vector)session.getAttribute("page.specialacct");
Hashtable datasession = (Hashtable)session.getAttribute("page.datasession");
String txn = session.getAttribute("page.cTxn").toString();
ventanilla.GenericClasses gc = new ventanilla.GenericClasses();
if (datasession == null)
   datasession = new Hashtable();
int k;
int l;
int lastTabIndex = 0;

%>
<html>
<head>
  <%@include file="style.jsf"%>
  <title>Datos de la Transaccion</title>
<script language="JavaScript">
<!--
var formName = 'Mio'
var specialacct = new Array()
function validate(form)
{
}


//-->
</script>
</head>
<body>
<br>
<form name="entry" action="../servlet/ventanilla.DataServlet" method="post">
<h1><%=session.getAttribute("page.txnImage")%></h1>
<h3>
<%out.print(session.getAttribute("page.cTxn") + "  " +
                (String)session.getAttribute("page.txnlabel"));%>
</h3>
<%
  //Sucursal
  String sucursal = (String)session.getAttribute("branch"); 
  String llave="";
  String mensaje = "1~1~0000000~BUSQUEDA SIN RESULTADOS~";
  
  Hashtable info = getInfo(sucursal);
  
  if( info.size() == 0 )
  {
    out.println("No existen cheques pendientes por imprimir registrados para esta Sucursal");    
    session.setAttribute("page.txnresponse", mensaje);
   }
  else 
  {    
	   String data="";
	   String datatmp="";
	   String datatmp1="";
	   String datatmp2="";
	   String datatmp3="";
	   String datatmp4="";
	   String datatmp5="";
	   String datatmp6="";
	   String datatmp7="";  
	   String datatmp8="";
	   String datatmp9="";
	   
	   mensaje = "0~1~0000000~BUSQUEDA ACEPTADA~";
	   session.setAttribute("page.txnresponse", mensaje);	
%>
<table border="1" >
    <tr bgcolor="#CCCCCC">       
        <td align="center">Beneficiario</td>
        <td align="center">Monto</td>
        <td align="center">Fecha Solicitud</td>
    </tr>

<%
   
   for(int i=1; i<=info.size(); i++)  
   
   {     
   	   data = (String)info.get("key"+i); 
	 //  System.out.println(data);
	   StringTokenizer tok = new StringTokenizer(data,"~");             
	   while(tok.hasMoreTokens()) 
	   { 
	    datatmp = tok.nextToken();
	    
	     
 %>
		<tr>
		  
		 <td align="center">
		 <a href="javascript:PrevDetalle('<%=data%>');Authorization();" title="Detalle"; onmouseover="self.status='Detalle';return true;" onmouseout="window.status='';" alt="Detalle"><%=datatmp%></a>
		 <input type="hidden" name="txtBeneficiario" value="<%out.print(datatmp);%>">
		 </td>
<%
		  datatmp1 = tok.nextToken();
		  datatmp1 = formatMonto(delCeros(datatmp1));
		 
		  
		
%>
		 	<td align="center"><%=datatmp1%></td>
<%
		   datatmp2 = tok.nextToken(); 
		   datatmp2 = formatFecha(datatmp2);
		  
		   
		   
%>
		 	<td align="center"><%=datatmp2%></td>
<%
           
		  datatmp3 = tok.nextToken();			   
		  datatmp4 = tok.nextToken();		 
		  datatmp5 = tok.nextToken();		 
		  datatmp6 = tok.nextToken();		  
		  datatmp7 = tok.nextToken();
		  datatmp8 = tok.nextToken(); 
		  datatmp9 = tok.nextToken(); 
%>
		
		 </tr>
<%  
	    }
	}
%>

</table>
<%
}

   session.setAttribute("txtCadImpresion", "NOIMPRIMIRNOIMPRIMIR");

%>
<input type="hidden" name="iTxn" value="<%= session.getAttribute("page.iTxn") %>">
<input type="hidden" name="oTxn" value="<%= session.getAttribute("page.oTxn") %>">
<input type="hidden" name="cTxn" value="<%= session.getAttribute("page.cTxn") %>">
<input type="hidden" name="sucursal" value="<%= session.getAttribute("branch") %>">
<input type="hidden" name="teller" value="<%= session.getAttribute("teller") %>">
<input type="hidden" name="moneda" value="<%= session.getAttribute("page.moneda")%>">
<input type="hidden" name="registro" value="<%=session.getAttribute("empno")%>">
<input type="hidden" name="tllrlvl" value="<%=session.getAttribute("tellerlevel")%>">
<input type="hidden" name="supervisor" value="<%= session.getAttribute("teller") %>">
<input type="hidden" name="AuthoOK" value="0">
<input type="hidden" name="SignOK" value="0">
<input type="hidden" name="validateFLD" value="0">
<input type="hidden" name="indice" value="0">
<input type="hidden" name="needSignature" value="<%out.print(txnIMGAutoriz);%>">
<input type="hidden" name="needAutho" value="<%out.print(txnAuthLevel);%>">
<input type="hidden" name="SignField" value="<%= session.getAttribute("page.SignField") %>">
<input type="hidden" name="txtPlazaSuc" value="<%= session.getAttribute("plaza") %>" >
  
<input type="hidden" name="val_datos" value="0">

<p>

<br><center><a href="javascript:cancelar(document.entry)"><img src="../ventanilla/imagenes/b_cancelar.gif" border="0"></a></center>

</form>

<script language="JavaScript">	
top.formName = '';
function Authorization()
{
    if(window.document.entry.AuthoOK.value != '1')
    {
        top.openDialog('../ventanilla/paginas/authoriz.jsp', 400, 200, 'top.setPrefs7()', window.document.entry);
    }
}
function Detalle(llave)
		{    
		   window.document.entry.val_datos.value=llave;
		//   alert(llave);
		   entry.submit();  		    	
		 
		}
function PrevDetalle(llave)
		{    
		  
		   window.document.entry.val_datos.value=llave;
		//   alert(llave);
		 //  entry.submit();  		    	
		 
		}
function cancelar(form)
{
 // alert("hola");
  document.entry.action = "../ventanilla/paginas/Final.jsp";
  entry.submit();
}
</script>

</body>
</html>