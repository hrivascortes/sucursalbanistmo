<!--
//*************************************************************************************************
//             Funcion: JSP para relaciones de 1009
//            Elemento: PageBuilderRels.jsp
//          Creado por: Alejandro Gonzalez Castro
//      Modificado por: Rutilo Zarco
//*************************************************************************************************
// CCN - 4360211 - 09/10/2004 - Se crea menu de relaciones
// CCN - 4360275 - 18/02/2005 - WAS 5.1
// CCN - 4360581 - 12/03/2007 - Se convierten los estilos en jsf y se hace un include en el jsp
//*************************************************************************************************
-->


<%@ page session="true" import="java.util.*"%>
<%
response.setHeader("Cache-Control","no-store"); //HTTP 1.1
response.setHeader("Pragma","no-cache"); //HTTP 1.0
response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<%
Vector listaCampos = (Vector)session.getAttribute("page.listcampos");
Vector listaContenidos = (Vector)session.getAttribute("page.listcont");
int k;
int i;
%>
<html>
<head>
  <title>Datos de la Transaccion</title>
  <%@include file="style.jsf"%>
<script language="JavaScript">
<!--

var formName = 'Mio'
var specialacct = new Array()
var bandera = 0;
function validate(form)
{
   form.validateFLD.value = '0';
   inputStr = '';
   
<%     String txn = (String)session.getAttribute("page.cTxn");
  for(i=0; i < listaCampos.size(); i++)
   {
    Vector vCampos = (Vector)listaCampos.get(i);
    vCampos = (Vector)vCampos.get(0);
    if( vCampos.get(4).toString().length() > 0 )
    {
      out.println("  if( !top.validate(window, document.entry." + vCampos.get(0) + ", '" + vCampos.get(4) +"') )");
      out.println("return;") ;
    }
   }
%>
  form.validateFLD.value = '1'
  if (document.entry.txtFechaEfect || document.entry.txtFechaError || document.entry.txtFechaCarta)
    document.entry.txtFechaSys.value=top.sysdatef();

 <%
    out.println("  form.submit()");
    out.println("var obj = document.getElementById('btnCont');");
    out.println("obj.onclick = new Function('return false');");
%>
}
//-->
</script>
</head>
<body bgcolor="#FFFFFF" onload="top.setFieldFocus(window.document)">
<br>
<% out.print("<form name=\"entry\" action=\"../servlet/ventanilla.RelacionesServlet\" method=\"post\" ");
   out.println(">");
%>
<h1><%=session.getAttribute("page.txnImage")%></h1>
<h3>
<%out.print(session.getAttribute("page.cTxn") + "  " + (String)session.getAttribute("page.txnlabel"));%>
</h3>
<table border="0" cellspacing="0" class="normaltextblack">
<%
for(i =  0; i < listaCampos.size(); i++) 
{
    Vector vCampos = (Vector)listaCampos.get(i);
    vCampos = (Vector)vCampos.get(0);
    out.println("<tr><td>" + vCampos.get(2) + ":</td><td width=\"10\">&nbsp;</td>");
    if(!vCampos.get(0).toString().substring(0,3).equals("lst")) 
    {
        out.print("  <td><input maxlength=\"" + vCampos.get(3) + "\" name=\"" + vCampos.get(0) + "\" size=\"" + vCampos.get(3) +"\" type=\"text\"" + " tabindex=\""+(i+1)+"\"");
        String CampoR = (String)vCampos.get(5);
        if(CampoR != null && CampoR.length() > 0){
            CampoR = CampoR.trim();
            out.print(" value=\"" + CampoR + "\"");
        }
    }
    else
    {
        out.print("  <td><select name=\"" + vCampos.get(0) + "\" tabindex=\"" + (i+1) + "\" size=\"" + "1" + "\"");
        if( vCampos.get(4).toString().length() > 0 )
        {
            if(vCampos.get(0).toString().substring(0,3).equals("rdo"))
                out.println(" onClick=\"top.validate(window, this, '" + vCampos.get(4) +"')\">");
            else
                out.println(" onChange=\"top.validate(window, this, '" + vCampos.get(4) +"')\"  >");
        }
        else
            out.println(">");
        if(vCampos.get(0).toString().equals("lstBanco"))
        {%>
            <option value="000">Todos los Bancos
<%        }
        for(k=0; k < listaContenidos.size(); k++){
            Vector v1Campos = (Vector)listaContenidos.get(k);
            for(int j = 0; j < v1Campos.size(); j++){
                Vector v1Camposa = (Vector)v1Campos.get(j);
                if(v1Camposa.get(0).toString().equals(vCampos.get(0).toString()))
                    out.println("  <option value=\"" + v1Camposa.get(3) + "\">" + v1Camposa.get(2));
            }
        }
        out.println("  </select></td>");
    }
    if( vCampos.get(4).toString().length() > 0 && !vCampos.get(0).toString().substring(0,3).equals("lst"))
        out.println("  onBlur=\"top.estaVacio(this)||top.validate(window, this, '" + vCampos.get(4) +"')\"  onKeyPress=\"top.keyPressedHandler(window, this, '" + vCampos.get(4) +"')\"   ></td>");
    else if(!vCampos.get(0).toString().substring(0,3).equals("lst"))
        out.println(">");
    out.println("</tr>");
}
	%>
</table>
<input type="hidden" name="iTxn" value="<%= session.getAttribute("page.iTxn") %>">
<input type="hidden" name="oTxn" value="<%= session.getAttribute("page.oTxn") %>">
<input type="hidden" name="cTxn" value="<%= session.getAttribute("page.cTxn") %>">
<input type="hidden" name="sucursal" value="<%= session.getAttribute("branch") %>">
<input type="hidden" name="teller" value="<%= session.getAttribute("teller") %>">
<input type="hidden" name="moneda" value="<%= session.getAttribute("page.moneda")%>">
<input type="hidden" name="tllrlvl" value="<%=session.getAttribute("tellerlevel")%>">
<input type="hidden" name="supervisor" value="<%= session.getAttribute("teller") %>">
<input type="hidden" name="returnform" value="N" >
<input type="hidden" name="validateFLD" value="0">
<p>
	<style>
		.invisible
		{
		  display:None;
		}

		.visible
		{
		  display:"";
		}
	</style>


<%
    out.println("<a id='btnCont' tabindex=\"" + (i+1) + "\" href=\"javascript:document.entry.returnform.value='S';validate(document.entry)\"><img src=\"../ventanilla/imagenes/b_continuar.gif\" border=\"0\"></a>");
%>

</form>
</body>
</html>
