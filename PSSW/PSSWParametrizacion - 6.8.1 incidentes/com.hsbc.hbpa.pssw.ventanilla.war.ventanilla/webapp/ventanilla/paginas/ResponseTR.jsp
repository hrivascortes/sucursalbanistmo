<!--
//*************************************************************************************************
//             Funcion: JSP que despliega respuesta de txns Transferencias
//            Elemento: ResponseTR.jsp
//          Creado por: Alejandro Gonzalez Castro
// Ultima Modificacion: 03/08/2004
//      Modificado por: Alejandro Gonzalez Castro
//*************************************************************************************************
// CCN - 4360175 - 03/08/2004 - Ajuste size de ventana de impresion
//*************************************************************************************************
-->

<%@ page session="true" import="java.util.*" %>
<%
response.setHeader("Cache-Control","no-store"); //HTTP 1.1
response.setHeader("Pragma","no-cache"); //HTTP 1.0
response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<%!
  private String getString(String s)
  {
    int len = s.length();
    for(; len>0; len--)
	  if(s.charAt(len-1) != ' ')
	    break;
    return s.substring(0, len);
  }

  private String getItem(String newCadNum, int newOption)
  {
   int iPIndex = newCadNum.indexOf("CONSECUTIVO:") + 12, NumSaltos, Num = 0, Ban = 0;
   String nCadNum = new String("");
   NumSaltos = newOption;
   while(Num < NumSaltos)
   {
    if(newCadNum.charAt(iPIndex) == 0x20)
    {
     while(newCadNum.charAt(iPIndex) == 0x20)
      iPIndex++;
     Num++;
    }
    else
     while(newCadNum.charAt(iPIndex) != 0x20)
      iPIndex++;
   }
   while(newCadNum.charAt(iPIndex) != 0x20 && iPIndex < newCadNum.length()){
    nCadNum = nCadNum + newCadNum.charAt(iPIndex);
    if(iPIndex < newCadNum.length())
      iPIndex++;
    if(iPIndex == newCadNum.length())
      break;
   }

   return nCadNum;
  }
%>
<%
Vector resp = (Vector)session.getAttribute("response");
Hashtable datasession = (Hashtable)session.getAttribute("page.datasession");
%>

<html>
<head>
  <title>Certificación</title>
<style type="text/css">Body {font-family: Arial, Helv; font-size: 10pt; font-weight: bold; color:#000000}</style>
<script language="javascript">
<!--
  sharedData = '0';

  function formSubmitter()
  {
    clearInterval(formSubmitter);
    
    if(sharedData != '0')
    {
      document.forms[0].submit()
      sharedData = '0';
    }
  }
//-->
</script>
</head>
<body bgcolor="#FFFFFF" onload="top.setFieldFocus(window.document);setInterval('formSubmitter()',2000)">
<p>
<%
    Stack flujotxn = new java.util.Stack();
    String codigo = "";
    String needOverride = "NO";
    String lineresp = "";
    if(session.getAttribute("page.flujotxn") != null)
  	flujotxn = (Stack)session.getAttribute("page.flujotxn");
    if(resp.size() != 4)
   	out.println("<b>Error de conexi&oacute;n</b><br>");
    else
    {
	try
	{
	   codigo = (String)resp.elementAt(0);
   	   if( !codigo.equals("0") )
    		out.println("<b>Transaccion Rechazada</b><br><br>");
           if ( codigo.equals("0") || codigo.equals("1")) {
     	       int lines = Integer.parseInt((String)resp.elementAt(1));
	       String szResp = (String)resp.elementAt(3);
	       for(int i=0; i<lines; i++)
	       {
	   	  String line = szResp.substring( i*78, Math.min(i*78+77, szResp.length()) );
	   	  lineresp = lineresp + line;
	     	  out.println(getString(line) + "<p>");
     		  if(Math.min(i*78+77, szResp.length()) == szResp.length())
      		     break;
    		}
	    }
            else {
               if ( codigo.equals("3") || codigo.equals("2"))
                  out.println("<b>Transacci&oacute;n Requiere Autorizaci&oacute;n</b>");
               else
                  out.println("<b>Ocurri&oacute; un Error de Comunicaci&oacute;n, Verifique.</b>");
            }
          }
     	catch(NumberFormatException nfe)
		{out.println("<b>Error de conexi&oacute;n</b><br>");}


		if( codigo.equals("0"))
		{
     		String DDAcuenta = (String)datasession.get("txtDDAO");
	   	  	String CDAcuenta = (String)datasession.get("txtCDAD");
   	  		String monto = (String)datasession.get("txtMonto");
	  		String txtudi = lineresp.substring(24,40);
	  		String montoUDI = "";
	  		String display = "";%>
	 		<br><br>Valor de la UDI : <%out.println(" " + txtudi + "");%>
			<%
			try
			{
				StringBuffer monto$ = new StringBuffer((String)datasession.get("txtMonto"));
		 		for(int i=0; i<monto$.length();)
 				{
  					if( monto$.charAt(i) == ',' )
   						monto$.deleteCharAt(i);
  					else
 						++i;
		 		}
		 		double dividendo = Double.parseDouble(monto$.toString());
		 		double divisor = Double.parseDouble(txtudi);
		 		double division = dividendo / divisor;
		 		StringBuffer res = new StringBuffer(Double.toString(division));
				int flag = 0;
		 		int enter = 0;
		 		int i = 0;
		 		int punto = 0;
		 		String entero = "";
		 		String cents = "";
		 		for(i=0; i<res.length(); ++i)
		 		{
		  			if( res.charAt(i) == 'E' )
		  			{
		   				flag = 1;
		   				break;
		  			}
				}
		 		if (flag == 0)
 				{
  					for(i=0; i<res.length(); ++i)
		  			{
   						if( res.charAt(i) == '.' )
   						{
       		    			entero = res.substring(0,i);
    						cents = res.substring(i+1,i+3);
    						break;
          				}
					}
 				}
				if (flag == 1)
				{
			  		for(i=0; i<res.length(); ++i)
	 		 		{
			   			if( res.charAt(i) == 'E' )
	   					{
		       		 	    punto = Integer.parseInt(res.substring(i+1,res.length()));
						    String tmp = res.substring(0,i);
	   				 		StringBuffer temp = new StringBuffer(tmp);
						    for(i=0; i<temp.length();++i)
	   				 		{
							     if( temp.charAt(i) == '.' )
							     {
								 	entero = temp.substring(0,i);
								    entero = entero + temp.substring(i+1,i+punto+1);
									cents = temp.substring(i+punto+1,i+punto+3);
									break;
						     	}
					    	}
        			    	break;
		   				}
					}
	 			}
				// area comun
				montoUDI = entero + cents;

				enter = entero.length();
				for (i = 0; i < (enter-(1+i))/3; i++)
	 			{
	  				enter = entero.length();
	  				entero = entero.substring(0,enter-(4*i+3))+','+entero.substring(enter-(4*i+3));
	 			}
	 			display = entero + "." + cents;
	 			datasession.put("montodisplayUDI", display);
	 			datasession.put("montoUDI", montoUDI);
	 			datasession.put("tasaUDI", txtudi);
     			session.setAttribute("page.datasession", datasession);
			}
			catch(Exception e)
				{System.out.println(e);}
		}
	}
%>

<p>
<form name="Txns" action="../../servlet/ventanilla.PageServlet" method="post">
<table border="0" cellspacing="0">
<tr>
<td>
<%
  String isCertificable = "";
  if(session.getAttribute("page.certifField") != null)
    isCertificable = (String)session.getAttribute("page.certifField");  // Para Certificación
  String txtCadImpresion  = ""; // Para Certificación
  if(!flujotxn.empty())
  {
    out.print("<div align=\"center\">");
    
    if(!isCertificable.equals("N"))
    { 
      // Para Certificación
      session.setAttribute("Consecutivo", getItem((String)resp.elementAt(3), 1)); // Para Certificación
      session.setAttribute("Tipo", isCertificable); // Para Certificación
      
      if(codigo.equals("0"))
        out.println("<script language=\"javascript\">top.openDialog('../../servlet/ventanilla.ImprimirServlet?txtCadImpresion=" + txtCadImpresion + "', 160, 120, 'top.setPrefs4()', document.Txns)</script>"); // Para Certificación
      else 
      {
        if ( codigo.equals("2") || codigo.equals("3"))
        {
	  needOverride = "SI";
	  out.print("<script languaje=\"javascript\"> top.openDialog('authoriz.jsp', 220, 130, 'top.setPrefs8()', document.Txns) </script>");
        }
        else
	  out.println("<a href=\"javascript:top.openDialog('../../servlet/ventanilla.ImprimirServlet?txtCadImpresion=" + txtCadImpresion + "', 160, 120, 'top.setPrefs4()', document.Txns)\"><img src=\"imagenes/b_continuar.gif\" border=\"0\"></a>"); // Para Certificación
      }
    }
    else
    {
      if(codigo.equals("0"))
      {
	out.println("<script language=\"javascript\">sharedData = '1'</script>");
	out.println("<font color='red'>procesando...</font>");
      }
      else 
      {
        if (codigo.equals("2") || codigo.equals("3")) 
        {
          needOverride = "SI";
          out.print("<script languaje=\"javascript\"> top.openDialog('authoriz.jsp', 220, 130, 'top.setPrefs8()', document.Txns) </script>");
        }
        else
          out.print("<input type=\"image\" src=\"imagenes/b_continuar.gif\" alt=\"\" border=\"0\">");
      }
    }
    
    out.print("</div>");
  }
  else
  {
    session.setAttribute("Consecutivo", getItem((String)resp.elementAt(3), 1)); // Para Certificación
    session.setAttribute("Tipo", isCertificable); // Para Certificación
    
    if ( codigo.equals("2") || codigo.equals("3")) 
    {
      needOverride = "SI";
      out.print("<script languaje=\"javascript\"> top.openDialog('authoriz.jsp', 220, 130, 'top.setPrefs8()', document.Txns) </script>");
    }
    else
      out.println("<script language=\"javascript\">top.openDialog('../../servlet/ventanilla.ImprimirServlet?txtCadImpresion=" + txtCadImpresion + "', 160, 120, 'top.setPrefs4()', document.Txns)</script>");
  }
%>
  <input type="hidden" name="transaction" value="<%= session.getAttribute("page.iTxn") %>">
  <input type="hidden" name="transaction1" value="<%= session.getAttribute("page.oTxn") %>">
  <input type="hidden" name="supervisor" value="<%= session.getAttribute("supervisor") %>">
  <input type="hidden" name="override" value="<%= needOverride%>">
</td>
</tr>
</table>
</form>
</body>
</html>
