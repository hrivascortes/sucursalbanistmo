<!--
//*************************************************************************************************
//             Funcion: JSP que realiza la presentacion de los datos de la txn0124 para su poseto
/             Elemento: PageBuilderIRCC.jsp
//          Creado por: Juvenal R. Fernandez Varela
// Ultima Modificacion: 05/08/2004
//      Modificado por: Juan Carlos Gaona Marcial
//*************************************************************************************************
// CCN - 4360171 - 23/07/2004 - Se agrega relacion 0124
// CCN - 4360175 - 05/08/2004 - Se habilita relacion 9029 con opciones monto Cero y Efectivo
// CCN - 4360390 - 07/10/2005 - Se agregan relaciones para la txn 9503
// CCN - 4360397 - 03/11/2005 - Se modifica validacion para relaciones sendservlet.
// CCN - 4360581 - 12/03/2007 - Se convierten los estilos en jsf y se hace un include en el jsp
//*************************************************************************************************
-->

<%@ page session="true"%>
<%
response.setHeader("Cache-Control","no-store"); //HTTP 1.1
response.setHeader("Pragma","no-cache"); //HTTP 1.0
response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
	<head>
		<title>Impresion de Relaciones</title>
		<%@include file="style.jsf"%>
	</head>
	<Script language="JavaScript">
		var valor1 = "1"
		var valor2 = "1"
	
		function sendservlet()
		{		
		    
		    if(document.entry.cTxn.value == 9503)
		    {
		      if(!top.validate(window, document.entry.opcion1, 'ValRel5503'))
		         {
		         document.entry.opcion2.focus();
		         return ;
		         }
		    } 		    
		    document.entry.action = "../servlet/ventanilla.RelacionesServlet"
		    document.entry.submit();
		}

		function setValues1()
		{
			
	 		valor1 = document.entry.opcion1.value;
		}

		function setValues2()
		{
	 		valor2 = document.entry.opcion2.value;
		}
		
   </SCRIPT>	
	<body bgcolor="#FFFFFF" onload="top.setFieldFocus(window.document)">   
		<form name="entry">
			<input type="hidden" name="cTxn" value="<%= session.getAttribute("page.cTxn") %>">			
			<input type="hidden" name="moneda" value="<%= session.getAttribute("page.moneda")%>">					
			<table border="0" cellspacing="0">       			
			<b><h3>&nbsp</b></h3>
<%						
			out.println("<b><h3>");
			out.print(session.getAttribute("page.cTxn") + "  " + (String)session.getAttribute("page.txnlabel"));
			out.println("</h3></b>");
			out.println("<tr><td>");		
			out.println("<br>");		
			out.println("</td></tr>");					
			String strTxn    = (String) session.getAttribute("page.cTxn");
			String strMoneda = (String) session.getAttribute("page.moneda");						
			if (strTxn.equals("9251"))
			{
				out.println("<tr>");
         	out.println("	<td><b>Seleccione : </b></td>");
		   	out.println("	<td>");
		   	out.println("		<SELECT name=opcion1 size=1 onchange=\"setValues1()\">");
		   	out.println("			<OPTION value=1>Local</OPTION>");
		   	out.println("			<OPTION value=2>Foraneo</OPTION>");
				out.println("		</SELECT>");
		   	out.println("	</td>");
		   	out.println("</tr>");				
				out.println("<tr>");
				out.println("<td><b>Numero de Banco:</b></td>");
				out.println("<td><input type=\"text\" maxlength=\"3\" name=\"Banco\" size=\"3\" ></td>");
		   	out.println("</tr>");		   											
			}			
			if (strTxn.equals("9029"))
			{
				out.println("<tr>");
         	out.println("	<td><b>Seleccione : </b></td>");
		   	out.println("	<td>");
		   	out.println("		<SELECT name=opcion1 size=1 onchange=\"setValues1()\">");
		   	out.println("			<OPTION value=1>En Ceros</OPTION>");
		   	out.println("			<OPTION value=2>En Efectivo y/o Cheque</OPTION>");
				out.println("		</SELECT>");
		   	out.println("	</td>");
		   	out.println("</tr>");				
			}			
			if (strTxn.equals("9503"))
			{
				out.println("<tr>");
         	out.println("	<td><b>Tipo de Reporte : </b></td>");
		   	out.println("	<td>");
		   	out.println("		<SELECT name=opcion1 size=1 onchange=\"setValues1()\">");
		   	out.println("			<OPTION value=1>Tesoreria de la Federación</OPTION>");
		   	out.println("			<OPTION value=2>Comisión Nacional del Agua</OPTION>");
		   	out.println("			<OPTION value=3>Declaraciones 1% sobre Erogaciones y 2% sobre Hospedaje</OPTION>");
		   	out.println("			<OPTION value=4>Declaraciones SF-2</OPTION>");
		   	out.println("			<OPTION value=5>Declaraciones SF-3</OPTION>");
		   	out.println("			<OPTION value=6>Declaraciones Tenencias 2005 Estado de Oaxaca</OPTION>");
		   	out.println("			<OPTION value=7>Declaraciones Tenencias 2005 Estado de Puebla</OPTION>");
				out.println("		</SELECT>");
		   	out.println("	</td>");
		   	out.println("</tr>");				
				out.println("<tr>");
         	out.println("	<td><b>Pago  : </b></td>");
		   	out.println("	<td>");
		   	out.println("		<SELECT name=opcion2 size=1 onchange=\"setValues2()\">");
		   	out.println("			<OPTION value=1>En Ceros</OPTION>");
		   	out.println("			<OPTION value=2>En Efectivo</OPTION>");
		   	out.println("			<OPTION value=3>En Documentos</OPTION>");
				out.println("		</SELECT>");
		   	out.println("	</td>");
		   	out.println("</tr>");		   											
			}			
			else if (strTxn.equals("9630"))
			{

				out.println("<tr>");
                                out.println("	<td><b>Reporte de Egresos:</b></td>");		   	
		   		out.println("	<td>");
		   		out.println("		<SELECT name=opcion1 size=1 onchange=\"setValues1()\">");
		   		out.println("			<OPTION value=1>Todas Las Transacciones de Egresos</OPTION>");			
        			if (strMoneda.equals("01"))
                out.println("			<OPTION value=5>0476 Disposición Carnet con P.O.S.</OPTION>");				
		   		out.println("			<OPTION value=9>0492 Caja Concentración Enviadas</OPTION>");			
				out.println("		</SELECT>");
		   		out.println("	</td>");
		   		out.println("</tr>");														
			}			
			else if (strTxn.equals("9820"))
			{
				out.println("<tr>");
         	out.println("	<td><b>Reporte de:</b></td>");		   	
		   	out.println("	<td>");
		   	out.println("		<SELECT name=opcion1 size=1 onchange=\"setValues1()\">");
		   	out.println("			<OPTION value=1 selected>Pagos del dia</OPTION>");			
		   	out.println("			<OPTION value=2>Pagos Desfasados</OPTION>");			
				out.println("		</SELECT>");
		   	out.println("	</td>");
		   	out.println("</tr>");														
			}			
			if (strTxn.equals("9640"))
			{
				if (strMoneda.equals("02"))
				{
			  		out.println("<tr>");
         		out.println("	<td><b>Reporte de :</b></td>");		   	
		   		out.println("	<td>");
		   		out.println("		<SELECT name=opcion1 size=1 onchange=\"setValues1()\">");
					out.println("			<OPTION value=1>Cheque de Viajero en Efectivo</OPTION>");
					out.println("			<OPTION value=2>Cheque de Viajero Abono a Cuenta</OPTION>");
					out.println("			<OPTION value=3>Cheques Extranjeros</OPTION>");
					out.println("			<OPTION value=4>Totales</OPTION>");
					out.println("		</SELECT>");
		   		out.println("	</td>");
		   		out.println("</tr>");							
				}
				else
				{
			  		out.println("<tr>");
         		out.println("	<td><b>Reporte de :</b></td>");		   	
		   		out.println("	<td>");
		   		out.println("		<SELECT name=opcion1 size=1 onchange=\"setValues1()\">");
		   		out.println("			<OPTION value=1 selected>Cheques Extranjeros</OPTION>");			
		   		out.println("			<OPTION value=2>Totales</OPTION>");			
					out.println("		</SELECT>");
		   		out.println("	</td>");
		   		out.println("</tr>");														
				}							
				out.println("<tr>");
         	out.println("	<td><b></b></td>");		   	
		   	out.println("	<td>");
		   	out.println("		<SELECT name=opcion2 size=1 onchange=\"setValues2()\">");
		     	out.println("			<OPTION value=1>Antes del Corte  </OPTION>");			
		     	out.println("			<OPTION value=2>Despues del Corte</OPTION>");			
			   out.println("		</SELECT>");
		      out.println("	</td>");
		   	out.println("</tr>");																					
			}						
			else if (strTxn.equals("9730"))
			{
				out.println("<tr>");
         	out.println("	<td><b>Reporte de Servicios Especiales :</b></td>");		   	
				out.println("<td><b>Numero de Servicio:</b></td>");
				out.println("<td><input type=\"text\" maxlength=\"6\" name=\"Servicio\" size=\"6\" ></td>");
		   	out.println("</tr>");														
			}
			else if (strTxn.equals("9732"))
			{
				out.println("<tr>");
         	out.println("	<td><b>Reporte de Ingresos:</b></td>");		   	
		   	out.println("	<td>");
		   	out.println("		<SELECT name=opcion1 size=1 onchange=\"setValues1()\">");
		   	out.println("			<OPTION value=1>Todas Las Transacciones de Ingresos</OPTION>");			
		   	out.println("			<OPTION value=7>0458 Dotación Recibida</OPTION>");			
		   	out.println("		</SELECT>");
		   	out.println("	</td>");
		   	out.println("</tr>");														
			}
			else if (strTxn.equals("9821"))
			{
				out.println("<tr>");
         	out.println("	<td><b>Seleccione Transacción: </b></td>");
		   	out.println("	<td>");
		   	out.println("		<SELECT name=opcion1 size=1 onchange=\"setValues1()\">");
		   	out.println("			<OPTION value=1>5405</OPTION>");
		   	out.println("			<OPTION value=2>5407</OPTION>");
				out.println("		</SELECT>");
		   	out.println("	</td>");
		   	out.println("</tr>");				
			}						
			else if (strTxn.equals("9009"))
			{
		    out.println("<tr>");
         	out.println("	<td><b>Seleccione: </b></td>");
		   	out.println("	<td>");
		   	out.println("		<SELECT name=opcion1 size=1 onchange=\"setValues1()\">");
		   	out.println("			<OPTION value=1>Reporte de cheque local</OPTION>");
		   	//out.println("			<OPTION value=2>Reporte para Intercambio</OPTION>");
		   	//out.println("			<OPTION value=3>Reporte para Truncamiento</OPTION>");
				out.println("		</SELECT>");
		   	out.println("	</td>");
		   	out.println("</tr>");				
			}						
			out.println("<tr><td>");		
			out.println("<br>");		
			out.println("</td></tr>");		
			out.println("<tr>");		
		   out.println("	<td>");
			out.print("<a href=\"JavaScript:sendservlet()\"><img src=\"../ventanilla/imagenes/b_aceptar.gif\" border=\"0\"></a>");
			out.println("	</td>");			
			out.println("</tr>");
%>			
		</table>			
		</form>
	</body>
</html>
