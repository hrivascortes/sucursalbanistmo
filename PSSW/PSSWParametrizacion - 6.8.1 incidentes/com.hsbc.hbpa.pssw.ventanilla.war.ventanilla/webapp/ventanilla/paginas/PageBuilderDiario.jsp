<!--
//*************************************************************************************************
//             Funcion: JSP que despliega opciones para consulta del Diario
//            Elemento: PageBuilderDiario.jsp
//          Creado por: Alejandro Gonzalez Castro
//      Modificado por: Fausto Rodrigo Flores Moreno
//*************************************************************************************************
// CCN - 4360175 - 03/08/2004 - Se restringen consultas a todos los cajeros, solo nivel 3 y 4
// CCN - 4360178 - 20/08/2004 - Habilitar transaccion CON para consultas al diario
// CCN - 4360221 - 22/10/2004 - Consultas por Status "C" Cancelada
// CCN - 4360364 - 19/08/2005 - No se muestra el cajero a consultar
// CCN - 4360581 - 12/03/2007 - Se convierten los estilos en jsf y se hace un include en el jsp
// CCN - 4360593 - 20/04/2007 - Se realizan modificaciones para corrección de reverso de un RAP
//*************************************************************************************************
-->

<%@ page session="true" import="java.util.*,ventanilla.com.bital.beans.Monedas"%>
<% 
response.setHeader("Cache-Control","no-store"); //HTTP 1.1
response.setHeader("Pragma","no-cache"); //HTTP 1.0
response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<%
    ventanilla.com.bital.admin.DiarioQuery DiarioSQL = new ventanilla.com.bital.admin.DiarioQuery();
    String suc = (String)session.getAttribute("branch");
    suc = suc.substring(1);
    DiarioSQL.getTellers(suc, false);
    
    if(session.getAttribute("session.DiarioSQL") != null)
        session.removeAttribute("session.DiarioSQL");
        
    if(session.getAttribute("session.DiarioRev") != null)
        session.removeAttribute("session.DiarioRev");
	
	if(session.getAttribute("idFlujo") != null)
		session.removeAttribute("idFlujo");
		
	if(session.getAttribute("page.flujotxn") != null){
        Stack flujotxn = (Stack)session.getAttribute("page.flujotxn");
        if(!flujotxn.isEmpty()){
        	flujotxn.clear();
        	session.setAttribute("page.flujotxn",flujotxn);
        }
    }
	
   	if(session.getAttribute("page.datasession") != null){
	    session.removeAttribute("page.datasession");
	}
%>
<html>
<head>
<%@include file="style.jsf"%>
<title>Datos de la Transaccion</title>
</head>
<body bgcolor="#FFFFFF" onload="top.setFieldFocus(window.document)">
<form name="diario" action="ResponseDiario.jsp" method="post">

<h1>Diario Electr&oacute;nico</h1>

<table border="0" cellspacing="0">
        <%
        Vector cajeros = DiarioSQL.getIDTellers();
        Vector nombres = DiarioSQL.getNameTellers();
        String cajero = null;
        String nivel = (String)session.getAttribute("tellerlevel");
        int size = cajeros.size();
        if(nivel.equals("1") || nivel.equals("2"))
           	cajero = (String)session.getAttribute("teller");
        else
           	cajero = (String)request.getParameter("cajero");
            %>
            <input type="hidden" name="cajero" value="<%=cajero%>" size="6" onfocus="blur()">
<tr>
    <td colspan="2">Tipo Diario :</td><td>&nbsp;&nbsp;</td>
    <td>
        <select name="tipodiario" tabindex="2">
        <option selected value="1">Diario Actual</option>
        <option value="2">Todos los Diarios</option>
        </select>
    </td>
    <td>&nbsp;&nbsp;</td>
    <td colspan="2">Orden de B&uacute;squeda :</td><td>&nbsp;&nbsp;</td>
    <td>
        <select name="orden" tabindex="3">
        <option selected value="1">Ascendente</option>
        <option value="2">Descendente</option>
        <option value="3">Por Monto Descendente</option>
        </select>
    </td>
</tr>
<tr>
    <td colspan="2">Moneda :</td><td>&nbsp;&nbsp;</td>
    <td>
        <select name="moneda" tabindex="4">
        <option selected value="0">Todas</option>
  <%
     Monedas monedas = Monedas.getInstance();
     java.util.Vector vMonedas = monedas.getMonedas_Syb();
     int iMon = 0;
     for(iMon=0;iMon<vMonedas.size();iMon++)
     {
     		out.println("<option value=\""+(String)((Vector)vMonedas.elementAt(iMon)).elementAt(0)+"\">"+(String)((Vector)vMonedas.elementAt(iMon)).elementAt(1)+"</option>");	
     		//System.out.println((String)((Vector)vMonedas.elementAt(iMon)).elementAt(0));
     }
   %>
        </select>
    </td>
    <td>&nbsp;&nbsp;</td>
    <td colspan="2">Registros por P&aacute;gina :</td><td>&nbsp;&nbsp;</td>
    <td>
        <select name="registros" tabindex="5">
        <option selected value="10">10</option>
        <option value="20">20</option>
        <option value="30">30</option>
        <option value="40">40</option>
        <option value="50">50</option>
        </select>
    </td>
</tr>
<tr><td colspan="9" align="center"><b>Criterios de B&uacute;squeda :</b></td></tr>
<tr>
    <td colspan="2">N&uacute;mero de Cuenta :</td><td>&nbsp;&nbsp;</td>
    <td><input name="cuenta" type="text" size="15" maxlength="15" tabindex="6" 
         onBlur="top.estaVacio(this)||top.validate(window, this, 'isnumeric')" 
         onKeyPress="top.keyPressedHandler(window, this, 'isnumeric')" ></td>
    <td>&nbsp;&nbsp;</td>
    <td colspan="2">Importe :</td><td>&nbsp;&nbsp;</td>
    <td><input name="importe" type="text" size="15" maxlength="15" tabindex="7" 
         onBlur="top.estaVacio(this)||top.validate(window, this, 'isCurrency')"
         onKeyPress="top.keyPressedHandler(window, this, 'isCurrency')" ></td>
</tr>
<tr>
    <td colspan="2">Total al Cargo :</td><td>&nbsp;&nbsp;</td>
    <td>
        <select name="totalC" tabindex="8" >
        <option selected value="0">Todos</option>
        <option value="1">1 - Efectivo Recibido</option>
        <option value="2">2 - Cuentas a la Vista</option>
        <option value="3">3 - Documentos</option>
        <option value="4">4 - Cuentas a Plazo</option>
        <option value="5">5 - Egresos Varios</option>
        <option value="6">6 - Cargos Varios</option>
        <option value="15">15 - Cobro Inmediato</option>
        <option value="16">16 - Compra Venta</option>
        </select>
    </td>
    <td>&nbsp;&nbsp;</td>
    <td colspan="2">Total al Abono :</td><td>&nbsp;&nbsp;</td>
    <td>
        <select name="totalA" tabindex="9">
        <option selected value="0">Todos</option>
        <option value="7">7 - Efectivo Pagado</option>
        <option value="8">8 - Documentos</option>
        <option value="9">9 - Cuentas a la Vista</option>
        <option value="10">10 - Cuentas a Plazo</option>
        <option value="11">11 - Ingresos Varios</option>
        <option value="12">12 - Abonos Varios</option>
        <option value="17">17 - Compra Venta</option>
        </select>
    </td>
</tr>
<tr>
    <td colspan="2">Consecutivo Inicial :</td><td>&nbsp;&nbsp;</td>
    <td><input name="consecutivoI" type="text" size="10" maxlength="10" tabindex="10" 
         onBlur="top.estaVacio(this)||top.validate(window, this, 'isnumeric')" 
         onKeyPress="top.keyPressedHandler(window, this, 'isnumeric')" ></td>
    <td>&nbsp;&nbsp;</td>
    <td colspan="2">Consecutivo Final :</td><td>&nbsp;&nbsp;</td>
    <td><input name="consecutivoF" type="text" size="10" maxlength="10" tabindex="11" 
         onBlur="top.estaVacio(this)||top.validate(window, this, 'isnumeric')" 
         onKeyPress="top.keyPressedHandler(window, this, 'isnumeric')" ></td>
</tr>
<tr>
    <td>Fecha Inicial :</td>
    <td align="right">D&iacute;a</td>
    <td>&nbsp;&nbsp;</td>
    <td>
        <select name="diaI" tabindex="12">
        <option selected value="00">--</option>
        <option value="01">01</option>
        <option value="02">02</option>
        <option value="03">03</option>
        <option value="04">04</option>
        <option value="05">05</option>
        <option value="06">06</option>
        <option value="07">07</option>
        <option value="08">08</option>
        <option value="09">09</option>
        <option value="10">10</option>
        <option value="11">11</option>
        <option value="12">12</option>
        <option value="13">13</option>
        <option value="14">14</option>
        <option value="15">15</option>
        <option value="16">16</option>
        <option value="17">17</option>
        <option value="18">18</option>
        <option value="19">19</option>
        <option value="20">20</option>
        <option value="21">21</option>
        <option value="22">22</option>
        <option value="23">23</option>
        <option value="24">24</option>
        <option value="25">25</option>
        <option value="26">26</option>
        <option value="27">27</option>
        <option value="28">28</option>
        <option value="29">29</option>
        <option value="30">30</option>
        <option value="31">31</option>
        </select>
    </td>
    <td>&nbsp;&nbsp;</td>
    <td>Fecha Final :</td>
    <td align="right">D&iacute;a</td>
    <td>&nbsp;&nbsp;</td>
    <td>
        <select name="diaF" tabindex="13">
        <option selected value="00">--</option>
        <option value="01">01</option>
        <option value="02">02</option>
        <option value="03">03</option>
        <option value="04">04</option>
        <option value="05">05</option>
        <option value="06">06</option>
        <option value="07">07</option>
        <option value="08">08</option>
        <option value="09">09</option>
        <option value="10">10</option>
        <option value="11">11</option>
        <option value="12">12</option>
        <option value="13">13</option>
        <option value="14">14</option>
        <option value="15">15</option>
        <option value="16">16</option>
        <option value="17">17</option>
        <option value="18">18</option>
        <option value="19">19</option>
        <option value="20">20</option>
        <option value="21">21</option>
        <option value="22">22</option>
        <option value="23">23</option>
        <option value="24">24</option>
        <option value="25">25</option>
        <option value="26">26</option>
        <option value="27">27</option>
        <option value="28">28</option>
        <option value="29">29</option>
        <option value="30">30</option>
        <option value="31">31</option>
        </select>
    </td>
</tr>
<tr>    
    <td>&nbsp;&nbsp;</td>
    <td align="right">Mes</td>
    <td>&nbsp;&nbsp;</td>
    <td>
        <select name="mesI" tabindex="14">
        <option selected value="00">--</option>
        <option value="01">01 - Enero</option>
        <option value="02">02 - Febrero</option>
        <option value="03">03 - Marzo</option>
        <option value="04">04 - Abril</option>
        <option value="05">05 - Mayo</option>
        <option value="06">06 - Junio</option>
        <option value="07">07 - Julio</option>
        <option value="08">08 - Agosto</option>
        <option value="09">09 - Septiembre</option>
        <option value="10">10 - Octubre</option>
        <option value="11">11 - Noviembre</option>
        <option value="12">12 - Diciembre</option>
        </select>
    </td>
    <td>&nbsp;&nbsp;</td>
    <td>&nbsp;&nbsp;</td>
    <td align="right">Mes</td>
    <td>&nbsp;&nbsp;</td>
    <td>
        <select name="mesF" tabindex="15">
        <option selected value="00">--</option>
        <option value="01">01 - Enero</option>
        <option value="02">02 - Febrero</option>
        <option value="03">03 - Marzo</option>
        <option value="04">04 - Abril</option>
        <option value="05">05 - Mayo</option>
        <option value="06">06 - Junio</option>
        <option value="07">07 - Julio</option>
        <option value="08">08 - Agosto</option>
        <option value="09">09 - Septiembre</option>
        <option value="10">10 - Octubre</option>
        <option value="11">11 - Noviembre</option>
        <option value="12">12 - Diciembre</option>
        </select>
    </td>
</tr>
<tr>    
    <td>&nbsp;&nbsp;</td>
    <td align="right">A&ntilde;o</td>
    <td>&nbsp;&nbsp;</td>
    <td>
        <select name="anoI" tabindex="16">
        <option selected value="0000">--</option>
        <option value="2000">2000</option>
        <option value="2001">2001</option>
        <option value="2002">2002</option>
        <option value="2003">2003</option>
        <option value="2004">2004</option>
        <option value="2005">2005</option>
        </select>
    </td>
    <td>&nbsp;&nbsp;</td>
    <td>&nbsp;&nbsp;</td>
    <td align="right">A&ntilde;o</td>
    <td>&nbsp;&nbsp;</td>
    <td>
        <select name="anoF" tabindex="17">
        <option selected value="0000">--</option>
        <option value="2000">2000</option>
        <option value="2001">2001</option>
        <option value="2002">2002</option>
        <option value="2003">2003</option>
        <option value="2004">2004</option>
        <option value="2005">2005</option>
        </select>
    </td>
</tr>
<tr>
    <td colspan="2">N&uacute;mero de Transacci&oacute;n :</td><td>&nbsp;&nbsp;</td>
    <td><input name="codigotxn" type="text" size="4" maxlength="4" tabindex="18" 
         onBlur="top.estaVacio(this)||top.validate(window, this, 'isnumeric')" 
         onKeyPress="top.keyPressedHandler(window, this, 'isnumeric')"></td>
    <td>&nbsp;&nbsp;</td>
    <td colspan="2">Status :</td><td>&nbsp;&nbsp;</td>
    <td>
        <select name="status" tabindex="19">
        <option selected value="T">Todos</option>
        <option value="A">A - Aceptada</option>
        <option value="R">R - Rechazada</option>
        <option value="C">C - Cancelada</option>
        <option value="X">X - Reversada</option>
        <option value="?">? - Dudosa</option>
        </select>
    </td>
</tr>
</table>
<input type="hidden" name="sucursal" value="<%= session.getAttribute("branch") %>">
<input type="hidden" name="teller" value="<%= session.getAttribute("teller") %>">
<input type="hidden" name="registro" value="<%=session.getAttribute("empno")%>">
<input type="hidden" name="tllrlvl" value="<%=session.getAttribute("tellerlevel")%>">
<input type="hidden" name="supervisor" value="<%= session.getAttribute("teller") %>">
<input type="hidden" name="nopagina" value="0">
<input type="hidden" name="diario" value="0">
<input type="hidden" name="AuthoOK" value="0">
<input type="hidden" name="SignOK" value ="0">
<input type="hidden" name="ip" value="<%=session.getAttribute("dirip")%>">
<p>
<a tabindex="20" onclick="this.onclick = new Function('return false');" href="javascript:submit();" onmouseover="self.status='Continuar';return true;" onmouseout="window.status='';" alt="Continuar"><img src="../imagenes/b_continuar.gif" border="0"></a>
</form>
</body>
<script language="JavaScript">
function submit()
    {
        window.document.diario.submit();
}
</script>
</html>
