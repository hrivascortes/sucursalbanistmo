<!-- 
165 
// CCN - 4360581 - 12/03/2007 - Se convierten los estilos en jsf y se hace un include en el jsp
-->

<%@ page session="true"%>
<%
response.setHeader("Cache-Control","no-store"); //HTTP 1.1
response.setHeader("Pragma","no-cache"); //HTTP 1.0
response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<% String message = (String)session.getAttribute("mensaje"); %>
<html>
<head>
<title>Resultado</title>
<title>Response Page</title>
<%@include file="style.jsf"%>
</head>
<body>
<b><%out.print(message);%></b>
</body>
</html>

