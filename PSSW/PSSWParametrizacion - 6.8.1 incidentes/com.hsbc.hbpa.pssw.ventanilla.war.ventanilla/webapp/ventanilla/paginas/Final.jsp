<!--
//*************************************************************************************************
//             Funcion: JSP que evalua Control de Efectivo
//            Elemento: Final.jsp
//          Creado por: Alejandro Gonzalez Castro
//      Modificado por: Fausto Rodrigo Flores Moreno
//*************************************************************************************************
// CCN - 4360253 - 07/01/2005 - Se evita posicionar el cursor en la caja en la Consulta de Totales del gerente
// CCN - 4360260 - 21/01/2005 - Se agrega funcion CRMRT que abre ventana para realizar interfaz con CRMRT.
// CCN - 4360279 - 18/02/2005 - Se modifica dns de producci�n
// CCN - 4360306 - 22/04/2005 - Se modifica funcion CRMRT() para poder realizar interfaz de CRMRT en el ambiente de Capacitacion.
// CCN - 4360347 - 22/07/2005 - Se modifica DNS para CRM en produccion.
// CCN - 4360364 - 19/08/2005 - Se activa Control de Efectivo en D�lares
// CCN - 4360368 - 02/09/2005 - Se activa mensaje para Efectivo.
// CCN - 4360374 - 08/09/2005 - Se genera nuevo paquete a peticion de QA.
// CCN - 4360395 - 21/10/2005 - Se agrega funcion que verifica tecla backspace y se elimina valiable de sesion
//                              para el mensaje de alerta de efectivo.
// CCN - 4360437 - 24/02/2006 - Se realizan modificaciones para caja de acceso r�pido
// CCN - 4360456 - 07/04/2006 - Se elimina validaci�n de CRM para sucursales.
// CCN - 4360540 - 09/11/2006 - Se realizan modificaciones para ficha de deposito
// CCN - 4360541 - 10/11/2006 - Se realizan modificaciones borrar variable de sesion con txns antriores que operan cheques.
// CCN - 4360556 - 12/01/2007 - Se realizan modificaciones para mostrar el mensaje de confirmaci�n para la txn 1003
// CCN - 4360558 - 17/01/2007 - Se agrega validaci�n para no mostrar el mensaje de confirmaci�n de la txn 1003 al realizar el reverso.
// CCN - 4360587 - 03/04/2007 - Se agrega condici�n para guardar en tabla TH_MONPIF para el control de Efectivo
// CCN - 4360589 - 16/04/2007 - Se realizan modificaciones para el monitoreo de efectivo en las sucursales
// CCN - 4360593 - 20/04/2007 - Se elimina variable idFlujo
// CCN - 4360618 - 12/06/2007 - Se realizan modificaciones para no mostrar mensaje de alerta de usuario bloqueado
// CCN - 4360640 - 12/07/2007 - Se realizan modificaciones no bloquear el usuario de b�veda.
// CCN - 46200xx - 19/10/2007 - Se realizan modificaciones para mensaje de alerta al limite de efectivo
//*************************************************************************************************
-->
<%@ page session="true" import="ventanilla.com.bital.util.*,java.util.*"%>
<% 
response.setHeader("Cache-Control","no-store"); //HTTP 1.1
response.setHeader("Pragma","no-cache"); //HTTP 1.0
response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
String horario = (String)session.getAttribute("horario");

String hab = "";
String cheqs = "";
String cheqsT = "";
String txn = (String)session.getAttribute("page.iTxn");
String HAB ="";

Hashtable datasession = null;
if(session.getAttribute("page.datasession")!=null){
datasession = (Hashtable)session.getAttribute("page.datasession");
HAB = (String)datasession.get("HAB");

}
if(session.getAttribute("viewCSIB") != null && session.getAttribute("viewCSIB").equals("1")){
session.removeAttribute("viewCSIB");
}
String proceso="";
%>
</body>

<%
if(session.getAttribute("CountChequesOK")!=null)
	cheqs=(String)session.getAttribute("CountChequesOK");
if(session.getAttribute("CountChequesT")!=null)
	cheqsT=(String)session.getAttribute("CountChequesT");
session.removeAttribute("CountChequesOK");
session.removeAttribute("CountChequesT");
if(datasession != null && datasession.get("SHAB") != null)
	hab = (String)datasession.get("SHAB");
%>
  <%!
	public Vector Campos(String cadena, String delimitador) {
		NSTokenizer parser = new NSTokenizer(cadena, delimitador);
		Vector campos = new Vector();
		while (parser.hasMoreTokens()) {
			String token = parser.nextToken();
			if (token == null)
				continue;
			campos.addElement(token);
		}
		return campos;
	}
	
	private String formatMonto(String s)
	{
		int len = s.length();
		if (len < 3)
			s = "0." + s;
		else
		{
			int n = (len - 3 ) / 3;
			s = s.substring(0, len - 2) + "." + s.substring(len - 2 , len);
			for (int i = 0; i < n; i++)
			{
				len = s.length();
				s = s.substring(0, (len -(i*3+6+i))) + "," + s.substring((len -(i*3+6+i)) , len);
			}
		}
		return s;
	}
	

  
  %>
<html>
    <head>
    <title>Panel de Captura</title>
<Script language="JavaScript">

document.onkeydown = function()
{ 
if (!top.KeyCheck(window))
    {
    return false;
    }
    else
    {
    return true;
    }

  	
} 

function checkCash()
{
    var overCash = '<%=(String)session.getAttribute("AlertCash")%>'
    var overCash2 = '<%=(String)session.getAttribute("AlertCashDlls")%>'

    if(overCash != 0)
        alert('El Efectivo en Caja en D�LARES excede el l�mite permitido,\nfavor de realizar la Concentraci�n a B�veda');
    
    if(overCash2 != 0)
        alert('El Efectivo en Caja en EUROS excede el l�mite permitido,\nfavor de realizar la Concentraci�n a B�veda');
        
    if(parent.frames["transaccionejecutar"].TransaccionAislada)    
        parent.frames["transaccionejecutar"].TransaccionAislada.transactionField.focus();
}


function checkEfec()
{
var efec='<%=(String)session.getAttribute("AlertEfec")%>';
if (efec == 1)
   {
   <%
    session.setAttribute("AlertEfec", "0");
    session.removeAttribute("AlertEfec");
   %> 
   alert('Recuerda tabular el movimiento para facilitar el cierre de tus operaciones');    
   }
    if(parent.frames["transaccionejecutar"].TransaccionAislada)    
        parent.frames["transaccionejecutar"].TransaccionAislada.transactionField.focus();   
}

function CRMRT()
{
<%  String crmactivado = (String)session.getAttribute("CRM");%>
<%  if(crmactivado != null)// && crmactivado.equals("S"))
    {%>
        var id = "<%=session.getAttribute("empno")%>";
        var branch = "<%=session.getAttribute("branch")%>";
        var event = "V";
        var cta = "<%=session.getAttribute("CRMcta")%>";
        var txn = "<%=session.getAttribute("CRMTxn")%>";
        var dbQualifier = "<%=session.getAttribute("CRMQualifier")%>";
        nameWindow = "CRMRealTime";
        options = "resizable=0,toolbar=0,scrollbars=0,menubar=0,width=400,height=200,top=235,left=140,status=0";

        if(dbQualifier == "p")
        	window.open("http://s04webcp01.mx.hsbc:9080/hsbc/listener.jsp?NUM_CTA="+cta+"&NUM_SUCURSAL="+branch+"&ID_EJECUTIVO="+id+"&ID_EVENTO="+event+"&TPO_TRANSACCION="+txn,nameWindow,options);
        else if(dbQualifier == "c")
	        window.open("http://s04retid01.mx.hsbc:9080/capacitacion/listener.jsp?NUM_CTA="+cta+"&NUM_SUCURSAL="+branch+"&ID_EJECUTIVO="+id+"&ID_EVENTO="+event+"&TPO_TRANSACCION="+txn,nameWindow,options);
        else
        	window.open("http://s04retid01.mx.hsbc:9080/hsbc/listener.jsp?NUM_CTA="+cta+"&NUM_SUCURSAL="+branch+"&ID_EJECUTIVO="+id+"&ID_EVENTO="+event+"&TPO_TRANSACCION="+txn,nameWindow,options);        	
        <%session.removeAttribute("CRM");
        session.removeAttribute("CRMTxn");
        session.removeAttribute("CRMcta");%> 
<%  }%>
    if(parent.frames["transaccionejecutar"].TransaccionAislada)    
        parent.frames["transaccionejecutar"].TransaccionAislada.transactionField.focus();
}

</script>
</head>
<%!
  ventanilla.GenericClasses gc = new ventanilla.GenericClasses();
%>
<%
    if(session.getAttribute("FichaDeposito") != null)		
		session.removeAttribute("FichaDeposito");
	if(session.getAttribute("idFlujo") != null)
		session.removeAttribute("idFlujo");
    String efe = (String)session.getAttribute("AlertCash");
    String efe2 = (String)session.getAttribute("AlertCashDlls");
    String efe3 = (String)session.getAttribute("PIFAlertCash");
    String efe4 = (String)session.getAttribute("PIFAlertCashDlls");
    if( (efe.equals("1") || efe2.equals("1")) && (!horario.equals("C")) )
    {
        ventanilla.com.bital.sfb.GerenteD GD = new ventanilla.com.bital.sfb.GerenteD();
        GD.setTxnCode("CON ");
        GD.setBranch(session.getAttribute("branch").toString());
        GD.setTeller(session.getAttribute("teller").toString());
        GD.setSupervisor(session.getAttribute("teller").toString());
        GD.setTotalsId("CONCEN");
        GD.setCurrCod1("SE HA INDICADO EFECTUAR CONCEN. DE EFVO");
        String consecliga =  gc.stringFormat(java.util.Calendar.HOUR_OF_DAY, 2) + gc.stringFormat(java.util.Calendar.MINUTE, 2) + gc.stringFormat(java.util.Calendar.SECOND, 2);
        ventanilla.com.bital.admin.Diario diario = new ventanilla.com.bital.admin.Diario();
        if(efe.equals("1"))
        {
            GD.setFromCurr("N$");
        	String sMessage = diario.invokeDiario(GD, consecliga, session.getAttribute("dirip").toString(), "01", session);
	        session.setAttribute("AlertCash", "0");
    	}
	    else if(efe2.equals("1"))
        {
            GD.setFromCurr("US$");
            String sMessage = diario.invokeDiario(GD, consecliga, session.getAttribute("dirip").toString(), "02", session);
            session.setAttribute("AlertCashDlls", "0");
        }

	%>  <body bgcolor="#FFFFFF" text="#000000" onLoad="checkCash();checkEfec();CRMRT();">
<% }
   else
   {%>
   		<body bgcolor="#FFFFFF" text="#000000" onLoad="checkEfec();CRMRT();">
<% }
    if(efe.equals("1") || efe2.equals("1")){
        MonitorEfectivo ME = new MonitorEfectivo();
        String lock = (String)session.getAttribute("ActiveLock");
        if(efe3.equals("1"))
        {
            String mfactor = String.valueOf(session.getAttribute("MontoFactor"));
            int resPIFINS = ME.InsertPIF(gc.quitap(mfactor) ,session.getAttribute("efectivoPIF").toString(),"01",session.getAttribute("teller").toString());
            if(lock.equals("A") && !horario.equals("C")){
				int resPIFUPD = ME.UpdateUsuarios(session.getAttribute("teller").toString());
			}else{
				session.setAttribute("PIFAlertCash","0");
			}
    	}
	    else if(efe4.equals("1"))
        {
        	String mfactor2 =String.valueOf(session.getAttribute("MontoFactor2"));
            int resPIFINS = ME.InsertPIF(gc.quitap(mfactor2) ,session.getAttribute("efectivoPIF").toString(),"02",session.getAttribute("teller").toString());
			if(lock.equals("A") && !horario.equals("C")){
				int resPIFUPD = ME.UpdateUsuarios(session.getAttribute("teller").toString());
			}else{
			    session.setAttribute("PIFAlertCashDlls","0");
			}
        }
    }
%>
<% 
  if (session.getAttribute("strRespuesta") != null) {    
	String Certifica1003 = "N";
	String strTmp = "";
	Vector vCamposT = new Vector();;
	Vector vParam = new Vector();
	String strRespuesta = session.getAttribute("strRespuesta").toString();
    session.removeAttribute("strRespuesta");	
	Vector vCampos = Campos(strRespuesta, "^");
	int bandera = 0;

	if(session.getAttribute("page.iTxn") != null && session.getAttribute("page.iTxn").equals("1003"))
		bandera = 1;
			else
		bandera = vCampos.size();
				
	for (int i = 0; i < bandera ; i++) {
		strTmp = (String) vCampos.elementAt(i);
		if (strTmp.startsWith("CERTIFICACION")) {
			strTmp = (String) vCampos.elementAt(i);
			vCamposT = Campos(strTmp, "@");
			strTmp = (String) vCamposT.elementAt(1);
			vCamposT = Campos(strTmp, "|");
			strTmp = (String) vCamposT.elementAt(0);
			vParam = Campos(strTmp, "~");
			Certifica1003 = "S";
		}
	}
   if(Certifica1003.equals("S")){
	String strCuenta = (String) vParam.elementAt(1);
	String strDivisa = (String) vParam.elementAt(2);
	strDivisa = gc.getDivisa(strDivisa.trim());
	if ((strDivisa.equals("N$")) || (strDivisa.equals("N$ ")))
		strDivisa = "$";
	//else
	//	strDivisa = "US$";
		int A = strRespuesta.indexOf("~A~");
		int C = strRespuesta.indexOf("~CONSECUTIVO");
		String strMonto = strRespuesta.substring(A+3,C);
	
	strTmp = (String) vParam.elementAt(12);
	int nlineas = strTmp.length() / 78;
	if (strTmp.length() % 78 > 0)
		nlineas++;				
	String strTmpL = "";						
	for (int j = 0; j < nlineas; j++) {
		if (((j * 78) + 78) < strTmp.length())
			strTmpL = strTmp.substring((j * 78), (j * 78) + 78);
		else
			strTmpL = strTmp.substring(j * 78, strTmp.length());
		if (j == 1 )
			j = nlineas + 2;
	}					
   if(vParam.elementAt(10).equals("A")){
    out.println("<font style='font-family:arial,helvetica;font-size:13px;' color='red'><B><br><br>Confirma con el Depositante que el deposito realizado corresponde a este cliente</b><br><br></font>");
    out.print("<table style='font-family:arial,helvetica;font-size:13px;'>");
    out.print("<tr>");
    out.print(" <td>"); 
	out.println(" Nombre : " + strTmpL);
    out.print(" </td>"); 
    out.print("<tr>");	    
    out.print(" <td>"); 
	out.println(" Cuenta : " + strCuenta );
    out.print(" </td>");
    out.print("<tr>");     
    out.print(" <td>"); 
	out.println(" Deposito Total : " + strDivisa + " "+formatMonto(strMonto));
    out.print(" </td>"); 
    out.print("</tr>");    
    out.print("</table>");
    }
   }
  }
  
  if(hab.equals("HAB"))
  {
    out.println("<font style='font-family:arial,helvetica;font-size:13px;' color='red'><B><br><br>RECUERDA GENERAR FORMATO SIB PARA ESTA TRANSACCI�N</b><br><br></font>");
  }
  else if(hab.equals("SHAB"))
  {
   	out.println("<font style='font-family:arial,helvetica;font-size:13px;' color='red'><B><br><br>CLIENTE CON HABITUALIDAD</b><br><br></font>");
  }
  if(!cheqs.equals("0") && !cheqs.equals("") && session.getAttribute("page.iTxn") != null & session.getAttribute("page.iTxn").equals("1003"))
  {
    out.println("<font style='font-family:arial,helvetica;font-size:13px;' color='red'><B><br><br>Se postearon exitosamente "+cheqs.toString()+" de "+cheqsT.toString()+" Cheques devueltos</b><br><br></font>");
  }
	if(session.getAttribute("montoChqDev1003") != null)
		session.removeAttribute("montoChqDev1003");

 %>
    </body>
</html>


