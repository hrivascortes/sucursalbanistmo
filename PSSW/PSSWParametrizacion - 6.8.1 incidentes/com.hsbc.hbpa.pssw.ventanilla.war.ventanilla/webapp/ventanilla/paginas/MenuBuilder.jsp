<!--
//*************************************************************************************************
//             Funcion: JSP que presenta los menues de todas las txns
//          Creado por: Alejandro Gonzalez Castro
//      Modificado por: Fredy Pe�a Moreno
//*************************************************************************************************
// CCN - 4360275 - 18/02/2005 - WAS 5.1
// CCN - 4360342 - 08/07/2005 - Se realizan modificaciones para departamentos.
// CCN - 4360364 - 19/08/2005 - Se controlan departamentos por Centro de Costos.
// CCN - 4360443 - 24/02/2006 - Se realizan modificaciones para no mostrar txns 0226 si no es depto
// CCN - 4360456 - 07/04/2006 - Se realizan modificaciones para txn secundarias por nivel y control de txns
				secundarias para departamentos.
// CCN - 4360510 - 08/09/2006 - Se generan los combos con las transaciones disponibles cada perfil
// CCN - 4360581 - 12/03/2007 - Se convierten los estilos en jsf y se hace un include en el jsp
// CCN - 4620048 - 14/12/2007 - Se modifica para compra venta
//*************************************************************************************************
-->

<%@ page session="true" errorPage="error.jsp" import="java.util.*"%>
<%
response.setHeader("Cache-Control","no-store"); //HTTP 1.1
response.setHeader("Pragma","no-cache"); //HTTP 1.0
response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
String[] proceso = request.getParameterValues("list");
String isdepto = (String)session.getAttribute("isForeign");
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
  <title>MenuBuilder</title>
<%@include file="style.jsf"%>
</head>
<body onLoad="setCurrentTxns(document.Txns)">
<%!
  private String stringFormat(int option, int NumDig)
  {
    java.util.Calendar now = java.util.Calendar.getInstance();
    String temp = new Integer(now.get(option)).toString();
    for(int i = temp.length(); i < NumDig; i++)
     temp = "0" + temp;

    return temp;
  }
%>
<%
 Vector menuOne = (Vector)session.getAttribute("menu.info");
 Vector menuTwo = (Vector)session.getAttribute("menu2.info");
 Vector menuThree = (Vector)session.getAttribute("monedas.info");
 String timestamp = stringFormat(java.util.Calendar.HOUR_OF_DAY, 2) + stringFormat(java.util.Calendar.MINUTE, 2) + stringFormat(java.util.Calendar.SECOND, 2);
 session.setAttribute("d_ConsecLiga", timestamp);
 //System.out.println("Este es el menu 1: "+menuOne);
 //System.out.println("Este es el menu 2: "+menuTwo);
%>
<SCRIPT LANGUAGE="JavaScript">
 var isdepto = '<%=session.getAttribute("isForeign")%>'
 var newTest = new Array()
<%
  for(int i = 0; i < menuOne.size(); i++){
   out.print("newTest[" + (i) + "] = new Array(");
   Vector Cols = (Vector)menuOne.get(i);
   for(int j = 0; j < Cols.size(); j++){
    session.setAttribute("txnProcess", Cols.get(0));
    out.print("'" + Cols.get(j) + "'");
    if(j + 1 != Cols.size())
     out.print(",");
   }
   out.println(")");
  }
%>

 //alert(newTest)
 var newTest2 = new Array()
<%
  for(int i = 0; i < menuTwo.size(); i++){
   out.print("newTest2[" + (i) + "] = new Array(");
   Vector Cols = (Vector)menuTwo.get(i);
   for(int j = 0; j < Cols.size(); j++){
    out.print("'" + Cols.get(j) + "'");
    if(j + 1 != Cols.size())
     out.print(",");
   }
   out.println(")");
  }
%>
//alert(newTest2)
 var newTest3 = new Array()
<%
  if(menuThree.size() == 0)
   out.print("newTest3[0]= new Array('No Disponible','No Disponible')");
  for(int i = 0; i < menuThree.size(); i++){
   out.print("newTest3[" + (i) + "] = new Array(");
   Vector Cols = (Vector)menuThree.get(i);
   for(int j = 0; j < Cols.size(); j++){
    out.print("'" + Cols.get(j) + "'");
    if(j + 1 != Cols.size())
     out.print(",");
   }
   out.println(")");
  }
%>

 function isPar(txnOrg){
   var ispar = false
   var h=0
   while(h<newTest.length && !ispar){
     tmpcad = newTest[h][1]
     if(tmpcad.indexOf('-')>-1)
       if(tmpcad.substring(0,tmpcad.indexOf('-'))==txnOrg)
         ispar=true
       else
         ispar=false
     else
       ispar=false
     h++
   }
   return ispar
 }

 function searchTxnSec(txnOrg, txnSec){
   var existe = false
   var g=0;
   while((g<newTest.length) && (existe==false)){
     cadTxnOrg = newTest[g][1];
     cadTxnSec = cadTxnOrg.substring(cadTxnOrg.indexOf('-')+1,cadTxnOrg.length);
     cadTxnOrg = cadTxnOrg.substring(0,4);    
     if(cadTxnOrg == txnOrg && cadTxnSec==txnSec){
       existe = true
     }else
       existe = false
     g++;
   }
   return existe;
 }
 
  function finaliza_txns(forma){
   var existe = false
   var g=0;
   var txnOrg;
   var txnSec;
   var curTxnSec;
   var llave;
   var tamano1 = forma.transaction.options.length;
   var tamano2 = forma.transaction1.options.length;
   
		for(Idx2 = 0; Idx2 < tamano2; Idx2++)
  		{
		  		txnOrg = forma.transaction.options[forma.transaction.selectedIndex].value;
		  		txnSec = forma.transaction1.options[Idx2].value;
		  		curTxnSec = forma.moneda.options[forma.moneda.selectedIndex].value;
				llave = txnOrg+txnSec+curTxnSec;
		  			   while((g<newTest2.length) && (existe==false)){
		   	 			llave_comparacion=newTest2[g][1]+newTest2[g][3]+newTest2[g][2];
		     			if(llave_comparacion == llave){
		       				existe = true
		    	 		}else
		       				existe = false
		     				g++;
		   				}
		  		if(!existe)
		  		{
		  			forma.transaction1.options[Idx2] = null;
		  			tamano2=tamano2-1;
		  			Idx2=Idx2-1;
		  			existe = false;
		  			g=0;
		  		}
  		}

  		if(tamano1>1){
  		    g=0;
  		    existe=false
  			for(Idx1 = 1; Idx1 < tamano1; Idx1++)
  			{
		  		txnOrg = forma.transaction.options[Idx1].value;
		  		curTxnSec = forma.moneda.options[forma.moneda.selectedIndex].value;
				llave = txnOrg+curTxnSec;
		  			   while((g<newTest2.length) && (existe==false)){
		   	 			llave_comparacion=newTest2[g][1]+newTest2[g][2];
		     			if(llave_comparacion == llave){
		       				existe = true
		    	 		}else
		       				existe = false
		     				g++;
		   				}
		  		if(!existe)
		  		{
		  			forma.transaction.options[Idx1] = null;
		  			tamano1=tamano1-1;
		  			Idx1=Idx1-1;
		  			existe = false;
		  			g=0;
		  		}
  			}
  		}

  		//alert(forma.transaction.options.length)
  		//alert(forma.transaction1.options.length)
  		
  		
  		if(forma.transaction1.options.length == 0)
  		{
   			forma.transaction1.options[0] = new Option();
   			eval("forma.transaction1.options[0].text='No Disponible '");
   			eval("forma.transaction1.options[0].value='ND'");
  		}
  		
  		
  		
  		
  		
  		
  		
  		
 }
 
 function repetido(cadTxn,forma){
   var again = false;
   var f=0;
   while((f<forma.transaction.length) && (again==false)){
     if(forma.transaction.options[f].value == cadTxn){
       again = true
     }else{
       again = false
     }
     f++;
   }
   return again; 
 }

 function setCurrentTxns(forma){

  TotalOpt = forma.transaction.options.length			//Inicializa Menues Primarios
  Idx1 = TotalOpt - 1
  while (Idx1 > -1) {
   forma.transaction.options[Idx1] = null
   Idx1 = Idx1 - 1
  }
  
  if(newTest2.length != 0){								//Inicializa Menues Secundarios
   TotalOpt = forma.transaction1.options.length
   Idx1 = TotalOpt - 1
   while (Idx1 > -1) {
    forma.transaction1.options[Idx1] = null
    Idx1 = Idx1 - 1
   }
  }
  
  TotalOpt = forma.moneda.options.length				//Inicializa Monedas
  Idx1 = TotalOpt - 1
  while (Idx1 > -1) {
   forma.moneda.options[Idx1] = null
   Idx1 = Idx1 - 1
  }
  <% if(proceso[0].equals("03")|| proceso[0].equals("26") || isdepto.equals("S") )
	{ 
  %>
  
  TotalOpt = forma.moneda_s.options.length				//Inicializa Monedas Secundarias
  Idx1 = TotalOpt - 1
  while (Idx1 > -1) {
   forma.moneda_s.options[Idx1] = null
   Idx1 = Idx1 - 1
    }
  <%
  }
  %>

  for(Idx1 = 0, Idx2 = 0; Idx1 < newTest.length; Idx1++, Idx2++)
  {
// LA TRANSACCION TIENE EL MISMO O MENOR NIVEL OK
   if( parseInt(newTest[Idx1][4]) <= parseInt(forma.tllrlvl.value) )
   {
    forma.transaction.options[Idx2] = new Option();


    txnTmp = newTest[Idx1][1];
    if(txnTmp.indexOf('-')>-1){
      if(repetido(txnTmp.substring(0,4),forma)==false){
        eval("forma.transaction.options[Idx2].text='" + txnTmp.substring(0,4) + " - " + newTest[Idx1][3] + "'");
        eval("forma.transaction.options[Idx2].value='" + txnTmp.substring(0,4) + "'");
      }else{
        Idx2 = Idx2 - 1;
      }
    }else{
    eval("forma.transaction.options[Idx2].text='" + newTest[Idx1][1] + " - " + newTest[Idx1][3] + "'");
    eval("forma.transaction.options[Idx2].value='" + newTest[Idx1][1] + "'");
   }
  }
  }
  
  if(newTest2.length != 0)
  {
    for(Idx1 = 0, Idx2 = 0; Idx1 < newTest2.length; Idx1++, Idx2++)
    {
    forma.transaction1.options[Idx2] = new Option();
    eval("forma.transaction1.options[Idx2].text='" + newTest2[Idx1][4] + " (" + newTest2[Idx1][3] + ")'");
    eval("forma.transaction1.options[Idx2].value='" + newTest2[Idx1][3] + "'");
   }
  }
  
  for(Idx1 = 0, Idx2 = 0; Idx1 < newTest3.length; Idx1++, Idx2++)
  {
   forma.moneda.options[Idx2] = new Option();
   eval("forma.moneda.options[Idx2].text='" + newTest3[Idx1][1] + "'");
   eval("forma.moneda.options[Idx2].value='" + newTest3[Idx1][0] + "'");
  }
  
  <% if(proceso[0].equals("03") || proceso[0].equals("26") || isdepto.equals("S") )
	{ 
  %>  
  for(Idx1 = 0, Idx2 = 0; Idx1 < newTest3.length; Idx1++, Idx2++)
  {
      	forma.moneda_s.options[Idx2] = new Option();
   		eval("forma.moneda_s.options[Idx2].text='" + newTest3[Idx1][1] + "'");
   		eval("forma.moneda_s.options[Idx2].value='" + newTest3[Idx1][0] + "'");
  }
  <%
  }
  %>  
  
  
    <% if(proceso[0].equals("03"))
	{ 
  %>
  
  TotalOpt = forma.transaction.options.length			//Inicializa Menues Primarios
  Idx1 = TotalOpt - 1
  while (Idx1 > -1) {
   forma.transaction.options[Idx1] = null
   Idx1 = Idx1 - 1
  }
    if(newTest2.length != 0){								//Inicializa Menues Secundarios
   TotalOpt = forma.transaction1.options.length
   Idx1 = TotalOpt - 1
   while (Idx1 > -1) {
    forma.transaction1.options[Idx1] = null
    Idx1 = Idx1 - 1
   }
  }
  
    if(forma.transaction1.options.length == 0)
  {
   forma.transaction1.options[0] = new Option();
   eval("forma.transaction1.options[0].text='No Disponible '");
   eval("forma.transaction1.options[0].value='ND'");
  }
    
  <%
  }
  %>
  
  

  if(forma.transaction.options.length == 0)
  {
   forma.transaction.options[0] = new Option();
   eval("forma.transaction.options[0].text='No Disponible '");
   eval("forma.transaction.options[0].value='ND'");
  }
  forma.transaction.options[0].selected = true;
  if(newTest2.length != 0)
   forma.transaction1.options[0].selected = true;
  forma.moneda.options[0].selected = true;
  <% if(!proceso[0].equals("03"))
	{ 
  %>
  changeTxnsVSCurrency(forma)
  <%
  }
  %>
 }

 function changeTxnsVSCurrency(forma)
 {
  Pointer2 = 0;
  Pointer = forma.transaction.options[forma.transaction.selectedIndex].value
  TotalOpt = forma.transaction.options.length
  okFlag = 0
  Idx1 = TotalOpt - 1
  while (Idx1 > -1) {
   forma.transaction.options[Idx1] = null
   Idx1 = Idx1 - 1
  }
  if(forma.moneda.options[forma.moneda.selectedIndex].value == "")  //Selecciona moneda Nacional
   forma.moneda.options[forma.moneda.selectedIndex].value = "01"
   
  for(Idx1 = 0, Idx2 = 0; Idx1 < newTest.length; Idx1++)
  {
  <% if(proceso[0].equals("03"))
	{ 
  %>
  		if(newTest[Idx1][2] == forma.moneda_s.options[forma.moneda_s.selectedIndex].value &&
  <%
  }
  else
  {
  %>
  		if(newTest[Idx1][2] == forma.moneda.options[forma.moneda.selectedIndex].value &&
    <%
  }
  %>
       parseInt(newTest[Idx1][4]) <= parseInt(forma.tllrlvl.value))
    {
    forma.transaction.options[Idx2] = new Option();

    txnTmp = newTest[Idx1][1];
    if(txnTmp.indexOf('-')>-1){
      if(repetido(txnTmp.substring(0,4),forma)==false){
        eval("forma.transaction.options[Idx2].text='" + txnTmp.substring(0,4) + " - " + newTest[Idx1][3] + "'");
        eval("forma.transaction.options[Idx2].value='" + txnTmp.substring(0,4) + "'");
      }else{
        Idx2 = Idx2 - 1;
      }
    }else{
    eval("forma.transaction.options[Idx2].text='" + newTest[Idx1][1] + " - " + newTest[Idx1][3] + "'");
    eval("forma.transaction.options[Idx2].value='" + newTest[Idx1][1] + "'");
    }
      eval("forma.transaction.options[Idx2].id='" + newTest[Idx1][0] + "'"); 
    Idx2++
    okFlag = 1
   }
  }
  
  if(okFlag == 0)
  {
   forma.transaction.options[0] = new Option();
   eval("forma.transaction.options[0].text='No Disponible '");
   eval("forma.transaction.options[0].value='ND'");
  }
  forma.transaction.options[0].selected = true;
  searchPrevTxn(forma, Pointer)
  if(newTest2.length != 0)
  {
   Pointer2 = forma.transaction1.options[forma.transaction1.selectedIndex].value
     changeSecTxnsVSOrig(forma, Pointer2)
     if(forma.transaction.options[forma.transaction.length-1].text == "")
        forma.transaction.options[forma.transaction.length-1] = null;
  }  
  <% if(proceso[0].equals("03") || proceso[0].equals("26"))
	{ 
  %> 
  changeTxnsSecVSCurrency(forma)
  <%
  }
  %>
    <% if(proceso[0].equals("03"))
	{ 
  %> 
  	
		if(forma.transaction.options[0].value=='ND' || forma.transaction1.options[0].value=='ND')
		{
		
		  	  TotalOpt = forma.transaction.options.length			//Inicializa Menues Primarios
			  Idx1 = TotalOpt - 1
			  while (Idx1 > -1) {
			   forma.transaction.options[Idx1] = null
			   Idx1 = Idx1 - 1
			  }
			    if(newTest2.length != 0){								//Inicializa Menues Secundarios
			   TotalOpt = forma.transaction1.options.length
			   Idx1 = TotalOpt - 1
			   while (Idx1 > -1) {
			    forma.transaction1.options[Idx1] = null
			    Idx1 = Idx1 - 1
			   }
			  }
			  
			  if(forma.transaction.options.length == 0)
			  {
			   forma.transaction.options[0] = new Option();
			   eval("forma.transaction.options[0].text='No Disponible '");
			   eval("forma.transaction.options[0].value='ND'");
			  }
			  
			  if(forma.transaction1.options.length == 0)
			  {
			   forma.transaction1.options[0] = new Option();
			   eval("forma.transaction1.options[0].text='No Disponible '");
			   eval("forma.transaction1.options[0].value='ND'");
			  }
		
		}
		
		  finaliza_txns(document.Txns)
		
  <%
  }
  %>
  

  
 }
 
<% if(proceso[0].equals("03") || proceso[0].equals("26"))
{ 
%> 
    function changeTxnsSecVSCurrency(forma)
    {
        Pointer2 = 0;
        Pointer = forma.transaction1.options[forma.transaction1.selectedIndex].value
        Pointer3 = forma.transaction.options[forma.transaction.selectedIndex].value
	    TotalOpt = forma.transaction1.options.length
	    okFlag = 0
	    Idx1 = TotalOpt - 1
	    while (Idx1 > -1) {
	        forma.transaction1.options[Idx1] = null
	        Idx1 = Idx1 - 1
	    }
    
        for(Idx1 = 0, Idx2 = 0; Idx1 < newTest2.length; Idx1++)
        {  
            if(newTest2[Idx1][2] == forma.moneda_s.options[forma.moneda_s.selectedIndex].value &&
            parseInt(newTest2[Idx1][5]) <= parseInt(forma.tllrlvl.value)
            && newTest2[Idx1][1] == Pointer3 && searchTxnSec(newTest2[Idx1][1], newTest2[Idx1][3])
            //&& searchTxnSecCurrency(forma.transaction.options[forma.transaction.selectedIndex].value, newTest2[Idx1][3],forma.moneda_s.options[forma.moneda_s.selectedIndex].value)
            )
            {
                forma.transaction1.options[Idx2] = new Option();
			    txnTmp = newTest2[Idx1][3];
			    if(txnTmp.indexOf('-')>-1){
				    if(repetido(txnTmp.substring(0,4),forma)==false){
				        eval("forma.transaction1.options[Idx2].text='" + newTest2[Idx1][4] + " (" + txnTmp.substring(0,4) + ")'");
				        eval("forma.transaction1.options[Idx2].value='" + txnTmp.substring(0,4) + "'");
				    }else{
				        Idx2 = Idx2 - 1;
				    }
			    }else{
				    eval("forma.transaction1.options[Idx2].text='" + newTest2[Idx1][4] + " (" + newTest2[Idx1][3] + ")'");
				    eval("forma.transaction1.options[Idx2].value='" + newTest2[Idx1][3] + "'");
				}
			    eval("forma.transaction1.options[Idx2].id='" + newTest2[Idx1][0] + "'"); 
			    Idx2++
			    okFlag = 1
			}
		}
		if(okFlag == 0)
		{		
		   forma.transaction1.options[0] = new Option();
		   eval("forma.transaction1.options[0].text='No Disponible '");
		   eval("forma.transaction1.options[0].value='ND'");
		  
		   if(valmoneda > 1 && valmoneda_s > 1){
		       eval("forma.transaction.options[0].text='No Disponible '");
			   eval("forma.transaction.options[0].value='ND'");
		
			    eval("forma.transaction1.options[0].text='No Disponible '");
			   eval("forma.transaction1.options[0].value='ND'");
		   }
		   if(valmoneda == 1 && valmoneda_s == 1){
		      // eval("forma.transaction.options[0].text='No Disponible '");
			  // eval("forma.transaction.options[0].value='ND'");
			   eval("forma.transaction1.options[0].text='No Disponible '");
			   eval("forma.transaction1.options[0].value='ND'");
		   }
		  
		}
 
		if(okFlag == 1)
		{
		    var valmoneda = parseInt(forma.moneda.options[forma.moneda.selectedIndex].value,10);
		    var valmoneda_s = parseInt(forma.moneda_s.options[forma.moneda_s.selectedIndex].value,10);
		    if(valmoneda == 1 && valmoneda_s == 1){
		       eval("forma.transaction.options[0].text='No Disponible '");
			   eval("forma.transaction.options[0].value='ND'");
		
			   eval("forma.transaction1.options[0].text='No Disponible '");
			   eval("forma.transaction1.options[0].value='ND'");
		    }
		 	if(valmoneda > 1 && valmoneda_s > 1){
		       eval("forma.transaction.options[0].text='No Disponible '");
			   eval("forma.transaction.options[0].value='ND'");
			   eval("forma.transaction1.options[0].text='No Disponible '");
			   eval("forma.transaction1.options[0].value='ND'");
		    }
		    
		}  
  
  		forma.transaction1.options[0].selected = true;

		searchPrevTxnSec(forma, Pointer)
		
  
		if(newTest2.length != 0)
		{
		    Pointer2 = forma.transaction1.options[forma.transaction1.selectedIndex].value
		    if(forma.transaction1.options[forma.transaction1.selectedIndex].value!="ND"){
		    <% if(!proceso[0].equals("03"))
			{ 
			%> 
		        changeSecTxnsVSOrig(forma, Pointer2)
		    <%
			} 
			%> 
		        if(forma.transaction1.options[forma.transaction1.length-1].text == "")
		            forma.transaction1.options[forma.transaction1.length-1] = null;
		    }
		    //else{
		       //  Pointer2 = 0;
		    // }		     
		}

    }
<%
}
%>

function ocultar(){ 
document.getElementById("moneda_s").style.visibility="hidden";
}

function mostrar(){ 
document.getElementById("moneda_s").style.visibility="visible"; 
}

 function changeSecTxnsVSOrig(forma, Pointer2)
 {
  var txn_c = forma.transaction.options[forma.transaction.selectedIndex].value;
  //alert(forma.CV_deptos.value);
  if(newTest2.length < 1)
   return true;
  TotalOpt = forma.transaction1.options.length
  okFlag = 0
  Idx1 = TotalOpt - 1
  while (Idx1 > -1) {
   forma.transaction1.options[Idx1] = null
   Idx1 = Idx1 - 1
  }
  
  
  //alert(forma.transaction.options[forma.transaction.selectedIndex].value)
  <%
  if(isdepto != null && isdepto.equals("S")){
  %>
  	if(txn_c == "0726" || txn_c == "4043" || txn_c == "C726" || txn_c == "5183" || txn_c == "0278" || txn_c == "2201" || txn_c == "4525")
  		mostrar();
  	else
  		ocultar();
  <%
  }
  %>
  
  var countemp = 0; 
  var banpar = 0;
  for(Idx1 = 0, Idx2 = 0; Idx1 < newTest2.length; Idx1++){
   if(newTest2[Idx1][2] == forma.moneda.options[forma.moneda.selectedIndex].value &&
      newTest2[Idx1][1] == forma.transaction.options[forma.transaction.selectedIndex].value &&
      parseInt(newTest2[Idx1][5]) <= parseInt(forma.tllrlvl.value) &&
      ((isdepto != "S" && newTest2[Idx1][3] != "0226") || isdepto == "S"))
    {
    if(isPar(newTest2[Idx1][1])){
	        banpar = 1;
	      if(searchTxnSec(newTest2[Idx1][1], newTest2[Idx1][3])){
	        if(newTest2[Idx1][3] != "" || newTest2[Idx1][3] != null){
		        forma.transaction1.options[countemp] = new Option();
		        eval("forma.transaction1.options[countemp].text='" + newTest2[Idx1][4] + " (" + newTest2[Idx1][3] + ")'");
		        eval("forma.transaction1.options[countemp].value='" + newTest2[Idx1][3] + "'");       
		        countemp++;
		     }    
	      }
	    }else{
	      if(newTest2[Idx1][3] != "" || newTest2[Idx1][3] != null){
		      forma.transaction1.options[countemp] = new Option();
		      eval("forma.transaction1.options[countemp].text='" + newTest2[Idx1][4] + " (" + newTest2[Idx1][3] + ")'");
		      eval("forma.transaction1.options[countemp].value='" + newTest2[Idx1][3] + "'");
		      countemp++;
		  }    
	    }
   }
  }
  
  if(countemp > 0)
    okFlag = 1;  
    
  if(okFlag == 0){
   forma.transaction1.options[0] = new Option();
   eval("forma.transaction1.options[0].text='No Disponible '");
   eval("forma.transaction1.options[0].value='ND'");
   if (forma.transaction.length == 1  && banpar == 1){
     eval("forma.transaction.options[0].text='No Disponible '");
     eval("forma.transaction.options[0].value='ND'");
   }else{
        if (banpar == 1){
        forma.transaction.options[forma.transaction.selectedIndex] = null;
        }
   } 
  }
  forma.transaction1.options[0].selected = true;
  searchPrevTxn2(forma, Pointer2);
  
}


 function searchPrevTxn(forma, Pointer){
  for(Idx1 = 0; Idx1 < forma.transaction.options.length; Idx1++)
   if(forma.transaction.options[Idx1].value == Pointer){
    IdxPointer = Idx1
    break
   }
   else
    IdxPointer = 0
  forma.transaction.options[IdxPointer].selected = true;
 }
 
function searchPrevTxnSec(forma, Pointer){
  for(Idx1 = 0; Idx1 < forma.transaction1.options.length; Idx1++)
   if(forma.transaction1.options[Idx1].value == Pointer){
    IdxPointer = Idx1
    break
   }
   else
    IdxPointer = 0
  forma.transaction1.options[IdxPointer].selected = true;
 } 

function searchPrevTxn2(forma, Pointer2)
{
	for(Idx1 = 0; Idx1 < forma.transaction1.options.length; Idx1++)
	    if(forma.transaction1.options[Idx1].value == Pointer2)
	    {
	        IdxPointer = Idx1 ;
	        break ;
	    }
	    else
	        IdxPointer = 0 ;
	forma.transaction1.options[IdxPointer].selected = true;
}

 function enviar(){
 <%if(((String)session.getAttribute("isForeign")).equals("S") || session.getAttribute("tipomenu").equals("T"))
 {%>
   window.document.Txns.txnProcess.value = window.document.Txns.transaction.options[window.document.Txns.transaction.selectedIndex].id;
    if(window.document.Txns.transaction.options[window.document.Txns.transaction.selectedIndex].value == '5503')
    {
        window.document.Txns.noservicios.value='1';
 }

<%}%>
<%
if(proceso[0].equals("03") || proceso[0].equals("26"))
{
%>
if(document.Txns.transaction.options[document.Txns.transaction.selectedIndex].value == "0278"
	|| document.Txns.transaction.options[document.Txns.transaction.selectedIndex].value == "5183"){
	if((Txns.moneda.options[Txns.moneda.selectedIndex].value == Txns.moneda_s.options[Txns.moneda_s.selectedIndex].value)		
	|| (Txns.moneda.options[Txns.moneda.selectedIndex].value != "01" && Txns.moneda_s.options[Txns.moneda_s.selectedIndex].value != "01"))
	{
		alert("La combinaci�n de monedas es invalida");
	 	return;

	}
}
<%}%>

<%
if(proceso[0].equals("03"))
{
%>
if(document.Txns.transaction1.options[document.Txns.transaction1.selectedIndex].value == "ND")
   {
		alert("Combinaci�n NO DISPONIBLE");
	 	return;

	}
<%}%>

<%
if(isdepto != null && isdepto.equals("S"))
{
%>

	if(document.Txns.transaction.options[document.Txns.transaction.selectedIndex].value == "0278"
	|| document.Txns.transaction.options[document.Txns.transaction.selectedIndex].value == "5183"
	|| document.Txns.transaction.options[document.Txns.transaction.selectedIndex].value == "2201"
	|| document.Txns.transaction.options[document.Txns.transaction.selectedIndex].value == "4525"
	|| document.Txns.transaction.options[document.Txns.transaction.selectedIndex].value == "0726"
	|| document.Txns.transaction.options[document.Txns.transaction.selectedIndex].value == "4043"
	|| document.Txns.transaction.options[document.Txns.transaction.selectedIndex].value == "C726"){
	if((Txns.moneda.options[Txns.moneda.selectedIndex].value == Txns.moneda_s.options[Txns.moneda_s.selectedIndex].value)		
	|| (Txns.moneda.options[Txns.moneda.selectedIndex].value != "01" && Txns.moneda_s.options[Txns.moneda_s.selectedIndex].value != "01"))
		{
			alert("La combinaci�n de monedas es invalida");
 			return;

		}
	}
<%}%>

   window.document.Txns.submit();
     var obj = document.getElementById('btnCont')
     obj.onclick = new Function('return false')
 }
</SCRIPT>
<form name="Txns" action="../servlet/ventanilla.PageServlet" method="post">
<input type="hidden" name="transaction2" value="0000">
<table>
<tr>
<td colspan="3">
  <select name="transaction" size="1" onChange="changeSecTxnsVSOrig(document.Txns)">
  <option value="0001">Un valor de inicio para transacciones de Entrada
  <option value="0002">Un valor de inicio para transacciones de Entrada
  <option value="0003">Un valor de inicio para transacciones de Entrada
  <option value="0004">Un valor de inicio para transacciones de Entrada
  <option value="0005">Un valor de inicio para transacciones de Entrada
  <option value="0006">Un valor de inicio para transacciones de Entrada
  <option value="0007">Un valor de inicio para transacciones de Entrada
  <option value="0008">Un valor de inicio para transacciones de Entrada
  <option value="0009">Un valor de inicio para transacciones de Entrada
  <option value="0010">Un valor de inicio para transacciones de Entrada
 </select>
</td>
<%
 if(menuTwo.size() != 0){
  out.print("<td colspan=\"3\">");
  out.print("<select name=\"transaction1\" size=\"1\">");
  out.print("<option value=\"0001\">Un valor de inicio para transaccion de Salida");
  out.print("<option value=\"0002\">Un valor de inicio para transaccion de Salida");
  out.print("<option value=\"0003\">Un valor de inicio para transaccion de Salida");
  out.print("<option value=\"0004\">Un valor de inicio para transaccion de Salida");
  out.print("<option value=\"0005\">Un valor de inicio para transaccion de Salida");
  out.print("</select>");
  out.print("</td>");
 }
%>
<%
// Proceso RAP --- AGC

if(proceso[0].equals("08"))
{
  out.print("<td>&nbsp;&nbsp;</td>");
  out.print("<td>No. de Servicios :</td>");
  out.print("<td>&nbsp;&nbsp;</td>");
  out.print("<td>");
  out.print("<select name=\"noservicios\" size=\"1\">");
  out.print("<option value=\"1\">1");
  //out.print("<option value=\"2\">2");
  //out.print("<option value=\"3\">3");
  //out.print("<option value=\"4\">4");
  out.print("</select>");
  out.print("</td>");
}%>
</tr>
</table>
<table>
<tr>
  <td width="25">Moneda:</td>
  <td width="10">&nbsp;</td>
  <td>
  <select name="moneda" size="1" onChange="changeTxnsVSCurrency(document.Txns)">
  <option value="0001">Valor inicio moneda
  <option value="0002">Valor inicio moneda
  <option value="0003">Valor inicio moneda
  </select>
  </td>
  <%
  
// Proceso Compra-Venta Panam� --- Humberto Balleza Mej�a
String[] proceso_s = request.getParameterValues("list");
if(proceso[0].equals("03") || proceso[0].equals("26"))
{
  out.print("<td width=\"25\">Moneda:</td>");
  out.print("<td width=\"10\">&nbsp;</td>");
  out.print("<td>");
  out.print("<select name=\"moneda_s\" size=\"1\" onChange=\"changeTxnsVSCurrency(document.Txns)\">");
  out.print("<option value=\"1\">Valor inicio moneda");
  out.print("<option value=\"2\">Valor inicio moneda");
  out.print("<option value=\"3\">Valor inicio moneda");
  out.print("</select>");
  out.print("</td>");
}
if(isdepto != null && isdepto.equals("S"))
{
  out.print("<td width=\"25\">Moneda:</td>");
  out.print("<td width=\"10\">&nbsp;</td>");
  out.print("<td>");
  out.print("<select name=\"moneda_s\" size=\"1\" onChange=\"changeTxnsVSCurrency(document.Txns)\">");
  out.print("<option value=\"1\">Valor inicio moneda");
  out.print("<option value=\"2\">Valor inicio moneda");
  out.print("<option value=\"3\">Valor inicio moneda");
  out.print("</select>");
  out.print("</td>");
}
%>
</tr>
</table>
<input type="hidden" name="tllrlvl" value="<%=session.getAttribute("tellerlevel")%>">
<input type="hidden" name="txnProcess" value="">
<input type="hidden" name="noservicios">
<input type="hidden" name="CV_deptos">
 <p><a id='btnCont' href="JavaScript:enviar()"><img src="../ventanilla/imagenes/b_aceptar.gif" border="0"></a>
</form>
<script> 
<%
if(isdepto != null && isdepto.equals("S"))
{
%>
	document.getElementById("moneda_s").style.visibility="hidden"; 
<%
}
%>
</script> 
</body>
</html>
