<!--
//*************************************************************************************************
//             Funcion: JSP que presenta las opciones de la txn 0814
//            Elemento: PageBuilder0814.jsp
//          Creado por: Alejandro Gonzalez Castro
//      Modificado por: Fausto Rodrigo Flores Moreno
//      Modificado por: Carolina Vela
//*************************************************************************************************
// CCN - 4360275 - 18/02/2005 - WAS 5.1
// CCN - 4360522 - 06/10/2006 - Se hacen modificaciones para eliminar la opcion de Otros Bancos
// CCN - 4360581 - 12/03/2007 - Se convierten los estilos en jsf y se hace un include en el jsp
// CCN - 4360584 - 23/03/2007 - Modificaciones para proyecto OPMN
// CCN - 4620010 - 19/09/2007 - Se hacen modificaciones para tropicalizacion
// CCN - 4620021 - 17/10/2007 - Se hacen validaciones para montos mayores a 10000.00 dlls, Humberto Balleza
// CCN - 4620024 - 24/10/2007 - Modificacione spara el campo de IVA.
//*************************************************************************************************
-->
 
<%@page language= "java" import="java.util.*,ventanilla.com.bital.beans.DatosVarios,ventanilla.com.bital.beans.Menues" contentType="text/html"%>
<%
response.setHeader("Cache-Control","no-store"); //HTTP 1.1
response.setHeader("Pragma","no-cache"); //HTTP 1.0
response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<%
Vector listaCampos = (Vector)session.getAttribute("page.listcampos");
Vector listaContenidos = (Vector)session.getAttribute("page.listcont");
String txnAuthLevel = (String)session.getAttribute("page.txnAuthLevel");
String txnIMGAutoriz = (String)session.getAttribute("page.txnIMGAutoriz");
Vector v = (Vector)session.getAttribute("page.specialacct");
String MontoPro = new String("");
int k;
%>
<html>
<head>
  <%@include file="style.jsf"%>
  <title>Datos de la Transaccion</title>
<script language="JavaScript">
<!--
var formName = 'Mio';
var specialacct = new Array();
var bandera = 0;
<%
if(!v.isEmpty())
{
  for(int iv = 0; iv < v.size(); iv++)
  {
    out.print("specialacct[" + iv + "]='" + v.get(iv) + "'" + '\n');
  }
}
%>

function validate(form)
{
  if( !top.validate(window, document.entry.lstForPago, 'wichForPago') )
    return;
<%
  for(int i=0; i < listaCampos.size(); i++)
  {
    Vector vCampos = (Vector)listaCampos.get(i);
    vCampos = (Vector)vCampos.get(0);
    
    if( vCampos.get(4).toString().length() > 0)
    {
      out.println("  if( !top.validate(window, document.entry." + vCampos.get(0) + ", '" + vCampos.get(4) +"') )");
      out.println("    return;");
    }
  }
%>

  form.validateFLD.value = '1';
   if (form.cTxn.value == "0814")
  {
  	 iMonto = document.entry.txtMonto.value
     iMonto = iMonto.toString().replace(/\$|\,/g,'');   
     intVal = iMonto.substring(0, iMonto.indexOf("."));
     decVal = iMonto.substring(iMonto.indexOf(".") + 1, iMonto.length);   
     Monto = Number(intVal + decVal);
     if ( Monto >= 1000000 && form.lstForPago.selectedIndex == 0) 
         bandera = 1;
     else
     {
         bandera = 0;
         entry.needAutho.value='0';
     }

   }

//Inicia Validaciones monto
<%
DatosVarios montos = DatosVarios.getInstance();                   
Vector vMontos = new Vector();        
vMontos =(Vector)montos.getDatosV("MontosOPMN");   
%>
  iMonto = document.entry.txtMonto.value
  iMonto = iMonto.toString().replace(/\$|\,/g,'');   
  intVal = iMonto.substring(0, iMonto.indexOf("."));
  decVal = iMonto.substring(iMonto.indexOf(".") + 1, iMonto.length);   
  Monto = Number(intVal + decVal);
  
  //if(Monto > <%=Integer.valueOf(vMontos.get(0).toString()) %> && document.entry.lstForPago.value == "01"){
  //  alert("Monto maximo excedido");
  //  return;
  //}
  //if(Monto > <%=Integer.valueOf(vMontos.get(1).toString()) %> && document.entry.lstForPago.value == "02"){
  //  alert("Monto maximo excedido");
  //  return;
  //}    
    
//Validar nombres de ordenante y beneficiario 
  if (document.entry.txtFechaEfect)
    document.entry.txtFechaSys.value=top.sysdatef();
    
  if (document.entry.txtFechaError)
    document.entry.txtFechaSys.value=top.sysdatef();    
/*Inician Validaciones con Comprobante Fiscal y Comprobante de IVA o COM
  var importe = form.txtImporteIVA.value.toString().replace(/\$|\,/g,'');
  var intVal = importe.substring(0, importe.indexOf("."));
  var decVal = importe.substring(importe.indexOf(".") + 1, importe.length);
  importe = intVal + decVal;
  
  if (form.lstdeIVACOM.selectedIndex == 1 && form.lstCF.selectedIndex == 1)
  {
    alert("Solo puede seleccionar un solo comprobante");
    return;
  }
  
  if (form.lstForPago.selectedIndex == 0) //Efectivo
  {
    if (form.lstCF.selectedIndex == 1)
    {
      alert("No puede seleccionar Comprobante Fiscal con Pago en Efectivo");
      return;
    }
    
    if (form.lstdeIVACOM.selectedIndex == 1 && (form.txtNombre.value.length == 0 || form.txtRFC01.value.length == 0 || form.txtDomici.value.length == 0) )
    {
      alert("Nombre, R.F.C. y Domicilio son datos requeridos...");
      return;
    }
    
    if (form.lstdeIVACOM.selectedIndex == 1 && importe > 0)
      form.txtImporteIVA.value = "";
  } else {
    if (form.lstdeIVACOM.selectedIndex == 1)
    {
      alert("No puede seleccionar Comprobante de IVA y COM con Cargo a Cuenta");
      return;
    }
    if (form.lstCF.selectedIndex == 1 && (form.txtRFC01.value.length == 0  || importe == 0) )
    {
      alert("R.F.C. y el Importe del IVA son datos requeridos...");
      return;
    } 
  }
*/
  if ( bandera == 1 ) {                       // Pedir autorizacion
        form.onSubmit="return validate(document.entry);"
        document.entry.needAutho.value = '1';
        AuthoandSignature(document.entry);
  }

  if ( bandera == 0) {
 <%
 if(!txnAuthLevel.equals("1") && !txnIMGAutoriz.equals("1")){
    out.println("  form.submit()");
    out.println("var obj = document.getElementById('btnCont');");
    out.println("obj.onclick = new Function('return false');");
    }
    
%> }

//<%
//if(!txnAuthLevel.equals("1") && !txnIMGAutoriz.equals("1"))
//{
//    out.println("  form.submit();");
//    out.println("var obj = document.getElementById('btnCont');"); 
//    out.println("obj.onclick = new Function('return false');");   
//}   
//%>
}


function AuthoandSignature(entry)
{
  var Cuenta;
  
  if(entry.validateFLD.value != '1')
    return;
    
  if(entry.needAutho.value == '1' && entry.AuthoOK.value != '1')
  {
    top.openDialog('../ventanilla/paginas/authoriz.jsp', 220, 130, 'top.setPrefs()', entry);
  }
  
  if(entry.needSignature.value == '1' && entry.SignField.value != '')
  {
    Cuenta = eval('entry.' + entry.SignField.value + '.value');
    top.openModalDialog('<%=request.getContextPath()%>/servlet/ventanilla.FirmasServlet?txtDDACuenta=' + Cuenta, 710, 430, 'top.setPrefs2()', entry);
  }
}

function Alltrim(inputStr)
{
  var inStr = new String(inputStr);

  while(inStr.charAt(0) == ' ')
    inStr = inStr.substring(1, inStr.length);

  while(inStr.charAt(inStr.length) == ' ')
    inStr = inStr.substring(0, inStr.length-1);

  return inStr;
}

//-->
</script>
</head>
<body bgcolor="#FFFFFF" onload="top.setFieldFocus(window.document)">
<%
out.print("<form name=\"entry\" action=\"../servlet/ventanilla.DataServlet\" method=\"post\" ");

if(txnAuthLevel.equals("1"))
  out.print("onSubmit=\"return validate(this)\"");
  
out.println(">");
%>

<h1><%=session.getAttribute("page.txnImage")%></h1>

<h3>
<%
out.print(session.getAttribute("page.cTxn") + "  " + (String)session.getAttribute("page.txnlabel"));
%>
</h3>
<table border="0" cellspacing="0">
<%
  int i = 0;
  for(i = 0; i < listaCampos.size(); i++)
  {

    if(i == 3)//if(i == 2)
    {
      out.print("<tr>");
      out.print(" <td>");
      out.print("  Para ser liquidada en:");
      out.print(" </td>");
      out.print(" <td>&nbsp</td>");
      out.print(" <td>");
      out.print("  "+(String)session.getAttribute("identidadApp"));
      out.print(" </td>");
      out.println("</tr>");
      out.print("<tr>");
      out.print(" <td>");
      out.print("  <i><b>Ordenante</b></i>");
      out.print(" </td>");
      out.println("</tr>");
    } else if( i == 5) {
      out.print("<tr>");
      out.print(" <td>");
      out.print("  <i><b>Beneficiario</b></i>");
      out.print(" </td>");
      out.println("</tr>");
    } else if (i == 6){ 
      out.print("<tr>");
      out.print(" <td colspan = \"3\">");
     // out.print("  <i><b>Llenar solo en caso de requerir comprobante de COM e IVA</b></i>");
      out.print(" </td>");
      out.println("</tr>");
%>
  <!--    <tr>
	<td>Requiere Comprobante de COM e IVA:</td>
  	<td width="10">&nbsp;</td>
  	<td><select name="lstdeIVACOM" size="1" tabindex="<% out.print(i + 1); %>">
  	<option value="0">NO
  	<option value="1">SI
  	</select></td>
	</tr>
	<tr>
	<td>Requiere Comprobante Fiscal:</td>
  	<td width="10">&nbsp;</td>
  	<td><select name="lstCF" size="1" tabindex="<% out.print(i + 1); %>">
  	<option value="0">NO
  	<option value="1">SI
  	</select></td>
      </tr>
     -->
<%
    }
    
    Vector vCampos = (Vector)listaCampos.get(i);
    vCampos = (Vector)vCampos.get(0);
    if(!vCampos.get(0).toString().equals("txtCodSeg"))
    {
      out.println("<tr>");
      out.println("  <td>" + vCampos.get(2) + ":</td>");
      out.println("  <td width=\"10\">&nbsp;</td>");
      
      if(!vCampos.get(0).toString().substring(0,3).equals("lst"))
      {
        out.print  ("  <td><input maxlength=\"" + vCampos.get(3) + "\" name=\"" + vCampos.get(0) + "\" " + 
                    " size=\"" + vCampos.get(3) +"\" type=\"text\" tabindex=\"" + (i + 1) + "\" ");
        String CampoR = (String)vCampos.get(5);
        
        if(CampoR != null && CampoR.length() > 0)
        {
          CampoR = CampoR.trim();
          out.print(" value=\"" + CampoR + "\"");
        }
      } else {
        out.print("  <td><select name=\"" + vCampos.get(0) + "\" size=\"" + vCampos.get(2) + "\" " + 
                  " tabindex=\"" + (i + 1) + "\" ");
                  
        if( vCampos.get(4).toString().length() > 0 )
          out.println(" onChange=\"top.validate(window, this, '" + vCampos.get(4) +"')\">");
        else
          out.println(">");
          
        for(k=0; k < listaContenidos.size(); k++)
        {
          Vector v1Campos = (Vector)listaContenidos.get(k);
          
          for(int j = 0; j < v1Campos.size(); j++)
          {
            Vector v1Camposa = (Vector)v1Campos.get(j);
            
            if(v1Camposa.get(0).toString().equals(vCampos.get(0)))
              out.println("  <option value=\"" + v1Camposa.get(3) + "\">" + v1Camposa.get(2));
          }
        }
        
        out.println("  </select></td>");
      }
     
      if( vCampos.get(4).toString().length() > 0 && !vCampos.get(0).toString().substring(0,3).equals("lst"))
      	if (vCampos.get(4).toString().equals("isEmptyStr"))
          out.println(" onChange=\"top.validate(window, this, '" + vCampos.get(4) +"')\"  " + 
                      " onKeyPress=\"top.keyPressedHandler(window, this, '" + vCampos.get(4) + "')\"></td>");
	else      
          out.println(" onBlur=\"top.estaVacio(this)||top.validate(window, this, '" + vCampos.get(4) +"')\"  " + 
                      " onKeyPress=\"top.keyPressedHandler(window, this, '" + vCampos.get(4) + "')\"></td>");
      else if(!vCampos.get(0).toString().substring(0,3).equals("lst"))
        out.println(">");
        
      out.println("</tr>");
    } else {
      i += 3 ;
    }
  }
%>
</table>
<input type="hidden" name="iTxn" value="<%= session.getAttribute("page.iTxn") %>">
<input type="hidden" name="oTxn" value="<%= session.getAttribute("page.oTxn") %>">
<input type="hidden" name="cTxn" value="<%= session.getAttribute("page.cTxn") %>">
<input type="hidden" name="sucursal" value="<%= session.getAttribute("branch") %>">
<input type="hidden" name="teller" value="<%= session.getAttribute("teller") %>">
<input type="hidden" name="moneda" value="<%= session.getAttribute("page.moneda")%>">
<input type="hidden" name="registro" value="<%=session.getAttribute("empno")%>">
<input type="hidden" name="tllrlvl" value="<%=session.getAttribute("tellerlevel")%>">
<input type="hidden" name="supervisor" value="<%= session.getAttribute("teller") %>">
<input type="hidden" value="<%= session.getAttribute("teller") %>">
<input type="hidden" name="AuthoOK" value="0">
<input type="hidden" name="SignOK" value="0">
<input type="hidden" name="validateFLD" value="0">
<input type="hidden" name="needSignature" value="<%out.print(txnIMGAutoriz);%>">
<input type="hidden" name="needAutho" value="<%out.print(txnAuthLevel);%>">
<input type="hidden" name="SignField" value="<%= session.getAttribute("page.SignField") %>">
<p>
<script language="JavaScript">
if (document.entry.txtFechaEfect)
{
  document.writeln("<input type=\"hidden\" name=\"txtFechaSys\">");
  document.entry.txtFechaEfect.value = top.sysdate();
}

if (document.entry.txtFechaError)
{
  document.writeln("<input type=\"hidden\" name=\"txtFechaSys\">");
  document.entry.txtFechaError.value = top.sysdate();
}
</script>
<%
if(txnAuthLevel.equals("1") || txnIMGAutoriz.equals("1"))
  out.println("<a id='btnCont' tabindex=\"" + (i + 1) + "\" href=\"javascript:validate(document.entry);javascript:AuthoandSignature(document.entry)\"><img src=\"../ventanilla/imagenes/b_continuar.gif\" border=\"0\"></a>");
else
  out.println("<a id='btnCont' tabindex=\"" + (i + 1) + "\" href=\"javascript:validate(document.entry)\"><img src=\"../ventanilla/imagenes/b_aceptar.gif\" border=\"0\"></a>");
%>
</form>
</body>
</html>
