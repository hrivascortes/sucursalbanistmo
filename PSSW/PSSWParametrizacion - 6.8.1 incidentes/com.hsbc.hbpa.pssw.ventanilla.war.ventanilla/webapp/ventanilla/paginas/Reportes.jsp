<!--
//*************************************************************************************************
//             Funcion: JSP para la seleccion de Reportes a presentar del Gerente
/             Elemento: Reportes.jsp
//          Creado por: Juvenal R. Fernandez Varela
//      Modificado por: Jesús Emmanuel López Rosales
//*************************************************************************************************
// CCN - 4360364 - 19/08/2005 - Se crea elemento para mostrar opciones del reporte
// CCN - 4360456 - 07/04/2006 - Se agraga código para reportes de gerente por sucursal y cajero de reversos y
//       			autorizaciones.
// CCN - 4360510 - 08/09/2006 - Agrega opcion en combo para seleccionar reporte de administracion de usuarios
// CCN - 4360581 - 12/03/2007 - Se convierten los estilos en jsf y se hace un include en el jsp
//*************************************************************************************************
-->

<%@ page session="true" import="java.util.*"%>
<%
response.setHeader("Cache-Control","no-store"); //HTTP 1.1
response.setHeader("Pragma","no-cache"); //HTTP 1.0
response.setDateHeader ("Expires", 0); //prevents caching at the proxy server

ventanilla.com.bital.admin.DiarioQuery DiarioSQL = new ventanilla.com.bital.admin.DiarioQuery();
String suc = (String)session.getAttribute("branch");
    suc = suc.substring(1);
    DiarioSQL.getTellers(suc, false);
Vector cajeros = DiarioSQL.getIDTellers();
Vector nombres = DiarioSQL.getNameTellers();
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
	<head>
		<%@include file="style.jsf"%>
		<title>Reportes de Gerente</title>
	</head>
	<Script language="JavaScript">
		function sendservlet()
		{		
			var Cajeros = document.entry.cajeros ;
			var indexc = Cajeros.selectedIndex ;
			var i=0
//********************Modifiacación para Reportes por Cajero y Sucursal			
			var BLista = document.entry.busqueda ;
			var index = BLista.selectedIndex ; 
			document.entry.TipoBusqueda.value = BLista.options[index].value;
            
            if(BLista.options[index].value=="S")
  			  document.entry.cajero.value = '<%=suc%>';
  			else
			document.entry.cajero.value = Cajeros.options[indexc].value;
//********************  FIN			
			var CLista = document.entry.opcion ;
			var index = CLista.selectedIndex ; 
			document.entry.TipoRep.value = CLista.options[index].value;
			
			if(CLista.options[index].value == 1)
				document.entry.action = "PageBuilderDiario.jsp";
		
			document.entry.submit();
		}
//****************Modificación para Reportes por Cajero y Sucrusal FFM inicio: 27/02/2006  fin: ?

		function cicloCajeros(){
		  var lstCajero = document.entry.cajeros;
		  lstCajero=lstRemove(lstCajero)
          <%for(int k=0; k<cajeros.size(); k++){
 		      out.println("lstCajero.options["+k+"]=new Option('"+cajeros.get(k)+" - "+nombres.get(k)+"','"+cajeros.get(k)+"')");
		  }%>
		}
		
		function lstRemove(lst){
          var i=0
          for(i=0;i<lst.length;i++)
            lst.remove(i)
          return lst;
        }  
      
        function habilitar(){
          var lstOpcion = document.entry.opcion;
          var index = lstOpcion.selectedIndex;
          var Lista = document.entry.busqueda;
          var lstCajero = document.entry.cajeros;
          var i=0
                   
          if(lstOpcion.options[index].value=="2" || lstOpcion.options[index].value=="3" || lstOpcion.options[index].value=="4"){
            Lista=lstRemove(Lista);
            Lista.options[0]=new Option("Cajero","C");
            Lista.options[1]=new Option("Sucursal","S");
            if(lstCajero.length==1){
              cicloCajeros()
            }
          }else{
            if(lstCajero.length==1){
              cicloCajeros()
            }
            if(Lista.length==2){
              Lista=lstRemove(Lista);
              Lista.options[0]=new Option("No Disponible","0");
            }
          }
        }
        
        function viewCajeros(){
          var lstSearch = document.entry.busqueda;
          var index = lstSearch.selectedIndex;
          var lstCajero = document.entry.cajeros;
          var i=0 
          
          if(lstSearch.options[index].value=="S"){
            while(lstCajero.length!=0){
              for(i=0;i<lstCajero.length;i++)
                lstCajero.remove(i)
            }
            lstCajero.options[0]=new Option("No Disponible","0");
          }else{
            cicloCajeros() 
          }          
        }
//*****************Fin de Modificación
   </SCRIPT>	
	<body bgcolor="#FFFFFF" onload="top.setFieldFocus(window.document)">   
		<form name="entry" action="../../servlet/ventanilla.Reportes">
			<b>Reportes</b><br><br>
			<table border="0" cellspacing="0">
			<tr>
         	<td><b>Seleccione : </b></td>
		   	<td>
<!-- ******************************* Modificacion para Reversos y Autorización  FFM -->
		   	<SELECT name="opcion" size="1" onChange="habilitar();">
		   			<OPTION value=1>Diario Electr&oacute;nico</OPTION>
		   			<OPTION value=2>Reporte Reversos</OPTION>
		   			<OPTION value=3>Reporte Autorizaciones</OPTION>		   			
		   			<OPTION value=4>Reporte Administración de Usuarios</OPTION>		   			
			</SELECT>
		   	</td>
		   	</tr>
			<tr>
			    <td><b>Mostrar por : </b></td>
			    <td>
			       <SELECT name="busqueda" size="1" onChange="viewCajeros();">
   		   			  <OPTION value="0">No Disponible</OPTION>
 			       </SELECT>
			    </td>
			 </tr>
			 <tr>
<!-- ******************************* FIN -->
			     <td><b>Cajero :</b></td>
				 <td>
			        <select name="cajeros">
			<%          for(int i=0; i<cajeros.size(); i++)
            			{%>
 			               <option value="<%=cajeros.get(i)%>"><%=cajeros.get(i)%> - <%=nombres.get(i)%></option>
			<%          }%>
			        </select>
			    </td>
			</tr>
		   	<tr>
		 	  	<td>
					<a href="JavaScript:sendservlet()"><img src="../imagenes/b_aceptar.gif" border="0"></a>
				</td>
			</tr>
		</table>
		<input type="hidden" name="TipoRep">		
    	<input type="hidden" name="TipoBusqueda">
		<input type="hidden" name="cajero">
		</form>
	</body>
</html>
