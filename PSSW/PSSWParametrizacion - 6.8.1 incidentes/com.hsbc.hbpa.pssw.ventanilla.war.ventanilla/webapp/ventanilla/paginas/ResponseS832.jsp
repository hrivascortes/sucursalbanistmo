<!-- 175 -->

<%@ page session="true" import="java.util.*"%>
<%
response.setHeader("Cache-Control","no-store"); //HTTP 1.1
response.setHeader("Pragma","no-cache"); //HTTP 1.0
response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<%!
  private String getString(String s)
  {
   int len = s.length();
   for(; len>0; len--)
    if(s.charAt(len-1) != ' ')
     break;
   return s.substring(0, len);
  }

  private String getItem(String newCadNum, int newOption)
  {
   int iPIndex = newCadNum.indexOf("CONSECUTIVO:") + 12, NumSaltos, Num = 0, Ban = 0;
   String nCadNum = new String("");
   NumSaltos = newOption;
   while(Num < NumSaltos)
   {
    if(newCadNum.charAt(iPIndex) == 0x20)
    {
     while(newCadNum.charAt(iPIndex) == 0x20)
      iPIndex++;
     Num++;
    }
    else
     while(newCadNum.charAt(iPIndex) != 0x20)
      iPIndex++;
   }
   while(newCadNum.charAt(iPIndex) != 0x20 && iPIndex < newCadNum.length()){
    nCadNum = nCadNum + newCadNum.charAt(iPIndex);
    if(iPIndex < newCadNum.length())
      iPIndex++;
    if(iPIndex == newCadNum.length())
      break;
   }

   return nCadNum;
  }
%>
<%
Vector resp = (Vector)session.getAttribute("response");
%>

<html>
<head>
  <title>Certificación</title>
<style type="text/css">Body {font-family: Arial, Helv; font-size: 10pt; font-weight: bold; color:#000000}</style>
</head>
<body bgcolor="#FFFFFF" onload="top.setFieldFocus(window.document)">
<p>
<%
String szResp = "";
String codigo = "";
String pcdOK = "";
String remesas = "";
String dirsuc = "";
int size = 0;
int pini = 0;
int ns = 0;
int i = 0;
int x = 0;
int z = 0;
Vector pcdcode = new Vector();
Vector code1 =  new Vector();
Vector code2 =  new Vector();
Vector data  = new Vector();
Hashtable datasession = (Hashtable)session.getAttribute("page.datasession");

Stack flujotxn = new java.util.Stack();
if(session.getAttribute("page.flujotxn") != null)
	flujotxn = (Stack)session.getAttribute("page.flujotxn");
if( resp.size() != 4 )
	out.println("<b>Error de conexi&oacute;n</b><br>");
else
{
	try
	{
    	codigo = (String)resp.elementAt(0);
   	 	if( !codigo.equals("0") )
    		out.println("<b>Transaccion Rechazada</b><br><br>");
      	int lines = Integer.parseInt((String)resp.elementAt(1));
    	szResp = (String)resp.elementAt(3);
    	for(i=0; i<lines; i++)
		{
    		String line = szResp.substring( i*78, Math.min(i*78+77, szResp.length()) );
     		out.println(getString(line) + "<p>");
     		if(Math.min(i*78+77, szResp.length()) == szResp.length())
      			break;
    	}
  	}
  	catch(NumberFormatException nfe)
	{
		out.println("<b>Error de conexi&oacute;n</b><br>");
	}

	if(codigo.equals("0"))
	{
		// NUMERO DE SERVICIOS CAPTURADOS
		String nos = (String)datasession.get("nservicios");
		ns = Integer.parseInt(nos);

		// OBTENCION DE CODIGOS DE PCD - 4 SERVICIOS
	    pcdcode.addElement(szResp.substring(156,157));
	    pcdcode.addElement(szResp.substring(169,170));
	    pcdcode.addElement(szResp.substring(182,183));
	    pcdcode.addElement(szResp.substring(234,235));

		// ADICION DE LA DIRECCION DE LA SUCURSAL PARA COMPROBANTE DE COBRO DE COMISION
		if(szResp.length() > 468)
			dirsuc = szResp.substring(468, szResp.length());

		// SERVICIO NO EXISTE - CODIGO 3
		for(x=0; x<ns; x++)
		{
			if ((pcdcode.elementAt(x).toString()).equals("3"))
			{
				pcdOK = "3";
//				out.println("<br>algun codigo igual a TRES");
				break;
			}
		}

		if(!pcdOK.equals("3"))
		{
			// SERVICIO EN AMBAS PCD'S - CODIGO 4
			for(x=0; x<ns; x++)
			{
				if ((pcdcode.elementAt(x).toString()).equals("4"))
				{
					pcdOK = "4";
//					out.println("<br>algun codigo igual a CUATRO");
					break;
				}
			}
		}

		if(!pcdOK.equals("3") && !pcdOK.equals("4"))
		{
			// SERVICIOS 1 Y 2
			for(x=0; x<ns; x++)
			{
				if((pcdcode.elementAt(x).toString()).equals("1"))
					code1.addElement(Integer.toString(x));
				else
					code2.addElement(Integer.toString(x));
			}
			// SERVICIOS 1 Y 2
			if(code2.size() > 0 && code1.size() > 0)
				pcdOK = "7";
			// SERVICIOS 1
			if (code1.size() == ns)
			{
				pcdOK = "1";
				data.addElement(pcdOK);
			}
			// SERVICIOS 2
			if (code2.size() == ns)
			{
				pcdOK = "2";
				data.addElement(pcdOK);
			}
		}

		if(pcdOK.equals("2"))
		{
			//SERVICIOS 2 - PCD 54820
//			out.println(" codigo(s) igual a a dos y / o  ceros");

			for(x=0; x<ns; x++)
			{
				// OBTENCION DE ATRIBUTOS DEL SERVICIO
				// LONGITUDES DE REFERENCIA 1 - 2 - 3
				if(x==0){pini = 156;}
				if(x==1){pini = 169;}
				if(x==2){pini = 182;}
				if(x==3){pini = 234;}
				data.addElement(szResp.substring(pini+1,pini+3));
				data.addElement(szResp.substring(pini+3,pini+5));
				data.addElement(szResp.substring(pini+5,pini+7));

				// FORMAS DE PAGO
				String dato = "";
				int NNN = 0;
				for (z=7; z<10; z++)
				{
					dato = szResp.substring(pini+z,pini+(z+1));
					if (dato.equals("S") || dato.equals("N"))
					{
						data.addElement(dato);
						if(dato.equals("N"))
							NNN = NNN + 1;
					}
					else
						data.addElement("S");
				}
				// VALIDACION DE FORMA DE PAGO -NNN-
				if (NNN == 3)
				{
					pcdOK = "8";
					break;
				}

				// VALIDACION DE LONGS REFERENCIAS Y COMPROBANTE DE PAGO
				for (z=10; z<12; z++)
				{
					dato = szResp.substring(pini+z,pini+(z+1));
					if (dato.equals("S") || dato.equals("N"))
						data.addElement(dato);
					else
						data.addElement("N");
				}

				// OBTENCION DE COMISION E IVA DE COMISION
				if(x==0){pini = 312;}
				if(x==1){pini = 324;}
				if(x==2){pini = 336;}
				if(x==3){pini = 390;}
				data.addElement(szResp.substring(pini,pini+9));
				data.addElement(szResp.substring(pini+9,pini+11));

				// VALIDACION DE REMESAS UN SERVICIO UN PAGO - OK
				int k = 10 * x;
//				out.println("<br>bital ___ : "+(k+4)+"+++" + data.elementAt(k+4).toString() + "+++");
//				out.println("<br>ci ______ : "+(k+5)+"+++" + data.elementAt(k+5).toString() + "+++");
//				out.println("<br>remesas _ : "+(k+6)+"+++" + data.elementAt(k+6).toString() + "+++");

				if( (data.elementAt(k+6).toString()).equals("S") &&
					(data.elementAt(k+4).toString()).equals("N") &&
			    	(data.elementAt(k+5).toString()).equals("N") )
				{
					String np = (String)datasession.get("nopagos"+(x+1));
					int npagos = Integer.parseInt(np);
//					out.println("<br>no pagos " + npagos);
					if (npagos != 1)
						pcdOK = "5";
					else
					{
						if(ns > 1)
							pcdOK = "5";
					}
//					out.println("<br>forma de pago solo remesas");
				}
//				else
//					out.println("otras formas de pago ");
			}  // for 2

			// VALIDACION TODOS LOS SERVICIOS LA MISMA FORMA DE PAGO
			if(!pcdOK.equals("5"))
			{
				size = data.size();
				int items = size / 10;
//				out.println("items: "+ items);
				for(z=1; z<items; z++)
				{
					if(!(((data.elementAt(4).toString()).equals(data.elementAt((z*10)+4).toString())) &&
						 ((data.elementAt(5).toString()).equals(data.elementAt((z*10)+5).toString())) &&
						 ((data.elementAt(6).toString()).equals(data.elementAt((z*10)+6).toString()))))
					{
//						out.println("ERROR DE COMPARACION");
						pcdOK = "6";
						break;
					}
				}
			}
			size = data.size();
//			out.println("<br>size antes de insertar formas de pago " + size);
			int items = size / 10;
//			out.println("items: "+ items);
			// INSERCION DE FORMAS DE PAGO GENERAL Y MONTO TOTAL
			data.insertElementAt(data.elementAt(4).toString(),1);
			data.insertElementAt(data.elementAt(5+1).toString(),2);
			data.insertElementAt(data.elementAt(6+2).toString(),3);
			data.insertElementAt((String)datasession.get("txtTot"),4);

			// BORRADO DE FORMAS DE PAGO DE CADA SERVICIO
			for (z=0; z<items; z++)
			{
				for (x=0; x<3; x++)
					data.removeElementAt(7*z+8);
			}
			// INSERCION DE NO. DE SERVICIO, NO. DE PAGOS Y MONTO
			String servicio = "";
			String nopagos = "";
			String monto = "";
			String consec ="";
			int y = 0;
			for (z=0; z<items; z++)
			{
				y = z+1;
				consec = Integer.toString(y);
				servicio = "servicio" + consec;
				nopagos = "nopagos" + consec;
				monto = "monto" + consec;
//				out.println("<br><br>servicio a recuperar : "+ servicio);
				data.insertElementAt((String)datasession.get(servicio),5+10*z);
				data.insertElementAt((String)datasession.get(nopagos),6+10*z);
				data.insertElementAt((String)datasession.get(monto),7+10*z);
			}
		} // pcdOK 2

		if(pcdOK.equals("1"))
		{
			// INSERCION DE MONTO TOTAL
			data.addElement((String)datasession.get("txtTot"));
			// INSERCION DE NO. DE SERVICIO, NO. DE PAGOS Y MONTO
			String servicio = "";
			String nopagos = "";
			String monto = "";
			String consec ="";
			int y = 0;
			for (z=0; z<ns; z++)
			{
				y = z+1;
				consec = Integer.toString(y);
				servicio = "servicio" + consec;
				nopagos = "nopagos" + consec;
				monto = "monto" + consec;
//				out.println("<br><br>servicio a recuperar : "+ servicio);
				data.insertElementAt((String)datasession.get(servicio),2+3*z);
				data.insertElementAt((String)datasession.get(nopagos),3+3*z);
				data.insertElementAt((String)datasession.get(monto),4+3*z);
			}
		}
		// INCLUSION DE LA DIRECCION DE LA SUCURSAL AL FINAL DEL VECTOR
		if(dirsuc.length() != 0)
			data.addElement(dirsuc);

		session.setAttribute("datos",data);
		session.setAttribute("pcd",pcdOK);

		for(int j =0; j <data.size(); j++)
			out.println("<br>data " + j + " : **" + (String)data.elementAt(j) + "**");

//		out.println("<br> code 1 : *" + (String)pcdcode.elementAt(0) + "*");
//		out.println("<br> code 2 : *" + (String)pcdcode.elementAt(1) + "*");
//		out.println("<br> code 3 : *" + (String)pcdcode.elementAt(2) + "*");
//		out.println("<br> code 4 : *" + (String)pcdcode.elementAt(3) + "*");
	}
}
%>
<p>
<%
   out.print("<form name=\"Txns\" action=\"../../servlet/ventanilla.PageServlet\" method=\"post\">");
%>
  <input type="hidden" name="transaction" value="<%= session.getAttribute("page.iTxn") %>">
  <input type="hidden" name="transaction1" value="<%= session.getAttribute("page.oTxn") %>">
<%
   out.print("</form>");
%>

</body>
<%
	if(pcdOK.equals("1") || pcdOK.equals("2"))
	{
		out.println("<b>Para redireccionar a PageBuilderSAT...</b>");
  		response.sendRedirect("PageBuilderSAT.jsp");
	}
	if(pcdOK.equals("3"))
		out.println("<b>El Servicio " + (String)datasession.get("servicio"+(x+1)) + " no existe...</b>");
	if(pcdOK.equals("4"))
		out.println("<b>Problemas con el alta de la clave del Servicio " + (String)datasession.get("servicio"+(x+1)) +", contactar al Ejecutivo de Cuenta...</b>");
	if(pcdOK.equals("5"))
		out.println("<b>El pago de servicios con Remesas solo es permitida para un servicio y un pago...</b>");
	if(pcdOK.equals("6"))
		out.println("<b>La combinaci&oacute;n de formas de pago entre los servicios no es compatible, realizar por separado...</b>");
	if(pcdOK.equals("7"))
	{
		String campo = "";
		int num = 0;
		String codes1 = "";
		for(x=0; x<code1.size(); x++)
		{
			campo =  (String)code1.elementAt(x);
			num = Integer.parseInt(campo);
			codes1 = codes1 + (String)datasession.get("servicio"+(num+1));
			if((code1.size() != 1) && (code1.size() != (x+1)))
				codes1 = codes1 + ", ";
			else
				codes1 = codes1 + "; ";
		}
		String codes2 = "";
		for(x=0; x<code2.size(); x++)
		{
			campo =  (String)code2.elementAt(x);
			num = Integer.parseInt(campo);
			codes2 = codes2 + (String)datasession.get("servicio"+(num+1));
			if((code2.size() != 1) && (code2.size() != (x+1)))
				codes2 = codes2 + ", ";
		}
		out.println("<b>Servicio(s) " + codes1 + " no compatible(s) con Servicio(s) " + codes2 + ". Procesar por separado...</b>");
	}
	if(pcdOK.equals("8"))
		out.println("<b>Problemas con la definici&oacute;n de la forma de pago del Servicio " + (String)datasession.get("servicio"+(x+1)) +", contactar al Ejecutivo de Cuenta...</b>");
%>
</html>
