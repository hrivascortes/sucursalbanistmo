<!--
//*************************************************************************************************
//             Funcion: JSP que despliega respuesta de txn 3009
//            Elemento: Response3009.jsp
//          Creado por: Alejandro Gonzalez Castro
//      Modificado por: Alejandro Gonzalez Castro
//*************************************************************************************************
// CCN - 4360175 - 03/08/2004 - Ajuste size de ventana de impresion
// CCN - 4360275 - 18/02/2005 - WAS 5.1
//*************************************************************************************************
-->

<%@ page session="true" import="java.util.*"%>
<%
response.setHeader("Cache-Control","no-store"); //HTTP 1.1
response.setHeader("Pragma","no-cache"); //HTTP 1.0
response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<%!
  private String getString(String s)
  {
    int len = s.length();
     for(; len>0; len--)
	  if(s.charAt(len-1) != ' ')
	    break;

    return s.substring(0, len);
  }

  private String getCantidad(String s)
   {
     int Indice = s.indexOf(".");
     String cadena = "";
     cadena = s.substring(Indice,Indice+3);
     int Indice2 = Indice;
     for(; Indice>0; Indice--)
	  if(s.charAt(Indice-1) == ' ')
	    break;

     cadena = s.substring(Indice,Indice2)+cadena;
     return cadena;
  }

 private String getItem(String newCadNum, int newOption)
  {
   int iPIndex = newCadNum.indexOf("CONSECUTIVO:") + 12, NumSaltos, Num = 0, Ban = 0;
   String nCadNum = new String("");
   NumSaltos = newOption;
   while(Num < NumSaltos)
   {
    if(newCadNum.charAt(iPIndex) == 0x20)
    {
     while(newCadNum.charAt(iPIndex) == 0x20)
      iPIndex++;
     Num++;
    }
    else
     while(newCadNum.charAt(iPIndex) != 0x20)
      iPIndex++;
   }
   while(newCadNum.charAt(iPIndex) != 0x20 && iPIndex < newCadNum.length()){
    nCadNum = nCadNum + newCadNum.charAt(iPIndex);
    if(iPIndex < newCadNum.length())
      iPIndex++;
    if(iPIndex == newCadNum.length())
      break;
   }

   return nCadNum;
  }

  private String quitaespacios(String s){
      String s1 = "";
      s1 = s;
      s = "";
      int len = s1.length();
      for ( int i = 0; i < len; i++ ){
        if ( s1.charAt(i) != ' ') {
           s = s + s1.charAt(i);
         }
        else
           s = s + "%20";
      }
      return s;
   }

 private String delCommaPointFromString(String newCadNum)
  {
    String nCad = new String(newCadNum);
    if ( nCad.indexOf(".") > -1) {
       nCad = nCad.substring(nCad.indexOf(".") + 1, nCad.length());
       if (nCad.length() != 2) {
           String szTemp = new String("");
           for (int j = nCad.length(); j < 2; j++)
               szTemp = szTemp + "0";
           newCadNum = newCadNum + szTemp;
       }
    }

    StringBuffer nCadNum = new StringBuffer(newCadNum);
    for (int i = 0; i < nCadNum.length(); i++)
        if (nCadNum.charAt(i) == ',' || nCadNum.charAt(i) == '.')
            nCadNum.deleteCharAt(i);

    return nCadNum.toString();
  }
%>
<% Vector resp = (Vector)session.getAttribute("response");
Hashtable datasession = (Hashtable)session.getAttribute("page.datasession");
String txtCDACuenta = (String)datasession.get("txtCDACuenta");
String txtMonto = (String)datasession.get("txtMonto");
String txtMoneda = (String)datasession.get("moneda");
String txtProducto ="";
String txtBeneficiario = "";
String txtFechaVen = "";
String txtTaza = "";
String txtPlazo = "";
String estatus = (String)resp.elementAt(0);

int lines = Integer.parseInt((String)resp.elementAt(1));

if (estatus.equals("0")) {     // Transaccion aceptada

   Vector lineas = new Vector();

   int numlines = Integer.parseInt((String)resp.elementAt(1));
   String szRespuesta = (String)resp.elementAt(3);
   for(int i=0; i<numlines; i++){
      String linea = szRespuesta.substring( i*78, Math.min(i*78+77, szRespuesta.length()) );
      lineas.addElement(getString(linea));
      if(Math.min(i*78+77, szRespuesta.length()) == szRespuesta.length())
       break;
     }
   String linearesp = "";
   linearesp = (String)lineas.elementAt(0);
    if(linearesp.length() >31)
        txtTaza = linearesp.substring(31);

   linearesp = (String)lineas.elementAt(1);
   txtProducto = linearesp.substring(31,33);

   txtBeneficiario = linearesp.substring(0,26).replace((char)0x2A,(char)0x20);
   txtFechaVen = linearesp.substring(40,linearesp.length());
   txtFechaVen = txtFechaVen.substring(6)+txtFechaVen.substring(4,6)+txtFechaVen.substring(2,4);
}
%>
<html>
<head>
  <title>Certificación</title>
<style type="text/css">Body {font-family: Arial, Helv; font-size: 10pt; font-weight: bold; color:#000000}</style>
<script language="javascript">
<!--
  sharedData = '0';

  function formSubmitter()
  {
    clearInterval(formSubmitter);
    
    if(sharedData != '0')
    {
      document.forms[0].submit()
      sharedData = '0';
    }
  }
//-->
</script>
</head>
<body bgcolor="#FFFFFF" onload="top.setFieldFocus(window.document);setInterval('formSubmitter()',2000)">
<p>
<%
   if( resp.size() != 4 )
    out.println("<b>Error de conexi&oacute;n</b><br>");
   else
   {
    try{
     String codigo = (String)resp.elementAt(0);
     if( codigo.equals("1"))
        out.println("<b>Transaccion Rechazada</b><br><br>");
     if( codigo.equals("0") || codigo.equals("1") ) {
        lines = Integer.parseInt((String)resp.elementAt(1));
        String szResp = (String)resp.elementAt(3);
        for(int i=0; i<lines; i++){
          String line = szResp.substring( i*78, Math.min(i*78+77, szResp.length()) );
          out.println(getString(line) + "<p>");
          if(Math.min(i*78+77, szResp.length()) == szResp.length())
             break;
        }
     }
     else {
         if ( codigo.equals("3") || codigo.equals("2"))
            out.println("<b>Transacci&oacute;n Requiere Autorizaci&oacute;n</b>");
         else
            out.println("<b>Ocurri&oacute; un Error de Comunicaci&oacute;n, Verifique.</b>");
       }
     if (codigo.equals("0")) {
        if (txtProducto.equals("Z1") || txtProducto.equals("ZA"))
           txtPlazo = "7  DIAS";
        if (txtProducto.equals("Z2") || txtProducto.equals("ZB"))
           txtPlazo = "28 DIAS";
        if (txtProducto.equals("Z3") || txtProducto.equals("ZC"))
           txtPlazo = "91 DIAS";
        if (txtProducto.equals("Z4") || txtProducto.equals("ZD"))
           txtPlazo = "182 DIAS";
        if (txtProducto.equals("Z5") || txtProducto.equals("ZE"))
           txtPlazo = "371 DIAS";
        if (txtProducto.equals("X2") || txtProducto.equals("XB"))
           txtPlazo = "3 MESES";
        if (txtProducto.equals("X3") || txtProducto.equals("XC"))
           txtPlazo = "6 MESES";
        if (txtProducto.equals("X4") || txtProducto.equals("XD"))
           txtPlazo = "9 MESES";
        if (txtProducto.equals("X5") || txtProducto.equals("XE"))
           txtPlazo = "13 MESES";
        if (txtProducto.equals("Y1") || txtProducto.equals("J2"))
           txtPlazo = "91 DIAS";
        if (txtProducto.equals("Y2") || txtProducto.equals("J3"))
           txtPlazo = "182 DIAS";
        if (txtProducto.equals("Y3") || txtProducto.equals("J5"))
           txtPlazo = "371 DIAS";
        if (txtProducto.equals("Y4") || txtProducto.equals("J6"))
           txtPlazo = "560 DIAS";
        if (txtProducto.equals("Y5") || txtProducto.equals("J7"))
           txtPlazo = "721 DIAS";
        if (txtProducto.equals("YA") || txtProducto.equals("I3"))
           txtPlazo = "91 DIAS";
        if (txtProducto.equals("YB") || txtProducto.equals("I4"))
           txtPlazo = "182 DIAS";
        if (txtProducto.equals("YC") || txtProducto.equals("I5"))
           txtPlazo = "371 DIAS";
        if (txtProducto.equals("YD") || txtProducto.equals("JF"))
           txtPlazo = "560 DIAS";
        if (txtProducto.equals("YE") || txtProducto.equals("JG"))
           txtPlazo = "721 DIAS";
        if (txtProducto.equals("J4") || txtProducto.equals("JD"))
           txtPlazo = "273 DIAS";
        if (txtProducto.equals("IC") || txtProducto.equals("JB"))
           txtPlazo = "91 DIAS";
        if (txtProducto.equals("ID") || txtProducto.equals("JC"))
           txtPlazo = "182 DIAS";
        if (txtProducto.equals("IE") || txtProducto.equals("JE"))
           txtPlazo = "371 DIAS";
        datasession.put("txtTaza",txtTaza);
        datasession.put("txtBeneficiario",txtBeneficiario);
        datasession.put("txtFechaVen",txtFechaVen);
        datasession.put("txtProducto",txtProducto);
        datasession.put("txtPlazo",txtPlazo);
        session.setAttribute("page.datasession",datasession);
     }
    }
    catch(NumberFormatException nfe){
      out.println("<b>Error de conexi&oacute;n</b><br>");
    }
   }
%>
<form name="Txns" action="../../servlet/ventanilla.PageServlet" method="post">
<table border="0" cellspacing="0">
<tr>
<td>
</td>
</tr>
<tr>
<td>
<%
  String isCertificable = (String)session.getAttribute("page.certifField");  // Para Certificación
  String txtCadImpresion  = ""; // Para Certificación
  Stack flujotxn = (Stack)session.getAttribute("page.flujotxn");
  String needOverride = "NO";
  String codigo = (String)resp.elementAt(0);
  if( codigo.equals("0"))
     txtCadImpresion = "~REINVERSION~" + txtBeneficiario + "~" + txtCDACuenta + "~"+ txtProducto + "~"+ txtPlazo +"~"+ delCommaPointFromString(txtMonto) + "~" + txtTaza + "~" + txtMoneda + "~" + txtFechaVen + "~";
  if(!flujotxn.empty()){
    out.print("<div align=\"center\">");
    
    if(!isCertificable.equals("N"))
    { 
       // Para Certificación
       session.setAttribute("Consecutivo", getItem((String)resp.elementAt(3), 1)); // Para Certificación
       session.setAttribute("Tipo", isCertificable); // Para Certificación
       session.setAttribute("txtCadImpresion", txtCadImpresion); // Para Certificación
       
       if(codigo.equals("0"))
     	  out.println("<script language=\"javascript\">top.openDialog('../../servlet/ventanilla.ImprimirServlet', 160, 120, 'top.setPrefs4()', document.Txns)</script>"); // Para Certificación
       else
       {
         if ( codigo.equals("3") || codigo.equals("2")) 
         {
            needOverride = "SI";
            out.print("<script languaje=\"javascript\"> top.openDialog('authoriz.jsp', 220, 130, 'top.setPrefs8()', document.Txns) </script>");
         }
         else
           out.println("<a href=\"javascript:top.openDialog('../../servlet/ventanilla.ImprimirServlet', 160, 120, 'top.setPrefs4()', document.Txns)\"><img src=\"../imagenes/b_continuar.gif\" border=\"0\"></a>"); // Para Certificación
       }
    }
    else 
    {
    	if(codigo.equals("0"))
    	{
     	   out.println("<script language=\"javascript\">sharedData = '1'</script>");
     	   out.println("<font color='red'>procesando...</font>");
     	}
    	else 
    	{
           if ( codigo.equals("3") || codigo.equals("2")) 
           {
              needOverride = "SI";
              out.print("<script languaje=\"javascript\"> top.openDialog('authoriz.jsp', 220, 130, 'top.setPrefs8()', document.Txns) </script>");
           }
           else
    	      out.print("<input type=\"image\" src=\"../imagenes/b_continuar.gif\" alt=\"\" border=\"0\">");
        }
    }
    
    out.print("</div>");
  }
  else{
      session.setAttribute("Consecutivo", getItem((String)resp.elementAt(3), 1)); // Para Certificación
      session.setAttribute("Tipo", isCertificable); // Para Certificación
      session.setAttribute("txtCadImpresion", txtCadImpresion); // Para Certificación
      if ( codigo.equals("3") || codigo.equals("2")) {
          needOverride = "SI";
          out.print("<script languaje=\"javascript\"> top.openDialog('authoriz.jsp', 220, 130, 'top.setPrefs8()', document.Txns) </script>");
      }
      else
         out.println("<script language=\"javascript\">top.openDialog('../../servlet/ventanilla.ImprimirServlet', 160, 120, 'top.setPrefs4()', document.Txns)</script>"); // Para Certificación
  }
%>
  <input type="hidden" name="transaction" value="<%= session.getAttribute("page.iTxn") %>">
  <input type="hidden" name="transaction1" value="<%= session.getAttribute("page.oTxn") %>">
  <input type="hidden" name="override" value="<%= needOverride%>">
  <input type="hidden" name="supervisor" value="<%= session.getAttribute("supervisor") %>">
</td>
</tr>
</table>
</form>
</body>
</html>
