<!--
// CCN - 4360581 - 12/03/2007 - Se convierten los estilos en jsf y se hace un include en el jsp
-->

<%@ page session="true"%>
<%
response.setHeader("Cache-Control","no-store"); //HTTP 1.1
response.setHeader("Pragma","no-cache"); //HTTP 1.0
response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
	<head>
		<%@include file="style.jsf"%>
		<title>Verificación de Disquete</title>
	</head>
	<Script language="JavaScript">
		function sendfile()
		{
			alert("Disquete Verificado")
      	document.entry.action = "../servlet/ventanilla.SendfileServlet"
			document.entry.submit();
		}
   </SCRIPT>	
	<body>
        <h1><%=session.getAttribute("page.txnImage")%></h1>
		<form name="entry">
			<h3>
<%						out.print(session.getAttribute("page.cTxn") + "  " + (String)session.getAttribute("page.txnlabel"));
%>
			</h3>
			<p>			
      	<input type="hidden" name="DiscoCHQM" >
      	<input type="hidden" name="Page" value="CCMA">
			<EMBED type="application/x-java-applet;version=1.2"
				java_ARCHIVE="Verdisk.jar"
				java_CODE ="VerDisk"
				java_NAME ="Cheques Masivos"
				java_CODEBASE =".."         	   
				WIDTH ="350"
				HEIGHT = "50"
				MAYSCRIPT>
			</EMBED>
			<br><br>		
			<a href="paginas/panelcaptura.html"><img src="../ventanilla/imagenes/b_aceptar.gif" border="0"></a>
		</form>
	</body>
</html>
