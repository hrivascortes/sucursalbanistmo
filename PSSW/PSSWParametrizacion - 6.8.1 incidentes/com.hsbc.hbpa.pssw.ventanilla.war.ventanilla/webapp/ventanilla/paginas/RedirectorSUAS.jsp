<!--
//*************************************************************************************************
///            Funcion: JSP para flujo de SUAS
//            Elemento: RdirectorSUAS.jsp
//          Creado por: Fausto Rodrigo Flores Moreno
//*************************************************************************************************
// CCN - 4360602 - 18/05/2007 - Se realizan modificaciones para proyecto Codigo de Barras SUAS
//*************************************************************************************************
-->

<%@ page session="true" import="ventanilla.com.bital.util.*,java.util.*"%>
<%
response.setHeader("Cache-Control","no-store"); //HTTP 1.1
response.setHeader("Pragma","no-cache"); //HTTP 1.0
response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
%>
<%
Vector frap = (Vector)session.getAttribute("FRAP");
Vector resp = (Vector)session.getAttribute("response");
String control = (String)resp.get(0);
Hashtable datasession = (Hashtable)session.getAttribute("page.datasession");
if(datasession == null)
	datasession = new Hashtable();
	
Stack flujotxn = new Stack();
if(session.getAttribute("page.flujotxn") != null)
	flujotxn = (java.util.Stack)session.getAttribute("page.flujotxn");
String tipo = (String)request.getParameter("lstTPSua");

datasession.put("idpagosSuas",(String)request.getParameter("idpagosSuas"));
datasession.put("txtTotalSUAS",(String)request.getParameter("txtTotalSUAS"));

if (flujotxn != null)
	flujotxn.removeAllElements();
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
  <title>Certificación</title>
<style type="text/css">
  Body {font-family: Arial, Helv; font-size: 9pt; font-weight: bold}
</style>

<script language="JavaScript">
<!--
  function redirect(){
    <%if(control.equals("0")){%>
    document.forms[0].submit();
    <%}%>
  }
//-->
</script>
</head>

<body bgcolor="#FFFFFF" onload="javascript:redirect();">
<p>
<%
String proceso = "";
out.print("<form name=\"Txns\" action=\"../../servlet/ventanilla.PageServlet\" method=\"post\">");
datasession.put("opcionSuas","2");
datasession.put("txtimporte",(String)request.getParameter("txtTotalSUAS"));
session.setAttribute("page.iTxn","0068");
if(tipo.equals("C"))
{
	datasession.put("txtMonto",(String)request.getParameter("txtTotalSUAS"));
	datasession.put("txtServicio","000131");
	datasession.put("txttipo","C");
	flujotxn.push("0772");
	flujotxn.push("5359");
	%>
	<input type="hidden" name="transaction" value="5359">
	<input type="hidden" name="transaction1" value="5359"><%
}
if(tipo.equals("O"))
{
	datasession.put("txtMonto",(String)request.getParameter("txtTotalSUAS"));
	datasession.put("txtServicio","000132");
	datasession.put("txttipo","O");
	flujotxn.push("0772");
	flujotxn.push("0580");%>
    <input type="hidden" name="transaction" value="0580">
    <input type="hidden" name="transaction1" value="0580"><%
}
if(tipo.equals("T"))
{
	datasession.put("txtCheque",(String)request.getParameter("txtTotalSUAS"));
    datasession.put("txtMontoCheque",(String)request.getParameter("txtTotalSUAS"));
	datasession.put("txtServicio","000130");
	datasession.put("txttipo","T");
	flujotxn.push("0772");
	flujotxn.push("5353");%>
    <input type="hidden" name="transaction" value="5353">
    <input type="hidden" name="transaction1" value="5353"><%
}
session.setAttribute("page.datasession", datasession);
session.setAttribute("page.flujotxn",flujotxn);
out.print("</form>");
%>
<p>
</body>
</html>