<!--
//*************************************************************************************************
///            Funcion: JSP que despliega respuesta de txn 0068
//            Elemento: Response0068.jsp
//          Creado por: Alejandro Gonzalez Castro
//      Modificado por: Fausto Rodrigo Flores Moreno
//*************************************************************************************************
// CCN - 4360291 - 18/03/2005 - Corrección a Pagos SUAS con Cobro Inmediato 
// CCN - 4360297 - 23/03/2005 - Se cambia CCN por peticion de QA, CCN anterior 4360291
// CCN - 4360301 - 29/03/2005 - Se corrige problema de Sintaxis
// CCN - 4360302 - 01/04/2005 - Se evita almacenar los datos para digitalizacion en pago SUA con Cobro Inmediato
// CCN - 4360368 - 02/09/2005 - Se genera la nueva version de SUAS
// CCN - 4360373 - 07/09/2005 - Se genera nuevo CCN a peticion de QA 
// CCN - 4360406 - 15/12/2005 - Nueva version de Suas
// CCN - 4360602 - 18/05/2007 - Se realizan modificaciones para proyecto Codigo de Barras SUAS
//*************************************************************************************************
-->

<%@ page session="true" import="java.util.*"%>
<%
response.setHeader("Cache-Control","no-store"); //HTTP 1.1
response.setHeader("Pragma","no-cache"); //HTTP 1.0
response.setDateHeader ("Expires", 0); //prevents caching at the proxy server*/
%>
<%!
   private String addpunto(String valor)
   {
      int longini = valor.length();
      String cents  = valor.substring(longini-2,longini);
	  String entero = valor.substring(0,longini-2);
	  int longente = entero.length();
	  for (int i = 0; i < (longente-(1+i))/3; i++){
		  longente = entero.length();
	    entero = entero.substring(0,longente-(4*i+3))+','+entero.substring(longente-(4*i+3));
	  }
  	entero = entero + '.' + cents;
	  return entero;
	}

  private String getItem(String newCadNum, int newOption)
  {
   int iPIndex = newCadNum.indexOf("CONSECUTIVO:") + 12, NumSaltos, Num = 0, Ban = 0;
   String nCadNum = new String("");
   NumSaltos = newOption;
   try{
    while(Num < NumSaltos){
     if(newCadNum.charAt(iPIndex) == 0x20){
      while(newCadNum.charAt(iPIndex) == 0x20)
       iPIndex++;
      Num++;
     }
     else
      while(newCadNum.charAt(iPIndex) != 0x20)
       iPIndex++;
    }
   while(newCadNum.charAt(iPIndex) != 0x20 && iPIndex < newCadNum.length()){
    nCadNum = nCadNum + newCadNum.charAt(iPIndex);
    if(iPIndex < newCadNum.length())
      iPIndex++;
    if(iPIndex == newCadNum.length())
      break;
   }
   }
   catch(IndexOutOfBoundsException e){
    //System.out.println("Fin de Cadena :" + nCadNum);
   }
   return nCadNum;
  }

  private String getString(String s)
  {
    int len = s.length();
    for(; len>0; len--)
	  if(s.charAt(len-1) != ' ')
	    break;
    return s.substring(0, len);
}


  private String getCantidad(String s)
   {
     int Indice = s.indexOf(".");
     String cadena = "";
     cadena = s.substring(Indice,Indice+3);
     int Indice2 = Indice;
     for(; Indice>0; Indice--)
	  if(s.charAt(Indice-1) == ' ')
	    break;

     cadena = s.substring(Indice,Indice2)+cadena;
     return cadena;
  }



  private String delnulls(String c)
  {
    if (c == null)
        c = "";
    StringBuffer newsb = new StringBuffer(c);
    int Len = newsb.length();
    for(int i=0; i<newsb.length(); i++){
     if(newsb.charAt(i) != '0'){
	    newsb.delete(0,i);
	    break;
	   }
    }
    return newsb.toString();
  }
%>
<html>
<%
  Vector resp = (Vector)session.getAttribute("response");
  Hashtable datasession = (Hashtable)session.getAttribute("page.datasession");
try
{
%>
<head>
  <title>Certificación</title>
<style type="text/css">Body {font-family: Arial, Helv; font-size: 10pt; font-weight: bold; color:#000000}</style>
<script language="JavaScript" >
function evalua(estatus)
{
	alert(estatus);
	document.Txns.action = "Final.jsp";
	document.Txns.submit();
}
</script>
</head>
<body>
<form name="Txns" action="../../servlet/ventanilla.PageServlet" method="post">
 <input type="hidden" name="txtInvoqued" value="somedatahere">
<table border="0" cellspacing="0">
<tr>
<td>
  <input type="hidden" name="transaction" value="<%= session.getAttribute("page.iTxn") %>">
  <input type="hidden" name="transaction1" value="<%= session.getAttribute("page.oTxn") %>">
</td>
</tr>
</table>
<p>
<%
   datasession.put("OutOff","0");
   String status = "";
   String tipo = "";
   String szResp = new String("");
   String needOverride = "NO";
   String cTxn = (String)datasession.get("cTxn");
   String codigo = "1";
   Stack flujotxn = new Stack();
   int numline = 0;
   
   String opSuas = "";
   if(datasession.get("opcionSuas")!=null){
	  opSuas = (String)datasession.get("opcionSuas");
   }else
   	  opSuas = "1"; 	
	 
    if( resp.size() != 4 )
        out.println("<b>Error de conexi&oacute;n</b><br>");
    else
    {
        try{codigo = (String)resp.elementAt(0);}
        catch(NumberFormatException nfe){out.println("<b>Error de conexi&oacute;n</b><br>");}
        if( codigo.equals("1"))
   	   out.println("<b>Transaccion Rechazada</b><br><br>");
        if( codigo.equals("0") || codigo.equals("1") ) {
           int lines = Integer.parseInt((String)resp.elementAt(1));
           szResp = (String)resp.elementAt(3);
           for(int i=0; ; i++)
           {
               String line = szResp.substring( i*78, Math.min(i*78+77, szResp.length()) );
               if (cTxn.equals("0817") && i == 3 && codigo.equals("0"))
                  {
                     status = line.substring(9,10);
                  }
               numline = i;
               if (i<3)
               {
                   out.println(getString(line) + "<p>");
               }
               if(Math.min(i*78+77, szResp.length()) == szResp.length())
                break;
           }
       }
       else {
         if ( codigo.equals("3") || codigo.equals("2"))
            out.println("<b>Transacci&oacute;n Requiere Autorizaci&oacute;n</b>");
         else
            out.println("<b>Ocurri&oacute; un Error de Comunicaci&oacute;n, Verifique.</b>");
       }
      if (codigo.equals("0"))
       {
           if (cTxn.equals("0817"))
           {
            String mensaje = "";
            if (!status.equals("R") && !status.equals(" "))
            {
                if (status.equals("N"))
                    {mensaje = "Registro no Encontrado.";}
                else if (status.equals("B"))
                    {mensaje = "Base de Datos no Disponible.";}
                else if (status.equals("D"))
                    {mensaje = "Datos no Disponibles.";}
                else if (status.equals("E"))
                    {mensaje = "Error al Leer el Archivo.";}
                else if (status.equals("M"))
                    {mensaje = "Diferencia de Montos.";}
                else if (status.equals("A"))
                    {mensaje = "Pago ya Realizado.";}
                datasession.put("OutOff","1");
                flujotxn = (Stack)session.getAttribute("page.flujotxn");
                while (!flujotxn.empty())
                {
                    flujotxn.pop();
                }
                %>
                <script>
                    valor = '<%=mensaje%>';
                    evalua(valor);
                </script>
<%          }
            else
            {
               int lines = Integer.parseInt((String)resp.elementAt(1));
               Vector lineas = new Vector();
               szResp = (String)resp.elementAt(3);
               for(int i=0; i<lines; i++){
                    String line = szResp.substring( i*78, Math.min(i*78+77, szResp.length()) );
                    lineas.addElement(getString(line));
                    if(Math.min(i*78+77, szResp.length()) == szResp.length())
                        break;
               }
               lines = lineas.size();
               String linearesp = "";
               linearesp = (String)lineas.elementAt(5);

			   if(opSuas.equals("1")){
	               String txtimporte = getCantidad(linearesp);
    	           datasession.put("txtimporte",txtimporte);
        	       linearesp = (String)lineas.elementAt(6);
            	   tipo = (String)datasession.get("lstTPSua");
	               datasession.put("txttipo",tipo);
    	           linearesp = (String)lineas.elementAt(9);
        	       String periodo = linearesp.substring(32,38); 
            	   datasession.put("txtperiodo",periodo);
            	   if( lineas.size() > 10 )
	            	   linearesp = (String)lineas.elementAt(10);
    	           else
        	         linearesp = "";
	               if (linearesp.length() == 0)
    	               linearesp = "0.00";
        	       String txtimporte1 = getCantidad(linearesp);
            	   if ( lines > 11) {
                	  linearesp = (String)lineas.elementAt(11);
	                  String txtimporte2 = getCantidad(linearesp);
    	              datasession.put("txtimporte2", txtimporte2);
        	       }
            	   datasession.put("txtimporte1", txtimporte1);
	               flujotxn = (Stack)session.getAttribute("page.flujotxn");
    	           Stack flujonew = new Stack();
        	       String newtxn = "";
            	   datasession.put("txtServicio","0129");
	               if (tipo.equals("C"))
    	            {
        	             newtxn = "5359";
            	         datasession.put("txtServicio","0131");
                	}
	               else if(tipo.equals("T"))
    	            {
        	            datasession.put("txtCheque",(String)datasession.get("txtMonto"));
            	        datasession.put("txtMontoCheque",(String)datasession.get("txtMonto"));
                	    newtxn = "5353";
	                    datasession.put("txtServicio","0130");
    	            }
        	       else if(tipo.equals("O"))
            	    {
	                    newtxn = "0580";
    	                datasession.put("txtServicio","0132");
        	        }
            	    String[] newflujo ={"0772",newtxn};
                	if (!newtxn.equals(""))
	                {
    	                for (int i=0; i<newflujo.length; i++)
        	                {flujonew.push(newflujo[i]);}
            	        flujotxn = flujonew;
                	    session.setAttribute("page.flujotxn", flujotxn);
	                }
				}
   	        }
		}
	
	if (cTxn.equals("0817"))
	{%><input id="cont_img" tabindex='1' type="image" src="../imagenes/b_continuar.gif" border="0">
	<script language = "javascript">
	
		var img = document.getElementById('cont_img');
		img.focus();
	
	</script>
	
	<%	}%>

        <%//Certificacion Inicio
        if (datasession.get("OutOff").toString().equals("0") && !cTxn.equals("0817")&& !cTxn.equals("0837"))
        {
            String isCertificable = (String)session.getAttribute("page.certifField");  // Para Certificación
            String txtCadImpresion  = ""; // Para Certificación
            session.setAttribute("Consecutivo", getItem((String)resp.elementAt(3), 1)); // Para Certificación
            session.setAttribute("Tipo", isCertificable); // Para Certificación
            
            if (cTxn.equals("0772") && opSuas.equals("1"))
            {
              String txtservicio = (String)datasession.get("txtServicio");
              txtCadImpresion = "~PAGOSUA~" + delnulls((String)datasession.get("txtnumctrl")) + "~" + delnulls((String)datasession.get("txtimporte1")) + "~" + delnulls((String)datasession.get("txtimporte2")) + "~" + delnulls((String)datasession.get("txtimporte")) + "~" + txtservicio.substring(2) + "~" + delnulls((String)datasession.get("txtperiodo")) + "~" + (String)datasession.get("txttipo") + "~";
            }
            
            if (cTxn.equals("0772") && opSuas.equals("2"))
            {
              String txtservicio = (String)datasession.get("txtServicio");
              String idpago = (String)datasession.get("idpagosSuas");
              if(idpago!=null){
	              if(idpago.equals("M")){
		            txtCadImpresion = "~PAGOSUA1~" + delnulls((String)datasession.get("txtnumctrl")) + "~" + delnulls((String)datasession.get("txtEMI1")) + "~~" + delnulls((String)datasession.get("txtTotalSUAS")) + "~" + txtservicio.substring(2) + "~" + delnulls((String)datasession.get("txtperiodo")) + "~" + (String)datasession.get("txttipo") + "~" + delnulls((String)datasession.get("txtRFCSUA")) + "~" + delnulls((String)datasession.get("txtPeriodo")) + "~";
	    	      }else if(idpago.equals("N")){
	        	  	txtCadImpresion = "~PAGOSUA1~" + delnulls((String)datasession.get("txtnumctrl")) + "~~" + delnulls((String)datasession.get("txtEMI2")) + "~" + delnulls((String)datasession.get("txtTotalSUAS")) + "~" + txtservicio.substring(2) + "~" + delnulls((String)datasession.get("txtperiodo")) + "~" + (String)datasession.get("txttipo") + "~" + delnulls((String)datasession.get("txtRFCSUA")) + "~" + delnulls((String)datasession.get("txtPeriodo")) + "~";
		          }else{
		          	txtCadImpresion = "~PAGOSUA1~" + delnulls((String)datasession.get("txtnumctrl")) + "~" + delnulls((String)datasession.get("txtEMI1")) + "~" + delnulls((String)datasession.get("txtEMI2")) + "~" + delnulls((String)datasession.get("txtTotalSUAS")) + "~" + txtservicio.substring(2) + "~" + delnulls((String)datasession.get("txtperiodo")) + "~" + (String)datasession.get("txttipo") + "~" + delnulls((String)datasession.get("txtRFCSUA")) + "~" + delnulls((String)datasession.get("txtPeriodo")) + "~";
	    	      }
	    	  }
            }
            
            String txn = session.getAttribute("page.cTxn").toString();
           
            if(!isCertificable.equals("N")){ // Para Certificación
      	      if(codigo.equals("0"))
      	        out.println("<script language=\"javascript\">top.openDialog('../../servlet/ventanilla.ImprimirServlet?txtCadImpresion=" + txtCadImpresion + "', 160, 120, 'top.setPrefs4()', document.Txns)</script>"); // Para Certificación
    	      else {
                 if ( codigo.equals("3") || codigo.equals("2")) {
                    needOverride = "SI";
                    out.print("<script languaje=\"javascript\"> top.openDialog('authoriz.jsp', 220, 130, 'top.setPrefs8()', document.Txns) </script>");
                 }
              else {
                 isCertificable = (String)session.getAttribute("page.certifField");  // Para Certificación
                 txtCadImpresion  = ""; // Para Certificación
                 session.setAttribute("Consecutivo", getItem((String)resp.elementAt(3), 1)); // Para Certificación
                 session.setAttribute("Tipo", isCertificable); // Para Certificación
          	 out.println("<a href=\"javascript:top.openDialog('../../servlet/ventanilla.ImprimirServlet?txtCadImpresion=" + txtCadImpresion + "', 160, 120, 'top.setPrefs4()', document.Txns)\"><img src=\"imagenes/b_continuar.gif\" border=\"0\"></a>"); // Para Certificación
               }
              }
            }
         }
         if(codigo.equals("1")){
           String isCertificable = (String)session.getAttribute("page.certifField");  // Para Certificación
           String txtCadImpresion  = ""; // Para Certificación
           session.setAttribute("Consecutivo", getItem((String)resp.elementAt(3), 1)); // Para Certificación
           session.setAttribute("Tipo", isCertificable); // Para Certificación
           out.println("<script language=\"javascript\">top.openDialog('../../servlet/ventanilla.ImprimirServlet?txtCadImpresion=" + txtCadImpresion + "', 160, 120, 'top.setPrefs4()', document.Txns)</script>"); // Para Certificación
         }
    }
          //Certificaion Fin  %>
<%}%>
<input type="hidden" name="supervisor" value="<%=session.getAttribute("supervisor") %>">
<input type="hidden" name="override" value="<%=needOverride%>">
<input type="hidden" name="transaction" value="<%=session.getAttribute("page.iTxn") %>">
<input type="hidden" name="transaction1" value="<%=session.getAttribute("page.oTxn") %>">
<input type="hidden" name="opcionSuas" value="<%=opSuas%>">
</form>
</body>
<%}catch(Exception e){
//System.out.println("Exception :" + e.toString());
//System.out.println("datasession Response0068 :" + datasession.toString());
//System.out.println("Resp Response0068 :" + resp);
//System.out.println("oTxn " + session.getAttribute("page.oTxn"));
//System.out.println("iTxn " + session.getAttribute("page.iTxn"));
//System.out.println("supervisor " + session.getAttribute("supervisor"));
///System.out.println("certifField" + session.getAttribute("page.certifField"));
//System.out.println("branch " + session.getAttribute("branch"));
//System.out.println("cTxn " + session.getAttribute("page.cTxn"));
//System.out.println("flujotxn " + session.getAttribute("page.flujotxn"));
}%>
</html>

