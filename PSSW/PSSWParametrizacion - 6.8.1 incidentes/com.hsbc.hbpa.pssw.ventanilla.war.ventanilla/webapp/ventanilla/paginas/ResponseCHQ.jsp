<!--
//*************************************************************************************************
//             Funcion: JSP que despliega respuesta de txn CHQ
//            Elemento: ResponseCHQ.jsp
//          Creado por: Alejandro Gonzalez Castro
// Ultima Modificacion: 03/08/2004
//      Modificado por: Alejandro Gonzalez Castro
//*************************************************************************************************
// CCN - 4360175 - 03/08/2004 - Ajuste size de ventana de impresion
//*************************************************************************************************
-->
<%@ page session="true"%>
<%
response.setHeader("Cache-Control","no-store"); //HTTP 1.1
response.setHeader("Pragma","no-cache"); //HTTP 1.0
response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
	<head>
		<title>Interface Impresión</title>
        <style type="text/css">Body {font-family: Arial, Helv; font-size: 10pt; font-weight: bold; color:#000000}</style>
	</head>
	<body>
		<form name="Txns">
			<table border="0" cellspacing="0">
				<tr>
					<td>
<%
	out.println("<script language=\"javascript\">top.openDialog('../../servlet/ventanilla.ImprimirServlet', 160, 120, 'top.setPrefs4()', document.Txns)</script>");
%>
						<input type="hidden" name="transaction" value="<%= session.getAttribute("page.iTxn") %>">
						<input type="hidden" name="transaction1" value="<%= session.getAttribute("page.oTxn") %>">
					</td>
				</tr>
			</table>
		</form>
	</body>
</html>
