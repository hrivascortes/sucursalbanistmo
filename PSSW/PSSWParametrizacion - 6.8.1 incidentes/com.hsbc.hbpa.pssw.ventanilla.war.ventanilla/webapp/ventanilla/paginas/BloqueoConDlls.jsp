<!--
//*************************************************************************************************
//             Funcion: JSP que muestra el mensaje de alerta para no concentraci�n a b�veda
//            Elemento: BloqueoConDlls.jsp
//          Creado por: Fausto Rodrigo Flores Moreno
//      Modificado por: Fausto Rodrigo Flores Moreno
//*************************************************************************************************
// CCN - 4360589 - 16/04/2007 - Se realizan modificaciones para el monitoreo de efectivo en las sucursales
// CCN - 4360640 - 12/07/2007 - Se cambia el mensaje de alerta cuando ocurre el bloqueo
//*************************************************************************************************
-->

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<HTML>
<HEAD>
<META http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<META name="GENERATOR" content="IBM WebSphere Studio">
<TITLE>Bloqueo de Cajero.jsp</TITLE>
<script type="text/JavaScript">
 function MensajeSesion(){
          alert("Haz excedido tu l�mite de d�lares en ventanilla, tu usuario ha sido bloqueado,\nespera llamada del �rea facultada para desbloquearte");
 }  
</script>
</HEAD>
<BODY onload="JavaScript:MensajeSesion();">
</BODY>
</HTML>
