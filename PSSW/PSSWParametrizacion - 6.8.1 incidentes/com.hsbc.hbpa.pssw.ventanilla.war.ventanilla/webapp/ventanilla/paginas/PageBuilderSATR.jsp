<!--
//*************************************************************************************************
//            Elemento: PageBuilderSATR.jsp
//             Funcion: JSP de captura. 
//          Creado por: Israel De Paz Mercado
// CCN - 4360409 - 06/01/2006 - Se crea PageBuilder para la linea de captura del SAT.
// CCN - 4360581 - 12/03/2007 - Se convierten los estilos en jsf y se hace un include en el jsp
-->


<%@ page session="true" import="java.util.*"%>
<%
response.setHeader("Cache-Control","no-store"); //HTTP 1.1
response.setHeader("Pragma","no-cache"); //HTTP 1.0
response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<%

String MontoPro = new String("");
String txnAuthLevel = (String)session.getAttribute("page.txnAuthLevel");
String txnIMGAutoriz = (String)session.getAttribute("page.txnIMGAutoriz");
Vector v = (Vector)session.getAttribute("page.specialacct");
Hashtable info = (Hashtable)session.getAttribute("info");



String pcdOK = (String)session.getAttribute("pcd");
Hashtable datasession = (Hashtable)session.getAttribute("page.datasession");
int k;
String noefec = (String)datasession.get("noefec");
%>
<html>
<head>
<title>Datos de la Transaccion</title>
<%@include file="style.jsf"%>
<script language="JavaScript">
var formName = 'Mio'
var specialacct = new Array()
<%
 if(!v.isEmpty())
 {
  for(int iv = 0; iv < v.size(); iv++)
	  {out.print("specialacct[" + iv + "]='" + v.get(iv) + "'" + '\n');}
 }
%>
function validate(form)
{
if (document.entry.txtEfectivo)
{<%
      out.println("  if( !top.validate(window, document.entry.txtEfectivo, 'isValMto5503') )");
      out.println("    return");%>
}


if (document.entry.txtCheque)
{<%
      out.println("  if( !top.validate(window, document.entry.txtCheque, 'isValMto5503') )");
      out.println("    return");%>
}
if (document.entry.txtCuenta)
{<%
      out.println("  if( !top.validate(window, document.entry.txtCuenta, 'isValMto5503') )");
      out.println("    return");%>
}
if (document.entry.txtDocsCI)
{<%
      out.println("  if( !top.validate(window, document.entry.txtDocsCI, 'isValMto5503') )");
      out.println("    return");%>
}
if (document.entry.txtRemesas)
{<%
      out.println("  if( !top.validate(window, document.entry.txtRemesas, 'isValMto5503') )");
      out.println("    return");%>
}




if(document.entry.txtEfectivo && document.entry.txtDocsCI)
{
<%
      out.println("  if( !top.validate(window, document.entry.txtEfectivo, 'Valid5503') )");
      out.println("    return");%>
}
<%
      out.println("  if( !top.validate(window, document.entry.subtotal, 'ValSum') )");
      out.println("    return");
      out.println("  if( !top.validate(window, document.entry.txns, 'FlujoRAP') )");
      out.println("    return");%>

if (document.entry.txtRemesas)
{<%
      out.println("  if( !top.validate(window, document.entry.txtRemesas, 'ValRem') )");
      out.println("    return");%>
}

form.validateFLD.value = '1'




<% if(!txnAuthLevel.equals("1") && !txnIMGAutoriz.equals("1"))
	{
    out.println("  form.submit();");
    out.println("var obj = document.getElementById('btnCont');"); 
    out.println("obj.onclick = new Function('return false');");   
	}   
%>
}
function AuthoandSignature(entry)
{
 var Cuenta;
 if(entry.validateFLD.value != '1')
  return
 if(entry.needAutho.value == '1' && entry.AuthoOK.value != '1')
 {
  top.openDialog('../ventanilla/paginas/authoriz.jsp', 220, 130, 'top.setPrefs()', entry)
 }
 if(entry.needSignature.value == '1' && entry.SignField.value != '')
{
  Cuenta = eval('entry.' + entry.SignField.value + '.value')
  top.openModalDialog('<%=request.getContextPath()%>/servlet/ventanilla.FirmasServlet?txtDDACuenta=' + Cuenta, 710, 430, 'top.setPrefs2()', entry)
 }
}
</script>
</head>

<body bgcolor="#FFFFFF" onload="top.setFieldFocus(window.document)">
<br>
<% 
   out.print("<form name=\"entry\" action=\"RedirectorSatR.jsp\" method=\"post\" ");
   if(txnAuthLevel.equals("1"))
    out.print("onSubmit=\"return validate(this)\"");
   out.println(">");
%>

<h1><%=session.getAttribute("page.txnImage")%></h1>

<h3>
5503  Desglose de Abono por Cobranza Especial
</h3>




<% if (pcdOK.equals("1"))
{%>
<table border="0">
<%if(!(noefec.equals("1")))
{%>
	<tr><td align="right">Efectivo :</td><td>&nbsp;&nbsp;</td>
	<td><input type="text" name="txtEfectivo" size="16" maxlength="13" onBlur="top.validate(window, this, 'isValMto5503')" onKeyPress="top.keyPressedHandler(window, this, 'isValMto5503')"></td></tr>
<%}%>
<tr><td align="right">Documentos <%=(String)session.getAttribute("identidadApp")%> :</td><td>&nbsp;&nbsp;</td>
<td><input type="text" name="txtCheque" size="16" maxlength="13" onBlur="top.validate(window, this, 'isValMto5503')" onKeyPress="top.keyPressedHandler(window, this, 'isValMto5503')"></td>
<tr><td align="right">Cobro Inmediato :</td><td>&nbsp;&nbsp;</td>
<td><input type="text" name="txtDocsCI" size="16" maxlength="13" onBlur="top.validate(window, this, 'isValMto5503')" onKeyPress="top.keyPressedHandler(window, this, 'isValMto5503')"></td>
<tr><td align="right">Remesas :</td><td>&nbsp;&nbsp;</td>
<td><input type="text" name="txtRemesas" size="16" maxlength="13" onBlur="top.validate(window, this, 'isValMto5503')" onKeyPress="top.keyPressedHandler(window, this, 'isValMto5503')"></td>
<%}%>

<% if (pcdOK.equals("2"))
{
String bital    =  info.get("FP1").toString();
String cobroi   =  info.get("FP2").toString();
String remesas  =  info.get("FP3").toString();
%>

<table border="0">
<%	if(bital.equals("S"))
	{%>
		<tr><td align="right">Efectivo :</td><td>&nbsp;&nbsp;</td>
		<td><input type="text" name="txtEfectivo" size="14" maxlength="13" onBlur="top.validate(window, this, 'isValMto5503')" onKeyPress="top.keyPressedHandler(window, this, 'isValMto5503')"></td></tr>
		<tr><td align="right">Documentos <%=(String)session.getAttribute("identidadApp")%> :</td><td>&nbsp;&nbsp;</td>
		<td><input type="text" name="txtCheque" size="14" maxlength="13" onBlur="top.validate(window, this, 'isValMto5503')" onKeyPress="top.keyPressedHandler(window, this, 'isValMto5503')"></td></tr>
<%	}
	if(cobroi.equals("S"))
	{%>
		<tr><td align="right">Cobro Inmediato :</td><td>&nbsp;&nbsp;</td>
		<td><input type="text" name="txtDocsCI" size="16" maxlength="13" onBlur="top.validate(window, this, 'isValMto5503')" onKeyPress="top.keyPressedHandler(window, this, 'isValMto5503')"></td></tr><%
	}
	if(remesas.equals("S"))
	{%>
		<tr><td align="right">Remesas :</td><td>&nbsp;&nbsp;</td>
		<td><input type="text" name="txtRemesas" size="16" maxlength="13" onBlur="top.validate(window, this, 'isValMto5503')" onKeyPress="top.keyPressedHandler(window, this, 'isValMto5503')"></td>
		</tr><%
	}
} 

%>

<tr><td align="right"><b>Total :</b></td><td>&nbsp;&nbsp;</td>

<td><b><%=(String)datasession.get("monto")%></b></td></tr>
</table>
<p>
<p>



<input type="hidden" name="subtotal">
<input type="hidden" name="txns">
<input type="hidden" name="remesas" value="0">
<input type="hidden" name="nservicios" value="1">
<input type="hidden" name="total" value="<%=(String)datasession.get("monto")%>">
<input type="hidden" name="npagos" value="1">
<%
session.setAttribute("entro","1");
Vector FRAP  = new Vector();
  session.setAttribute("FRAP",FRAP);
  
  Stack flujotxn = (Stack)session.getAttribute("page.flujotxn");
  
  
  %>

<input type="hidden" name="iTxn" value="<%= session.getAttribute("page.iTxn") %>">
<input type="hidden" name="oTxn" value="<%= session.getAttribute("page.oTxn") %>">
<input type="hidden" name="cTxn" value="S027">
<input type="hidden" name="sucursal" value="<%= session.getAttribute("branch") %>">
<input type="hidden" name="teller" value="<%= session.getAttribute("teller") %>">
<input type="hidden" name="moneda" value="<%= session.getAttribute("page.moneda")%>">
<input type="hidden" name="registro" value="<%=session.getAttribute("empno")%>">
<input type="hidden" name="tllrlvl" value="<%=session.getAttribute("tellerlevel")%>">
<input type="hidden" name="supervisor" value="<%= session.getAttribute("teller") %>">
<input type="hidden" name="AuthoOK" value="0">
<input type="hidden" name="SignOK" value="0">
<input type="hidden" name="validateFLD" value="0">
<input type="hidden" name="needSignature" value="<%out.print(txnIMGAutoriz);%>">
<input type="hidden" name="needAutho" value="<%out.print(txnAuthLevel);%>">
<input type="hidden" name="SignField" value="<%= session.getAttribute("page.SignField") %>">
<p>
<%
 if(txnAuthLevel.equals("1") || txnIMGAutoriz.equals("1"))
    out.println("<a id='btnCont' href=\"javascript:validate(document.entry);javascript:AuthoandSignature(document.entry)\" ><img src=\"../imagenes/b_continuar.gif\" border=\"0\"></a>");
   else
    out.println("<a id='btnCont' href=\"javascript:validate(document.entry)\"><img src=\"../imagenes/b_continuar.gif\" border=\"0\"></a>");
%>
</form>
</body>
</html>
