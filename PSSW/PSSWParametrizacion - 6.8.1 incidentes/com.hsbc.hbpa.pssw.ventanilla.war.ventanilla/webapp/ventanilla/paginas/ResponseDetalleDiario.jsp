<!-- 
//*************************************************************************************************
//             Funcion: JSP que muestra Detalle de Txns del Diario
//            Elemento: ResponseDetallesDiario.jsp
//      Modificado por: alejandro Gonzalez Castro
//*************************************************************************************************
// CCN - XXXXXXX - 06/07/2004 - Se modifica envio de datos para impresion
// CCN - 4360581 - 12/03/2007 - Se convierten los estilos en jsf y se hace un include en el jsp
//*************************************************************************************************
-->
<%@ page session="true" import="java.util.*"%>
<%
response.setHeader("Cache-Control","no-store"); //HTTP 1.1
response.setHeader("Pragma","no-cache"); //HTTP 1.0
response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<%
	String cajero = (String)request.getParameter("teller");
	String suc = (String)request.getParameter("sucursal");
	String llave = (String)request.getParameter("key");
	ventanilla.com.bital.admin.DiarioDetalle DetalleSQL = new ventanilla.com.bital.admin.DiarioDetalle();
	Hashtable Datos = DetalleSQL.executeDQuery(cajero, suc, llave);
%>
<html>
<head>
	<title>Detalle de Transaccion</title>
	<%@include file="style.jsf"%>
</head>
<body bgcolor="#FFFFFF">
<form name="detalledisplay" action="ResponseDiario.jsp" method="post">
<h1>Detalle de Transacci&oacute;n</h1>
<table border=1>
<tr bgcolor="#CCCCCC">
	<td rowspan="2" align="center">Transacci&oacute;n</td>
	<td colspan="2" align="center">Totales</td>
	<td rowspan="2" align="center">Estatus</td>
	<td rowspan="2" align="center">Consecutivo</td>
	<td rowspan="2" align="center">Fecha<br>Operaci&oacute;n</td>
	<td rowspan="2" align="center">Fecha<br>Efectiva</td>
	<td rowspan="2" align="center">Usuario</td>
	<td rowspan="2" align="center">Supervisor</td>
</tr>
<tr bgcolor="#CCCCCC">
	<td align="center">Cargo</td>
	<td align="center">Abono</td>
</tr>

<tr>
	<td align="center"><%=Datos.get("txn")%></td>
	<td align="center"><%=Datos.get("cargo")%></td>
	<td align="center"><%=Datos.get("abono")%></td>
	<td align="center"><%=Datos.get("estatus")%></td>
	<td align="center"><%=Datos.get("consecutivo")%></td>
	<td align="center"><%=Datos.get("fechaoper")%></td>
	<td align="center"><%=Datos.get("fechaefe")%></td>
	<td align="center"><%=cajero%></td>
	<td align="center"><%=Datos.get("supervisor")%></td>
</tr>
<tr bgcolor="#CCCCCC">
	<td align="center">Moneda</td>
	<td align="center" colspan="3">Cuenta</td>
	<td align="center" colspan="2">Monto</td>
	<td align="center" colspan="3">Efectivo</td>
</tr>
<tr>
<%
  String monedas = (String)Datos.get("divisa");
  if (monedas.equals("N$"))
  {%>
  	 <td align="center">$</td>
  <%} else
    {%>
	<td align="center"><%=Datos.get("divisa")%></td>
  <%
    }%>
	<td align="right" colspan="3"><%=Datos.get("cuenta")%></td>
	<td align="right" colspan="2"><%=Datos.get("monto")%></td>
	<td align="right" colspan="3"><%=Datos.get("efectivo")%></td>
</tr>
<tr><td bgcolor="#CCCCCC">Descripci&oacute;n :</td>
<td colspan="8"><%=Datos.get("descripcion")%></td></tr>
<tr><td bgcolor="#CCCCCC">Referencia :</td>
<td colspan="8"><%=Datos.get("refer")%></td></tr>
<tr><td bgcolor="#CCCCCC">Referencia 1 :</td>
<td colspan="8"><%=Datos.get("refer1")%></td></tr>
<tr><td bgcolor="#CCCCCC">Referencia 2 :</td>
<td colspan="8"><%=Datos.get("refer2")%></td></tr>
<tr><td bgcolor="#CCCCCC">Referencia 3 :</td>
<td colspan="8"><%=Datos.get("refer3")%></td></tr>
<tr><td bgcolor="#CCCCCC">Serial :</td>
<td colspan="8"><%=Datos.get("serial")%></td></tr>
<tr bgcolor="#CCCCCC">
<td align="center" colspan="9">Respuesta</td>
</tr>
<tr><td  colspan="9">
<% 
	String lineas = Datos.get("respuesta").toString();
	int large = lineas.length();
	int nolineas = large / 78;
	int res = large % 78;
	if(res > 0)
		nolineas++;
	String line = "";		
	for(int i=0; i<nolineas; i++) 
	{
    	line = lineas.substring( i*78, Math.min(i*78+77, large));
    	%>
		<%=line%><br>
		<%
	    if(Math.min(i*78+77, large) == large)
      		break;
    }
%>
    </td></tr>    	
<tr><td align="center" colspan="9">
	<a href="javascript:submite()" onmouseover="self.status='Regresar';return true;" onmouseout="window.status='';" alt="Regresar"><img src="../imagenes/b_regresar.gif" border="0"></a>
</td></tr>
</table>
<input type="hidden" name="nopagina" value="<%=request.getParameter("nopaginaorig")%>">
<input type="hidden" name="cajero" value="<%=request.getParameter("teller")%>">
</form>
</body>
<script language="JavaScript">
top.formName = '';
function submite()
{
  document.detalledisplay.submit();
}
</script>
</html>