<!--
//*************************************************************************************************
//             Funcion: JSP para despliege de txns OPEE
//            Elemento: PageBuilderOPEE.jsp
//      Modificado por: Jesus Emmanuel Lopez Rosales
//*************************************************************************************************
// CCN - 4360533 - 20/10/2006 - Se crea jsp para txns de OPEE
// CCN - 4360576 - 09/03/2007 - Modificaciones para proyecto OPMN
// CCN - 4360581 - 12/03/2007 - Se convierten los estilos en jsf y se hace un include en el jsp
// CCN - 4360584 - 23/03/2007 - Modificaciones para proyecto OPMN
//*************************************************************************************************
-->
<%@ page session="true" import="java.util.*"%>
<%
response.setHeader("Cache-Control","no-store"); //HTTP 1.1
response.setHeader("Pragma","no-cache"); //HTTP 1.0
response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">


<%

String MontoPro = new String("");
Vector listaCampos = (Vector)session.getAttribute("page.listcampos");

Vector listaContenidos = (Vector)session.getAttribute("page.listcont");

String txnAuthLevel = (String)session.getAttribute("page.txnAuthLevel");

String txnIMGAutoriz = (String)session.getAttribute("page.txnIMGAutoriz");

Vector v = (Vector)session.getAttribute("page.specialacct");

Hashtable datasession = (Hashtable)session.getAttribute("page.datasession");

/*ALEX ORIA 08062015**/
if (datasession == null)
   datasession = new Hashtable();

String txn = session.getAttribute("page.cTxn").toString();

int k;
int i;

//Asignar valor de idflujo para flujos internos
     if(txn.equals("5159")){
        session.setAttribute("idFlujo","03");
     }else if(txn.equals("5183")){
        session.setAttribute("idFlujo","04");
     }   
     
     
%>

<html>
<head>
  <title>Datos de la Transaccion</title>
  <%@include file="style.jsf"%>
<script language="JavaScript">
<!--

var formName = 'Mio'
var specialacct = new Array()
<%

 if(!v.isEmpty()){
  for(int iv = 0; iv < v.size(); iv++){
   out.print("specialacct[" + iv + "]='" + v.get(iv) + "'" + '\n');
  }
 }
%>
function validate(form)
{
   form.validateFLD.value = '0';
   inputStr = '';
   var bandera = 0;  
   document.entry.needAutho.value = '0';   

<%
  for(i=0; i < listaCampos.size(); i++)
   {
   
    Vector vCampos = (Vector)listaCampos.get(i);
    vCampos = (Vector)vCampos.get(0);
   
   if( vCampos.get(4).toString().length() > 0 && datasession.get(vCampos.get(0)) == null)
    {
     
      out.println("  if( !top.validate(window, document.entry." + vCampos.get(0) + ", '" + vCampos.get(4) +"') )");
      if (vCampos.get(0).toString().equals("txtFolio") && vCampos.get(4).toString().equals("isEmptyStr")) 
      { %>
        if(document.entry.returnform.value == "N"  ) {
           return false;
         }else {
            document.entry.returnform.value = "N";
            return;
         }
    <% }
       else{
            out.println("return;") ;
        }   
      }
      
      
   }
  
%>
  form.validateFLD.value = '1'
  if (document.entry.txtFechaEfect || document.entry.txtFechaError || document.entry.txtFechaCarta)
    document.entry.txtFechaSys.value=top.sysdatef();

  if ( bandera == 0 ) {
 <%
 if(!txnAuthLevel.equals("1") && !txnIMGAutoriz.equals("1")){
    out.println("  form.submit()");
    out.println("var obj = document.getElementById('btnCont');");
    out.println("obj.onclick = new Function('return false');");
    }%>
  }

  var Moneda = '<%= session.getAttribute("page.moneda")%>';

var iMonto =0;
if(document.entry.txtMonto != undefined){
   iMonto = document.entry.txtMonto.value
   
}else{   
   iMonto = document.entry.MontoOPEE.value
}   
   iMonto = iMonto.toString().replace(/\$|\,/g,'');   
   intVal = iMonto.substring(0, iMonto.indexOf("."));
   decVal = iMonto.substring(iMonto.indexOf(".") + 1, iMonto.length);   
   Monto = Number(intVal + decVal);
  
  <%if(txn.equals("5177") || txn.equals("5159") || txn.equals("5183") || txn.equals("5179")){ %>
      if((Monto > 1000000 && Moneda == '02') || (Monto > 10000000 && Moneda == '01')){
         bandera = 1     
       }else{
         bandera = 0
       }
  <%}%>        

  	if ( bandera == 1 ) {                       // Pedir autorizacion
        form.onSubmit="return validate(document.entry);"
        document.entry.needAutho.value = '1';
        AuthoandSignature(document.entry);
  	}
  	
  	if(entry.needAutho.value == '1' && entry.AuthoOK.value != '1'){
  		top.openDialog('../ventanilla/paginas/authoriz.jsp', 220, 130, 'top.setPrefs()', entry)
 	}    
}
function AuthoandSignature(entry)
{
 var Cuenta;
 if(entry.returnform != undefined && entry.txtFolio != undefined)
   if(entry.txtFolio.value.length == 0)
     return;

 if(entry.validateFLD.value != '1')
  return


	
	

 if(entry.needAutho.value == '1' && entry.AuthoOK.value != '1')
 {
  top.openDialog('../ventanilla/paginas/authoriz.jsp', 220, 130, 'top.setPrefs()', entry)
 }

 if(entry.needSignature.value == '1' && entry.SignField.value != '')
 {
  Cuenta = eval('entry.' + entry.SignField.value + '.value')
  top.openModalDialog('<%=request.getContextPath()%>/servlet/ventanilla.FirmasServlet?txtDDACuenta=' + Cuenta, 710, 430, 'top.setPrefs2()', entry)
 }
 
}

function Alltrim(inputStr)
{
  var inStr = new String(inputStr)

  while(inStr.charAt(0) == ' ')
    inStr = inStr.substring(1, inStr.length)

  while(inStr.charAt(inStr.length) == ' ')
    inStr = inStr.substring(0, inStr.length-1)

  return inStr
}

//-->
</script>
</head>
<body bgcolor="#FFFFFF" onload="top.setFieldFocus(window.document)">
<br>
<% out.print("<form name=\"entry\" action=\"../servlet/ventanilla.DataServlet\" method=\"post\" ");
   if(txnAuthLevel.equals("1"))
    out.print("onSubmit=\"return validate(this)\"");
   out.println(">");
%>
<h1><%=session.getAttribute("page.txnImage")%></h1>
<h3>
<%out.print(session.getAttribute("page.cTxn") + "  " + (String)session.getAttribute("page.txnlabel"));%>
</h3>
<table border="0" cellspacing="0" class="normaltextblack">
<%
	String moneda = (String)session.getAttribute("page.moneda");
	String moneda_s = (String)session.getAttribute("page.moneda_s");
	
	for(i =  0; i < listaCampos.size(); i++){
		Vector vCampos = (Vector)listaCampos.get(i);
	    vCampos = (Vector)vCampos.get(0);
	    if(!vCampos.get(0).toString().equals("txtCodSeg")){
	    	out.println("<tr>");
	    
	    if(txn.equals("5183")){
	    	if (vCampos.get(0).equals("txtDDACuenta"))
                vCampos.set(2,"N�mero de Cuenta");
           	else if (vCampos.get(0).equals("lstBanCorresp"))
                vCampos.set(2,"Bancos Corresponsales");
	    	else if (vCampos.get(0).equals("txtTipoCambio"))
                vCampos.set(2,"Tipo de Cambio");
	    	else if (moneda.equals("01") && !moneda_s.equals("01")){
	    			if(vCampos.get(0).equals("txtMonto1"))
                		vCampos.set(2,"Monto a Enviar");
                	else if(vCampos.get(0).equals("txtMonto"))
                		vCampos.set(2,"Monto");
                	}
	    	else if (!moneda.equals("01") && moneda_s.equals("01")){
	    			if(vCampos.get(0).equals("txtMonto1"))
                		vCampos.set(2,"Monto");
                	else if(vCampos.get(0).equals("txtMonto"))
                		vCampos.set(2,"Monto a Enviar");
                	}
            }
        else if (vCampos.get(0).equals("txtDDACuenta"))
                vCampos.set(2,"N�mero de Cuenta");
	    else if (vCampos.get(0).equals("txtMonto1"))
                vCampos.set(2,"Monto a Enviar");
                
	     	out.println("  <td>" + vCampos.get(2) + ":</td>");	     	
	     	out.println("  <td width=\"10\">&nbsp;</td>");
	     	
	     	if(datasession.get(vCampos.get(0)) != null){
	     	  if(vCampos.get(0).equals("txtTipoCambio")){
	     	  		String TipCamb = (String)datasession.get(vCampos.get(0));
     	  		    java.text.DecimalFormat reformattedValue = new java.text.DecimalFormat("#,###.0000");
	     	  	    String returnTipCamb = reformattedValue.format((double)(java.lang.Double.parseDouble(TipCamb) / 10000.00));
	     	  	    out.print  ("  <td>" + returnTipCamb + "</td>");
	     	  }
              else 
              		out.print  ("  <td>" + datasession.get(vCampos.get(0)) + "</td>");                            
              if(vCampos.get(2).equals("Monto")){
                out.print("<input name=\"MontoOPEE\" type=\"hidden\" value=\"" + datasession.get(vCampos.get(0)) + "\">");                 
              }             
              out.println("</tr>");
    		}
    		else{
		     	if(!vCampos.get(0).toString().substring(0,3).equals("lst")){
	            	if(vCampos.get(0).toString().substring(0,3).equals("pss")){
	                	out.print  ("  <td><input maxlength=\"" + vCampos.get(3) + "\" name=\"" + vCampos.get(0) + "\" size=\"" + vCampos.get(3) +"\" type=\"Password\"" + " tabindex=\""+(i+1)+"\"");
	            	}
	            	else{
	                	out.print("  <td><input maxlength=\"" + vCampos.get(3) + "\" name=\"" + vCampos.get(0) + "\" size=\"" + vCampos.get(3) +"\" type=\"text\"" + " tabindex=\""+(i+1)+"\"");
	            	}
	
		      		String CampoR = (String)vCampos.get(5);
		      		if(CampoR != null && CampoR.length() > 0){
		        		CampoR = CampoR.trim();
		        		out.print(" value=\"" + CampoR + "\"");
		      		}
	              	String level = (String)session.getAttribute("tellerlevel");
	              	if (vCampos.get(0).toString().equals("txtFechaEfect") && level.equals("1"))
	                out.print(" onfocus=\"blur()\"");
		     	}
		     	else{
		      		out.print("  <td><select name=\"" + vCampos.get(0) + "\" tabindex=\"" + (i+1) + "\" size=\"" + "1" + "\"");
		      		if( vCampos.get(4).toString().length() > 0 ){
		       			if(vCampos.get(0).toString().substring(0,3).equals("rdo"))
	           				out.println(" onClick=\"top.validate(window, this, '" + vCampos.get(4) +"')\">");
		       			else
		         			out.println(" onChange=\"top.validate(window, this, '" + vCampos.get(4) +"')\"  >");
		      		}
		      		else
		       			out.println(">");
		      		for(k=0; k < listaContenidos.size(); k++){
		       			Vector v1Campos = (Vector)listaContenidos.get(k);
		       			for(int j = 0; j < v1Campos.size(); j++){
		         			Vector v1Camposa = (Vector)v1Campos.get(j);
		         			if(v1Camposa.get(0).toString().equals(vCampos.get(0).toString()))
		           				out.println("  <option value=\"" + v1Camposa.get(3) + "\">" + v1Camposa.get(2));
		       			}
		      		}
		      		out.println("  </select></td>");
		     	}
		     	if( vCampos.get(4).toString().length() > 0 && !vCampos.get(0).toString().substring(0,3).equals("lst"))
		      		out.println("  onBlur=\"top.estaVacio(this)||top.validate(window, this, '" + vCampos.get(4) +"')\"  onKeyPress=\"top.keyPressedHandler(window, this, '" + vCampos.get(4) +"')\"   ></td>");
		     	else if(!vCampos.get(0).toString().substring(0,3).equals("lst"))
		      		out.println(">");
		     	out.println("</tr>");
		     }
	    }
	    else{
	    	%><%@include file="fieldLector.jsf"%><%
	     	i = k;
	    }
	}
	%>
</table>
<input type="hidden" name="iTxn" value="<%= session.getAttribute("page.iTxn") %>">
<input type="hidden" name="oTxn" value="<%= session.getAttribute("page.oTxn") %>">
<input type="hidden" name="cTxn" value="<%= session.getAttribute("page.cTxn") %>">
<input type="hidden" name="sucursal" value="<%= session.getAttribute("branch") %>">
<input type="hidden" name="teller" value="<%= session.getAttribute("teller") %>">
<input type="hidden" name="moneda" value="<%= session.getAttribute("page.moneda")%>">
<input type="hidden" name="moneda2" value="<%= session.getAttribute("page.moneda_s")%>">
<input type="hidden" name="registro" value="<%=session.getAttribute("empno")%>">
<input type="hidden" name="tllrlvl" value="<%=session.getAttribute("tellerlevel")%>">
<input type="hidden" name="supervisor" value="<%= session.getAttribute("teller") %>">
<input type="hidden" name="AuthoOK" value="0">
<input type="hidden" name="SignOK" value="0">
<input type="hidden" name="validateFLD" value="0">
<input type="hidden" name="needSignature" value="<%out.print(txnIMGAutoriz);%>">
<input type="hidden" name="needAutho" value="<%out.print(txnAuthLevel);%>">
<input type="hidden" name="SignField" value="<%= session.getAttribute("page.SignField") %>">
<input type="hidden" name="override" value="N">
<input type="hidden" name="txtFees" value="0">
<input type="hidden" name="compania" value="20">
<input type="hidden" name="txtSelectedItem" value="0" >
<input type="hidden" name="returnform" value="N" >
<input type="hidden" name="returnform2" value="N" >

<input type="hidden" name="passw" value="<%=session.getAttribute("password")%>">
<input type="hidden" name="MontoRetiroPesos" value = "<%=session.getAttribute("Retiro.Pesos")%>">
<input type="hidden" name="MontoRetiroDolar" value = "<%=session.getAttribute("Retiro.Dolar")%>">
<p><script>
 if (document.entry.txtFechaEfect)
 {
  document.writeln("<input type=hidden name=txtFechaSys>");
  document.entry.txtFechaEfect.value=top.sysdate();
 }
 if (document.entry.txtFechaError)
 {
  document.writeln("<input type=hidden name=txtFechaSys>");
  document.entry.txtFechaError.value=top.sysdate();
 }
 if (document.entry.txtFechaCarta)
 {
  document.writeln("<input type=hidden name=txtFechaSys>");
  document.entry.txtFechaCarta.value=top.sysdate();
 }
</script>
	<style>
		.invisible
		{
		  display:None;
		}

		.visible
		{
		  display:"";
		}
	</style>


<% if(txnAuthLevel.equals("1") || txnIMGAutoriz.equals("1"))
    out.println("<a id='btnCont' tabindex=\"" + (i+1) + "\" href=\"javascript:document.entry.returnform.value='S';validate(document.entry);javascript:AuthoandSignature(document.entry)\" ><img src=\"../ventanilla/imagenes/b_continuar.gif\" border=\"0\"></a>");
   else
    out.println("<a id='btnCont' tabindex=\"" + (i+1) + "\" href=\"javascript:document.entry.returnform.value='S';validate(document.entry)\"><img src=\"../ventanilla/imagenes/b_continuar.gif\" border=\"0\"></a>");
%>

</form>
</body>
</html>

