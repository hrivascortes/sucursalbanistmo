<!--
//*************************************************************************************************
//             Funcion: JSP que despliegaFormato Cheque de Genrecia
//            Elemento: formatochqcaja.jsp
//          Creado por: Alejandro Gonzalez Castro
// Ultima Modificacion: 03/08/2004
//      Modificado por: Alejandro Gonzalez Castro
//*************************************************************************************************
// CCN - 4360175 - 03/08/2004 - Ajuste size de ventana de impresion
// CCN - 4360189 - 03/09/2004 - Se despliega el monto correcto en la impresion de cheque de gerencia en d�lares txn 4115
// CCN - 4360237 - 05/11/2004 - Se envia la cadena de impresi�n en la sesi�n en vez de hacerlo como parametro
// CCN - 4360424 - 03/02/2006 - Se hacen modificaciones para txns de combinadas y divisas
//*************************************************************************************************
-->

<%@ page import="com.bital.util.Common, java.util.Hashtable, com.bital.util.Currency" %>
<%
response.setHeader("Cache-Control","no-store"); //HTTP 1.1
response.setHeader("Pragma","no-cache"); //HTTP 1.0
response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
%>
<html>
<head>
  <title>Formato de Impresion</title>
<style type="text/css">Body {font-family: Arial, Helv; font-size: 10pt; font-weight: bold; color:#000000}</style>
</head>
<%!


  private String notNull(String value)
  {
    if( value == null )
      return "";
    else
      return value;
  }


%>

<body>
<table border="0" cellspacing="0">
</table>
<hr size="1">
<%
  Hashtable local  = (Hashtable)session.getAttribute("page.datasession");

  String monto       = (String)local.get("txtMonto");
  int moneda         = Integer.parseInt((String)local.get("moneda"));
  String Txn = (String)session.getAttribute("page.oTxn");
  if ( Txn.equals("4115") ) {
      if ( moneda == 1 )
      {
          monto = (String)local.get("txtMonto1");         
          moneda = 2;
      }
      else
          moneda = 1;
  }
  String sucursal    = (String)session.getAttribute("branch");
  sucursal = sucursal.substring(1,sucursal.length());
  java.util.Hashtable datosSucursal = (java.util.Hashtable)session.getAttribute("branchinfo");

  String NomSuc = datosSucursal.get("C_NUM_SUCURSAL").toString().trim() + " " + datosSucursal.get("D_NOM_SUCURSAL").toString().trim();
  String txtNomPlaza = datosSucursal.get("D_NOM_PLAZA").toString().trim();

  String txtConcepto = "";
  if ( local.get("txtCDACuenta")!= null)
     txtConcepto = (String)local.get("txtCDACuenta")+ (String)local.get("txtBeneficiario");
  else {
      if ( local.get("txtMotivo") != null)
         txtConcepto = (String)local.get("txtMotivo");
      else
         txtConcepto = notNull((String)local.get("txtDDACuenta"));
  }
%>
<table border="0" cellspacing="0" width="80%">
<tr>
<%if(Txn.equals("4157") || Txn.equals("0061")|| Txn.equals("4155"))
  {
   if(Txn.equals("4157")){
%>
  	<td><font class="label">Cheque de Gerencia No. </font><i><%= local.get("txtSerialExp") %></i></td>
<%
   }if(Txn.equals("0061")||Txn.equals("4155")){
%>
    <td><font class="label">Cheque de Gerencia No. </font><i><%= local.get("txtSerialCheq") %></i></td>
<%
   }
}else
  {%>
  <td><font class="label">Cheque de Gerencia No. </font><i><%= local.get("txtSerial") %></i></td>
<%}%>
  <td>&nbsp;</td>
  <td><i><%= txtNomPlaza%></i></td>
</tr>
<tr>
  <td colspan="2">&nbsp;</td>
  <td><font class="label">Fecha: </font><i><%= Common.getCurrentDay() %></i></td>
</tr>
<tr>
  <td class="label">Paguese por este cheque a:</td>
  <td>&nbsp;</td>
  <td class="label">Importe:</td>
</tr>
<tr>
  <td><i><%= local.get("txtBeneficiario") %></i></td>
  <td>&nbsp;</td>
  <td align="right"><i>$ <%=monto%></i></td>
</tr>
<tr>
  <td class="label">La cantidad de:</td>
  <td colspan="2">&nbsp;</td>
</tr>
<tr>
  <td colspan="3"><i><%= Currency.convert(monto, moneda) %></i></td>
</tr>
<tr>
  <td class="label">Con cargo a:</td>
  <td colspan="2">&nbsp;</td>
</tr>
<tr>
  <td class="label">Cuenta: <i><%= notNull((String)local.get("txtDDACuenta")) %></i></td>
  <td>&nbsp;</td>
  <td><font class="label">Ofna: </font><i><%=NomSuc%></i></td>
</tr>
<tr>
  <td class="label">Nombre:</td>
  <td>&nbsp;</td>
  <td><font class="label">Num: </font><%=sucursal%><i></i></td>
</tr>
<tr>
  <td>&nbsp;</td>
  <td class="label">Concepto:</td>
  <td><%= notNull((String)local.get("txtMotivo")) %></td>
</tr>
</table>

<form name="Txns" action="<%=request.getContextPath()%>/servlet/ventanilla.PageServlet" method="post">

<%

   String Serial = "";
   if(Txn.equals("4157"))
   		Serial = (String)local.get("txtSerialExp");
   else if(Txn.equals("0061") || Txn.equals("4155"))
   		Serial = (String)local.get("txtSerialCheq");
   else
   		Serial = (String)local.get("txtSerial");
   
   String txtCadImpresion = "CHQCAJA~" + local.get("txtBeneficiario") + "~" + moneda + "~" + monto + "~"+ Serial + "~" + NomSuc + "~" + txtConcepto + "~" + txtNomPlaza + "~";
   session.setAttribute("txtCadImpresion",txtCadImpresion);
   out.println("<a href=\"javascript:top.openDialog('../servlet/ventanilla.ImprimirServlet', 160, 120, 'top.setPrefs4()', document.Txns);\" onmouseover=\"self.status='Aceptar';return true;\" onmouseout=\"window.status='';\"><img src=\"../ventanilla/imagenes/b_aceptar.gif\" border=\"0\"></a>");
%>
  <input type="hidden" name="transaction" value="<%= session.getAttribute("page.iTxn") %>">
  <input type="hidden" name="transaction1" value="<%= session.getAttribute("page.oTxn") %>">
</form>
<%--
  // Secuencia de comprobante fiscal
<% if( local.get("txtComision") != null )
   { %>
<form action="cobrocom.jsp" method="post">
<input type="image" src="../ventanilla/imagenes/b_aceptar.gif" border="0">
</form>
<% } %>
--%>
</body>
</html>
