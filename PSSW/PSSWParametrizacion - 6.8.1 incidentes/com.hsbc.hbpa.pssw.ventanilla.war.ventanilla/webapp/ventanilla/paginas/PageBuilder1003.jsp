<!--
//*************************************************************************************************
//             Funcion: JSP que presenta las opciones para la txn 0572
//            Elemento: PageBuilder1003.jsp
//          Creado por: Alejandro Gonzalez Castro
//      Modificado por: Juvenal R. Fernandez V
//*************************************************************************************************
//CCN - 4360268 - 04/02/2005 - Se realizan adecuaciones para eliminar Agencia NY
//CCN - 4360334 - 17/06/2005 - Se convierte fieldLector a fragmento
//CCN - 4360488 - 09/06/2006 - Se modifica monto limite a depositar de 250,000.00 a 90,000.00
//CCN - 4360581 - 12/03/2007 - Se convierten los estilos en jsf y se hace un include en el jsp
//CCN - 4620021 - 17/10/2007 - Se agregan validaciones para montos mayores a 10000 dlls, Humberto Balleza
//CCN - 4620021 - 05/06/2015 - ALEX ORIA - Se agregan funcion para eliminar la solicitud de autorizacion para las transacciones 1001,1003,ARP Y P001
//*************************************************************************************************
-->

<%@ page session="true" import="java.util.*,
	ventanilla.GenericClasses"%>
<%
response.setHeader("Cache-Control","no-store"); //HTTP 1.1
response.setHeader("Pragma","no-cache"); //HTTP 1.0
response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<%
String MontoPro = new String("");
Vector listaCampos = (Vector)session.getAttribute("page.listcampos");
Vector listaContenidos = (Vector)session.getAttribute("page.listcont");
String txnAuthLevel = (String)session.getAttribute("page.txnAuthLevel");
String txnIMGAutoriz = (String)session.getAttribute("page.txnIMGAutoriz");
Vector v = (Vector)session.getAttribute("page.specialacct");
String val1003 = (String)session.getAttribute("bandera1003");
Hashtable datasession = (Hashtable)session.getAttribute("page.datasession");
int k;

String blag = (String)session.getAttribute("blagP001");
String montoKronner=null;
String ctaRepago=null;
session.setAttribute("pasoP001","NO");
if(blag!= null && blag.equals("0")){
	 ventanilla.GenericClasses gc = new ventanilla.GenericClasses();
	 session.setAttribute("validaP1","P001");	
	 //montoKronner=(String)session.getAttribute("montoCtoKroner");
	 ctaRepago=(String)session.getAttribute("ctaRepago");	 
	 montoKronner = request.getParameter("hdMtoLetra");
	 //montoKronner=gc.FormatAmount(montoKronner);
	 session.setAttribute("montoKronner",montoKronner);
	 
}
else 
	session.setAttribute("validaP1","");	
%>
<html>
<head>
  <%@include file="style.jsf"%>
  <title>Datos de la Transaccion</title>
<script  language="JavaScript">
<!--
var formName = 'Mio'
var specialacct = new Array()
var bandera = 0;

<%
if(!v.isEmpty())
{
  for(int iv = 0; iv < v.size(); iv++)
  {
    out.print("specialacct[" + iv + "]='" + v.get(iv) + "'" + '\n');
  }
}
%>

function validate(form)
{
<%
  for(int i=0; i < listaCampos.size(); i++)
  {
    Vector vCampos = (Vector)listaCampos.get(i);
    vCampos = (Vector)vCampos.get(0);
    if( vCampos.get(4).toString().length() > 0 )
    {
      out.println("  if( !top.validate(window, document.entry." + vCampos.get(0) + ", '" + vCampos.get(4) +"') )");
      out.println("    return;");
    }
  }
  
  if(val1003 !=null && val1003.equals("SI"))
  {	
  	  //out.println("  if( !top.validate(window, document.entry." + vCampos.get(0) + ", '" + vCampos.get(4) +"') )");
      //out.println("    return;");
      %>
			var eP001 = document.entry.txtEfectivo.value;
			var cP001 = document.entry.txtCheque.value;
			//alert("El monto en efectivo es: "+eP001 + "El monto en cheques es: "+cP001);
			//alert("El monto kroner es: <%=montoKronner%>");
			
			inputStr = Alltrim(eP001);
     		inputStr = inputStr.toString().replace(/\$|\,/g,'');
     		intVal = inputStr.substring(0, inputStr.indexOf("."));
     		decVal = inputStr.substring(inputStr.indexOf(".") + 1, inputStr.length);
    	 	montoe = Number(intVal + decVal);
    	 	
    	 	inputStr = Alltrim(cP001);
     		inputStr = inputStr.toString().replace(/\$|\,/g,'');
     		intVal = inputStr.substring(0, inputStr.indexOf("."));
     		decVal = inputStr.substring(inputStr.indexOf(".") + 1, inputStr.length);
    	 	montoc = Number(intVal + decVal);
    	 	
    	 	var mKroner = "<%=montoKronner%>";
    	 	inputStr = Alltrim(mKroner);
     		inputStr = inputStr.toString().replace(/\$|\,/g,'');
     		intVal = inputStr.substring(0, inputStr.indexOf("."));
     		decVal = inputStr.substring(inputStr.indexOf(".") + 1, inputStr.length);
    	 	montok = Number(intVal + decVal);
    	 	
    	 	//alert(montok);
    	 	//alert(montoe+montoc);
    	 	
    	 	if(montok != montoe+montoc)
    	 		{
    	 			alert("La combinación de montos es incorrecta para el pago especificado corregir");
    	 			return;
    	 		}

			
      <%
  }
  				
%>
	
  form.validateFLD.value = '1'
  if (document.entry.txtFechaEfect || document.entry.txtFechaError || document.entry.txtFechaCarta)
    document.entry.txtFechaSys.value = top.sysdatef();

  if ( form.iTxn.value == "1003" )
  {
     inputStr = Alltrim(form.txtEfectivo.value);
     inputStr = inputStr.toString().replace(/\$|\,/g,'');
     intVal = inputStr.substring(0, inputStr.indexOf("."));
     decVal = inputStr.substring(inputStr.indexOf(".") + 1, inputStr.length);
     monto = Number(intVal + decVal);
     //inputStr = Alltrim(form.txtCheque.value);
     //inputStr = inputStr.toString().replace(/\$|\,/g,'');
     //intVal = inputStr.substring(0, inputStr.indexOf("."));
     //decVal = inputStr.substring(inputStr.indexOf(".") + 1, inputStr.length);
     //monto = monto + Number(intVal + decVal);
     if ( monto >= 1000000 ) 
         bandera = 1;
     else
         bandera = 0;
         
      
    if(isTxnWithoutValidateXMonto()) { 
      bandera = 0; 
    }  // TCS 20150604 ALEX ORIA  
     
     
     if ( bandera == 1 )
     {                       // Pedir autorizacion
        form.onSubmit = "return validate(document.entry);";
        document.entry.needAutho.value = '1';
        AuthoandSignature(document.entry);
     }
  }
  
  if ( bandera == 0 ) {
<%

if(!txnAuthLevel.equals("1") && !txnIMGAutoriz.equals("1")){
  
  	out.println("  form.submit()");
        out.println("var obj = document.getElementById('btnCont');");
        out.println("obj.onclick = new Function('return false');");
}

  
%> 
  }
}


function AuthoandSignature(entry)
{
  var Cuenta;
  if(entry.validateFLD.value != '1')
    return;
    
  if(entry.needAutho.value == '1' && entry.AuthoOK.value != '1')
  {
    top.openDialog('../ventanilla/paginas/authoriz.jsp', 220, 130, 'top.setPrefs()', entry);
  }
  
  if(entry.needSignature.value == '1' && entry.SignField.value != '')
  {
    Cuenta = eval('entry.' + entry.SignField.value + '.value');
    top.openModalDialog('<%=request.getContextPath()%>/servlet/ventanilla.FirmasServlet?txtDDACuenta=' + Cuenta, 710, 430, 'top.setPrefs2()', entry);
  }
}


function Alltrim(inputStr)
{
  var inStr = new String(inputStr);

  while(inStr.charAt(0) == ' ')
    inStr = inStr.substring(1, inStr.length);

  while(inStr.charAt(inStr.length) == ' ')
    inStr = inStr.substring(0, inStr.length-1);

  return inStr;
}

function isTxnWithoutValidateXMonto(){

	//Requerimiento 011 Se adiciona campo para validar TXN por Monto - Validacion TXN 1001 1003 P001 ARP 

	stTxnVar      ='<%= (String)session.getAttribute("txnsWithOutValidateMonto")%>'
	stTxnVarForm  ='<%= session.getAttribute("page.iTxn").toString()%>'
	stcTxnVarForm ='<%= session.getAttribute("page.cTxn").toString()%>'
   
    var dato = stTxnVar.split('*');
	
	for( pos = 0; pos < dato.length ; pos++){
		if( dato[pos] == stTxnVarForm) { //TXN 1001 1003 P001 ARP
			  	return true;
		 }
		  
	}
	 	return false;
}


//-->
</script>
</head>
<body bgcolor="#FFFFFF" onload="top.setFieldFocus(window.document)">
<br>
<form name="entry" action="../servlet/ventanilla.DataServlet" method="post"
<% 
if(txnAuthLevel.equals("1"))
  out.print("onSubmit=\"return validate(this)\"");
%>
>
<h1><%=session.getAttribute("page.txnImage")%></h1>
<h3>
<%
out.print(session.getAttribute("page.cTxn") + "  " + (String)session.getAttribute("page.txnlabel"));
%>
</h3>
<%
int i = 0;  // Contador de campos
%>
<table border="0" cellspacing="0">
<%
  for(i = 0; i < listaCampos.size(); i++)
  {
    Vector vCampos = (Vector)listaCampos.get(i);
    vCampos = (Vector)vCampos.get(0);
    
    if(!vCampos.get(0).toString().equals("txtCodSeg"))
    {
      out.println("<tr>");
      out.println("  <td>" + vCampos.get(2) + ":</td>");
      out.println("  <td width=\"10\">&nbsp;</td>");
      
      if(!vCampos.get(0).toString().substring(0,3).equals("lst"))
      {
        if(vCampos.get(0).toString().substring(0,3).equals("pss"))
        {
	  out.print  ("  <td><input maxlength=\"" + vCampos.get(3) + "\" name=\"" + vCampos.get(0) + "\"" + 
	              " size=\"" + vCampos.get(3) +"\" type=\"Password\" tabindex=\"" + (i + 1) + "\" ");
	} else {
          out.print("  <td><input maxlength=\"" + vCampos.get(3) + "\" name=\"" + vCampos.get(0) + "\"" + 
                    " size=\"" + vCampos.get(3) +"\" type=\"text\" tabindex=\"" + (i + 1) + "\" ");
        }
        
	String CampoR = (String)vCampos.get(5);
	//System.out.println("|" + vCampos.get(0) + "| " + ctaRepago + " " + CampoR + " " + CampoR.length());
	CampoR = CampoR.trim();
	//System.out.println(vCampos.get(0) + " " + ctaRepago + " " + CampoR + " " + CampoR.length());
	if(CampoR != null && CampoR.length() > 0)
	{
	  out.print(" value=\"" + CampoR + "\"");
	}
	else if(ctaRepago != null && vCampos.get(0).equals("txtDDACuenta"))
	{
		out.print(" value=\"" + ctaRepago + "\"" );
		session.setAttribute("txnProcess","00");
		session.setAttribute("pasoP001","SI");
	}
	else if(ctaRepago != null && vCampos.get(0).equals("txtEfectivo"))
	{
		out.print(" value=\"" + montoKronner + "\"");	
		ctaRepago=null;
		montoKronner=null;
		
	}
      } else {
	out.print("  <td><select name=\"" + vCampos.get(0) + "\" size=\"1\" tabindex=\"" + (i + 1) + "\"");
	
	// 2004-01-24 13:02 ARM: A los controles 'select' no se les agrega el evento 'onKeyPress'
	if( vCampos.get(3).toString().length() > 0 )
	{
	  if(vCampos.get(0).toString().substring(0,3).equals("rdo"))
	    out.println(" onClick=\"top.validate(window, this, '" + vCampos.get(4) +"')\">");
	  else
	    out.println(" onChange=\"top.validate(window, this, '" + vCampos.get(4) +"')\">");
        } else
	  out.println(">");
	  
	for(k = 0; k < listaContenidos.size(); k++)
	{
	  Vector v1Campos = (Vector)listaContenidos.get(k);
	  for(int j = 0; j < v1Campos.size(); j++)
	  {
	    Vector v1Camposa = (Vector)v1Campos.get(j);
	    if(v1Camposa.get(0).toString().equals(vCampos.get(0).toString()))
	      out.println("  <option value=\"" + v1Camposa.get(3) + "\">" + v1Camposa.get(2));
	  }
	}
	
	out.println("  </select></td>");
      }
	     
      if( vCampos.get(4).toString().length() > 0 && !vCampos.get(0).toString().substring(0,3).equals("lst"))
	out.println(" onBlur=\"top.estaVacio(this)||top.validate(window, this, '" + vCampos.get(4) +"')\"" + 
	            " onKeyPress=\"top.keyPressedHandler(window, this, '" + vCampos.get(4) +"')\" ></td>");
      else if(!vCampos.get(0).toString().substring(0,3).equals("lst"))
	out.println(">");
	
      out.println("</tr>");
    } else {
      %><%@include file="fieldLector.jsf"%><% 
      i = k;
    }
  }
%>
</table>
<input type="hidden" name="iTxn" value="<%= session.getAttribute("page.iTxn") %>">
<input type="hidden" name="oTxn" value="<%= session.getAttribute("page.oTxn") %>">
<input type="hidden" name="cTxn" value="<%= session.getAttribute("page.cTxn") %>">
<input type="hidden" name="sucursal" value="<%= session.getAttribute("branch") %>">
<input type="hidden" name="teller" value="<%= session.getAttribute("teller") %>">
<input type="hidden" name="moneda" value="<%= session.getAttribute("page.moneda")%>">
<input type="hidden" name="registro" value="<%=session.getAttribute("empno")%>">
<input type="hidden" name="tllrlvl" value="<%=session.getAttribute("tellerlevel")%>">
<input type="hidden" name="supervisor" value="<%= session.getAttribute("teller") %>">
<input type="hidden" name="AuthoOK" value="0">
<input type="hidden" name="SignOK" value="0">
<input type="hidden" name="validateFLD" value="0">
<input type="hidden" name="needSignature" value="<%out.print(txnIMGAutoriz);%>">
<input type="hidden" name="needAutho" value="<%out.print(txnAuthLevel);%>">
<input type="hidden" name="SignField" value="<%= session.getAttribute("page.SignField") %>">
<input type="hidden" name="override" value="N">
<input type="hidden" name="ctaRepago" value="20">
<input type="hidden" name="montoKroner" value="20">
<input type="hidden" name="compania" value="20">
<p>
<script>
 
if (document.entry.txtFechaEfect)
{
  document.writeln("<input type=\"hidden\" name=\"txtFechaSys\">");
  document.entry.txtFechaEfect.value = top.sysdate();
}

if (document.entry.txtFechaError)
{
  document.writeln("<input type=\"hidden\" name=\"txtFechaSys\">");
  document.entry.txtFechaError.value = top.sysdate();
}

if (document.entry.txtFechaCarta)
{
  document.writeln("<input type=\"hidden\" name=\"txtFechaSys\">");
  document.entry.txtFechaCarta.value = top.sysdate();
}
</script>
<% if(txnAuthLevel.equals("1") || txnIMGAutoriz.equals("1"))
     out.println("<a id='btnCont' tabindex=\"" + (i + 1) + "\" href=\"javascript:validate(document.entry);javascript:if(!isTxnWithoutValidateXMonto()){AuthoandSignature(document.entry);}\" ><img src=\"../ventanilla/imagenes/b_continuar.gif\" border=\"0\"></a>");
   else
     out.println("<a id='btnCont' tabindex=\"" + (i + 1) + "\" href=\"javascript:validate(document.entry)\"><img src=\"../ventanilla/imagenes/b_continuar.gif\" border=\"0\"></a>");
	String pruebaPso=(String)session.getAttribute("pasoP001");
	//System.out.println(pruebaPso);
%>
</form>
</body>
</html>
