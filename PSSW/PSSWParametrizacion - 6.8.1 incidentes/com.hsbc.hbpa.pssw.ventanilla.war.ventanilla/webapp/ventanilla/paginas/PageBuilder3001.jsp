<!--
//*************************************************************************************************
//             Funcion: JSP que presenta las opciones de la txn 3001
//            Elemento: PageBuilder3001.jsp
//          Creado por: Alejandro Gonzalez Castro
//      Modificado por: Marco Lara Palacios
//*************************************************************************************************
// CCN - 4360275 - 18/02/2005 - WAS 5.1
// CCN - 4360581 - 12/03/2007 - Se convierten los estilos en jsf y se hace un include en el jsp
//CCN -         - 20/11/2007 - Se agregan validaciones para que las inversiones no acepten Cobro Inmediato, Humberto Balleza
//*************************************************************************************************
-->

<%@ page session="true" import="java.util.*"%>
<%
response.setHeader("Cache-Control","no-store"); //HTTP 1.1
response.setHeader("Pragma","no-cache"); //HTTP 1.0
response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<%
Vector lcs = (Vector)session.getAttribute("page.listcampos");
Vector lct = (Vector)session.getAttribute("page.listcont");
String txnAuthLevel = (String)session.getAttribute("page.txnAuthLevel");
String txnIMGAutoriz = (String)session.getAttribute("page.txnIMGAutoriz");
Hashtable datasession = (Hashtable)session.getAttribute("page.datasession");

%>
<html>
<head>
  <%@include file="style.jsf"%>
  <title>Datos de la Transaccion
 <%!
   private String getItem(String newCadNum, int newOption)
  {
   int iPIndex = newCadNum.indexOf("CONSECUTIVO:") + 12, NumSaltos, Num = 0, Ban = 0;
   String nCadNum = new String("");
   NumSaltos = newOption;
   while(Num < NumSaltos)
   {
    if(newCadNum.charAt(iPIndex) == 0x20)
    {
     while(newCadNum.charAt(iPIndex) == 0x20)
      iPIndex++;
     Num++;
    }
    else
     while(newCadNum.charAt(iPIndex) != 0x20)
      iPIndex++;
   }
   while(newCadNum.charAt(iPIndex) != 0x20)
    nCadNum = nCadNum + newCadNum.charAt(iPIndex++);

   return nCadNum;
  }
 %>

  </title>
<script   language="JavaScript">
<!--
var formName = 'Mio'
function validate(form)
{
<%
  for(int i=0; i < lcs.size(); i++)
   {
    Vector vCampos = (Vector)lcs.get(i);
    vCampos = (Vector)vCampos.get(0);
    if( vCampos.get(4).toString().length() > 0 && datasession.get(vCampos.get(0)) == null)
    {
      out.println("  if( !top.validate(window, document.entry." + vCampos.get(0) + ", '" + vCampos.get(4) +"') )");
      out.println("    return");
    }
   }
%>
  form.validateFLD.value = '1'

  if (document.entry.txtFechaEfect || document.entry.txtFechaError || document.entry.txtFechaCarta)
    document.entry.txtFechaSys.value=top.sysdatef();

<%
    out.println("  form.submit()");
    out.println("var obj = document.getElementById('btnCont');"); 
    out.println("obj.onclick = new Function('return false');");   
    
%>
}
function AuthoandSignature(entry)
{
 var Cuenta;
 if(entry.validateFLD.value != '1')
  return
 if(entry.needAutho.value == '1' && entry.AuthoOK.value != '1'){
  top.openDialog('../paginas/authoriz.jsp', 220, 130, 'top.setPrefs()', entry)
 }

 if(entry.needSignature.value == '1' && entry.SignField.value != ''){
  Cuenta = eval('entry.' + entry.SignField.value + '.value')
  top.openModalDialog('<%=request.getContextPath()%>/servlet/ventanilla.FirmasServlet?txtDDACuenta=' + Cuenta, 710, 430, 'top.setPrefs2()', entry)
 }
 }
//-->
</script>
</head>
<body onload="top.setFieldFocus(window.document)">
<br>
<% out.print("<form name=\"entry\" action=\"../servlet/ventanilla.DataServlet\" method=\"post\" ");
   if(txnAuthLevel.equals("1"))
    out.print("onSubmit=\"return validate(this)\"");
   out.println(">");
%>

<h1><%=session.getAttribute("page.txnImage")%></h1>

<h3>
<%out.print(session.getAttribute("page.cTxn") + "  " +
                (String)session.getAttribute("page.txnlabel"));%>
</h3>
<table border="0" cellspacing="0">
<%
  for(int i =  0; i < lcs.size(); i++)
   {
    Vector vCampos = (Vector)lcs.get(i);
    vCampos = (Vector)vCampos.get(0);
    if(vCampos.get(0).toString().equals("txtMontoCobro") || vCampos.get(0).toString().equals("txtMontoRemesa")){
    }
    else{
     out.println("<tr>");
     out.println("  <td>" + vCampos.get(2) + ":</td>");
     out.println("  <td width=\"10\">&nbsp;</td>");
     if(datasession.get(vCampos.get(0)) != null){
       out.print  ("  <td>" + datasession.get(vCampos.get(0)) + "</td");
      }
     else{
        out.print  ("  <td><input maxlength=\"" + vCampos.get(3) + "\" name=\"" + vCampos.get(0) + "\" size=\"" + vCampos.get(3) +"\" type=\"text\"" + "\" tabindex=\""+i+"\"");
        String CampoR = (String)vCampos.get(5);
        if(CampoR != null && CampoR.length() > 0){
           CampoR = CampoR.trim();
           out.print(" value=\"" + CampoR + "\"");
        }
     }
     if( vCampos.get(4).toString().length() > 0 && !vCampos.get(0).toString().substring(0,3).equals("lst") && datasession.get(vCampos.get(0)) == null)
      out.println(" onChange=\"top.validate(window, this, '" + vCampos.get(4) +"')\"></td>");
     else if(!vCampos.get(0).toString().substring(0,3).equals("lst"))
      out.println(">");
     out.println("</tr>");
     }
    }
    
%></table>
<input type="hidden" name="iTxn" value="<%= session.getAttribute("page.iTxn") %>">
<input type="hidden" name="oTxn" value="<%= session.getAttribute("page.oTxn") %>">
<input type="hidden" name="cTxn" value="<%= session.getAttribute("page.cTxn") %>">
<input type="hidden" name="sucursal" value="<%= session.getAttribute("branch") %>">
<input type="hidden" name="teller" value="<%= session.getAttribute("teller") %>">
<input type="hidden" name="moneda" value="<%= session.getAttribute("page.moneda")%>">
<input type="hidden" name="registro" value="<%=session.getAttribute("empno")%>">
<input type="hidden" name="tllrlvl" value="<%=session.getAttribute("tellerlevel")%>">
<input type="hidden" name="supervisor" value="<%= session.getAttribute("teller") %>">
<input type="hidden" name="AuthoOK" value="0">
<input type="hidden" name="SignOK" value="0">
<input type="hidden" name="validateFLD" value="0">
<input type="hidden" name="needSignature" value="<%out.print(txnAuthLevel);%>">
<input type="hidden" name="needAutho" value="<%out.print(txnIMGAutoriz);%>">
<input type="hidden" name="SignField" value="<%= session.getAttribute("page.SignField") %>">
<input type="hidden" name="compania" value="20">
<p>
<script>
 if (document.entry.txtFechaEfect)
 {
  document.writeln("<input type=hidden name=txtFechaSys>");
  document.entry.txtFechaEfect.value=top.sysdate();
 }
 if (document.entry.txtFechaError)
 {
  document.writeln("<input type=hidden name=txtFechaSys>");
  document.entry.txtFechaError.value=top.sysdate();
 }
 if (document.entry.txtFechaCarta)
 {
  document.writeln("<input type=hidden name=txtFechaSys>");
  document.entry.txtFechaCarta.value=top.sysdate();
 }
</script>
<% if(txnAuthLevel.equals("1") || txnIMGAutoriz.equals("1"))
    out.println("<a id='btnCont' href=\"javascript:validate(document.entry);javascript:AuthoandSignature(document.entry)\"><img src=\"../ventanilla/imagenes/b_continuar.gif\" border=\"0\"></a>");
   else
    out.println("<a id='btnCont' href=\"javascript:validate(document.entry)\"><img src=\"../ventanilla/imagenes/b_continuar.gif\" border=\"0\"></a>");
%>
</form>
</body>
</html>
