<!--
//*************************************************************************************************
///            Funcion: JSP que muestra el Descglose de la txn 0838
//            Elemento: PageBuilderRAPCal.jsp
//          Creado por: Fausto Rodrigo Flores Moreno
//*************************************************************************************************
// CCN - 4360574 - 09/03/2007 - Se crea response para mostrar el desglose de consulta 0838
//*************************************************************************************************
-->

<%@ page session="true" import="java.util.*, ventanilla.GenericClasses"%>
<%
response.setHeader("Cache-Control","no-store"); //HTTP 1.1
response.setHeader("Pragma","no-cache"); //HTTP 1.0
response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<%!
  private String getString(String s)
  {
   int len = s.length();
   for(; len>0; len--)
    if(s.charAt(len-1) != ' ')
     break;
   return s.substring(0, len);
  }

  private String getItem(String newCadNum, int newOption)
  {
   int iPIndex = newCadNum.indexOf("CONSECUTIVO:") + 12, NumSaltos, Num = 0, Ban = 0;
   String nCadNum = new String("");
   NumSaltos = newOption;
   while(Num < NumSaltos)
   {
    if(newCadNum.charAt(iPIndex) == 0x20)
    {
     while(newCadNum.charAt(iPIndex) == 0x20)
      iPIndex++;
     Num++;
    }
    else
     while(newCadNum.charAt(iPIndex) != 0x20)
      iPIndex++;
   }
   while(newCadNum.charAt(iPIndex) != 0x20 && iPIndex < newCadNum.length()){
    nCadNum = nCadNum + newCadNum.charAt(iPIndex);
    if(iPIndex < newCadNum.length())
      iPIndex++;
    if(iPIndex == newCadNum.length())
      break;
   }

   return nCadNum;
  }

 %>
<%
Vector resp = (Vector)session.getAttribute("response");
Hashtable info = (Hashtable)session.getAttribute("info");
String pcdOK = (String)session.getAttribute("pcd");
Hashtable datasession = (Hashtable)session.getAttribute("page.datasession");

%>
<html>
<head>
<link rel="StyleSheet" type="text/css" href="../estilos/style.css">
  <title>Certificación</title>
<script language="javascript">
var opcion = "SI";

	function cancel()
	{
	    document.Txns.action="../paginas/Final.jsp";
	    document.Txns.submit();
	}
	
	function No_Descuento()
	{	
		document.Txns.no_descuento.value = "NO"
		document.Txns.submit();
	}
	
	function sub()
	{	
		document.Txns.submit();
	}
</script>
</head>
<body bgcolor="#FFFFFF">
<form name="Txns" action="../paginas/PageBuilderRAP.jsp" method="post">
<%
	String Desc_Recargo = (String)datasession.get("Desc_Recargo");

	GenericClasses gc = new GenericClasses();
%>
<!--VISUALIZACIÓN DE LA INFORMACIÓN OBTENIDA-->
		<h1><%=session.getAttribute("page.txnImage")%></h1>
		<%if(Integer.parseInt(datasession.get("Monto_D_R").toString())!= 0){%>
			<%if(Desc_Recargo.equals("D")){%>
				<h3>5503  Desglose de Pagos Referenciados con Descuentos</h3>	
			<%}else if(Desc_Recargo.equals("R")){%>
				<h3>5503  Desglose de Pagos Referenciados con Recargos </h3>
			<%}
		  }else{%>
				<h3>5503  Desglose de Pagos Referenciados </h3>
		<%}%>
		<table border="0" cellspacing="0">
		   <tr>
		    <td>Servicio:</td>
		    <td width="10">&nbsp;</td>
		    <td align="right"><%=info.get("Servicio1").toString()%></td>
		   </tr>
		   <tr>
		    <td>Importe:</td>
		    <td width="10">&nbsp;</td>
		    <td align="right"><%=gc.FormatAmount(info.get("MontoOrg").toString())%></td>
		   </tr>		   
  	     <%if(Integer.parseInt(datasession.get("Monto_D_R").toString())!= 0){%>
  	       	<tr> 
  	     <%		if(Desc_Recargo.equals("D")){%>
		    		<td>Descuento:</td>
		 <%		}else if(Desc_Recargo.equals("R")){%>
 		     		<td>Recargo:</td>
 		 <%		}%>
   		   			<td width="10">&nbsp;</td>
  		     		<td align="right"><%=gc.FormatAmount(datasession.get("Monto_D_R").toString())%></td>
		   	</tr>
		   	<tr>
		    	<td>Fecha Limite:</td>
		    	<td width="10">&nbsp;</td>
		    	<td align="right"><%=gc.FormatDate(datasession.get("FechaL").toString())%></td>
		   	</tr>
  	     <%	if(Desc_Recargo.equals("R")){%>
		   	<tr>
		    	<td>Dias vencidos:</td>
		    	<td width="10">&nbsp;</td>
		    	<td align="right"><%=datasession.get("DiasVencidos").toString()%></td>
		   	</tr>
		 <%	}
		   }
		 %>
		   <tr>
		    <td>Monto a Pagar:</td>
		    <td width="10">&nbsp;</td>
		    <td align="right"><%=gc.FormatAmount(datasession.get("MontoPagar").toString())%></td>
		   </tr>
		</table>
<table>
<%if(Desc_Recargo.equals("D"))
	out.println("<tr><td> <b>El pago de este servicio aplica descuento por pronto pago</b> </td></tr>");
  else if(Desc_Recargo.equals("R"))
	out.println("<tr><td> <b>El pago de este servicio requiere el cobro de un recargo</b> </td></tr>");%>	
  <tr><td></td></tr> 
</table>
<table>
 <tr>
 	<td>&nbsp;</td>
 </tr> 
 <tr>
  <td><%out.println("<a id='btnCont' href=\"javascript:sub()\"><img src=\"../imagenes/b_continuar.gif\" border=\"0\"></a>");%>
  </td>
  <td><%out.println("<a id='btnCan' href=\"javascript:cancel()\" ><img src=\"../imagenes/b_cancelar.gif\" border=\"0\"></a>");%>
  </td>
  <%if(Desc_Recargo.equals("D")){%>
  <td><%out.println("<a id='btnNDes' href=\"javascript:No_Descuento()\"><img src=\"../imagenes/b_node.gif\" border=\"0\"></a>");%>
  </td>
  <%}%>
 </tr>
</table> 
</td>
</tr>
</table>
<input type="hidden" name="rapcalc" value="SI">
<input type="hidden" name="no_descuento" value="SI">
<input type="hidden" name="transaction" value="<%= session.getAttribute("page.iTxn") %>">
<input type="hidden" name="transaction1" value="<%= session.getAttribute("page.oTxn") %>">
<input type="hidden" name="supervisor" value="<%= session.getAttribute("supervisor") %>">
</form>
</body>
</html>