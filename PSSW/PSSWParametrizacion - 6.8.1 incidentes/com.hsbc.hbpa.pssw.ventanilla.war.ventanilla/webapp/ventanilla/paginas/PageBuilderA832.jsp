<%//*************************************************************************************************
//             Funcion: JSP que muestra la pantalla de inicio para RAPL
//            Elemento: PageBuilderA832.jsp
//*************************************************************************************************
//CR      Proyecto POLAS. 
//*************************************************************************************************%>

<%@ page session="true"  import="java.util.Vector,java.util.Hashtable,java.util.Enumeration,ventanilla.com.bital.beans.DatosVarios,ventanilla.com.bital.sfb.ServicioRAPL"  %>
<%
response.setHeader("Cache-Control","no-store"); //HTTP 1.1
response.setHeader("Pragma","no-cache"); //HTTP 1.0
response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<%
String MontoPro = new String("");
String txnAuthLevel = (String)session.getAttribute("page.txnAuthLevel");
String txnIMGAutoriz = (String)session.getAttribute("page.txnIMGAutoriz");
Vector v = (Vector)session.getAttribute("page.specialacct");
// Esta variable (v) tiene las cuentas especiales.
String noservicios =  (String)session.getAttribute("page.noservicios");
int nserv = Integer.parseInt(noservicios);
int k;
Hashtable datasession = (Hashtable)session.getAttribute("page.datasession");
if(datasession == null)
  datasession = new Hashtable();
  
 ventanilla.com.bital.beans.DatosVarios arpCombo = ventanilla.com.bital.beans.DatosVarios.getInstance();                   
        Vector vecRAP1 = new Vector();  
    	vecRAP1 =(Vector)arpCombo.getDatosV("ARP");   
    	session.setAttribute("servicioARP", vecRAP1);
	DatosVarios rapl = DatosVarios.getInstance();                   
    Hashtable htRAPL = null;      
    htRAPL =(Hashtable)rapl.getDatosH("SERVICRAPL");
      
  
%>
<html>
<head>
<%@include file="style.jsf"%>
<title>Datos de la Transaccion</title>
<script  language="JavaScript">
<!--
var formName = 'Mio'
var specialacct = new Array()
var aServicioPOLAS = new Array()
var aServicioRAPM = new Array()

    <%	
     DatosVarios rap1 = DatosVarios.getInstance();                   
     Hashtable hashRAP1 = null;       
     hashRAP1 =(Hashtable)rap1.getDatosH("SERVICRAPL");    
     
   	 Enumeration keys1 = hashRAP1.keys();    	
    	while(keys1.hasMoreElements())
    	{
    	  	String key1 = (String)keys1.nextElement();
    	   	ServicioRAPL srapl1 = (ServicioRAPL)hashRAP1.get(key1);
    	   %>
    	   aServicioPOLAS['<%=srapl1.getNoServicio()%>'] = 1
      <%} %>
      
        <%
            
			 Vector vecRAP = new Vector();   
			 vecRAP =(Vector)rap1.getDatosV("RAPCP");
			 int szvecRAP = vecRAP.size();
			 for (int x=0;x<szvecRAP;x++)
			 {
			    Vector vect = (Vector)vecRAP.get(x);
			    String serv = (String)vect.get(1); 
			    if (!serv.equals("0000"))
			    { %>  
			    	
			    	aServicioRAPM[<%=serv%>]= 1
			 		
			 <%	}
			 }
 			%>
      
      
<%

//Se cargan las cuenta especiales al array (specialacct), ** Alparecer no se usa.
if(!v.isEmpty())
{
  for(int iv = 0; iv < v.size(); iv++){
  out.print("specialacct[" + iv + "]='" + v.get(iv) + "'" + '\n');
  }
}%>

function validate(form)
{
  if( !top.validate(window, document.entry.servicio1, 'isnumeric') )
    return;

  form.validateFLD.value = '1'

<% if(!txnAuthLevel.equals("1") && !txnIMGAutoriz.equals("1"))
	{
    out.println("  form.submit();");
    out.println("var obj = document.getElementById('btnCont');"); 
    out.println("obj.onclick = new Function('return false');");   
	}   
%>
}
function AuthoandSignature(entry)
{
 var Cuenta;
 if(entry.validateFLD.value != '1')
  return
 if(entry.needAutho.value == '1' && entry.AuthoOK.value != '1')
 {
  top.openDialog('../ventanilla/paginas/authoriz.jsp', 220, 130, 'top.setPrefs()', entry)
 }
 if(entry.needSignature.value == '1' && entry.SignField.value != '')
{
  Cuenta = eval('entry.' + entry.SignField.value + '.value')
  top.openModalDialog('<%=request.getContextPath()%>/servlet/ventanilla.FirmasServlet?txtDDACuenta=' + Cuenta, 710, 430, 'top.setPrefs2()', entry)
 }
}
function valserv(){
	if(document.entry.servicio1.value != ''){
	   strServicio = document.entry.servicio1.value;

   	   var auxiliar = aServicioPOLAS[strServicio]
   	   if(null != auxiliar){
   	     return
   	   }else{   	        	
   	       auxiliar = aServicioRAPM[strServicio]
	 	   if (null != auxiliar ){
	   	   		      alert("Para el servicio " + strServicio + " usar Txn RAPM Cobranza Personalizada: \n - Ingresa al men� Principal de RAP y selecciona la opci�n RAPM (Cobranza Personalizada). \n - Esta operaci�n por el momento no se ingresa por n�mero de txn. \n - En caso de dudas, consulta el IT CASE5-009/Anexo Operaci�n o llama al CIB ") ;
					  document.entry.servicio1.value = "";
					  document.entry.servicio1.focus();
					  return;
	  	   	}else{
   	   		alert("Para el servicio " + strServicio + " usar Txn 5503 Dep�sito Cobranza Especial: \n - Ingresa al men� Principal de RAP y selecciona la opci�n 5503 (Dep�sito Cobranza Especial). \n - Esta operaci�n por el momento no se ingresa para pago en l�nea.") ;
   	   		document.entry.servicio1.value = "";
            document.entry.servicio1.focus();
					return;
			}
   	     
   	   }
   	   	   
  	   	}
   	
   	return
}
//-->
</script>
</head>
<body bgcolor="#FFFFFF" onload="top.setFieldFocus(window.document)">
<br>
<% out.print("<form name=\"entry\" action=\"../servlet/ventanilla.DataServlet\" method=\"post\" ");
   if(txnAuthLevel.equals("1")){
    out.print("onSubmit=\"return validate(this)\"");
    }
   out.println(">");
%>

<h1><%=session.getAttribute("page.txnImage")%></h1>

<h3>
<%out.print(session.getAttribute("page.cTxn") + "  " +
                (String)session.getAttribute("page.txnlabel"));%>
</h3>

<table border="0">

<tr>
	<td>Servicio :</td><td>&nbsp;&nbsp;</td>
	<td><input tabindex="1" type="text" name="servicio1" size="7" maxlength="7" onBlur="valserv()" onKeyPress="top.estaVacio(this)||top.keyPressedHandler(window, this, 'isValServ')"></td>
</tr>
    <tr><td colspan='3'><b>Capturar N&uacute;mero de Cuenta de Cheques para Clientes ...</b><td>
</tr>
<tr>
    <td align="right">Cuenta :</td><td>&nbsp;&nbsp;</td>
    <td><input tabindex="3" type="text" name="txtDDACuenta" size="10" maxlength="10" onBlur="top.estaVacio(this)||top.validate(window, this, 'isValidAcct')"   onKeyPress="top.estaVacio(this)||top.keyPressedHandler(window, this, 'isValidAcct')" ></td>
</tr>

</table>

<input type="hidden" name="lstServicio" value="<%=(String)datasession.get("servicio")%>">
<input type="hidden" name="nopagos1" value="1">
<input type="hidden" name="monto1" value="0">
<input type="hidden" name="nservicios" value="1">
<input type="hidden" name="tot" value="0">
<input type="hidden" name="txtTot" value="0.00">
<input type="hidden" name="np" value="0">
<input type="hidden" name="flujorapcapr" value="1">
<input type="hidden" name="s1" value="X">
<input type="hidden" name="flujorap" value="1">
<input type="hidden" name="noefec" value="0">
<input type="hidden" name="ser60" value="0">
<input type="hidden" name="ser670" value="0">
<input type="hidden" name="iTxn" value="<%= session.getAttribute("page.iTxn")%>">
<input type="hidden" name="oTxn" value="<%= session.getAttribute("page.oTxn")%>">
<input type="hidden" name="cTxn" value="<%= session.getAttribute("page.cTxn")%>">
<input type="hidden" name="sucursal" value="<%= session.getAttribute("branch")%>">
<input type="hidden" name="teller" value="<%= session.getAttribute("teller")%>">
<input type="hidden" name="moneda" value="<%= session.getAttribute("page.moneda")%>">
<input type="hidden" name="registro" value="<%=session.getAttribute("empno")%>">
<input type="hidden" name="tllrlvl" value="<%=session.getAttribute("tellerlevel")%>">
<input type="hidden" name="supervisor" value="<%= session.getAttribute("teller") %>">
<input type="hidden" name="AuthoOK" value="0">
<input type="hidden" name="SignOK" value="0">
<input type="hidden" name="validateFLD" value="0">
<input type="hidden" name="needSignature" value="<%out.print(txnIMGAutoriz);%>">
<input type="hidden" name="needAutho" value="<%out.print(txnAuthLevel);%>">
<input type="hidden" name="SignField" value="<%= session.getAttribute("page.SignField") %>">
<input type="hidden" name="compania" value="20">
<p>

<% 
if(txnAuthLevel.equals("1") || txnIMGAutoriz.equals("1")){
    out.println("<a id='btnCont' tabindex='4' href=\"javascript:validate(document.entry);javascript:AuthoandSignature(document.entry)\" ><img src=\"../ventanilla/imagenes/b_continuar.gif\" border=\"0\"></a>");
    }
   else{
     out.println("<a id='btnCont' tabindex='4' href=\"javascript:validate(document.entry)\"><img src=\"../ventanilla/imagenes/b_continuar.gif\" border=\"0\"></a>");
    }
%>
</form>
</body>
</html>



