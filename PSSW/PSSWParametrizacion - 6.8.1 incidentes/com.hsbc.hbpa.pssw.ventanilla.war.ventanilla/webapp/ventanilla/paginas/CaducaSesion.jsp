<!--
//*************************************************************************************************
//             Funcion: JSP para el login al sistema
//            Elemento: CaducaSesion.jsp
//          Creado por: Yair Jesus Chavez Antonel
//*************************************************************************************************
// CCN - 4360532 - 20/10/2006 - Se agrega jsp para mensaje de sesi�n caducada
//*************************************************************************************************
-->
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<HTML>
<HEAD>
<%@ page 
language="java"
contentType="text/html; charset=ISO-8859-1"
pageEncoding="ISO-8859-1"
%>
<META http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<META name="GENERATOR" content="IBM WebSphere Studio">
<TITLE>CaducaSesion.jsp</TITLE>
<script type="text/JavaScript">
 function MensajeSesion(){
          alert("La Sesi�n ha caducado.\nFavor de firmarse nuevamente");
 }  
</script>
</HEAD>
<BODY onload="JavaScript:MensajeSesion();">
</BODY>
</HTML>
