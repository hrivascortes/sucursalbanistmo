<!--
//*************************************************************************************************
//             Funcion: JSP que genera las opciones para pago de SAT
//            Elemento: ResponseS5031.jsp
//          Creado por: Alejandro Gonzalez Castro
// Ultima Modificacion: 03/08/2004
//      Modificado por: Rutilo Zarco Reyes
//*************************************************************************************************
// CCN - 4360163 - 06/07/2004 - Se envia entidad federativa para pagos SAT v2.1
// CCN - 4360172 - 22/07/2004 - Se corrigen conceptos a menos de 52 posiciones
// CCN - 4360175 - 03/08/2004 - Se ajusta size ventana de impresion
// CCN - 4360237 - 05/11/2004 - SAT v3.0
// CCN - 4360245 - 15/11/2004 - Se evita imprimir sello digital con error de comunicacion
// CCN - 4360407 - 15/12/2005 - Nueva version sat 4.0
// CCN - 4360545 - 17/11/2006 - Se modifica la impresion de pagos del SAT
// CCN - 4360552 - 28/12/2006 - Se reversan cambios del ccn 4360545

//*************************************************************************************************
-->

<%@ page session="true" import="java.util.*"%>
<%
response.setHeader("Cache-Control","no-store"); //HTTP 1.1
response.setHeader("Pragma","no-cache"); //HTTP 1.0
response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
<title>Results</title>
<style type="text/css">Body {font-family: Arial, Helv; font-size: 10pt; font-weight: bold; color:#000000}</style>
</head>
<body>
Proceso de RAP Terminado...
<form name="ImprimeDiario">
<%
String codigoSAT = (String)session.getAttribute("Code.Pago.SAT");
session.removeAttribute("Code.Pago.SAT");
Hashtable datasession = (Hashtable)session.getAttribute("page.datasession");
if (codigoSAT.equals("0"))
{
    Hashtable cadenaoriginal = (Hashtable)session.getAttribute("cadena.original");
    Hashtable nombreconcepto = getconcepts();
    Hashtable periodos = getperiodos();
    Hashtable entidades = getentidades();
    Hashtable secretarias = getSecretarias();
    Hashtable cadenaDPA = (Hashtable)cadenaoriginal.get("Cadena.DPA");
    Vector codigo = getcodImp();
    Vector codiCO = getcodCO();
    String materia = (String)cadenaoriginal.get("40001");
    String tipleye = "S";
    if (materia.equals("004"))
    {
       nombreconcepto.put("04","Importe");
       nombreconcepto.put("20","Cantidad a Pagar");
        if (cadenaoriginal.get("10001") == null)
       datasession.put("txtRfc", "             ");
       tipleye = "C";
    }
    String txtnomb = (String)datasession.get("txtnombre");
    txtnomb = txtnomb.trim();
    String txtrfc = null;
    String curp = "                  ";
    if (materia.equals("010"))
    {
      txtrfc= (String)cadenaoriginal.get("10001");
      String txtrfc1 = (String)datasession.get("txtRfc");
      if (txtrfc1.startsWith("0"))
      {
         txtrfc1 = txtrfc1.substring(1);
         datasession.put("txtRfc",txtrfc1);
      }
      curp = (String)cadenaoriginal.get("10002");
      if (txtrfc == null)
      {
         String n10003 = (String)cadenaoriginal.get("10003");
         if (n10003 != null)
         {
           txtnomb = (String)cadenaoriginal.get("10003") + " ";
           if (cadenaoriginal.get("10004") != null)
             txtnomb += (String)cadenaoriginal.get("10004") + " ";
           txtnomb += (String)cadenaoriginal.get("10005");
         }
        datasession.put("txtRfc", "             ");
      }
      if (curp == null)
         curp = " ";
    }
    int numconce = Integer.parseInt((String)cadenaoriginal.get("numconce"));
    if (numconce > 1)
       tipleye = "P";
    String sat = "SAT~" + (String)datasession.get("txtRfc") + "~"+ curp + "~" + materia + tipleye + "~" + txtnomb + "~" + (String)datasession.get("txtRefer16") + "~" + addpunto((String)cadenaoriginal.get("10017")) + "~" + (String)cadenaoriginal.get("20008") + "~"  + (String)cadenaoriginal.get("40008") + "~";
    String cad_original = "";
    int issecretaria = 0;
    for (int j=0; j<numconce; j++) //Numero de conceptos pagados
    {
        String clave = (String)cadenaoriginal.get("clvimpuesto" + Integer.toString(j));
//System.out.println("clave " + clave + " j " + j);
        sat += (String)nombreconcepto.get(clave) + "~";
        if (clave.equals("145")) //Creditos Fiscales
          sat += "~";        
        //Armado de cadena original
        if (materia.equals("010"))
        {
          String clave1 = (String)cadenaDPA.get("clvimpuesto" + Integer.toString(j));
          for (int i=0; i<codiCO.size(); i++)   //Armado de cadena original
          {
            String campoCO = clave1 + (String)codiCO.get(i) + Integer.toString(j);
            String valorCO = (String)cadenaDPA.get(campoCO);
            if (valorCO != null)
                cad_original += campoCO.substring(0,campoCO.length()-1) + "=" + valorCO + "|"; //Arma cadena original
          }          
        }
        else 
        {
          for (int i=0; i<codiCO.size(); i++)   //Armado de cadena original
          {
            String campoCO = clave + (String)codiCO.get(i) + Integer.toString(j);
            String valorCO = (String)cadenaoriginal.get(campoCO);
            if (valorCO != null)
                cad_original += campoCO.substring(0,campoCO.length()-1) + "=" + valorCO + "|"; //Arma cadena original
          }          
        }
        for (int i=0; i<codigo.size(); i++)   //Numero de codigos disponibles
        {
            String campo = clave + (String)codigo.get(i) + Integer.toString(j);
//System.out.println("campo " + campo);
                String valor = (String)cadenaoriginal.get(campo);
            if (valor != null)
            {
//System.out.println("campo " + campo + " valor " + valor);
                if (codigo.get(i).equals("02"))
                {
                    sat += (String)nombreconcepto.get(codigo.get(i)) + ":" + periodos.get(setloncad(valor, 3, "0")) + "~";
                    if (clave.equals("147") && materia.equals("010")) //DPA -- trae periodo
                      {
                     if (issecretaria == 0)
                        sat += "Dependencia: ~" + secretarias.get((String)cadenaoriginal.get("30005")) + "~";
                     issecretaria++;
                     sat += "Clave de Referencia del DPA: ~" + (String)cadenaoriginal.get("14733" + Integer.toString(j)) + "~";
                     sat += "Cadena de la dependencia: ~" + (String)cadenaoriginal.get("14734" + Integer.toString(j)) + "~";
                     nombreconcepto.put("04","Importe");
                    }
                    if (clave.equals("120") && materia.equals("010")) //DPA -- trae iva
                      nombreconcepto.put("04","Impuesto a Cargo");
                }
                      else
                      {
                    if(!codigo.get(i).equals("08") && !codigo.get(i).equals("16") && !codigo.get(i).equals("33") && !codigo.get(i).equals("34")
                       && !codigo.get(i).equals("29") && !codigo.get(i).equals("30")) //DPA 4.0 no se incluyen 33 y 34. 29 - 30 para ejercicio
                    {
                        sat += (String)nombreconcepto.get(codigo.get(i)) + ": ~";
			            if (codigo.get(i).equals("02") || codigo.get(i).equals("27") || codigo.get(i).equals("31"))
                            sat += valor;
                        if (codigo.get(i).equals("22"))
                            sat += (String)nombreconcepto.get(valor);
                        if (codigo.get(i).equals("32"))
                            sat += (String)entidades.get(valor);                            
                        if (codigo.get(i).equals("23"))
                            sat += setformatdate(valor);
                        if (codigo.get(i).equals("03") || codigo.get(i).equals("04") || codigo.get(i).equals("05") || codigo.get(i).equals("06") ||
                            codigo.get(i).equals("07") || codigo.get(i).equals("17") || codigo.get(i).equals("18") ||
                            codigo.get(i).equals("19") || codigo.get(i).equals("21") || codigo.get(i).equals("20") || codigo.get(i).equals("15"))
                            sat += addpunto(valor);
                        if (codigo.get(i).equals("09") || codigo.get(i).equals("14") || codigo.get(i).equals("10") || codigo.get(i).equals("24") ||
                            codigo.get(i).equals("25") || codigo.get(i).equals("26") || codigo.get(i).equals("12") || codigo.get(i).equals("13") || 
                            codigo.get(i).equals("35") || codigo.get(i).equals("36") || codigo.get(i).equals("37"))
                            sat += "(" + addpunto(valor) + ")";
                        
                        if (codigo.get(i).equals("20"))
                            sat += "\n~";
                        else
                            sat += "~";
                     
                      
                      }
                    }
                }
            else{
             if (clave.equals("147") && materia.equals("010") && codigo.get(i).equals("02"))  //DPA no trae periodo
             {
                    nombreconcepto.put("04","Importe");
                    sat += (String)nombreconcepto.get(codigo.get(i)) + ": ~";
                    sat += "Dependencia: ~" + secretarias.get((String)cadenaoriginal.get("30005")) + "~";
                    sat += "Clave de Referencia del DPA: ~" + (String)cadenaoriginal.get("14733" + Integer.toString(j)) + "~";
                    sat += "Cadena de la dependencia: ~" + (String)cadenaoriginal.get("14734" + Integer.toString(j)) + "~";
             }}
          }
    
        if (clave.equals("135")) //R35 Personas Fisicas Concepto Pagos del Ejercicio
        {
          String fecoper = (String)cadenaoriginal.get("13529" + Integer.toString(j));
          String numoper = (String)cadenaoriginal.get("13530" + Integer.toString(j));
          if (fecoper != null && numoper != null)
             sat += "Manifiesto" + fecoper.substring(6) + "/" + fecoper.substring(4,6) + "/" + fecoper.substring(0,4) + numoper + "\n~\n~" ;
        }
       
    }
    datasession.remove("flujorap");
    session.setAttribute("page.datasession",datasession);
    if (materia.equals("004")) //Creditos Fiscales
    {
      String encabezado = "";
      if (cadenaoriginal.get("10001") != null)
         encabezado = "||10001=" + (String) cadenaoriginal.get("10001") + "|10017=" + (String)cadenaoriginal.get("10017");
      else
        encabezado = "||10017=" + (String) cadenaoriginal.get("10017");
      cad_original = encabezado + "|20001=" + (String)cadenaoriginal.get("20001") +
                     "|20002=" + (String)cadenaoriginal.get("20002") + "|40002=" + (String)cadenaoriginal.get("40002") + "|40003=" + (String)cadenaoriginal.get("40003") + "|40008=" + (String)cadenaoriginal.get("40008") + "|" + cad_original + "30003="+(String)cadenaoriginal.get("30003") + "||";
    }
    else if(materia.equals("010") && txtrfc == null) //DPA
    {
      String encabezado = "";
      if (cadenaoriginal.get("10001") == null)
      {
        if (cadenaoriginal.get("10003") != null)
        {
           encabezado = "||10003=" + (String)cadenaoriginal.get("10003");
           if (cadenaoriginal.get("10004") != null)
             encabezado += "|10004=" + (String)cadenaoriginal.get("10004");
           encabezado += "|10005=" + (String)cadenaoriginal.get("10005");
        }
        if(cadenaoriginal.get("10006") != null)
           encabezado = "||10006=" + (String)cadenaoriginal.get("10006");
      } 
      cad_original = encabezado + "|10017=" + (String)cadenaoriginal.get("10017") + "|20001=" + (String)cadenaoriginal.get("20001") +
                     "|20002=" + (String)cadenaoriginal.get("20002") + "|40002=" + (String)cadenaoriginal.get("40002") + "|40003=" + (String)cadenaoriginal.get("40003") + "|40008=" + (String)cadenaoriginal.get("40008") + "|" + cad_original + "30003="+(String)cadenaoriginal.get("30003") + "||";
    }
    else
      cad_original = "||10001=" + (String)cadenaoriginal.get("10001") + "|10017=" + (String)cadenaoriginal.get("10017") + "|20001=" + (String)cadenaoriginal.get("20001") +
                     "|20002=" + (String)cadenaoriginal.get("20002") + "|40002=" + (String)cadenaoriginal.get("40002") + "|40003=" + (String)cadenaoriginal.get("40003") + "|40008=" + (String)cadenaoriginal.get("40008") + "|" + cad_original + "30003="+(String)cadenaoriginal.get("30003") + "||";

    sat+="CADENA ORIGINAL \n~" + cad_original + "\n~SELLO DIGITAL \n ~||" + (String)cadenaoriginal.get("30001") + "||~";

    session.setAttribute("txtCadImpresion", sat);
//System.out.println("cadenaimpresion :" + sat);
}
else
{
  out.println("<b>Transaccion Rechazada.</b><br><br>");
  out.println("<b>Verifique Status con el Procesador Central...</b><br><br>");
  datasession.remove("flujorap");
  session.setAttribute("page.datasession",datasession);
  session.removeAttribute("strFlujoRap");
  session.setAttribute("txtCadImpresion", "NOIMPRIMIRNOIMPRIMIRNOIMPRIMIR");
  
}
%>
    <script language="javascript">
    	function callOpenDialog()
     {
		top.openDialog('../../servlet/ventanilla.ImprimirServlet', 160, 120, 'top.setPrefs4()', document.Txns);
    	}
    </script>
    <%
    out.println("<a href=\"javascript:callOpenDialog()\"><img src=\"../imagenes/b_continuar.gif\" border=\"0\"></a>");
%>
</form>
</body>
</html>
<%!
  private String addpunto(String valor)
  {
        valor = valor + "00";
	int longini = valor.length();
    String cents  = valor.substring(longini-2,longini);
	String entero = valor.substring(0,longini-2);
	int longente = entero.length();
	for (int i = 0; i < (longente-(1+i))/3; i++)
	{
		longente = entero.length();
	    entero = entero.substring(0,longente-(4*i+3))+','+entero.substring(longente-(4*i+3));
	}
//  	entero = entero + '.' + cents;
	return entero;
  }

  private Hashtable getconcepts()
  {
    Hashtable nameconcept = new Hashtable();
nameconcept.put("105","ISR personas f�sicas. Actividad empresarial y profesional");
nameconcept.put("106","ISR personas f�sicas. Actividad empresarial. R�gimen intermedio para la Federaci�n");
nameconcept.put("108","ISR personas f�sicas. Arrendamiento de inmuebles (uso o goce)");
nameconcept.put("109","ISR personas f�sicas. De los dem�s ingresos");
nameconcept.put("110","ISR retenciones por salarios");
nameconcept.put("111","ISR retenciones por asimilados a salarios");
nameconcept.put("115","ISR otras retenciones");
nameconcept.put("116","ISR retenciones por pagos al extranjero");
nameconcept.put("136","ISR  personas f�sicas. Salarios");
nameconcept.put("137","ISR  enajenaci�n de bienes (excepto terrenos y/o construcciones)");
nameconcept.put("138","ISR  fedatarios p�blicos y retenciones.  Enajenaci�n de bienes");
nameconcept.put("139","ISR  personas f�sicas. Adquisici�n de bienes");
nameconcept.put("140","ISR  fedatarios p�blicos. Adquisici�n  de bienes");
nameconcept.put("141","ISR  retenciones por premios");
nameconcept.put("117","Impuesto al activo");
nameconcept.put("119","Impuesto al valor agregado");
nameconcept.put("134","IVA ajuste");
nameconcept.put("120","IVA actos accidentales");
nameconcept.put("121","IVA retenciones");
nameconcept.put("122","IEPS por bebidas alcoh�licas");
nameconcept.put("128","IEPS por cerveza");
nameconcept.put("129","IEPS por bebidas refrescantes");
nameconcept.put("123","IEPS por alcohol, alcohol desnaturalizado y mieles incristalizables");
nameconcept.put("124","IEPS por tabacos labrados");
nameconcept.put("130","IEPS por telecomunicaciones");
nameconcept.put("131","IEPS por refrescos y sus concentrados");
nameconcept.put("125","IEPS retenciones");
nameconcept.put("132","Impuesto sustitutivo del cr�dito al salario");
nameconcept.put("133","Impuesto a la venta de bienes y servicios suntuarios");
nameconcept.put("147","Derechos, Productos y Aprovechamientos");
nameconcept.put("107","ISR personas f�sicas. Actividad empresarial. Peque�os contribuyentes");
nameconcept.put("146","ISR enajenaci�n de terrenos y/o construcciones");
nameconcept.put("148","ISR personas f�sicas. Actividad empresarial. R�gimen Intermedio para E.F.");
nameconcept.put("150","ISR-IVA Peque�os contribuyentes. Cuota �nica");
nameconcept.put("149","IVA. Peque�os contribuyentes");
nameconcept.put("135","ISR  personas f�sicas");
nameconcept.put("142","ISR por inversiones en reg�menes fiscales preferentes");
nameconcept.put("144","Impuesto sobre tenencia o uso de aeronaves");
nameconcept.put("145","Cr�dito Fiscal");
    nameconcept.put("02","Per�odo");
    nameconcept.put("03","Impuesto a Favor");
    nameconcept.put("04","Impuesto a Cargo");
    nameconcept.put("05","Parte Actualizada");
    nameconcept.put("06","Recargos");
    nameconcept.put("07","Multa por Correccion");
    nameconcept.put("09","Credito al Salario");
    nameconcept.put("10","Cr�dito IEPS Diesel sector primario y minero");
    nameconcept.put("12","Otros Estimulos");
    nameconcept.put("13","Certificados");
    nameconcept.put("14","Compensaciones");
    nameconcept.put("15","Monto pagado con anterioridad");
    nameconcept.put("17","Cantidad a Cargo");
    nameconcept.put("18","Importe de la 1ra Parcialidad");
    nameconcept.put("19","Importe sin la 1ra Parcialidad");
    nameconcept.put("31","Numero de Credito");
    nameconcept.put("20","Cantidad Pagada");
    nameconcept.put("21","Cantidad a Favor");
    nameconcept.put("22","Tipo de Pago");
    nameconcept.put("23","Fecha del pago realizado con anterioridad");
    nameconcept.put("24","Diesel Automotriz para transporte");
    nameconcept.put("25","Uso de Infraestructura carretera de cuota");
    nameconcept.put("26","Producci�n de agave");
    nameconcept.put("27","Ejercicio");
    nameconcept.put("29","Fecha de Declaracion del Ejercicio");
    nameconcept.put("30","Numero de Operacion de Recibido en el SAT");
    nameconcept.put("32","Entidad Federativa");
//    nameconcept.put("33","Clave de Referencia del DPA");
//    nameconcept.put("34","Cadena de la dependencia");
    nameconcept.put("35","Diesel Marino");
    nameconcept.put("36","Subsidio para el empleo");
    nameconcept.put("37","Subsidio para la nivelaci�n del ingreso");
    nameconcept.put("1","Normal");
    nameconcept.put("2","Complementaria");
    nameconcept.put("3","Correccion Fiscal");
    return nameconcept;
}
public Vector getcodImp()
{ //Columnas en formato para impresion
    Vector codigo = new Vector();
    codigo.add("31");
    codigo.add("02");
    codigo.add("27");
    codigo.add("22");
    codigo.add("32");
    codigo.add("03");	
    codigo.add("04");	
    codigo.add("05");	
    codigo.add("06");		
    codigo.add("07");		
//    codigo.add("08");	//Total de contribuciones
    codigo.add("09");	
    codigo.add("14");	
    codigo.add("10");	
    codigo.add("24");	
    codigo.add("25");	
    codigo.add("26");	
    codigo.add("12");	
    codigo.add("35");	
    codigo.add("36");		
    codigo.add("37");	
//    codigo.add("16");	 //Total de aplicaciones
    codigo.add("17");
    codigo.add("23");		
    codigo.add("15");	
    codigo.add("18");	
    codigo.add("19");
    codigo.add("21");
    codigo.add("20");
    codigo.add("29");
    codigo.add("30");
    codigo.add("33");
    codigo.add("34");
    return codigo;
}
public Vector getcodCO()
{ //Columnas en formato de cadena original
    Vector codigo = new Vector();
    codigo.add("02");
    codigo.add("27");
    codigo.add("22");
    codigo.add("23");
    codigo.add("03");
    codigo.add("04");
    codigo.add("05");
    codigo.add("06");
    codigo.add("07");
    codigo.add("08");
    codigo.add("09");
    codigo.add("14");
    codigo.add("10");
    codigo.add("24");
    codigo.add("25");
    codigo.add("26");
    codigo.add("12");
    codigo.add("15");
    codigo.add("16");
    codigo.add("17");
    codigo.add("18");
    codigo.add("19");
    codigo.add("21");
    codigo.add("20");
    codigo.add("29");
    codigo.add("30");
    codigo.add("31");
    codigo.add("32");
    codigo.add("33");
    codigo.add("34"); //Version 4.0
    codigo.add("35"); //Version 4.0
    codigo.add("36"); //Version 4.0
    codigo.add("37"); //Version 4.0
    return codigo;
}
public Hashtable getperiodos()
{
    Hashtable periodos = new Hashtable();
    periodos.put("001","Enero");
    periodos.put("002","Febrero");
    periodos.put("003","Marzo");
    periodos.put("004","Abril");
    periodos.put("005","Mayo");
    periodos.put("006","Junio");
    periodos.put("007","Julio");
    periodos.put("008","Agosto");
    periodos.put("009","Septiembre");
    periodos.put("010","Octubre");
    periodos.put("011","Noviembre");
    periodos.put("012","Diciembre");
    periodos.put("013","lo Enero-Marzo");
    periodos.put("014","2o Abril-Junio");
    periodos.put("015","3o Julio-Septiembre");
    periodos.put("016","4o Octubre-Diciembre");
    periodos.put("017","lo Enero-Abril");
    periodos.put("018","2o Mayo-Agosto");
    periodos.put("019","3o Septiembre-Diciembre");
    periodos.put("020","lo Enero-Junio");
    periodos.put("021","2o Julio-Diciembre");
    periodos.put("022","Enero-Junio");
    periodos.put("023","Febrero-Julio");
    periodos.put("024","Marzo-Agosto");
    periodos.put("025","Abril-Septiembre");
    periodos.put("026","Mayo-Octubre");
    periodos.put("027","Junio-Noviembre");
    periodos.put("028","Julio-Diciembre");
    periodos.put("029","Agosto-Enero");
    periodos.put("030","Septiembre-Febrero");
    periodos.put("031","Octubre-Marzo");
    periodos.put("032","Noviembre-Abril");
    periodos.put("033","Diciembre-Mayo");
    periodos.put("034","Ajuste de Ejercicio");
    periodos.put("035","Del Ejercicio");
    periodos.put("036","Enero-Febrero      ");
    periodos.put("037","Marzo-Abril        ");
    periodos.put("038","Mayo-Junio         ");
    periodos.put("039","Julio-Agosto       ");
    periodos.put("040","Septiembre-Octubre ");
    periodos.put("041","Noviembre-Diciembre");    
    return periodos;
}

public Hashtable getentidades()
{
   Hashtable entidades = new Hashtable();
   entidades.put("50001","01 Aguascalientes     ");
   entidades.put("50002","02 Baja California    ");
   entidades.put("50003","03 Baja California Sur");
   entidades.put("50004","04 Campeche           ");
   entidades.put("50005","05 Coahuila           ");
   entidades.put("50006","06 Colima             ");
   entidades.put("50007","07 Chiapas            ");
   entidades.put("50008","08 Chihuahua          ");
   entidades.put("50009","09 Distrito Federal   ");
   entidades.put("50010","10 Durango            ");
   entidades.put("50011","11 Guanajuato         ");
   entidades.put("50012","12 Guerrero           ");
   entidades.put("50013","13 Hidalgo            ");
   entidades.put("50014","14 Jalisco            ");
   entidades.put("50015","15 Estado de M�xico   ");
   entidades.put("50016","16 Michoac�n          ");
   entidades.put("50017","17 Morelos            ");
   entidades.put("50018","18 Nayarit            ");
   entidades.put("50019","19 Nuevo Le�n         ");
   entidades.put("50020","20 Oaxaca             ");
   entidades.put("50021","21 Puebla             ");
   entidades.put("50022","22 Quer�taro          ");
   entidades.put("50023","23 Quintana Roo       ");
   entidades.put("50024","24 San Luis Potos�    ");
   entidades.put("50025","25 Sinaloa            ");
   entidades.put("50026","26 Sonora             ");
   entidades.put("50027","27 Tabasco            ");
   entidades.put("50028","28 Tamaulipas         ");
   entidades.put("50029","29 Tlaxcala           ");
   entidades.put("50030","30 Veracruz           ");
   entidades.put("50031","31 Yucat�n            ");
   entidades.put("50032","32 Zacatecas          ");
   return entidades; 
}

    public String setloncad(String cadena, int numzeros, String caracter)
    {
        if (cadena == null)
            cadena = "";
        for (int i=cadena.length(); i<numzeros; i++) {
            if (caracter.equals("0"))
            { cadena = "0" + cadena;}
            else
            { cadena += " ";}
        }
        return cadena;
    }
    public String setformatdate(String date)
    {
        return date = date.substring(0,4) + "/" + date.substring(4,6) + "/" + date.substring(6);
    }

    public Hashtable getSecretarias()
    {
   Hashtable secretarias = new Hashtable();
secretarias.put("01","Secretar�a de Gobernaci�n");
secretarias.put("02","Secretar�a de Relaciones Exteriores");
secretarias.put("03","Secretar�a de la Defensa Nacional");
secretarias.put("04","Secretar�a de Marina");
secretarias.put("05","Secretar�a de Seguridad P�blica");
secretarias.put("06","Secretar�a de Hacienda y Cr�dito P�blico");
secretarias.put("07","Secretar�a de Desarrollo Social");
secretarias.put("08","Secretar�a de Medio Ambiente y Recursos Naturales");
secretarias.put("09","Secretar�a de Energ�a");
secretarias.put("10","Secretar�a de Econom�a");
secretarias.put("11","Sria. de Agricultura, Ganader�a, Desarrollo Rural, Pesca y Alim.");
secretarias.put("12","Secretar�a de Comunicaciones y Transportes");
secretarias.put("13","Secretar�a de la Funci�n P�blica");
secretarias.put("14","Secretar�a de Educaci�n P�blica");
secretarias.put("15","Secretar�a de Salud");
secretarias.put("16","Secretar�a del Trabajo y Previsi�n Social");
secretarias.put("17","Secretar�a de la Reforma Agraria");
secretarias.put("18","Secretar�a de Turismo");
secretarias.put("19","Consejer�a Jur�dica del Ejecutivo Federal");
secretarias.put("20","Archivo General de la Naci�n");
secretarias.put("21","Comisi�n Calificadora de Publicaciones y Revistas Ilustradas ");
secretarias.put("22","Instituto Nacional de Migraci�n ");
secretarias.put("23","Consejo de Menores Prevenci�n y Readaptaci�n Social ");
secretarias.put("24","Comisi�n Nacional Bancaria y de Valores ");
secretarias.put("25","Comisi�n Nacional de Seguros y Fianzas ");
secretarias.put("26","Comisi�n Nacional del Sistema de Ahorro para el Retiro ");
secretarias.put("27","Instituto Nacional de Geograf�a, Estad�stica e Inform�tica ");
secretarias.put("28","Servicio de Administraci�n Tributaria ");
secretarias.put("29","Tesorer�a de la Federaci�n ");
secretarias.put("30","Comisi�n Nacional de Areas Naturales Protegidas ");
secretarias.put("31","Procuradur�a Federal de Protecci�n al Ambiente ");
secretarias.put("32","Comisi�n Nacional Forestal ");
secretarias.put("33","Comisi�n Nacional de Seguridad Nuclear y Salvaguardas ");
secretarias.put("34","Comisi�n Nacional para el Ahorro de Energ�a ");
secretarias.put("35","Comisi�n Reguladora de Energ�a ");
secretarias.put("36","Comisi�n Federal de Competencia"); 
secretarias.put("37","Comisi�n Federal de Mejora Regulatoria ");
secretarias.put("38","Fondo Nacional de Apoyo a las Empresas en Solidaridad ");
secretarias.put("39","Instituto Mexicano de la Propiedad Industrial ");
secretarias.put("40","Procuradur�a Federal del Consumidor ");
secretarias.put("41","Apoyos y Servicios a la Comercializaci�n Agropecuaria ");
secretarias.put("42","Comisi�n Nacional de Acuacultura y Pesca ");
secretarias.put("43","Servicio de Informaci�n y Estad�stica Agroalimentaria y Pesquera ");
secretarias.put("44","Servicio Nacional de Inspecci�n y Certificaci�n de Semillas ");
secretarias.put("45","Servicio Nacional de Sanidad, Inocuidad y Calidad Agroalimentaria ");
secretarias.put("46","Comisi�n Federal de Telecomunicaciones ");
secretarias.put("47","Servicios a la Navegaci�n en el Espacio A�reo Mexicano ");
secretarias.put("48","Caminos y Puentes Federales de Ingresos y Servicios Conexos ");
secretarias.put("49","Instituto Nacional de Administraci�n y Aval�os de Bienes Nacionales ");
secretarias.put("50","Consejo Nacional para la Cultura y las Artes ");
secretarias.put("51","Instituto Nacional del Derecho de Autor ");
secretarias.put("52","Colegio Nacional de Educaci�n Profesional T�cnica ");
secretarias.put("53","Comisi�n Nacional de Cultura F�sica y Deporte ");
secretarias.put("54","Centro Nacional de Transplantes ");
secretarias.put("55","Comisi�n Federal para la Protecci�n contra Riesgos Sanitarios ");
secretarias.put("56","Comisi�n Nacional de Arbitraje M�dico ");
secretarias.put("57","Direcci�n General del Centro Nacional de Transfusi�n Sangu�nea ");
secretarias.put("58","Procuradur�a Federal de la Defensa del Trabajo ");
secretarias.put("59","Consejo Nacional de Ciencia y Tecnolog�a ");
secretarias.put("60","Instituto Federal de Acceso a la Informaci�n P�blica ");
secretarias.put("61","Comisi�n Nacional para el Desarrollo de los Pueblos Ind�genas ");
secretarias.put("62","Procuradur�a General de la Rep�blica");
secretarias.put("63","Tribunal Federal de Justicia Fiscal y Administrativa");
secretarias.put("64","Poder Judicial");
secretarias.put("65","Servicio de Administraci�n y Enajenaci�n de Bienes");
secretarias.put("99","Otra");
   return secretarias;
}
%>