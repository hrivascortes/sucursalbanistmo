<!--
//*************************************************************************************************
//             Funcion: JSP que detalle del cheque a imprimir
//            Elemento: ResponseLAhorro.jsp
//          Creado por: YGX
//*************************************************************************************************
-->

<%@ page session="true" import="java.util.*,ventanilla.GenericClasses"%>
<%
 response.setHeader("Cache-Control","no-store"); //HTTP 1.1
 response.setHeader("Pragma","no-cache"); //HTTP 1.0
 response.setDateHeader ("Expires", 0); //prevents caching at the proxy server

 Hashtable datasession = (Hashtable)session.getAttribute("page.datasession");  

%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html>
<script language="javascript" >

function cancelar(form)
{
  document.entry.action = "../paginas/Final.jsp";
  document.entry.submit();
}



function validates(form)
{
    var j=0;
    if (document.entry.devFondos.value=0    )
    {
       return false;
    }else
        
        return true;
}

</script>

<head>
<%@include file="style.jsf"%>

</head>

<body bgcolor="#FFFFFF" onload="top.setFieldFocus(window.document)">   
<%  
  java.util.Stack flujotxn = new java.util.Stack();
  String codigo = "";
  String needOverride = "NO";
  String txn = session.getAttribute("page.cTxn").toString();
 
  if(session.getAttribute("page.flujotxn") != null)
  	flujotxn = (java.util.Stack)session.getAttribute("page.flujotxn");
%>  
<form name="entry" action="../../servlet/ventanilla.PageServlet" method="post">      


<h3><%out.print(session.getAttribute("page.cTxn") + "  " +(String)session.getAttribute("page.txnlabel"));%></h3>



<table align="center" cellspacing="0">
	<tr>       
        <td align="center">Devolucion de fondos a:</td>           
         <td align="center">  
        <select name="devFondos" tabindex="3" >
         <option value="0">Seleccione una opcion</option>
         <option value="1">Devolucion de Fondos a Cuenta Corriente</option>
         <option value="2">Devolucion de Fondos a Cuenta Contable (Departamentos)</option>        
        </select>      
        </td>  
    </tr>
</table>
	
  <input type="hidden" name="tip_cta"      value="0">
  <input type="hidden" name="transaction" value="<%= session.getAttribute("page.iTxn") %>">
  <input type="hidden" name="transaction1" value="<%= session.getAttribute("page.oTxn") %>">
  <input type="hidden" name="supervisor" value="<%= session.getAttribute("supervisor") %>">

	

    
<br>
<center>
      <a href="javascript:validate(document.entry)"><img src="../imagenes/b_aceptar.gif" border="0"></a>
      
      <a href="javascript:cancelar(document.entry)"><img src="../imagenes/b_cancelar.gif" border="0"></a>
</center>

</form>
</body>
<script language="JavaScript">
top.formName = '';

function validate(form)
			{
			
			 
			 if(!obtienedatos())
			 {
			   alert("Para Continuar debe seleccionar una opcion...");
		       return;
			 
			 }
			 	
		      //if(!validates(form))	
		      //{
		      // alert("Para Continuar debe seleccionar una opcion...");
		      // return;
		      //}

				
			   form.submit();  
			}
			
function obtienedatos()
{
	var valor;
    valor =	document.entry.devFondos.value;	
    document.entry.tip_cta.value= valor;	
    
   // alert("alert" + valor);
    
    if(valor == "0")
    {
     return false;
    }
    else
    {    
      return true;
    }
    
	alert(valor);
}

</script>
</html>
