<!--
//*************************************************************************************************
//             Funcion: JSP que muestra el detalle del usuario que fue bloqueado
//            Elemento: DetalleBloqueo.jsp
//          Creado por: Fausto R. Flores Moreno
//*************************************************************************************************
// CCN - 4360589 - 16/04/2007 - Se realizan modificaciones para el monitoreo de efectivo en las sucursales
//*************************************************************************************************
-->
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<HTML>
<HEAD>
<%@ page language="java" import="java.util.*,ventanilla.com.bital.util.MonitorEfectivo,ventanilla.GenericClasses"%>
<%--@ page language="java" import="java.util.*,MonitorEfectivo,GenericClasses"--%>
<% 
   MonitorEfectivo monitoreo = new MonitorEfectivo();
   
   String Cajero = request.getParameter("usersBloq");
   String userPIF = "";
   if(session.getAttribute("userPIF")!=null)
   	   userPIF = (String)session.getAttribute("userPIF");
   else
	   userPIF = (String)request.getParameter("userC");
   
   String resultado = monitoreo.getBloqueadoDtll(Cajero);
%>
<link rel="StyleSheet" type="text/css" href="../estilos/style.css">
<TITLE>Bloqueo de Cajero.jsp</TITLE>
<script type="text/JavaScript">
	function Regresar()
	{
		document.detalle.submit();
	}
</script>
</HEAD>
<BODY>
<form name="detalle" action="ControlEfectivo.jsp">
<%
	GenericClasses gc = new GenericClasses();
%>
	<br><br><br>
	<table align="center" width="650px">
	<tr>
	<th bgcolor='#999999' align="center"><FONT size="2" color="#ffffff" face="Arial">SUCURSAL</font></th>
	<th bgcolor='#999999' align="center"><FONT size="2" color="#ffffff" face="Arial">CAJERO</font></th>
	<th bgcolor='#999999' align="center"><FONT size="2" color="#ffffff" face="Arial">RACF</font></th>
	<th bgcolor='#999999' align="center"><FONT size="2" color="#ffffff" face="Arial">NOMBRE</font></th>
	<th bgcolor='#999999' align="center"><FONT size="2" color="#ffffff" face="Arial">DIRECCION IP</font></th>
	</tr>
	<tr>
<%			StringTokenizer row;
			int j = 0;
			String valor = "";
			out.println("<td align='center' ><FONT size='2' color='black' face='Arial'>"+Cajero.substring(0,4)+"</font></td>");
			out.println("<td align='center' ><FONT size='2' color='black' face='Arial'>"+Cajero+"</font></td>");

			row = new StringTokenizer(resultado,"|");
			j = 0;
			while(row.hasMoreElements()){
				valor = row.nextToken().toString();
				out.println("<td align='center' ><FONT size='2' color='black' face='Arial'>"+valor+"</font></td>");
				j++;
			}
			out.println("</tr>");
%>	<tr><td colspan="5"><hr color="red"></hr></td></tr>
	</table>
	<table align="center">
	<tr>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td align="center" valign="middle">
				<a id='btnAct' href="javascript:Regresar();" onmouseover="self.status='Regresar';return true;" onmouseout="window.status='';"><img src="../imagenes/b_regresar.gif" alt="Regresar" border="0"></a>
<!--			<a id='btnAct' href="javascript:Regresar();" onmouseover="self.status='Regresar';return true;" onmouseout="window.status='';"><img src="../images/b_regresar.gif" alt="Regresar" border="0"></a>-->
		</td>
	</tr>
	</table>
	<input type="hidden" name="userC" value="<%=userPIF%>">
</form>
</BODY>
</HTML>
