<!--
//*************************************************************************************************
//             Funcion: JSP que despliega respuesta de txn Reverso
//            Elemento: ResponseReverso.jsp
//          Creado por: Alejandro Gonzalez Castro
//      Modificado por: Rutili Zarco Reyes
//*************************************************************************************************
// CCN - 4360175 - 03/08/2004 - Ajuste size de ventana de impresion
// CCN - 4360189 - 03/09/2004 - Se modifica invoke a Reverso para Control de Efectivo
// CCN - 4360402 - 11/11/2005 - Se realizan modificaciones para poder ejecutar los reversos ligados
// CCN - 4360581 - 12/03/2007 - Se convierten los estilos en jsf y se hace un include en el jsp
//*************************************************************************************************
-->
<%@ page session="true" import="java.util.*"%>
<%
response.setHeader("Cache-Control","no-store"); //HTTP 1.1
response.setHeader("Pragma","no-cache"); //HTTP 1.0
response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<%!
  private String getString(String s)
  {
   int len = s.length();
   for(; len>0; len--)
    if(s.charAt(len-1) != ' ')
     break;
   return s.substring(0, len);
  }

  private String getItem(String newCadNum, int newOption)
  {
   int iPIndex = newCadNum.indexOf("CONSECUTIVO:") + 12, NumSaltos, Num = 0, Ban = 0;
   String nCadNum = new String("");
   NumSaltos = newOption;
   while(Num < NumSaltos)
   {
    if(newCadNum.charAt(iPIndex) == 0x20)
    {
     while(newCadNum.charAt(iPIndex) == 0x20)
      iPIndex++;
     Num++;
    }
    else
     while(newCadNum.charAt(iPIndex) != 0x20)
      iPIndex++;
   }
   while(newCadNum.charAt(iPIndex) != 0x20 && iPIndex < newCadNum.length()){
    nCadNum = nCadNum + newCadNum.charAt(iPIndex);
    if(iPIndex < newCadNum.length())
      iPIndex++;
    if(iPIndex == newCadNum.length())
      break;
   }

   return nCadNum;
  }
%>

<%
    int norev = Integer.parseInt(session.getAttribute("noreverso").toString());
    ventanilla.com.bital.admin.DiarioReverso DiarioRev = null;
    if(norev == 0)
    {
        if(session.getAttribute("session.DiarioRev") == null)
        {
            DiarioRev = new ventanilla.com.bital.admin.DiarioReverso();
            session.setAttribute("session.DiarioRev", DiarioRev);
        }
        int indice = Integer.parseInt(request.getParameter("indice").toString());
        int nopagina = Integer.parseInt(request.getParameter("nopagina").toString());
        nopagina--;

        ventanilla.com.bital.admin.DiarioQuery DiarioSQL = null;

        DiarioSQL = (ventanilla.com.bital.admin.DiarioQuery)session.getAttribute("session.DiarioSQL");
        Hashtable Data = new Hashtable();
        Data = DiarioSQL.getData(nopagina);
        Vector Cdate    = new Vector();
        Vector Cconlink = new Vector();

        Cdate       = (Vector)Data.get("Date");
        Cconlink    = (Vector)Data.get("ConLink");

        String cajero   = request.getParameter("cajero").toString();
        String date     = (String)Cdate.get(indice);
        String conlink  = (String)Cconlink.get(indice);
        String consec   = request.getParameter("consecutivo").toString();
        String supervisor = request.getParameter("supervisor").toString();

        DiarioRev.setConsecRev(consec);
        DiarioRev.invokeReverso(cajero, supervisor, date, conlink);
        session.setAttribute("consecutivoLiga", conlink);
    }

    DiarioRev = (ventanilla.com.bital.admin.DiarioReverso)session.getAttribute("session.DiarioRev");

    int code = DiarioRev.executeReverso(norev, session);

    Vector resp = DiarioRev.getMessage(code);
    String txn = DiarioRev.getTxnCode();
    String codigo = "";
    String cTxn = (String) session.getAttribute("page.cTxn");
    
        if(session.getAttribute("DELETESIB")!=null){
    	if(session.getAttribute("DELETESIB").equals("DELETESIB")){
    	    	out.println("<b>Registro formulario UAF eliminado</b><br>");
    	    	session.removeAttribute("DELETESIB");
    	    	}
    	    }
    

    if(resp.size() != 4 )
    	out.println("<b>Error de conexi&oacute;n</b><br>");
    else
    {
        try
        {
            codigo = (String)resp.elementAt(0);
            if( codigo.equals("1"))
                out.println("<b>Transaccion Rechazada</b><br><br>");
            if( codigo.equals("0") || codigo.equals("1") )
            {
                int lines = Integer.parseInt((String)resp.elementAt(1));
                String szResp = (String)resp.elementAt(3);
                for(int i=0; i<lines; i++)
                {
                    String line = szResp.substring( i*78, Math.min(i*78+77, szResp.length()) );
                    out.println(getString(line) + "<p>");
                    if(Math.min(i*78+77, szResp.length()) == szResp.length())
                        break;
                }
            }
            else
            {
                if ( codigo.equals("T"))
                    out.println("<b>Ocurri&oacute; un Error de Comunicaci&oacute;n, Verifique.</b>");
                else
                    out.println("<b>Transacci&oacute;n Requiere Autorizaci&oacute;n</b>");
            }
        }
        catch(NumberFormatException nfe){out.println("<b>Error de conexi&oacute;n</b><br>");}
    }
    norev++;
    session.setAttribute("noreverso", String.valueOf(norev));
    int totalrevs = DiarioRev.getNumRevs();
    if(codigo.equals("1") && txn.equals("DSLZ"))
        session.setAttribute("txtCadImpresion", "NOIMPRIMIRNOIMPRIMIRNOIMPRIMIR");
%>
<html>
<head>
    <title>Diario</title>
    <%@include file="style.jsf"%>
<script language="javascript" src="validate.js">
</script>
</head>
<body bgcolor="#FFFFFF">
<form name="diariorev" action="ResponseReverso.jsp" method="post">
    <input type="hidden" name="alfa" value="0">
</form>
<%
    if(session.getAttribute("txtCadImpresion") != null)
      session.removeAttribute("txtCadImpresion");
    session.setAttribute("page.cTxn", DiarioRev.getTxnCode());
    session.setAttribute("Consecutivo", DiarioRev.getConsecutivo()); // Para Certificación
    session.setAttribute("Tipo", "C"); // Para Certificación
    session.setAttribute("isreverso","si");

    if(DiarioRev.getTxnCode().equals("0528"))
        session.setAttribute("txtCadImpresion", "NOIMPRIMIRNOIMPRIMIR");

    out.println("<script>openDialog('../../servlet/ventanilla.ImprimirServlet', 160, 120, 'top.setPrefs13()', document.diariorev)</script>"); // Para Certificación
    
    if(norev < totalrevs)
    {
        session.setAttribute("ReversoLink", "S");
    }

    // Control Posting de Reversos - Stop when failed
    if(codigo.equals("1"))
    {
        session.setAttribute("ReversoLink", "N");
    }
%>

</body>
</html>
