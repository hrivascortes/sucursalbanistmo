<!--
//*************************************************************************************************
//             Funcion: JSP que despliega respuesta de txn 5103
//            Elemento: Response5103.jsp
//          Creado por: Alejandro Gonzalez Castro
//      Modificado por: Alejandro Gonzalez Castro
//*************************************************************************************************
// CCN - 4360175 - 03/08/2004 - Ajuste size de ventana de impresion
// CCN - 4360275 - 18/02/2005 - WAS 5.1
//*************************************************************************************************
-->
<%@ page session="true" import="java.util.*"%>
<%
response.setHeader("Cache-Control","no-store"); //HTTP 1.1
response.setHeader("Pragma","no-cache"); //HTTP 1.0
response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<%
%>
<%!
  private String getString(String s)
  {
    int len = s.length();
    for(; len>0; len--)
	  if(s.charAt(len-1) != ' ')
	    break;

    return s.substring(0, len);
  }

 private String formatMonto(String s)
  {
    int len = s.length();
    if (len < 3)
    	s = "0." + s;
    else
    {
      int n = (len - 3 ) / 3;
    	s = s.substring(0, len - 2) + "." + s.substring(len - 2 , len);
      for (int i = 0; i < n; i++)
      {
      	len = s.length();
    		s = s.substring(0, (len -(i*3+6+i))) + "," + s.substring((len -(i*3+6+i)) , len);
    	}
    }
    return s;
  }

   private String quitaespacios(String s){
      String s1 = "";
      s1 = s;
      s = "";
      int len = s1.length();
      for ( int i = 0; i < len; i++ ){
        if ( s1.charAt(i) != ' ') {
           s = s + s1.charAt(i);
         }
        else
           s = s + "%20";
      }
      return s;
   }
%>
<% Vector resp = (Vector)session.getAttribute("response");
Hashtable datasession= (Hashtable)session.getAttribute("page.datasession");
String estatus = (String)resp.elementAt(0);
String titular = "";


if (estatus.equals("0") ) {     // Transaccion aceptada

   Vector lineas = new Vector();

   int numlines = Integer.parseInt((String)resp.elementAt(1));
   String szRespuesta = (String)resp.elementAt(3);
   for(int i=0; i<numlines; i++){
      String linea = szRespuesta.substring( i*78, Math.min(i*78+77, szRespuesta.length()) );
      lineas.addElement(getString(linea));
      if(Math.min(i*78+77, szRespuesta.length()) == szRespuesta.length())
       break;
     }
   titular = (String)lineas.elementAt(1);
}

String montopro;
  if ( (String)datasession.get("MontoTotal") == null)
       montopro = "0";
  else
       montopro = (String)datasession.get("MontoTotal");

String montotot = (String)datasession.get("txtCheque");

montopro = formatMonto(montopro);
String needOverride = "NO";
%>

<html>
<head>
  <title>Certificación
<%!
   private String getItem(String newCadNum, int newOption)
  {
   int iPIndex = newCadNum.indexOf("CONSECUTIVO:") + 12, NumSaltos, Num = 0, Ban = 0;
   String nCadNum = new String("");
   NumSaltos = newOption;
   while(Num < NumSaltos)
   {
    if(newCadNum.charAt(iPIndex) == 0x20)
    {
     while(newCadNum.charAt(iPIndex) == 0x20)
      iPIndex++;
     Num++;
    }
    else
     while(newCadNum.charAt(iPIndex) != 0x20)
      iPIndex++;
   }
   while(newCadNum.charAt(iPIndex) != 0x20 && iPIndex < newCadNum.length()){
    nCadNum = nCadNum + newCadNum.charAt(iPIndex);
    if(iPIndex < newCadNum.length())
      iPIndex++;
    if(iPIndex == newCadNum.length())
      break;
   }


   return nCadNum;
  }
 %>
</title>
<style type="text/css">Body {font-family: Arial, Helv; font-size: 10pt; font-weight: bold; color:#000000}</style>
<script language="javascript">
<!--
  sharedData = '0';

  function formSubmitter()
  {
    clearInterval(formSubmitter);
    
    if(sharedData != '0')
    {
      document.forms[0].submit()
      sharedData = '0';
    }
  }
//-->
</script>
</head>
<body bgcolor="#FFFFFF" onload="top.setFieldFocus(window.document);setInterval('formSubmitter()',2000)">
<form name="Txns" action="../../servlet/ventanilla.PageServlet" method="post">
<table border="0" cellspacing="0">
<tr>
<td>
</td>
</tr>
<tr>
<td>
<%
  String isCertificable = (String)session.getAttribute("page.certifField");  // Para Certificación
  String txtCadImpresion  = ""; // Para Certificación
  String cajero = (String)datasession.get("teller");
  String liga = (String)session.getAttribute("d_ConsecLiga");
  if ( montopro.equals(montotot))  {
      txtCadImpresion = "~REMESA~" + titular +"~"+cajero+"~"+liga+"~";
  }
  Stack flujotxn = (Stack)session.getAttribute("page.flujotxn");
  if(!flujotxn.empty())
  {
    out.print("<div align=\"center\">");
    
    if(!isCertificable.equals("N"))
    {
      // Para Certificación
      session.setAttribute("Consecutivo", getItem((String)resp.elementAt(3), 1)); // Para Certificación
      session.setAttribute("Tipo", isCertificable); // Para Certificación
      session.setAttribute("txtCadImpresion", txtCadImpresion); // Para Certificación
      
      if(estatus.equals("0"))
        out.println("<script language=\"javascript\">top.openDialog('../../servlet/ventanilla.ImprimirServlet?txtCadImpresion', 160, 120, 'top.setPrefs4()', document.Txns)</script>"); // Para Certificación
      else if ( estatus.equals("2") || estatus.equals("3"))
      {
	needOverride = "SI";
	out.print("<script languaje=\"javascript\"> top.openDialog('authoriz.jsp', 220, 130, 'top.setPrefs8()', document.Txns) </script>");
      }
      else
	out.println("<a href=\"javascript:top.openDialog('../../servlet/ventanilla.ImprimirServlet?txtCadImpresion', 160, 120, 'top.setPrefs4()', document.Txns)\"><img src=\"imagenes/b_continuar.gif\" border=\"0\"></a>"); // Para Certificación
    }
    else
    {
      if(estatus.equals("0"))
      {
	out.println("<script language=\"javascript\">sharedData = '1'</script>");
	out.println("<font color='red'>procesando...</font>");
      }
      else if ( estatus.equals("2") || estatus.equals("3")) 
      {
	needOverride = "SI";
	out.print("<script languaje=\"javascript\"> top.openDialog('authoriz.jsp', 220, 130, 'top.setPrefs8()', document.Txns) </script>");
      }
      else
	out.print("<input type=\"image\" src=\"imagenes/b_continuar.gif\" alt=\"\" border=\"0\">");
    }
    
    out.print("</div>");
  }
  else
  {
    session.setAttribute("Consecutivo", getItem((String)resp.elementAt(3), 1)); // Para Certificación
    session.setAttribute("Tipo", isCertificable); // Para Certificación
    session.setAttribute("txtCadImpresion", txtCadImpresion); // Para Certificación
    
    if ( estatus.equals("3") || estatus.equals("2"))
    {
      needOverride = "SI";
      out.print("<script languaje=\"javascript\"> top.openDialog('authoriz.jsp', 220, 130, 'top.setPrefs8()', document.Txns) </script>");
    }
    else 
    {
      out.println("<script language=\"javascript\">top.openDialog('../../servlet/ventanilla.ImprimirServlet', 160, 120, 'top.setPrefs4()', document.Txns)</script>"); // Para Certificación
    }
  }
%>

  <input type="hidden" name="transaction" value="<%= session.getAttribute("page.iTxn") %>">
<%

  if ( montopro.equals(montotot))  {

      out.println("<input type=\"hidden\" name=\"transaction1\" value=\""+ session.getAttribute("page.oTxn")+"\">");
   }
  else{
      out.println("<input type=\"hidden\" name=\"transaction1\" value=\""+ session.getAttribute("page.iTxn")+"\">");
     }
 %>
  <input type="hidden" name="moneda" value="<%= session.getAttribute("page.moneda") %>">
  <input type="hidden" name="supervisor" value="<%= session.getAttribute("supervisor") %>">
  <input type="hidden" name="override" value="<%= needOverride%>">
</td>
</tr>
</table>
</form>
<p>
<%
   if( resp.size() != 4 )
    out.println("<b>Error de conexi&oacute;n</b><br>");
   else
   {
    try{
     String codigo = (String)resp.elementAt(0);
 	if( codigo.equals("1"))
   	   out.println("<b>Transaccion Rechazada</b><br><br>");
          if( codigo.equals("0") || codigo.equals("1") ) {
     	   int lines = Integer.parseInt((String)resp.elementAt(1));
    	   String szResp = (String)resp.elementAt(3);
    	   for(int i=0; i<lines; i++) {
    		   String line = szResp.substring( i*78, Math.min(i*78+77, szResp.length()) );
     		   out.println(getString(line) + "<p>");
     		   if(Math.min(i*78+77, szResp.length()) == szResp.length())
      		    break;
    	   }
       }
       else {
         if ( codigo.equals("3") || codigo.equals("2"))
            out.println("<b>Transacci&oacute;n Requiere Autorizaci&oacute;n</b>");
         else
            out.println("<b>Ocurri&oacute; un Error de Comunicaci&oacute;n, Verifique.</b>");
       }
  }
    catch(NumberFormatException nfe){
     out.println("<b>Error de conexi&oacute;n</b><br>");
    }
   }
%>
</body>
</html>
