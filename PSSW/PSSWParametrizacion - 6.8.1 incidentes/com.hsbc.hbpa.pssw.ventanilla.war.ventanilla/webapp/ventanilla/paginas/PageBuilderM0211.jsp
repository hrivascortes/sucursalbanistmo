<!--

// CCN - 4360581 - 12/03/2007 - Se convierten los estilos en jsf y se hace un include en el jsp
-->

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<!-- PageBuilderM0211.jsp -->

<%@page session="true" import="java.util.Hashtable"%>
<%
response.setHeader("Cache-Control","no-store"); //HTTP 1.1
response.setHeader("Pragma","no-cache"); //HTTP 1.0
response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
%>
<html>
<head>
  <title>Alta de usuario temporal</title>
  <%@include file="style.jsf"%>
<%
  Hashtable info = (Hashtable)session.getAttribute("info.user");
%>
<script language="JavaScript" type="text/javascript">
<!--
var checkval = true
function valida(value)
{
  checkval = value
}

function AllTrim(inputStr)
{
  while( inputStr.charAt(0) == ' ' )
    inputStr = inputStr.substring(1, inputStr.length)

  while( inputStr.charAt(inputStr.length-1) == ' ' )
    inputStr = inputStr.substring(0, inputStr.length-1)

  return inputStr
}

function valregexp(field, regexp)
{
  var inputStr = AllTrim(field.value.toUpperCase())
  field.value = inputStr

  if( inputStr.length == 0 )
    return false
  
  var buf = inputStr.match(regexp)
  if( buf != inputStr )
    return false
  return true
}

function validateForm(form)
{
  if( !checkval )
    return true
  
  if( !valregexp(form.nombre, '[A-Z.,� ]{1,40}') )
  {
    alert('Caracteres inv�lidos en el nombre, verifique')
    form.nombre.focus()
    return false
  }
  if( !valregexp(form.promotor, '[A-Z][0-9]{4}') )
  {
    alert('La clave de promotor es incorrecta')
    form.promotor.focus()
    return false
  }
  if( !valregexp(form.origen, '[0-9]{1,4}') )
  {
    alert('N�mero de sucursal no v�lido')
    form.origen.focus()
    return false
  }
  else
  {
    while( form.origen.value.length < 4 )
      form.origen.value = '0' + form.origen.value
  }
  if( !valregexp(form.usuario, '[A-Z]{2}[0-9]{6}') )
  {
    alert('La clave de Usuario RACF es incorrecta')
    form.usuario.focus()
    return false
  }

  var usuario = form.usuario.value.substring(2)
  if( form.registro.value.indexOf(usuario) < 0 )
  {
    alert('Usuario RACF no corresponde con el Registro de Empleado')
    form.usuario.focus()
    return false
  }

  return true
}
//-->
</script>
</head>
<body bgcolor="white" onload="top.setFieldFocus(window.document)">
<p><br>
<b>Datos del Usuario de Apoyo</b>
<form action="ventanilla.Group03GTE" method="post" onsubmit="return validateForm(this)">
<table border="0" cellspacing="0" width="600">
<tr>
  <td><b>Nombre:</b></td>
  <td><input type="text" name="nombre" size="40" maxlength="40"></td>
</tr>
<tr>
  <td><b>Promotor a asignar:</b></td>
  <td><input type="text" name="promotor" size="5" maxlength="5"></td>
</tr>
<tr>
  <td><b>Sucursal de Origen:</b></td>
  <td><input type="text" name="origen" size="4" maxlength="4"></td>
</tr>
<tr>
  <td><b>Sucursal a Asignar:</b></td>
  <td><%= info.get("sucursal") %></td>
</tr>
<tr>
  <td><b>Registro:</b></td>
  <td><%= info.get("registro") %></td>
</tr>
<tr>
  <td><b>Usuario RACF:</b></td>
  <td><input type="text" name="usuario" size="8" maxlength="8"></td>
</tr>
<tr>
  <td><b>Per�odo (d�as naturales):</b></td>
  <td><%= info.get("periodo") %></td>
</tr>
<tr>
  <td colspan="2">&nbsp;</td>
</tr>
<tr>
  <td><input type="image" name="[Cancelar]" src="../ventanilla/imagenes/b_cancelar.gif" border="0" onclick="valida(false)"></td>
  <td align="right"><input type="image" name="[Aceptar]" src="../ventanilla/imagenes/b_aceptar.gif" border="0" onclick="valida(true)"></td>
</tr>
</table>
<input type="hidden" name="registro" value="<%=info.get("registro")%>">
<input type="hidden" name="confirmar" value="yes">
</form>
</body>
</html>