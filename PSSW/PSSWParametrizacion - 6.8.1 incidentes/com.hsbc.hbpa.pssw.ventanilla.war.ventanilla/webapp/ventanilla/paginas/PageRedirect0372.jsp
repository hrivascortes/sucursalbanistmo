<!--
//*************************************************************************************************
//             Funcion: JSP para redireccion de txn 0372
//            Elemento: PageRedirect0372.jsp
//      Modificado por: Juvenal R. Fernandez V.
//*************************************************************************************************
// CCN - 4360334 - 17/06/2005 - Se modifica redireccion a relativa para WAS5
//*************************************************************************************************
-->

<%@page session="true" %>
<%
response.setHeader("Cache-Control","no-store"); //HTTP 1.1
response.setHeader("Pragma","no-cache"); //HTTP 1.0
response.setDateHeader ("Expires", 0); //prevents caching at the proxy server*/
%>
<%
  String proceso = (String)session.getAttribute("txnProcess");
  if( proceso.equals("05") )
  {
    response.sendRedirect("page0372.jsp");
  }
  else if( proceso.equals("13") )
  {
    response.sendRedirect("PageBuilder0372.jsp");
  }
%>
