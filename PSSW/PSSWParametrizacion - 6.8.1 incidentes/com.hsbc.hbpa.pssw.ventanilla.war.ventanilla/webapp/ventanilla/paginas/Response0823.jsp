<!--
//*************************************************************************************************
//             Funcion: JSP que presenta las opciones de la txn 0823
//            Elemento: Response0823.jsp
//          Creado por: Alejandro Gonzalez Castro
//      Modificado por: Fredy Pe�a Moreno
//*************************************************************************************************
// CCN - 4360163 - 06/07/2004 - Se controla click sobre el boton aceptar
// CCN - 4360189 - 03/09/2004 - Se redirecciona a Final.jsp por Control de Efectivo
// CCN - 4360204 - 24/09/2004 - Se ajusta bot�n de cancelar para que aparezca a la altura correcta
// CCN - 4360274 - 16/02/2005 - se corrige despliegue de tipo de cambio
// CCN - 4360306 - 22/04/2005 - Se realizan modificaciones para el WAS 5.1
// CCN - 4360477 - 19/05/2006 - Se agrega validaci�n para no permitir monto cero en dolares en la txn 0726-0106
// CCN - 4360489 - 14/06/2005 - Se realizan correcciones en txn 0729-0778
// CCN - 4360533 - 20/10/2006 - Se realizan modificaciones para OPEE
// CCN - 4360576 - 09/03/2007 - Modificaciones para proyecto OPMN
// CCN - 4360619 - 15/06/2007 - Se eliminan systems
// CCN - 4620048 - 14/12/2007 - Se modifica para compra venta
//*************************************************************************************************
-->

<%@ page session="true" import="java.util.*,java.util.Stack,ventanilla.com.bital.beans.DatosVarios,ventanilla.GenericClasses"%>
<%
response.setHeader("Cache-Control","no-store"); //HTTP 1.1
response.setHeader("Pragma","no-cache"); //HTTP 1.0
response.setDateHeader ("Expires", 0); //prevents caching at the proxy server

String Itxn = session.getAttribute("page.iTxn").toString();
String Otxn = session.getAttribute("page.oTxn").toString();
//Asignar valor de idflujo para flujos internos
   if((Itxn.equals("0128")||Itxn.equals("0130")) && Otxn.equals("4101")){
     session.setAttribute("idFlujo","07");
   }
GenericClasses gc = new GenericClasses();
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<%!
  private String getString(String s)
  {
    int len = s.length();
    for(; len>0; len--)
	  if(s.charAt(len-1) != ' ')
	    break;

    return s.substring(0, len);
  }

  private String getItem(String newCadNum, int newOption)
  {
   int iPIndex = newCadNum.indexOf("CONSECUTIVO:") + 12, NumSaltos, Num = 0, Ban = 0;
   String nCadNum = new String("");
   NumSaltos = newOption;
   while(Num < NumSaltos)
   {
    if(newCadNum.charAt(iPIndex) == 0x20)
    {
     while(newCadNum.charAt(iPIndex) == 0x20)
      iPIndex++;
     Num++;
    }
    else
     while(newCadNum.charAt(iPIndex) != 0x20)
      iPIndex++;
   }
   while(newCadNum.charAt(iPIndex) != 0x20 && iPIndex < newCadNum.length()){
    nCadNum = nCadNum + newCadNum.charAt(iPIndex);
    if(iPIndex < newCadNum.length())
      iPIndex++;
    if(iPIndex == newCadNum.length())
      break;
   }
  
   return nCadNum;
  }

  private String delLeadingfZeroes(String newString){
   StringBuffer newsb = new StringBuffer(newString);
   int i = 0;
   while(newsb.charAt(i) == '0')
    newsb.deleteCharAt(0);
	
   return newsb.toString();
  }

  private String delCommaPointFromString(String newCadNum)
  {
   String nCad = new String(newCadNum);
   if( nCad.indexOf(".") > -1)
   {
    nCad = nCad.substring(nCad.indexOf(".") + 1, nCad.length());
    if(nCad.length() != 2)
    {
     String szTemp = new String("");
     for(int j = nCad.length(); j < 2; j++)
      szTemp = szTemp + "0";
     newCadNum = newCadNum + szTemp;
    }
   }
   if (newCadNum.length() == 2)
      newCadNum = "0" + newCadNum;
   if (newCadNum.length() == 1)
      newCadNum = "00" + newCadNum;
   StringBuffer nCadNum = new StringBuffer(newCadNum);
   for(int i = 0; i < nCadNum.length(); i++)
    if(nCadNum.charAt(i) == ',' || nCadNum.charAt(i) == '.')
     nCadNum.deleteCharAt(i);
   
   return nCadNum.toString();
  }

  private String fillWithZeroes(String newString){
   int Indice = newString.indexOf(".");
   for(int i = Indice; i < Indice + 1; i++)
    newString = newString + "0";
	
   return newString;
  }

  private String setFormat(String newCadNum)
  {
    int iPIndex = newCadNum.indexOf(".");
    String nCadNum = new String(newCadNum.substring(0, iPIndex));
    for (int i = 0; i < (int)((nCadNum.length()-(1+i))/3); i++)
     nCadNum = nCadNum.substring(0, nCadNum.length()-(4*i+3))+','+nCadNum.substring(nCadNum.length()-(4*i+3));

    return nCadNum + newCadNum.substring(iPIndex, newCadNum.length());
  }

  private String setPointToString(String newCadNum)
  {
   int iPIndex = newCadNum.indexOf("."), iLong;
   String szTemp;

   if(iPIndex > 0)
   {
    newCadNum = new String(newCadNum + "00");
    newCadNum = newCadNum.substring(0, iPIndex + 1) + newCadNum.substring(iPIndex + 1, iPIndex + 3);
   }
   else
   {
    for(int i = newCadNum.length(); i < 3; i++)
     newCadNum = "0" + newCadNum;
    iLong = newCadNum.length();
    if(iLong == 3)
     szTemp = newCadNum.substring(0, 1);
    else
     szTemp = newCadNum.substring(0, iLong - 2);
    newCadNum = szTemp + "." + newCadNum.substring(iLong - 2);
   }
   
   return newCadNum;
  }
%>
<%
  Vector resp = (Vector)session.getAttribute("response");
  Hashtable datasession = (Hashtable)session.getAttribute("page.datasession");
  String oTxn = (String)session.getAttribute("page.oTxn");
  String iTxn = (String)session.getAttribute("page.iTxn");
  String Currency = (String)session.getAttribute("page.moneda");
  String moneda_s = (String)session.getAttribute("page.moneda_s");
  String codigo = new String("");
  String BanMonto = "NE";
  String cvmoneda = "";
    if(Currency.equals("01"))
        cvmoneda=moneda_s;
    else
        cvmoneda=Currency;
  cvmoneda = gc.getDivisa(cvmoneda);
%>
<html>
<head>
  <title>Certificaci�n</title>
<style type="text/css">Body {font-family: Arial, Helv; font-size: 10pt; font-weight: bold; color:#000000}</style>
</head>
<body>
<p>
<%
   String szResp = new String("");
   if( resp.size() != 4 )
    out.println("<b>Error de conexi&oacute;n</b><br>");
   else
   {
//    try{
     codigo = (String)resp.elementAt(0);
     if( !codigo.equals("0") )
      out.println("<b>Error en transaccion</b><br>");
     int lines = Integer.parseInt((String)resp.elementAt(1));
     szResp = (String)resp.elementAt(3);
     
     for(int i=0; ; i++){
      String line = szResp.substring( i*78, Math.min(i*78+77, szResp.length()) );
      out.println(getString(line) + "<p>");
      if(Math.min(i*78+77, szResp.length()) == szResp.length())
       break;
  
   }
/*    }
    catch(NumberFormatException nfe){
     out.println("<b>Error de conexi&oacute;n</b><br>");
    }*/
   }
%>
<form name="Txns" action="../../servlet/ventanilla.PageServlet" method="post">
<%
  if ( codigo.equals("0")) {
    String Monto = delCommaPointFromString((String)datasession.get("txtMonto1") + "00");
    long lMonto = Long.parseLong(Monto);
    String MontoUSD = delCommaPointFromString((String)datasession.get("txtMonto1"));
    String MontoLimite = "";
    long USDMonto = Long.parseLong(MontoUSD);
    Long NumeroC = new Long(0);
    String txtComisionMN = "0.00";
    String txtComisionUSD = "0.00";
    String txtIvaMN = "0.00";
    String txtIvaUSD = "0.00";
    String reqauto = "0";
    int iTemp = 0;
    String Numero = new String("0.00");
    if(codigo.equals("0")){
    if ( !iTxn.equals("0726") || ( iTxn.equals("0726") && ( !oTxn.equals("4115") && !oTxn.equals("0594")))) {
       String tipoCambio = getItem(szResp,7);
       String PlazaTP = "0" + getItem(szResp,8);
       //MontoLimite = delLeadingfZeroes(getItem(szResp,9));
       MontoLimite = "00000001000000";
       tipoCambio = delLeadingfZeroes(tipoCambio.substring(0, tipoCambio.length() - 5));
        if(lMonto > 0){
           NumeroC = new Long(lMonto * Long.parseLong(tipoCambio) / 1000000);
           Monto = setFormat(setPointToString(delCommaPointFromString(NumeroC.toString())));
           datasession.put("txtMonto",Monto);
        }
        else{
           iTemp = 1;
           Monto = delCommaPointFromString((String)datasession.get("txtMonto"));
           NumeroC = new Long((Long.parseLong(Monto + "0000") / Long.parseLong(tipoCambio)));
           Monto = setFormat(setPointToString(delCommaPointFromString(NumeroC.toString())));
           datasession.put("txtMonto1",Monto);
           MontoUSD = delCommaPointFromString((NumeroC.toString()));
        }
        Monto = MontoUSD.substring(MontoUSD.length() - 2, MontoUSD.length());
        if ( !Monto.equals("00") &&  oTxn.equals("0106") && !iTxn.equals("C726") ) {
            if ( !iTxn.equals("4057") && !iTxn.equals("4059") && !iTxn.equals("4063")) {
               Monto = MontoUSD.substring(0,MontoUSD.length() - 2 ) + "00";
               datasession.put("txtMonto1",Monto);
               NumeroC = new Long(Monto);
               lMonto = Long.parseLong(MontoUSD.substring(0,MontoUSD.length() - 2 ) + "0000");
               Long MontoMN = new Long(lMonto * Long.parseLong(tipoCambio) / 1000000);
               Monto = setFormat(setPointToString(delCommaPointFromString(MontoMN.toString())));
               USDMonto = NumeroC.longValue();
               datasession.put("txtMonto",Monto);
           }
        }
       Numero = setFormat(setPointToString(delCommaPointFromString(NumeroC.toString())));
       datasession.put("txtTipoCambio", tipoCambio);
       datasession.put("txtTC", getItem(szResp,7));
       datasession.put("txtPlaza", PlazaTP);
       datasession.put("txtTotalMN", (String)datasession.get("txtMonto"));
       datasession.put("txtTotalUSD", (String)datasession.get("txtMonto1"));
       datasession.put("txtComisionMN", txtComisionMN);
       datasession.put("txtComisionUSD", txtComisionUSD);
       datasession.put("txtIvaMN", txtIvaMN);
       datasession.put("txtIvaUSD", txtIvaUSD);
     }
    else
       MontoLimite = (String)datasession.get("txtMontoLimite");
     if ( Currency.equals("01"))
         datasession.put("txtMontoC", (String)datasession.get("txtMonto1"));
     else
         datasession.put("txtMontoC", (String)datasession.get("txtMonto"));
     if ( iTxn.equals("0726") ||  oTxn.equals("0106")) {
        if ( USDMonto > Long.parseLong(MontoLimite)) {
           reqauto = "1";
        }
     }
     if ( iTxn.equals("0112") || iTxn.equals("0114"))
        datasession.put("txtMontoPesos", (String)datasession.get("txtMonto"));
     session.setAttribute("page.reqauto",(String)reqauto);
     session.setAttribute("page.datasession", (Hashtable)datasession);
   }
}
%>
<% if ( codigo.equals("0")) { %>
<table border="0" cellspacing="0">
<tr>
 <table>
    <%String TipoCambio = (String)datasession.get("txtTipoCambio");
    java.text.DecimalFormat reformattedValue = new java.text.DecimalFormat("#,###.0000");
    String returnTipoCambio = reformattedValue.format((double)(java.lang.Double.parseDouble(TipoCambio) / 10000.00));
      if(iTxn.equals("4033")){
       DatosVarios montos = DatosVarios.getInstance();                   
       Vector vMontos = new Vector();        
       vMontos =(Vector)montos.getDatosV("MontosOPMN");
       String MontoCV = delCommaPointFromString(datasession.get("txtMonto").toString());
      if( Integer.parseInt(MontoCV) > Integer.parseInt(vMontos.get(1).toString()) ){        
         BanMonto = "E"; 
    %>  
      <tr>
       <td colspan="5"><font color="red">***MONTO LIMITE EXCEDIDO***</font></td>      
      </tr>    
    <%}
     }
	//String divisa, divisa_s;
	//if (Currency.equals("01"))
  	//	divisa = "USD$";
  	//else
  	//	divisa = gc.getDivisa(Currency);
    %> 
  <tr>
   <td>
    <table>  
      <tr>
       <td>Monto USD:</td>
       <td width="10">&nbsp;</td>
       <td align="right"><%
              out.print(setFormat(setPointToString(delCommaPointFromString((String)datasession.get("txtMonto")))));
         %></td>
      </tr>
   <!--   <tr>
       <td>Comisi�n:</td>
       <td width="10">&nbsp;</td>
       <td align="right"><%
           // out.print(setFormat(setPointToString(delCommaPointFromString((String)datasession.get("txtComisionMN")))));
        %> </td>
      </tr>
      <tr>
       <td>IVA:</td>
       <td width="10">&nbsp;</td>
       <td align="right"><%
           //out.print(setFormat(setPointToString(delCommaPointFromString((String)datasession.get("txtIvaMN")))));
        %> </td>
      </tr>-->
      <tr>
       <td>Total:</td>
       <td width="10">&nbsp;</td>
       <td align="right"><%
           out.print(setFormat(setPointToString(delCommaPointFromString((String)datasession.get("txtTotalMN")))));
           //if (moneda_s.equals("01"))
		  	//divisa_s = "USD";
  		   //else
  			//divisa_s = gc.getDivisa(moneda_s);
           %></td>
      </tr>
    </table>
   </td>
   <td>
    <table>
      <tr>
       <td>Monto <%=cvmoneda%>:</td>
       <td width="10">&nbsp;</td>
       <td align="right"><%
              out.print(setFormat(setPointToString(delCommaPointFromString((String)datasession.get("txtMonto1")))));
           %></td>
      </tr>
   <!--   <tr>
       <td>Comisi�n:</td>
       <td width="10">&nbsp;</td>
       <td align="right"><%
           // out.print(setFormat(setPointToString(delCommaPointFromString((String)datasession.get("txtComisionUSD")))));
        %>
       </td>
      </tr>
      <tr>
       <td>IVA:</td>
       <td width="10">&nbsp;</td>
       <td align="right"><%
            //out.print(setFormat(setPointToString(delCommaPointFromString((String)datasession.get("txtIvaUSD")))));
        %></td>
      </tr>-->
      <tr>
       <td>Total:</td>
       <td width="10">&nbsp;</td>
       <td align="right"><%
            out.print(setFormat(setPointToString(delCommaPointFromString((String)datasession.get("txtTotalUSD")))));
        %></td>
      </tr>
    </table>
   </td>
  </tr>
 </table>
</tr>
 <tr>
 <td>Tipo de Cambio :</td>
 <td width="10">&nbsp;</td>
  <td align="right"><%=returnTipoCambio%></td>
</tr>
</table>
<br><br>
<% } %>
<%
  Stack flujotxn = (Stack)session.getAttribute("page.flujotxn");
  if(!flujotxn.empty() || (Itxn.equals("0128") && Otxn.equals("4101")) || (Itxn.equals("0130") && Otxn.equals("4101")) ){
   out.print("<div align=\"left\">");
   if((iTxn.equals("4033") && BanMonto.equals("NE")) || !iTxn.equals("4033")){
     out.println("<a id='btnCont' href=\"javascript:sub()\" ><img src=\"../imagenes/b_aceptar.gif\" border=\"0\"></a>");
   }  
   out.println("<a href=\"javascript:cancel()\" ><img src=\"../imagenes/b_cancelar.gif\" border=\"0\"></a>");
   out.print("</div>");
  }
%>
  <input type="hidden" name="transaction" value="<%= session.getAttribute("page.iTxn") %>">
  <input type="hidden" name="transaction1" value="<%= session.getAttribute("page.oTxn") %>">
</form>
<script>
function cancel()
{
    document.Txns.action="../paginas/Final.jsp";
    document.Txns.submit();
}
function sub()
{
    if(document.Txns.transaction.value=='0726')
     {
   <% if(datasession.get("txtTotalUSD")!=null)
       {
	   	if(((String)datasession.get("txtTotalUSD")).equals("000"))
      { %>
       alert('El Total en US$ debe ser mayor o igual a US$1.00')
    <%}
      else
      {%>
    document.Txns.submit();
	 var obj = document.getElementById('btnCont')
	 obj.onclick = new Function('return false')    
	   <%}
	   }
	   else{%>
    	  document.Txns.submit();
	      var obj = document.getElementById('btnCont')
	      obj.onclick = new Function('return false')
	<%}%>
    }
    else
    {
      <%if((Itxn.equals("0128") && Otxn.equals("4101")) || (Itxn.equals("0130") && Otxn.equals("4101")) ){ 
          Stack flujoNvo = new Stack();
          flujotxn.push("4101");
          session.setAttribute("page.flujotxn",flujotxn);%>
          
         document.Txns.action = "../../servlet/ventanilla.DataServlet";
      <%}%>   
       document.Txns.submit();
	   var obj = document.getElementById('btnCont')
	   obj.onclick = new Function('return false')
	}     
}
</script>
</body>
</html>

