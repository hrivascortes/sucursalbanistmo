<!--
//*************************************************************************************************
//             Funcion: JSP que despliega respuesta del GTE
//            Elemento: DisplayGTE.jsp
//          Creado por: Alejandro Gonzalez Castro
//      Modificado por: Jes�s Emmanuel L�pez Rosales
//*************************************************************************************************
// CCN - 4360175 - 03/08/2004 - Ajuste size de ventana de impresion
// CCN - 4360189 - 03/09/2004 - Se evita actualizar usuario de RACF 
//                              y se elimina opcion de Digitalizacion
//                              Se muestra la opcion de tipo de Sucursal para Control Efectivo
// CCN - 4360275 - 18/02/2005 - WAS 5.1
// CCN - 4360347 - 22/07/2005 - Validar usuario capturado de RACF(Auditoria)
// CCN - 4360351 - 22/07/2005 - Se elimina validacion para desarrollo
// CCN - 4360510 - 08/09/2006 - Mostrar el combo de perfiles de usuario para asignarle uno al usuario
// CCN - 4360518 - 21/09/2005 - Se realiza validaci�n para no mostrar combo de perfiles cuando el usuario a modificar es un gerente o supervisor
// CCN - 4360581 - 12/03/2007 - Se convierten los estilos en jsf y se hace un include en el jsp
//*************************************************************************************************
-->
<%@ page session="true" import="java.util.*,ventanilla.com.bital.beans.*"%>
<%
response.setHeader("Cache-Control","no-store"); //HTTP 1.1
response.setHeader("Pragma","no-cache"); //HTTP 1.0
response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
DatosVarios dV = DatosVarios.getInstance();
Vector vec = new Vector();
vec =(Vector)dV.getDatosV("PERFIL");

DatosVarios perfilt = DatosVarios.getInstance();
Hashtable hPerfil = perfilt.getDatosH("PERFILT");

%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<%!
    private String nbsp(String s)
    {
        String snew = "";
        for (int i=0; i<s.length(); i++)
        {
            if (s.charAt(i) != 0x20)
                snew += s;
        }
        if (snew.length() == 0)
            return "&nbsp;";
        return s;
    }
%>

<% Hashtable data = (Hashtable)session.getAttribute("datos");
	String txn = (String)data.get("control");
	int i = 0;
	String ambiente = "d";
	javax.naming.Context initialContext = null;
	try
	{	
		initialContext = new javax.naming.InitialContext();
		ambiente = (String)initialContext.lookup("java:comp/env/AMB");
	}
	catch( javax.naming.NamingException namingException ){
		//System.out.println( "Error ConnectionPoolingManager::Context.lookup: [" + namingException.toString() + "]");
	}
	%>
<html>
<head>
<%
if (txn.equals("M031"))
{%>
	<script type="text/javascript" language="JavaScript">
	<!--
	function validate(form)
	{
	  if( !top.validate(window, form.txtFirma, 'isCurr8') )
	  {
	    form.txtFirma.focus()
	    return false
	  }
	  if( !top.validate(window, form.txtMonto, 'isCurr8') )
	  {
	    form.txtMonto.focus()
	    return false
	  }
	}
	//-->
	</script>
<%}%>
<%if (txn.equals("M011"))
{%>
	<script type="text/javascript" language="JavaScript">
	<!--
	function validate(form)
	{
	  if( !top.validate(window, form.txtUserRACF, 'UserRACF'))
	  {
	    form.txtUserRACF.focus()
	    return false
	  }
	  
	  if( !top.validate(window, form.txtNombre, 'isEmptyStr') )
	  {
	    form.txtNombre.focus()
	    return false
	  }
	}
	
	function CambioNombre(form)
	{
	   form.modifnombre.value = "S";
	  if( !top.validate(window, form.txtNombre, 'isEmptyStr') )
	  {
	    form.txtNombre.focus()
	    return false
	  }
	}
	
	function CambioPerfil(form)
	{
	   form.modifperfil.value = "S";
	   form.actperfil.value = form.lstPerfil.options[form.lstPerfil.selectedIndex].text;
	}
	
	function CambioRACF(form)
	{
		var env = '<%out.print(ambiente);%>'
	   form.modifracf.value = "S";
	   if(env == "d")
	    return true;
	   if( !top.validate(window, form.txtUserRACF, 'UserRACF') )
	   {
		    form.txtUserRACF.focus();
	    	return false;
	   }
	}
	
	//-->
	</script>
<%}%>

<%@include file="style.jsf"%>
</head>
<title>Funciones del Gerente</title>
<body onload="top.setFieldFocus(window.document)">
<br>
<%
if (txn.equals("M012"))
{
	String regs = (String)data.get("regs");
	int cols = Integer.parseInt(regs);
	String perfil = "";
	String nivel ="";
%>
     <h1>Lista de Usuarios</h1>
	<table border="1">
	<tr bgcolor="#CCCCCC" align="center">
	<td>CAJERO</td>
	<td>NOMBRE</td>
	<td>USUARIO<br>RACF</td>
	<td>REGISTRO</td>
	<td>NIVEL</td>
	<td>STATUS<br>SIGN-ON</td></tr>
	<%for(i=1;i<cols+1;i++)
	{%>
	<tr>
		<td align="center"><%=data.get("cajero"+i)%></td>
		<td><%=nbsp((String)data.get("nombre"+i))%></td>
		<td align="center"><%=nbsp((String)data.get("idracf"+i))%></td>
		<td align="center"><%=nbsp((String)data.get("regnom"+i))%></td>
		<%if(data.get("nivel"+i).equals("1"))
			{nivel = "Cajero Sucursal";}
		if(data.get("nivel"+i).equals("2"))
			{nivel = "Cajero Departamental";}
		if(data.get("nivel"+i).equals("3"))
			{nivel = "Supervisor";}
		if(data.get("nivel"+i).equals("4"))
			{nivel = "Gerente";}%>
		<td align="center"><%=nivel%></td>
		<td align="center"><%=data.get("status"+i)%></td>
	</tr>	
	<%}%>	
	</table>
<%}
if (txn.equals("M011"))
{%>
     <h1>Modificaci&oacute;n de Usuario</h1>
	<form name="entry" action="ventanilla.DBUsers" method="post" onSubmit="return validate(this)">
	<table border="0" cellspacing="0">
	<tr>	<td align=right><b>Cajero :  </b></td>
	<td>&nbsp;&nbsp;</td>
	<input name="txtIDCajero" type="hidden" size=8 maxlength=8 value="<%=data.get("1")%>">
	<td><%=data.get("1")%></td></tr>
	<tr><td align=right><b>Reactivar Contrase&ntilde;a :  </b></td>
	<td>&nbsp;&nbsp;</td>
	<td><input name="txtPssw" type="checkbox"></td></tr>
	<tr><td align=right><b>Usuario RACF :  </b></td>
	<td>&nbsp;&nbsp;</td>
	<%	
	if( ((String)session.getAttribute("tellerlevel")).equals("4") )
	{
	%>
	<td><input name="txtUserRACF" type="text" size=8 maxlength=8 value="<%=data.get("2")%>" onChange="javascript:CambioRACF(document.entry)"></td></tr>
	<%}
	else
	{
	%>
	<td><%=data.get("2")%></td></tr>
  <%}%>
	<tr><td align=right><b>Registro N&oacute;mina :  </b></td>
	<td>&nbsp;&nbsp;</td>
	<td><%=data.get("3")%></td></tr>
	<tr><td align=right><b>Nombre :  </b></td>
	<td>&nbsp;&nbsp;</td>
	<td><input name="txtNombre" type="text" size=30 maxlength=30 value="<%=data.get("4")%>" onchange="javascript:CambioNombre(document.entry)"></td></tr>
	<%
	String nivel = "";
	if(data.get("5").equals("1")){nivel = "1 - Cajero Sucursal";}
	if(data.get("5").equals("2")){nivel = "2 - Cajero Departamental";}
	if(data.get("5").equals("3")){nivel = "3 - Supervisor";}
	if(data.get("5").equals("4")){nivel = "4 - Gerente";}%>
	<tr><td align=right><b>Nivel :  </b></td>
	<td>&nbsp;&nbsp;</td>
	<td><%=nivel%></td></tr>
	<tr><td align=right><b>Horario :  </b></td>
	<td>&nbsp;&nbsp;</td>
	<td><%=data.get("6")%></td></tr>
	<tr><td align=right><b>Direcci&oacute;n IP :  </b></td>
	<td>&nbsp;&nbsp;</td>
	<td><%=data.get("7")%></td></tr>
	<%
	  String perfselec = "";
	  if(!((String)session.getAttribute("isForeign")).equals("S") && !data.get("5").equals("3") && !data.get("5").equals("4")){
	%>
	<tr><td align=right><b>Perfil :  </b></td>
	<td>&nbsp;&nbsp;</td>
	<td>
	<select name="lstPerfil" id="lstPerfil" onchange="javascript:CambioPerfil(document.entry)">
	<%
	StringTokenizer st = null;
	String clave = "";
	String desc = "";
	String id = "";	
	for(int j = 0; j < vec.size();j++){
		st = new StringTokenizer((String)vec.elementAt(j), "*");
		clave = st.nextToken();
		id = st.nextToken();
		if(id.equals("0000") && hPerfil.containsKey(clave)){
			st.nextToken();
			st.nextToken();
			desc = st.nextToken(); 
			if(clave.equals(data.get("8"))){%>
				<option value=<%=clave%> selected><%=desc%></option>
				<%perfselec = desc; %>
			<%}else{%>
				<option value=<%=clave%>><%=desc%></option>
			<%}
		}
	}%>
	</select>
	</td></tr>
	<%}%>
	</table>
	<input type="hidden" name="transaction" value="M111">
	<input type="hidden" name="transaction1" value="0">
	<input type="hidden" name="ambiente" value='<%out.print(ambiente);%>'>
	<input type="hidden" name="modifnombre" value='N'>
	<input type="hidden" name="modifracf" value='N'>
    <input type="hidden" name="modifperfil" value='N'>
	<input type="hidden" name="antracf" value='<%=data.get("2")%>'>
	<input type="hidden" name="antnombre" value='<%=data.get("4")%>'>
	<input type="hidden" name="antperfil" value='<%=perfselec%>'>	
	<input type="hidden" name="actperfil" value='<%=perfselec%>'>
		
	<p><input type="image" src="../ventanilla/imagenes/b_aceptar.gif" border="0"></p>
	</form>
<%}%>

<%
if (txn.equals("M031"))
{%>
     <h1>L&iacute;mite de Firma y Otras Opciones</h1>
	<form name="entry" action="ventanilla.DBUsers" method="post" onSubmit="return validate(this)">
	<table border="0" cellspacing="8" >
	<tr><td align=right>L�mite de Firma :  </td>
	<td><input name="txtFirma" type="text" size=10 maxlength=10 onBlur="top.estaVacio(this)||top.validate(window, this, 'isCurr8')" onKeyPress="top.keyPressedHandler(window, this,'isCurr8')"/> </tr>
	<tr><td align=left>Expedir Cheques de Caja en </td></tr>
	<tr><td align=right>Retiros de Inversiones Mayores o Iguales a :  </td>
	<td><input name="txtMonto" type="text" size=10 maxlength=10 onBlur="top.estaVacio(this)||top.validate(window, this, 'isCurr8')" onKeyPress="top.keyPressedHandler(window, this,'isCurr8')" /> </tr>

	<tr><td align=right>Hora de Corte Remesas M. N. (HH : MM : SS) :  </td>
	<td>
	<%String rempesos = (String)data.get("3");
	  String remdol = (String)data.get("4");%>
	<select name="txtRemPesosH" type="text">
	<%for(i=0;i<24;i++)
	{if(i == Integer.parseInt(rempesos.substring(0,2)))
		{if(i<10){out.println("<option selected value=\"0" + i + "\">" + 0 + i);}
			else{out.println("<option selected value=\"" + i + "\">" + i);}
		}
		else
		{if(i<10){out.println("<option value=\"0" + i + "\">" + 0 + i);}
			else{out.println("<option value=\"" + i + "\">" + i);}
		}
	}%>
	</select><b> : </b>
	<select name="txtRemPesosM" type="text">
	<%for(i=0;i<60;i++)
	{if(i == Integer.parseInt(rempesos.substring(2,4)))
		{if(i<10){out.println("<option selected value=\"0" + i + "\">" + 0 + i);}
			else{out.println("<option selected value=\"" + i + "\">" + i);}
		}
		else
		{if(i<10){out.println("<option value=\"0" + i + "\">" + 0 + i);}
			else{out.println("<option value=\"" + i + "\">" + i);}
		}
	}%>
	</select><b> : </b>
	<select name="txtRemPesosS" type="text">
	<%for(i=0;i<60;i++)
	{if(i == Integer.parseInt(rempesos.substring(4,6)))
		{if(i<10){out.println("<option selected value=\"0" + i + "\">" + 0 + i);}
			else{out.println("<option selected value=\"" + i + "\">" + i);}
		}
		else
		{if(i<10){out.println("<option value=\"0" + i + "\">" + 0 + i);}
			else{out.println("<option value=\"" + i + "\">" + i);}
		}
	}%>
	</select>
	</td>
	</tr>

	<tr><td align=right>Hora de Corte Remesas US $ (HH : MM : SS) :  </td>
	<td>
	<select name="txtRemDolaresH" type="text">
	<%for(i=0;i<24;i++)
	{if(i == Integer.parseInt(remdol.substring(0,2)))
		{if(i<10){out.println("<option selected value=\"0" + i + "\">" + 0 + i);}
			else{out.println("<option selected value=\"" + i + "\">" + i);}
		}
		else
		{if(i<10){out.println("<option value=\"0" + i + "\">" + 0 + i);}
			else{out.println("<option value=\"" + i + "\">" + i);}
		}
	}%>
	</select><b> : </b>
	<select name="txtRemDolaresM" type="text">
	<%for(i=0;i<60;i++)
	{if(i == Integer.parseInt(remdol.substring(2,4)))
		{if(i<10){out.println("<option selected value=\"0" + i + "\">" + 0 + i);}
			else{out.println("<option selected value=\"" + i + "\">" + i);}
		}
		else
		{if(i<10){out.println("<option value=\"0" + i + "\">" + 0 + i);}
			else{out.println("<option value=\"" + i + "\">" + i);}
		}
	}%>
	</select><b> : </b>
	<select name="txtRemDolaresS" type="text">
	<%for(i=0;i<60;i++)
	{if(i == Integer.parseInt(remdol.substring(4,6)))
		{if(i<10){out.println("<option selected value=\"0" + i + "\">" + 0 + i);}
			else{out.println("<option selected value=\"" + i + "\">" + i);}
		}
		else
		{if(i<10){out.println("<option value=\"0" + i + "\">" + 0 + i);}
			else{out.println("<option value=\"" + i + "\">" + i);}
		}
	}%>
	</select>
	</td>
	</tr>
	<tr><td align=right>No. NAFINSA :</td>
	<td><input name="txtNafin" type="text" value="<%=(String)data.get("5")%>" size=6 maxlength=6 onBlur="top.estaVacio(this)||top.validate(window, this, 'isnumeric')" onKeyPress="top.keyPressedHandler(window, this,'isnumeric')" /></td></tr>
        <tr><td align=right>Sucursal Tipo :</td>
         <td>
           <select name="txtTipoSuc" type="text">  
<%         Vector tipo = new Vector(5);
           tipo.add("A");
           tipo.add("B");
           tipo.add("C");
           tipo.add("D");
           tipo.add("E");
           String tipoSuc = (String)data.get("6");
           for(i=0; i<tipo.size(); i++)
           {
                if(tipo.get(i).toString().equals(tipoSuc))
                {%>
                    <option value="<%=tipo.get(i)%>" selected><%=tipo.get(i)%></option>
<%              }
                else
                {%>
                    <option value="<%=tipo.get(i)%>"><%=tipo.get(i)%></option>
<%                }
          }%>
          </select>
         </td>
        </tr>
        </table>

	<input type="hidden" name="transaction" value="M031">
	<input type="hidden" name="transaction1" value="0">
	<p><input type="image" src="../ventanilla/imagenes/b_aceptar.gif" border="0"></p>

<script>
	document.entry.txtFirma.value=top.Currency('<%=data.get("1")%>');
	document.entry.txtMonto.value=top.Currency('<%=data.get("2")%>');
</script>
</form>
<%
}%>
</body>
</html>
