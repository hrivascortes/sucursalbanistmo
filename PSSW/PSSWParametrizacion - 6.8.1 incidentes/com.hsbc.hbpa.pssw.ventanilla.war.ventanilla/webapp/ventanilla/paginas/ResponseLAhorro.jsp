<!--
//*************************************************************************************************
//             Funcion: JSP que detalle del cheque a imprimir
//            Elemento: ResponseLAhorro.jsp
//          Creado por: YGX
//*************************************************************************************************
-->

<%@ page session="true" import="java.util.*,ventanilla.GenericClasses"%>
<%
 response.setHeader("Cache-Control","no-store"); //HTTP 1.1
 response.setHeader("Pragma","no-cache"); //HTTP 1.0
 response.setDateHeader ("Expires", 0); //prevents caching at the proxy server

 Hashtable datasession = (Hashtable)session.getAttribute("page.datasession");
 Hashtable movimientos = (Hashtable)session.getAttribute("page.movimientos");
   
   String renglon      = (String) session.getAttribute("page.renglon");
   String cta_xml      = (String) session.getAttribute("page.cta_xml");
   String sucursal_xml = (String) session.getAttribute("page.sucursal_xml");
  // System.out.println("LAhorro sucursal"+sucursal_xml);		   
   String nombre_xml   = (String) session.getAttribute("page.nombre_xml");
   String error_perfil = (String) session.getAttribute("page.error_perfil");
   String error_mov    = (String) session.getAttribute("page.error_mov");
   

%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<%
	
	session.setAttribute("page.getoTxnView","Response"); 
		
	String fecha_oper =" ";
    String num_cheq   =" ";
    String tran_id    =" ";    
    String monto      =" ";
    String tipTran    =" ";
    String saldo      =" ";
    String rastrId    =" ";
    String descripcion=" ";	
%>

<html>
<script language="javascript" >

function cancelar(form)
{
  document.entry.action = "../paginas/Final.jsp";
  document.entry.submit();
}



function validates(form)
{
    var j =0;
    
    if (!document.entry.numMov.length)
       j=1
       
    for(var i =0; i < document.entry.numMov.length;i++)
    {
        if(document.entry.numMov[i].checked)
            j++;
    }
    
    if(j>0)
    {
       return true;
    }else
        
        return false;
}

</script>

<head>
<%@include file="style.jsf"%>

</head>

<body bgcolor="#FFFFFF" onload="top.setFieldFocus(window.document)">     
<form name="entry" action="../paginas/ResponseIDMOV.jsp" method="post">      
<h1><%=session.getAttribute("page.txnImage")%></h1>

<h3><%out.print(session.getAttribute("page.cTxn") + "  " +(String)session.getAttribute("page.txnlabel"));%></h3>

<%
    
  if( !error_perfil.equals("0") )
  {
    out.println("No existen datos registrados para esta Cuenta en ese rango de Fecha");        
   }
  else 
  {    	     
	   	
%>
<table border="1" align="center" cellspacing="0">
	<tr bgcolor="#CCCCCC">       
        <td align="center">Cuenta</td>
        <td align="center">Nombre</td>        
    </tr>
    <tr>       
        <td align="center"><%=cta_xml%></td>
        <td align="center"><%=nombre_xml%></td>        
    </tr>
</table>

<br>
<%
  }
  //Sucursal
  String llave="";
  String data="";  
  String mensaje = "1~1~0000000~BUSQUEDA SIN RESULTADOS~";  
  
  
  if( !error_mov.equals("0") )
  {
    out.println("No existen movimientos registrados para esta Cuenta en ese rango de Fecha");    
    session.setAttribute("page.txnresponse", mensaje);
   }
  else 
  {    	     
	   mensaje = "0~1~0000000~BUSQUEDA ACEPTADA~";
	   session.setAttribute("page.txnresponse", mensaje);	
%>

<table border="1" align="center" cellspacing="0" >

	<tr bgcolor="#CCCCCC">       
        <td align="center" colspan="9">Movimientos de la Cuenta</td>
    </tr>
    <tr bgcolor="#CCCCCC">       
    	<td align="center">     </td>
        <td align="center">Fecha</td>
        <td align="center">Cheque</td>
        <td align="center">Movimiento</td>
        <td align="center">Retiros</td>
        <td align="center">Despositos</td>        
    </tr>
<%
   
   for(int i=1; i<=movimientos.size(); i++)  
   
   {     
   	   data = (String)movimientos.get("key"+i); 
	   //System.out.println(data);	
	   	   
	   StringTokenizer tok = new StringTokenizer(data,"&");             
	   while(tok.hasMoreTokens()) 
	   { 
	    fecha_oper = tok.nextToken();     
		num_cheq   = tok.nextToken();
			if(num_cheq.length()==0) num_cheq = " ";	
		tran_id    = tok.nextToken();		
		monto      = tok.nextToken();
		tipTran    = tok.nextToken();
		saldo      = tok.nextToken();		
		rastrId    = tok.nextToken();
		descripcion = tok.nextToken();	
%>

<tr>
	<td align="center"><input type="radio" id="numMov" name="numMov" value="<%=i%>" onclick="if (this.checked) obtienedatos(<%=i%>)" ></td>
	<td align="center"><%=fecha_oper%></td>
	<td align="center"><%=num_cheq%></td>
	<td align="center"><%=descripcion%></td>
	<td align="center"> 
<%
			if(tipTran.equals("D"))	
			{	out.println(monto); } 
%>
	</td>
	<td align="center"> 
<%
			if(tipTran.equals("C"))	
			{	out.println(monto); } 
%>
	</td>
</tr>

<%
	   
	    
	    }
	} 
%>
</table>
<%
   }
%>

	<input type="hidden" name="movi" value="">

<p>

<br>
<center>
      <a href="javascript:cancelar(document.entry)"><img src="../imagenes/b_cancelar.gif" border="0"></a>
   
      <a href="javascript:validate(document.entry)"><img src="../imagenes/b_imprimir.gif" border="0"></a>
</center>

</form>
</body>
<script language="JavaScript">
top.formName = '';

function validate(form)
			{
		      if(!validates(form))	
		      {
		       alert("Para Continuar debe seleccionar un registro...");
		       return;
		      }
			
				//top.openDialog('../../servlet/ventanilla.ImprimirServlet', 160, 120, 'top.setPrefs4()', document.entry); 
				//return;
				
			   form.submit();  
			}
			
function obtienedatos(valor)
{
//	document.Txns.retiro.value = valor;
	document.entry.movi.value = valor;
//	alert(valor);
}

</script>
</html>
