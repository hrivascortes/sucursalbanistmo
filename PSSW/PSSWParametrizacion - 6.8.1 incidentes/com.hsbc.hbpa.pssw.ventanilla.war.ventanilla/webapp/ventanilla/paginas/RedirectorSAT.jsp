<!-- 
//*************************************************************************************************
//             Funcion: JSP que muestra la salida de SAT
//            Elemento: RedirectorSAT.jsp
//          Creado por: Rutilo Zarco Reyes
//      Modificado por: Juvenal R. Fernandez V.
//*************************************************************************************************
// CCN - 4360246 - 19/11/2004 - Se elimina display de log
//*************************************************************************************************
-->

<%@ page session="true" import="ventanilla.com.bital.util.*,java.util.*"%>
<%
response.setHeader("Cache-Control","no-store"); //HTTP 1.1
response.setHeader("Pragma","no-cache"); //HTTP 1.0
response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
%>
<%
Vector resp = (Vector)session.getAttribute("response");
String control = (String)resp.get(0);
Vector frap = (Vector)session.getAttribute("FRAP");
String txn = "";

if(frap.isEmpty())
{
	//String txns = request.getParameter("txns");
        Stack flujotxn = new java.util.Stack();
        if(session.getAttribute("page.flujotxn") != null)
              flujotxn = (Stack)session.getAttribute("page.flujotxn");
        String txns = (String)flujotxn.pop();
        long txtCheque = Long.parseLong(quitap((String)request.getParameter("txtCheque")));
        if (txtCheque == 0)
            txns = (String)flujotxn.pop();
        flujotxn.add(txns);
	try
	{
		NSTokenizer txnstmp = new NSTokenizer(txns, "*");
		while( txnstmp.hasMoreTokens() )
		{
			String token = txnstmp.nextToken();
			if( token == null )
				continue;
            frap.addElement(token);
		}
	}
    catch(Exception e)
    {
    }
}
	txn = frap.firstElement().toString();
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
  <title>Certificación</title>
<style type="text/css">
  Body {font-family: Arial, Helv; font-size: 9pt; font-weight: bold}
</style>

<script language="JavaScript">
<!--
  function redirect()
  {
   	document.forms[0].submit();
  }
//-->
</script>
</head>

<%if(txn.equals("5503"))
{%><body bgcolor="#FFFFFF" onload="top.setFieldFocus(window.document)"><%}
else{%><body bgcolor="#FFFFFF" onload="javascript:redirect();"><%}%>

<p>
<%
if(txn.equals("5503") && control.equals("0"))
{
	String proceso = "PageBuilderS503.jsp";
	response.sendRedirect(proceso);
}
if(txn.equals("5353") || txn.equals("S503"))
{
   out.print("<form name=\"Txns\" action=\"../../servlet/ventanilla.PageServlet\" method=\"post\">");
%>
  <input type="hidden" name="transaction" value="<%=txn%>">
  <input type="hidden" name="transaction1" value="<%=txn%>">
<%
   out.print("</form>");
}

// Agregar campos de jsp PageBuilderRAP a datasession
String[] values;
Enumeration params = request.getParameterNames();

Hashtable datasession = (Hashtable)session.getAttribute("page.datasession");
if(datasession == null)
	datasession = new Hashtable();
while( params.hasMoreElements() )
{
    String param = (String)params.nextElement();
    values = request.getParameterValues( param );
    for(int i=0; i<values.length; i++)
	    datasession.put(param, values[i]);
}

// Remove a variable de sesion para Flujo de Control RAP
// if(!(frap.isEmpty()))
if(frap.firstElement().toString().equals("5503"))
{
	datasession.remove("flujorap");
}
else
{// Remove a Primer elemento del Vector de Control de Flujo

	frap.removeElementAt(0);
}

session.setAttribute("page.datasession", datasession);

%>
<p>
</body>
</html>
<%!
	private String quitap(String Valor)
	{
		StringBuffer Cantidad = new StringBuffer(Valor);
		for(int i=0; i<Cantidad.length();)
			if( Cantidad.charAt(i) == ',' || Cantidad.charAt(i) == '.' )
				Cantidad.deleteCharAt(i);
			else
				++i;
		return Cantidad.toString();
	}
%>
