<!-- PageBuilderM021.jsp
//***************************************************************************************************
// CCN - 4360428 - 21/02/2006 - Se modifica la validacion para alta de usuarios de apoyo a plataforma
// CCN - 4360581 - 12/03/2007 - Se convierten los estilos en jsf y se hace un include en el jsp
//***************************************************************************************************
 -->
<%@ page session="true"%>
<%
response.setHeader("Cache-Control","no-store"); //HTTP 1.1
response.setHeader("Pragma","no-cache"); //HTTP 1.0
response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
  <title>Datos de la Transaccion</title>
  <%@include file="style.jsf"%>
<script language="JavaScript" type="text/javascript" src="validate.js"></script>
<script language="JavaScript" type="text/javascript">
<!--
var checkval = true
function valida(value)
{
  checkval = value
}

function validateForm(form)
{
  if( !checkval )
    return true
//MODIFICAIC�N PARA VALIDACI�N DE RACF 
  if( !top.validate(window, form.registro, 'isEmptyStr','isnumeric','isSerie') )
    return false
  return true
}
//-->
</script>
</head>
<body bgcolor="white" onload="top.setFieldFocus(window.document)">
<p><br>
<b>Datos del Usuario de Apoyo</b>
<p><br>
<form action="../../servlet/ventanilla.Group03GTE" method="post" onsubmit="return validateForm(this)">
<table border="0" cellspacing="0" width="400">
<tr>
  <td><b>Registro:</b></td>
  <td><input type="text" name="registro" size="8" maxlength="8" onchange="fillRecord(this)"></td>
</tr>
<tr>
  <td><b>Per�odo (d�as naturales):</b></td>
  <td>
    <select name="periodo" size="1">
	<option value="1" selected>1</option>
	<option value="2">2</option>
	<option value="3">3</option>
	<option value="4">4</option>
	<option value="5">5</option>
	<option value="6">6</option>
	<option value="7">7</option>
	<option value="8">8</option>
	<option value="9">9</option>
	<option value="10">10</option>
	<option value="11">11</option>
	<option value="12">12</option>
	<option value="13">13</option>
	<option value="14">14</option>
	<option value="15">15</option>
	</select>
  </td>
</tr>
<tr>
  <td colspan="2">&nbsp;</td>
</tr>
<tr>
  <td><input type="image" name="[Cancelar]" src="../imagenes/b_cancelar.gif" border="0" onclick="valida(false)"></td>
  <td align="right"><input type="image" name="[Aceptar]" src="../imagenes/b_aceptar.gif" border="0" onclick="valida(true)"></td>
</tr>
</table>
<input type="hidden" name="inicial" value="yes">
</form>
</body>
</html>
