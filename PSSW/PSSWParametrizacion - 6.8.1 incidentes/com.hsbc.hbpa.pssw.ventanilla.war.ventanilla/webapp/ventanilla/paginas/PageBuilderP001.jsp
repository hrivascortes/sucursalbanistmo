<!--
//*************************************************************************************************
//             Funcion: JSP para despliege de txns varias
//            Elemento: PageBuilderP001.jsp
//      Modificado por: Jesus Emmanuel Lopez Rosales
//*************************************************************************************************
// CCN - 4360275 - 18/02/2005 - WAS 5.1
// CCN - 4360334 - 17/06/2005 - Se convierte fieldLector a fragmento
// CCN - 4360390 - 07/10/2005 - Se agrega validacion para acdo 1053.
// CCN - 4360443 - 24/02/2006 - Se realizan modificaciones para pedir autorizaci�n para el depto 9010
// CCN - 4360477 - 19/05/2006 - Se agrega validaci�n para txn 5405 en tipo de identifiaci�n cuando es n�merico
// CCN - 4360533 - 20/10/2006 - Se realizan modificaciones para OPEE
// CCN - 4360572 - 23/02/2007 - Se realizan modificacion en validaci�n de monto de txn 5357 y bandera de autoriz remota en txn 0041
// CCN - 4360581 - 12/03/2007 - Se convierten los estilos en jsf y se hace un include en el jsp
// CCN - 4360584 - 23/03/2007 - Modificaciones para proyecto OPMN
// CCN - 4360619 - 15/06/2007 - Se eliminan systems
// CCN - 4620018 - 12/10/2007 - Se valida monto 
// CCN - 4620021 - 17/10/2007 - Se agregan validaciones para montos mayores a 10000 dlls, Humberto Balleza
//*************************************************************************************************
-->
<%@ page session="true" import="java.util.*,
	ventanilla.GenericClasses"%>

<%
response.setHeader("Cache-Control","no-store"); //HTTP 1.1
response.setHeader("Pragma","no-cache"); //HTTP 1.0
response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<%
String MontoPro = new String("");
Vector listaCampos = (Vector)session.getAttribute("page.listcampos");
Vector listaContenidos = (Vector)session.getAttribute("page.listcont");
String txnAuthLevel = (String)session.getAttribute("page.txnAuthLevel");
String txnIMGAutoriz = (String)session.getAttribute("page.txnIMGAutoriz");
Vector v = (Vector)session.getAttribute("page.specialacct");
Hashtable datasession = (Hashtable)session.getAttribute("page.datasession");
String txn = session.getAttribute("page.cTxn").toString();
String creditoIni=(String)session.getAttribute("creditoIni");
ventanilla.GenericClasses gc = new ventanilla.GenericClasses();
if (datasession == null)
   datasession = new Hashtable();
int k;
int i;
//String edoCtaKronner = "000153049620071130000099000000000000001JUAN ROBERTO *TORRES ESPINOSA           0001500000000000000020071102024cmtH BANK (PANAMA) S20110202102008020402USDDOLAR AMERICANO  1305030000200712030001452125487500000000                 1000114160000000000000000000150000000000000000000000150000000000000000000000000000000000000000000000000000000000000000000125000000000000000000000008319450000000000000000000000000000000000000000000000000000133319450000000000000000000000000000000000000000000000000001508319450000000000000000000000000000TRADICIONAL         00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000010000000000000                    00000000000000000000000000000000VIGENTE   SUCURSAL DEFAULT                        * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *                                                             * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 00                                                                                                    00000000000410000000000000000000000000000000000________________________________________________________________________________________________________________________0000000           1201102020000000000000000000000000000000000000000000000000001400000000000000000000000000000000000000013331945000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000012500000000000000000000000000000000000000000000000000000000000000000000000000000000000000000200711150000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000";
String cta =(String)session.getAttribute("ctaRepago");
StringBuffer edoCtaKronner = new StringBuffer((String)session.getAttribute("respuestaKronner"));
//validaciones del nombre del Cliente
StringBuffer cliente= new StringBuffer(edoCtaKronner.substring(38,78).trim()); 
edoCtaKronner.substring(38,78).trim();	
for (int y=0; y<cliente.length(); y++){
			char c = cliente.charAt(y);
			int flag =0;						
			if((c < 'A' && c <'Z') || c=='_'){
							
				if(c==' '){
					c=' ';
					cliente.insert(y,c);
					flag=1;
				}
				cliente.deleteCharAt(y);
				if(flag!=1){
					c=' ';
					cliente.insert(y,c);
				
				}
						
			}    
	}

session.setAttribute("cliente",cliente);
String numCredito=edoCtaKronner.substring(2,10).trim();
session.setAttribute("creditoKronner",numCredito);
String montoLetra =edoCtaKronner.substring(1571,1582).trim();	
montoLetra=gc.FormatAmount(montoLetra);
String SaldoExig=edoCtaKronner.substring(730,741).trim();
SaldoExig=gc.FormatAmount(SaldoExig);
//session.setAttribute("montoCtoKroner",montoLetra);
session.setAttribute("page.iTxn","P001");
String blag = (String)session.getAttribute("blagP001");

%>
<html>
<head>
  <title>Pagos Voluntarios</title>
 <%@include file="style.jsf"%>
<script language="javascript">
function cancelar()
{
  var form = document.forms[0];
  form.action = "Final.jsp";
  form.submit();
}

function actualizaValor(form)
{
			if(form.ckLetra[0].checked)
				valor = form.ckLetra[0].value;
			else if(form.ckLetra[1].checked)
				valor = form.ckLetra[1].value;
			else 
				valor = "0";
			document.entry.rdoTipoPago.value=valor;
}

function validate(acp,money,form)
{
	var montoCaja=document.entry.txtImporte.value;	
	/*if(money!=montoCaja){
		var monto= montoCaja/100;
		
	}else{
		var monto= montoCaja;
	}*/
	document.entry.hdMtoLetra.value=montoCaja;
	if(montoCaja==''){
		alert("Ingresa el monto a Pagar");
		return;
	}
	if(montoCaja == "0.00"){
		alert("Monto Inv�lido");
		return;
	}
	var seguir = confirm("�Est� seguro de realizar la operaci�n de pago al cr�dito No."+acp+" por un importe de $"+montoCaja);	
	
	if(!seguir){	
			return;
		}
		else{
			actualizaValor(form)
			form.submit();
			}
}
</script>
</head>
<body bgcolor="#FFFFFF" onload="top.setFieldFocus(window.document)">
<br>
<form name="entry" action="../../servlet/ventanilla.PageServlet" method="post">
<p>
<h1>Pago de Pr�stamos</h1>
<h3>
	P001  Pagos Voluntarios
</h3>
<table border="0" cellspacing="0">
		<tr>
			<td>Cliente: </td>
			<td><%=cliente%></td>
			<td width="10">&nbsp;</td>
		<tr>
			<td>Cuenta:</td>
			<td><%=cta%></td>
			<td width="10">&nbsp;</td>
		<tr>
			<td>N�mero de prestamo: </td>
			<td><%=numCredito%></td>
			<td width="10">&nbsp;</td>
		<tr>
			<td>N�mero de Cr�dito Anterior:</td>
			<td><%=creditoIni%></td>
			<td width="10">&nbsp;</td>
		<tr>
			<td>Total Letra: </td>
			<td><%=montoLetra%></td>
			<td width="10">&nbsp;</td>
		<tr>
			<td>Monto Exigible :</td>
			<td><%=SaldoExig%></td>
			<td width="10">&nbsp;</td>
		<tr>
			<td>Importe a Pagar:</td> 
			<td><input name="txtImporte" type="text" value="<%=montoLetra%>" onChange="top.validate(window, this, 'isCurrency')"></td>
			  
			<td><input type="hidden" name="hdMontoJSP" value="<%=montoLetra%>"></td>
			<td width="10">&nbsp;</td>
		</tr>
		<tr>			
			<td colspan='2'>
			<input  name="ckLetra" type="Radio"  value="4"> Pago Directo a Capital&nbsp;&nbsp;
            <input name="ckLetra" type="Radio"  value="3" > Pago Siguientes Amortizaciones PP/Autos/Hipo<br></td>
			<td width="10">&nbsp;</td>
		</tr>
		<tr>
			<td>
			<br>
				<a id='btnCont' href="javascript:document.entry.returnform.value='S';validate('<%=numCredito%>','<%=montoLetra%>',document.entry)"><img src="../imagenes/b_continuar.gif" border="0"></a></td>
				
			<td>
			<br>
				<a id='btnCanc' href="javascript:cancelar()"><img src="../imagenes/b_cancelar.gif" border="0"></a></td>
		</tr>
</table>
	<input type="hidden" name="iTxn" value="<%= session.getAttribute("page.iTxn") %>">
	<input type="hidden" name="oTxn" value="<%= session.getAttribute("page.oTxn") %>">
	<input type="hidden" name="cTxn" value="<%= session.getAttribute("page.cTxn") %>">
	<input type="hidden" name="sucursal" value="<%= session.getAttribute("branch") %>">
	<input type="hidden" name="teller" value="<%= session.getAttribute("teller") %>">
	<input type="hidden" name="moneda" value="<%= session.getAttribute("page.moneda")%>">
	<input type="hidden" name="registro" value="<%=session.getAttribute("empno")%>">
	<input type="hidden" name="tllrlvl" value="<%=session.getAttribute("tellerlevel")%>">
	<input type="hidden" name="supervisor" value="<%= session.getAttribute("teller") %>">
	<input type="hidden" name="AuthoOK" value="0">
	<input type="hidden" name="SignOK" value="0">
	<input type="hidden" name="validateFLD" value="0">
	<input type="hidden" name="needSignature" value="<%out.print(txnIMGAutoriz);%>">
	<input type="hidden" name="needAutho" value="<%out.print(txnAuthLevel);%>">
	<input type="hidden" name="SignField" value="<%= session.getAttribute("page.SignField") %>">
	<input type="hidden" name="override" value="N">
	<input type="hidden" name="txtFees" value="0">
	<input type="hidden" name="blagP001" value="0">
	<input type="hidden" name="compania" value="20">
	<input type="hidden" name="txtSelectedItem" value="0" >
	<input type="hidden" name="returnform" value="N" >
	<input type="hidden" name="returnform2" value="N" >
	<input type="hidden" name="txtPlazaSuc" value="<%= session.getAttribute("plaza") %>" >
	<input type="hidden" name="passw" value="<%=session.getAttribute("password")%>">
	<input type="hidden" name="MontoRetiroPesos" value = "<%=session.getAttribute("Retiro.Pesos")%>">
	<input type="hidden" name="MontoRetiroDolar" value = "<%=session.getAttribute("Retiro.Dolar")%>">
	<input type="hidden" name="hdMtoLetra" value = "">
	<input type="hidden" name="rdoTipoPago" value ="">
</form>
</body>
</html>
