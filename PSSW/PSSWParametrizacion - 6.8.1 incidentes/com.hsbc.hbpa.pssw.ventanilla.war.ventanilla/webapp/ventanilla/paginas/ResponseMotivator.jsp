<!--
//*************************************************************************************************
//             Funcion: JSP que despliega las TXN'S de motivator realizadas por el ejecutivo. 
//            Elemento: ResponseMotivator.jsp
//          Creado por: Israel De Paz Mercado
//*************************************************************************************************
// CCN - 4360314 - 06/05/2005 - Se crea jsp para desplegar las Txns realizadas por el ejecutivo para Motivator.
// CCN - 4360581 - 12/03/2007 - Se convierten los estilos en jsf y se hace un include en el jsp
//*************************************************************************************************
-->

<%@ page session="true" import="java.util.*,ventanilla.com.bital.beans.DatosVarios" errorPage="/ventanilla/paginas/error.jsp"%>
<%
//response.setHeader("Cache-Control","no-store"); //HTTP 1.1
response.setHeader("Pragma","no-cache"); //HTTP 1.0
response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
%>
<html>
<head>
<title>Motivator</title>
<%@include file="style.jsf"%>
</head>

<body bgcolor='#FFFFFF' class="p">


<%
    
    int SelectMotiv = Integer.parseInt((String)session.getAttribute("FlagSelectMot"));
    session.removeAttribute("page.getoTxnView");
    session.removeAttribute("txnProcess");
    session.removeAttribute("FlagSelectMot");
    
      
if (SelectMotiv == 0)
   {
   
       DatosVarios mot = DatosVarios.getInstance();                   
       Vector vecMOT = new Vector();        
       vecMOT =(Vector)mot.getDatosV("MOTIVATOR");                           
                      
       
       
       int lon = vecMOT.size();
       Vector vecTXN = new Vector(); 
       
       
    %>
    <b>Catalogo de Transacciones de Motivator</b><br><br>
    <table border="1" align="center" cellspacing="0" cellpadding="5">
      <tr class="p"><td colspan="2"  align="center" ><b>C&nbsp;A&nbsp;T&nbsp;A&nbsp;L&nbsp;O&nbsp;G&nbsp;O&nbsp;&nbsp;&nbsp;M&nbsp;O&nbsp;T&nbsp;I&nbsp;V&nbsp;A&nbsp;T&nbsp;O&nbsp;R</b></td></tr>
      <tr>
      <tr class="p"><td align="center" >Categor&iacute;a</td>
          <td align="center">Transacciones</td>
      </tr>
      <%for(int i=0;i<lon-1;i++)
          {
           String llave = (String)vecMOT.get(i);
           i=i+1;
           vecTXN = (Vector)vecMOT.get(i);
           String listaTXN="";%>
       <tr class="p">
          <td align='center'><%out.print(llave);%></td>
          <td>
              <table border="1" align="left" cellspacing="0" cellpadding="5">
                <tr class="p">
                   <%
                   int contador=0;
                   for(int x=0;x<vecTXN.size();x++)
                     {
                        listaTXN = (String)vecTXN.get(x);
                      	if (contador == 10)
                          {%>
                     		</tr><tr class="p"><td align='left'><%out.print(listaTXN);%></td>     
                          <%contador=0;
                          }
                          else
                          {%>     
                     		<td align='left'><%out.print(listaTXN);%></td>
                    <%    
	                    }
	                 contador = contador+1;   
	               }%>                                        
               </tr>
              </table>    
           </td>
        </tr><%
         }   
        %></table><%
   
   
   
   
   }
else
{   
    Vector VecMotiv = (Vector)session.getAttribute("VecMotivator");
    int FlagMotiv = ((Integer)session.getAttribute("FlagMotivator")).intValue();
    String registro = (String)session.getAttribute("empno");

		int sz = VecMotiv.size();
		
		if (FlagMotiv == 1)
		   {    out.println("<Table border='0'>");
		        out.println("<tr class=\"p\"><td><b>Transacci�n Rechazada</b></td></tr><tr></tr>");
			for(int i=0; i<sz; i++)
		   	{
		   	out.println("<tr></tr><tr><td><b>"+VecMotiv.get(i)+"</b></td></tr>");
		   	}
		   	out.println("</Table>"); 
		   }
		   else if(FlagMotiv == 0)
		           {    
		  	        int SzVecMotiv = VecMotiv.size();
                                String line = (String)VecMotiv.get(5);
				out.println("<b>Transacci�n Realizadas por el Ejecutivo "+registro+"</b><br><br>");
				out.println("<Table border='0' align='center' cellspacing='0' cellpadding='3' class=\"p\">");
		                out.println("<tr class=\"p\">");
				out.println("<td align='center' bgcolor='#CCCCCC'><b>A</b></td>");
				out.println("<td align='center' bgcolor='#CCCCCC'><b>B</b></td>");
				out.println("<td align='center' bgcolor='#CCCCCC'><b>C</b></td>");
				out.println("<td align='center' bgcolor='#CCCCCC'><b>D</b></td>");
				out.println("<td align='center' bgcolor='#CCCCCC'><b>E</b></td>");
				out.println("<td align='center' bgcolor='#CCCCCC'><b>F</b></td>");
				out.println("<td align='center' bgcolor='#CCCCCC'><b>G</b></td>");
				out.println("<td align='center' bgcolor='#CCCCCC'><b>H</b></td>");    
				out.println("<td align='center' bgcolor='#CCCCCC'><b>I</b></td>");
				out.println("<td align='center' bgcolor='#CCCCCC'><b>J</b></td>");
				out.println("<td align='center' bgcolor='#CCCCCC'><b>Sucursal</b></td>"); 
				out.println("</tr>");   
				
				   for (int y=5; y<SzVecMotiv; y++)
				    {   //despliega el numero de TXN�S por categoria
				    
				       
				        line = (String)VecMotiv.get(y);
				        line = line.trim();
				        %>
				        <tr class="p">                      
				        <%
				         //out.println("<tr class=\"p\">");                      
				        
				        int lon=line.length();
				        int r=0;
				        
				        if(lon<50)
				           {
				           r=0;
				           }
				           else
				           {
				           r=1;
				           }
				           
				        for(int x=r;x<line.length();x++)   
				    	{
				    	out.println("<td align='center'>"+line.substring(x,x+4)+"</td>");
				    	x = x+4;
				    	}    
				    	//despliega datos de la fecha,horario,hora entrada, hora salida,sucursal
				    	y =y+1;
				    	line = (String)VecMotiv.get(y);
				    	line = line.trim();
				    	
				    	out.println("<td align='center' colspan='2'>"+line.substring(15,20)+"</td>");
				    	out.println("</tr>");                       
				    	out.println("<tr class=\"p\">");                       
				    	out.println("<td align='center' colspan='2' ><b>Hor:&nbsp;</b>"+line.substring(6,7)+"</td>");
				    	out.println("<td align='center'></td>");
				    	out.println("<td align='center' colspan='2'><b>Entrada:&nbsp;</b>"+line.substring(7,9)+":"+line.substring(9,11)+"</td>");
				    	out.println("<td align='center'></td>");
				    	out.println("<td align='center' colspan='2'><b>Salida:&nbsp;</b>"+line.substring(11,13)+":"+line.substring(13,15)+"</td>");
				    	out.println("<td align='center'></td>");
				    	out.println("<td align='center' colspan='2'><b>Fecha:</b>"+line.substring(4,6)+"/"+line.substring(2,4)+"/20"+line.substring(0,2)+"</td>");
					out.println("</tr>");                      
					out.println("<tr>");
					out.println("<td align='center'>&nbsp;</td>");                       
				    	out.println("</tr>");  
				    	 
				    }	
				
				
				
				
				
				out.println("</Table>");
		           }  
       

                
             
           
  
}                
                
             
                

          
                
%>                            
</body>                       
</html>                       
                              
                              
