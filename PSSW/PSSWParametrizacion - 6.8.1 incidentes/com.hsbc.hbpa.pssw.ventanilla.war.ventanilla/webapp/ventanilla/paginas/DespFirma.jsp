<!-- DespFirma.jsp
//*************************************************************************************************
//             Funcion: JSP que presenta las firmas 
//          Creado por: Alejandro Gonzalez Castro
// Ultima Modificacion: 20/07/2004
//      Modificado por: Alejandro Gonzalez Castro
//*************************************************************************************************
// CCN -4360170- 20/07/2004 - Se carga firmas con applet
// CCN -4360180- 20/08/2004 - Se graban y actualizan firmas de forma local
// CCN - 4360455 - 07/04/2006 - Se realizan modifiaciones para esquema de cheques devueltos.
// CCN - 4360462 - 10/04/2005 - Se genera nuevo paquete por errores en paquete anterior
//*************************************************************************************************
-->
<%@ page session="true"%>
<%
response.setHeader("Cache-Control","no-cache"); //HTTP 1.1
response.setHeader("Pragma","no-cache"); //HTTP 1.0
response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
%>
<%!
  private String stringFormat(int option, int sum)
  {
    java.util.Calendar now = java.util.Calendar.getInstance();
    String temp = new Integer(now.get(option) + sum).toString();
    if( temp.length() != 2 )
      temp = "0" + temp;

    return temp;
  }
%>
<HTML>
	<HEAD>
		<TITLE>Visualización de FIRMAS</TITLE>
		
		<style type="text/css">
		@media print{body {display:none;}}
	    </style>	
		<SCRIPT language="JavaScript">
			var activaBoton = 0
			
			function activar()
			{
				activaBoton = 1
			}

			function IERedirectorChqDev()
			{
				if (activaBoton != 1) 
					return;			
				document.forms['signature'].action = "PageBuilderChqDev.jsp";
				document.forms['signature'].target = "panel";
				document.forms['signature'].submit();
				window.close();
			}
			function IEhandleOK(form)
			{
				if (activaBoton != 1) 
					return;
				
				form.SignOK.value = '1';
				window.returnValue = 'button=2;' + getFormData(document.signature);
				window.close();
			}
			function IEhandleCancel()
			{
				if (activaBoton != 1) 
					return;
				
				window.returnValue = 'button=0';
				window.close();
			}

			function getFormData(form)
			{
				var searchString = "";
				var hiddensElements;
				
				hiddensElements = form.getElementsByTagName("INPUT");
				for (var i = 0; i < hiddensElements.length; i++)
				{
					if(hiddensElements[i].type == "hidden")
						searchString += escape(hiddensElements[i].name) + "=" + escape(hiddensElements[i].value) + ";";
				}
				searchString = searchString.substring(0,searchString.length - 1);
				
				return searchString;
			}

		</SCRIPT>
		<SCRIPT language="javascript" for="FirmasControl" event="Activar">

			activar();

		</SCRIPT>

	</HEAD>
	<BODY bgcolor="#FFFFFF" onLoad="if (top.opener) top.opener.blockEvents()" onUnload="if (top.opener) top.opener.unblockEvents();" TOPMARGIN="0" LEFTMARGIN="0" MARGINWIDTH="0" MARGINHEIGHT="0">
<FORM name="signature">
 <input type="hidden" name="SignOK" value="1">
 <table width="100%" border="0">
	<tr valign="center">
		<td align="center">
<APPLET archive='despfirmas.jar' code='CFirmas' codebase='../' name='firmas' width='710' height='300'>
					<PARAM name="Cuenta" value="<%=session.getAttribute("Cuenta")%>">
					<PARAM name="CuentaEsp" value="<%=session.getAttribute("CuentaEspecial")%>">
					<PARAM name="OffLine" value="0">
					<PARAM name="RemoteIP" value="<%=request.getServerName()%>">
					<PARAM name="RemotePort" value="<%=request.getServerPort()%>">
					<PARAM name="Ruta" value="<%=request.getContextPath()%>">
					<PARAM NAME="mayscript" VALUE="true">
<%
	String date = new String(stringFormat(java.util.Calendar.YEAR, 0) + stringFormat(java.util.Calendar.MONTH, 1) + stringFormat(java.util.Calendar.DAY_OF_MONTH, 0));
	out.print(" <PARAM NAME=\"Fecha\" value=\"" + date + "\">");
%>

</APPLET>
		</td>
	</tr>
 </table>
<DIV STYLE="text-align:center">
<%
	if(session.getAttribute("CuentaEspecial") != null)
    session.removeAttribute("CuentaEspecial");
	out.print("<BUTTON style=\"width:69px; heigth:27px; border:0px; background-color:white\"  onclick=\"javascript:IEhandleCancel()\"><img src=\"../imagenes/b_cancelar.gif\" border=\"0\"></BUTTON>");  		
	if(session.getAttribute("page.datasession") != null)
	{	
		java.util.Hashtable ht = (java.util.Hashtable) session.getAttribute("page.datasession");
		if (ht.get("chqDev") != null) {
			String strchqDev = ht.get("chqDev").toString();
			if (strchqDev.equals("SI"))
				out.print("<BUTTON style=\"width:69px; heigth:27px; border:0px; background-color:white\"  onclick=\"javascript:IERedirectorChqDev()\"><img src=\"../imagenes/b_coincide.gif\" border=\"0\"></BUTTON>");  				
		}
	}
	out.print("<BUTTON style=\"width:69px; heigth:27px; border:0px; background-color:white\"  onclick=\"javascript:IEhandleOK(document.signature)\"><img src=\"../imagenes/b_aceptar.gif\" border=\"0\"></BUTTON>");
%>
</DIV>
</FORM>
</BODY>
</HTML>
