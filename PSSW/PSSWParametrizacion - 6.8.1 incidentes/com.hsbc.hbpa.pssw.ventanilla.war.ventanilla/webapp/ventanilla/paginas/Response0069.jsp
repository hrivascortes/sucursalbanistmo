<!--
//*************************************************************************************************
//             Funcion: JSP que presenta las opciones de la txn 0069
//            Elemento: Response0069.jsp
//          Creado por: Alejandro Gonzalez Castro
//      Modificado por: Juan Carlos Gaona
//*************************************************************************************************
// CCN -4360163- 06/07/2004 - Se controla click sobre el boton aceptar
// CCN - 4360175 - 03/08/2004 - Se ajusta size de ventana de impresion
// CCN - 4360275 - 18/02/2005 - WAS 5.1
//*************************************************************************************************
-->

<%@ page session="true" import="java.util.*,java.net.URLEncoder"%>
<%
response.setHeader("Cache-Control","no-store"); //HTTP 1.1
response.setHeader("Pragma","no-cache"); //HTTP 1.0
response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<%!
   private String replacespace(String campo)
   {
      String temp = "";
      for (int i=0;  i<campo.length(); i++)
         if(campo.charAt(i) != ' ')
            temp = temp + campo.charAt(i);
         else
            temp = temp + 0x20;
      return temp;
   }

   private String replaceCats(String campo)
   {
      String temp = "";
      for (int i=0;  i<campo.length(); i++)
         if(campo.charAt(i) != '#')
            temp = temp + campo.charAt(i);
         else
            temp = temp + "%23";
      return temp;
   }
        
	private String addpunto(String valor)
	{
      int longini = valor.length();
      if (longini == 1)
      {
         valor = "0.0" + valor; 
         return valor;
      }
      String cents  = valor.substring(longini-2,longini);
	   String entero = valor.substring(0,longini-2);
	   int longente = entero.length();
	   for (int i = 0; i < (longente-(1+i))/3; i++)
	   {
         longente = entero.length();
         entero = entero.substring(0,longente-(4*i+3))+','+entero.substring(longente-(4*i+3));
      }
      entero = entero + '.' + cents;
      return entero;
   }

   private String getdomi(String r)
   {
      String domicilio = r.substring(77,108);
		return domicilio;
   }
   
  private String getString(String s)
  {
    int len = s.length();
    for(; len>0; len--)
	  if(s.charAt(len-1) != ' ')
	    break;

    return s.substring(0, len);
  }

   private String getItem(String newCadNum, int newOption)
   {
      int iPIndex = newCadNum.indexOf("CONSECUTIVO:") + 12, NumSaltos, Num = 0, Ban = 0;
      String nCadNum = new String("");
      NumSaltos = newOption;
      while(Num < NumSaltos)
      {
         if(newCadNum.charAt(iPIndex) == 0x20)
         {
            while(newCadNum.charAt(iPIndex) == 0x20)
            iPIndex++;
            Num++;
         }
         else
            while(newCadNum.charAt(iPIndex) != 0x20)
         iPIndex++;
      }
      try
      {
         while(newCadNum.charAt(iPIndex) != 0x20)
         nCadNum = nCadNum + newCadNum.charAt(iPIndex++);
      }
      catch(StringIndexOutOfBoundsException e)
      {
         //System.out.println("fin de cadena: " + e);
      }
      return nCadNum;
   }

  private String delLeadingfZeroes(String newString){
   StringBuffer newsb = new StringBuffer(newString);
   int Len = newsb.length();
   for(int i = 0; i < Len; i++)
    if(newsb.charAt(i) != '0')
     break;
    else
     newsb.deleteCharAt(i);

   return newsb.toString();
  }

  private String setPointToString(String newCadNum)
  {
   int iPIndex = newCadNum.indexOf("."), iLong;
   String szTemp;

   if(iPIndex > 0)
   {
    newCadNum = new String(newCadNum + "00");
    newCadNum = newCadNum.substring(0, iPIndex + 1) + newCadNum.substring(iPIndex + 1, iPIndex + 3);
   }
   else
   {
    for(int i = newCadNum.length(); i < 3; i++)
     newCadNum = "0" + newCadNum;
    iLong = newCadNum.length();
    if(iLong == 3)
     szTemp = newCadNum.substring(0, 1);
    else
     szTemp = newCadNum.substring(0, iLong - 2);
    newCadNum = szTemp + "." + newCadNum.substring(iLong - 2);
   }

   return newCadNum;
  }

  private String delPointOfString(String newCadNum)
  {
   int iPIndex = newCadNum.indexOf(".");

   if(iPIndex > 0)
   {
    newCadNum = newCadNum.substring(0, iPIndex) +
                newCadNum.substring(iPIndex + 1, newCadNum.length());
   }

   return newCadNum;
  }

  private String delCommaPointFromString(String newCadNum)
  {
   String nCad = new String(newCadNum);
   if( nCad.indexOf(".") > -1)
   {
    nCad = nCad.substring(nCad.indexOf(".") + 1, nCad.length());
    if(nCad.length() != 2)
    {
     String szTemp = new String("");
     for(int j = nCad.length(); j < 2; j++)
      szTemp = szTemp + "0";
     newCadNum = newCadNum + szTemp;
    }
   }

   StringBuffer nCadNum = new StringBuffer(newCadNum);
   for(int i = 0; i < nCadNum.length(); i++)
    if(nCadNum.charAt(i) == ',' || nCadNum.charAt(i) == '.')
     nCadNum.deleteCharAt(i);

   return nCadNum.toString();
  }

  private String setFormat(String newCadNum)
  {
    int iPIndex = newCadNum.indexOf(".");
    String nCadNum = new String(newCadNum.substring(0, iPIndex));
    for (int i = 0; i < (int)((nCadNum.length()-(1+i))/3); i++)
     nCadNum = nCadNum.substring(0, nCadNum.length()-(4*i+3))+','+nCadNum.substring(nCadNum.length()-(4*i+3));

    return nCadNum + newCadNum.substring(iPIndex, newCadNum.length());
  }

  private String longtoString(long newlong){
    String newString = String.valueOf(newlong);
    return newString.toString();
  }

%>
<%
  Vector resp = (Vector)session.getAttribute("response");
  Hashtable datasession = (Hashtable)session.getAttribute("page.datasession");
  String needOverride = "NO";
%>
<html>
<head>
  <title>Certificación</title>
<style type="text/css">Body {font-family: Arial, Helv; font-size: 10pt; font-weight: bold; color:#000000}</style>
</head>
<script language="JavaScript">
function aceptar(forma)
{
	forma.submit();
	var obj = document.getElementById('cont')
	obj.onclick = new Function('return false')
	
}
function changeaction()
{
    document.Txns.action = "../../servlet/ventanilla.PageServlet";
}
</script>
<body>
<form name="Txns" action="../../servlet/ventanilla.DataServlet" method="post">
<p>
<%
   String cTxn = (String)datasession.get("cTxn");
   String newtxn = "";
   String szResp = new String("");
   String Nombre = "";
   String Curp = "";
   String codigo = "1";
   Stack flujotxn = (Stack)session.getAttribute("page.flujotxn");
   if( resp.size() != 4 )
    out.println("<b>Error de conexi&oacute;n</b><br>");
   else
   {
     try
   	{
     	codigo = (String)resp.elementAt(0);
 	if( codigo.equals("1"))
   	   out.println("<b>Transaccion Rechazada</b><br><br>");

        if( codigo.equals("0") || codigo.equals("1") ) {
     	   int lines = Integer.parseInt((String)resp.elementAt(1));
    	       szResp = (String)resp.elementAt(3);
    	   for(int i=0; i<lines; i++) {
    	       String line = szResp.substring( i*78, Math.min(i*78+77, szResp.length()) );
               if(i == 3)
                  Nombre = line.trim();
               if(i == 4)
                  Curp = line.trim().substring(0,18);
     	       out.println(getString(line) + "<p>");
     	       if(Math.min(i*78+77, szResp.length()) == szResp.length())
      		    break;
    	   }
       }
       else {
         if ( codigo.equals("3") || codigo.equals("2"))
            out.println("<b>Transacci&oacute;n Requiere Autorizaci&oacute;n</b>");
         else
            out.println("<b>Ocurri&oacute; un Error de Comunicaci&oacute;n, Verifique.</b>");
       }      
    }
    catch(NumberFormatException nfe){
      out.println("<b>Error de conexi&oacute;n</b><br>");
    }
  }

   if (codigo.equals("0"))
   {
       
        String ftime = (String)session.getAttribute("ftime");
        if (ftime.equals("si"))
        {
            Stack flujonew = new Stack();
            Vector alltxns = new Vector();
            long efectivo = Long.parseLong(delCommaPointFromString((String)datasession.get("txtEfectivo")));
            long monto = Long.parseLong(delCommaPointFromString((String)datasession.get("txtMonto")));
            long chqmtoB = Long.parseLong(delCommaPointFromString((String)datasession.get("txtChqsBital")));
            long chqmtoO = Long.parseLong(delCommaPointFromString((String)datasession.get("txtChqsOtros")));
            long doctos = chqmtoB + chqmtoO;
            session.setAttribute("doctos", addpunto(Long.toString(doctos)));
            long total = efectivo + doctos;
            session.setAttribute("total", addpunto(Long.toString(total)));
            if (efectivo == 0 || efectivo < monto)
            {
                boolean flag = false;
                if (chqmtoO > 0)
                {
                    long montotal = monto - efectivo;
                    long docs = montotal - chqmtoB;
                    datasession.put("txtChequeCI", addpunto(Long.toString(docs)));
                    if(efectivo == 0)
                        alltxns.add("1009");
                    else
                    {
                        alltxns.add("SS03");
                        alltxns.add("1009");
                        flag = true;
                    }
%>                  <Script language='JavaScript'>
                      changeaction();
                    </SCRIPT>
<%              }
                if (chqmtoB > 0)
                {
                    long montotal = monto - efectivo;
                    long docs = montotal - chqmtoO;
                    datasession.put("txtCheque", addpunto(Long.toString(docs)));
                    if(flag == false)
                    {
                        if(chqmtoO > 0)
                        {   
                            alltxns.removeElement("1009");
                            alltxns.add("SS03");
                            alltxns.add("1009");
                        }
                        else
                            alltxns.add("SS03");
                    }
                    alltxns.add("5353");
                    %>
                    <Script language='JavaScript'>
                      changeaction();
                    </SCRIPT>
<%              }
                
                for (int i = 0; i <alltxns.size(); i++)
                    { flujonew.push(alltxns.get(i));}
                flujotxn = flujonew;
                session.setAttribute("page.flujotxn",flujotxn);
            }
            if (efectivo == monto)
            {
                newtxn = (String)flujotxn.pop();
               // System.out.println("newtxn ["+newtxn+"]");
                datasession.put("cTxn",newtxn);
                session.setAttribute("page.cTxn", (String)datasession.get("cTxn"));
            }
            session.setAttribute("ftime","no");
        }
        else
        {
            if (!flujotxn.empty())
            {
                newtxn = flujotxn.pop().toString();
                datasession.put("cTxn",newtxn);
                session.setAttribute("page.cTxn", (String)datasession.get("cTxn"));
            }
        }
        if (!datasession.get("cTxn").toString().equals("SS03"))
        {
                if (flujotxn.empty())
                     flujotxn.push("1111");
                if(newtxn.equals("1111"))
                    flujotxn.pop();
         }
%>
<%//Certificacion Inicio
    if (!cTxn.equals("0857"))
    {
        session.setAttribute("page.cTxn",cTxn);
        String isCertificable = (String)session.getAttribute("page.certifField");  // Para Certificación
        String txtCadImpresion  = ""; // Para Certificación
        if(!isCertificable.equals("N")) // Para Certificación
        {
            session.setAttribute("Consecutivo", getItem((String)resp.elementAt(3), 1)); // Para Certificación
    	    session.setAttribute("Tipo", isCertificable); // Para Certificación
    	    if(codigo.equals("0"))
      	       out.println("<script language=\"javascript\">top.openDialog('../../servlet/ventanilla.ImprimirServlet?txtCadImpresion=" + txtCadImpresion + "', 160, 120, 'top.setPrefs4()', document.Txns)</script>"); // Para Certificación
    	    else {
               if ( codigo.equals("3") || codigo.equals("2")) {
                   needOverride = "SI";
                   out.print("<script languaje=\"javascript\"> top.openDialog('authoriz.jsp', 220, 130, 'top.setPrefs8()', document.Txns) </script>");
               }
               else
          	    out.println("<a href=\"javascript:top.openDialog('../../servlet/ventanilla.ImprimirServlet?txtCadImpresion=" + txtCadImpresion + "', 160, 120, 'top.setPrefs4()', document.Txns)\"><img src=\"imagenes/b_continuar.gif\" border=\"0\"></a>"); // Para Certificación
            }
        }
    }
    else
    {
        String txtCadImpresion  = ""; // Para Certificación
        txtCadImpresion = "~AFOREDEP~"+(String)session.getAttribute("d_ConsecLiga") + "~"
                                      + Nombre + "~"
                                      + (String)datasession.get("txtAfiliacion") + "~"
                                      + delCommaPointFromString((String)datasession.get("txtEfectivo")) + "~" 
                                      + delCommaPointFromString((String)session.getAttribute("doctos")) + "~"
                                      + delCommaPointFromString((String)session.getAttribute("total")) + "~"
                                      + Curp + "~";
        session.setAttribute("txtAfore", txtCadImpresion);
%>
        <table>
            <tr>
                <td colspan=3><a id="cont" tabindex="1" href="JavaScript:aceptar(document.Txns)"><img src="../imagenes/b_continuar.gif" alt="Continuar" border=0></a></td>
            </tr>
        </table>
        
        <script language="javascript">
        
        	var liga = document.getElementById("cont");
        	liga.focus();
        	
        </script >
<%    }
  //Certificaion Fin  %>
<%}
%>
<table border="0" cellspacing="0">
<tr>
<td>
</td>
</tr>
<tr>
<td>
  <input type="hidden" name="transaction" value="<%= session.getAttribute("page.iTxn") %>">
  <input type="hidden" name="transaction1" value="<%= session.getAttribute("page.oTxn") %>">
  <input type="hidden" name="supervisor" value="<%= session.getAttribute("teller") %>">
  <input type="hidden" name="override" value="<%= needOverride%>">
  <input type="hidden" name="moneda" value="<%= session.getAttribute("page.moneda") %>">
</td>
</tr>
</table>
</form>
</body>
</html>
