<!--
//*************************************************************************************************
//             Funcion: JSP para despliege de txns varias
//            Elemento: PageBuilder.jsp
//      Modificado por: Jesus Emmanuel Lopez Rosales
//*************************************************************************************************
// CCN - 4360275 - 18/02/2005 - WAS 5.1
// CCN - 4360334 - 17/06/2005 - Se convierte fieldLector a fragmento
// CCN - 4360390 - 07/10/2005 - Se agrega validacion para acdo 1053.
// CCN - 4360443 - 24/02/2006 - Se realizan modificaciones para pedir autorización para el depto 9010
// CCN - 4360477 - 19/05/2006 - Se agrega validación para txn 5405 en tipo de identifiación cuando es númerico
// CCN - 4360533 - 20/10/2006 - Se realizan modificaciones para OPEE
// CCN - 4360572 - 23/02/2007 - Se realizan modificacion en validación de monto de txn 5357 y bandera de autoriz remota en txn 0041
// CCN - 4360581 - 12/03/2007 - Se convierten los estilos en jsf y se hace un include en el jsp
// CCN - 4360584 - 23/03/2007 - Modificaciones para proyecto OPMN
// CCN - 4360619 - 15/06/2007 - Se eliminan systems
// CCN - 4620018 - 12/10/2007 - Se valida monto 
// CCN - 4620021 - 17/10/2007 - Se agregan validaciones para montos mayores a 10000 dlls, Humberto Balleza
// CR -  CR11593 - 14/02/2011 - Corrección en impresión de pago voluntario
//*************************************************************************************************
-->
<%@ page session="true" import="java.util.*,
	ventanilla.GenericClasses"%>

<%
response.setHeader("Cache-Control","no-store"); //HTTP 1.1
response.setHeader("Pragma","no-cache"); //HTTP 1.0
response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<%
String MontoPro = new String("");
Vector listaCampos = (Vector)session.getAttribute("page.listcampos");
Vector listaContenidos = (Vector)session.getAttribute("page.listcont");
String txnAuthLevel = (String)session.getAttribute("page.txnAuthLevel");
String txnIMGAutoriz = (String)session.getAttribute("page.txnIMGAutoriz");
Vector v = (Vector)session.getAttribute("page.specialacct");
Hashtable datasession = (Hashtable)session.getAttribute("page.datasession");
String txn = session.getAttribute("page.cTxn").toString();
ventanilla.GenericClasses gc = new ventanilla.GenericClasses();
if (datasession == null)
   datasession = new Hashtable();
int k;
int i;
//String edoCtaKronner = "000153049620071130000099000000000000001JUAN ROBERTO *TORRES ESPINOSA           0001500000000000000020071102024cmtH BANK (PANAMA) S20110202102008020402USDDOLAR AMERICANO  1305030000200712030001452125487500000000                 1000114160000000000000000000150000000000000000000000150000000000000000000000000000000000000000000000000000000000000000000125000000000000000000000008319450000000000000000000000000000000000000000000000000000133319450000000000000000000000000000000000000000000000000001508319450000000000000000000000000000TRADICIONAL         00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000010000000000000                    00000000000000000000000000000000VIGENTE   SUCURSAL DEFAULT                        * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *                                                             * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 00                                                                                                    00000000000410000000000000000000000000000000000________________________________________________________________________________________________________________________0000000           1201102020000000000000000000000000000000000000000000000000001400000000000000000000000000000000000000013331945000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000012500000000000000000000000000000000000000000000000000000000000000000000000000000000000000000200711150000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000";
String edoCtaKronner =session.getAttribute("respuestaKronner").toString();
String cta =(String)session.getAttribute("ctaRepago");
String cliente =session.getAttribute("cliente").toString();
String numCredito =(String)session.getAttribute("creditoKronner");
%>
<html>
<head>
  <title>Pagos Voluntarios</title>
 <%@include file="style.jsf"%>
<script language="javascript">

function cancelar()
{
  var form = document.forms[0]
  form.action = "Final.jsp";
  form.submit();
}

function aceptar(forma)
{
	forma.submit();
    var obj = document.getElementById('btnCont');
    if(obj !=null)
    	obj.onclick = new Function('return false');
}
  
</script>
</head>
<body bgcolor="#FFFFFF" onload="top.setFieldFocus(window.document)">
<br>
<form name="entry">
<p>
<h1>Pago de Préstamos</h1>
<h3>
	P001  Pagos Voluntarios
</h3>
<%!

  private String getItem(String newCadNum, int newOption)
  {
   int iPIndex = newCadNum.indexOf("CONSECUTIVO:") + 12, NumSaltos, Num = 0, Ban = 0;
   String nCadNum = new String("");
   NumSaltos = newOption;
   while(Num < NumSaltos)
   {
    if(newCadNum.charAt(iPIndex) == 0x20)
    {
     while(newCadNum.charAt(iPIndex) == 0x20)
      iPIndex++;
     Num++;
    }
    else
     while(newCadNum.charAt(iPIndex) != 0x20)
      iPIndex++;
   }
   while(newCadNum.charAt(iPIndex) != 0x20 && iPIndex < newCadNum.length()){
    nCadNum = nCadNum + newCadNum.charAt(iPIndex);
    if(iPIndex < newCadNum.length())
      iPIndex++;
    if(iPIndex == newCadNum.length())
      break;
   }

   return nCadNum;
  }
%>
<%
				
//SALDOS VIGENTES
String capitalSVig =(String)session.getAttribute("RS_SDO_CAPVIG");
capitalSVig=gc.FormatAmount(capitalSVig);
String interesSVig=(String)session.getAttribute("RS_SDO_INTS");
interesSVig=gc.FormatAmount(interesSVig);
String feciSVig=(String)session.getAttribute("RS_IMP_FECI_VIG");
feciSVig=gc.FormatAmount(feciSVig);
String gastosSVig=(String)session.getAttribute("RS_GASTOSVIG");
gastosSVig=gc.FormatAmount(gastosSVig);
//String segurosSVig=(String)session.getAttribute("RS_SEGURO");
//segurosSVig=gc.FormatAmount(segurosSVig);
String segVidaVig=(String)session.getAttribute("RS_IMP_SEG_VIG");
segVidaVig=gc.FormatAmount(segVidaVig);
//String segOtrosVig=(String)session.getAttribute("RS_IMP_SEGOVIG");
//segOtrosVig=gc.FormatAmount(segOtrosVig);
String comisionesSVig=(String)session.getAttribute("RS_COMISIONESVIG");
comisionesSVig=gc.FormatAmount(comisionesSVig);
String totalVigente=(String)session.getAttribute("RS_TOT_VIGENTE");
totalVigente=gc.FormatAmount(totalVigente);

// SALDOS VENCIDOS
String capitalSVen =(String)session.getAttribute("RS_SDO_VENC");
capitalSVen=gc.FormatAmount(capitalSVen);
String interesSVen=(String)session.getAttribute("RS_INT_VENC");
interesSVen=gc.FormatAmount(interesSVen);
String feciSVen=(String)session.getAttribute("RS_IMP_FECI_VEN");
feciSVen=gc.FormatAmount(feciSVen);
String gastosSVen=(String)session.getAttribute("RS_GASTOS");
gastosSVen=gc.FormatAmount(gastosSVen);
//String segurosSVen=(String)session.getAttribute("RS_SDO_SEGVEN");
//segurosSVen=gc.FormatAmount(segurosSVen);
String segVidaVen=(String)session.getAttribute("RS_IMP_SEG_VEN");
segVidaVen=gc.FormatAmount(segVidaVen);
//String segOtrosVen=(String)session.getAttribute("RS_IMP_SEGOVEN");
//segOtrosVen=gc.FormatAmount(segOtrosVen);
String comisionesSVen=(String)session.getAttribute("RS_COMISIONES");
comisionesSVen=gc.FormatAmount(comisionesSVen);
String moratorioSVen=(String)session.getAttribute("RS_SDO_MORA");
moratorioSVen=gc.FormatAmount(moratorioSVen);
String totalVencido=(String)session.getAttribute("RS_TOT_VENCIDO");
totalVencido=gc.FormatAmount(totalVencido);

//TOTALES
//String totalPagado=(String)session.getAttribute("RS_TOT_ADEUDO");
//String totalPagado=(String)datasession.get("txtMonto");
String totalPagado=(String)datasession.get("MontoFinal");
//totalPagado=gc.FormatAmount(totalPagado);
String totalAdeudo=(String)session.getAttribute("TOTAL_ADEUDO");
totalAdeudo=gc.FormatAmount(totalAdeudo);

//AMORIZACIONES SIGUIENTES
String amorSig = (String)session.getAttribute("RS_IVA_CAP_VEN");
amorSig = gc.FormatAmount(amorSig);

//ITBMS
String itbmsVencido=(String)session.getAttribute("RS_ITBM_VEN");
itbmsVencido=gc.FormatAmount(itbmsVencido);
String itbmsVigente=(String)session.getAttribute("RS_ITBM_VIG");
itbmsVigente=gc.FormatAmount(itbmsVigente);


%>
<table border="0" cellspacing="0">
		<tr>
			<td>Cliente: </td>
			<td width="300"><%=cliente%></td>
			<td width="10">&nbsp;</td>
		</tr>
		<tr>
			<td>Cuenta:</td>
			<td><%=cta%></td>
			<td width="10">&nbsp;</td>
		<tr>
			<td>Número de Crédito: </td>
			<td><%=numCredito%></td>
			<td width="10">&nbsp;</td>
		<tr>			
			<td></td>
			<td width="10">&nbsp;</td>	
		<tr>
			<td><b>SALDOS VIGENTES:</b></td>
			<td></td>
			<td width="10">&nbsp;</td>
		<tr>
			<td>Capital: </td>
			<td><%=capitalSVig%></td>
			<td width="10">&nbsp;</td>
		<tr>
			<td>Interés :</td>
			<td><%=interesSVig%></td>
			<td width="10">&nbsp;</td>
		<tr>
			<td>FECI:</td> 
			<td><%=feciSVig%></td>
			<td width="10">&nbsp;</td>
		<tr>		
			<td>Gastos:</td> 
			<td><%=gastosSVig%></td>
			<td width="10">&nbsp;</td>
		<tr>		
			<td>Seguros:</td> 
			<td><%=segVidaVig%></td>
			<td width="10">&nbsp;</td>
		<tr>		
			<td>Comisiones:</td>  
			<td><%=comisionesSVig%></td>
			<td width="10">&nbsp;</td>
		<tr>	
			<td>ITBMS:</td>  
			<td><%=itbmsVigente%></td>
			<td width="10">&nbsp;</td>
		<tr>	
			<td><b>Total Vigente: </b></td> 
			<td><%=totalVigente%></td>
			<td width="10">&nbsp;</td>
		<tr>			
			<td></td>
			<td width="10">&nbsp;</td>
		<tr>			
			<td><b>SALDOS VENCIDOS:</b></td>			
		<tr>			
			<td>Capital:</td>   
			<td><%=capitalSVen%></td>	
			<td width="10">&nbsp;</td>
		<tr>			
			<td>Interes: </td> 
			<td><%=interesSVen%></td>
			<td width="10">&nbsp;</td>
		<tr>		
			<td>FECI: </td> 
			<td><%=feciSVen%></td>
			<td width="10">&nbsp;</td>
		<tr>			
			<td>Gastos: </td> 
			<td><%=gastosSVen%></td>
			<td width="10">&nbsp;</td>
		<tr>		
			<td>Seguros: </td> 
			<td><%=segVidaVen%></td>
			<td width="10">&nbsp;</td>
		<tr>			
			<td>Comisiones: </td> 
			<td><%=comisionesSVen%></td>
			<td width="10">&nbsp;</td>
		<tr>
			<td>ITBMS:</td>  
			<td><%=itbmsVencido%></td>
			<td width="10">&nbsp;</td>
		<tr>		
			<td>Moratorios: </td> 
			<td><%=moratorioSVen%></td>
			<td width="10">&nbsp;</td>
		<tr>		
			<td><b>Total Vencido: </b></td> 
			<td><%=totalVencido%></td>
			<td width="10">&nbsp;</td>
		<tr>			
			<td><b>Cargo por aplicar mes corriente: </b></td> 
			<td><%=amorSig%></td>
			<td width="10">&nbsp;</td>
		</tr>
		<tr>			
			<td> </td> 
			<td> </td>
			<td width="10">&nbsp;</td>
		<tr>			
			<td><b>TOTAL PAGADO: </b></td> 
			<td><%=totalPagado%></td>
			<td width="10">&nbsp;</td>
		<tr>			
			<td><b>TOTAL ADEUDADO:</b></td> 
			<td><%=totalAdeudo%></td>
			<td width="10">&nbsp;</td>
		</tr>
		<tr>
			<td>
				<br><a id="btnCont" tabindex = "1" href="JavaScript:cancelar()"><img src="../imagenes/b_continuar.gif" border="0"></a></td>
				
		</tr>
</table>

<script language="javascript">
	var liga = document.getElementById("btnCont");
	liga.focus();
</script>


<%  //Certificación Inicio
	Vector resp = (Vector)session.getAttribute("response");
	String isCertificable = (String)session.getAttribute("page.certifField");  // Para Certificación
    String txtCadImpresion  = ""; // Para Certificación
    session.setAttribute("Consecutivo", getItem((String)resp.elementAt(3), 1)); // Para Certificación
    session.setAttribute("Tipo", isCertificable); // Para Certificación
	txtCadImpresion = "PAGOVOLUNTARIO" + "~" + cliente + "~" + numCredito + "~" + capitalSVig + "~" + interesSVig + "~"
						+ feciSVig + "~" + gastosSVig + "~" + segVidaVig + "~" + comisionesSVig + "~" + totalVigente 
						+ "~" + capitalSVen + "~" + interesSVen + "~" + feciSVen + "~" + gastosSVen + "~" + segVidaVen 
						+ "~" + comisionesSVen + "~" + moratorioSVen + "~" + totalVencido + "~" + totalPagado + "~" + 
                                                totalAdeudo + "~" + amorSig + "~" + itbmsVigente + "~" + itbmsVencido+ "~";

   if(!isCertificable.equals("N")){ // Para Certificación
		out.println("<script language=\"javascript\">top.openDialog('../../servlet/ventanilla.ImprimirServlet?txtCadImpresion=" + txtCadImpresion + "', 160, 120, 'top.setPrefs4()', document.Txns)</script>"); // Para Certificación
   } 
%>

<input type="hidden" name="iTxn" value="<%= session.getAttribute("page.iTxn") %>">
<input type="hidden" name="oTxn" value="<%= session.getAttribute("page.oTxn") %>">
<input type="hidden" name="cTxn" value="<%= session.getAttribute("page.cTxn") %>">
<input type="hidden" name="sucursal" value="<%= session.getAttribute("branch") %>">
<input type="hidden" name="teller" value="<%= session.getAttribute("teller") %>">
<input type="hidden" name="moneda" value="<%= session.getAttribute("page.moneda")%>">
<input type="hidden" name="registro" value="<%=session.getAttribute("empno")%>">
<input type="hidden" name="tllrlvl" value="<%=session.getAttribute("tellerlevel")%>">
<input type="hidden" name="supervisor" value="<%= session.getAttribute("teller") %>">
<input type="hidden" name="AuthoOK" value="0">
<input type="hidden" name="SignOK" value="0">
<input type="hidden" name="validateFLD" value="0">
<input type="hidden" name="needSignature" value="<%out.print(txnIMGAutoriz);%>">
<input type="hidden" name="needAutho" value="<%out.print(txnAuthLevel);%>">
<input type="hidden" name="SignField" value="<%= session.getAttribute("page.SignField") %>">
<input type="hidden" name="override" value="N">
<input type="hidden" name="txtFees" value="0">
<input type="hidden" name="blagP001" value="0">
<input type="hidden" name="compania" value="20">
<input type="hidden" name="txtSelectedItem" value="0" >
<input type="hidden" name="returnform" value="N" >
<input type="hidden" name="returnform2" value="N" >
<input type="hidden" name="txtPlazaSuc" value="<%= session.getAttribute("plaza") %>" >
<input type="hidden" name="passw" value="<%=session.getAttribute("password")%>">
<input type="hidden" name="MontoRetiroPesos" value = "<%=session.getAttribute("Retiro.Pesos")%>">
<input type="hidden" name="MontoRetiroDolar" value = "<%=session.getAttribute("Retiro.Dolar")%>">


</form>
</body>
</html>
