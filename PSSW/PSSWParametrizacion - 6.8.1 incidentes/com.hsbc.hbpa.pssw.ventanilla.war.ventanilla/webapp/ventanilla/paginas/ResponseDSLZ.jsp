<!--
//*************************************************************************************************
//             Funcion: JSP que despliega respuesta de txn DSLZ
//            Elemento: ResponseDSLZ.jsp
//          Creado por: Alejandro Gonzalez Castro
//      Modificado por: Alejandro Gonzalez Castro
//*************************************************************************************************
// CCN - 4360175 - 03/08/2004 - Ajuste size de ventana de impresion
// CCN - 4360189 - 03/09/2004 - Se redirecciona a Final.jsp por Control de Efectivo
// CCN - 4360275 - 18/02/2005 - WAS 5.1
// CCN - 4360275 - 18/02/2005 - Se realizan modificaciones para la presentacion correcta
//                              de los datos del beneficiario y ordenante.
//*************************************************************************************************
-->
<%@ page session="true" import="java.util.*"%>
<%
response.setHeader("Cache-Control","no-store"); //HTTP 1.1
response.setHeader("Pragma","no-cache"); //HTTP 1.0
response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<%!
private String getString(String s)
{
    int len = s.length();
    
    for(; len>0; len--)
	if(s.charAt(len-1) != ' ')
	    break;

    return s.substring(0, len);
}

private String setPointToString(String newCadNum)
{
   int iPIndex = newCadNum.indexOf("."), iLong;
   String szTemp;

   if(iPIndex > 0)
   {
     newCadNum = new String(newCadNum + "00");
     newCadNum = newCadNum.substring(0, iPIndex + 1) + newCadNum.substring(iPIndex + 1, iPIndex + 3);
   } else {
     for(int i = newCadNum.length(); i < 3; i++)
       newCadNum = "0" + newCadNum;
       
     iLong = newCadNum.length();
     
     if(iLong == 3)
       szTemp = newCadNum.substring(0, 1);
     else
       szTemp = newCadNum.substring(0, iLong - 2);
       
     newCadNum = szTemp + "." + newCadNum.substring(iLong - 2);
   }

   return newCadNum;
}

private String delPointOfString(String newCadNum)
{
   int iPIndex = newCadNum.indexOf(".");

   if(iPIndex > 0)
   {
    newCadNum = newCadNum.substring(0, iPIndex) +
                newCadNum.substring(iPIndex + 1, newCadNum.length());
   }

   return newCadNum;
}


private String delCommaPointFromString(String newCadNum)
{
   String nCad = new String(newCadNum);
   
   if( nCad.indexOf(".") > -1)
   {
     nCad = nCad.substring(nCad.indexOf(".") + 1, nCad.length());
     if(nCad.length() != 2)
     {
       String szTemp = new String("");
       
       for(int j = nCad.length(); j < 2; j++)
         szTemp = szTemp + "0";
         
       newCadNum = newCadNum + szTemp;
     }
   }

   StringBuffer nCadNum = new StringBuffer(newCadNum);
   for(int i = 0; i < nCadNum.length(); i++)
     if(nCadNum.charAt(i) == ',' || nCadNum.charAt(i) == '.')
       nCadNum.deleteCharAt(i);

   return nCadNum.toString();
}


private String setFormat(String newCadNum)
{
    int iPIndex = newCadNum.indexOf(".");
    String nCadNum = new String(newCadNum.substring(0, iPIndex));
    
    for (int i = 0; i < (int)((nCadNum.length()-(1+i))/3); i++)
      nCadNum = nCadNum.substring(0, nCadNum.length()-(4*i+3))+','+nCadNum.substring(nCadNum.length()-(4*i+3));

    return nCadNum + newCadNum.substring(iPIndex, newCadNum.length());
}
%>
<%
    Vector resp = (Vector)session.getAttribute("response");
    Hashtable datasession = (Hashtable)session.getAttribute("page.datasession");
    String Continuar = "SI";
    String ordenSuspendida = "NO";
%>
<html>
<head>
    <title>Certificación</title>
    <style type="text/css">Body {font-family: Arial, Helv; font-size: 10pt; font-weight: bold; color:#000000}</style>
    <script language="javascript">
        function doSubmit()
        {
          if(document.forms[0].borrarPagina.value=='SI')
            document.forms[0].action = 'Final.jsp';
            
          if(document.forms[0].liberarOrden.value=='SI')
            document.forms[0].action = '../../servlet/ventanilla.DataServlet';
            
          document.forms[0].submit();
        }
    </script>
</head>
<body bgcolor="#FFFFFF">

<%
    String iTxn = (String)session.getAttribute("page.iTxn");
    
    if(!iTxn.equals("4045"))
      out.print("<form name=\"Txns\" action=\"../../servlet/ventanilla.PageServlet\" method=\"post\">");
    else
      out.print("<form name=\"Txns\" action=\"../../servlet/ventanilla.DataServlet\" method=\"post\">");
      
    if(session.getAttribute("txtCasoEsp") != null && !datasession.get("cTxn").toString().equals("SLOP"))
    {
      out.println("<script language='javascript'>top.openDialog('../../servlet/ventanilla.ImprimirServlet', 160, 120, 'top.setPrefs4()', document.Txns)</script>");
    }
%>
    <input type="hidden" name="transaction" value="<%= session.getAttribute("page.iTxn") %>">
    <input type="hidden" name="transaction1" value="<%= session.getAttribute("page.oTxn") %>">
<p>
<%
    String statusOrden = new String("");
    if(datasession.get("statusOrden") != null)
      statusOrden = datasession.get("statusOrden").toString();
  
    String szResp = "  ";
    String txtStatus = "";
    if( resp.size() != 4 ) 
      out.println("<b>Error de conexi&oacute;n</b><br>");
    else
    {
        try
        {
            int codigo = Integer.parseInt((String)resp.elementAt(0));
            
            if( codigo != 0 )
            {
                String msgResp = (String)resp.elementAt(3);
                out.println("<b>Error en transaccion</b><br>");
                if ( datasession.get("cTxn").toString().equals("SLOP"))
                {
                    out.println(msgResp);  
                    statusOrden = "LIBERADA";
                }
                else
                {
       	           int linesM = Integer.parseInt((String)resp.elementAt(1));
       	           
     	           for(int i=0; i<linesM; i++) 
     	           {
    		       String linea = msgResp.substring( i*78, Math.min(i*78+77, msgResp.length()) );
     		       out.println(getString(linea) + "<p>");
     		       
     		       if(Math.min(i*78+77, msgResp.length()) == msgResp.length())
      		         break;
    	           }
                }
            } 
            else 
            {
                int lines = Integer.parseInt((String)resp.elementAt(1));
                szResp = (String)resp.elementAt(3);
                String itxn = (String)datasession.get("iTxn");
                String txn = (String)datasession.get("cTxn");
                String bBoton = (String)datasession.get("Cancelar.y");
                
                if ( szResp.indexOf("CONSECUTIVO:") > -1)
                   szResp = szResp.substring(21,24);
                   
                if(txn.equals("DSLZ"))
                {
                    if(szResp.length() > 3)
                    {
                        if(szResp.substring(0, 3).equals("003"))
                        {
                            out.println("-* TRANSACCION ACEPTADA POR MERVA *-<br><br>");
                            out.println("Número de Orden: " + getString(szResp.substring(3, szResp.length())) + "<p>");
                        }
                    }
                } 
                else if(txn.equals("SLOP") && !statusOrden.equals("LIBERADA"))
                {
                    if(szResp.substring(0, 1).equals("0"))
                    {
                        String txtOSN = szResp.substring(11, 19);
                        String txtFecha = szResp.substring(150, 156);
                        String txtMoneda = szResp.substring(156, 159);
                        String txtMonto = szResp.substring(159, 175);
                        String txtApeOrd = szResp.substring(178, 208);
                        String txtNomOrd = szResp.substring(208, 228);
                        String txtApeBen = szResp.substring(228, 258);
                        String txtNomBen = szResp.substring(258, 278);
                        String txtBanco = szResp.substring(320, 324);
                        String txtCiudad = szResp.substring(278, 298);
                        String txtInstruc = "";
                        
                        if(szResp.length() > 328)
                            txtInstruc = szResp.substring(328, szResp.length());
                            
                        String eTxn = (String)session.getAttribute("page.iTxn");
                        
                        if ( eTxn.equals("0112") || eTxn.equals("0114"))
                        {
                            String MontoOrden = setFormat(setPointToString(delCommaPointFromString(txtMonto.trim())));
                            String txtMontoPesos = (String)datasession.get("txtMontoPesos");
                            String MontoPesos = setFormat(setPointToString(delCommaPointFromString(txtMontoPesos.trim())));
                            
                            if (!MontoOrden.equals(MontoPesos))
                            {
                                out.println("Error!! El monto de la orden ES DIFERENTE  al capturado");
                                datasession.put("cTxn", "freeOrder");
                                datasession.put("txtOSN", txtOSN.trim());
                                session.setAttribute("page.datasession", datasession);
                                Continuar = "NO";
                            }
                        }
                        
                        if ( Continuar.equals("SI"))
                        {
                            txtStatus = szResp.substring(324, szResp.length());
                            if((txtStatus.trim().substring(0,3).equals("EXP") && !iTxn.equals("0585"))|| 
                               (txtStatus.trim().substring(0,3).equals("REA") && !iTxn.equals("0585")) ||
                               (txtStatus.trim().substring(0,3).equals("SUS") && iTxn.equals("0023")) ||
                               (txtStatus.trim().substring(0,3).equals("LIQ") && iTxn.equals("0585")))
                            {
                                out.println("<table border=\"0\">");
                                out.println(" <tr>");
                                out.print("  <td>Estatus:</td>" + "<td width=\"10\">&nbsp;</td><td>"  + txtStatus.substring(0,3) + "</td><td width=\"10\">&nbsp;</td>");
                                out.println("<td>Folio:</td>" + "<td width=\"10\">&nbsp;</td><td>"  + datasession.get("txtNumOrden") + "</td>");
                                out.println(" </tr>");
                                out.println(" <tr>");
                                out.print("  <td>Monto:</td><td width=\"10\">&nbsp;</td><td>" + setFormat(setPointToString(delCommaPointFromString(txtMonto.trim()))) + "</td>");
                                out.println(" </tr>");
                                out.println(" <tr>");
                                out.println("<td><i><b>Ordenante:</b></i></td>");
                                out.println(" </tr>");
                                out.println(" <tr>");
                                out.println(" <td>Nombre:</td><td width=\"10\">&nbsp;</td><td>" + txtNomOrd + "</td>");
                                out.println(" </tr>");
                                out.println(" <tr>");
                                out.println(" <td>Apellido:</td><td width=\"10\">&nbsp;</td><td>" + txtApeOrd + "</td>");
                                out.println(" </tr>");
                                out.println(" <tr>");
                                out.println("<td><i><b>Beneficiario:</b></i></td>");
                                out.println(" </tr>");
                                out.println(" <tr>");
                                out.println(" <td>Nombre:</td><td width=\"10\">&nbsp;</td><td>" + txtNomBen + "</td>");
                                out.println(" </tr>");
                                out.println(" <tr>");
                                out.println(" <td>Apellido:</td><td width=\"10\">&nbsp;</td><td>" + txtApeBen + "</td>");
                                out.println(" </tr>");
                                out.println("</table>");
                                datasession.put("txtMonto", txtMonto.trim());
                                datasession.put("txtFecha", txtFecha.trim());
                                datasession.put("txtOSN", txtOSN.trim());
                                datasession.put("txtBanco", txtBanco.trim());
                                datasession.put("txtApeOrd", txtApeOrd.trim());
                                datasession.put("txtNomOrd", txtNomOrd.trim());
                                datasession.put("txtApeBen", txtApeBen.trim());
                                datasession.put("txtNomBen", txtNomBen.trim());
                                datasession.put("txtStatusOrden", txtStatus.substring(0,3));
                                txtCiudad = txtCiudad.trim();
                                txtInstruc = txtInstruc.trim();
                                if(txtCiudad.length() > 0)
                                    datasession.put("txtCiudad", txtCiudad);
                                if(txtInstruc.length() > 0)
                                    datasession.put("txtInstruc", txtInstruc);
                                session.setAttribute("page.datasession", datasession);
                            }
                            else
                            {
                                if(szResp.substring(0, 1).equals("0"))
                                {
                                    txtOSN = szResp.substring(11, 19);
                                    datasession.put("txtOSN", txtOSN.trim());
                                }
                                
                                if (!iTxn.equals("0585"))
                                   out.println("La orden YA SE LIQUIDÓ / SUSPENDIÓ / O FUE REVERSADA con anterioridad, presione cancelar.");
                                else
                                   out.println("La orden NO tiene estatus LIQUIDADA, presione cancelar.");  
                                
                                datasession.put("cTxn", "freeOrder");
                                session.setAttribute("page.datasession", datasession);
                            }
                        }
                    }
                    else
                    {
                        out.print(szResp.trim().substring(10,szResp.length()));
                    }
                }
                else if(txn.equals("LQOP"))
                {
                    if(szResp.length() == 3)
                        if(szResp.equals("003"))
                        {
                            if (!statusOrden.equals("LIBERADA"))
                            {
                               if(itxn.equals("0022"))
                                  out.print("La Orden de Pago ha sido LIQUIDADA");
                               else
                                   out.print("La Orden de Pago ha sido CANCELADA");
                           }
                        }
                }
                else if(((txn.equals("SLOP") && statusOrden.equals("LIBERADA"))) || txn.equals("freeOrder"))
                {
                    //out.println("presione cancelar...");
                }
                else if(txn.equals("4045") && !(statusOrden.equals("LIBERADA")))
                {
                    if(szResp.length() == 3)
                        if(szResp.equals("003"))
                        {
                            out.print("La Orden de Pago ha sido SUSPENDIDA");
                            ordenSuspendida = "SI";
                        }
                }
                else if(txn.equals("4045") && statusOrden.equals("LIBERADA"))
                {
                    response.sendRedirect("Final.jsp");
                }
            }
        }
        catch(NumberFormatException nfe)
        {
          out.println("<b>Error de conexi&oacute;n</b><br>");
        }
    }
    
    Stack flujotxn = (Stack)session.getAttribute("page.flujotxn");
    if(iTxn.equals("4045"))
    {
        if(!flujotxn.empty())
        {
            flujotxn.pop();
            session.setAttribute("page.flujotxn", flujotxn);
            datasession.put("cTxn","4045");
            session.setAttribute("page.datasession", datasession);
        }
    }
%>
  <input type="hidden" name="transaction" value="<%= session.getAttribute("page.iTxn") %>">
  <input type="hidden" name="transaction1" value="<%= session.getAttribute("page.oTxn") %>">
<%
    String isCertificable = (String)session.getAttribute("page.certifField");
    if(!flujotxn.empty())
    {
        out.print("<div align=\"center\">");
        
        if(isCertificable.equals("1"))
          out.println("<a href=\"javascript:top.openDialog('../../servlet/ventanilla.ImprimirServlet', 160, 120, 'top.setPrefs4()', document.Txns)\"><img src=\"imagenes/b_continuar.gif\" border=\"0\"></a>"); // Para Certificación
        else
        {
        }
        
        out.print("</div>");
    }
%>
  <br>
  <% 
  String liberarOrden = new String("SI");
  
  if(statusOrden.equals("LIBERADA"))
    liberarOrden = new String("NO");
%>
  <input type="hidden" name="liberarOrden" value="NO">
  <input type="hidden" name="borrarPagina" value="NO">
<%
if (txtStatus.equals(""))
  txtStatus = "000";
  
if(szResp.substring(0, 1).equals("0") && 
      ((txtStatus.substring(0,3).trim().equals("EXP") && !iTxn.equals("0585")) ||
       (txtStatus.substring(0,3).trim().equals("REA") && !iTxn.equals("0585")) ||
       (txtStatus.substring(0,3).trim().equals("SUS") && iTxn.equals("0023")) ||
       (txtStatus.substring(0,3).trim().equals("LIQ") && iTxn.equals("0585"))) &&
       !(statusOrden.equals("LIBERADA")) && Continuar.equals("SI") && ordenSuspendida.equals("NO"))
{
%>
        <a href="javascript:doSubmit()"><img name="Aceptar" src="../imagenes/b_aceptar.gif" alt="Aceptar" border="0"></a>
<%
}

String Txne = (String)session.getAttribute("page.iTxn");

if ( !statusOrden.equals("LIBERADA")) 
{
%>
  <a href="javascript:document.forms[0].borrarPagina.value='SI';document.forms[0].liberarOrden.value='<%=liberarOrden%>';doSubmit()"><img name="Cancelar" src="../imagenes/b_cancelar.gif" alt="Cancelar" border="0"></a>
<%
}
%>
</form>
</body>
</html>
