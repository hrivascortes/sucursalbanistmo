<!--
//*************************************************************************************************
//             Funcion: JSP que despliega respuesta de txn Nomina
//            Elemento: ResponseNOM1.jsp
//          Creado por: Alejandro Gonzalez Castro
// Ultima Modificacion: 03/08/2004
//      Modificado por: Alejandro Gonzalez Castro
//*************************************************************************************************
// CCN - 4360175 - 03/08/2004 - Ajuste size de ventana de impresion
//*************************************************************************************************
-->

<%@ page session="true" import="java.util.Hashtable"%>
<%
response.setHeader("Cache-Control","no-store"); //HTTP 1.1
response.setHeader("Pragma","no-cache"); //HTTP 1.0
response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
<title>Results</title>
<style type="text/css">Body {font-family: Arial, Helv; font-size: 10pt; font-weight: bold; color:#000000}</style>
</head>
<body>
Proceso de Nomina Terminado...
<form name="ImprimeDiario">
<%
	Hashtable data = (Hashtable)session.getAttribute("page.datasession");
	String cajero =  (String)session.getAttribute("teller");
	String contrato = (String)data.get("contrato");
	String nombre = (String)data.get("nombre");
	String nomina = "NOMINA~" + cajero + "~" + contrato + "~" + nombre + "~";
  nomina = java.net.URLEncoder.encode(nomina);
  out.println("<a href=\"javascript:top.openDialog('../../servlet/ventanilla.ImprimirServlet?txtCadImpresion=" + nomina + "', 160, 120, 'top.setPrefs4()', document.Txns)\"><img src=\"../imagenes/b_continuar.gif\" border=\"0\"></a>");
%>
</form>
</body>
</html>
