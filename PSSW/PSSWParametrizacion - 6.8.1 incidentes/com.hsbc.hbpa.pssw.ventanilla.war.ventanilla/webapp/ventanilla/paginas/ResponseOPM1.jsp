<!--
//*************************************************************************************************
//             Funcion: JSP que despliega respuesta de txn OP Masivas
//            Elemento: ResponseOPM1.jsp
//          Creado por: Alejandro Gonzalez Castro
// Ultima Modificacion: 03/08/2004
//      Modificado por: Alejandro Gonzalez Castro
//*************************************************************************************************
// CCN - 4360175 - 03/08/2004 - Ajuste size de ventana de impresion
//*************************************************************************************************
-->
<%@ page session="true" import="java.util.Hashtable"%>
<%
response.setHeader("Cache-Control","no-store"); //HTTP 1.1
response.setHeader("Pragma","no-cache"); //HTTP 1.0
response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
<title>Results</title>
<style type="text/css">Body {font-family: Arial, Helv; font-size: 10pt; font-weight: bold; color:#000000}</style>
</head>
<body>
Proceso de Expedici&oacute;n Masiva de Ordenes de Pago Terminado...
<br><br>
<form name="actopmas">
<%
	Hashtable data = (Hashtable)session.getAttribute("page.datasession");
	String opm = "OrdenesPagoMasivas~";
	String txn = (String)data.get("cTxn");
	if(txn.equals("0025"))
	{%>
	
<OBJECT classid="clsid:8AD9C840-044E-11D1-B3E9-00805F499D93" codebase="http://java.sun.com/products/plugin/1.2.2/jinstall-1_2_2-win.cab#Version=1,2,2,0" WIDTH ="350" HEIGHT = "50">
		<PARAM name="type" value="application/x-java-applet;version=1.2">
		<PARAM name="java_ARCHIVE" value="DiskUpdateOP.jar">
		<PARAM name="java_CODE" value="DiskUpdateOP">
		<PARAM name="java_NAME" value="updateop">
		<PARAM name="java_CODEBASE" value="../">
		<PARAM NAME="mayscript" VALUE="true">
</OBJECT>

<COMMENT>	
<EMBED type="application/x-java-applet;version=1.2"
	java_ARCHIVE="DiskUpdateOP.jar"
	java_CODE ="DiskUpdateOP"
	java_NAME ="updateop"
	java_CODEBASE ="../"
	WIDTH ="350"
	HEIGHT = "50"
	MAYSCRIPT>
</EMBED>
</COMMENT>


<p>
<%  	out.println("<a href=\"javascript:top.openDialog('../../servlet/ventanilla.ImprimirServlet?txtCadImpresion=" + opm + "', 160, 120, 'top.setPrefs4()', document.Txns)\"><img src=\"../imagenes/b_aceptar.gif\" border=\"0\"></a>");%>
		<input type="hidden" name="ctacargo" value="<%=data.get("ctacargo")%>">
		<input type="hidden" name="nomcargo" value="<%=data.get("nomcargo")%>">
<%	for (int i= 1; i <11; i++)
		{%>
			<input type="hidden" name="montos<%=i%>" value="<%=data.get("montos"+i)%>">
			<input type="hidden" name="nombres<%=i%>" value="<%=data.get("nombres"+i)%>">
			<input type="hidden" name="apellidos<%=i%>" value="<%=data.get("apellidos"+i)%>">
			<input type="hidden" name="status<%=i%>" value="<%=data.get("status"+i)%>">
			<input type="hidden" name="consecs<%=i%>" value="<%=data.get("consecs"+i)%>">
		<%}%>
<%	}
	else
	{%>
<%  	out.println("<a href=\"javascript:top.openDialog('../../servlet/ventanilla.ImprimirServlet?txtCadImpresion=" + opm + "', 160, 120, 'top.setPrefs4()', document.Txns)\"><img src=\"../imagenes/b_aceptar.gif\" border=\"0\"></a>");%>
		<input type="hidden" name="ctacargo" value="<%=data.get("txtDDA")%>">
		<input type="hidden" name="nomcargo" value="<%=data.get("txtNomEmpresa")%>">
		<input type="hidden" name="montos1" value="<%=data.get("montos1")%>">
		<input type="hidden" name="nombres1" value="<%=data.get("nombres1")%>">
		<input type="hidden" name="apellidos1" value="<%=data.get("apellidos1")%>">
  	<input type="hidden" name="status1" value="<%=data.get("status1")%>">
	  <input type="hidden" name="consecs1" value="<%=data.get("consecs1")%>">
<%	}%>
		<input type="hidden" name="mtocargo" value="<%=data.get("mtocargo")%>">
		<input type="hidden" name="codecargo" value="<%=data.get("codecargo")%>">
		<input type="hidden" name="cajero" value="<%=data.get("teller")%>">
		<input type="hidden" name="sucursal" value="<%=data.get("sucursal")%>">
		<input type="hidden" name="date" value="<%=data.get("date")%>">
</form>
</body>
</html>
