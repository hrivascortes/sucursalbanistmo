<!--
//*************************************************************************************************
///            Funcion: JSP que detalles de Kroner
//            Elemento: ResponseP001.jsp
//          Creado por: Carolina Vela Seraf�n
//    
//*************************************************************************************************
// CCN -
//*************************************************************************************************
-->

<%@ page session="true" import="java.util.*,
	ventanilla.GenericClasses"%>
<%
response.setHeader("Cache-Control","no-store"); //HTTP 1.1
response.setHeader("Pragma","no-cache"); //HTTP 1.0
response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
Hashtable datasession = (Hashtable)session.getAttribute("page.datasession");
GenericClasses gc = new GenericClasses();
//String respKroner = "000153049620071130000099000000000000001JUAN ROBERTO *TORRES ESPINOSA           0001500000000000000020071102024cmtH BANK (PANAMA) S20110202102008020402USDDOLAR AMERICANO  1305030000200712030001452125487500000000                 1000114160000000000000000000150000000000000000000000150000000000000000000000000000000000000000000000000000000000000000000125000000000000000000000008319450000000000000000000000000000000000000000000000000000133319450000000000000000000000000000000000000000000000000001508319450000000000000000000000000000TRADICIONAL         00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000010000000000000                    00000000000000000000000000000000VIGENTE   SUCURSAL DEFAULT                        * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *                                                             * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 00                                                                                                    00000000000410000000000000000000000000000000000________________________________________________________________________________________________________________________0000000           1201102020000000000000000000000000000000000000000000000000001400000000000000000000000000000000000000013331945000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000012500000000000000000000000000000000000000000000000000000000000000000000000000000000000000000200711150000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000";
String cta ="4000000000";//(String)session.getAttribute("ctaKronner");
StringBuffer edoCtaKronner = new StringBuffer((String)session.getAttribute("respuestaKronner"));
session.setAttribute("respuestaKronner",edoCtaKronner);
String creditoIni=(String)session.getAttribute("creditoIni");
//String edoCtaKronner = "000153049620071130000099000000000000001JUAN ROBERTO *TORRES ESPINOSA           0001500000000000000020071102024cmtH BANK (PANAMA) S20110202102008020402USDDOLAR AMERICANO  1305030000200712030001452125487500000000                 1000114160000000000000000000150000000000000000000000150000000000000000000000000000000000000000000000000000000000000000000125000000000000000000000008319450000000000000000000000000000000000000000000000000000133319450000000000000000000000000000000000000000000000000001508319450000000000000000000000000000TRADICIONAL         00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000010000000000000                    00000000000000000000000000000000VIGENTE   SUCURSAL DEFAULT                        * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *                                                             * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 00                                                                                                    00000000000410000000000000000000000000000000000________________________________________________________________________________________________________________________0000000           1201102020000000000000000000000000000000000000000000000000001400000000000000000000000000000000000000013331945000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000012500000000000000000000000000000000000000000000000000000000000000000000000000000000000000000200711150000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000";
String cliente= session.getAttribute("cliente").toString();
String numCredito=edoCtaKronner.substring(0,8).trim();
String montoLetra =(String)session.getAttribute("montoKronner");
//montoLetra=gc.FormatAmount(montoLetra);
String SaldoExig=edoCtaKronner.substring(730,741).trim();
SaldoExig=gc.FormatAmount(SaldoExig);
//String total=(String)session.getAttribute("montoKronner");
String doctocmtH="";

%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<%
Vector resp = (Vector)session.getAttribute("response");
%>
<html>
<head>
  <title>Pagos Voluntarios</title>
 <%@include file="style.jsf"%>
<script language="javascript">

function validate(form){
		
		form.action="ResponseSaldosPV.jsp";
		form.submit();
		
	
}
 
</script>
</head>
<body bgcolor="#FFFFFF" onload="top.setFieldFocus(window.document)">
<br>
<form name="entry" action="../../servlet/ventanilla.PageServlet" method="post">
<p>
<h1>Pago de Pr�stamos</h1>
<h3>
	P001  Pagos Voluntarios
</h3>

<table border="0" cellspacing="0">
		<tr>
			<td>Cliente: </td>
			<td><%=cliente%></td>
			<td width="10">&nbsp;</td>
		<tr>
			<td>Cuenta:</td>
			<td><%=cta%></td>
			<td width="10">&nbsp;</td>
		<tr>
			<td>N�mero de prestamo: </td>
			<td><%=numCredito%></td>
			<td width="10">&nbsp;</td>
		<tr>
			<td>N�mero de Cuenta Anterior:</td>
			<td><%=creditoIni%></td>
			<td width="10">&nbsp;</td>
		<tr>
			<td>Total Letra: </td>
			<td><%=montoLetra%></td>
			<td width="10">&nbsp;</td>
		<tr>
			<td>Monto Exigible :</td>
			<td><%=SaldoExig%></td>
			<td width="10">&nbsp;</td>
		<tr>
			<td>Importe a Pagar:</td> 
			<td><%=montoLetra%></td>
			<td width="10">&nbsp;</td>
		</tr>	
			<tr>
			<td>
				<a id='btnCont' href="javascript:validate(document.entry)"><img src="../imagenes/b_continuar.gif" border="0"></a></td>
</table>
  <input type="hidden" name="transaction" value="<%= session.getAttribute("page.iTxn") %>">
  <input type="hidden" name="transaction1" value="<%= session.getAttribute("page.oTxn") %>">
  <input type="hidden" name="override" value="">
 
</form>
</body>
</html>


