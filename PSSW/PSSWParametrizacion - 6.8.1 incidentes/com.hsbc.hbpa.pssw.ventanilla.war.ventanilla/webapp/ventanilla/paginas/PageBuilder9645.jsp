<!--
//*************************************************************************************************
//             Funcion: JSP que despliega captura de txn 9645
//            Elemento: PageBuilder9645.jsp
//          Creado por: Alejandro Gonzalez Castro
//      Modificado por: Alejandro Gonzalez Castro
//*************************************************************************************************
// CCN - 4360175 - 03/08/2004 - Ajuste size de ventana de impresion
// CCN - 4360275 - 18/02/2005 - WAS 5.1
// CCN - 4360581 - 12/03/2007 - Se convierten los estilos en jsf y se hace un include en el jsp
//*************************************************************************************************
-->

<%@ page session="true" import="java.util.*"%>
<%
response.setHeader("Cache-Control","no-store"); //HTTP 1.1
response.setHeader("Pragma","no-cache"); //HTTP 1.0
response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<%
 Vector listaCampos = (Vector)session.getAttribute("page.listcampos");
 Vector listaContenidos = (Vector)session.getAttribute("page.listcont");
 int k;
%>
<html>
<head>
 <%@include file="style.jsf"%>
 <title>Datos de la Transaccion</title>
 <script   language="javascript">
 <!--
  var causas = new Array();
  
  causas[0] = new String('NO ESTA FIRMADO');
  causas[1] = new String('BANDA INCORRECTA');
  causas[2] = new String('NO HAY CONTINUIDAD EN LOS ENDOSOS');
  causas[3] = new String('SE COBRA POR CANTIDAD DISTINTA A LA QUE VALE');
  causas[4] = new String('NO CUMPLE CON LAS ESPECIFICACIONES ESTABLECIDAS POR BANXICO');
  causas[5] = new String('NO CONTIENE ORDEN INCONDICIONAL DE PAGAR DINERO');
  causas[6] = new String('2a PRESENTACION NO AUTORIZADA');
  causas[7] = new String('NEGOCIADO INDEBIDAMENTE');
  causas[8] = new String('ESTA MUTILADO / DETERIORADO');
  causas[9] = new String('CARECE DE FECHA');

  function replaceWhiteSpaces(inputString){
    var inputLength = inputString.length;
    var outputString = new String();
    var tempChar = ' ';
    var i = 0;
    for(; i < inputLength; i++){
      tempChar = inputString.charAt(i);
      if(tempChar == ' ')
        tempChar = '%20';
      outputString += tempChar;
    }
    return outputString;
  }
  
  function valida(){
    var arrayIndex = 0;
    NumCausa = -1;
    for(; arrayIndex < document.forms[0].rdoCausaDevCI.length; arrayIndex++){
         if (document.forms[0].rdoCausaDevCI[arrayIndex].checked=="1") {
            NumCausa = arrayIndex;
         }
    }
   return NumCausa;
  }


  function submitPrint(){
   var formName = document.forms[0];
   var cadImpresion = new String('PROTESTO2~');
   if ( valida() < 0) {
      alert("Error, Debe seleccionar una Causa.")
      return;
   }
   cadImpresion = cadImpresion + causas[valida()] + "~"
   cadImpresion = '../servlet/ventanilla.ImprimirServlet?txtCadImpresion=' + replaceWhiteSpaces(cadImpresion);
   top.openDialog(cadImpresion, 160, 120, 'top.setPrefs4()', document.forms[0]);
  }
 //-->
 </script>
</head>
<body bgcolor="#FFFFFF" onload="top.setFieldFocus(window.document);">
<br>
<form name="RechazoCI">
<h3>
<%out.print(session.getAttribute("page.cTxn") + "  " + (String)session.getAttribute("page.txnlabel"));%>
</h3>
<table border="0" cellspacing="0">
<%
    for(int i =  0; i < listaCampos.size(); i++){
  	  java.util.Vector vCampos = (java.util.Vector)listaCampos.get(i);
	  vCampos = (Vector)vCampos.get(0);
          out.println("<tr>");
	  out.println("  <td> <b>" + vCampos.get(2) + ": </b></td>");
          out.println("</tr> <tr> </tr>");
	  out.println("  <td width=\"10\">&nbsp;</td>");
	  for(k=0; k < listaContenidos.size(); k++){
	     Vector v1Campos = (Vector)listaContenidos.get(k);
	     for(int j = 0; j < v1Campos.size(); j++){
	         Vector v1Camposa = (Vector)v1Campos.get(j);
	         if(v1Camposa.get(0).toString().equals(vCampos.get(0).toString())) {
                   out.println("<tr>");
	           out.print  ("  <td><input type=\"radio\"" + " name=\"" + vCampos.get(0) + "\"" );
	           out.println("  value=\"" + v1Camposa.get(3) + "\">" + v1Camposa.get(2));
                   out.println("</tr>");
                 }
	       }
	      }
	   }
	%>
 </table>
 <p>
 <a href="javascript:submitPrint()"><img src="../ventanilla/imagenes/b_continuar.gif" border="0"></a>
 <input type="hidden" name="sucursal" value="<%= session.getAttribute("branch") %>">
 </form>
 </body>
</html>
