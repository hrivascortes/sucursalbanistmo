<!--
//*************************************************************************************************
//            Elemento: PageBuilderChqDev.jsp
//             Funcion: JSP para Cheques Devueltos. 
//          Creado por: Israel de Paz Mercado
//*************************************************************************************************
// CCN - 4360455 - 07/04/2005 - Se cra JSP para el esquema de cheques devueltos.
// CCN - 4360462 - 10/04/2005 - Se genera nuevo paquete por errores en paquete anterior
// CCN - 4360581 - 12/03/2007 - Se convierten los estilos en jsf y se hace un include en el jsp
//*************************************************************************************************
-->


<%@ page session="true" import="java.util.*"  %>

<%
response.setHeader("Cache-Control","no-store"); //HTTP 1.1
response.setHeader("Pragma","no-cache"); //HTTP 1.0
response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
<%@include file="style.jsf"%>
<script language="JavaScript">
function submite()
{
entry.submit();
}
</script></head>
<body bgcolor="#FFFFFF" onload="submite()">
<br>

<%
Hashtable datasession = (Hashtable)session.getAttribute("page.datasession");
if(datasession == null)
  datasession = new Hashtable();
String resp5353 = (String) datasession.get("resp5353");
  
if (resp5353 == null)
   resp5353="NO";
   else
   datasession.remove("resp5353");

if (resp5353.equals("SI")) 
   {
   out.print("<form name=\"entry\" action=\"../servlet/ventanilla.ChequeDev\" method=\"post\" ");
   }
   else
       {
        out.print("<form name=\"entry\" action=\"../../servlet/ventanilla.ChequeDev\" method=\"post\" ");
       }  

  %>
  
<input type="hidden" name="lstChqDev " value="<%= (String)datasession.get("lstChqDev") %>">
<input type="hidden" name="txtCodSeg" value="<%= (String)datasession.get("txtCodSeg") %>">
<input type="hidden" name="txtCveTran" value="<%= (String)datasession.get("txtCveTran") %>">
<input type="hidden" name="txtDDACuenta2" value="<%= (String)datasession.get("txtDDACuenta2") %>">
<input type="hidden" name="txtSerial2" value="<%= (String)datasession.get("txtSerial2") %>">
<input type="hidden" name="txtMontoCheque" value="<%= (String)datasession.get("txtMontoCheque") %>">
<input type="hidden" name="txtFirmaFun" value="<%= (String)datasession.get("txtFirmaFun") %>"> 
<%
datasession.remove("txtMontoCheque");
datasession.remove("txtCodSeg");
datasession.remove("txtCveTran");
datasession.remove("txtDDACuenta2");
datasession.remove("txtSerial2");
datasession.remove("txtFirmaFun");
session.setAttribute("page.datasession",datasession);
%>
</form>     
</body>
</html>
