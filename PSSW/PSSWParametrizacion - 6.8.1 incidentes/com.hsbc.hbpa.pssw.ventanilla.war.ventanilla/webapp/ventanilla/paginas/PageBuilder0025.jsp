<!--
//*************************************************************************************************
//             Funcion: JSP que presenta las opciones de la txns 0025
//            Elemento: PageBuilder0025.jsp
//          Creado por: Alejandro Gonzalez Castro
//      Modificado por: Marco Lara Palacios
//*************************************************************************************************
// CCN - 4360275 - 18/02/2005 - WAS 5.1
// CCN - 4360581 - 12/03/2007 - Se convierten los estilos en jsf y se hace un include en el jsp
//*************************************************************************************************
-->

<%@ page session="true" import="java.util.*"%>
<%
response.setHeader("Cache-Control","no-store"); //HTTP 1.1
response.setHeader("Pragma","no-cache"); //HTTP 1.0
response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<%
String MontoPro = new String("");
Vector listaCampos = (Vector)session.getAttribute("page.listcampos");
Vector listaContenidos = (Vector)session.getAttribute("page.listcont");
String txnAuthLevel = (String)session.getAttribute("page.txnAuthLevel");
String txnIMGAutoriz = (String)session.getAttribute("page.txnIMGAutoriz");
Vector v = (Vector)session.getAttribute("page.specialacct");
int k;
%>
<html>
<head>
  <%@include file="style.jsf"%>
  <title>Datos de la Transaccion</title>
<script language="JavaScript">
<!--
var formName = 'Mio';
var specialacct = new Array();

<%
if(!v.isEmpty())
{
  for(int iv = 0; iv < v.size(); iv++)
  {
    out.print("specialacct[" + iv + "]='" + v.get(iv) + "'" + '\n');
  }
}
%>

function validate(form)
{
<%
      out.println("if( !top.verifyDisk(window))");
      out.println("    return; ");
%>

  form.validateFLD.value = '1';
<%
 if(!txnAuthLevel.equals("1") && !txnIMGAutoriz.equals("1"))
 {
    out.println("  form.submit();");
    out.println("var obj = document.getElementById('btnCont');"); 
    out.println("obj.onclick = new Function('return false');");       
 }
%>
}


function AuthoandSignature(entry)
{
  var Cuenta;
  
  if(entry.validateFLD.value != '1')
    return;
    
  if(entry.needAutho.value == '1' && entry.AuthoOK.value != '1')
  {
    top.openDialog('../paginas/authoriz.jsp', 220, 130, 'top.setPrefs()', entry);
  }
  
  if(entry.needSignature.value == '1' && entry.SignField.value != '')
  {
    Cuenta = eval('entry.' + entry.SignField.value + '.value');
    top.openModalDialog('<%=request.getContextPath()%>/servlet/ventanilla.FirmasServlet?txtDDACuenta=' + Cuenta, 710, 430, 'top.setPrefs2()', entry);
  }
}
//-->
</script>
</head>
<body bgcolor="#FFFFFF" onload="top.setFieldFocus(window.document)">
<br>
<%
out.print("<form name=\"entry\" action=\"../servlet/ventanilla.DataServlet\" method=\"post\" ");

if(txnAuthLevel.equals("1"))
  out.print("onSubmit=\"return validate(this)\"");
  
out.println(">");
%>
<h1><%=session.getAttribute("page.txnImage")%></h1>
<h3>
<%
out.print(session.getAttribute("page.cTxn") + "  " + (String)session.getAttribute("page.txnlabel"));
%>
</h3>

<input type="hidden" name="ctacargo" value="0">
<input type="hidden" name="mtocargo" value="0">
<input type="hidden" name="nomcargo" value="0">
<input type="hidden" name="CurrPago" value="0">
 
<input type="hidden" name="montos1" value="0">
<input type="hidden" name="montos2" value="0">
<input type="hidden" name="montos3" value="0">
<input type="hidden" name="montos4" value="0">
<input type="hidden" name="montos5" value="0">
<input type="hidden" name="montos6" value="0">
<input type="hidden" name="montos7" value="0">
<input type="hidden" name="montos8" value="0">
<input type="hidden" name="montos9" value="0">
<input type="hidden" name="montos10" value="0">

<input type="hidden" name="nombres1" value="0">
<input type="hidden" name="nombres2" value="0">
<input type="hidden" name="nombres3" value="0">
<input type="hidden" name="nombres4" value="0">
<input type="hidden" name="nombres5" value="0">
<input type="hidden" name="nombres6" value="0">
<input type="hidden" name="nombres7" value="0">
<input type="hidden" name="nombres8" value="0">
<input type="hidden" name="nombres9" value="0">
<input type="hidden" name="nombres10" value="0">

<input type="hidden" name="apellidos1" value="0">
<input type="hidden" name="apellidos2" value="0">
<input type="hidden" name="apellidos3" value="0">
<input type="hidden" name="apellidos4" value="0">
<input type="hidden" name="apellidos5" value="0">
<input type="hidden" name="apellidos6" value="0">
<input type="hidden" name="apellidos7" value="0">
<input type="hidden" name="apellidos8" value="0">
<input type="hidden" name="apellidos9" value="0">
<input type="hidden" name="apellidos10" value="0">

<input type="hidden" name="iTxn" value="<%= session.getAttribute("page.iTxn") %>">
<input type="hidden" name="oTxn" value="<%= session.getAttribute("page.oTxn") %>">
<input type="hidden" name="cTxn" value="<%= session.getAttribute("page.cTxn") %>">
<input type="hidden" name="sucursal" value="<%= session.getAttribute("branch") %>">
<input type="hidden" name="teller" value="<%= session.getAttribute("teller") %>">
<input type="hidden" name="moneda" value="<%= session.getAttribute("page.moneda")%>">
<input type="hidden" name="registro" value="<%=session.getAttribute("empno")%>">
<input type="hidden" name="tllrlvl" value="<%=session.getAttribute("tellerlevel")%>">
<input type="hidden" name="supervisor" value="<%= session.getAttribute("teller") %>">
<input type="hidden" name="AuthoOK" value="0">
<input type="hidden" name="SignOK" value="0">
<input type="hidden" name="compania" value="20">
<input type="hidden" name="validateFLD" value="0">
<input type="hidden" name="needSignature" value="<%out.print(txnIMGAutoriz);%>">
<input type="hidden" name="needAutho" value="<%out.print(txnAuthLevel);%>">
<input type="hidden" name="SignField" value="<%= session.getAttribute("page.SignField") %>">
<p> 

<OBJECT classid="clsid:8AD9C840-044E-11D1-B3E9-00805F499D93" codebase="http://java.sun.com/products/plugin/1.2.2/jinstall-1_2_2-win.cab#Version=1,2,2,0" WIDTH ="350" HEIGHT = "50">
		<PARAM name="type" value="application/x-java-applet;version=1.2">
		<PARAM name="java_ARCHIVE" value="DiskOPMAS.jar">
		<PARAM name="java_CODE" value="DiskOPMAS">
		<PARAM name="java_NAME" value="opmas">
		<PARAM name="java_CODEBASE" value="../ventanilla/">
		<PARAM NAME="mayscript" VALUE="true">
</OBJECT>

<COMMENT>
<EMBED type="application/x-java-applet;version=1.2"
            java_ARCHIVE="DiskOPMAS.jar"
            java_CODE ="DiskOPMAS"
            java_NAME ="opmas"
            java_CODEBASE ="../ventanilla/"
            WIDTH ="350"
            HEIGHT = "50"
            MAYSCRIPT>
</EMBED>
</COMMENT>


<br><br>
<%
if(txnAuthLevel.equals("1") || txnIMGAutoriz.equals("1"))
  out.println("<a id='btnCont' href=\"javascript:validate(document.entry);javascript:AuthoandSignature(document.entry)\"><img src=\"../ventanilla/imagenes/b_aceptar.gif\" border=\"0\"></a>");
else
  out.println("<a id='btnCont' href=\"javascript:validate(document.entry)\"><img src=\"../ventanilla/imagenes/b_aceptar.gif\" border=\"0\"></a>");
%>
</form>
</body>
</html>
