<!--
//*************************************************************************************************
//            Elemento: PageBuilderSelectTotales.jsp
//             Funcion: JSP paraseleccion de totales
//          Creado por: Alejandro Gonzalez Castro
// Ultima Modificacion: 12/07/2004
//      Modificado por: Alejandro Gonzalez Castro
//*************************************************************************************************
// CCN -4360169- 12/07/2004 - Se corrige problema de impresion del Diario en sesion
// CCN -4360211- 08/10/2004 - Se selecciona totales en una sola pagina
// CCN - 4360581 - 12/03/2007 - Se convierten los estilos en jsf y se hace un include en el jsp
//*************************************************************************************************
-->
<%@page session="true"
import="java.util.*,ventanilla.com.bital.beans.Monedas"%>
<%
response.setHeader("Cache-Control","no-store"); //HTTP 1.1
response.setHeader("Pragma","no-cache"); //HTTP 1.0
response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
%>
<html>
<head>
   <title>Seleccion de Totales.</title>
	 <%@include file="style.jsf"%>
</head>
<body>
<%
    if(session.getAttribute("txtCadImpresion") != null)
        session.removeAttribute("txtCadImpresion");
 
session.setAttribute("page.getoTxnView","TotalesPCentral");
session.setAttribute("txnProcess","16");
%>
<Script language="JavaScript">
<!--
function Continuar(f)
{
  var seleccion = f.typetotal[f.typetotal.selectedIndex].value;
  if (seleccion == "0011")
  {
    f.iTxn.value = "0011";
    f.oTxn.value = "0011";
    f.cTxn.value = "0011";
  }
  else
{
    f.iTxn.value = "M005";
    f.oTxn.value = "0013";
    f.cTxn.value = "0013";
}
  f.submit();
}
//-->
</Script>
<h1>Totales</h1>
<form name="totales" action="<%=request.getContextPath()%>/servlet/ventanilla.DataServlet" method="post">
 <table>
     <tr><td>Tipo totales:</td>
       <td><select name="typetotal" size="1">
           <option value="0011" selected>Totales del Cajero</option>
           <option value="0013">Totales de Sucursal</option>
           </select>
       </td>
     </tr>
  <tr><td>Moneda:</td>
  <td><select name="moneda">
  <%
     Monedas monedas = Monedas.getInstance();
     java.util.Vector vMonedas = monedas.getMonedas();
     int iMon = 0;
     for(iMon=0;iMon<vMonedas.size();iMon++)
     {
     		out.println("<option value=\""+(String)((Vector)vMonedas.elementAt(iMon)).elementAt(0)+"\">"+(String)((Vector)vMonedas.elementAt(iMon)).elementAt(1));	
     		//System.out.println((String)((Vector)vMonedas.elementAt(iMon)).elementAt(0));
     }
   %>
  </select></td>
  </tr>
<input type="hidden" name="iTxn">
<input type="hidden" name="oTxn">
<input type="hidden" name="cTxn">
<input type="hidden" name="sucursal" value="<%= session.getAttribute("branch") %>">
<input type="hidden" name="teller" value="<%= session.getAttribute("teller") %>">
<input type="hidden" name="registro" value="<%=session.getAttribute("empno")%>">
<input type="hidden" name="tllrlvl" value="<%=session.getAttribute("tellerlevel")%>">
<input type="hidden" name="supervisor" value="<%= session.getAttribute("teller") %>">
<input type="hidden" name="AuthoOK" value="0">
<input type="hidden" name="SignOK" value="0">
<input type="hidden" name="validateFLD" value="0">
<input type="hidden" name="needSignature" value="0">
<input type="hidden" name="needAutho" value="0">
<input type="hidden" name="SignField" value="">
<tr><td><a href="javascript:Continuar(document.totales)" ><img src="../imagenes/b_continuar.gif" border="0"></a></td></tr>
</table>
</form>

</body>
</html>

