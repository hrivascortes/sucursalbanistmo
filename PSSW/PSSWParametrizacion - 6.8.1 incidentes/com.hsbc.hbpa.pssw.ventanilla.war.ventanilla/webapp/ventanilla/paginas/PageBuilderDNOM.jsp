<!--
//*************************************************************************************************
//             Funcion: JSP que presenta las opciones de la txn DNOM
//            Elemento: PageBuilderDNOM.jsp
//          Creado por: Alejandro Gonzalez Castro
//      Modificado por: Marco Lara Palacios
//*************************************************************************************************
// CCN - 4360275 - 18/02/2005 - WAS 5.1
// CCN - 4360581 - 12/03/2007 - Se convierten los estilos en jsf y se hace un include en el jsp
//*************************************************************************************************
-->

<%@ page session="true" import="java.util.*"%>
<%
response.setHeader("Cache-Control","no-store"); //HTTP 1.1
response.setHeader("Pragma","no-cache"); //HTTP 1.0
response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<%
String MontoPro = new String("");
Vector listaCampos = (Vector)session.getAttribute("page.listcampos");
Vector listaContenidos = (Vector)session.getAttribute("page.listcont");
String txnAuthLevel = (String)session.getAttribute("page.txnAuthLevel");
String txnIMGAutoriz = (String)session.getAttribute("page.txnIMGAutoriz");
Vector v = (Vector)session.getAttribute("page.specialacct");
int k=0;
%>
<html>
<head>
  <%@include file="style.jsf"%>
  <title>Datos de la Transaccion</title>
<script type="text/javascript" language="JavaScript">
<!--
var formName = 'Mio'
var specialacct = new Array()
<%
 if(!v.isEmpty()){
  for(int iv = 0; iv < v.size(); iv++){
   out.print("specialacct[" + iv + "]='" + v.get(iv) + "'" + '\n');
  }
 }
%>
function validate(form)
{
<% if ( session.getAttribute("page.iTxn").toString().equals("M007")) {%>
 if  (form.txtCveTran.value.length == 0 || form.txtDDACuenta2.value.length == 0 || form.txtSerial2.value.length == 0 ) {
      alert("Introduzca Banda del Cheque.")
      return;
 }
 if (!top.validate(window, document.entry.txtCodSeg, 'ChequeD'))
 return
 if (!top.validate(window, document.entry.txtCveTran, 'ChequeD'))
 return
 if (!top.validate(window, document.entry.txtDDACuenta2, 'ChequeD'))
 return
 if (!top.validate(window, document.entry.txtSerial2, 'ChequeD'))
 return
<% } %>

<%
      out.println("if( !top.verifynom(window))");
      out.println("    return ");
%>

  form.validateFLD.value = '1'
<% if(!txnAuthLevel.equals("1") && !txnIMGAutoriz.equals("1"))
	{
    out.println("  form.submit();");
    out.println("var obj = document.getElementById('btnCont');"); 
    out.println("obj.onclick = new Function('return false');");   
	}   
%>
}
function AuthoandSignature(entry)
{
 var Cuenta;
 if(entry.validateFLD.value != '1')
  return
  

  
  if (entry.iTxn.value == 'M007')	
 	{
 	CveTransito=entry.txtCveTran.value;
 	}

 if(entry.needAutho.value == '1' && entry.AuthoOK.value != '1')
 {
 		if (entry.iTxn.value == 'M007')
  		    top.openDialog('../paginas/authoriz.jsp', 220, 130, 'top.setPrefs15()', entry)
  		else
  			top.openDialog('../paginas/authoriz.jsp', 220, 130, 'top.setPrefs()', entry)
 }

 if(entry.needSignature.value == '1' && entry.SignField.value != '')
 {
  Cuenta = eval('entry.' + entry.SignField.value + '.value')
	if (entry.iTxn.value == 'M007')	
  		top.openModalDialog('<%=request.getContextPath()%>/servlet/ventanilla.FirmasServlet?txtDDACuenta=' + Cuenta+'&txtCveTran='+CveTransito, 710, 430, 'top.setPrefs2()', entry)
  	else
  		top.openModalDialog('<%=request.getContextPath()%>/servlet/ventanilla.FirmasServlet?txtDDACuenta=' + Cuenta, 710, 430, 'top.setPrefs2()', entry)
 }
}
//-->
</script>
</head>
<body bgcolor="#FFFFFF" onload="top.setFieldFocus(window.document)">
<br>
<% out.print("<form name=\"entry\" action=\"../servlet/ventanilla.DataServlet\" method=\"post\" ");
   if(txnAuthLevel.equals("1"))
    out.print("onSubmit=\"return validate(this)\"");
   out.println(">");
%>

<h1><%=session.getAttribute("page.txnImage")%></h1>

<h3>
<%out.print(session.getAttribute("page.cTxn") + "  " +
                (String)session.getAttribute("page.txnlabel"));%>
</h3>

<% String txn = session.getAttribute("page.cTxn").toString();
   if (txn.equals("M007"))
   {%>
		<table border="0" cellspacing="0">
		<tr><td rowspan="3" valign=top><b>Cargo :</b></td><td colspan="9">&nbsp;</td></tr>
		<tr align="center">
		<td colspan="2" >&nbsp;</td>
		<!--
		<td>C&oacute;digo de Seg.</td><td width="10">&nbsp;</td>
		-->
		<td>&nbsp;</td><td width="10">&nbsp;</td>
		<td>Cve. de Tr&aacute;nsito</td><td width="10">&nbsp;</td>
		<td>Cuenta</td><td width="10">&nbsp;</td>
		<td>Serial</td>
		</tr>
		<tr align="center">
		<td>Banda :</td><td width="20">&nbsp;</td>
		<td>
		<%
		   Vector vCampos = (Vector)listaCampos.get(0);
		   vCampos = (Vector)vCampos.get(0);
		   out.print("<input size=\"" + vCampos.get(3) + "\" name=\"" + vCampos.get(0) +"\" maxlength=\"" + 35 +"\" type=\"text\" tabindex=\"" + (++k) + "\"");
		   if( vCampos.get(3).toString().length() > 0 )
		    out.print("  onBlur=\"top.estaVacio(this)||top.validate(window, this, '" + vCampos.get(4) +"')\"  onKeyPress=\"top.keyPressedHandler(window, this, '" + vCampos.get(4) +"')\">");
		   else
		   {
    			out.print(">");
		   }
		%>
		</td><td width="10">&nbsp;</td>
		<td>
		<%
			vCampos = (Vector)listaCampos.get(1);
		   	vCampos = (Vector)vCampos.get(0);
   			out.print("<input size=\"" + vCampos.get(3) + "\" name=\"" + vCampos.get(0) +"\" maxlength=\"" + 35 +"\" type=\"text\" tabindex=\"" + (++k) + "\"");
		   	if( vCampos.get(3).toString().length() > 0 )
    			out.print("  onBlur=\"top.estaVacio(this)||top.validate(window, this, '" + vCampos.get(4) +"')\"  onKeyPress=\"top.keyPressedHandler(window, this, '" + vCampos.get(4) +"')\">");
   			else
   			{
			    out.print(">");
   			}
		%>
		</td><td width="10">&nbsp;</td>
		<td>
		<%
   			vCampos = (Vector)listaCampos.get(2);
   			vCampos = (Vector)vCampos.get(0);
   			out.print("<input size=\"" + vCampos.get(3) + "\" name=\"" + vCampos.get(0) +"\" maxlength=\"" + 35 +"\" type=\"text\" tabindex=\"" + (++k) + "\"");
   			if( vCampos.get(3).toString().length() > 0 )
    			out.print("  onBlur=\"top.estaVacio(this)||top.validate(window, this, '" + vCampos.get(4) +"')\"  onKeyPress=\"top.keyPressedHandler(window, this, '" + vCampos.get(4) +"')\">");
   			else
   			{
    			out.print(">");
   			}
		%>
		</td><td width="10">&nbsp;</td>
		<td>
		<%
   			vCampos = (Vector)listaCampos.get(3);
   			vCampos = (Vector)vCampos.get(0);
   			out.print("<input size=\"" + vCampos.get(3) + "\" name=\"" + vCampos.get(0) +"\" maxlength=\"" + 35 +"\" type=\"text\" tabindex=\"" + (++k) + "\"");
   			if( vCampos.get(3).toString().length() > 0 )
    			out.print("  onBlur=\"top.estaVacio(this)||top.validate(window, this, '" + vCampos.get(4) +"')\"  onKeyPress=\"top.keyPressedHandler(window, this, '" + vCampos.get(4) +"')\">");
   			else
   			{
    			out.print(">");
   			}
		%>
		</td>
		</tr>
		</table>
<%
}%>
<%java.util.Hashtable numDep = (java.util.Hashtable)session.getAttribute("branchinfo");%>
<input type="hidden" name="depnom" value="<%= numDep.get("N_DEP_NOMINA").toString() %>">
<input type="hidden" name="codseg" value="0">
<input type="hidden" name="cvetran" value="0">
<input type="hidden" name="ctacargo" value="0">
<input type="hidden" name="serial" value="0">
<input type="hidden" name="Scuentas1" value="0">
<input type="hidden" name="Scuentas2" value="0">
<input type="hidden" name="Scuentas3" value="0">
<input type="hidden" name="Scuentas4" value="0">
<input type="hidden" name="Scuentas5" value="0">
<input type="hidden" name="Smontos1" value="0">
<input type="hidden" name="Smontos2" value="0">
<input type="hidden" name="Smontos3" value="0">
<input type="hidden" name="Smontos4" value="0">
<input type="hidden" name="Smontos5" value="0">
<!--campos para nuevo proceso de nomina sin threads-->
<input type="hidden" name="firsttime" value="si">
<input type="hidden" name="numtotnom" value="0">
<!--campos para nuevo proceso de nomina sin threads-->

<input type="hidden" name="iTxn" value="<%= session.getAttribute("page.iTxn") %>">
<input type="hidden" name="oTxn" value="<%= session.getAttribute("page.oTxn") %>">
<input type="hidden" name="cTxn" value="<%= session.getAttribute("page.cTxn") %>">
<input type="hidden" name="sucursal" value="<%= session.getAttribute("branch") %>">
<input type="hidden" name="teller" value="<%= session.getAttribute("teller") %>">
<input type="hidden" name="moneda" value="<%= session.getAttribute("page.moneda")%>">
<input type="hidden" name="registro" value="<%=session.getAttribute("empno")%>">
<input type="hidden" name="tllrlvl" value="<%=session.getAttribute("tellerlevel")%>">
<input type="hidden" name="supervisor" value="<%= session.getAttribute("teller") %>">
<input type="hidden" name="AuthoOK" value="0">
<input type="hidden" name="compania" value="20">
<input type="hidden" name="SignOK" value="0">
<input type="hidden" name="validateFLD" value="0">
<input type="hidden" name="needSignature" value="<%out.print(txnIMGAutoriz);%>">
<input type="hidden" name="needAutho" value="<%out.print(txnAuthLevel);%>">
<input type="hidden" name="SignField" value="<%= session.getAttribute("page.SignField") %>">
<p>
<OBJECT classid="clsid:8AD9C840-044E-11D1-B3E9-00805F499D93" codebase="http://java.sun.com/products/plugin/1.2.2/jinstall-1_2_2-win.cab#Version=1,2,2,0" WIDTH ="350" HEIGHT = "50">
		<PARAM name="type" value="application/x-java-applet;version=1.2">
		<PARAM name="java_ARCHIVE" value="DiskNom.jar">
		<PARAM name="java_CODE" value="DiskNOM">
		<PARAM name="java_NAME" value="nomina">
		<PARAM name="java_CODEBASE" value="../ventanilla/">
		<PARAM NAME="mayscript" VALUE="true">
		<COMMENT>
<EMBED type="application/x-java-applet;version=1.2"
            java_ARCHIVE="DiskNom.jar"
            java_CODE ="DiskNOM"
            java_NAME ="nomina"
            java_CODEBASE ="../ventanilla/"
            WIDTH ="350"
            HEIGHT = "50"
            MAYSCRIPT>
</EMBED>
		</COMMENT>
</OBJECT>
<br><br>
<% if(txnAuthLevel.equals("1") || txnIMGAutoriz.equals("1"))
    out.println("<a id='btnCont' href=\"javascript:validate(document.entry);javascript:AuthoandSignature(document.entry)\"><img src=\"../ventanilla/imagenes/b_continuar.gif\" border=\"0\"></a>");
   else
    out.println("<a id='btnCont' href=\"javascript:validate(document.entry)\"><img src=\"../ventanilla/imagenes/b_continuar.gif\" border=\"0\"></a>");
%>
</form>
</body>
</html>
