<!-- 157 -->

<%@ page session="true" import="java.util.Hashtable"%>
<%
response.setHeader("Cache-Control","no-store"); //HTTP 1.1
response.setHeader("Pragma","no-cache"); //HTTP 1.0
response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<META HTTP-EQUIV="REFRESH" content="5">
<html>
<head>
<style type="text/css">Body {font-family: Arial, Helv; font-size: 10pt; font-weight: bold; color:#000000}</style>
  <title>Results</title>
</head>
<Script language="JavaScript">
function chgPag()
{
	document.rap5503.submit();
}
</Script>
<body>
<%
	String status = "*";
	out.println("<br>Procesando RAP, por favor espere...");
	Hashtable datos = (Hashtable)session.getAttribute("page.datasession");

	Integer pag = (Integer)datos.get("pago");
	Integer serv  = (Integer)datos.get("servicio");
	String codet = (String)datos.get("codetxn");
	if(!(codet.equals("X")))
	{
		if (codet.equals("0"))
			status = "Aceptado";
		else
			status = "Rechazado";
		out.println("<br><br>Procesando Servicio : " + serv + " / Pago No. " + pag + " - "+ status );
	}


	String cend = (String)datos.get("end");
	if(!(cend.equals("X")))
	{
		if(cend.equals("1"))
		{
			out.println("<br><br>Proceso terminado...");
%>
			<form name="rap5503" action="Response55031.jsp" method="Post">
				<input type="hidden" name="inpSubmit">
			</form>
			<script language="JavaScript" >
				chgPag();
			</script>
<%		}
	}
%>
</body>
</html>
