<!--
//*************************************************************************************************
//            Elemento: Help.jsp
//             Funcion: JSP para seleccion de ayuda
//          Creado por: Israel De Paz Mercado
// Ultima Modificacion: 16/02/2005
//      Modificado por: Israel De Paz Mercado
//*************************************************************************************************
// CCN - 43602314 - 06/05/2005 - Se crea jsp para la seleccion de ayuda.
// CCN - 4360581 - 12/03/2007 - Se convierten los estilos en jsf y se hace un include en el jsp
//*************************************************************************************************
-->
<%@ page session="true"%>
<% 
response.setHeader("Cache-Control","no-store"); //HTTP 1.1
response.setHeader("Pragma","no-cache"); //HTTP 1.0
response.setDateHeader ("Expires", 0); //prevents caching at the proxy server



session.setAttribute("page.getoTxnView","ResponseMotivator");
session.setAttribute("txnProcess","16");
session.setAttribute("FlagSelectMot","0");
%>


<html>
<head>
   <title>Ayuda.</title>
   <%@include file="style.jsf"%>
<script language="JavaScript">

function envia()
{
    if(document.forms[0].typehelp.value == 1)
      {
       document.forms[0].action="ResponseCatTxns.jsp";
       document.forms[0].iTxn.value = "";
       document.forms[0].oTxn.value = "";
       document.forms[0].cTxn.value = "";
      }
      
    if(document.forms[0].typehelp.value == 2)
      {
       document.forms[0].action="ResponseMotivator.jsp";
      } 
      

document.forms[0].submit();
      
}

</script>
</head>
<body>


<h1>Ayuda</h1>
<form name="help" action="../../servlet/ventanilla.DataServlet" method="post">
 <table>
     <tr><td>Selecci&oacute;n:</td>
       <td><select name="typehelp" size="1">
           <option value="1" selected>Catalogo de Transacciones de PSSW</option>
           <option value="2">Catalogo de Transacciones de Motivator</option>
           <option value="3">Transacción Realizadas por el Ejecutivo</option>
           </select>
       </td>
     </tr>
  
<tr><td><a href="JavaScript:envia();" ><br><img src="../imagenes/b_continuar.gif" border="0"></a></td></tr>
</table>
	<input type="hidden" name="iTxn" value="0093">
	<input type="hidden" name="oTxn" value="0093">
	<input type="hidden" name="cTxn" value="0093">
	<input type="hidden" name="sucursal" value="<%= session.getAttribute("branch") %>">
	<input type="hidden" name="teller" value="<%= session.getAttribute("teller") %>">
	<input type="hidden" name="registro" value="<%=session.getAttribute("empno")%>">
	<input type="hidden" name="tllrlvl" value="<%=session.getAttribute("tellerlevel")%>">
	<input type="hidden" name="supervisor" value="<%= session.getAttribute("teller") %>">
</form>

</body>
</html>
