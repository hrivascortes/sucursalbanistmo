<!--
//*************************************************************************************************
//             Funcion: JSP que despliega respuesta de WU
//            Elemento: ResponseWUDO.jsp
//          Creado por: Alejandro Gonzalez Castro
//      Modificado por: Rutilo Zarco Reyes
//*************************************************************************************************
// CCN - 4360189 - 03/09/2004 - Inicio WU
// CCN - 4360383 - 26/09/2005 - Se elimina doble click
// CCN - 4360581 - 12/03/2007 - Se convierten los estilos en jsf y se hace un include en el jsp
//*************************************************************************************************
-->

<%@ page session="true" import="java.util.*"%>
<%
response.setHeader("Cache-Control","no-store"); //HTTP 1.1
response.setHeader("Pragma","no-cache"); //HTTP 1.0
response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<%
Hashtable data = new Hashtable();
data = (Hashtable)session.getAttribute("WUdata");
%>

<html>
<head>
  <title>Certificación</title>
	<%@include file="style.jsf"%>
<script   language="JavaScript">
function ok(form)
{
    form.WUaction.value="pagar";
    form.submit();
   var obj = document.getElementById('btnCont');
   obj.onclick = new Function('return false');      
}
function can(form)
{
    form.WUaction.value="canorden";
    form.submit();
   var obj = document.getElementById('btnCont');
   obj.onclick = new Function('return false');      
}
</script>
</head>
<body bgcolor="#FFFFFF" onload="top.setFieldFocus(window.document)">
<form name="entry" action="../../servlet/ventanilla.Group02WU" method="post">
<table border="0">
<tr>
    <td colspan="6" align="center"><b>Detalle de la Transferencia</b></td>
</tr>
<tr><td colspan="3"><b>Datos del Beneficiario</b></td></tr>
<tr><td align="right">Nombre :</td><td>&nbsp;&nbsp;</td><td><%=(String)data.get("ben")%></td></tr>
<tr><td align="right">MTCN :</td><td>&nbsp;&nbsp;</td><td><%=(String)data.get("mtcn")%></td></tr>
<tr><td colspan="3"><b>Datos del Ordenante</b></td></tr>
<tr><td align="right">Nombre :</td><td>&nbsp;&nbsp;</td><td><%=(String)data.get("ord")%></td></tr>
<tr><td align="right">Direccion :</td><td>&nbsp;&nbsp;</td><td><%=(String)data.get("dir")%></td></tr>
<tr><td align="right">Ciudad :</td><td>&nbsp;&nbsp;</td><td><%=(String)data.get("ciudad")%></td></tr>
<tr><td align="right">Estado y C.P. :</td><td>&nbsp;&nbsp;</td><td><%=(String)data.get("estado")%></td></tr>
<tr><td align="right">Tel&eacute;fono :</td><td>&nbsp;&nbsp;</td><td><%=(String)data.get("tel")%></td></tr>
<tr><td align="right">Fecha Colocaci&oacute;n :</td><td>&nbsp;&nbsp;</td><td><%=(String)data.get("fecha")%></td></tr>
<tr><td align="right">Hora Colocaci&oacute;n :</td><td>&nbsp;&nbsp;</td><td><%=(String)data.get("hora")%></td></tr>
<tr><td align="right">Tipo de Cambio W.U. :</td><td>&nbsp;&nbsp;</td><td><%=(String)data.get("tipocambio")%></td></tr>
<tr><td align="right">Moneda :</td><td>&nbsp;&nbsp;</td><td><%=(String)data.get("moneda")%></td></tr>
<tr><td align="right">Contrase&ntilde;a :</td><td>&nbsp;&nbsp;</td><td><%=(String)data.get("contra")%></td></tr>
<tr><td align="right">Importe USD :</td><td>&nbsp;&nbsp;</td><td><%=(String)data.get("mtoUS")%></td></tr>
<tr><td align="right">Importe M.N. :</td><td>&nbsp;&nbsp;</td><td><%=(String)data.get("mto")%></td></tr>
<tr><td colspan="3">Capture el N&uacute;mero de Cuenta, si el pago es con Abono a Cuenta</td></tr>
<tr><td align="right">Cuenta :</td><td>&nbsp;&nbsp;</td><td><input type="text" name="txtCuenta" size="10" maxlength="10" /></td></tr>
</table>
<p>
<input type="hidden" name="WUaction" value="">
<a id='btnCont' href="javascript:ok(document.entry);"><img src="../imagenes/b_continuar.gif" border="0"></a>
<a id='btnCont' href="javascript:can(document.entry);"><img src="../imagenes/b_cancelar.gif" border="0"></a>
</form>
</body>
</html>
