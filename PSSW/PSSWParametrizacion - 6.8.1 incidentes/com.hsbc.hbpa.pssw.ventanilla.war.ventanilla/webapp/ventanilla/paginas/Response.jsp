<!--
//*************************************************************************************************
///            Funcion: JSP que despliegarespuesta de txns
//            Elemento: Response.jsp
//          Creado por: Alejandro Gonzalez Castro
//      Modificado por: Fausto Rodrigo Flores Moreno
//*************************************************************************************************
// CCN - 4360172 - 22/07/2004 - Se modifica size de ventana de impresion
// CCN - 4360211 - 08/10/2004 - Se elimina mensaje de documentos pendientes por digitalizar
// CCN - 4360275 - 18/02/2005 - WAS 5.1
// CCN - 4360368 - 02/09/2005 - Adecuacion para Depto 9024 txn 1065
// CCN - 4360374 - 08/09/2005 - Se genera nuevo paquete a peticion de QA.
// CCN - 4360493 - 23/06/2006 - Se realizan modificaciones para mostrar el nombre del proveedor en la respuesta de la txn 1053
//*************************************************************************************************
-->

<%@ page session="true" import="java.util.*"%>
<%
response.setHeader("Cache-Control","no-store"); //HTTP 1.1
response.setHeader("Pragma","no-cache"); //HTTP 1.0
response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
Hashtable datasession = (Hashtable)session.getAttribute("page.datasession");
String creditoKronner = (String)session.getAttribute("creditoKronner");
String P002=null;
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<%!
  private String getString(String s)
  {
   int len = s.length();
   for(; len>0; len--)
    if(s.charAt(len-1) != ' ')
     break;
   return s.substring(0, len);
  }

  private String getItem(String newCadNum, int newOption)
  {
   int iPIndex = newCadNum.indexOf("CONSECUTIVO:") + 12, NumSaltos, Num = 0, Ban = 0;
   String nCadNum = new String("");
   NumSaltos = newOption;
   while(Num < NumSaltos)
   {
    if(newCadNum.charAt(iPIndex) == 0x20)
    {
     while(newCadNum.charAt(iPIndex) == 0x20)
      iPIndex++;
     Num++;
    }
    else
     while(newCadNum.charAt(iPIndex) != 0x20)
      iPIndex++;
   }
   while(newCadNum.charAt(iPIndex) != 0x20 && iPIndex < newCadNum.length()){
    nCadNum = nCadNum + newCadNum.charAt(iPIndex);
    if(iPIndex < newCadNum.length())
      iPIndex++;
    if(iPIndex == newCadNum.length())
      break;
   }

   return nCadNum;
  }
  
  private String getNombreSIBDDA(String newCadNum, int newOption)
  {
   int iPIndex = newCadNum.indexOf("CONSECUTIVO:") + 12, NumSaltos, Num = 0, Ban = 0;
   String nCadNum = new String("");
   NumSaltos = newOption;
   while(Num < NumSaltos)
   {
    if(newCadNum.charAt(iPIndex) == 0x20)
    {
     while(newCadNum.charAt(iPIndex) == 0x20)
      iPIndex++;
     Num++;
    }
    else
     while(newCadNum.charAt(iPIndex) != 0x20)
      iPIndex++;
   }
   nCadNum=newCadNum.substring(iPIndex,newCadNum.length());
   return nCadNum;
  }
  
    private String getNombreSIBCDA(String newCadNum, int newOption, String cta)
  {
   //System.out.println(newCadNum);
   int inicio = newCadNum.indexOf(cta) + 15;
   int fin = newCadNum.indexOf("MONEDA");
   String nCadNum = new String("");
   nCadNum = newCadNum.substring(inicio,fin);
   //System.out.println(nCadNum);
   return nCadNum.trim();
  }
  
  private String getProveedor(String ClaveProv)//SE OBTINENE EL NOMBRE DEL PROVEEDOR
  {
  	boolean find=false;
  	int i = 0;
  	String Proveedor = "";
    ventanilla.com.bital.beans.DatosVarios rap = ventanilla.com.bital.beans.DatosVarios.getInstance();
    Vector vecProvCHQ = new Vector();        
    vecProvCHQ =(Vector)rap.getDatosV("PROVCHQ");

  	while(!find && i<vecProvCHQ.size()){
  		StringTokenizer dato = new StringTokenizer(vecProvCHQ.elementAt(i).toString(),"*");

  		if(ClaveProv.equals(dato.nextToken().toString()))
  		{
  			find = true;
			Proveedor = dato.nextToken();
  		}
  		else
  		{
  			dato.nextToken();
  		}
  		i++;
  		dato=null;
  	}	
  	return(Proveedor);
  	
  }
  

%>
<%
Vector resp = (Vector)session.getAttribute("response");
String estatus = (String)resp.elementAt(0);
%>
<html>
<head>
  <title>Certificación</title>
<style type="text/css">Body {font-family: Arial, Helv; font-size: 10pt; font-weight: bold; color:#000000}</style>
<script language="javascript">
<!--
  sharedData = '0';

  function formSubmitter(){
    clearInterval(formSubmitter);
    if(sharedData != '0')
    {
      document.forms[0].submit()
      sharedData = '0';
    }
  }
//-->
</script>
</head>
<body bgcolor="#FFFFFF" onload="top.setFieldFocus(window.document);setInterval('formSubmitter()',2000)">
<p>
<%
  java.util.Stack flujotxn = new java.util.Stack();
  String codigo = "";
  String needOverride = "NO";
  String txn = session.getAttribute("page.cTxn").toString();
 
  if(session.getAttribute("page.flujotxn") != null)
  	flujotxn = (java.util.Stack)session.getAttribute("page.flujotxn");
  	
  if(txn.equals("4523"))
  {
  	if(estatus.equals("0"))
  	out.println("-* TRANSACCION ACEPTADA *-<br><br>");
  }
    
  if( resp.size() != 4 ){
  	if(txn.equals("1003") || txn.equals("P002") ){
  		String respKronner=(String)session.getAttribute("respKronner");
  		if(respKronner != null){
  			out.println("<b>EL Crédito Kronner no Existe....</b><br>");
  		}else{
  		 out.println("<b>Error de conexi&oacute;n con Kronner</b><br>");
  		 }
  	}
  	else{
   		out.println("<b>Error de conexi&oacute;n</b><br>");
   	}
  }
  else
  {
   	try
   	{
		codigo = (String)resp.elementAt(0);
	 	if( codigo.equals("1"))
	   	   out.println("<b>Transaccion Rechazada</b><br><br>");
	    if( codigo.equals("0") || codigo.equals("1") ){
	    	int lines = Integer.parseInt((String)resp.elementAt(1));
	    	String szResp = (String)resp.elementAt(3);
			if(txn.equals("1053")){
		    	if(szResp.length()>285){//Si es mayor se asegura que puede estar la clave del proveedor
	        		out.println(getProveedor(szResp.substring(285,288))+"<p>");
	        		session.setAttribute("PROVCHQ",getProveedor(szResp.substring(285,288)));
	        	}
	        }
	        if(txn.equals("ISIB")){
	        	String tipoCta="";
	        	tipoCta=(String)datasession.get("lstCuentaSIB");
				if(codigo.equals("0") && tipoCta.equals("02"))
				{
					//datasession.put("txtNomCte",getItem((String)resp.elementAt(3), 2) +" "+getItem((String)resp.elementAt(3), 3));
					datasession.put("txtNomCte",getNombreSIBDDA((String)resp.elementAt(3), 2));
					session.setAttribute("page.datasession",datasession);
				}else if(codigo.equals("0") && tipoCta.equals("03")){
					datasession.put("txtNomCte",getNombreSIBCDA((String)resp.elementAt(3), 2, (String)datasession.get("txtCtaSIB")));
					session.setAttribute("page.datasession",datasession);
				}
				else
				{
					session.setAttribute("page.txtNomCte", "");
				}
			}
//INI: CAMBIOSNOMBRECORTO
                if (txn.equals("0080")) {
                    if (codigo.equals("0")) {
                        datasession.put("txtCliente",getNombreSIBDDA((String)resp.elementAt(3), 2));
                        session.setAttribute("page.datasession",datasession);
                    } else {
                        session.setAttribute("page.txtCliente", "");
                    }
                }
//FIN: CAMBIOSNOMBRECORTO
	    	for(int i=0; i<lines; i++){
	    		   String line = szResp.substring( i*78, Math.min(i*78+77, szResp.length()) );
	     		   out.println(getString(line) + "<p>");
	     		   if(Math.min(i*78+77, szResp.length()) == szResp.length())
	      		    break;
	    	}
		}
	    else{
	    	if ( codigo.equals("3") || codigo.equals("2"))
	        	out.println("<b>Transacci&oacute;n Requiere Autorizaci&oacute;n</b>");
	        else
	            out.println("<b>Ocurri&oacute; un Error de Comunicaci&oacute;n, Verifique.</b>");
		}
  	}
  	catch(NumberFormatException nfe){
     out.println("<b>Error de conexi&oacute;n</b><br>");
    }
  }
%>
<p>
<form name="Txns" action="../../servlet/ventanilla.PageServlet" method="post">
<table border="0" cellspacing="0">
<tr>
<td>
</td>
</tr>
<tr>
<td>
<%
  String isCertificable = "";
  if(session.getAttribute("page.certifField") != null)
    isCertificable = (String)session.getAttribute("page.certifField");  // Para Certificación
	
	String Suc = session.getAttribute("branch").toString();

  String txtCadImpresion  = ""; // Para Certificación
  
  
  if(txn.equals("1523") && Suc.equals("09024"))
	isCertificable = "N";  	
	

  if(txn.equals("0061") && codigo.equals("1")){
  	if(!flujotxn.empty())
  		  	flujotxn.removeAllElements();
  }


  if(!flujotxn.empty())
  {
    out.print("<div align=\"center\">");
    
    if(!isCertificable.equals("N")) // Para Certificación
    {
      session.setAttribute("Consecutivo", getItem((String)resp.elementAt(3), 1)); // Para Certificación
      session.setAttribute("Tipo", isCertificable); // Para Certificación
      
      if(codigo.equals("0"))
        out.println("<script language=\"javascript\">top.openDialog('../../servlet/ventanilla.ImprimirServlet?txtCadImpresion=" + txtCadImpresion + "', 160, 120, 'top.setPrefs4()', document.Txns)</script>"); // Para Certificación
      else
      {
        if ( codigo.equals("3") || codigo.equals("2")) 
        {
	  needOverride = "SI";
	  out.print("<script languaje=\"javascript\"> top.openDialog('authoriz.jsp', 220, 130, 'top.setPrefs8()', document.Txns) </script>");
	}
	else
	  out.println("<a href=\"javascript:top.openDialog('../../servlet/ventanilla.ImprimirServlet?txtCadImpresion=" + txtCadImpresion + "', 160, 120, 'top.setPrefs4()', document.Txns)\"><img src=\"imagenes/b_continuar.gif\" border=\"0\"></a>"); // Para Certificación
      }
    }
    else
    {
      if(codigo.equals("0"))
      {
	out.println("<script language=\"javascript\">sharedData = '1'</script>");
	out.println("<font color='red'>procesando...</font>");
      }
      else 
      {
        if ( codigo.equals("3") || codigo.equals("2")) 
        {
          needOverride = "SI";
          out.print("<script languaje=\"javascript\"> top.openDialog('authoriz.jsp', 220, 130, 'top.setPrefs8()', document.Txns) </script>");
        }
        else
      	  out.print("<input type=\"image\" src=\"../imagenes/b_aceptar.gif\" alt=\"\" border=\"0\">");
      }
    }
    
    out.print("</div>");
  }
  else
  {
    if(!isCertificable.equals("N")) // Para Certificación
    {
      session.setAttribute("Consecutivo", getItem((String)resp.elementAt(3), 1)); // Para Certificación
      session.setAttribute("Tipo", isCertificable); // Para Certificación
      
      if ( codigo.equals("3") || codigo.equals("2")) 
      {
	needOverride = "SI";
	out.print("<script languaje=\"javascript\"> top.openDialog('authoriz.jsp', 220, 130, 'top.setPrefs8()', document.Txns) </script>");
      }
      else
      {
	if (txn.equals("0096") && session.getAttribute("page.iTxn").toString().equals("4009") && codigo.equals("1")) //Transaccion 4009
        { 
          out.println("<script language=\"javascript\">alert('Transacción Rechazada.')</script>");
        }
    P002= (String)session.getAttribute("blagP002");
 	if(P002!=null && P002.equals("psP002")){
		String CFESP = "P002~" + creditoKronner + "~";
		if(CFESP.length() > 0)
			session.setAttribute("CFESP", CFESP);
		out.println("<script language=\"javascript\">top.openDialog('../../servlet/ventanilla.ImprimirServlet', 160, 120, 'top.setPrefs4()', document.Txns)</script>"); // Para Certificación
		}
        out.println("<script language=\"javascript\">top.openDialog('../../servlet/ventanilla.ImprimirServlet', 160, 120, 'top.setPrefs4()', document.Txns)</script>"); // Para Certificación
      }
    }
  }
%>
  <input type="hidden" name="transaction" value="<%= session.getAttribute("page.iTxn") %>">
  <input type="hidden" name="transaction1" value="<%= session.getAttribute("page.oTxn") %>">
  <input type="hidden" name="supervisor" value="<%= session.getAttribute("supervisor") %>">
  <input type="hidden" name="override" value="<%= needOverride%>">
</td>
</tr>
</table>
</form>
</body>
</html>


