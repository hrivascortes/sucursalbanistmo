<!-- 
//*************************************************************************************************
//             Funcion: JSP que muestra la salida de SAT
//            Elemento: RedirectorCSIB.jsp
//          Creado por: Hugo G RIVAS
//*************************************************************************************************
// CR - 262793 - 10/11/2012 - Creacion de elemento proyecto Daily Cash
//*************************************************************************************************
-->

<%@ page session="true" import="ventanilla.com.bital.util.*,java.util.*"%>
<%
response.setHeader("Cache-Control","no-store"); //HTTP 1.1
response.setHeader("Pragma","no-cache"); //HTTP 1.0
response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
%>
<script language="JavaScript">

</script>
<%
Hashtable datasession = (Hashtable)session.getAttribute("page.datasession");
if(datasession == null)
	datasession = new Hashtable();

Vector resp = (Vector)session.getAttribute("response");
String control = (String)resp.get(0);
String txn = (String)session.getAttribute("page.iTxn");
String HAB = (String)datasession.get("HAB");
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
  <title>Certificación</title>
<style type="text/css">
  Body {font-family: Arial, Helv; font-size: 9pt; font-weight: bold}
</style>
</head>
<script language="JavaScript">
<!--
  function redirect(){
    <%if(control.equals("0")){%>
    document.forms[0].submit();
    <%}%>
  }
//-->
</script>
   <body bgcolor="#FFFFFF" onload="javascript:redirect();">
<%
String proceso="";
if((txn.equals("1003") || txn.equals("1001")) && control.equals("0") && !HAB.equals("XHAB"))
{
out.print("<form name=\"Txns\" action=\"../../servlet/ventanilla.PageServlet\" method=\"post\">");
%>
  <input type="hidden" name="transaction" value="<%="CSIB"%>">
  <input type="hidden" name="transaction1" value="<%="CSIB"%>">
<%
   out.print("</form>");
}
else{
	proceso = "Final.jsp";
	response.sendRedirect(proceso);

}
session.setAttribute("page.datasession", datasession);
%>
</body>
</html>
