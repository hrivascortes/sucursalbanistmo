<!-- 
168 
// CCN - 4360581 - 12/03/2007 - Se convierten los estilos en jsf y se hace un include en el jsp
-->

<%@ page session="true" import="java.util.Hashtable"%>
<%
response.setHeader("Cache-Control","no-store"); //HTTP 1.1
response.setHeader("Pragma","no-cache"); //HTTP 1.0
response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<META HTTP-EQUIV="REFRESH" content="5">
<html>
<head>
  <title>Results</title>
  <%@include file="style.jsf"%>
</head>
<Script language="JavaScript">
function chgPag()
{
	document.nomina.submit();
}
</Script>
<body>
<%
	String status = "*";
	Hashtable datos = (Hashtable)session.getAttribute("page.datasession");

	String codec = (String)datos.get("codecargo");
	out.println("<br>Procesando Nomina, por favor espere...");
	if(!(codec.equals("X")))
	{
		if (codec.equals("0"))
		{
			out.println("<br><br>Cargo aceptado ...");
  			Integer count = new Integer(0);
			if (datos.get("conteo")!= null)
			{
			count = (Integer)datos.get("conteo");
			}
			Integer size  = new Integer(0);
			if (datos.get("numero")!=null)
			{
				size = (Integer)datos.get("numero");
			}
			String codea = (String)datos.get("codeabono");
			if(!(codea.equals("X")))
			{
				if (codea.equals("0"))
					status = "Aceptado";
				else
					status = "Rechazado";
				out.println("<br><br>Abonos procesados: " + count + " de " + size + " - " + status );
			}
		}
		else
		{
			out.println("<br><br>El cargo fue rechazado, imprima la certificacion correspondiente...");
                        out.println("<script language=\"JavaScript\">alert('El cargo fue rechazado, imprima la certificacion correspondiente...');</script>");
		}
	}

	String cend = (String)datos.get("end");
	if(!(cend.equals("X")))
	{
		if(cend.equals("1"))
		{
			out.println("<br><br>Proceso terminado...");
%>
			<form name="nomina" action="ResponseNOM1.jsp" method="Post">
				<input type="hidden" name="inpSubmit">
			</form>
			<script language="JavaScript" >
				chgPag();
			</script>
<%		}
	}
%>
</body>
</html>