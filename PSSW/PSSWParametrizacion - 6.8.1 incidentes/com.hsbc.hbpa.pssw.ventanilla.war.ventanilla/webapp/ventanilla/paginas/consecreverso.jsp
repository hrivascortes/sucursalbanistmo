<!-- 
// CCN - 4360581 - 12/03/2007 - Se convierten los estilos en jsf y se hace un include en el jsp
-->

<%@ page session="true"%>
<%
response.setHeader("Cache-Control","no-store"); //HTTP 1.1
response.setHeader("Pragma","no-cache"); //HTTP 1.0
response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
 <head>
 <title>Consecutivo Reverso</title>
 <%@include file="style.jsf"%>
 <script language="JavaScript">
 <!--
  var Nav4 = ((navigator.appName == "Netscape") && (parseInt(navigator.appVersion) >= 4))
  function closeme()
  {
   window.close()
  }

  function handleOK(form)
  {
   if(window.opener && !window.opener.closed)
   {
    transferData()
    window.opener.dialogWin.returnFunc.doValidate()
   }
   else{
   alert("Se cerró la ventana de Autorización!")
  }
  closeme()
  return false
 }

 function handleCancel()
 {
  window.opener.dialogWin.returnedValue = '0'
  window.opener.dialogWin.returnFunc.doValidate()
  closeme()
  return false
 }
 
function getFormData(form){
  var searchString = ""
  var onePair
  for (var i = 0; i < form.elements.length; i++){
   if (form.elements[i].type == "text"){
    onePair = escape(form.elements[i].name) + "&" + form.elements[i].value
   }
   else continue
   searchString += onePair + "&"
  }
  return searchString
 }

 function transferData()
 {
  if(top.opener && !top.opener.closed)
  {
   top.opener.dialogWin.returnedValue = getFormData(document.prefs)
  }
 }


function verifyConsec()
{
    var valor = document.prefs.txtConsecutivo.value;
    var buf = valor.match("[0-9]*");
    if( buf != valor )
	  {
        alert('El valor debe ser numérico, reintente.');
        document.prefs.txtConsecutivo.focus();
        return false;
    }
    buf = valor.length;
    for(var i=0; i<7-buf; i++)
    {
        valor = '0' + valor;
    }
    document.prefs.txtConsecutivo.value = valor;
    return true;
}

function verifyConsecButton(){
  if(verifyConsec())
    handleOK();
}
//-->
 </script>
 </head>
 <body onLoad="top.opener.setFieldFocus(window.document); if (top.opener) top.opener.blockEvents(); top.formName=document.forms[0];" onUnload="if (top.opener) top.opener.unblockEvents()" topmargin="0" leftmargin="0" bgproperties="fixed">
  <form name="prefs" action="javascript:verifyConsec()" method="POST">
   <table border="0" cellspacing="0" cellpadding="0" align="center">
    <tr>
     <td >Consecutivo:</td><td></td><td><input name="txtConsecutivo" type="text" value="" size="7" maxlength="7" onchange="verifyConsec();"></td>
    </tr>
   </table>
   <table witdh="60%" border="0" cellspacing="10" cellpadding="0" align="center">
    <tr>
     <td><a href="javascript:parent.handleCancel()"><img src="../imagenes/b_cancelar.gif" border="0"></a></td>
     <td><a href="javascript:verifyConsecButton();"><img src="../imagenes/b_aceptar.gif" border="0"></a></td>
    </tr>
   </table>
  </form>
 </body>
</html>