<!--
//*************************************************************************************************
///            Funcion: JSP para flujo del RAP
//            Elemento: RdirectorRAP.jsp
//          Creado por: Alejandro Gonzalez Castro
//      Modificado por: Alejandro Gonzalez Castro
//*************************************************************************************************
// CCN - 4360409 - 06/01/2006 - Se crea JSP para redirecionar el flujo en la Linea de Captura del SAT.
//*************************************************************************************************
-->

<%@ page session="true" import="ventanilla.com.bital.util.*,java.util.*"%>
<%
response.setHeader("Cache-Control","no-store"); //HTTP 1.1
response.setHeader("Pragma","no-cache"); //HTTP 1.0
response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
%>
<%
Vector frap = (Vector)session.getAttribute("FRAP");
String txn = "";
Vector resp = (Vector)session.getAttribute("response");
String control = (String)resp.get(0);
Hashtable datasession = (Hashtable)session.getAttribute("page.datasession");
if(datasession == null)
	datasession = new Hashtable();
	
Stack flujotxn = new java.util.Stack();
flujotxn = (Stack)session.getAttribute("page.flujotxn");	
	
	
if (session.getAttribute("entro").equals("1"))	
{
if (!request.getParameter("txtEfectivo").equals("null")	)
   {	
 //  System.out.println("Efectivo:RedirectorSatR:"+request.getParameter("txtEfectivo"));
 //	System.out.println("Cheque:RedirectorSatR:"+request.getParameter("txtCheque"));
   	String 	total = (String)datasession.get("monto");
   if(request.getParameter("txtEfectivo").equals(total))
      {
   //     System.out.println("igual");
     //   Stack flujotxn = new java.util.Stack();
    //    flujotxn = (Stack)session.getAttribute("page.flujotxn");
        txn = (String)flujotxn.pop();
      }  
    //else
      //System.out.println("diferente");
   }
   
}   
txn = (String)flujotxn.pop();
flujotxn.add(txn);
	
//if(frap.isEmpty())
//{
//	String txns = request.getParameter("txns");
//	try
//	{
//		NSTokenizer txnstmp = new NSTokenizer(txns, "*");
//		while( txnstmp.hasMoreTokens() )
//		{
//			String token = txnstmp.nextToken();
//			
//			if( token == null )
//				continue;
  //          frap.addElement(token);
//		}
//	}
//    catch(Exception e)
//    {
//    }
//}
//  if(frap != null)
//	  txn = frap.firstElement().toString();
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
  <title>Certificación</title>
<style type="text/css">
  Body {font-family: Arial, Helv; font-size: 9pt; font-weight: bold}
</style>

<script language="JavaScript">
<!--
  function redirect(){
    <%if(control.equals("0")){%>
    document.forms[0].submit();
    <%}%>
  }
//-->
</script>
</head>

<%if(txn.equals("R503"))
{%><body bgcolor="#FFFFFF" onload="top.setFieldFocus(window.document)"><%}
else{%><body bgcolor="#FFFFFF" onload="javascript:redirect();"><%}%>

<p>
<%
String proceso = "";
if(txn.equals("5359"))
{
   out.print("<form name=\"Txns\" action=\"../../servlet/ventanilla.PageServlet\" method=\"post\">");
    datasession.remove("MontoTotal");
%>
  <input type="hidden" name="transaction" value="5359">
  <input type="hidden" name="transaction1" value="5359">
<%
   out.print("</form>");
}
if(txn.equals("0604"))
{
   out.print("<form name=\"Txns\" action=\"../../servlet/ventanilla.PageServlet\" method=\"post\">");
%>
  <input type="hidden" name="transaction" value="0604">
  <input type="hidden" name="transaction1" value="0604">
<%
   out.print("</form>");
}
if(txn.equals("0510"))
{
   out.print("<form name=\"Txns\" action=\"../../servlet/ventanilla.PageServlet\" method=\"post\">");
%>
  <input type="hidden" name="transaction" value="0510">
  <input type="hidden" name="transaction1" value="0510">
<%
   out.print("</form>");
}
//if(txn.equals("5503") && control.equals("0"))
if(txn.equals("R503") && control.equals("0"))
{
datasession.put("cTxn", "S027");
//session.setAttribute("page.cTxn","S027");
   %>
   
   <input type="hidden" name="cTxn" value="S027">
   <%
//   Vector resp = (Vector)session.getAttribute("response");
	//proceso = "PageBuilder5503.jsp";
	proceso= "../../servlet/ventanilla.DataServlet";
	response.sendRedirect(proceso);
	
	
	
}
if(txn.equals("5353"))
{
   out.print("<form name=\"Txns\" action=\"../../servlet/ventanilla.PageServlet\" method=\"post\">");
%>
  <input type="hidden" name="transaction" value="5353">
  <input type="hidden" name="transaction1" value="5353">
<%
   out.print("</form>");
}

// Agregar campos de jsp PageBuilderRAP a datasession
String[] values;
Enumeration params = request.getParameterNames();

while( params.hasMoreElements() )
{
    String param = (String)params.nextElement();
    values = request.getParameterValues( param );
    for(int i=0; i<values.length; i++)
	    datasession.put(param, values[i]);
}

// Remove a variable de sesion para Flujo de Control RAP
// if(!(frap.isEmpty()))
//if(frap.firstElement().toString().equals("5503"))
//{
//	frap.removeElementAt(0);
	//datasession.remove("flujorap");
//}
//else
//{// Remove a Primer elemento del Vector de Control de Flujo
//	frap.removeElementAt(0);
//}
session.setAttribute("page.datasession", datasession);
%>
<p>
</body>
</html> 
