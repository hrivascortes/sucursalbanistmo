<!--
//*************************************************************************************************
//             Funcion: JSP que presenta la respuesta de la txn 0730
//            Elemento: Response0730.jsp
//          Creado por: Alejandro Gonzalez Castro
//      Modificado por: Fausto Rodrigo Flores Moreno
//*************************************************************************************************
// CCN - 4360175 - 03/08/2004 - Se ajusta size de ventana de impresion
// CCN - 4360189 - 03/09/2004 - Se redirecciona a Final.jsp por Control de Efectivo
// CCN - 4360275 - 18/02/2005 - WAS 5.1
// CCN - 4360349 - 22/07/2005 - Se realizan adecuaciones para el cobro de comision a clientes y no clientes
// CCN - 4360364 - 19/08/2005 - Se modifican textos "Comision Cliente/Comision no Cliente" para telmex
//								Se modifica flujo para la txn 0730 con servicio 304379 para recibir pagos en 0.00
// CCN - 4360446 - 10/03/2006 - Proyecto TDI fase 2 cobro de comision
// CCN - 4360567 - 02/02/2007 - Se realiza corrección para evitar ivas duplicados o faltantes
//*************************************************************************************************
-->

<%@ page session="true" import="java.util.*"%>
<%
response.setHeader("Cache-Control","no-store"); //HTTP 1.1
response.setHeader("Pragma","no-cache"); //HTTP 1.0
response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<%
%>
<%!
        private String replacespace(String campo)
        {
            String temp = "";
            for (int i=0;  i<campo.length(); i++)
            {
                if(campo.charAt(i) != ' ')
                {temp = temp + campo.charAt(i);}
                else
                {temp = temp + "%20";}
            }
            return temp;
        }

       private String replaceCats(String campo)
        {
            String temp = "";
            for (int i=0;  i<campo.length(); i++)
            {
                if(campo.charAt(i) != '#')
                {temp = temp + campo.charAt(i);}
                else
                {temp = temp + "%23";}
            }
            return temp;
        }

	private String addpunto(String valor)
	{
	int longini = valor.length();
    String cents  = valor.substring(longini-2,longini);
	String entero = valor.substring(0,longini-2);
	int longente = entero.length();
	for (int i = 0; i < (longente-(1+i))/3; i++)
	{
		longente = entero.length();
	    entero = entero.substring(0,longente-(4*i+3))+','+entero.substring(longente-(4*i+3));
	}
  	entero = entero + '.' + cents;
	return entero;
	}

  private String getdomi(String r)
  {
		String domicilio = r.substring(77,108);
		return domicilio;
  }
  private String getString(String s)
  {
    int len = s.length();
    for(; len>0; len--)
	  if(s.charAt(len-1) != ' ')
	    break;

    return s.substring(0, len);
  }

  private String getItem(String newCadNum, int newOption)
  {
   int iPIndex = newCadNum.indexOf("CONSECUTIVO:") + 12, NumSaltos, Num = 0, Ban = 0;
   String nCadNum = new String("");
   NumSaltos = newOption;
   while(Num < NumSaltos)
   {
    if(newCadNum.charAt(iPIndex) == 0x20)
    {
     while(newCadNum.charAt(iPIndex) == 0x20)
      iPIndex++;
     Num++;
    }
    else
     while(newCadNum.charAt(iPIndex) != 0x20)
      iPIndex++;
   }
   while(newCadNum.charAt(iPIndex) != 0x20 && iPIndex < newCadNum.length()){
    nCadNum = nCadNum + newCadNum.charAt(iPIndex);
    if(iPIndex < newCadNum.length())
      iPIndex++;
    if(iPIndex == newCadNum.length())
      break;
   }

   return nCadNum;
  }

  private String delLeadingfZeroes(String newString)
  {
   StringBuffer newsb = new StringBuffer(newString);
   int Len = newsb.length();
   for(int i=0; i<newsb.length(); i++)
   {
    if(newsb.charAt(i) != '0')
    {
	 newsb.delete(0,i);
	 break;
	}
   }
   return newsb.toString();
  }

  private String setPointToString(String newCadNum)
  {
   int iPIndex = newCadNum.indexOf("."), iLong;
   String szTemp;

   if(iPIndex > 0)
   {
    newCadNum = new String(newCadNum + "00");
    newCadNum = newCadNum.substring(0, iPIndex + 1) + newCadNum.substring(iPIndex + 1, iPIndex + 3);
   }
   else
   {
    for(int i = newCadNum.length(); i < 3; i++)
     newCadNum = "0" + newCadNum;
    iLong = newCadNum.length();
    if(iLong == 3)
     szTemp = newCadNum.substring(0, 1);
    else
     szTemp = newCadNum.substring(0, iLong - 2);
    newCadNum = szTemp + "." + newCadNum.substring(iLong - 2);
   }

   return newCadNum;
  }

  private String delPointOfString(String newCadNum)
  {
   int iPIndex = newCadNum.indexOf(".");

   if(iPIndex > 0)
   {
    newCadNum = newCadNum.substring(0, iPIndex) +
                newCadNum.substring(iPIndex + 1, newCadNum.length());
   }

   return newCadNum;
  }

  private String delCommaPointFromString(String newCadNum)
  {
   String nCad = new String(newCadNum);
   if( nCad.indexOf(".") > -1)
   {
    nCad = nCad.substring(nCad.indexOf(".") + 1, nCad.length());
    if(nCad.length() != 2)
    {
     String szTemp = new String("");
     for(int j = nCad.length(); j < 2; j++)
      szTemp = szTemp + "0";
     newCadNum = newCadNum + szTemp;
    }
   }

   StringBuffer nCadNum = new StringBuffer(newCadNum);
   for(int i = 0; i < nCadNum.length(); i++)
    if(nCadNum.charAt(i) == ',' || nCadNum.charAt(i) == '.')
     nCadNum.deleteCharAt(i);

   return nCadNum.toString();
  }

  private String setFormat(String newCadNum)
  {
    int iPIndex = newCadNum.indexOf(".");
    String nCadNum = new String(newCadNum.substring(0, iPIndex));
    for (int i = 0; i < (int)((nCadNum.length()-(1+i))/3); i++)
     nCadNum = nCadNum.substring(0, nCadNum.length()-(4*i+3))+','+nCadNum.substring(nCadNum.length()-(4*i+3));

    return nCadNum + newCadNum.substring(iPIndex, newCadNum.length());
  }

  private String longtoString(long newlong){
    String newString = String.valueOf(newlong);
    return newString.toString();
  }

%>
<%
  Vector resp = (Vector)session.getAttribute("response");
  Hashtable datasession = (Hashtable)session.getAttribute("page.datasession");
%>
<html>
<head>
  <title>Certificación</title>
<style type="text/css">Body {font-family: Arial, Helv; font-size: 10pt; font-weight: bold; color:#000000}</style>
</head>
<script language="JavaScript">
function aceptar(forma)
{
	forma.submit();
     var obj = document.getElementById('cont_img');
     if(obj!=null)
     obj.onclick = new Function('return false');
}
function cancelar(forma)
{
  forma.action = "../paginas/Final.jsp";
	forma.submit();
}
function continuar(forma)
{
  alert("documento Impreso...");
}
function changeaction()
{
    document.Txns.action = "../../servlet/ventanilla.PageServlet";
}
</script>
<body>
<%
  String cTxn = (String)datasession.get("cTxn");
  String fTime = (String)datasession.get("fTime");
  if(cTxn.equals("0730") && fTime.equals("1")){
   out.println("<form name=\"Txns\" action=\"../../servlet/ventanilla.DataServlet\" method=\"post\">");
   out.println("<input type=\"hidden\" name=\"txtInvoqued\" value=\"" + "somedatahere" + "\">");
  }
  else
 {  out.println("<form name=\"Txns\" action=\"../../servlet/ventanilla.PageServlet\" method=\"post\">");
   out.println("<input type=\"hidden\" name=\"txtInvoqued\" value=\"" + "somedatahere" + "\">");}
%>
<table border="0" cellspacing="0">
<tr>
<td>
</td>
</tr>
<tr>
<td>
  <input type="hidden" name="transaction" value="<%= session.getAttribute("page.iTxn") %>">
  <input type="hidden" name="transaction1" value="<%= session.getAttribute("page.oTxn") %>">
</td>
</tr>
</table>
<p>
<%
    String txtServicio=datasession.get("txtCtaCia").toString();
    String txtCadImpresion = new String("");
    if(session.getAttribute("servEspeciales") != null){
      String cadenaTemporal = session.getAttribute("servEspeciales").toString();
      if(cadenaTemporal.equals("SI")){
        txtCadImpresion = "NOIMPRIMIR";
      }
      session.removeAttribute("servEspeciales");
    }
   String szResp = new String("");
   int codigo = 1;
   if( resp.size() != 4 )
    out.println("<b>Error de conexi&oacute;n</b><br>");
   else
   {
    try{
     codigo = Integer.parseInt((String)resp.elementAt(0));
    }
    catch(NumberFormatException nfe){
     out.println("<b>Error de conexi&oacute;n</b><br>");
    }
    if( codigo != 0 )
      out.println("<b>Error en transaccion</b><br>");
     int lines = Integer.parseInt((String)resp.elementAt(1));
     szResp = (String)resp.elementAt(3);
     for(int i=0; ; i++){
      String line = szResp.substring( i*78, Math.min(i*78+77, szResp.length()) );
      out.println(getString(line) + "<p>");
      if(Math.min(i*78+77, szResp.length()) == szResp.length())
       break;
     }
     
     if( codigo != 0 && datasession.get("flagCuenta").toString().equals("1"))
			out.println("Numero de Cuenta "+datasession.get("txtCuentaCom").toString()+" no existe");
   }
%>
<%
if (codigo == 0)
{
String cuenta = datasession.get("txtCuentaCom").toString();
     String comCliente="NO";
     String Comision="N";
//Tarjeta TDI
        String TarjetaTDI = null;
        if (session.getAttribute("Tarjeta.TDI") != null)
           TarjetaTDI = (String)session.getAttribute("Tarjeta.TDI");
//Tarjeta TDI
     if (txtServicio.equals("101026") || txtServicio.equals("101067") )
      {
        
       if (fTime.equals("1"))
	  {
	   Comision = szResp.substring(546,547);
	   if (!cuenta.equals("") || TarjetaTDI != null) //Tarjeta TDI
	       comCliente = "SI"; 
          }   
          else{
              Comision = datasession.get("ComServ0728").toString();
              }
      }


 if(!txtServicio.equals("101026") || !txtServicio.equals("101067") )     
   {
     if (datasession.get("txtCuentaCom").toString().equals(""))
        Comision="S";
   }
   
   datasession.put("ComServ0728",Comision);
      
		   
   if (Comision.equals("N") && fTime.equals("1"))
      {
      	datasession.put("CobCom","no");
 	datasession.put("txtCuentaCom", "");
%>

	<div id='div_cont' class='invisible'>
        <table border="0" cellspacing="0">
	<tr>
	<td><a id='cont_img' href="JavaScript:aceptar(document.Txns) "><img src="../imagenes/b_continuar.gif" border="0"></a></td>
	<td><a href="JavaScript:cancelar(document.Txns) "><img src="../imagenes/b_cancelar.gif" border="0"></a></td>
        </tr>
	</table>
	</div>	
	
	
<%//	fTime = "2";
}
    Stack flujotxn = (Stack)session.getAttribute("page.flujotxn");
    long efectivo = Long.parseLong(delCommaPointFromString((String)datasession.get("txtEfectivo")));
    long monto = Long.parseLong(delCommaPointFromString((String)datasession.get("txtMonto")));
    Stack flujonew = new Stack();
    String[] newflujo = {"0372","0360","S730","5353"};
    if (((efectivo == 0 && !txtServicio.equals("304379"))|| efectivo < monto) && session.getAttribute("VC").toString().equals("1"))
    {
	for (int i=0; i<newflujo.length; i++)
	{flujonew.push(newflujo[i]);}
	flujotxn = flujonew;
	session.setAttribute("page.flujotxn",flujotxn);
        long docs = monto - efectivo;
        if(docs==0)
        datasession.put("txtCheque", "0.00");
        else
        datasession.put("txtCheque", addpunto(Long.toString(docs)));
%>
        <Script language='JavaScript'>
            changeaction();
        </SCRIPT>
<%    }
      else
      {session.setAttribute("VC","2");}
      
      
      if (efectivo == 0 && monto == 0 && txtServicio.equals("304379") && fTime.equals("1"))
         {
         String[] nuevoflujo = {"0372","0360"};
         Stack flujonuevo = new Stack();
         for (int i=0; i<nuevoflujo.length; i++)
      	 {flujonuevo.push(nuevoflujo[i]);}
	     flujotxn = flujonuevo;
	     session.setAttribute("page.flujotxn",flujotxn); 
         }
             
//*datos de descarga***********************************************************************************//

   if((cTxn.equals("0730") || cTxn.equals("S730")) && fTime.equals("2") && Comision.equals("N"))
   while(!flujotxn.empty())
    flujotxn.pop();



   if(!flujotxn.empty() && !fTime.equals("1") && Comision.equals("S")
 	&& datasession.get("CobCom").toString().equals("si")){
   out.print("<div align=\"center\">");
   out.print("</div>");
  }
 datasession.put("txtCuentaCom", cuenta);
 
 

 if (Comision.equals("N"))
 {datasession.put("CobCom","NO"); }
%>
<%


 if(fTime.equals("1") && Comision.equals("S"))
 {
   String txtComision;
   String txtPIVA = new String("");
   
   if (txtServicio.equals("101026") || txtServicio.equals("101067") )
      {
         int pini = 546;
          if (comCliente.equals("SI"))
              txtComision = szResp.substring(pini+1,pini+5);
   	      else
   	         {
   	          txtComision = szResp.substring(pini+5,pini+9);
   		  }
       
          pini = 312;
          txtPIVA = szResp.substring(pini+9,pini+11);
      } 
      else
         {
          txtComision = getItem(szResp, 3);
          txtPIVA = getItem(szResp, 4);
         }
  
   String txtdomici = getdomi(szResp);
  int intcom = Integer.parseInt(txtComision);
  int intiva = Integer.parseInt(txtPIVA);
  
   if (intcom > 0 && intiva > 0)
   {
   	datasession.put("txtComision", delLeadingfZeroes(txtComision));
   	datasession.put("txtPIVA", delLeadingfZeroes(txtPIVA));
   	datasession.put("txtdomici", delLeadingfZeroes(txtdomici));
   	session.setAttribute("page.datasession", datasession);
	datasession.put("CobCom","si");
    }
    else
    {
        if (efectivo == 0 || efectivo < monto)
        {
            flujonew = new Stack();
            String[] newflujos = {"S730","5353"};
            for (int i=0; i<newflujos.length; i++)
            {flujonew.push(newflujos[i]);}
            flujotxn = flujonew;
            session.setAttribute("page.flujotxn",flujotxn);
         }
	 datasession.put("CobCom","no");
    }
    
    
	if (datasession.get("CobCom").toString().equals("si"))
	{
%>
	<table border="0" cellspacing="0">
	<tr>
	<td>Monto:</td>
	<td></td>
	<td  align="right"><%
	 long montoComision = Long.parseLong(datasession.get("txtComision").toString());
	 long montoTxn = Long.parseLong(delCommaPointFromString(datasession.get("txtMonto").toString()));
	 long montoIVA = Long.parseLong(datasession.get("txtPIVA").toString()) * montoComision / 100;
	 long montoTotal = montoComision + montoTxn + montoIVA;
	 out.print(setFormat(setPointToString(longtoString(montoTxn))));
	%></td>
	</tr>
	<tr>
	<td>
	<%
	if (Comision.equals("S"))
	   {
		if (comCliente.equals("SI"))
			out.println("Comision Cliente:");
		else if (comCliente.equals("NO"))
			out.println("Comision No Cliente:");
		}
		else
		out.println("Comision:");
	%>						
	</td>
	<td></td>
	<td align="right"><%=setFormat(setPointToString(longtoString(montoComision)))%></td>
	</tr>
	<tr>
	<td>IVA:</td>
	<td></td>
	<td align="right"><%=setFormat(setPointToString(longtoString(montoIVA)))%></td>
	</tr>
	<tr>
	<td>Total:</td>
	<td></td>
	<td align="right"><%=setFormat(setPointToString(longtoString(montoTotal)))%></td>
	</tr>
	<tr>
	<td><a id='cont_img' href="JavaScript:aceptar(document.Txns) "><img src="../imagenes/b_continuar.gif" border="0"></a></td>
	<td><a href="JavaScript:cancelar(document.Txns) "><img src="../imagenes/b_cancelar.gif" border="0"></a></td>
	<script language="javascript">
	
		var liga = document.getElementById('cont_img');
		liga.focus();
	
	</script>
</tr>
	</table>
<%
	   	datasession.put("montoComision", longtoString(montoComision));
   		datasession.put("montoIVA", longtoString(montoIVA));
                long Totalcomiva = montoComision + montoIVA;
                datasession.put("Totalcomiva", longtoString(Totalcomiva));
	}
  }
%>
<% // txn 0730
    if (session.getAttribute("page.cTxn").toString().equals("S730"))
    {session.setAttribute("page.cTxn","0730");}
%>
<%
    if (efectivo != monto && datasession.get("CobCom").toString().equals("no"))
    {fTime = "2";}
%>
<% String isCertificable = (String)session.getAttribute("page.certifField");  // Para Certificación

   String domicilioSuc = new String("");
   if(!isCertificable.equals("N") && !fTime.equals("1")){ // Para Certificación
    session.setAttribute("Consecutivo", getItem((String)resp.elementAt(3), 1)); // Para Certificación
    session.setAttribute("Tipo", isCertificable); // Para Certificación
    if (cTxn.equals("0372") && datasession.get("lstComprobante").toString().equals("si"))
     {
      if(session.getAttribute("domicilioSuc") != null){
        domicilioSuc = session.getAttribute("domicilioSuc").toString();
        session.removeAttribute("domicilioSuc");
      }
      txtCadImpresion = "~CFISCAL~" + (String)datasession.get("txtNombre") + "~" + (String)datasession.get("txtRfc") + "~" + "RECAUDACIONES" + "~" + "PAGO DE SERVICIOS" + "~" + (String)datasession.get("txtDomici") + "~" + setFormat(setPointToString((String)datasession.get("montoComision"))) + "~" + setFormat(setPointToString((String)datasession.get("montoIVA"))) + "~" + setFormat(setPointToString((String)datasession.get("Totalcomiva"))) + "~" + domicilioSuc;
    }
     session.setAttribute("txtCadImpresion", txtCadImpresion); // Para Certificación
     out.println("<script language=\"javascript\">top.openDialog('../../servlet/ventanilla.ImprimirServlet', 160, 120, 'top.setPrefs4()', document.Txns)</script>"); // Para Certificación
   }
%>
<%
  if(fTime.equals("1")){
   datasession.put("fTime", "2");
   session.setAttribute("page.datasession", datasession);
  }
%>
</form>
<%
	if (datasession.get("CobCom").toString().equals("no") && efectivo == monto)
	{
	 datasession.put("CobCom","NO");
        while(!flujotxn.empty())
        flujotxn.pop();
%>
	<script language="JavaScript">
		aceptar(document.Txns);
	</script>
<%	} else{
			
			%>
			<script language='javascript'>
 			var o;
 			o = document.getElementById('div_cont');
 			if(o!=null){
 				o.className='visible';
 			}
			</script>			
			<%
	}
}%>
</body>
</html>
