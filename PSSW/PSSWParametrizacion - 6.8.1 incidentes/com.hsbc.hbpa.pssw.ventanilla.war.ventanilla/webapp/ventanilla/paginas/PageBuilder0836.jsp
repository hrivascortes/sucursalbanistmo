<!--
//*************************************************************************************************
//             Funcion: JSP que muestra las opciones para la txn 0836
//            Elemento: PageBuilder0836.jsp
//          Creado por: Alejandro Gonzalez Castro
//      Modificado por: Fausto Rodrigo Flores Moreno
//*************************************************************************************************
// CCN - 4360253 - 07/01/2005 - Se agregan servicios 3922, 4852, 5647, 5711, 6491, 6612.
// CCN - 4360259 - 21/01/2005 - Se agrega el servicio 5815.
// CCN - 4360276 - 17/02/2005 - Se toman de persistencia los servicios para catalogo de RAP CP.
// CCN - 4360493 - 23/06/2006 - Se realizan modifiaciones para evitar errores de captura del servicio RAP
// CCN - 4360581 - 12/03/2007 - Se convierten los estilos en jsf y se hace un include en el jsp
//*************************************************************************************************
-->

<%@ page session="true"  import="java.util.*"  %>
<%
response.setHeader("Cache-Control","no-store"); //HTTP 1.1
response.setHeader("Pragma","no-cache"); //HTTP 1.0
response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<%
String MontoPro = new String("");
String txnAuthLevel = (String)session.getAttribute("page.txnAuthLevel");
String txnIMGAutoriz = (String)session.getAttribute("page.txnIMGAutoriz");
Vector v = (Vector)session.getAttribute("page.specialacct");
String noservicios =  (String)session.getAttribute("page.noservicios");
int nserv = Integer.parseInt(noservicios);
int k;
%>
<html>
<head>
<%@include file="style.jsf"%>
<title>Datos de la Transaccion</title>
<script  language="JavaScript">
<!--
var formName = 'Mio'
var specialacct = new Array()
<%
if(!v.isEmpty())
{
  for(int iv = 0; iv < v.size(); iv++){out.print("specialacct[" + iv + "]='" + v.get(iv) + "'" + '\n');}
}%>

function validate(form)
{
  if( !top.validate(window, document.entry.txtServicio, 'isnumeric') )
    return;
  if( !top.validate(window, document.entry.txtReferencia, 'isEmptyStr') )
    return;
  form.validateFLD.value = '1'

<% if(!txnAuthLevel.equals("1") && !txnIMGAutoriz.equals("1"))
	{
    out.println("  form.submit();");
    out.println("var obj = document.getElementById('btnCont');"); 
    out.println("obj.onclick = new Function('return false');");   
	}   
%>
}
function AuthoandSignature(entry)
{
 var Cuenta;
 if(entry.validateFLD.value != '1')
  return
 if(entry.needAutho.value == '1' && entry.AuthoOK.value != '1')
 {
  top.openDialog('../ventanilla/paginas/authoriz.jsp', 220, 130, 'top.setPrefs()', entry)
 }
 if(entry.needSignature.value == '1' && entry.SignField.value != '')
{
  Cuenta = eval('entry.' + entry.SignField.value + '.value')
  top.openModalDialog('<%=request.getContextPath()%>/servlet/ventanilla.FirmasServlet?txtDDACuenta=' + Cuenta, 710, 430, 'top.setPrefs2()', entry)
 }
}
//-->
</script>
</head>
<body bgcolor="#FFFFFF" onload="top.setFieldFocus(window.document)">
<br>
<% out.print("<form name=\"entry\" action=\"../servlet/ventanilla.DataServlet\" method=\"post\" ");
   if(txnAuthLevel.equals("1"))
    out.print("onSubmit=\"return validate(this)\"");
   out.println(">");
%>

<h1><%=session.getAttribute("page.txnImage")%></h1>

<h3>
<%out.print(session.getAttribute("page.cTxn") + "  " +
                (String)session.getAttribute("page.txnlabel"));%>
</h3>

<table border="0">
<tr>
    <td>Cat&aacute;logo de Servicios :</td><td>&nbsp;&nbsp;</td>
    <td>
           <select tabindex="1" name="txtServicio" >
            <%
            
            //LLENADO DE LOS SERVICIOS PERMITIDOS PARA RAPM LEIDOS DESDE DATOS_VARIOS (LECTURA EN PERSITENCIA)
         
            ventanilla.com.bital.beans.DatosVarios rap = ventanilla.com.bital.beans.DatosVarios.getInstance();                   
            Vector vecRAP = new Vector();        
            vecRAP =(Vector)rap.getDatosV("RAPCP");   
             
            int szvecRAP = vecRAP.size();
            int FlagServicio = 0;
            for (int x=0;x<szvecRAP;x++)
            {
            Vector vec = (Vector)vecRAP.get(x);
            int serv = Integer.parseInt((String)vec.get(1));
            String servDes = (String)vec.get(2);
               if (serv != 0)
                  {%>
                  <option value="<%=serv%>"><%=serv%> - <%=servDes%></option>
                 <%}
            }%>


        </select>
    </td>
</tr>
<tr>
    <td>Referencia :</td><td>&nbsp;&nbsp;</td>
    <td><input tabindex="3" type="text" name="txtReferencia" size="40" maxlength="40" onBlur="top.estaVacio(this)||top.validate(window, this, 'isEmptyStr')"  onKeyPress="top.estaVacio(this)||top.keyPressedHandler(window, this, 'isEmptyStr')"></td>
</tr>
    <tr><td colspan='3'><b>Capturar N&uacute;mero de Cuenta de Cheques para Clientes ...</b><td>
</tr>
<tr>
    <td align="right">Cuenta :</td><td>&nbsp;&nbsp;</td>
    <td><input tabindex="4" type="text" name="txtDDACuenta" size="10" maxlength="10" onBlur="top.estaVacio(this)||top.validate(window, this, 'isValidAcct')"   onKeyPress="top.estaVacio(this)||top.keyPressedHandler(window, this, 'isValidAcct')" ></td>
</tr>
</table>

<input type="hidden" name="nservicios" value="1">
<input type="hidden" name="tot" value="0">
<input type="hidden" name="np" value="0">
<input type="hidden" name="flujorap" value="1">
<input type="hidden" name="noefec" value="0">
<input type="hidden" name="ser60" value="0">
<input type="hidden" name="ser670" value="0">
<input type="hidden" name="iTxn" value="<%= session.getAttribute("page.iTxn")%>">
<input type="hidden" name="oTxn" value="<%= session.getAttribute("page.oTxn")%>">
<input type="hidden" name="cTxn" value="<%= session.getAttribute("page.cTxn")%>">
<input type="hidden" name="sucursal" value="<%= session.getAttribute("branch")%>">
<input type="hidden" name="teller" value="<%= session.getAttribute("teller")%>">
<input type="hidden" name="moneda" value="<%= session.getAttribute("page.moneda")%>">
<input type="hidden" name="registro" value="<%=session.getAttribute("empno")%>">
<input type="hidden" name="tllrlvl" value="<%=session.getAttribute("tellerlevel")%>">
<input type="hidden" name="supervisor" value="<%= session.getAttribute("teller") %>">
<input type="hidden" name="AuthoOK" value="0">
<input type="hidden" name="SignOK" value="0">
<input type="hidden" name="validateFLD" value="0">
<input type="hidden" name="needSignature" value="<%out.print(txnIMGAutoriz);%>">
<input type="hidden" name="needAutho" value="<%out.print(txnAuthLevel);%>">
<input type="hidden" name="SignField" value="<%= session.getAttribute("page.SignField") %>">
<input type="hidden" name="compania" value="20">
<p>

<% if(txnAuthLevel.equals("1") || txnIMGAutoriz.equals("1"))
    out.println("<a id='btnCont' tabindex='5' href=\"javascript:validate(document.entry);javascript:AuthoandSignature(document.entry)\" ><img src=\"../ventanilla/imagenes/b_continuar.gif\" border=\"0\"></a>");
   else
    out.println("<a id='btnCont' tabindex='5' href=\"javascript:validate(document.entry)\"><img src=\"../ventanilla/imagenes/b_continuar.gif\" border=\"0\"></a>");
%>
</form>
</body>
</html>

