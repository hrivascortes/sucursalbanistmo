<!--
//*************************************************************************************************
//             Funcion: JSP que presenta las opciones de la txn 0528
//            Elemento: PageBuilder0528.jsp
//          Creado por: Alejandro Gonzalez Castro
//      Modificado por: Juvenal R. Fernandez V.
//*************************************************************************************************
// CCN - 4360275 - 18/02/2005 - WAS 5.1
// CCN - 4360334 - 17/06/2005 - Se convierte fieldLector a fragmento
// CCN - 4360581 - 12/03/2007 - Se convierten los estilos en jsf y se hace un include en el jsp
//*************************************************************************************************
-->

<%@ page session="true" import="java.util.*"%>
<%
response.setHeader("Cache-Control","no-store"); //HTTP 1.1
response.setHeader("Pragma","no-cache"); //HTTP 1.0
response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<%
Vector listaCampos = (Vector)session.getAttribute("page.listcampos");
Vector listaContenidos = (Vector)session.getAttribute("page.listcont");
String txnAuthLevel = (String)session.getAttribute("page.txnAuthLevel");
String txnIMGAutoriz = (String)session.getAttribute("page.txnIMGAutoriz");

Hashtable datasession = (Hashtable)session.getAttribute("page.datasession");
if (datasession == null)
   datasession = new Hashtable();
int k;
int lastTabIndex = 0;
String MontoPro = new String("000");
if ( datasession.get("MontoTotal") == null){
   MontoPro = "000";
   datasession.put("txtNoCheques",new Integer(0).toString());
   session.setAttribute("page.datasession", (Hashtable)datasession);
}
else
      MontoPro = (String)datasession.get("MontoTotal");

MontoPro = formatMonto(MontoPro);

%>
<html>
<head>
  <%@include file="style.jsf"%>
  <title>Datos de la Transaccion  </title>
<script language="JavaScript">
<!--
var formName = 'Mio'
var continuar = '0';

<%!
  private String formatMonto(String s)
  {
    int len = s.length();
    if (len < 3){
    	s = "0." + s;
     }
    else
    {
      int n = (len - 3 ) / 3;
    	s = s.substring(0, len - 2) + "." + s.substring(len - 2 , len);
      for (int i = 0; i < n; i++)
      {
      	len = s.length();
    		s = s.substring(0, (len -(i*3+6+i))) + "," + s.substring((len -(i*3+6+i)) , len);
    	}
    }
    return s;
  }
%>

function validate(form)
{
if  ( form.txtCodSeg.value.length == 0 || form.txtCveTran.value.length == 0 || form.txtDDACuenta2.value.length == 0 || form.txtSerial2.value.length == 0 ) {
      alert("Introduzca Banda del Cheque.")
      return;
 }

<%
  for(int i=0; i < listaCampos.size(); i++)
   {
    Vector vCampos = (Vector)listaCampos.get(i);
    vCampos = (Vector)vCampos.get(0);
    if( vCampos.get(4).toString().length() > 0 )
    {
       if ( vCampos.get(0).equals("rdoACorte")) {
          if (MontoPro.equals("0.00")) {
              out.println("  if( !top.validate(window, document.entry." + vCampos.get(0) + "[0], '" + vCampos.get(4) +"') ){");
              out.println("      return}");
          }
       }
      else {
            out.println("  if( !top.validate(window, document.entry." + vCampos.get(0) + ", '" + vCampos.get(4) +"') )");
            out.println("    return ");
     }
    }
  }
%>
  form.validateFLD.value = '1';
  form.TxnSig.value = '0528';
  if (document.entry.txtFechaEfect || document.entry.txtFechaError || document.entry.txtFechaCarta)
    document.entry.txtFechaSys.value=top.sysdatef();

}

function Continua4009(form)
	{
    form.action = '../servlet/ventanilla.PageServlet?transaction=' + form.iTxn.value + '&transaction1=' + form.oTxn.value;
    <%
      datasession.put("txtMontoT", MontoPro);
    %>
    form.submit();
    var obj = document.getElementById('btnCont2');
    obj.onclick = new Function('return false');
  }



function Continua0528(form)
	{
     if ( form.validateFLD.value != '1' )
       return;

    form.action = '../servlet/ventanilla.DataServlet';
    form.submit();
    var obj = document.getElementById('btnCont');
    obj.onclick = new Function('return false');
  }


//-->
</script>
</head>
<body bgcolor="#FFFFFF" onload="top.setFieldFocus(window.document)">
<br>
<%
   out.print("<form name=\"entry\" action=\"../servlet/ventanilla.DataServlet\" method=\"post\" ");
   if(txnAuthLevel.equals("1"))
    out.print("onSubmit=\"return validate(this)\"");
   out.println(">");
%>
<h1><%=session.getAttribute("page.txnImage")%></h1>

<h3> <%out.print(session.getAttribute("page.cTxn") + "  " +
                (String)session.getAttribute("page.txnlabel"));%>   </h3>

<table border="0" cellspacing="0">
 <%
    for(int i =  0; i < listaCampos.size(); i++)
      {
	  Vector vCampos = (Vector)listaCampos.get(i);
	  vCampos = (Vector)vCampos.get(0);

	  if(!vCampos.get(0).toString().equals("txtCodSeg"))
	    {
	       out.println("<tr>");
	       out.println("  <td>" + vCampos.get(2) + ":</td>");
	       out.println("  <td width=\"10\">&nbsp;</td>");
	       if(!vCampos.get(0).toString().substring(0,3).equals("lst") )
	       {
		        if (vCampos.get(0).toString().substring(0,3).equals("pss"))
		          {   out.print  ("  <td><input maxlength=\"" + vCampos.get(3) + "\" name=\"" + vCampos.get(0) + "\" size=\"" + vCampos.get(3) +"\" type=\"Password\"" + "\" tabindex=\""+(i+1)+"\"");}
		        else
		          {   out.print  ("  <td><input maxlength=\"" + vCampos.get(3) + "\" name=\"" + vCampos.get(0) + "\" size=\"" + vCampos.get(3) +"\" type=\"text\""+ "\" tabindex=\""+(i+1)+"\"");}
	                String CampoR = (String)vCampos.get(5);
	                if(CampoR != null && CampoR.length() > 0){
	                   CampoR = CampoR.trim();
	                   out.print(" value=\"" + CampoR + "\"");
	               }
	        }
	       else{
	         out.print("  <td><select tabindex=\""+(i+1)+"\" name=\"" + vCampos.get(0) + "\" size=\"" + "1" + "\"");
	         if( vCampos.get(4).toString().length() > 0 ){
	            if(vCampos.get(0).toString().substring(0,3).equals("rdo"))
                  out.println(" onClick=\"top.validate(window, this, '" + vCampos.get(4) +"')\">");
	            else
	                out.println(" onChange=\"top.validate(window, this, '" + vCampos.get(4) +"')\">");
	         }
	         else
	             out.println(">");
	         for(k=0; k < listaContenidos.size(); k++){
	             Vector v1Campos = (Vector)listaContenidos.get(k);
	             for(int j = 0; j < v1Campos.size(); j++){
	                Vector v1Camposa = (Vector)v1Campos.get(j);
	                if(v1Camposa.get(0).toString().equals(vCampos.get(0)))
	                    out.println("  <option value=\"" + v1Camposa.get(3) + "\">" + v1Camposa.get(2));
	             }
	         }
	         out.println("  </select></td>");
	       }
	       if( vCampos.get(4).toString().length() > 0 && !vCampos.get(0).toString().substring(0,3).equals("lst"))
	          //out.println(" onChange=\"top.validate(window, this, '" + vCampos.get(4) +"')\"></td>");
	          out.println("  onBlur=\"top.estaVacio(this)||top.validate(window, this, '" + vCampos.get(4) +"')\"  onKeyPress=\"top.keyPressedHandler(window, this, '" + vCampos.get(4) +"')\"   ></td>");
	       else if(!vCampos.get(0).toString().substring(0,3).equals("lst"))
	               out.println(">");
	       out.println("</tr>");
	    }
	  else
	     {
	        %><%@include file="fieldLector.jsf"%><%
	        i = k ;
	        lastTabIndex = k+i;
	      }
      }
 %>
  <td>Monto Acumulado:</td>
  <td width="10" align="right">&nbsp;</td>
  <td align="right"><%
    out.print(MontoPro);
  %></td>
 </tr>
  <td>Numero de Cheques:</td>
  <td width="10" align="right">&nbsp;</td>
  <td align="right"><%
    out.print(datasession.get("txtNoCheques").toString());
  %></td>
 </tr>

</table>
 <p>
 <p>
 <p> <% out.print("Para terminar lectura de Cheques seleccione Imprimir"); %>

<input type="hidden" name="iTxn" value="<%= session.getAttribute("page.iTxn") %>">
<input type="hidden" name="oTxn" value="<%= session.getAttribute("page.oTxn") %>">
<input type="hidden" name="cTxn" value="<%= session.getAttribute("page.cTxn") %>">
<input type="hidden" name="sucursal" value="<%= session.getAttribute("branch") %>">
<input type="hidden" name="teller" value="<%= session.getAttribute("teller") %>">
<input type="hidden" name="moneda" value="<%= session.getAttribute("page.moneda")%>">
<input type="hidden" name="registro" value="<%=session.getAttribute("empno")%>">
<input type="hidden" name="tllrlvl" value="<%=session.getAttribute("tellerlevel")%>">
<input type="hidden" name="supervisor" value="<%= session.getAttribute("teller") %>">
<input type="hidden" name="validateFLD" value="0">
<input type="hidden" name="AuthoOK" value="0">
<input type="hidden" name="compania" value="20">
<input type="hidden" name="SignOK" value="0">
<input type="hidden" name="validateFLD" value="0">
<input type="hidden" name="needSignature" value="<%out.print(txnIMGAutoriz);%>">
<input type="hidden" name="needAutho" value="<%out.print(txnAuthLevel);%>">
<input type="hidden" name="SignField" value="<%= session.getAttribute("page.SignField") %>">
<input type="hidden" name="TxnSig" value="0000">
<p>
<script>
 if (document.entry.txtFechaEfect)
 {
  document.writeln("<input type=hidden name=txtFechaSys>");
  document.entry.txtFechaEfect.value=top.sysdate();
 }
 if (document.entry.txtFechaError)
 {
  document.writeln("<input type=hidden name=txtFechaSys>");
  document.entry.txtFechaError.value=top.sysdate();
 }
 if (document.entry.txtFechaCarta)
 {
  document.writeln("<input type=hidden name=txtFechaSys>");
  document.entry.txtFechaCarta.value=top.sysdate();
 }
</script>
<%
    out.println("<a id='btnCont' tabindex = \""+(lastTabIndex + 1)+"\" href=\"javascript:validate(document.entry);javascript:Continua0528(document.entry)\"><img src=\"../ventanilla/imagenes/b_continuar.gif\" border=\"0\"></a>");
    if (Integer.parseInt(datasession.get("txtNoCheques").toString()) > 0)
       out.println("<a id='btnCont2' href=\"javascript:Continua4009(document.entry)\"><img src=\"../ventanilla/imagenes/b_imprimir.gif\" border=\"0\"></a>");
%>
</form>
</body>
</html>
