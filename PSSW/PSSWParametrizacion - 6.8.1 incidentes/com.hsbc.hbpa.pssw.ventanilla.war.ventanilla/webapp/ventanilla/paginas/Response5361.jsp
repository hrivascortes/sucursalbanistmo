<!--
//*************************************************************************************************
///            Funcion: JSP que despliega respuesta de txn 5361
//            Elemento: Response5361.jsp
//          Creado por: Israel De Paz Mercado
//    Modificado por: Fausto Rodrigo Flores Moreno
//*************************************************************************************************
// CCN - 4360455 - 07/04/2006 - Se crea JSP para el esquema de cheques devueltos.
// CCN - 4360462 - 10/04/2006 - Se genera nuevo paquete por errores en paquete anterior
// CCN - 4360474 - 05/05/2006 - Se realizan modificaciones para mostrar mensaje de espera cuando se procesan devoluciones
//*************************************************************************************************
-->

<%@ page session="true" import="java.util.*"%>
<%
response.setHeader("Cache-Control","no-store"); //HTTP 1.1
response.setHeader("Pragma","no-cache"); //HTTP 1.0
response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<%!
  private String getString(String s)
  {
   int len = s.length();
   for(; len>0; len--)
    if(s.charAt(len-1) != ' ')
     break;
   return s.substring(0, len);
  }

  private String getItem(String newCadNum, int newOption)
  {
   int iPIndex = newCadNum.indexOf("CONSECUTIVO:") + 12, NumSaltos, Num = 0, Ban = 0;
   String nCadNum = new String("");
   NumSaltos = newOption;
   while(Num < NumSaltos)
   {
    if(newCadNum.charAt(iPIndex) == 0x20)
    {
     while(newCadNum.charAt(iPIndex) == 0x20)
      iPIndex++;
     Num++;
    }
    else
     while(newCadNum.charAt(iPIndex) != 0x20)
      iPIndex++;
   }
   while(newCadNum.charAt(iPIndex) != 0x20 && iPIndex < newCadNum.length()){
    nCadNum = nCadNum + newCadNum.charAt(iPIndex);
    if(iPIndex < newCadNum.length())
      iPIndex++;
    if(iPIndex == newCadNum.length())
      break;
   }

   return nCadNum;
  }
%>
<%
Vector resp = (Vector)session.getAttribute("response");
%>
<html>
<head>
  <title>Response5361</title>
<style type="text/css">Body {font-family: Arial, Helv; font-size: 10pt; font-weight: bold; color:#000000}</style>
<script language="javascript">
<!--
  sharedData = '0';

  function formSubmitter(){
    clearInterval(formSubmitter);
    if(sharedData != '0')
    {
      document.forms[0].submit()
      sharedData = '0';
    }
  }
//-->
</script>
</head>
<body bgcolor="#FFFFFF"  onload="top.setFieldFocus(window.document);setInterval('formSubmitter()',2000)">
<%
  java.util.Stack flujotxn = new java.util.Stack();
  String codigo = "";
  String needOverride = "NO";
 

  if(session.getAttribute("page.flujotxn") != null)
  	flujotxn = (java.util.Stack)session.getAttribute("page.flujotxn");

  if( resp.size() != 4 )
   out.println("<b>Error de conexi&oacute;n</b><br>");
  else
  {
   	try
   	{
     	codigo = (String)resp.elementAt(0);
 	if( codigo.equals("1"))
   	   out.println("<b>Transaccion Rechazada</b><br><br>");
        if( codigo.equals("0") || codigo.equals("1") ) {
     	   int lines = Integer.parseInt((String)resp.elementAt(1));
    	   String szResp = (String)resp.elementAt(3);
    	   for(int i=0; i<lines; i++) {
    		   String line = szResp.substring( i*78, Math.min(i*78+77, szResp.length()) );
     		   out.println(getString(line) + "<p>");
     		   if(Math.min(i*78+77, szResp.length()) == szResp.length())
      		    break;
    	   }
       }
       else {
         if ( codigo.equals("3") || codigo.equals("2"))
            out.println("<b>Transacci&oacute;n Requiere Autorizaci&oacute;n</b>");
         else
            out.println("<b>Ocurri&oacute; un Error de Comunicaci&oacute;n, Verifique.</b>");
       }
  	}
  	catch(NumberFormatException nfe){
     out.println("<b>Error de conexi&oacute;n</b><br>");
    }
    
    Hashtable chequesDev = (Hashtable)session.getAttribute("ChequesDevueltos");
    
    if(chequesDev.isEmpty()){
    	session.removeAttribute("totalChqDev");
        session.removeAttribute("conteoChqDev");
		out.println("<br><br>Proceso terminado...");
	}
	
    String proceso= "PageBuilder5361.jsp";
	response.sendRedirect(proceso);
  }
%>
<form name="Txns" action="../../servlet/ventanilla.PageServlet" method="post">
<table border="0" cellspacing="0">
<tr>
<td>
</td>
</tr>
<tr>
<td>
  <input type="hidden" name="transaction" value="<%= session.getAttribute("page.iTxn") %>">
  <input type="hidden" name="transaction1" value="<%= session.getAttribute("page.oTxn") %>">
  <input type="hidden" name="supervisor" value="<%= session.getAttribute("supervisor") %>">
  <input type="hidden" name="override" value="<%= needOverride%>">
</td>
</tr>
</table>
</form>
</body>
</html>
