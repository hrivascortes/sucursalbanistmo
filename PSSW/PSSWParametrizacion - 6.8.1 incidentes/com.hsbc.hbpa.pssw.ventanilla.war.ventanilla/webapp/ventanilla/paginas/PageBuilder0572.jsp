<!--
//*************************************************************************************************
//             Funcion: JSP que presenta las opciones para la txn 0572
//            Elemento: PageBuilder0572.jsp
//          Creado por: Alejandro Gonzalez Castro
//      Modificado por: Juvenal R. Fernandez V
//*************************************************************************************************
//CCN - 4360163 - 06/07/2004 - Se modifica el pago de GirosCB en efectivo
//CCN - 4360268 - 04/02/2005 - Se realizan adecuaciones para eliminar Agencia NY
// CCN - 4360334 - 17/06/2005 - Se convierte fieldLector a fragmento
// CCN - 4360581 - 12/03/2007 - Se convierten los estilos en jsf y se hace un include en el jsp
//*************************************************************************************************
-->

<%@ page session="true" import="java.util.*"%>
<%
response.setHeader("Cache-Control","no-store"); //HTTP 1.1
response.setHeader("Pragma","no-cache"); //HTTP 1.0
response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<%
Vector listaCampos = (Vector)session.getAttribute("page.listcampos");
Vector listaContenidos = (Vector)session.getAttribute("page.listcont");
String txnAuthLevel = (String)session.getAttribute("page.txnAuthLevel");
String txnIMGAutoriz = (String)session.getAttribute("page.txnIMGAutoriz");
Hashtable datasession = (Hashtable)session.getAttribute("page.datasession");
int k;
String MontoPro = new String("");
%>
<%!
  private String setPointToString(String newCadNum)
  {
   int iPIndex = newCadNum.indexOf("."), iLong;
   String szTemp;

   if(iPIndex > 0)
   {
    newCadNum = new String(newCadNum + "00");
    newCadNum = newCadNum.substring(0, iPIndex + 1) + newCadNum.substring(iPIndex + 1, iPIndex + 3);
   }
   else
   {
    for(int i = newCadNum.length(); i < 3; i++)
     newCadNum = "0" + newCadNum;
    iLong = newCadNum.length();
    if(iLong == 3)
     szTemp = newCadNum.substring(0, 1);
    else
     szTemp = newCadNum.substring(0, iLong - 2);
    newCadNum = szTemp + "." + newCadNum.substring(iLong - 2);
   }

   return newCadNum;
  }

  private String delCommaPointFromString(String newCadNum)
  {
   String nCad = new String(newCadNum);
   if( nCad.indexOf(".") > -1)
   {
    nCad = nCad.substring(nCad.indexOf(".") + 1, nCad.length());
    if(nCad.length() != 2)
    {
     String szTemp = new String("");
     for(int j = nCad.length(); j < 2; j++)
      szTemp = szTemp + "0";
     newCadNum = newCadNum + szTemp;
    }
   }

   StringBuffer nCadNum = new StringBuffer(newCadNum);
   for(int i = 0; i < nCadNum.length(); i++)
    if(nCadNum.charAt(i) == ',' || nCadNum.charAt(i) == '.')
     nCadNum.deleteCharAt(i);

   return nCadNum.toString();
  }

  private String setFormat(String newCadNum)
  {
    int iPIndex = newCadNum.indexOf(".");
    String nCadNum = new String(newCadNum.substring(0, iPIndex));
    for (int i = 0; i < (int)((nCadNum.length()-(1+i))/3); i++)
     nCadNum = nCadNum.substring(0, nCadNum.length()-(4*i+3))+','+nCadNum.substring(nCadNum.length()-(4*i+3));

    return nCadNum + newCadNum.substring(iPIndex, newCadNum.length());
  }

%>
<html>
<head>
  <%@include file="style.jsf"%>
  <title>Datos de la Transaccion</title>
  <script language='javascript'>
<!--
 function validate(form){
<%
  for(int i=0; i < listaCampos.size(); i++){
    Vector vCampos = (Vector)listaCampos.get(i);
    vCampos = (Vector)vCampos.get(0);
    if( vCampos.get(4).toString().length() > 0 ){
      out.println("  if( !top.validate(window, document.entry." + vCampos.get(0) + ", '" + vCampos.get(4) +"') )");
      out.println("    return");
    }
   }
%>
<%if (datasession.get("lstForPago").toString().equals("02"))
	{%>
	  if( !top.validate(window, document.entry.txtRFC, 'isRFCCF') )
    	return
	  if( !top.validate(window, document.entry.txtIVA, 'isCurrency') )
	    return
  Cuenta = <%=(String)datasession.get("txtDDACuenta")%>
  top.openModalDialog('<%=request.getContextPath()%>/servlet/ventanilla.FirmasServlet?txtDDACuenta=' + Cuenta, 710, 430, 'top.setPrefs2()', document.entry)
<% }
else {
    out.println("  form.submit();");
    out.println("var obj = document.getElementById('btnCont');"); 
    out.println("obj.onclick = new Function('return false');");   
   }%>
}
//-->
  </script>
</head>
<body onload="top.setFieldFocus(window.document)">
<form name="entry" action="../servlet/ventanilla.DataServlet" method="post">
<h1><%=session.getAttribute("page.txnImage")%></h1>
<h3>
<%String iTxn = (String)datasession.get("iTxn");
  if(iTxn.equals("0031"))
   out.print("4089");
  else
   out.print(session.getAttribute("page.cTxn"));
  out.print("  " + (String)session.getAttribute("page.txnlabel"));
  String monto = new String();
  String comision = new String();
  String iva = new String();
  if(iTxn.equals("0031")){
   monto = setFormat(setPointToString(delCommaPointFromString((String)datasession.get("txtMonto"))));
   comision = new String("");
   if(datasession.get("montoComision") != null)
     comision = setFormat(setPointToString(delCommaPointFromString((String)datasession.get("montoComision"))));
   iva = new String("");
   if(datasession.get("montoIVA") != null)
     iva = setFormat(setPointToString(delCommaPointFromString((String)datasession.get("montoIVA"))));
   }
  else{
   monto = setFormat(setPointToString(delCommaPointFromString((String)datasession.get("montoTotal"))));
   }
%>
</h3>
<table>
	<tr>
		<td>Monto: </td>
                <td width="10">&nbsp;</td>
		<td align="right"><%=monto%></td>
	</tr>
<%
 if(comision.length() > 0){
   out.print("<tr>");
		out.println("<td>Comisi�n: </td>");
                out.println("<td width=\"10\">&nbsp;</td>");
		out.println("<td align=\"right\">" + comision + "</td>");
   out.print("</tr>");
   out.print("<tr>");
		out.println("<td>IVA: </td>");
                out.println("<td width=\"10\">&nbsp;</td>");
		out.println("<td align=\"right\">" + iva + "</td>");
   out.print("</tr>");
 }
 String lstForPago = (String)datasession.get("lstForPago");
 if(!lstForPago.equals("02")){
	out.println("<tr>");
		out.println("<td>Ordenante: </td>");
                out.println("<td width=\"10\">&nbsp;</td>");
		out.println("<td align=\"right\">" + (String)datasession.get("txtNomOrd") + "</td>");
	out.println("</tr>");
 }
%>
<%
  int vlong = listaCampos.size();

  for(int i =  0; i < vlong; i++)
   {
    Vector vCampos = (Vector)listaCampos.get(i);
    vCampos = (Vector)vCampos.get(0);
    if(!vCampos.get(0).toString().equals("txtCodSeg")){
     out.println("<tr>");
     out.println("  <td>" + vCampos.get(2) + ":</td>");
     out.println("  <td width=\"10\">&nbsp;</td>");
     if(!vCampos.get(0).toString().substring(0,3).equals("lst")){
      out.print  ("  <td><input maxlength=\"" + vCampos.get(3) + "\" name=\"" + vCampos.get(0) + "\" size=\"" + vCampos.get(3) +"\" type=\"text\"");
      String CampoR = (String)vCampos.get(5);
      if(CampoR != null && CampoR.length() > 0){
       CampoR = CampoR.trim();
        out.print(" value=\"" + CampoR + "\"");
      }
     }
     else{
      out.print("  <td><select name=\"" + vCampos.get(0) + "\" size=\"1\"");
      if( vCampos.get(4).toString().length() > 0 )
       out.println(" onChange=\"top.validate(window, this, '" + vCampos.get(4) +"')\">");
      else
       out.println(">");
      for(k=0; k < listaContenidos.size(); k++){
       Vector v1Campos = (Vector)listaContenidos.get(k);
       for(int j = 0; j < v1Campos.size(); j++){
        Vector v1Camposa = (Vector)v1Campos.get(j);
        if(v1Camposa.get(0).toString().equals(vCampos.get(0)))
         out.println("  <option value=\"" + v1Camposa.get(3) + "\">" + v1Camposa.get(2));
       }
      }
      out.println("  </select></td>");
     }
     if( vCampos.get(3).toString().length() > 0 && !vCampos.get(0).toString().substring(0,3).equals("lst"))
      out.println(" onBlur=\"top.estaVacio(this)||top.validate(window, this, '" + vCampos.get(4) +"')\"" + "onKeyPress=\"top.keyPressedHandler(window, this, '" + vCampos.get(4) +"')\"></td>");
     else if(!vCampos.get(0).toString().substring(0,3).equals("lst"))
      out.println(">");
     out.println("</tr>");
    }
    else{
     %><%@include file="fieldLector.jsf"%><%
     i += 3 ;
    }
   }
%>
<%
if (datasession.get("lstForPago").toString().equals("02"))
{%>
	<tr>
	<td>Requiere Comprobante Fiscal:</td>
  	<td width="10">&nbsp;</td>
  	<td><select name="lstCF" size="1">
  	<option value="0">NO
  	<option value="1">SI
  	</select></td>
	</tr>
	<tr>
  	<td>R.F.C.:</td>
  	<td width="10">&nbsp;</td>
  	<td><input maxlength="19" name="txtRFC" size="19" type="text" value="" onChange="top.validate(window, this, 'isRFCCF')"></td>
	</tr>
	<tr>
  	<td>Importe de I.V.A.:</td>
  	<td width="10">&nbsp;</td>
  	<td><input maxlength="11" name="txtIVA" size="14" type="text" value="" onChange="top.validate(window, this, 'isCurrency')"></td>
	</tr>
<%}%>
</table>
<input type="hidden" name="iTxn" value="<%= session.getAttribute("page.iTxn") %>">
<input type="hidden" name="oTxn" value="<%= session.getAttribute("page.oTxn") %>">
<input type="hidden" name="cTxn" value="<%= session.getAttribute("page.cTxn") %>">
<input type="hidden" name="sucursal" value="<%= session.getAttribute("branch") %>">
<input type="hidden" name="teller" value="<%= session.getAttribute("teller") %>">
<input type="hidden" name="moneda" value="<%= session.getAttribute("page.moneda")%>">
<input type="hidden" name="registro" value="<%=session.getAttribute("empno")%>">
<input type="hidden" name="tllrlvl" value="<%=session.getAttribute("tellerlevel")%>">
<input type="hidden" name="supervisor" value="<%= session.getAttribute("teller") %>">
<input type="hidden" name="AuthoOK" value="0">
<input type="hidden" name="SignOK" value="0">
<input type="hidden" name="validateFLD" value="0">
<input type="hidden" name="compania" value="20">
<input type="hidden" name="needSignature" value="<%out.print(txnAuthLevel);%>">
<input type="hidden" name="needAutho" value="<%out.print(txnIMGAutoriz);%>">
<input type="hidden" name="SignField" value="<%= session.getAttribute("page.SignField") %>">
<input type="hidden" name="txtInvoqued" value="01">
<p>
<%
  out.println("<a id='btnCont' href=\"javascript:validate(document.entry);\"><img src=\"../ventanilla/imagenes/b_continuar.gif\" border=\"0\"></a>");
%>
</form>
</body>
</html>