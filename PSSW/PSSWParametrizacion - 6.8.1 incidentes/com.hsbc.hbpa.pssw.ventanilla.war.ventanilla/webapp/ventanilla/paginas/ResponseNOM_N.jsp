<!-- 
//**********************************************************************************************************
// CCN - 4360456 - 07/04/2006 - Se realizan correcciones para autorización remota en ctas con estatus 3 y 4.
// CCN - 4360581 - 12/03/2007 - Se convierten los estilos en jsf y se hace un include en el jsp
//**********************************************************************************************************
-->
<%@ page session="true" import="java.util.Hashtable"%>
<%
response.setHeader("Cache-Control","no-store"); //HTTP 1.1
response.setHeader("Pragma","no-cache"); //HTTP 1.0
response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<!--<META HTTP-EQUIV="REFRESH" content="5"> -->
<html>
<head>
  <title>Results</title>
  <%@include file="style.jsf"%>
</head>
<Script language="JavaScript">
function chgPag(redirection)
{
	document.nomina.action = redirection;
	document.nomina.submit();
}
</Script>
<body>
<%
  String status = "*";
  Hashtable datos = (Hashtable)session.getAttribute("page.datasession");
  String needOvrr = "NO";
  String redirection = "";

  String codec = (String)datos.get("codecargo");
  String codea = (String)datos.get("codeabono");

  out.println("<br><Strong>Realizando proceso de Nomina, por favor espere...</Strong>");
  out.println("<br>");
  if (codec.equals("0"))
  {
    out.println("<br><font color=blue><Strong>Cargo aceptado ...</Strong></font>");
    out.println("<br>");
    Integer count = new Integer(0);
    if (datos.get("conteo")!= null)
      count = (Integer)datos.get("conteo");
    Integer size  = new Integer(0);
    if (datos.get("numero")!=null)
      size = (Integer)datos.get("numero");
    if(!(codea.equals("X")))
    {
      if (codea.equals("0"))
        status = "<font color=green><strong>Aceptado</strong></font>";
      else
        if(!codea.equals("3")){         
        status = "<font color=red><strong>Rechazado</strong></font>";
      out.println("<br><font color=blue><strong>Abonos procesados: " + count + " de " + size + " - " + status + "</strong></font>");
    }
        else
        {
          out.println("<b>Transacci&oacute;n Requiere Autorizaci&oacute;n</b>");
        }
    }
  }
  else
  {
    out.println("<br><font color=red>El cargo fue rechazado, imprima la certificacion correspondiente...</font>");
    out.println("<script language=\"JavaScript\">alert('El cargo fue rechazado, imprima la certificacion correspondiente...');</script>");
  }
%>
  <form name="nomina" action="" method="Post">
<%
  out.println("<br>");
  String cend = (String)datos.get("end");

  if(!codea.equals("3")){
  if(cend.equals("1"))
  {
    datos.remove("ctas");
    datos.remove("size");
    datos.remove("mtos");
    datos.remove("codecargo");
    datos.remove("numero");
    datos.remove("conteo");
    datos.remove("codeabono");
    datos.remove("Scuentas");
    datos.remove("Smontos");

    session.setAttribute("page.datasession",datos);
    	out.println("<br><font color=blue><strong>Proceso terminado...</strong></font>");
    	redirection = "ResponseNOM1.jsp";
      }
      else
        redirection = "../../servlet/ventanilla.DataServlet";
 %> 
   <script language="JavaScript" >
   chgPag('<%=redirection%>');
   </script>
<%}
  else
  {
    needOvrr = "SI";
    out.print("<script languaje=\"javascript\"> top.openDialog('authoriz.jsp', 220, 130, 'top.setPrefs8()', document.nomina) </script>");
  }
%>
      <input type="hidden" name="inpSubmit">
  <input type="hidden" name="supervisor" value="<%=datos.get("supervisor").toString()%>">
  <input type="hidden" name="override" value="<%=needOvrr%>">
    </form>
</body>
</html>
