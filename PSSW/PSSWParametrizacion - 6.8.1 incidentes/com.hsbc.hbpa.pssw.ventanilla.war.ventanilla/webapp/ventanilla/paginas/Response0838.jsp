<!--
//*************************************************************************************************
///            Funcion: JSP que despliegarespuesta de txn 0838
//            Elemento: Response0838.jsp
//          Creado por: Fausto Rodrigo Flores Moreno
//*************************************************************************************************
// CCN - 4360574 - 09/03/2007 - Se crea response para mostrar respuesta de consulta 0838
//*************************************************************************************************
-->

<%@ page session="true" import="java.util.*, ventanilla.GenericClasses"%>
<%
response.setHeader("Cache-Control","no-store"); //HTTP 1.1
response.setHeader("Pragma","no-cache"); //HTTP 1.0
response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<%!
  private String getString(String s)
  {
   int len = s.length();
   for(; len>0; len--)
    if(s.charAt(len-1) != ' ')
     break;
   return s.substring(0, len);
  }
 %>
<%
Vector resp = (Vector)session.getAttribute("response");
Hashtable info = (Hashtable)session.getAttribute("info");
String pcdOK = (String)session.getAttribute("pcd");
Hashtable datasession = (Hashtable)session.getAttribute("page.datasession");

int k;
%>
<html>
<head>
  <title>Certificación</title>
  <style type="text/css">Body {font-family: Arial, Helv; font-size: 10pt; font-weight: bold; color:#000000}</style>
</head>
<body bgcolor="#FFFFFF">
<p>
<%
  java.util.Stack flujotxn = new java.util.Stack();

  String szResp = "";
  String codigo = "";
  String needOverride = "NO";
  String txn = session.getAttribute("page.cTxn").toString();

  if(session.getAttribute("page.flujotxn") != null)
  	flujotxn = (java.util.Stack)session.getAttribute("page.flujotxn");

   if( resp.size() != 4 )
    out.println("<b>Error de conexi&oacute;n</b><br>");
   else
   {
     codigo = (String)resp.elementAt(0);
     if( !codigo.equals("0") )
       out.println("<b>Error en transaccion</b><br>");
     int lines = Integer.parseInt((String)resp.elementAt(1));
     szResp = (String)resp.elementAt(3);
     for(int i=0; ; i++){
       String line = szResp.substring( i*78, Math.min(i*78+77, szResp.length()) );
       out.println(getString(line) + "<p>");
       if(Math.min(i*78+77, szResp.length()) == szResp.length())
         break;
     }
   }
   
//obtención de la información valores y flags	

	if(codigo.equals("0")){
		GenericClasses gc = new GenericClasses();

		String MontoOrg = gc.quitap(info.get("txtTot").toString());
		String Monto_D_R= gc.quitap(gc.FormatAmount(szResp.substring(155,169)));
		String FechaL = "20"+szResp.substring(169,175);
		String DiasVencidos = String.valueOf(Integer.parseInt(szResp.substring(175,178)));
		String MontoPagar = gc.quitap(gc.FormatAmount(szResp.substring(178,szResp.length())));
		String Desc_Recargo = "";
		
		if(Integer.parseInt(MontoOrg)>Integer.parseInt(MontoPagar))
			Desc_Recargo="D";
		else if(Integer.parseInt(MontoOrg)<Integer.parseInt(MontoPagar))
			Desc_Recargo="R";
		else if(Integer.parseInt(MontoOrg)==Integer.parseInt(MontoPagar))
			Desc_Recargo="N";
		
	
		info.put("txtReferencia", datasession.get("txtReferRapCal").toString());
		info.put("MontoOrg",MontoOrg);
	
		datasession.put("Desc_Recargo",Desc_Recargo);
		datasession.put("MontoPagar",MontoPagar);
		datasession.put("FechaL",FechaL);
		datasession.put("Monto_D_R",Monto_D_R);
		datasession.put("DiasVencidos",DiasVencidos);
		datasession.put("Desc_Recargo",Desc_Recargo);
		
		session.setAttribute("info",info);
		session.setAttribute("page.datasession",datasession);
		
		out.println("<b>Para redireccionar a PageBuilderRAPCal...</b>");
		response.sendRedirect("../paginas/PageBuilderRAPCal.jsp");
	}
%>
</p>
</form>
</body>
</html>
