<!--
//*************************************************************************************************
//             Funcion: JSP que presenta las opciones de la txn 0604
//            Elemento: PageBuilder0604.jsp
//          Creado por: Alejandro Gonzalez Castro
//      Modificado por: Juvenal R. Fernandez V.
//*************************************************************************************************
// CCN - 4360275 - 18/02/2005 - WAS 5.1
// CCN - 4360334 - 17/06/2005 - Se convierte fieldLector a fragmento
// CCN - 4360581 - 12/03/2007 - Se convierten los estilos en jsf y se hace un include en el jsp
// CCN -         - 10/01/2008 - Se hacen modificaciones para transacción global de cheques locales
//*************************************************************************************************
-->

<%@ page session="true"  import="java.util.*"%>
<%
response.setHeader("Cache-Control","no-store"); //HTTP 1.1
response.setHeader("Pragma","no-cache"); //HTTP 1.0
response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<%
Vector listaCampos = (Vector)session.getAttribute("page.listcampos");
Vector listaContenidos = (Vector)session.getAttribute("page.listcont");
String txnAuthLevel = (String)session.getAttribute("page.txnAuthLevel");
String txnIMGAutoriz = (String)session.getAttribute("page.txnIMGAutoriz");
Vector v = (Vector)session.getAttribute("page.specialacct");
int k;
Hashtable datasession = (Hashtable)session.getAttribute("page.datasession");
// out.println("hashtable datasession : " + datasession);
if (datasession == null)
   datasession = new Hashtable();
String RAP = "";
if(datasession.get("flujorap") != null)
{
	RAP = datasession.get("flujorap").toString();
}
%>
<html>
<head>
  <%@include file="style.jsf"%>
  <title>Datos de la Transaccion </title>
 <script  language="JavaScript">
<!--
var formName = 'Mio'
var specialacct = new Array()
var autoriza = 0;
<%
 if(!v.isEmpty()){
  for(int iv = 0; iv < v.size(); iv++){
   out.print("specialacct[" + iv + "]='" + v.get(iv) + "'" + '\n');
  }
 }
  String MontoPro;
  String MontoRAP = (String)datasession.get("txtDocsCI");
  if ( (String)datasession.get("MontoTotal") == null)
       MontoPro = "00";
  else
       MontoPro = (String)datasession.get("MontoTotal");
  MontoPro = formatMonto(MontoPro);

  out.print("MontoPro='" + MontoPro + "';" +'\n');
  out.print("MontoRAP='" + MontoRAP + "';" +'\n');
%>

<%!
  private String formatMonto(String s)
  {
    int len = s.length();
    if (len < 3)
    	s = "0." + s;
    else
    {
      int n = (len - 3 ) / 3;
    	s = s.substring(0, len - 2) + "." + s.substring(len - 2 , len);
      for (int i = 0; i < n; i++)
      {
      	len = s.length();
    		s = s.substring(0, (len -(i*3+6+i))) + "," + s.substring((len -(i*3+6+i)) , len);
    	}
    }
    return s;
  }
%>

function validate(form)
{
//if  ( form.txtCodSeg.value.length == 0 || form.txtCveTran.value.length == 0 || form.txtDDACuenta2.value.length == 0 || form.txtSerial2.value.length == 0 ) {
//      alert("Introduzca Banda del Cheque.")
//      return;
// }
<%
for(int i=0; i < listaCampos.size(); i++)
{
    Vector vCampos = (Vector)listaCampos.get(i);
    vCampos = (Vector)vCampos.get(0);
    if( vCampos.get(4).toString().length() > 0 && !vCampos.get(0).toString().equals("txtDocsLoc"))
    {
      if ( vCampos.get(0).equals("rdoACorte")) {
         if (MontoPro.equals("0.00")) {
            out.println("  if( !top.validate(window, document.entry." + vCampos.get(0) + ", '" + vCampos.get(4) +"') )");
            out.println("    return ");
            }
      }
      else {
         out.println("  if( !top.validate(window, document.entry." + vCampos.get(0) + ", '" + vCampos.get(4) +"') )");
         if (vCampos.get(1).equals("txtCodSeg") || vCampos.get(1).equals("txtCveTran") || vCampos.get(1).equals("txtDDACuenta2") || vCampos.get(1).equals("txtSerial2")) {
             out.println(" {    if ( autoriza==0 )");
             out.println("          autoriza = 1;   }");
         }
         else {
             out.println("    return");
         }
      }
    }
}
%>

if( !top.validate(window, document.entry.txtMonto, 'isMontoRAP') )
   		return

form.validateFLD.value = '1'
if ( autoriza == 1 )
{
	form.onSubmit="return validate(document.entry);"
    document.entry.needAutho.value = '1';
    AuthoandSignature(document.entry);
}

<% if(!txnAuthLevel.equals("1") && !txnIMGAutoriz.equals("1"))
   {
	 out.println("if( autoriza != 1 ){");
    out.println("   form.submit() ");
    out.println("var obj = document.getElementById('btnCont');"); 
    out.println("obj.onclick = new Function('return false');}");   
   }
%>
}
function AuthoandSignature(entry)
{
 var Cuenta;
 if(entry.validateFLD.value != '1')
  return
 if(entry.needAutho.value == '1' && entry.AuthoOK.value != '1'){
  top.openDialog('../paginas/authoriz.jsp', 220, 130, 'top.setPrefs()', entry)
 }
 if(entry.needSignature.value == '1' && entry.SignField.value != ''){
   if (entry.txtDDACuenta2.value == '00103000023' || entry.txtDDACuenta2.value == '03902222222' )
       Cuenta = eval('entry.txtFirmaFun.value')
   else
       Cuenta = eval('entry.' + entry.SignField.value + '.value')
  top.openModalDialog('<%=request.getContextPath()%>/servlet/ventanilla.FirmasServlet?txtDDACuenta=' + Cuenta, 710, 430, 'top.setPrefs2()', entry)
 }
}
//-->
</script>
</head>
<body bgcolor="#FFFFFF"  onload="top.setFieldFocus(window.document)">
<br>
<% out.print("<form name=\"entry\" action=\"../servlet/ventanilla.DataServlet\" method=\"post\" ");
   if(txnAuthLevel.equals("1"))
    out.print("onSubmit=\"return validate(this)\"");
   out.println(">");
%>
<h1><%=session.getAttribute("page.txnImage")%></h1>

<h3><%out.print(session.getAttribute("page.cTxn")+"  "+(String)session.getAttribute("page.txnlabel"));%></h3>

<table border="0" cellspacing="0">
<%
	  int i = 0;
	  for(i =  0; i < listaCampos.size(); i++)
	   {
	    Vector vCampos = (Vector)listaCampos.get(i);
	    vCampos = (Vector)vCampos.get(0);
	    if(!vCampos.get(0).toString().equals("txtCodSeg")){
	     out.println("<tr>");
	     out.println("  <td>" + vCampos.get(2) + ":</td>");
	     out.println("  <td width=\"10\">&nbsp;</td>");
	     if(!vCampos.get(0).toString().equals("txtDocsLoc"))
	     {
			     if(!vCampos.get(0).toString().substring(0,3).equals("lst")){
					  if(vCampos.get(0).toString().substring(0,3).equals("pss")){
					    out.print  ("  <td><input maxlength=\"" + vCampos.get(3) + "\" name=\"" + vCampos.get(0) + "\" size=\"" + vCampos.get(3) +"\" type=\"Password\"");
					  }
		        else{
		          out.print("  <td><input maxlength=\"" + vCampos.get(3) + "\" name=\"" + vCampos.get(0) + "\" size=\"" + vCampos.get(3) +"\" type=\"text\"");
		        }
			      String CampoR = (String)vCampos.get(5);
			      if(CampoR != null && CampoR.length() > 0){
			        CampoR = CampoR.trim();
			        out.print(" value=\"" + CampoR + "\"");
			      }
			     }
			     else{
			      out.print("  <td><select name=\"" + vCampos.get(0) + "\" size=\"" + "1" + "\"");
			      if( vCampos.get(3).toString().length() > 0 ){
			       if(vCampos.get(0).toString().substring(0,3).equals("rdo"))
		           out.println(" onClick=\"top.validate(window, this, '" + vCampos.get(4) +"')\">");
			       else
			         out.println(" onChange=\"top.validate(window, this, '" + vCampos.get(4) +"')\">");
			      }
			      else
			       out.println(">");
			      for(k=0; k < listaContenidos.size(); k++){
			       Vector v1Campos = (Vector)listaContenidos.get(k);
			       for(int j = 0; j < v1Campos.size(); j++){
			         Vector v1Camposa = (Vector)v1Campos.get(j);
			         if(v1Camposa.get(0).toString().equals(vCampos.get(0).toString()))
			           out.println("  <option value=\"" + v1Camposa.get(3) + "\">" + v1Camposa.get(2));
			       }
			      }
			      out.println("  </select></td>");
			     }
			     if( vCampos.get(4).toString().length() > 0 && !vCampos.get(0).toString().substring(0,3).equals("lst"))
			      out.println(" onChange=\"top.validate(window, this, '" + vCampos.get(4) +"')\"></td>");
			     else if(!vCampos.get(0).toString().substring(0,3).equals("lst"))
			      out.println(">");
			     out.println("</tr>");
			}
			else
			out.println("  <td>" + (String)datasession.get("txtNumPropios") + "</td></tr>");
	    }
	    else{
	     %><%@include file="fieldLector.jsf"%><%
	     i = k;
	    }
	   }
	%>

<%
String nopagos =  (String)datasession.get("npagos");
int npag = Integer.parseInt(nopagos);
if(npag > 1)
{%>
    <tr>
    <td>Monto:</td>
    <td width="10">&nbsp;</td>
    <td><%=MontoRAP%></td>
    <input name="txtMonto"type="hidden" value="<%=MontoRAP%>">
    </tr>
<%
}
else
{%>
    <tr>
    <td>Monto:</td>
    <td width="10">&nbsp;</td>
    <td><input maxlength="11" tabindex="<%=(i+1)%>" name="txtMonto" size="14" type="text" value="" onBlur="top.validate(window, this, 'isMontoRAP')" onKeyPress="top.estaVacio(this)||top.keyPressedHandler(window, this, 'isMontoRAP')" ></td>
    </tr>
    <tr><td><b> Monto Procesado :</b></td><td width="10">&nbsp;</td>
    <td><%=MontoPro%></td></tr>
    <tr><td><b>Monto Total :</b></td><td width="10">&nbsp;</td>
    <td><%=MontoRAP%></td></tr>
<%}%>
</table>
<p>
<input type="hidden" name="iTxn" value="<%= session.getAttribute("page.iTxn") %>">
<input type="hidden" name="oTxn" value="<%= session.getAttribute("page.oTxn") %>">
<input type="hidden" name="cTxn" value="<%= session.getAttribute("page.cTxn") %>">
<input type="hidden" name="sucursal" value="<%= session.getAttribute("branch") %>">
<input type="hidden" name="teller" value="<%= session.getAttribute("teller") %>">
<input type="hidden" name="moneda" value="<%= session.getAttribute("page.moneda")%>">
<input type="hidden" name="registro" value="<%=session.getAttribute("empno")%>">
<input type="hidden" name="tllrlvl" value="<%=session.getAttribute("tellerlevel")%>">
<input type="hidden" name="supervisor" value="<%= session.getAttribute("teller") %>">
<input type="hidden" name="txtDocsLoc" value="<%= (String)datasession.get("txtNumPropios")%>">
<input type="hidden" name="AuthoOK" value="0">
<input type="hidden" name="SignOK" value="0">
<input type="hidden" name="compania" value="20">
<input type="hidden" name="validateFLD" value="0">
<input type="hidden" name="needSignature" value="<%out.print(txnIMGAutoriz);%>">
<input type="hidden" name="needAutho" value="<%out.print(txnAuthLevel);%>">
<input type="hidden" name="SignField" value="<%= session.getAttribute("page.SignField") %>">
<p>
<% if(txnAuthLevel.equals("1") || txnIMGAutoriz.equals("1"))
    out.println("<a id='btnCont' tabindex=\""+(i+1)+"\" href=\"javascript:validate(document.entry);javascript:AuthoandSignature(document.entry)\"><img src=\"../ventanilla/imagenes/b_continuar.gif\" border=\"0\"></a>");
   else
    out.println("<a id='btnCont' tabindex=\""+(i+1)+"\" href=\"javascript:validate(document.entry)\"><img src=\"../ventanilla/imagenes/b_continuar.gif\" border=\"0\"></a>");
%>
</form>
</body>
</html>
