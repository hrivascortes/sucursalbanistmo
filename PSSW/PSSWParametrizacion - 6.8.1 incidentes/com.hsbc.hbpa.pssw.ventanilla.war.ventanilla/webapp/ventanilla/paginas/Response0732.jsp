<!--
//*************************************************************************************************
//             Funcion: JSP que despliega respuesta de txn 0732
//            Elemento: Response0732.jsp
//          Creado por: Alejandro Gonzalez Castro
//      Modificado por: Alejandro Gonzalez Castro
//      Modificado por: Fredy Peña Moreno
//*************************************************************************************************
// CCN - 4360189 - 03/09/2004 - Se redirecciona a Final.jsp por Control de Efectivo
// CCN - 4360237 - 05/11/2004 - Se controla envio de forma
// CCN - 4360275 - 18/02/2005 - WAS 5.1
// CCN - 4620008 - 14/09/2007 - Se modifica flujo para evitar el cobro de iva	
//*************************************************************************************************
-->

<%@ page session="true" import="java.util.*"%>
<%
response.setHeader("Cache-Control","no-store"); //HTTP 1.1
response.setHeader("Pragma","no-cache"); //HTTP 1.0
response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<%!
	private String addpunto(String valor)
	{
	int longini = valor.length();
    String cents  = valor.substring(longini-2,longini);
	String entero = valor.substring(0,longini-2);
	int longente = entero.length();
	for (int i = 0; i < (longente-(1+i))/3; i++)
	{
		longente = entero.length();
	    entero = entero.substring(0,longente-(4*i+3))+','+entero.substring(longente-(4*i+3));
	}
  	entero = entero + '.' + cents;
	return entero;
	}

  private String getdomi(String r)
  {
		String domicilio = r.substring(77,108);
		return domicilio;
  }
  private String getString(String s)
  {
    int len = s.length();
    for(; len>0; len--)
	  if(s.charAt(len-1) != ' ')
	    break;

    return s.substring(0, len);
  }

  private String getItem(String newCadNum, int newOption)
  {
   int iPIndex = newCadNum.indexOf("CONSECUTIVO:") + 12, NumSaltos, Num = 0, Ban = 0;
   String nCadNum = new String("");
   NumSaltos = newOption;
   while(Num < NumSaltos)
   {
    if(newCadNum.charAt(iPIndex) == 0x20)
    {
     while(newCadNum.charAt(iPIndex) == 0x20)
      iPIndex++;
     Num++;
    }
    else
     while(newCadNum.charAt(iPIndex) != 0x20)
      iPIndex++;
   }
   while(newCadNum.charAt(iPIndex) != 0x20 && iPIndex < newCadNum.length()){
    nCadNum = nCadNum + newCadNum.charAt(iPIndex);
    if(iPIndex < newCadNum.length())
      iPIndex++;
    if(iPIndex == newCadNum.length())
      break;
   }
   return nCadNum;
  }

  private String delLeadingfZeroes(String newString)
  {
   StringBuffer newsb = new StringBuffer(newString);
   int Len = newsb.length();
   for(int i=0; i<newsb.length(); i++)
   {
    if(newsb.charAt(i) != '0')
    {
	 newsb.delete(0,i);
	 break;
	}
   }
   return newsb.toString();
  }

  private String setPointToString(String newCadNum)
  {
   int iPIndex = newCadNum.indexOf("."), iLong;
   String szTemp;

   if(iPIndex > 0)
   {
    newCadNum = new String(newCadNum + "00");
    newCadNum = newCadNum.substring(0, iPIndex + 1) + newCadNum.substring(iPIndex + 1, iPIndex + 3);
   }
   else
   {
    for(int i = newCadNum.length(); i < 3; i++)
     newCadNum = "0" + newCadNum;
    iLong = newCadNum.length();
    if(iLong == 3)
     szTemp = newCadNum.substring(0, 1);
    else
     szTemp = newCadNum.substring(0, iLong - 2);
    newCadNum = szTemp + "." + newCadNum.substring(iLong - 2);
   }

   return newCadNum;
  }

  private String delPointOfString(String newCadNum)
  {
   int iPIndex = newCadNum.indexOf(".");

   if(iPIndex > 0)
   {
    newCadNum = newCadNum.substring(0, iPIndex) +
                newCadNum.substring(iPIndex + 1, newCadNum.length());
   }

   return newCadNum;
  }

  private String delCommaPointFromString(String newCadNum)
  {
   String nCad = new String(newCadNum);
   if( nCad.indexOf(".") > -1)
   {
    nCad = nCad.substring(nCad.indexOf(".") + 1, nCad.length());
    if(nCad.length() != 2)
    {
     String szTemp = new String("");
     for(int j = nCad.length(); j < 2; j++)
      szTemp = szTemp + "0";
     newCadNum = newCadNum + szTemp;
    }
   }

   StringBuffer nCadNum = new StringBuffer(newCadNum);
   for(int i = 0; i < nCadNum.length(); i++)
    if(nCadNum.charAt(i) == ',' || nCadNum.charAt(i) == '.')
     nCadNum.deleteCharAt(i);

   return nCadNum.toString();
  }

  private String setFormat(String newCadNum)
  {
    int iPIndex = newCadNum.indexOf(".");
    String nCadNum = new String(newCadNum.substring(0, iPIndex));
    for (int i = 0; i < (int)((nCadNum.length()-(1+i))/3); i++)
     nCadNum = nCadNum.substring(0, nCadNum.length()-(4*i+3))+','+nCadNum.substring(nCadNum.length()-(4*i+3));

    return nCadNum + newCadNum.substring(iPIndex, newCadNum.length());
  }

  private String longtoString(long newlong){
    String newString = String.valueOf(newlong);
    return newString.toString();
  }

%>
<%
  Vector resp = (Vector)session.getAttribute("response");
  Hashtable datasession = (Hashtable)session.getAttribute("page.datasession");
%>
<html>
<head>
  <title>Certificación</title>
<style type="text/css">Body {font-family: Arial, Helv; font-size: 10pt; font-weight: bold; color:#000000}</style>
</head>
<script language="JavaScript">
function aceptar(forma)
{
	forma.submit();
    var obj = document.getElementById('btnCont');
    if(obj !=null)
    	obj.onclick = new Function('return false');
}
function cancelar(forma)
{
	  document.Txns.action = "../paginas/Final.jsp";
	  document.Txns.submit();
}
function continuar(forma)
{
  alert("documento Impreso...");
}
function imprime()
{
	if(!window.confirm("Desea Imprimir Comprobante."))
	{
	  document.Txns.action = "paginas/ayuda.html";
	  document.Txns.submit();
	}
}
function changeaction()
{
    document.Txns.action = "../../servlet/ventanilla.PageServlet";
}
</script>
<body>
<%
  String cTxn = (String)datasession.get("cTxn");
  String oTxn = (String)datasession.get("oTxn");
  String ftime = (String)session.getAttribute("ftime");
   if(ftime.equals("2") || ftime.equals("3")){
   out.println("<form name=\"Txns\" action=\"../../servlet/ventanilla.DataServlet\" method=\"post\">");
   out.println("<input type=\"hidden\" name=\"txtInvoqued\" value=\"" + "somedatahere" + "\">");
  }
  else
  { out.println("<form name=\"Txns\"  method=\"post\">"); }
%>
<table border="0" cellspacing="0">
<tr>
<td>
</td>
</tr>
<tr>
<td>
  <input type="hidden" name="transaction" value="<%= session.getAttribute("page.iTxn") %>">
  <input type="hidden" name="transaction1" value="<%= session.getAttribute("page.oTxn") %>">
</td>
</tr>
</table>
<p>
<%
   String szResp = new String("");
   int codigo = 1;
   if( resp.size() != 4 )
    out.println("<b>Error de conexi&oacute;n</b><br>");
   else
   {
    try{
     codigo = Integer.parseInt((String)resp.elementAt(0));
    }
    catch(NumberFormatException nfe){
     out.println("<b>Error de conexi&oacute;n</b><br>");
    }
    if( codigo != 0 )
      out.println("<b>Error en transaccion</b><br>");
     int lines = Integer.parseInt((String)resp.elementAt(1));
     szResp = (String)resp.elementAt(3);
     for(int i=0; ; i++){
      String line = szResp.substring( i*78, Math.min(i*78+77, szResp.length()) );
      out.println(getString(line) + "<p>");
      if(Math.min(i*78+77, szResp.length()) == szResp.length())
       break;
     }
   }
%>
<%
if (codigo == 0)
{
  if(ftime.equals("3"))
  {
	Stack flujotxn = (Stack)session.getAttribute("page.flujotxn");
	if (!flujotxn.empty())
	{
		String newtxn = flujotxn.pop().toString();
		datasession.put("cTxn",newtxn);
                if (flujotxn.empty())
                   flujotxn.push("0000");
        	if(newtxn.equals("0000"))
                   flujotxn.pop();
	}
        else
        {session.setAttribute("ftime", "4");}
	   out.print("<div align=\"center\">");
	   out.print("</div>");

  }

  if(ftime.equals("2") && !cTxn.equals("0456") && !cTxn.equals("0458") && !cTxn.equals("0468"))
 {
   String txtComision = getItem(szResp, 3);
   String txtPIVA = getItem(szResp, 4);
   txtPIVA = "0";
   String txtdomici = getdomi(szResp);
   int intcom = Integer.parseInt(txtComision);
   int intiva = Integer.parseInt(txtPIVA);
   if (intcom > 0 && intiva > 0)
   {
   	datasession.put("txtComision", delLeadingfZeroes(txtComision));
   	datasession.put("txtPIVA", delLeadingfZeroes(txtPIVA));
   	datasession.put("txtdomici", delLeadingfZeroes(txtdomici));
   	session.setAttribute("page.datasession", datasession);
	datasession.put("CobCom","si");
	}else if(intcom > 0 && (cTxn.equals("0360") || cTxn.equals("0352")))
	{
		datasession.put("txtComision", delLeadingfZeroes(txtComision));
   		datasession.put("txtPIVA", delLeadingfZeroes(txtPIVA));
   		datasession.put("txtdomici", delLeadingfZeroes(txtdomici));
   		session.setAttribute("page.datasession", datasession);
		datasession.put("CobCom","si");
	}
	else
	{
	 datasession.put("CobCom","no");
	}
	if (datasession.get("CobCom").toString().equals("si"))
	{
%>
	   <table border="0" cellspacing="0">
	   <tr>
	   <td>Monto:</td>
	   <td></td>
	   <td  align="right"><%
	   long montoComision = Long.parseLong(datasession.get("txtComision").toString());
	   long montoTxn = Long.parseLong(delCommaPointFromString((String)datasession.get("txtMonto")));
	   long efectivo = Long.parseLong(delCommaPointFromString((String)datasession.get("txtEfectivo")));
	   //long iva = Long.parseLong((String)datasession.get("txtPIVA"));
	   long iva = 0;
	   long montoIVA = (montoTxn * iva) / 100;
	   //long montoIVA = 0;
	   //long montoTotal = montoComision + montoTxn + montoIVA;
	   long montoTotal = montoComision + montoTxn;
	   out.print(setFormat(setPointToString(longtoString(montoTxn))));
	   //datasession.put("txtMtoIva",setFormat(setPointToString(longtoString(montoIVA))));
	   %></td>
	   </tr>
	   <tr>
	   <!--<tr>
	   <td>Referencia:</td>
	   <td></td>
	   <td align="right">IVA</td>
	   </tr>-->
	   <tr>
	   <td>Efectivo:</td>
	   <td></td>
	   <td align="right"><%
	   out.print(setFormat(setPointToString(longtoString(efectivo))));
	   %></td>
	   </tr>
	   <!--<tr>
	   <td>Monto Total:</td>
	   <td></td>
	   <td align="right">setFormat(setPointToString(longtoString(montoIVA)))</td>
	   <td align="right">--><%
	   //out.print(setFormat(setPointToString(longtoString(montoTotal))));
	   %><!--</td>
	   </tr>-->
	   <tr>
	   <td><br></br><a id="btnCont" tabindex = "1" href="JavaScript:aceptar(document.Txns) "><img src="../imagenes/b_continuar.gif" border="0"></a></td>
	   <td><br></br><a tabindex = "2" href="JavaScript:cancelar(document.Txns) "><img src="../imagenes/b_cancelar.gif" border="0"></a></td>
	   </tr>
	   </table>
	   
	   <script language="javascript">
	
	   var liga = document.getElementById("btnCont");
	   liga.focus();
		
	   </script>

	<%String isCertificable = (String)session.getAttribute("page.certifField");  // Para Certificación
	}%>



<%}%>

<%
  long efectivo = Long.parseLong(delCommaPointFromString((String)datasession.get("txtEfectivo")));
  long monto = Long.parseLong(delCommaPointFromString((String)datasession.get("txtMonto")));
  if(ftime.equals("2") || cTxn.equals("0456") || cTxn.equals("0458") || cTxn.equals("0468"))
  {
        if (efectivo == 0 || efectivo < monto)
        {
            long docs = monto - efectivo;
            datasession.put("txtCheque", addpunto(Long.toString(docs)));
            Stack flujotxn = (Stack)session.getAttribute("page.flujotxn");
            String txn = (String)session.getAttribute("page.cTxn");
            txn = "S" + txn.substring(1);
            String[] newflujo = {txn,"5353"};
            for (int i=0; i<newflujo.length; i++)
            { flujotxn.push(newflujo[i]);}
%>
        <Script language='JavaScript'>
            changeaction();
        </SCRIPT>
<%      }
// Datos para Descarga *************************************************
  }
%>

<%
if (!ftime.equals("2"))
{
  String isCertificable = (String)session.getAttribute("page.certifField");  // Para Certificación
  String txtCadImpresion  = ""; // Para Certificación
   if(!isCertificable.equals("N")){ // Para Certificación
    session.setAttribute("Consecutivo", getItem((String)resp.elementAt(3), 1)); // Para Certificación
    session.setAttribute("Tipo", isCertificable); // Para Certificación
    session.setAttribute("txtCadImpresion", txtCadImpresion); // Para Certificación
    if ( cTxn.startsWith("08")) //Transacción de consulta
		out.println("<a href=\"#\" onClick=\"top.openDialog('../../servlet/ventanilla.ImprimirServlet', 160, 120, 'top.setPrefs4()', Txns)\"><img src=\"../imagenes/b_continuar.gif\" border=\"0\"></a>"); // Para Certificación
    else
		out.println("<script language=\"javascript\">top.openDialog('../../servlet/ventanilla.ImprimirServlet', 160, 120, 'top.setPrefs4()', document.Txns)</script>"); // Para Certificación
   }
}
else if (cTxn.equals("0352") || cTxn.equals("0360")){
       String txtdomici = getdomi(szResp);
       if (txtdomici.equals("                               ") )
	       out.println("<script language=\"javascript\">top.openDialog('../../servlet/ventanilla.ImprimirServlet', 160, 120, 'top.setPrefs4()', document.Txns)</script>"); // Para Certificación
       }
%>

<%  if ((cTxn.equals("0456") || cTxn.equals("0458") || cTxn.equals("0468")) && (efectivo != monto))
    {%>
        <SCRIPT>
            aceptar(document.Txns);
        </SCRIPT>
<%  }
}%>
</form>
</body>
</html>
