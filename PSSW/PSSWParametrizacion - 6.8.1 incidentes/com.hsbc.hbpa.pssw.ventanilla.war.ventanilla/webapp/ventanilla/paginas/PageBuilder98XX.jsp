<!-- 
// CCN - 4360581 - 12/03/2007 - Se convierten los estilos en jsf y se hace un include en el jsp
-->

<%@ page session="true" import="java.util.*"%>
<%
response.setHeader("Cache-Control","no-store"); //HTTP 1.1
response.setHeader("Pragma","no-cache"); //HTTP 1.0
response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<%
String MontoPro = new String("");
Vector listaCampos = (Vector)session.getAttribute("page.listcampos");
Vector listaContenidos = (Vector)session.getAttribute("page.listcont");
String txnAuthLevel = (String)session.getAttribute("page.txnAuthLevel");
String txnIMGAutoriz = (String)session.getAttribute("page.txnIMGAutoriz");
Vector v = (Vector)session.getAttribute("page.specialacct");
Hashtable datasession = (Hashtable)session.getAttribute("page.datasession");
int k;
%>
<html>
<head>
  <%@include file="style.jsf"%>
  <title>Datos de la Transaccion</title>
<script language="JavaScript">
<!--
var formName = 'Mio'
function validate(form)
{
<%
for(int i=0; i < listaCampos.size(); i++)
{
	Vector vCampos = (Vector)listaCampos.get(i);
    vCampos = (Vector)vCampos.get(0);
    if( vCampos.get(3).toString().length() > 0 )
    {
      out.println("  if( !top.validate(window, document.entry." + vCampos.get(0) + ", '" + vCampos.get(4) +"') )");
      out.println("    return");
    }
}
%>
  form.validateFLD.value = '1'
  return true;
}
function AuthoandSignature(entry)
{
 var Cuenta;
 if(entry.validateFLD.value != '1')
  return
 if(entry.needAutho.value == '1' && entry.AuthoOK.value != '1')
 {
  top.openDialog('../ventanilla/paginas/authoriz.jsp', 220, 130, 'top.setPrefs()', entry)
 }
 if(entry.needSignature.value == '1' && entry.SignField.value != '')
{
  Cuenta = eval('entry.' + entry.SignField.value + '.value')
  top.openModalDialog('<%=request.getContextPath()%>/servlet/ventanilla.FirmasServlet?txtDDACuenta=' + Cuenta, 710, 430, 'top.setPrefs2()', entry)
 }
}
//-->
</script>
</head>
<body bgcolor="#FFFFFF" onload="top.setFieldFocus(window.document)">
<br>
<% out.print("<form name=\"entry\" action=\"../servlet/ventanilla.DataServlet\" method=\"post\" ");
   if(txnAuthLevel.equals("1"))
    out.print("onSubmit=\"return validate(this)\"");
   out.println(">");
%>
<h1><%=session.getAttribute("page.txnImage")%></h1>
<h3>
<%out.print(session.getAttribute("page.cTxn") + "  " +
                (String)session.getAttribute("page.txnlabel"));%>
</h3>
<br>
<table border="0" cellspacing="0">
<%
 out.println("<tr><td align=\"right\">Cuenta DDA :</td><td width=\"20\">&nbsp;</td><td align=\"right\">");
 out.print(datasession.get("txtDDAO"));
 out.print("</td></tr>");
 out.println("<tr><td align=\"right\">Cuenta CDA :</td><td width=\"20\">&nbsp;</td><td align=\"right\">");
 out.print(datasession.get("txtCDAD"));
 out.print("</td></tr>");
 out.println("<tr><td align=\"right\">Monto en $ :</td><td width=\"20\">&nbsp;</td><td align=\"right\">");
 out.print(datasession.get("txtMonto"));
 out.print("</td></tr>");
 out.println("<tr><td align=\"right\">Valor UDI :</td><td width=\"20\">&nbsp;</td><td align=\"right\">");
 out.print(datasession.get("tasaUDI"));
 out.print("</td></tr>");
 out.println("<tr><td align=\"right\">Monto en UDIs :</td><td width=\"20\">&nbsp;</td><td align=\"right\">");
 out.print(datasession.get("montodisplayUDI"));
 out.print("</td></tr>");
%>
</table><br><br>

<input type="hidden" name="iTxn" value="<%= session.getAttribute("page.iTxn") %>">
<input type="hidden" name="oTxn" value="<%= session.getAttribute("page.oTxn") %>">
<input type="hidden" name="cTxn" value="<%= session.getAttribute("page.cTxn") %>">
<input type="hidden" name="sucursal" value="<%= session.getAttribute("branch") %>">
<input type="hidden" name="teller" value="<%= session.getAttribute("teller") %>">
<input type="hidden" name="moneda" value="<%= session.getAttribute("page.moneda")%>">
<input type="hidden" name="registro" value="<%=session.getAttribute("empno")%>">
<input type="hidden" name="tllrlvl" value="<%=session.getAttribute("tellerlevel")%>">
<input type="hidden" name="supervisor" value="<%= session.getAttribute("teller") %>">
<input type="hidden" name="AuthoOK" value="0">
<input type="hidden" name="SignOK" value="0">
<input type="hidden" name="validateFLD" value="0">
<input type="hidden" name="needSignature" value="<%out.print(txnAuthLevel);%>">
<input type="hidden" name="needAutho" value="<%out.print(txnIMGAutoriz);%>">
<p>
<% if(txnAuthLevel.equals("1") || txnIMGAutoriz.equals("1"))
    out.println("<a name='cont' href=\"javascript:validate(document.entry);javascript:AuthoandSignature(document.entry)\"><img src=\"../ventanilla/imagenes/b_continuar.gif\" border=\"0\"></a>");
   else
    out.println("<input name='cont' type=\"image\" src=\"../ventanilla/imagenes/b_continuar.gif\" border=\"0\">");
%>
<script language="javascript">
	window.focus();
	document.all["cont"].focus();
</script>

</form>
</body>
</html>
