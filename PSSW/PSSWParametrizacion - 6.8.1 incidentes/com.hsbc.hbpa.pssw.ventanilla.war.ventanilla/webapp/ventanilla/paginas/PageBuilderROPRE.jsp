<!--
//*************************************************************************************************
//             Funcion: JSP para relaciones de 1009
//            Elemento: PageBuilderRels.jsp
//          Creado por: Alejandro Gonzalez Castro
//      Modificado por: Rutilo Zarco
//*************************************************************************************************
// CCN - 4360211 - 09/10/2004 - Se crea menu de relaciones
// CCN - 4360275 - 18/02/2005 - WAS 5.1
// CCN - 4360581 - 12/03/2007 - Se convierten los estilos en jsf y se hace un include en el jsp
//*************************************************************************************************
-->

<%@ page session="true" import="java.util.*"%>
<%
response.setHeader("Cache-Control","no-store"); //HTTP 1.1
response.setHeader("Pragma","no-cache"); //HTTP 1.0
response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<%
Vector lcs = (Vector)session.getAttribute("page.listcampos");
Vector lct = (Vector)session.getAttribute("page.listcont");
String txnAuthLevel = (String)session.getAttribute("page.txnAuthLevel");
String txnIMGAutoriz = (String)session.getAttribute("page.txnIMGAutoriz");
%>
<html>
	<head>
		<title>Reporte de Ordenes de Pago (OPRE)</title>
		<%@include file="style.jsf"%>
		<script   language="JavaScript">
			var valor1 = "1"

			function sendservlet()
			{
	 			document.entry.opcion1.value = valor1;			
				document.entry.action = "../servlet/ventanilla.OP_018R"
				document.entry.submit();
			}
		
			function setValues1()
			{
	 			valor1 = document.entry.opcion1.value;
			}		
		</script> 
	</head>
	<body bgcolor="#FFFFFF" onload="top.setFieldFocus(window.document)">
		<br>
		<form name="entry" method="post">
			<h3>
				<%out.print(session.getAttribute("page.cTxn") + "  " +
					(String)session.getAttribute("page.txnlabel"));%>
			</h3>
			<table>
				<tr>
					<td><b>Reportes de Odenes de Pago:</b></td>
					<td>
						<SELECT name=opcion1 size=1 onchange="setValues1()">
							<OPTION value=1>Por Pagar</OPTION>
							<OPTION value=2>Por Devolver</OPTION>
							<OPTION value=3>Pagado Manualmente</OPTION>
							<OPTION value=4>Banco Nacionales</OPTION>
							<OPTION value=5>Reverso Manual</OPTION>
							<OPTION value=6>Ordenes a Investigar</OPTION>
						</SELECT>
					</td>
				</tr>
			</table>
			<input type="hidden" name="sucursal" value="<%= session.getAttribute("branch") %>">
			<input type="hidden" name="teller" value="<%= session.getAttribute("teller") %>">
			<input type="hidden" name="registro" value="<%=session.getAttribute("empno")%>">
			<p>
			<a href="javascript:sendservlet(document.entry)"><img src="../ventanilla/imagenes/b_aceptar.gif" border="0"></a>
		</form>
	</body>
</html>
