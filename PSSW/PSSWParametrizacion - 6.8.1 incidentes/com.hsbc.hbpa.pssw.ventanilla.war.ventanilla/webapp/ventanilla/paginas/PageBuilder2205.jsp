<!--
//*************************************************************************************************
//             Funcion: JSP para despliege de txns varias
//            Elemento: PageBuilder2205.jsp
//      Modificado por: Jesus Emmanuel Lopez Rosales
//*************************************************************************************************
// CCN - 4360275 - 18/02/2005 - WAS 5.1
// CCN - 4360334 - 17/06/2005 - Se convierte fieldLector a fragmento
// CCN - 4360390 - 07/10/2005 - Se agrega validacion para acdo 1053.
// CCN - 4360443 - 24/02/2006 - Se realizan modificaciones para pedir autorizaci�n para el depto 9010
// CCN - 4360477 - 19/05/2006 - Se agrega validaci�n para txn 5405 en tipo de identifiaci�n cuando es n�merico
// CCN - 4360533 - 20/10/2006 - Se realizan modificaciones para OPEE
// CCN - 4360572 - 23/02/2007 - Se realizan modificacion en validaci�n de monto de txn 5357 y bandera de autoriz remota en txn 0041
// CCN - 4360581 - 12/03/2007 - Se convierten los estilos en jsf y se hace un include en el jsp
// CCN - 4360584 - 23/03/2007 - Modificaciones para proyecto OPMN
// CCN - 4360619 - 15/06/2007 - Se eliminan systems
//*************************************************************************************************
-->
<%@ page session="true" import="java.util.*,ventanilla.GenericClasses"%>
<%
response.setHeader("Cache-Control","no-store"); //HTTP 1.1
response.setHeader("Pragma","no-cache"); //HTTP 1.0
response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<%
String MontoPro = new String("");
Vector listaCampos = (Vector)session.getAttribute("page.listcampos");
Vector listaContenidos = (Vector)session.getAttribute("page.listcont");
String txnAuthLevel = (String)session.getAttribute("page.txnAuthLevel");
String txnIMGAutoriz = (String)session.getAttribute("page.txnIMGAutoriz");
Vector v = (Vector)session.getAttribute("page.specialacct");
Hashtable datasession = (Hashtable)session.getAttribute("page.datasession");
String txn =  session.getAttribute("page.cTxn").toString();
ventanilla.GenericClasses gc = new ventanilla.GenericClasses();
if (datasession == null)
   datasession = new Hashtable();
int k;
int i;



%>

<html>
<head>
  <title>Datos de la Transaccion</title>
  <%@include file="style.jsf"%>
<script language="JavaScript">
<!--

var formName = 'Mio'
var specialacct = new Array()
var bandera = 0;
<%//Asignar valor de idflujo para flujos internos
   if(txn.equals("5209")){
      session.setAttribute("idFlujo","01");
   } 
%>
<%
 if(!v.isEmpty()){
  for(int iv = 0; iv < v.size(); iv++){
   out.print("specialacct[" + iv + "]='" + v.get(iv) + "'" + '\n');
  }
 }
%>



function validate(form)
{
   document.entry.fecha4525.value=top.sysdatef();
   form.validateFLD.value = '0';
   inputStr = '';
   
   <%
    if ( txn.equals("4537") || txn.equals("4539")  || txn.equals("4543") || txn.equals("4545") || txn.equals("4547") || txn.equals("1053")) {%>
    if  ( form.txtCodSeg.value.length == 0 || form.txtCveTran.value.length == 0 || form.txtDDACuenta2.value.length == 0 || form.txtSerial2.value.length == 0 ) {
        alert("Introduzca Banda del Cheque.")
        return;
   }
   <% } %>
   <% if ( txn.equals("5357")) {%>
     inputStr = Alltrim(form.txtMonto.value)
     inputStr = inputStr.toString().replace(/\$|\,/g,'');
     intVal = inputStr.substring(0, inputStr.indexOf("."))
     decVal = inputStr.substring(inputStr.indexOf(".") + 1, inputStr.length)
     monto = Number(intVal + decVal);
     seleccion = form.lstTipoDocto.options[form.lstTipoDocto.selectedIndex].value;
     if ( monto > 900000 && seleccion == "IDLI") {
        alert("Monto mayor al permitido con Licencia! ")
        return;
    }        
  <%}%>
  <% if ( txn.equals("5405")) {%>
       opselec = form.lsttipiden.selectedIndex;
       if(opselec == 0){
         if ( isNaN(form.txtnumiden.value) ){
	 	    alert("Valor inv�lido para Licencia, s�lo n�meros. Verifique...");
		    form.txtnumiden.value = "";
		    form.txtnumiden.focus();
		    return;
	 	  }
       }
   <%}%> 
<%
  for(i=0; i < listaCampos.size(); i++)
   {
    Vector vCampos = (Vector)listaCampos.get(i);
    vCampos = (Vector)vCampos.get(0);
    if( vCampos.get(4).toString().length() > 0 && !vCampos.get(0).toString().equals("txtMonto") && !vCampos.get(0).toString().equals("txtMonto1"))
    {    
        out.println("  if( !top.validate(window, document.entry." + vCampos.get(0) + ", '" + vCampos.get(4) +"') )");
        if (vCampos.get(0).toString().equals("txtFolio") && vCampos.get(4).toString().equals("isEmptyStr")) 
        {        
        %>
         if(document.entry.returnform.value == "N"  ) {
           return false;
         }
         else {
           document.entry.returnform.value = "N";
           return;
         }
    <% }
        else
          out.println("return;") ;
      }
  }
%>
  form.validateFLD.value = '1'
  if (document.entry.txtFechaEfect || document.entry.txtFechaError || document.entry.txtFechaCarta)
    document.entry.txtFechaSys.value=top.sysdatef();

  if ( form.iTxn.value == "1001" )
  {
     inputStr = Alltrim(form.txtEfectivo.value);
     inputStr = inputStr.toString().replace(/\$|\,/g,'');
     intVal = inputStr.substring(0, inputStr.indexOf("."));
     decVal = inputStr.substring(inputStr.indexOf(".") + 1, inputStr.length);
     monto = Number(intVal + decVal);
     inputStr = Alltrim(form.txtCheque.value);
     inputStr = inputStr.toString().replace(/\$|\,/g,'');
     intVal = inputStr.substring(0, inputStr.indexOf("."));
     decVal = inputStr.substring(inputStr.indexOf(".") + 1, inputStr.length);
     monto = monto + Number(intVal + decVal);
     if ( monto >= 25000000 ) 
         bandera = 1;
     else
         bandera = 0;
 }

  if ( form.iTxn.value == "0041" && form.cTxn.value == "0825"){
     seleccion = form.lstTipoDocto.options[form.lstTipoDocto.selectedIndex].value;
     if ( seleccion == "IDPA")
         bandera = 1;                              // Pedir Autorizacion
     else 
         bandera = 0;
  }

  if ( form.iTxn.value == "1005" || form.iTxn.value == "5405" || form.iTxn.value == "5407")
  {
     if ( form.iTxn.value == "1005" )
		if (form.txtMonto != null) 
			inputStr = Alltrim(form.txtMonto.value);
     else
     {
       if (form.txtMontoNoCero != null)
         inputStr = Alltrim(form.txtMontoNoCero.value);
     }
     inputStr = inputStr.toString().replace(/\$|\,/g,'');
     intVal = inputStr.substring(0, inputStr.indexOf("."))
     decVal = inputStr.substring(inputStr.indexOf(".") + 1, inputStr.length)
     monto = Number(intVal + decVal);
     if (( monto > 10000000 && form.iTxn.value == "1005") ||
         ( monto > 5000000 && ( form.iTxn.value == "5405" || form.iTxn.value == "5407")))
         bandera = 1;
     else 
         bandera = 0;
  }
  
  
 var suc ='<%= (String)session.getAttribute("branch")%>'

 if (form.iTxn.value == '0228' && suc == '09010')
     bandera = 1; 
  
  if ( bandera == 1 ) {                       // Pedir autorizacion
        form.onSubmit="return validate(document.entry);"
        document.entry.needAutho.value = '1';
        AuthoandSignature(document.entry);
  }

  if ( bandera == 0) {
 <%
 if(!txnAuthLevel.equals("1") && !txnIMGAutoriz.equals("1")){
    out.println("  form.submit()");
    out.println("var obj = document.getElementById('btnCont');");
    out.println("obj.onclick = new Function('return false');");
    }
    
%> }
}
function AuthoandSignature(entry)
{
 var Cuenta;
 if(entry.returnform != undefined && entry.txtFolio != undefined)
   if(entry.txtFolio.value.length == 0)
     return;

 if(entry.validateFLD.value != '1')
  return

    
	
	

 if(entry.needAutho.value == '1' && entry.AuthoOK.value != '1')
 {
  top.openDialog('../ventanilla/paginas/authoriz.jsp', 220, 130, 'top.setPrefs()', entry)
 }

 if(entry.needSignature.value == '1' && entry.SignField.value != '')
 {
  Cuenta = eval('entry.' + entry.SignField.value + '.value')
  top.openModalDialog('<%=request.getContextPath()%>/servlet/ventanilla.FirmasServlet?txtDDACuenta=' + Cuenta, 710, 430, 'top.setPrefs2()', entry)
 }
 
}

function Alltrim(inputStr)
{
  var inStr = new String(inputStr)

  while(inStr.charAt(0) == ' ')
    inStr = inStr.substring(1, inStr.length)

  while(inStr.charAt(inStr.length) == ' ')
    inStr = inStr.substring(0, inStr.length-1)

  return inStr
}

//-->
</script>
</head>
<body bgcolor="#FFFFFF" onload="top.setFieldFocus(window.document)">
<br>
<% out.print("<form name=\"entry\" action=\"../servlet/ventanilla.DataServlet\" method=\"post\" ");
   if(txnAuthLevel.equals("1") || session.getAttribute("page.cTxn").toString().equals("0825"))
   out.print("onSubmit=\"return validate(this)\"");
   out.println(">");
%>
<h1><%=session.getAttribute("page.txnImage")%></h1>
<h3>
<%out.print(session.getAttribute("page.cTxn") + "  " + (String)session.getAttribute("page.txnlabel"));%>
</h3>
<table border="0" cellspacing="0" class="normaltextblack">

<%if(txn.equals("5183")){%>
<tr>
	<td>Monto USD:</td>
    <td width="10">&nbsp;</td>
    <td align="right">
    	<%	String a = "";
    		if(datasession.get("txtMonto1") == null)
    			a = "";
    		else
    			a = datasession.get("txtMonto1").toString().trim();
    	%>
    	<%=a%>
    </td>
</tr>
<%}%>

<%if(txn.equals("5159")){%>
<tr>
	<td>Monto:</td>
    <td width="10">&nbsp;</td>
    <td align="right">
    	<%	String b = "";
    		if(datasession.get("txtMonto") == null)
    			b = "";
    		else
    			b = datasession.get("txtMonto").toString().trim();
    	%>
    	<%=b%>
    </td>
</tr>
<%}%>


<%
	String moneda = (String)session.getAttribute("page.moneda");
	String moneda_s = (String)session.getAttribute("page.moneda_s");
	  for(i =  0; i < listaCampos.size(); i++)
	   {
	    Vector vCampos = (Vector)listaCampos.get(i);
	    vCampos = (Vector)vCampos.get(0);
	    if(!vCampos.get(0).toString().equals("txtCodSeg")){
	     out.println("<tr>");
	     if (vCampos.get(0).equals("txtDDACuenta") && (txn.equals("0130") || txn.equals("0128"))){
	         out.println("  <td>N�mero de Cuenta Abono:</td>");      
	     }else if(txn.equals("4525")){
	    	if (vCampos.get(0).equals("txtDDACuenta"))
                out.println("  <td>N�mero de Cuenta:</td>");
           	else if (vCampos.get(0).equals("lstBanCorresp1"))
                out.println("  <td>Bancos Corresponsales:</td>");
	    	else if (vCampos.get(0).equals("txtTipoCambio"))
                out.println("  <td>Tipo de Cambio:</td>");
            else if (vCampos.get(0).equals("txtRefTIRE"))
         		out.println("  <td>Referencia:</td>");
	    	else if (moneda.equals("01") && !moneda_s.equals("01")){
	    			if(vCampos.get(0).equals("txtMonto"))
                		out.println("  <td>Monto Recibido:</td>");
                	else if(vCampos.get(0).equals("txtMonto1"))
                		out.println("  <td>Monto Abonar:</td>");
                	}
	    	else if (!moneda.equals("01") && moneda_s.equals("01")){
	    			if(vCampos.get(0).equals("txtMonto"))
                		out.println("  <td>Monto Abonar:</td>");
                	else if(vCampos.get(0).equals("txtMonto1"))
                		out.println("  <td>Monto Recibido:</td>");
                	}
         }else{
	         out.println("  <td>" + vCampos.get(2) + ":</td>");
	     }	     	     
	     out.println("  <td width=\"10\">&nbsp;</td>");

         if(datasession.get(vCampos.get(0)) != null)
		{
		  if(vCampos.get(0).toString().equals("txtMonto")){
		    out.print  ("  <td>" + datasession.get(vCampos.get(0)) + "</td>");
  	 		out.println("</tr>");				  
		  }else if(vCampos.get(0).toString().equals("txtTipoCambio")){
		  	String TipoCambio = (String)datasession.get("txtTipoCambio");
    		java.text.DecimalFormat reformattedValue = new java.text.DecimalFormat("#,###.0000");
    		String returnTipoCambio = reformattedValue.format((double)(java.lang.Double.parseDouble(TipoCambio) / 10000.00));
		 	out.print  ("  <td>");
		 	out.print(returnTipoCambio);
		 	out.print  ("  </td>");
		    out.println("</tr>");
		  }
		  else{
		    out.print  ("  <td>" + datasession.get(vCampos.get(0)) + "</td>");
		    out.println("</tr>");
		  }
		}
        else{ 

	     if(!vCampos.get(0).toString().substring(0,3).equals("lst"))
       {
            if(vCampos.get(0).toString().substring(0,3).equals("pss"))
            {
                out.print  ("  <td><input maxlength=\"" + vCampos.get(3) + "\" name=\"" + vCampos.get(0) + "\" size=\"" + vCampos.get(3) +"\" type=\"Password\"" + " tabindex=\""+(i+1)+"\"");
            }
            else
            {
                out.print("  <td><input maxlength=\"" + vCampos.get(3) + "\" name=\"" + vCampos.get(0) + "\" size=\"" + vCampos.get(3) +"\" type=\"text\"" + " tabindex=\""+(i+1)+"\"");
            }

	      String CampoR = (String)vCampos.get(5);
	      if(CampoR != null && CampoR.length() > 0){
	        CampoR = CampoR.trim();
	        out.print(" value=\"" + CampoR + "\"");
	      }
              String level = (String)session.getAttribute("tellerlevel");
              if (vCampos.get(0).toString().equals("txtFechaEfect") && level.equals("1") && 
                 (txn.equals("2503") || txn.equals("2525") || txn.equals("3009") || txn.equals("3075") ||
                  txn.equals("3077") || txn.equals("3105") || txn.equals("3313") || txn.equals("3314")))
                out.print(" onfocus=\"blur()\"");
	     }
	     else{
	      out.print("  <td><select name=\"" + vCampos.get(0) + "\" tabindex=\"" + (i+1) + "\" size=\"" + "1" + "\"");
//	      if( vCampos.get(3).toString().length() > 0 ){
	      if( vCampos.get(4).toString().length() > 0 ){
	       if(vCampos.get(0).toString().substring(0,3).equals("rdo"))
           out.println(" onClick=\"top.validate(window, this, '" + vCampos.get(4) +"')\">");
	       else
	         out.println(" onChange=\"top.validate(window, this, '" + vCampos.get(4) +"')\"  >");
	      }
	      else
	       out.println(">");
	      for(k=0; k < listaContenidos.size(); k++){
	       Vector v1Campos = (Vector)listaContenidos.get(k);
	       for(int j = 0; j < v1Campos.size(); j++){
	         Vector v1Camposa = (Vector)v1Campos.get(j);
	         if(v1Camposa.get(0).toString().equals(vCampos.get(0).toString()))
	           out.println("  <option value=\"" + v1Camposa.get(3) + "\">" + v1Camposa.get(2));
	       }
	      }
	      out.println("  </select></td>");
	     }
	     if( vCampos.get(4).toString().length() > 0 && !vCampos.get(0).toString().substring(0,3).equals("lst"))
	      out.println("  onBlur=\"top.estaVacio(this)||top.validate(window, this, '" + vCampos.get(4) +"')\"  onKeyPress=\"top.keyPressedHandler(window, this, '" + vCampos.get(4) +"')\"   ></td>");
	     else if(!vCampos.get(0).toString().substring(0,3).equals("lst"))
	      out.println(">");
	     out.println("</tr>");
	    }
	    }
	    else{
	     %><%@include file="fieldLector.jsf"%><%
	     i = k;
	    }
	   }
	%>
<%if(txn.equals("0128") || txn.equals("0130")){%>
  <tr>
  <td>Ejecutivo:</td>
  <td width="10">&nbsp;</td>
  <td align="left"><%= session.getAttribute("teller") %></td>
 </tr>
<%}%>	
</table>
<input type="hidden" name="iTxn" value="<%= session.getAttribute("page.iTxn") %>">
<input type="hidden" name="oTxn" value="<%= session.getAttribute("page.oTxn") %>">
<input type="hidden" name="cTxn" value="<%= session.getAttribute("page.cTxn") %>">
<input type="hidden" name="sucursal" value="<%= session.getAttribute("branch") %>">
<input type="hidden" name="teller" value="<%= session.getAttribute("teller") %>">
<input type="hidden" name="moneda" value="<%= session.getAttribute("page.moneda")%>">
<input type="hidden" name="registro" value="<%=session.getAttribute("empno")%>">
<input type="hidden" name="tllrlvl" value="<%=session.getAttribute("tellerlevel")%>">
<input type="hidden" name="supervisor" value="<%= session.getAttribute("teller") %>">
<input type="hidden" name="AuthoOK" value="0">
<input type="hidden" name="SignOK" value="0">
<input type="hidden" name="validateFLD" value="0">
<input type="hidden" name="needSignature" value="<%out.print(txnIMGAutoriz);%>">
<input type="hidden" name="needAutho" value="<%out.print(txnAuthLevel);%>">
<input type="hidden" name="SignField" value="<%= session.getAttribute("page.SignField") %>">
<input type="hidden" name="override" value="N">
<input type="hidden" name="txtFees" value="0">
<input type="hidden" name="compania" value="20">
<input type="hidden" name="txtSelectedItem" value="0" >
<input type="hidden" name="returnform" value="N" >
<input type="hidden" name="returnform2" value="N" >
<input type="hidden" name="txtPlazaSuc" value="<%= session.getAttribute("plaza") %>" >
<input type="hidden" name="fecha4525" values="">
<input type="hidden" name="moneda2" value="<%= session.getAttribute("page.moneda_s")%>">


<input type="hidden" name="passw" value="<%=session.getAttribute("password")%>">
<input type="hidden" name="MontoRetiroPesos" value = "<%=session.getAttribute("Retiro.Pesos")%>">
<input type="hidden" name="MontoRetiroDolar" value = "<%=session.getAttribute("Retiro.Dolar")%>">
<p><script>
 if (document.entry.txtFechaEfect)
 {
  document.writeln("<input type=hidden name=txtFechaSys>");
  document.entry.txtFechaEfect.value=top.sysdate();
 }
 if (document.entry.txtFechaError)
 {
  document.writeln("<input type=hidden name=txtFechaSys>");
  document.entry.txtFechaError.value=top.sysdate();
 }
 if (document.entry.txtFechaCarta)
 {
  document.writeln("<input type=hidden name=txtFechaSys>");
  document.entry.txtFechaCarta.value=top.sysdate();
 }
</script>
	<style>
		.invisible
		{
		  display:None;
		}

		.visible
		{
		  display:"";
		}
	</style>


<% if(txnAuthLevel.equals("1") || txnIMGAutoriz.equals("1"))
    out.println("<a id='btnCont' tabindex=\"" + (i+1) + "\" href=\"javascript:document.entry.returnform.value='S';validate(document.entry);javascript:AuthoandSignature(document.entry)\" ><img src=\"../ventanilla/imagenes/b_continuar.gif\" border=\"0\"></a>");
   else
    out.println("<a id='btnCont' tabindex=\"" + (i+1) + "\" href=\"javascript:document.entry.returnform.value='S';validate(document.entry)\"><img src=\"../ventanilla/imagenes/b_continuar.gif\" border=\"0\"></a>");
%>

</form>
</body>
</html>
