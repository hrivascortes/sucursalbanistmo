<!--
//*************************************************************************************************
//             Funcion: JSP que presenta los totales del cajero
//            Elemento: ResponseTotales.jsp
//          Creado por: Alejandro Gonzalez Castro
//      Modificado por: Juvenal Fernandez Varela
//*************************************************************************************************
// CCN - 4360306 - 22/04/2005 - Se realizan modificaciones para el WAS 5.1
//*************************************************************************************************
-->

<%@ page session="true" import="java.util.*"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<%!
  private String getString(String s)
  {
    int len = s.length();
    for(; len>0; len--)
	  if(s.charAt(len-1) != ' ')
	    break;

    return s.substring(0, len);
  }
%>
<%
 Vector resp = (Vector)session.getAttribute("response");
%>

<html>
<head>
  <title>Certificación</title>
<style type="text/css">Body {font-family: Arial, Helv; font-size: 10pt; font-weight: bold; color:#000000}</style>
</head>
<Script Language="JavaScript">
function mensaje()
{
	alert("PROCESANDO INFORMACION DEL HOST, ESPERE UNOS SEGUNDOS POR FAVOR....");
	openTotales()
}
function openTotales()
{
        nameWindow = "TotalesPC";
		options = "resizable=0,toolbar=0,scrollbars=0,menubar=0,width=790,height=430,top=0,left=0,status=0";
<%
    if( resp.size() != 4 )
    {
        String mensaje = "Error de conexión con el Host";
        out.println("alert(\"" + mensaje + "\")");
    }
    else
    {
%>
        window.open("TotalesPCentral.jsp",nameWindow,options);
        window.close("newWindowTotales");
   <%}%>
}
</Script>
<body onLoad="openTotales()">
<p>&nbsp;</p>
<p>&nbsp;</p>
</body>
</html>
