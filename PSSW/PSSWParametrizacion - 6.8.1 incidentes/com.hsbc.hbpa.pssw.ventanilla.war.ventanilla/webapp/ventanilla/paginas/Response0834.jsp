<!--
//*************************************************************************************************
//             Funcion: JSP que despliega respuesta de txn 0834
//            Elemento: Response0834.jsp
//          Creado por: Alejandro Gonzalez Castro
//      Modificado por: Alejandro Gonzalez Castro
//*************************************************************************************************
// CCN - 4360275 - 18/02/2005 - WAS 5.1
//*************************************************************************************************
-->

<%@ page session="true" import="java.util.*"%>
<%
response.setHeader("Cache-Control","no-store"); //HTTP 1.1
response.setHeader("Pragma","no-cache"); //HTTP 1.0
response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<%!
  private String getString(String s)
  {
   int len = s.length();
   for(; len>0; len--)
    if(s.charAt(len-1) != ' ')
     break;
   return s.substring(0, len);
  }

  private String getItem(String newCadNum, int newOption)
  {
   int iPIndex = newCadNum.indexOf("CONSECUTIVO:") + 12, NumSaltos, Num = 0, Ban = 0;
   String nCadNum = new String("");
   NumSaltos = newOption;
   while(Num < NumSaltos)
   {
    if(newCadNum.charAt(iPIndex) == 0x20)
    {
     while(newCadNum.charAt(iPIndex) == 0x20)
      iPIndex++;
     Num++;
    }
    else
     while(newCadNum.charAt(iPIndex) != 0x20)
      iPIndex++;
   }
   while(newCadNum.charAt(iPIndex) != 0x20 && iPIndex < newCadNum.length()){
    nCadNum = nCadNum + newCadNum.charAt(iPIndex);
    if(iPIndex < newCadNum.length())
      iPIndex++;
    if(iPIndex == newCadNum.length())
      break;
   }

   return nCadNum;
  }
%>
<% Vector resp = (Vector)session.getAttribute("response"); %>

<html>
<head>
  <title>Certificación</title>
<style type="text/css">Body {font-family: Arial, Helv; font-size: 10pt; font-weight: bold; color:#000000}</style>
</head>
<body bgcolor="#FFFFFF" onload="top.setFieldFocus(window.document)">
<p>
<%
  Stack flujotxn = (Stack)session.getAttribute("page.flujotxn");

String iva = "";
String cta = "";
String comvta = "";
String comdev = "";
String comrepdoc = "";
String sbc = "";
String codigo = "";
  if( resp.size() != 4 )
   out.println("<b>Error de conexi&oacute;n</b><br>");
  else
  {
  	try
  	{
     	codigo = (String)resp.elementAt(0);
   	 	if( !codigo.equals("0") )
    		out.println("<b>Transaccion Rechazada</b><br><br>");
    	int lines = Integer.parseInt((String)resp.elementAt(1));
    	String szResp = (String)resp.elementAt(3);
    	for(int i=0; i<lines; i++)
		{
    		String line = szResp.substring( i*78, Math.min(i*78+77, szResp.length()) );
     		out.println(getString(line) + "<p>");
     		if(Math.min(i*78+77, szResp.length()) == szResp.length())
      			break;
    	}
   		if(codigo.equals("0"))
		{
		    iva = szResp.substring(41,43);
			cta = szResp.substring(234,244);
			comvta = szResp.substring(244,249);
			comdev = szResp.substring(249,254);
			comrepdoc = szResp.substring(254,259);
			sbc = szResp.substring(259,szResp.length());
			session.setAttribute("iva",iva);
			session.setAttribute("cta",cta);
			session.setAttribute("comvta",comvta);
			session.setAttribute("comdev",comdev);
			session.setAttribute("comrd",comrepdoc);
			session.setAttribute("sbc",sbc);
			flujotxn.pop();
			session.setAttribute("page.flujotxn",flujotxn);
		}
  	}
  	catch(NumberFormatException nfe)
  	{out.println("<b>Error de conexi&oacute;n</b><br>");}
  }
%>
<p>
<%
	if(codigo.equals("0"))
  {
   		response.sendRedirect("PageBuilderVouchers.jsp");
    }
%>
</html>
