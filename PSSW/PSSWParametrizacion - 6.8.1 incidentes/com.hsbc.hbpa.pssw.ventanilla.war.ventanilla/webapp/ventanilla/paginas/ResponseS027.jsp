<!--
//*************************************************************************************************
///            Funcion: JSP que despliega respuesta de txn RAP
//            Elemento: ResponseS027.jsp
//          Creado por: Israel de Paz
// CCN - 4360409 - 06/01/2006 - Se crea Response para la linea de captura del SAT.
//*************************************************************************************************
-->

<%@ page session="true" import="java.util.*"%>
<%
response.setHeader("Cache-Control","no-store"); //HTTP 1.1
response.setHeader("Pragma","no-cache"); //HTTP 1.0
response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<%!





  private String getString(String s)
  {
   int len = s.length();
   for(; len>0; len--)
    if(s.charAt(len-1) != ' ')
     break;
   return s.substring(0, len);
  }


  private String getItem(String newCadNum, int newOption)
  {
   int iPIndex = newCadNum.indexOf("CONSECUTIVO:") + 12, NumSaltos, Num = 0, Ban = 0;
   String nCadNum = new String("");
   NumSaltos = newOption;
   while(Num < NumSaltos)
   {
    if(newCadNum.charAt(iPIndex) == 0x20)
    {
     while(newCadNum.charAt(iPIndex) == 0x20)
      iPIndex++;
     Num++;
    }
    else
     while(newCadNum.charAt(iPIndex) != 0x20)
      iPIndex++;
   }
   while(newCadNum.charAt(iPIndex) != 0x20 && iPIndex < newCadNum.length()){
    nCadNum = nCadNum + newCadNum.charAt(iPIndex);
    if(iPIndex < newCadNum.length())
      iPIndex++;
    if(iPIndex == newCadNum.length())
      break;
   }

   return nCadNum;
  }


%>

<html>
<head>
  <title>Certificación</title>
<style type="text/css">Body {font-family: Arial, Helv; font-size: 10pt; font-weight: bold; color:#000000}</style>
</head>
<body bgcolor="#FFFFFF"   >

<%
Vector resp = (Vector)session.getAttribute("response");


  java.util.Stack flujotxn = new java.util.Stack();
  String codigo = "";
  String needOverride = "NO";


  if( resp.size() != 4 )
   out.println("<b>Error de conexi&oacute;n</b><br>");
  else
  { 
   	try
   	{
     	codigo = (String)resp.elementAt(0);
   	 	if( !codigo.equals("0") )
    		out.println("<b>Transaccion Rechazada</b><br><br>");
     	int lines = Integer.parseInt((String)resp.elementAt(1));
    	String szResp = (String)resp.elementAt(3);
    	for(int i=0; i<lines; i++)
		{
    		String line = szResp.substring( i*78, Math.min(i*78+77, szResp.length()) );
     		out.println(getString(line) + "<p>");
     		if(Math.min(i*78+77, szResp.length()) == szResp.length())
      		break;
    	}
  	}
  	catch(NumberFormatException nfe){
     out.println("<b>Error de conexi&oacute;n</b><br>");
    }
  }
  
   String txtCadImpresion  = ""; // Para Certificación
   session.setAttribute("page.cTxn","5503");
   String txn = session.getAttribute("page.cTxn").toString();
  
  Hashtable datasession = (Hashtable)session.getAttribute("page.datasession");
  String monto= (String)datasession.get("txtMonto");
  monto = monto.replace(',',' ');
  monto = monto.replace('.',' ');
  String montoTemp="";
  
  for(int i=0 ; i < monto.length() ; i++)
     {
     

      if(monto.charAt(i) != ' ')
      {
      montoTemp=montoTemp + monto.charAt(i); 
      }
      
      
     }
  monto=montoTemp;    
  String txns ="";
  flujotxn = (Stack)session.getAttribute("page.flujotxn");
  if(!flujotxn.empty())
  {
  txns = (String)flujotxn.pop();
  if (txns.equals("R503"))
  {
  java.util.Stack flujotxnn = new java.util.Stack();
  session.setAttribute("page.flujotxn",flujotxnn); 
  }
  }

  
  
  String lc= (String)datasession.get("referenciaSat");
  session.setAttribute("Consecutivo", getItem((String)resp.elementAt(3), 1)); // Para Certificación
  txtCadImpresion="~SATLC~"+lc+"~"+monto+"~";   

   if (codigo.equals("0"))
   txtCadImpresion="~SATLC~"+lc+"~"+monto+"~";   
   else
   txtCadImpresion="";   
   
   out.println("<script language=\"javascript\">top.openDialog('../../servlet/ventanilla.ImprimirServlet?txtCadImpresion=" + txtCadImpresion + "', 160, 120, 'top.setPrefs4()', document.Txns)</script>"); // Para Certificación

 

	
%>
</body>
</html>
