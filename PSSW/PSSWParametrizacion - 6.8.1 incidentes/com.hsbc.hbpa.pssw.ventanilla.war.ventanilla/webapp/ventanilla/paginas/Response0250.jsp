<!--
//*************************************************************************************************
//             Funcion: JSP que despliega respuesta de txn 0250
//            Elemento: Response0250.jsp
//          Creado por: Alejandro Gonzalez Castro
//      Modificado por: Alejandro Gonzalez Castro
//*************************************************************************************************
// CCN - 4360175 - 03/08/2004 - Ajuste size de ventana de impresion
// CCN - 4360189 - 03/09/2004 - Se redirecciona a Final.jsp por Control de Efectivo
//*************************************************************************************************
-->

<%@ page session="true"%>
<%
response.setHeader("Cache-Control","no-store"); //HTTP 1.1
response.setHeader("Pragma","no-cache"); //HTTP 1.0
response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
%>

<%!
  private String stringFormat(int option, int sum)
  {
    java.util.Calendar now = java.util.Calendar.getInstance();
    String temp = new Integer(now.get(option) + sum).toString();
    if( temp.length() != 2 )
      temp = "0" + temp;

    return temp;
  }
  
  private String getString(String s)
  {
   int len = s.length();
   for(; len>0; len--)
    if(s.charAt(len-1) != ' ')
     break;
   return s.substring(0, len);
  }  
%>
<HTML>
	<HEAD>
		<TITLE>Visualización de la Ultima Transacción</TITLE>
		<SCRIPT language="JavaScript">

			function handleOK(form)
			{
				top.openDialog('../../servlet/ventanilla.ImprimirServlet', 160, 120, 'top.setPrefs4()', document.signature); 
				return;
			}

			function handleCancel()
			{
		 		document.signature.action = "Final.jsp";
		 		document.signature.target = "panel";
		 		document.signature.submit();			
			}
		</SCRIPT>
	</HEAD>
<style type="text/css">Body {font-family: Arial, Helv; font-size: 10pt; font-weight: bold; color:#000000}</style>
<BODY bgcolor="#FFFFFF">
<%
			String strDate = new String(stringFormat(java.util.Calendar.YEAR, 0) + stringFormat(java.util.Calendar.MONTH, 1) + stringFormat(java.util.Calendar.DAY_OF_MONTH, 0));
			String strTime = stringFormat(java.util.Calendar.HOUR_OF_DAY, 0) + stringFormat(java.util.Calendar.MINUTE, 0) + stringFormat(java.util.Calendar.SECOND, 0);
			String strCajero = (String)session.getAttribute("teller");
			String strRespuesta = (String)session.getAttribute("txtCadImpresion");
			String strOutLine = new String("");
			strOutLine = strRespuesta.substring(10,17);
			strOutLine = strOutLine + " " + strDate + " " + strTime + " ";
			strOutLine = strOutLine + strRespuesta.substring(0,4) + " " + strCajero + " " + strCajero.substring(0,4) + "\n";			
			out.println("<br>" + strOutLine);
     	   int lines = Integer.parseInt(strRespuesta.substring(7,9));
    	   String szResp = strRespuesta.substring(18,strRespuesta.length()-1);
    	   for(int i=0; i<lines; i++) 
    	   {
				String line = szResp.substring( i*78, Math.min(i*78+77, szResp.length()) );
				StringBuffer lineBk = new StringBuffer();
				int longi = line.length();
				for(int j = 0; j < longi; j++){
				  if(line.charAt(j) == 0x20)
				    lineBk.append("&nbsp;");
				  else
				    lineBk.append(line.charAt(j));
				}
				out.println("<br>" + getString(lineBk.toString()));
				if(Math.min(i*78+77, szResp.length()) == szResp.length())
				break;
			}			
%>
<FORM name="signature">
 <input type="hidden" name="SignOK" value="1">
<DIV STYLE="text-align:left">
  		<a href="javascript:handleCancel()"><img src="../imagenes/b_cancelar.gif" border="0"></a>
		&nbsp;&nbsp;&nbsp;		
  		<a href="javascript:handleOK(document.signature)"><img src="../imagenes/b_imprimir.gif" border="0"></a>  		
</DIV>
</FORM>
</BODY>
</HTML>
