<!--
//*************************************************************************************************
//             Funcion: JSP que de datos para monitoreo
//            Elemento: test.jsp
//          Creado por: Marco Lara Palacios
//      Modificado por: Juvenal Fernandez
//*************************************************************************************************
// CCN - 4360334 - 17/06/2005 - Se elimina variable dbQualifier
//*************************************************************************************************
-->
<%@page language="java" import="java.util.Date,java.sql.*,com.bital.util.ConnectionPoolingManager"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
  <title>Medicion de Tiempos</title>
</head>
<body>
<%
  String sql1 = "SELECT * FROM TW_DIARIO_ELECTRON WHERE D_SUCURSAL='01915'";  
  String sql2 = "INSERT INTO TW_DIARIO_ELECTRON(D_TXN, D_OPERADOR, D_SUCURSAL, D_CONNUREG) VALUES('0825', '190005', '01900', '9999999999999999')";
  String sql3 = "DELETE FROM TW_DIARIO_ELECTRON WHERE D_CONNUREG='9999999999999999' AND D_TXN= '0825'";

  //Cambiar sentencias con las definidas en WAS
  if( config.getServletContext().getAttribute("sql1") != null )
    sql1 = (String)config.getServletContext().getAttribute("sql1");
  if( config.getServletContext().getAttribute("sql2") != null )
    sql2 = (String)config.getServletContext().getAttribute("sql2");
  if( config.getServletContext().getAttribute("sql3") != null )
    sql3 = (String)config.getServletContext().getAttribute("sql3");

  ResultSet rs = null;
  PreparedStatement stmt = null;

  long t1,t2, tt1,tt2;

  out.println("<b>INICIO: </b>" + new Date() + "<p>");


  //LOGIN
  tt1 = t1 = System.currentTimeMillis();
  ventanilla.com.bital.sfb.Login lsb = new ventanilla.com.bital.sfb.Login();
  lsb.setTxnCode("0081");
  lsb.setBranch("1900");
  lsb.setTeller("190005");
  lsb.setSupervisor("190005");
  lsb.setCashIn("A449470");
  
  out.println("<b>" + lsb.toString() + "</b><br>");
  lsb.execute();
  t2 = System.currentTimeMillis();
  out.println("Tiempo de ejecución para 0081: <b><font color=\"red\">" + (t2-t1) + " ms.</font></b><br><p>");


  // SELECT
  t1 = System.currentTimeMillis();
  Connection pooledConnection = ConnectionPoolingManager.getPooledConnection();
  try
  {
    int count = 0;
    if( pooledConnection != null )
    {
      stmt = pooledConnection.prepareStatement(sql1);
      rs = stmt.executeQuery();

      while( rs.next() )
        ++count;

      rs.close();
      out.println(count + " registro(s) leido(s) en <b>" + sql1 + "</b><br>");
    }
    else
      out.println("Error al obtener conexión en SELECT<br>");
  }
  catch(SQLException e1)
  {
    out.println("Error al ejecutar sentencia SELECT<br>");
    out.println(e1.getMessage());
    out.println("<p>");
  }
  finally
  {
    if( stmt != null )
      stmt.close();
    if( pooledConnection != null )
      pooledConnection.close();
  }
  t2 = System.currentTimeMillis();
  out.println("Tiempo de ejecución para SELECT: <b><font color=\"red\">" + (t2-t1) + " ms.</font></b><br><p>");


  //INSERT
  t1 = System.currentTimeMillis();
  pooledConnection = ConnectionPoolingManager.getPooledConnection();
  try
  {
    int count = 0;
    if( pooledConnection != null )
    {
      stmt = pooledConnection.prepareStatement(sql2);
      count = stmt.executeUpdate();
      out.println(count + " registro(s) agregado(s) en <b>" + sql2 + "</b><br>");
    }
    else
      out.println("Error al obtener conexión en INSERT<br>");
  }
  catch(SQLException e1)
  {
    out.println("Error al ejecutar sentencia INSERT<br>");
    out.println(e1.getMessage());
    out.println("<p>");
  }
  finally
  {
    if( stmt != null )
      stmt.close();
    if( pooledConnection != null )
      pooledConnection.close();
  }
  t2 = System.currentTimeMillis();
  out.println("Tiempo de ejecución para INSERT: <b><font color=\"red\">" + (t2-t1) + " ms.</font></b><br><p>");


  //DELETE
  t1 = System.currentTimeMillis();
  pooledConnection = ConnectionPoolingManager.getPooledConnection();
  try
  {
    int count = 0;
    if( pooledConnection != null )
    {
      stmt = pooledConnection.prepareStatement(sql3);
      count = stmt.executeUpdate();
      out.println(count + " registro(s) eliminado(s) en <b>" + sql3 + "</b><br>");
    }
    else
      out.println("Error al obtener conexión en DELETE<br>");
  }
  catch(SQLException e1)
  {
    out.println("Error al ejecutar sentencia DELETE<br>");
    out.println(e1.getMessage());
    out.println("<p>");
  }
  finally
  {
    if( stmt != null )
      stmt.close();
    if( pooledConnection != null )
      pooledConnection.close();
  }
  t2 = System.currentTimeMillis();
  out.println("Tiempo de ejecución para DELETE: <b><font color=\"red\">" + (t2-t1) + " ms.</font></b><br><p>");


  //LOGOUT
  t1 = System.currentTimeMillis();
  lsb = new ventanilla.com.bital.sfb.Login();
  lsb.setTxnCode("0082");
  lsb.setBranch("1900");
  lsb.setTeller("190005");
  lsb.setSupervisor("190005");
  
  out.println("<b>" + lsb.toString() + "</b><br>");
  lsb.execute();
  tt2 = t2 = System.currentTimeMillis();
  out.println("Tiempo de ejecución para 0082: <b><font color=\"red\">" + (t2-t1) + " ms.</font></b><br><p>");
  out.println("<b>FIN: </b>" + new Date() + " (<b><font color=\"red\">" + (tt2-tt1) + " ms</font></b>)");
%>
</body>
</html>
