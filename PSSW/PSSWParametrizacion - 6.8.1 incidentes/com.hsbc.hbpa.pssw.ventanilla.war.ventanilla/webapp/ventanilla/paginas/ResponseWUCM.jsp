<!--
//*************************************************************************************************
//             Funcion: JSP que despliega respuesta de WU multiple
//            Elemento: ResponseWUCM.jsp
//          Creado por: Alejandro Gonzalez Castro
//      Modificado por: Rutilo Zarco Reyes
//*************************************************************************************************
// CCN - 4360189 - 03/09/2004 - Inicio WU
// CCN - 4360253 - 07/01/2005 - Se corrige problema en consulta multiple cuando solo existe un beneficiario
// CCN - 4360383 - 26/09/2005 - Se elimina doble click
// CCN - 4360581 - 12/03/2007 - Se convierten los estilos en jsf y se hace un include en el jsp
//*************************************************************************************************
-->

<%@page session="true" import="java.util.*"%>
<%
response.setHeader("Cache-Control","no-store"); //HTTP 1.1
response.setHeader("Pragma","no-cache"); //HTTP 1.0
response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<%
Hashtable data = new Hashtable();
data = (Hashtable)session.getAttribute("WUdata");
Vector ben = new Vector();
Vector ord = new Vector();
Vector mto = new Vector();
Vector sts = new Vector();
Vector fch = new Vector();
Vector key = new Vector();
ben = (Vector)data.get("beneficiario");
ord = (Vector)data.get("ordenante");
mto = (Vector)data.get("monto");
sts = (Vector)data.get("status");
fch = (Vector)data.get("fecha");
key = (Vector)data.get("dbkey");
%>

<html>
<head>
  <title>Certificación</title>
  <%@include file="style.jsf"%>
<script   language="JavaScript">
function validate(form)
{
    var j =0;
    if (!document.entry.orden.length)
       j=1
    for(var i =0; i < document.entry.orden.length;i++)
    {
        if(document.entry.orden[i].checked)
            j++;
    }
    if(j>0)
    {
        document.entry.submit();
        var obj = document.getElementById('btnCont');
        obj.onclick = new Function('return false');  
    }
    else
        alert("Para Continuar debe seleccionar un registro...");
}
</script>
</head>
<body bgcolor="#FFFFFF" onload="top.setFieldFocus(window.document)">
<form name="entry" action="../../servlet/ventanilla.Group01WU" method="post">
<table border="0">
<tr>
    <th colspan="10" align="center"><b>Selecci&oacute;n de Transferencia</b></th>
</tr>
<tr bgcolor="#CCCCCC">
    <td>&nbsp;&nbsp;</td>
    <td align="center">Beneficiario</td>
    <td>&nbsp;&nbsp;</td>
    <td align="center">Ordenante</td>
    <td>&nbsp;&nbsp;</td>
    <td align="center">Monto M.N.</td>
    <td>&nbsp;&nbsp;</td>
    <td align="center">Status</td>
    <td>&nbsp;&nbsp;</td>
    <td align="center">Fecha</td>
</tr>
<%
int size = ben.size();
for(int i=0; i <size; i++)
{%>
    <tr>
    <td><input type="radio" name="orden" value="<%=(String)key.get(i)%>" /></td>
    <td><%=(String)ben.get(i)%></td>
    <td>&nbsp;&nbsp;</td>
    <td><%=(String)ord.get(i)%></td>
    <td>&nbsp;&nbsp;</td>
    <td align="right"><%=(String)mto.get(i)%></td>
    <td>&nbsp;&nbsp;</td>
    <td align="center"><%=(String)sts.get(i)%></td>
    <td>&nbsp;&nbsp;</td>
    <td><%=(String)fch.get(i)%></td>
    </tr>
<%
}
%>
</table>
<input type="hidden" name="WUaction">
<a id='btnCont' href="javascript:validate(document.entry)"><img src="../imagenes/b_continuar.gif" border="0"></a>
<a id='btnCont' href="javascript:document.entry.WUaction.value='canbusqueda';document.entry.action='../../servlet/ventanilla.Group02WU';document.entry.submit();"><img src="../imagenes/b_cancelar.gif" border="0"></a>
</form>
</body>
</html>
