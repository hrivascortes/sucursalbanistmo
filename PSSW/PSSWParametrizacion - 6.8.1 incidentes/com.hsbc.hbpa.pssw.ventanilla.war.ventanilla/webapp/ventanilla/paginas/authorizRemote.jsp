<!--
//*************************************************************************************************
//             Funcion: JSP que despliega Autorizacion Remota
//            Elemento: authorizRemote.jsp
//          Creado por: Alejandro Gonzalez Castro
// Ultima Modificacion: 03/08/2004
//      Modificado por: Alejandro Gonzalez Castro
//*************************************************************************************************
// CCN - 4360175 - 03/08/2004 - Se habilita reenvio a otros Ejecutivos
// CCN - 4360581 - 12/03/2007 - Se convierten los estilos en jsf y se hace un include en el jsp
//*************************************************************************************************
-->

<%@ page session="true" import="java.util.Hashtable"%>
<%
response.setHeader("Cache-Control","no-store"); //HTTP 1.1
response.setHeader("Pragma","no-cache"); //HTTP 1.0
response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
%>
<%
String indice = request.getParameter("indice");
session.setAttribute("autho.indice",indice);
Hashtable cajeros = (Hashtable)session.getAttribute("user.par");
java.util.Enumeration enume = cajeros.keys();
StringBuffer supervisor = new StringBuffer();
while(enume.hasMoreElements())
{
  String tmp = (String)enume.nextElement();
  if (tmp.indexOf(indice) != -1)
  {
    supervisor.append(tmp.replace(indice.charAt(0),'1') + "=" + (String)cajeros.get(tmp) + "#");
  }
}
supervisor.append("numrow" + "=2#"); //Numero de supervisores a consultar default 2.
%>
<html>
 <head>
 <title>Autorización Remota</title>
 <%@include file="style.jsf"%>
<script language="JavaScript" src="validate.js"></script>
 <script language="JavaScript">
 <!--
  var Nav4 = ((navigator.appName == "Netscape") && (parseInt(navigator.appVersion) >= 4))
  function closeme(){
   window.close()
  }
  function handleOK(form){
//   if(window.opener && !window.opener.closed){
    transferData()
    window.opener.dialogWin.returnFunc.doValidate()
//   }
//   else{
//   alert("Se cerró la ventana de Autorización!")
//  }
  closeme()
  return false
 }
 function checkAutho(){
   var txtAutho = "0";
   if(txtAutho == '1'){
     handleOK()
   }
 }
 function handleCancel(){
  window.opener.dialogWin.returnedValue = '0'
  window.opener.dialogWin.returnFunc.doValidate()
  closeme()
  return false
 }
 function getFormData(form){
  var searchString = ""
  var onePair
  for (var i = 0; i < form.elements.length; i++){
   if (form.elements[i].type == "hidden"){
    onePair = escape(form.elements[i].name) + "&" + form.elements[i].value
   }
   else continue
   searchString += onePair + "&"
  }
  return searchString
 }
 function transferData(){
  if(top.opener && !top.opener.closed){
   top.opener.dialogWin.returnedValue = getFormData(document.prefs)
  }
 }

 function verifyUsers()
 {
   document.prefs.action = '../../servlet/ventanilla.authorizRemote';
     document.prefs.submit();
 }

 function cancelartxn()
 {

    alert("TRANSACCION NO AUTORIZADA. \n Seleccione otro Supervisor o Gerente.");
    prefs.action="UsersAutoRemote.jsp";
    prefs.submit();
/*    if (window.opener.panel != undefined)
    {

      if (window.opener.panel.document.forms[0].name == 'diariodisplay')
        window.opener.panel.document.forms[0].action = "panelcaptura.html";
      else
        window.opener.panel.document.forms[0].action = "<%=request.getContextPath()%>/ventanilla/paginas/panelcaptura.html";

      window.opener.panel.document.forms[0].submit();
    }
    closeme();*/
 }
//-->
 </script>
 </head>
 <body>
  <form name="prefs" action="../ventanilla/paginas/panelcaptura.html" method="POST">
  <br>
   <p>Esperando Respuesta de Autorizaci&oacute;n Remota... </p>
   <p>Espere un momento por favor.</p>
   <input name="txtAutoriza" type="hidden" value="0">
   <input name="txtSupervisor" type="hidden" value="">
   <input name="txtTeller" type="hidden" value="">
   <table>
     <tr>
        <td>
	<OBJECT classid="clsid:8AD9C840-044E-11D1-B3E9-00805F499D93" codebase="http://java.sun.com/products/plugin/1.2.2/jinstall-1_2_2-win.cab#Version=1,2,2,0" width="2" height= "2"> 
          	<PARAM name="type" value="application/x-java-applet;version=1.2">
		<PARAM name="java_ARCHIVE" value="Authorization.jar">
		<PARAM name="java_CODE" value="authorizRemote">
		<PARAM name="java_NAME" value="Authorization">
		<PARAM name="java_CODEBASE" value="../">
		<PARAM name="chain" value="<%=supervisor.toString()%>">
		<PARAM name="dataauthoriz" value="<%=session.getAttribute("data.to.authoriz")%>">
		<PARAM name="MAYSCRIPT" value="true">
		<COMMENT>
	<EMBED type="application/x-java-applet;version=1.2"
		java_ARCHIVE="Authorization.jar"
		java_CODE="authorizRemote"
		java_NAME="Authorization"
		java_CODEBASE = "../"
		width="2"
		Height= "2"
		chain="<%=supervisor.toString()%>"
		dataauthoriz="<%=session.getAttribute("data.to.authoriz")%>"
		MAYSCRIPT>
	</EMBED>
		</COMMENT> 
          </OBJECT>
        </td>
     </tr>
   </table>
  </form>
 </body>
 </html>
