<!-- 
//*************************************************************************************************
//             Funcion: JSP que realiza la presentacion de los datos de la txn0124 para su poseto
//            Elemento: PageBuilder0124.jsp
//          Creado por: Juvenal R. Fernandez Varela
//      Modificado por: Juvenal R. Fernandez Varela
//*************************************************************************************************
// CCN -4360171 - 23/07/2004 - Se crea nueva txn 0124
// CCN - 4360275 - 18/02/2005 - WAS 5.1
// CCN - 4360581 - 12/03/2007 - Se convierten los estilos en jsf y se hace un include en el jsp
//*************************************************************************************************
-->
<%@ page session="true" import="java.util.*"%>
<%
response.setHeader("Cache-Control","no-store"); //HTTP 1.1
response.setHeader("Pragma","no-cache"); //HTTP 1.0
response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<%
Vector listaCampos = (Vector)session.getAttribute("page.listcampos");
Vector listaContenidos = (Vector)session.getAttribute("page.listcont");
String txnAuthLevel = (String)session.getAttribute("page.txnAuthLevel");
String txnIMGAutoriz = (String)session.getAttribute("page.txnIMGAutoriz");
Hashtable datasession = (Hashtable)session.getAttribute("page.datasession");
if(datasession == null)
  datasession = new Hashtable();
int k = 0;
String compania = "20";
%>
<html>
<head>
  <%@include file="style.jsf"%>
  <title>Datos de la Transaccion</title>
<!--<script type="application/x-javascript" language="JavaScript">-->
<script language="JavaScript">
<!--
var formName = 'Mio'
var isok = 0;
function validate(form)
{
<%
  for(int i=0; i < listaCampos.size(); i++)
   {
    Vector vCampos = (Vector)listaCampos.get(i);
    vCampos = (Vector)vCampos.get(0);

    if( vCampos.get(4).toString().length() > 0 && datasession.get(vCampos.get(0)) == null)
    {
      out.println("  if( !top.validate(window, document.entry." + vCampos.get(0) + ", '" + vCampos.get(4) +"') )");
      out.println("    return");
    }
  }
%>
  isok = 1;
  form.validateFLD.value = '1'

<% if(!txnAuthLevel.equals("1") && !txnIMGAutoriz.equals("1"))
  {
    out.println("  form.submit()");
    out.println("var obj = document.getElementById('continu');"); 
    out.println("obj.onclick = new Function('return false');");
  }
%>

}
function AuthoandSignature(entry)
{
 if (isok == 0)
   return
 isok = 0;
 var Cuenta;
 if(entry.validateFLD.value != '1')
  return
 if(entry.needAutho.value == '1' && entry.AuthoOK.value != '1'){
  top.openDialog('../ventanilla/paginas/authoriz.jsp', 220, 130, 'top.setPrefs()', entry)
 }

 if(entry.needSignature.value == '1' && entry.SignField.value != ''){
  Cuenta = eval('entry.' + entry.SignField.value + '.value')
  top.openDialog('../servlet/ventanilla.FirmasServlet?txtDDACuenta=' + Cuenta, 640, 335, 'top.setPrefs2()', entry)
 }
 }
//-->
</script>
</head>
<body onload="top.setFieldFocus(window.document);">
<br>
<% out.print("<form name=\"entry\" action=\"../servlet/ventanilla.DataServlet\" method=\"post\" ");
   if(txnAuthLevel.equals("1"))
    out.print("onSubmit=\"return validate(this)\"");
   out.println(">");
%>

<h1><%=session.getAttribute("page.txnImage")%></h1>

<h3>
<%out.print(session.getAttribute("page.cTxn") + "  " +
                (String)session.getAttribute("page.txnlabel"));%>
</h3>
<table border="0" cellspacing="0">
<%
  for(int i =  0; i < listaCampos.size(); i++)
  {
    Vector vCampos = (Vector)listaCampos.get(i);
    vCampos = (Vector)vCampos.get(0);

      out.println("<tr>");
      out.println("  <td>" + vCampos.get(2) + ":</td>");
      out.println("  <td width=\"10\">&nbsp;</td>");

      if( !vCampos.get(0).toString().substring(0,3).equals("lst") )
      {
        if( datasession.get(vCampos.get(0)) != null )
        {
          out.print  ("  <td>" + datasession.get(vCampos.get(0)) + "</td");
        }
        else
        {
          out.print  ("  <td><input maxlength=\"" + vCampos.get(3) + "\" name=\"" + vCampos.get(0) + "\" size=\"" + vCampos.get(3) +"\" type=\"text\"");
          String CampoR = (String)vCampos.get(5);
          if(CampoR != null && CampoR.length() > 0)
          {
            CampoR = CampoR.trim();
            out.print(" value=\"" + CampoR + "\"");
          }
        }
      }
      else
      {
        out.print("  <td><select name=\"" + vCampos.get(0) + "\" size=\"" + "1" + "\"");

        if( vCampos.get(3).toString().length() > 0 && datasession.get(vCampos.get(0)) == null)
          out.println(" onChange=\"top.validate(window, this, '" + vCampos.get(4) +"')\">");
        else
          out.println(">");

        for(k=0; k < listaContenidos.size(); k++)
        {
          Vector v1Campos = (Vector)listaContenidos.get(k);
            if( v1Campos.get(0).toString().equals(vCampos.get(0)) )
              out.println("  <option value=\"" + v1Campos.get(3) + "\">" + v1Campos.get(2));
        }

        out.println("  </select></td>");
      }

      if( vCampos.get(3).toString().length() > 0 && !vCampos.get(0).toString().substring(0,3).equals("lst") && datasession.get(vCampos.get(0)) == null )
        out.println(" onChange=\"top.validate(window, this, '" + vCampos.get(4) +"')\"></td>");
      else if(!vCampos.get(0).toString().substring(0,3).equals("lst"))
        out.println(">");
      out.println("</tr>");
  }
%>
</table>
<input type="hidden" name="iTxn" value="<%= session.getAttribute("page.iTxn") %>">
<input type="hidden" name="oTxn" value="<%= session.getAttribute("page.oTxn") %>">
<input type="hidden" name="cTxn" value="<%= session.getAttribute("page.cTxn") %>">
<input type="hidden" name="sucursal" value="<%= session.getAttribute("branch") %>">
<input type="hidden" name="teller" value="<%= session.getAttribute("teller") %>">
<input type="hidden" name="moneda" value="<%= session.getAttribute("page.moneda")%>">
<input type="hidden" name="registro" value="<%=session.getAttribute("empno")%>">
<input type="hidden" name="tllrlvl" value="<%=session.getAttribute("tellerlevel")%>">
<input type="hidden" name="supervisor" value="<%= session.getAttribute("teller") %>">
<input type="hidden" name="AuthoOK" value="0">
<input type="hidden" name="SignOK" value="0">
<input type="hidden" name="compania" value="<%out.print(compania);%>">
<input type="hidden" name="validateFLD" value="0">
<input type="hidden" name="needSignature" value="<%out.print(txnIMGAutoriz);%>">
<input type="hidden" name="needAutho" value="<%out.print(txnAuthLevel);%>">
<input type="hidden" name="SignField" value="<%= session.getAttribute("page.SignField") %>">
<input type="hidden" name="override" value="N">
<p>
<%  if(txnAuthLevel.equals("1") || txnIMGAutoriz.equals("1"))
     out.println("<a id='continu' href=\"javascript:validate(document.entry);javascript:AuthoandSignature(document.entry)\"><img src=\"../ventanilla/imagenes/b_continuar.gif\" border=\"0\"></a>");
    else
     out.println("<a id='continu' href=\"javascript:validate(document.entry)\"><img src=\"../ventanilla/imagenes/b_continuar.gif\" border=\"0\"></a>");
%>
</form>
</body>
</html>
