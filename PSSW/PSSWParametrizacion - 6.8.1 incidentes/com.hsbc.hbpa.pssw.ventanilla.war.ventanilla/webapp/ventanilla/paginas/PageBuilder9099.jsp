<!--
//*************************************************************************************************
//             Funcion: JSP que despliega captura de txn 9099
//            Elemento: PageBuilder9099.jsp
//          Creado por: Alejandro Gonzalez Castro
//      Modificado por: Alejandro Gonzalez Castro
//*************************************************************************************************
// CCN - 4360175 - 03/08/2004 - Ajuste size de ventana de impresion
// CCN - 4360275 - 18/02/2005 - WAS 5.1
// CCN - 4360581 - 12/03/2007 - Se convierten los estilos en jsf y se hace un include en el jsp
//*************************************************************************************************
-->

<%@ page session="true" import="java.util.*"%>
<%
response.setHeader("Cache-Control","no-store"); //HTTP 1.1
response.setHeader("Pragma","no-cache"); //HTTP 1.0
response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<%
	if(session.getAttribute("txtCadImpresion") != null)
    session.removeAttribute("txtCadImpresion");
	if(session.getAttribute("txtResponse") != null)
    session.removeAttribute("txtResponse");
	if(session.getAttribute("strFlujoRap") != null)
    session.removeAttribute("strFlujoRap");
  	if(session.getAttribute("txtCasoEsp") != null)
	 session.removeAttribute("txtCasoEsp");
  	if(session.getAttribute("ReversoLink") != null)
	 session.removeAttribute("ReversoLink");
 Vector listaCampos = (Vector)session.getAttribute("page.listcampos");
 Vector listaContenidos = (Vector)session.getAttribute("page.listcont");
 int k;
%>
<html>
<head>
 <%@include file="style.jsf"%>
 <title>Datos de la Transaccion</title>
 <script   language="javascript">
 <!--
  function submitPrint(){
   var formName = document.forms[0];
   var numBanco = new String('');
   if(formName.lstTipoRel.selectedIndex == 1){
     numBanco = new String(formName.lstBanco[formName.lstBanco.selectedIndex].value);
   }
   var cadImpresion = new String('RELORDENPAGO~' + numBanco + '~');
   cadImpresion = 'ventanilla.ImprimirServlet?txtCadImpresion=' + cadImpresion;

   top.openDialog(cadImpresion, 160, 120, 'top.setPrefs4()', document.forms[0]);
  }
 //-->
 </script>
</head>
<body bgcolor="#FFFFFF" onload="top.setFieldFocus(window.document);">
<br>
<form name="girosCitiBank">
<h1><%=session.getAttribute("page.txnImage")%></h1>
<h3>
<%out.print(session.getAttribute("page.cTxn") + "  " + (String)session.getAttribute("page.txnlabel"));%>
</h3>
<table border="0" cellspacing="0">
<%
	for(int i =  0; i < listaCampos.size(); i++){
	  java.util.Vector vCampos = (java.util.Vector)listaCampos.get(i);
	  vCampos = (Vector)vCampos.get(0);
    if(!vCampos.get(0).toString().equals("txtCodSeg")){
	    out.println("<tr>");
	    out.println("  <td>" + vCampos.get(2) + ":</td>");
	    out.println("  <td width=\"10\">&nbsp;</td>");
	    if(!vCampos.get(0).toString().substring(0,3).equals("lst")){
			  if(vCampos.get(0).toString().substring(0,3).equals("pss")){
			    out.print  ("  <td><input maxlength=\"" + vCampos.get(3) + "\" name=\"" + vCampos.get(0) + "\" size=\"" + vCampos.get(3) +"\" type=\"Password\"" + "\" tabindex=\""+i+"\"");
			  }
        else{
          out.print("  <td><input maxlength=\"" + vCampos.get(3) + "\" name=\"" + vCampos.get(0) + "\" size=\"" + vCampos.get(3) +"\" type=\"text\"" + "\" tabindex=\""+i+"\"");
        }
	      String CampoR = (String)vCampos.get(5);
	      if(CampoR != null && CampoR.length() > 0){
	        CampoR = CampoR.trim();
	        out.print(" value=\"" + CampoR + "\"");
	      }
	     }
	     else{
	      out.print("  <td><font face=\"Courier\"><select name=\"" + vCampos.get(0) + "\" size=\"" + "1" + "\"");
	      if( vCampos.get(4).toString().length() > 0 ){
	       if(vCampos.get(0).toString().substring(0,3).equals("rdo"))
           out.println(" onClick=\"top.validate(window, this, '" + vCampos.get(4) +"')\">");
	       else
	         out.println(" onChange=\"top.validate(window, this, '" + vCampos.get(4) +"')\">");
	      }
	      else
	       out.println(">");
	      for(k=0; k < listaContenidos.size(); k++){
	       Vector v1Campos = (Vector)listaContenidos.get(k);
	       for(int j = 0; j < v1Campos.size(); j++){
	         Vector v1Camposa = (Vector)v1Campos.get(j);
	         if(v1Camposa.get(0).toString().equals(vCampos.get(0).toString()))
	           out.println("  <option value=\"" + v1Camposa.get(3) + "\">" + v1Camposa.get(2));
	       }
	      }
	      out.println("  </select></font></td>");
	     }
	     if( vCampos.get(4).toString().length() > 0 && !vCampos.get(0).toString().substring(0,3).equals("lst"))
	      out.println(" onChange=\"top.validate(window, this, '" + vCampos.get(4) +"')\"></td>");
	     else if(!vCampos.get(0).toString().substring(0,3).equals("lst"))
	      out.println(">");
	     out.println("</tr>");
	    }
	    else{
	     %><%--@include file="fieldLector.jsp"--%><%
	     //i = k;
	    }
	   }
	%>
 </table>
 <p>
 <a href="javascript:submitPrint()"><img src="../ventanilla/imagenes/b_aceptar.gif" border="0"></a>
 <input type="hidden" name="sucursal" value="<%= session.getAttribute("branch") %>">
 </form>
 </body>
</html>
