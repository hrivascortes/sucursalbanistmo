<!--
//*************************************************************************************************
//             Funcion: JSP que presenta las opciones de la txn 4057
//            Elemento: PageBuilder4057.jsp
//          Creado por: Alejandro Gonzalez Castro
//      Modificado por: Juvenal R. Fernandez V.
//*************************************************************************************************
// CCN - 4360275 - 18/02/2005 - WAS 5.1
// CCN - 4360334 - 17/06/2005 - Se convierte fieldLector a fragmento
// CCN - 4360581 - 12/03/2007 - Se convierten los estilos en jsf y se hace un include en el jsp
//*************************************************************************************************
-->

<%@ page session="true" import="java.util.*"%>
<%
response.setHeader("Cache-Control","no-store"); //HTTP 1.1
response.setHeader("Pragma","no-cache"); //HTTP 1.0
response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<%
Vector listaCampos = (Vector)session.getAttribute("page.listcampos");
Vector listaContenidos = (Vector)session.getAttribute("page.listcont");
String txnAuthLevel = (String)session.getAttribute("page.reqauto");
String txnIMGAutoriz = (String)session.getAttribute("page.txnIMGAutoriz");

int k=0;
int i=0;
String MontoPro = new String("");
%>
<html>
<head>
  <%@include file="style.jsf"%>
  <title>Datos de la Transaccion</title>
<script type="text/javascript" language="JavaScript">
<!--
var formName = 'Mio'
function validate(form)
{
<%
    if ( session.getAttribute("page.cTxn").toString().equals("4059")) { 
%>
 if  ( form.txtCodSeg.value.length == 0 || form.txtCveTran.value.length == 0 || form.txtDDACuenta2.value.length == 0 || form.txtSerial2.value.length == 0 ) {
      alert("Introduzca Banda del Cheque.")
      return;
 }
<% } %>

<%
  for(i=0; i < listaCampos.size(); i++)
   {
    Vector vCampos = (Vector)listaCampos.get(i);
    vCampos = (Vector)vCampos.get(0);
    if( vCampos.get(4).toString().length() > 0 )
    {
      out.println("  if( !top.validate(window, document.entry." + vCampos.get(0) + ", '" + vCampos.get(4) +"') )");
      out.println("    return");
    }
   }
%>

  form.validateFLD.value = '1'
  if (document.entry.txtFechaEfect)
    document.entry.txtFechaSys.value=top.sysdatef();
<% if(!txnAuthLevel.equals("1") && !txnIMGAutoriz.equals("1"))
	{
    out.println("  form.submit();");
    out.println("var obj = document.getElementById('btnCont');"); 
    out.println("obj.onclick = new Function('return false');");   
	}   
//  return true;
%>
}
function AuthoandSignature(entry)
{
 var Cuenta;
 if(entry.validateFLD.value != '1')
  return
 if(entry.needAutho.value == '1' && entry.AuthoOK.value != '1'){
  top.openDialog('../ventanilla/paginas/authoriz.jsp', 220, 130, 'top.setPrefs()', entry)
 }
 if(entry.needSignature.value == '1' && entry.SignField.value != ''){
   if (entry.iTxn.value == '4059' )
       Cuenta = eval('entry.' + entry.SignField.value + '.value')
   else
       Cuenta = eval('entry.txtFirmaFun.value')
      
  top.openModalDialog('<%=request.getContextPath()%>/servlet/ventanilla.FirmasServlet?txtDDACuenta=' + Cuenta, 710, 430, 'top.setPrefs2()', entry)
 }
}
//-->
</script>
</head>
<body onload="top.setFieldFocus(window.document)">
<br>
<% out.print("<form name=\"entry\" action=\"../servlet/ventanilla.DataServlet\" method=\"post\" ");
   if(txnAuthLevel.equals("1"))
    out.print("onSubmit=\"return validate(this)\"");
   out.println(">");
%>

<h1><%=session.getAttribute("page.txnImage")%></h1>

<h3>
<%out.print(session.getAttribute("page.cTxn") + "  " +
                (String)session.getAttribute("page.txnlabel"));%>
</h3>
<table border="0" cellspacing="0">
<%
  Hashtable datasession = (Hashtable)session.getAttribute("page.datasession");
  for(i = 0; i < listaCampos.size(); i++)
   {
    Vector vCampos = (Vector)listaCampos.get(i);
    vCampos = (Vector)vCampos.get(0);
    if(!vCampos.get(0).toString().equals("txtCodSeg")){
     out.println("<tr>");
     if (vCampos.get(0).equals("txtDDACuenta"))
        vCampos.set(2,"Numero Cuenta de Abono");
     out.println("  <td>" + vCampos.get(2) + ":</td>");
     out.println("  <td width=\"10\">&nbsp;</td>");
     if(!vCampos.get(0).toString().substring(0,3).equals("lst")){
      out.print  ("  <td><input maxlength=\"" + vCampos.get(3) + "\" name=\"" + vCampos.get(0) + "\" size=\"" + vCampos.get(3) +"\" type=\"text\" tabindex=\"" + (i+1) + "\"");
      String CampoR = (String)vCampos.get(5);
      if(CampoR != null && CampoR.length() > 0){
       CampoR = CampoR.trim();
/*       if(CampoR.charAt(0) == '=')
        out.print(" value=\"javascript:" + CampoR.substring(1) + "\"");
       else*/
        out.print(" value=\"" + CampoR + "\"");
      }
     }
     else{
      out.print("  <td><select name=\"" + vCampos.get(0) + "\" tabindex=\"" + (i+1) + "\" size=\"1\"");
      if( vCampos.get(4).toString().length() > 0 )
       out.println(" onChange=\"top.validate(window, this, '" + vCampos.get(4) +"')\">");
      else
       out.println(">");
      for(k=0; k < listaContenidos.size(); k++){
	       Vector v1Campos = (Vector)listaContenidos.get(k);
	       for(int j = 0; j < v1Campos.size(); j++){
	         Vector v1Camposa = (Vector)v1Campos.get(j);
	         if(v1Camposa.get(0).toString().equals(vCampos.get(0).toString()))
	           out.println("  <option value=\"" + v1Camposa.get(3) + "\">" + v1Camposa.get(2));
       }
      }
      out.println("  </select></td>");
     }
     if( vCampos.get(4).toString().length() > 0 && !vCampos.get(0).toString().substring(0,3).equals("lst"))
      out.println(" onBlur=\"top.estaVacio(this)||top.validate(window, this, '" + vCampos.get(4) +"')\" onKeyPress=\"top.keyPressedHandler(window, this, '" + vCampos.get(4) +"')\"></td>");
     else if(!vCampos.get(0).toString().substring(0,3).equals("lst"))
      out.println(">");
     out.println("</tr>");
    }
    else{
     %><%@include file="fieldLector.jsf"%><%
     i += 3 ;
    }
   }
%>
  <td>Monto:</td>
  <td width="10" align="right">&nbsp;</td>
  <td align="right"><%
   String moneda = (String)session.getAttribute("page.moneda");
   if(moneda.equals("01")){
    String monto = (String)datasession.get("txtMonto");
    if(!monto.equals("0.00"))
     out.print(datasession.get("txtMonto"));
    else
     out.print(datasession.get("txtMontoC"));
   }
   else
    out.print(datasession.get("txtMonto1"));
  %></td>
 </tr>
  <tr>
  <td>Ejecutivo:</td>
  <td width="10">&nbsp;</td>
  <td align="right"><%= session.getAttribute("teller") %></td>
 </tr>
</table>
<input type="hidden" name="iTxn" value="<%= session.getAttribute("page.iTxn") %>">
<input type="hidden" name="oTxn" value="<%= session.getAttribute("page.oTxn") %>">
<input type="hidden" name="cTxn" value="<%= session.getAttribute("page.cTxn") %>">
<input type="hidden" name="sucursal" value="<%= session.getAttribute("branch") %>">
<input type="hidden" name="teller" value="<%= session.getAttribute("teller") %>">
<input type="hidden" name="moneda" value="<%= session.getAttribute("page.moneda")%>">
<input type="hidden" name="registro" value="<%=session.getAttribute("empno")%>">
<input type="hidden" name="tllrlvl" value="<%=session.getAttribute("tellerlevel")%>">
<input type="hidden" name="supervisor" value="<%= session.getAttribute("teller") %>">
<input type="hidden" name="AuthoOK" value="0">
<input type="hidden" name="SignOK" value="0">
<input type="hidden" name="validateFLD" value="0">
<input type="hidden" name="compania" value="20">
<input type="hidden" name="needSignature" value="<%out.print(txnIMGAutoriz);%>">
<input type="hidden" name="needAutho" value="<%out.print(txnAuthLevel);%>">
<input type="hidden" name="SignField" value="<%= session.getAttribute("page.SignField") %>">
<input type="hidden" name="txtFees" value="0">

<p>
<script>
 if (document.entry.txtFechaEfect)
 {
  document.writeln("<input tabindex=\"<%=(i++)%>\" type=hidden name=txtFechaSys>");
  document.entry.txtFechaEfect.value=top.sysdate();
 }
</script>
<% if(txnAuthLevel.equals("1") || txnIMGAutoriz.equals("1"))
    out.println("<a id='btnCont' tabindex=\"" + (i++) + "\" href=\"javascript:validate(document.entry);javascript:AuthoandSignature(document.entry)\"><img src=\"../ventanilla/imagenes/b_aceptar.gif\" border=\"0\"></a>");
   else
    out.println("<a id='btnCont' tabindex=\"" + (i++) + "\" href=\"javascript:validate(document.entry)\"><img src=\"../ventanilla/imagenes/b_aceptar.gif\" border=\"0\"></a>");
%>
</form>
</body>
</html>
