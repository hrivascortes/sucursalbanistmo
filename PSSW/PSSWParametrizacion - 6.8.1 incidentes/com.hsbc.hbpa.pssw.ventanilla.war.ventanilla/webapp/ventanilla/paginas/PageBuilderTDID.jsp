<!--
//*************************************************************************************************
//             Funcion: JSP que despliega los campos de la txn 0030
//            Elemento: PageBuilderTDID.jsp
//          Creado por: Rutilo Zarco Reyes
//      Modificado por: Rutilo Zarco Reyes
//*************************************************************************************************
// CCN - 4360446 - 10/03/2006 - Proyecto TDI fase 2 cobro de comision
// CCN - 4360581 - 12/03/2007 - Se convierten los estilos en jsf y se hace un include en el jsp
//*************************************************************************************************
-->
<%@ page session="true" import="java.util.*"%>
<%
response.setHeader("Cache-Control","no-store"); //HTTP 1.1
response.setHeader("Pragma","no-cache"); //HTTP 1.0
response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
%>
<%!
  private String getString(String s)
  {
   int len = s.length();
   for(; len>0; len--)
    if(s.charAt(len-1) != ' ')
     break;
   return s.substring(0, len);
  }
  private String setFormat(String newCadNum) {
    int iPIndex = newCadNum.indexOf(".");
    String nCadNum = new String(newCadNum.substring(0, iPIndex));
    for (int i = 0; i < (int)((nCadNum.length()-(1+i))/3); i++)
        nCadNum = nCadNum.substring(0, nCadNum.length()-(4*i+3))+','+nCadNum.substring(nCadNum.length()-(4*i+3));
    
    return nCadNum + newCadNum.substring(iPIndex, newCadNum.length());
  }
  private String setPointToString(String newCadNum) {
    int iPIndex = newCadNum.indexOf("."), iLong;
    String szTemp;
    
    if(iPIndex > 0) {
        newCadNum = new String(newCadNum + "00");
        newCadNum = newCadNum.substring(0, iPIndex + 1) + newCadNum.substring(iPIndex + 1, iPIndex + 3);
    }
    else {
        for(int i = newCadNum.length(); i < 3; i++)
            newCadNum = "0" + newCadNum;
        iLong = newCadNum.length();
        if(iLong == 3)
            szTemp = newCadNum.substring(0, 1);
        else
            szTemp = newCadNum.substring(0, iLong - 2);
        newCadNum = szTemp + "." + newCadNum.substring(iLong - 2);
    }
    return newCadNum;
  }
  private String setzeros(String dato, int longi)
  {
     int i = dato.length();
     while(i<longi)
     {
      dato = "0" + dato;
      i++;
      }
      return dato;
  }
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<HTML>
<HEAD>
<TITLE>PageBuiderTDID.jsp</TITLE>
<%@include file="specialstyle.jsf"%>
</HEAD>
<Script language="JavaScript">
function cancelar()
{
  var form = document.forms[0]
  form.action = "Final.jsp";
  form.submit();
}

function validate(continuar)
{
 var form = document.forms[0]
 var NumTxnFre = form.NumTxnFre.value;
 var checked = 0;
 for (var i=1; i<=Number(NumTxnFre); i++)
 {
   gField = eval("form.accion" + (i));
   if (gField.checked)
   {
     checked ++ 
     field = eval("form.monto" + (i))
     if (field.value.length == 0)
     {
       alert("Debe capturar el Monto...")
       field.focus()
       return
     }  
     if( !top.validate(window, field, 'isCurrency') )
       return
     comision = eval("form.comision" + (i))
     iva = eval("form.iva" + (i))
     total = Number(comision.value.replace(/\$|\,|\./g,'')) + Number(iva.value.replace(/\$|\,|\./g,'')) + Number(field.value.replace(/\$|\,|\./g,''))
     if (total == 0)
     {
       alert("El Monto debe ser mayor a 0.00...")
       field.focus()
       return
     }
     gtotal = eval("form.total" + (i))
     gtotal.value = total
     if( !top.validate(window, gtotal, 'isCurrency') )
       return
   }
   else
     eval("form.total" + (i) + ".value = ''")
 }
 if (checked == 0)
 {
   alert("Debe seleccionar un Servicio...")
   return
 }  

 if (continuar == '1')
 {
   form.cTxn.value = "P030"
   form.submit()
 }
}
function resubmit(txn)
{
    var form = document.forms[0]
    var process  = '13'
    if (txn == '5503')
      process = '08'
    form.txnProcess.value = process
    form.action = '../../servlet/ventanilla.PageServlet'
    form.target = 'panel'
    form.transaction.value = txn
    form.submit()
}
</script>
<BODY>
<P><b>Recepcion Automatizada de Pagos TDI.</b></P>
<%
Vector resp = (Vector)session.getAttribute("response");
String codigo = null;
String szResp = null;
    try
	{
        codigo = (String)resp.elementAt(0);
        if(!codigo.equals("0"))
        {
          out.println("<b>Transaccion Rechazada...</b><br><br>");
          int lines = Integer.parseInt((String)resp.elementAt(1));
          szResp = (String)resp.elementAt(3);
          for(int i=0; i<lines; i++)
          {
            String line = szResp.substring( i*78, Math.min(i*78+77, szResp.length()) );
            out.println("<b>" + getString(line) + "</b><p>");
            if(Math.min(i*78+77, szResp.length()) == szResp.length())
                break;
          }
        }
  	}
  	catch(NumberFormatException nfe)
	{out.println("<b>Error de conexi&oacute;n</b><br>");}
%>
<form name="entry" action="../../servlet/ventanilla.DataServlet" method="post">
<!--<table border="0" cellspacing="0" align="center">
  <tr>
<% if (codigo.equals("0"))
   {%>
    <td><a id='btnCont' tabindex="1" href="javascript:document.entry.returnform.value='S';validate('1')"><img src="../imagenes/b_continuar.gif" border="0"></a></td>
<% }%>
    <td><a id='btnCont' tabindex="2" href="javascript:document.entry.returnform.value='S';cancelar()"><img src="../imagenes/b_cancelar.gif" border="0"></a></td>
    <td><a id='btnCont' tabindex="3" href="javascript:document.entry.returnform.value='S';resubmit('5503')"><img src="../imagenes/b_rap5.gif" border="0"></a></td>
    <td><a id='btnCont' tabindex="4" href="javascript:document.entry.returnform.value='S';resubmit('0728')"><img src="../imagenes/b_tele.gif" border="0"></a></td>
    <td><a id='btnCont' tabindex="5" href="javascript:document.entry.returnform.value='S';resubmit('0730')"><img src="../imagenes/b_luz.gif" border="0"></a></td>
  </tr>
</table> -->
<% if (codigo.equals("0"))
  {
    Hashtable datasession = (Hashtable)session.getAttribute("page.datasession");
    Vector txntdi = (Vector)datasession.get("Txn.Tdi");
    Vector datatdi = (Vector)datasession.get("DataxTxn.Tdi");
    int numtxntdi = txntdi.size();
   %>
<table border="1" cellspacing="0" align="center">
  <tr class="normaltextredbold">
     <td colspan="8" align="center"><Font color="black">Tarjeta TDI:</font>&nbsp;&nbsp;&nbsp;<b><%=datasession.get("tarjetaTDI")%></b></td>
  </tr>
  <tr align="center" bgcolor="#CCCCCC" class="normaltext" >
    <td><b>--</b></td>
    <td><b>Txn</b></td>
    <td><b>Servicio</b></td>
    <td><b>Descripci&oacute;n</b></td>
    <td><b>Monto</b></td>
    <td><b>Comisi&oacute;n</b></td>
    <td><b>Iva</b></td>
    <td><b>Total</b></td>
  </tr>
<% //int tab = (numtxntdi * 2) + 6;
   int tab = 1;
   if (numtxntdi == 0)
   {%>
    <tr class="normaltextredbold">
      <td colspan="8"><b>La Tarjeta TDI no tiene Transacciones Frecuentes, seleccione un servicio.</b></td>
   </tr>
   <%}
   for (int i =0; i<numtxntdi; i++)
   {
     Vector fields = (Vector)txntdi.get(i);
     Hashtable info = (Hashtable)datatdi.get(i);
     String comision = setFormat(setPointToString((String)info.get("Monto.Comision")));
     String iva = setFormat(setPointToString((String)info.get("Monto.Iva")));
     String refer1 = (String)fields.get(6);
     String refer2 = (String)fields.get(7);
     String refer3 = (String)fields.get(8);
     String txn = (String)fields.get(3);
     String servicio = (String)fields.get(4);
     servicio = servicio.trim();
     String nombserv = "No Disponible";
     txn = txn.trim();
     refer1= refer1.trim();
     refer2= refer2.trim();
     refer3= refer3.trim();
     if (txn.equals("0728"))
       refer1 = refer1.substring(0,10);
     ventanilla.com.bital.beans.DatosVarios datos = ventanilla.com.bital.beans.DatosVarios.getInstance();
     String nombre = datos.getDatosv("LOC" + setzeros(servicio,7));
     if (nombre != null)
       nombserv = nombre;
  %>
  <tr align="center" class="normaltext">
     <td><input type="checkbox" name="accion<%=i+1%>" tabindex="<%=tab++%>" onclick="validate('0')"></td>
     <td><b><%=txn%></b></td>  
     <td><%=servicio%></td>
     <td><%=nombserv%></td>
     <td><input type="text" name="monto<%=i+1%>" value="" maxlength="17" size="17" tabindex="<%=tab++%>" onchange="validate('0')"></td>
     <td><%=comision%><input type="hidden" name="comision<%=i+1%>" value="<%=comision%>"></td>
     <td><%=iva%><input type="hidden" name="iva<%=i+1%>" value="<%=iva%>"></td> 
<!--     <td>0.00<input type="hidden" name="comision<%=i+1%>" value="0.00"></td>
     <td>0.00<input type="hidden" name="iva<%=i+1%>" value="0.00"></td> -->
     <td><input type="text" name="total<%=i+1%>" value="" maxlength="17" size="17" readonly="true"></td>
  </tr>
  <tr>
     <td colspan="8">
     <table width="100%" align="center">
     <tr align="left" class="normaltext">
       <td><b>Referencia 1</b></td>
       <td><b>Referencia 2</b></td>
       <td><b>Referencia 3</b></td>
     </tr>
     </table>
	 <table width="100%" align="center">
     <tr align="left" class="normaltextred">
       <td><%=refer1%></td>
       <td><%=refer2%></td>
       <td><%=refer3%></td>
     </tr>
     </table>
     </td>
  </tr>
<%  } %>
</table>  
<table border="0" cellspacing="0" align="center">
  <tr>
<% if (numtxntdi > 0)
   {%>
    <td><a id='btnCont' tabindex="<%=tab++%>" href="javascript:document.entry.returnform.value='S';validate('1')"><img src="../imagenes/b_continuar.gif" border="0"></a></td>
<% }%>
    <td><a id='btnCont' tabindex="<%=tab++%>" href="javascript:document.entry.returnform.value='S';cancelar()"><img src="../imagenes/b_cancelar.gif" border="0"></a></td>
    <td><a id='btnCont' tabindex="<%=tab++%>" href="javascript:document.entry.returnform.value='S';resubmit('5503')"><img src="../imagenes/b_rap5.gif" border="0"></a></td>
    <td><a id='btnCont' tabindex="<%=tab++%>" href="javascript:document.entry.returnform.value='S';resubmit('0728')"><img src="../imagenes/b_tele.gif" border="0"></a></td>
    <td><a id='btnCont' tabindex="<%=tab++%>" href="javascript:document.entry.returnform.value='S';resubmit('0730')"><img src="../imagenes/b_luz.gif" border="0"></a></td>
  </tr>
</table>
<input type="hidden" name="NumTxnFre" value="<%=numtxntdi%>">
<%}%>
<input type="hidden" name="iTxn" value="<%= session.getAttribute("page.iTxn") %>">
<input type="hidden" name="oTxn" value="<%= session.getAttribute("page.oTxn") %>">
<input type="hidden" name="cTxn" value="<%= session.getAttribute("page.cTxn") %>">
<input type="hidden" name="sucursal" value="<%= session.getAttribute("branch") %>">
<input type="hidden" name="teller" value="<%= session.getAttribute("teller") %>">
<input type="hidden" name="moneda" value="<%= session.getAttribute("page.moneda")%>">
<input type="hidden" name="registro" value="<%=session.getAttribute("empno")%>">
<input type="hidden" name="tllrlvl" value="<%=session.getAttribute("tellerlevel")%>">
<input type="hidden" name="supervisor" value="<%= session.getAttribute("teller") %>">
<input type="hidden" name="AuthoOK" value="0">
<input type="hidden" name="SignOK" value="0">
<input type="hidden" name="validateFLD" value="0">
<input type="hidden" name="SignField" value="<%= session.getAttribute("page.SignField") %>">
<input type="hidden" name="override" value="N">
<input type="hidden" name="txtFees" value="0">
<input type="hidden" name="compania" value="20">
<input type="hidden" name="txtSelectedItem" value="0" >
<input type="hidden" name="returnform" value="N" >
<input type="hidden" name="returnform2" value="N" >
<input type="hidden" name="passw" value="<%=session.getAttribute("password")%>">
<input type="hidden" name="transaction" value="0000">
<input type="hidden" name="txnaislada" value="yes">
<input type="hidden" name="txnProcess" value="">
<input type="hidden" name="quickaccess" value="1">
<input type="hidden" name="noservicios" value="1">
<input type="hidden" name="isTarjetaTDI" value="1">
</form>
</BODY>
</HTML>