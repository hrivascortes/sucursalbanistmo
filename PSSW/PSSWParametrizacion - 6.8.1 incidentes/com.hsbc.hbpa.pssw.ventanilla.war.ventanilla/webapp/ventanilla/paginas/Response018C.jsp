<!--
//*************************************************************************************************
//             Funcion: JSP que presenta la respuesta de la txn 0018
//            Elemento: Response0018C.jsp
//          Creado por: Alejandro Gonzalez Castro
//      Modificado por: Alejandro Gonzalez Castro
//*************************************************************************************************
// CCN - 4360189 - 03/09/2004 - Se redirecciona a Final.jsp por Control de Efectivo
// CCN - 4360275 - 18/02/2005 - WAS 5.1
//*************************************************************************************************
-->

<%@page session="true" import="java.util.*"%>
<%
response.setHeader("Cache-Control","no-store"); //HTTP 1.1
response.setHeader("Pragma","no-cache"); //HTTP 1.0
response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<%!
  private String formatMonto(String s)
  {
    int len = s.length();
    if (len < 3)
    	s = "0." + s;
    else
    {
      int n = (len - 3 ) / 3;
    	s = s.substring(0, len - 2) + "." + s.substring(len - 2 , len);
      for (int i = 0; i < n; i++)
      {
      	len = s.length();
    		s = s.substring(0, (len -(i*3+6+i))) + "," + s.substring((len -(i*3+6+i)) , len);
    	}
    }
    return s;
  }

  private String delCeros(String s)
  {
    int len = 0;
    int space = 0;
    for(len = 0 ; len < s.length(); len++)
	  if(s.charAt(len) == '0')
	   space ++;
	  else
	   break;
    return s.substring(space, s.length());
  }
%>
<% Vector resp = (Vector)session.getAttribute("response");
	String banco1=null;
	String banco2=null;
	String banco3=null;
	String cta=null;
	String comen=null;
%>
<html>
<head>
<title>Consulta/mantenimiento OPRE</title>
<style type="text/css">Body {font-family: Arial, Helv; font-size: 10pt; font-weight: bold; color:#000000}</style>
</head>
<script language="JavaScript">
var valor="2";
var txnaproc = "0824";

function aceptar(forma)
{
	document.Txns.cTxn.value=txnaproc;
	document.Txns.opcion.value=valor;
	forma.submit();
}

function cancelar(forma)
{
	forma.action = "Final.jsp";
	forma.target = "panel";
	forma.submit();
}

function setValues()
{
	 valor = document.Txns.opcion.value;
	 if ((valor == 6 ) || (valor == 8))
	 	txnaproc = "18C1";
}

</script>
<body>
  <form name="Txns" action="../../servlet/ventanilla.DataServlet" method="post">
    <table border="0" cellspacing="0">
    <tr>
      <td>
        <input type="hidden" name="iTxn" value="018C">
        <input type="hidden" name="opcion">
        <input type="hidden" name="cTxn">
        <input type="hidden" name="BancoCE" value=<%out.print(banco1);%>>
        <input type="hidden" name="BancoCL" value=<%out.print(banco2);%>>
        <input type="hidden" name="BancoL" value=<%out.print(banco3);%>>
        <input type="hidden" name="cuenta" value=<%out.print(cta);%>>
        <input type="hidden" name="comentario" value=<%out.print(comen);%>>
        <input type="hidden" name="respaproc" value=<%out.print(session.getAttribute("respaproc"));%>>
        <input type="hidden" name="branch" value=<%out.print(session.getAttribute("branch"));%>>
      </td>
    </tr>
<%
   if( resp.size() < 1 )
   {
     out.println("<b>Error de conexi&oacute;n</b><br>");
   }
   else
   {
     try
     {
       int codigo = Integer.parseInt((String)resp.elementAt(0));
       String errorstr = (String)resp.elementAt(1);
       if((codigo != 0)||(resp.size() == 4))
       {
       	if (resp.size() == 4)
       		errorstr = (String)resp.elementAt(3);
			out.println("<tr><td>    </td></tr><tr><td>    </td></tr><tr><td>    </td></tr><tr><td>    </td></tr>");
			out.println("<tr><td>    </td></tr><tr><td>    </td></tr><tr><td>    </td></tr><tr><td>    </td></tr>");
			out.println("<tr><td>    </td></tr><tr><td>    </td></tr><tr><td>    </td></tr><tr><td>    </td></tr>");
		   out.println("<tr><td><b>" + errorstr + "</b></td></tr>");
			out.println("<tr><td>    </td></tr><tr><td>    </td></tr><tr><td>    </td></tr><tr><td>    </td></tr>");
			out.println("<tr><td>    </td></tr><tr><td>    </td></tr><tr><td>    </td></tr><tr><td>    </td></tr>");
			out.println("<tr><td>    </td></tr><tr><td>    </td></tr><tr><td>    </td></tr><tr><td>    </td></tr>");
			out.println("<tr>");
			if (codigo != 0)
			{
		   	out.println("      <td><a href=\"JavaScript:cancelar(document.Txns) \"><img src=\"../imagenes/b_cancelar.gif\" border=\"0\"></a></td>");
         	out.println("    </tr>");
	    	}
	    }
	    else
	    {
	      String accionOrden = (String)resp.elementAt(4);
		   out.println("    <tr>");
         out.println("      <td>Numero de OSN <b>:</b></td>");
         String OSN = (String)resp.elementAt(2);
         out.println("      <td><b> " + OSN + "</b></td>");
         out.println("      <td>Numero de TRN <b>:</b></td>");
         String TRN = (String)resp.elementAt(15);
         out.println("      <td><b>" + TRN + "</b></td>");
		   out.println("    </tr>");
		   out.println("    <tr>");
         out.println("      <td>Fecha Valor <b>:</b></td>");
	      String  fecha = (String)resp.elementAt(9);
	      fecha = "20" + fecha.substring(1,fecha.length());
	      fecha = fecha.substring(6,fecha.length()) + "/" + fecha.substring(4,6) + "/" + fecha.substring(0,4);
         out.println("      <td><b>" + fecha + "</b></td>");
         out.println("      <td>Banco Emisor <b>:</b></td>");
	      String  banco = (String)resp.elementAt(5);
         out.println("      <td><b>" + banco + "</b></td>");
		   out.println("    </tr>");
		   out.println("    <tr>");
         out.println("      <td>Ordenante <b>:</b></td>");
	      String  Ordenante = (String)resp.elementAt(13);
         out.println("      <td><b><h5>" + Ordenante + "</h5></b></td>");
		   out.println("    </tr>");
		   out.println("    <tr>");
  		   out.println("      <td>Banco C. Emisor <b>:</b></td>");
			if (accionOrden.charAt(11) !='2')
  		   {
		   	out.print("      <td><input maxlength=\"12\" name=\"BancoCE\" size=\"12\" type=\"text\"");
	      	banco1 = (String)resp.elementAt(6);
	      	if ((banco1 != null) && (banco1.length() > 0))
	      	{
	        		banco1 = banco1.trim();
           		out.print(" value=\"" + banco1 + "\"");
  		   	}
         	out.println("></td>");
	         out.println("      <td>Banco C. Liquidador <b>:</b></td>");
			   out.print("      <td><input maxlength=\"12\" name=\"BancoCL\" size=\"12\" type=\"text\"");
		      banco2 = (String)resp.elementAt(7);
	   	   if ((banco2 != null) && (banco2.length() > 0))
	      	{
	        		banco2 = banco2.trim();
           		out.print(" value=\"" + banco2 + "\"");
  		   	}
         	out.println("> </td>");
		   	out.println("    </tr>");
		   	out.println("    <tr>");
		   	out.println("      <td>Banco Liquidador <b>:</b></td>");
		   	out.print("      <td><input maxlength=\"35\" name=\"BancoL\" size=\"20\" type=\"text\"");
	      	banco3 = (String)resp.elementAt(8);
		      if ((banco3 != null) && (banco3.length() > 0))
		      {
	   	     banco3 = banco3.trim();
         	  out.print(" value=\"" + banco3 + "\"");
  		   	}
         	out.print("> </td>");
         	out.println("      <td>Cuenta<b>:</b></td>");
		   	out.print("      <td><input maxlength=\"18\" name=\"cuenta\" size=\"12\" type=\"text\"");
	      	cta = delCeros((String)resp.elementAt(16));
	      	if ((cta != null) && (cta.length() > 0))
	      	{
	        		cta = cta.trim();
           		out.print(" value=\"" + cta + "\"");
  		   	}
         	out.print("> </td>");
         }
         else
         {
		      banco1 = (String)resp.elementAt(6);
	         out.println("      <td><b>" + banco1 + "</b></td>");
	         out.println("      <td>Banco C. Liquidador <b>:</b></td>");
		      banco2 = (String)resp.elementAt(7);
	         out.println("      <td><b>" + banco2 + "</b></td>");
		   	out.println("    </tr>");
		   	out.println("    <tr>");
		   	out.println("      <td>Banco Liquidador <b>:</b></td>");
		      banco3 = (String)resp.elementAt(8);
	         out.println("      <td><b>" + banco3 + "</b></td>");
         	out.println("      <td>Cuenta<b>:</b></td>");
	      	cta = delCeros((String)resp.elementAt(16));
	         out.println("      <td><b>" + cta + "</b></td>");
         }
		   out.println("    </tr>");
		   out.println("    <tr>");
         out.println("      <td>Moneda <b>:</b></td>");
	      String  monedastr = (String)resp.elementAt(10);
         out.println("      <td><b>" + monedastr + "</b></td>");
  		   out.println("      <td>Monto <b>:</b></td>");
	      String  Monto = delCeros((String)resp.elementAt(11));
	      Monto = formatMonto(Monto);
         out.println("      <td><b>" + Monto + "</b></td>");
		   out.println("    </tr>");
		   out.println("    <tr>");
         out.println("      <td>Beneficiario<b>:</b></td>");
	      String  Bene = (String)resp.elementAt(12);
         out.println("      <td><b><h5>" + Bene + "</h5></b></td>");
		   out.println("    </tr>");
			if (accionOrden.charAt(11) !='2')
  		   {
			   out.println("    <tr>");
			   out.println("      <td>Estatus de la Orden<b>:</b></td>");
			   out.println("    </tr>");
				String statusOrden = (String)resp.elementAt(4);
				if (statusOrden.charAt(0) =='1')
			  		out.println("    <tr><td></td><td><b><h5>Moneda Invalida            </h5></b></td></tr>");
				if (statusOrden.charAt(1) =='1')
			     out.println("    <tr><td></td><td><b><h5>Banco Liquidador Invalido  </h5></b></td></tr>");
			   if (statusOrden.charAt(2) =='1')
				  out.println("    <tr><td></td><td><b><h5>Banco Corresp. Invalido    </h5></b></td></tr>");
				if (statusOrden.charAt(3) =='1')
		     		out.println("    <tr><td></td><td><b><h5>Cuenta Invalida para MERVA </h5></b></td></tr>");
				if (statusOrden.charAt(4) =='1')
		     		out.println("    <tr><td></td><td><b><h5>Fecha Futura               </h5></b></td></tr>");
				if (statusOrden.charAt(5) =='1')
		     		out.println("    <tr><td></td><td><b><h5>Posible Duplicidad TRN            </h5></b></td></tr>");
				if (statusOrden.charAt(6) =='1')
		     		out.println("    <tr><td></td><td><b><h5>No Existe la Cuenta               </h5></b></td></tr>");
				if (statusOrden.charAt(6) =='2')
		     		out.println("    <tr><td></td><td><b><h5>Cuenta Beneficiario no Disponible</h5></b></td></tr>");
				if (statusOrden.charAt(7) =='1')
		     		out.println("    <tr><td></td><td><b><h5>Subproducto HOGAN Invalido       </h5></b></td></tr>");
				if (statusOrden.charAt(7) =='2')
		     		out.println("    <tr><td></td><td><b><h5>Subproducto HOGAN no Disponible  </h5></b></td></tr>");
				if (statusOrden.charAt(8) =='1')
		     		out.println("    <tr><td></td><td><b><h5>Estatus Invalido de la Cta HOGAN</h5></b></td></tr>");
		   	if (statusOrden.charAt(8) =='2')
		     		out.println("    <tr><td></td><td><b><h5>Estatus de la Cta HOGAN no Disp.</h5></b></td></tr>");
				if (statusOrden.charAt(9) =='1')
		     		out.println("    <tr><td></td><td><b><h5>Cuenta con Restriccion HOGAN    </h5></b></td></tr>");
		   	if (statusOrden.charAt(9) =='2')
		     		out.println("    <tr><td></td><td><b><h5>Restricciones HOGAN no Disp.    </h5></b></td></tr>");
		   	if (statusOrden.charAt(12) =='1')
		     		out.println("    <tr><td></td><td><b><h5>Devolucion Comision,Iva,Monto   </h5></b></td></tr>");
		   	if (statusOrden.charAt(12) =='2')
		     		out.println("    <tr><td></td><td><b><h5>Devolucion no Disponible        </h5></b></td></tr>");
				if (statusOrden.charAt(14) =='1')
		     		out.println("    <tr><td></td><td><b><h5>Hogan no estuvo Disponible      </h5></b></td></tr>");
         	out.println("    <tr><td>Comentarios<b>:</b></td>");
		      comen = (String)resp.elementAt(14);
			   out.print("      <td><input maxlength=\"40\" name=\"comentario\" size=\"20\" type=\"text\"");
	   	   if ((comen != null) && (comen.length() > 0))
	      	{
	        		comen = comen.trim();
           		out.print(" value=\"" + comen + "\"");
  		   	}
         	out.println("> </td>");
		   	out.println("      <td>Mtto. a realizar<b>:</b></td>");
		   	out.println("      <td>");
		   	out.println("        <SELECT name=opcion size=1 onchange=\"setValues()\">");
				if ((accionOrden.charAt(11) !='1') && (accionOrden.charAt(11) !='0'))
				{
			  		out.println("          <OPTION value=1>Pagado Manual </OPTION>");
		     		out.println("          <OPTION value=7>Por Pagar     </OPTION>");
			  		if (accionOrden.charAt(11) !='9')
			  		{
			    		out.println("          <OPTION value=6>Por Devolver  </OPTION>");
		       		out.println("          <OPTION value=8>Banco Nacional</OPTION>");
		     		}
		   	}
		   	else
			  		out.println("          <OPTION value=2>Cambio a estatus 2 </OPTION>");
		   	out.println("        </SELECT>");
	      	out.println("      </td>");
         	out.println("    </tr>");
				out.println("<tr><td>    </td></tr><tr><td>    </td></tr><tr><td>    </td></tr><tr><td>    </td></tr>");
				out.println("<tr><td>    </td></tr><tr><td>    </td></tr><tr><td>    </td></tr><tr><td>    </td></tr>");
         	out.println("    <tr>");
         	out.println("      <td><a href=\"JavaScript:aceptar(document.Txns) \"><img src=\"../imagenes/b_aceptar.gif\" border=\"0\"></a></td>");
			}
			else
         	out.println("    <tr>");
		   out.println("      <td><a href=\"JavaScript:cancelar(document.Txns) \"><img src=\"../imagenes/b_cancelar.gif\" border=\"0\"></a></td>");
         out.println("    </tr>");
	    }
     }
     catch(NumberFormatException nfe)
     {
       out.println("<b>Error de conexi&oacute;n (mainframe)</b><br>");
     }
   }
%>
    </table>
  </form>
</body>
</html>
