<!--
//*************************************************************************************************
//            Funcion: JSP que despliega txn 0372
//            Elemento: page0372.jsp
//          Creado por: Alejandro Gonzalez Castro
//      Modificado por: Alejandro Gonzalez Castro
//*************************************************************************************************
// CCN - 4360192 - 31/08/2004 - Evitar doble click sobre el boton
//*************************************************************************************************
-->


<%@ page session="true" import="java.util.Hashtable"%>
<%
response.setHeader("Cache-Control","no-store"); //HTTP 1.1
response.setHeader("Pragma","no-cache"); //HTTP 1.0
response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
%>
<html>
<head>
  <title>0732</title>
<style type="text/css">Body {font-family: Arial, Helv; font-size: 10pt; font-weight: bold; color:#000000}</style>
</head>
<body>
<strong>INGRESOS VARIOS</strong>
<p>
<%
  Hashtable local = (Hashtable)session.getAttribute("page.datasession");
%>
<table border="0" cellspacing="0">
<tr>
  <td><strong>N&uacute;mero de Servicio</strong></td>
  <td width="10">&nbsp;</td>
  <td align="right">0372</td>
</tr>
<tr>
  <td><strong>Referencia</strong></td>
  <td></td>
  <td align="right">IVA COM.CHQ.CAJA</td>
</tr>
<tr>
  <td><strong>Efectivo</strong></td>
  <td></td>
  <td align="right"><%= local.get("txtIVA") %></td>
</tr>
<tr>
  <td><strong>Monto</strong></td>
  <td></td>
  <td align="right"><%= local.get("txtIVA") %></td>
</tr>
</table>
<p>
<form name="entry" action="../../servlet/ventanilla.DataServlet" method="post" onsubmit="submite(document.entry)">
<input type="hidden" name="iTxn" value="<%= session.getAttribute("page.iTxn") %>">
<input type="hidden" name="oTxn" value="<%= session.getAttribute("page.oTxn") %>">
<input type="hidden" name="cTxn" value="<%= session.getAttribute("page.cTxn") %>">
<input type="hidden" name="sucursal" value="<%= session.getAttribute("branch") %>">
<input type="hidden" name="teller" value="<%= session.getAttribute("teller") %>">
<input type="hidden" name="moneda" value="<%= session.getAttribute("page.moneda")%>">
<input type="hidden" name="registro" value="<%=session.getAttribute("empno")%>">
<input type="hidden" name="tllrlvl" value="<%=session.getAttribute("tellerlevel")%>">
<input type="hidden" name="supervisor" value="<%= session.getAttribute("teller") %>">
<input type="hidden" name="AuthoOK" value="0">
<input type="hidden" name="SignOK" value="0">
<input type="hidden" name="validateFLD" value="0">
<input type="hidden" name="SignField" value="<%= session.getAttribute("page.SignField") %>">

<input type="hidden" name="txtEfectivo" value="<%= local.get("txtIVA") %>">
<input type="hidden" name="txtReferenc" value="IVA COM.CHQ.CAJA">
<input id='btnCont' type="image" src="../imagenes/b_aceptar.gif" border="0">
</form>
<script language="JavaScript">
function submite(form)
{
    form.submit();
    var obj = document.getElementById('btnCont');
    obj.onclick = new Function('return false');
}
</script>
</body>
</html>
