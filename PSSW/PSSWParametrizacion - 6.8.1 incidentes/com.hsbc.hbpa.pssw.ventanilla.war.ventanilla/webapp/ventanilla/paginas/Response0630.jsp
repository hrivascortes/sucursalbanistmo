<!--
//*************************************************************************************************
//             Funcion: JSP que despliega respuesta de txn 0630
//            Elemento: Response0630.jsp
//          Creado por: Alejandro Gonzalez Castro
//      Modificado por: Juvenal Fernandez
//*************************************************************************************************
// CCN - 4360175 - 03/08/2004 - Ajuste size de ventana de impresion
// CCN - 4360189 - 03/09/2004 - Se resta la comision del monto Total en vez de sumarla
//                              y Se redirecciona a Final.jsp por Control de Efectivo
// CCN - 4360275 - 18/02/2005 - WAS 5.1
//*************************************************************************************************
-->
<%@ page session="true" import="java.util.*"%>
<%!
        private String replacespace(String campo)
        {
            String temp = "";
            for (int i=0;  i<campo.length(); i++)
            {
                if(campo.charAt(i) != ' ')
                {temp = temp + campo.charAt(i);}
                else
                {temp = temp + "%20";}
            }
            return temp;
        }

       private String replaceCats(String campo)
        {
            String temp = "";
            for (int i=0;  i<campo.length(); i++)
            {
                if(campo.charAt(i) != '#')
                {temp = temp + campo.charAt(i);}
                else
                {temp = temp + "%23";}
            }
            return temp;
        }

	private String addpunto(String valor)
	{
	int longini = valor.length();
    String cents  = valor.substring(longini-2,longini);
	String entero = valor.substring(0,longini-2);
	int longente = entero.length();
	for (int i = 0; i < (longente-(1+i))/3; i++)
	{
		longente = entero.length();
	    entero = entero.substring(0,longente-(4*i+3))+','+entero.substring(longente-(4*i+3));
	}
  	entero = entero + '.' + cents;
	return entero;
	}

  private String getdomi(String r)
  {
		String domicilio = r.substring(77,108);
		return domicilio;
  }
  private String getString(String s)
  {
    int len = s.length();
    for(; len>0; len--)
	  if(s.charAt(len-1) != ' ')
	    break;

    return s.substring(0, len);
  }

  private String getItem(String newCadNum, int newOption)
  {
   int iPIndex = newCadNum.indexOf("CONSECUTIVO:") + 12, NumSaltos, Num = 0, Ban = 0;
   String nCadNum = new String("");
   NumSaltos = newOption;
   while(Num < NumSaltos)
   {
    if(newCadNum.charAt(iPIndex) == 0x20)
    {
     while(newCadNum.charAt(iPIndex) == 0x20)
      iPIndex++;
     Num++;
    }
    else
     while(newCadNum.charAt(iPIndex) != 0x20)
      iPIndex++;
   }
   while(newCadNum.charAt(iPIndex) != 0x20 && iPIndex < newCadNum.length()){
    nCadNum = nCadNum + newCadNum.charAt(iPIndex);
    if(iPIndex < newCadNum.length())
      iPIndex++;
    if(iPIndex == newCadNum.length())
      break;
   }
   return nCadNum;
  }

  private String delLeadingfZeroes(String newString)
  {
   StringBuffer newsb = new StringBuffer(newString);
   int Len = newsb.length();
   for(int i=0; i<newsb.length(); i++)
   {
    if(newsb.charAt(i) != '0')
    {
	 newsb.delete(0,i);
	 break;
	}
   }
   return newsb.toString();
  }

  private String setPointToString(String newCadNum)
  {
   int iPIndex = newCadNum.indexOf("."), iLong;
   String szTemp;

   if(iPIndex > 0)
   {
    newCadNum = new String(newCadNum + "00");
    newCadNum = newCadNum.substring(0, iPIndex + 1) + newCadNum.substring(iPIndex + 1, iPIndex + 3);
   }
   else
   {
    for(int i = newCadNum.length(); i < 3; i++)
     newCadNum = "0" + newCadNum;
    iLong = newCadNum.length();
    if(iLong == 3)
     szTemp = newCadNum.substring(0, 1);
    else
     szTemp = newCadNum.substring(0, iLong - 2);
    newCadNum = szTemp + "." + newCadNum.substring(iLong - 2);
   }

   return newCadNum;
  }

  private String delPointOfString(String newCadNum)
  {
   int iPIndex = newCadNum.indexOf(".");

   if(iPIndex > 0)
   {
    newCadNum = newCadNum.substring(0, iPIndex) +
                newCadNum.substring(iPIndex + 1, newCadNum.length());
   }

   return newCadNum;
  }

  private String delCommaPointFromString(String newCadNum)
  {
   String nCad = new String(newCadNum);
   if( nCad.indexOf(".") > -1)
   {
    nCad = nCad.substring(nCad.indexOf(".") + 1, nCad.length());
    if(nCad.length() != 2)
    {
     String szTemp = new String("");
     for(int j = nCad.length(); j < 2; j++)
      szTemp = szTemp + "0";
     newCadNum = newCadNum + szTemp;
    }
   }

   StringBuffer nCadNum = new StringBuffer(newCadNum);
   for(int i = 0; i < nCadNum.length(); i++)
    if(nCadNum.charAt(i) == ',' || nCadNum.charAt(i) == '.')
     nCadNum.deleteCharAt(i);

   return nCadNum.toString();
  }

  private String setFormat(String newCadNum)
  {
    int iPIndex = newCadNum.indexOf(".");
    String nCadNum = new String(newCadNum.substring(0, iPIndex));
    for (int i = 0; i < (int)((nCadNum.length()-(1+i))/3); i++)
     nCadNum = nCadNum.substring(0, nCadNum.length()-(4*i+3))+','+nCadNum.substring(nCadNum.length()-(4*i+3));

    return nCadNum + newCadNum.substring(iPIndex, newCadNum.length());
  }

  private String longtoString(long newlong){
    String newString = String.valueOf(newlong);
    return newString.toString();
  }

%>
<%
  Vector resp = (Vector)session.getAttribute("response");
  Hashtable datasession = (Hashtable)session.getAttribute("page.datasession");
%>
<html>
<head>
  <title>Certificación</title>
<style type="text/css">Body {font-family: Arial, Helv; font-size: 10pt; font-weight: bold; color:#000000}</style>
</head>
<script language="javascript">
function aceptar(forma)
{
	forma.submit();
}
function cancelar(forma)
{
   forma.action = "../paginas/Final.jsp";
   forma.submit();
}
function continuar(forma)
{
  alert("documento Impreso...");
}
function imprime()
{
	if(!window.confirm("Desea Imprimir Comprobante."))
	{
	  document.Txns.action = "paginas/ayuda.html";
	  document.Txns.submit();
	}
}
  numVeces = 1;
  var controlInterval;
  function validate(){
    clearInterval(controlInterval);
    document.forms[0].submit();
  }
  
</script>
<body>
<%
  String cTxn = (String)datasession.get("cTxn");
  String oTxn = (String)datasession.get("oTxn");

  if(oTxn.equals("0476") && !cTxn.equals("0372")){
   out.println("<form name=\"Txns\" action=\"../../servlet/ventanilla.DataServlet\" method=\"post\">");
   out.println("<input type=\"hidden\" name=\"txtInvoqued\" value=\"" + "somedatahere" + "\">");
  }
  else
  {   out.println("<form name=\"Txns\" method=\"post\">");}
%>
<table border="0" cellspacing="0">
<tr>
<td>
</td>
</tr>
<tr>
<td>
  <input type="hidden" name="transaction" value="<%= session.getAttribute("page.iTxn") %>">
  <input type="hidden" name="transaction1" value="<%= session.getAttribute("page.oTxn") %>">
</td>
</tr>
</table>
<p>
<%
   String szResp = new String("");
   int codigo = 1;
   if( resp.size() != 4 )
    out.println("<b>Error de conexi&oacute;n</b><br>");
   else
   {
    try{
     codigo = Integer.parseInt((String)resp.elementAt(0));
    }
    catch(NumberFormatException nfe){
     out.println("<b>Error de conexi&oacute;n</b><br>");
    }
    if( codigo != 0 )
      out.println("<b>Error en transaccion</b><br>");
     int lines = Integer.parseInt((String)resp.elementAt(1));
     szResp = (String)resp.elementAt(3);
     for(int i=0; ; i++){
      String line = szResp.substring( i*78, Math.min(i*78+77, szResp.length()) );
      out.println(getString(line) + "<p>");
      if(Math.min(i*78+77, szResp.length()) == szResp.length())
       break;
     }
   }
%>
<%
  if(oTxn.equals("0476"))
  {
	Stack flujotxn = (Stack)session.getAttribute("page.flujotxn");
	if (!flujotxn.empty())
	{
		String newtxn = flujotxn.pop().toString();
		datasession.put("cTxn",newtxn);
                if (flujotxn.empty())
                   flujotxn.push("0000");
        	if(newtxn.equals("0000"))
                   flujotxn.pop();
                session.setAttribute("page.cTxn", cTxn);
	}
  }
  else
  { datasession.put("CobCom","no");}

  if(oTxn.equals("0476") && !cTxn.equals("0372") && !cTxn.equals("0829")){
   out.print("<div align=\"center\">");
   out.print("</div>");
  }
 if(cTxn.equals("0829"))
 {
   String txtComision = getItem(szResp, 3);
   String txtPIVA = getItem(szResp, 4);
   String txtdomici = getdomi(szResp);
   int intcom = Integer.parseInt(txtComision);
   int intiva = Integer.parseInt(txtPIVA);
   if (intcom > 0 && intiva > 0)
   {
   	datasession.put("txtComision", delLeadingfZeroes(txtComision));
   	datasession.put("txtPIVA", delLeadingfZeroes(txtPIVA));
   	datasession.put("txtdomici", delLeadingfZeroes(txtdomici));
   	session.setAttribute("page.datasession", datasession);
	datasession.put("CobCom","si");
	}
	else
	{
	 datasession.put("CobCom","no");
	}
	if (datasession.get("CobCom").toString().equals("si"))
	{
%>
	<table border="0" cellspacing="0">
	<tr>
	<td>Monto:</td>
	<td></td>
	<td  align="right"><%
	 long montoComision = Long.parseLong(datasession.get("txtComision").toString());
	 long montoTxn = Long.parseLong(delCommaPointFromString(datasession.get("txtMonto").toString()));
	 long montoIVA = Long.parseLong(datasession.get("txtPIVA").toString()) * montoComision / 100;
	 long montoTotal = montoTxn - montoComision - montoIVA;
	 out.print(setFormat(setPointToString(longtoString(montoTxn))));
	%></td>
	</tr>
	<tr>
	<td>Comision:</td>
	<td></td>
	<td align="right"><%=setFormat(setPointToString(longtoString(montoComision)))%></td>
	</tr>
	<tr>
	<td>IVA:</td>
	<td></td>
	<td align="right"><%=setFormat(setPointToString(longtoString(montoIVA)))%></td>
	</tr>
	<tr>
	<td>Total:</td>
	<td></td>
	<td align="right"><%=setFormat(setPointToString(longtoString(montoTotal)))%></td>
	</tr>
	<tr>
	<td><a id='continuar_img' tabindex = '1' href="javascript:aceptar(document.Txns) "><img src="../imagenes/b_continuar.gif" border="0"></a></td>
	<td><a tabindex = '2' href="javascript:cancelar(document.Txns) "><img src="../imagenes/b_cancelar.gif" border="0"></a></td>
	</tr>
	</table>
	<script language="javascript">
	
		var liga = document.getElementById('continuar_img');
		liga.focus();

	</script>
<%
	   	datasession.put("montoComision", longtoString(montoComision));
   		datasession.put("montoIVA", longtoString(montoIVA));
                long Totalcomiva = montoComision + montoIVA;
                datasession.put("Totalcomiva", longtoString(Totalcomiva));
	}
  }
%>

<%//Certificacion Inicio
  String isCertificable = (String)session.getAttribute("page.certifField");  // Para Certificación
  String txtCadImpresion  = ""; // Para Certificación
   if(!isCertificable.equals("N") && !cTxn.equals("0829")){ // Para Certificación
    session.setAttribute("Consecutivo", getItem((String)resp.elementAt(3), 1)); // Para Certificación
    session.setAttribute("Tipo", isCertificable); // Para Certificación
    if ( cTxn.equals("0372") && oTxn.equals("0476")) {
        long Monto1 = Long.parseLong(delCommaPointFromString(datasession.get("txtMonto").toString())) - Long.parseLong(datasession.get("montoComision").toString()) - Long.parseLong(datasession.get("montoIVA").toString());
        Long TotalCI = new Long(Monto1);
        String txtTotal = TotalCI.toString();
        txtCadImpresion = "~COMPTDC~" + datasession.get("txtRefer10630").toString() + "~" + datasession.get("txtMonto").toString() + "~" + setFormat(setPointToString(datasession.get("montoComision").toString())) + "~" + setFormat(setPointToString(datasession.get("montoIVA").toString())) + "~" + setFormat(setPointToString(txtTotal)) + "~";
    }
     if (cTxn.equals("0372") && datasession.get("lstComprobante").toString().equals("si")) 
     {
      if ( !oTxn.equals("0476") ) 
         txtCadImpresion = "~CFISCAL~" + datasession.get("txtNombre").toString().toUpperCase() + "~" + (String)datasession.get("txtRfc") + "~ ~ ~" + datasession.get("txtDomici").toString().toUpperCase() + "~" + setFormat(setPointToString((String)datasession.get("montoComision"))) + "~" + setFormat(setPointToString((String)datasession.get("montoIVA"))) + "~" + setFormat(setPointToString((String)datasession.get("Totalcomiva"))) + "~" + (String)datasession.get("txtdomici") + "~";
      else
         txtCadImpresion = txtCadImpresion + "CFISCAL~" + datasession.get("txtNombre").toString().toUpperCase() + "~" + (String)datasession.get("txtRfc") + "~TARJETA DE CREDITO~DISPOSICION EN EFECTIVO~" + datasession.get("txtDomici").toString().toUpperCase() + "~" + setFormat(setPointToString((String)datasession.get("montoComision"))) + "~" + setFormat(setPointToString((String)datasession.get("montoIVA"))) + "~" + setFormat(setPointToString((String)datasession.get("Totalcomiva"))) + "~" + (String)datasession.get("txtdomici") + "~";
      txtCadImpresion = replaceCats(txtCadImpresion);
      }
      session.setAttribute("txtCadImpresion", txtCadImpresion); // Para Certificación
      if ( cTxn.startsWith("08")) //Transacción de consulta
          out.println("<a href=\"#\" onClick=\"top.openDialog('../../servlet/ventanilla.ImprimirServlet', 160, 120, 'top.setPrefs4()', Txns)\"><img src=\"../imagenes/b_continuar.gif\" border=\"0\"></a>"); // Para Certificación
      else
          out.println("<script language=\"javascript\">top.openDialog('../../servlet/ventanilla.ImprimirServlet', 160, 120, 'top.setPrefs4()', document.Txns)</script>"); // Para Certificación
   }
  //Certificaion Fin  %>
</form>
<%
	if (datasession.get("CobCom").toString().equals("no") && oTxn.equals("0476"))
	{
	 datasession.put("CobCom","NO");%>
	<script language="javascript">
		aceptar(document.Txns);
	</script>
<%	}
%>
</body>
</html>
