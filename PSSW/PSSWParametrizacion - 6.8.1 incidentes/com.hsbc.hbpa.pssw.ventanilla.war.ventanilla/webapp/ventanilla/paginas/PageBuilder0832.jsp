<!--
//*************************************************************************************************
//            Elemento: PageBuilder832.jsp
//             Funcion: JSP de captura. 
//          Creado por: Alejandro Gonzalez Castro
// Ultima Modificacion: 21/07/2006
//      Modificado por: Fausto Rodrigo Flores Moreno
//*************************************************************************************************
// CCN - 4360276 - 17/02/2005 - Se agrega funcion serv() para validar el bloqueo de Cobranza personalizada
// CCN - 4360402 - 11/11/2005 - Se modifica texto de la funcion serv() para validar el bloqueo de Cobranza personalizada
// CCN - 4360425 - 03/02/2006 - Proyecto Loch Ness
// CCN - 4360498 - 21/07/2006 - Se realiza validaci�n para bloqueo de servicios correspondientes a Cobranza Personalizada
//                              que contengan ceros a la izquierda
// CCN - 4360581 - 12/03/2007 - Se convierten los estilos en jsf y se hace un include en el jsp
//*************************************************************************************************
-->

<%@ page session="true" import="java.util.*"  %>

<% 
response.setHeader("Cache-Control","no-store"); //HTTP 1.1
response.setHeader("Pragma","no-cache"); //HTTP 1.0
response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<%
String MontoPro = new String("");
String txnAuthLevel = (String)session.getAttribute("page.txnAuthLevel");
String txnIMGAutoriz = (String)session.getAttribute("page.txnIMGAutoriz");
Vector v = (Vector)session.getAttribute("page.specialacct");
String TxnE = (String)session.getAttribute("page.iTxn");
Hashtable datasession = (Hashtable)session.getAttribute("page.datasession");
if(datasession == null)
  datasession = new Hashtable();

String noservicios =  (String)session.getAttribute("page.noservicios");
int nserv = Integer.parseInt(noservicios);
int k;
%>
<html>
<head>
<%@include file="style.jsf"%>
<title>Datos de la Transaccion</title>
<link rel="stylesheet" href="../ventanilla/estilos/jquery-ui.css">
<script language="javascript" type="text/javascript" src="../ventanilla/js/jquery.js"></script>
<script language="javascript" type="text/javascript" src="../ventanilla/js/jquery-ui.js"></script>
<script language="javascript" type="text/javascript">

$(function() {
 var srvRAP = [
  <%
  ventanilla.com.bital.beans.DatosVarios arpDVarios = ventanilla.com.bital.beans.DatosVarios.getInstance();
  Vector vecRAP = (Vector)arpDVarios.getDatosV("ARP");
  if (vecRAP != null && !vecRAP.isEmpty()) {
  	session.setAttribute("servicioARP", vecRAP);
  	for (int j = 0; j < vecRAP.size(); j++) {
  	    String dato = (String)vecRAP.get(j);
  	    StringTokenizer ser = new StringTokenizer(dato,"*");
  	    String strVisual = ser.nextToken();
  	    if (strVisual.equalsIgnoreCase("s")) {
  			String noServicio = String.valueOf(Integer.parseInt(ser.nextToken()));
  	 	 	String strDescripcion = ser.nextToken();
  	 	 	%>
  	 	    {value:"<%=noServicio%>", label:"<%=strDescripcion+".- "+noServicio%>", enLinea:false},
  	 		<%
  	 	}
  	}
  }
  
  %>];<%
  
  Hashtable hashRAPL = (Hashtable)arpDVarios.getDatosH("SERVICRAPL");
  if (hashRAPL != null && !hashRAPL.isEmpty()) {
  	Enumeration keysRAPL = hashRAPL.keys();
  	while (keysRAPL.hasMoreElements()) {
  	  	String keyRAPL = (String)keysRAPL.nextElement();
  	  	ventanilla.com.bital.sfb.ServicioRAPL srvRAPL = (ventanilla.com.bital.sfb.ServicioRAPL)hashRAPL.get(keyRAPL);
  	  	%>
  	  	var result = $.grep(srvRAP, function(e){ return e != null && e.value == "<%=srvRAPL.getNoServicio()%>"; });
  	  	if (result.length == 0) {
  	  		srvRAP.push({value:"<%=srvRAPL.getNoServicio()%>", label:"<%=srvRAPL.getDescServicio()+".- "+srvRAPL.getNoServicio()%>", enLinea:true});
  	  	} else {
   	  		result[0].enLinea = true;
  	  	}
  		<%
  	}
  }
  %>

<% for (int i = 1; i < nserv+1;i++) { %>

 $( "#servicioBusq<%=i%>" )
 	.autocomplete({
       minLength: 0,
       source: srvRAP,
       focus: function( event, ui ) {
         return false;
       },
       select: function( event, ui ) {
         $("#servicio<%=i%>").val( ui.item.value );
         $("#servicioBusq<%=i%>").val( ui.item.label );
         $("#snServicioEnLinea<%=i%>").val( ui.item.enLinea );
         return false;
       }
     })
     .autocomplete( "instance" )._renderItem = function( ul, item ) {
	    return $( "<li>" )
	       .append( "<a>" + item.label + "</a>" )
	       .appendTo( ul );
	 };
 });
 
<%}%>
</script>
<script language="javaScript">
<!--
function wasNotEnter(){

	if(window.event.keyCode == '13'){
		return false;		
	}else{
		return true;		
	}
}
var formName = 'Mio'
var specialacct = new Array()
<%
if(!v.isEmpty())
{
  for(int iv = 0; iv < v.size(); iv++){out.print("specialacct[" + iv + "]='" + v.get(iv) + "'" + '\n');}
}%>

function validate(form)
{
<%
    if(TxnE.equals("5503"))
    {
        for (int i = 1; i < nserv+1;i++)
        {
            out.println("  if( !top.validate(window, document.entry.servicio" + i + ", 'isnumeric') )");
            out.println("    return");
            out.println("  if( !top.validate(window, document.entry.nopagos" + i + ", 'isnumeric') )");
            out.println("    return");
            out.println("  if( !top.validate(window, document.entry.monto" + i + ", 'isValMtoRAP') )");
            out.println("    return");             
        }
        %>
        if ( <%=nserv%> == 1 && document.entry.snServicioEnLinea<%=nserv%>.value == "true") {
        	document.entry.iTxn.value = "ARPL";
        	document.entry.oTxn.value = "ARPL";
        	document.entry.cTxn.value = "A832";
        	document.entry.flujorapcapr.value = "1";
        	document.entry.np.value = "0";
        }
        <%
    }
%>
  form.validateFLD.value = '1'
  if (document.entry.txtFechaEfect || document.entry.txtFechaError || document.entry.txtFechaCarta)
    document.entry.txtFechaSys.value=top.sysdatef();
  
<% if(!txnAuthLevel.equals("1") && !txnIMGAutoriz.equals("1"))
	{
    out.println("  form.submit();");
    out.println("var obj = document.getElementById('btnCont');"); 
    out.println("obj.onclick = new Function('return false');");   
	}   
%>
}
function AuthoandSignature(entry)
{
 var Cuenta;
 if(entry.validateFLD.value != '1')
  return
 if(entry.needAutho.value == '1' && entry.AuthoOK.value != '1')
 {
  top.openDialog('../ventanilla/paginas/authoriz.jsp', 220, 130, 'top.setPrefs()', entry)
 }
 if(entry.needSignature.value == '1' && entry.SignField.value != '')
{
  Cuenta = eval('entry.' + entry.SignField.value + '.value')
  top.openModalDialog('<%=request.getContextPath()%>/servlet/ventanilla.FirmasServlet?txtDDACuenta=' + Cuenta, 710, 430, 'top.setPrefs2()', entry)
 }
}

function serv()
{<%
 ventanilla.com.bital.beans.DatosVarios rap = ventanilla.com.bital.beans.DatosVarios.getInstance();                   
 Vector vecRAP1 = new Vector();        
 vecRAP1 =(Vector)rap.getDatosV("RAPCP");   
 Vector vecRAPtmp = new Vector();
 String servTmp = "";
 String strServ = "";
          
 int szvecRAP = vecRAP1.size();
 int FlagServicio = 0;
 for (int x=0;x<szvecRAP;x++)
 {
    Vector vect = (Vector)vecRAP1.get(x);
    String serv = (String)vect.get(1);
    if (!serv.equals("0000"))
    {   
 		servTmp = (String)vect.get(1);
   		for(int h = 0; h<=(7-serv.length());h++)
   		{
  			String strElement = (String)vect.get(0)+","+servTmp+","+(String)vect.get(2);
       		vecRAPtmp.addElement(strElement);
   			servTmp = "0"+servTmp;
   		}
   	}
 }
 for (int i = 1; i < nserv+1;i++)
 {    
 	for (int x=0;x<vecRAPtmp.size();x++)
    {
       String dato = (String)vecRAPtmp.get(x);
       StringTokenizer ser = new StringTokenizer(dato,",");
       ser.nextToken();
       strServ = ser.nextToken();
       ser.nextToken();
    %>
       strServicio = document.entry.servicio<%=i%>.value
       if(strServicio == '<%=strServ%>')
       {
          alert("Para el servicio "+<%=String.valueOf(Integer.parseInt(strServ))%>+" usar Txn RAPM Cobranza Personalizada: \n - Ingresa al men� Principal de RAP y selecciona la opci�n RAPM (Cobranza Personalizada). \n - Esta operaci�n por el momento no se ingresa por n�mero de txn. \n - En caso de dudas, consulta el IT CASE5-009/Anexo Operaci�n o llama al CIB ") ;
          document.entry.servicio<%=i%>.value = "";
          document.entry.servicio<%=i%>.focus();
          return;
       } 
       else
       {
          if(top.estaVacio(document.entry.servicio<%=i%>))
            return;
          else if(!top.validate(window, document.entry.servicio<%=i%>, 'isValServ'))
        	   return;
       }
    <% 
    }
 }%>
}

//-->
</script>
</head>
<body bgcolor="#FFFFFF" onload="top.setFieldFocus(window.document)">
<br>
<% out.print("<form name=\"entry\" action=\"../servlet/ventanilla.DataServlet\" method=\"post\" ");
   if(txnAuthLevel.equals("1"))
    out.print("onSubmit=\"return validate(this)\"");
   out.println(">");
%>

<h1><%=session.getAttribute("page.txnImage")%></h1>

<h3>
<%out.print(session.getAttribute("page.cTxn") + "  " +
                (String)session.getAttribute("page.txnlabel"));%>
</h3>

<%if(TxnE.equals("5503"))
  {
  %>
    <table border="0">
    <tr align="center">
    <td>&nbsp;&nbsp;</td><td>&nbsp;&nbsp;</td>
    <td><b>Servicio</b></td><td>&nbsp;&nbsp;</td>
    <td><b>No. de Pagos</b></td><td>&nbsp;&nbsp;</td>
    <td><b>Monto</b></td></tr>
<%  for (int i = 1; i < nserv+1;i++)
    {%>
        <tr><td><%=i%>.-</td><td>&nbsp;&nbsp;</td>
		<%
        String servicio, strDescripcion="";
        Vector vDescripcion = new Vector();      

		if(vecRAP != null && vecRAP.size()>0){ %>
        <td>
        	<input type="text" size="60" maxlength="200" name="servicioBusq<%=i%>" id="servicioBusq<%=i%>" onBlur="top.keyPressedHandler(window, this, 'isValServ')"/>
        	<input type="hidden" name="servicio<%=i%>" id="servicio<%=i%>"/>
        	<input type="hidden" name="snSrvBusqEnLinea<%=i%>" id="snServicioEnLinea<%=i%>"/>
        </td> 
       <%} else { %>
        <td><input type="text" name="servicio<%=i%>" size="7" maxlength="7"  onKeyPress="top.estaVacio(this)||top.keyPressedHandler(window, this, 'isValServ')"></td>       
	    <% }
		
		%>
        <td>&nbsp;&nbsp;</td>
        <td align="center"><input type="text" name="nopagos<%=i%>" size="2" maxlength="2" onBlur="top.estaVacio(this)||top.validate(window, this, 'isnumeric')" onKeyPress="top.estaVacio(this)||top.keyPressedHandler(window, this, 'isnumeric')" ></td>
        <td>&nbsp;&nbsp;</td>
        <td><input type="text" name="monto<%=i%>" size="14" maxlength="11"  onBlur="top.estaVacio(this)||top.validate(window, this, 'isValMtoRAP')" onKeyPress="wasNotEnter(this)||this.blur()" ></td>
        </tr>
<%  }%>
    <tr>
    <td colspan="4"></td>
    <td align="right"><b>Total :</b></td><td>&nbsp;&nbsp;</td>
    <td><input type="text" name="txtTot" size="16" maxlength="13" value="0.00" onBlur="top.validate(window, this, 'notChange')" onKeyPress="top.keyPressedHandler(window, this, 'notChange')"></td>
    </tr>
    </table>
<%  for (int i = 1; i < nserv+1;i++)
    {%>
	<input type="hidden" name="s<%=i%>" value="X">
<%  }%>
    <input type="hidden" name="nservicios" value="<%=session.getAttribute("page.noservicios")%>">
    <input type="hidden" name="tot" value="0">
    <input type="hidden" name="np" value="0">
<%   if(session.getAttribute("Tarjeta.TDI") != null) //Loch Ness
     {%>
<table>
<tr align="left">
   <td><b>N&uacute;mero de Tarjeta TDI: </td>
   <td><b><font color="red"><%=(String)session.getAttribute("Tarjeta.TDI")%></font></td>
</tr>
</table>
<%   }%>
<p>
<b>Capturar N&uacute;mero de Cuenta de Cheques para Clientes ...</b>
<p>
<table>
<tr><td align="right">Cuenta :</td><td>&nbsp;&nbsp;</td>
<td><input type="text" name="txtDDACuenta" size="10" maxlength="10" onBlur="top.estaVacio(this)||top.validate(window, this, 'isValidAcct')" onKeyPress="top.estaVacio(this)||top.keyPressedHandler(window, this, 'isValidAcct')"></td>
</table>
<%
}
else
{%>
    <table border="0">
    <tr align="center">
    <td><b>Servicio</b></td><td>&nbsp;&nbsp;</td>
    <td><b>No. de Pagos</b></td><td>&nbsp;&nbsp;</td>
    <td><b>Monto</b></td></tr>
     <tr align="center">
     <td><%=(String)datasession.get("txtServicio")%></td><td>&nbsp;&nbsp;</td>
     <td>1</td><td>&nbsp;&nbsp;</td>
     <td><%=(String)datasession.get("txtMonto")%></td>
     </tr>
    </table>
    <input type="hidden" name="lstServicio" value="<%=(String)datasession.get("servicio")%>">
    <input type="hidden" name="servicio1" value="<%=(String)datasession.get("txtServicio")%>">
     <input type="hidden" name="nopagos1" value="1">
     <input type="hidden" name="monto1" value="<%=(String)datasession.get("txtMonto")%>">
     <input type="hidden" name="nservicios" value="1">
     <input type="hidden" name="tot" value="<%=(String)datasession.get("txtMonto")%>">
     <input type="hidden" name="txtTot" value="<%=(String)datasession.get("txtMonto")%>">
     <input type="hidden" name="np" value="1">
<%}%>

<input type="hidden" name="flujorap" value="1">
<input type="hidden" name="flujorapcapr">
<input type="hidden" name="noefec" value="0">
<input type="hidden" name="ser60" value="0">
<input type="hidden" name="ser670" value="0">
<input type="hidden" name="iTxn" value="<%= session.getAttribute("page.iTxn")%>">
<input type="hidden" name="oTxn" value="<%= session.getAttribute("page.oTxn")%>">
<input type="hidden" name="cTxn" value="<%= session.getAttribute("page.cTxn")%>">
<input type="hidden" name="sucursal" value="<%= session.getAttribute("branch")%>">
<input type="hidden" name="teller" value="<%= session.getAttribute("teller")%>">
<input type="hidden" name="moneda" value="<%= session.getAttribute("page.moneda")%>">
<input type="hidden" name="registro" value="<%=session.getAttribute("empno")%>">
<input type="hidden" name="tllrlvl" value="<%=session.getAttribute("tellerlevel")%>">
<input type="hidden" name="supervisor" value="<%= session.getAttribute("teller") %>">
<input type="hidden" name="AuthoOK" value="0">
<input type="hidden" name="SignOK" value="0">
<input type="hidden" name="validateFLD" value="0">
<input type="hidden" name="compania" value="20">
<input type="hidden" name="needSignature" value="<%out.print(txnIMGAutoriz);%>">
<input type="hidden" name="needAutho" value="<%out.print(txnAuthLevel);%>">
<input type="hidden" name="SignField" value="<%= session.getAttribute("page.SignField") %>">
<p>

<% if(txnAuthLevel.equals("1") || txnIMGAutoriz.equals("1"))
    out.println("<a id='btnCont' href=\"javascript:validate(document.entry);javascript:AuthoandSignature(document.entry)\" ><img src=\"../ventanilla/imagenes/b_continuar.gif\" border=\"0\"></a>");
   else
    out.println("<a id='btnCont' href=\"javascript:validate(document.entry)\"><img src=\"../ventanilla/imagenes/b_continuar.gif\" border=\"0\"></a>");
%>
</form>
</body>
</html>
