<!--
//*************************************************************************************************
///            Funcion: JSP que despliega respuesta de txn RAP
//            Elemento: PageBuilder5503.jsp
//          Creado por: Alejandro Gonzalez Castro
//      Modificado por: Fausto Rodrigo Flores Moreno
//*************************************************************************************************
// CCN - 4360267 - 31/01/2005 - Se modifica el acceso a los bines
// CCN - 4360274 - 16/02/2005 - Error de impresi�n cuado el domicilio de la sucursal tiene car�cter de #
// CCN - 4360390 - 07/10/2005 - Se realizan modificaciones para cfe
// CCN - 4360446 - 10/03/2006 - Proyecto TDI fase 2 cobro de comision
// CCN - 4360492 - 23/06/2006 - Se realizan modificaciones para el esquema de Cobro Comisi�n Usuarios RAP
// CCN - 4360581 - 12/03/2007 - Se convierten los estilos en jsf y se hace un include en el jsp
// CCN - 4360585 - 23/03/2007 - Se realizan modificaciones para RAP Calculadora
// CCN -         - 10/01/2008 - Se hacen modificaciones para transacci�n global de cheques locales
// CCN -         - 24/07/2009 - Se agrega validaci�n para alfanum�rico.
//*************************************************************************************************
-->

<%@ page session="true" import="java.util.*"%>
<%
response.setHeader("Cache-Control","no-store"); //HTTP 1.1
response.setHeader("Pragma","no-cache"); //HTTP 1.0
response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<%
String MontoPro = new String("");
String txnAuthLevel = "0";
String txnIMGAutoriz = "0";
Vector v = (Vector)session.getAttribute("page.specialacct");
Vector data = (Vector)session.getAttribute("datos");
Hashtable info = (Hashtable)session.getAttribute("info");
int k;
Hashtable datasession = (Hashtable)session.getAttribute("page.datasession");
String txn = (String)session.getAttribute("page.iTxn");
String ser60 = (String)datasession.get("ser60");
String ser670 = (String)datasession.get("ser670");
Vector bines =  new Vector();
Vector binesAMEX = new Vector();
if (ser60.equals("1"))
{
    bines = (Vector)session.getAttribute("bines");
}
if (ser670.equals("1"))
{
    binesAMEX = (Vector)session.getAttribute("binesAMEX");
}
if(datasession.get("RapCCheque")==null)
	datasession.put("RapCCheque","N");
	
Vector frap = (Vector)session.getAttribute("FRAP");
ventanilla.GenericClasses gc = new ventanilla.GenericClasses();
%>
<html>
<head>
  <%@include file="style.jsf"%>
  <title>Datos de la Transaccion</title>

<script language="JavaScript">
function cancelar(forma)
{
  forma.action = "../paginas/Final.jsp";
	forma.submit();
}
function FormatCurr(val, field)
{
  var num = val.toString().replace(/\$|\,/g,'')

  intVal = num.substring(0, num.indexOf("."))
  decVal = num.substring(num.indexOf(".") + 1, num.length)
  num = intVal + decVal;

  if( isNaN(num) )
  {
    return false
  }

  cents = Math.floor((num)%100)
  num = Math.floor((num)/100).toString()

  if(cents < 10)
    cents = "0" + cents;
  if(cents.length == 1)
    cents = cents + "0";

  for (var i = 0; i < Math.floor((num.length-(1+i))/3); i++)
    num = num.substring(0,num.length-(4*i+3))+','+num.substring(num.length-(4*i+3))

  num = num + '.' + cents
  eval("document.entry."+field+".value = '"+ num + "'");
}

var formName = 'Mio'
var specialacct = new Array()
<%
if(!v.isEmpty())
{
    for(int iv = 0; iv < v.size(); iv++)
        {out.print("specialacct[" + iv + "]='" + v.get(iv) + "'" + '\n');}
}%>

<%if(ser60.equals("1"))
{%>
	var bines = new Array(<%=bines.size()%>);
	var banco = new Array(<%=bines.size()%>);
<%
	if(!bines.isEmpty())
	{
		Vector row = new Vector();
		for(int x=0; x<bines.size(); x++)
		{
             		row = (Vector)bines.get(x);
			out.print("bines["+x+"]='"+row.get(0)+"'"+'\n');
			out.print("banco["+x+"]='"+row.get(1)+"'"+'\n');
		}
	}
}
%>

<%if(ser670.equals("1"))
{%>
	var binesAMEX = new Array(<%=binesAMEX.size()%>);
	var bancoAMEX = new Array(<%=binesAMEX.size()%>);
<%
	if(!binesAMEX.isEmpty())
	{
		Vector row = new Vector();
		for(int x=0; x<binesAMEX.size(); x++)
		{
             		row = (Vector)binesAMEX.get(x);
			out.print("binesAMEX["+x+"]='"+row.get(0)+"'"+'\n');
			out.print("bancoAMEX["+x+"]='"+row.get(1)+"'"+'\n');
		}
	}
}
%>


function validate(form)
{
    form.validateFLD.value = '1'
<%
    // OBTENCION DE TIPO DE SERVICIO
    String tipo = "";
    tipo = info.get("PCDCode").toString();

    // NUMERO DE SERVICIOS CAPTURADOS
    String nos = (String)datasession.get("nservicios");
    int ns = Integer.parseInt(nos);

    int np = 0;
    int lr = 0;
    int mtoiva = 0;
    int iva = 0;
    int mtocom = 0;
    int mtocom1 = 0;
    int mtocom2 = 0;
    String npagos = "";
    String lref = "";
    String com = "";
    String com1 = "";
    String com2 = "";
    String mto = "";
    String ivatmp = "";
    String servicio = "";
    String comppago = "";
    String cobrocom = "";
    String CobCom = "";
    String Cliente ="";
	String fRapCal = "";

    for (int i = 1; i < ns+1;i++)
    {
        servicio = info.get("Servicio"+i).toString();
        // NUMERO DE PAGOS POR SERVICIO
        npagos = info.get("NoPagos"+i).toString();
        np = Integer.parseInt(npagos);
        for (int j = 1; j < np+1;j++)
        {%>
            if( !top.validate(window, document.entry.montos<%=i%>p<%=j%>, 'MtoServ') )
            return
            if(document.entry.compcoms<%=i%>p<%=j%> != null)
            {
                if (document.entry.compcoms<%=i%>p<%=j%>.checked == true)
                {
                    if( !top.validate(window, document.entry.nombres<%=i%>p<%=j%>, 'isEmptyStr') )
                    return
                    if( !top.validate(window, document.entry.doms<%=i%>p<%=j%>, 'isEmptyStr') )
                    return
                    if( !top.validate(window, document.entry.rfcs<%=i%>p<%=j%>, 'isEmptyStr') )
                    return
                }
                else //Agregado para validar mensaje en Cob Comision
                {
                	if( !top.validate(window, document.entry.nombres<%=i%>p<%=j%>, 'isMsgComprobante') )
                    return
                    if( !top.validate(window, document.entry.doms<%=i%>p<%=j%>, 'isMsgComprobante') )
                    return
                    if( !top.validate(window, document.entry.rfcs<%=i%>p<%=j%>, 'isMsgComprobante') )
                    return
                }//--FIN
                
            }
<%          for (int l = 1; l < 4;l++)
            {
                // LONGITUDES DE REFERENCIA
                if(tipo.equals("1"))
                    lref = "40";
                if(tipo.equals("2"))
                    lref = info.get("Long"+l+"s"+i).toString();
                lr = Integer.parseInt(lref);
                if(lr > 0)
                {
                    // INCLUSION DE CAMPOS PARA RFC SERVICIOS 480 / 448 / 297 / 66
                    if(servicio.equals("480") || servicio.equals("448") || servicio.equals("297") || servicio.equals("66"))
				{%>
                        if( !top.validate(window, document.entry.ref<%=l%>s<%=i%>p<%=j%>, 'isValRFC') )
                        return
<%                      break;
				}
				else if(servicio.equals("1880"))
				{%>
                        if( !top.validate(window, document.entry.ref<%=l%>s<%=i%>p<%=j%>, 'Ref1880') )
                        return
<%                      break;
				}
				else if(servicio.equals("669") || servicio.equals("670"))
				{%>
                        if( !top.validate(window, document.entry.ref<%=l%>s<%=i%>p<%=j%>, 'TDCAmex') )
                        return
<%                      break;
				}
				else if(servicio.equals("60"))
				{%>
                        if( !top.validate(window, document.entry.ref<%=l%>s<%=i%>p<%=j%>, 'ValTDC') )
                        return
<%                      break;
				}
				else
				{
                        if(!txn.equals("RAPM"))
                        {
                            if(lr == 40)
                            {%>
                                if( !top.validate(window, document.entry.ref<%=l%>s<%=i%>p<%=j%>, 'ValRef40') )
                                return
<%                          }
                            else
                            {
                                String FlagR = info.get("FlagRef"+i).toString();
                                if(FlagR.equals("F"))
                                {%>
                                if( !top.validate(window, document.entry.ref<%=l%>s<%=i%>p<%=j%>, 'ValRefFija') )
                                return
<%                              }
                                else
                                {%>
                                if( !top.validate(window, document.entry.ref<%=l%>s<%=i%>p<%=j%>, 'isEmptyStr') )
                                return
<%                              }
                            }
                        }
                    }
                    
               if(servicio.equals("9999"))
				{
					if(datasession.get("RAPCalculadora")!=null){
						if(!datasession.get("RAPCalculadora").toString().equals("S"))
					    {
				%>		  if( !top.validate(window, document.entry.ref<%=l%>s<%=i%>p<%=j%>, 'ValCFE') )
					      	return
     <%     	          break;
                		}    
					}else{%>
						if( !top.validate(window, document.entry.ref<%=l%>s<%=i%>p<%=j%>, 'ValCFE') )
					      return
     <%     	        break;
           			}    
				}
			}
		}
	}
}%>

<%  if(!txnAuthLevel.equals("1") && !txnIMGAutoriz.equals("1"))
	{
    out.println("  form.submit();");
    out.println("var obj = document.getElementById('btnCont');"); 
    out.println("obj.onclick = new Function('return false');");   
	}   
%>
}

function AuthoandSignature(entry)
{
 var Cuenta;
 if(entry.validateFLD.value != '1')
  return
 if(entry.needAutho.value == '1' && entry.AuthoOK.value != '1'){
  top.openDialog('../paginas/authoriz.jsp', 220, 130, 'top.setPrefs()', entry)
 }
 if(entry.needSignature.value == '1' && entry.SignField.value != ''){
  Cuenta = eval('entry.' + entry.SignField.value + '.value')
  top.openModalDialog('<%=request.getContextPath()%>/servlet/ventanilla.FirmasServlet?txtDDACuenta=' + Cuenta, 710, 430, 'top.setPrefs2()', entry)
 }
}
</script>
</head>
<body bgcolor="#FFFFFF" onload="top.setFieldFocus(window.document)">
<br>
<% out.print("<form name=\"entry\" action=\"../../servlet/ventanilla.DataServlet\" method=\"post\" ");
   if(txnAuthLevel.equals("1"))
    out.print("onSubmit=\"return validate(this)\"");
   out.println(">");
%>

<h1><%=session.getAttribute("page.txnImage")%></h1>

<h3>
5503 Cobranza
</h3>
<input type="hidden" name="nos" value="<%=nos%>">
<table>
<tr><td><b>Monto Total :</b></td><td>&nbsp;&nbsp;</td>
<%if(datasession.get("Desc_Recargo")!=null){
	if((datasession.get("Desc_Recargo").toString().equals("D") || datasession.get("Desc_Recargo").toString().equals("R")) && datasession.get("no_descuento").toString().equals("SI")){%>
		<td><b><%=gc.FormatAmount(datasession.get("MontoPagar").toString())%></b></td></tr><%
	}else if(datasession.get("Desc_Recargo").toString().equals("D") && datasession.get("no_descuento").toString().equals("NO")){%>
		<td><b><%=info.get("txtTot").toString()%></b></td></tr><%
	}else if(datasession.get("Desc_Recargo").toString().equals("N")){%>
		<td><b><%=info.get("txtTot").toString()%></b></td></tr><%
	}

  }else{%>
	<td><b><%=info.get("txtTot").toString()%></b></td></tr>
<%}%>
</table>
<br>
<%
// CICLO POR SERVICIO
for (int x = 1; x < ns+1; x++)
{
    servicio = info.get("Servicio"+x).toString();
    // NUMERO DE PAGOS POR SERVICIO
    
	// OBTENCION DE TIPO DE RAP, CALCULADORA - CAL_COB_COMISION - NORMAL
	if(datasession.get("RAPCalculadora")!=null)
		fRapCal = (String)datasession.get("RAPCalculadora");
		
    npagos = info.get("NoPagos"+x).toString();
    np = Integer.parseInt(npagos);

	//REINICIAR VARIABLES DE MONTO "cobro comision usuariso"
    mtocom1 = 0;
    mtocom2 = 0;
    // SE ASIGNAN A 0 PARA EVITAR ERROR CUANDO SE PASA SERV1 CON COMISION Y SERV2 SIN COMISION
    // EL ERROR SE QUEDABA GUARADA LA COMISI�N DEL PRIMER SERVICIO
    
    
    //ENCABEZADO POR SERVICIO%>
    <table border="0">
    <tr><td><b>Servicio :</b></td><td>&nbsp;&nbsp;</td>
	    <td align="center"><b><%=datasession.get("servicioBusq"+x)%></td><td>&nbsp;&nbsp;</td>
	    <td>Cuenta :</td><td>&nbsp;</td><td><b><%=datasession.get("DDAARP")%></b></td>
    </tr>
    <input type="hidden" name="s<%=x%>" value="<%=info.get("Servicio"+x).toString()%>">
    <tr><td><b>Monto :</b></td><td>&nbsp;&nbsp;</td>
<%  if(fRapCal.equals("S")){
		if((datasession.get("Desc_Recargo").toString().equals("D") || datasession.get("Desc_Recargo").toString().equals("R")) && datasession.get("no_descuento").toString().equals("SI")){%>
			<td><b><%=gc.FormatAmount(datasession.get("MontoPagar").toString())%></b></td><td>&nbsp;&nbsp;</td><%
		}else if(datasession.get("Desc_Recargo").toString().equals("D") && datasession.get("no_descuento").toString().equals("NO")){%>
			<td><b><%=info.get("Monto"+x).toString()%></b></td><td>&nbsp;&nbsp;</td><%
		}else if(datasession.get("Desc_Recargo").toString().equals("N")){%>
			<td><b><%=info.get("Monto"+x).toString()%></b></td><td>&nbsp;&nbsp;</td><%
		}

	}else{%>
		<td><b><%=info.get("Monto"+x).toString()%></b></td><td>&nbsp;&nbsp;</td>
<%	}%>
    <td><b>Monto Disponible :</b></td><td>&nbsp;&nbsp;</td>
<%  if(np == 1)
    {%>
        <td><input tabstop="false" contentEditable = "false" type="text" name="montod<%=x%>" size="14" maxlength="11" value="0.00"></td></tr>
<%  }
    else
    {%>
        <td><input type="text" name="montod<%=x%>" size="14" maxlength="11" value="<%=info.get("Monto"+x).toString()%>"></td></tr>
<%  }%>
    </table>
    <!-- ENCABEZADO PAGOS POR SERVICIO //-->
    <table border="0">
    <tr><td colspan="8" align="center"><b>PAGOS</b></td></tr>
    <tr>
    <td align="center"><b>No.</b></td><td>&nbsp;&nbsp;</td>
    <td align="center"><b>Monto</b></td><td>&nbsp;&nbsp;</td>
<%  // INCLUSION DE TITULO REFERENCIAS TIPO 1
    if(tipo.equals("1"))
    {
        // INCLUSION DE TITULO PARA RFC SERVICIOS 480 / 448 / 297 / 66
        if(servicio.equals("480") || servicio.equals("448") || servicio.equals("297") || servicio.equals("66"))
        {%>
            <td align="center"><b>R. F. C.</b></td>
<%      }
        // INCLUSION DE TITULO SERVICIO 1880
        else if(servicio.equals("1880"))
        {%>
            <td>&nbsp;&nbsp;</td><td>&nbsp;&nbsp;</td>
            <td align="center"><b>Referencia</b></td>
<%      }
        // INCLUSION DE TITULO SERVICIOS 669 / 670
        else if(servicio.equals("669") || servicio.equals("670"))
        {%>
            <td>&nbsp;&nbsp;</td><td>&nbsp;&nbsp;</td>
            <td align="center"><b>No. de TDC</b></td>
<%      }
        // TITULOS DEMAS SERVICIOS
        else
        {%>
            <td>&nbsp;&nbsp;</td><td>&nbsp;&nbsp;</td>
            <td align="center"><b>Referencias</b></td>
<%      }
    }

    // OBTENCION DE DATOS DE COMISION E IVA PARA SERVICIOS TIPO 2
    if(tipo.equals("2"))
    {
        // COMISION POR SERVICIO
        com1 = info.get("Com"+x).toString();
        mtocom1 = Integer.parseInt(com1);%>
        <input type="hidden" name="comgs<%=x%>" value="<%=mtocom1%>">
<%        Cliente = (String)datasession.get("Cliente");
//Tarjeta TDI
        String TarjetaTDI = null;
        if (session.getAttribute("Tarjeta.TDI") != null)
           TarjetaTDI = (String)session.getAttribute("Tarjeta.TDI");
//Tarjeta TDI
        // COMISION EXTRA POR SERVICIO
        CobCom = info.get("CobCom"+x).toString();

        if(CobCom.equals("S"))
        {
            if(Cliente.equals("S") || TarjetaTDI != null) //Tarjeta TDI
                com2 = info.get("ComCli"+x).toString();
            else
                com2 = info.get("ComNoCli"+x).toString();
            mtocom2 = Integer.parseInt(com2);
        }

        // SUMA DE COMISIONES
        mtocom = mtocom1 + mtocom2;

        // CONTROL PARA COBRO DE COMISION
        if (mtocom > 0)
            cobrocom = "S";
        else
            cobrocom = "N";
        
        // CALCULO DE IVA
        mtoiva = mtocom;
        ivatmp = info.get("Iva"+x).toString();
        iva = Integer.parseInt(ivatmp);%>
        <input type="hidden" name="ivas<%=x%>" value="<%=iva%>">
<%        mtoiva = mtocom * iva / 100; 
    }

	// INCLUSION DE TITULOS DE COMISION, IVA, ACEPTA, COBRO COM Y COMPROBANTE COBRO COM
	// PARA SERVICIOS TIPO 2
    if(tipo.equals("2"))
    {
        comppago = info.get("CompPago"+x).toString();%>
        <input type="hidden" name="comppago<%=x%>" value="<%=comppago%>">
<%
        //compcobrocom = info.get("CompCom"+x).toString();
//        if(servicio.equals("60"))
  //          {compcobrocom = "S";}
        // TITULOS COMISION E IVA / COBRO COMISION
        if(cobrocom.equals("S"))
        {%>
            <td align="center"><b>Comisi&oacute;n</b></td><td>&nbsp;&nbsp;</td>
            <td align="center"><b>I.V.A.</b></td>
            <td align="center"><b>Cobro<br>Comisi&oacute;n</b></td><%
          //  if(compcobrocom.equals("S"))
                //{%><td align="center"><b>Comprobante<br>Comisi&oacute;n</b></td><%//}%>
            <td align="center"><b>Acepta<br>Pago</b></td>
<%      }
        else
            {%><td colspan="4">&nbsp;&nbsp;</td><%}%>
        </tr>
<%  }%>

    <input type="hidden" name="mtos<%=x%>" value="<%=info.get("Monto"+x).toString()%>">
    <input type="hidden" name="nps<%=x%>" value="<%=np%>">
<%  // CICLO PAGOS POR SERVICIO 
    for (int y = 1; y < np+1;y++)
    {
        if(tipo.equals("1"))
        {
            for (int  z= 1; z < 4;z++)
            {
                if(z == 1)
                {%>
                    <tr>
				<td align="right"><%=y%></td><td>&nbsp;&nbsp;</td>
<%                  // MONTO FIJO CUANDO EXISTE SOLO UN PAGO POR SERVICIO
                  if(np == 1)
                  {
   						if(fRapCal.equals("S"))
        	            {
                            if((datasession.get("Desc_Recargo").toString().equals("D") || datasession.get("Desc_Recargo").toString().equals("R")) && datasession.get("no_descuento").toString().equals("SI")){%>
                            	<td><input tabstop="false" contentEditable = "false" type="text" name="montopagar" value="<%=gc.FormatAmount(datasession.get("MontoPagar").toString())%>" size="14" maxlength="11"></td><%
                            }else if(datasession.get("Desc_Recargo").toString().equals("D") && datasession.get("no_descuento").toString().equals("NO")){%>
								<td><input tabstop="false" contentEditable = "false" type="text" name="montopagar" value="<%=info.get("Monto"+x).toString()%>" size="14" maxlength="11"></td>
 						  <%}else if(datasession.get("Desc_Recargo").toString().equals("N")){%>
								<td><input tabstop="false" contentEditable = "false" type="text" name="montopagar" value="<%=info.get("Monto"+x).toString()%>" size="14" maxlength="11"></td>
 						  <%}%>                           
                            <input type="hidden" name="montos<%=x%>p<%=y%>" value="<%=info.get("Monto"+x).toString()%>" size="14" maxlength="11">
<%					  	}
					    else
				    	{%>
                            <td><input tabstop="false" contentEditable = "false" type="text" name="montos<%=x%>p<%=y%>" value="<%=info.get("Monto"+x).toString()%>" size="14" maxlength="11"></td>
<%		                 }
		 		  }else{%>                       
                        <td><input type="text" name="montos<%=x%>p<%=y%>" size="14" maxlength="11" onBlur="top.estaVacio(this)||top.validate(window, this, 'MtoServ')" onKeyPress="top.keyPressedHandler(window, this, 'MtoServ')"></td>
<%				  }
%>
				<td>&nbsp;&nbsp;</td>
<%				// INCLUSION DE CAMPOS PARA RFC SERVICIOS 480 / 448 / 297 / 66
				if(servicio.equals("480") || servicio.equals("448") || servicio.equals("297") || servicio.equals("66"))
				{%>
        				<td><input type="text" name="ref<%=z%>s<%=x%>p<%=y%>" size="13" maxlength="13" onChange="top.validate(window, this, 'isValRFC')"></td>
<%                  }
				else if(servicio.equals("1880"))
				{%>
                        <td align="right">&nbsp;&nbsp;</td><td>&nbsp;&nbsp;</td>
                        <td><input type="text" name="ref<%=z%>s<%=x%>p<%=y%>" size="7" maxlength="7" onChange="top.validate(window, this, 'Ref1880')"></td><%
				}
				else if(servicio.equals("669") || servicio.equals("670"))
				{%>
                        <td align="right">&nbsp;&nbsp;</td><td>&nbsp;&nbsp;</td>
                        <td><input type="text" name="ref<%=z%>s<%=x%>p<%=y%>" size="15" maxlength="15" onChange="top.validate(window, this, 'TDCAmex')"></td><%
				}
				else
				{
					if(fRapCal.equals("S"))
                    {%>
                            <td colspan="4"><%=(String)datasession.get("txtReferRapCal")%></td>
                            <input type="hidden" name="ref<%=z%>s<%=x%>p<%=y%>" value="<%=(String)datasession.get("txtReferRapCal")%>" size="<%=lr%>" maxlength="<%=lr%>"><%
				  	}
				    else
				    {%>
                        <td align="right"><%=z%> .-</td><td>&nbsp;&nbsp;</td>
                        <td><input type="text" name="ref<%=z%>s<%=x%>p<%=y%>" size="40" maxlength="<%=lr%>" onChange="top.validate(window, this, 'ValRef40')"></td><%
                    }
				}%>
				</tr>
 <%              }
                else
                {
                    // COMISION DE REFERENCIAS PARA SERVICIOS 480 / 448 / 297 / 66 / 1880
				if(!(servicio.equals("480") || servicio.equals("448") || servicio.equals("297") || servicio.equals("66") || servicio.equals("1880") || servicio.equals("669")|| servicio.equals("670")))
				{%>
                        <tr>
                        <td colspan="4">&nbsp;&nbsp;</td>
                        <td align="right"><%=z%> .-</td><td>&nbsp;&nbsp;</td>
                        <td><input type="text" name="ref<%=z%>s<%=x%>p<%=y%>" size="40" maxlength="<%=lr%>" onChange="top.validate(window, this, 'ValRef40')"></td>
                        </tr>
<%          		}
                }
            }
        } // TERMINA IF PARA SERVICIOS TIPO 1

        if(tipo.equals("2"))
        {%>
            <tr>
            <td align="right"><%=y%></td><td>&nbsp;&nbsp;</td>
<%          if(txn.equals("RAPM"))
            {%>
                <input type="hidden" name="montos<%=x%>p<%=y%>" value="<%=info.get("Monto"+x).toString()%>" size="14" maxlength="11">
                <td align="center"><%=info.get("Monto"+x).toString()%></td>
<%          }
            else
            {%>
<%                  if(np == 1)
                    {
                      	if(fRapCal.equals("S"))
        	            {
        	            	if((datasession.get("Desc_Recargo").toString().equals("D") || datasession.get("Desc_Recargo").toString().equals("R")) && datasession.get("no_descuento").toString().equals("SI")){%>
                            	<td align="center"><%=gc.FormatAmount(datasession.get("MontoPagar").toString())%></td><%
                            }else if(datasession.get("Desc_Recargo").toString().equals("D") && datasession.get("no_descuento").toString().equals("NO")){%>
								<td align="center"><%=info.get("Monto"+x).toString()%></td>
						  <%}else if(datasession.get("Desc_Recargo").toString().equals("N")){%>
								<td align="center"><%=info.get("Monto"+x).toString()%></td>
						  <%}%>
                            <input type="hidden" name="montos<%=x%>p<%=y%>" value="<%=info.get("Monto"+x).toString()%>" size="14" maxlength="11">
<%					  	}else{%>
                        <td><input tabstop="false" contentEditable = "false" type="text" name="montos<%=x%>p<%=y%>" value="<%=info.get("Monto"+x).toString()%>" size="14" maxlength="11"></td>
<%                  	}
					}
                    else{%>
 	                       <td><input type="text" name="montos<%=x%>p<%=y%>" size="14" maxlength="11" onBlur="top.estaVacio(this)||top.validate(window, this, 'MtoServ')" onKeyPress="top.keyPressedHandler(window, this, 'MtoServ')" ></td>
<% 	                }

	        }
%>
            <td>&nbsp;&nbsp;</td>
<%          if(cobrocom.equals("S"))
            {%>
                <input type="hidden" name="comts<%=x%>p<%=y%>" value=<%=mtocom%>>
                <td><input tabstop="false" contentEditable = "false" type="text" name="coms<%=x%>p<%=y%>" size="12" maxlength="9"></td>
                <script>FormatCurr('<%=mtocom%>', 'coms<%=x%>p<%=y%>')</script>
                <td>&nbsp;&nbsp;</td>
                <td><input tabstop="false" contentEditable = "false" type="text" name="ivas<%=x%>p<%=y%>" size="12" maxlength="9"></td>
                <script>FormatCurr('<%=mtoiva%>', 'ivas<%=x%>p<%=y%>')</script>
<%              if(servicio.equals("60"))
                {%>
                    <td align="center"><input type="checkbox" name="cobrocoms<%=x%>p<%=y%>" value="1"></td>
<%              }
                else
                {%>
                    <td align="center">SI<input type="hidden" name="cobrocoms<%=x%>p<%=y%>" value="1"></td>
<%              }
//                if(compcobrocom.equals("S"))
 //               {%>
                    <td align="center"><input type="checkbox" name="compcoms<%=x%>p<%=y%>" value="1"></td>
                <%//}%>

<!-- FLAG PARA TXN DE COMISI�N A USUARIOS-->
                <input type="hidden" name="ComServRAP<%=x%>" value="<%=info.get("ComServRAP"+x)%>"></td>
<!--           FIN           -->

			 <td align="center"><input type="checkbox" name="aceptas<%=x%>p<%=y%>" value="1" checked></td>
<%          }
            else
            {%>
                <td colspan="4">&nbsp;&nbsp;</td>
                <input type="hidden" name="aceptas<%=x%>p<%=y%>" value="1">
<%          }%>
            </tr>
<%          int intRef =0;
			for (int w = 1; w < 4; w++)
            {
               lref = info.get("Long"+w+"s"+x).toString();
               lr = Integer.parseInt(lref);
               if(lr > 0)
			{%>
                    <tr>
				<td></td><td>&nbsp;&nbsp;</td>
<%                  if(servicio.equals("60"))
				{%>
                        <td align="right">N&uacuteMERO DE TARJETA  :</td><td>&nbsp;&nbsp;</td>
                        <%if (!info.get("Servicio1").toString().equals("9999"))
                          {%>
                        <td colspan="4"><input type="text" name="ref<%=w%>s<%=x%>p<%=y%>" size="<%=lr%>" maxlength="<%=lr%>" onBlur="top.estaVacio(this)||top.validate(window, this, 'ValTDC')" onKeyPress="top.estaVacio(this)||top.keyPressedHandler(window, this, 'ValTDC')"></td>
                        <%}else{%>
                        <td colspan="4"><input type="text" name="ref<%=w%>s<%=x%>p<%=y%>" size="<%=lr%>" maxlength="<%=lr%>" onBlur="top.estaVacio(this)||top.validate(window, this, 'ValMtoCFE')" onKeyPress="top.estaVacio(this)||top.keyPressedHandler(window, this, 'ValMtoCFE')"></td>
				<%  }
                }
				else
				{
				Vector vecRefARP = (Vector)info.get("vecReferencia");
				if(vecRefARP.size()>0){
					%>
                        <td align="right"><%=(String)vecRefARP.elementAt(intRef)%> :</td><td>&nbsp;&nbsp;</td>
				<%intRef++;}
				  else{ %>
				  <td align="right">Referencia <%=w%> :</td><td>&nbsp;&nbsp;</td>
				  <%}
                      if(txn.equals("RAPM"))
                        {%>
                            <td colspan="4"><%=(String)datasession.get("txtReferencia")%></td>
                            <input type="hidden" name="ref<%=w%>s<%=x%>p<%=y%>" value="<%=(String)datasession.get("txtReferencia")%>" size="<%=lr%>" maxlength="<%=lr%>">
<%                      }
                        else
                        {
                     		if(fRapCal.equals("S"))
		                    {%>
        	                    <td colspan="4"><%=(String)datasession.get("txtReferRapCal")%></td>
            	                <input type="hidden" name="ref<%=w%>s<%=x%>p<%=y%>" value="<%=(String)datasession.get("txtReferRapCal")%>" size="<%=lr%>" maxlength="<%=lr%>"><%
						  	}
						    else
						    {
                            	String Flag = info.get("FlagRef"+x).toString();
	                            if(Flag.equals("F"))
    	                        {%>
        	                        <td colspan="4"><input type="text" name="ref<%=w%>s<%=x%>p<%=y%>" size="<%=lr%>" maxlength="<%=lr%>" onChange="top.validate(window, this, 'ValRefFija')&&top.validate(window, this, 'isAlphaNumeric')"></td>
            	                <%}
                	            else
                    	        {%>
                        	        <td colspan="4"><input type="text" name="ref<%=w%>s<%=x%>p<%=y%>" size="<%=lr%>" maxlength="<%=lr%>" onBlur="(top.estaVacio(this)||top.validate(window, this, 'isEmptyStr'))&&top.validate(window, this, 'isAlphaNumeric')" onKeyPress="top.keyPressedHandler(window, this, 'isEmptyStr')&&top.validate(window, this, 'isAlphaNumeric')" ></td><%                          	}
							}
                        }
                    }%>
				</tr>
<%              }
            }
			
			//CAMPOS DE DESGLOSE PARA RAP CALCULADORA
            if(fRapCal.equals("S")){
            	if(datasession.get("no_descuento").toString().equals("SI")){
	            	if(Integer.parseInt(datasession.get("Monto_D_R").toString())!=0){%>
			            <tr><td></td><td>&nbsp;&nbsp;</td><td>&nbsp;&nbsp;</td><td>&nbsp;&nbsp;</td>
			            <td align="center" colspan="6"><b>Desglose por Recargo/Descuento</b></td></tr>
				  	    <tr><td></td><td>&nbsp;&nbsp;</td><td align="right">Importe :</td><td>&nbsp;&nbsp;</td><td colspan="4"><%=gc.FormatAmount(info.get("MontoOrg").toString())%></td></tr>
	  	    		 	<%if(!datasession.get("Monto_D_R").toString().equals("0")){
	  	    	 			out.println("<input type=\"hidden\" name=\"MtoDes_Rec\" value=\""+datasession.get("Monto_D_R").toString()+"\">");
  	    					if(datasession.get("Desc_Recargo").toString().equals("D")){%>
			    				<td></td><td>&nbsp;&nbsp;</td><td align="right">Descuento :</td>
						<%	}else if(datasession.get("Desc_Recargo").toString().equals("R")){%>
 					     		<td></td><td>&nbsp;&nbsp;</td><td align="right">Recargo :</td>
 				 		<%	}
 		 				  }%>
 		 				 <td>&nbsp;&nbsp;</td><td colspan="4"><%=gc.FormatAmount(datasession.get("Monto_D_R").toString())%></td></tr> 
	 		 			 <input type="hidden" name = "Monto_D_R" value="<%=gc.FormatAmount(datasession.get("Monto_D_R").toString())%>">
 			 			<tr><td></td><td>&nbsp;&nbsp;</td><td align="right">Monto a Pagar :</td><td>&nbsp;&nbsp;</td><td colspan="4"><%=gc.FormatAmount(datasession.get("MontoPagar").toString())%></td></tr>
	 			 		<%
	 			 	}
	 		 	}
			}
            
            
            // CAMPOS PARA COMPROBANTE DE COBRO DE COMISION E IVA
//            if(cobrocom.equals("S") && compcobrocom.equals("S") )
            if(cobrocom.equals("S"))
            {%>
                <tr><td></td><td>&nbsp;&nbsp;</td><td>&nbsp;&nbsp;</td><td>&nbsp;&nbsp;</td>
                <td align="center" colspan="6"><b>Datos para el Comprobante del Cobro de Comisi&oacute;n e I.V.A.</b></td></tr>
                <tr><td></td><td>&nbsp;&nbsp;</td><td align="right">Nombre :</td><td>&nbsp;&nbsp;</td>
                <td colspan="6"><input type="text" name="nombres<%=x%>p<%=y%>" size="50" maxlength="50" onChange="top.validate(window, this, 'isMsgComprobante')"></td></tr>
                <tr><td></td><td>&nbsp;&nbsp;</td><td align="right">Domicilio :</td><td>&nbsp;&nbsp;</td>
                <td colspan="6"><input type="text" name="doms<%=x%>p<%=y%>" size="50" maxlength="50" onChange="top.validate(window, this, 'isMsgComprobante')"></td></tr>
                <tr><td></td><td>&nbsp;&nbsp;</td><td align="right">R.F.C. :</td><td>&nbsp;&nbsp;</td>
                <td colspan="6"><input type="text" name="rfcs<%=x%>p<%=y%>" size="13" maxlength="13" onChange="top.validate(window, this, 'isMsgComprobante')"></td></tr>
<%            }
        } // TERMINA IF PARA SERVICIOS TIPO 2
    }%>
	</table>
<%}%>

<input type="hidden" name="tiposerv" value="<%=tipo%>">
<input type="hidden" name="CurrServ" value="0">
<input type="hidden" name="CurrPago" value="0">
<input type="hidden" name="ConRAP" value="0">
<input type="hidden" name="mtoRAP" value="0">
<%if(frap.contains("0604")){%>
<input type="hidden" name="rdoACorte" value="03">
<%}%>
<input type="hidden" name="iTxn" value="<%= session.getAttribute("page.iTxn") %>">
<input type="hidden" name="oTxn" value="<%= session.getAttribute("page.oTxn") %>">
<input type="hidden" name="cTxn" value="<%= session.getAttribute("page.oTxn") %>">
<input type="hidden" name="sucursal" value="<%= session.getAttribute("branch") %>">
<input type="hidden" name="teller" value="<%= session.getAttribute("teller") %>">
<input type="hidden" name="moneda" value="<%= session.getAttribute("page.moneda")%>">
<input type="hidden" name="registro" value="<%=session.getAttribute("empno")%>">
<input type="hidden" name="tllrlvl" value="<%=session.getAttribute("tellerlevel")%>">
<input type="hidden" name="supervisor" value="<%= session.getAttribute("teller") %>">
<input type="hidden" name="AuthoOK" value="0">
<input type="hidden" name="SignOK" value="0">
<input type="hidden" name="validateFLD" value="0">
<input type="hidden" name="needSignature" value="0">
<input type="hidden" name="MtoTotal" value="<%=info.get("txtTot").toString()%>">
<input type="hidden" name="compania" value="20">
<input type="hidden" name="needAutho" value="<%out.print(txnAuthLevel);%>">
<input type="hidden" name="SignField" value="<%= session.getAttribute("page.SignField") %>">
<p>
<%
if (ser60.equals("1"))
{
	if(session.getAttribute("bines") != null)
	{
    	bines = new Vector();
    	session.setAttribute("bines", bines);
  	}
}%>

<%
if (ser670.equals("1"))
{
	if(session.getAttribute("binesAMEX") != null)
	{
    	binesAMEX = new Vector();
    	session.setAttribute("binesAMEX", binesAMEX);
  	}
}%>

<%if(servicio.equals("9999"))
    {%>
     <table border=0>
     <tr>
<% if(txnAuthLevel.equals("1") || txnIMGAutoriz.equals("1"))
          out.println("<td><a id='btnCont' href=\"javascript:validate(document.entry);javascript:AuthoandSignature(document.entry)\" ><img src=\"../imagenes/b_continuar.gif\" border=\"0\"></a></td>");
        else
          out.println("<td><a id='btnCont' href=\"javascript:validate(document.entry)\"><img src=\"../imagenes/b_continuar.gif\" border=\"0\"></a></td>");
      %>
      <td></td><td></td><td></td><td></td>
	  <td></td><td></td><td></td><td></td>
	  <td><a href="JavaScript:cancelar(document.entry) "><img src="../imagenes/b_cancelar.gif" border="0"></a></td>
<% }else
    if(txnAuthLevel.equals("1") || txnIMGAutoriz.equals("1"))
    out.println("<a id='btnCont' href=\"javascript:validate(document.entry);javascript:AuthoandSignature(document.entry)\" ><img src=\"../imagenes/b_continuar.gif\" border=\"0\"></a>");
   else
    out.println("<a id='btnCont' href=\"javascript:validate(document.entry)\"><img src=\"../imagenes/b_continuar.gif\" border=\"0\"></a>");
%>

</tr>
</table>	
</form>
</body>
</html>

