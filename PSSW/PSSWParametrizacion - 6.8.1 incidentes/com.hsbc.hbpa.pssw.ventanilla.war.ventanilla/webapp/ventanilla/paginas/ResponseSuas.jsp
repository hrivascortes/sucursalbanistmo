<!--
//*************************************************************************************************
///            Funcion: JSP que despliega respuesta de txn 0068
//            Elemento: ResponseSuas.jsp
//          Creado por: Fausto Rodrigo Flores Moreno
//*************************************************************************************************
// CCN - 4360602 - 18/05/2007 - Se realizan modificaciones para proyecto Codigo de Barras SUAS
//*************************************************************************************************
-->

<%@ page session="true" import="java.util.*, ventanilla.GenericClasses"%>
<%
response.setHeader("Cache-Control","no-store"); //HTTP 1.1
response.setHeader("Pragma","no-cache"); //HTTP 1.0
response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<%!
  private String getString(String s)
  {
   int len = s.length();
   for(; len>0; len--)
    if(s.charAt(len-1) != ' ')
     break;
   return s.substring(0, len);
  }
%>
<%
Vector resp = (Vector)session.getAttribute("response");
Hashtable datasession = (Hashtable)session.getAttribute("page.datasession");
%>
<html>
<head>
  <title>Certificación</title>
<style type="text/css">Body {font-family: Arial, Helv; font-size: 10pt; font-weight: bold; color:#000000}</style>
<script language="javascript">
<!--
function submitir()
{
    document.entry.submit();
}
//-->
</script>
</head>
<body bgcolor="#FFFFFF">
<p>
<%
  java.util.Stack flujotxn = new java.util.Stack();
  String codigo = "";
  String needOverride = "NO";
  String txn = session.getAttribute("page.cTxn").toString();
  String status = "";
  if(session.getAttribute("page.flujotxn") != null)
  	flujotxn = (java.util.Stack)session.getAttribute("page.flujotxn");
  
  if(datasession.get("statusSUAS")!=null)
	status = (String)datasession.get("statusSUAS");
  
  if( resp.size() != 4 )
   out.println("<b>Error de conexi&oacute;n</b><br>");
  else
  {
   	try
   	{
     	codigo = (String)resp.elementAt(0);
 	if( codigo.equals("1"))
   	   out.println("<b>Transaccion Rechazada</b><br><br>");
        if( codigo.equals("0") || codigo.equals("1") ) {
     	   int lines = Integer.parseInt((String)resp.elementAt(1));
    	   String szResp = (String)resp.elementAt(3);
    	   for(int i=0; i<lines; i++) {
    		   String line = szResp.substring( i*78, Math.min(i*78+77, szResp.length()) );
     		   out.println(getString(line) + "<p>");
     		   if(Math.min(i*78+77, szResp.length()) == szResp.length())
      		    break;
    	   }
       }
       else {
         if ( codigo.equals("3") || codigo.equals("2"))
            out.println("<b>Transacci&oacute;n Requiere Autorizaci&oacute;n</b>");
         else
            out.println("<b>Ocurri&oacute; un Error de Comunicaci&oacute;n, Verifique.</b>");
       }
  	}
  	catch(NumberFormatException nfe){
     out.println("<b>Error de conexi&oacute;n</b><br>");
    }
  }
  String mensaje = "";
  if(datasession.get("mensajeSUAS")!=null)
  	mensaje = (String) datasession.get("mensajeSUAS");
  out.println(mensaje);
%>
<p>
<%if(!status.equals("OK")){
  	 out.print("<form name=\"entry\" action=\"Final.jsp\" method=\"post\" >");%>
	 <table border="0" cellspacing="0">
	 <tr><td></td></tr>
	 <input id="cont_img" tabindex='1' type="image" src="../imagenes/b_continuar.gif" border="0" onclick="javascript:submitir()">
	 <tr><td></td></tr>
	</table>
<%}
  else
  {
    out.print("<form name=\"entry\" action=\"../../servlet/ventanilla.PageServlet\" method=\"post\" >");
%>
	<input type="hidden" name="transaction" value="<%= session.getAttribute("page.iTxn") %>">
	<input type="hidden" name="transaction1" value="<%= session.getAttribute("page.oTxn") %>">
	<input type="hidden" name="supervisor" value="<%= session.getAttribute("supervisor") %>">
	<input type="hidden" name="override" value="<%= needOverride%>">
	<input type="hidden" name="opcionSuas" value="2">
    <script>
  		document.entry.submit();
	</script>
<%}%>
</form>
</body>
</html>


