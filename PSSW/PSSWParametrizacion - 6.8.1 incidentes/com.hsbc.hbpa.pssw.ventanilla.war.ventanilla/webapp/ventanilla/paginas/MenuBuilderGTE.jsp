<!--
//*************************************************************************************************
//             Funcion: JSP que despliegaMenu Gte
//            Elemento: MenuBuilderGTE.jsp
//          Creado por: Alejandro Gonzalez Castro
//      Modificado por: Marco Lara Palacios
//*************************************************************************************************
// CCN - 4360176 - 06/08/2004 - Se habilita control de usuarios de apoyo
// CCN - 4360275 - 18/02/2005 - WAS 5.1
// CCN - 4360581 - 12/03/2007 - Se convierten los estilos en jsf y se hace un include en el jsp
//*************************************************************************************************
-->

<%@ page session="true" errorPage="error.jsp" import="java.util.*"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
  <title>Untitled</title>
<%@include file="style.jsf"%>
</head>
<body onLoad="initialMenues(document.Txns)">
<%
Vector menuOne = (Vector)session.getAttribute("menu.info");
Vector menuTwo = (Vector)session.getAttribute("menu2.info");
%>
<SCRIPT LANGUAGE="JavaScript">
var newTest = new Array()
<%
  for(int i = 0; i < menuOne.size(); i++)
  {
   out.print("newTest[" + (i) + "] = new Array(");
   Vector Cols = (Vector)menuOne.get(i);
   for(int j = 0; j < Cols.size(); j++)
   {
    session.setAttribute("txnProcess", Cols.get(0));
    out.print("'" + Cols.get(j) + "'");
    if(j + 1 != Cols.size())
     out.print(",");
   }
   out.println(")");
  }
%>

 var newTest2 = new Array()
<%
  for(int i = 0; i < menuTwo.size(); i++){
   out.print("newTest2[" + (i) + "] = new Array(");
   Vector Cols = (Vector)menuTwo.get(i);
   for(int j = 0; j < Cols.size(); j++){
    out.print("'" + Cols.get(j) + "'");
    if(j + 1 != Cols.size())
     out.print(",");
   }
   out.println(")");
  }
%>

function initialMenues(forma){
 setCurrentTxns(forma)
 changeSecTxnsVSOrig(forma)
}

function setCurrentTxns(forma){
	TotalOpt = forma.transaction.options.length
  	Idx1 = TotalOpt - 1
  	while (Idx1 > -1){
   		forma.transaction.options[Idx1] = null
   		Idx1 = Idx1 - 1
  	}
  	if(newTest2.length != 0){
   		TotalOpt = forma.transaction1.options.length
   		Idx1 = TotalOpt - 1
	   	while (Idx1 > -1){
    		forma.transaction1.options[Idx1] = null
    		Idx1 = Idx1 - 1
   		}
  	}
	for(Idx1 = 0, Idx2 = 0; Idx1 < newTest.length; Idx1++, Idx2++){
   		if(parseInt(newTest[Idx1][4]) <= parseInt(forma.tllrlvl.value) && 
   			parseInt(forma.tllrlvl.value) > 2)
   		{
    		forma.transaction.options[Idx2] = new Option();
    		eval("forma.transaction.options[Idx2].text='" + newTest[Idx1][3] + " (" + newTest[Idx1][1] + ")'");
		    eval("forma.transaction.options[Idx2].value='" + newTest[Idx1][1] + "'");
   		}
  	}
  	if(newTest2.length != 0){
   		for(Idx1 = 0, Idx2 = 0; Idx1 < newTest2.length; Idx1++, Idx2++){
    		forma.transaction1.options[Idx2] = new Option();
    		eval("forma.transaction1.options[Idx2].text='" + newTest2[Idx1][4] + " (" + newTest2[Idx1][3] + ")'");
    		eval("forma.transaction1.options[Idx2].value='" + newTest2[Idx1][3] + "'");
   		}
  	}
  	if(forma.transaction.options.length == 0){
   		forma.transaction.options[0] = new Option();
		  eval("forma.transaction.options[0].text='No Disponible '");
	   	eval("forma.transaction.options[0].value='ND'");
  	}
  	forma.transaction.options[0].selected = true;
	  if(newTest2.length != 0)
   		forma.transaction1.options[0].selected = true;
}


function changeSecTxnsVSOrig(forma)
{
	if(newTest2.length < 1)
   		return true;
  	TotalOpt = forma.transaction1.options.length
  	okFlag = 0
  	Idx1 = TotalOpt - 1
  	while (Idx1 > -1){
   		forma.transaction1.options[Idx1] = null
   		Idx1 = Idx1 - 1
  	}
  	for(Idx1 = 0, Idx2 = 0; Idx1 < newTest2.length; Idx1++){
   		if(newTest2[Idx1][1] == forma.transaction.options[forma.transaction.selectedIndex].value){
    		forma.transaction1.options[Idx2] = new Option();
		    eval("forma.transaction1.options[Idx2].text='" + newTest2[Idx1][4] + " (" + newTest2[Idx1][3] + ")'");
		    eval("forma.transaction1.options[Idx2].value='" + newTest2[Idx1][3] + "'");
		    Idx2++
		    okFlag = 1
   		}
  	}
  	if(okFlag == 0){
   		forma.transaction1.options[0] = new Option();
   		eval("forma.transaction1.options[0].text='No Disponible '");
   		eval("forma.transaction1.options[0].value='ND'");
  	}
  	forma.transaction1.options[0].selected = true;
}

function selgte(forma)
{
	opcion1 = document.Txns.transaction[document.Txns.transaction.selectedIndex].value;
	opcion2 = document.Txns.transaction1[document.Txns.transaction1.selectedIndex].value;

	if (opcion1 == "M001")
	{
		if (opcion2 == "M011")
			document.Txns.action =  "../servlet/ventanilla.PageServlet";
		else if (opcion2 == "M012")
			document.Txns.action =  "../servlet/ventanilla.DBUsers";
		else if (opcion2 == "M013")
			document.Txns.action =  "../servlet/ventanilla.PageServlet";
	}
  else if (opcion1 == "M002")
  {
    if( opcion2 == "M021" )
      document.Txns.action = "../ventanilla/paginas/PageBuilderM021.jsp";
    else if( opcion2 == "M022" )
      document.Txns.action = "../ventanilla/paginas/PageBuilderM022.jsp";
  }
	else if (opcion1 == "M003")
	{
		document.Txns.action =  "../servlet/ventanilla.DBUsers";
	}
	else if (opcion1 == "M005")
	{
		if (opcion2 == "0013")
			document.Txns.action =  "../servlet/ventanilla.PageServlet";
        else if (opcion2 == "M051")
            document.Txns.action = "../ventanilla/paginas/PageBuilderGte0013.jsp";
	}
//	else if (opcion1 == "0093"){document.Txns.action =  "../servlet/ventanilla.PageServlet";}
	document.Txns.submit();
}

</script>
<form name="Txns" action="JavaScript:selgte(document.Txns)" method="post">
<input type="hidden" name="transaction2" value="0000">
<table>
<tr>
<td>
  <select name="transaction" size="1" onChange="changeSecTxnsVSOrig(document.Txns)">
  <option value="0001">Un valor de inicio para transacciones de Entrada
  <option value="0002">Un valor de inicio para transacciones de Entrada
  <option value="0003">Un valor de inicio para transacciones de Entrada
  <option value="0004">Un valor de inicio para transacciones de Entrada
  <option value="0005">Un valor de inicio para transacciones de Entrada
  <option value="0006">Un valor de inicio para transacciones de Entrada
  <option value="0007">Un valor de inicio para transacciones de Entrada
  <option value="0008">Un valor de inicio para transacciones de Entrada
  <option value="0009">Un valor de inicio para transacciones de Entrada
  <option value="0010">Un valor de inicio para transacciones de Entrada
 </select>
</td>
<%
 if(menuTwo.size() != 0){
  out.print("<td>");
  out.print("<select name=\"transaction1\" size=\"1\">");
  out.print("<option value=\"0001\">Un valor de inicio para transaccion de Salida");
  out.print("<option value=\"0002\">Un valor de inicio para transaccion de Salida");
  out.print("<option value=\"0003\">Un valor de inicio para transaccion de Salida");
  out.print("<option value=\"0004\">Un valor de inicio para transaccion de Salida");
  out.print("<option value=\"0005\">Un valor de inicio para transaccion de Salida");
  out.print("</select>");
  out.print("</td>");
 }
%>
</tr>
</table>
<input type="hidden" name="tllrlvl" value="<%=session.getAttribute("tellerlevel")%>">
<input type="hidden" name="moneda" value="01">
  <p><input type="image" src="../ventanilla/imagenes/b_aceptar.gif" border="0">
</form>
</body>
</html>
