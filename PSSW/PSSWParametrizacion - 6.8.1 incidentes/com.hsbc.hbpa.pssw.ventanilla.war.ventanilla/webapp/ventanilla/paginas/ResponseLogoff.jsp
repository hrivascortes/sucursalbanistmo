<!--
//*************************************************************************************************
///            Funcion: JSP que muestra mensaje de salida
2//            Elemento: ResponseLogoff.jsp
//          Creado por: Alejandro Gonzalez Castro
// Ultima Modificacion: 22/07/2004
//      Modificado por: Alejandro Gonzalez Castro
//*************************************************************************************************
// CCN - 4360211 - 08/10/2004 - Se elimina mensaje de documentos pendientes por digitalizar
// CCN - 4360291 - 18/03/2005 - Se agrega funcion Cerrar() para poder cerrar la ventana.
// CCN - 4360297 - 23/03/2005 - Se cambia CCN por peticion de QA, CCN anterior 4360291
// CCN - 4360387 - 07/10/2005 - Se realizan adecuaciones para pedir autorización cuando se desfirme el cajero.
// CCN - 4360581 - 12/03/2007 - Se convierten los estilos en jsf y se hace un include en el jsp
//*************************************************************************************************
-->

<%@ page session="true"%>
<%
response.setHeader("Cache-Control","no-store"); //HTTP 1.1
response.setHeader("Pragma","no-cache"); //HTTP 1.0
response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<% String message = (String)session.getAttribute("mensaje");
%>
<html>
<head>
<title>Resultado</title>
<%@include file="specialstyle.jsf"%>
<title>Response Page</title>
</head>
<Script language="JavaScript">
  function AuthoandSignature(entry)
{
  top.openDialog('authoriz.jsp', 220, 130, 'top.setPrefs()', entry)
 

}

  function Cerrar()
  {
    top.close("PSSW");
  }
</Script>

<%

String txnIMGAutoriz = (String)session.getAttribute("page.txnIMGAutoriz");
if (message.length() > 0 && session.getAttribute("tellerlevel").equals("1")) 
 {
  session.setAttribute("FlagAutoriz","1");
  String txn;
 
  if (session.getAttribute("OutTxn").equals("1"))
      txn="0083";
  else
      txn="0082";    
 
  session.setAttribute("page.cTxn",txn);


 %>
 
 
   <body>
<form name="entry" action="../../servlet/ventanilla.Logoff" method="post">
<input type="hidden" name="iTxn" value="<%=txn%>">
<input type="hidden" name="oTxn" value="<%=txn%>">
<input type="hidden" name="cTxn" value="<%=txn%>">
<input type="hidden" name="sucursal" value="<%= session.getAttribute("branch") %>">
<input type="hidden" name="teller" value="<%= session.getAttribute("teller") %>">
<input type="hidden" name="moneda" value="00">
<input type="hidden" name="registro" value="<%=session.getAttribute("empno")%>">
<input type="hidden" name="tllrlvl" value="<%=session.getAttribute("tellerlevel")%>">
<input type="hidden" name="supervisor" value="<%= session.getAttribute("teller") %>">
<input type="hidden" name="AuthoOK" value="0">
<input type="hidden" name="SignOK" value="0">
<input type="hidden" name="validateFLD" value="0">
<input type="hidden" name="needSignature" value="">
<input type="hidden" name="needAutho" value="1">
<input type="hidden" name="SignField" value="">
<input type="hidden" name="override" value="N">
<input type="hidden" name="txtFees" value="0">
<input type="hidden" name="compania" value="20">
<input type="hidden" name="txtSelectedItem" value="0" >
<input type="hidden" name="returnform" value="N" >
<input type="hidden" name="returnform2" value="N" >

<table>
<tr><td>&nbsp;</td></tr>
<tr><td class="normaltextredbold"><%out.print(message);%></td></tr>


<tr><td>&nbsp;</td></tr>
<tr><td><a href="javascript:AuthoandSignature(entry)"><img src="../imagenes/b_continuar.gif" border="0"></a></td></tr>
<table>
</form>
</body>
<%}else%>
<body onload="Cerrar()">

</html>
