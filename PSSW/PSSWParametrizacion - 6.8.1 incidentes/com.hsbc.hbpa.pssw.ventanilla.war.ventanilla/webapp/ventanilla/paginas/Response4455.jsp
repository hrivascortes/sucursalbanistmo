<!--
//*************************************************************************************************
//             Funcion: JSP que despliega respuesta de txn 4455
//            Elemento: Response4455.jsp
//          Creado por: Alejandro Gonzalez Castro
//      Modificado por: Alejandro Gonzalez Castro
//*************************************************************************************************
// CCN - 4360175 - 03/08/2004 - Ajuste size de ventana de impresion
// CCN - 4360275 - 18/02/2005 - WAS 5.1
//*************************************************************************************************
-->

<%@ page session="true" import="java.util.*"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<%!
  private String getString(String s)
  {
   int len = s.length();
   for(; len>0; len--)
    if(s.charAt(len-1) != ' ')
     break;
   return s.substring(0, len);
  }

  private String getItem(String newCadNum, int newOption)
  {
   int iPIndex = newCadNum.indexOf("CONSECUTIVO:") + 12, NumSaltos, Num = 0, Ban = 0;
   String nCadNum = new String("");
   NumSaltos = newOption;
   while(Num < NumSaltos)
   {
    if(newCadNum.charAt(iPIndex) == 0x20)
    {
     while(newCadNum.charAt(iPIndex) == 0x20)
      iPIndex++;
     Num++;
    }
    else
     while(newCadNum.charAt(iPIndex) != 0x20)
      iPIndex++;
   }
   while(newCadNum.charAt(iPIndex) != 0x20 && iPIndex < newCadNum.length()){
    nCadNum = nCadNum + newCadNum.charAt(iPIndex);
    if(iPIndex < newCadNum.length())
      iPIndex++;
    if(iPIndex == newCadNum.length())
      break;
   }

   return nCadNum;
  }
%>
<% Vector resp = (Vector)session.getAttribute("response");
    Hashtable datasession = (Hashtable)session.getAttribute("page.datasession");
%>

<html>
<head>
  <title>Certificación</title>
<style type="text/css">Body {font-family: Arial, Helv; font-size: 10pt; font-weight: bold; color:#000000}</style>
</head>
<body bgcolor="#FFFFFF" onload="top.setFieldFocus(window.document)">
<p>
<%
  Stack flujotxn = (Stack)session.getAttribute("page.flujotxn");
  if( resp.size() != 4 )
   out.println("<b>Error de conexi&oacute;n</b><br>");
  else{
   try{
     String codigo = (String)resp.elementAt(0);
   	 if( !codigo.equals("0") )
    	out.println("<b>Transaccion Rechazada</b><br><br>");
      int lines = Integer.parseInt((String)resp.elementAt(1));
    	String szResp = (String)resp.elementAt(3);
    	for(int i=0; i<lines; i++){
    		String line = szResp.substring( i*78, Math.min(i*78+77, szResp.length()) );
     		out.println(getString(line) + "<p>");
     		if(Math.min(i*78+77, szResp.length()) == szResp.length())
      		break;
    	}
  	}
  	catch(NumberFormatException nfe){
     out.println("<b>Error de conexi&oacute;n</b><br>");
    }
  }
%>
<p>
<%
   out.print("<form name=\"Txns\" action=\"../../servlet/ventanilla.PageServlet\" method=\"post\">");
%>
<table border="0" cellspacing="0">
<tr>
<td>
</td>
</tr>
<tr>
<td>
<%
  String isCertificable = (String)session.getAttribute("page.certifField");  // Para Certificación
  String codigo = (String)resp.elementAt(0);
  String txtCadImpresion = "";
  if( codigo.equals("0") )
     txtCadImpresion  = "~PAGINFBM~" + (String)datasession.get("txtcredito") + "~" + (String)datasession.get("txtpago") + "~" + (String)datasession.get("txtmtto") + "~" + (String)datasession.get("txtmora") + "~" + (String)datasession.get("txttotal") + "~"; // Para Certificación
  session.setAttribute("page.cTxn","5503");
  if(!flujotxn.empty()){
   out.print("<div align=\"center\">");
    if(!isCertificable.equals("N")){ // Para Certificación
    session.setAttribute("Consecutivo", getItem((String)resp.elementAt(3), 1)); // Para Certificación
    session.setAttribute("Tipo", isCertificable); // Para Certificación
    out.println("<a id='continuar_img' tabindex='1' href=\"javascript:top.openDialog('../../servlet/ventanilla.ImprimirServlet?txtCadImpresion=" + txtCadImpresion + "', 160, 120, 'top.setPrefs4()', document.Txns)\"><img src=\"../imagenes/b_continuar.gif\" border=\"0\"></a>"); // Para Certificación
   }
   else
    out.print("<input type=\"image\" src=\"../imagenes/b_continuar.gif\" alt=\"\" border=\"0\">");
   out.print("</div>");
  }
  else{
   session.setAttribute("Consecutivo", getItem((String)resp.elementAt(3), 1)); // Para Certificación
   session.setAttribute("Tipo", isCertificable); // Para Certificación
   out.println("<a id='continuar_img' tabindex='1' href=\"javascript:top.openDialog('../../servlet/ventanilla.ImprimirServlet?txtCadImpresion=" + txtCadImpresion + "', 160, 120, 'top.setPrefs4()', document.Txns)\"><img src=\"../imagenes/b_continuar.gif\" border=\"0\"></a>");
  }
%>
  <script language="javascript">
  
  	var liga = document.getElementById('continuar_img');
  	liga.focus();
  	
  </script>
  <input type="hidden" name="transaction" value="<%= session.getAttribute("page.iTxn") %>">
  <input type="hidden" name="transaction1" value="<%= session.getAttribute("page.oTxn") %>">
</td>
</tr>
</table>
<%
   out.print("</form>");
%>
</body>
</html>
