<!--
//*************************************************************************************************
//             Funcion: JSP que presenta las opciones de la txn 4043
//            Elemento: PageBuilder4043.jsp
//          Creado por: Alejandro Gonzalez Castro
//      Modificado por: Fredy Pe�a Moreno
//*************************************************************************************************
// CCN - 4360275 - 18/02/2005 - WAS 5.1
// CCN - 4360334 - 17/06/2005 - Se convierte fieldLector a fragmento
// CCN - 4360437 - 24/02/2006 - Se realizan validaciones para no mostrar campo de nombre
// CCN - 4360581 - 12/03/2007 - Se convierten los estilos en jsf y se hace un include en el jsp
// CCN - 4620048 - 14/12/2007 - Modificaciones para compra venta
//*************************************************************************************************
-->

<%@ page session="true" import="java.util.*"%>
<%
response.setHeader("Cache-Control","no-store"); //HTTP 1.1
response.setHeader("Pragma","no-cache"); //HTTP 1.0
response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<%
Vector listaCampos = (Vector)session.getAttribute("page.listcampos");
Vector listaContenidos = (Vector)session.getAttribute("page.listcont");
String txnAuthLevel = (String)session.getAttribute("page.reqauto");
String txnIMGAutoriz = (String)session.getAttribute("page.txnIMGAutoriz");
//String txnIMGAutoriz = (String)session.getAttribute("page.reqauto");

int k=0;
int i=0;
String MontoPro = new String("");
String oTxn = (String)session.getAttribute("page.oTxn");  
String iTxn = (String)session.getAttribute("page.iTxn");  
%>
<html>
<head>
  <%@include file="style.jsf"%>
  <title>Datos de la Transaccion</title>
<script type="text/javascript" language="JavaScript">
<!--
var formName = 'Mio'
var bandera=0;
function validate(form)
{
<%

  for(i=0; i < listaCampos.size(); i++)
   {
    Vector vCampos = (Vector)listaCampos.get(i);
    vCampos = (Vector)vCampos.get(0);
					if ( ( vCampos.get(4).toString().length() > 0 )  && !iTxn.equals("0726") && !iTxn.equals("C726"))
    {
      out.println("  if( !top.validate(window, document.entry." + vCampos.get(0) + ", '" + vCampos.get(4) +"') )");
      out.println("    return");
    }
    if ( ( vCampos.get(4).toString().length() > 0 )  && iTxn.equals("0726") && oTxn.equals("4111") && vCampos.get(0).toString().equals("txtDDACuenta1"))
   	{
      out.println("  if( !top.validate(window, document.entry." + vCampos.get(0) + ", '" + vCampos.get(4) +"') )");
      out.println("    return");
    }
   }
%>				
  if ( form.cTxn.value == "0726" || form.oTxn.value == "0106" )
  {
         bandera = 1;
 }

  form.validateFLD.value = '1'
  if (document.entry.txtFechaEfect)
    document.entry.txtFechaSys.value=top.sysdatef();


  if ( bandera == 1 ) {                       // Pedir autorizacion
        form.onSubmit="return validate(document.entry);"
        document.entry.needAutho.value = '1';
        AuthoandSignature(document.entry);
  }

  if ( bandera == 0) {
 <%
 if(!txnAuthLevel.equals("1") && !txnIMGAutoriz.equals("1")){
    out.println("  form.submit()");
    out.println("var obj = document.getElementById('btnCont');");
    out.println("obj.onclick = new Function('return false');");
    }
    
%> }
}
function AuthoandSignature(entry)
{
 var Cuenta;
// alert("Autorizacion:" + entry.needAutho.value + ":" + entry.needSignature.value + ":" + entry.AuthoOK.value + ":" + entry.SignOK.value)
 if(entry.validateFLD.value != '1')
  return
 if(entry.needAutho.value == '1' && entry.AuthoOK.value != '1'){
   top.openDialog('../ventanilla/paginas/authoriz.jsp', 220, 130, 'top.setPrefs()', entry)
 }
 if(entry.needSignature.value == '1' && entry.SignField.value != ''){
  Cuenta = eval('entry.' + entry.SignField.value + '.value')
//  alert('Alerta Uno:<' + entry.SignField.value + '>' + Cuenta)
  top.openModalDialog('<%=request.getContextPath()%>/servlet/ventanilla.FirmasServlet?txtDDACuenta=' + Cuenta, 710, 430, 'top.setPrefs2()', entry)
 }
}
//-->
</script>
</head>
<body onload="top.setFieldFocus(window.document)">
<br>
<% out.print("<form name=\"entry\" action=\"../servlet/ventanilla.DataServlet\" method=\"post\" ");
   if(txnAuthLevel.equals("1"))
    out.print("onSubmit=\"return validate(this)\"");
   out.println(">");
%>

<h1><%=session.getAttribute("page.txnImage")%></h1>

<h3>
<%out.print(session.getAttribute("page.cTxn") + "  " +
                (String)session.getAttribute("page.txnlabel"));%>
</h3>
<table border="0" cellspacing="0">
<%
  Hashtable datasession = (Hashtable)session.getAttribute("page.datasession");
  for(i =  0; i < listaCampos.size(); i++)
   {
    Vector vCampos = (Vector)listaCampos.get(i);
    vCampos = (Vector)vCampos.get(0);
    if(!vCampos.get(0).toString().equals("txtCodSeg")){
     if (! ((iTxn.equals("C726")||iTxn.equals("0726")) && vCampos.get(0).toString().equals("txtCliente")))     
     { /* validacion para el nombre */
     out.println("<tr>");
     //if (vCampos.get(0).equals("txtDDACuenta"))
     //   vCampos.set(2,"Numero de Cuenta de Debito");
     //else if (vCampos.get(0).equals("txtDDACuenta1"))
     //   vCampos.set(2,"Numero de Cuenta de Cr�dito");
     if (oTxn.equals("0120") || oTxn.equals("0122")){
        if(i == 1){
            out.print("<tr>");
            out.print(" <td>");
            out.print("  <i><b>Ordenante</b></i>");
            out.print(" </td>");
            out.println("</tr>");
        }
        else if(i == 3){
          out.print("<tr>");
          out.print(" <td>");
          out.print("  <i><b>Beneficiario</b></i>");
          out.print(" </td>");
          out.println("</tr>");
       }    
     }
     if (vCampos.get(0) != null && vCampos.get(0).equals("txtDDACuenta"))
	 	out.println("  <td> N&uacute;mero de Cuenta de D&eacute;bito:</td>");
	 else if (vCampos.get(0) != null && vCampos.get(0).equals("txtDDACuenta1"))
	 	out.println("  <td> N&uacute;mero de Cuenta de Cr&eacute;dito:</td>");
     out.println("  <td width=\"10\">&nbsp;</td>");
     if(!vCampos.get(0).toString().substring(0,3).equals("lst")){
      out.print  ("  <td><input maxlength=\"" + vCampos.get(3) + "\" name=\"" + vCampos.get(0) + "\" size=\"" + vCampos.get(3) +"\" tabindex=\"" + (i+1) + "\" type=\"text\"");
      String CampoR = (String)vCampos.get(5);
      if(CampoR != null && CampoR.length() > 0){
       CampoR = CampoR.trim();
/*       if(CampoR.charAt(0) == '=')
        out.print(" value=\"javascript:" + CampoR.substring(1) + "\"");
       else*/
        out.print(" value=\"" + CampoR + "\"");
      }
     }
     else{
      out.print("  <td><select name=\"" + vCampos.get(0) + "\" tabindex=\"" + (i+1) + "\" size=\"1\"");
      if( vCampos.get(4).toString().length() > 0 )
       out.println(" onChange=\"top.validate(window, this, '" + vCampos.get(4) +"')\">");
      else
       out.println(">");
      for(k=0; k < listaContenidos.size(); k++){
	       Vector v1Campos = (Vector)listaContenidos.get(k);
	       for(int j = 0; j < v1Campos.size(); j++){
	         Vector v1Camposa = (Vector)v1Campos.get(j);
	         if(v1Camposa.get(0).toString().equals(vCampos.get(0).toString()))
	           out.println("  <option value=\"" + v1Camposa.get(3) + "\">" + v1Camposa.get(2));
       }
      }
      out.println("  </select></td>");
     }
     if( vCampos.get(4).toString().length() > 0 && !vCampos.get(0).toString().substring(0,3).equals("lst"))
      out.println(" onBlur=\"top.estaVacio(this)||top.validate(window, this, '" + vCampos.get(4) +"')\"  onKeyPress=\"top.keyPressedHandler(window, this, '" + vCampos.get(4) +"')\"></td>");
     else if(!vCampos.get(0).toString().substring(0,3).equals("lst"))
      out.println(">");
     out.println("</tr>");
     } /*  Aqui termina if de la condicion para el nombre*/
    }
    else{
     %><%@include file="fieldLector.jsf"%><%
     i += 3 ;
    }
   }

//if (oTxn.equals("4111") && iTxn.equals("4043"))
//{
%> <!--
	<tr>
	<td>Requiere Comprobante Fiscal:</td>
  	<td width="10">&nbsp;</td>
  	<td><select name="lstCF" tabindex="<%//=(++i)%>" size="1">
  	<option value="0">NO
  	<option value="1">SI
  	</select></td>
	</tr>
	<tr>
  	<td>R.F.C.:</td>
  	<td width="10">&nbsp;</td>
  	<td><input maxlength="19" name="txtRFC" size="19" tabindex="<%//=(++i)%>" type="text" value="" onBlur="top.estaVacio(this)||top.validate(window, this, 'isRFCCF')" onKeyPress="top.keyPressedHandler(window, this, 'isRFCCF')"></td>
	</tr>
	<tr>
  	<td>Importe de I.V.A.:</td>
  	<td width="10">&nbsp;</td>
  	<td><input maxlength="11" name="txtIVA" size="14" tabindex="<%//=(++i)%>" type="text" value="" onBlur="top.estaVacio(this)||top.validate(window, this, 'isRFCCF')" onKeyPress="top.keyPressedHandler(window, this, 'isRFCCF')"></td>
	</tr> -->
<%//}%>
  <td>Monto:</td>
  <td width="10" align="right">&nbsp;</td>
  <td align="right"><%
   String moneda = (String)session.getAttribute("page.moneda");
   if(moneda.equals("01")){
    String monto = (String)datasession.get("txtMonto");
    if(!monto.equals("0.00"))
     out.print(datasession.get("txtMonto"));
    else
     out.print(datasession.get("txtMontoC"));
   }
   else
    out.print(datasession.get("txtMonto1"));
  %></td>
 </tr>
  <tr>
  <td>Ejecutivo:</td>
  <td width="10">&nbsp;</td>
  <td align="right"><%= session.getAttribute("teller") %></td>
 </tr>
</table>
<input type="hidden" name="iTxn" value="<%= session.getAttribute("page.iTxn") %>">
<input type="hidden" name="oTxn" value="<%= session.getAttribute("page.oTxn") %>">
<input type="hidden" name="cTxn" value="<%= session.getAttribute("page.cTxn") %>">
<input type="hidden" name="sucursal" value="<%= session.getAttribute("branch") %>">
<input type="hidden" name="teller" value="<%= session.getAttribute("teller") %>">
<input type="hidden" name="moneda" value="<%= session.getAttribute("page.moneda")%>">
<input type="hidden" name="registro" value="<%=session.getAttribute("empno")%>">
<input type="hidden" name="tllrlvl" value="<%=session.getAttribute("tellerlevel")%>">
<input type="hidden" name="supervisor" value="<%= session.getAttribute("teller") %>">
<input type="hidden" name="AuthoOK" value="0">
<input type="hidden" name="SignOK" value="0">
<input type="hidden" name="validateFLD" value="0">
<input type="hidden" name="compania" value="20">
<input type="hidden" name="needSignature" value="<%out.print(txnIMGAutoriz);%>">
<input type="hidden" name="needAutho" value="<%out.print(txnAuthLevel);%>">
<input type="hidden" name="SignField" value="<%= session.getAttribute("page.SignField") %>">
<p>
<script>
 if (document.entry.txtFechaEfect)
 {
  document.writeln("<input tabindex=\"<%=(++i)%>\" type=hidden name=txtFechaSys>");
  document.entry.txtFechaEfect.value=top.sysdate();
 }
</script>
<% if(txnAuthLevel.equals("1") || txnIMGAutoriz.equals("1"))
    out.println("<a id='btnCont' tabindex=\"" + (++i) + "\" href=\"javascript:validate(document.entry);javascript:AuthoandSignature(document.entry)\"><img src=\"../ventanilla/imagenes/b_aceptar.gif\" border=\"0\"></a>");
   else
    out.println("<a id='btnCont' tabindex=\"" + (++i) + "\" href=\"javascript:validate(document.entry)\"><img src=\"../ventanilla/imagenes/b_aceptar.gif\" border=\"0\"></a>");
%>
</form>
</body>
</html>
