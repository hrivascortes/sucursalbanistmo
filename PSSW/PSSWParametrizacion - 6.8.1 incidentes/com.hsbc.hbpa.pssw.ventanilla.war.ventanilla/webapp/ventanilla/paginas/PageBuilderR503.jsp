<!--
//*************************************************************************************************
//            Elemento: PageBuilderR503.jsp
//             Funcion: PageBuilder que redireciona el proceso del SAT. 
//          Creado por: Israel de Paz Mercado
// CCN - 4360409 - 06/01/2006 - Se crea PageBuilder para la Linea de Captura del SAT.
// CCN - 4360581 - 12/03/2007 - Se convierten los estilos en jsf y se hace un include en el jsp
//*************************************************************************************************
-->


<%@ page session="true" import="java.util.*"  %>

<%
response.setHeader("Cache-Control","no-store"); //HTTP 1.1
response.setHeader("Pragma","no-cache"); //HTTP 1.0
response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">



<html>
<head>
<%@include file="style.jsf"%>




</script>
</head>
<body bgcolor="#FFFFFF" onload="top.setFieldFocus(window.document)">
<input type="hidden" name="iTxn" value="<%= session.getAttribute("page.iTxn") %>">
<input type="hidden" name="oTxn" value="<%= session.getAttribute("page.oTxn") %>">
<input type="hidden" name="cTxn" value="S027">
<input type="hidden" name="sucursal" value="<%= session.getAttribute("branch") %>">
<input type="hidden" name="teller" value="<%= session.getAttribute("teller") %>">
<input type="hidden" name="moneda" value="<%= session.getAttribute("page.moneda")%>">
<input type="hidden" name="registro" value="<%=session.getAttribute("empno")%>">
<input type="hidden" name="tllrlvl" value="<%=session.getAttribute("tellerlevel")%>">
<input type="hidden" name="supervisor" value="<%= session.getAttribute("teller") %>">
<input type="hidden" name="AuthoOK" value="0">
<input type="hidden" name="SignOK" value="0">
<input type="hidden" name="validateFLD" value="0">
<input type="hidden" name="SignField" value="<%= session.getAttribute("page.SignField") %>">

<%
    Stack flujotxn = new java.util.Stack();
    flujotxn = (Stack)session.getAttribute("page.flujotxn");
    String txn="R503";
    flujotxn.add(txn);
    session.setAttribute("entro","2");
    String proceso= "RedirectorSatR.jsp";
    response.sendRedirect(proceso);
	
%>
</body>
</html>