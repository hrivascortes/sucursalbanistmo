<!--
//*************************************************************************************************
//             Funcion: JSP que despliega captura de txn 9646
//            Elemento: PageBuilder9646.jsp
//          Creado por: Alejandro Gonzalez Castro
//      Modificado por: Alejandro Gonzalez Castro
//*************************************************************************************************
// CCN - 4360175 - 03/08/2004 - Ajuste size de ventana de impresion
// CCN - 4360246 - 19/11/2004 - Se agregan causas de devolucion de cheques
// CCN - 4360275 - 18/02/2005 - WAS 5.1
// CCN - 4360581 - 12/03/2007 - Se convierten los estilos en jsf y se hace un include en el jsp
//*************************************************************************************************
-->


<%@ page session="true" import="java.util.*"%>
<%
response.setHeader("Cache-Control","no-store"); //HTTP 1.1
response.setHeader("Pragma","no-cache"); //HTTP 1.0
response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<%
 Vector listaCampos = (Vector)session.getAttribute("page.listcampos");
 Vector listaContenidos = (Vector)session.getAttribute("page.listcont");
 int k;
%>
<html>
<head>
 <%@include file="style.jsf"%>
 <title>Datos de la Transaccion</title>
 <script   language="javascript">
 <!--
  var causas = new Array();
  
  causas[0] = new String('FONDOS INSUFICIENTES SEGUN NUESTROS LIBROS (ART. 175)');
  causas[1] = new String('NO TIENE CUENTA CON NOSOTROS EL LIBRADOR (ART. 175)');
  causas[2] = new String('FALTA LA FIRMA DEL LIBRADOR (ART. 176)');
  causas[3] = new String('LA FIRMA DEL LIBRADOR NO ES COMO LA QUE TENEMOS REGISTRADA (ART. 194)');
  causas[4] = new String('LA NUM. DEL CHEQUE NO CORRESP. A LOS ESQUELETOS MINISTRADOS AL LIBR. (ART. 175))');
  causas[5] = new String('EL NUM. DE CHEQUE CORRESP. A UN TALONARIO QUE SE REPORTO EXTRAVIADO (ART. 194)');
  causas[6] = new String('NO ES A NUESTRO CARGO (ART. 175)');
  causas[7] = new String('TENEMOS ORDEN JUDICIAL DE NO PAGARLO (ART. 42 Y SIGUIENTES)');
  causas[8] = new String('HA SIDO REVOCADO Y YA VENCIO EL PLAZO LEGAL PARA SU PRESENTACION (ART. 185)');
  causas[9] = new String('EL LIBRADOR SE ENCUENTRA EN ESTADO DE CONCURSO O SUSPENSION DE PAGOS (ART. 188)');
  causas[10] = new String('NO HAY CONTINUIDAD EN LOS ENDOSOS (ART. 39)');
  causas[11] = new String('POR HABERSE NEGOCIADO INDEBIDAMENTE (ARTS. 179,199,200 Y 201)');
  causas[12] = new String('ES PAGADERO EN OTRA MONEDA');
  causas[13] = new String('ESTA ALTERADO');
  causas[14] = new String('SE COBRA POR CANTIDAD INDISTINTA');
  causas[15] = new String('CARECE DE FECHA');
  causas[16] = new String('YA PAGAMOS EL ORIGINAL Y DUPLICADO');
  causas[17] = new String('ESTA MUTILADO � DETERIORADO');
  causas[18] = new String('NO ES COMPENSABLE');
  causas[19] = new String('POR NO CUMPLIR MENCIONES Y REQUERIMIENTOS');
  causas[20] = new String('NO CONTIENE ORDEN INCONDICIONAL');
  causas[21] = new String('NO ESTA EXPRESADO EN N$');
  causas[22] = new String('PAGO SUSPENDIDO');
  causas[23] = new String('NO PAGAR A SOLICITUD DEL LIBRADOR');
  causas[24] = new String('OTRAS');


  function replaceWhiteSpaces(inputString){
    var inputLength = inputString.length;
    var outputString = new String();
    var tempChar = ' ';
    var i = 0;
    for(; i < inputLength; i++){
      tempChar = inputString.charAt(i);
      if(tempChar == ' ')
        tempChar = '%20';
      outputString += tempChar;
    }
    return outputString;
  }
  
  function valida(){
    var arrayIndex = 0;
    NumCausa = -1;
    for(; arrayIndex < document.forms[0].rdoCausaDevCQ.length; arrayIndex++){
         if (document.forms[0].rdoCausaDevCQ[arrayIndex].checked=="1") {
            NumCausa = arrayIndex;
         }
    }
   return NumCausa;
  }



  function submitPrint(){
   var formName = document.forms[0];
   var cadImpresion = new String('PROTESTO1~');
   if ( valida() < 0) {
      alert("Error, Debe seleccionar una Causa.")
      return;
   }
   cadImpresion = cadImpresion + causas[valida()] + "~"
   cadImpresion = '../servlet/ventanilla.ImprimirServlet?txtCadImpresion=' + replaceWhiteSpaces(cadImpresion);
   top.openDialog(cadImpresion, 160, 120, 'top.setPrefs4()', document.forms[0]);
  }
 //-->
 </script>
</head>
<body bgcolor="#FFFFFF" onload="top.setFieldFocus(window.document);">
<br>
<form name="RechazoCI">
<h3>
<%out.print(session.getAttribute("page.cTxn") + "  " + (String)session.getAttribute("page.txnlabel"));%>
</h3>
<table border="0" cellspacing="0">
<%
    int col = 1;
    for(int i =  0; i < listaCampos.size(); i++){
  	  java.util.Vector vCampos = (java.util.Vector)listaCampos.get(i);
	  vCampos = (Vector)vCampos.get(0);
          out.println("<tr>");
	  out.println("  <td> <b>" + vCampos.get(2) + ": </b></td>");
          out.println("</tr> <tr>");
	  out.println("  <td width=\"10\">&nbsp;</td> </tr>");
	  for(k=0; k < listaContenidos.size(); k++){
	     Vector v1Campos = (Vector)listaContenidos.get(k);
	     for(int j = 0; j < v1Campos.size(); j++){
	         Vector v1Camposa = (Vector)v1Campos.get(j);
	         if(v1Camposa.get(0).toString().equals(vCampos.get(0).toString())) {
                   if (col == 1)
                   {
                        out.println("<tr>");
                   }
	           out.print  ("  <td><input type=\"radio\"" + " name=\"" + vCampos.get(0) + "\"" );
	           out.println("  value=\"" + v1Camposa.get(3) + "\">" + v1Camposa.get(2) + "</td>");
                   if( col == 2)
                   {
                      out.println("</tr>");
                      col = 1;
                   }
                    else
                        col++;
                 }
	       }
            if( col == 2)
                out.println("</tr>");
	      }
	   }
	%>
 </table>
 <p>
 <a href="javascript:submitPrint()"><img src="../ventanilla/imagenes/b_continuar.gif" border="0"></a>
 <input type="hidden" name="sucursal" value="<%= session.getAttribute("branch") %>">
 </form>
 </body>
</html>
