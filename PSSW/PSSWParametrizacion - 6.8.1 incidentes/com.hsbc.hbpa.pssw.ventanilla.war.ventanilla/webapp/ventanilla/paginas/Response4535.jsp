<!--
//*************************************************************************************************
//             Funcion: JSP que despliega respuesta de txn 4535
//            Elemento: Response4535.jsp
//          Creado por: Alejandro Gonzalez Castro
//      Modificado por: Alejandro Gonzalez Castro
//*************************************************************************************************
// CCN - 4360175 - 03/08/2004 - Ajuste size de ventana de impresion
// CCN - 4360275 - 18/02/2005 - WAS 5.1
//*************************************************************************************************
-->

<%@ page session="true" import="java.util.*"%>
<%
response.setHeader("Cache-Control","no-store"); //HTTP 1.1
response.setHeader("Pragma","no-cache"); //HTTP 1.0
response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<%!
  private String reformatString( String inputString ){
    StringBuffer tempString = new StringBuffer(inputString);
    String outputString = "";
    int inputLength = inputString.length(), i = -1;
    while(++i<inputLength){
      if(tempString.charAt(i) == ' ')
        outputString += "%20";
      else
        outputString += tempString.charAt(i);
    }
    return outputString;
  }

  private String getString(String s)
  {
   int len = s.length();
   for(; len>0; len--)
    if(s.charAt(len-1) != ' ')
     break;
   return s.substring(0, len);
  }

  private String getItem(String newCadNum, int newOption)
  {
   int iPIndex = newCadNum.indexOf("CONSECUTIVO:") + 12, NumSaltos, Num = 0, Ban = 0;
   String nCadNum = new String("");
   NumSaltos = newOption;
   while(Num < NumSaltos)
   {
    if(newCadNum.charAt(iPIndex) == 0x20)
    {
     while(newCadNum.charAt(iPIndex) == 0x20)
      iPIndex++;
     Num++;
    }
    else
     while(newCadNum.charAt(iPIndex) != 0x20)
      iPIndex++;
   }
   while(newCadNum.charAt(iPIndex) != 0x20 && iPIndex < newCadNum.length()){
    nCadNum = nCadNum + newCadNum.charAt(iPIndex);
    if(iPIndex < newCadNum.length())
      iPIndex++;
    if(iPIndex == newCadNum.length())
      break;
   }

   return nCadNum;
  }

  private String quitaespacios(String s){
      String s1 = "";
      s1 = s;
      s = "";
      int len = s1.length();
      for ( int i = 0; i < len; i++ ){
        if ( s1.charAt(i) != ' ') {
           s = s + s1.charAt(i);
         }
        else
           s = s + "%20";
      }
      return s;
   }

%>
<%  Vector resp = (Vector)session.getAttribute("response");
  Hashtable datasession = (Hashtable)session.getAttribute("page.datasession");
  int moneda  = Integer.parseInt((String)datasession.get("moneda"));

  java.util.Hashtable datosSucursal = (java.util.Hashtable)session.getAttribute("branchinfo");

  String NomSuc = "SUC. " + datosSucursal.get("C_NUM_SUCURSAL").toString() + " " + datosSucursal.get("D_NOM_SUCURSAL").toString();

%>

<html>
<head>
  <title>Certificación</title>
<style type="text/css">Body {font-family: Arial, Helv; font-size: 10pt; font-weight: bold; color:#000000}</style>
</head>
<body bgcolor="#FFFFFF">
<p>
<%
  String needOverride = "NO";
  String codigo = "";
  Stack flujotxn = (Stack)session.getAttribute("page.flujotxn");
  if( resp.size() != 4 )
   out.println("<b>Error de conexi&oacute;n</b><br>");
  else
  {
     try
  	{
       codigo = (String)resp.elementAt(0);
       if( codigo.equals("1"))
   	   out.println("<b>Transaccion Rechazada</b><br><br>");
        if( codigo.equals("0") || codigo.equals("1") ) {
     	   int lines = Integer.parseInt((String)resp.elementAt(1));
    	   String szResp = (String)resp.elementAt(3);
    	   for(int i=0; i<lines; i++) {
    		   String line = szResp.substring( i*78, Math.min(i*78+77, szResp.length()) );
     		   out.println(getString(line) + "<p>");
     		   if(Math.min(i*78+77, szResp.length()) == szResp.length())
      		    break;
    	   }
       }
       else {
         if ( codigo.equals("3") || codigo.equals("2"))
            out.println("<b>Transacci&oacute;n Requiere Autorizaci&oacute;n</b>");
         else
            out.println("<b>Ocurri&oacute; un Error de Comunicaci&oacute;n, Verifique.</b>");
         }
      }
      catch(NumberFormatException nfe)
      {out.println("<b>Error de conexi&oacute;n</b><br>");}
  }
%>
<p>
<%
   out.print("<form name=\"Txns\" action=\"../../servlet/ventanilla.PageServlet\" method=\"post\">");
%>
<table border="0" cellspacing="0">
<tr>
<td>
</td>
</tr>
<tr>
<td>
<%
  String isCertificable = (String)session.getAttribute("page.certifField");  // Para Certificación
  String txtCadImpresion  = ""; // Para Certificación
  String txtResp = (String)resp.elementAt(0);
  if( txtResp.equals("0") )
      txtCadImpresion = "~CHQCERT~" + moneda +"~"+(String)datasession.get("txtMonto")+"~"+ NomSuc.trim() +"~";
       

  if(!flujotxn.empty()){
    out.print("<div align=\"center\">");
    if(!isCertificable.equals("N")){ // Para Certificación
         session.setAttribute("Consecutivo", getItem((String)resp.elementAt(3), 1)); // Para Certificación
    	 session.setAttribute("Tipo", isCertificable); // Para Certificación
         session.setAttribute("txtCadImpresion", txtCadImpresion); // Para Certificación
         
    	 if(codigo.equals("0"))
      	    out.println("<script language=\"javascript\">top.openDialog('../../servlet/ventanilla.ImprimirServlet', 160, 120, 'top.setPrefs4()', document.Txns)</script>"); // Para Certificación
    	 else {
           if ( codigo.equals("3") || codigo.equals("2")) {
               needOverride = "SI";
               out.print("<script languaje=\"javascript\"> top.openDialog('authoriz.jsp', 220, 130, 'top.setPrefs8()', document.Txns) </script>");
           }
           else
          	out.println("<a href=\"javascript:top.openDialog('../../servlet/ventanilla.ImprimirServlet', 160, 120, 'top.setPrefs4()', document.Txns)\"><img src=\"../imagenes/b_continuar.gif\" border=\"0\"></a>"); // Para Certificación
         }
    }
    else
       {
          if(codigo.equals("0"))
   	     out.println("<script language=\"javascript\">sharedData = '1'</script>");
      	  else {
           if ( codigo.equals("3") || codigo.equals("2")) {
              needOverride = "SI";
              out.print("<script languaje=\"javascript\"> top.openDialog('authoriz.jsp', 220, 130, 'top.setPrefs8()', document.Txns) </script>");
           }
           else
       		out.print("<input type=\"image\" src=\"../imagenes/b_continuar.gif\" alt=\"\" border=\"0\">");
          }
   	}
    out.print("</div>");
  }
  else{
    if(!isCertificable.equals("N")) // Para Certificación
      {
     	 session.setAttribute("Consecutivo", getItem((String)resp.elementAt(3), 1)); // Para Certificación
   	 session.setAttribute("Tipo", isCertificable); // Para Certificación
         session.setAttribute("txtCadImpresion", txtCadImpresion); // Para Certificación
         
         if ( codigo.equals("3") || codigo.equals("2")) {
              needOverride = "SI";
              out.print("<script languaje=\"javascript\"> top.openDialog('authoriz.jsp', 220, 130, 'top.setPrefs8()', document.Txns) </script>");
         }
         else
             out.println("<a href=\"javascript:top.openDialog('../../servlet/ventanilla.ImprimirServlet', 160, 120, 'top.setPrefs4()', document.Txns)\"><img src=\"../imagenes/b_continuar.gif\" border=\"0\"></a>");
    }
    else
    {   	
    	session.setAttribute("txtCasoEsp", "");
    	session.setAttribute("txtCadImpresion", txtCadImpresion);
       	out.println("<a href=\"javascript:top.openDialog('../../servlet/ventanilla.ImprimirServlet', 160, 120, 'top.setPrefs4()', document.Txns)\"><img src=\"../imagenes/b_continuar.gif\" border=\"0\"></a>"); // Para Certificación
       	}
  }
%>
  <input type="hidden" name="transaction" value="<%= session.getAttribute("page.iTxn") %>">
  <input type="hidden" name="transaction1" value="<%= session.getAttribute("page.oTxn") %>">
<input type="hidden" name="supervisor" value="<%= session.getAttribute("supervisor") %>">
  <input type="hidden" name="override" value="<%= needOverride%>">
</td>
</tr>
</table>
</form>
</body>
</html>
