<!--
//*************************************************************************************************
//             Funcion: JSP que presenta las opciones de la txn 0823
//            Elemento: PageBuilder0823.jsp
//          Creado por: Alejandro Gonzalez Castro
//      Modificado por: Jesus Emmanuel Lopez Rosales
//      Modificado por: Fredy Pe�a Moreno
//*************************************************************************************************
// CCN - 4360275 - 18/02/2005 - WAS 5.1
// CCN - 4360437 - 24/02/2006 - Se realizan modificaciones validar monto en dolares mayor igual a 1 d�lar
// CCN - 4360477 - 19/05/2006 - Se elimna validaci�n monto en dolares mayor igual a 1 d�lar
// CCN - 4360581 - 12/03/2007 - Se convierten los estilos en jsf y se hace un include en el jsp
// CCN - 4360584 - 23/03/2007 - Modificaciones para proyecto OPMN
// CCN - 4620048 - 14/12/2007 - Modificaciones para compra venta, validacion de monto mayor a 10000 DLLs
//*************************************************************************************************
-->

<%@ page session="true" import="java.util.*,ventanilla.com.bital.beans.DatosVarios,ventanilla.GenericClasses"%>
<%
response.setHeader("Cache-Control","no-store"); //HTTP 1.1
response.setHeader("Pragma","no-cache"); //HTTP 1.0
response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<%
Vector lcs = (Vector)session.getAttribute("page.listcampos");
Vector lct = (Vector)session.getAttribute("page.listcont");
String txnAuthLevel = (String)session.getAttribute("page.txnAuthLevel");
String txnIMGAutoriz = (String)session.getAttribute("page.txnIMGAutoriz");
String TxnI = (String)session.getAttribute("page.iTxn");
String TxnO = (String)session.getAttribute("page.oTxn");
int i = 0;
String moneda_s = (String)session.getAttribute("page.moneda_s");
GenericClasses gc = new GenericClasses();
%>
<html>
<head>
  <%@include file="style.jsf"%>
  <title>Datos de la Transaccion</title>
<script type="text/javascript" language="JavaScript">
<!--
var formName = 'Mio'
var bandera = 0;
function validate(form)
{
<%
  for(i=0; i < lcs.size(); i++)
   {
    Vector vCampos = (Vector)lcs.get(i);
    vCampos = (Vector)vCampos.get(0);
    if( vCampos.get(3).toString().length() > 0 )
    {
        out.println("  if( !top.validate(window, document.entry." + vCampos.get(0) + ", '" + vCampos.get(4) +"') )");
        out.println("    return");
    }
   }
%>

// Validaciones monto OPMN
<%
if(session.getAttribute("page.iTxn").toString().equals("4033")){
  DatosVarios montos = DatosVarios.getInstance();                   
  Vector vMontos = new Vector();        
  vMontos =(Vector)montos.getDatosV("MontosOPMN");   
%>

  iMonto = document.entry.txtMonto.value
  iMonto = iMonto.toString().replace(/\$|\,/g,'');   
  intVal = iMonto.substring(0, iMonto.indexOf("."));
  decVal = iMonto.substring(iMonto.indexOf(".") + 1, iMonto.length);   
  Monto = Number(intVal + decVal);
  
  if(Monto > <%=Integer.valueOf(vMontos.get(1).toString()) %> && form.iTxn.value == "4033"){
    alert("Monto maximo excedido");
    return;
  }
<%
}
%>
  if(parseFloat(form.txtMonto.value) > 0.00 && parseFloat(form.txtMonto1.value) > 0.00){
    alert('Favor de introducir un solo monto.')
    return
  }


  if(parseFloat(form.txtMonto.value) == 0.00 && parseFloat(form.txtMonto1.value) == 0.00){
    alert('Favor de introducir un monto.')
    return
  }

  if ( form.iTxn.value == "C726" ) {
    if(parseFloat(form.txtMonto.value) > 0.00 ) {
       alert('Introducir Centavos.')
       return
    }
    if (parseFloat(form.txtMonto1.value) > 0.99 ) {
       alert('Introducir solo Centavos.')
       return
    }
  }

  if (form.txtTipoCambioCV.value != "0.0000" && form.txtAutoriCV.value.length != 0 ) {
    alert('Introducir o Autorizacion o Tipo de Cambio.')
    return
  }

  if(parseFloat(form.txtMonto.value) > 0.00 && form.txtAutoriCV.value.length != 0){
    alert('Introducir Monto.')
    return
  }
  
//VALIDACIONES CONTROL DE EFECTIVO PARA MONTOS MAYORES A 10000 dlls
<%
if(session.getAttribute("page.iTxn").toString().equals("0726")||session.getAttribute("page.oTxn").toString().equals("0106")){
%>

  iMonto = document.entry.txtMonto.value
  iMonto = iMonto.toString().replace(/\$|\,/g,'');   
  intVal = iMonto.substring(0, iMonto.indexOf("."));
  decVal = iMonto.substring(iMonto.indexOf(".") + 1, iMonto.length);  
  Monto = Number(intVal + decVal);  
  
  iMonto1 = document.entry.txtMonto1.value
  iMonto1 = iMonto1.toString().replace(/\$|\,/g,'');   
  intVal1 = iMonto1.substring(0, iMonto1.indexOf("."));
  decVal1 = iMonto1.substring(iMonto1.indexOf(".") + 1, iMonto1.length);  
  Monto1 = Number(intVal1 + decVal1);  
    
  if ( Monto >= 1000000 || Monto1 >= 1000000 ) 
      bandera = 1;
  else
      bandera = 0;

<%
}
%>
  if ( bandera == 1 ) {                       // Pedir autorizacion
        form.onSubmit="return validate(document.entry);"
        document.entry.needAutho.value = '1';
        AuthoandSignature(document.entry);
  }
  if ( bandera == 0) {
  <%
      if(!txnAuthLevel.equals("1") && !txnIMGAutoriz.equals("1")){
          out.println("  form.submit()");
          out.println("var obj = document.getElementById('btnCont');");            
          out.println("obj.onclick = new Function('return false');");           
      }
    
%> }  
  
  function AuthoandSignature(entry)
  {
      if(entry.needAutho.value == '1' && entry.AuthoOK.value != '1')
      {
          top.openDialog('../ventanilla/paginas/authoriz.jsp', 220, 130, 'top.setPrefs()', entry)
      } 
  }
  
}
//-->
</script>
</head>
<body bgcolor="#FFFFFF" onload="top.setFieldFocus(window.document)">
<br>
<% out.print("<form name=\"entry\" action=\"../servlet/ventanilla.DataServlet\" method=\"post\" ");
   if(txnAuthLevel.equals("1"))
    out.print("onSubmit=\"return validate(this)\"");
   out.println(">");
%>

<h1><%=session.getAttribute("page.txnImage")%></h1>

<h3>
<%out.print(session.getAttribute("page.cTxn") + "  " +
                (String)session.getAttribute("page.txnlabel"));%>
</h3>
<table border="0" cellspacing="0">
<tr>
 <table>
  <tr>
   <td>
    <table>
<%
  Vector vCampos = (Vector)lcs.get(0);
  vCampos = (Vector)vCampos.get(0);
  String cvmoneda = (String)session.getAttribute("page.moneda");
  //String divisa, divisa_s;
  i=0;
  //if (cvmoneda.equals("01"))
  //	divisa = "USD$";
  //else
  //	divisa = gc.getDivisa(cvmoneda);
  if(!TxnI.equals("0110") && !TxnO.equals("0778"))
  {
%>
      <tr>
       <td> Monto USD: </td>
       <td width="10">&nbsp;</td>
       <td><input maxlength="<%=vCampos.get(3)%>" name="<%=vCampos.get(0)%>" size="<%=vCampos.get(3)%>" tabindex="<%= (++i) %>" type="text" onBlur="top.estaVacio(this)||top.validate(window, this, '<%=vCampos.get(4)%>')" onKeyPress="top.keyPressedHandler(window, this, '<%=vCampos.get(4)%>')"></td>
      </tr>
   <!--   <tr>
       <td>Comisi�n:</td>
       <td width="10">&nbsp;</td>
       <td>0.00</td>
      </tr>
      <tr>
       <td>IVA:</td>
       <td width="10">&nbsp;</td>
       <td>0.00</td>
      </tr>
      <tr>
       <td>Total:</td>
       <td width="10">&nbsp;</td>
       <td>0.00</td>
      </tr>-->
    </table>
   </td>
   <td>
    <table>
<%
  }
else
  {%>
  <input type="hidden" name="txtMonto" value="0.00">
<%    }
  vCampos = (Vector)lcs.get(1);
  vCampos = (Vector)vCampos.get(0);
  
  if(cvmoneda.equals("01"))
     cvmoneda=moneda_s;
  cvmoneda = gc.getDivisa(cvmoneda);
  //if (moneda_s.equals("01"))
  //	divisa_s = "USD";
  //else
  //	divisa_s = gc.getDivisa(moneda_s);
		
%>
      <tr>
       <td>Monto <%=cvmoneda%>:</td>
       <td width="10">&nbsp;</td>
       <td><input maxlength="<%=vCampos.get(3)%>" name="<%=vCampos.get(0)%>" size="<%=vCampos.get(3)%>" tabindex="<%= (++i) %>" type="text" onBlur="top.estaVacio(this)||top.validate(window, this, '<%=vCampos.get(4)%>')" onKeyPress="top.keyPressedHandler(window, this, '<%=vCampos.get(4)%>')"></td>
      </tr>
 <!--     <tr>
       <td>Comisi�n:</td>
       <td width="10">&nbsp;</td>
       <td>0.00</td>
      </tr>
      <tr>
       <td>IVA:</td>
       <td width="10">&nbsp;</td>
       <td>0.00</td>
      </tr>
      <tr>
       <td>Total:</td>
       <td width="10">&nbsp;</td>
       <td>0.00</td>
      </tr>-->
    </table>
   </td>
  </tr>
 </table>
</tr>
</table>
<table><%
  for(i =  2; i < lcs.size(); i++)
	   {
	    vCampos = (Vector)lcs.get(i);
	    vCampos = (Vector)vCampos.get(0);
	     out.println("<tr>");
	     out.println("  <td>" + vCampos.get(2) + ":</td>");
	     out.println("  <td width=\"10\">&nbsp;</td>");
       out.print("  <td><input maxlength=\"" + vCampos.get(3) + "\" name=\"" + vCampos.get(0) + "\" size=\"" + vCampos.get(3) +"\" tabindex=\"" + (i+1) + "\" type=\"text\"" );
       out.println(" onBlur=\"top.estaVacio(this)||top.validate(window, this, '" + vCampos.get(4) +"')\" onKeyPress=\"top.keyPressedHandler(window, this, '" + vCampos.get(4) +"')\">");
	     out.println("</td>");
	     out.println("</tr>");
    }%>
</table>

<input type="hidden" name="iTxn" value="<%= session.getAttribute("page.iTxn") %>">
<input type="hidden" name="oTxn" value="<%= session.getAttribute("page.oTxn") %>">
<input type="hidden" name="cTxn" value="<%= session.getAttribute("page.cTxn") %>">
<input type="hidden" name="sucursal" value="<%= session.getAttribute("branch") %>">
<input type="hidden" name="teller" value="<%= session.getAttribute("teller") %>">
<input type="hidden" name="moneda" value="<%= session.getAttribute("page.moneda")%>">
<input type="hidden" name="registro" value="<%=session.getAttribute("empno")%>">
<input type="hidden" name="tllrlvl" value="<%=session.getAttribute("tellerlevel")%>">
<input type="hidden" name="supervisor" value="<%= session.getAttribute("teller") %>">
<input type="hidden" name="AuthoOK" value="0">
<input type="hidden" name="SignOK" value="0">
<input type="hidden" name="validateFLD" value="0">
<input type="hidden" name="compania" value="20">
<input type="hidden" name="needSignature" value="<%out.print(txnAuthLevel);%>">
<input type="hidden" name="needAutho" value="<%out.print(txnIMGAutoriz);%>">
<input type="hidden" name="SignField" value="<%= session.getAttribute("page.SignField") %>">


<p>
<script>
 if (document.entry.txtRegistro)
 {
  document.writeln("<input tabindex=\"<%= (++i) %>\" type=hidden name=registroemp >");
  document.entry.txtRegistro.value=top.RegEmpl(window);
 }
</script>
<% if(txnAuthLevel.equals("1") || txnIMGAutoriz.equals("1"))
    out.println("<a id='btnCont' tabindex=\"" + (i+1) + "\" href=\"javascript:validate(document.entry);javascript:AuthoandSignature(document.entry)\" ><img src=\"../ventanilla/imagenes/b_aceptar.gif\" border=\"0\"></a>");
   else
    out.println("<a id='btnCont' tabindex=\"" + (i+1) + "\" href=\"javascript:validate(document.entry)\"><img src=\"../ventanilla/imagenes/b_aceptar.gif\" border=\"0\"></a>");
%>
</form>
</body>
</html>
