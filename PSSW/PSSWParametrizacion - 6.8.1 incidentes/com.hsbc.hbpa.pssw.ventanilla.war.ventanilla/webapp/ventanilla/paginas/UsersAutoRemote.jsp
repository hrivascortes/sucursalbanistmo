<!-- 192 -->

<%@page import="java.util.Hashtable"%>
<%Hashtable cajeros = (Hashtable)session.getAttribute("user.par");%>
<html>
<head>
  <title>Usuarios Disponibles para Autorizar.</title>
</head>
<body onLoad="top.opener.setFieldFocus(window.document); if (top.opener) top.opener.blockEvents(); top.formName=document.forms[0];" onUnload="if (top.opener) top.opener.unblockEvents()" topmargin="0" leftmargin="0" bgproperties="fixed">
<Script language='JavaScript'>
<!--
function Continua(valor,f)
{
  f.indice.value = valor;
  f.submit();
}
function cancelar(f)
{
  window.close();
}
//-->
</Script>
<form name="list" action="authorizRemote.jsp" method="post">
<table>
  <tr>
    <td colspan="2">Seleccione Supervisor o Gerente que debe Autorizar...</td>
  </tr>
  <tr>
    <td colspan="2">&nbsp;</td>
  </tr>

<% int numrow = Integer.parseInt((String)cajeros.get("numrow"));
  if (numrow == 1)
  {%>
  <tr>
    <td colspan="2"><b>No hay Supervisores y/o Gerente firmados en este momento, Intente mas tarde...</b></td>
  </tr>
  <tr>
    <td colspan="2"><a href="javascript:cancelar(document.list)"><img src="../imagenes/b_cancelar.gif" border="0"></a></td>
  </tr>
<%}
  for (int i=1; i<numrow; i++)
  {
   String nombre = (String)cajeros.get("D_NOMBRE" + i);
   int nivel = Integer.parseInt((String)cajeros.get("N_NIVEL" + i));%>
  <tr>
  <%if (nivel == 3)
    {%><td>Supervisor :</td><%}
    else
    {%><td>Gerente :</td><%}%>
    <td><a href="Javascript:Continua(<%=i%>,document.list)"><%=nombre%></a></td>
  </tr>
<%}%>
</table>
<input type="hidden" name="indice" value="0">
</form>
</body>
</html>
