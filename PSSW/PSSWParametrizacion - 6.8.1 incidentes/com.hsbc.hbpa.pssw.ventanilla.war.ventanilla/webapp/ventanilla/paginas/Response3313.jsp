<!--
//*************************************************************************************************
//             Funcion: JSP que despliega respuesta de txn 3313
//            Elemento: Response3313.jsp
//          Creado por: Alejandro Gonzalez Castro
//      Modificado por: Juan Carlos Gaona Marcial
//*************************************************************************************************
// CCN - 4360175 - 03/08/2004 - Ajuste size de ventana de impresion
// CCN - 4360275 - 18/02/2005 - WAS 5.1
// CCN - 4360477 - 19/05/2006 - Se realizan correcciones para el nuevo formato de impresion en inversiones
//*************************************************************************************************
-->
<%@ page session="true" import="java.util.*"%>
<%
response.setHeader("Cache-Control","no-store"); //HTTP 1.1
response.setHeader("Pragma","no-cache"); //HTTP 1.0
response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<%
%>
<%!
  private String getString(String s)
  {
    int len = s.length();
     for(; len>0; len--)
	  if(s.charAt(len-1) != ' ')
	    break;

    return s.substring(0, len);
  }

  private String getCantidad(String s)
   {
     int Indice = s.indexOf(".");
     String cadena = "";
     cadena = s.substring(Indice,Indice+3);
     int Indice2 = Indice;
     for(; Indice>0; Indice--)
	  if(s.charAt(Indice-1) == ' ')
	    break;

     cadena = s.substring(Indice,Indice2)+cadena;
     return cadena;
  }

 private String getItem(String newCadNum, int newOption)
  {
   int iPIndex = newCadNum.indexOf("CONSECUTIVO:") + 12, NumSaltos, Num = 0, Ban = 0;
   String nCadNum = new String("");
   NumSaltos = newOption;
   while(Num < NumSaltos)
   {
    if(newCadNum.charAt(iPIndex) == 0x20)
    {
     while(newCadNum.charAt(iPIndex) == 0x20)
      iPIndex++;
     Num++;
    }
    else
     while(newCadNum.charAt(iPIndex) != 0x20)
      iPIndex++;
   }
   while(newCadNum.charAt(iPIndex) != 0x20 && iPIndex < newCadNum.length()){
    nCadNum = nCadNum + newCadNum.charAt(iPIndex);
    if(iPIndex < newCadNum.length())
      iPIndex++;
    if(iPIndex == newCadNum.length())
      break;
   }
   return nCadNum;
  }
%>
<% Vector resp = (Vector)session.getAttribute("response");
Hashtable datasession = (Hashtable)session.getAttribute("page.datasession");%>

<html>
<head>
  <title>Certificación</title>
<style type="text/css">Body {font-family: Arial, Helv; font-size: 10pt; font-weight: bold; color:#000000}</style>
</head>
<script language="javascript">
<!--
  sharedData = '0';

  function formSubmitter()
  {
    clearInterval(formSubmitter);
    
    if(sharedData != '0')
    {
      document.forms[0].submit()
      sharedData = '0';
    }
  }
//-->
</script>
<body bgcolor="#FFFFFF" onload="top.setFieldFocus(window.document);setInterval('formSubmitter()',2000)">
<p>
<%
   if( resp.size() != 4 )
    out.println("<b>Error de conexi&oacute;n</b><br>");
   else
   {
    try{
     String codigo = (String)resp.elementAt(0);
     if( !codigo.equals("0") )
        out.println("<b>Error en transaccion</b><br>");
     int lines = Integer.parseInt((String)resp.elementAt(1));
     Vector lineas = new Vector();
     String szResp = (String)resp.elementAt(3);
     for(int i=0; i<lines; i++){
        String line = szResp.substring( i*78, Math.min(i*78+77, szResp.length()) );
        out.println(getString(line) + "<p>");
        lineas.addElement(getString(line));
        if(Math.min(i*78+77, szResp.length()) == szResp.length())
           break;
     }

     lines = lineas.size();
     String linearesp = "";
     if (codigo.equals("0")) {
        linearesp = (String)lineas.elementAt(2);
        String txtBeneficiario = linearesp.substring(35,linearesp.length()).replace('*',' ');
        linearesp = (String)lineas.elementAt(9);
        String txtMontoRetiro = getCantidad(linearesp);
        linearesp = (String)lineas.elementAt(7);
        String txtISR = getCantidad(linearesp);
        linearesp = (String)lineas.elementAt(6);
        String txtIntDev = getCantidad(linearesp);
        linearesp = (String)lineas.elementAt(4);
        String txtMonedaCDA = linearesp.substring(13,linearesp.length());
        linearesp = (String)lineas.elementAt(5);
        String txtSaldo = getCantidad(linearesp);
        linearesp = (String)lineas.elementAt(11);
        String txtFecAper = linearesp.substring(19,25);
        linearesp = (String)lineas.elementAt(12);
        String txtFecVen = linearesp.substring(19,25);
        String txtCDACuenta = (String)datasession.get("txtCDACuenta");

	// 2004-03-31 ARM: Cambio notificado por cmtH (Se sustituyo la linea comentada por las 3 que siguen)
	// String UDI = (String)lineas.elementAt(14);
        String UDI = "";
        if(txtMonedaCDA.equals("UDI"))
            UDI = (String)lineas.elementAt(14);

        datasession.put("txtBeneficiario",txtBeneficiario);
        datasession.put("txtMonedaCDA",txtMonedaCDA);
        datasession.put("txtMontoRetiro",txtMontoRetiro);
        datasession.put("txtMonto",txtMontoRetiro);
        datasession.put("txtISR",txtISR);
        datasession.put("txtIntDev",txtIntDev);
        datasession.put("txtSaldo",txtSaldo);
        datasession.put("txtFecAper",txtFecAper);
        datasession.put("txtFecVen",txtFecVen);
        datasession.put("txtCDAO",txtCDACuenta);
        datasession.put("txtCDA",txtCDACuenta);
        
	// 2004-03-31 ARM: Cambio notificado por cmtH (Se inserto la condicion if)
        if(txtMonedaCDA.equals("UDI"))
          datasession.put("txtUDI",UDI);
        
        session.setAttribute("page.datasession",datasession);
     }
    }

    catch(NumberFormatException nfe){
     out.println("<b>Error de conexi&oacute;n</b><br>");
    }
   }
%>
<form name="Txns" action="../../servlet/ventanilla.PageServlet" method="post">
<table border="0" cellspacing="0">
<tr>
<td>
</td>
</tr>
<tr>
<td>
<%
  String isCertificable = (String)session.getAttribute("page.certifField");  // Para Certificación
  String txtCadImpresion  = ""; // Para Certificación
  Stack flujotxn = (Stack)session.getAttribute("page.flujotxn");
  String codigo = (String)resp.elementAt(0);
  if(!flujotxn.empty())
  {
    out.print("<div align=\"center\">");
    
    /*if(!isCertificable.equals("N"))
    { 
      // Para Certificación
      session.setAttribute("Consecutivo", getItem((String)resp.elementAt(3), 1)); // Para Certificación
      session.setAttribute("Tipo", isCertificable); // Para Certificación
      
      if(codigo.equals("0"))
	out.println("<script language=\"javascript\">top.openDialog('../../servlet/ventanilla.ImprimirServlet?txtCadImpresion=" + txtCadImpresion + "', 160, 120, 'top.setPrefs4()', document.Txns)</script>"); // Para Certificación
      else
      	out.println("<a href=\"javascript:top.openDialog('../../servlet/ventanilla.ImprimirServlet?txtCadImpresion=" + txtCadImpresion + "', 160, 120, 'top.setPrefs4()', document.Txns)\"><img src=\"imagenes/b_continuar.gif\" border=\"0\"></a>"); // Para Certificación
    }
    else 
    {*/
      if(codigo.equals("0"))
      {
	out.println("<script language=\"javascript\">sharedData = '1'</script>");
	out.println("<font color='red'>procesando...</font>");
      }
      else
 	out.print("<input type=\"image\" src=\"imagenes/b_continuar.gif\" alt=\"\" border=\"0\">");
    /*}*/
    
    out.print("</div>");
  }
  /*else
  {
    session.setAttribute("Consecutivo", getItem((String)resp.elementAt(3), 1)); // Para Certificación
    session.setAttribute("Tipo", isCertificable); // Para Certificación
    out.println("<script language=\"javascript\">top.openDialog('../../servlet/ventanilla.ImprimirServlet', 160, 120, 'top.setPrefs4()', document.Txns)</script>"); // Para Certificación
  }*/
%>
  <input type="hidden" name="transaction" value="<%= session.getAttribute("page.iTxn") %>">
  <input type="hidden" name="transaction1" value="<%= session.getAttribute("page.oTxn") %>">
</td>
</tr>
</table>
</form>
</body>
</html>
