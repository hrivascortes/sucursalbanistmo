<!--
//*************************************************************************************************
//             Funcion: JSP para despliege de txns varias
//            Elemento: PageBuilder.jsp 
//      Modificado por: Jesus Emmanuel Lopez Rosales
//*************************************************************************************************
// CCN - 4360275 - 18/02/2005 - WAS 5.1
// CCN - 4360334 - 17/06/2005 - Se convierte fieldLector a fragmento
// CCN - 4360390 - 07/10/2005 - Se agrega validacion para acdo 1053.
// CCN - 4360443 - 24/02/2006 - Se realizan modificaciones para pedir autorizaci�n para el depto 9010
// CCN - 4360477 - 19/05/2006 - Se agrega validaci�n para txn 5405 en tipo de identifiaci�n cuando es n�merico
// CCN - 4360533 - 20/10/2006 - Se realizan modificaciones para OPEE
// CCN - 4360572 - 23/02/2007 - Se realizan modificacion en validaci�n de monto de txn 5357 y bandera de autoriz remota en txn 0041
// CCN - 4360581 - 12/03/2007 - Se convierten los estilos en jsf y se hace un include en el jsp
// CCN - 4360584 - 23/03/2007 - Modificaciones para proyecto OPMN
// CCN - 4360619 - 15/06/2007 - Se eliminan systems
// CCN - 4620018 - 12/10/2007 - Se valida monto 
// CCN - 4620021 - 17/10/2007 - Se agregan validaciones para montos mayores a 10000 dlls, Humberto Balleza
// CCN - 4620021 - 05/06/2015 - ALEX ORIA - Se agregan funcion para eliminar la solicitud de autorizacion para las transacciones 1001,1003,ARP Y P001
//*************************************************************************************************
-->
<%@ page session="true" import="java.util.*,
	ventanilla.GenericClasses,
	com.bital.util.Comunica"%>

<%
response.setHeader("Cache-Control","no-store"); //HTTP 1.1
response.setHeader("Pragma","no-cache"); //HTTP 1.0
response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<%
String MontoPro = new String("");
ventanilla.GenericClasses gc = new ventanilla.GenericClasses();
Vector listaCampos = (Vector)session.getAttribute("page.listcampos");
Vector listaContenidos = (Vector)session.getAttribute("page.listcont");
String txnAuthLevel = (String)session.getAttribute("page.txnAuthLevel");
String txnIMGAutoriz = (String)session.getAttribute("page.txnIMGAutoriz");
Vector v = (Vector)session.getAttribute("page.specialacct");
Hashtable datasession = (Hashtable)session.getAttribute("page.datasession");
String txn = session.getAttribute("page.cTxn").toString();
String inTxn =session.getAttribute("page.iTxn").toString();
String ctaRepago=null;
String montoKronner=null;
String P002=null;
if(inTxn.equals("P001")){
	session.setAttribute("blagP001","1");
	ctaRepago=(String)session.getAttribute("ctaRepago");
}
if(inTxn.equals("4019")){
	P002= (String)session.getAttribute("blagP002");
 	if(P002!=null && P002.equals("psP002")){
		montoKronner = datasession.get("txtMonto").toString();
		
	}else {
		P002=null;
	}
}
if (datasession == null)
   datasession = new Hashtable();
int k;
int i;

if(inTxn.equals("0500"))
	session.setAttribute("idFlujo","01");

%>
	
<html>
<head>
  <title>Datos de la Transaccion</title>
  <%@include file="style.jsf"%>
<script language="JavaScript">
<!--

var formName = 'Mio'
var specialacct = new Array()
var bandera = 0;
<%//Asignar valor de idflujo para flujos internos
   if(txn.equals("5209")){
 } 
   
  
  
%>
<%
 if(!v.isEmpty()){
  for(int iv = 0; iv < v.size(); iv++){
   out.print("specialacct[" + iv + "]='" + v.get(iv) + "'" + '\n');
  }
 }
%>
function validate(form)
{
   document.entry.fecha4523.value=top.sysdatef();
   form.validateFLD.value = '0';
   inputStr = '';
   
   <%
    if ( txn.equals("4537") || txn.equals("4539")  || txn.equals("4543") || txn.equals("4545") || txn.equals("4547") || txn.equals("1053") || txn.equals("PCGA")) {%>
    if  ( form.txtCveTran.value.length == 0 || form.txtDDACuenta2.value.length == 0 || form.txtSerial2.value.length == 0 ) {
        alert("Introduzca Banda del Cheque.")
        return;
   }
   <% } %>
// CCN -         - 17/10/2007 - Se agregan validaciones para montos mayores a 10000 dlls, Humberto Balleza
  <% if ( txn.equals("5405")) {%>
       opselec = form.lsttipiden.selectedIndex;
       if(opselec == 0){
         if ( isNaN(form.txtnumiden.value) ){
	 	    alert("Valor inv�lido para Licencia, s�lo n�meros. Verifique...");
		    form.txtnumiden.value = "";
		    form.txtnumiden.focus();
		    return;
	 	  }
       }
   <%}%> 
<%
  for(i=0; i < listaCampos.size(); i++)
   {
    Vector vCampos = (Vector)listaCampos.get(i);
    vCampos = (Vector)vCampos.get(0);
//    if( vCampos.get(4).toString().length() > 0 && !vCampos.get(0).toString().equals("txtMonto"))
	if( vCampos.get(4).toString().length() > 0 && datasession.get(vCampos.get(0)) == null)
    {    
        out.println("  if( !top.validate(window, document.entry." + vCampos.get(0) + ", '" + vCampos.get(4) +"') )");
        if (vCampos.get(0).toString().equals("txtFolio") && vCampos.get(4).toString().equals("isEmptyStr")) 
        {        
        %>
         if(document.entry.returnform.value == "N"  ) {
           return false;
         }
         else {
           document.entry.returnform.value = "N";
           return;
         }
    <% }
        else
          out.println("return;") ;
      }
  }
%>
  form.validateFLD.value = '1'
  if (document.entry.txtFechaEfect || document.entry.txtFechaError || document.entry.txtFechaCarta)
    document.entry.txtFechaSys.value=top.sysdatef();

  if (form.dvCheque != null && form.dvCheque.value != '' && form.txtSerial2 != null && form.txtSerial2.value.length > 0) {
	  form.noCheque.value = (form.txtSerial2.value).substr(0,(form.txtSerial2.value).length - 1);
	  form.txtSerial2.value = form.noCheque.value;
  }
  if (form.dvCuenta != null && form.dvCuenta.value != '' && form.txtDDACuenta2 != null && form.txtDDACuenta2.value.length > 0){
	  form.noCuenta.value = top.filler((form.txtDDACuenta2.value).substr(0,(form.txtDDACuenta2.value).length - 1), 11);
	  form.txtDDACuenta2.value = form.noCuenta.value;
  }

  if ( form.iTxn.value == "1001" )
  {
     inputStr = Alltrim(form.txtEfectivo.value);
     inputStr = inputStr.toString().replace(/\$|\,/g,'');
     intVal = inputStr.substring(0, inputStr.indexOf("."));
     decVal = inputStr.substring(inputStr.indexOf(".") + 1, inputStr.length);
     monto = Number(intVal + decVal);
     //inputStr = Alltrim(form.txtCheque.value);
     //inputStr = inputStr.toString().replace(/\$|\,/g,'');
     //intVal = inputStr.substring(0, inputStr.indexOf("."));
     //decVal = inputStr.substring(inputStr.indexOf(".") + 1, inputStr.length);
     //monto = monto + Number(intVal + decVal);
     if ( monto >= 1000000 ) 
         bandera = 1;
     else
         bandera = 0;
 }
 
  if (form.cTxn.value == "0568" && form.lstForLiq.options[form.lstForLiq.selectedIndex].value == '01')
  {
  	 iMonto = "<%=(String)datasession.get("txtMonto")%>";
     iMonto = iMonto.toString().replace(/\$|\,/g,'');   
     intVal = iMonto.substring(0, iMonto.indexOf("."));
     decVal = iMonto.substring(iMonto.indexOf(".") + 1, iMonto.length);   
     Monto = Number(intVal + decVal);
     if ( Monto >= 1000000) 
         bandera = 1;
     else
     {
         bandera = 0;
         entry.needAutho.value='0';
     }

   }

   if ( form.iTxn.value == "5357" || form.cTxn.value == "0829" 
   || form.cTxn.value == "4153" || form.cTxn.value == "0810"
   || form.cTxn.value == "S165" || form.cTxn.value == "S365" 
   || form.cTxn.value == "0476" || form.cTxn.value == "1053"
   || form.cTxn.value == "4537" || form.cTxn.value == "PCGA")
  {
     inputStr = Alltrim(form.txtMonto.value);
     inputStr = inputStr.toString().replace(/\$|\,/g,'');
     intVal = inputStr.substring(0, inputStr.indexOf("."));
     decVal = inputStr.substring(inputStr.indexOf(".") + 1, inputStr.length);
     monto = Number(intVal + decVal);
     if ( monto >= 1000000 ) 
         bandera = 1;
     else
     {
         bandera = 0;
         entry.needAutho.value='0';
     }
 }
 
  if ( form.iTxn.value == "1005" || form.iTxn.value == "5405" || form.iTxn.value == "5407")
  {
     if ( form.iTxn.value == "1005" )
		if (form.txtMonto != null) 
			inputStr = Alltrim(form.txtMonto.value);
     else
     {
       if (form.txtMontoNoCero != null)
         inputStr = Alltrim(form.txtMontoNoCero.value);
     }
     inputStr = inputStr.toString().replace(/\$|\,/g,'');
     intVal = inputStr.substring(0, inputStr.indexOf("."))
     decVal = inputStr.substring(inputStr.indexOf(".") + 1, inputStr.length)
     monto = Number(intVal + decVal);
     if (( monto > 10000000 && form.iTxn.value == "1005") ||
         ( monto > 5000000 && ( form.iTxn.value == "5405" || form.iTxn.value == "5407")))
         bandera = 1;
     else 
         bandera = 0;
  }
  
  if ( form.iTxn.value == "0022" || form.cTxn.value  == "0568" ){
  if(form.cTxn.value  == "0568"){
  if(document.entry.lstForLiq.value == null){
  	document.entry.lstForLiq.value=' ';
  	
	var lista= document.entry.lstForLiq.value;
	if(lista==0){	
 <%
  			String c = "";
  			int montoEntero = 0;
 			if(datasession.get("txtMonto") == null){
    			c = "";
    			}
    		else{
    		
    			c = datasession.get("txtMonto").toString().trim();
    			if(c.indexOf(",")>0 && !inTxn.equals("4019"))
    			{
   			    c = c.replace(",","");
 
    				//String cent =(c.substring(0,c.indexOf(",")));
    				//String decn =(c.substring(c.indexOf(",")+1,c.indexOf(".")));
    				//String entero = cent+decn; 
    				String entero = c.substring(0,c.indexOf("."));
    				montoEntero= Integer.parseInt(entero);
    			}
    		if(montoEntero>=10000){ 			
 %>	
  		bandera = 1;
 <%
 			
 		}
 	}
 %>
  			}
  		}
  	}
  }
<!-- VALIDACION PARA PAGO VOLUNTARIO -->
 
  if(form.iTxn.value=='P001' || form.iTxn.value=='P002' ){
  
  var creditoIni= document.entry.txtCredito.value;
  var lstCredito= document.entry.lstCredito.value;
  document.entry.hdCreditoIni.value=creditoIni;
  
  if(lstCredito.value=='BCC'){
   <%  		   		
   		session.setAttribute("valorLista","BCC");
   %>
   }
   else if(lstCredito.value=='HCC'){
      <%  		   		
   		session.setAttribute("valorLista","HCC");
   %>
   }
   else {
   <%  		   		
   		session.setAttribute("valorLista","CRE");
   %>
   }
  }
 
 var suc ='<%= (String)session.getAttribute("branch")%>'

 if (form.iTxn.value == '0228' && suc == '09010')
     bandera = 1; 
     
  
  if(isTxnWithoutValidateXMonto()) { 
     bandera = 0; 
   }  // TCS 20150604 ALEX ORIA  
  
  if ( bandera == 1 ) {                       // Pedir autorizacion
       form.onSubmit="return validate(document.entry);"
        document.entry.needAutho.value = '1';
        AuthoandSignature(document.entry);
  }

  if ( bandera == 0) {
    
 <%
 if(!txnAuthLevel.equals("1") && !txnIMGAutoriz.equals("1")){
    out.println("  form.submit()");
    out.println("var obj = document.getElementById('btnCont');");
    out.println("obj.onclick = new Function('return false');");
    }
    
%> }
}
function AuthoandSignature(entry)
{
 var Cuenta;
 var CveTransito;
 if(entry.returnform != undefined && entry.txtFolio != undefined)
   if(entry.txtFolio.value.length == 0)
     return;

 if(entry.validateFLD.value != '1')
  return
  
 if (entry.iTxn.value == '1051' || entry.iTxn.value == '1053'
 	|| entry.iTxn.value == '4543' || entry.iTxn.value == '4547'
 	|| entry.iTxn.value == '4537' || entry.iTxn.value == '4539'
 	|| entry.iTxn.value == '4545' || entry.iTxn.value == '1079'
 	|| entry.iTxn.value == 'PCGA')	
 	{
 	CveTransito=entry.txtCveTran.value;
 	}

 if(entry.needAutho.value == '1' && entry.AuthoOK.value != '1')
 {
 		if (entry.iTxn.value == '1053' || entry.iTxn.value == '4537'  || entry.iTxn.value == '1079' || entry.iTxn.value == 'PCGA')
  		    top.openDialog('../ventanilla/paginas/authoriz.jsp', 220, 130, 'top.setPrefs15()', entry)
  		else
  			top.openDialog('../ventanilla/paginas/authoriz.jsp', 220, 130, 'top.setPrefs()', entry)
 }

 if(entry.needSignature.value == '1' && entry.SignField.value != '')
 {
  Cuenta = eval('entry.' + entry.SignField.value + '.value')
	if (entry.iTxn.value == '1051' || entry.iTxn.value == '1053'
	|| entry.iTxn.value == '4543' || entry.iTxn.value == '4547'
	|| entry.iTxn.value == '4537' || entry.iTxn.value == '4539'
	|| entry.iTxn.value == '4545' || entry.iTxn.value == '1079'
	|| entry.iTxn.value == 'PCGA')	
  		top.openModalDialog('<%=request.getContextPath()%>/servlet/ventanilla.FirmasServlet?txtDDACuenta=' + Cuenta+'&txtCveTran='+CveTransito, 710, 430, 'top.setPrefs2()', entry)
  	else
  		top.openModalDialog('<%=request.getContextPath()%>/servlet/ventanilla.FirmasServlet?txtDDACuenta=' + Cuenta, 710, 430, 'top.setPrefs2()', entry)
 }
 
}

function Alltrim(inputStr)
{
  var inStr = new String(inputStr)

  while(inStr.charAt(0) == ' ')
    inStr = inStr.substring(1, inStr.length)

  while(inStr.charAt(inStr.length) == ' ')
    inStr = inStr.substring(0, inStr.length-1)

  return inStr
}


//funcion valida status del cheque de gerencia
function validaChecked(){
	//*****Como se agreg� en el onblur hay que vaidar que sea solo para la transaccion 1197
	if(entry.iTxn.value == '1197' && document.entry.chkChqGerencia != undefined){
		var txtBenefic = document.entry.txtBeneficiario;
		var chkMarcaCheque = document.entry.chkChqGerencia;
		if (chkMarcaCheque.value =! undefined && chkMarcaCheque.checked){
			if(chkMarcaCheque.checked && txtBenefic.value != "CKG "){
			    txtBenefic.value = txtBenefic.value.replace("CKG ",'');	
				txtBenefic.value="CKG "+ txtBenefic.value.substring(0,36);
			}
		}else {	
		    txtBenefic.value = txtBenefic.value.replace("CKG ",'');
		}
	}
}

function isTxnWithoutValidateXMonto(){

	//Requerimiento 011 Se adiciona campo para validar TXN por Monto - Validacion TXN 1001 1003 P001 ARP 

	stTxnVar      ='<%= (String)session.getAttribute("txnsWithOutValidateMonto")%>'
	stTxnVarForm  ='<%= session.getAttribute("page.iTxn").toString()%>'
	stcTxnVarForm ='<%= session.getAttribute("page.cTxn").toString()%>'
   
    var dato = stTxnVar.split('*');
	
	for( pos = 0; pos < dato.length ; pos++){
		 if( dato[pos] == stTxnVarForm) { //TXN 1001 1003 P001 ARP
			  	return true;
		 }
		  
	}
	 	return false;
}

//-->
</script>
</head>
<body bgcolor="#FFFFFF" onload="top.setFieldFocus(window.document)">
<br>
<% out.print("<form name=\"entry\" action=\"../servlet/ventanilla.DataServlet\" method=\"post\" ");
   if(txnAuthLevel.equals("1") || session.getAttribute("page.cTxn").toString().equals("0825"))
   out.print("onSubmit=\"return validate(this)\"");
   out.println(">");
%>
<h1><%=session.getAttribute("page.txnImage")%></h1>
<h3>
<%
if(session.getAttribute("page.iTxn").toString().equals("P001")){
out.print(session.getAttribute("page.iTxn") + "  " + (String)session.getAttribute("page.txnlabel"));
}
else 
out.print(session.getAttribute("page.cTxn") + "  " + (String)session.getAttribute("page.txnlabel"));%>
</h3>
<table border="0" cellspacing="0" class="normaltextblack">

<%if(txn.equals("5183")){%>
<tr>
	<td>Monto USD:</td>
    <td width="10">&nbsp;</td>
    <td align="right">
    	<%	String a = "";
    		if(datasession.get("txtMonto1") == null)
    			a = "";
    		else
    			a = datasession.get("txtMonto1").toString().trim();
    	%>
    	<%=a%>
    </td>
</tr>
<%}%>

<%if(txn.equals("5159")){%>
<tr>
	<td>Monto:</td>
    <td width="10">&nbsp;</td>
    <td align="right">
    	<%	String b = "";
    		if(datasession.get("txtMonto") == null)
    			b = "";
    		else
    			b = datasession.get("txtMonto").toString().trim();
    	%>
    	<%=b%>
    </td>
</tr>
<%}%>


<%
	

	  for(i =  0; i < listaCampos.size(); i++)
	   {
	    Vector vCampos = (Vector)listaCampos.get(i);
	    vCampos = (Vector)vCampos.get(0);
	    if(!vCampos.get(0).toString().equals("txtCodSeg")){
	     out.println("<tr>");
	     if (vCampos.get(0).equals("txtDDACuenta") && (txn.equals("0130") || txn.equals("0128"))){
	         out.println("  <td>N�mero de Cuenta Abono:</td>");      
	     }else if(vCampos.get(0).equals("txtRefTIRE") && (txn.equals("2199") || txn.equals("4523"))){
        		out.println("  <td>Referencia:</td>");
	     }else{
	         out.println("  <td>" + vCampos.get(2) + ":</td>");
	     }	     	     
	     out.println("  <td width=\"10\">&nbsp;</td>");
		
         if(datasession.get(vCampos.get(0)) != null)
		{
			if(vCampos.get(0).equals("txtTipoCambio")){
	     		String TipCamb = (String)datasession.get(vCampos.get(0));
     	  	    java.text.DecimalFormat reformattedValue = new java.text.DecimalFormat("#,###.0000");
	     	    String returnTipCamb = reformattedValue.format((double)(java.lang.Double.parseDouble(TipCamb) / 10000.00));
	     	    out.print  ("  <td>" + returnTipCamb + "</td>");
	     	}
		    else 
		    	out.print  ("  <td>" + datasession.get(vCampos.get(0)) + "</td>");
  	 		out.println("</tr>");				  
		}
        else{ 

	     if(!vCampos.get(0).toString().substring(0,3).equals("lst"))
       {
            if(vCampos.get(0).toString().substring(0,3).equals("pss"))
            {
                out.print  ("  <td><input maxlength=\"" + vCampos.get(3) + "\" name=\"" + vCampos.get(0) + "\" size=\"" + vCampos.get(3) +"\" type=\"Password\"" + " tabindex=\""+(i+1)+"\"");
            }
         //checkbox cheque de gerencia
            else if (vCampos.get(0).toString().equals("chkChqGerencia")){
      	         out.print  ("  <td><input name=\"" + vCampos.get(0) + "\"  type=\"checkbox\"" + " tabindex=\""+(i+1)+"\" onclick=\"validaChecked();\"" );          
            }
            else
            {
                out.print("  <td><input maxlength=\"" + vCampos.get(3) + "\" name=\"" + vCampos.get(0) + "\" size=\"" + vCampos.get(3) +"\" type=\"text\"" + " tabindex=\""+(i+1)+"\"");
            }
 //    ACTUALIZAR P002 
  //       System.out.println("antes");
    //       if(vCampos.get(0).equals("txtDDACuenta") && P002 != null )
	//			{	System.out.println("entro al if");
	//				out.print(" value=\"" + ctaRepago + "\"" );
	//				session.setAttribute("txnProcess","00");
					
	//			} 
	//		System.out.println("antes dos"); 
	  //  if(vCampos.get(0).equals("txtMonto") && P002 != null)
		//		{
		//			out.print(" value=\"" + montoKronner + "\"");	
				
				
		//}
//end P002

	      String CampoR = (String)vCampos.get(5);
	      if(CampoR != null && CampoR.length() > 0){
	        CampoR = CampoR.trim();
	        //System.out.println(vCampos.get(0) + " " + ctaRepago + " " + CampoR + " " + CampoR.length());
	        out.print(" value=\"" + CampoR + "\"");
	         
	      }
	      		
              String level = (String)session.getAttribute("tellerlevel");
              if (vCampos.get(0).toString().equals("txtFechaEfect") && level.equals("1") && 
                 (txn.equals("2503") || txn.equals("2525") || txn.equals("3009") || txn.equals("3075") ||
                  txn.equals("3077") || txn.equals("3105") || txn.equals("3313") || txn.equals("3314")))
                out.print(" onfocus=\"blur()\"");
	     }
	     

	     else{
	      out.print("  <td><select name=\"" + vCampos.get(0) + "\" tabindex=\"" + (i+1) + "\" size=\"" + "1" + "\"");
//	      if( vCampos.get(3).toString().length() > 0 ){
	      if( vCampos.get(4).toString().length() > 0 ){
	       if(vCampos.get(0).toString().substring(0,3).equals("rdo"))
           out.println(" onClick=\"top.validate(window, this, '" + vCampos.get(4) +"')\">");
	       else
	         out.println(" onChange=\"top.validate(window, this, '" + vCampos.get(4) +"')\"  >");
	      }
	      else
	       out.println(">");
	      for(k=0; k < listaContenidos.size(); k++){
	       Vector v1Campos = (Vector)listaContenidos.get(k);
	       for(int j = 0; j < v1Campos.size(); j++){
	         Vector v1Camposa = (Vector)v1Campos.get(j);
	         if(v1Camposa.get(0).toString().equals(vCampos.get(0).toString()))
	           out.println("  <option value=\"" + v1Camposa.get(3) + "\">" + v1Camposa.get(2));
	       }
	      }
	      out.println("  </select></td>");
	      
	      	 
	     }
	     if( vCampos.get(4).toString().length() > 0 && !vCampos.get(0).toString().substring(0,3).equals("lst"))
	      out.println("  onBlur=\"top.estaVacio(this)||top.validate(window, this, '" + vCampos.get(4) +"');validaChecked();\"  onKeyPress=\"top.keyPressedHandler(window, this, '" + vCampos.get(4) +"')\"   ></td>");
	     else if(!vCampos.get(0).toString().substring(0,3).equals("lst"))
	      out.println(">");
	     out.println("</tr>");
	    }
	    }
	    else{
	     %><%@include file="fieldLector.jsf"%><%
	     i = k;
	    }
	   }
	%>
<%if(txn.equals("0128") || txn.equals("0130")){%>
  <tr>
  <td>Ejecutivo:</td>
  <td width="10">&nbsp;</td>
  <td align="left"><%= session.getAttribute("teller") %></td>
 </tr>
<%}%>	
</table>
<input type="hidden" name="iTxn" value="<%= session.getAttribute("page.iTxn") %>">
<input type="hidden" name="oTxn" value="<%= session.getAttribute("page.oTxn") %>">
<input type="hidden" name="cTxn" value="<%= session.getAttribute("page.cTxn") %>">
<input type="hidden" name="sucursal" value="<%= session.getAttribute("branch") %>">
<input type="hidden" name="teller" value="<%= session.getAttribute("teller") %>">
<input type="hidden" name="moneda" value="<%= session.getAttribute("page.moneda")%>">
<input type="hidden" name="registro" value="<%=session.getAttribute("empno")%>">
<input type="hidden" name="tllrlvl" value="<%=session.getAttribute("tellerlevel")%>">
<input type="hidden" name="supervisor" value="<%= session.getAttribute("teller") %>">
<input type="hidden" name="AuthoOK" value="0">
<input type="hidden" name="SignOK" value="0">
<input type="hidden" name="validateFLD" value="0">
<input type="hidden" name="needSignature" value="<%out.print(txnIMGAutoriz);%>">
<input type="hidden" name="needAutho" value="<%out.print(txnAuthLevel);%>">
<input type="hidden" name="SignField" value="<%= session.getAttribute("page.SignField") %>">
<input type="hidden" name="override" value="N">
<input type="hidden" name="txtFees" value="0">
<input type="hidden" name="compania" value="20">
<input type="hidden" name="txtSelectedItem" value="0" >
<input type="hidden" name="returnform" value="N" >
<input type="hidden" name="returnform2" value="N" >
<input type="hidden" name="txtPlazaSuc" value="<%= session.getAttribute("plaza") %>" >
<input type="hidden" name="hdCreditoIni" value="">
<input type="hidden" name="specialCveTran" value="">


<input type="hidden" name="passw" value="<%=session.getAttribute("password")%>">
<input type="hidden" name="MontoRetiroPesos" value = "<%=session.getAttribute("Retiro.Pesos")%>">
<input type="hidden" name="MontoRetiroDolar" value = "<%=session.getAttribute("Retiro.Dolar")%>">
<input type="hidden" name="fecha4523" values="">

<input type="hidden" name="isChequeFormatoNuevo" value="N">
<input type="hidden" name="dvCuenta" value="">
<input type="hidden" name="dvCheque" value="">
<input type="hidden" name="dvRuta" value="">

<input type="hidden" name="noCheque" value="">
<input type="hidden" name="noCuenta" value="">

<p><script>
 if (document.entry.txtFechaEfect)
 {
  document.writeln("<input type=hidden name=txtFechaSys>");
  document.entry.txtFechaEfect.value=top.sysdate();
 }
 if (document.entry.txtFechaError)
 {
  document.writeln("<input type=hidden name=txtFechaSys>");
  document.entry.txtFechaError.value=top.sysdate();
 }
 if (document.entry.txtFechaCarta)
 {
  document.writeln("<input type=hidden name=txtFechaSys>");
  document.entry.txtFechaCarta.value=top.sysdate();
 }
</script>
	<style>
		.invisible
		{
		  display:None;
		}

		.visible
		{
		  display:"";
		}
	</style>


<% if(txnAuthLevel.equals("1") || txnIMGAutoriz.equals("1"))
    out.println("<a id='btnCont' tabindex=\"" + (i+1) + "\" href=\"javascript:document.entry.returnform.value='S';validate(document.entry);javascript:if(!isTxnWithoutValidateXMonto()){AuthoandSignature(document.entry);}\" ><img src=\"../ventanilla/imagenes/b_continuar.gif\" border=\"0\"></a>");
   else
    out.println("<a id='btnCont' tabindex=\"" + (i+1) + "\" href=\"javascript:document.entry.returnform.value='S';validate(document.entry)\"><img src=\"../ventanilla/imagenes/b_continuar.gif\" border=\"0\"></a>");
%>

</form>
</body>
</html>
