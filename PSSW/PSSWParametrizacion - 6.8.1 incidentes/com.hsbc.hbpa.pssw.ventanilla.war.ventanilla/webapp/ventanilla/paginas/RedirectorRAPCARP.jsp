<%
//*************************************************************************************************
///            Funcion: JSP para flujo del RAP
//            Elemento: RdirectorRAP.jsp
//          Creado por: Alejandro Gonzalez Castro
//      Modificado por: Israel de Paz Mercado
//*************************************************************************************************
// CCN - 4360211 - 08/10/2004 - Se elimina mensaje al log
// CCN - 4360455 - 0704/2005 - Se realizan modificaciones para el esquema de cheques devueltos.
// CCN - 4360462 - 10/04/2005 - Se genera nuevo paquete por errores en paquete anterior
//*************************************************************************************************
%>

<%@ page session="true" import="ventanilla.com.bital.util.NSTokenizer,java.util.Vector,java.util.Hashtable,
java.util.Enumeration,ventanilla.GenericClasses"%>
<%
response.setHeader("Cache-Control","no-store"); //HTTP 1.1
response.setHeader("Pragma","no-cache"); //HTTP 1.0
response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
%>
<%
Vector frap = (Vector)session.getAttribute("FRAP");
String txn = "";
Vector resp = (Vector)session.getAttribute("response");
String control = (String)resp.get(0);
Hashtable datasession = (Hashtable)session.getAttribute("page.datasession");
Hashtable info = (Hashtable)session.getAttribute("info");
if(datasession == null)
	datasession = new Hashtable();
	

String totalRAP = (String)datasession.get("tot");	
String montoCheque = request.getParameter("txtCheque");	
String chqdev = (String)datasession.get("chqDev");

if(datasession.get("txtEfectivo") != null && datasession.get("efectivoCSIB") == null){
datasession.put("efectivoCSIB",(String)datasession.get("txtEfectivo"));
}


if(chqdev != null)
{
  if (chqdev.equals("SI"))
     {
     if(montoCheque != null)
       {
       if(totalRAP.equals(montoCheque))
          datasession.put("chqDev","SI");
       else
          datasession.put("chqDev","NO");
       }
    }
}



if(frap.isEmpty())
{
	String txns = request.getParameter("txns");
	try
	{
		NSTokenizer txnstmp = new NSTokenizer(txns, "*");
		while( txnstmp.hasMoreTokens() )
		{
			String token = txnstmp.nextToken();
			if( token == null )
				continue;
            frap.addElement(token);
		}
	}
    catch(Exception e)
    {
    }
}

if(frap != null){
	if(frap.size()>0){
		txn = frap.firstElement().toString();
	}else{
		GenericClasses gc = new GenericClasses();
	  	String strEfectivo = datasession.get("efectivoCSIB")==null?"0.00":(String)datasession.get("efectivoCSIB");	  	
	 	long efectivo = Long.parseLong(gc.quitap(strEfectivo));
	 	String servicio = (String)info.get("Servicio1");
	 	//IDDEVA, Se agrega bandera para mostrar/ no mostrar formulario UAF
	 	String muestraForm = (String)datasession.get("formulariaUAF");
	 	if(null == muestraForm){
	 	   muestraForm = "NO";
	 	 }
	 	if( efectivo >= 1000000 
	 	   && ( servicio.equals("60") || servicio.equals("670") ||  muestraForm.trim().equals("SI")))
	 	{
	  		txn="CSIB";
	  	}
	}
}
/*  if(frap != null)
	  txn = frap.firstElement().toString();*/
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
  <title>Certificación</title>
<style type="text/css">
  Body {font-family: Arial, Helv; font-size: 9pt; font-weight: bold}
</style>

<script language="JavaScript">
<!--
  function redirect(){
    <%if(control.equals("0")){%>
    document.forms[0].submit();
    <%}%>
  }
//-->
</script>
</head>

<%if(txn.equals("5503"))
{%><body bgcolor="#FFFFFF" onload="top.setFieldFocus(window.document)"><%}
else{%><body bgcolor="#FFFFFF" onload="javascript:redirect();"><%}%>

<p>
<%
String proceso = "";
if(txn.equals("5359"))
{
   out.print("<form name=\"Txns\" action=\"../../servlet/ventanilla.PageServlet\" method=\"post\">");
    datasession.remove("MontoTotal");
%>
  <input type="hidden" name="transaction" value="5359">
  <input type="hidden" name="transaction1" value="5359">
<%
   out.print("</form>");
}
if(txn.equals("0604"))
{
   out.print("<form name=\"Txns\" action=\"../../servlet/ventanilla.PageServlet\" method=\"post\">");
%>
  <input type="hidden" name="transaction" value="0604">
  <input type="hidden" name="transaction1" value="0604">
<%
   out.print("</form>");
}
if(txn.equals("0510"))
{
   out.print("<form name=\"Txns\" action=\"../../servlet/ventanilla.PageServlet\" method=\"post\">");
%>
  <input type="hidden" name="transaction" value="0510">
  <input type="hidden" name="transaction1" value="0510">
<%
   out.print("</form>");
}
if(txn.equals("5503") && control.equals("0"))
{
	proceso = "PageBuilder5503CAPR.jsp";
	response.sendRedirect(proceso);
}
if(txn.equals("5353"))
{
   out.print("<form name=\"Txns\" action=\"../../servlet/ventanilla.PageServlet\" method=\"post\">");
%>
  <input type="hidden" name="transaction" value="5353">
  <input type="hidden" name="transaction1" value="5353">
<%
   out.print("</form>");
}

if(txn.equals("CSIB"))
{
   out.print("<form name=\"Txns\" action=\"../../servlet/ventanilla.PageServlet\" method=\"post\">");
%>
  <input type="hidden" name="transaction" value="CSIB">
  <input type="hidden" name="transaction1" value="5503">
<%
   out.print("</form>");
}

// Agregar campos de jsp PageBuilderRAP a datasession
String[] values;
Enumeration params = request.getParameterNames();

while( params.hasMoreElements() )
{
    String param = (String)params.nextElement();
    values = request.getParameterValues( param );
    for(int i=0; i<values.length; i++)
	    datasession.put(param, values[i]);
}

// Remove a variable de sesion para Flujo de Control RAP
// if(!(frap.isEmpty()))
if(frap.size() > 0){
	if(frap.firstElement().toString().equals("5503"))
	{
		frap.removeElementAt(0);
		//datasession.remove("flujorap");
	}
	else
	{// Remove a Primer elemento del Vector de Control de Flujo
		frap.removeElementAt(0);
	}
}
session.setAttribute("page.datasession", datasession);
%>
<p>
</body>
</html> 
