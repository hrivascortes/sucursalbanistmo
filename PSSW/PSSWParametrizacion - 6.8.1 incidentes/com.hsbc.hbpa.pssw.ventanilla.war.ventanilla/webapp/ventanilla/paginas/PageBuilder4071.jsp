<!--
//*************************************************************************************************
//             Funcion: JSP que presenta las opciones de la txn 4071
//            Elemento: PageBuilder4071.jsp
//          Creado por: Alejandro Gonzalez Castro
//      Modificado por: Fausto Rodrigo Flores Moreno
//*************************************************************************************************
// CCN - 4360275 - 18/02/2005 - WAS 5.1
// CCN - 4360334 - 17/06/2005 - Se convierte fieldLector a fragmento
// CCN - 4360522 - 06/10/2006 - Se hacen modificaciones para eliminar la opcion de Otros Bancos
// CCN - 4360581 - 12/03/2007 - Se convierten los estilos en jsf y se hace un include en el jsp
//*************************************************************************************************
-->

<%@ page session="true" import="java.util.*"%>
<%
response.setHeader("Cache-Control","no-store"); //HTTP 1.1
response.setHeader("Pragma","no-cache"); //HTTP 1.0
response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<%
Vector listaCampos = (Vector)session.getAttribute("page.listcampos");
Vector listaContenidos = (Vector)session.getAttribute("page.listcont");
String txnAuthLevel = (String)session.getAttribute("page.txnAuthLevel");
String txnIMGAutoriz = (String)session.getAttribute("page.txnIMGAutoriz");
Hashtable datassesion = (Hashtable)session.getAttribute("page.datasession");
String MontoPro = new String("");
int k;
int vlong = listaCampos.size();

  vlong = listaCampos.size() - 3;

vlong = 1;
%>

<html>
<head>
  <%@include file="style.jsf"%>
  <title>Datos de la Transaccion</title>
<script language="JavaScript">
<!--
var formName = 'Mio';

function validate(form)
{
<%
  for(int i=0; i < vlong; i++)
  {
    Vector vCampos = (Vector)listaCampos.get(i);
    vCampos = (Vector)vCampos.get(0);
    
    if( vCampos.get(3).toString().length() > 0 )
    {
      out.println("  if( !top.validate(window, document.entry." + vCampos.get(0) + ", '" + vCampos.get(4) +"') )");
      out.println("    return; ");
    }
  }

%>
 
}
function AuthoandSignature(entry)
{
  var Cuenta;
 
  if(entry.needSignature.value == '1' && entry.SignField.value != '')
  {
    Cuenta = eval('entry.' + entry.SignField.value + '.value');
    top.openModalDialog('<%=request.getContextPath()%>/servlet/ventanilla.FirmasServlet?txtDDACuenta=' + Cuenta, 710, 430, 'top.setPrefs2()', entry);
  }
}

//-->
</script>
</head>
<body onload="top.setFieldFocus(window.document)">
<br>
<%
out.print("<form name=\"entry\" action=\"../servlet/ventanilla.DataServlet\" method=\"post\" ");

if(txnAuthLevel.equals("1"))
  out.print("onSubmit=\"return validate(this)\"");
  
out.println(">");
%>

<h1><%=session.getAttribute("page.txnImage")%></h1>

<h3>

<%
//if(lstBanLiq.trim().equals("01"))
  out.print(session.getAttribute("page.cTxn"));
//else
//  out.print("4073");
  
out.print("  " + (String)session.getAttribute("page.txnlabel"));
%>

</h3>
<table border="0" cellspacing="0">
<tr>
 <td>Monto:</td>
 <td width="10">&nbsp;</td>
 <td><%=datassesion.get("txtMonto")%></td>
</tr>

<%
  int i = 0;
  for(i =  0; i < vlong; i++)
  {
    if(i == 1)
    {
      out.print("<tr>");
      out.print(" <td>");
      out.print("  <i><b>Beneficiario</b></i>");
      out.print(" </td>");
      out.print("</tr>");
    }
    
    Vector vCampos = (Vector)listaCampos.get(i);
    vCampos = (Vector)vCampos.get(0);
    if(!vCampos.get(0).toString().equals("txtCodSeg"))
    {
      out.println("<tr>");
      out.println("  <td>" + vCampos.get(2) + ":</td>");
      out.println("  <td width=\"10\">&nbsp;</td>");
      
      if(!vCampos.get(0).toString().substring(0,3).equals("lst"))
      {
        out.print ("  <td><input maxlength=\"" + vCampos.get(3) + "\" name=\"" + vCampos.get(0) + "\"" + 
                   " size=\"" + vCampos.get(3) +"\" type=\"text\" tabindex=\"" + (i + 1) + "\"");
        String CampoR = (String)vCampos.get(5);
        
        if(CampoR != null && CampoR.length() > 0)
        {
          CampoR = CampoR.trim();
          out.print(" value=\"" + CampoR + "\"");
        }
      } else {
        out.print("  <td><select name=\"" + vCampos.get(0) + "\" size=\"" + vCampos.get(2) + "\" " + 
                  "tabindex=\"" + (i + 1) + "\"");
        
        if( vCampos.get(3).toString().length() > 0 )
          out.println(" onChange=\"top.validate(window, this, '" + vCampos.get(4) +"')\">");
        else
          out.println(">");
          
        for(k=0; k < listaContenidos.size(); k++)
        {
          Vector v1Campos = (Vector)listaContenidos.get(k);
          
          for(int j = 0; j < v1Campos.size(); j++)
          {
            Vector v1Camposa = (Vector)v1Campos.get(j);
            
            if(v1Camposa.get(0).toString().equals(vCampos.get(0)))
              out.println("  <option value=\"" + v1Camposa.get(3) + "\">" + v1Camposa.get(2));
          }
        }
        
        out.println("  </select></td>");
      }
      
      if( vCampos.get(3).toString().length() > 0 && !vCampos.get(0).toString().substring(0,3).equals("lst"))
//        out.println(" onChange=\"top.validate(window, this, '" + vCampos.get(4) +"')\"></td>");
        out.println(" onBlur=\"top.estaVacio(this)||top.validate(window, this, '" + vCampos.get(4) +"')\" " + 
                    " onKeyPress=\"top.keyPressedHandler(window, this, '" + vCampos.get(4) + "')\" ></td>");
      else if(!vCampos.get(0).toString().substring(0,3).equals("lst"))
        out.println(">");
        
      out.println("</tr>");
    } else {
      %><%@include file="fieldLector.jsf"%><%
      i += 3 ;
    }
  }
%></table>
<input type="hidden" name="iTxn" value="<%= session.getAttribute("page.iTxn") %>">
<input type="hidden" name="oTxn" value="<%= session.getAttribute("page.oTxn") %>">
<input type="hidden" name="cTxn" value="<%= session.getAttribute("page.cTxn") %>">
<input type="hidden" name="sucursal" value="<%= session.getAttribute("branch") %>">
<input type="hidden" name="teller" value="<%= session.getAttribute("teller") %>">
<input type="hidden" name="moneda" value="<%= session.getAttribute("page.moneda")%>">
<input type="hidden" name="registro" value="<%=session.getAttribute("empno")%>">
<input type="hidden" name="tllrlvl" value="<%=session.getAttribute("tellerlevel")%>">
<input type="hidden" name="supervisor" value="<%= session.getAttribute("teller") %>">
<input type="hidden" name="AuthoOK" value="0">
<input type="hidden" name="SignOK" value="0">
<input type="hidden" name="compania" value="20">
<input type="hidden" name="validateFLD" value="0">
<input type="hidden" name="needSignature" value="1">
<input type="hidden" name="needAutho" value="<%out.print(txnIMGAutoriz);%>">
<input type="hidden" name="SignField" value="txtDDACuenta">
<p>
<% 
  out.println("<a tabindex=\"" + (i + 1) + "\" href=\"javascript:validate(document.entry);javascript:AuthoandSignature(document.entry)\" ><img src=\"../ventanilla/imagenes/b_aceptar.gif\" border=\"0\"></a>");
%>
</form>
</body>
</html>

<!-- Termina PageBuilder4071.jsp -->
