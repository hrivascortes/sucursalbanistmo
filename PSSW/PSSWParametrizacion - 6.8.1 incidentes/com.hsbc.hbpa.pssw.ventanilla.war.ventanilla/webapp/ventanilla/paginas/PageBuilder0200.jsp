<!--
//*************************************************************************************************
//             Funcion: JSP que despliegaTxn 0200
//            Elemento: PageBuilder0200.jsp
//          Creado por: Alejandro Gonzalez Castro
//      Modificado por: Alejandro Gonzalez Castro
//*************************************************************************************************
// CCN - 4360178 - 20/08/2004 - Se modifica llamado de constructor de clase balance
// CCN - 4360189 - 03/09/2004 - Se cambia invoke a clase Balance
// CCN - 4360275 - 18/02/2005 - WAS 5.1
// CCN - 4360581 - 12/03/2007 - Se convierten los estilos en jsf y se hace un include en el jsp
//*************************************************************************************************
-->

<%@ page session="true" import="ventanilla.com.bital.admin.Balance,java.util.*"%>
<%
response.setHeader("Cache-Control","no-store"); //HTTP 1.1
response.setHeader("Pragma","no-cache"); //HTTP 1.0
response.setDateHeader ("Expires", 0); //prevents caching at the proxy server*/
%>
  
<%!
   private String addpunto(String valor)
   {
      int longini = valor.length();
      if (longini == 1)
      {
         valor = "0.0" + valor; 
         return valor;
      }      
      String cents  = valor.substring(longini-2,longini); 
      String entero = valor.substring(0,longini-2);
      int longente = entero.length();
      for (int i = 0; i < (longente-(1+i))/3; i++)
      {
         longente = entero.length();
         entero = entero.substring(0,longente-(4*i+3))+','+entero.substring(longente-(4*i+3));
      }
      entero = entero + '.' + cents;
      return entero;
   }
	 
   private String addhora(String valor)
	{
      String hora = valor.substring(1,3) + ":" + valor.substring(3,5) + ":" + valor.substring(5);
      return hora;
	}
%>

<%
Vector lcs = (Vector)session.getAttribute("page.listcampos");
Vector lct = (Vector)session.getAttribute("page.listcont");
String txnAuthLevel = (String)session.getAttribute("page.txnAuthLevel");
String txnIMGAutoriz = (String)session.getAttribute("page.txnIMGAutoriz");
Vector v = (Vector)session.getAttribute("page.specialacct");
%>
<html>
<head>
  <%@include file="style.jsf"%>
  <title>Datos de la Transaccion</title>
<script language="JavaScript">
<!--
var formName = 'Mio'
var specialacct = new Array()
function validate(form)
{
}
//-->
</script>
</head>
<body>
<br>
<form name="balance" action="PageBuilder0200.jsp" method="post">
<h1><%=session.getAttribute("page.txnImage")%></h1>
<h3>
<%out.print(session.getAttribute("page.cTxn") + "  " +
                (String)session.getAttribute("page.txnlabel"));%>
</h3>
<table border="1" >
<tr>
	<td align="center"><Strong>Cajero </strong></td>
	<td align="center"><Strong>Txn 0201 Aumentos</strong></td>
	<td align="center"><Strong>Txn 0202 Disminuciones</strong></td>
	<td align="center"><Strong>Hora </strong></td>
</tr>
<%
	Balance balance = new Balance(session);
     balance.ExecuteSql();

	String txn = "";
	long numtxn0201 = 0;
	long numtxn0202 = 0;
	if (balance.getStatus() != 0 )
	{%>
		<P>Error de Conexion de la Base de Datos ...</p>
<%	}
	else
	{
		Hashtable campos = balance.getcampos();
		Hashtable names = balance.getnames();
		for (int i=1; i<balance.getfila(); i++)
		{%>
			<tr>		
<%			for (int j=1; j<=balance.getcolumn(); j++)
			{
				String puntero = "f" + i + "c" +j;
				String campo = (String)campos.get(puntero);
				String name = (String)names.get(puntero);
				if (name.equals("D_TXN"))
				{txn = campo;}
				else if(name.equals("D_EFECTIVO"))
				{		if (txn.equals("0201"))
						{
							long suma = Long.parseLong(campo);
							numtxn0201 = numtxn0201 + suma;
						%>	
						<td align="right"><%=addpunto(campo)%></td>
						<td align="right">0.00</td>
<%						}else{
							long suma = Long.parseLong(campo);
							numtxn0202 = numtxn0202 + suma;
%>
						<td align="right">0.00</td>
						<td align="right"><%=addpunto(campo)%></td>
<%						}
				}
				else if(name.equals("D_CONSECUTIVO"))
				{%>		<td><%=addhora(campo)%></td><%}
				else
				{%>		<td><%=campo%></td><%}
			}%>
			</tr>
<%		}
	}%>
		<tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>									
		</tr>
		<tr>
			<td><Strong>Totales</strong></td>
			<%if (numtxn0201 > 0)
			{%>
			<td align="right"><Strong><%=addpunto(Long.toString(numtxn0201))%></strong></td>
			<%}else{%>
			<td align="right"><Strong>0.00</strong></td>
			<%}			
			if (numtxn0202 > 0)
			{%>
			<td align="right"><Strong><%=addpunto(Long.toString(numtxn0202))%></strong></td>
			<%}else{%>
			<td align="right"><Strong>0.00</strong></td>
			<%}%>			
			<td><Strong>&nbsp;</strong></td>			
		</tr>
</table>
<input type="hidden" name="iTxn" value="<%= session.getAttribute("page.iTxn") %>">
<input type="hidden" name="oTxn" value="<%= session.getAttribute("page.oTxn") %>">
<input type="hidden" name="cTxn" value="<%= session.getAttribute("page.cTxn") %>">
<input type="hidden" name="sucursal" value="<%= session.getAttribute("branch") %>">
<input type="hidden" name="teller" value="<%= session.getAttribute("teller") %>">
<input type="hidden" name="moneda" value="<%= session.getAttribute("page.moneda")%>">
<input type="hidden" name="registro" value="<%=session.getAttribute("empno")%>">
<input type="hidden" name="tllrlvl" value="<%=session.getAttribute("tellerlevel")%>">
<input type="hidden" name="supervisor" value="<%= session.getAttribute("teller") %>">
<input type="hidden" name="AuthoOK" value="0">
<input type="hidden" name="SignOK" value="0">
<input type="hidden" name="validateFLD" value="0">
<input type="hidden" name="needSignature" value="<%out.print(txnIMGAutoriz);%>">
<input type="hidden" name="needAutho" value="<%out.print(txnAuthLevel);%>">
<input type="hidden" name="SignField" value="<%= session.getAttribute("page.SignField") %>">
<p>
</form>
<script>
	<%if (numtxn0201 != numtxn0202)
	{%>	alert("Transacciones NO Saldadas");<%}
	else
	{%> alert("Transacciones Saldadas");<%}%>
</script>
</body>
</html>
