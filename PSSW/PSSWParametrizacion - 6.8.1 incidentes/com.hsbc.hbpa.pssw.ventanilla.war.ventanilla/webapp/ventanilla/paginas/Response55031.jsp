<%
//*************************************************************************************************
///            Funcion: JSP que despliega respuesta de txn RAP
//            Elemento: Response55031.jsp
//          Creado por: Alejandro Gonzalez Castro
//      Modificado por: Fausto Rodrigo Flores Moreno
//*************************************************************************************************
// CCN - 4360175 - 03/08/2004 - Se agregan nombres de beneficiarios visibles / ajusta size ventana
//                              impresion
// CCN - 4360274 - 16/02/2005 - Error de impresi�n cuado el domicilio de la sucursal tiene car�cter 
//                              de #
// CCN - 4360455 - 07/04/2006 - Se realizan modificaciones para el esquema de cheques devueltos.
// CCN - 4360462 - 10/04/2006 - Se genera nuevo paquete por errores en paquete anterior
// CCN - 4360474 - 05/05/2006 - Se realizan modificaciones para mostrar mensaje de espera cuando se procesan devoluciones de cheques
// CCN - 4360581 - 12/03/2007 - Se convierten los estilos en jsf y se hace un include en el jsp
// CCN - 4360585 - 23/03/2007 - Se realizan modificaciones para RAP Calculadora
//*************************************************************************************************
 %>

<%@ page session="true" import="java.util.*, ventanilla.GenericClasses"%>
<%
response.setHeader("Cache-Control","no-store"); //HTTP 1.1
response.setHeader("Pragma","no-cache"); //HTTP 1.0
response.setDateHeader ("Expires", 0); //prevents caching at the proxy server

%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
<title>Results</title>
<%@include file="specialstyle.jsf"%>
</head>
<%!
  private String quitaespacios(String s)
  {
      String s1 = "";
      s1 = s;
      s = "";
      int len = s1.length();
      for ( int i = 0; i < len; i++ )
	  {
        if ( s1.charAt(i) != ' ')
		{
           s = s + s1.charAt(i);
        }
        else
           s = s + "%20";
      }
      return s;
   }
%>
<body>
<p>Proceso de RAP Terminado...<p>
<form name="ImprimeDiario">
<%
	Hashtable data = (Hashtable)session.getAttribute("page.datasession");
	String beneficiario1 = (String)data.get("beneficiario1");
	if(beneficiario1 == null)
		beneficiario1 = "";
	String beneficiario2 = (String)data.get("beneficiario2");
	if(beneficiario2 == null)
		beneficiario2 = "";
	String beneficiario3 = (String)data.get("beneficiario3");
	if(beneficiario3 == null)
		beneficiario3 = "";
	String beneficiario4 = (String)data.get("beneficiario4");
	if(beneficiario4 == null)
		beneficiario4 = "";

	String compago1 = (String)data.get("comppago1");
	if(compago1 == null)
		compago1 = "";
	String compago2 = (String)data.get("comppago2");
	if(compago2 == null)
		compago2 = "";
	String compago3 = (String)data.get("comppago3");
	if(compago3 == null)
		compago3 = "";
	String compago4 = (String)data.get("comppago4");
	if(compago4 == null)
		compago4 = "";
	
	String RapCal = (String)data.get("RAPCalculadora");
	String AplicaDesc = (String)data.get("no_descuento");
	String DescRec = (String) data.get("Desc_Recargo");
	String Recargo = "";
	String mtoRecargo = "";
	String Descuento = "";
	String mtoDescuento = "";
	String mtoPagar = "";
	String totalpagar = "";
	
if(data.get("txtEfectivo") != null && data.get("efectivoCSIB") == null){
data.put("efectivoCSIB",(String)data.get("txtEfectivo"));

}
	
	GenericClasses gc = new GenericClasses();
	if(RapCal != null){
		if(RapCal.equals("S")){
			if(AplicaDesc.equals("SI")){
	        	if(DescRec.equals("D")){
		        	Descuento = "D";
	        		mtoDescuento = gc.quitap((String)data.get("Monto_D_R"));
	        		totalpagar = "T";
	        		mtoPagar = gc.quitap((String)data.get("MontoPagar"));
		   		}else{
		   			if(DescRec.equals("R")){
		        		Recargo = "R";
		        		mtoRecargo = gc.quitap((String)data.get("Monto_D_R"));
		        		totalpagar = "T";
		        		mtoPagar = (String)data.get("MontoPagar");
			   		}
			   	}
	         }
	     }
	}

    String txtCadRapCalculadora = Recargo + "~" + mtoRecargo + "~" + Descuento + "~" + mtoDescuento + "~" + totalpagar + "~" + mtoPagar + "~";
    
// variable de control para fin de flujorap
    data.put("finrap", "1");
    data.put("finrapcapr", "1");
    
    
String NumServ = (String) data.get("s1");
  	String strEfectivo = (String)data.get("efectivoCSIB")==null?"0.00":(String)data.get("efectivoCSIB");	  	
 	long efectivo = Long.parseLong(gc.quitap(strEfectivo));
 	//IDDEVA-Se agrego bandera de Habitualidas
 	String sHAB = (String)data.get("HAB"); 	
 	
 	//IDDEVA - Daily Cash
 	//if(efectivo >= 1000000 && (NumServ.equals("60") || NumServ.equals("670"))){	
 	if( (efectivo >= 1000000) && (NumServ.equals("60") || NumServ.equals("670") ||  0 != sHAB.trim().compareTo("XHAB"))){	
 		data.put("finrap", "2");
 		data.put("finrapcapr", "2");
 	}


    String CI = "";
    if(data.get("CIrap") != null)
    {
        CI = (String)data.get("CIrap");
        if(CI.equals("1")){
            data.put("finrap", "2");
            data.put("finrapcapr", "2");
         }
    }

    String REM = "";
    if(data.get("REMrap") != null)
    {
        REM = (String)data.get("REMrap");
        if(REM.equals("1")){
            data.put("finrap", "2");
            data.put("finrapcapr", "2");
         }
    }

	if(data.get("codigo").equals("1")){
		data.put("finrap", "1");
		data.put("finrapcapr", "1");	
	}

    String ChqDev = (String)data.get("chqDev");
    
    if (ChqDev != null)
    {
    if (ChqDev.equals("SI") && (session.getAttribute("ChequesDevueltos") != null))
       {
        if (session.getAttribute("codigo5503").equals("0"))
        {
          Stack flujotxn = (Stack)session.getAttribute("page.flujotxn");
           if (flujotxn == null || flujotxn.empty())
              {
              flujotxn.push("5361");
              Hashtable chequesdev = (Hashtable)session.getAttribute("ChequesDevueltos");
              session.setAttribute("page.flujotxn", flujotxn);
              session.setAttribute("txn5361","5361");
              session.setAttribute("totalChqDev",String.valueOf(chequesdev.size()));
              session.setAttribute("conteoChqDev","1");
              }  
          session.removeAttribute("codigo5503");    
        }      
       }
    }   

    session.setAttribute("page.datasession", data);
    String rap = "RAP~" + quitaespacios(beneficiario1) + "~" + quitaespacios(beneficiario2) + "~" + quitaespacios(beneficiario3) + "~" + quitaespacios(beneficiario4) + "~~" + compago1 + "~" + compago2 + "~" + compago3 + "~" + compago4 + "~" + txtCadRapCalculadora;
%>
<table>
<%
if(beneficiario1.length() > 0)
{%>
    <tr class="normaltextredbold"><td>Beneficiario 1 :</td><td>&nbsp;&nbsp;</td><td><%=beneficiario1%></td></tr>
<%
}
if(beneficiario2.length() > 0)
{%>
    <tr class="normaltextredbold"><td>Beneficiario 2 :</td><td>&nbsp;&nbsp;</td><td><%=beneficiario2%></td></tr>
<%
}
if(beneficiario3.length() > 0)
{%>
    <tr class="normaltextredbold"><td>Beneficiario 3 :</td><td>&nbsp;&nbsp;</td><td><%=beneficiario3%></td></tr>
<%
}
if(beneficiario4.length() > 0)
{%>
    <tr class="normaltextredbold"><td>Beneficiario 4 :</td><td>&nbsp;&nbsp;</td><td><%=beneficiario4%></td></tr>
<%
}
%>
</table>
</form>
<script>
    top.openDialog('../../servlet/ventanilla.ImprimirServlet?txtCadImpresion=<%=rap%>', 160, 120, 'top.setPrefs4()', document.Txns);
</script>
</body>
</html>
