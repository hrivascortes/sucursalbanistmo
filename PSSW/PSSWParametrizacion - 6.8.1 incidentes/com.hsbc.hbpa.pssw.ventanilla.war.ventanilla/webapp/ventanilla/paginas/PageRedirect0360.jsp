<!--
//*************************************************************************************************
//             Funcion: JSP para redireccion de txn 0360
//            Elemento: PageRedirect0360.jsp
//      Modificado por: Juvenal R. Fernandez V.
//*************************************************************************************************
// CCN - 4360334 - 17/06/2005 - Se modifica redireccion a relativa para WAS5
//*************************************************************************************************
-->

<%@page session="true" import="java.util.Hashtable"%>
<%
response.setHeader("Cache-Control","no-store"); //HTTP 1.1
response.setHeader("Pragma","no-cache"); //HTTP 1.0
response.setDateHeader ("Expires", 0); //prevents caching at the proxy server*/
%>
<%
  String proceso = (String)session.getAttribute("txnProcess");
  Hashtable datasession = (Hashtable)session.getAttribute("page.datasession");  
  String iTxn = (String)session.getAttribute("page.iTxn");
  if( proceso.equals("13") )
  {
		if (iTxn.equals("0732"))
		{response.sendRedirect("PageBuilder0732.jsp");}
		else
	    {response.sendRedirect("PageBuilder0360.jsp");}
  }
%>
