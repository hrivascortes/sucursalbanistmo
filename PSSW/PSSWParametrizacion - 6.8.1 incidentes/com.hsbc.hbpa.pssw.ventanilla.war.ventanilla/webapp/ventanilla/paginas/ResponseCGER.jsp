<!--
//*************************************************************************************************
//             Funcion: JSP que detalle del cheque a imprimir
//            Elemento: ResponseCGER.jsp
//          Creado por: YGX
//*************************************************************************************************
-->

<%@ page session="true" import="java.util.*"%>
<%
response.setHeader("Cache-Control","no-store"); //HTTP 1.1
response.setHeader("Pragma","no-cache"); //HTTP 1.0
response.setDateHeader ("Expires", 0); //prevents caching at the proxy server

 Hashtable datasession = (Hashtable)session.getAttribute("page.datasession");

%>
<%!
  String c = "";
  String c_imp="";
  String c_ger = "";

  
  private String formatMonto(String s)
	{
		int len = s.length();
 		if (len < 3)
            if(len < 2) {
                s = "0.0" + s;
            } else {
                s = "0." + s;
            }
    	else
    	{
      	int n = (len - 3 ) / 3;
    		s = s.substring(0, len - 2) + "." + s.substring(len - 2 , len);
      	for (int i = 0; i < n; i++)
      	{
      		len = s.length();
    			s = s.substring(0, (len -(i*3+6+i))) + "," + s.substring((len -(i*3+6+i)) , len);
    		}
    	}
      for (int i = s.length(); i < 16 ; i++)
    		s = " " + s;
    	return s;
  	}

  private String delCeros(String s)
  {
    int len = 0;
    int space = 0;
    for(len = 0 ; len < s.length(); len++)
	  if(s.charAt(len) == '0')
	   space ++;
	  else
	   break;
    return s.substring(space, s.length());
  }
  
	private String formatFecha(String s)
	{
		String fechaAux;
		
		fechaAux = "/" + s.substring(0,4);
		fechaAux = "/" + s.substring(4,6) + fechaAux;
		fechaAux = s.substring(6,8) + fechaAux;	    		
		
    	return fechaAux;
  	}  

%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<%
	
   String datos  = (String)request.getParameter("val_datos");		
   String llave  = (String)datasession.get("val_datos");
   String datatmp="";
   String datatmp1="";
   String datatmp2="";
   String datatmp3="";
   String datatmp4="";
   String datatmp5="";
   String datatmp6="";
   String datatmp7="";  
   String datatmp8=""; 
   String datatmp9=""; 
   
   session.setAttribute("page.getoTxnView","Response");
%>

<html>
<script language="javascript" >
function validate(form)
{


   if  (!isnumchec()) 
   {
        alert("Debe indicar el serial del cheque de gerencia. Verifique...");
        return;
   }
   
  if (!isSize())
  
  {
  
    alert("La longitud debe ser igual a 7 digitos. Verifique...");
    return
  }
	
   else  
   
  
 form.submit();  
}

// Funci�n que valida que el argumento sea un n�mero de cheque v�lido
function isnumchec()
{
   
    var numchec = document.entry.txtSerial.value;
   // alert(numchec);
    
    if (numchec == "")
    {
		return false;
	}
		
    if ( isInteger(document.entry.txtSerial.value) == false)
    {
    	
    	return false;
    }		
	
	
    return true;
}

function isSize(inputVal)
{
	var numchec = document.entry.txtSerial.value;
	
	if(numchec.length==7)
	{
		return true ;
	}
	else
	return false ;
}


function isInteger(inputVal)
{
	var inputStr = inputVal.toString() ;
	for (var i = 0; i < inputStr.length; i++)
	{
		var oneChar = inputStr.charAt(i) ;
		if (oneChar < "0" || oneChar > "9")
			return false ;
	}
	return true ;
}

function cancelar(form)
{
  document.entry.action = "../paginas/Final.jsp";
  document.entry.submit();
}

function refill()
{

var numchec = document.entry.txtSerial.value;
var maxDigits = 7 ;

if  (!isnumchec()) 
   {
	    document.entry.txtSerial.value="";
        alert("Debe indicar el serial del cheque de gerencia. Verifique...");
        return;
   }

while( numchec.length < maxDigits )
		numchec = '0' + numchec ;
	document.entry.txtSerial.value = numchec ;
}

</script>

<head>
		
	<%@include file="style.jsf"%>
	<style type="text/css">Body {font-family: Arial, Helv; font-size: 10pt; font-weight: bold; color:#000000}</style>

</head>

<body bgcolor="#FFFFFF" onload="top.setFieldFocus(window.document)">     
<form name="entry" action="../../servlet/ventanilla.DataServlet" method="post">      
<h1><%=session.getAttribute("page.txnImage")%></h1>
<h3>
<%out.print(session.getAttribute("page.cTxn") + "  " +
                (String)session.getAttribute("page.txnlabel"));%>
</h3>
		<table >
	<tr>
			<td></td>
			<td colspan="8"></td>
		</tr>
		
		<tr>
			<td>Serial :</td>
			<td colspan="8"><input type="text" name="txtSerial" size="7" maxlength="7" onBlur="javascript:refill()" ></td>
		</tr>

<%		
   
   StringTokenizer tok = new StringTokenizer(llave,"~");             
   while(tok.hasMoreTokens()) 
   { 
   
     datatmp = tok.nextToken(); 
%>
	
		<tr>
			<td>Beneficiario :</td>
			<td colspan="8"><% out.print(datatmp);%></td>
			<input type="hidden" name="txtBeneficiario" value="<%out.print(datatmp);%>">
			
		
		</tr>
<%
  datatmp1 = tok.nextToken();
  %>
  <input type="hidden" name="txtMonto" value="<%=datatmp1%>">
<%
  datatmp1 = formatMonto(delCeros(datatmp1));
%>
		
		<tr>
			<td>Monto :</td>
			<td colspan="8"><%=datatmp1%></td>
			<input type="hidden" name="txtMonto" value="<%=datatmp1%>">
			
		</tr>
		

		<tr>
			<td><b>Ingrese los siguientes</b></td>
			<td colspan="8"><b> datos (opcional) :</b></td>			
		</tr>
<%
  datatmp2 = tok.nextToken();
  datatmp3 = tok.nextToken();
%>
		<tr>
			<td>Detalle Pago :</td>
			<td colspan="8" border=1><%=datatmp3%></td>
			<input type="hidden" name="txtFchSol" value="<%=datatmp2%>">
			<input type="hidden" name="txtDetalle" value="<%=datatmp3%>">
			
		</tr>
<%
  datatmp4 = tok.nextToken();
%>
		<tr>
			<td>Direccion Beneficiario :</td>
			<td colspan="8" border=1><%=datatmp4%></td>
			<input type="hidden" name="adress" value="<%=datatmp4%>">
			
		</tr>
<%
  datatmp5 = tok.nextToken();
%>		
		<tr>
			<td>Persona Autorizada :</td>
			<td colspan="8"><%=datatmp5%></td>
			<input type="hidden" name="txtPAutorizado" value="<%=datatmp5%>">
			
		</tr>
<%
  datatmp6 = tok.nextToken();
 
%>
		<tr>
			<td>Cedula Autorizacion :</td>
			<td colspan="8"><%=datatmp6%></td>
			<input type="hidden" name="txtCedula" value="<%=datatmp6%>">
			
		</tr>

<%

  datatmp7 = tok.nextToken();
  datatmp8 = tok.nextToken();
  datatmp9 = tok.nextToken();
}

%>
<input type="hidden" name="txtConureg" value="<%=datatmp7%>">
<input type="hidden" name="txtSucDestino" value="<%=datatmp8%>">
<input type="hidden" name="txtCuenta" value="<%=datatmp9%>">

<tr>
   <td>
      <br><center><a href="javascript:cancelar(document.entry)"><img src="../imagenes/b_cancelar.gif" border="0"></a></center>
   </td>
   <td>
      <br><center><a href="javascript:validate(document.entry)"><img src="../imagenes/b_continuar.gif" border="0"></a></center>	
   </td>
   
</tr>

</table>
   
   
    <input type="hidden" name="iTxn" value="0136">
    <input type="hidden" name="oTxn" value="0136">
    <input type="hidden" name="cTxn" value="0136">
   
 
	
</form>
</body>
</html>
