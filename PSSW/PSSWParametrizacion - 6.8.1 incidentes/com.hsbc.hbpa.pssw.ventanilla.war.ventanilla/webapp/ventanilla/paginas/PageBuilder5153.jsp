<!--
//*************************************************************************************************
//             Funcion: JSP que presenta las opciones de la txn 5153
//            Elemento: PageBuilder5153.jsp
//          Creado por: Alejandro Gonzalez Castro
//      Modificado por: Marco Lara Palacios
//*************************************************************************************************
// CCN - 4360275 - 18/02/2005 - WAS 5.1
// CCN - 4360581 - 12/03/2007 - Se convierten los estilos en jsf y se hace un include en el jsp
//*************************************************************************************************
-->

<%@ page session="true" import="java.util.*"%>
<%
response.setHeader("Cache-Control","no-store"); //HTTP 1.1
response.setHeader("Pragma","no-cache"); //HTTP 1.0
response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<%
String MontoPro = new String("");
Vector listaCampos = (Vector)session.getAttribute("page.listcampos");
Vector listaContenidos = (Vector)session.getAttribute("page.listcont");
String txnAuthLevel = (String)session.getAttribute("page.txnAuthLevel");
String txnIMGAutoriz = (String)session.getAttribute("page.txnIMGAutoriz");
Vector v = (Vector)session.getAttribute("page.specialacct");
int i=0;
%>
<html>

<head>
<%@include file="style.jsf"%>
<title>Datos de la Transaccion</title>
<script type="text/javascript" language="JavaScript">
<!--
var formName = 'Mio'
var specialacct = new Array()
<%
 if(!v.isEmpty()){
  for(int iv = 0; iv < v.size(); iv++){
   out.print("specialacct[" + iv + "]='" + v.get(iv) + "'" + '\n');
  }
 }
%>
function validate(form)
{
<%
      out.println("if( !top.verifynom(window))");
      out.println("    return");
      out.println("if( !top.cadenas(window))");
      out.println("    return");
%>
  form.validateFLD.value = '1'
  if (document.entry.txtFechaEfect || document.entry.txtFechaError || document.entry.txtFechaCarta)
  document.entry.txtFechaSys.value=top.sysdatef();
<% if(!txnAuthLevel.equals("1") && !txnIMGAutoriz.equals("1"))
	{
    out.println("  form.submit();");
    out.println("var obj = document.getElementById('btnCont');"); 
    out.println("obj.onclick = new Function('return false');");   
	}   
%>
}
function AuthoandSignature(entry)
{
 var Cuenta;
 if(entry.validateFLD.value != '1')
  return
 if(entry.needAutho.value == '1' && entry.AuthoOK.value != '1'){
  top.openDialog('../ventanilla/paginas/authoriz.jsp', 220, 130, 'top.setPrefs()', entry)
 }
 if(entry.needSignature.value == '1' && entry.SignField.value != ''){
  Cuenta = eval('entry.' + entry.SignField.value + '.value')
  top.openModalDialog('<%=request.getContextPath()%>/servlet/ventanilla.FirmasServlet?txtDDACuenta=' + Cuenta, 710, 430, 'top.setPrefs2()', entry)
 }
}
//-->
</script>

</head>
<body onload="top.setFieldFocus(window.document)">
<br>
<% out.print("<form name=\"entry\" action=\"../servlet/ventanilla.DataServlet\" method=\"post\" ");
   if(txnAuthLevel.equals("1"))
    out.print("onSubmit=\"return validate(this)\"");
   out.println(">");
%>

<h1><%=session.getAttribute("page.txnImage")%></h1>

<h3>
<%out.print(session.getAttribute("page.cTxn") + "  " +
                (String)session.getAttribute("page.txnlabel"));%>
</h3>

<table border="0" cellspacing="0">
<tr>
<td rowspan="3" valign=top><b>Cargo :</b></td>
<td colspan="4">&nbsp;</td>
</tr>
<tr>
<%
  Vector vCampos = (Vector)listaCampos.get(0);
  vCampos = (Vector)vCampos.get(0);
  out.println("  <td>" + vCampos.get(2) + ":</td>");
  out.println("  <td width=\"10\">&nbsp;</td>");
  out.print  ("  <td><input maxlength=\"" + vCampos.get(3) + "\" name=\"" + vCampos.get(0) + "\" size=\"" + vCampos.get(3) +"\" tabindex=\"" + (++i) + "\" type=\"text\"" +
              " onBlur=\"top.estaVacio(this)||top.validate(window, this, '" + vCampos.get(4) +"')\" onKeyPress=\"top.keyPressedHandler(window, this, '" + vCampos.get(4) +"')\"></td>");
  vCampos = (Vector)listaCampos.get(1);
  vCampos = (Vector)vCampos.get(0);
  out.println("  <td width=\"20\">&nbsp;</td>");
  out.println("  <td>" + vCampos.get(2) + ":</td>");
  out.println("  <td width=\"10\">&nbsp;</td>");
  out.print  ("  <td><input maxlength=\"" + vCampos.get(3) + "\" name=\"" + vCampos.get(0) + "\" size=\"" + vCampos.get(3) +"\" tabindex=\"" + (++i) + "\" type=\"text\"" +
              " onBlur=\"top.estaVacio(this)||top.validate(window, this, '" + vCampos.get(4) +"')\" onKeyPress=\"top.keyPressedHandler(window, this, '" + vCampos.get(4) +"')\"></td>");
%>
</tr>
<tr>
<%
  vCampos = (Vector)listaCampos.get(2);
  vCampos = (Vector)vCampos.get(0);
  out.println("  <td>" + vCampos.get(2) + ":</td>");
  out.println("  <td width=\"10\">&nbsp;</td>");
  out.print  ("  <td><input maxlength=\"" + vCampos.get(3) + "\" name=\"" + vCampos.get(0) + "\" size=\"" + vCampos.get(3) +"\" tabindex=\"" + (++i) + "\" type=\"text\"" +
              " onBlur=\"top.estaVacio(this)||top.validate(window, this, '" + vCampos.get(4) +"')\" onKeyPress=\"top.keyPressedHandler(window, this, '" + vCampos.get(4) +"')\"></td>");
%>
</tr>
</table>
<br>

<table cellspacing="0"  border="0">
<tr>
<td rowspan="8" valign=top><b>Abonos:</b></td>
<td align="center">Cuentas</td><td>&nbsp;</td>
<td align="center">Montos</td><td>&nbsp;</td>
<td colspan="3">&nbsp;</td>
</tr>
<tr>
<td rowspan="4"><select name="cuentas" tabindex="<%=(++i)%>" size="10" onChange="JavaScript:top.equalmto(window)"><option value=0>- Cuentas de Abono -</select></td>
<td rowspan="4" width="20">&nbsp</td>
<td rowspan="4"><select name="montos" tabindex="<%=(++i)%>" size="10" onChange="JavaScript:top.equalcta(window)"><option value=0>- Montos de Abono -</select></td>
<td rowspan="4" width="20">&nbsp</td>
<%
  vCampos = (Vector)listaCampos.get(3);
  vCampos = (Vector)vCampos.get(0);
  out.println("  <td>" + vCampos.get(2) + ":</td>");
  out.println("  <td width=\"10\">&nbsp;</td>");
  out.print  ("  <td><input maxlength=\"" + vCampos.get(3) + "\" name=\"" + vCampos.get(0) + "\" size=\"" + vCampos.get(3) +"\" tabindex=\"" + (++i) + "\" type=\"text\"" +
              " onBlur=\"top.estaVacio(this)||top.validate(window, this, '" + vCampos.get(4) +"')\" onKeyPress=\"top.keyPressedHandler(window, this, '" + vCampos.get(4) +"')\"></td>");
%>
</tr>
<tr>
<%
  vCampos = (Vector)listaCampos.get(4);
  vCampos = (Vector)vCampos.get(0);
  out.println("  <td>" + vCampos.get(2) + ":</td>");
  out.println("  <td width=\"10\">&nbsp;</td>");
  out.print  ("  <td><input maxlength=\"" + vCampos.get(3) + "\" name=\"" + vCampos.get(0) + "\" size=\"" + vCampos.get(3) +"\" tabindex=\"" + (++i) + "\" type=\"text\"" +
              " onBlur=\"top.estaVacio(this)||top.validate(window, this, '" + vCampos.get(4) +"')\"  onKeyPress=\"top.keyPressedHandler(window, this, '" + vCampos.get(4) +"')\"></td>");
%>
</tr>
<tr><td colspan="3">&nbsp;</td></tr>
<tr>
<td colspan="3" align="center">
<a tabindex="<%=(++i)%>" href="JavaScript:top.addNewOption(window)"><img src="../ventanilla/imagenes/b_agregar.gif" border="0" alt="Agregar Abono"></a><p>
<a tabindex="<%=(++i)%>" href="JavaScript:top.modifyOption(window)"><img src="../ventanilla/imagenes/b_modificar.gif" border="0" alt="Modificar Abono"></a><p>
<a tabindex="<%=(++i)%>" href="JavaScript:top.removeOption(window)"><img src="../ventanilla/imagenes/b_eliminar.gif" border="0" alt="Eliminar Abono"></a>
</td>
</tr>
<tr><td colspan="7">&nbsp;</td></tr>
<tr>
<td align="center"><b>No. de Abonos:</b></td><td>&nbsp;</td>
<td align="center"><b>Monto Abonos:</b></td><td>&nbsp;</td>
<td colspan="3">&nbsp;</td>
</tr>
<tr>
<td align="center"><input tabindex="<%=(++i)%>" type="text" name="txtTotalA" value="0" size="5" onChange="top.validate(window, this, 'notChange')"></td><td>&nbsp;</td>
<td align="center"><input tabindex="<%=(++i)%>" type="text" name="txtMontoT" value="0" size="15" onChange="top.validate(window, this, 'notChange')"></td><td>&nbsp;</td>
<td colspan="3">&nbsp;</td>
</tr>
</table>

<input type="hidden" name="ctacargo" value="0">
<input type="hidden" name="mtocargo" value="0">
<input type="hidden" name="noabonos" value="0">
<input type="hidden" name="mtoabonos" value="0">
<input type="hidden" name="Scuentas" value="0">
<input type="hidden" name="Smontos" value="0">

<%java.util.Hashtable numDep = (java.util.Hashtable)session.getAttribute("branchinfo");%>
<input type="hidden" name="depnom" value="<%= numDep.get("N_DEP_NOMINA").toString() %>">
<input type="hidden" name="iTxn" value="<%= session.getAttribute("page.iTxn") %>">
<input type="hidden" name="oTxn" value="<%= session.getAttribute("page.oTxn") %>">
<input type="hidden" name="cTxn" value="<%= session.getAttribute("page.cTxn") %>">
<input type="hidden" name="sucursal" value="<%= session.getAttribute("branch") %>">
<input type="hidden" name="teller" value="<%= session.getAttribute("teller") %>">
<input type="hidden" name="moneda" value="<%= session.getAttribute("page.moneda")%>">
<input type="hidden" name="registro" value="<%=session.getAttribute("empno")%>">
<input type="hidden" name="tllrlvl" value="<%=session.getAttribute("tellerlevel")%>">
<input type="hidden" name="supervisor" value="<%= session.getAttribute("teller") %>">
<input type="hidden" name="AuthoOK" value="0">
<input type="hidden" name="compania" value="20">
<input type="hidden" name="SignOK" value="0">
<input type="hidden" name="validateFLD" value="0">
<input type="hidden" name="needSignature" value="<%out.print(txnIMGAutoriz);%>">
<input type="hidden" name="needAutho" value="<%out.print(txnAuthLevel);%>">
<input type="hidden" name="SignField" value="<%= session.getAttribute("page.SignField") %>">
<input type="hidden" name="CurrAbono" value="_">
<p>
<script>
 if (document.entry.txtFechaEfect)
 {
  document.writeln("<input tabindex=\"<%=(++i)%>\" type=hidden name=txtFechaSys>");
  document.entry.txtFechaEfect.value=top.sysdate();
  }
</script>
<% if(txnAuthLevel.equals("1") || txnIMGAutoriz.equals("1"))
      out.println("<a id='btnCont' tabindex=\"" + (++i) + "\" href=\"javascript:validate(document.entry);javascript:AuthoandSignature(document.entry)\"><img src=\"../ventanilla/imagenes/b_continuar.gif\" border=\"0\"></a>");
   else
//    out.println("<a tabindex=\"" + (++i) + "\" href=\"top.cadenas();javascript:validate(document.entry)\"><img src=\"../ventanilla/imagenes/b_aceptar.gif\" border=\"0\"></a>");      
      out.println("<a id='btnCont' tabindex=\"" + (++i) + "\" href=\"javascript:validate(document.entry)\"><img src=\"../ventanilla/imagenes/b_continuar.gif\" border=\"0\"></a>");      
%>
<input type="hidden" name="numero" value="0">
<input type="hidden" name="count" value="0">
<input type="hidden" name="codecargo" value="X">
<input type="hidden" name="codeabono" value="X">
<input type="hidden" name="end" value="X">
</form>
</body>
</html>
