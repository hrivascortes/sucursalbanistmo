<!--
//*************************************************************************************************
//             Funcion: JSP que despliega Cobro Comision
//            Elemento: cobrocom.jsp
//          Creado por: Alejandro Gonzalez Castro
// Ultima Modificacion: 03/08/2004
//      Modificado por: Alejandro Gonzalez Castro
//*************************************************************************************************
// CCN - 4360175 - 03/08/2004 - Ajuste size de ventana de impresion
//*************************************************************************************************
-->

<%@ page contentType="text/html" session="true" import="java.util.Hashtable, com.bital.util.Common, com.bital.util.Currency"%>
<%
response.setHeader("Cache-Control","no-store"); //HTTP 1.1
response.setHeader("Pragma","no-cache"); //HTTP 1.0
response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
%>
<%
  Hashtable local = (Hashtable)session.getAttribute("page.datasession");
  String monto = (String)local.get("txtComision");
  String iva   = (String)local.get("txtIVA");
  String total = calculaTotal(monto, iva);
  String DomEsta = "";
  int moneda = Integer.parseInt((String)local.get("moneda"));
  
  String iTxn = (String)local.get("iTxn");
  String Concepto="";
  if(iTxn.equals("0031"))
    Concepto = "EXPEDICION DE GIROS";
  else     
    Concepto = "EXPEDICION DE CHEQUE DE CAJA";
  
  if ( (String)local.get("txtConcepto")!= null)
      Concepto = (String)local.get("txtConcepto");
  if ( (String)local.get("txtDomEsta")!= null)
      DomEsta = (String)local.get("txtDomEsta");
%>
<html>
<head>
  <title>COBRO DE COMISIONES</title>
<style type="text/css">Body {font-family: Arial, Helv; font-size: 10pt; font-weight: bold; color:#000000}</style>
<script language="JavaScript">
<!--
function validate(form)
{
  if( !top.validate(window, form.txtNomCliente, 'isEmptyStr') )
  {
    form.txtNomCliente.focus()
    return false
  }
  if( !top.validate(window, form.txtRFC, 'isEmptyStr') )
  {
    form.txtRFC.focus()
    return false
  }
  if( !top.validate(window, form.txtAddrCliente, 'isEmptyStr') )
  {
    form.txtAddrCliente.focus()
    return false
  }

  // Armar txtCadImpresion
  form.txtCadImpresion.value = 'COMPCOM~' + form.txtNomCliente.value + '~'
      //+ form.txtRFC.value + '~CHEQUES~EXPEDICION DE CHEQUE DE CAJA~' + form.txtAddrCliente.value + '~'
      + form.txtRFC.value + '~CHEQUES~<%= Concepto %>~' + form.txtAddrCliente.value + '~'
      + '<%= local.get("txtComision") %>~<%= local.get("txtIVA") %>'
      + '~<%= total %>~<%= DomEsta %>~<%= local.get("moneda")%>~'

  var str = "ventanilla.ImprimirServlet?txtCadImpresion=" + escape(form.txtCadImpresion.value)
  top.openDialog(str, 160, 120, 'top.setPrefs4()', document.entry)

  return false
}
//-->
</script>
</head>
<body>
<strong>COBRO DE COMISIONES</strong>
<p>
<%!
  private String unformatCur(String value)
  {
    if( value == null )
      return "000";

    StringBuffer monto = new StringBuffer(value);
    for(int i=0; i<monto.length();)
    {
      if( monto.charAt(i) == ',' )
        monto.deleteCharAt(i);
      else
        ++i;
    }

    return monto.toString();
  }

  private double parseNumber(String number)
  {
    double result;
    try
    {
      result = Double.parseDouble(number);
    }
    catch(NumberFormatException e)
    {
      result = 0.00;
    }

    return result;
  }

  private String calculaTotal(String comision, String iva)
  {
    double result = parseNumber(unformatCur(comision));
    result = result + parseNumber(unformatCur(iva));

    return Common.formatCur(result);
  }
%>
<form name="entry" action="../../servlet/ventanilla.ImprimirServlet" method="post" onsubmit="return validate(this)" target="impresion">
<input type="hidden" name="txtCadImpresion">
<table border="0" cellspacing="0">
<tr>
  <td class="label">Domicilio Sucursal:</td>
  <td width="10">&nbsp;</td>
  <td><i><%= DomEsta %></i></td>
</tr>
<tr>
  <td class="label">Nombre del Cliente:</td>
  <td>&nbsp;</td>
  <td><input type="text" name="txtNomCliente" size="40" maxlength="40" onchange="top.validate(window, this, 'isEmptyStr')"></td>
</tr>
<tr>
  <td class="label">R.F.C.</td>
  <td>&nbsp;</td>
  <td><input type="text" name="txtRFC" size="13" maxlength="13" onchange="top.validate(window, this, 'isEmptyStr')"></td>
</tr>
<tr>
  <td class="label">Domicilio Cliente:</td>
  <td>&nbsp;</td>
  <td><input type="text" name="txtAddrCliente" size="40" maxlength="40" onchange="top.validate(window, this, 'isEmptyStr')"></td>
</tr>
</table>
<hr size="1" width="75%">
<table border="0" cellspacing="0">
<tr>
  <td class="label">Servicio:</td>
  <td width="10">&nbsp;</td>
  <td><i>CHEQUES</i></td>
</tr>
<tr>
  <td colspan="6" class="label">Concepto:</td>
  <td width="10">&nbsp;</td>
  <td><i><%= Concepto %></i></td>
  <td width="10">&nbsp;</td>
  <td colspan="2">&nbsp;</td>
</tr>
<tr>
  <td colspan="2">&nbsp;</td>
  <td colspan="2"><i>Comisi&oacute;n:</i></td>
  <td align="right">$ <%= local.get("txtComision") %></td>
  <td>&nbsp;</td>
</tr>
<tr>
  <td colspan="2">&nbsp;</td>
  <td colspan="2"><i>I.V.A:</i></td>
  <td align="right">$ <%= local.get("txtIVA") %></td>
  <td>&nbsp;</td>
</tr>
<tr>
  <td colspan="2">&nbsp;</td>
  <td colspan="2"><i>Total:</i></td>
  <td align="right">$ <%= total %></td>
  <td>&nbsp;</td>
</tr>
<tr>
  <td colspan="4" class="label">Importe:</td>
  <td>&nbsp;</td>
  <td><%= Currency.convert(total, moneda) %></td>
</tr>
</table>
<br>
<input type="image" src="../ventanilla/imagenes/b_imprimir.gif" border="0">
</form>
</body>
</html>
