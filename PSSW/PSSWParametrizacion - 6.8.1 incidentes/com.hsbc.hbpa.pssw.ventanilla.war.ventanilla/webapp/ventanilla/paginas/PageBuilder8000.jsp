<!--
//*************************************************************************************************
//             Funcion: JSP que presenta los datos de la txn 8000
//            Elemento: PageBuilder8000.jsp
//          Creado por: Alejandro Gonzalez Castro
//      Modificado por: Juvenal Fernandez
//*************************************************************************************************
// CCN - 4360347 - 22/07/2005 - Se realizan correciones a la funcion validate
// CCN - 4360364 - 19/08/2005 - Se controlan departamentos por Centro de Costos.
// CCN - 4360581 - 12/03/2007 - Se convierten los estilos en jsf y se hace un include en el jsp
//*************************************************************************************************
-->

<%@ page session="true"%>
<%
response.setHeader("Cache-Control","no-store"); //HTTP 1.1
response.setHeader("Pragma","no-cache"); //HTTP 1.0
response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
	<head>
		<%@include file="style.jsf"%>
		<title>Visualizacion de Firmas</title>
	</head>
	<Script language="JavaScript">
	<!--
		var formName = 'Mio';
		var sync = 0;

		function validate(form)
		{
	
		 if(document.entry.CuentaFirmas.value.length > 5 && document.entry.CuentaFirmas.value.length < 9)
		   {
		   alert("El numero de Cuenta no es valido.... Por favor verifique...") ;
		   document.entry.CuentaFirmas.focus();
		   document.entry.CuentaFirmas.value="";
		   return;
		   }
		   else
		   {
		    sync = 1;
			if(document.entry.CuentaFirmas.value.length == 0 )
    			return;
    			
			if(document.entry.CuentaFirmas.value.length == 9 )
				document.entry.CuentaFirmas.value = "0" + document.entry.CuentaFirmas.value;
			
	
			  if( document.entry.CuentaFirmas.value.length == 10  )
			  {
			    if( !top.validate(window, document.entry.CuentaFirmas, 'isValidCta') )
  			      {
  			      return;
    			      }	
   			  }	
   			  
			top.openModalDialog('<%=request.getContextPath()%>/servlet/ventanilla.FirmasServlet?txtDDACuenta=' + document.entry.CuentaFirmas.value, 710, 435, 'top.setPrefs9()', form);
			sync = 0;
		   }	
		}
		
		function local_keyPressedHandler(frame, form, field)
		{
		  if (frame.event.keyCode == "13")
		  {
		    if (field.value != "") 
		      validate(form);
		  }
		}
		
//-->
   </SCRIPT>
	<body bgcolor="#FFFFFF" onload="top.setFieldFocus(window.document)">
			<form name="entry" >
      <input type="hidden" name="sucursal" value="<%= session.getAttribute("branch") %>">
      <input type="hidden" name="moneda" value="<%= session.getAttribute("page.moneda")%>">
      <input type="hidden" name="iTxn" value="<%= session.getAttribute("page.iTxn") %>">
      <input type="hidden" name="oTxn" value="<%= session.getAttribute("page.oTxn") %>">
      <input type="hidden" name="cTxn" value="<%= session.getAttribute("page.cTxn") %>">
      <input type="hidden" name="tllrlvl" value="<%=session.getAttribute("tellerlevel")%>">
      <input type="hidden" name="departamento" value="<%=session.getAttribute("isForeign")%>">      
      <input type="hidden" name="compania" value="20">
			<table border="0" cellspacing="0">
				<b><h3> &nbsp; </h3></b>
				<b><h3>
<%
			out.print(session.getAttribute("page.cTxn") + "  " + (String)session.getAttribute("page.txnlabel"));
%>
				</h3></b>
				<tr><td>
				<br>
				</td></tr>
				<tr>
					<td><b>Cuenta :</b>
					</td>
					<td><input type="text" maxlength="15" name="CuentaFirmas" size="15" onBlur="top.estaVacio(this)||top.validate(window, this, 'isnumeric')">
					</td>
		   	</tr>
				<tr><td><br></td></tr>
				<tr>
					<td>
						<a href="javascript:validate(document.entry)"><img src="../ventanilla/imagenes/b_aceptar.gif" border="0"></a>
					</td>
					
				</tr>
				<input type="text" style="display:None">
		</table>
		</form>
	</body>
</html>
