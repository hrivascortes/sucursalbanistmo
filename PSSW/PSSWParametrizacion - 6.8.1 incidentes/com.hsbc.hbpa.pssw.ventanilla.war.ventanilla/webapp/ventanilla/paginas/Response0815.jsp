<!--
//*************************************************************************************************
//             Funcion: JSP que despliega respuesta de txn 0815
//            Elemento: Response0815.jsp
//          Creado por: Alejandro Gonzalez Castro
//      Modificado por: Alejandro Gonzalez Castro
//*************************************************************************************************
// CCN - 4360175 - 03/08/2004 - Ajuste size de ventana de impresion
// CCN - 4360275 - 18/02/2005 - WAS 5.1
//*************************************************************************************************
-->
<%@ page session="true" import="java.util.*"%>
<%
response.setHeader("Cache-Control","no-store"); //HTTP 1.1
response.setHeader("Pragma","no-cache"); //HTTP 1.0
response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<%
%>
<%!
  private String getString(String s)
  {
    int len = s.length();
    for(; len>0; len--)
	  if(s.charAt(len-1) != ' ')
	    break;

    return s.substring(0, len);
  }

  private String getItem(String newCadNum, int newOption)
  {
   //----
   int iPIndex = newCadNum.indexOf("CONSECUTIVO:") + 12, NumSaltos, Num = 0, Ban = 0;
   String nCadNum = new String("");
   NumSaltos = newOption;
   while(Num < NumSaltos)
   {
    if(newCadNum.charAt(iPIndex) == 0x20)
    {
     while(newCadNum.charAt(iPIndex) == 0x20)
      iPIndex++;
     Num++;
    }
    else
     while(newCadNum.charAt(iPIndex) != 0x20)
      iPIndex++;
   }
   while(newCadNum.charAt(iPIndex) != 0x20 && iPIndex < newCadNum.length()){
    nCadNum = nCadNum + newCadNum.charAt(iPIndex);
    if(iPIndex < newCadNum.length())
      iPIndex++;
    if(iPIndex == newCadNum.length())
      break;
   }

   return nCadNum;
  }

  private String delLeadingfZeroes(String newString){
   StringBuffer newsb = new StringBuffer(newString);
   int i = 0;
   while(newsb.charAt(i) == '0')
    newsb.deleteCharAt(0);

   return newsb.toString();
  }

  private String delCommaPointFromString(String newCadNum)
  {
   String nCad = new String(newCadNum);
   if( nCad.indexOf(".") > -1)
   {
    nCad = nCad.substring(nCad.indexOf(".") + 1, nCad.length());
    if(nCad.length() != 2)
    {
     String szTemp = new String("");
     for(int j = nCad.length(); j < 2; j++)
      szTemp = szTemp + "0";
     newCadNum = newCadNum + szTemp;
    }
   }

   StringBuffer nCadNum = new StringBuffer(newCadNum);
   for(int i = 0; i < nCadNum.length(); i++)
    if(nCadNum.charAt(i) == ',' || nCadNum.charAt(i) == '.')
     nCadNum.deleteCharAt(i);

   return nCadNum.toString();
  }

  private String fillWithZeroes(String newString){
   int Indice = newString.indexOf(".");
   for(int i = Indice; i < Indice + 1; i++)
    newString = newString + "0";

   return newString;
  }

  private String setFormat(String newCadNum)
  {
    int iPIndex = newCadNum.indexOf(".");
    String nCadNum = new String(newCadNum.substring(0, iPIndex));
    for (int i = 0; i < (int)((nCadNum.length()-(1+i))/3); i++)
     nCadNum = nCadNum.substring(0, nCadNum.length()-(4*i+3))+','+nCadNum.substring(nCadNum.length()-(4*i+3));

    return nCadNum + newCadNum.substring(iPIndex, newCadNum.length());
  }

  private String setPointToString(String newCadNum)
  {
   int iPIndex = newCadNum.indexOf("."), iLong;
   String szTemp;

   if(iPIndex > 0)
   {
    newCadNum = new String(newCadNum + "00");
    newCadNum = newCadNum.substring(0, iPIndex + 1) + newCadNum.substring(iPIndex + 1, iPIndex + 3);
   }
   else
   {
    for(int i = newCadNum.length(); i < 3; i++)
     newCadNum = "0" + newCadNum;
    iLong = newCadNum.length();
    if(iLong == 3)
     szTemp = newCadNum.substring(0, 1);
    else
     szTemp = newCadNum.substring(0, iLong - 2);
    newCadNum = szTemp + "." + newCadNum.substring(iLong - 2);
   }

   return newCadNum;
  }
%>
<%
  Vector resp = (Vector)session.getAttribute("response");
  Hashtable datasession = (Hashtable)session.getAttribute("page.datasession");
  String forPago = (String)datasession.get("lstForPago");
%>
<html>
<head>
  <title>Certificación</title>
<style type="text/css">Body {font-family: Arial, Helv; font-size: 10pt; font-weight: bold; color:#000000}</style>
<style>
.invisible
{
  display:None;
}

.visible
{
  display:"";
}
</style>
<script language='javascript'>
<!--
  sharedData = '0';

  function formSubmitter()
  {
    clearInterval(formSubmitter);
    if(sharedData != '0')
    {
      document.forms[0].submit()
    }
    
    sharedData = '0';
  }
//-->
</script>
</head>
<body onload="setInterval('formSubmitter()',1000)">
<p>
<%
   String szResp = new String("");
   String codigo = new String("");
   if( resp.size() != 4 )
    out.println("<b>Error de conexi&oacute;n</b><br>");
   else
   {
    try{
     codigo = (String)resp.elementAt(0);
     if( !codigo.equals("0") )
      out.println("<b>Error en transaccion</b><br>");
     int lines = Integer.parseInt((String)resp.elementAt(1));
     szResp = (String)resp.elementAt(3);
     for(int i=0; ; i++){
      String line = szResp.substring( i*78, Math.min(i*78+77, szResp.length()) );
      out.println(getString(line) + "<p>");
      if(Math.min(i*78+77, szResp.length()) == szResp.length())
       break;
     }
    }
    catch(NumberFormatException nfe){
     out.println("<b>Error de conexi&oacute;n</b><br>");
    }
   }
%>
<form name="Txns" action="../../servlet/ventanilla.PageServlet" method="post">
<%
 
  if(forPago.equals("01") && codigo.equals("0"))
  {
    String montoComision = getItem(szResp,3);
    String porcentajeIVA = getItem(szResp,4);
    String nombre = szResp.substring(77, 77+41).trim();
    String monto = delCommaPointFromString((String)datasession.get("txtMonto"));
    montoComision = delLeadingfZeroes(montoComision);
    Long montoIVA = new Long((Long.parseLong(montoComision) * Long.parseLong(porcentajeIVA)) / 100);
    Long montoTotal = new Long(Long.parseLong(monto)+ Long.parseLong(montoComision)  + montoIVA.longValue());
    datasession.put("montoComision", montoComision);
    datasession.put("montoIVA", montoIVA.toString());
    datasession.put("montoTotal", montoTotal.toString());
    datasession.put("txtNomOrd", nombre.replace('*',' ').trim());
    session.setAttribute("page.datasession",datasession);
    out.println("<script language=\"javascript\">sharedData = '1'</script>");
  }
  else
  {
    if (!forPago.equals("01") && codigo.equals("0"))
      out.println("<script language=\"javascript\">sharedData = '1'</script>");
  }
%>

<table border="0" cellspacing="0">
<tr>
<td>
</td>
</tr>
<tr>
<td>
<%
  Stack flujotxn = (Stack)session.getAttribute("page.flujotxn");
  String iTxn = (String)datasession.get("iTxn");
  if(!forPago.equals("01") && !iTxn.equals("0031"))
  {
    while(!flujotxn.empty())
    {
      flujotxn.pop();
    }
    
    //String lstBanLiq = (String)datasession.get("lstBanLiq");
    flujotxn.push("4071");
  }
  
  if(!forPago.equals("01") && iTxn.equals("0031"))
  {
    while(!flujotxn.empty())
    {
      flujotxn.pop();
    }
    
    //String lstBanLiq = (String)datasession.get("lstBanLiq");
    flujotxn.push("0572");
  }
  
  if(!codigo.equals("0"))
  {
    while(!flujotxn.empty())
      flujotxn.pop();
    //session.setAttribute("page.flujotxn", flujotxn);
  }
  
  session.setAttribute("page.flujotxn", flujotxn);
  String isCertificable = (String)session.getAttribute("page.certifField");
  String txtCadImpresion  = "NXXXCHVI0170#XXXXPRUEBA1#NXXXPRUEBA2#NCSGPRUEBA3#";
  
  if(!flujotxn.empty())
  {
    out.print("<div id=\"div_cont\" class=\"invisible\" align=\"center\">");
    
    if(isCertificable.equals("1"))
      out.println("<a href=\"#\" onClick=\"top.openDialog('../../servlet/ventanilla.ImprimirServlet?txtCadImpresion=" + txtCadImpresion + "', 160, 120, 'top.setPrefs4()', Txns)\"><img src=\"imagenes/b_continuar.gif\" border=\"0\"></a>");
    else
      out.print("<input type=\"image\" src=\"../imagenes/b_continuar.gif\" alt=\"\" border=\"0\">");
      
    out.print("</div>");
  }
%>
  <input type="hidden" name="transaction" value="<%= session.getAttribute("page.iTxn") %>">
  <input type="hidden" name="transaction1" value="<%= session.getAttribute("page.oTxn") %>">
  
<script language='javascript'>

 var o;
 o = document.getElementById('div_cont');
 
 if(sharedData != '0')
 {
   o.innerHTML = "<font color='red'>procesando...</font>";
 }
 
 o.className='visible';
</script>
  
</td>
</tr>
</table>
</form>
</body>
</html>
