<%
//*************************************************************************************************
//             Funcion: JSP para despliege de Formato SIB
//            Elemento: PageBuilderSIB.jsp
//      Modificado por: Humberto Enrique Balleza Mej�a
//*************************************************************************************************
// CCN - 4620021 - Creaci�n de Vista
//*************************************************************************************************
 %>
<%@ page session="true" import="java.util.*,ventanilla.GenericClasses" isThreadSafe="true"%>
<%
	response.setHeader("Cache-Control", "no-store"); //HTTP 1.1
	response.setHeader("Pragma", "no-cache"); //HTTP 1.0
	response.setDateHeader("Expires", 0); //prevents caching at the proxy server
	request.getContextPath();
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<%
	String MontoPro = new String("");
	Vector listaCampos = (Vector) session.getAttribute("page.listcampos");
	Vector listaContenidos = (Vector) session.getAttribute("page.listcont");
	String txnAuthLevel = (String) session.getAttribute("page.txnAuthLevel");
	String txnIMGAutoriz = (String) session.getAttribute("page.txnIMGAutoriz");
	Vector v = (Vector) session.getAttribute("page.specialacct");
	Hashtable datasession = (Hashtable) session.getAttribute("page.datasession");
	String txn = session.getAttribute("page.cTxn").toString();

	
	
	//completar en la tabla datos varios para el tipo de txn
	Vector txnMan = new Vector();
	txnMan.add("0566");
	txnMan.add("0568");
	txnMan.add("5503");
	txnMan.add("0476");
	txnMan.add("4153");
	txnMan.add("0106");
	txnMan.add("0604");
	txnMan.add("ARPL");

	Vector txnAut = new Vector();
	txnAut.add("1003");
	txnAut.add("1001");
	txnAut.add("1053");
	txnAut.add("5357");
	txnAut.add("1057");
	txnAut.add("0468");
	txnAut.add("4537");
	txnAut.add("1193");
	txnAut.add("1195");
	txnAut.add("0726");
	txnAut.add("4111");
	txnAut.add("4043");
	
	ventanilla.GenericClasses gc = new ventanilla.GenericClasses();
	if (datasession == null)
		datasession = new Hashtable();
	int k;
	int i;
	
%>

<html>
<head>
<title>Datos de la Transaccion</title>
<%@include file="style.jsf"%>
<script language="JavaScript">
<!--

var formName = 'Mio'
var specialacct = new Array()
var bandera = 0;

<%
 if(!v.isEmpty()){
  for(int iv = 0; iv < v.size(); iv++){
   out.print("specialacct[" + iv + "]='" + v.get(iv) + "'" + '\n');
  }
 }
%>
function validate(form)
{
   form.validateFLD.value = '0';
   inputStr = '';
<%
//Proyecto Daily Cash - Segregacion de tipos de txn presentacion de formulario
	String cTxn = (String)datasession.get("cTxn");
	String iTxn = (String)datasession.get("iTxn");
	String oTxn = (String)datasession.get("oTxn");
	
	//IDDEVA-Daily Cash CR Bandera para identificar el servicio de ARP que se tiene que mostrar como Manual o Aut
	// solo los servicios 60 y 670 seran operados de manera manual, los dem�s de manera automatica	
	// {true.- Automatico, flase.- Manual}

	boolean bLlenadoManual = false;
	boolean bLlenadoAuto = false;
	
	if(iTxn.equals("0726") && oTxn.equals("0106")){
		cTxn = "0106";		
	}
	
	
	if( txnAut.contains(cTxn) ){
	   bLlenadoAuto=true;
	}
	
	if( txnMan.contains(cTxn) ){
	   if ( 0==cTxn.trim().compareTo("5503") 
	   ||   0==cTxn.trim().compareTo("ARPL")
	   ||   0==cTxn.trim().compareTo("0604")  
	   )
	   {     
	         String sCveServicioARP = (String)datasession.get("servicio1");
		     if( 0 == sCveServicioARP.trim().compareTo("60")  
		      || 0 == sCveServicioARP.trim().compareTo("670") )
		     {
		         
		         bLlenadoManual = true;
		     }else{
		         bLlenadoManual = false;
		         bLlenadoAuto=true;
	   	         
		     }  
	   }else{
	      bLlenadoManual = true;
	   }	
	}
	
	
	
	
	
  for(i=0; i < listaCampos.size(); i++)
   {
   
       Vector vCampos = (Vector)listaCampos.get(i);
       vCampos = (Vector)vCampos.get(0);
        if(iTxn.equals("ISIB") && cTxn.equals("ISIB")){
    if( vCampos.get(4).toString().length() > 0 && !vCampos.get(0).toString().equals("txtCtaSIB"))
    {    
    	
    	if(datasession.get(vCampos.get(0)) == null){
        	out.println("  if( !top.validate(window, document.entry." + vCampos.get(0) + ", '" + vCampos.get(4) +"') )");
        	out.println("return;") ;
        }
    }
  }
  //Validacion de campos nulos
  else {
//  if (txnMan.contains(cTxn)){
  if (bLlenadoManual){
  	if ( vCampos.get(0).toString().equals("txtNomDep")){
  	            out.println("  if( !top.validate(window, document.entry.txtNomDep, 'isEmptyStr') )");
        	out.println("return;") ;}
  	
      else if ( vCampos.get(0).toString().equals("txtCedDep")){
            out.println("  if( !top.validate(window, document.entry.txtCedDep, 'isEmptyStr') )");
        	out.println("return;") ;}
      
      else if ( vCampos.get(0).toString().equals("txtCtaSIB")){
                  out.println("  if( !top.validate(window, document.entry.txtCtaSIB, 'isEmptyStr') )");
        	out.println("return;") ;}
      
      else if (  vCampos.get(0).toString().equals("txtNomCte")){
            out.println("  if( !top.validate(window, document.entry.txtNomCte, 'isEmptyStr') )");
        	out.println("return;") ;}
      
      else if ( vCampos.get(0).toString().equals("txtCedCte")){
      out.println("  if( !top.validate(window, document.entry.txtCedCte, 'isEmptyStr') )");
        	out.println("return;") ;}
  }
  else {
  	if ( vCampos.get(0).toString().equals("txtNomDep")){
  	            out.println("  if( !top.validate(window, document.entry.txtNomDep, 'isEmptyStr') )");
        	out.println("return;") ;}
  	
      else if ( vCampos.get(0).toString().equals("txtCedDep")){
            out.println("  if( !top.validate(window, document.entry.txtCedDep, 'isEmptyStr') )");
        	out.println("return;") ;}
  }
  }
 }
%>
  form.validateFLD.value = '1'
  if (document.entry.txtFechaEfect || document.entry.txtFechaError || document.entry.txtFechaCarta)
    document.entry.txtFechaSys.value=top.sysdatef();

 
 var suc ='<%= (String)session.getAttribute("branch")%>'
 
  if ( bandera == 1 ) {                       // Pedir autorizacion
        form.onSubmit="return validate(document.entry);"
        document.entry.needAutho.value = '1';
        AuthoandSignature(document.entry);
  }

  if ( bandera == 0) {
 <%
 if(!txnAuthLevel.equals("1") && !txnIMGAutoriz.equals("1")){
    out.println("  form.submit()");
    out.println("var obj = document.getElementById('btnCont');");
    out.println("obj.onclick = new Function('return false');");
    }
    
%> }
}
function AuthoandSignature(entry)
{
 var Cuenta;
 if(entry.returnform != undefined && entry.txtFolio != undefined)
   if(entry.txtFolio.value.length == 0)
     return;

 if(entry.validateFLD.value != '1')
  return

 if(entry.needAutho.value == '1' && entry.AuthoOK.value != '1')
 {
  top.openDialog('../ventanilla/paginas/authoriz.jsp', 220, 130, 'top.setPrefs()', entry)
 }

 if(entry.needSignature.value == '1' && entry.SignField.value != '')
 {
  Cuenta = eval('entry.' + entry.SignField.value + '.value')
  top.openModalDialog('<%=request.getContextPath()%>/servlet/ventanilla.FirmasServlet?txtDDACuenta=' + Cuenta, 710, 430, 'top.setPrefs2()', entry)
 }
 
}

function Alltrim(inputStr)
{
  var inStr = new String(inputStr)

  while(inStr.charAt(0) == ' ')
    inStr = inStr.substring(1, inStr.length)

  while(inStr.charAt(inStr.length) == ' ')
    inStr = inStr.substring(0, inStr.length-1)

  return inStr
}
//-->

<%if(session.getAttribute("viewCSIB") != null && session.getAttribute("viewCSIB").equals("1"))
       out.println("alert('Debe de completar la captura del Formulario UAF');");
%>

</script>
</head>
<body bgcolor="#FFFFFF" onload="top.setFieldFocus(window.document)">
<br>
<%
	//out.print("<form name=\"entry\" action=\"../servlet/ventanilla.com.bital.admin.SpFunc_SIB\" method=\"post\" ");
	out.print("<form name=\"entry\" action=\"" + request.getContextPath() + "/servlet/ventanilla.DataServlet\" method=\"post\" ");
	session.setAttribute("viewCSIB","1");
	
	if (txnAuthLevel.equals("1")|| session.getAttribute("page.cTxn").toString().equals("0825"))
		out.print("onSubmit=\"return validate(this)\"");
    	out.println(">");
%>
<h1><%=session.getAttribute("page.txnImage")%></h1>
<h3>
<%
			out.print(session.getAttribute("page.cTxn") + "  "+ (String) session.getAttribute("page.txnlabel"));
%>
</h3>
<table border="0" cellspacing="0" class="normaltextblack">
	<%
		String lstSIB = "";
		if (datasession.get("lstCuentaSIB") == null) {
			lstSIB = "02";
			datasession.put("lstCuentaSIB", lstSIB);
		}else{
			lstSIB = (String) datasession.get("lstCuentaSIB");
		}

		for (i = 0; i < listaCampos.size(); i++) {
			Vector vCampos = (Vector) listaCampos.get(i);
			vCampos = (Vector) vCampos.get(0);
			
			//if ((iTxn.equals("ISIB") && cTxn.equals("ISIB"))|| txnMan.contains(cTxn)|| txnAut.contains(cTxn)) {
			if ((iTxn.equals("ISIB") && cTxn.equals("ISIB"))|| bLlenadoManual || bLlenadoAuto ) {
			
				if (!( ( vCampos.get(0).toString().equals("lstCtaCon") && !lstSIB.equals("05") ) 
				    || ( vCampos.get(0).toString().equals("txtCtaSIB") && lstSIB.equals("05")))) {
				if (!vCampos.get(0).toString().equals("txtCodSeg")) {
				
				   out.println("<tr>");
				   if (vCampos.get(0).equals("txtDDACuenta")&& (txn.equals("0130") || txn.equals("0128"))) {
					 out.println("  <td>N�mero de Cuenta Abono:</td>");
				   }else {
					 out.println("  <td>" + vCampos.get(2)+ ":</td>");
				   }
				   out.println("  <td width=\"10\">&nbsp;</td>");
					//IDDEVA Se cambi el if por banderas
				   //if (txnMan.contains(cTxn) || txnAut.contains(cTxn)) {
				   if (bLlenadoManual || bLlenadoAuto ) {
					  //if(txnAut.contains(cTxn)){
					  if( bLlenadoAuto ){
								if (vCampos.get(0).toString().equals("lstOperacion")) {
								out.print("  <td>" + datasession.get("lstOperacion").toString() + "</td>");
								out.println("</tr>");
								continue;
									}
								if (vCampos.get(0).toString().equals("lstRubro")) {
								out.print("  <td>" + datasession.get("lstRubro").toString() + "</td>");
								out.println("</tr>");
								continue;
									}
								if (vCampos.get(0).toString().equals("txtCtaSIB")) {
								if (datasession.get("cTxn").toString().equals("1053") || datasession.get("cTxn").toString().equals("4537")){
								out.print("  <td>" + datasession.get("txtDDACuenta2").toString() + "</td>");
								datasession.put("txtCtaSIB",datasession.get("txtDDACuenta2"));
									}
								else if (datasession.get("cTxn").toString().equals("1057")){
								out.print("  <td>" + datasession.get("txtDDARetiro").toString() + "</td>");
								datasession.put("txtCtaSIB",datasession.get("txtDDARetiro"));
									}					
								else if (datasession.get("cTxn").toString().equals("0468")){
								out.print("  <td>" + datasession.get("txtDDADeposito").toString() + "</td>");
								datasession.put("txtCtaSIB",datasession.get("txtDDADeposito"));
									}										
				                else if ((datasession.get("iTxn").toString().equals("0726")&& datasession.get("oTxn").toString().equals("4111"))){
									out.print("  <td>" + datasession.get("txtDDACuenta1").toString() + "</td>");
									datasession.put("txtCtaSIB",datasession.get("txtDDACuenta1"));
								}else if( datasession.get("iTxn").toString().equals("5503") || datasession.get("iTxn").toString().equals("ARPL") )
								{
									//IDDEVA Se agrega la Txn 5503 para RAP
									out.print("  <td>" + datasession.get("CuentaRAP").toString() + "</td>");
									datasession.put("txtCtaSIB",datasession.get("CuentaRAP"));
															
								}		
								else{
									out.print("  <td>" + datasession.get("txtDDACuenta").toString() + "</td>");
									datasession.put("txtCtaSIB",datasession.get("txtDDACuenta"));				
								}
								out.println("</tr>");
				
								continue;
									}
									
								if (vCampos.get(0).toString().equals("txtMonto")) 
								{
									if (datasession.get("cTxn").toString().equals("1053") 
									|| datasession.get("cTxn").toString().equals("5357")
									|| datasession.get("cTxn").toString().equals("1057") 
									|| datasession.get("cTxn").toString().equals("0468") 
									|| datasession.get("cTxn").toString().equals("4537") 
									|| datasession.get("cTxn").toString().equals("1193") 
									|| datasession.get("cTxn").toString().equals("1195")){
										out.print("  <td>" + datasession.get("txtMonto").toString() + "</td>");
									}
									else if ((datasession.get("iTxn").toString().equals("4043")&& datasession.get("oTxn").toString().equals("0106"))
									||(datasession.get("iTxn").toString().equals("0726")&& datasession.get("oTxn").toString().equals("4111")))
									{
										out.print("  <td>" + datasession.get("txtTotalMN").toString() + "</td>");
										datasession.put("txtMonto",datasession.get("txtTotalMN"));								
									}else{
										out.print("  <td>" + datasession.get("txtEfectivo").toString() + "</td>");
										datasession.put("txtMonto",datasession.get("txtEfectivo"));								
									}
									out.println("</tr>");
									continue;
								}					
								if (vCampos.get(0).toString().equals("txtNomCte")) {
								out.print("  <td>" + datasession.get("txtNomCte").toString() + "</td>");			
								out.println("</tr>");
								continue;
									}
								if (vCampos.get(0).toString().equals("txtCedCte")) {
								out.print("  <td>" + datasession.get("txtCedCte").toString() + "</td>");
								out.println("</tr>");
								continue;
									}
								if (vCampos.get(0).toString().equals("lstNacCte")) {
								out.print("  <td>" + datasession.get("lstNacCte").toString() + "</td>");
								out.println("</tr>");
								continue;
									}	
							
						}
						
						if (vCampos.get(0).toString().equals("txtMonto")) {
		                if( datasession.get("cTxn").toString().equals("5503") 
		                  || datasession.get("cTxn").toString().equals("ARPL")
		                  ||datasession.get("cTxn").toString().equals("0604")){
						out.print("  <td>" + datasession.get("efectivoCSIB").toString() + "</td>");
						datasession.put("txtMonto",datasession.get("efectivoCSIB"));
						out.println("</tr>");
						continue;
						}
						}
						if(!(session.getAttribute("user.par")==null && session.getAttribute("autho.indice")==null)){
					Hashtable supervisores = (Hashtable) session.getAttribute("user.par");
					String indice = (String) session.getAttribute("autho.indice");
							if (vCampos.get(0).toString().equals("txtFunc")) {
						String Nom_Sup = (String) supervisores.get("D_NOMBRE" + indice);
						out.print("  <td>" + Nom_Sup + "</td>");
						out.println("</tr>");
						datasession.put("txtFunc",Nom_Sup);								
						continue;
							}
							
						if (vCampos.get(0).toString().equals("lstCargoFunc")) {
						String Nivel_Sup = (String) supervisores.get("N_NIVEL" + indice);
					         if (Nivel_Sup.equals("4"))
					     	 Nivel_Sup = "Gerente";
					         else
					    	 Nivel_Sup = "Sup. de Operaciones";
						out.print("  <td>" + Nivel_Sup + "</td>");
						out.println("</tr>");
						datasession.put("lstCargoFunc",Nivel_Sup);												
						continue;
							}
						 }
						 else{
						 String Nivel_Sup ="";
						 String Nom_Sup ="";
						 
						 }
						 
						}
						if (datasession.get(vCampos.get(0)) != null) {
							if (vCampos.get(0).toString().equals("txtMonto")) {
						out.print("  <td>"+ datasession.get(vCampos.get(0))+ "</td>");
						out.println("</tr>");
							} else {
						out.print("  <td>"+ datasession.get(vCampos.get(0))+ "</td>");
						out.println("</tr>");
							}
						} else {
		
							if (!vCampos.get(0).toString().substring(0, 3)
							.equals("lst")) {
						if (vCampos.get(0).toString().substring(0,
								3).equals("pss")) {
							out.print("  <td><input maxlength=\""
							+ vCampos.get(3) + "\" name=\""
							+ vCampos.get(0) + "\" size=\""
							+ vCampos.get(3)
							+ "\" type=\"Password\""
							+ " tabindex=\"" + (i + 1)
							+ "\"");
						} else {
							out.print("  <td><input maxlength=\""
							+ vCampos.get(3) + "\" name=\""
							+ vCampos.get(0) + "\" size=\""
							+ vCampos.get(3)
							+ "\" type=\"text\""
							+ " tabindex=\"" + (i + 1)
							+ "\"");
						}
		
						String CampoR = (String) vCampos.get(5);
						if (CampoR != null && CampoR.length() > 0) {
							CampoR = CampoR.trim();
							out.print(" value=\"" + CampoR + "\"");
						}
							} else {
						out.print("  <td><select name=\""
								+ vCampos.get(0) + "\" tabindex=\""
								+ (i + 1) + "\" size=\"" + "1"
								+ "\"");
						//	      if( vCampos.get(3).toString().length() > 0 ){
						if (vCampos.get(4).toString().length() > 0) {
							if (vCampos.get(0).toString()
							.substring(0, 3).equals("rdo"))
								out
								.println(" onClick=\"top.validate(window, this, '"
								+ vCampos.get(4)
								+ "')\">");
							else
								out
								.println(" onChange=\"top.validate(window, this, '"
								+ vCampos.get(4)
								+ "')\"  >");
						} else
							out.println(">");
						for (k = 0; k < listaContenidos.size(); k++) {
							Vector v1Campos = (Vector) listaContenidos
							.get(k);
							for (int j = 0; j < v1Campos.size(); j++) {
								Vector v1Camposa = (Vector) v1Campos
								.get(j);
								if (v1Camposa
								.get(0)
								.toString()
								.equals(
								vCampos.get(0)
										.toString()))
							out
									.println("  <option value=\""
									+ v1Camposa
									.get(3)
									+ "\">"
									+ v1Camposa
									.get(2));
							}
						}
						out.println("  </select></td>");
							}
							if (vCampos.get(4).toString().length() > 0
							&& !vCampos.get(0).toString()
							.substring(0, 3).equals("lst"))
						out
								.println("  onBlur=\"top.estaVacio(this)||top.validate(window, this, '"
								+ vCampos.get(4)
								+ "')\"  onKeyPress=\"top.keyPressedHandler(window, this, '"
								+ vCampos.get(4)
								+ "')\"   ></td>");
							else if (!vCampos.get(0).toString().substring(
							0, 3).equals("lst"))
						out.println(">");
							out.println("</tr>");
						}
						//**************
			} else {
	%>
	<%@include file="fieldLector.jsf"%>
	<%
			i = k;
			}
				}
			} 
		}
	%>

</table>
<input type="hidden" name="iTxn" value="<%= session.getAttribute("page.iTxn") %>">
<input type="hidden" name="oTxn" value="<%= session.getAttribute("page.oTxn") %>">
<input type="hidden" name="cTxn" value="<%= session.getAttribute("page.cTxn") %>">
<input type="hidden" name="sucursal" value="<%= session.getAttribute("branch") %>">
<input type="hidden" name="teller" value="<%= session.getAttribute("teller") %>">
<input type="hidden" name="moneda" value="<%= session.getAttribute("page.moneda")%>">
<input type="hidden" name="registro" value="<%=session.getAttribute("empno")%>">
<input type="hidden" name="tllrlvl" value="<%=session.getAttribute("tellerlevel")%>">
<input type="hidden" name="supervisor" value="<%= session.getAttribute("teller") %>">
<input type="hidden" name="AuthoOK" value="0">
<input type="hidden" name="SignOK" value="0">
<input type="hidden" name="validateFLD" value="0">
<input type="hidden" name="needSignature" value="<%out.print(txnIMGAutoriz);%>">
<input type="hidden" name="needAutho" value="<%out.print(txnAuthLevel);%>">
<input type="hidden" name="SignField" value="<%= session.getAttribute("page.SignField") %>">
<input type="hidden" name="override" value="N">
<input type="hidden" name="txtFees" value="0">
<input type="hidden" name="compania" value="20">
<input type="hidden" name="txtSelectedItem" value="0" >
<input type="hidden" name="returnform" value="N" >
<input type="hidden" name="returnform2" value="N" >
<input type="hidden" name="txtPlazaSuc" value="<%= session.getAttribute("plaza") %>" >


<input type="hidden" name="passw" value="<%=session.getAttribute("password")%>">
<input type="hidden" name="MontoRetiroPesos" value = "<%=session.getAttribute("Retiro.Pesos")%>">
<input type="hidden" name="MontoRetiroDolar" value = "<%=session.getAttribute("Retiro.Dolar")%>">
<p>
<style>
		.invisible
		{
		  display:None;
		}

		.visible
		{
		  display:"";
		}
</style>


<% if(txnAuthLevel.equals("1") || txnIMGAutoriz.equals("1"))
    out.println("<a id='btnCont' tabindex=\"" + (i+1) + "\" href=\"javascript:document.entry.returnform.value='S';validate(document.entry);javascript:AuthoandSignature(document.entry)\" ><img src=\"" + request.getContextPath() + "/ventanilla/imagenes/b_continuar.gif\" border=\"0\"></a>");
   else
  out.println("<a id='btnCont' tabindex=\"" + (i+1) + "\" href=\"javascript:validate(document.entry);\"><img src=\"" + request.getContextPath() + "/ventanilla/imagenes/b_continuar.gif\" border=\"0\"></a>");
%>

</form>
</body>
</html>
