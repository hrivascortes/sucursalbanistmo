<!--
//*************************************************************************************************
//             Funcion: JSP que despliega Comision e IVA
//            Elemento: ComIVAFmt.jsp
//          Creado por: Alejandro Gonzalez Castro
//      Modificado por: Juvenal R. Fernandez Varela
//*************************************************************************************************
// CCN - 4360275 - 18/02/2005 - WAS 5.1
//*************************************************************************************************
-->

<%@ page session="true" import="java.util.*"%>
<%
response.setHeader("Cache-Control","no-store"); //HTTP 1.1
response.setHeader("Pragma","no-cache"); //HTTP 1.0
response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<%
%>
<%!
  private String getString(String s)
  {
    int len = s.length();
    for(; len>0; len--)
      if(s.charAt(len-1) != ' ')
        break;

    return s.substring(0, len);
  }

  private String getItem(String newCadNum, int newOption)
  {
    int iPIndex = newCadNum.indexOf("CONSECUTIVO:") + 12, NumSaltos, Num = 0, Ban = 0;
    String nCadNum = new String("");
    NumSaltos = newOption;
    while(Num < NumSaltos)
    {
      if(newCadNum.charAt(iPIndex) == 0x20)
      {
        while(newCadNum.charAt(iPIndex) == 0x20)
          iPIndex++;
          
        Num++;
      } else
        while(newCadNum.charAt(iPIndex) != 0x20)
          iPIndex++;
    }
      
    while(newCadNum.charAt(iPIndex) != 0x20)
      nCadNum = nCadNum + newCadNum.charAt(iPIndex++);

    return nCadNum;
  }

  private String delLeadingfZeroes(String newString)
  {
    StringBuffer newsb = new StringBuffer(newString);
    int i = 0;
      
    while(newsb.charAt(i) == '0')
      newsb.deleteCharAt(0);

    return newsb.toString();
  }

  private String delCommaPointFromString(String newCadNum)
  {
    String nCad = new String(newCadNum);
    if( nCad.indexOf(".") > -1)
    {
      nCad = nCad.substring(nCad.indexOf(".") + 1, nCad.length());
      
      if(nCad.length() != 2)
      {
        String szTemp = new String("");
        
        for(int j = nCad.length(); j < 2; j++)
          szTemp = szTemp + "0";
          
        newCadNum = newCadNum + szTemp;
      }
    }

    StringBuffer nCadNum = new StringBuffer(newCadNum);
    for(int i = 0; i < nCadNum.length(); i++)
      if(nCadNum.charAt(i) == ',' || nCadNum.charAt(i) == '.')
        nCadNum.deleteCharAt(i);

    return nCadNum.toString();
  }

  private String fillWithZeroes(String newString)
  {
    int Indice = newString.indexOf(".");
    for(int i = Indice; i < Indice + 1; i++)
      newString = newString + "0";

    return newString;
  }

  private String setFormat(String newCadNum)
  {
    int iPIndex = newCadNum.indexOf(".");
    String nCadNum = new String(newCadNum.substring(0, iPIndex));
    for (int i = 0; i < (int)((nCadNum.length()-(1+i))/3); i++)
      nCadNum = nCadNum.substring(0, nCadNum.length()-(4*i+3))+','+nCadNum.substring(nCadNum.length()-(4*i+3));

    return nCadNum + newCadNum.substring(iPIndex, newCadNum.length());
  }

  private String setPointToString(String newCadNum)
  {
    int iPIndex = newCadNum.indexOf("."), iLong;
    String szTemp;

    if(iPIndex > 0)
    {
      newCadNum = new String(newCadNum + "00");
      newCadNum = newCadNum.substring(0, iPIndex + 1) + newCadNum.substring(iPIndex + 1, iPIndex + 3);
    } else {
      for(int i = newCadNum.length(); i < 3; i++)
        newCadNum = "0" + newCadNum;
      iLong = newCadNum.length();
      
      if(iLong == 3)
        szTemp = newCadNum.substring(0, 1);
      else
        szTemp = newCadNum.substring(0, iLong - 2);
        
      newCadNum = szTemp + "." + newCadNum.substring(iLong - 2);
    }

    return newCadNum;
  }


  private String Convert(String newCadNum, int Pos)
  {
    String Regresa = new String("");
    String[] Auxiliar = {"Cero", "Un ", "Dos ", "Tres ", "Cuatro ", "Cinco ", "Seis ", "Siete ", "Ocho ", "Nueve ", "Diez ",
                         "Once ", "Doce ", "Trece ", "Catorce ", "Quince ", "Dieciseis ", "Diecisiete ", "Dieciocho ", "Diecinueve ", "Veinte ",
                         "Veintiun ", "Veintidos ", "Veintitres ", "Veinticuatro ", "Veinticinco ", "Veintiseis ", "Veintisiete ", "Veintiocho ", "Veintinueve "};
    String[] Decenas  = {"", "Diez ", "Veinte ", "Treinta ", "Cuarenta ", "Cincuenta ", "Sesenta ", "Setenta ", "Ochenta ", "Noventa "};
    String[] Centenas = {"", "Ciento ", "Doscientos ", "Trescientos ", "Cuatrocientos ", "Quinientos ", "Seiscientos ", "Setecientos ", "Ochocientos ", "Novecientos "};
    String[] Millares = {"", "Mil ", "Millon(es) ", "Mil Millones "};

    Integer Num = new Integer("0");
    int Ban = 0;

    for(int i = newCadNum.length(); i < 3; i++)
      newCadNum = "0" + newCadNum;

    if(newCadNum.substring(0,1).equals("1") && newCadNum.substring(1,3).equals("00"))
      Regresa = "Cien ";
    else
      Regresa = Centenas[Num.parseInt(newCadNum.substring(0,1))];
      
    if(Num.parseInt(newCadNum.substring(1,3)) < 30)
    {
      Ban = 1;
      Regresa = Regresa + Auxiliar[Num.parseInt(newCadNum.substring(1,3))];
    } else
      Regresa = Regresa + Decenas[Num.parseInt(newCadNum.substring(1,2))];
      
    if(Num.parseInt(newCadNum.substring(2,3)) > 0 && Ban == 0)
      Regresa = Regresa + "y " + Auxiliar[Num.parseInt(newCadNum.substring(2,3))];

    return Regresa + Millares[Pos];
  }

  private String extractHundreds(String newCadNum, String Currency)
  {
    String New, Cadena = "";
    int Pos = 0, Parte;

    int iPIndex = newCadNum.indexOf(".");
    String newCadNum1 = new String(newCadNum.substring(0, iPIndex));
    Parte = (int) newCadNum1.length() / 3;
    
    if(newCadNum.length() % 3 == 0)
      Parte--;
      
    for (;Pos < newCadNum1.length();)
    {
      if(Pos == 0)
      {
        Pos = newCadNum1.length() - ((int)(newCadNum1.length() / 3)) * 3;
        
        if(Pos == 0 && newCadNum.length() < 4)
          Pos = newCadNum.length();
        else if(Pos == 0 && newCadNum.length() > 4)
          Pos = 3;
          
        New = newCadNum1.substring(0, Pos);
      } else {
        New = newCadNum1.substring(Pos, Pos+3);
        Pos+=3;
      }
      Cadena = Cadena + Convert(New, Parte);
      Parte--;
    }
    return Cadena + " " + Currency + " " + "(" + newCadNum.substring(iPIndex + 1, newCadNum.length()) + "/100)." ;
  }
%>
<%
  Vector resp = (Vector)session.getAttribute("response");
  Hashtable datasession = (Hashtable)session.getAttribute("page.datasession");
%>
<html>
<head>
  <title>Certificación</title>
<style type="text/css">
  Body {font-family: Arial, Helv; font-size: 10pt; font-weight: bold}
</style>
</head>
<body>
<p>
<%
   String szResp = new String("");
   if( resp.size() != 4 )
     out.println("<b>Error de conexi&oacute;n</b><br>");
   else
   {
     try
     {
       int codigo = Integer.parseInt((String)resp.elementAt(0));
       
       if( codigo != 0 )
         out.println("<b>Error en transaccion</b><br>");
         
       int lines = Integer.parseInt((String)resp.elementAt(1));
       szResp = (String)resp.elementAt(3);
       
       for(int i=0; ; i++)
       {
         String line = szResp.substring( i*78, Math.min(i*78+77, szResp.length()) );
         if(Math.min(i*78+77, szResp.length()) == szResp.length())
           break;
       }
     } catch(NumberFormatException nfe) {
       out.println("<b>Error de conexi&oacute;n</b><br>");
     }
   }
%>
<%
 Hashtable htBinfo = (Hashtable)session.getAttribute("branchinfo");
 String montoComision = getItem(szResp,3);
 String porcentajeIVA = getItem(szResp,4);
 String monto = delCommaPointFromString((String)datasession.get("txtMonto"));
 montoComision = delLeadingfZeroes(montoComision);
 Long montoIVA = new Long((Long.parseLong(montoComision) * Long.parseLong(porcentajeIVA)) / 100);
 Long montoTotal = new Long(Long.parseLong(monto)+ Long.parseLong(montoComision)  + montoIVA.longValue());
 Long montoCOMeIVA = new Long(Long.parseLong(montoComision)  + montoIVA.longValue());
 datasession.put("montoComision", montoComision);
 datasession.put("montoIVA", montoIVA.toString());
 datasession.put("montoTotal", montoTotal.toString());
 String txtRFC = new String((String)datasession.get( "txtRFC01" ));
 txtRFC = txtRFC.toUpperCase();
 String txtIVA = (String)datasession.get( "txtImporteIVA" );
 String nomicli = new String("N.D.");
 String domicli = new String("N.D.");
 
 if(datasession.get("txtNombre") != null)
   nomicli = datasession.get("txtNombre").toString();
   
 if(datasession.get("txtDomici") != null)
   domicli = (String)datasession.get("txtDomici");
   
 session.setAttribute("page.datasession",datasession);
%>
<form name="Txns" action="../../servlet/ventanilla.PageServlet" method="post">
<b><i>Recibo de COBRO de COMISIONES</i></b><br>
<table border="0" cellspacing="0">
 <tr>
  <td>Domicilio Sucursal:</td><td width="10">&nbsp;</td><td><%=datasession.get("domiSuc")%></td>
 </tr>
 <tr>
  <td>Nombre Cliente:</td><td width="10">&nbsp;</td><td><%=nomicli%></td>
 </tr>
 <tr>
  <td>R.F.C.:</td><td width="10">&nbsp;</td><td><%=txtRFC%></td>
 </tr>
 <tr>
  <td>Domicilio Cliente:</td><td width="10">&nbsp;</td><td><%=domicli%></td>
 </tr>
 <tr>
  <td>Servicio:</td><td width="10">&nbsp;</td><td>CHEQUES</td>
 </tr>
 <tr>
  <td>Concepto:</td><td width="10">&nbsp;</td>
   <td>
    <table border="0" cellspacing="0">
     <tr>
      <td>Comisión:</td><td width="10">&nbsp;</td><td align="right"><%=setFormat(setPointToString(montoComision))%></td>
     </tr>
     <tr>
      <td>IVA:</td><td width="10">&nbsp;</td><td align="right"><%=setFormat(setPointToString(montoIVA.toString()))%></td>
     </tr>
     <tr>
      <td>Total:</td><td width="10">&nbsp;</td><td align="right"><%=setFormat(setPointToString(montoCOMeIVA.toString()))%></td>
     </tr>
    </table>
   </td>
 </tr>
 <tr>
  <td>Importe en letra:</td><td width="10">&nbsp;</td><td><b><i><%=extractHundreds(setPointToString(montoCOMeIVA.toString()), "Pesos")%></i><b></td>
 </tr>
</table>
<%
Stack flujotxn = (Stack)session.getAttribute("page.flujotxn");

if(!flujotxn.empty())
{
  out.print("<div align=\"center\">");
  out.print("<input type=\"image\" src=\"../imagenes/b_continuar.gif\" alt=\"\" border=\"0\">");
  out.print("</div>");
}
%>
  <input type="hidden" name="transaction" value="<%= session.getAttribute("page.iTxn") %>">
  <input type="hidden" name="transaction1" value="<%= session.getAttribute("page.oTxn") %>">
</form>
</body>
</html>

<!-- Termina ComIVAFmt.jsp -->
