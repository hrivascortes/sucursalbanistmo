<!--
//*************************************************************************************************
//             Funcion: JSP que presenta las opciones para Vouchers
//            Elemento: PageBuilderVouchers.jsp 
//          Creado por: Alejandro Gonzalez Castro
//      Modificado por: Juvenal R. Fernandez V
//*************************************************************************************************
//CCN - 4360268 - 18/02/2005 - Se realiza cambio para evitar problema con la fecha Juliana
//CCN - 4360274 - 16/02/2005 - Se realiza cambio para evitar problema con la fecha Juliana
// CCN - 4360581 - 12/03/2007 - Se convierten los estilos en jsf y se hace un include en el jsp
//*************************************************************************************************
-->

<%@ page session="true" import="java.util.*"%>
<%
response.setHeader("Cache-Control","no-store"); //HTTP 1.1
response.setHeader("Pragma","no-cache"); //HTTP 1.0
response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<%
String MontoPro = new String("");
Vector lcs = (Vector)session.getAttribute("page.listcampos");
Vector lct = (Vector)session.getAttribute("page.listcont");
String txnAuthLevel = (String)session.getAttribute("page.txnAuthLevel");
String txnIMGAutoriz = (String)session.getAttribute("page.txnIMGAutoriz");
Vector v = (Vector)session.getAttribute("page.specialacct");
Hashtable data = (Hashtable)session.getAttribute("page.datasession");
int k = 0;
int aux = 1;
%>
<%!
   private String CalendarToString(int option, int sum)
   {
    java.util.Calendar now = java.util.Calendar.getInstance();
    String temp = new Integer(now.get(option) + sum).toString();
    if( temp.length() != 2 )
      temp = "0" + temp;
    return temp;
  }
%>

<html>
<head>
  <title>Datos de la Transaccion</title>
  <%@include file="style.jsf"%>
<script>
function FormatCurr(val, field)
{
//alert ("valor " + val);
//alert("field" + field);
  var num = val.toString().replace(/\$|\,/g,'')

  intVal = num.substring(0, num.indexOf("."))
  decVal = num.substring(num.indexOf(".") + 1, num.length)
  num = intVal + decVal;

 // alert(num);
  if( isNaN(num) )
  {
    return false
  }

  cents = Math.floor((num)%100)
  num = Math.floor((num)/100).toString()

  if(cents < 10)
    cents = "0" + cents;
  if(cents.length == 1)
    cents = cents + "0";

  for (var i = 0; i < Math.floor((num.length-(1+i))/3); i++)
    num = num.substring(0,num.length-(4*i+3))+','+num.substring(num.length-(4*i+3))

  num = num + '.' + cents
//  alert("document.entry."+field+".value = "+ num)
  eval("document.entry."+field+".value = '"+ num + "'");
}

function getCurrentDate(field)
{
  var dDay = <%=CalendarToString(java.util.Calendar.DAY_OF_MONTH, 0)%>
  var dMonth = <%=CalendarToString(java.util.Calendar.MONTH, 0)%>
  dMonth = dMonth + 1;
  if(dMonth < 10)
   dMonth = '0' + dMonth;
  var dYear = new String('<%=CalendarToString(java.util.Calendar.YEAR, 0)%>')
  if(dDay < 10)
   dDay = '0' + dDay
  dYear = dYear.substring(1,5)
  var fecha = dDay + '' + dMonth + '' + dYear;

//	alert (fecha);
  eval("document.entry."+field+".value = fecha");
  return;
 }

function juldate(nfield)
{
	var fecha = eval("document.entry."+nfield+".value");
	var DD = fecha.substring(0,2);
	var MM = fecha.substring(2,4);
	var YY = fecha.substring(4,fecha.length);
	DD = Number(DD);
	MM = Number(MM);
	YY = Number(YY);

//	alert("dia: " + DD + " mes "+ MM + " year " + YY );
	//HR=eval(form.nhour.value)
	//MN=eval(form.nminute.value)

	with (Math)
	{
//      HR = 12 + (MN / 60);
     	GGG = 1;
      	if (YY <= 1585) GGG = 0;
	    	JD = -1 * floor(7 * (floor((MM + 9) / 12) + YY) / 4);
      	S = 1;
	  	if ((MM - 9)<0) S=-1;
      		A = abs(MM - 9);

      	J1 = floor(YY + S * floor(A / 7));
      	J1 = -1 * floor((floor(J1 / 100) + 1) * 3 / 4);
      	JD = JD + floor(275 * MM / 9) + DD + (GGG * J1);
        JD = JD + 1721027 + 2 * GGG + 367 * YY - 0.5;
//     	JD = JD + 1721027 + 2 * GGG + 367 * YY;
        JD = JD + (12 / 24);
    }
//	alert ("fecha juliana " + JD);
	return JD;
}

// 2004-03-26 ARM: Se agrego esta funcion para que solamente valide la fecha cuando despues de que se presione <ENTER>
function evaldateKeyPressed(field)
{
  if (window.event.keyCode == "13")
  {
    field.blur();
  }
}

function evaldate(field)
{
	var txn = document.entry.cTxn.value;

	var gField = eval("document.entry." + field.name);
//	alert(gField);
	fechacap = juldate(gField.name);
//	alert("fecha capturada" + fechacap);
	fechahoy = juldate('txtFechaSys');
//	alert("fecha sys" + fechahoy);

	var fechalim = fechahoy - 20;
	//	alert("fecha limite: " + fechalim);

	if (!(txn == "0073"))
	{
		if((fechacap > fechalim) && (fechacap <= fechahoy))
		{
	//		alert ("fecha correcta");
			return true;
		}
		else
		{
			alert ("Fecha fuera de rango. (Hasta 19 d�as anteriores a la fecha del Sistema)");
			gField.value = document.entry.txtFechaSys.value;
			gField.focus();
			return false;
		}
	}
	else
	{
		var fechalimi = fechalim - 365;
		if((fechacap > fechalimi) && (fechacap <= fechalim))
		{
//			alert ("fecha correcta");
			return true;
		}
		else
		{
			alert ("Fecha fuera de rango. (De 19 d�as anteriores a la fecha del Sistema hasta 365 d�as antes)");
			gField.value = document.entry.txtFechaSys.value;
			gField.focus();
			return false;
		}
	}
}

function validate(form)
{
<%
	String ndevs = (String)data.get("txtNoDevs");
	int devs = Integer.parseInt(ndevs);
	for (int i = 1; i <devs+1;i++)
    {
      out.println("  if( !top.validate(window, document.entry.tarjetaD" + i + ", 'isTDC') )");
      out.println("    return");
      out.println("  if( !top.validate(window, document.entry.autoD" + i + ", 'isnumeric') )");
      out.println("    return");
      out.println("  if( !evaldate(document.entry.fechaD" + i + ") )");
      out.println("    return");
      out.println("  if( !top.validate(window, document.entry.montoD" + i + ", 'isValMto') )");
      out.println("    return");
    }
	String nvtas = (String)data.get("txtNoVtas");
    int vtas = Integer.parseInt(nvtas);
	for (int i = 1; i <vtas+1;i++)
    {
      out.println("  if( !top.validate(window, document.entry.tarjetaV" + i + ", 'isTDC') )");
      out.println("    return");
      out.println("  if( !top.validate(window, document.entry.autoV" + i + ", 'isnumeric') )");
      out.println("    return");
      out.println("  if( !evaldate(document.entry.fechaV" + i + ") )");
      out.println("    return");
      out.println("  if( !top.validate(window, document.entry.montoV" + i + ", 'isValMto') )");
      out.println("    return");
    }
   out.println("  form.submit()");
   out.println("var obj = document.getElementById('btnCont');");
   out.println("obj.onclick = new Function('return false');");   
%>
}
</script>
</head>
<body bgcolor="#FFFFFF" onload="top.setFieldFocus(window.document)">
<br>
<form name="entry" action="../../servlet/ventanilla.DataServlet" method="post"
<%   if(txnAuthLevel.equals("1"))
    out.print("onSubmit=\"return validate(this)\"");
%>
>

<h1><%=session.getAttribute("page.txnImage")%></h1>
<h3>
<%
String txn = (String)session.getAttribute("page.iTxn");
if( txn.equals("0066"))
	out.print(txn + "   " + "Dep�sito / Devoluciones Vouchers TDC");
if( txn.equals("0072"))
	out.print(txn + "   " + "Dep�sito / Devoluciones Aclaraciones Vouchers TDC");
if( txn.equals("0073"))
	out.print(txn + "   " + "Dep�sito / Devoluciones Extempor�neas Vouchers TDC");
%>
</h3>
<%
if(devs != 0)
{%>
	<table cellspacing="0" cellpadding="0" border="0">
	<tr><td colspan="9" align="center"><h3>CAPTURA DE DEVOLUCIONES</h3></td></tr>

	<tr>
	<td colspan="2"></td>
	<td align="center"><h3>No. Devoluciones :</h3></td><td>&nbsp;&nbsp;</td>
	<td align="center"><h3><%= data.get("txtNoDevs")%></h3></td>
	<td colspan ="4"></td>
	</tr>

	<tr align="center">
   		<td>&nbsp;&nbsp;</td><td>&nbsp;&nbsp;</td>
   		<td><b>N&uacute;mero<br>de Tarjeta</b></td><td>&nbsp;&nbsp;</td>
   		<td><b>N&uacute;mero<br>Autorizaci&oacute;n</b></td><td>&nbsp;&nbsp;</td>
   		<td><b>Fecha<br>DDMMAAAA</b></td><td>&nbsp;&nbsp;</td>
   		<td><b>Monto</b></td>
	</tr>

	<%
	
	for (int i = 1; i <devs+1;i++)
	{%>
	<tr>
	<td><%=i%>.-</td><td>&nbsp;&nbsp;</td>                          
	<td><input type="text" name="tarjetaD<%=i%>" size="16" tabIndex="<%=(aux++)%>" maxlength="16" onBlur="top.estaVacio(this) || top.validate(window, this, 'isTDC')" onKeyPress="top.keyPressedHandler(window, this, 'isTDC')"></td>
	<td>&nbsp;&nbsp;</td>
	<td align="center"><input type="text" name="autoD<%=i%>" size="6" tabIndex="<%=(aux++)%>" maxlength="6" onBlur="top.estaVacio(this) || top.validate(window, this, 'isnumeric')"  onKeyPress="top.keyPressedHandler(window, this, 'isnumeric')"></td>
	<td>&nbsp;&nbsp;</td>   
	<td><input type="text" name="fechaD<%=i%>" size="8" tabIndex="<%=(aux++)%>" maxlength="8" onBlur="evaldate(this)"  onKeyPress="evaldateKeyPressed(this)"></td>
	<script>getCurrentDate('fechaD<%=i%>');</script>
	<td>&nbsp;&nbsp;</td>
	<td><input type="text" name="montoD<%=i%>" size="17" tabIndex="<%=(aux++)%>" maxlength="13"  onBlur="top.estaVacio(this) || top.validate(window, this, 'isValMto')"  onKeyPress="top.keyPressedHandler(window, this, 'isValMto')"></td>
	</tr>
	<%
	}
	%>
	<tr>
	<td colspan="6"></td>
	<td><b>Acumulado :</b></td><td>&nbsp;&nbsp;</td><td><input type="text" tabIndex="<%=(aux++)%>" name="txtAcumD" size="17" maxlength="13" value="0.00" onChange= "top.validate(window, this, 'notChange')"></td>
	</tr>

	<tr>
	<td colspan="6"></td>
	<td><b>Total :</b></td><td>&nbsp;&nbsp;</td><td align="right"><b><%= data.get("txtMtoDevs")%></b></td>
	</tr>
	</table><br><br>
<%
}
if(vtas != 0)
{%>

	<table cellspacing="0" cellpadding="0" border="0">
	<tr><td colspan="9" align="center"><h3>CAPTURA DE VENTAS</h3></td></tr>

	<tr>
		<td colspan="2"></td>
		<td	align="center"><h3>No. Ventas :</h3></td><td>&nbsp;&nbsp;</td>
		<td align="center"><h3><%= data.get("txtNoVtas")%></h3></td>
		<td colspan ="4"></td>
	</tr>

	<tr align="center">
   		<td>&nbsp;&nbsp;</td><td>&nbsp;&nbsp;</td>
	    <td><b>N&uacute;mero<br>de Tarjeta</b></td><td>&nbsp;&nbsp;</td>
	    <td><b>N&uacute;mero<br>Autorizaci&oacute;n</b></td><td>&nbsp;&nbsp;</td>
   		<td><b>Fecha<br>DDMMAAAA</b></td><td>&nbsp;&nbsp;</td>
    	<td><b>Monto</b></td>
	</tr>

	<%
	
	for (int i = 1; i <vtas+1;i++)
	{%>
	<tr><td><%=i%>.-</td><td>&nbsp;&nbsp;</td>
	<td><input type="text" name="tarjetaV<%=i%>" size="16" tabIndex="<%=(aux++)%>" maxlength="16" onBlur="top.estaVacio(this) || top.validate(window, this, 'isTDC')" onKeyPress="top.keyPressedHandler(window, this, 'isTDC')"></td>
	<td>&nbsp;&nbsp;</td>
	<td align="center"><input type="text" name="autoV<%=i%>" size="6" tabIndex="<%=(aux++)%>" maxlength="6" onBlur="top.estaVacio(this) || top.validate(window, this, 'isnumeric')"  onKeyPress="top.keyPressedHandler(window, this, 'isnumeric')"></td>
	<td>&nbsp;&nbsp;</td>
	<td><input type="text" name="fechaV<%=i%>" size="8" tabIndex="<%=(aux++)%>" maxlength="8" onBlur="evaldate(this)"  onKeyPress="evaldateKeyPressed(this)"></td>
	<script>getCurrentDate('fechaV<%=i%>');</script>
	<td>&nbsp;&nbsp;</td>
	<td><input type="text" name="montoV<%=i%>" size="17" tabIndex="<%=(aux++)%>" maxlength="13"  onBlur="top.estaVacio(this) || top.validate(window, this, 'isValMto')"  onKeyPress="top.keyPressedHandler(window, this, 'isValMto')"></td>
	</tr>
	<%}
	%>
	<tr>
	<td colspan="6"></td>
	<td><b>Acumulado :</b></td><td>&nbsp;&nbsp;</td><td><input type="text" name="txtAcumV" size="17" tabIndex="<%=(aux++)%>" maxlength="13" value="0.00" onChange="top.validate(window, this, 'notChange')"></td>
	</tr>

	<tr>
	<td colspan="6"></td>
	<td><b>Total :</b></td><td>&nbsp;&nbsp;</td><td align="right"><b><%= data.get("txtMtoVtas")%></b></td>
	</tr>
	</table><br><br>
<%
}%>

<table cellspacing="0" cellpadding="0" border="0">
<tr><td colspan="5" align="center"><h3>RESUMEN</h3></td></tr>

<tr><td><b>N&uacute;mero de Cuenta :</b></td><td>&nbsp;&nbsp;</td>
<%String Txn = (String)data.get("cTxn");
  if(Txn.equals("0066")){%>
  		<td align="right"><%= session.getAttribute("cta")%></td>
<%}else{%>
		<td align="right"><input type="text" name="cuenta" size="10" tabIndex="<%=(aux++)%>" maxlength="10" value="<%= session.getAttribute("cta")%>" onBlur="top.estaVacio(this) || top.validate(window, this, 'isValidAcct')" onKeyPress="top.KeyPressedHandler(window, this, 'isValidAcct')"></td>
<%}%>
<td colspan="2"></td></tr>

<tr><td><b>N&uacute;mero de Negocio :</b></td><td>&nbsp;&nbsp;</td>
<td align="right"><%= data.get("txtNoNegocio")%></td><td colspan="2"></td></tr>

<tr><td><b>D&iacute;as SBC :</b></td><td>&nbsp;&nbsp;</td>
<td align="right"><%= session.getAttribute("sbc")%></td><td colspan="2"></td></tr>

<tr><td>&nbsp;&nbsp;</td></tr>

<tr align="center"><td colspan="2">&nbsp;</td><td><b>DEVOLUCIONES</b></td><td>&nbsp;&nbsp;</td><td><b>VENTAS</b></td></tr>

<tr><td><b>N&uacute;mero:</b></td><td>&nbsp;&nbsp;</td>
    <td align="center"><%= data.get("txtNoDevs")%></td><td>&nbsp;&nbsp;</td>
	<td align="center"><%= data.get("txtNoVtas")%></td>
</tr>

<tr><td><b>Monto :</b></td><td>&nbsp;&nbsp;</td>
    <td align="left"><input type="text" name="montoD" size="17" tabIndex="<%=(aux++)%>" maxlength="13" value="<%= data.get("txtMtoDevs")%>" onChange="top.validate(window, this, 'notChange')"></td>
	<td>&nbsp;&nbsp;</td>
    <td align="left"><input type="text" name="montoV" size="17" tabIndex="<%=(aux++)%>" maxlength="13" value="<%= data.get("txtMtoVtas")%>" onChange="top.validate(window, this, 'notChange')"></td>
</tr>

<%
    StringBuffer montod = new StringBuffer((String)data.get("txtMtoDevs") );
    for(int i=0; i<montod.length();)
    {
      if( montod.charAt(i) == ',' || montod.charAt(i) == '.' )
        montod.deleteCharAt(i);
      else
        ++i;
    }
	String tmp1 = montod.toString();
	long mtod = Long.parseLong(tmp1);

    StringBuffer montov = new StringBuffer((String)data.get("txtMtoVtas") );
    for(int i=0; i<montov.length();)
    {
      if( montov.charAt(i) == ',' || montov.charAt(i) == '.' )
        montov.deleteCharAt(i);
      else
        ++i;
    }
	String tmp2 = montov.toString();
    long mtov = Long.parseLong(tmp2);

	long sumd = 0;
	long sumv = 0;
	long comd = 0;
	long comv = 0;
	long ivad = 0;
	long ivav = 0;
	long iva = 0;
	String tasaiva = (String)session.getAttribute("iva");
	iva = Long.parseLong(tasaiva);

	if (mtod > 0.0)
	{
		String comdevs = (String)session.getAttribute("comdev");
		comd = Long.parseLong(comdevs);
		comd = mtod * comd / 10000;
    	ivad = ( comd * iva ) / 100;
	    sumd = mtod + comd + ivad;
	}
	if (mtov > 0.0)
	{
		String comvtas = (String)session.getAttribute("comvta");
		comv = Long.parseLong(comvtas);
		comv = mtov * comv / 10000;
		ivav = ( comv * iva ) / 100;
    	sumv = mtov - comv - ivav;
	}
%>
<tr><td><b>Comisi&oacute;n :</b></td><td>&nbsp;&nbsp;</td>
<% if(Txn.equals("0066")){%>
    <td align="left"><input type="text" name="ComDev" size="17" tabIndex="<%=(aux++)%>" maxlength="13"  onChange="top.validate(window, this, 'notChange')"></td>
<%}else{%>
    <td align="left"><input type="text" name="ComDev" size="17" tabIndex="<%=(aux++)%>" maxlength="13"  onBlur="top.estaVacio(this) || top.validate(window, this, 'isCurrency')" onKeyPress="top.KeyPressedHandler(window, this, 'isCurrency')"></td>
<%}%>
	<script>FormatCurr(<%= comd%>, 'ComDev')</script>
	<td>&nbsp;&nbsp;</td>
<% if(Txn.equals("0066")){%>
    <td align="left"><input type="text" name="ComVta" size="17" tabIndex="<%=(aux++)%>" maxlength="13"  onChange="top.validate(window, this, 'notChange')"></td>
<%}else{%>
    <td align="left"><input type="text" name="ComVta" size="17" tabIndex="<%=(aux++)%>" maxlength="13"  onBlur="top.estaVacio(this) || top.validate(window, this, 'isCurrency')" onKeyPress="top.KeyPressedHandler(window, this, 'isCurrency')"></td>
<%}%>
	<script>FormatCurr(<%= comv%>, 'ComVta')</script>
</tr>
<tr>
	<td><b>I.V.A. :</b></td><td>&nbsp;&nbsp;</td>
    <td align="left"><input type="text" name="IVAD" size="17" tabIndex="<%=(aux++)%>" maxlength="13" onChange="top.validate(window, this, 'notChange')"></td>
	<script>FormatCurr(<%= ivad%>, 'IVAD')</script>
    <td>&nbsp;&nbsp;</td>
    <td align="left"><input type="text" name="IVAV" size="17" tabIndex="<%=(aux++)%>" maxlength="13" onChange="top.validate(window, this, 'notChange')"></td>
	<script>FormatCurr(<%= ivav%>, 'IVAV')</script>
</tr>
<tr><td><b>Total :</b></td><td>&nbsp;&nbsp;</td>
    <td><input type="text" name="montoTD" size="18" tabIndex="<%=(aux++)%>" maxlength="14" onChange="top.validate(window, this, 'notChange')"></td>
	<script>FormatCurr(<%= sumd%>, 'montoTD')</script>
    <td>&nbsp;&nbsp;</td>
    <td><input type="text" name="montoTV" size="18" tabIndex="<%=(aux++)%>" maxlength="14" onChange="top.validate(window, this, 'notChange')"></td>
	<script>FormatCurr(<%= sumv%>, 'montoTV')</script>
</tr>
</table>
<br><br>
<input type="hidden" name="iTxn" value="<%= session.getAttribute("page.iTxn") %>">
<input type="hidden" name="oTxn" value="<%= session.getAttribute("page.oTxn") %>">
<input type="hidden" name="cTxn" value="<%= session.getAttribute("page.oTxn") %>">
<input type="hidden" name="sucursal" value="<%= session.getAttribute("branch") %>">
<input type="hidden" name="compania" value="20">
<input type="hidden" name="txtFechaSys"><script>getCurrentDate('txtFechaSys');</script>
<input type="hidden" name="MTD"><script>FormatCurr(<%= sumd%>, 'MTD')</script>
<input type="hidden" name="MTV"><script>FormatCurr(<%= sumv%>, 'MTV')</script>
<input type="hidden" name="ivaD"><script>FormatCurr(<%= ivad%>, 'ivaD')</script>
<input type="hidden" name="ivaV"><script>FormatCurr(<%= ivav%>, 'ivaV')</script>
<input type="hidden" name="mtoD" value="<%= data.get("txtMtoDevs")%>">
<input type="hidden" name="mtoV" value="<%= data.get("txtMtoVtas")%>">
<input type="hidden" name="cD"><script>FormatCurr(<%= comd%>, 'cD')</script>
<input type="hidden" name="cV"><script>FormatCurr(<%= comv%>, 'cV')</script>
<input type="hidden" name="noD" value="<%= data.get("txtNoDevs")%>">
<input type="hidden" name="noV" value="<%= data.get("txtNoVtas")%>">
<input type="hidden" name="dsbc" value="<%= session.getAttribute("sbc")%>">

<%    out.println("<a id='btnCont' tabIndex=\"" + (aux++) + "\" href=\"javascript:validate(document.entry)\"><img src=\"../imagenes/b_continuar.gif\" border=\"0\"></a>");%>
<%
session.setAttribute("montod",data.get("txtMtoDevs"));
session.setAttribute("montov",data.get("txtMtoVtas"));
session.setAttribute("sumads",Long.toString(sumd));
session.setAttribute("sumavs",Long.toString(sumv));
session.setAttribute("ivads",Long.toString(ivad));
session.setAttribute("ivavs",Long.toString(ivav));
session.setAttribute("comds",Long.toString(comd));
session.setAttribute("comvs",Long.toString(comv));
%>
</form>
</body>
</html>
