<!--
//*************************************************************************************************
//            Elemento: transaccionejecutar.jsp
//             Funcion: JSP para la caja de acceso rapido
//          Creado por: Alejandro Gonzalez Castro
//      Modificado por: Jes�s Emmanuel L�pez Rosales
//*************************************************************************************************
// CCN -4360169- 12/07/2004 - Se corrige problema de impresion del Diario en sesion
// CCN -4360210- 30/09/2004 - Se verifican transacciones disponibles por nivel
// CCN - 4360275 - 18/02/2005 - WAS 5.1
// CCN - 4360510 - 08/09/2006 - valida que la txn a ejecutar pertenezca al perfil
// CCN - 4360581 - 12/03/2007 - Se convierten los estilos en jsf y se hace un include en el jsp
//*************************************************************************************************
-->
<%@ page session="true"
import="ventanilla.com.bital.beans.Menues,java.util.*,ventanilla.com.bital.beans.DatosVarios,ventanilla.com.bital.beans.Monedas"%>
<% 
response.setHeader("Cache-Control","no-store"); //HTTP 1.1
response.setHeader("Pragma","no-cache"); //HTTP 1.0
response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
%>
<%
  Menues menues = Menues.getInstance();
  Vector menurs = (Vector)menues.getMenues("S");
  String tellerID = "";
  String tellerlevel = "";
  String idPerfil = "";
  if(session.getAttribute("userinfo") != null)
  {
    java.util.Hashtable userInfo = (java.util.Hashtable)session.getAttribute("userinfo");
    tellerID = userInfo.get("C_CAJERO").toString();
    tellerlevel = userInfo.get("N_NIVEL").toString();
    idPerfil = session.getAttribute("tellerperfil").toString();
  }
  
  DatosVarios pertxn = DatosVarios.getInstance();            
  Hashtable hpertxn = pertxn.getDatosH("PERFILT");
  Vector vpertxn = new Vector();
  vpertxn =(Vector)hpertxn.get(idPerfil);
%>
<html>
 <head>
  <title>Panel de Captura</title>
	<%@include file="style.jsf"%>
  <script type="text/javascript" language="javascript">
<!--
 var aTxns = new Array()
 var aAvisos = new Array()
 var aTxnsPerf = new Array()
 aAvisos[0] = 'Por favor digite una transacci�n.'
 aAvisos[1] = 'Tipo de moneda inv�lido para la transacci�n.'
 aAvisos[2] = 'La transacci�n no puede ser localizada.'
 aAvisos[3] = 'Por favor verifique!'
 aAvisos[4] = 'Si el problema persiste intente desde\nel men� de transacciones.'
<%
 int j=0;
  for(int i = 0; i < menurs.size(); i++)
    {
   Vector tempmenu = (Vector)menurs.get(i);
   String niveltxn = tempmenu.get(4).toString();
   int nivtxn = Integer.parseInt(niveltxn);
   int nivtell = Integer.parseInt(tellerlevel);

   if(nivtxn <= nivtell)
   {
    out.print(" aTxns[" + j + "] = new Array(");
    out.println("'"   + tempmenu.get(0) +
               "','" + tempmenu.get(1) +
                      "','" + tempmenu.get(2) +
                      "','" + tempmenu.get(5) +
                      "','" + tempmenu.get(6) +
                      "','" + tempmenu.get(4) + "')");
    j++;
   }
  }
  
  for(int i = 0; i < vpertxn.size() ; i++)
    {
     out.println("aTxnsPerf[" + i + "] = '" + vpertxn.elementAt(i) +"'");
   } 
%>

function LocalizarTransaccion(forma)
{
    var EspacioEncontrado = forma.transactionField.value.indexOf(" ")
    var i
    var TxnEncontrada = 1
    var monedaInvalida = 1;

    if(forma.transactionField.value.length == 0 || EspacioEncontrado != -1)
    {
        alert(aAvisos[0])
        forma.transactionField.value = ''
        forma.transactionField.focus()
        return
    }
    
	for(j = 0; j < aTxnsPerf.length; j++){
      if(aTxnsPerf[j] == forma.transactionField.value || aTxnsPerf[j] =="9999"){      
        i=0;
        banfuera = false;
	    while(banfuera == false && i < aTxns.length)
	    {        
		        if(forma.transactionField.value == aTxns[i][1])
		        {
		                TxnEncontrada = 0
		            if(forma.moneda.options[forma.moneda.selectedIndex].value == aTxns[i][2])
		            {
		                monedaInvalida = 0
		                banfuera = true;
		            }
		        }	           
		  i++;      
	    }
	  }
	}
  

    if(monedaInvalida != 0 && TxnEncontrada == 0)
    {
    alert(aAvisos[1] + '\n' + aAvisos[3])
    forma.transactionField.value = ''
    forma.moneda.selectedIndex = 0
    return
    }
  
    if(TxnEncontrada != 0){
   alert(aAvisos[2] + '\n' + aAvisos[3] + '\n' + aAvisos[4])
   forma.transactionField.value = ''
   forma.moneda.selectedIndex = 0
   return
  }
  else{
   if(forma.transactionField.value != '0011'){
    if(forma.transactionField.value == '5503')
    {
        forma.noservicios.value='1';
    }    
    forma.txnProcess.value = aTxns[i-1][0]
    forma.methood = 'post'
    forma.action = '../../servlet/ventanilla.PageServlet'
    forma.target = 'panel'
    forma.transaction.value = forma.transactionField.value
    
    forma.transactionField.value = ''
    forma.submit()
   }
   else{
    forma.tellerID.value = '<%=tellerID%>'
    forma.method='post'
    forma.action='PageBuilderTotalesC.jsp'
    forma.target='panel'
    forma.transactionField.value=''
    forma.submit()
   }
  }
 }
//-->
 </script>
 </head>
 <body bgcolor="#FFFFFF" text="#000000">
  <form name="TransaccionAislada" onsubmit="javascript:LocalizarTransaccion(document.TransaccionAislada);return false;">
   <table border="0" cellpadding="0" cellspacing="1" align="center width="100%">
    <tr>
     <td width="500">&nbsp;</td>
     <td align="center" valign="middle" class="normaltextblack">Transacci�n:</td>
     <td width="2">&nbsp;</td>
     <td align="center" valign="middle"><input type="text" name="transactionField" size="04" maxlength="04"></td>
     <td width="5">&nbsp;</td>
     <td align="center" valign="middle" class="normaltextblack"><select name="moneda" size="01">
     <%
     Monedas monedas = Monedas.getInstance();
     java.util.Vector vMonedas = monedas.getMonedas();
     int iMon = 0;
     for(iMon=0;iMon<vMonedas.size();iMon++)
     {
     		out.println("<option value=\""+(String)((Vector)vMonedas.elementAt(iMon)).elementAt(0)+"\">"+(String)((Vector)vMonedas.elementAt(iMon)).elementAt(1));	
     }
     %>
     </select></td>
     <td>&nbsp;&nbsp;<td>
     <td align="center" valign="middle"><a href="javascript:LocalizarTransaccion(document.TransaccionAislada);" onmouseover="self.status='Ir';return true;" onmouseout="window.status='';"><img src="../imagenes/im_ir.gif" border="0" alt="Ir"></a></td>
    </tr>
   </table>
   <input type="hidden" name="transaction" value="0000">
   <input type="hidden" name="txnaislada" value="yes">
   <input type="hidden" name="txnProcess" value="">
   <input type="hidden" name="tellerID" value="">
   <input type="hidden" name="noservicios">
   <input type="hidden" name="quickaccess" value="1">
  </form>
 </body>
</html>
