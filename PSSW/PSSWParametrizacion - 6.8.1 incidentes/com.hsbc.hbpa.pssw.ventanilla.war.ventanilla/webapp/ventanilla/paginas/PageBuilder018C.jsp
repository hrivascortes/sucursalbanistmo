<!--
//*************************************************************************************************
//             Funcion: JSP que presenta las opciones de la txn 018C
//            Elemento: PageBuilder018C.jsp
//          Creado por: Alejandro Gonzalez Castro
//      Modificado por: Juvenal R. Fernandez V.
//*************************************************************************************************
// CCN - 4360275 - 18/02/2005 - WAS 5.1
// CCN - 4360334 - 17/06/2005 - Se elimina variable dbQualifier
//*************************************************************************************************
-->

<%@ page session="true" import="java.util.*"%>
<%
response.setHeader("Cache-Control","no-store"); //HTTP 1.1
response.setHeader("Pragma","no-cache"); //HTTP 1.0
response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<%!
	private String formatMonto(String s)
	{
		int len = s.length();
 		if (len < 3)
    		s = "0." + s;
    	else
    	{
      	int n = (len - 3 ) / 3;
    		s = s.substring(0, len - 2) + "." + s.substring(len - 2 , len);
      	for (int i = 0; i < n; i++)
      	{
      		len = s.length();
    			s = s.substring(0, (len -(i*3+6+i))) + "," + s.substring((len -(i*3+6+i)) , len);
    		}
    	}
      for (int i = s.length(); i < 16 ; i++)
    		s = " " + s;
    	return s;
  	}

  private String delCeros(String s)
  {
    int len = 0;
    int space = 0;
    for(len = 0 ; len < s.length(); len++)
	  if(s.charAt(len) == '0')
	   space ++;
	  else
	   break;
    return s.substring(space, s.length());
  }

	private String formatFecha(String s)
	{
		String fechaAux;

		if (s.substring(0,1).equals("1"))
			fechaAux = "/20" + s.substring(1,3);
		else
			fechaAux = "/19" + s.substring(1,3);
		fechaAux = "/" + s.substring(3,5) + fechaAux;
		fechaAux = s.substring(5,7) + fechaAux;
    	return fechaAux;
  	}

	private String formatCuenta(String s)
	{
      for (int i = s.length(); i < 18 ; i++)
    		s = s + " ";
    	return s;
  	}

  	private Vector getDatos()
  	{
		Vector datos = new Vector();
		String cajero;
		String respuesta;
		String numOsn;
		int intVeces = 0;
		ventanilla.com.bital.admin.OperacionesDBBean obQuery = new ventanilla.com.bital.admin.OperacionesDBBean();
		String strAccion = new String("");
		strAccion = " SELECT D_OSN , D_RESPUESTA  FROM TW_CTRL_OPRE WHERE D_STATUS = 'C'";
		respuesta = obQuery.EjecutaQuery(strAccion);
		intVeces = (respuesta.length() / 452);
		for (int i = 0 ; i < intVeces ; i++)
		{
				datos.addElement(formatFecha(respuesta.substring((i*452) + 112 , (i*452) + 119)));             /* Fecha de recepcion       */
				datos.addElement(formatCuenta(delCeros(respuesta.substring((i*452) + 433 , (i*452) + 451))));  /* Cuenta                   */
				numOsn = respuesta.substring((i*452) , (i*452) + 6);
				datos.addElement(numOsn); 							                            							  /* Numero de Osn            */
				datos.addElement(formatFecha(respuesta.substring((i*452) + 208 , (i*452) + 215)));             /* Fecha Valor              */
				datos.addElement(respuesta.substring((i*452) + 215 , (i*452) + 218));                          /* Moneda                   */
				datos.addElement(formatMonto(delCeros(respuesta.substring((i*452) + 218 , (i*452) + 231))));   /* Monto de la transaccion	*/
				datos.addElement(respuesta.substring((i*452) + 412 , (i*452) + 428));                          /* TRN			               */
				datos.addElement(respuesta.substring((i*452) + 269 , (i*452) + 304));                          /* Nombre del beneficiario	*/
		}
		return datos;
	}
%>
<% Vector resp = (Vector)session.getAttribute("response"); %>
<html>
<head>
<title>Consulta/mantenimiento OPRE</title>
  <style type="text/css">
  Body {font-family: Courier; font-size: 8pt; font-weight: bold}
  </style>
</head>
<script language="JavaScript">
var valor = ""

function aceptar(forma)
{
	document.Txns.numOsn.value = valor;
	forma.submit();
}

function setValues()
{
	 valor = document.Txns.numOsn.value;
}

</script>
<body>
  <form name="Txns" action="../../servlet/ventanilla.DataServlet" method="post">
    <table border="0" cellspacing="0">
    <tr>
      <td>
        <input type="hidden" name="iTxn"   value="018C">
        <input type="hidden" name="numOsn" value="1">
        <input type="hidden" name="cTxn"  value="18C2">
        <input type="hidden" name="compania" value="20">
        <input type="hidden" name="page.getoTxnView" value="Response018C">
        <input type="hidden" name="CM" value="Consulta-Multiple">
        <input type="hidden" name="branch" value=<%out.print(session.getAttribute("branch"));%>>
      </td>
    </tr>
<%
   if( resp.size() != 4 )
   {
     out.println("<b>Error de conexi&oacute;n</b><br>");
   }
   else
   {
   	Vector datos = new Vector();
   	datos = getDatos();
      int contador = datos.size() /8 ;
	   out.println("    <tr>");
   	out.println("      <td><b><h5>&nbsp;&nbsp;RECEPCION&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CUENTA&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;OSN&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;F. VALOR&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MON&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MONTO&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TRN&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;BENEFICIARIO</h5></b></td>");
	   out.println("    </tr>");
	   out.println("    <tr>");
   	out.println("      <td>");
   	out.println("<FONT FACE = \"Courier\" SIZE = -6 >");
     	if (contador < 8 )
			out.println("        <SELECT NAME=numOsn SIZE = " + contador + " onClick=\"setValues()\">");
		else
			out.println("        <SELECT NAME=numOsn SIZE=8 onClick=\"setValues()\">");
     	for (int i=0 ; i < contador; i++ )
     	{
     		int k = 0;
		   String osnaux =   (String) datos.elementAt(2+(i*8));
		   String strFiller = "                    ";
		   osnaux = osnaux.trim();
		   String strMonto = (String) datos.elementAt(5+(i*8));
		   strMonto = strMonto.trim();
		   for( k=strMonto.length()-1; k < 18; k++)
		   	strMonto = "&nbsp;" + strMonto;
		   String strTrn = (String) datos.elementAt(6+(i*8));
		   for( k=strTrn.length()-1; k < 18; k++)
		   	strTrn =  strTrn + "&nbsp;";
		   String valoraux = (String) datos.elementAt(0+(i*8)) + "&nbsp;" + (String) datos.elementAt(1+(i*8)) + "&nbsp;" + (String) datos.elementAt(2+(i*8)) + "&nbsp;" + (String) datos.elementAt(3+(i*8)) + "&nbsp;" + (String) datos.elementAt(4+(i*8)) + "&nbsp;" + strMonto + "&nbsp;" + strTrn + "&nbsp;" + (String) datos.elementAt(7+(i*8));
			out.println("      <OPTION value=" + osnaux+ ">" + valoraux + "</OPTION>");
		}
		out.println("        </SELECT>");
		out.println("        </FONT>");
	   out.println("      </td>");
      out.println("    </tr>");
      if (contador > 0)
      {
      	out.println("    <tr>");
      	out.println("      <td><a href=\"JavaScript:aceptar(document.Txns) \"><img src=\"../imagenes/b_aceptar.gif\" border=\"0\"></a></td>");
      	out.println("    </tr>");
   	}
	}
%>
    </table>
  </form>
</body>
</html>
