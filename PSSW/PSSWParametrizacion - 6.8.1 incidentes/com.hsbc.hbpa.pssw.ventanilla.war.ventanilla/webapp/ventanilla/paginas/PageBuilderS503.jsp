<!--
//*************************************************************************************************
///            Funcion: JSP que despliega respuesta de txn RAP
//            Elemento: PageBuilderS503.jsp
//          Creado por: Alejandro Gonzalez Castro
//      Modificado por: Juvenal Fernandez
//*************************************************************************************************
// CCN - 4360267 - 31/01/2005 - Se modifica el acceso a los bines
// CCN - 4360581 - 12/03/2007 - Se convierten los estilos en jsf y se hace un include en el jsp
//*************************************************************************************************
-->

<%@ page session="true" import="java.util.*"%>
<%
response.setHeader("Cache-Control","no-store"); //HTTP 1.1
response.setHeader("Pragma","no-cache"); //HTTP 1.0
response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<%
String MontoPro = new String("");
String txnAuthLevel = "0";
String txnIMGAutoriz = "0";
Vector v = (Vector)session.getAttribute("page.specialacct");
Vector data = (Vector)session.getAttribute("datos");
int k;
Hashtable datasession = (Hashtable)session.getAttribute("page.datasession");
String ser60 = (String)datasession.get("ser60");
String ser670 = (String)datasession.get("ser670");
Vector bines =  new Vector();
Vector binesAMEX = new Vector();
Hashtable cadenaoriginal = (Hashtable)session.getAttribute("cadena.original");
if (ser60.equals("1"))
{
	bines = (Vector)session.getAttribute("bines");
}
if (ser670.equals("1"))
{
    binesAMEX = (Vector)session.getAttribute("binesAMEX");
}
%>
<html>
<head>
  <title>Datos de la Transaccion</title>
  <%@include file="style.jsf"%>
<script  language="JavaScript">
function FormatCurr(val, field)
{
  var num = val.toString().replace(/\$|\,/g,'')

  intVal = num.substring(0, num.indexOf("."))
  decVal = num.substring(num.indexOf(".") + 1, num.length)
  num = intVal + decVal;

  if( isNaN(num) )
  {
    return false
  }

  cents = Math.floor((num)%100)
  num = Math.floor((num)/100).toString()

  if(cents < 10)
    cents = "0" + cents;
  if(cents.length == 1)
    cents = cents + "0";

  for (var i = 0; i < Math.floor((num.length-(1+i))/3); i++)
    num = num.substring(0,num.length-(4*i+3))+','+num.substring(num.length-(4*i+3))

  num = num + '.' + cents
  eval("document.entry."+field+".value = '"+ num + "'");
}

var formName = 'Mio'
var specialacct = new Array()
<%
 if(!v.isEmpty()){
  for(int iv = 0; iv < v.size(); iv++){
   out.print("specialacct[" + iv + "]='" + v.get(iv) + "'" + '\n');
  }
 }
%>
<%if(ser60.equals("1"))
{%>
	var bines = new Array(<%=bines.size()%>);
	var banco = new Array(<%=bines.size()%>);
<%
	if(!bines.isEmpty())
	{
		Vector row = new Vector();
		for(int x=0; x<bines.size(); x++)
		{
		     row = (Vector)bines.get(x);
			out.print("bines["+x+"]='"+row.get(0)+"'"+'\n');
			out.print("banco["+x+"]='"+row.get(1)+"'"+'\n');
		}
	}
}
%>

<%if(ser670.equals("1"))
{%>
	var binesAMEX = new Array(<%=binesAMEX.size()%>);
	var bancoAMEX = new Array(<%=binesAMEX.size()%>);
<%
	if(!binesAMEX.isEmpty())
	{
		Vector row = new Vector();
		for(int x=0; x<binesAMEX.size(); x++)
		{
             		row = (Vector)binesAMEX.get(x);
			out.print("binesAMEX["+x+"]='"+row.get(0)+"'"+'\n');
			out.print("bancoAMEX["+x+"]='"+row.get(1)+"'"+'\n');
		}
	}
}
%>


function validate(form)
{
  form.validateFLD.value = '1'
<%
// OBTENCION DE TIPO DE SERVICIO
String tipo = "";
int items = 0;
tipo = data.elementAt(0).toString();
// VARIABLE QUE INDICA EL SIZE DE DATA POR SERVICIO Y POSICION
int longdata = 0;
int posicion = 0;

// NUMERO DE SERVICIOS CAPTURADOS
if(tipo.equals("1"))
{
	longdata = 3;
	posicion = 1;
}
if(tipo.equals("2"))
{
	longdata = 10;
	posicion = 4;
}

items = data.size() / longdata;

int i = 0;
int j = 0;
int l = 0;
int np = 0;
int lr = 0;
int mtoiva = 0;
int iva = 0;
int mtocom = 0;
String npagos = "";
String lref = "";
String com = "";
String mto = "";
String ivatmp = "";
String servicio = "";
String compcobrocom = "";
String cobrocom = "";

%>
<% if(!txnAuthLevel.equals("1") && !txnIMGAutoriz.equals("1"))
	{
    out.println("  form.submit();");
    out.println("var obj = document.getElementById('continu');"); 
    out.println("obj.onclick = new Function('return false');");   
	}   
%>


}
function AuthoandSignature(entry)
{
 var Cuenta;
 if(entry.validateFLD.value != '1')
  return
 if(entry.needAutho.value == '1' && entry.AuthoOK.value != '1'){
  top.openDialog('../ventanilla/paginas/authoriz.jsp', 220, 130, 'top.setPrefs()', entry)
 }
 if(entry.needSignature.value == '1' && entry.SignField.value != ''){
  Cuenta = eval('entry.' + entry.SignField.value + '.value')
  top.openModalDialog('<%=request.getContextPath()%>/servlet/ventanilla.FirmasServlet?txtDDACuenta=' + Cuenta, 710, 430, 'top.setPrefs2()', entry)
 }
}
</script>
</head>
<body bgcolor="#FFFFFF" onload="top.setFieldFocus(window.document)">
<br>
<% out.print("<form name=\"entry\" action=\"../servlet/ventanilla.DataServlet\" method=\"post\" ");
   if(txnAuthLevel.equals("1"))
    out.print("onSubmit=\"return validate(this)\"");
   out.println(">");
%>

<h1><%=session.getAttribute("page.txnImage")%></h1>

<h3>
S503 Abono por Cobranza Especial SAT
</h3>
<input type="hidden" name="nos" value="<%=items%>">
<table>
<tr><td><b>Monto Total :</b></td><td>&nbsp;&nbsp;</td>
<td><b><%=data.elementAt(posicion).toString()%></b></td></tr>
</table>
<br>
<%
// CICLO POR SERVICIO
for (i = 1; i < items+1;i++)
{%>
	<!-- ENCABEZADO POR SERVICIO //-->
	<table border="0">
	<tr><td><b>Servicio :</b></td><td>&nbsp;&nbsp;</td>
	<td align="center"><b><%=data.elementAt((i-1)*longdata+(posicion+1)).toString()%></b></td><td colspan="4"></td></tr>
	<input type="hidden" name="s<%=i%>" value="<%=data.elementAt((i-1)*longdata+(posicion+1)).toString()%>">
	<tr><td><b>Monto :</b></td><td>&nbsp;&nbsp;</td>
	<td><b><%=data.elementAt((i-1)*longdata+(posicion+3)).toString()%></b></td><td>&nbsp;&nbsp;</td>
	</table>
	<!-- ENCABEZADO PAGOS POR SERVICIO //-->
	<table border="0">
	<tr><td colspan="8" align="center"><b>PAGOS</b></td></tr>
	<tr>
	<td align="center"><b>No.</b></td><td>&nbsp;&nbsp;</td>
	<td align="center"><b>Monto</b></td><td>&nbsp;&nbsp;</td>
	<% // INCLUSION DE TITULO REFERENCIAS TIPO 1
	npagos = data.elementAt((i-1)*longdata+(posicion+2)).toString();
	np = Integer.parseInt(npagos);
	%>
	<input type="hidden" name="mtos<%=i%>" value="<%=data.elementAt((i-1)*longdata+(posicion+3)).toString()%>">
	<input type="hidden" name="nps<%=i%>" value="<%=np%>">
	<%
	// CICLO PAGOS POR SERVICIO
	for (j = 1; j < np+1;j++)
	{
		if(tipo.equals("1"))
		{
			for (l = 1; l < 4;l++)
			{
				if(l == 1)
				{%>
					<tr>
					<td align="right"><%=j%></td><td>&nbsp;&nbsp;</td>
					<td><input tabindex="<%=i%>" type="text" name="montos<%=i%>p<%=j%>" size="14" maxlength="11" onChange="top.validate(window, this, 'MtoServ')" value="<%=addpunto((String)cadenaoriginal.get("10017"))%>" tabstop='false' contentEditable = 'false' ></td>
					<td>&nbsp;&nbsp;</td>
        				<td align="right"><%=l%> .-</td><td>&nbsp;&nbsp;</td>
					<td><input tabindex="<%=i%>" type="text" name="ref<%=l%>s<%=i%>p<%=j%>" size="40" maxlength="<%=lr%>" onChange="top.validate(window, this, 'ValRef')"></td>
					</tr>
                              <%}
				else
				{
					// OMISION DE REFERENCIAS PARA SERVICIOS 480 / 448 / 297 / 66 / 1880
					if(!(servicio.equals("480") || servicio.equals("448") || servicio.equals("297") || servicio.equals("66") || servicio.equals("1880") || servicio.equals("669")|| servicio.equals("670")))
					{%>
						<tr>
						<td colspan="4">&nbsp;&nbsp;</td>
						<td align="right"><%=l%> .-</td><td>&nbsp;&nbsp;</td>
						<td><input tabindex="<%=i%>" type="text" name="ref<%=l%>s<%=i%>p<%=j%>" size="40" maxlength="<%=lr%>" onChange="top.validate(window, this, 'ValRef')"></td>
						</tr>
			<%		}
				}
			}
		} // TERMINA IF PARA SERVICIOS TIPO 1

		if(tipo.equals("2"))
		{%>
			<tr>
			<td align="right"><%=j%></td><td>&nbsp;&nbsp;</td>
			<td><input tabindex="<%=i%>" type="text" name="montos<%=i%>p<%=j%>" size="14" maxlength="11" value="<%=addpunto((String)cadenaoriginal.get("10017"))%>" tabstop='false' contentEditable = 'false' ></td>
			<td>&nbsp;&nbsp;</td>
			</tr>
<%			for (l = 1; l < 4;l++)
			{
				lref = data.elementAt((i-1)*longdata+(posicion+3)+l).toString();
				lr = Integer.parseInt(lref);
				if(lr > 0)
				{%>
					<tr>
					<td></td><td>&nbsp;&nbsp;</td>
					<%if(servicio.equals("60"))
					{%>
						<td align="right">Referencia <%=l%> :</td><td>&nbsp;&nbsp;</td>
						<td colspan="4"><input tabindex="<%=i%>" type="text" name="ref<%=l%>s<%=i%>p<%=j%>" size="<%=lr%>" maxlength="<%=lr%>" onChange="top.validate(window, this, 'ValTDC')"></td>
					<%}
					else
					{%>
						<td align="right">Referencia <%=l%> :</td><td>&nbsp;&nbsp;</td>
						<td colspan="4"><input tabindex="<%=i%>" type="text" name="ref<%=l%>s<%=i%>p<%=j%>" size="<%=lr%>" maxlength="<%=lr%>" onChange="top.validate(window, this, 'ValRef')"
                                                <%if (l==1)
                                                  {%>value="<%=datasession.get("txtRfc")%>"<%}
                                                  else
                                                  {%>value="<%=datasession.get("txtRefer16")%>"<%}%>
                                                tabstop='false' contentEditable = 'false' ></td>
					<%}%>
					</tr>
				<%}
			}
			// CAMPOS PARA COMPROBANTE DE COBRO DE COMISION E IVA
			if(cobrocom.equals("S") && compcobrocom.equals("S"))
			{%>
			<tr><td></td><td>&nbsp;&nbsp;</td><td>&nbsp;&nbsp;</td><td>&nbsp;&nbsp;</td>
			<td align="center" colspan="6"><b>Datos para el Comprobante del Cobro de Comisi&oacute;n e I.V.A.</b></td></tr>
			<tr><td></td><td>&nbsp;&nbsp;</td><td align="right">Nombre :</td><td>&nbsp;&nbsp;</td>
			<td colspan="6"><input tabindex="<%=i%>" type="text" name="nombres<%=i%>p<%=j%>" size="50" maxlength="50"></td></tr>
			<tr><td></td><td>&nbsp;&nbsp;</td><td align="right">Domicilio :</td><td>&nbsp;&nbsp;</td>
			<td colspan="6"><input tabindex="<%=i%>" type="text" name="doms<%=i%>p<%=j%>" size="50" maxlength="50"></td></tr>
			<tr><td></td><td>&nbsp;&nbsp;</td><td align="right">R.F.C. :</td><td>&nbsp;&nbsp;</td>
			<td colspan="6"><input tabindex="<%=i%>" type="text" name="rfcs<%=i%>p<%=j%>" size="13" maxlength="13"></td></tr>
<%			session.setAttribute("domsuc",data.lastElement().toString());
			}
		} // TERMINA IF PARA SERVICIOS TIPO 2
	}%>
	</table>
<%}%>

<input type="hidden" name="tiposerv" value="<%=tipo%>">
<input type="hidden" name="iTxn" value="<%= session.getAttribute("page.iTxn") %>">
<input type="hidden" name="oTxn" value="<%= session.getAttribute("page.oTxn") %>">
<!-- Modificado para proceso RAP//-->
<input type="hidden" name="cTxn" value="<%= session.getAttribute("page.cTxn") %>">
<input type="hidden" name="sucursal" value="<%= session.getAttribute("branch") %>">
<input type="hidden" name="teller" value="<%= session.getAttribute("teller") %>">
<input type="hidden" name="moneda" value="<%= session.getAttribute("page.moneda")%>">
<input type="hidden" name="registro" value="<%=session.getAttribute("empno")%>">
<input type="hidden" name="tllrlvl" value="<%=session.getAttribute("tellerlevel")%>">
<input type="hidden" name="supervisor" value="<%= session.getAttribute("teller") %>">
<input type="hidden" name="AuthoOK" value="0">
<input type="hidden" name="SignOK" value="0">
<input type="hidden" name="validateFLD" value="0">
<!-- Modificado para proceso RAP//-->
<input type="hidden" name="needSignature" value="0">
<input type="hidden" name="needAutho" value="<%out.print(txnAuthLevel);%>">
<input type="hidden" name="SignField" value="<%= session.getAttribute("page.SignField") %>">
<p>
<%
if (ser60.equals("1"))
{
	if(session.getAttribute("bines") != null)
	{
    	bines = new Vector();
    	session.setAttribute("bines", bines);
  	}
}%>

<%
if (ser670.equals("1"))
{
	if(session.getAttribute("binesAMEX") != null)
	{
    	binesAMEX = new Vector();
    	session.setAttribute("binesAMEX", binesAMEX);
  	}
}%>

<% if(txnAuthLevel.equals("1") || txnIMGAutoriz.equals("1"))
    out.println("<a id='continu' tabindex='"+(i+1)+"' href=\"javascript:validate(document.entry);javascript:AuthoandSignature(document.entry)\" ><img src=\"../ventanilla/imagenes/b_aceptar.gif\" border=\"0\"></a>");
   else
    out.println("<a id='continu' tabindex='"+(i+1)+"' href=\"javascript:validate(document.entry)\"><img src=\"../ventanilla/imagenes/b_aceptar.gif\" border=\"0\"></a>");
%>

<script language="javascript">
	window.focus();
	document.all['continu'].focus();
</script>

</form>
</body>
<%!
  private String addpunto(String valor)
  {
        valor = valor + "00";
	int longini = valor.length();
    String cents  = valor.substring(longini-2,longini);
	String entero = valor.substring(0,longini-2);
	int longente = entero.length();
	for (int i = 0; i < (longente-(1+i))/3; i++)
	{
		longente = entero.length();
	    entero = entero.substring(0,longente-(4*i+3))+','+entero.substring(longente-(4*i+3));
	}
  	entero = entero + '.' + cents;
	return entero;
  }
%>
</html>
