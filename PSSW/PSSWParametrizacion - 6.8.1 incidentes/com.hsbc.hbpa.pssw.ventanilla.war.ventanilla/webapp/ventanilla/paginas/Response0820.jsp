<!--
//*************************************************************************************************
//             Funcion: JSP que despliega respuesta de txn 0820
//            Elemento: Response0820.jsp
//          Creado por: Alejandro Gonzalez Castro
// Ultima Modificacion: 03/08/2004
//      Modificado por: Alejandro Gonzalez Castro
//*************************************************************************************************
// CCN - 4360175 - 03/08/2004 - Ajuste size de ventana de impresion
// CCN - 4360275 - 18/02/2005 - WAS 5.1
//*************************************************************************************************
-->

<%@ page session="true" import="java.util.*"%>
<%
response.setHeader("Cache-Control","no-store"); //HTTP 1.1
response.setHeader("Pragma","no-cache"); //HTTP 1.0
response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<%
%>
<%!
  private String getdomi(String r)
  {
		String domicilio = r.substring(77,108);
		return domicilio;
  }
  private String getString(String s)
  {
    int len = s.length();
    for(; len>0; len--)
	  if(s.charAt(len-1) != ' ')
	    break;

    return s.substring(0, len);
  }

  private String getItem(String newCadNum, int newOption)
  {
   int iPIndex = newCadNum.indexOf("CONSECUTIVO:") + 12, NumSaltos, Num = 0, Ban = 0;
   String nCadNum = new String("");
   NumSaltos = newOption;
   while(Num < NumSaltos)
   {
    if(newCadNum.charAt(iPIndex) == 0x20)
    {
     while(newCadNum.charAt(iPIndex) == 0x20)
      iPIndex++;
     Num++;
    }
    else
     while(newCadNum.charAt(iPIndex) != 0x20)
      iPIndex++;
   }
   while(newCadNum.charAt(iPIndex) != 0x20 && iPIndex < newCadNum.length()){
    nCadNum = nCadNum + newCadNum.charAt(iPIndex);
    if(iPIndex < newCadNum.length())
      iPIndex++;
    if(iPIndex == newCadNum.length())
      break;
   }

   return nCadNum;
  }
%>
<%
  Vector resp = (Vector)session.getAttribute("response");
  Hashtable datasession = (Hashtable)session.getAttribute("page.datasession");
%>
<html>
<head>
  <title>Certificación</title>
<style type="text/css">Body {font-family: Arial, Helv; font-size: 10pt; font-weight: bold; color:#000000}</style>
</head>
<body>
<form name="entry">
<table border="0" cellspacing="0">
<tr>
<td>
  <input type="hidden" name="transaction" value="<%= session.getAttribute("page.iTxn") %>"  />
  <input type="hidden" name="transaction1" value="<%= session.getAttribute("page.oTxn") %>" />
</td>
</tr>
</table>
<p>
<%
   String lineresp = "";
   String szResp = new String("");
   int codigo = 1;
   if( resp.size() != 4 )
    out.println("<b>Error de conexi&oacute;n</b><br>");
   else
   {
    try{
     codigo = Integer.parseInt((String)resp.elementAt(0));
    }
    catch(NumberFormatException nfe){
     out.println("<b>Error de conexi&oacute;n</b><br>");
    }
    if( codigo != 0 )
      out.println("<b>Error en transaccion</b><br>");
     int lines = Integer.parseInt((String)resp.elementAt(1));
     szResp = (String)resp.elementAt(3);
     for(int i=0; ; i++){
      String line = szResp.substring( i*78, Math.min(i*78+77, szResp.length()) );
	  if (i== 4)
	  {lineresp = line;}
      out.println(getString(line)+"<p>");
      if(Math.min(i*78+77, szResp.length()) == szResp.length())
       break;
     }
   }
%>
<%
if (codigo == 0)
{
    String RFC = lineresp.substring(1,13);
    String txtRfc = (String)datasession.get("txtRfc");

    if (RFC.length() < 13)
        {RFC= " " + RFC;}

    if (!RFC.equals(txtRfc))
    {%>
        <script language="JavaScript">
            alert("Alerta... El R.F.C. no coincide...");
        </script>
<%	}%>
<%//Certificacion Inicio
  String isCertificable = (String)session.getAttribute("page.certifField");  // Para Certificación
  String txtCadImpresion  = ""; // Para Certificación
   if(!isCertificable.equals("N") ){ // Para Certificación
    session.setAttribute("Consecutivo", getItem((String)resp.elementAt(3), 1)); // Para Certificación
    session.setAttribute("Tipo", isCertificable); // Para Certificación
    out.println("<script language=\"javascript\">top.openDialog('../../servlet/ventanilla.ImprimirServlet?txtCadImpresion=" + txtCadImpresion + "', 160, 120, 'top.setPrefs4()', document.Txns)</script>"); // Para Certificación
   }
  //Certificaion Fin  %>
<%
}
%>
</form>
</body>
</html>
