<!--
//*************************************************************************************************
//             Funcion: JSP que presenta datos varios a la sucursal
//            Elemento: seccioninformativa.jsp
//          Creado por: Alejandro Gonzalez Castro
//      Modificado por: Fausto Rodrigo Flores Moreno
//*************************************************************************************************
// CCN - 4360409 - 05/01/2006 - Se modifica version a 3.1
// CCN - 4360427 - 03/02/2006 - Se modifica version a 3.2
// CCN - 4360437 - 24/02/2006 - Se modifica version a 3.3
// CCN - 4360456 - 07/03/2006 - Se modifica version a 3.4
// CCN - 4360477 - 19/05/2006 - Se modifica versi�n a 3.5
// CCN - 4360493 - 23/06/2006 - Se modifica versi�n a 3.6
// CCN - 4360495 - 04/08/2006 - Se modifica versi�n a 3.7
// CCN - 4360510 - 08/09/2006 - Se muestar el perfil actual o el combo para cambiar de perfil, se modifica version a 3.8
// CCN - 4360518 - 21/09/2005 - Se agrega en sesion varaible para controlar ruta para el intercambio de perfiles.
// CCN - 4360532 - 20/10/2006 - Se modifica versi�n a 3.9
// CCN - 4360567 - 02/02/2007 - Se modifica versi�n a 4.0
// CCN - 4360581 - 12/03/2007 - Se convierten los estilos en jsf y se hace un include en el jsp
// CCN - 4360602 - 18/05/2007 - Se modifica versi�n a 4.2
// CO - CR7564 - 26/01/2011 - Se modifica versi�n a 4.9
//*************************************************************************************************
-->
<%@page language= "java" import="java.util.*,ventanilla.com.bital.beans.DatosVarios" contentType="text/html"%>
<%@ page session="true"%>
<% 
response.setHeader("Cache-Control","no-store"); //HTTP 1.1
response.setHeader("Pragma","no-cache"); //HTTP 1.0
response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
DatosVarios perfiles = DatosVarios.getInstance();                   
Vector perfil = new Vector();        
perfil =(Vector)perfiles.getDatosV("PERFIL");
String ICR = perfiles.getDatosv("ICR");
//Inicializaci�n del Vector de Divisas
		Vector tiposCambio = new Vector();
		tiposCambio.add("EURC");
		tiposCambio.add("EURV");
		tiposCambio.add("GBPC");
		tiposCambio.add("GBPV");

String sPerfil = (String)session.getAttribute("tellerperfil");
String sPerfilBase = (String)session.getAttribute("perfilbase");

String perfilAct = "";
String perBase = "";
String banswitch = "";

String updatecurr = new String("0");
String updateICR = new String("0");
int x=0;
boolean banfuera = false;
while(banfuera == false && x < perfil.size()){
	String cad = (String)perfil.elementAt(x);
	if(cad.substring(0,2).equals(sPerfil) && cad.substring(3,7).equals("0000")){
		perfilAct = cad.substring(12,cad.length());	 	
		banfuera = true;
	}
	x++;
}

int y=0;
banfuera = false;
while(banfuera == false && y < perfil.size()){
	String cad = (String)perfil.elementAt(y);
	if(cad.substring(0,2).equals(sPerfilBase) && cad.substring(3,7).equals("0000")){
		perBase = cad.substring(12,cad.length());	 	
        banswitch = cad.substring(8,9);		
		banfuera = true;
	}
	y++;
}


String cadena = "";
String cTC = session.getAttribute("PRESSTC").toString();
if(updatecurr.equals("1") || cTC.equals("S") || updateICR.equals("1"))
   {
        cadena = "../ventanilla/imagenes/format.horizontal.separator.gif";
 }
    else
    {
  		cadena = "../imagenes/format.horizontal.separator.gif";
  }
 session.setAttribute("PRESSTC","N");
%>
<html>
 <head>
<%!
   private String CalendarToString(int option, int sum)
   {
    java.util.Calendar now = java.util.Calendar.getInstance();
    String temp = new Integer(now.get(option) + sum).toString();
    if( temp.length() != 2 )
      temp = "0" + temp;
    return temp;
  }
%>
  <title>Panel de Captura</title>
<%
   if(session.getAttribute("updatecurr") != null)
     updatecurr = (String)session.getAttribute("updatecurr");
   if(session.getAttribute("updateICR") != null)
   	 updateICR = (String)session.getAttribute("updateICR");
   if(updatecurr.equals("1") || updateICR.equals("1"))
   {%>
        <%@include file="specialstyle.jsf"%>
<%      session.removeAttribute("updatecurr");
		session.removeAttribute("updateICR");
    }
    else
    {%>
    <%@include file="specialstyle.jsf"%>
<%  }%>

  <style type="text/javascript">
<!--
 ids.hora.fontfamily="arial";
 ids.hora.fontSize="12px";
 ids.hora.color="#FF0000";
 ids.hora.fontweight="bold";
//-->
  </style>
  <script language="javascript">
<!--
 var rotate_delay = 5000;
 var current = 0;
 var slide = new Array();
 slide[0] = new String('../imagenes/Defraudador03.gif')
 slide[1] = new String('../imagenes/Defraudador05.gif')
 slide[2] = new String('../imagenes/Defraudador06.gif')

 function recargar(){
 	var selObjA = document.getElementById("perfil");
	var cad;
 	if(selObjA.options[selObjA.selectedIndex].value != '0'){ 		 		
 		cad = selObjA.options[selObjA.selectedIndex].value; 
 		
 		<%
 		  if(updatecurr.equals("0") || updateICR.equals("0")){%>		
 		  document.seccion.action = "../../main.jsp?perf="+cad;
 		<%}else{%>
 		  document.seccion.action = "../main.jsp?perf="+cad;
 		<%}%>
 		document.seccion.target = "_parent";
 		document.seccion.submit();
 	} 	
 }	
	

 function ComienzaRotacion(){
  setInterval("rotate()", rotate_delay)
 }

 function rotate()
 {
   var forma = this.document.Defraudadores;
   var i = 0;
   if(current > slide.length - 1)
     current = 0;
     
   forma.src = slide[current]
   current = current + 1
 }

 var seconds = <%=CalendarToString(java.util.Calendar.SECOND, 0)%> + 1;
 var minutes = <%=CalendarToString(java.util.Calendar.MINUTE, 0)%>;
 var hours = <%=CalendarToString(java.util.Calendar.HOUR_OF_DAY, 0)%>;

function startClock()
{
  setInterval('updateClock()', 1000);
}


function updateClock()
{
  var strSeconds;
  var strMinutes;
  var strHours;
  
  seconds++;
  if(seconds >= 60)
  {
   minutes++;
   seconds = 0;
  }
  
  if(minutes >= 60)
  {
   hours++;
   minutes = 0;
  }
  
  if(hours >= 24)
  {
   hours = 0;
  }

  if(seconds < 10)
    strSeconds = '0' + seconds;
  else
    strSeconds = seconds;
   
  if(minutes < 10)
    strMinutes = '0' + minutes;
  else
    strMinutes = minutes;
  
  if(hours < 10)
    strHours = '0' + hours;
  else
    strHours = hours;

  hora.innerHTML = strHours + ':' + strMinutes + ':' + strSeconds;
}


function getCurrentDate()
{
   var aDay = new Array('Domingo', 'Lunes', 'Martes', 'Mi�rcoles', 'Jueves', 'Viernes', 'S�bado');
   var aMonth = new Array ('Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic');
   var dDay = <%=CalendarToString(java.util.Calendar.DAY_OF_MONTH, 0)%>;
   var dMonth = <%=CalendarToString(java.util.Calendar.MONTH, 0)%>;
   var dYear = new String('<%=CalendarToString(java.util.Calendar.YEAR, 0)%>');
   if(dDay < 10)
     dDay = '0' + dDay;
   
   dYear = dYear.substring(1,5);
   document.write(dDay + '/' + aMonth[dMonth] + '/' + dYear);
}
function submite(forma)
  {
    forma.submit();
  }
  
function submiteICR(forma)
  {
	if(top.validate(window, document.ICR.txtCuentaICR,'isnumeric'))
    	forma.submit();
  }
  
//-->
 </script>
<% String Sucursal = new String();
   String Teller = new String();
   String Empno = new String();

   if(session.getAttribute("branch") != null)
     Sucursal = (String)session.getAttribute("branch");
   if(session.getAttribute("teller") != null)
     Teller = (String)session.getAttribute("teller");
   if(session.getAttribute("empno") != null)
     Empno = (String)session.getAttribute("empno");
%>
 </head>
 <body bgcolor="#FFFFFF" onload="startClock();">
 <table border="0" cellpadding="0" cellspacing="0" align="left" width="100%">
 <tr>
<% if(updatecurr.equals("1") || updateICR.equals("1"))
   {%>
      <td><img src="../ventanilla/imagenes/t_ventanilla.gif"></td>
<% }
    else
    {%>
  <td><img src="../imagenes/t_ventanilla.gif"></td>
<%  }%>
 </tr>
 <tr>
  <td><table border="0" cellpadding="0" cellspacing="0" align="left" width="85%">
       <tr>
         <td class="normaltextred" valign="middle">Sucursal:</td>
         <td class="normaltextredbold" valign="middle" align="right"><%=Sucursal.substring(1,5)%></td>
       </tr>
       <tr>
         <td class="normaltextred" valign="middle">Usuario:</td>
         <td class="normaltextredbold" valign="middle" align="right"><%=Teller%></td>
       </tr>
       <tr>
         <td class="normaltextred" valign="middle">Fecha:</td>
         <td class="normaltextredbold" valign="middle" align="right">
           <script language="javascript">getCurrentDate()</script>
         </td>
       </tr>
       <tr>
        <td class="normaltextred" valign="middle">Hora:</td>
        <td class="normaltextredbold" valign="middle" align="right"><div ID="hora"></div></td>
       </tr>
       <%-- ---------------------------MUESTRA INFORMACION DE PERFIL PARA SUCURSALES---------------------------- 
                                                                                                                  --%>
       <%
         java.util.Hashtable userinfo = null;
		 if(session.getAttribute("userinfo") != null)
		 {
		     userinfo = (java.util.Hashtable)session.getAttribute("userinfo");
		 }
         
         
         if(!((String)session.getAttribute("isForeign")).equals("S") && !(userinfo.get("N_NIVEL").equals("3") || userinfo.get("N_NIVEL").equals("4"))){%>
       
       <tr>
       	<td colspan="2"><img src="<%=cadena%>" width="100%" height="1">
       	</td>
       </tr>
       <tr>
         <td colspan="2" class="normaltextred" align="center" valign="middle">Perfil :</td>
       </tr>
       <tr>
       	 <%if(sPerfilBase.trim().equals("00") || banswitch.equals("0")){%>
         	<td colspan="2" class="normaltextredbold" align="center" valign="middle" align="right"><%=perfilAct%></td>
         <%}else{%>
         	<td>&nbsp;</td>
         <%}%>
       </tr>
       <tr>
         <td colspan="2" class="normaltextbold" align="center" valign="middle" align="right">
         <%if(!sPerfilBase.trim().equals("00") && banswitch.equals("1")){%>
         <select name="perfil" id="perfil" onchange="JavaScript:recargar();" onmouseover="self.status='Cambio de Perfil';return true;" title="Seleccione">
         	<option value='<%=sPerfil%>' class="normaltextbold" selected="selected"><%=perfilAct%>
         	<%if(sPerfil.trim().equals("00")){%>
         		<option value='<%=sPerfilBase%>' class="normaltextbold"><%=perBase%>
         	<%}else{%>
         		<option value="00" class="normaltextbold">Universal
         	<%}%>         	
         </select>
         <%}%>
         </td>
       </tr>
       <tr>
         <td>&nbsp;</td>
       </tr>

		<tr><td colspan="2"><img src="<%=cadena%>" width="100%" height="1"></td></tr>
		<%}%>
       <%-- ---------------------------FIN MUESTRA INFORMACION DE PERFIL PARA SUCURSALES------------------------ 
                                                                                                                  --%>		
      </table></td>
 </tr>
 <tr>
  <td height="50" valign="middle" align="center" class="normaltextred">Versi�n: <span class="normaltextredbold">6.7.0</span></td>
 </tr>
 <tr>
<% if(updatecurr.equals("1") || updateICR.equals("1"))
   {%>
      <td><img src="../ventanilla/imagenes/t_informativa.gif"></td>
<% }
    else
    {%>
  <td><img src="../imagenes/t_informativa.gif"></td>
<%  }%>
 </tr>
 <tr>
   <td>
     <table border="0" cellpadding="0" cellspacing="0" align="left" width="80%">
       <tr>
        <th align="center" width="40">Moneda&nbsp;&nbsp;</th><th align="center" width="40">Compra</th><th align="center" width="40">Venta</th>
       </tr>
<%if(session.getAttribute("foreignExchange") != null){
   java.util.Hashtable htOFE = (java.util.Hashtable)session.getAttribute("foreignExchange");
   int i=0;
   if(htOFE.size() > 0)
{
     //java.util.Iterator htOFEIte = htOFE.keySet().iterator();
     while(htOFE.size() > i){
       String htElemC = new String("N.D.");
       String htElemV = new String("N.D.");
                htElemC = (String)tiposCambio.get(i);
       i=i+1;
                htElemV = (String)tiposCambio.get(i);
       i=i+1;
%>
       <tr>
        <td class="noundline" align="left"><%=htElemC.substring(0, 3)%></td><td class="noundline" align="right"><%=htOFE.get(htElemC)%></td><td class="noundline" align="center"><%=htOFE.get(htElemV)%></td>
       </tr>
<%
     }
   }
   else{
%>
       <tr>
        <td class="noundline" align="left">N.D.</td><td class="noundline" align="right">N.D.</td><td class="noundline" align="center">N.D.</td>
       </tr>
       <tr>
        <td class="noundline" align="left">N.D.</td><td class="noundline" align="right">N.D.</td><td class="noundline" align="center">N.D.</td>
       </tr>
       <tr>
<% }
  }%>
        <td>&nbsp;</td>
       </tr>
       <tr><td class="noundline" colspan="3" align="center"><a href="javascript:submite(document.seccion);" title="Actualizar" >Actualizar Tipo de Cambio</a></td></tr>
      </table>
    </td>
 </tr>
 <tr><td colspan="3">&nbsp;&nbsp;</td></tr>
<tr> 
  <td class="noundline" colspan="3" align="center">
    <% if(updatecurr.equals("1"))
      {
         out.println("Ultima Actualizaci&oacuten");
         out.println("<br>Hora:"+CalendarToString(java.util.Calendar.HOUR_OF_DAY, 0)+ ":" + CalendarToString(java.util.Calendar.MINUTE, 0)+ ":" + CalendarToString(java.util.Calendar.SECOND, 0));
      }
    %>
  </td>
</tr>
 <tr>
<% if(updatecurr.equals("1") || updateICR.equals("1"))
   {%>
        <td><img src="../ventanilla/imagenes/format.horizontal.separator.gif" width="100%" height="1"></td>
<% }
    else
    {%>
  <td><img src="../imagenes/format.horizontal.separator.gif" width="100%" height="1"></td>
<%  }%>
 </tr>
 <tr>
<!--  <td valign="middle" align="center" class="normaltextredbold"><br><img src="../imagenes/TransparentSpace.gif" width="56" height="75" name="Defraudadores"><br>Empleados del Mes</td>-->
 </tr>
 <tr>
<% if(updatecurr.equals("1") || updateICR.equals("1"))
   {%>
        <td><img src="../ventanilla/imagenes/format.horizontal.separator.gif" width="100%" height="1"></td>
<% }
    else
    {%>
  <td><img src="../imagenes/format.horizontal.separator.gif" width="100%" height="1"></td>
<%  }%>
 </tr>
 <tr>
 <!-- Modificaci�n para ICR -->
<form name='ICR'  
 <% if(updateICR.equals("1") || updatecurr.equals("1"))
   {%>
         action="../servlet/ventanilla.ICRServlet" method="post">
<% }
    else
    {%>
          action="../../servlet/ventanilla.ICRServlet" method="post">
<%  }%>
       <tr>
         <td colspan="2" class="normaltextred" align="left" valign="middle">CONSULTA CUENTA</td>
       </tr>
              <tr>
         <td colspan="2" class="normaltextred" align="left" valign="middle"><input name='txtCuentaICR' type='text' maxlength=16 size='16'></td>
       </tr>
              <tr>
         <td colspan="2" align="left" valign="middle">
         	<select name='lstTipoCuenta'>
         		<option value='BCC'>DDA Banitsmo</option>
         		<option value='BCP'>DPF Banitsmo</option>
         		<option value='HCC'>DDA HBPA</option>
         		<option value='HCP'>DPF HBPA</option>
         	</select>
         </td>
       </tr>
       <tr>
       		<td class="noundline" colspan="3" align="left"><a href="javascript:submiteICR(document.ICR);" title="Buscar" >Buscar cuenta</a></td>
       </tr>
       <tr> 
       <% if(updateICR.equals("1"))
       {
       %>
  			<td class="normaltextred" colspan="3" align="left">
  				<%
  				String flagICR = "";
  					if(session.getAttribute("flagICR") != null)
		     			flagICR = (String)session.getAttribute("flagICR");
  				if(flagICR.equals("0"))
			    {
			       	out.println("La cuenta es:");
			       	out.println("<BR>"+(String)session.getAttribute("ctaICR"));
			    }
			    else
			    {
			    	out.println("NO EXISTE LA CUENTA");
			    }
			    %>
			</td>
		<%
		}	
		%>
		</tr>
<input type="hidden" name="teller" value="<%=Teller%>">
<input type="hidden" name="branch" value="<%=Sucursal%>">
<input type="hidden" name="empno" value="<%=Empno%>">
<input type="hidden" name="moneda" value="01">
</form>
 <!-- Termina modificaci�n ICR -->
 </tr>
 </table>

 <form
<% if(updatecurr.equals("1") || updateICR.equals("1"))
   {%>
         name="seccion" action="../servlet/ventanilla.CurrencyServlet" method="post">
<% }
    else
    {%>
          name="seccion" action="../../servlet/ventanilla.CurrencyServlet" method="post">
<%  }%>

   <input type="hidden" name="teller" value="<%=Teller%>">
   <input type="hidden" name="branch" value="<%=Sucursal%>">
   <input type="hidden" name="empno" value="<%=Empno%>">
</form>
</body>
</html>


