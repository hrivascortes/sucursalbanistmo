<!--
// CCN - 4360581 - 12/03/2007 - Se convierten los estilos en jsf y se hace un include en el jsp
// CCN - 4620032 - 09/11/2007 - Se modifica menu
-->

<%@ page session="true" import="java.util.Stack" %>
<%
response.setHeader("Cache-Control","no-store"); //HTTP 1.1
response.setHeader("Pragma","no-cache"); //HTTP 1.0
response.setDateHeader ("Expires", 0); //prevents caching at the proxy server*/
%>
<html>
<head>
  <title>SubMenu</title>
<%@include file="style.jsf"%>
</head>
<body>
<form name="menu" action="ventanilla.PageServlet" method="post">
<table border="0" cellspacing="0">
<tr>
  <td>Opci&oacute;n:</td>
  <td width="10">&nbsp;</td>
  <td>
  <select name="transaction1" size="1">
    <option value="S165">Cancelaci�n de cheque/Pago en efectivo
    <option value="S265">Cancelaci�n de Cheque de Gerencia con Cr�dito a Cuenta
    <option value="S365">Pago de cheque suspendido/Pago en efectivo
    <option value="S465">Pago de Cheque de Gerencia Suspendido con Cr�dito a Cuenta
  </select>
  </td>
</tr>
</table>
<input type="hidden" name="moneda" value="<%= session.getAttribute("page.moneda") %>">
<input type="hidden" name="transaction" value="<%= session.getAttribute("page.iTxn") %>">
<%
  Stack flujotxn = new Stack();
  session.setAttribute("page.flujotxn", flujotxn);
// Modificacion para flujo 0065
  session.removeAttribute("page.iTxn");
%>
<p>
<input type="image" src="../ventanilla/imagenes/b_aceptar.gif" border="0">
</form>
</body>
</html>