<!--
//*************************************************************************************************
//             Funcion: JSP que despliega el menu de ventanilla
//            Elemento: procesosdisponibles.jsp
//          Creado por: Alejandro Gonzalez Castro
//      Modificado por: Jes�s Emmanuel L�pez Rosales
//      Modificado por: Carolina Velas
//*************************************************************************************************
// CCN - 4360274 - 16/02/2005 - Se cambia fuente y estilo para procesos disponibles
// CCN - 4360323 - 24/05/2005 - Se envian nuevamente los elementos con el cambio del tama�o de la letra
// CCN - 4360342 - 08/07/2005 - Se realizan modificaciones para departamentos.
// CCN - 4360364 - 19/08/2005 - Se controlan departamentos por Centro de Costos.
// CCN - 4360364 - 19/08/2005 - Se modifica estableciendo el menu de reportes.
// CCN - 4360368 - 02/09/2005 - Se agrega menu de GTE a Departamentos.
// CCN - 4360374 - 08/09/2005 - Se genera nuevo paquete a peticion de QA.
// CCN - 4360424 - 03/02/2006 - Se agrega al men� opciones de combinadas y divisas
// CCN - 4360510 - 08/09/2006 - Valida que si no hay tnxs para un proceso no se pinta
// CCN - 4360581 - 12/03/2007 - Se convierten los estilos en jsf y se hace un include en el jsp
// CCN - 4620008 - 14/09/2007 - Se hacen modificaciones para tropicalizaci�n
//*************************************************************************************************
-->
<%@page language= "java" import="java.util.*,ventanilla.com.bital.beans.DatosVarios,ventanilla.com.bital.beans.Menues" contentType="text/html"%>
<html>
 <head>
  <title>Procesos Disponibles</title>
  <%@include file="specialstyle.jsf"%>
  <script language="JavaScript" src="validate.js">
  </script>
 </head>
<%

DatosVarios process = DatosVarios.getInstance();                   
Vector procesos = new Vector();        
procesos =(Vector)process.getDatosV("PROCESOS");

DatosVarios perfiles = DatosVarios.getInstance();                   
Vector vperfiles = new Vector();        
vperfiles =(Vector)perfiles.getDatosV("PERFIL");

Vector vmenu = new Vector();


DatosVarios perfilt = DatosVarios.getInstance();
Hashtable hPerfil = perfilt.getDatosH("PERFILT");
Vector vperfilt = new Vector();
int y = 0;
String tipoEnt = "";
byte banperexst = 0;
while( y < vperfiles.size() && banperexst==0){
	 String cad = vperfiles.elementAt(y).toString();
	 String indice = cad.substring(0,2);
	 if(session.getAttribute("tellerperfil").equals(indice) && cad.substring(3,7).equals("0000")){
	    tipoEnt = cad.substring(10,11); 
	    session.setAttribute("tipomenu",tipoEnt);
	    banperexst = 1;	    
	 }
	 y++;
}

vperfilt = (Vector) hPerfil.get(session.getAttribute("tellerperfil"));
java.util.Hashtable userinfo = null;
java.util.Hashtable branchinfo = null;
if(session.getAttribute("userinfo") != null)
{
     userinfo = (java.util.Hashtable)session.getAttribute("userinfo");
}
if(session.getAttribute("branchinfo") != null)
{
     branchinfo = (java.util.Hashtable)session.getAttribute("branchinfo");
}
%>
 <body bgcolor="#CCCCCC">
   <script language="JavaScript">
         <% 
            String cadperfdel = (String)session.getAttribute("perfdel");
            if(cadperfdel.equals("S")){
              session.setAttribute("perfdel","N");
              out.println("alert('Su perfil a sido eliminado y se le a asignado perfil universal')");
           }%>    
  </script>
 <layer style="position:absolute;top:2;">
  <table border="0" cellpadding="2" cellspacing="1">
<%
String mensaje = "Si esta clave de usuario se vuelve a utilizar despu&eacute;s de realizada esta transacci&oacute;n, los  movimientos que se digiten tendr&aacute;n la fecha de proceso del siguiente d&iacute;a h&aacute;bil";


if (session.getAttribute("isForeign").equals("S") || session.getAttribute("tipomenu").equals("T"))
{
	if ( userinfo.get("N_NIVEL").equals("3") || userinfo.get("N_NIVEL").equals("4"))
	{%>
    	<tr class="procesosdisponibles">&nbsp;</tr>
	    <tr class="procesosdisponibles">
    	<td valign="middle" width=""><a href="../../servlet/ventanilla.MenuServlet?list=99"   tabindex="-1" target="panel" title="Gerente" onmouseover="self.status='Gerente';return true;"><img src="../imagenes/bullet1.gif" border="0">Gerente</a></td>
	    <td valign="middle" width=""><a href="PageBuilderGerente.jsp"   tabindex="-1" target="panel" title="Totales" onmouseover="self.status='Totales';return true;"><img src="../imagenes/bullet1.gif" border="0"> Totales</a></td>
	    <td valign="middle" width=""><a href="Reportes.jsp"   tabindex="-1" target="panel" title="Reportes" onmouseover="self.status='Reportes';return true;"><img src="../imagenes/bullet1.gif" border="0"> Reportes</a></td>
    	<td valign="middle" width=""></td>
    	<td valign="middle" width=""><a href="../../servlet/ventanilla.Logoff?salida=0"   tabindex="-1" target="panel" title="Salida Temporal" onmouseover="self.status='Salida Temporal';return true;"><img src="../imagenes/bullet1.gif" border="0"> Salida Temporal</a></td>
	    <td valign="middle" width=""></td>
    	<td valign="middle" width=""><a href="../../servlet/ventanilla.Logoff?salida=1" onClick="return confirm('<%out.print(mensaje);%>')"   tabindex="-1" target="panel" title="Salida permanente" onmouseover="self.status='Salida Permanente';return true;"><img src="../imagenes/bullet1.gif" border="0"> Salir Permanente</a></td>
	    <!--td valign="middle"><a href="Help.jsp"   tabindex="-1" target="panel" title="Ayuda" onmouseover="self.status='Ayuda';return true;"><img src="../imagenes/bullet1.gif" border="0"> Ayuda</a></td-->
    	</tr>
	<%}
	else
	{%>
	    <tr class="procesosdisponibles">&nbsp;</tr>
		<tr class="procesosdisponibles">
	    <td valign="middle" width=""><a href="../../servlet/ventanilla.MenuServlet?list=9000"   tabindex="-1" target="panel" title="Departamentos" onmouseover="self.status='Departamentos';return true;"><img src="../imagenes/bullet1.gif" border="0">Transacciones Disponibles</a></td>
		<td valign="middle"><a href="PageBuilderSelectTotales.jsp"   tabindex="-1" target="panel" title="Totales" onmouseover="self.status='Totales';return true;"><img src="../imagenes/bullet1.gif" border="0"> Totales</a></td>
	    <td valign="middle" width=""><a href="PageBuilderDiario.jsp"   tabindex="-1" target="panel" title="Diario Electr&oacute;nico" onmouseover="self.status='Diario Electr&oacute;nico';return true;"><img src="../imagenes/bullet1.gif" border="0"> Diario Electr&oacute;nico</a></td>
    	<td valign="middle" width=""></td>
	   	<td valign="middle" width=""><a href="../../servlet/ventanilla.Logoff?salida=0"   tabindex="-1" target="panel" title="Salida Temporal" onmouseover="self.status='Salida Temporal';return true;"><img src="../imagenes/bullet1.gif" border="0"> Salida Temporal</a></td>
	    <td valign="middle" width=""></td>
    	<td valign="middle" width=""><a href="../../servlet/ventanilla.Logoff?salida=1" onClick="return confirm('<%out.print(mensaje);%>')"   tabindex="-1" target="panel" title="Salida permanente" onmouseover="self.status='Salida Permanente';return true;"><img src="../imagenes/bullet1.gif" border="0"> Salir Permanente</a></td>
	    </tr>
	<%}
}
else if(userinfo.get("N_NIVEL").equals("1") || userinfo.get("N_NIVEL").equals("2"))
{%>
	<tr class="procesosdisponibles">
	<% 
	  Menues menues = Menues.getInstance();
	  
	  int j=0;
	  for(int i = 0; i < procesos.size(); i++){
		  String cad = procesos.elementAt(i).toString();
		  String indice = cad.substring(0,2);
		  String clave = cad.substring(3,5);
		  String desc = cad.substring(6, cad.length());
		       
        vmenu = (Vector)menues.getMenues(clave);   

        int g;
        int banpint = 0;      
        int f = 0;
        boolean banfuera = false; 
        while(banfuera == false && f<vmenu.size())
		{ 
		           g = 0;
                   boolean banfuera2 = false;
                   while(banfuera2 == false && g<vperfilt.size() && banpint == 0){ 
                       String perfil = (String)vperfilt.elementAt(g);
                       
                       Vector Numtxn = (Vector)vmenu.elementAt(f);                        
                       if (perfil.indexOf("-") >= 0)    
                          perfil = perfil.substring(0,4);
                       if((Numtxn.elementAt(1).equals(perfil) || vperfilt.elementAt(g).equals("9999")) && !clave.equals("23")){                          
                                       
		%>		
    	                 <td valign="middle" width=""><a href="../../servlet/ventanilla.MenuServlet?list=<%=clave%>" target="panel" title="<%=desc.toString().trim()%>" tabindex="-1" onmouseover="self.status='<%=desc.toString().trim()%>';return true;"><img src="../imagenes/bullet1.gif" border="0"> <%=desc.toString().trim()%></a></td>
    	<%          
                         j++;
    	                 banpint = 1;
    	                     	        
    	               }
			        	if((j == 6 && banpint == 1) || (i == procesos.size() && f==vmenu.size() && g==vperfilt.size()) ){%>
				    		</tr>
				    		<%if(i < procesos.size() && banpint == 1)%>
				    		<tr class="procesosdisponibles">
				    	   <%j=0;
				    	     banfuera2 = true; 
    	                }
    	               g++; 
    	          }
    	     if(banpint == 1){
    	        banfuera = true;
    	     }
    	     f++;        
    	}	 
	  }%>
       
   <tr class="procesosdisponibles">
	<td valign="middle" width=""><a href="../../servlet/ventanilla.MenuServlet?list=23"   target="panel" title="Reportes" tabindex="-1" onmouseover="self.status='Reportes';return true;"><img src="../imagenes/bullet1.gif" border="0"> Reportes</a></td>
    <td valign="middle"><a href="PageBuilderSelectTotales.jsp"   tabindex="-1" target="panel" title="Totales" onmouseover="self.status='Totales';return true;"><img src="../imagenes/bullet1.gif" border="0"> Totales</a></td>
    <td valign="middle"><a href="PageBuilderDiario.jsp"   tabindex="-1" target="panel" title="Diario Electr&oacute;nico" onmouseover="self.status='Diario Electr&oacute;nico';return true;"><img src="../imagenes/bullet1.gif" border="0"> Diario Electr&oacute;nico</a></td>
    <td valign="middle" width=""><a href="../../servlet/ventanilla.Logoff?salida=0"   tabindex="-1" target="panel" title="Salida Temporal" onmouseover="self.status='Salida Temporal';return true;"><img src="../imagenes/bullet1.gif" border="0"> Salida Temporal</a></td>
    <td valign="middle" width=""><a href="../../servlet/ventanilla.Logoff?salida=1" onClick="return confirm('<%out.print(mensaje);%>')"   tabindex="-1" target="panel" title="Salida Permanente"><img src="../imagenes/bullet1.gif" border="0"> Salir Permanente</a></td>
    <!--td valign="middle"><a href="Help.jsp"   tabindex="-1" target="panel" title="Ayuda" onmouseover="self.status='Ayuda';return true;"><img src="../imagenes/bullet1.gif" border="0"> Ayuda</a></td-->
   </tr>
<%}
else if ( userinfo.get("N_NIVEL").equals("3") || userinfo.get("N_NIVEL").equals("4"))
{%>
    <tr class="procesosdisponibles">&nbsp;</tr>
    <tr class="procesosdisponibles">
    <td valign="middle" width=""><a href="../../servlet/ventanilla.MenuServlet?list=99"   tabindex="-1" target="panel" title="Gerente" onmouseover="self.status='Gerente';return true;"><img src="../imagenes/bullet1.gif" border="0">Gerente</a></td>
    <td valign="middle" width=""><a href="PageBuilderGerente.jsp"   tabindex="-1" target="panel" title="Totales" onmouseover="self.status='Totales';return true;"><img src="../imagenes/bullet1.gif" border="0"> Totales</a></td>
    <td valign="middle" width=""><a href="Reportes.jsp"   tabindex="-1" target="panel" title="Reportes" onmouseover="self.status='Reportes';return true;"><img src="../imagenes/bullet1.gif" border="0"> Reportes</a></td>
    <td valign="middle" width=""></td>
    <td valign="middle" width=""><a href="../../servlet/ventanilla.Logoff?salida=0"   tabindex="-1" target="panel" title="Salida Temporal" onmouseover="self.status='Salida Temporal';return true;"><img src="../imagenes/bullet1.gif" border="0"> Salida Temporal</a></td>
    <td valign="middle" width=""></td>
    <td valign="middle" width=""><a href="../../servlet/ventanilla.Logoff?salida=1" onClick="return confirm('<%out.print(mensaje);%>')"   tabindex="-1" target="panel" title="Salida permanente" onmouseover="self.status='Salida Permanente';return true;"><img src="../imagenes/bullet1.gif" border="0"> Salir Permanente</a></td>
    <!--td valign="middle"><a href="Help.jsp"   tabindex="-1" target="panel" title="Ayuda" onmouseover="self.status='Ayuda';return true;"><img src="../imagenes/bullet1.gif" border="0"> Ayuda</a></td-->
    </tr>
<%}%>
  </table>
  </layer>
 </body>
</html>
