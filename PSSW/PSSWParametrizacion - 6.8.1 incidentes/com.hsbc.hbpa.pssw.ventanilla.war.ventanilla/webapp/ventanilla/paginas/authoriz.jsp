<!-- 
// CCN - 4360581 - 12/03/2007 - Se convierten los estilos en jsf y se hace un include en el jsp
-->

<%@ page session="true"%>
<%
response.setHeader("Cache-Control","no-store"); //HTTP 1.1
response.setHeader("Pragma","no-cache"); //HTTP 1.0
response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
%>
<HTML>
  <HEAD>
     <title>Autorización Remota</title>
     <%@include file="style.jsf"%>
 </HEAD>
 <script language="JavaScript">
 <!--
  var Nav4 = ((navigator.appName == "Netscape") && (parseInt(navigator.appVersion) >= 4))
  function closeme(){
   window.close()
  }
  function handleOK(form){
   if(window.opener && !window.opener.closed){
    transferData()
    window.opener.dialogWin.returnFunc.doValidate()
   }
   else{
   alert("Se cerró la ventana de Autorización!")
  }
  closeme()
  return false
 }
 function checkAutho(){
   var txtAutho = "0";
   if(txtAutho == '1'){
     handleOK()
   }
 }
 function handleCancel(){
  window.opener.dialogWin.returnedValue = '0'
  window.opener.dialogWin.returnFunc.doValidate()
  closeme()
  return false
 }
 function getFormData(form){
  var searchString = ""
  var onePair
  for (var i = 0; i < form.elements.length; i++){
   if (form.elements[i].type == "hidden"){
    onePair = escape(form.elements[i].name) + "&" + form.elements[i].value
   }
   else continue
   searchString += onePair + "&"
  }
  return searchString
 }
 function transferData(){
  if(top.opener && !top.opener.closed){
   top.opener.dialogWin.returnedValue = getFormData(document.prefs)
  }
 }

 function verifyUsers()
 {
//alert(window.opener.panel.document.forms[0].cajero.options[window.opener.panel.document.forms[0].cajero.selectedIndex].value);
  document.prefs.action = '../../servlet/ventanilla.authorizRemote';
  if(window.opener.panel != undefined){
    
    if (window.opener.panel.location.toString().indexOf("ResponseDiario") != -1)
    { //Autorizacion por Reverso de Diario.
       document.prefs.indice.value = window.opener.panel.document.forms[0].indice.value;
       document.prefs.nopagina.value = window.opener.panel.document.forms[0].nopagina.value;
    } 
   
    
    else  //Cambio de Usuario en el Diario
    if (window.opener.panel.location.toString().indexOf("PageBuilderDiario") != -1)
    {
       document.prefs.newcajero.value = window.opener.panel.document.forms[0].cajero.options[window.opener.panel.document.forms[0].cajero.selectedIndex].value;
    }
    else //Autorizacion por Flujo normal de Txn.
    if (window.opener.panel.location.toString().indexOf("PageBuilder") != -1 || window.opener.panel.location.toString().indexOf("PageServlet") != -1)
    {
       for (var i=0; i < window.opener.panel.document.forms[0].elements.length; i++)
         if (window.opener.panel.document.forms[0].elements[i].type == "text")
           document.prefs.ValuesToAuto.value = document.prefs.ValuesToAuto.value + window.opener.panel.document.forms[0].elements[i].name + "=" + window.opener.panel.document.forms[0].elements[i].value + "#";
    }
    else
    { //Autorizacion por Override

       document.prefs.override.value= "1";
    }
  }
  else
  {
    document.prefs.teller.value = window.opener.document.forms[0].teller.value;
    document.prefs.firmaCajeroC.value = '1';
  }
  document.prefs.submit();
 }
//-->
 </script>
 </head>
<body onLoad="top.opener.setFieldFocus(window.document); if (top.opener) top.opener.blockEvents(); top.formName=document.forms[0];checkAutho(); verifyUsers();" onUnload="if (top.opener) top.opener.unblockEvents()" topmargin="0" leftmargin="0" bgproperties="fixed">
   <form name="prefs" action="" method="POST">
   <br>
   <p>Buscando Supervisores firmados para solicitar Autorizaci&oacute;n Remota,</p>
   <p>Por Favor espere...</p>
   <input name="ValuesToAuto" type="hidden" value="">
   <input name="indice" type="hidden" value="-1">
   <input name="nopagina" type="hidden" value="">
   <input name="override" type="hidden" value="0">
   <input name="newcajero" type="hidden" value="0">
   <input type="hidden" name="firmaCajeroC" value="0">
   <input type="hidden" name="teller" value="">
  </form>
 </body>
 </html>