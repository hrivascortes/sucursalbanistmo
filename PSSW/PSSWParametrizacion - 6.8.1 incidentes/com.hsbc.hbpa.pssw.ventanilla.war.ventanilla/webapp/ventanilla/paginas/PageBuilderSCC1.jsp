<!-- 
118 
// CCN - 4360581 - 12/03/2007 - Se convierten los estilos en jsf y se hace un include en el jsp
-->

<%@ page session="true"%>
<%
response.setHeader("Cache-Control","no-store"); //HTTP 1.1
response.setHeader("Pragma","no-cache"); //HTTP 1.0
response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
	<head>
		<title>Protección de Cheques Empresariales</title>
		<%@include file="style.jsf"%>
	</head>
	<Script language="JavaScript">
		var valor1 = "1"
	
		function sendservlet()
		{
			document.entry.opcion1.value = valor1;		
			document.entry.action = "../servlet/ventanilla.ServiciosServlet"
			document.entry.submit();
		}

		function setValues1()
		{
	 		valor1 = document.entry.opcion1.value;
		}
		
   </SCRIPT>	
	<body bgcolor="#FFFFFF" onload="top.setFieldFocus(window.document)">   
        <h1><%=session.getAttribute("page.txnImage")%></h1>
		<form name="entry">
			<input type="hidden" name="cTxn" value="<%= session.getAttribute("page.cTxn") %>">
			<input type="hidden" name="moneda" value="<%= session.getAttribute("page.moneda")%>">
			<input type="hidden" name="parte" value="parte1">
			<table border="0" cellspacing="0">       			
<%						
			out.println("<b><h3>");
			out.print(session.getAttribute("page.cTxn") + "  " + (String)session.getAttribute("page.txnlabel"));
			out.println("</b></h3>");
			out.println("<tr><td>");		
			out.println("<br>");		
			out.println("</td></tr>");					
			String strTxn    = (String) session.getAttribute("page.cTxn");
			String strMoneda = (String) session.getAttribute("page.moneda");						
			out.println("<tr>");
			out.println("	<td><b>Servicio de :</b></td>");		   	
			out.println("	<td>");
			out.println("		<SELECT name=opcion1 size=1 onchange=\"setValues1()\">");
			out.println("			<OPTION value=1>Alta </OPTION>");
			out.println("			<OPTION value=2>Cancelación</OPTION>");
			out.println("			<OPTION value=3>Suspensión</OPTION>");
			out.println("			<OPTION value=4>Reactivación</OPTION>");
			out.println("		</SELECT>");
			out.println("	</td>");
			out.println("</tr>");
			out.println("<tr><td>");
			out.println("<br>");
			out.println("</td></tr>");
			out.println("<tr>");
		   out.println("	<td>");
			out.print("<a href=\"JavaScript:sendservlet()\"><img src=\"../ventanilla/imagenes/b_aceptar.gif\" border=\"0\"></a>");
			out.println("	</td>");			
			out.println("</tr>");
%>			
		</table>			
		</form>
	</body>
</html>
