<!--
//*************************************************************************************************
//             Funcion: JSPgenerico para txn N181
//            Elemento: PageBuilderN181.jsp
//          Creado por: Alejandro Gonzalez Castro
//      Modificado por: Juvenal R. Fernandez V.
//*************************************************************************************************
// CCN - 4360275 - 18/02/2005 - WAS 5.1
// CCN - 4360334 - 17/06/2005 - Se convierte fieldLector a fragmento
// CCN - 4360581 - 12/03/2007 - Se convierten los estilos en jsf y se hace un include en el jsp
//*************************************************************************************************
-->
<%@ page session="true" import="java.util.*"%>
<%
response.setHeader("Cache-Control","no-store"); //HTTP 1.1
response.setHeader("Pragma","no-cache"); //HTTP 1.0
response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<%
String MontoPro = new String("");
Vector listaCampos = (Vector)session.getAttribute("page.listcampos");
Vector listaContenidos = (Vector)session.getAttribute("page.listcont");
String txnAuthLevel = (String)session.getAttribute("page.txnAuthLevel");
String txnIMGAutoriz = (String)session.getAttribute("page.txnIMGAutoriz");
Vector v = (Vector)session.getAttribute("page.specialacct");
java.util.Hashtable InfoSuc = (java.util.Hashtable)session.getAttribute("branchinfo");
java.util.Hashtable datasession = (java.util.Hashtable)session.getAttribute("page.datasession");
if(datasession == null)
  datasession = new java.util.Hashtable();
int k;
%>
<%!
    private String addpointcomm(String valor)
    {
	int longini = valor.length();
	String entero = "";
	String cents = "";
	if (longini > 2)
	{
          cents  = valor.substring(longini-2,longini);
	  entero = valor.substring(0,longini-2);
//	String entero = valor.substring(0,longini);
	  int longente = entero.length();
	  for (int i = 0; i < (longente-(1+i))/3; i++)
	  {
            longente = entero.length();
	    entero = entero.substring(0,longente-(4*i+3))+','+entero.substring(longente-(4*i+3));
	  }
  	  entero = entero + '.' + cents;
  	}
  	else
  	{
  	  if (longini == 1)
  	    entero = "0.0" + valor;
  	  else
  	    entero = "0." + valor;
  	}
	return entero;
    }
%>
<html>
<head>
  <title>Datos de la Transaccion</title>
  <%@include file="style.jsf"%>
<script language="JavaScript">
<!--
var formName = 'Mio'
var specialacct = new Array()
<%
 if(!v.isEmpty()){
  for(int iv = 0; iv < v.size(); iv++){
   out.print("specialacct[" + iv + "]='" + v.get(iv) + "'" + '\n');
  }
 }
%>
function validate(form)
{
<%
  for(int i=0; i < listaCampos.size(); i++)
   {
    Vector vCampos = (Vector)listaCampos.get(i);
    vCampos = (Vector)vCampos.get(0);
    if( vCampos.get(4).toString().length() > 0 && datasession.get(vCampos.get(0)) == null)
    {
      out.println("  if( !top.validate(window, document.entry." + vCampos.get(0) + ", '" + vCampos.get(4) +"') )");
      out.println("    return");
    }
   }
%>
  form.validateFLD.value = '1'
<%
 if(!txnAuthLevel.equals("1") && !txnIMGAutoriz.equals("1"))
	{
    out.println("  form.submit();");
    out.println("var obj = document.getElementById('btnCont');"); 
    out.println("obj.onclick = new Function('return false');");   
	}   
%>
}
function AuthoandSignature(entry)
{
 var Cuenta;
 if(entry.validateFLD.value != '1')
  return
 if(entry.needAutho.value == '1' && entry.AuthoOK.value != '1')
 {
  top.openDialog('../ventanilla/paginas/authoriz.jsp', 220, 130, 'top.setPrefs()', entry)
 }

 if(entry.needSignature.value == '1' && entry.SignField.value != '')
{
  Cuenta = eval('entry.' + entry.SignField.value + '.value')
  top.openModalDialog('<%=request.getContextPath()%>/servlet/ventanilla.FirmasServlet?txtDDACuenta=' + Cuenta, 710, 430, 'top.setPrefs2()', entry)
 }
}
//-->
</script>
</head>
<body bgcolor="#FFFFFF" onload="top.setFieldFocus(window.document)">
<br>
<% out.print("<form name=\"entry\" action=\"../servlet/ventanilla.DataServlet\" method=\"post\" ");
   if(txnAuthLevel.equals("1"))
    out.print("onSubmit=\"return validate(this)\"");
   out.println(">");
%>
<h1><%=session.getAttribute("page.txnImage")%></h1>
<h3>
<%out.print(session.getAttribute("page.cTxn") + "  " + (String)session.getAttribute("page.txnlabel"));%>
</h3>
<table border="0" cellspacing="0">
<%
int i = 0;
for(i =  0; i < listaCampos.size(); i++)
{
    Vector vCampos = (Vector)listaCampos.get(i);
    vCampos = (Vector)vCampos.get(0);
    if(!vCampos.get(0).toString().equals("txtCodSeg"))
    {
        out.println("<tr>");
        out.println("  <td>" + vCampos.get(2) + ":</td>");
        out.println("  <td width=\"10\">&nbsp;</td>");
        if(!vCampos.get(0).toString().substring(0,3).equals("lst"))
        {
            if( datasession.get(vCampos.get(0)) != null )
            {
                if(vCampos.get(0).toString().equals("txtMonto"))
                {
                    out.print  ("  <td>" + addpointcomm(datasession.get(vCampos.get(0)).toString()) + "</td");
                }
                else
                    out.print  ("  <td>" + datasession.get(vCampos.get(0)) + "</td");
            }
            else
            {
                if(vCampos.get(0).toString().equals("txtDepositante") || vCampos.get(0).toString().equals("txtADisposicion") ||
                   vCampos.get(0).toString().equals("txtConcepto") || vCampos.get(0).toString().equals("txtAutoridad"))
                {
                    out.print  ("  <td><textarea cols=\"40\" rows=\"4\" maxlength=\"" + vCampos.get(3) + "\" name=\"" + vCampos.get(0) + "\" size=\"" + vCampos.get(3) +"\" tabindex=\""+(i+1)+"\"");
                }
                else if(vCampos.get(0).toString().substring(0,3).equals("pss"))
                {
                    out.print  ("  <td><input maxlength=\"" + vCampos.get(3) + "\" name=\"" + vCampos.get(0) + "\" size=\"" + vCampos.get(3) +"\" type=\"Password\"" + "\" tabindex=\""+(i+1)+"\"");
                }
                else
                {
                    out.print("  <td><input maxlength=\"" + vCampos.get(3) + "\" name=\"" + vCampos.get(0) + "\" size=\"" + vCampos.get(3) +"\" type=\"text\"" + "\" tabindex=\""+(i+1)+"\"");
                }
                String CampoR = (String)vCampos.get(5);
                if(CampoR != null && CampoR.length() > 0)
                {
                    CampoR = CampoR.trim();
                    out.print(" value=\"" + CampoR + "\"");
                }
            }
        }
        else
        {
            out.print("  <td><select tabindex='"+(i+1)+"' name=\"" + vCampos.get(0) + "\" size=\"" + "1" + "\"");
    	      //if( vCampos.get(3).toString().length() > 0 ){
            if( vCampos.get(4).toString().length() > 0 )
            {
                if(vCampos.get(0).toString().substring(0,3).equals("rdo"))
                    out.println(" onClick=\"top.validate(window, this, '" + vCampos.get(4) +"')\">");
                else
                    out.println(" onChange=\"top.validate(window, this, '" + vCampos.get(4) +"')\">");
            }
            else
            {
                out.println(">");
        }
	      for(k=0; k < listaContenidos.size(); k++){
	       Vector v1Campos = (Vector)listaContenidos.get(k);
	       for(int j = 0; j < v1Campos.size(); j++){
	         Vector v1Camposa = (Vector)v1Campos.get(j);
	         if(v1Camposa.get(0).toString().equals(vCampos.get(0).toString()))
	           out.println("  <option value=\"" + v1Camposa.get(3) + "\">" + v1Camposa.get(2));
	       }
	      }
	      out.println("  </select></td>");
	     }
	     if( vCampos.get(4).toString().length() > 0 && !vCampos.get(0).toString().substring(0,3).equals("lst")&& !vCampos.get(0).toString().equals("txtDepositante")  && !vCampos.get(0).toString().equals("txtADisposicion") &&
           !vCampos.get(0).toString().equals("txtConcepto") && !vCampos.get(0).toString().equals("txtAutoridad"))
	      out.println(" onBlur=\"top.estaVacio(this)||top.validate(window, this, '" + vCampos.get(4) +"')\" onKeyPress=\"top.keyPressedHandler(window, this, '" + vCampos.get(4) +"')\" ></td>");
	     else if(!vCampos.get(0).toString().substring(0,3).equals("lst") && !vCampos.get(0).toString().equals("txtDepositante")  && !vCampos.get(0).toString().equals("txtADisposicion") &&
               !vCampos.get(0).toString().equals("txtConcepto") && !vCampos.get(0).toString().equals("txtAutoridad"))
            out.println(">");
	     else
       out.println(" onChange=\"top.validate(window, this, '" + vCampos.get(4) +"')\"></textarea></td>");
//	      out.println("></textarea></td>");
	     out.println("</tr>");
	    }
	    else{
	     %><%@include file="fieldLector.jsf"%><%
	     i = k;
	    }
	   }
	%>
</table>
<%!		private String CalendarToString(int option, int sum)
   		{
    		java.util.Calendar now = java.util.Calendar.getInstance();
		    String temp = new Integer(now.get(option) + sum).toString();
		    if( temp.length() != 2 )
		      temp = "0" + temp;
		    return temp;
  		}%>
<%
		String day = CalendarToString(java.util.Calendar.DAY_OF_MONTH, 0);
		String month = CalendarToString(java.util.Calendar.MONTH, 0);
    String[] months = new String[]{"01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12"};
    int mes = Integer.parseInt(month);
	 	String year = CalendarToString(java.util.Calendar.YEAR, 0);
    year = year.substring(1);
    String fecha = day + " / " + months[mes] + " / " + year;
%>
<table>
<%String txn = (String)session.getAttribute("page.cTxn");%>
<tr><td>Plaza :</td><td width ="60">&nbsp;</td><td><%=InfoSuc.get("D_NOM_PLAZA").toString()%></td></tr>
<%if(txn.equals("N181"))
{%>
    <tr><td>Oficina :</td><td width ="60">&nbsp;</td><td><%=InfoSuc.get("D_NOM_SUCURSAL").toString()%></td></tr>
    <tr><td>No. NAFIN :</td><td width ="60">&nbsp;</td><td><%=InfoSuc.get("D_NAFIN").toString()%></td></tr>
<%}%>
<tr><td>Fecha :</td><td width ="60">&nbsp;</td><td><%=fecha%></td></tr>
</table>

<input type="hidden" name="nomplaza" value="<%=InfoSuc.get("D_NOM_PLAZA").toString()%>">
<input type="hidden" name="nomsuc" value="<%=InfoSuc.get("D_NOM_SUCURSAL").toString()%>">
<input type="hidden" name="nafin" value="<%=InfoSuc.get("D_NAFIN").toString()%>">
<input type="hidden" name="fecha" value="<%=fecha%>">

<input type="hidden" name="iTxn" value="<%= session.getAttribute("page.iTxn") %>">
<input type="hidden" name="oTxn" value="<%= session.getAttribute("page.oTxn") %>">
<input type="hidden" name="cTxn" value="<%= session.getAttribute("page.cTxn") %>">
<input type="hidden" name="sucursal" value="<%= session.getAttribute("branch") %>">
<input type="hidden" name="teller" value="<%= session.getAttribute("teller") %>">
<input type="hidden" name="moneda" value="<%= session.getAttribute("page.moneda")%>">
<input type="hidden" name="registro" value="<%=session.getAttribute("empno")%>">
<input type="hidden" name="tllrlvl" value="<%=session.getAttribute("tellerlevel")%>">
<input type="hidden" name="supervisor" value="<%= session.getAttribute("teller") %>">
<input type="hidden" name="AuthoOK" value="0">
<input type="hidden" name="SignOK" value="0">
<input type="hidden" name="validateFLD" value="0">
<input type="hidden" name="needSignature" value="<%out.print(txnIMGAutoriz);%>">
<input type="hidden" name="needAutho" value="<%out.print(txnAuthLevel);%>">
<input type="hidden" name="SignField" value="<%= session.getAttribute("page.SignField") %>">
<input type="hidden" name="override" value="N">
<p>
<% if(txnAuthLevel.equals("1") || txnIMGAutoriz.equals("1"))
    out.println("<a id='btnCont' tabindex='"+(i+1)+"' href=\"javascript:validate(document.entry);javascript:AuthoandSignature(document.entry)\" ><img src=\"../ventanilla/imagenes/b_continuar.gif\" border=\"0\"></a>");
   else
    out.println("<a id='btnCont' tabindex='"+(i+1)+"' href=\"javascript:validate(document.entry)\"><img src=\"../ventanilla/imagenes/b_continuar.gif\" border=\"0\"></a>");
%>
</form>
</body>
</html>
