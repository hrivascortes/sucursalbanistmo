<!--
//*************************************************************************************************
//             Funcion: JSP para el login al sistema
//          Creado por: Alejandro Gonzalez Castro
//      Modificado por: Fausto R. Flores Moreno
//*************************************************************************************************
// CCN -4360163- 06/07/2004 - Se controla click sobre el boton aceptar
// CCN -4360189- 03/09/2004 - Se valida que el registro corresponda con el usuario de RACF
// CCN -4360334- 17/06/2005 - Se elimina variable dbQualifier
// CCN -4360336- 17/06/2005 - Se cambia la obtenci�n de variables de ambiente
// CCN - 4360364 - 19/08/2005 - Se controlan departamentos por Centro de Costos.
//				Se modifica el acceso presentando la lista de los cajeros.						
// CCN - 4360374 - 08/09/2005 - Se genera nuevo paquete a peticion de QA.
// CCN - 4360428 - 21/02/2006 - Se modifica la validacion para registro de usarios nuevos generados por seg. l�gica
// CCN - 4360456 - 07/04/2006 - Se realiza correcci�n para registros de usuarios de 7 digitos.
// CCN - 4360541 - 10/11/2006 - Se elimina system para mostrar sentencia SQL
// CCN - 4360581 - 12/03/2007 - Se convierten los estilos en jsf y se hace un include en el jsp
//*************************************************************************************************
// Proyecto PSSW Panam�
//	Modificado por: Humberto Enrique Balleza Mej�a
	
-->
<%@ page session="true" import="java.util.*,java.sql.*,com.bital.util.ConnectionPoolingManager"%>
<html> 
<%
String ambiente = "";
javax.naming.Context initialContext = null;
String RACF = null;
String sRACF = null;
String sucursal = null;
String PostFixUR = ConnectionPoolingManager.getPostFixUR();
Vector cajeros = null;	
String band = "N";

		RACF = request.getRemoteUser().toString();
	
	sRACF= RACF.toUpperCase();	
		
	if(RACF.startsWith("i")||RACF.startsWith("I"))
    	RACF = RACF.substring(1);
	else
    	RACF = RACF.substring(2);
	int len = RACF.length();
	
	try
	{	
		initialContext = new javax.naming.InitialContext();
		ambiente = (String)initialContext.lookup("java:comp/env/AMB");
	}
		catch( javax.naming.NamingException namingException ){
		//System.out.println( "Error ConnectionPoolingManager::Context.lookup: [" + namingException.toString() + "]");
	}	
	
  // SELECT
  String sql = null;
  Connection pooledConnection = null;
  PreparedStatement newPreparedStatement = null;
  
  if(ambiente.equalsIgnoreCase("P")||ambiente.equalsIgnoreCase("Q"))
  {
    sql = "SELECT D_NUM_SUCURSAL FROM TC_IP_SUCURSALES WHERE  C_DIRECCION_IP = ? "+PostFixUR;
    pooledConnection = ConnectionPoolingManager.getPooledConnection( );
    ResultSet query = null;
    try{
        if(pooledConnection != null) {
            newPreparedStatement = pooledConnection.prepareStatement("SELECT D_NUM_SUCURSAL FROM TC_IP_SUCURSALES WHERE  C_DIRECCION_IP = ? "+PostFixUR);
            newPreparedStatement.setString(1,request.getRemoteAddr());
            
            query = newPreparedStatement.executeQuery();
            if(query.next()) 
                sucursal = query.getString("D_NUM_SUCURSAL");
                
        }
    }
    catch(java.sql.SQLException sqlException) {
	    //System.out.println("Error firma::getIP: [" + sql + "] ");
        //System.out.println("Error firma::getIP: [" + sqlException.toString() + "] ");
    }
    finally {
        try{
            if(query != null){
                query.close();
                query = null;
            }
            if(newPreparedStatement != null){
                newPreparedStatement.close();
                newPreparedStatement = null;
            }
            if(pooledConnection != null){
                pooledConnection.close();
                pooledConnection = null;
            }
        }
        catch(java.sql.SQLException sqlException){
            //System.out.println("Error firma::getIP FINALLY: [" + sqlException.toString() + "] ");
        }
    }
    
   
  sucursal = sucursal + "%";	
  sql = "SELECT C_CAJERO FROM TC_USUARIOS_SUC WHERE D_EJCT_CTA='"+sRACF+"' AND C_CAJERO LIKE '"+sucursal+"' ORDER BY C_CAJERO "+PostFixUR;
  }
  else
  sql = "SELECT C_CAJERO FROM TC_USUARIOS_SUC WHERE D_EJCT_CTA='"+sRACF+"' ORDER BY C_CAJERO "+PostFixUR;
	  
  ResultSet rs = null;
  PreparedStatement stmt = null;
  pooledConnection = ConnectionPoolingManager.getPooledConnection();

  try
  {
    if( pooledConnection != null )
    {
	    if(ambiente.equalsIgnoreCase("P")||ambiente.equalsIgnoreCase("Q"))
	    {
    	  stmt = pooledConnection.prepareStatement("SELECT C_CAJERO FROM TC_USUARIOS_SUC WHERE D_EJCT_CTA=? AND C_CAJERO LIKE ? ORDER BY C_CAJERO "+PostFixUR);
	      stmt.setString(1,sRACF);
    	  stmt.setString(2,sucursal);
	      rs = stmt.executeQuery();
	    }
	    else
	    {
       	  stmt = pooledConnection.prepareStatement("SELECT C_CAJERO FROM TC_USUARIOS_SUC WHERE D_EJCT_CTA=? ORDER BY C_CAJERO "+PostFixUR);
	      stmt.setString(1,sRACF);
	      rs = stmt.executeQuery();
	    }
	      
      if(rs != null)
      {
      	cajeros = new Vector();
   		while(rs.next() )
   		{
    		cajeros.add(rs.getString(1).trim());
   		}
	  	
	  	if(cajeros.size() <= 0 )
		{
			if(sRACF.startsWith("SG") || sRACF.startsWith("DG") )
				band = "S";
			else
		  		session.setAttribute("error","El usuario de RACF capturado, no tiene cajeros asignados. Solicite al Gerente que le asigne un cajero.<br> Hasta no realizar este mantenimiento su acceso sera denegado");
	  	}
        rs.close();
	  }
    }
    else
 		session.setAttribute("error","Error al obtener conexi�n en SELECT, intente de nuevo porfavor. Acceso denegado");
  }
  catch(Exception e1)
  {
   	session.setAttribute("error","Error al obtener conexi�n en SELECT, intente de nuevo porfavor. Acceso denegado");
    //System.out.println(e1.getMessage());
  }
  finally
  {
    if( stmt != null )
      stmt.close();
    if( pooledConnection != null )
      pooledConnection.close();
  }

//FIN SELECT   
%>

 <title>Firma al Sistema</title>
 <%@include file="style.jsf"%>

 <script language="JavaScript">
<!--
 
 function containsElement(arr, ele) {
  var found = false, index = 0;
  while(!found && index < arr.length)
   if(arr[index] == ele)
  found = true;
  else
  index++;
  return found;
 }
 
 function getIndex(input) {
  var index = -1, i = 0, found = false;
  while (i < input.form.length && index == -1)
   if (input.form[i] == input)index = i;
   else i++;
  return index;
 }


function setFieldFocus(documento) {
 if (documento.forms.length > 0) {
  var field = documento.forms[0];
  //alert(field.length)
  for (i = 0; i < field.length; i++) {
   if(field.elements[i] != null){
    if ((field.elements[i].type == "text") || (field.elements[i].type == "textarea") || (field.elements[i].type.toString().charAt(0) == "s")) {
     documento.forms[0].elements[i].focus();
     break;
    }
   }
  }
 }
}

function isRACF(form)
{

  var txtRegistro = form.idNum.value
  var noRACF = '<%out.print(RACF);%>'
  var sRACF = '<%out.print(sRACF);%>'
  var len = <%out.print(len);%>
  var env = '<%out.print(ambiente);%>'
   if(env == "d" || env=="c")
		return true
   if(len<7){
     if(txtRegistro.charAt(0)=='0' && sRACF.charAt(2)!='0')
       txtRegistro=txtRegistro.substring(1);
     else
     txtRegistro=txtRegistro.substring(0,6);
   }
    if(txtRegistro == noRACF)
        return true
    else
    {
        form.idNum.focus()
        form.idNum.select()
        alert("N�mero de Registro invalido, no coincide con el usuario RACF.")
        form.idNum.value = ''
        return false
    }
}

function isSeries(form)
{
  var txtRegistro = form.idNum.value
  var car = txtRegistro.substring(0,1), i, cont = 0

  for(i = 1; i < txtRegistro.length; i++){
   if(txtRegistro.substring(i, i + 1) == car)
    cont++
  }
  if(cont == txtRegistro.length - 1){
   form.idNum.focus()
   form.idNum.select()
   alert("El <REGISTRO> est� incorrecto, vuelva a intentarlo.")
   txtRegistro = ''
   return false
  }
  return true
}

function isValidRegister(form)
{
  var sum = 0
  var txtRegistro = form.idNum.value

  for(var i=txtRegistro.length-2; i >= 0; i--)
  {
    if( i % 2 == 0 )
      sum += parseInt( txtRegistro.charAt(i) )
    else
    {
      temp = parseInt( txtRegistro.charAt(i) * 2 )
      if( temp < 10 )
        sum += temp
      else
        sum = sum + Math.floor(temp / 10) + (temp % 10)
    }
  }
  if( (sum % 10 == 0 ) && (parseInt(txtRegistro.charAt(txtRegistro.length-1)) == 0) )
    return true
  else if( (10-(sum % 10)) == parseInt(txtRegistro.charAt(txtRegistro.length-1)) )
    return true
  else{
    form.idNum.focus()
    form.idNum.select()
    alert("El registro est� incorrecto, vuelva a intentarlo.")
    return false
  }
}

function isEmptyField(inStr)
{
  var inputStr = Alltrim(inStr)
  var car, i, car2
  inputStr = inputStr.toUpperCase()
  if (inputStr == null || inputStr == "")
    return true
  return false
}

function Alltrim(inputStr)
{
  var inStr = new String(inputStr)

  while(inStr.charAt(0) == ' ')
    inStr = inStr.substring(1, inStr.length)

  while(inStr.charAt(inStr.length) == ' ')
    inStr = inStr.substring(0, inStr.length-1)

  return inStr
}

function validation(form)
{

  if( isEmptyField(form.teller.value) )
  {
    alert("El <CAJERO> es requerido.")
    form.teller.focus()
    return
  }

  if( isEmptyField(form.idNum.value) )
  {
    alert("El <REGISTRO> es requerido.")
    form.idNum.focus()
    return
  }

//Validaci�n de Registro
  if(!isSeries(form) || !isRACF(form))
   return

   if(!checkPass())
        return; 

   if(form.flagPass.value == '1')
   {
        if(!checkConfPass())
            return;
   }

  if(form.horario.selectedIndex != 2)
  {
    form.submit()
	 var obj = document.getElementById('btnCont')
	 obj.onclick = new Function('return false')
    
  }
  else
  {
    top.openDialog('ventanilla/paginas/authoriz.jsp', 220, 130, 'top.setPrefs14()', form.name)
  }
}

function checkPass()
{
    var pass = document.Login.password.value;
    pass =  pass.toUpperCase();
    
    if(pass.length != 8)
    {
        alert("El Password debe contener 8 posiciones")
          document.Login.password.value = "";
          document.Login.password.focus();
  		return false;
    }

	var buf = pass.match("[A-Z0-9�]*")
	if( buf != pass )
	{
		alert("El valor capturado contiene caracteres inv�lidos, verifique...\n Acepta A-Z y 0-9 ")
          document.Login.password.value = "";
          document.Login.password.focus();
  		return false;
	}
     
     if(pass == "12345678")
        return true;

     var count = 0;
     for(var i=0; i < pass.length ; i++)
     {
        var letter = pass.substring(i,i+1);
        var tmp = letter.match("[A-Z�]*");
        
        if(tmp == letter)
        {
            count = count + 1;
        }
    }
    if(count < 2)
    {
        alert("El Password debe contener al menos dos letras");
          document.Login.password.value = "";
          document.Login.password.focus();
        return false;
    }
    if(count > 6)
    {
        alert("El Password debe contener al menos dos n�meros");
          document.Login.password.value = "";
          document.Login.password.focus();
        return false;
    }

    document.Login.password.value= pass;
    return true;
}

function checkConfPass()
{
    var pass = document.Login.password.value;
    pass =  pass.toUpperCase();
    var pass1 = document.Login.password1.value;
    pass1 = pass1.toUpperCase();

    if(pass == "12345678")
    {
        document.Login.password.value = "";
        document.Login.password1.value = "";
        document.Login.password.focus();
        alert("Contrase�a inv�lida...")
        return false;
    }

    if(pass == pass1)
    {
        document.Login.password.value = pass;
        document.Login.password1.value = pass1;
        return true;
    }
    else
    {
        document.Login.password1.value = "";
        document.Login.password1.focus();
        alert("La Confirmaci�n de la Contrase�a es inv�lida...")
        return false;
    }
}

//-->
</script>
</head>
<%
String nomImagen = null;
  Connection pooledConnection1 = null;
  PreparedStatement newPreparedStatement1 = null;
  try{
    pooledConnection1 = ConnectionPoolingManager.getPooledConnection( );
    ResultSet query1 = null;
    
        if(pooledConnection1 != null) {
            newPreparedStatement1 = pooledConnection1.prepareStatement("SELECT D_DATOS FROM TC_DATOS_VARIOS WHERE C_CLAVE = ?"+PostFixUR);
            newPreparedStatement1.setString(1, "IDENTIDAD3");
            
            query1 = newPreparedStatement1.executeQuery();
            if(query1 != null){
					if(query1.next())
           nomImagen = query1.getString("D_DATOS").trim();
          }
  }
  }
	      catch(Exception e1)
	      {
	      System.out.println(e1);
	      }
	      finally
  {
    if( newPreparedStatement1 != null )
      newPreparedStatement1.close();
    if( pooledConnection1 != null )
      pooledConnection1.close();
  } %>
<body text="#104A7B" bgcolor="#FFFFFF" onload="setFieldFocus(window.document)">
<div align="center"><img src="<%="ventanilla/imagenes/"+nomImagen%>" width="582" height="41" border="0">
<p class="error">
<%

  String msg = (String)session.getAttribute("error");
  int flag = 0;
  if( msg != null)
{
    out.print("<font color=#CC0C0C><b>" + msg + "</b><font>");
    session.removeAttribute("error");
    if(msg.startsWith("El Usuario") || msg.startsWith("La Contra"))
    {
        flag = 1;
    }
  }
  else
  {
    out.print("&nbsp;");
  }
%></p></div>
 <form name="Login" action="servlet/ventanilla.LoginServlet" method="POST">
  <table border="0" cellspacing="0" align="center">
   <tr>
<% if (flag > 0)
   {%>
   		<td>Cajero:</td>
		<td width="50">&nbsp;</td>
        <td><%=(String)session.getAttribute("teller")%></td>
        <input name="teller" type="hidden" value="<%=(String)session.getAttribute("teller")%>">
<%  }
	else if( (sRACF.startsWith("SG") || sRACF.startsWith("DG")) && cajeros.size() <= 0)
    {%>    
   		<td>Cajero:</td>
		<td width="50">&nbsp;</td>
        <td><input maxlength="6" name="teller" size="6" type="text" onKeyUp="return FieldautoTab(this, event);" onchange="javascript:fillRecord(this)"></td>
<%	}
    else
    {%>
	    <td>Seleccione Cajero:</td>
    	<td width="50">&nbsp;</td>
		<td><select name="teller" size="1">
	 <%	
	      for(int j = 0; j < cajeros.size(); j++){
	           out.println("  <option value=\"" + cajeros.get(j) + "\">" + cajeros.get(j));
	       }
	      out.println("</td>");
    }%>
   </tr>
   <tr>
    <td>Contrase&ntilde;a:</td>
    <td>&nbsp;</td>
    <td><input maxlength="8" name="password" size="8" type="password" onKeyUp="return FieldautoTab(this, event);" onchange="javascript:checkPass()"></td>
   </tr>
<%  if(flag >0)
    {%>
   <tr>
    <td>Confirme Contrase&ntilde;a:</td>
    <td>&nbsp;</td>
    <td><input maxlength="8" name="password1" size="8" type="password" onKeyUp="return FieldautoTab(this, event);" onchange="javascript:checkConfPass()"></td>
   </tr>
<%  }%>
    <tr>
    <td>N&uacute;mero de Registro:</td>
    <td>&nbsp;</td>
<% if (flag > 0)
   {%>
        <td><%=(String)session.getAttribute("empno")%></td>
        <input name="idNum" type="hidden" value="<%=(String)session.getAttribute("empno")%>">
<%  }
    else
    {%>    
    <td><input maxlength="7" name="idNum" size="7" type="text" onKeyUp="return FieldautoTab(this, event);" onchange="javascript:fillRecord(this)"></td>
<%  }%>
   </tr>
   <tr>
    <td>Horario:</td>
    <td>&nbsp;</td>
    <td><select name="horario">
          <option value="A">A
          <option value="B">B
          <option value="C">C
        </select>
    </td>
   </tr>
   <tr>
<%  if(flag >0)
    {%>
    <td align="center" valign="middle" colspan="3"><br><p><a id='btnCont' href="javascript:top.validation(document.Login);" onmouseover="self.status='Aceptar';return true;" onmouseout="window.status='';"><img src="ventanilla/imagenes/b_aceptar.gif" alt="Aceptar" border="0"></a></td>
    <input type="hidden" name="flagPass" value="1">
<%  }
    else
    {%>
    <td align="center" valign="middle" colspan="3"><br><p><a id='btnCont' href="javascript:top.validation(document.Login);" onmouseover="self.status='Aceptar';return true;" onmouseout="window.status='';"><img src="ventanilla/imagenes/b_aceptar.gif" alt="Aceptar" border="0"></a></td>
    <input type="hidden" name="flagPass" value="0">
<%  }%>
   </tr>
<%  if(flag == 0)
    {%>
   <tr>&nbsp;</tr>
   <tr>
   <td colspan="3">
    <input type="checkbox" name="cambio">Marque si requiere cambio de Contrase&ntilde;a
   </td>
   </tr>
<%  }%>
  </table>
  <input type="hidden" name="supervisor" value="">
  <input type="hidden" name="SignOK" value="0">
  <input type="hidden" name="band" value="<%=band%>">
 </form>
</body>
</html> 
