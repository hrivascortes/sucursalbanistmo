<!--
//*************************************************************************************************
//             Funcion: JSP que despliega respuesta de txn 98XX
//            Elemento: Response98XX.jsp
//          Creado por: Alejandro Gonzalez Castro
// Ultima Modificacion: 03/08/2004
//      Modificado por: Alejandro Gonzalez Castro
//*************************************************************************************************
// CCN - 4360175 - 03/08/2004 - Ajuste size de ventana de impresion
//*************************************************************************************************
-->
<%@ page session="true" import="java.util.*"%>
<%
response.setHeader("Cache-Control","no-store"); //HTTP 1.1
response.setHeader("Pragma","no-cache"); //HTTP 1.0
response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<%!
  private String getString(String s)
  {
   int len = s.length();
   for(; len>0; len--)
    if(s.charAt(len-1) != ' ')
     break;
   return s.substring(0, len);
  }

  private String getItem(String newCadNum, int newOption)
  {
   int iPIndex = newCadNum.indexOf("CONSECUTIVO:") + 12, NumSaltos, Num = 0, Ban = 0;
   String nCadNum = new String("");
   NumSaltos = newOption;
   while(Num < NumSaltos)
   {
    if(newCadNum.charAt(iPIndex) == 0x20)
    {
     while(newCadNum.charAt(iPIndex) == 0x20)
      iPIndex++;
     Num++;
    }
    else
     while(newCadNum.charAt(iPIndex) != 0x20)
      iPIndex++;
   }
   while(newCadNum.charAt(iPIndex) != 0x20 && iPIndex < newCadNum.length()){
    nCadNum = nCadNum + newCadNum.charAt(iPIndex);
    if(iPIndex < newCadNum.length())
      iPIndex++;
    if(iPIndex == newCadNum.length())
      break;
   }

   return nCadNum;
  }
  String tasa ="";
%>
<%
Vector resp = (Vector)session.getAttribute("response");
Hashtable datasession = (Hashtable)session.getAttribute("page.datasession");
%>

<html>
<head>
  <title>Certificación</title>
<style type="text/css">Body {font-family: Arial, Helv; font-size: 10pt; font-weight: bold; color:#000000}</style>
<script language="javascript">
<!--
  sharedData = '0';

  function formSubmitter(){
    clearInterval(formSubmitter);
    if(sharedData != '0')
    {
      document.forms[0].submit()
      sharedData = '0';
    }
  }
//-->
</script>

</head>
<body bgcolor="#FFFFFF" onload="top.setFieldFocus(window.document);setInterval('formSubmitter()',2000)">
<p>
<%
  Stack flujotxn = new java.util.Stack();
  String codigo = "";
  String lineresp = "";
  String needOverride = "NO";
  if(session.getAttribute("page.flujotxn") != null)
  	flujotxn = (Stack)session.getAttribute("page.flujotxn");

  if( resp.size() != 4 )
   out.println("<b>Error de conexi&oacute;n</b><br>");
  else
  {
     try
   	{
     	codigo = (String)resp.elementAt(0);
   	if( !codigo.equals("0") )
    	    out.println("<b>Transaccion Rechazada</b><br><br>");
        if ( codigo.equals("0") || codigo.equals("2")) {
     	   int lines = Integer.parseInt((String)resp.elementAt(1));
    	   String szResp = (String)resp.elementAt(3);
    	   for(int i=0; i<lines; i++)
		{
    		String line = szResp.substring( i*78, Math.min(i*78+77, szResp.length()) );
   			lineresp = lineresp + line;
     		out.println(getString(line) + "<p>");
     		if(Math.min(i*78+77, szResp.length()) == szResp.length())
      		break;
    	   }
	   tasa =  lineresp.substring(32,39);
        }
        else {
            if ( codigo.equals("2") || codigo.equals("3")) {
               out.println("<b>Transacci&oacute;n Requiere Autorizaci&oacute;n</b>");
            }
            else
              out.println("<b>Ocurri&oacute; un Error de Comunicaci&oacute;n, Verifique.</b>");
        }
     }
     catch(NumberFormatException nfe){
     out.println("<b>Error de conexi&oacute;n</b><br>");
    }
  }
%>
<p>
<form name="Txns" action="servlet/PageServlet" method="post">
<table border="0" cellspacing="0">
<tr>
<td>
<%
  String isCertificable = "";
  if(session.getAttribute("page.certifField") != null)
    isCertificable = (String)session.getAttribute("page.certifField");  // Para Certificación

  String txtCadImpresion  = ""; // Para Certificación

  String udi = (String)datasession.get("tasaUDI");
  String monto = (String)datasession.get("montodisplayUDI");
  txtCadImpresion = "~TRANSFERENCIA~" + tasa.trim() + "~" + udi.trim() + "~" + monto + "~";

  if(!flujotxn.empty()){
   out.print("<div align=\"center\">");
    if(!isCertificable.equals("N")){ // Para Certificación
       session.setAttribute("Consecutivo", getItem((String)resp.elementAt(3), 1)); // Para Certificación
       session.setAttribute("Tipo", isCertificable); // Para Certificación
       if(codigo.equals("0"))
         out.println("<script language=\"javascript\">top.openDialog('../../servlet/ventanilla.ImprimirServlet?txtCadImpresion=" + txtCadImpresion + "', 160, 120, 'top.setPrefs4()', document.Txns)</script>"); // Para Certificación
       else {
          if ( codigo.equals("2") || codigo.equals("3")) {
             needOverride = "SI";
             out.print("<script languaje=\"javascript\"> top.openDialog('authoriz.jsp', 220, 130, 'top.setPrefs8()', document.Txns) </script>");
          }
          else
            out.println("<a href=\"javascript:top.openDialog('../../servlet/ventanilla.ImprimirServlet?txtCadImpresion=" + txtCadImpresion + "', 160, 120, 'top.setPrefs4()', document.Txns)\"><img src=\"imagenes/b_continuar.gif\" border=\"0\"></a>"); // Para Certificación
       }
   }
   else{
      if(codigo.equals("0")){
        out.println("<script language=\"javascript\">sharedData = '1'</script>");
        out.println("<font color='red'>procesando...</font>");
        
      }
      else {
         if ( codigo.equals("2") || codigo.equals("3")) {
           needOverride = "SI";
           out.print("<script languaje=\"javascript\"> top.openDialog('authoriz.jsp', 220, 130, 'top.setPrefs8()', document.Txns) </script>");
         }
         else
            out.print("<input type=\"image\" src=\"imagenes/b_continuar.gif\" alt=\"\" border=\"0\">");
      }
   }
   out.print("</div>");
  }
  else{
   session.setAttribute("Consecutivo", getItem((String)resp.elementAt(3), 1)); // Para Certificación
   session.setAttribute("Tipo", isCertificable); // Para Certificación
   if ( codigo.equals("2") || codigo.equals("3")) {
      needOverride = "SI";
      out.print("<script languaje=\"javascript\"> top.openDialog('authoriz.jsp', 220, 130, 'top.setPrefs8()', document.Txns) </script>");
   }
   else
     out.println("<script language=\"javascript\">top.openDialog('../../servlet/ventanilla.ImprimirServlet?txtCadImpresion=" + txtCadImpresion + "', 160, 120, 'top.setPrefs4()', document.Txns)</script>");
  }
%>
  <input type="hidden" name="transaction" value="<%= session.getAttribute("page.iTxn") %>">
  <input type="hidden" name="transaction1" value="<%= session.getAttribute("page.oTxn") %>">
  <input type="hidden" name="supervisor" value="<%= session.getAttribute("supervisor") %>">
  <input type="hidden" name="override" value="<%= needOverride%>">
</td>
</tr>
</table>
</form>
</body>
</html>
