<!--
//*************************************************************************************************
//             Funcion: JSP que muestra las consultas del Diario
//             Elemento: ResponseDiario.jsp
//          Creado por: Alejandro Gonzalez Castro
//      Modificado por: alejandro Gonzalez Castro
//*************************************************************************************************
// CCN - 4360163 - 06/07/2004 - Se modifica envio de datos para impresion
// CCN - 4360171 - 23/07/2004 - Se despliega el significado de los estatus del Diario
// CCN - 4360175 - 03/08/2004 - Se ajusta size ventana impresion
// CCN - 4360178 - 20/08/2004 - Se agregan datos para efectuar txn CON para consultas del diario
// CCN - 4360189 - 03/09/2004 - Se cambia invoke a DiarioQuery para Control de Efectivo
// CCN - 4360204 - 24/09/2004 - Se agrega columna de moneda y se valida que cuando se cierra la ventana
// 		   	 			  de autorización la txn no se reverse
// CCN - 4360402 - 11/11/2005 - Se realizan modificaciones para el detalle en el diario electronico
// CCN - 4360581 - 12/03/2007 - Se convierten los estilos en jsf y se hace un include en el jsp
//*************************************************************************************************
-->
<%@ page session="true" import="java.util.*"%>
<%
response.setHeader("Cache-Control","no-store"); //HTTP 1.1
response.setHeader("Pragma","no-cache"); //HTTP 1.0
response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<%

    int nopagina     = Integer.parseInt(request.getParameter("nopagina").toString());

    String diario = (String)request.getParameter("diario");
    String printcad = "DIARIO~";

    ventanilla.com.bital.admin.DiarioQuery DiarioSQL = null;
    if(diario != null)
    {
        if(session.getAttribute("session.DiarioSQL") == null)
        {
          DiarioSQL = new ventanilla.com.bital.admin.DiarioQuery();
          session.setAttribute("session.DiarioSQL", DiarioSQL);
        }

        String cajero       = request.getParameter("cajero").toString();
        String tipodiario   = request.getParameter("tipodiario").toString();
        String orden        = request.getParameter("orden").toString();
        String moneda       = request.getParameter("moneda").toString();
        int registros       = Integer.parseInt(request.getParameter("registros").toString());
        String cuenta       = request.getParameter("cuenta").toString();
        String importe      = request.getParameter("importe").toString();
        String totalC       = request.getParameter("totalC").toString();
        String totalA       = request.getParameter("totalA").toString();
        String consecutivoI = request.getParameter("consecutivoI").toString();
        String consecutivoF = request.getParameter("consecutivoF").toString();
        String diaI         = request.getParameter("diaI").toString();
        String mesI         = request.getParameter("mesI").toString();
        String anoI         = request.getParameter("anoI").toString();
        String diaF         = request.getParameter("diaF").toString();
        String mesF         = request.getParameter("mesF").toString();
        String anoF         = request.getParameter("anoF").toString();
        String codigotxn    = request.getParameter("codigotxn").toString();
        String status       = request.getParameter("status").toString();
        String nivel        = request.getParameter("tllrlvl").toString();
        String cajeroq      = request.getParameter("teller").toString();
        String ip           = request.getParameter("ip").toString();

        // SETS
        DiarioSQL.setTeller(cajero);
        DiarioSQL.setTypeJournal(tipodiario);
        DiarioSQL.setOrder(orden);
        DiarioSQL.setCurrency(moneda);
        DiarioSQL.setRows(registros);
        DiarioSQL.setAccount(cuenta);
        DiarioSQL.setAmount(importe);
        DiarioSQL.setTotalC(totalC);
        DiarioSQL.setTotalA(totalA);
        DiarioSQL.setConsecFirst(consecutivoI);
        DiarioSQL.setConsecLast(consecutivoF);
        DiarioSQL.setDateFirst(anoI+mesI+diaI);
        DiarioSQL.setDateLast(anoF+mesF+diaF);
        DiarioSQL.setTxnCode(codigotxn);
        DiarioSQL.setStatus(status);
        DiarioSQL.setLevel(nivel);
        DiarioSQL.setTellerQ(cajeroq);
        DiarioSQL.setIP(ip);

        DiarioSQL.executeQuery(session);
    }

    DiarioSQL = (ventanilla.com.bital.admin.DiarioQuery)session.getAttribute("session.DiarioSQL");
    printcad = printcad + DiarioSQL.getTeller() + "~";
    Hashtable Data = new Hashtable();
    Data = DiarioSQL.getData(nopagina);
    Vector Ctxn     = new Vector();
    Vector Cstatus  = new Vector();
    Vector Cconsec  = new Vector();
    Vector Cdate    = new Vector();
    Vector Caccount = new Vector();
    Vector Camount  = new Vector();
    Vector Ccash    = new Vector();
    Vector Ctotalc  = new Vector();
    Vector Ctotala  = new Vector();
    Vector Cserial  = new Vector();
    Vector Cref     = new Vector();
    Vector Crev     = new Vector();
    Vector Cconlink = new Vector();
    Vector Cmoneda  = new Vector();
    Vector Cconnureg = new Vector();

    Ctxn        = (Vector)Data.get("Txn");
    Cstatus     = (Vector)Data.get("Status");
    Cconsec     = (Vector)Data.get("Consec");
    Cdate       = (Vector)Data.get("Date");
    Caccount    = (Vector)Data.get("Account");
    Camount     = (Vector)Data.get("Amount");
    Ccash       = (Vector)Data.get("Cash");
    Ctotalc     = (Vector)Data.get("TotalC");
    Ctotala     = (Vector)Data.get("TotalA");
    Cserial     = (Vector)Data.get("Serial");
    Cref        = (Vector)Data.get("Ref");
    Crev        = (Vector)Data.get("Rev");
    Cconlink    = (Vector)Data.get("ConLink");
    Cmoneda     = (Vector)Data.get("Moneda");
    Cconnureg   = (Vector)Data.get("Connureg");
    int size = Ctxn.size();
    
%>
<html>
<head>
    <title>Diario</title>
		<%@include file="style.jsf"%>
</head>
<body bgcolor="#FFFFFF">
<form name="diariodisplay" action="ResponseDiario.jsp" method="post">
<%if(size > 0)
{%>
    <h1>Diario Electr&oacute;nico</h1>
    <table border="1" align="center" cellspacing="0" width="778">
    <tr bgcolor="#CCCCCC">
        <td align="center">Txn</td>
        <td align="center">Mon.</td>
        <td align="center">Sts.</td>
        <td align="center">Consecutivo</td>
        <td align="center" width="88">Fecha</td>
        <td align="center">Cuenta</td>
        <td align="center">Monto</td>
        <td align="center">Efectivo</td>
        <td align="center">Total<br>Cargo</td>
        <td align="center">Total<br>Abono</td>
        <td align="center">Serial</td>
        <td align="center">Referencia</td>
    </tr>
    <%for(int i=0; i<size; i++)
      {
    
      
      %>
        <tr>
<%      printcad = printcad + (String)Ctxn.get(i)+"~"+(String)Cstatus.get(i)+"~"+(String)Cconsec.get(i)+"~";
        printcad = printcad + (String)Cdate.get(i)+"~"+(String)Caccount.get(i)+"~"+(String)Camount.get(i)+"~";
        printcad = printcad + (String)Ccash.get(i)+"~"+(String)Ctotalc.get(i)+"~"+(String)Ctotala.get(i)+"~";
        printcad = printcad + (String)Cserial.get(i)+"~"+(String)Cref.get(i)+"~";
        session.setAttribute("txtCadImpresion", printcad);
         //ygx
        String mlocal = (String)Cmoneda.get(i);
        //System.out.println(mlocal);
%>
            <td align="center"><a href="javascript:Detalle('<%=Cconnureg.get(i)%>');" title="Detalle"; onmouseover="self.status='Detalle';return true;" onmouseout="window.status='';" alt="Detalle"><%=Ctxn.get(i)%></a></td>
            
            <%
              if (mlocal.equals("N$"))
              {%>
               <td align="center">$</td>
              
            <%} else 
              {%>
               <td align="center"><%=Cmoneda.get(i)%></td>
            <%
              }
            %>
            
            <td align="center">
                <%String status = (String)Cstatus.get(i);
                  String reverso = (String)Crev.get(i);
                    if( status.equals("A") && reverso.equals("S"))
                    {%>
                        <a href="javascript:datosrev(<%=i%>);Authorization();" title="Reversar"; onmouseover="self.status='Reversar';return true;" onmouseout="window.status='';" alt="Reversar">
                        <%=Cstatus.get(i)%></a>
                  <%}
                    else if( status.equals("?") && reverso.equals("S"))
                    {%>
                        <a href="javascript:datosrev(<%=i%>);Consecutivo();Authorization();" title="Reversar"; onmouseover="self.status='Reversar';return true;" onmouseout="window.status='';" alt="Reversar">
                        <%=Cstatus.get(i)%></a>
                  <%}
                    else
                    {%><%=Cstatus.get(i)%><%}%>
            </td>
            <td align="center" width="68"><%=Cconsec.get(i)%></td>
            <td align="center" width="88" vAlign="baseline"><%=Cdate.get(i)%></td>
            <td align="right" ><%=Caccount.get(i)%></td>
            <td align="right"><%=Camount.get(i)%></td>
            <td align="right"><%=Ccash.get(i)%></td>
            <td align="center"><%=Ctotalc.get(i)%></td>
            <td align="center"><%=Ctotala.get(i)%></td>
            <td align="right"><%=Cserial.get(i)%></td>
            <td align="left"><%=Cref.get(i)%></td>
        </tr>
<%  }%>
    <tr>
    <td colspan="12" align="center">
    <br>
    <%  if(nopagina>0)
        {%>
            <a href="javascript:prevpage();submite()" onmouseover="self.status='Pagina Anterior';return true;" onmouseout="window.status='';" alt="Pagina Anterior"><img src="../imagenes/b_pagina_anterior.gif" border="0"></a>
        <%}%>
<%      if(nopagina+1 != DiarioSQL.getNoPages())
        {%>
            <a href="javascript:submite()" onmouseover="self.status='Pagina Siguiente';return true;" onmouseout="window.status='';" alt="Pagina Siguiente"><img src="../imagenes/b_pagina_siguiente.gif" border="0"></a>
<%      }%>
    	  <a href="javascript:top.openDialog('../../servlet/ventanilla.ImprimirServlet', 160, 120, 'top.setPrefs4()', document.dariodisplay)"><img src="../imagenes/b_imprimir.gif" border="0"></a>
    </tr>
    <tr>
        <td align="center" colspan="12" align="right">P&aacute;gina <%=nopagina+1%> de <%=DiarioSQL.getNoPages()%></td>
    </tr>
    <tr bgcolor="#CCCCCC"><td colspan="12" align="center">Descripcion de Estatus</td></tr>
    <tr>
        <td colspan="6">A - Aceptada / Txn Original</td>
        <td colspan="6">C - Aceptada / Txn Reverso</td>
    </tr>
    <tr>
        <td colspan="6">R - Rechazada</td>
        <td colspan="6">X - Reversada / Txn Original</td>
    </tr>  
    <tr>
        <td colspan="6">? - Estatus Desconocido</td>
        <td colspan="6">P - Estatus Desconocido / Txn Reverso</td>
    </tr>
    </table>
<%
}
else
{%>
    <p>
    <b>No existen Registros para ese Criterio de Selecci&oacute;n...</b>
<%}%>
<input type="hidden" name="teller" value="<%=session.getAttribute("teller")%>">
<input type="hidden" name="sucursal" value="<%=session.getAttribute("branch")%>">
<input type="hidden" name="cajero" value="<%=request.getParameter("cajero").toString()%>">
<input type="hidden" name="supervisor" value="<%= session.getAttribute("teller") %>">
<input type="hidden" name="AuthoOK" value="0">
<input type="hidden" name="SignOK" value="0">
<input type="hidden" name="consecutivo" value="0">
<input type="hidden" name="indice" value="0">
<input type="hidden" name="nopagina" value="<%=nopagina+1%>">
<input type="hidden" name="nopaginaorig" value="<%=nopagina%>">
<%session.setAttribute("noreverso", "0");%>
<input type="hidden" name="key" value="0">
</form>
</body>
<script language="JavaScript">
top.formName = '';
function Authorization()
{
    if(window.document.diariodisplay.AuthoOK.value != '1')
    {
        top.openDialog('../../ventanilla/paginas/authoriz.jsp', 400, 200, 'top.setPrefs7()', window.document.diariodisplay);
    }
}

function Consecutivo()
{
    top.openDialog('../../ventanilla/paginas/consecreverso.jsp', 220, 90, 'top.setPrefs11()', window.document.diariodisplay);
}

function prevpage()
{
   <%nopagina = nopagina-1;%>
   window.document.diariodisplay.nopagina.value=<%=nopagina%>;
}

function datosrev(indice)
{
    window.document.diariodisplay.indice.value = indice;
    window.document.diariodisplay.action = "ResponseReverso.jsp";
}
function submite()
{
  document.diariodisplay.action = "ResponseDiario.jsp";
  document.diariodisplay.submit();
}
function Detalle(llave)
{
    window.document.diariodisplay.key.value = llave;
	document.diariodisplay.action = "ResponseDetalleDiario.jsp";
  	document.diariodisplay.submit();
}
</script>
</html>
