<!--
//*************************************************************************************************
//             Funcion: JSP que presenta las opciones de la txn 018C
//            Elemento: PageBuilderM022.jsp
//          Creado por: Alejandro Gonzalez Castro
//      Modificado por: Juvenal R. Fernandez V.
//*************************************************************************************************
// CCN - 4360334 - 17/06/2005 - Se elimina variable dbQualifier
// CCN - 4360581 - 12/03/2007 - Se convierten los estilos en jsf y se hace un include en el jsp
//*************************************************************************************************
-->
<%@page language="java" session="true" import="java.util.Hashtable"%>
<%
response.setHeader("Cache-Control","no-store"); //HTTP 1.1
response.setHeader("Pragma","no-cache"); //HTTP 1.0
response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
%>
<%!

  private Hashtable getInfo(String sucursal)
  {
    String statementPS = com.bital.util.ConnectionPoolingManager.getStatementPostFix();
    java.sql.Connection pooledConnection = com.bital.util.ConnectionPoolingManager.getPooledConnection( );
    java.sql.Statement stmt = null;
    java.sql.ResultSet rs = null;
    java.sql.ResultSetMetaData rsmd = null;

    Hashtable result = new Hashtable();
    try {
      if(pooledConnection != null) {
        stmt = pooledConnection.createStatement();
        StringBuffer cadsql = new StringBuffer(256);
        cadsql.append("SELECT C_SUCURSAL, C_REGISTRO, C_NOMBRE, C_ORIGEN, C_USUARIO, C_PROMOTOR, C_PERIODO FROM ")
          .append("TC_USUARIOS_APOYO WHERE C_SUCURSAL='")
          .append(sucursal).append("'")
          .append(statementPS);

        rs = stmt.executeQuery(cadsql.toString());
        rsmd = rs.getMetaData();
        String campo=null, name=null;
        while(rs.next()) {
          for(int i = 1; i<=rsmd.getColumnCount(); i++) {
            campo = rs.getString(i);
            name = rsmd.getColumnName(i);
            result.put(name, campo);
          }
          result.put("C_PERIODO", rs.getDate(7));
        }
      }
    }
    catch(java.sql.SQLException sqlException) {
      //System.out.println("Error PageBuilderM022::ExecuteSql [" + sqlException + "]");
    }
    finally {
      try {
        if(rsmd != null){
          rsmd = null;
        }
        if(rs != null) {
          rs.close();
          rs = null;
        }
        if(stmt != null) {
          stmt.close();
          stmt = null;
        }
        if(pooledConnection != null) {
          pooledConnection.close();
          pooledConnection = null;
        }
      }
      catch(java.sql.SQLException sqlException) {
        //System.out.println("Error FINALLY [" + sqlException + "]");
      }
    }
    return result;
  }

  private String getDateFormat(java.sql.Date today)
  {
    java.text.SimpleDateFormat formatter;
    String output;
    
    formatter = new java.text.SimpleDateFormat("MMMMMMMMMM dd, yyyy", new java.util.Locale("es", "MX"));
    output = formatter.format(today);
    
    return output.toUpperCase();
  }
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
  <title>Datos de la Transaccion</title>
  <%@include file="style.jsf"%>
<script language="JavaScript" type="text/javascript">
<!--
//-->
</script>
</head>
<body bgcolor="white" onload="top.setFieldFocus(window.document)">
<p><br>
<b>Baja de Usuario de Apoyo</b>
<p><br>
<%
  String sucursal = (String)session.getAttribute("branch");
  Hashtable info = getInfo(sucursal.substring(1));

  if( info.size() == 0 )
    out.println("No existen Usuarios de Apoyo registrados para esta Sucursal");
  else
  {%>
<form action="../../servlet/ventanilla.Group03GTE" method="post">
<table border="0" cellspacing="0" width="400">
<tr>
  <td><b>Nombre:</b></td>
  <td><%= info.get("C_NOMBRE") %></td>
</tr>
<tr>
  <td><b>Promotor asignado:</b></td>
  <td><%= info.get("C_PROMOTOR") %></td>
</tr>
<tr>
  <td><b>Sucursal de Origen:</b></td>
  <td><%= info.get("C_ORIGEN") %></td>
</tr>
<tr>
  <td><b>Sucursal Actual:</b></td>
  <td><%= info.get("C_SUCURSAL") %></td>
</tr>
<tr>
  <td><b>Registro:</b></td>
  <td><%= info.get("C_REGISTRO") %></td>
</tr>
<tr>
  <td><b>Usuario RACF:</b></td>
  <td><%= info.get("C_USUARIO") %></td>
</tr>
<tr>
  <td><b>V�lido hasta:</b></td>
  <td><%= getDateFormat((java.sql.Date)info.get("C_PERIODO")) %></td>
</tr>
<tr>
  <td colspan="2">&nbsp;</td>
</tr>
<tr>
  <td><input type="image" name="[Cancelar]" src="../imagenes/b_cancelar.gif" border="0"></td>
  <td align="right"><input type="image" name="[Eliminar]" src="../imagenes/b_eliminar.gif" border="0"></td>
</tr>
</table>
<input type="hidden" name="eliminar" value="yes">
<input type="hidden" name="sucursal" value="<%=info.get("C_SUCURSAL")%>">
<input type="hidden" name="registro" value="<%=info.get("C_REGISTRO")%>">
</form>
<%}%>
</body>
</html>
