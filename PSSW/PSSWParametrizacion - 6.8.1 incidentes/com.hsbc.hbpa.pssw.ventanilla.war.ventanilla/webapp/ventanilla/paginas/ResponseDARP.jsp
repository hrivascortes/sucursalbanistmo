<!--
//*************************************************************************************************
///            Funcion: JSP que despliegarespuesta de txns
//            Elemento: Response.jsp
//          Creado por: Carolina Velas
//*************************************************************************************************
// CCN - 4620008 - 14/09/2007 - Se agrega para txn DARP
//*************************************************************************************************
-->

<%@ page session="true" import="java.util.*"%>
<%

response.setHeader("Cache-Control","no-store"); //HTTP 1.1
response.setHeader("Pragma","no-cache"); //HTTP 1.0
response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
String valorCaja = request.getParameter("txtCRPCuenta");

%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<%
   ventanilla.com.bital.beans.DatosVarios datosv = ventanilla.com.bital.beans.DatosVarios.getInstance();
   Vector select = (Vector)datosv.getDatosV("CARP"); 
   Vector lstCuenta = new Vector();
   Vector lstNumServicio = new Vector();
   Vector lstNomServicio = new Vector();
   String cuenta = "";
   String numero="";
   String nombre="";
   StringTokenizer dato =null;
     
   for(int i =0; i<select.size(); i++){
	   String datosVector = (String)select.get(i);
	  
	    dato = new StringTokenizer(datosVector,"*");
	  	
	  	while (dato.hasMoreElements())
	  	{  	
			cuenta = dato.nextToken();
			lstCuenta.add(cuenta);	
			numero = dato.nextToken();
			lstNumServicio.add(numero);
			nombre= dato.nextToken();
			lstNomServicio.add(nombre);
					  	
	  	} 
  	}  
%>
<html>
<head>
  <title>Servicios Asociados a las Cuentas</title>
	<%@include file="style.jsf"%>
</head>
<body bgcolor="#FFFFFF">
 <H1>Modulo de Consulta ARP</H1>
	   <table align="left">
	   
	<%
		int flag = 0;
		for(int a=0; a<lstCuenta.size(); a++){
			
			String validacion = (String)lstCuenta.get(a);
			if(validacion.equals(valorCaja)){
			    out.println("<tr align='left'>");
				out.println("<td><b>Clave de ARP :</td>");
			    out.println("<td align='left'>"+lstNumServicio.get(a));			    
			    out.println("</tr>");			    
				out.println("<tr align='left'>");
				out.println("<td><b>Nombre de la Empresa :</td>");
			    out.println("<td align='left' >"+lstNomServicio.get(a));
				out.println("</tr>");
				out.println("<tr align='left'>");
				out.println("<td>&nbsp;</td>");
				out.println("</tr>");
				
				flag=1;	
			}					
		}
		if(flag == 0){
			out.println("<tr align='center'>");
			out.println("<td  align='left'>Cuenta sin servicios asociados</td>");
			out.println("</tr>");
		}
	%>
	</table>
</body>
</html>
