<!--
//*************************************************************************************************
//             Funcion: JSP que despliega los campos de la txn 0030
//            Elemento: ResponseTDI.jsp
//          Creado por: Rutilo Zarco Reyes
//      Modificado por: Rutilo Zarco Reyes
//*************************************************************************************************
// CCN - 4360446 - 10/03/2006 - Proyecto TDI fase 2 cobro de comision
// CCN - 4360452 - 27/03/2006 - Correccin para impresion de comprobante TDI
// CCN - 4360581 - 12/03/2007 - Se convierten los estilos en jsf y se hace un include en el jsp
//*************************************************************************************************
-->
<%@ page session="true" import="java.util.*"%>
<%
response.setHeader("Cache-Control","no-store"); //HTTP 1.1
response.setHeader("Pragma","no-cache"); //HTTP 1.0
response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
%>
<%!
private String addpunto(String valor)
{
    int longini = valor.length();
    String cents  = valor.substring(longini-2,longini);
    String entero = valor.substring(0,longini-2);
    int longente = entero.length();
    for (int i = 0; i < (longente-(1+i))/3; i++)
    {
        longente = entero.length();
        entero = entero.substring(0,longente-(4*i+3))+','+entero.substring(longente-(4*i+3));
    }
    entero = entero + '.' + cents;
    return entero;
}
private String delCommaPointFromString(String newCadNum) {
    String nCad = new String(newCadNum);
    if( nCad.indexOf(".") > -1) {
        nCad = nCad.substring(nCad.indexOf(".") + 1, nCad.length());
        if(nCad.length() != 2) {
            String szTemp = new String("");
            for(int j = nCad.length(); j < 2; j++)
                szTemp = szTemp + "0";
            newCadNum = newCadNum + szTemp;
        }
    }
    StringBuffer nCadNum = new StringBuffer(newCadNum);
    for(int i = 0; i < nCadNum.length(); i++)
        if(nCadNum.charAt(i) == ',' || nCadNum.charAt(i) == '.')
            nCadNum.deleteCharAt(i);
    return nCadNum.toString();
}
  private String quitaespacios(String s)
  {
      String s1 = "";
      s1 = s;
      s = "";
      int len = s1.length();
      for ( int i = 0; i < len; i++ )
	  {
        if ( s1.charAt(i) != ' ')
		{
           s = s + s1.charAt(i);
        }
        else
           s = s + "%20";
      }
      return s;
   }

  private String getString(String s)
  {
   int len = s.length();
   for(; len>0; len--)
    if(s.charAt(len-1) != ' ')
     break;
   return s.substring(0, len);
  }

  private String getItem(String newCadNum, int newOption)
  {
   int iPIndex = newCadNum.indexOf("CONSECUTIVO:") + 12, NumSaltos, Num = 0, Ban = 0;
   String nCadNum = new String("");
   NumSaltos = newOption;
   while(Num < NumSaltos)
   {
    if(newCadNum.charAt(iPIndex) == 0x20)
    {
     while(newCadNum.charAt(iPIndex) == 0x20)
      iPIndex++;
     Num++;
    }
    else
     while(newCadNum.charAt(iPIndex) != 0x20)
      iPIndex++;
   }
   while(newCadNum.charAt(iPIndex) != 0x20 && iPIndex < newCadNum.length()){
    nCadNum = nCadNum + newCadNum.charAt(iPIndex);
    if(iPIndex < newCadNum.length())
      iPIndex++;
    if(iPIndex == newCadNum.length())
      break;
   }

   return nCadNum;
  }
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<%
	Hashtable datasession = (Hashtable)session.getAttribute("page.datasession");
    int NumTxnFre = Integer.parseInt((String)datasession.get("NumTxnFre"));
    Vector txntdi = (Vector)datasession.get("Txn.Tdi");
    Vector datatdi = (Vector)datasession.get("DataxTxn.Tdi");
    String comprobante = (String)datasession.get("lstComprobante");    
    if (comprobante == null)
       comprobante = "no";
    String loch = "";
    if (comprobante.equals("si"))
      loch = "~";
    Hashtable branchinfo = (Hashtable)session.getAttribute("branchinfo");
    String colonia = (String)branchinfo.get("D_COLONIA");
    String domicilioSuc = (String)branchinfo.get("D_CALLE");
    domicilioSuc = domicilioSuc.trim() + "   " + colonia.trim();
    for (int i=0; i<NumTxnFre; i++) //Transacciones Frecuentes
    {
      String accion = (String)datasession.get("accion" + (i+1));
      if (accion != null) //Transacciones seleccionadas
      {
        Vector fields = (Vector)txntdi.get(i);
        Hashtable info = (Hashtable)datatdi.get(i);
      	String txn = (String)fields.get(3);
      	String total = (String)datasession.get("total" + (i+1));
        String comision = (String)datasession.get("comision" + (i+1));
        String iva = (String)datasession.get("iva" + (i+1));
      	if (txn.equals("5503"))
      	{
          String ben = (String)datasession.get("beneficiario" + i);
          if (ben == null)
            ben = "";
         loch = loch + "LOCH~" + quitaespacios(ben) + "~~~~~";
         String compago = "N";
          if (comprobante.equals("si"))
           compago = "S";
         loch = loch + compago + "~~~~";
         if (comprobante.equals("si"))
            loch = loch + (String)datasession.get("txtRfc") + "~" + (String)datasession.get("txtNombre") + "~" + (String)datasession.get("txtDomici") + "~";
      	}
      	if (txn.equals("0728") || txn.equals("0730"))
      	{
        long com = Long.parseLong(delCommaPointFromString(comision));
        long liva = Long.parseLong(delCommaPointFromString(iva));
        long totic = com + liva;
        String totalci = addpunto(Long.toString(totic));

          if (comprobante.equals("si"))
            loch = loch + "LCFISCAL~" + (String)datasession.get("txtNombre") + "~" + (String)datasession.get("txtRfc") + "~" + "RECAUDACIONES" + "~" + "PAGO DE SERVICIOS" + "~" + (String)datasession.get("txtDomici") + "~" + comision + "~" + iva + "~" + totalci + "~" + domicilioSuc + "~" + "01" + "~";
      	}
      }
    }
//System.out.println("txtCadImpresion " + loch);
%>
<HTML>
<HEAD>
<META http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<META name="GENERATOR" content="IBM WebSphere Studio">
<META http-equiv="Content-Style-Type" content="text/css">
<TITLE>ResponseTDI.jsp</TITLE>
<%@include file="style.jsf"%>
</HEAD>
<BODY>
<%Vector resp = (Vector)session.getAttribute("response");
  String codigo = "";
  if( resp.size() != 4 )
   out.println("<b>Error de conexi&oacute;n</b><br>");
  else
  {
   	try
   	{
     	codigo = (String)resp.elementAt(0);
 	if( codigo.equals("1"))
   	   out.println("<b>Transaccion Rechazada</b><br><br>");
        if( codigo.equals("0") || codigo.equals("1") ) {
     	   int lines = Integer.parseInt((String)resp.elementAt(1));
    	   String szResp = (String)resp.elementAt(3);
    	   for(int i=0; i<lines; i++) {
    		   String line = szResp.substring( i*78, Math.min(i*78+77, szResp.length()) );
     		   out.println("<b>" + getString(line) + "<p>");
     		   if(Math.min(i*78+77, szResp.length()) == szResp.length())
      		    break;
    	   }
       }
       else {
         if ( codigo.equals("3") || codigo.equals("2"))
            out.println("<b>Transacci&oacute;n Requiere Autorizaci&oacute;n</b>");
         else
            out.println("<b>Ocurri&oacute; un Error de Comunicaci&oacute;n, Verifique.</b>");
       }
  	}
  	catch(NumberFormatException nfe){
     out.println("<b>Error de conexi&oacute;n</b><br>");
    }
  }
%>
<form name="entry">
 <INPUT type="hidden" name="dummy" value="0">
</form>
 <script>
    top.openDialog('../../servlet/ventanilla.ImprimirServlet?txtCadImpresion=<%=loch%>', 160, 120, 'top.setPrefs4()', document.Txns);
</script>
</BODY>
</HTML>
