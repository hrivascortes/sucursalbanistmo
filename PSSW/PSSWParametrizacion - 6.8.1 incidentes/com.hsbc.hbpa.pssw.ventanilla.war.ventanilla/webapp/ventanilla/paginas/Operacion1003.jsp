<!--
//*************************************************************************************************
//            Elemento: Operacion1003.jsp
//             Funcion: JSP para visualización de datos txn 1003
//          Creado por: Juan carlos Gaona Marcial
//*************************************************************************************************
// CCN - 4360508 - 08/09/2006 - Visualización de los datos para la txn 1003(nombre,cuenta y monto)
// CCN - 4360581 - 12/03/2007 - Se convierten los estilos en jsf y se hace un include en el jsp
//*************************************************************************************************
-->
<%@ page session="true" import="ventanilla.com.bital.util.*,java.util.*"%>
<%
response.setHeader("Cache-Control","no-store"); //HTTP 1.1
response.setHeader("Pragma","no-cache"); //HTTP 1.0
response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
 <head>
<%@include file="style.jsf"%>
  <title>CONFIRMACION DE DATOS</title>
 </head>
 <body>
<font style="font-family:arial,helvetica;font-size:13px;" color="red"><B><br><br>Confirma con el Depositante que el deposito realizado corresponde a este cliente</b></font>
<br><br>
  <%!
	public Vector Campos(String cadena, String delimitador) {
		NSTokenizer parser = new NSTokenizer(cadena, delimitador);
		Vector campos = new Vector();
		while (parser.hasMoreTokens()) {
			String token = parser.nextToken();
			if (token == null)
				continue;
			campos.addElement(token);
		}
		return campos;
	}
	
	private String formatMonto(String s)
	{
		int len = s.length();
		if (len < 3)
			s = "0." + s;
		else
		{
			int n = (len - 3 ) / 3;
			s = s.substring(0, len - 2) + "." + s.substring(len - 2 , len);
			for (int i = 0; i < n; i++)
			{
				len = s.length();
				s = s.substring(0, (len -(i*3+6+i))) + "," + s.substring((len -(i*3+6+i)) , len);
			}
		}
		return s;
	}
  
  %>
  <%
    out.print("<table style='font-family:arial,helvetica;font-size:13px;'>");
	String strTmp = "";
	Vector vCamposT = new Vector();;
	Vector vParam = new Vector();
	String strRespuesta = session.getAttribute("strRespuesta").toString();
    session.removeAttribute("strRespuesta");	
	Vector vCampos = Campos(strRespuesta, "^");   
	for (int i = 0; i < vCampos.size(); i++) {
		strTmp = (String) vCampos.elementAt(i);
		if (strTmp.startsWith("CERTIFICACION")) {
			strTmp = (String) vCampos.elementAt(i);
			vCamposT = Campos(strTmp, "@");
			strTmp = (String) vCamposT.elementAt(1);
			vCamposT = Campos(strTmp, "|");
			strTmp = (String) vCamposT.elementAt(0);
			vParam = Campos(strTmp, "~");
		}
	}
	String strCuenta = (String) vParam.elementAt(1);
	String strDivisa = (String) vParam.elementAt(2);
	if ((strDivisa.equals("N$")) || (strDivisa.equals("N$ ")))
		strDivisa = "$";
	else
		strDivisa = "US$";
	String strMonto = (String) vParam.elementAt(11);
	strTmp = (String) vParam.elementAt(12);
	int nlineas = strTmp.length() / 78;
	if (strTmp.length() % 78 > 0)
		nlineas++;				
	String strTmpL = "";						
	for (int j = 0; j < nlineas; j++) {
		if (((j * 78) + 78) < strTmp.length())
			strTmpL = strTmp.substring((j * 78), (j * 78) + 78);
		else
			strTmpL = strTmp.substring(j * 78, strTmp.length());
		if (j == 1 )
			j = nlineas + 2;
	}					
    out.print("<tr>");
    out.print(" <td>"); 
	out.println(" Nombre : " + strTmpL );
    out.print(" </td>"); 
    out.print("<tr>");	    
    out.print(" <td>"); 
	out.println(" Cuenta : " + strCuenta );
    out.print(" </td>");
    out.print("<tr>");     
    out.print(" <td>"); 
	out.println(" Deposito Total : " + strDivisa + formatMonto(strMonto));
    out.print(" </td>"); 
    out.print("</tr>");    
    out.print("</table>");	
 %>
 </body>
</html>
