<!--
//*************************************************************************************************
//             Funcion: JSP que presenta las opciones de la txns 0024
//            Elemento: PageBuilder0024.jsp
//          Creado por: Alejandro Gonzalez Castro
//      Modificado por: Marco Lara Palacios
//*************************************************************************************************
// CCN - 4360275 - 18/02/2005 - WAS 5.1
// CCN - 4360581 - 12/03/2007 - Se convierten los estilos en jsf y se hace un include en el jsp
//*************************************************************************************************
-->

<%@ page session="true" import="java.util.*"%>
<%
response.setHeader("Cache-Control","no-store"); //HTTP 1.1
response.setHeader("Pragma","no-cache"); //HTTP 1.0
response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<%
String MontoPro = new String("");
Vector listaCampos = (Vector)session.getAttribute("page.listcampos");
Vector listaContenidos = (Vector)session.getAttribute("page.listcont");
String txnAuthLevel = (String)session.getAttribute("page.txnAuthLevel");
String txnIMGAutoriz = (String)session.getAttribute("page.txnIMGAutoriz");
Vector v = (Vector)session.getAttribute("page.specialacct");
int k;
%>
<html>
<head>
<%@include file="style.jsf"%>
<title>Datos de la Transaccion</title>
<script language="JavaScript">
<!--
var formName = 'Mio';
var specialacct = new Array();

<%
if(!v.isEmpty())
{
  for(int iv = 0; iv < v.size(); iv++)
  {
    out.print("specialacct[" + iv + "]='" + v.get(iv) + "'" + '\n');
  }
}
%>


function validate(form)
{
<%
      out.println("if( !top.verifyOPM(window))");
      out.println("    return; ");
      out.println("if( !top.cadenasOPM(window))");
      out.println("    return; ");
%>

  form.validateFLD.value = '1';
<%
if(!txnAuthLevel.equals("1") && !txnIMGAutoriz.equals("1"))
{
    out.println("  form.submit();");
    out.println("var obj = document.getElementById('btnCont');"); 
    out.println("obj.onclick = new Function('return false');");   
}   
%>
}


function AuthoandSignature(entry)
{
  var Cuenta;
  
  if(entry.validateFLD.value != '1')
    return;
    
  if(entry.needAutho.value == '1' && entry.AuthoOK.value != '1')
  {
    top.openDialog('../ventanilla/paginas/authoriz.jsp', 220, 130, 'top.setPrefs()', entry);
  }
  
  if(entry.needSignature.value == '1' && entry.SignField.value != '')
  {
    Cuenta = eval('entry.' + entry.SignField.value + '.value');
    top.openModalDialog('<%=request.getContextPath()%>/servlet/ventanilla.FirmasServlet?txtDDACuenta=' + Cuenta, 710, 430, 'top.setPrefs2()', entry);
  }
}
//-->
</script>

</head>
<body bgcolor="#FFFFFF" onload="top.setFieldFocus(window.document)">
<br>
<%
out.print("<form name=\"entry\" action=\"../servlet/ventanilla.DataServlet\" method=\"post\" ");

if(txnAuthLevel.equals("1"))
  out.print("onSubmit=\"return validate(this)\"");
  
out.println(">");
%>
<h1><%=session.getAttribute("page.txnImage")%></h1>
<h3>
<%
out.print(session.getAttribute("page.cTxn") + "  " + (String)session.getAttribute("page.txnlabel"));
%>
</h3>

<table border="0">
<tr><td rowspan="2" valign="top"><b>Cargo :</b></td>
<%
  Vector vCampos = (Vector)listaCampos.get(0);
  vCampos = (Vector)vCampos.get(0);
  out.println("  <td colspan=\"2\">" + vCampos.get(2) + ":</td>");
  out.println("  <td>&nbsp;</td>");
  out.print  ("  <td colspan=\"5\"><input maxlength=\"" + vCampos.get(3) + "\" name=\"" + vCampos.get(0) + "\" " + 
              " size=\"" + vCampos.get(3) +"\" type=\"text\" tabindex=\"1\" " +
              " onBlur=\"top.estaVacio(this)||top.validate(window, this, '" + vCampos.get(4) +"')\" " + 
              " onKeyPress=\"top.keyPressedHandler(window, this, '" + vCampos.get(4) + "')\"  ></td>");
%></tr>
<tr>
<%
  vCampos = (Vector)listaCampos.get(1);
  vCampos = (Vector)vCampos.get(0);
  out.println("  <td>" + vCampos.get(2) + ":</td>");
  out.println("  <td width=\"10\">&nbsp;</td>");
  out.print  ("  <td colspan=\"2\"><input maxlength=\"" + vCampos.get(3) + "\" name=\"" + vCampos.get(0) + "\" " + 
              " size=\"" + vCampos.get(3) +"\" type=\"text\" tabindex=\"2\" " +
              " onBlur=\"top.estaVacio(this)||top.validate(window, this, '" + vCampos.get(4) +"')\" " + 
              " onKeyPress=\"top.keyPressedHandler(window, this, '" + vCampos.get(4) + "')\"  ></td>");
  vCampos = (Vector)listaCampos.get(2);
  vCampos = (Vector)vCampos.get(0);
  out.println("  <td width=\"10\">&nbsp;</td>");
  out.println("  <td>" + vCampos.get(2) + ":</td>");
  out.println("  <td width=\"10\">&nbsp;</td>");
  out.print  ("  <td><input maxlength=\"" + vCampos.get(3) + "\" name=\"" + vCampos.get(0) + "\" " + 
   	      " size=\"14\" type=\"text\" tabindex=\"3\" " +
              " onBlur=\"top.estaVacio(this)||top.validate(window, this, '" + vCampos.get(4) +"')\"  " + 
              " onKeyPress=\"top.keyPressedHandler(window, this, '" + vCampos.get(4) + "')\" ></td>");
%></tr>
</table>
<br>
<table border="0">
<tr>
<td><b>Abonos:</b></td><td colspan="5">&nbsp;</td>
</tr>

<tr>
<td align="center">Nombre :</td><td>&nbsp;</td>
<td align="center">Apellido :</td><td>&nbsp;</td>
<td align="center">Monto :</td><td>&nbsp;</td>
<td rowspan="2"><a tabindex="7" href="JavaScript:top.addNewOptionOPM(window)"><img src="../ventanilla/imagenes/b_agregar.gif" border="0" alt="Agregar Abono"></a><p></td>
</tr>

<tr>
<%
  vCampos = (Vector)listaCampos.get(3);
  vCampos = (Vector)vCampos.get(0);
  out.print  ("  <td align=\"center\"><input maxlength=\"" + vCampos.get(3) + "\" name=\"" + vCampos.get(0) + "\" " + 
              " size=\"" + vCampos.get(3) + "\" type=\"text\" tabindex=\"4\" " +
              " onBlur=\"top.estaVacio(this)||top.validate(window, this, '" + vCampos.get(4) +"')\" " + 
              " onKeyPress=\"top.keyPressedHandler(window, this, '" + vCampos.get(4) + "')\" ></td>");
  vCampos = (Vector)listaCampos.get(4);
  vCampos = (Vector)vCampos.get(0);
  out.println("  <td width=\"10\">&nbsp;</td>");
  out.print  ("  <td align=\"center\"><input maxlength=\"" + vCampos.get(3) + "\" name=\"" + vCampos.get(0) + "\" " + 
              " size=\"20\" type=\"text\" tabindex=\"5\" " +
              " onBlur=\"top.estaVacio(this)||top.validate(window, this, '" + vCampos.get(4) +"')\" " + 
              " onKeyPress=\"top.keyPressedHandler(window, this, '" + vCampos.get(4) + "')\" ></td>");
  vCampos = (Vector)listaCampos.get(5);
  vCampos = (Vector)vCampos.get(0);
  out.println("  <td width=\"10\">&nbsp;</td>");
  out.print  ("  <td align=\"center\"><input maxlength=\"" + vCampos.get(3) + "\" name=\"" + vCampos.get(0) + "\" " + 
             " size=\"14\" type=\"text\" tabindex=\"6\" " +
             " onBlur=\"top.estaVacio(this)||top.validate(window, this, '" + vCampos.get(4) +"')\"  " + 
             " onKeyPress=\"top.keyPressedHandler(window, this, '" + vCampos.get(4) + "')\" ></td>");
%>
<td>&nbsp;</td>
</tr>

<tr>
<td><select name="nombres" size="10" tabindex="8" STYLE="width: 150px;" onChange="JavaScript:top.equalnomOPM(window)"><option value=0>- Nombres Abonos -</select></td>
<td width="10">&nbsp</td>
<td><select name="apellidos" size="10" tabindex="9" STYLE="width: 195px;" onChange="JavaScript:top.equalapeOPM(window)"><option value=0>- A p e l l i d o s - A b o n o s -</select></td>
<td width="10">&nbsp</td>
<td><select name="montos" size="10" tabindex="10" STYLE="width: 135px;" onChange="JavaScript:top.equalmtoOPM(window)"><option value=0>- Montos Abonos -</select></td>
<td width="10">&nbsp</td>
<td align="center">
<a tabindex="11" href="JavaScript:top.modifyOptionOPM(window)"><img src="../ventanilla/imagenes/b_modificar.gif" border="0" alt="Modificar Abono"></a><p><p>
<a tabindex="12" href="JavaScript:top.removeOptionOPM(window)"><img src="../ventanilla/imagenes/b_eliminar.gif" border="0" alt="Eliminar Abono"></a>
</td>
</tr>

<tr>
<td colspan="3" align="center"><b>No. de Abonos:</b></td><td>&nbsp;</td>
<td align="center"><b>Monto Abonos:</b></td><td>&nbsp;</td>
</tr>

<tr>
<td colspan="3" align="center">
<input type="text" name="txtTotalA" value="0" size="5" disabled ></td>
<td>&nbsp;</td>

<td align="center">
<input type="text" name="txtMontoT" value="0" size="15" disabled ></td><td>&nbsp;</td>
</tr>
</table>
<input type="hidden" name="ctacargo" value="0">
<input type="hidden" name="mtocargo" value="0">
<input type="hidden" name="nomcargo" value="0">
<input type="hidden" name="noabonos" value="0">
<input type="hidden" name="mtoabonos" value="0">
<input type="hidden" name="CurrPago" value="0">

<input type="hidden" name="Snombres" value="0">
<input type="hidden" name="Sapellidos" value="0">
<input type="hidden" name="Smontos" value="0">

<input type="hidden" name="iTxn" value="<%= session.getAttribute("page.iTxn") %>">
<input type="hidden" name="oTxn" value="<%= session.getAttribute("page.oTxn") %>">
<input type="hidden" name="cTxn" value="<%= session.getAttribute("page.cTxn") %>">
<input type="hidden" name="sucursal" value="<%= session.getAttribute("branch") %>">
<input type="hidden" name="teller" value="<%= session.getAttribute("teller") %>">
<input type="hidden" name="moneda" value="<%= session.getAttribute("page.moneda")%>">
<input type="hidden" name="registro" value="<%=session.getAttribute("empno")%>">
<input type="hidden" name="tllrlvl" value="<%=session.getAttribute("tellerlevel")%>">
<input type="hidden" name="supervisor" value="<%= session.getAttribute("teller") %>">
<input type="hidden" name="compania" value="20">
<input type="hidden" name="AuthoOK" value="0">
<input type="hidden" name="SignOK" value="0">
<input type="hidden" name="validateFLD" value="1">
<input type="hidden" name="needSignature" value="<%out.print(txnIMGAutoriz);%>">
<input type="hidden" name="needAutho" value="<%out.print(txnAuthLevel);%>">
<input type="hidden" name="SignField" value="<%= session.getAttribute("page.SignField") %>">
<p>
<%
if(txnAuthLevel.equals("1") || txnIMGAutoriz.equals("1"))
  out.println("<a id='btnCont' tabindex=\"13\" href=\"javascript:validate(document.entry);javascript:AuthoandSignature(document.entry)\"><img src=\"../ventanilla/imagenes/b_aceptar.gif\" border=\"0\"></a>");
else
  out.println("<a id='btnCont' tabindex=\"13\" href=\"javascript:validate(document.entry)\"><img src=\"../ventanilla/imagenes/b_aceptar.gif\" border=\"0\"></a>");
%>
</form>
</body>
</html>
