<%
//*************************************************************************************************
///            Funcion: JSP que despliega respuesta de txn RAP
//            Elemento: Response0832.jsp
//          Creado por: Alejandro Gonzalez Castro
//      Modificado por: Fausto Rodrigo Flores Moreno
//*************************************************************************************************
// CCN - 4360274 - 16/02/2005 - Error de impresi�n cuado el domicilio de la sucursal tiene car�cter de #
// CCN - 4360455 - 07/04/2005 - Se realizan modificaciones para el esquema de cheques devueltos.
// CCN - 4360462 - 10/04/2005 - Se genera nuevo paquete por errores en paquete anterior
// CCN - 4360492 - 23/06/2006 - Se realizan modificaciones para el esquema de Cobro Comisi�n Usuarios RAP
// CCN - 4360574 - 09/03/2007 - Se realizan modificaciones para el esquema de RAP Calculadora
//*************************************************************************************************
%>

<%@ page session="true" import="java.util.Vector,java.util.Hashtable,java.util.Stack"%>
<%
response.setHeader("Cache-Control","no-store"); //HTTP 1.1
response.setHeader("Pragma","no-cache"); //HTTP 1.0
response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<%!
  private String getString(String s)
  {
   int len = s.length();
   for(; len>0; len--)
    if(s.charAt(len-1) != ' ')
     break;
   return s.substring(0, len);
  }

  private String getItem(String newCadNum, int newOption)
  {
   int iPIndex = newCadNum.indexOf("CONSECUTIVO:") + 12, NumSaltos, Num = 0, Ban = 0;
   String nCadNum = new String("");
   NumSaltos = newOption;
   while(Num < NumSaltos)
   {
    if(newCadNum.charAt(iPIndex) == 0x20)
    {
     while(newCadNum.charAt(iPIndex) == 0x20)
      iPIndex++;
     Num++;
    }
    else
     while(newCadNum.charAt(iPIndex) != 0x20)
      iPIndex++;
   }
   while(newCadNum.charAt(iPIndex) != 0x20 && iPIndex < newCadNum.length()){
    nCadNum = nCadNum + newCadNum.charAt(iPIndex);
    if(iPIndex < newCadNum.length())
      iPIndex++;
    if(iPIndex == newCadNum.length())
      break;
   }

   return nCadNum;
  }
%>
<%
Vector resp = (Vector)session.getAttribute("response");
%>

<html>
<head>
  <title>Certificaci�n</title>
<style type="text/css">Body {font-family: Arial, Helv; font-size: 10pt; font-weight: bold; color:#000000}</style>
</head>
<body bgcolor="#FFFFFF"  onload="top.setFieldFocus(window.document)" >
<p>
<%
String szResp = "";
String codigo = "";
String pcdOK = "";
String remesas = "";
String chqDev="";
int iServ = 0; //RAP CAL
int size = 0;
int pini = 0;
int ns = 0;
int i = 0;
int x = 0;
int z = 0;
int RapCal = 0;
Vector pcdcode = new Vector();
Vector code1 =  new Vector();
Vector code2 =  new Vector();
Hashtable info = new Hashtable();
Hashtable datasession = (Hashtable)session.getAttribute("page.datasession");

Stack flujotxn = new java.util.Stack();
if(session.getAttribute("page.flujotxn") != null)
	flujotxn = (Stack)session.getAttribute("page.flujotxn");
if( resp.size() != 4 )
	out.println("<b>Error de conexi&oacute;n</b><br>");
else
{
	try
	{
        codigo = (String)resp.elementAt(0);
        if( !codigo.equals("0") )
        out.println("<b>Transaccion Rechazada</b><br><br>");
        int lines = Integer.parseInt((String)resp.elementAt(1));
        szResp = (String)resp.elementAt(3);
        for(i=0; i<lines; i++)
        {
            String line = szResp.substring( i*78, Math.min(i*78+77, szResp.length()) );
            out.println(getString(line) + "<p>");
            if(Math.min(i*78+77, szResp.length()) == szResp.length())
                break;
        }
  	}
  	catch(NumberFormatException nfe)
	{out.println("<b>Error de conexi&oacute;n</b><br>");}

	if(codigo.equals("0"))
	{
		// NUMERO DE SERVICIOS CAPTURADOS
		String nos = (String)datasession.get("nservicios");
		ns = Integer.parseInt(nos);
		String noP = (String)datasession.get("nopagos1");
		int nPagos = Integer.parseInt(noP);
		String cuentaRAP =null;
        //IDDEVA Se obtiene el valor de CuentaRAP
        if( szResp.length() >= 639){
		    cuentaRAP = szResp.substring(629,639);
	     	datasession.put("CuentaRAP",cuentaRAP);          
	    }
	    
	    if(ns == 1 && nPagos == 1)
	    {	   
	     if(szResp.length() > 567 ) 
	     { 
	         if(szResp.substring(556,557).equals("1"))		     
		     {
				 
		         chqDev="SI";
	             datasession.put("chqDev",chqDev);
	        	 cuentaRAP = szResp.substring(629,639);//szResp.substring(557,567);
	        	 datasession.put("CuentaRAP",cuentaRAP);          
	             if(session.getAttribute("ChequesDevueltos") != null)
	                 session.removeAttribute("ChequesDevueltos"); 
	         }else
		     {
		         chqDev="NO";
	             datasession.put("chqDev",chqDev);
		     }
	     }else
	     {
	         chqDev="NO";
             datasession.put("chqDev",chqDev);    
             }
	     }
	     

	        
	

		// OBTENCION DE CODIGOS DE PCD - 4 SERVICIOS
	    pcdcode.addElement(szResp.substring(156,157));
	    pcdcode.addElement(szResp.substring(169,170));
	    pcdcode.addElement(szResp.substring(182,183));
	    pcdcode.addElement(szResp.substring(234,235));

		// SERVICIO NO EXISTE - CODIGO 3
		for(x=0; x<ns; x++)
		{
			if ((pcdcode.elementAt(x).toString()).equals("3"))
			{
				pcdOK = "3";
				break;
			}
		}

		if(!pcdOK.equals("3"))
		{
			// SERVICIO EN AMBAS PCD'S - CODIGO 4
			for(x=0; x<ns; x++)
			{
				if ((pcdcode.elementAt(x).toString()).equals("4"))
				{
					pcdOK = "4";
					break;
				}
			}
		}

		if(!pcdOK.equals("3") && !pcdOK.equals("4"))
		{
			// SERVICIOS 1 Y 2
			for(x=0; x<ns; x++)
			{
				if((pcdcode.elementAt(x).toString()).equals("1"))
					code1.addElement(Integer.toString(x));
				else
					code2.addElement(Integer.toString(x));
			}
			// SERVICIOS 1 Y 2
			if(code2.size() > 0 && code1.size() > 0)
				pcdOK = "7";
			// SERVICIOS 1
			if (code1.size() == ns)
			{
				pcdOK = "1";
                    info.put("PCDCode", pcdOK);
			}
			// SERVICIOS 2
			if (code2.size() == ns)
			{
				pcdOK = "2";
                    info.put("PCDCode", pcdOK);
			}
		}

		if(pcdOK.equals("2"))
		{
			//SERVICIOS 2 - PCD 54820
			for(x=0; x<ns; x++)
			{
				// OBTENCION DE ATRIBUTOS DEL SERVICIO
				// LONGITUDES DE REFERENCIA 1 - 2 - 3
				if(x==0){pini = 156;} // servicio 1
				if(x==1){pini = 169;} // servicio 2
				if(x==2){pini = 182;} // servicio 3
				if(x==3){pini = 234;} // servicio 4
                    info.put("Long1s"+(x+1),szResp.substring(pini+1,pini+3));
                    info.put("Long2s"+(x+1),szResp.substring(pini+3,pini+5));
                    info.put("Long3s"+(x+1),szResp.substring(pini+5,pini+7));

				// OBTENCION DE FORMAS DE PAGO
				String dato = "";
				int NNN = 0;
				for (z=7; z<10; z++)
				{
					dato = szResp.substring(pini+z,pini+(z+1));
					if (dato.equals("S") || dato.equals("N"))
					{
                              info.put("FP"+(z-6)+"s"+(x+1), dato);
						if(dato.equals("N"))
							NNN = NNN + 1;
					}
					else
                         {
                              info.put("FP"+(z-6)+"s"+(x+1), "S");
                         }
				}

				
                    // VALIDACION DE FORMA DE PAGO -NNN-
				if (NNN == 3)
				{
					pcdOK = "8";
					break;
				}

                    // OBTENCION DE COMPROBANTE DE PAGO
                    dato = szResp.substring(pini+10,pini+11);
        			if (!dato.equals("S") && !dato.equals("N"))
                        dato = "N";
                    info.put("CompPago"+(x+1), dato);

                    // OBTENCION DE VALIDACION DE LONGITUDES DE REFERENCIAS
                    dato = szResp.substring(pini+11,pini+12);
        			if (!dato.equals("S") && !dato.equals("N"))
                        dato = "N";
                    info.put("ValRefs"+(x+1), dato);

				// OBTENCION DE COMISION E IVA DE COMISION
				if(x==0){pini = 312;}
				if(x==1){pini = 324;}
				if(x==2){pini = 336;}
				if(x==3){pini = 390;}

                    info.put("Com"+(x+1), szResp.substring(pini,pini+9));
                    info.put("Iva"+(x+1), szResp.substring(pini+9,pini+11));

				// OBTENCION DE COBRO COMISION, COMISION CLIENTE, COMISION NO CLIENTE Y FLAG DE REFERENCIAS
				if(x==0){pini = 546;}
				if(x==1){pini = 557;}
				if(x==2){pini = 568;}
				if(x==3){pini = 579;}
                    info.put("CobCom"+(x+1), szResp.substring(pini,pini+1));
                    info.put("ComCli"+(x+1), szResp.substring(pini+1,pini+5));
					
					if(x==3)
                    	info.put("ComNoCli"+(x+1), szResp.substring(pini+5,pini+7)+szResp.substring(pini+45,pini+47));
					else
                    info.put("ComNoCli"+(x+1), szResp.substring(pini+5,pini+9));

//OBTENCI�N DEL FLAG DE TXN DE COMISION
				if(x==0){pini = 556;}//DEFINIR POSICIONES
				if(x==1){pini = 567;}//       "
				if(x==2){pini = 578;}//       "
				if(x==3){pini = 627;}//       "
                if(szResp.substring(pini,pini+1).equals("2")){
		          info.put("ComServRAP"+(x+1), "1");//txn 0792
		        }else if(szResp.substring(pini,pini+1).equals("3")){
		          info.put("ComServRAP"+(x+1), "0");//txn 0736
		          datasession.put("RAPCalculadora", "S");
		          RapCal = 1;
		          iServ = x;
		        }
		        else if(szResp.substring(pini,pini+1).equals("4")){
  	        	  info.put("ComServRAP"+(x+1), "1");//txn 0792
   		          datasession.put("RAPCalculadora", "S");
		          RapCal = 1;
   		          iServ = x;
		        }else{
		          info.put("ComServRAP"+(x+1), "0");//txn 0736
   		        }
				
//----FIN DE CODIGO
                    if(x == 3)
                        info.put("FlagRef"+(x+1), szResp.substring(pini+9,szResp.length()));
                    else
                        info.put("FlagRef"+(x+1), szResp.substring(pini+9,pini+10));

                    // VALIDACION DE REMESAS UN SERVICIO UN PAGO
                    if( info.get("FP1s"+(x+1)).toString().equals("N") &&
                        info.get("FP2s"+(x+1)).toString().equals("N") &&
                        info.get("FP3s"+(x+1)).toString().equals("S") )
                    /*if( (info.get("FP1s"+(x+1)).toString().equals("N")  || info.get("FP1s"+(x+1)).toString().equals("S") )&&
                        (info.get("FP2s"+(x+1)).toString().equals("N") || info.get("FP2s"+(x+1)).toString().equals("S"))&&
                        info.get("FP3s"+(x+1)).toString().equals("S") )*/
                    {
					String np = (String)datasession.get("nopagos"+(x+1));
					int npagos = Integer.parseInt(np);
					if (npagos != 1)
                         {
						pcdOK = "5";
                              break;
                         }
					else
					{
						if(ns > 1)
                              {
							pcdOK = "5";
                                   break;
                               }    
					}
				}
			}

			// VALIDACION TODOS LOS SERVICIOS LA MISMA FORMA DE PAGO
			if( ns > 1)
               {
                    for(int h=1; h<ns; h++)
                    {
                        if(!(info.get("FP1s"+h).toString().equals(info.get("FP1s"+(h+1)).toString()) &&
                             info.get("FP2s"+h).toString().equals(info.get("FP2s"+(h+1)).toString()) &&
                             info.get("FP3s"+h).toString().equals(info.get("FP3s"+(h+1)).toString())))
                        {
        					pcdOK = "6";
                    		break;
                        }
                    }
               }


			// INSERCION DE FORMAS DE PAGO GENERAL Y MONTO TOTAL
               info.put("FP1", info.get("FP1s1").toString());
               info.put("FP2", info.get("FP2s1").toString());
               info.put("FP3", info.get("FP3s1").toString());
               info.put("txtTot", (String)datasession.get("txtTot"));

			for (int m=1; m<5; m++)
			{
				for (x=1; x<4; x++)
                        info.remove("FP"+x+"s"+m);
			}

			// INSERCION DE NO. DE SERVICIO, NO. DE PAGOS Y MONTO
			String servicio = "";
			String nopagos = "";
			String monto = "";
			String consec ="";
			int intServicio;
			Vector vecRAP = (Vector)session.getAttribute("servicioARP");
			Vector vecReferencia = new Vector();
			for (int n=1; n<ns+1; n++)
			{
				consec = Integer.toString(n);
				servicio = "servicio" + consec;
				for (int np=0; np<vecRAP.size(); np++){
                 String datoTmp = (String)vecRAP.get(np);
                 String [] dato = datoTmp.split("\\*");
              	 intServicio = Integer.parseInt(dato[1]);
		    	 dato[1] = String.valueOf(intServicio);
                 
                 if(dato[1].equals((String)datasession.get(servicio)) && dato.length >= 4){
                 for(int nps = 3; nps < dato.length ; nps++){
	                 vecReferencia.add(dato[nps]);
                 }
                 
                 }
				}
				nopagos = "nopagos" + consec;
				monto = "monto" + consec;
                    info.put("Servicio"+n, (String)datasession.get(servicio));
                    info.put("NoPagos"+n, (String)datasession.get(nopagos));
                    info.put("Monto"+n, (String)datasession.get(monto));
                    info.put("vecReferencia", vecReferencia);
               }

               
		} // pcdOK 2

		if(pcdOK.equals("1"))
		{
			// INSERCION DE MONTO TOTAL
               info.put("txtTot", (String)datasession.get("txtTot"));
			// INSERCION DE NO. DE SERVICIO, NO. DE PAGOS Y MONTO
			String servicio = "";
			String nopagos = "";
			String monto = "";
			String consec ="";
			for (int n=1; n<ns+1; n++)
			{
				consec = Integer.toString(n);
				servicio = "servicio" + consec;
				nopagos = "nopagos" + consec;
				monto = "monto" + consec;
                    info.put("Servicio"+n, (String)datasession.get(servicio));
                    info.put("NoPagos"+n, (String)datasession.get(nopagos));
                    info.put("Monto"+n, (String)datasession.get(monto));
               }
		}
		
		if(RapCal == 1 && ns > 1)
			pcdOK = "9";
		else if (RapCal == 1 && ns == 1 && nPagos>1)
			pcdOK = "10";//consultar con Alex

		session.setAttribute("pcd",pcdOK);
		session.setAttribute("info",info);

	}
	
	if(RapCal == 1){
   	  if(flujotxn == null || flujotxn.isEmpty()) 
            flujotxn.push("0838");
    }else{
    	RapCal = 0;
        datasession.put("RAPCalculadora", "N");
    }
}
%>
<p>
<%
   out.print("<form name=\"Txns\" action=\"../../servlet/ventanilla.PageServlet\" method=\"post\">");
%>
  <input type="hidden" name="transaction" value="<%= session.getAttribute("page.iTxn") %>">
  <input type="hidden" name="transaction1" value="<%= session.getAttribute("page.oTxn") %>">
<%
   out.print("</form>");
%>

</body>
<%
//	out.println("pcdOK code : "+ pcdOK);
	if(pcdOK.equals("1") || pcdOK.equals("2"))
	{
		if(RapCal == 0){
	  		out.println("<b>Para redireccionar a PageBuilderRAP...</b>");
            if(session.getAttribute("page.cTxn").toString().compareTo("C832")==0)
				response.sendRedirect("../paginas/PageBuilderRAPCAPR.jsp");
            else if(session.getAttribute("page.cTxn").toString().compareTo("A832")==0)
            	response.sendRedirect("../paginas/PageBuilderCAPR.jsp");
			else
				response.sendRedirect("../paginas/PageBuilderRAP.jsp");
		}else{%>
  			<script>
  				document.Txns.submit();
		  	</script>
		<%}
	}
	if(pcdOK.equals("3"))
		out.println("<b>El Servicio " + (String)datasession.get("servicio"+(x+1)) + " no existe...</b>");
	if(pcdOK.equals("4"))
		out.println("<b>Problemas con el alta de la clave del Servicio " + (String)datasession.get("servicio"+(x+1)) +", contactar al Ejecutivo de Cuenta...</b>");
	if(pcdOK.equals("5"))
		out.println("<b>El pago de servicios con Remesas solo es permitida para un servicio y un pago...</b>");
	if(pcdOK.equals("6"))
		out.println("<b>La combinaci&oacute;n de formas de pago entre los servicios no es compatible, realizar por separado...</b>");
	if(pcdOK.equals("7"))
	{
		String campo = "";
		int num = 0;
		String codes1 = "";
		for(x=0; x<code1.size(); x++)
		{
			campo =  (String)code1.elementAt(x);
			num = Integer.parseInt(campo);
			codes1 = codes1 + (String)datasession.get("servicio"+(num+1));
			if((code1.size() != 1) && (code1.size() != (x+1)))
				codes1 = codes1 + ", ";
			else
				codes1 = codes1 + "; ";
		}
		String codes2 = "";
		for(x=0; x<code2.size(); x++)
		{
			campo =  (String)code2.elementAt(x);
			num = Integer.parseInt(campo);
			codes2 = codes2 + (String)datasession.get("servicio"+(num+1));
			if((code2.size() != 1) && (code2.size() != (x+1)))
				codes2 = codes2 + ", ";
		}
		out.println("<b>Servicio(s) " + codes1 + " no compatible(s) con Servicio(s) " + codes2 + ". Procesar por separado...</b>");
	}
	if(pcdOK.equals("8"))
		out.println("<b>Problemas con la definici&oacute;n de la forma de pago del Servicio " + (String)datasession.get("servicio"+(x+1)) +", contactar al Ejecutivo de Cuenta...</b>");
	if(pcdOK.equals("9"))
		out.println("<b>El servicio "+ (String)datasession.get("servicio"+(iServ+1))+" no puede ser pagado con otros servicios favor de capturarlo  de nuevo</b>");
	if(pcdOK.equals("10"))
		out.println("<b>El servicio "+ (String)datasession.get("servicio"+(iServ+1))+" solo permite realizar un solo pago</b>");
	
	session.setAttribute("page.datasession",datasession);	
%>
</html>

