<!--
//*************************************************************************************************
//            Elemento: PageBuilder0027.jsp
//             Funcion: JSP de captura del SAT. 
//          Creado por: Israel de Paz Mercado
// CCN - 4360409 - 06/01/2006 - Se crea PageBuilder para la Linea de Captura del SAT.
// CCN - 4360581 - 12/03/2007 - Se convierten los estilos en jsf y se hace un include en el jsp
//*************************************************************************************************
-->


<%@ page session="true" import="java.util.*"  %>

<%
response.setHeader("Cache-Control","no-store"); //HTTP 1.1
response.setHeader("Pragma","no-cache"); //HTTP 1.0
response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<%
String Monto = new String("");
String txnAuthLevel = (String)session.getAttribute("page.txnAuthLevel");
String txnIMGAutoriz = (String)session.getAttribute("page.txnIMGAutoriz");
Vector listaCampos = (Vector)session.getAttribute("page.listcampos");
String TxnE = (String)session.getAttribute("page.iTxn");

Hashtable datasession = (Hashtable)session.getAttribute("page.datasession");
if(datasession == null)
  datasession = new Hashtable();
  
  
  String iTxn = (String)session.getAttribute("page.iTxn");
  String oTxn = (String)session.getAttribute("page.oTxn");
  String cTxn = (String)session.getAttribute("page.cTxn");  
  String txnProcess = (String)session.getAttribute("txnProcess");
  String montotran = "";
  String txtTxn = cTxn;
  
  
  
  
int i,k;  

%>

<html>
<head>
<%@include file="style.jsf"%>


<script language="JavaScript">


function validate(form)
{

if (document.entry.txtMonto)
{
<%
      out.println("  if( !top.validate(window, document.entry.txtMonto, 'isCurrency') )");
      out.println("    return");%>
}

if (document.entry.referenciaSat)
{
<%
      out.println("  if( !top.validate(window, document.entry.referenciaSat, 'valRefSat') )");
      out.println("    return");%>
}



form.submit();

}





</script>
</head>
<body bgcolor="#FFFFFF" onload="top.setFieldFocus(window.document)">
<br>
<% out.print("<form name=\"entry\" action=\"../servlet/ventanilla.DataServlet\" method=\"post\" ");
   if(txnAuthLevel.equals("1"))
    out.print("onSubmit=\"return validate(this)\"");
   out.println(">");
%>
<h1><%=session.getAttribute("page.txnImage")%></h1>
<h3>
<%out.print(session.getAttribute("page.cTxn") + "  " + (String)session.getAttribute("page.txnlabel"));%>
</h3>
<table border="0" cellspacing="0" class="normaltextblack">
<tr>



<%

for(i =  0; i < listaCampos.size(); i++)
{
	  Vector vCampos = (Vector)listaCampos.get(i);
	  vCampos = (Vector)vCampos.get(0);
	  out.println("<tr>");
	  out.println("  <td align='left'>" + vCampos.get(2) + ":</td>");
	 
	   if(!vCampos.get(0).toString().substring(0,3).equals("lst"))
          {
              if(vCampos.get(0).toString().substring(0,3).equals("pss"))
                out.print  ("  <td><input  align='left'   maxlength=\"" + vCampos.get(3) + "\" name=\""+vCampos.get(0)+"\"   size=\"" + vCampos.get(3) +"\" type=\"Password\"" + " tabindex=\""+(i+1)+"\"" );
              else
                if(vCampos.get(0).toString().equals("txtMonto"))
                out.print("  <td><input  align='left'   maxlength='14' name=\""+vCampos.get(0)+"\"    size='14' type=\"text\"" + " tabindex=\""+(i+1)+"\"");
                else
                out.print("  <td><input  align='left'   maxlength=\"" + vCampos.get(3) + "\" name=\""+vCampos.get(0)+"\"    size=\"" + vCampos.get(3) +"\" type=\"text\"" + " tabindex=\""+(i+1)+"\"");
          }  
      
      out.println("</td>");     
      out.println("</tr>");          
}
 out.println("</table>");

   if(txnAuthLevel.equals("1") || txnIMGAutoriz.equals("1"))
    out.println("<a id='btnCont' tabindex=\"" + (i+1) + "\" href=\"javascript:validate(document.entry);javascript:AuthoandSignature(document.entry)\" ><img src=\"../ventanilla/imagenes/b_continuar.gif\" border=\"0\"></a>");
   else
    out.println("<a id='btnCont' tabindex=\"" + (i+1) + "\" href=\"javascript:validate(document.entry)\"><img src=\"../ventanilla/imagenes/b_continuar.gif\" border=\"0\"></a>");


%>
<input type="hidden" name="iTxn" value="<%= session.getAttribute("page.iTxn") %>">
<input type="hidden" name="oTxn" value="<%= session.getAttribute("page.oTxn") %>">
<input type="hidden" name="cTxn" value="<%= session.getAttribute("page.cTxn") %>">
<input type="hidden" name="sucursal" value="<%= session.getAttribute("branch") %>">
<input type="hidden" name="teller" value="<%= session.getAttribute("teller") %>">
<input type="hidden" name="moneda" value="<%= session.getAttribute("page.moneda")%>">
<input type="hidden" name="registro" value="<%=session.getAttribute("empno")%>">
<input type="hidden" name="tllrlvl" value="<%=session.getAttribute("tellerlevel")%>">
<input type="hidden" name="supervisor" value="<%= session.getAttribute("teller") %>">
<input type="hidden" name="AuthoOK" value="0">
<input type="hidden" name="SignOK" value="0">
<input type="hidden" name="validateFLD" value="0">
<input type="hidden" name="needSignature" value="<%out.print(txnIMGAutoriz);%>">
<input type="hidden" name="needAutho" value="<%out.print(txnAuthLevel);%>">
<input type="hidden" name="SignField" value="<%= session.getAttribute("page.SignField") %>">
<input type="hidden" name="override" value="N">
<input type="hidden" name="txtFees" value="0">
<input type="hidden" name="compania" value="20">
<input type="hidden" name="txtSelectedItem" value="0" >
<input type="hidden" name="returnform" value="N" >
<input type="hidden" name="returnform2" value="N" >

<input type="hidden" name="passw" value="<%=session.getAttribute("password")%>">

   

</form>     
</body>
</html>
