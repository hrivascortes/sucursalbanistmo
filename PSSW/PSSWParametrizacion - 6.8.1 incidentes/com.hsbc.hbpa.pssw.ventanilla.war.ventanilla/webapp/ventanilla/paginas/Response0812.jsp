<!--
//*************************************************************************************************
//             Funcion: JSP que despliega respuesta de txn 0812
//            Elemento: Response0812.jsp
//          Creado por: Alejandro Gonzalez Castro
//      Modificado por: Alejandro Gonzalez Castro
//*************************************************************************************************
// CCN - 4360275 - 18/02/2005 - WAS 5.1
//*************************************************************************************************
-->

<%@ page session="true" import="java.util.*"%>
<%
response.setHeader("Cache-Control","no-store"); //HTTP 1.1
response.setHeader("Pragma","no-cache"); //HTTP 1.0
response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<%!
  private String getString(String s)
  {
    int len = s.length();
     for(; len>0; len--)
	  if(s.charAt(len-1) != ' ')
	    break;

    return s.substring(0, len);
  }

  private String getCantidad(String s)
   {
     int Indice = s.indexOf(".");
     String cadena = "";
     cadena = s.substring(Indice,Indice+3);
     int Indice2 = Indice;
     for(; Indice>0; Indice--)
	  if(s.charAt(Indice-1) == ' ')
	    break;

     cadena = s.substring(Indice,Indice2)+cadena;
     return cadena;
  }
private String getItem(String newCadNum, int newOption)
  {
   int iPIndex = newCadNum.indexOf("CONSECUTIVO:") + 12, NumSaltos, Num = 0, Ban = 0;
   String nCadNum = new String("");
   NumSaltos = newOption;
   while(Num < NumSaltos)
   {
    if(newCadNum.charAt(iPIndex) == 0x20)
    {
     while(newCadNum.charAt(iPIndex) == 0x20)
      iPIndex++;
     Num++;
    }
    else
     while(newCadNum.charAt(iPIndex) != 0x20)
      iPIndex++;
   }
   while(newCadNum.charAt(iPIndex) != 0x20 && iPIndex < newCadNum.length()){
    nCadNum = nCadNum + newCadNum.charAt(iPIndex);
    if(iPIndex < newCadNum.length())
      iPIndex++;
    if(iPIndex == newCadNum.length())
      break;
   }

   return nCadNum;
  }


%>
<% Vector resp = (Vector)session.getAttribute("response");
Hashtable datasession = (Hashtable)session.getAttribute("page.datasession");
String cTxn = (String)session.getAttribute("page.cTxn");
%>
<html>
<head>
  <title>Certificación</title>
<style type="text/css">Body {font-family: Arial, Helv; font-size: 10pt; font-weight: bold; color:#000000}</style>
</head>
<script language="javascript">
</script>
<body>
<p>
<%
   if( resp.size() != 4 )
    out.println("<b>Error de conexi&oacute;n</b><br>");
   else
   {
    try{
     String codigo = (String)resp.elementAt(0);
     if( !codigo.equals("0") )
        out.println("<b>Error en transaccion</b><br>");
     int lines = Integer.parseInt((String)resp.elementAt(1));
     Vector lineas = new Vector();
     String szResp = (String)resp.elementAt(3);
     for(int i=0; i<lines; i++){
        String line = szResp.substring( i*78, Math.min(i*78+77, szResp.length()) );
        if ( !codigo.equals("0") || cTxn.equals("0067"))
            out.println(getString(line) + "<p>");
        lineas.addElement(getString(line));
        if(Math.min(i*78+77, szResp.length()) == szResp.length())
           break;
     }

     lines = lineas.size();
     String linearesp = "";
     if (codigo.equals("0") && cTxn.equals("0812") ) {
        linearesp = (String)lineas.elementAt(0);
        String txtNombre = linearesp.substring(41);
        linearesp = (String)lineas.elementAt(1);
        String txtRFC = linearesp.substring(41);
        linearesp = (String)lineas.elementAt(2);
        String txtCuenta = linearesp.substring(41);
        out.println("<table border=\"0\">");
        out.println(" <tr>");
        out.print("  <td>Nombre:</td>" + "<td width=\"10\">&nbsp;</td><td>"  + txtNombre + "</td><td width=\"10\">&nbsp;</td>");
        out.println(" </tr>");
        out.println(" <tr>");
        out.println("<td>RFC:</td>" + "<td width=\"10\">&nbsp;</td><td>"  + txtRFC + "</td>");
        out.println(" </tr>");
        out.println(" </table>");
        datasession.put("txtDDACuenta",txtCuenta);
        }
    }

    catch(NumberFormatException nfe){
     out.println("<b>Error de conexi&oacute;n</b><br>");
    }
   }
%>
<form name="Txns" action="../../servlet/ventanilla.PageServlet" method="post">
<table border="0" cellspacing="0">
<tr>
<td>
</td>
</tr>
<tr>
<td>

<%
  Stack flujotxn = (Stack)session.getAttribute("page.flujotxn");
  if(!flujotxn.empty()){
   out.print("<div align=\"center\">");
   out.print("<input type=\"image\" src=\"../imagenes/b_aceptar.gif\" alt=\"\" border=\"0\">");
   out.print("</div>");
  }
%>
  <input type="hidden" name="transaction" value="<%= session.getAttribute("page.iTxn") %>">
  <input type="hidden" name="transaction1" value="<%= session.getAttribute("page.oTxn") %>">
</td>
</tr>
</table>
</form>
</body>
</html>
