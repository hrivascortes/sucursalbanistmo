<!-- 
//*************************************************************************************************
//             Funcion: JSP que carga la impresion
//            Elemento: DespImp.jsp
//          Creado por: Juan Carlos Gaona
//      Modificado por: Fausto Rodrigo Flores Moreno
//*************************************************************************************************
// CCN -4360170- 20/07/2004 - Se carga impresion con applet
// CCN - 4360189 - 03/09/2004 - Se cambia final.html por Final.jsp
// CCN - 4360246 - 19/11/2004 - Se envia la respuesta de Hogan codificada para evitar problemas de impresion
// CCN - 4360455 - 07/04/2006 - Se realizan modifiaciones para esquema de cheques devueltos.
// CCN - 4360462 - 10/04/2005 - Se genera nuevo paquete por errores en paquete anterior
// CCN - 4360493 - 23/06/2006 - Se realizan modificaciones para imprimir el nombre del proveedor en la txn 1053
// CCN - 4360509 - 08/09/2006 - Se realizan modificaciones la ficha de deposito y perfiles por usuario
// CCN - 4360556 - 12/01/2007 - Se realiza correcci�n para enviar txn 1003 a final.jsp
//*************************************************************************************************
--> 

<%@ page session="true"%>
<%
response.setHeader("Cache-Control","no-store"); //HTTP 1.1
response.setHeader("Pragma","no-cache"); //HTTP 1.0
response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
%>
<%!
  private String stringFormat(int option, int sum)
  {
    java.util.Calendar now = java.util.Calendar.getInstance();
    String temp = new Integer(now.get(option) + sum).toString();
    if( temp.length() != 2 )
      temp = "0" + temp;

    return temp;
  }
%>
<HTML>
<HEAD>
 <TITLE>Impresi�n</TITLE>
</HEAD>
<Script language="JavaScript">

	function submiteForma()  
	{
          document.forms['fImpresion'].action = "Final.jsp";
          document.forms['fImpresion'].target = "panel";
          document.forms['fImpresion'].submit();
          setTimeout("window.close()",1000); 
	}

	function submite1003()  
	{
          document.forms['fImpresion'].action = "RedirectorCSIB.jsp";
          document.forms['fImpresion'].target = "panel";
          document.forms['fImpresion'].submit();
          setTimeout("window.close()",1000); 
	}

	function submiteDiario()
	{
		document.forms['fImpresion'].action = "ResponseReverso.jsp";
		document.forms['fImpresion'].target = "panel";
		document.forms['fImpresion'].submit();
		setTimeout("window.close()",1000);
	}

	function submiteCRM()
	{
		document.forms['fImpresion'].action = "PageBuilderCRM.jsp";
		document.forms['fImpresion'].target = "panel";
		document.forms['fImpresion'].submit();
		setTimeout("window.close()",1000);
	}

	function submiteRap()
	{
		document.forms['fImpresion'].action = "RedirectorRAP.jsp";
		document.forms['fImpresion'].target = "panel";
		document.forms['fImpresion'].submit();
		setTimeout("window.close()",1000);
	}

	function submiteRAPL()
	{
		document.forms['fImpresion'].action = "RedirectorRAPCARP.jsp";
		document.forms['fImpresion'].target = "panel";
		document.forms['fImpresion'].submit();
		setTimeout("window.close()",1000);
	}
	
	function submite0818()
	{
		document.forms['fImpresion'].action = "PageBuilderSCC2.jsp";
		document.forms['fImpresion'].target = "panel";
		document.forms['fImpresion'].submit();
		setTimeout("window.close()",1000);
	}
	
	function submiteRapCheqDev()
	{
		document.forms['fImpresion'].action = "PageBuilder5361.jsp";
		document.forms['fImpresion'].target = "panel";
		document.forms['fImpresion'].submit();
		setTimeout("window.close()",1000);
	}

	
	function CloseWindows()
	{
		window.close()
		}
	

</script>
<BODY TOPMARGIN="0" LEFTMARGIN="0" MARGINWIDTH="0" MARGINHEIGHT="0">
<SCRIPT language="JavaScript">
	var formName = 'Mio'

	function closeme()
	{
		window.close()
		}

	function handleOK(form)
	{
 		if(window.opener && !window.opener.closed)
 		{
			transferData()
			window.opener.dialogWin.returnFunc.doValidate()
			}
		else
		{
			alert("Se cerr� la ventana de Autorizaci�n!")
		}
		closeme()
		return false
	}

	function handleCancel()
	{
		window.opener.dialogWin.returnedValue = '0'
		window.opener.dialogWin.returnFunc.doValidate()
			closeme()
		return false
	}

	function getFormData(form)
	{
		var searchString = ""
		var onePair
		for (var i = 0; i < form.elements.length; i++)
		{
			if (form.elements[i].type == "hidden")
				onePair = escape(form.elements[i].name) + "&" +escape(form.elements[i].value)
			else
				continue
			searchString += onePair + "&"
		}
			return searchString
	}

	function transferData()
	{
		if(top.opener && !top.opener.closed)
		{
//			top.opener.dialogWin.returnedValue = getFormData(document.Impresion)
	                        window.opener.dialogWin.returnedValue = getFormData(document.forms['fImpresion']) ;
                        
		}
	}
</SCRIPT>
<%
	String txtCadImpresion = new String("");
	String txtResponse = new String("");
	String txtTmp = new String("");
	String txtReverso = new String("N");
	if(session.getAttribute("txtCadImpresion") != null)
		txtCadImpresion = session.getAttribute("txtCadImpresion").toString();
	if(session.getAttribute("txtResponse") != null)
		txtResponse = session.getAttribute("txtResponse").toString();
	if(session.getAttribute("parte") != null)
	{
		String strParte = (String)session.getAttribute("parte");
		if (strParte.equals("RELACION0818~"))
			txtCadImpresion = strParte + (String)session.getAttribute("opcion1") + "~";
	}
	if(session.getAttribute("ReversoLink") != null)
		txtReverso = session.getAttribute("ReversoLink").toString();
	if(session.getAttribute("transactionID") != null)	
		if (session.getAttribute("transactionID").toString().equals("1053"))
			if (session.getAttribute("PROVCHQ") != null)
				txtResponse = txtResponse + "PROVEEDOR@" + session.getAttribute("PROVCHQ").toString()+"^";
        
		%>

<applet code='Imprimir' codebase='../' name='Impresion' archive='imprimir.jar' WIDTH ="160" HEIGHT = "120" mayscript>
          <PARAM NAME="RemoteIP" value = "<%=request.getServerName()%>">
          <PARAM NAME="RemotePort" value = "<%=request.getServerPort()%>">
          <PARAM NAME="Ruta" value = "<%=request.getContextPath()%>">
		<PARAM NAME="Reverso" value ="<%out.print(txtReverso);%>">
		<PARAM NAME="Respuesta" value ="<%out.print(java.net.URLEncoder.encode(txtResponse));%>">
		<PARAM NAME="Cadena" value ="<%out.print(java.net.URLEncoder.encode(txtCadImpresion));%>">
    <%java.util.Hashtable htReq = null;

	String date = new String(stringFormat(java.util.Calendar.YEAR, 0) + stringFormat(java.util.Calendar.MONTH, 1) + stringFormat(java.util.Calendar.DAY_OF_MONTH, 0));
	String time = stringFormat(java.util.Calendar.HOUR_OF_DAY, 0) + stringFormat(java.util.Calendar.MINUTE, 0) + stringFormat(java.util.Calendar.SECOND, 0);
	if(session.getAttribute("branchinfo") != null)
    {
	  java.util.Hashtable ht = (java.util.Hashtable)session.getAttribute("branchinfo");
	  String branchInfo = "NOMSUC=" + ht.get("D_NOM_SUCURSAL").toString() + "@";
	  branchInfo += "NUMSUC=" + ht.get("C_NUM_SUCURSAL").toString() + "@";
	  branchInfo += "DOMSUC=" + ht.get("D_CALLE").toString() + " " + ht.get("D_COLONIA").toString() + "@";
	  branchInfo += "PLZSUC=" + ht.get("D_NUM_PLAZA").toString() + "@";
	  branchInfo += "PLZNOM=" + ht.get("D_NOM_PLAZA").toString() + "@";
	  branchInfo += "CIUSUC=" + ht.get("D_CIUDAD").toString() + "@";	  
	  out.print(" <PARAM NAME=\"DatosSucursal\" value=\"" + branchInfo + "\">");
    }
	if(session.getAttribute("strFlujoRap") != null)
	  out.print(" <PARAM NAME=\"Flujo\" value=\"" + session.getAttribute("strFlujoRap").toString() + "\">");
	if(session.getAttribute("strFlujoRapCAPR") != null)
	  out.print(" <PARAM NAME=\"FlujoCAPR\" value=\"" + session.getAttribute("strFlujoRapCAPR").toString() + "\">");  
	if(session.getAttribute("CFESP") != null)
	  out.print(" <PARAM NAME=\"CFESP\" value=\"" + session.getAttribute("CFESP").toString() + "\">");
	if(session.getAttribute("crmInfo") != null)
	  out.print(" <PARAM NAME=\"crmInfo\" value=\"" + session.getAttribute("cmrInfo") + "\">");
	else
	  out.print(" <PARAM NAME=\"crmInfo\" value=\"SIN-DATOS\">");
	if(session.getAttribute("parte") != null)
	{
		String strParte = (String)session.getAttribute("parte");
		session.removeAttribute("parte");
		if (strParte.equals("RELACION0818~"))
			session.removeAttribute("opcion1");
		else
	  		out.print(" <PARAM NAME=\"Flujo\" value=\"" + 10 + "\">");
	}
	if(session.getAttribute("FichaDeposito") != null)
		out.print(" <PARAM NAME=\"strValorFicha\" value=\"" + session.getAttribute("FichaDeposito").toString() +  "\"> ");	
	else
		out.print(" <PARAM NAME=\"strValorFicha\" value=\"N\"> ");
	if(session.getAttribute("montoChqDev1003") != null)
		out.print(" <PARAM NAME=\"strMontoChqDev1003\" value=\"" + session.getAttribute("montoChqDev1003").toString() +  "\"> ");	
	else
		out.print(" <PARAM NAME=\"strMontoChqDev1003\" value=\"0\"> ");
	if(session.getAttribute("FichaActiva") != null)
	{
		if(session.getAttribute("FichaActiva").toString().equals("S"))
			out.print(" <PARAM NAME=\"strFicha\" value=\"S\"> ");	
		else
			out.print(" <PARAM NAME=\"strFicha\" value=\"N\"> ");	
	}
	else
		out.print(" <PARAM NAME=\"strFicha\" value=\"N\"> ");	
	if(session.getAttribute("tellerID") != null)
	  out.print(" <PARAM NAME=\"strCajero\" value=\"" + session.getAttribute("tellerID").toString() + "\"> ");
	if(session.getAttribute("tellerperfil") != null)
	  out.print(" <PARAM NAME=\"strTellerPerfil\" value=\"" + session.getAttribute("tellerperfil").toString() + "\"> ");
	else
	  out.print(" <PARAM NAME=\"strTellerPerfil\" value=\"00\"> ");
	if(session.getAttribute("Consecutivo") != null)
	  out.print(" <PARAM NAME=\"Consecutivo\" value=\"" + session.getAttribute("Consecutivo").toString() + "\" >");
	if(session.getAttribute("transactionID") != null)
	  out.print(" <PARAM NAME=\"strTransaccion\" value=\"" + session.getAttribute("transactionID").toString() + "\">");
	out.print(" <PARAM NAME=\"strDate\" value=\"" + date + "\">");
	out.print(" <PARAM NAME=\"strTime\" value=\"" + time + "\">");	
out.print(" <PARAM NAME=\"strIdentidad\" value=\"" + session.getAttribute("identidadApp").toString() + "\">");
out.print(" <PARAM NAME=\"strIdentidad2\" value=\"" + session.getAttribute("identidadApp2").toString() + "\">");
%>

</applet>


<%

	if(session.getAttribute("txtCadImpresion") != null)
 	   	session.removeAttribute("txtCadImpresion");
	if(session.getAttribute("txtResponse") != null)
	    session.removeAttribute("txtResponse");
	if(session.getAttribute("strFlujoRap") != null)
    	session.removeAttribute("strFlujoRap");
    if(session.getAttribute("strFlujoRapCAPR") != null)
    session.removeAttribute("strFlujoRapCAPR");
	if(session.getAttribute("CFESP") != null)
	    session.removeAttribute("CFESP");
  	if(session.getAttribute("txtCasoEsp") != null)
		session.removeAttribute("txtCasoEsp");
  	if(session.getAttribute("ReversoLink") != null)
		session.removeAttribute("ReversoLink");
	if (session.getAttribute("PROVCHQ") != null)
		session.removeAttribute("PROVCHQ");	 
	if(session.getAttribute("txn5361") != null)
		session.removeAttribute("txn5361");

	 
	 
	 
 	if(session.getAttribute("transactionID") != null)
		if (session.getAttribute("transactionID").toString().equals("txn1003"))
			if(session.getAttribute("FichaDeposito") != null)		
				session.removeAttribute("FichaDeposito");

	 	 
	
%>
	<FORM NAME="fImpresion" action="" method="POST">
   	<input type="hidden" name="nothing" value="nothing">
</FORM>

</BODY>
</HTML>


