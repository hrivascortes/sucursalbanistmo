<!--
// CCN - 4360581 - 12/03/2007 - Se convierten los estilos en jsf y se hace un include en el jsp
-->

<%@ page session="true"%>
<%
response.setHeader("Cache-Control","no-store"); //HTTP 1.1
response.setHeader("Pragma","no-cache"); //HTTP 1.0
response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
	<head>
		<%@include file="style.jsf"%>
		<title>Impresión de Relación de Cheques</title>
		<base href="http://<%out.print(request.getServerName() + request.getContextPath());%>/">
	</head>
	<Script language="JavaScript">
		function sendfile()
		{
			document.entry.action = "<%=request.getContextPath()%>/servlet/SendfileServlet"
			document.entry.submit();
		}
   </SCRIPT>	
	<body>
          <h1><%=session.getAttribute("page.txnImage")%></h1>
		<form name="entry">
			<h3>
<%						out.print(session.getAttribute("page.cTxn") + "  " + (String)session.getAttribute("page.txnlabel"));
%>
			</h3>
			<p>	
			<br><br>					
      	<input type="hidden" name="Page"    value="IMPR">
			<a href="JavaScript:sendfile()"><img src="imagenes/Aceptar.jpg" border="0"></a>
		</form>
	</body>
</html>
