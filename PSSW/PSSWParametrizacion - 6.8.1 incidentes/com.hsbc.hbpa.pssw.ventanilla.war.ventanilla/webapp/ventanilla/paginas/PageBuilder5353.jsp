<!--
//*************************************************************************************************
//             Funcion: JSP que presenta las opciones para la txn 0572
//            Elemento: PageBuilder5353.jsp
//          Creado por: Alejandro Gonzalez Castro
//      Modificado por: Juan Carlos Gaona Marcial
//*************************************************************************************************
//CCN - 4360268 - 04/02/2005 - Se realizan adecuaciones para eliminar Agencia NY
// CCN - 4360334 - 17/06/2005 - Se convierte fieldLector a fragmento
// CCN - 4360455 - 07/04/2005 - Se realizan modificaciones para el esquema de cheques devueltos.
// CCN - 4360462 - 10/04/2005 - Se genera nuevo paquete por errores en paquete anterior
// CCN - 4360508 - 08/09/2006 - Se realizan modificaciones para la eliminaci�n de la variable de ficha
// CCN - 4360581 - 12/03/2007 - Se convierten los estilos en jsf y se hace un include en el jsp
//*************************************************************************************************
-->
 
<%@ page session="true"  import="java.util.*"%> 
<%
response.setHeader("Cache-Control","no-store"); //HTTP 1.1
response.setHeader("Pragma","no-cache"); //HTTP 1.0
response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<%
Vector listaCampos = (Vector)session.getAttribute("page.listcampos");
Vector listaContenidos = (Vector)session.getAttribute("page.listcont");
String txnAuthLevel = (String)session.getAttribute("page.txnAuthLevel");
String txnIMGAutoriz = (String)session.getAttribute("page.txnIMGAutoriz");
Vector v = (Vector)session.getAttribute("page.specialacct");
Hashtable datasession = (Hashtable)session.getAttribute("page.datasession");
String chqDev = (String)datasession.get("chqDev");
String txn = session.getAttribute("page.cTxn").toString();
String inTxn =session.getAttribute("page.iTxn").toString();
String outTxn = session.getAttribute("page.oTxn").toString();
String moneda = session.getAttribute("page.moneda").toString();
String flagKroner="NO";
if(session.getAttribute("pasoP001")!=null)
	flagKroner = session.getAttribute("pasoP001").toString();

if (chqDev == null)
    chqDev="NO";
    
if(inTxn.equals("1003") && flagKroner.equals("NO") && (moneda.equals("01") || moneda.equals("N$")))
	chqDev="SI";

datasession.put("chqDev",chqDev);

for (int i=0; i < listaCampos.size(); i++)
  {
    Vector vCampos = (Vector)listaCampos.get(i);
    Vector vCampo = (Vector)vCampos.get(0);
    
    if(chqDev.equals("SI") && vCampo.get(4).equals("isValidCheque")) 
      {
       vCampo.set(4,"isValidChequeDev");
       vCampos.setElementAt(vCampo,0);
       listaCampos.set(i,vCampos);
      }
  }  

if (datasession == null)
  datasession = new Hashtable();

int k;
%>
<html>
<head>
  <%@include file="style.jsf"%>
  <title>Datos de la Transaccion
<%!

private String getItem(String newCadNum, int newOption)
{
  int iPIndex = newCadNum.indexOf("CONSECUTIVO:") + 12, NumSaltos, Num = 0, Ban = 0;
  String nCadNum = new String("");
  NumSaltos = newOption;
  while(Num < NumSaltos)
  {
    if(newCadNum.charAt(iPIndex) == 0x20)
    {
      while(newCadNum.charAt(iPIndex) == 0x20)
        iPIndex++;
        
      Num++;
    } else {
      while(newCadNum.charAt(iPIndex) != 0x20)
        iPIndex++;
    }
  }
    
  while(newCadNum.charAt(iPIndex) != 0x20)
    nCadNum = nCadNum + newCadNum.charAt(iPIndex++);

  return nCadNum;
}
%>
  </title>

<script  language="JavaScript">
<!--
var formName = 'Mio'
var specialacct = new Array()
<%
if(!v.isEmpty())
{
  for(int iv = 0; iv < v.size(); iv++)
  {
    out.print("specialacct[" + iv + "]='" + v.get(iv) + "'" + '\n');
  }
}
  
String MontoPro;
String MontoCheques = (String)datasession.get("txtCheque");
if ( (String)datasession.get("MontoTotal") == null)
{
  MontoPro = "00";
  if (session.getAttribute("FichaDeposito") != null)
  	session.removeAttribute("FichaDeposito");
}
else
  MontoPro = (String)datasession.get("MontoTotal");
  
MontoPro = formatMonto(MontoPro);

out.print("MontoPro='" + MontoPro + "';" +'\n');
out.print("MontoCheques='" + MontoCheques + "';" +'\n');
%>

<%!
private String formatMonto(String s)
{
  int len = s.length();
  if (len < 3)
    s = "0." + s;
  else
  {
    int n = (len - 3 ) / 3;
    s = s.substring(0, len - 2) + "." + s.substring(len - 2 , len);
    for (int i = 0; i < n; i++)
    {
      len = s.length();
      s = s.substring(0, (len -(i*3+6+i))) + "," + s.substring((len -(i*3+6+i)) , len);
    }
  }
  
  return s;
}
%>

function validate(form)
{
  if (form.txtCveTran.value.length == 0 || form.txtDDACuenta2.value.length == 0 || form.txtSerial2.value.length == 0 )
  {
    alert("Introduzca Banda del Cheque.");
    return;
  }
  
<%
  for (int i=0; i < listaCampos.size(); i++)
  {
    Vector vCampos = (Vector)listaCampos.get(i);
    vCampos = (Vector)vCampos.get(0);
    if( vCampos.get(4).toString().length() > 0 && datasession.get(vCampos.get(0)) == null)
    {
      out.println("  if( !top.validate(window, document.entry." + vCampos.get(0) + ", '" + vCampos.get(4) +"') )");
      out.println("    return;");
    }
  }
%>
  form.validateFLD.value = '1';

  if (document.entry.txtFechaEfect || document.entry.txtFechaError || document.entry.txtFechaCarta)
     document.entry.txtFechaSys.value=top.sysdatef();

  if (form.dvCheque != null && form.dvCheque.value != '' && form.txtSerial2 != null && form.txtSerial2.value.length > 0) {
	 form.noCheque.value = (form.txtSerial2.value).substr(0,(form.txtSerial2.value).length - 1);
	 form.txtSerial2.value = form.noCheque.value;
  }
  if (form.dvCuenta != null && form.dvCuenta.value != '' && form.txtDDACuenta2 != null && form.txtDDACuenta2.value.length > 0){
	 form.noCuenta.value = top.filler((form.txtDDACuenta2.value).substr(0,(form.txtDDACuenta2.value).length - 1), 11);
	 form.txtDDACuenta2.value = form.noCuenta.value;
  }

<%
  if(!txnAuthLevel.equals("1") && !txnIMGAutoriz.equals("1"))
	{
    out.println("  form.submit();");
    out.println("var obj = document.getElementById('btnCont');"); 
    out.println("obj.onclick = new Function('return false');");   
	}   
%>
}


function AuthoandSignature(entry)
{
  var Cuenta;
  if(entry.validateFLD.value != '1')
    return;
    
  if(entry.needAutho.value == '1' && entry.AuthoOK.value != '1')
  {
    top.openDialog('/ventanilla/paginas/authoriz.jsp', 220, 130, 'top.setPrefs()', entry);
  }
  
  if(entry.needSignature.value == '1' && entry.SignField.value != '')
  {
    if (entry.txtDDACuenta2.value == '00103000023' || entry.txtDDACuenta2.value == '03902222222' )
      Cuenta = eval('entry.txtFirmaFun.value');
    else
      Cuenta = eval('entry.' + entry.SignField.value + '.value');
      
//Para que viaje el c�digo de seguridad
  
  var cveTran = entry.txtCveTran.value;
      
     <%
   if (chqDev != null)
   {
     if (chqDev.equals("SI")) 
     {%>
         if(entry.lstChqDev.value == 00) 
           {
           	var Cod = entry.txtCodSeg.value;
			var Cve = entry.txtCveTran.value;
			var Cuenta = entry.txtDDACuenta2.value;
			var Serial = entry.txtSerial2.value;
			var Monto = entry.txtMontoCheque.value;
			var Firma = entry.txtFirmaFun.value;
            top.openModalDialog('<%=request.getContextPath()%>/servlet/ventanilla.FirmasServlet?txtCodSeg='+Cod+'&txtCveTran='+Cve+'&txtDDACuenta2='+Cuenta+'&txtSerial2='+Serial+'&txtMontoCheque='+Monto+'&txtFirmaFun='+Firma+'&txtDDACuenta=' + Cuenta, 710, 430, 'top.setPrefs2()', entry);
           }      
           else
           {
           entry.action="../servlet/ventanilla.ChequeDev";
           entry.submit();
           }
        <%}else
              {%>   
    top.openModalDialog('<%=request.getContextPath()%>/servlet/ventanilla.FirmasServlet?txtDDACuenta=' + Cuenta+'&txtCveTran='+cveTran, 710, 430, 'top.setPrefs2()', entry);
            <%}
     }
     else
     {%>
    top.openModalDialog('<%=request.getContextPath()%>/servlet/ventanilla.FirmasServlet?txtDDACuenta=' + Cuenta+'&txtCveTran='+cveTran, 710, 430, 'top.setPrefs2()', entry);
          <%}%>  
      
   
   
  }
}
//-->
</script>
</head>
<body bgcolor="#FFFFFF"  onload="top.setFieldFocus(window.document)">
<br>
<form name="entry" action="../servlet/ventanilla.DataServlet" method="post"
<%if(txnAuthLevel.equals("1"))
  out.print("onSubmit=\"alert('Submit');return validate(this)\"");
%>
>
<%
if (session.getAttribute("txtfactor")==null)
  if (datasession.get("txtfactor")!=null)
    session.setAttribute("txtfactor", (String)datasession.get("txtfactor"));
    
if (session.getAttribute("txttipoinf")==null)
  if (datasession.get("txttipoinf")!=null)
    session.setAttribute("txttipoinf", (String) datasession.get("txttipoinf"));
    
if (session.getAttribute("txtmesinf")==null)
  if (datasession.get("txtmesinf")!=null)
    session.setAttribute("txtmesinf", (String) datasession.get("txtmesinf"));
    
if (session.getAttribute("txtverif")==null)
  if (datasession.get("txtverif")!=null)
    session.setAttribute("txtverif", (String) datasession.get("txtverif"));

String RAP = (String)datasession.get("flujorap");
if(RAP == null)
  RAP = "";

if (RAP.equals("1"))
{
%>
	<h1>Recepci&oacute;n Automatizada de Pagos</h1>
<%
} else {
%>
    <h1><%=session.getAttribute("page.txnImage")%></h1>
<%
}
%>

<h3> 5353 - D�bito por cheque propio depositado / 4103 - Cheque de gerencia recibido en deposito </h3>
<table border="0" cellspacing="0">
<%

int i = 0;
for(i = 0; i < listaCampos.size(); i++)
{
  Vector vCampos = (Vector)listaCampos.get(i);
  vCampos = (Vector)vCampos.get(0);
  
 
if(chqDev == null) 
   chqDev = "NO";

if( (!vCampos.get(0).equals("lstChqDev")) || (chqDev.equals("SI") && vCampos.get(0).equals("lstChqDev")) )
{
  
  if(!vCampos.get(0).toString().equals("txtCodSeg"))
  {
    out.println("<tr>");
    out.println("  <td>" + vCampos.get(2) + ":</td>");
    out.println("  <td width=\"10\">&nbsp;</td>");
    
    if(!vCampos.get(0).toString().substring(0,3).equals("lst"))
    {
      if (vCampos.get(0).toString().equals("txtMontoCheque") && datasession.get("txtMontoCheque") != null && chqDev.equals("NO") )
      { 
        out.println(" <td>" + datasession.get("txtMontoCheque") + "</td>");
      } else {
        if (vCampos.get(0).toString().substring(0,3).equals("pss"))
        {
          out.print  ("  <td><input maxlength=\"" + vCampos.get(3) + "\" name=\"" + vCampos.get(0) + "\"" + 
                      " size=\"" + vCampos.get(3) +"\" type=\"Password\" tabindex=\"" + (i + 1) + "\"");
        } else {
          out.print  ("  <td><input maxlength=\"" + vCampos.get(3) + "\" name=\"" + vCampos.get(0) + "\"" + 
                      " size=\"" + vCampos.get(3) +"\" type=\"text\" tabindex=\"" + (i + 1) + "\"");
        }
        
        String CampoR = (String)vCampos.get(5);
	if(CampoR != null && CampoR.length() > 0)
	{
	  CampoR = CampoR.trim();
	  out.print(" value=\"" + CampoR + "\"");
	}
      }
    } else {
      out.print("  <td><select name=\"" + vCampos.get(0) + "\" size=\"" + vCampos.get(2) + "\"");
 
      // 2004-01-24 13:19 ARM: A los controles select no se les agrego el evento 'onKeyPressed'
      if( vCampos.get(3).toString().length() > 0 )
      {
	if(vCampos.get(0).toString().substring(0,3).equals("rdo"))
	  out.println(" onClick=\"top.validate(window, this, '" + vCampos.get(4) +"')\">");
	else
	  out.println(" onChange=\"top.validate(window, this, '" + vCampos.get(4) +"')\">");
      } else {
      	out.println(">");
      }
      
      for(k=0; k < listaContenidos.size(); k++)
      {
	Vector v1Campos = (Vector)listaContenidos.get(k);
	    int j;
	   for(j=0; j < v1Campos.size(); j++)
	   {
	     Vector v2Campos = (Vector)v1Campos.get(j);
             if(v2Campos.get(0).toString().equals(vCampos.get(0)))
	        {
	        out.println("  <option value=\"" + v2Campos.get(3) + "\">" + v2Campos.get(2));
	        }
	   }     
      }
      out.println("  </select></td>");
    }
    
    if (vCampos.get(0).toString().equals("txtMontoCheque") && datasession.get("txtMontoCheque") != null)
    {
      out.println("<td>&nbsp;</td>");
    } else {
      if( vCampos.get(4).toString().length() > 0 && !vCampos.get(0).toString().substring(0,3).equals("lst"))
	out.println(" onBlur=\"top.estaVacio(this)||top.validate(window, this, '" + vCampos.get(4) +"')\"  " + 
		    " onKeyPress=\"top.keyPressedHandler(window, this, '" + vCampos.get(4) +"')\" ></td>");
      else if(!vCampos.get(0).toString().substring(0,3).equals("lst"))
	out.println(">");
	out.println("</tr>");
    }
  } else {
    %><%@include file="fieldLector.jsf"%><%
    i = k ;
  }
  
  
 } 
  
}
%>
</table>
<p>
<p><b> Capture Numero de Firma solo para Cheque de Caja.</b>
<table border="0" cellspacing="0">
<%
   out.println("<tr>");
   out.println("<td> <b> Monto Procesado </b>");
   out.println("<td>" + MontoPro + "</td>");
   out.println("</tr>");
   out.println("<tr>");
   out.println("<td> <b> Monto Total </b>");
   out.println("<td>" + MontoCheques + "</td>");
   out.println("</tr>");

%>
</table>
<input type="hidden" name="ValChqDev" value="<%= chqDev %>">
<input type="hidden" name="iTxn" value="<%= session.getAttribute("page.iTxn") %>">
<input type="hidden" name="oTxn" value="<%= session.getAttribute("page.oTxn") %>">
<input type="hidden" name="cTxn" value="<%= session.getAttribute("page.cTxn") %>">
<input type="hidden" name="sucursal" value="<%= session.getAttribute("branch") %>">
<input type="hidden" name="teller" value="<%= session.getAttribute("teller") %>">
<input type="hidden" name="moneda" value="<%= session.getAttribute("page.moneda")%>">
<input type="hidden" name="registro" value="<%=session.getAttribute("empno")%>">
<input type="hidden" name="tllrlvl" value="<%=session.getAttribute("tellerlevel")%>">
<input type="hidden" name="supervisor" value="<%= session.getAttribute("teller") %>">
<input type="hidden" name="AuthoOK" value="0">
<input type="hidden" name="SignOK" value="0">
<input type="hidden" name="compania" value="20">
<input type="hidden" name="validateFLD" value="0">
<input type="hidden" name="needSignature" value="<%out.print(txnIMGAutoriz);%>">
<input type="hidden" name="needAutho" value="<%out.print(txnAuthLevel);%>">
<input type="hidden" name="SignField" value="<%= session.getAttribute("page.SignField") %>">
<input type="hidden" name="txtFees" value="0">

<input type="hidden" name="isChequeFormatoNuevo" value="N">
<input type="hidden" name="dvCuenta" value="">
<input type="hidden" name="dvCheque" value="">
<input type="hidden" name="dvRuta" value="">
<input type="hidden" name="noCheque" value="">
<input type="hidden" name="noCuenta" value="">


<p>
<script>
if (document.entry.txtFechaEfect)
{
  document.writeln("<input type=\"hidden\" name=\"txtFechaSys\">");
  document.entry.txtFechaEfect.value = top.sysdate();
}

if (document.entry.txtFechaError)
{
  document.writeln("<input type=\"hidden\" name=\"txtFechaSys\">");
  document.entry.txtFechaError.value = top.sysdate();
}

if (document.entry.txtFechaCarta)
{
  document.writeln("<input type=\"hidden\" name=\"txtFechaSys\">");
  document.entry.txtFechaCarta.value = top.sysdate();
}
</script>
<% if(txnAuthLevel.equals("1") || txnIMGAutoriz.equals("1"))
    out.println("<a id='btnCont' tabindex=\"" + (i + 1) + "\" href=\"javascript:validate(document.entry);javascript:AuthoandSignature(document.entry)\"><img src=\"../ventanilla/imagenes/b_continuar.gif\" border=\"0\"></a>");
   else
    out.println("<a id='btnCont' tabindex=\"" + (i + 1) + "\" href=\"javascript:alert('Boton');validate(document.entry)\"><img src=\"../ventanilla/imagenes/b_continuar.gif\" border=\"0\"></a>");
%>
</form>
</body>
</html>
