<!--
//*************************************************************************************************
///            Funcion: JSP que despliegar Respuesta de txns de Nomina
//            Elemento: Response04NOM.jsp
//          Creado por: Alejandro Gonzalez Castro
//      Modificado por: Alejandro Gonzalez Castro
//*************************************************************************************************
// CCN - 4360275 - 18/02/2005 - WAS 5.1
//*************************************************************************************************
-->

<%@ page session="true" import="java.util.*"%>
<%
response.setHeader("Cache-Control","no-store"); //HTTP 1.1
response.setHeader("Pragma","no-cache"); //HTTP 1.0
response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<%!
  private String getString(String s)
  {
   int len = s.length();
   for(; len>0; len--)
    if(s.charAt(len-1) != ' ')
     break;
   return s.substring(0, len);
  }%>
<%
Vector resp = (Vector)session.getAttribute("response");
	Hashtable datos = (Hashtable)session.getAttribute("page.datasession");
%>
<html>
<head>
  <title>Certificación</title>
<style type="text/css">Body {font-family: Arial, Helv; font-size: 10pt; font-weight: bold; color:#000000}</style>
<script language="javascript">
<!--
  sharedData = '0';

  function formSubmitter(){
    clearInterval(formSubmitter);
    if(sharedData != '0')
    {
      document.forms[0].submit()
      sharedData = '0';
     }
  }
//-->
</script>
</head>
<body bgcolor="#FFFFFF" onload="top.setFieldFocus(window.document);setInterval('formSubmitter()',2000)">
<center><b>-Nomina-</b></center>
<p>
<%
  java.util.Stack flujotxn = new java.util.Stack();
  String codigo = "";
  String needOverride = "NO";
  if(session.getAttribute("page.flujotxn") != null)
  	flujotxn = (java.util.Stack)session.getAttribute("page.flujotxn");

  if( resp.size() != 4 )
   out.println("<b>Error de conexi&oacute;n</b><br>");
  else
  {
   	try
   	{

     	codigo = (String)resp.elementAt(0);
   	 	if( !codigo.equals("0") )
    		out.println("<b>Transaccion Rechazada</b><br><br>");

     	int lines = Integer.parseInt((String)resp.elementAt(1));
    	String szResp = (String)resp.elementAt(3);
    	for(int i=0; i<lines; i++)
		{
    		String line = szResp.substring( i*78, Math.min(i*78+77, szResp.length()) );
     		out.println(getString(line) + "<p>");
     		if(Math.min(i*78+77, szResp.length()) == szResp.length())
      		break;
    	}

  	}
  	catch(NumberFormatException nfe){
     out.println("<b>Error de conexi&oacute;n</b><br>");
    }
  }
%>
<p>
<form name="Txns" action="../../servlet/ventanilla.DataServlet" method="post">
  <input type="hidden" name="transaction" value="<%= session.getAttribute("page.iTxn") %>">
  <input type="hidden" name="transaction1" value="<%= session.getAttribute("page.oTxn") %>">
<%
    String cend = "X";
    if(datos.get("end") !=null)
    cend = (String)datos.get("end");

	if(!cend.equals("X"))
	{
		if(cend.equals("1"))
			out.println("<br><b>Proceso terminado...</b>");
        {%>
         <script language="JavaScript" >
         document.forms[0].action = "Response51531.jsp";
         document.forms[0].submit()
		 </script>
        <%  }
    }
    else
	{
        if(datos.get("codigo") == null)
        {
%>
            <b>Iniciando Posting de Proceso Nomina...</b>
            <p>
            <b>Por favor espere</b>
<%      }
        else
        {
            String code = (String)datos.get("codigo");

            if (code.equals("0"))
                code = "Aceptado";
            else
                code = "Rechazado";
            String CurrA=(String)datos.get("CurrAbono");
            if(CurrA.equals("1"))
            {
%>
                <b>Cargo - <%=code%></b>
<%
            }
            else
            {
%>
                <b>Abono <%=Integer.parseInt(CurrA)-1%> de <%=Integer.parseInt((String)datos.get("sizectas"))-1%> - <%=code%></b>

<%
            }
        }
   	    out.println("<script language=\"javascript\">sharedData = '1'</script>");
 	}
%>
</form>
</body>
</html>
