<!--
//*************************************************************************************************
//             Funcion: JSP que presenta la respuesta de la txn 0070
//            Elemento: Response0070.jsp
//          Creado por: Alejandro Gonzalez Castro
//      Modificado por:alejandro Gonzalez Castro
//*************************************************************************************************
// CCN - 4360175 - 03/08/2004 - Se ajusta size ventana de impresion
// CCN - 4360189 - 03/09/2004 - Se redirecciona a Final.jsp por Control de Efectivo
// CCN - 4360275 - 18/02/2005 - WAS 5.1
//*************************************************************************************************
-->

<%@ page session="true" import="java.util.*"%>
<%
response.setHeader("Cache-Control","no-store"); //HTTP 1.1
response.setHeader("Pragma","no-cache"); //HTTP 1.0
response.setDateHeader ("Expires", 0); //prevents caching at the proxy server*/
%>
<%!
   
   private String addpunto(String valor)
   {
      int longini = valor.length();
      String cents  = valor.substring(longini-2,longini);
      String entero = valor.substring(0,longini-2);
      int longente = entero.length();
      for (int i = 0; i < (longente-(1+i))/3; i++)
      {
         longente = entero.length();
         entero = entero.substring(0,longente-(4*i+3))+','+entero.substring(longente-(4*i+3));
      }
      entero = entero + '.' + cents;
      return entero;
   }

  private String getString(String s)
  {
    int len = s.length();
    for(; len>0; len--)
	  if(s.charAt(len-1) != ' ')
	    break;

    return s.substring(0, len);
  }

  private String getItem(String newCadNum, int newOption)
  {
   int iPIndex = newCadNum.indexOf("CONSECUTIVO:") + 12, NumSaltos, Num = 0, Ban = 0;
   String nCadNum = new String("");
   NumSaltos = newOption;
   while(Num < NumSaltos)
   {
    if(newCadNum.charAt(iPIndex) == 0x20)
    {
     while(newCadNum.charAt(iPIndex) == 0x20)
      iPIndex++;
     Num++;
    }
    else
     while(newCadNum.charAt(iPIndex) != 0x20)
      iPIndex++;
   }
   while(newCadNum.charAt(iPIndex) != 0x20 && iPIndex < newCadNum.length()){
    nCadNum = nCadNum + newCadNum.charAt(iPIndex);
    if(iPIndex < newCadNum.length())
      iPIndex++;
    if(iPIndex == newCadNum.length())
      break;
   }

   return nCadNum;
  }

  private String delLeadingfZeroes(String newString)
  {
   StringBuffer newsb = new StringBuffer(newString);
     while(newsb.length()>0)
       {
		if(newsb.charAt(0) == '0')
	       newsb.deleteCharAt(0);
	     else
	       break;  
	  };
	if(newsb.length()==0)
      newsb.append("000") ;
   return newsb.toString();
  }

%>
<% Vector resp = (Vector)session.getAttribute("response");
Hashtable datasession = (Hashtable)session.getAttribute("page.datasession");%>
<html>
<head>
  <title>Certificación</title>
<style type="text/css">Body {font-family: Arial, Helv; font-size: 10pt; font-weight: bold; color:#000000}</style>
</head>
<script language="JavaScript">

function aceptar(forma)
{
  if ( document.Txns.retiro.value == '0' ) {
      alert("No ha seleccionado el retiro,\n por favor verifique.");
       return;
  }
	forma.submit();
   var obj = document.getElementById('btnCont')
	obj.onclick = new Function('return false')
}
function obtienedato(valor)
{
	document.Txns.retiro.value = valor;
}
function cancelar(forma)
{
  document.Txns.action = "../paginas/Final.jsp";
  document.Txns.submit();
}
</script>
<body>
<form name="Txns" action="../../servlet/ventanilla.PageServlet" method="post">
 <input type="hidden" name="txtInvoqued" value="somedatahere">
<table border="0" cellspacing="0">
<tr>
<td>
  <input type="hidden" name="transaction" value="<%= session.getAttribute("page.iTxn") %>">
  <input type="hidden" name="transaction1" value="<%= session.getAttribute("page.oTxn") %>">
</td>
</tr>
</table>
<p>

<%
  if( resp.size() != 4 )
    out.println("<b>Error de conexi&oacute;n</b><br>");
   else
   {
    try{
     String codigo = (String)resp.elementAt(0);
     if( !codigo.equals("0") )
        out.println("<b>Error en transaccion</b><br>");
     int lines = Integer.parseInt((String)resp.elementAt(1));
     Vector lineas = new Vector();
     String szResp = (String)resp.elementAt(3);
     for(int i=0; i<lines; i++){
        String line = szResp.substring( i*78, Math.min(i*78+77, szResp.length()) );
        if( !codigo.equals("0") )
           out.println(getString(line) + "<p>");
          lineas.addElement(line);
        if(Math.min(i*78+77, szResp.length()) == szResp.length())
           break;
     }

     lines = lineas.size();
     String linearesp = "";
     if (codigo.equals("0")) {
        int numeroretiros = 0;
        linearesp = (String)lineas.elementAt(3);
        String txtAfore = linearesp.substring(0,3);
        String txCURP = linearesp.substring(3,21);
        linearesp = (String)lineas.elementAt(4);
        String txtISR = delLeadingfZeroes(linearesp.substring(0,13));
        String txtLeyenda = linearesp.substring(13,38);
        String txtLeyAport = linearesp.substring(38,linearesp.length());
        linearesp = (String)lineas.elementAt(5);
        String txtTitular = getString(linearesp.substring(0,40).replace('$',' '));
        linearesp = (String)lineas.elementAt(6);
        String txtBeneficiari1 = getString(linearesp.substring(0,linearesp.length()));
        linearesp = (String)lineas.elementAt(7);
        String txtBeneficiari2 = getString(linearesp.substring(0,linearesp.length()));
        linearesp = (String)lineas.elementAt(8);
        String txtBeneficiari3 = getString(linearesp.substring(0,linearesp.length()));
        linearesp = (String)lineas.elementAt(9);
        String txtFolio1 = linearesp.substring(0,5);
        String txtFecha1 = linearesp.substring(5,13);
        String txtValorTitulo1 = delLeadingfZeroes(linearesp.substring(13,22));
        String txtTitulos1 = delLeadingfZeroes(linearesp.substring(22,33));
        linearesp = (String)lineas.elementAt(10);
        String txtMonto01 = delLeadingfZeroes(linearesp.substring(0,13));
        String txtMonto11 = delLeadingfZeroes(linearesp.substring(13,26));
        String txtMonto1 = delLeadingfZeroes(linearesp.substring(26,39));
        linearesp = (String)lineas.elementAt(11);
        String txtMonCar01 = linearesp.substring(0,4);
        String txtMonCar11 = linearesp.substring(4,8);
        numeroretiros++;
        String txtFolio2 = "";
        String txtFecha2 = "";
        String txtValorTitulo2 = "";
        String txtTitulos2 = "";
        String txtMonto02 = "";
        String txtMonto12 = "";
        String txtMonto2 = "";
        String txtMonCar02 = "";
        String txtMonCar12 = "";
        String txtFolio3 = "";
        String txtFecha3 = "";
        String txtValorTitulo3 = "";
        String txtTitulos3 = "";
        String txtMonto03 = "";
        String txtMonto13 = "";
        String txtMonto3 = "";
        String txtMonCar03 = "";
        String txtMonCar13 = "";
        if ( lineas.size() > 13 ) {
           linearesp = (String)lineas.elementAt(12);
           txtFolio2 = linearesp.substring(0,5);
           txtFecha2 = linearesp.substring(5,13);
           txtValorTitulo2 = linearesp.substring(13,22);
           txtTitulos2 = linearesp.substring(22,33);
           linearesp = (String)lineas.elementAt(13);
           txtMonto02 = delLeadingfZeroes(linearesp.substring(0,13));
           txtMonto12 = delLeadingfZeroes(linearesp.substring(13,26));
           txtMonto2 = delLeadingfZeroes(linearesp.substring(26,39));
           linearesp = (String)lineas.elementAt(14);
           txtMonCar02 = linearesp.substring(0,4);
           txtMonCar12 = linearesp.substring(4,8);
           numeroretiros++;
           if ( lineas.size() > 15 ) {
	            linearesp = (String)lineas.elementAt(15);
              txtFolio3 = linearesp.substring(0,5);
              txtFecha3 = linearesp.substring(5,13);
              txtValorTitulo3 = linearesp.substring(13,22);
              txtTitulos3 = linearesp.substring(22,33);
              linearesp = (String)lineas.elementAt(16);
              txtMonto03 = delLeadingfZeroes(linearesp.substring(0,13));
              txtMonto13 = delLeadingfZeroes(linearesp.substring(13,26));
              txtMonto3 = delLeadingfZeroes(linearesp.substring(26,39));
              linearesp = (String)lineas.elementAt(17);
              txtMonCar03 = linearesp.substring(0,4);
              txtMonCar13 = linearesp.substring(4,8);
              numeroretiros++;
          }
        }
        datasession.put("txtAfore",txtAfore);
        datasession.put("txtCURP",txCURP);
        datasession.put("txtISR",txtISR);
        datasession.put("txtLeyenda",txtLeyenda);
        datasession.put("txtLeyAport",txtLeyAport);
        datasession.put("txtTitular",txtTitular);
        datasession.put("txtBeneficiari1",txtBeneficiari1);
        datasession.put("txtBeneficiari2",txtBeneficiari2);
        datasession.put("txtBeneficiari3",txtBeneficiari3);
        datasession.put("txtFolio1",txtFolio1);
        datasession.put("txtFecha1",txtFecha1);
        datasession.put("txtValorTitulo1",txtValorTitulo1);
        datasession.put("txtTitulos1",txtTitulos1);
        datasession.put("txtMonto01",txtMonto01);
        datasession.put("txtMonto11",txtMonto11);
        datasession.put("txtMonto1",txtMonto1);
        datasession.put("txtMonCar01",txtMonCar01);
        datasession.put("txtMonCar11",txtMonCar11);
        datasession.put("txtFolio2",txtFolio2);
        datasession.put("txtFecha2",txtFecha2);
        datasession.put("txtValorTitulo2",txtValorTitulo2);
        datasession.put("txtTitulos2",txtTitulos2);
        datasession.put("txtMonto02",txtMonto02);
        datasession.put("txtMonto12",txtMonto12);
        datasession.put("txtMonto2",txtMonto2);
        datasession.put("txtMonCar02",txtMonCar02);
        datasession.put("txtMonCar12",txtMonCar12);
        datasession.put("txtFolio3",txtFolio3);
        datasession.put("txtFecha3",txtFecha3);
        datasession.put("txtValorTitulo3",txtValorTitulo3);
        datasession.put("txtTitulos3",txtTitulos3);
        datasession.put("txtMonto03",txtMonto03);
        datasession.put("txtMonto13",txtMonto13);
        datasession.put("txtMonto3",txtMonto3);
        datasession.put("txtMonCar03",txtMonCar03);
        datasession.put("txtMonCar13",txtMonCar13);
        datasession.put("txtNumRetiros",Integer.toString(numeroretiros));
        session.setAttribute("page.datasession",datasession);
        out.println("<table>");
        out.println("<tr>");
        out.println("<td> Afiliacion : </td>");
        out.println("<td> &nbsp; </td>");
        out.println("<td> <strong>" + (String)datasession.get("txtAfiliacion") + "</strong> </td>");
        out.println("<td> &nbsp; </td>");
        out.println("</tr> <tr>");
        out.println("<td> Nombre: </td>");
        out.println("<td> &nbsp; </td>");
        out.println("<td> <strong> " + txtTitular + "</strong> </td>");
        out.println("<td> &nbsp; </td>");
        out.println("</tr>");
        out.println("<tr> <td> Tipo de Retiro: </td> </tr>");
        out.println("</table>");
        out.println("<table>");
        for (int i = 1; i <= numeroretiros; i++) {
           out.println("<tr>");
           out.print("<td ><input type=\"radio\" name=\"NumeroRet\" value=\"" + i +"\" onClick=\"if (this.checked) {obtienedato("+ i + ")}\" >" );
           out.println(datasession.get("lstTipRet").toString() + " " + txtLeyenda + "</td>");
           out.println("<td> &nbsp; </td>");
           out.println("<td> &nbsp; </td>");
           out.println("<td>" + addpunto(delLeadingfZeroes(datasession.get("txtMonto"+ i).toString())) + "</td>");
           out.println("</tr>");
        }
        out.println("</table>");
        out.println("<p>");
      }
     }
     catch(NumberFormatException nfe){
     out.println("<b>Error de conexi&oacute;n</b><br>");
    }
   }
%>
<form name="Txns" action="../../servlet/ventanilla.PageServlet" method="post">
<table border="0" cellspacing="0">
<tr>
<td>
</td>
</tr>
<tr>
<td>
<%
  String txtCadImpresion  = ""; // Para Certificación
  String codigo = (String)resp.elementAt(0);
  out.print("<div align=\"center\">");
  session.setAttribute("Consecutivo", getItem((String)resp.elementAt(3), 1)); // Para Certificación
  if(!codigo.equals("0"))
     		out.println("<script language=\"javascript\">top.openDialog('../../servlet/ventanilla.ImprimirServlet', 160, 120, 'top.setPrefs4()', document.Txns)</script>"); // Para Certificación
  else {
   	    out.println("<a id='btnCont' href=\"javascript:aceptar(document.Txns)\"><img src=\"../imagenes/b_continuar.gif\" border=\"0\"></a>");
        out.println("<a href=\"javascript:cancelar(document.Txns)\"><img src=\"../imagenes/b_cancelar.gif\" border=\"0\"></a>");
  }
   out.print("</div>");
%>
  <input type="hidden" name="transaction" value="<%= session.getAttribute("page.iTxn") %>">
  <input type="hidden" name="transaction1" value="<%= session.getAttribute("page.oTxn") %>">
  <input type="hidden" name="retiro" value="0">
</td>
</tr>
</table>
</form>
</body>
</html>
