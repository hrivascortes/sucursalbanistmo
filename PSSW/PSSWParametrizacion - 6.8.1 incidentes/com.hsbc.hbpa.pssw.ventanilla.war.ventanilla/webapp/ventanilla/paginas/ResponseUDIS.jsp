<!--
//*************************************************************************************************
//             Funcion: JSP que despliega respuesta de txn UDIS
//            Elemento: ResponseUDIS.jsp
//          Creado por: Alejandro Gonzalez Castro
//      Modificado por: Alejandro Gonzalez Castro
//*************************************************************************************************
// CCN - 4360175 - 03/08/2004 - Ajuste size de ventana de impresion
// CCN - 4360275 - 18/02/2005 - WAS 5.1
//*************************************************************************************************
-->
<%@ page session="true" import="java.util.*"%>
<%
response.setHeader("Cache-Control","no-store"); //HTTP 1.1
response.setHeader("Pragma","no-cache"); //HTTP 1.0
response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<%
%>
<%!
  private String getString(String s)
  {
    int len = s.length();
     for(; len>0; len--)
	  if(s.charAt(len-1) != ' ')
	    break;

    return s.substring(0, len);
  }

  private String getCantidad(String s)
   {
     int Indice = s.indexOf(".");
     String cadena = "";
     cadena = s.substring(Indice,Indice+3);
     int Indice2 = Indice;
     for(; Indice>0; Indice--)
	  if(s.charAt(Indice-1) == ' ')
	    break;

     cadena = s.substring(Indice,Indice2)+cadena;
     return cadena;
  }
private String getItem(String newCadNum, int newOption)
  {
   int iPIndex = newCadNum.indexOf("CONSECUTIVO:") + 12, NumSaltos, Num = 0, Ban = 0;
   String nCadNum = new String("");
   NumSaltos = newOption;
   while(Num < NumSaltos)
   {
    if(newCadNum.charAt(iPIndex) == 0x20)
    {
     while(newCadNum.charAt(iPIndex) == 0x20)
      iPIndex++;
     Num++;
    }
    else
     while(newCadNum.charAt(iPIndex) != 0x20)
      iPIndex++;
   }
   while(newCadNum.charAt(iPIndex) != 0x20 && iPIndex < newCadNum.length()){
    nCadNum = nCadNum + newCadNum.charAt(iPIndex);
    if(iPIndex < newCadNum.length())
      iPIndex++;
    if(iPIndex == newCadNum.length())
      break;
   }

   return nCadNum;
  }

  private String delCommaPointFromString(String newCadNum)
  {
   String nCad = new String(newCadNum);
   if( nCad.indexOf(".") > -1)
   {
    nCad = nCad.substring(nCad.indexOf(".") + 1, nCad.length());
    if(nCad.length() != 2)
    {
     String szTemp = new String("");
     for(int j = nCad.length(); j < 2; j++)
      szTemp = szTemp + "0";
     newCadNum = newCadNum + szTemp;
    }
   }

   StringBuffer nCadNum = new StringBuffer(newCadNum);
   for(int i = 0; i < nCadNum.length(); i++)
    if(nCadNum.charAt(i) == ',' || nCadNum.charAt(i) == '.')
     nCadNum.deleteCharAt(i);

   return nCadNum.toString();
  }

  // 2004-03-31 ARM: Cambio notificado por cmtH (Se agrego la funcion 'delCommaFromString')
  private String delCommaFromString(String newCadNum)
  {
   String nCad = new String(newCadNum);
   if( nCad.indexOf(".") > -1)
   {
    nCad = nCad.substring(nCad.indexOf(".") + 1, nCad.length());
    if(nCad.length() != 2)
    {
     String szTemp = new String("");
     for(int j = nCad.length(); j < 2; j++)
      szTemp = szTemp + "0";
     newCadNum = newCadNum + szTemp;
    }
   }

   StringBuffer nCadNum = new StringBuffer(newCadNum);
   for(int i = 0; i < nCadNum.length(); i++)
    if(nCadNum.charAt(i) == ',')
     nCadNum.deleteCharAt(i);

   return nCadNum.toString();
  }


  private String setPointToString(String newCadNum)
  {
   int iPIndex = newCadNum.indexOf("."), iLong;
   String szTemp;

   if(iPIndex > 0)
   {
    newCadNum = new String(newCadNum + "00");
    newCadNum = newCadNum.substring(0, iPIndex + 1) + newCadNum.substring(iPIndex + 1, iPIndex + 3);
   }
   else
   {
    for(int i = newCadNum.length(); i < 3; i++)
     newCadNum = "0" + newCadNum;
    iLong = newCadNum.length();
    if(iLong == 3)
     szTemp = newCadNum.substring(0, 1);
    else
     szTemp = newCadNum.substring(0, iLong - 2);
    newCadNum = szTemp + "." + newCadNum.substring(iLong - 2);
   }

   return newCadNum;
  }

  private String setFormat(String newCadNum)
  {
    int iPIndex = newCadNum.indexOf(".");
    String nCadNum = new String(newCadNum.substring(0, iPIndex));
    for (int i = 0; i < (int)((nCadNum.length()-(1+i))/3); i++)
     nCadNum = nCadNum.substring(0, nCadNum.length()-(4*i+3))+','+nCadNum.substring(nCadNum.length()-(4*i+3));

    return nCadNum + newCadNum.substring(iPIndex, newCadNum.length());
  }

%>
<% Vector resp = (Vector)session.getAttribute("response");
Hashtable datasession = (Hashtable)session.getAttribute("page.datasession");
String cTxn = (String)session.getAttribute("page.cTxn");
%>
<html>
<head>
  <title>Certificación</title>
<style type="text/css">Body {font-family: Arial, Helv; font-size: 10pt; font-weight: bold; color:#000000}</style>
</head>
<script language="javascript">
</script>
<body>
<p>
<%
  java.util.Stack flujotxn = new java.util.Stack();
  String codigo = "";
  String needOverride = "NO";
 int lines;
Vector lineas = new Vector();
  if(session.getAttribute("page.flujotxn") != null)
  	flujotxn = (java.util.Stack)session.getAttribute("page.flujotxn");
   if( resp.size() != 4 )
    out.println("<b>Error de conexi&oacute;n 1 </b><br>");
   else
   {
    try{
     codigo = (String)resp.elementAt(0);
     if( codigo.equals("1"))
        out.println("<b>Transaccion Rechazada</b><br><br>");
     if( codigo.equals("0") || codigo.equals("1") ) {
        lines = Integer.parseInt((String)resp.elementAt(1));
        String szResp = (String)resp.elementAt(3);
        for(int i=0; i<lines; i++){
           String line = szResp.substring( i*78, Math.min(i*78+77, szResp.length()) );
           if ( !codigo.equals("0") || cTxn.equals("0067"))
              out.println(getString(line) + "<p>");
           lineas.addElement(getString(line));
           if(Math.min(i*78+77, szResp.length()) == szResp.length())
              break;
        }
     }
     else {
         if ( codigo.equals("3") || codigo.equals("2"))
            out.println("<b>Transacci&oacute;n Requiere Autorizaci&oacute;n</b>");
         else
            out.println("<b>Ocurri&oacute; un Error de Comunicaci&oacute;n, Verifique.</b>");
     }
     lines = lineas.size();
     String linearesp = "";
     if (codigo.equals("0") /*&& cTxn.equals("0812") */) {
        linearesp = (String)lineas.elementAt(0);
        // 2004-03-31 ARM: Cambio notificado por cmtH (Las 10 lineas siguientes fueron sustituidas por las 11 siguientes)
        /*
        //String ValUDIS = linearesp.substring(linearesp.length()-9);
        String ValUDIS = delCommaPointFromString((String)datasession.get("txtUDI"));
       String Monto = delCommaPointFromString((String)datasession.get("txtMonto"));
        long lMonto = Long.parseLong(Monto);
       long MtoUDI = Long.parseLong(ValUDIS);
        Long MontoMN = new Long(0);
         // MontoMN = new Long(lMonto * Long.parseLong(ValUDIS));
        MontoMN = new Long(lMonto * MtoUDI);

       Monto = setFormat(setPointToString(delCommaPointFromString(MontoMN.toString())));
       */
       
	// 2004-03-31 ARM: Cambio notificado por cmtH (Las 11 lineas siguiente sustituyen a las 10 anteriores)
        String ValUDIS = delCommaFromString((String)datasession.get("txtUDI"));
        String Monto = delCommaFromString((String)datasession.get("txtMonto"));
        double lMonto = Double.parseDouble(Monto);
        double MtoUDI = Double.parseDouble(ValUDIS);
        Double MontoMN = new Double(0);
        MontoMN = new Double(lMonto * MtoUDI);
        Monto = MontoMN.toString();
        int point = Monto.indexOf(".");
        point = point + 3;
        Monto = Monto.substring(0,point);
        Monto = setFormat(setPointToString(delCommaPointFromString(Monto)));
       
        datasession.put("txtMonto1",Monto);
        out.println("<table border=\"0\">");
        out.println(" <tr>");
        out.print("  <td><b>Valor UDI:</b></td>" + "<td width=\"10\">&nbsp;</td><td>"  + ValUDIS + "</td><td width=\"10\">&nbsp;</td>");
        out.println(" </tr>");
        out.println(" <tr>");
        out.println("<td><b>Monto en M.N.:</b></td>" + "<td width=\"10\">&nbsp;</td><td>"  + Monto + "</td>");
        out.println(" </tr>");
        out.println(" <tr>");
        
        // 2004-03-31 ARM: Cambio notificado por cmtH (Se sustitiyo la linea comentada por la siguiente)
        //out.println("<td><b>Cuenta de Abono:</b></td>" + "<td width=\"10\">&nbsp;</td><td>"  + (String)datasession.get("txtDDDACuenta") + "</td>");
        out.println("<td><b>Cuenta de Abono:</b></td>" + "<td width=\"10\">&nbsp;</td><td>"  + (String)datasession.get("txtDDACuenta") + "</td>");
        
        out.println(" </tr>");
        out.println(" </table>");
        }
    }

    catch(NumberFormatException nfe){
   System.out.println("NFE " + nfe);
     out.println("<b>Error de conexi&oacute;n 2 </b><br>");
    }
   }
%>
<form name="Txns" action="../../servlet/ventanilla.PageServlet" method="post">
<table border="0" cellspacing="0">
<tr>
<td>
</td>
</tr>
<tr>
<td>
<%
  String isCertificable = "";
  if(session.getAttribute("page.certifField") != null)
    isCertificable = (String)session.getAttribute("page.certifField");  // Para Certificación
  String txtCadImpresion  = ""; // Para Certificación
  if(!flujotxn.empty())
  {
    out.print("<div align=\"center\">");
    if(!isCertificable.equals("N")) // Para Certificación
    {
    	session.setAttribute("Consecutivo", getItem((String)resp.elementAt(3), 1)); // Para Certificación
    	session.setAttribute("Tipo", isCertificable); // Para Certificación
    	if(codigo.equals("0"))
      		out.println("<script language=\"javascript\">top.openDialog('../../servlet/ventanilla.ImprimirServlet?txtCadImpresion=" + txtCadImpresion + "', 160, 120, 'top.setPrefs4()', document.Txns)</script>"); // Para Certificación
    	else {
           if ( codigo.equals("3") || codigo.equals("2")) {
               flujotxn.push(session.getAttribute("page.cTxn"));
               session.setAttribute("page.flujotxn",flujotxn);
               needOverride = "SI";
               out.print("<script languaje=\"javascript\"> top.openDialog('authoriz.jsp', 220, 130, 'top.setPrefs8()', document.Txns) </script>");
           }
           else
               out.println("<a href=\"javascript:top.openDialog('../../servlet/ventanilla.ImprimirServlet?txtCadImpresion=" + txtCadImpresion + "', 160, 120, 'top.setPrefs4()', document.Txns)\"><img src=\"imagenes/b_continuar.gif\" border=\"0\"></a>"); // Para Certificación
        }
     }
     else
	{
    	  if(codigo.equals("0"))
	    {
     	      out.println("<script language=\"javascript\">sharedData = '1'</script>");
    	    }
    	  else {
             if ( codigo.equals("3") || codigo.equals("2")) {
                flujotxn.push(session.getAttribute("page.cTxn"));
                session.setAttribute("page.flujotxn",flujotxn);
                needOverride = "SI";
                out.print("<script languaje=\"javascript\"> top.openDialog('authoriz.jsp', 220, 130, 'top.setPrefs8()', document.Txns) </script>");
             }
             else
      	        out.print("<input type=\"image\" src=\"../imagenes/b_aceptar.gif\" alt=\"\" border=\"0\">");
          }
   	}
   	out.print("</div>");
  }
  else
  {
    if(!isCertificable.equals("N")) // Para Certificación
    {
      session.setAttribute("Consecutivo", getItem((String)resp.elementAt(3), 1)); // Para Certificación
      session.setAttribute("Tipo", isCertificable); // Para Certificación
      if ( codigo.equals("3") || codigo.equals("2")) {
           flujotxn.push(session.getAttribute("page.cTxn"));
           session.setAttribute("page.flujotxn",flujotxn);
           needOverride = "SI";
           out.print("<script languaje=\"javascript\"> top.openDialog('authoriz.jsp', 220, 130, 'top.setPrefs8()', document.Txns) </script>");
         }
      else
   	   out.println("<script language=\"javascript\">top.openDialog('../../servlet/ventanilla.ImprimirServlet', 160, 120, 'top.setPrefs4()', document.Txns)</script>"); // Para Certificación
      }
  }
%>
  <input type="hidden" name="transaction" value="<%= session.getAttribute("page.iTxn") %>">
  <input type="hidden" name="transaction1" value="<%= session.getAttribute("page.oTxn") %>">
  <input type="hidden" name="override" value="<%= needOverride%>">
  <input type="hidden" name="supervisor" value="<%= session.getAttribute("supervisor") %>">
</td>
</tr>
</table>
</form>
</body>
</html>
