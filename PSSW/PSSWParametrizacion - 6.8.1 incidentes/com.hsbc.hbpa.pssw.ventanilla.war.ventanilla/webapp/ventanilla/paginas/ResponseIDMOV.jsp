<!--
//*************************************************************************************************
//             Funcion: JSP que despliega respuesta de txn Vouchers
//            Elemento: ResponseVouchers1.jsp
//          Creado por: Alejandro Gonzalez Castro
// Ultima Modificacion: 03/08/2004
//      Modificado por: Alejandro Gonzalez Castro
//*************************************************************************************************
// CCN - 4360175 - 03/08/2004 - Ajuste size de ventana de impresion
//*************************************************************************************************
-->
<%@ page session="true" import="java.util.*"%>
<%
response.setHeader("Cache-Control","no-store"); //HTTP 1.1
response.setHeader("Pragma","no-cache"); //HTTP 1.0
response.setDateHeader ("Expires", 0); //prevents caching at the proxy server

 Hashtable datasession = (Hashtable)session.getAttribute("page.datasession");
 Hashtable movimientos = (Hashtable)session.getAttribute("page.movimientos");
   
   String renglon      = (String) session.getAttribute("page.renglon");
   String cta_xml      = (String) session.getAttribute("page.cta_xml");
   String sucursal_xml = (String) session.getAttribute("page.sucursal_xml");		   
   String nombre_xml   = (String) session.getAttribute("page.nombre_xml");
   String error_perfil = (String) session.getAttribute("page.error_perfil");
   String error_mov    = (String) session.getAttribute("page.error_mov");
   
   String movi  = (String)request.getParameter("movi");	
   
   //System.out.println("movimientos:" + movi);	
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
<title>Results</title>
<style type="text/css">Body {font-family: Arial, Helv; font-size: 10pt; font-weight: bold; color:#000000}</style>
</head>
<body>
Proceso de Impresion de Libreta de Ahorro...
<form name="ImprimeLibretaA">
<%

	String printcad = "LAHORRO~";
	String data="";  
	
	session.setAttribute("page.getoTxnView","Response"); 
		
	String fecha_oper =" ";
    String num_cheq   =" ";
    String tran_id    =" ";    
    String monto      =" ";
    String tipTran    =" ";
    String saldo      =" ";
    String rastrId    =" ";
    String descripcion=" ";	
    
    for(int i=Integer.parseInt(movi); i<=movimientos.size(); i++)  
   
   	{     
   	   data = (String)movimientos.get("key"+i); 
	  ///* System.out.println(data);	
	  // System.out.println(i);	
	   	   
	   StringTokenizer tok = new StringTokenizer(data,"&");             
	   while(tok.hasMoreTokens()) 
	   { 
	    fecha_oper = tok.nextToken();     
	    	if(fecha_oper.length()==0) fecha_oper = ". ";
		num_cheq   = tok.nextToken();
			if(num_cheq.length()==0) num_cheq = ". ";	
		tran_id    = tok.nextToken();
			if(tran_id.length()==0) tran_id = ". ";		
		monto      = tok.nextToken();
			if(monto.length()==0) monto = ". ";
		tipTran    = tok.nextToken();
			if(tipTran.length()==0) tipTran = ". ";
		saldo      = tok.nextToken();		
			if(saldo.length()==0) saldo = ". ";
		rastrId    = tok.nextToken();
			if(rastrId.length()==0) rastrId = ". ";
		descripcion = tok.nextToken();	
			if(descripcion.length()==0) descripcion = ". ";
		
		printcad = printcad + renglon    + "~" + nombre_xml + "~" + cta_xml + "~" + sucursal_xml +"~";
        printcad = printcad + fecha_oper + "~" + num_cheq   + "~" + tran_id +"~";
        printcad = printcad + monto      + "~" + tipTran    + "~" + saldo   +"~";
        printcad = printcad + rastrId    + "~" + descripcion+"~";
        
        session.setAttribute("txtCadImpresion", printcad);
	    
	    }
	    
	  }
	
	int lineaImpDisp = (25- Integer.parseInt(renglon)) +1;	
	//System.out.println("linea Dips:"+lineaImpDisp);
	//System.out.println(printcad);    
	out.println("<a href=\"javascript:top.openDialog('../../servlet/ventanilla.ImprimirServlet', 160, 120, 'top.setPrefs4()', document.ImprimeLibretaA); \"><img src=\"../imagenes/b_continuar.gif\" border=\"0\"></a>");	
	
%>
</form>
</body>
</html>
