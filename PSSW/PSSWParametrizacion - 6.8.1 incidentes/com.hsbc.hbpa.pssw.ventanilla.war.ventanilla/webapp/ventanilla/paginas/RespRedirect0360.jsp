<!--
//*************************************************************************************************
//             Funcion: JSP para redireccion de txn 0360
//            Elemento: ResponseRedirect0360.jsp
//      Modificado por: Juvenal R. Fernandez V.
//*************************************************************************************************
// CCN - 4360334 - 17/06/2005 - Se modifica redireccion a relativa para WAS5
//*************************************************************************************************
-->

<%@page session="true" import="java.util.Hashtable"%>
<%
response.setHeader("Cache-Control","no-store"); //HTTP 1.1
response.setHeader("Pragma","no-cache"); //HTTP 1.0
response.setDateHeader ("Expires", 0); //prevents caching at the proxy server*/
%>
<%
  String proceso = (String)session.getAttribute("txnProcess");
  Hashtable datasession = (Hashtable)session.getAttribute("page.datasession");  
  String iTxn = (String)datasession.get("iTxn");
  if( proceso.equals("13") )
  {
  	if (iTxn.equals("0728"))
	{    response.sendRedirect("Response0728.jsp");}
	if (iTxn.equals("0730"))
	{    response.sendRedirect("Response0730.jsp");}	
	if (iTxn.equals("0732"))
	{    response.sendRedirect("Response0732.jsp");}	
  }
%>
