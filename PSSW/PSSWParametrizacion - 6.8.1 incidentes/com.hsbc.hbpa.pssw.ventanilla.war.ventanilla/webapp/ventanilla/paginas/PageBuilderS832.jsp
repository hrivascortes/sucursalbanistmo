<!-- 
//*************************************************************************************************
//             Funcion: JSP que realiza la presentacion de los datos de la txn 832
//            Elemento: PageBuilderS832.jsp
//          Creado por: Alejandro Gonzalez Castro
// Ultima Modificacion: 31/05/2004
//      Modificado por: Rutilo Zarco Reyes
//*************************************************************************************************
// CCN - 4360171 - 23/07/2004 - Se agrega seleccion de 13- 13A
// CCN - 4360237 - 05/11/2004 - SAT v3.0
// CCN - 4360275 - 18/02/2005 - WAS 5.1
// CCN - 4360407 - 15/12/2005 - Nueva version SAT 4.0
// CCN - 4360581 - 12/03/2007 - Se convierten los estilos en jsf y se hace un include en el jsp
//*************************************************************************************************
-->

<%@ page session="true" import="java.util.*"%>
<%
response.setHeader("Cache-Control","no-store"); //HTTP 1.1
response.setHeader("Pragma","no-cache"); //HTTP 1.0
response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<%
String MontoPro = new String("");
String txnAuthLevel = (String)session.getAttribute("page.txnAuthLevel");
String txnIMGAutoriz = (String)session.getAttribute("page.txnIMGAutoriz");
Vector v = (Vector)session.getAttribute("page.specialacct");
String noservicios =  (String)session.getAttribute("page.noservicios");
int nserv = Integer.parseInt(noservicios);
int k;
java.util.Hashtable cadenaoriginal = (java.util.Hashtable)session.getAttribute("cadena.original");
Hashtable datasession = (Hashtable)session.getAttribute("page.datasession");
%>
<html>
<head>
<title>Datos de la Transaccion</title>
<%@include file="style.jsf"%>
<script language="JavaScript">
<!--
var formName = 'Mio'
var specialacct = new Array()
<%
if(!v.isEmpty())
{
  for(int iv = 0; iv < v.size(); iv++){out.print("specialacct[" + iv + "]='" + v.get(iv) + "'" + '\n');}
}%>

function validate(form)
{
  form.validateFLD.value = '1'
  if (document.entry.txtFechaEfect || document.entry.txtFechaError || document.entry.txtFechaCarta)
    document.entry.txtFechaSys.value=top.sysdatef();
<%if(((String)cadenaoriginal.get("40001")).equals("005"))
 {%>
  if (!confirm("El tipo de formulario " + entry.pagoA.options[entry.pagoA.selectedIndex].value + " es correcto."))
    return;
<%}%>
<% if(!txnAuthLevel.equals("1") && !txnIMGAutoriz.equals("1"))
{
    out.println("  form.submit();");
    out.println("var obj = document.getElementById('continu');"); 
    out.println("obj.onclick = new Function('return false');");   
}   
%>
}
function AuthoandSignature(entry)
{
 var Cuenta;
 if(entry.validateFLD.value != '1')
  return
 if(entry.needAutho.value == '1' && entry.AuthoOK.value != '1'){
  top.openDialog('../ventanilla/paginas/authoriz.jsp', 220, 130, 'top.setPrefs()', entry)
 }
 if(entry.needSignature.value == '1' && entry.SignField.value != ''){
  Cuenta = eval('entry.' + entry.SignField.value + '.value')
  top.openModalDialog('<%=request.getContextPath()%>/servlet/ventanilla.FirmasServlet?txtDDACuenta=' + Cuenta, 710, 430, 'top.setPrefs2()', entry)
 }
}
//-->
</script>
</head>
<body bgcolor="#FFFFFF" onload="top.setFieldFocus(window.document)">
<br>
<% out.print("<form name=\"entry\" action=\"../servlet/ventanilla.DataServlet\" method=\"post\" ");
   if(txnAuthLevel.equals("1"))
    out.print("onSubmit=\"return validate(this)\"");
   out.println(">");
%>

<h1><%=session.getAttribute("page.txnImage")%></h1>

<h3>
<%out.print(session.getAttribute("page.cTxn") + "  " +
                (String)session.getAttribute("page.txnlabel"));%>
</h3>

<table border="0">
<tr>
   <td colspan="2"><b>Nombre: </b></td>
   <td colspan="3" class="ta"><b><%=(String)datasession.get("txtnombre")%></b></td> 
</tr>
<TR>
  <TD colspan = "5">&nbsp;</td>
</TR>
<tr align="center">
<td>&nbsp;&nbsp;</td><td>&nbsp;&nbsp;</td>
<td><b>Servicio</b></td><td>&nbsp;&nbsp;</td>
<td><b>No. de Pagos</b></td><td>&nbsp;&nbsp;</td>
<td><b>Monto</b></td>
<%if(((String)cadenaoriginal.get("40001")).equals("005"))
 {%>
<td><b>Tipo de Formulario</b></td></tr>
<%}
int i = 0;
for (i = 1; i < nserv+1;i++)
{%>
<tr><td><%=i%>.-</td><td>&nbsp;&nbsp;</td>
<td><input type="text" tabindex="<%=i%>" name="servicio<%=i%>" size="7" maxlength="7" tabstop="false" contentEditable = "false" value="4376"></td>
<td>&nbsp;&nbsp;</td>
<td align="center"><input tabindex="<%=i%>" type="text" name="nopagos<%=i%>" size="2" maxlength="2" tabstop="false" contentEditable = "false" value="1"></td>
<td>&nbsp;&nbsp;</td>
<td><input type="text" tabindex="<%=i%>" name="monto<%=i%>" size="14" maxlength="11" tabstop="false" contentEditable = "false" value="<%=addpunto((String)cadenaoriginal.get("10017"))%>"></td>
<%if(((String)cadenaoriginal.get("40001")).equals("005"))
 {%>
  <td>
   <select name="pagoA" tabindex="<%=i%>">
     <option value="13">13
     <option value="13A">13A
   </select>
  </td>
 <%}%>
</tr>
<%}%>
<tr>
<td colspan="4"></td>
<td align="right"><b>Total :</b></td><td>&nbsp;&nbsp;</td>
<td><input tabindex="<%=i%>" type="text" name="txtTot" size="16" maxlength="13" onChange="top.validate(window, this, 'notChange')" tabstop="false" contentEditable = "false" value="<%=addpunto((String)cadenaoriginal.get("10017"))%>"></td>
</tr>
</table>
<%
for (int z = 1; z < nserv+1;z++)
{%>
	<input type="hidden" name="s<%=z%>" value="X">
<%}%>
<input type="hidden" name="nservicios" value="<%=session.getAttribute("page.noservicios")%>">
<input type="hidden" name="tot" value="0">
<input type="hidden" name="np" value="0">
<input type="hidden" name="flujorap" value="1">
<input type="hidden" name="noefec" value="0">
<input type="hidden" name="ser60" value="0">
<input type="hidden" name="ser670" value="0">
<input type="hidden" name="iTxn" value="<%= session.getAttribute("page.iTxn")%>">
<input type="hidden" name="oTxn" value="<%= session.getAttribute("page.oTxn")%>">
<input type="hidden" name="cTxn" value="<%= session.getAttribute("page.cTxn")%>">
<input type="hidden" name="sucursal" value="<%= session.getAttribute("branch")%>">
<input type="hidden" name="teller" value="<%= session.getAttribute("teller")%>">
<input type="hidden" name="moneda" value="<%= session.getAttribute("page.moneda")%>">
<input type="hidden" name="registro" value="<%=session.getAttribute("empno")%>">
<input type="hidden" name="tllrlvl" value="<%=session.getAttribute("tellerlevel")%>">
<input type="hidden" name="supervisor" value="<%= session.getAttribute("teller") %>">
<input type="hidden" name="AuthoOK" value="0">
<input type="hidden" name="SignOK" value="0">
<input type="hidden" name="validateFLD" value="0">
<input type="hidden" name="needSignature" value="<%out.print(txnIMGAutoriz);%>">
<input type="hidden" name="needAutho" value="<%out.print(txnAuthLevel);%>">
<input type="hidden" name="SignField" value="<%= session.getAttribute("page.SignField") %>">
<p>


<% if(txnAuthLevel.equals("1") || txnIMGAutoriz.equals("1"))
    out.println("<a id='continu' tabindex='"+(i+1)+"' href=\"javascript:validate(document.entry);javascript:AuthoandSignature(document.entry)\" ><img src=\"../ventanilla/imagenes/b_aceptar.gif\" border=\"0\"></a>");
   else
    out.println("<a id='continu' tabindex='"+(i+1)+"' href=\"javascript:validate(document.entry)\"><img src=\"../ventanilla/imagenes/b_aceptar.gif\" border=\"0\"></a>");
%>

<script language="javascript">
	document.all['continu'].focus();
</script>

</form>
</body>
</html>
<%!
  private String addpunto(String valor)
  {
        valor = valor + "00";
	int longini = valor.length();
    String cents  = valor.substring(longini-2,longini);
	String entero = valor.substring(0,longini-2);
	int longente = entero.length();
	for (int i = 0; i < (longente-(1+i))/3; i++)
	{
		longente = entero.length();
	    entero = entero.substring(0,longente-(4*i+3))+','+entero.substring(longente-(4*i+3));
	}
  	entero = entero + '.' + cents;
	return entero;
  }
%>
