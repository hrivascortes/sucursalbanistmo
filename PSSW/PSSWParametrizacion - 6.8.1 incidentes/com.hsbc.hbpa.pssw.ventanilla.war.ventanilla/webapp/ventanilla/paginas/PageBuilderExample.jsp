<!--
//*************************************************************************************************
//             Funcion: JSPgenerico para txns varias
//            Elemento: PageBuilderExample.jsp
//          Creado por: Alejandro Gonzalez Castro
//      Modificado por: Fausto Rodrigo Flores Moreno
//*************************************************************************************************
// CCN - 4360178 - 20/08/2004 - Se evita autosumbmit para txn 9815 de inversiones
// CCN - 4360275 - 18/02/2005 - WAS 5.1
// CCN - 4360334 - 17/06/2005 - Se convierte fieldLector a fragmento
// CCN - 4360581 - 12/03/2007 - Se convierten los estilos en jsf y se hace un include en el jsp
// CCN - 4360584 - 23/03/2007 - Modificaciones para proyecto OPMN
// CCN - 4360593 - 20/04/2007 - Se agrega flujo para corrección de ivas duplicados
// CCN - 4620021 - 05/06/2015 - ALEX ORIA - Se agregan funcion para eliminar la solicitud de autorizacion para las transacciones 1001,1003,ARP Y P001
//*************************************************************************************************
-->

<%@ page session="true" import="java.util.*"%>
<%
response.setHeader("Cache-Control","no-store"); //HTTP 1.1
response.setHeader("Pragma","no-cache"); //HTTP 1.0
response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<%//Asignar valor de idflujo para flujos internos
   String txn = session.getAttribute("page.cTxn").toString();
   if(txn.equals("0061")){session.setAttribute("idFlujo","02");}
   if(txn.equals("4155")){session.setAttribute("idFlujo","01");}

Vector listaCampos = (Vector)session.getAttribute("page.listcampos");
Vector listaContenidos = (Vector)session.getAttribute("page.listcont");
String txnAuthLevel = (String)session.getAttribute("page.txnAuthLevel");
String txnIMGAutoriz = (String)session.getAttribute("page.txnIMGAutoriz");
Hashtable datasession = (Hashtable)session.getAttribute("page.datasession");
if(datasession == null)
  datasession = new Hashtable();
int k;
String MontoPro = new String("");
String redirect = "";
String compania = "20";
if ( datasession.get("compania") != null) {
  compania = datasession.get("compania").toString();
}

%>
<html>
<head>
  <%@include file="style.jsf"%>
  <title>Datos de la Transaccion</title>
<script  language="JavaScript">
<!--
var formName = 'Mio'
var isok = 0;


function validate(form)
{

<%
  for(int i=0; i < listaCampos.size(); i++)
  {
    Vector vCampos = (Vector)listaCampos.get(i);
    vCampos = (Vector)vCampos.get(0);

    if( vCampos.get(0).toString().equals("txtCveRFC") )
    {
      out.println("if( form.txtCveRFC.value.length > 0 )");
      out.println("  form.lstCF.value = 'S'");
    }

    if( vCampos.get(4).toString().length() > 0 && datasession.get(vCampos.get(0)) == null)
    {
      out.println("  if( !top.validate(window, document.entry." + vCampos.get(0) + ", '" + vCampos.get(4) +"') )");
      out.println("    return");
    }
    
    if(datasession.get(vCampos.get(0)) != null)
      redirect = "S";
  }
%>
  isok = 1;
  form.validateFLD.value = '1'
  if (document.entry.txtFechaEfect || document.entry.txtFechaError || document.entry.txtFechaCarta)
    document.entry.txtFechaSys.value=top.sysdatef();

      if (form.dvCheque != null && form.dvCheque.value != '' && form.txtSerial2 != null && form.txtSerial2.value.length > 0) {
		 form.noCheque.value = (form.txtSerial2.value).substr(0,(form.txtSerial2.value).length - 1);
		 form.txtSerial2.value = form.noCheque.value;
	  }
	  if (form.dvCuenta != null && form.dvCuenta.value != '' && form.txtDDACuenta2 != null && form.txtDDACuenta2.value.length > 0){
		 form.noCuenta.value = top.filler((form.txtDDACuenta2.value).substr(0,(form.txtDDACuenta2.value).length - 1), 11);
		 form.txtDDACuenta2.value = form.noCuenta.value;
	  }
    
   if(isTxnWithoutValidateXMonto()) { 
      <% 
       out.println("  form.submit();");
       out.println("var obj = document.getElementById('btnCont');"); 
       out.println("obj.onclick = new Function('return false');");  
       %>
    }else{

		<% if( ((!txnAuthLevel.equals("1")) && (!txnIMGAutoriz.equals("1"))))  
			{
		    out.println("  form.submit();");
		    out.println("var obj = document.getElementById('btnCont');"); 
		    out.println("obj.onclick = new Function('return false');");   
			}   
		%>
     }		
}


function AuthoandSignature(entry)
{
  if (isok == 0)
    return
    
  isok = 0;
  var Cuenta;
  if(entry.validateFLD.value != '1')
    return
    
  if(entry.needAutho.value == '1' && entry.AuthoOK.value != '1')
  {
    top.openDialog('../ventanilla/paginas/authoriz.jsp', 220, 130, 'top.setPrefs()', entry)
  }

  if(entry.needSignature.value == '1' && entry.SignField.value != '')
  {
    Cuenta = eval('entry.' + entry.SignField.value + '.value')
    top.openModalDialog('<%=request.getContextPath()%>/servlet/ventanilla.FirmasServlet?txtDDACuenta=' + Cuenta, 710, 430, 'top.setPrefs2()', entry)
  }
}

function doNothing(){
	return true;
}


function formSubmitter()
{
		
  if(document.forms[0].cTxn.value != '4155' && document.forms[0].cTxn.value != '0061' && document.forms[0].cTxn.value != '9809' && document.forms[0].cTxn.value != '9815' && document.forms[0].cTxn.value != '9711')
  {
    <%
      if(redirect.equals("S"))
      {%>
 	document.forms[0].submit();
    <%}%>
  }
  			
}

function isTxnWithoutValidateXMonto(){

	//Requerimiento 011 Se adiciona campo para validar TXN por Monto - Validacion TXN 1001 1003 P001 ARP 

	stTxnVar ='<%= (String)session.getAttribute("txnsWithOutValidateMonto")%>'
	stTxnVarForm ='<%= session.getAttribute("page.iTxn").toString()%>'
	stcTxnVarForm ='<%= session.getAttribute("page.cTxn").toString()%>'
   
   
    var dato = stTxnVar.split('*');
	
	for( pos = 0; pos < dato.length ; pos++){
		if( dato[pos] == stTxnVarForm) { //TXN 1001 1003 P001 ARP
			  	return true;
		 }
		  
	}
	 	return false;
}
//-->
</script>

</head>
<body onload="top.setFieldFocus(window.document);formSubmitter();">
<br>
<% out.print("<form name=\"entry\" action=\"../servlet/ventanilla.DataServlet\" method=\"post\" ");
   if(txnAuthLevel.equals("1"))
    out.print("onSubmit=\"return validate(this)\"");
   out.println(">");
%>

<h1><%=session.getAttribute("page.txnImage")%></h1>

<h3>
<%out.print(session.getAttribute("page.cTxn") + "  " +
                (String)session.getAttribute("page.txnlabel"));%>
</h3>
<table border="0" cellspacing="0">
<%
  for(int i =  0; i < listaCampos.size(); i++)
  {
    Vector vCampos = (Vector)listaCampos.get(i);
    vCampos = (Vector)vCampos.get(0);

    if( vCampos.get(0).toString().equals("txtCveRFC") )
    {
      if (session.getAttribute("page.cTxn").toString().equals("0061") && 
          session.getAttribute("page.iTxn").toString().equals("0061"))
      {%>
       <tr>
         <td>Requiere Comprobante Fiscal:</td>
         <td width="10">&nbsp;</td>
         <td><select name="ComproFiscal" size="1">
              <option value="0">NO
              <option value="1">SI
            </select></td>
       </tr>
    <%}
      out.println("<tr>");
      out.println("<td colspan=\"3\"><b>Ingrese los siguientes datos si desea comprobante fiscal</b></td>");
      out.println("<input type=\"hidden\" name=\"lstCF\" value=\"0\">");
      out.println("</tr>");
    }

    if( !vCampos.get(0).toString().equals("txtCodSeg") )
    {
      out.println("<tr>");
      out.println("  <td>" + vCampos.get(2) + ":</td>");
      out.println("  <td width=\"10\">&nbsp;</td>");

      if( !vCampos.get(0).toString().substring(0,3).equals("lst") )
      {
        if( datasession.get(vCampos.get(0)) != null )
        {
          out.print  ("  <td>" + datasession.get(vCampos.get(0)) + "</td");
        }
        else
        {
          out.print  ("  <td><input maxlength=\"" + vCampos.get(3) + "\" name=\"" + vCampos.get(0) + "\" size=\"" + vCampos.get(3) +"\" type=\"text\"");
          String CampoR = (String)vCampos.get(5);

          if(CampoR != null && CampoR.length() > 0)
          {
            CampoR = CampoR.trim();
            out.print(" value=\"" + CampoR + "\"");
          }
        }
      }
      else
      {
        out.print("  <td><select name=\"" + vCampos.get(0) + "\" size=\"" + "1" + "\"");
	// 2004-01-24 13:22 ARM: A los elementos 'select' no se les agrego el evento 'onKeyPress'
        if( vCampos.get(3).toString().length() > 0 && datasession.get(vCampos.get(0)) == null)
          out.println(" onChange=\"top.validate(window, this, '" + vCampos.get(4) +"')\">");
        else
          out.println(">");

        for(k=0; k < listaContenidos.size(); k++)
        {
          Vector v1Campos = (Vector)listaContenidos.get(k);
            if( v1Campos.get(0).toString().equals(vCampos.get(0)) )
              out.println("  <option value=\"" + v1Campos.get(3) + "\">" + v1Campos.get(2));
        }

        out.println("  </select></td>");
      }

      if( vCampos.get(3).toString().length() > 0 && !vCampos.get(0).toString().substring(0,3).equals("lst") && datasession.get(vCampos.get(0)) == null )
        out.println(" onBlur=\"top.estaVacio(this)||top.validate(window, this, '" + vCampos.get(4) +"')\" " +
                    " onKeyPressed=\"top.keyPressedHandler(window, this, '" + vCampos.get(4) +"')\"></td>");
      else if(!vCampos.get(0).toString().substring(0,3).equals("lst"))
        out.println(">");
      out.println("</tr>");
    }
    else
    {
      %><%@include file="fieldLector.jsf"%><%
      i = k ;
    }
  }
%>
</table>
<input type="hidden" name="iTxn" value="<%= session.getAttribute("page.iTxn") %>">
<input type="hidden" name="oTxn" value="<%= session.getAttribute("page.oTxn") %>">
<input type="hidden" name="cTxn" value="<%= session.getAttribute("page.cTxn") %>">
<input type="hidden" name="sucursal" value="<%= session.getAttribute("branch") %>">
<input type="hidden" name="teller" value="<%= session.getAttribute("teller") %>">
<input type="hidden" name="moneda" value="<%= session.getAttribute("page.moneda")%>">
<input type="hidden" name="registro" value="<%=session.getAttribute("empno")%>">
<input type="hidden" name="tllrlvl" value="<%=session.getAttribute("tellerlevel")%>">
<input type="hidden" name="supervisor" value="<%= session.getAttribute("teller") %>">
<input type="hidden" name="AuthoOK" value="0">
<input type="hidden" name="SignOK" value="0">
<input type="hidden" name="compania" value="<%out.print(compania);%>">
<input type="hidden" name="validateFLD" value="0">
<input type="hidden" name="needSignature" value="<%out.print(txnIMGAutoriz);%>">
<input type="hidden" name="needAutho" value="<%out.print(txnAuthLevel);%>">
<input type="hidden" name="SignField" value="<%= session.getAttribute("page.SignField") %>">
<input type="hidden" name="override" value="N">
<input type="hidden" name="isChequeFormatoNuevo" value="N">
<input type="hidden" name="dvCuenta" value="">
<input type="hidden" name="dvCheque" value="">
<input type="hidden" name="dvRuta" value="">

<input type="hidden" name="noCheque" value="">
<input type="hidden" name="noCuenta" value="">

<p>
<script>

if (document.entry.txtFechaEfect)
{
  document.writeln("<input type=\"hidden\" name=\"txtFechaSys\">");
  document.entry.txtFechaEfect.value = top.sysdate();
}

if (document.entry.txtFechaError)
{
  document.writeln("<input type=\"hidden\" name=\"txtFechaSys\">");
  document.entry.txtFechaError.value = top.sysdate();
}

if (document.entry.txtFechaCarta)
{
  document.writeln("<input type=\"hidden\" name=\"txtFechaSys\">");
  document.entry.txtFechaCarta.value = top.sysdate();
}

</script>
<style>
.invisible{
	display:None;

}

.visible{

	display:"";
}
</style>

<div id='div_cont' class='invisible'>
<% if(!session.getAttribute("page.cTxn").toString().equals("4155") || 
      !session.getAttribute("page.cTxn").toString().equals("0061"))
    if(txnAuthLevel.equals("1") || txnIMGAutoriz.equals("1"))
     out.println("<a id='btnCont' href=\"javascript:validate(document.entry);javascript:if(!isTxnWithoutValidateXMonto()){AuthoandSignature(document.entry);}\"><img src=\"../ventanilla/imagenes/b_continuar.gif\" border=\"0\"></a>");
    else
     out.println("<a id='btnCont' href=\"javascript:validate(document.entry)\"><img src=\"../ventanilla/imagenes/b_continuar.gif\" border=\"0\"></a>");
%>

</div>

<script language='javascript'>

 var o;
 o = document.getElementById('div_cont');
 
 if(document.forms[0].cTxn.value != '4155' && document.forms[0].cTxn.value != '0061' && document.forms[0].cTxn.value != '9809' && document.forms[0].cTxn.value != '9815' && document.forms[0].cTxn.value != '9711' && <%=redirect.equals("S")%>){
   
   o.innerHTML = "<font color='red'>procesando...</font>";
    	
 }
 
 o.className='visible';
</script>


</form>
</body>
</html>
