<!--
//*************************************************************************************************
//             Funcion: JSP que muestra los reportes de Reversos y Autorizaciones al Gerente
/             Elemento: ResponseReportes.jsp
//          Creado por: Juvenal R. Fernandez Varela
//      Modificado por: Fausto R. Flores Moreno
//*************************************************************************************************
// CCN - 4360364 - 19/08/2005 - Se crea elemento para mostrar los reportes del GTE
// CCN - 4360456 - 07/04/2006 - Se agraga c�digo para reportes de gerente por sucursal y cajero de reversos y
				autorizaciones.
// CCN - 4360581 - 12/03/2007 - Se convierten los estilos en jsf y se hace un include en el jsp
//*************************************************************************************************
-->

<%@page language= "java" import="java.util.Vector, ventanilla.GenericClasses" contentType="text/html"%>
<%
  Vector Datos  = (Vector)session.getAttribute("Log.Registros");
  String Tipo   = (String)session.getAttribute("Log.Tipo");
  String TipoB  = (String)session.getAttribute("Log.TipoB");
  int nopagina = 0;
  int totalpages= Integer.parseInt((String)session.getAttribute("Log.totalpages"));
  if(request.getParameter("nopagina")==null){
    nopagina = 0;
  }else{
    nopagina = Integer.parseInt(request.getParameter("nopagina").toString());
  }
  
  String printcad = "";
  int l = 0;
  int i = 0, g = 0;
  boolean coinciden = true;
  if(Tipo.equals("A"))
  {
  		Tipo = "Autorizaciones";
  		printcad = "AUTORIZACION";
  		l = 12;
  }
  else
  {
  		Tipo= "Reversos";
		l = 7;
		printcad = "REVERSO";
  }
%>
<html>
<head>
  <title>Reportes Gerente</title>
  <%@include file="style.jsf"%>
</head>
<body bgcolor="white">
<form name="reportes" method="post" action="ResponseReportes.jsp">
<%
String cajero = "";
int k=0;

if(Datos.size()>0){%>
<table border="0" cellspacing="0" cellpadding="0" width="350">
  <tr>
  	<td><font size="2"><b><%=Tipo%></b></font></td>
  </tr>
  <tr><td><br></td></tr>
  <%if(TipoB.equals("C")){
  	  cajero = (String)((Vector)Datos.get(0)).get(1);
  	  printcad+="1~"+cajero+"~";
  %>  <tr>	
        <td><font size="2"><b>Cajero:<%=cajero%></b></font></td>
      </tr>
  <%}else{
      printcad+="2~"+ (String)((Vector)Datos.get(0)).get(0)+"~";
  %>
  <tr>	
        <td><font size="2"><b>Sucursal:<% out.println((String)((Vector)Datos.get(0)).get(1).toString().substring(0,4));%></b></font></td>
  </tr>
   <%}%>
  </table><br>
<table border="1" cellspacing="1" cellpadding="1" width="350">
	<tr bgcolor="#CCCCCC" align='center'>
	   <%if(TipoB.equals("S")){%>
	   <td rowspan="2"><b>Cajero</b></td>
	   <%}
	     if(Tipo.equals("Reversos")) 
   		{%>
		<td colspan="2">
	  <%}
		else{%>
		<td colspan="1">
		<%}%>
			<font size="2"><b><%=Tipo.substring(0,l)%></b></font></td>	
		<td>&nbsp;</td>		
		<td colspan="3"><font size="2"><b>Ejecutivo</b></font></td>
		<td>&nbsp;</td>
		<td colspan="4"><font size="2"><b>Autorizador</b></font></td>
		<td>&nbsp;</td>
		<td colspan="5"><font size="2"><b>Transacci&oacute;n Original</b></font></td>		
	</tr>
  <tr bgcolor="#CCCCCC" align='center'>
      <%if(Tipo.equals("Reversos")){%>
    <td>C&oacute;digo</td>
   	  <%}%>
      <td>Hora</td>
    <td>&nbsp;</td>
    <td>Nombre</td>
    <td>Usuario RACF</td>
    <td>Direcci&oacute;n IP</td>
    <td>&nbsp;</td>
    <td>Cajero</td>
    <td>Nombre</td>
    <td>Usuario RACF</td>
    <td>Direcci&oacute;n IP</td>
    <td>&nbsp;</td>
    <td>C&oacute;digo</td>
    <td>Fecha</td>
    <td>Hora</td>    
    <td>Monto</td>
    <td>Divisa</td>
  </tr>
  <%
    i=nopagina*10;
    g=i;
    k=0;
    String codRev = "";
    while(i<Datos.size() && k<10)
    {
	  cajero = (String)((Vector)Datos.get(i)).get(1);
  	  GenericClasses GC = new GenericClasses();
	  coinciden = true;
      while(coinciden && g<Datos.size() && k<10)
    { 
        if(cajero.equals((String)((Vector)Datos.get(g)).get(1)))
        {
          Vector tmp = (Vector)Datos.get(g);
	    out.println("<tr align='center'>");
  	      if(TipoB.equals("S"))
  	        out.println("<td>"+cajero+"</td>");
    	for(int j=2; j<tmp.size(); j++)
    	{	
   		    if(!Tipo.equals("Reversos") || j!=2)
    		  out.println("<td>"+tmp.get(j)+"</td>");
    	  if(j==2)
    	  {
	    	  if(Tipo.equals("Reversos"))
	    	  {
				  int codigoRev = Integer.parseInt((String)tmp.get(10));
				  codigoRev++;
			    codRev = GC.StringFiller(String.valueOf(codigoRev),4, false,"0");    	        
		    	  out.println("<td>"+GC.StringFiller(String.valueOf(codigoRev),4, false,"0")+"</td>");      	  
    	        out.println("<td>"+tmp.get(j)+"</td>");
		      }		    	  
		      out.println("<td bgcolor='#CCCCCC'>&nbsp;</td>");  
	      }
    	  if(j==5||j==9)
	    	  out.println("<td bgcolor='#CCCCCC'>&nbsp;</td>");
    	  }
    	  for(int d=0;d<((Vector)Datos.get(g)).size();d++)
          {
            if(d!=0 && d!=3 && d!=7 && d!=12 && TipoB.equals("S"))
              if(Tipo.equals("Reversos") && d==2)
                printcad = printcad + codRev + "~" + ((Vector)Datos.get(g)).get(d).toString().trim()+"~";
              else
                printcad = printcad + ((Vector)Datos.get(g)).get(d).toString().trim()+"~";
            else if(d>1 && d!=3 && d!=7 && d!=12)
               if(Tipo.equals("Reversos") && d==2)
                printcad = printcad + codRev + "~" + ((Vector)Datos.get(g)).get(d).toString().trim()+"~";
              else
                printcad = printcad + ((Vector)Datos.get(g)).get(d).toString().trim()+"~";
	    }
    	  out.println("</tr>");
        }else{
          coinciden=false;
          g--;
        }
        g++;k++;
      }
      i=g-1;
      i++;
    }
    session.setAttribute("txtCadImpresion", printcad);
  %>
  <center>
  </table align="center"><br><br>
<%if(nopagina>0){
    session.setAttribute("Log.nopagina",String.valueOf(nopagina-1));
%>
    <a href="javascript:prevpage();submite()" onmouseover="self.status='Pagina Anterior';return true;" onmouseout="window.status='';" alt="Pagina Anterior"><img src="../imagenes/b_pagina_anterior.gif" border="0"></a>
<%}%>
<%if(nopagina+1 != totalpages){
    session.setAttribute("Log.nopagina",String.valueOf(nopagina+1));
%>
    <a href="javascript:submite()" onmouseover="self.status='Pagina Siguiente';return true;" onmouseout="window.status='';" alt="Pagina Siguiente"><img src="../imagenes/b_pagina_siguiente.gif" border="0"></a>
<%}%>
  <a href="javascript:top.openDialog('../../servlet/ventanilla.ImprimirServlet', 160, 120, 'top.setPrefs4()', document.reportes)"><img src="../imagenes/b_imprimir.gif" border="0"></a>
<%
}else
	out.println("<b>No se han realizado " +Tipo+"...</b>");
%>
<input type="hidden" name="nopagina" value="<%=nopagina+1%>">
</form>
</body>
  <script language="JavaScript">
    function prevpage(){
     <%nopagina = nopagina-1;%>
     window.document.reportes.nopagina.value=<%=nopagina%>;
    }
    
    function submite(){
      document.reportes.action = "ResponseReportes.jsp";
      document.reportes.submit();
    }
  </script>
</html>

