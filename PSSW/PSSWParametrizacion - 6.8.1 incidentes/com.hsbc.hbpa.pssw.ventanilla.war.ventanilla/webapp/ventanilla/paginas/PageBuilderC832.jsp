<%
//*************************************************************************************************
//            Elemento: PageBuilderC832.jsp
//             Funcion: JSP de captura para RAPL. 
//*************************************************************************************************
// 
//*************************************************************************************************
%>

<%@ page session="true" import="java.util.Hashtable,ventanilla.com.bital.sfb.ServicioRAPL,
ventanilla.com.bital.beans.DatosVarios,java.util.Vector"  %>

<% 
response.setHeader("Cache-Control","no-store"); //HTTP 1.1
response.setHeader("Pragma","no-cache"); //HTTP 1.0
response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<%
String MontoPro = new String("");
String txnAuthLevel = (String)session.getAttribute("page.txnAuthLevel");
String txnIMGAutoriz = (String)session.getAttribute("page.txnIMGAutoriz");
Vector v = (Vector)session.getAttribute("page.specialacct");
String TxnE = (String)session.getAttribute("page.iTxn");

Hashtable datasession = (Hashtable)session.getAttribute("page.datasession");
if(datasession == null)
  datasession = new Hashtable();


//String noservicios =  (String)session.getAttribute("page.noservicios");
session.setAttribute("page.noservicios","1");
String noservicios= "1";

int nserv = Integer.parseInt(noservicios);
int k;
boolean continuar=true; 
%>
<html>
<head>
<%@include file="style.jsf"%>
<title>Datos de la Transaccion</title>
<script language="JavaScript">
<!--

function cancelar()
{
	document.entry.action='../ventanilla/paginas/Final.jsp';
    document.entry.submit();
}

function enviaMonto(){

	alert("Envia montos");
}

function wasNotEnter(){

	if(window.event.keyCode == '13'){
		return false;		
	}else{
		return true;		
	}
}
var formName = 'Mio'
var specialacct = new Array()
<%
if(!v.isEmpty())
{
  for(int iv = 0; iv < v.size(); iv++){out.print("specialacct[" + iv + "]='" + v.get(iv) + "'" + '\n');}
}%>

function validate(form)
{
<%
    
	 if(TxnE.equals("RAPL"))
    {
       
            out.println("  if( !top.validate(window, document.entry.monto, 'isValMtoRAPL') )");
            out.println("    return");
    }
    
    if(TxnE.equals("5503"))
    {
           
    
        for (int i = 1; i < nserv+1;i++)
        {
            
            out.println("  if( !top.validate(window, document.entry.servicio" + i + ", 'isValServ') )");
            out.println("    return");
            out.println("  if( !top.validate(window, document.entry.nopagos" + i + ", 'isValNoPagos') )");
            out.println("    return");
            out.println("  if( !top.validate(window, document.entry.monto" + i + ", 'isValMtoRAP') )");
            out.println("    return");
        }
    }
	
	for(int i = 1; i < nserv+1; i++){%>
		if(document.entry.servicio<%=i%>.value == "1926"){
			if(document.entry.txtCreditoKroner<%=i%>.value.length == 0){
				alert("Debe de Capturar No. de Cr�dito");
				document.entry.txtCreditoKroner<%=i%>.focus();
				return;
			}else{
				if(isNaN(document.entry.txtCreditoKroner<%=i%>.value)){
					alert("El No. de Cr�dito debe de ser n�merico");
					document.entry.txtCreditoKroner<%=i%>.value = "";
					document.entry.txtCreditoKroner<%=i%>.focus();
					return;
				}else{
					if(document.entry.txtCreditoKroner<%=i%>.value.length < 10){
						var len = 10 - document.entry.txtCreditoKroner<%=i%>.value.length;
						var ceros = "";
						for(i = 0; i < len; i++)
							ceros += "0";
						document.entry.txtCreditoKroner<%=i%>.value = ceros + document.entry.txtCreditoKroner<%=i%>.value;
					}
				}
			}
			if(document.entry.rdoPagoAD<%=i%>[0].checked == false && document.entry.rdoPagoAD<%=i%>[1].checked == false){
				alert("Debe de seleccionar tipo de pago");
				return;
			} 
			if(document.entry.rdoPagoAD<%=i%>[1].checked && document.entry.noAdelanto<%=i%>.value.length == 0){
				alert("Debe de Capturar el N�mero de Pagos a Adelantar");
				document.entry.noAdelanto<%=i%>.focus();
				return;
			}else if(document.entry.rdoPagoAD<%=i%>[1].checked && document.entry.noAdelanto<%=i%>.value.length > 0){
				if(isNaN(document.entry.noAdelanto<%=i%>.value)){
					alert("En n�mero de pagos debe de ser n�merico");
					document.entry.noAdelanto<%=i%>.value = "";
					document.entry.noAdelanto<%=i%>.focus();
					return;
				}else{
					if(Number(document.entry.noAdelanto<%=i%>.value) == 0){
						alert("N�mero de pagos adelantados debe ser mayor a 0");
						document.entry.noAdelanto<%=i%>.value = "";
						document.entry.noAdelanto<%=i%>.focus();
						return;
					}
				}
			}
		}
	<%}%>
  form.validateFLD.value = '1';
  if (document.entry.txtFechaEfect || document.entry.txtFechaError || document.entry.txtFechaCarta)
    document.entry.txtFechaSys.value=top.sysdatef();
<% if(!txnAuthLevel.equals("1") && !txnIMGAutoriz.equals("1"))
	{
    out.println("  document.entry.monto1.value=document.entry.monto.value");
    out.println("  document.entry.txtTot.value=document.entry.monto.value");
    out.println("  document.entry.tot.value=document.entry.monto.value");
    out.println("  form.submit();");
    out.println("var obj = document.getElementById('btnCont');"); 
    out.println("obj.onclick = new Function('return false');");   
	}   
%>
}
function AuthoandSignature(entry)
{
 var Cuenta;
 if(entry.validateFLD.value != '1')
  return
 if(entry.needAutho.value == '1' && entry.AuthoOK.value != '1')
 {
  top.openDialog('../ventanilla/paginas/authoriz.jsp', 220, 130, 'top.setPrefs()', entry);
 }
 if(entry.needSignature.value == '1' && entry.SignField.value != '')
{
  Cuenta = eval('entry.' + entry.SignField.value + '.value')
  top.openModalDialog('<%=request.getContextPath()%>/servlet/ventanilla.FirmasServlet?txtDDACuenta=' + Cuenta, 710, 430, 'top.setPrefs2()', entry)
 }
}


function check(pos){
	var status = eval("document.entry.rdoPagoAD" + pos + "[1].checked");
	if(status){
		eval("document.entry.noAdelanto" + pos + ".disabled = false");
		alert("El pago cubrir� la amortizaci�n m�s pr�xima incluyendo los intereses a la inmediata\nfecha de exigibilidad (no puede hacerse parcial, solo completa) y no debe tener adeudos\nvencidos a la fecha que se pretende realizar el pago");
		document.entry.noAdelanto1.focus();
	}else{
		eval("document.entry.noAdelanto" + pos + ".disabled = true");
		alert("El pago se aplicar� a capital, inter�s, IVA y comisiones, a la fecha de este pago\nSi el importe es mayor al exigible se aplicar� directo a Capital.");
		document.entry.txtDDACuenta.focus();
	}
}

function init(num) {
  var fila;
  var col;
  fila = document.getElementById('RAP').getElementsByTagName('tr');
  for(i=0; i < fila.length; i++){
    col = fila[i].getElementsByTagName('td');
    if(i == 0)
       j = num - 6;
    else
       j = num;
    for(j = j; j < col.length; j++){
       fila[i].getElementsByTagName('td')[j].style.display='none';
    }
  }
}

function ocultarKroner(row,num,ver) {
	var fila;
    var col;
    var dis;
    var j = 0;
    var i = 0;

  	dis = ver ? '' : 'none';
	  
	// despliega los titulos
	fila = document.getElementById('RAP').getElementsByTagName('tr');
	for(i = 0; i < 2; i++){
	  	col = fila[i].getElementsByTagName('td');
	   	if(i == 0)
	   		j = num - 6;
	   	else
	   		j = num;
	  	for(j = j;j < col.length; j++){
	   		fila[i].getElementsByTagName('td')[j].style.display=dis;
	   	}
	}
	// despliga el renglon correspondiente
	col = fila[row].getElementsByTagName('td');
	for(j = num;j < col.length; j++)
		fila[row].getElementsByTagName('td')[j].style.display=dis;
}

//-->
</script>
</head>
<body bgcolor="#FFFFFF" onload="top.setFieldFocus(window.document);">
<br>
<% out.print("<form name=\"entry\" action=\"../servlet/ventanilla.DataServlet\" method=\"post\" ");
   if(txnAuthLevel.equals("1"))
    out.print("onSubmit=\"return validate(this)\"");
   out.println(">");
%>

<h1><%=session.getAttribute("page.txnImage")%></h1>

<h3>
<%out.print(session.getAttribute("page.cTxn") + "  " + (String)session.getAttribute("page.txnlabel"));%>
</h3>

<%if(TxnE.equals("5503")) { %>
    <table id="RAP" border="0">
		<tr align="center">
			<td colspan="8">&nbsp;&nbsp;</td>
			<td  align="center"><b>Cr�dito Kroner</b></td><td >&nbsp;&nbsp;</td>
			<td  align="center" colspan="3"><b>Tipo de Pagos</b></td>
			<td>&nbsp;&nbsp;</td><td>&nbsp;&nbsp;</td>
		</tr>
	    <tr align="center">
	    	<td>&nbsp;&nbsp;</td><td>&nbsp;&nbsp;</td>
	    	<td><b>Servicio</b></td><td>&nbsp;&nbsp;</td>
	    	<td><b>No. de Pagos</b></td><td>&nbsp;&nbsp;</td>
	    	<td><b>Monto</b></td><td>&nbsp;&nbsp;</td>
			<td ><b>No. de Cr�dito</b></td><td >&nbsp;&nbsp;</td>
			<td ><b>Pago Directo</b></td><td >&nbsp;&nbsp;</td>
			<td ><b>Pago Adelantado</b></td><td >&nbsp;&nbsp;</td>
			<td ><b>No. Adelantos</b></td>
	    </tr>
<%  for (int i = 1; i < nserv+1;i++)
    {%>
        <tr>
        	<td><%=i%>.-</td><td>&nbsp;&nbsp;</td>
        	<td><input type="text" name="servicio<%=i%>" size="7" maxlength="7" onBlur="serv()" onKeyPress="top.estaVacio(this)||top.keyPressedHandler(window, this, 'isValServ')"  tabindex="<%=i+1%>"></td>
        	<td>&nbsp;&nbsp;</td>
        	<td align="center"><input type="text" name="nopagos<%=i%>" size="2" maxlength="2" onBlur="top.estaVacio(this)||top.validate(window, this, 'isValNoPagos')" onKeyPress="top.estaVacio(this)||top.keyPressedHandler(window, this, 'isValNoPagos')" tabindex="<%=(i+1)%>"></td>
        	<td>&nbsp;&nbsp;</td>
        	<td><input type="text" name="monto<%=i%>" size="14" maxlength="11"  onBlur="top.estaVacio(this)||top.validate(window, this, 'isValMtoRAP')" onKeyPress="wasNotEnter(this)||this.blur()" tabindex="<%=(i+1)%>"></td>
        	<td>&nbsp;&nbsp;</td>
        	<td id="headKroner"><input type="text" name="txtCreditoKroner<%=i%>" size="10" maxlength="10"  onBlur="" onKeyPress="wasNotEnter(this)||this.blur()" disabled="disabled" tabindex="<%=(i+1)%>"></td>
        	<td>&nbsp;&nbsp;</td>
        	<td id="headKroner" align="center"><input type="radio" name="rdoPagoAD<%=i%>" value="pd" onClick="check(<%=i%>)" disabled="disabled" tabindex="<%=(i+1)%>"></td>
        	<td>&nbsp;&nbsp;</td>
        	<td id="headKroner" align="center"><input type="radio" name="rdoPagoAD<%=i%>" value="pa" onClick="check(<%=i%>)" disabled="disabled" tabindex="<%=(i+1)%>"></td>
        	<td >&nbsp;&nbsp;</td>
        	<td id="headKroner" align="center"><input type="text" name="noAdelanto<%=i%>" size="3" maxlength="3"  onBlur="" onKeyPress="wasNotEnter(this)||this.blur()" disabled="disabled" tabindex="<%=(i+1)%>"></td>
        </tr>
<%  }%>
	    <tr>
	    	<td colspan="4"></td>
	    	<td align="right"><b>Total :</b></td><td>&nbsp;&nbsp;</td>
	    	<td><input type="text" name="txtTot" size="16" maxlength="13" value="0.00" onBlur="top.validate(window, this, 'notChange')" onKeyPress="top.keyPressedHandler(window, this, 'notChange')"></td>
	    </tr>
    </table>
<%  for (int i = 1; i < nserv+1;i++)
    {%>
	<input type="hidden" name="s<%=i%>" value="X">
<%  }%>
    <input type="hidden" name="nservicios" value="<%=session.getAttribute("page.noservicios")%>">
    <input type="hidden" name="tot" value="0">
    <input type="hidden" name="np" value="0">
<%   if(session.getAttribute("Tarjeta.TDI") != null) //Loch Ness
     {%>
<table>
<tr align="left">
   <td><b>N&uacute;mero de Tarjeta TDI: </td>
   <td><b><font color="red"><%=(String)session.getAttribute("Tarjeta.TDI")%></font></td>
</tr>
</table>
<%   }%>
<p>
<b>Capturar N&uacute;mero de Cuenta de Cheques para Clientes ...</b>
<p>
<table>
<tr><td align="right">Cuenta :</td><td>&nbsp;&nbsp;</td>
<td><input type="text" name="txtDDACuenta" size="10" maxlength="10" onBlur="top.estaVacio(this)||top.validate(window, this, 'isValidAcct')" onKeyPress="top.estaVacio(this)||top.keyPressedHandler(window, this, 'isValidAcct')"></td>
</table>
<%
}
else
{
 
	///POLAS
	
		    ventanilla.com.bital.beans.DatosVarios arpCombo = ventanilla.com.bital.beans.DatosVarios.getInstance();                   
        Vector vecRAP = new Vector();  
    	vecRAP =(Vector)arpCombo.getDatosV("ARP");   
    	session.setAttribute("servicioARP", vecRAP);
	DatosVarios rapl = DatosVarios.getInstance();                   
    Hashtable htRAPL = null;      
    htRAPL =(Hashtable)rapl.getDatosH("SERVICRAPL");
    ServicioRAPL srapl = (ServicioRAPL)htRAPL.get(datasession.get("txtServicio"));

    String strTPago=srapl.getTipoPago();
 

%>
    <table border="0">
    <tr>
    	<td><b>Servicio: </b></td><td><b><%=srapl.getDescServicio()%></b></td>
    </tr>
    <tr>
    	<td><b>Clave Servicio: </b></td><td><b><%=srapl.getNoServicio()%></b></td>
    </tr>
    <tr>
    	<td><b>Referencia: </b></td><td><b><%=datasession.get("txtReferencia")%></b></td>
    </tr>
    
     <%   
     
    if(srapl.isConsulta()){
    	String bnderrorSP= (String)session.getAttribute("bnderrorSP");
    	String statusrefeSP= (String)session.getAttribute("statusrefeSP");
    	
    	
   		session.removeAttribute("bnderrorSP"); 
   		session.removeAttribute("statusrefeSP");  
   		     	
	    String mtoPolas= (String)session.getAttribute("mtoSP");
	   	    
    	session.removeAttribute("mtoSP");
     	
     	
     	if  (bnderrorSP.compareTo("0")==0){  //No hubo error
     		if(statusrefeSP.compareTo("0")==0){ //Estatus de la ref ok
     			if(strTPago.compareTo("P")==0){ //Tipo pago Parcial
    			%>
    			<tr>
    				<td><b>Saldo Deudor: </b></td><td><input type="text" name="monto" size="14" maxlength="11"  onBlur="top.estaVacio(this)||top.validate(window, this, 'isValMtoRAPL')" onKeyPress="wasNotEnter(this)||this.blur()" tabindex="1" value="<%=mtoPolas%>"></td> 	
    			</tr>
    
    		<%}else{%>
    			<tr>
    				<td><b>Saldo Deudor: </b></td><td><input type="text" name="monto" size="14" maxlength="11"  onBlur="top.estaVacio(this)||top.validate(window, this, 'isValMtoRAPL')" onKeyPress="wasNotEnter(this)||this.blur()" tabindex="1" value="<%=mtoPolas%>" disabled="disabled"></td> 	
    			</tr>
    			
    		<%}
    			%>
    		</table>
     			<%continuar=true;
     		
     		}else{ 
     			//Estatus de la Ref invalida
     			%>
     			</table>
    	  	
    	  		<b><font color='red'>La referencia es invalida. Favor de invitar al Usuario a revisar su referencia y monto con su proveedor de Servicios</font><b>
    	  		<br>    	  	
     		<%
				 continuar=false;
     		}
     	
     	} 
     	else{//en caso de erro con web polas
     		if(strTPago.compareTo("P")==0){
    		//Captura monto
    	%>
    		<tr>
    			<td><b>Saldo Deudor: </b></td><td><input type="text" name="monto" size="14" maxlength="11"  onBlur="top.validate(window, this, 'isValMtoRAPL')" onKeyPress="wasNotEnter(this)||this.blur()" tabindex="1"></td> 	
    		</tr>
    		</table>
    	<% 
    		continuar=true;
    	
    	}else{
    	  	//muestra dislay de error no se puede pagar
    	  	
    	  	%>
    	  	</table>
    	  	<b><font color='red'>Servicio no disponible por el momento. El Cliente �nicamente recibe el pago total del adeudo.  No es posible consultar el monto con el proveedor de Servicios</font><b>
    	  	<br>
    	 	<%
    	 	 continuar=false;
    	 }
     	
     	}
    %>
    
    <% }
    else{
    	
    		//Captura monto
    	%>
    		<tr>
    			<td><b>Saldo Deudor: </b></td><td><input type="text" name="monto" size="14" maxlength="11"  onBlur="top.validate(window, this, 'isValMtoRAPL')" onKeyPress="wasNotEnter(this)||this.blur()" tabindex="1"></td> 	
    		</tr>
    		</table>
    	<% 
    		continuar=true; 
    
    }
          
	%>
    
   
     <input type="hidden" name="servicio1" value="<%=(String)datasession.get("txtServicio")%>">
     <input type="hidden" name="nopagos1" value="1">
     <input type="hidden" name="monto1" value="">
     <input type="hidden" name="nservicios" value="1">
     <input type="hidden" name="tot" value="">
     <input type="hidden" name="txtTot" value="">
     <input type="hidden" name="np" value="1">
<%}%>
<input type="hidden" name="flujorapcapr" value="1">
<input type="hidden" name="flujorap" value="1">
<input type="hidden" name="noefec" value="0">
<input type="hidden" name="ser60" value="0">
<input type="hidden" name="iTxn" value="<%= session.getAttribute("page.iTxn")%>">
<input type="hidden" name="oTxn" value="<%= session.getAttribute("page.oTxn")%>">
<input type="hidden" name="cTxn" value="<%= session.getAttribute("page.cTxn")%>">
<input type="hidden" name="sucursal" value="<%= session.getAttribute("branch")%>">
<input type="hidden" name="teller" value="<%= session.getAttribute("teller")%>">
<input type="hidden" name="moneda" value="<%= session.getAttribute("page.moneda")%>">
<input type="hidden" name="registro" value="<%=session.getAttribute("empno")%>">
<input type="hidden" name="tllrlvl" value="<%=session.getAttribute("tellerlevel")%>">
<input type="hidden" name="supervisor" value="<%= session.getAttribute("teller") %>">
<input type="hidden" name="AuthoOK" value="0">
<input type="hidden" name="SignOK" value="0">
<input type="hidden" name="validateFLD" value="0">
<input type="hidden" name="compania" value="20">
<input type="hidden" name="needSignature" value="<%out.print(txnIMGAutoriz);%>">
<input type="hidden" name="needAutho" value="<%out.print(txnAuthLevel);%>">
<input type="hidden" name="SignField" value="<%= session.getAttribute("page.SignField") %>">
<br>
	 <%
       	if (continuar){
       		 if(txnAuthLevel.equals("1") || txnIMGAutoriz.equals("1"))
		   		 out.println("<a id='btnCont' tabindex='2' href=\"javascript:validate(document.entry);javascript:AuthoandSignature(document.entry)\" ><img src=\"../ventanilla/imagenes/b_continuar.gif\" border=\"0\"></a>");
		   	else
		    	out.println("<a id='btnCont'  tabindex='2' href=\"javascript:validate(document.entry)\"><img src=\"../ventanilla/imagenes/b_continuar.gif\" border=\"0\"></a>");    		
       	} 
       	else{
       		out.println("<a href=\"JavaScript:cancelar()\"><img src=\"../ventanilla/imagenes/b_cancelar.gif\" border=0 alt=\"Cancelar\"></a>");
       	}   
                
	%>
</form>
</body>
</html>
