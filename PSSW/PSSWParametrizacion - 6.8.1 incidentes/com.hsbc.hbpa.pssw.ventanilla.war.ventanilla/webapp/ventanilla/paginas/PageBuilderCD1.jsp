<!--
// CCN - 4360581 - 12/03/2007 - Se convierten los estilos en jsf y se hace un include en el jsp
-->

<%@ page session="true"%>
<%
response.setHeader("Cache-Control","no-store"); //HTTP 1.1
response.setHeader("Pragma","no-cache"); //HTTP 1.0
response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
    <head>
        <%@include file="style.jsf"%>
        <title>Proceso de Digitalizacion</title>
    </head>
    <Script language="JavaScript">

    function FieldautoTab(input, e) 
    {
        var len = getFieldSize(input);
        var keyCode = (isNN) ? e.which : e.keyCode;
        var filter = (isNN) ? [0,8,9] : [0,8,9,16,17,18,37,38,39,40,46];
        if(input.value.length >= len && !containsElement(filter,keyCode)) 
        {
            input.value = input.value.slice(0, len);
            input.form[(getIndex(input)+1) % input.form.length].select();
            input.form[(getIndex(input)+1) % input.form.length].focus();
        }
        function containsElement(arr, ele) 
        {
            var found = false, index = 0;
            while(!found && index < arr.length)
            if(arr[index] == ele)
                found = true;
            else
                index++;
            return found;
        }
        function getIndex(input) 
        {
            var index = -1, i = 0, found = false;
            while (i < input.form.length && index == -1)
            if (input.form[i] == input) index = i;
            function FieldautoTab(input, e) 
            {
                var len = getFieldSize(input);
                var keyCode = (isNN) ? e.which : e.keyCode;
                var filter = (isNN) ? [0,8,9] : [0,8,9,16,17,18,37,38,39,40,46];
                if(input.value.length >= len && !containsElement(filter,keyCode)) 
                {
                    input.value = input.value.slice(0, len);
                    input.form[(getIndex(input)+1) % input.form.length].select();
                    input.form[(getIndex(input)+1) % input.form.length].focus();
                }
                function containsElement(arr, ele) 
                {
                    var found = false, index = 0;
                    while(!found && index < arr.length)
                        if(arr[index] == ele)
                            found = true;
                        else
                            index++;
                    return found;
                }
                function getIndex(input) 
                {
                    var index = -1, i = 0, found = false;
                    while (i < input.form.length && index == -1)
                    if (input.form[i] == input)index = i;
                    else i++;
                    return index;
                }
            return true;
            }
            else i++;
            return index;
            }
        return true;
    }

    function getFieldSize(field)
    {
        gField = eval("window.document.forms[0]." + field.name)
	var fieldToString = gField.toString();
	var wordPosition = fieldToString.indexOf('size');
        var maxLength = 8;
        var attributeValue = '';
        if(wordPosition > -1)
        {
            wordPosition += 5;
            while(fieldToString[wordPosition] >= '0' && fieldToString[wordPosition] <= '9' && wordPosition < fieldToString.length)
            attributeValue += fieldToString[wordPosition++];
            maxLength = Number(attributeValue);
        }
        return maxLength;
    }

    function fillRecord(field)
    {
        var maxLength = getFieldSize(field);
        while( gField.value.length < maxLength )
        gField.value = '0' + gField.value;
    }

    function sendservlet()
    {	        
        document.entry.action = "../../servlet/ventanilla.DigitalizarServlet"
        document.entry.submit();
    }
		
    </SCRIPT>	
    <body onload="setFieldFocus(window.document)">
        <form name="entry">
            <input type="hidden" name="cTxn" value="<%= session.getAttribute("page.cTxn") %>">
            <input type="hidden" name="moneda" value="<%= session.getAttribute("page.moneda")%>">
            <input type="hidden" name="opcion1" value="1">
            <table border="0" cellspacing="0" align="center">
            <tr>
                <td>Cajero:</td>
                <td width="50">&nbsp;</td>
                <td><input maxlength="6" name="cajero" size="6" type="text" onKeyUp="return FieldautoTab(this, event);" onchange="javascript:fillRecord(this)"></td>
            </tr>
            <tr>
                <td>Contrase&ntilde;a:</td>
                <td>&nbsp;</td>
                <td><input maxlength="8" name="password" size="8" type="password" onKeyUp="return FieldautoTab(this, event);" onchange="javascript:checkPass()"></td>
            </tr>
            <tr>
                <td>
                <a href="JavaScript:sendservlet()"><img src="../imagenes/b_aceptar.gif" border="0"></a>
                </td>
            </tr>
            </table>
        </form>
    </body>
</html> 
