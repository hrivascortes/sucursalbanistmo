// =============================================================================================
// Archivo: validate.js
// Descripción: Contiene las validaciones del lado del cliente para PSSW
// CCN - 4360409 - 06/01/2006 - Se crean validaciones para la Linea de Captura del SAT
// CCN - 4360424 - 03/02/2006 - Se crean validaciones para txns combinadas y divisas
// CCN - 4360425 - 03/02/2006 - Proyecto Tarjeta de Identificacion TDI
// CCN - 4360431 - 14/02/2006 - Se realizan correcciones a validaciones para txn combinadas
// CCN - 4360428 - 21/02/2006 - Se modifica la validacion para registro de usarios nuevos generados por seg. lógica
// CCN - 4360437 - 24/02/2006 - Se realizan correcciones a validaciones para txns 0602,4455
// CCN - 4360446 - 10/03/2006 - Se agrega condicion en rfc para 0030 TDI
// CCN - 4360455 - 07/04/2006 - Se agrega funcion para valida cheque devueltos
// CCN - 4360456 - 07/04/2006 - Se agrega condición para bloqueo de inversiones con prefijo 21365 aperturadas por Conexion Personal.
// CCN - 4360462 - 10/04/2006 - Se genera nuevo paquete por errores en paquete anterior
// CCN - 4360487 - 09/06/2006 - Se realiza modificación en la validación del monto para no permitir centavos en la txn 0027
// CCN - 4360493 - 23/06/2006 - Se realizan modificaciones para bloqueo de inversiones con prefijo 84000, monto no cero en txns para depto 9005,
//				se valida que no se escriban servicios con prefijos 0, 00, 000 en la txn 5503
// CCN - 4360492 - 23/06/2006 - Se realizan modificaciones para el esquema de Cobro Comisión Usuarios RAP
// CCN - 4360494 - 07/07/2006 - Se agrega validación para el campo Tarjeta TDI de la txn 0030
// CCN - 4360468 - 21/07/2006 - Se elimina if en funcion isValServ donde se validava que un servicio que tiene prefijo 0, 00 o 000 se mandaba mensaje de alerta
// CCN - 4360508 - 08/09/2006 - Se realiza validacion para no acpetar espacios en blanco en los servicios 669 y 670
// CCN - 4360512 - 08/09/2006 - Se realiza validacion para campo numero de folio en txn 0238
// CCN - 4360515 - 14/09/2006 - Se realiza modificacion para corregir problema productivo con la validación de Tipo de Identifiación en Pasaporte y No Requiere para la txn 1053
// CCN - 4360516 - 18/09/2006 - Se realiza cambio en validacion de tipo de identificacion para la txn 1053
// CCN - 4360522 - 06/10/2006 - Se realizan cmabios para no valida el campo lstBanLiq para la txn 0021
// CCN - 4360533 - 20/10/2006 - Se realizan modificaciones para OPEE
// CCN - 4360533 - 14/12/2006 - Se realizan modificaciones para agregar validacion de monto 0 en txns 5161 y 5183
// CCN - 4360574 - 09/03/2007 - Se agregan validaciones las formas de pago de un servicio RAP que utilice el esquema de RAP Calculadora
// CCN - 4360576 - 09/03/2007 - Modificaciones para proyecto OPMN
// CCN - 4360592 - 20/04/2007 - Modificaciones para proyecto OPMN
// CCN - 4360598 - 04/05/2007 - Modificaciones para proyecto OPMN nombres de beneficiario y ordenantes sin asterisco.
// CCN - 4360602 - 18/05/2007 - Se agregan funciones de validación para el pago de SUAS con cedula
// CCN - 4620008 - 14/09/2007 - Se agregan funciones de validación para la txn DARP
// CCN -         - 08/11/2007 - Se agrega validación de txtMonto para formato SIB - Humberto Enrique Balleza Mejía
// CCN -         - 10/01/2008 - Se hacen modificaciones para transacción global de cheques locales
// CCN -         - 24/07/2009 - Se agrega validación para alfanumérico.
// =============================================================================================

// =============================================================================================
// Definición de variables globales
// =============================================================================================
// Contiene una referencia al frame que contiene el campo a validar
var gFrame ;
// Contiene una referencia a la forma que contiene el campo a validar
var gForm ;
// Contiene una referencia al campo a validar
var gField ;
// Nombre del metodo de validacion
var gMethod ;
// Contiene una referencia a un diálogo externo al IE
var dialogWin ;
var IELinkClicks ;
var tdc = "";


// =============================================================================================
// Definición de funciones de validación
// =============================================================================================
var dispatchLookup = new Array() ;
dispatchLookup["isnumeric"] = new dispatcher(isnumeric) ;
dispatchLookup["isValidDesc1795"] = new dispatcher(isValidDesc1795) ;
dispatchLookup["isValidDesc1797"] = new dispatcher(isValidDesc1797) ;
dispatchLookup["isValidDesc1295"] = new dispatcher(isValidDesc1295) ;
dispatchLookup["isCurrenNoCero"] = new dispatcher(isCurrenNoCero) ;
dispatchLookup["PasswordOK"] = new dispatcher(PasswordOK) ;
dispatchLookup["isnumchec"] = new dispatcher(isnumchec) ;
dispatchLookup["isafore"] = new dispatcher(isafore) ;
dispatchLookup["isCurrency"] = new dispatcher(isCurrency) ;
dispatchLookup["isleetarj"] = new dispatcher(isleetarj) ;
dispatchLookup["isInfonavit"] = new dispatcher(isInfonavit) ;
dispatchLookup["isCBA"] = new dispatcher(isCBA) ;
dispatchLookup["ValidNAFIN"] = new dispatcher(ValidNAFIN) ;
dispatchLookup["EfecSAR"] = new dispatcher(EfecSAR) ;
dispatchLookup["SumSAR"] = new dispatcher(SumSAR) ;
dispatchLookup["SumTotSAR"] = new dispatcher(SumTotSAR) ;
dispatchLookup["iscontrase"] = new dispatcher(iscontrase) ;
dispatchLookup["isValAfore"] = new dispatcher(isValAfore) ;
dispatchLookup["isValTarj"] = new dispatcher(isValTarj) ;
dispatchLookup["isValNIPs"] = new dispatcher(isValNIPs) ;
dispatchLookup["isaniPago"] = new dispatcher(isaniPago) ;
dispatchLookup["isSAAC"] = new dispatcher(isSAAC) ;
dispatchLookup["isIngresos"] = new dispatcher(isIngresos) ;
dispatchLookup["isNumReg"] = new dispatcher(isNumReg) ;
dispatchLookup["isEgresos"] = new dispatcher(isEgresos) ;
dispatchLookup["isBanMag"] = new dispatcher(isBanMag) ;
dispatchLookup["isReferTIP"] = new dispatcher(isReferTIP) ;
dispatchLookup["isRefer"] = new dispatcher(isRefer) ;
dispatchLookup["isBimestre"] = new dispatcher(isBimestre) ;
dispatchLookup["isSAR"] = new dispatcher(isSAR) ;
dispatchLookup["isValRFC"] = new dispatcher(isValRFC) ;
dispatchLookup["isBenef"] = new dispatcher(isBenef) ;
dispatchLookup["isTipPago"] = new dispatcher(isTipPago) ;
dispatchLookup["isValidDias"] = new dispatcher(isValidDias) ;
dispatchLookup["isFolioSerb"] = new dispatcher(isFolioSerb) ;
dispatchLookup["istelmex"] = new dispatcher(istelmex) ;
dispatchLookup["isValidHora"] = new dispatcher(isValidHora) ;
dispatchLookup["isAutoriCV"] = new dispatcher(isAutoriCV) ;
dispatchLookup["isTipoCambioCV"] = new dispatcher(isTipoCambioCV) ;
dispatchLookup["isChequeCaja"] = new dispatcher(isChequeCaja) ;
dispatchLookup["isValidCheque"] = new dispatcher(isValidCheque) ;
dispatchLookup["isValidChequeDev"] = new dispatcher(isValidChequeDev) ;
dispatchLookup["isDecInteger"] = new dispatcher(isDecInteger) ;
dispatchLookup["isValidSer4061"] = new dispatcher(isValidSer4061) ;
dispatchLookup["isValidAcct"] = new dispatcher(isValidAcct) ;
dispatchLookup["isValidCta"] = new dispatcher(isValidCta) ;
dispatchLookup["isACDO"] = new dispatcher(isACDO) ;
dispatchLookup["isBancoRem"] = new dispatcher(isBancoRem) ;
dispatchLookup["isValidDesc1523"] = new dispatcher(isValidDesc1523) ;
dispatchLookup["isValidDesc1197"] = new dispatcher(isValidDesc1197) ;
dispatchLookup["isValidDesc5903"] = new dispatcher(isValidDesc5903) ;
dispatchLookup["isValidDesc4503"] = new dispatcher(isValidDesc4503) ;
dispatchLookup["isValidDate"] = new dispatcher(isValidDate) ;
dispatchLookup["isRegistro"] = new dispatcher(isRegistro) ;
dispatchLookup["isIDCajero"] = new dispatcher(isIDCajero) ;
dispatchLookup["isCurr8"] = new dispatcher(isCurr8) ;
dispatchLookup["isRFCCF"] = new dispatcher(isRFCCF) ;
dispatchLookup["isAbonoAcct"] = new dispatcher(isAbonoAcct) ;
dispatchLookup["verPrint"] = new dispatcher(verPrint) ;
dispatchLookup["isValidChoice"] = new dispatcher(isValidChoice) ;
dispatchLookup["toUpperCase"] = new dispatcher(toUpperCase) ;
dispatchLookup["CtaCargo"] = new dispatcher(CtaCargo) ;
dispatchLookup["isValidaFirma"] = new dispatcher(isValidaFirma) ;
dispatchLookup["isMontoChCaja"] = new dispatcher(isMontoChCaja) ;
dispatchLookup["isValidMonto"] = new dispatcher(isValidMonto) ;
dispatchLookup["MtoCargo"] = new dispatcher(MtoCargo) ;
dispatchLookup["Cheque"] = new dispatcher(Cheque) ;
dispatchLookup["ChequeD"] = new dispatcher(ChequeD) ;
dispatchLookup["isACorte"] = new dispatcher(isACorte) ;
dispatchLookup["isValidCurrProc"] = new dispatcher(isValidCurrProc) ;
dispatchLookup["isValidAuthori"] = new dispatcher(isValidAuthori) ;
dispatchLookup["isCodeSeg"] = new dispatcher(isCodeSeg) ;
dispatchLookup["isValidMto"] = new dispatcher(isValidMto);
dispatchLookup["isValidNo"] = new dispatcher(isValidNo) ;
dispatchLookup["isTDC"] = new dispatcher(isTDC) ;
dispatchLookup["notChange"] = new dispatcher(notChange) ;
dispatchLookup["isValMtoRAPL"] = new dispatcher(isValMtoRAPL) ;
dispatchLookup["isValMto"] = new dispatcher(isValMto) ;
dispatchLookup["isValServ"] = new dispatcher(isValServ) ;
dispatchLookup["isValMtoRAP"] = new dispatcher(isValMtoRAP) ;
dispatchLookup["isValMto5503"] = new dispatcher(isValMto5503) ;
dispatchLookup["Valid5503"] = new dispatcher(Valid5503) ;
dispatchLookup["ValSum"] = new dispatcher(ValSum) ;
dispatchLookup["MtoServ"] = new dispatcher(MtoServ) ;
dispatchLookup["ValRef40"] = new dispatcher(ValRef40) ;
dispatchLookup["ValRefFija"] = new dispatcher(ValRefFija) ;
dispatchLookup["FlujoRAP"] = new dispatcher(FlujoRAP) ;
dispatchLookup["isMontoRAP"] = new dispatcher(isMontoRAP) ;
dispatchLookup["Ref1880"] = new dispatcher(Ref1880) ;
dispatchLookup["TDCAmex"] = new dispatcher(TDCAmex) ;
dispatchLookup["ValTDC"] = new dispatcher(ValTDC) ;
dispatchLookup["ValRem"] = new dispatcher(ValRem) ;
dispatchLookup["MtoCOPM"] = new dispatcher(MtoCOPM) ;
dispatchLookup["wichForPago"] = new dispatcher(wichForPago) ;
//dispatchLookup["wichLiqBank"] = new dispatcher(wichLiqBank) ;
dispatchLookup["isSerial"] = new dispatcher(isSerial) ;
//ygx
dispatchLookup["isSerialCheq"] = new dispatcher(isSerialCheq) ;

dispatchLookup["isMontoCargo"] = new dispatcher(isMontoCargo) ;
dispatchLookup["isEmptyStr"] = new dispatcher(isEmptyStr) ;
dispatchLookup["isnumcredito"] = new dispatcher(isnumcredito) ;
dispatchLookup["iscutTDC"] = new dispatcher(iscutTDC) ;
dispatchLookup["isDigVer"] = new dispatcher(isDigVer) ;
dispatchLookup["isValFolioSeg"] = new dispatcher(isValFolioSeg) ;
dispatchLookup["isnumdpa"] = new dispatcher(isnumdpa) ;
dispatchLookup["ValRel5503"] = new dispatcher(ValRel5503) ;
dispatchLookup["UserRACF"] = new dispatcher(UserRACF) ;
dispatchLookup["ValCFE"] = new dispatcher(ValCFE) ;
dispatchLookup["valRefSat"] = new dispatcher(valRefSat) ;
dispatchLookup["isValidaSua"] = new dispatcher(isValidaSua) ;

//SE MODIFICA PARA CORRECCION MONTO EN CERO TXN 4455
dispatchLookup["isMenorIgual4455"] = new dispatcher(isMenorIgual4455) ;
dispatchLookup["isSerie"] = new dispatcher(isSerie) ;
dispatchLookup["isMsgComprobante"] = new dispatcher(isMsgComprobante);
dispatchLookup["isTarjetaTDI"] = new dispatcher(isTarjetaTDI);

dispatchLookup["isnumorden"] = new dispatcher(isnumorden);
dispatchLookup["isnum"] = new dispatcher(isnum);
dispatchLookup["isValidKroner"] = new dispatcher(isValidKroner);
dispatchLookup["isSIB"] = new dispatcher(isSIB);
dispatchLookup["isValidARP"] = new dispatcher(isValidARP);
dispatchLookup["isValidDocsATC"] = new dispatcher(isValidDocsATC);
dispatchLookup["isAlphaNumeric"] = new dispatcher(isAlphaNumeric);

// =============================================================================================
// Definición de mensajes de error
// IMPORTANTE: Si agrega una nueva "Función de Validación", agregue un nodo para dicha "Función de Validación"
// en el arreglo 'messageLookup' (Siguiente seccion).
// =============================================================================================
var messageIni = new Array() ;
messageIni["isnumeric"] = new String("Valor inválido sólo números, verifique...") ;
messageIni["isValidDesc1795"] = new String("Descripción generica no valida...\nverifique con el Administrador") ;
messageIni["isValidDesc1797"] = new String("Descripción generica no valida...\nverifique con el Administrador") ;
messageIni["isValidDesc1295"] = new String("Descripción generica no valida...\nverifique con el Administrador") ;
messageIni["isCurrenNoCero"] = new String("Monto no válido. Verifique...") ;
messageIni["PasswordOK"] = new String("El Password no coincide, verifique...") ;
messageIni["isnumchec"] = new String("Debe Indicar el numero de cheques. Verifique...") ;
messageIni["isafore"] = new String("Total de documentos es diferente a monto. Verifique...") ;
messageIni["isCurrency"] = new String("Cantidad no valida. Verifique...") ;
messageIni["isleetarj"] = new String("Información de Codigo de Barras Invalida. Verifique...") ;
messageIni["isInfonavit"] = new String("Numero de Expediente Invalido, verifique ...") ;
messageIni["isCBA"] = new String("Tipo de Cuenta Invalido, verifique ...") ;
messageIni["ValidNAFIN"] = new String("Longitud de campo invalida, verifique...") ;
messageIni["EfecSAR"] = new String("El Efectivo no puede ser mayor al Monto. Por favor verifique...") ;
messageIni["SumSAR"] = new String("Cantidad no valida. Por favor verifique...") ;
messageIni["SumTotSAR"] = new String("Cantidad no valida. Por favor verifique...") ;
messageIni["iscontrase"] = new String("Contraseña Invalida. Verifique...") ;
messageIni["isValAfore"] = new String("Número de Cuenta de Afore es Invalido. Verifique...") ;
messageIni["isValTarj"] = new String("Número de Tarjeta es Invalido. Verifique...") ;
messageIni["isValNIPs"] = new String("Los números de NIPs deben ser iguales\nPor favor verifique.") ;
messageIni["isaniPago"] = new String("Valor Invalido. Verifique...") ;
messageIni["isSAAC"] = new String("Solo S o N. Verifique...") ;
messageIni["isIngresos"] = new String("Referencia o Folio Invalido. Verifique...") ;
messageIni["isNumReg"] = new String("Numero de Registro Invalido. Verifique...") ;
messageIni["isEgresos"] = new String("Numero de Referencia Invalida. Verifique...") ;
messageIni["isBanMag"] = new String("Numero de banda magnética invalido. Verifique...") ;
messageIni["isReferTIP"] = new String("Referencia inválida. Por favor verifique...") ;
messageIni["isRefer"] = new String("Referencia Inválida, verifique...") ;
messageIni["isBimestre"] = new String("Bimestre Invalido, verifique...") ;
messageIni["isSAR"] = new String("El Total de Efectivo y/o Monto es Invalido, verifique...") ;
messageIni["isValRFC"] = new String("R.F.C. Invalido, verifique...") ;
messageIni["isBenef"] = new String("Identificacion del cliente no existe, verifique...") ;
messageIni["isTipPago"] = new String("Debe capturar numero de cuenta, verifique...") ;
messageIni["isValidDias"] = new String("Numero de dias no válido. Por favor verifique...") ;
messageIni["isFolioSerb"] = new String("Numero de Folio debe ser de 6 digitos, verifique...") ;
messageIni["istelmex"] = new String("Numero de Referencia no valido, verifique...") ;
messageIni["isValidHora"] = new String("Hora Invalida, verifique ...") ;
messageIni["isAutoriCV"] = new String("Introduzca Autorizacion (5 digitos). Por favor verifique.") ;
messageIni["isTipoCambioCV"] = new String("Tipo de Cambio no valido. Verifique ...") ;
messageIni["isChequeCaja"] = new String("Longitud de campo invalida, verifique...") ;
messageIni["isValidCheque"] = new String("Error en la Banda del Cheque...") ;
messageIni["isValidChequeDev"] = new String("Error en la Banda del Cheque.") ;
messageIni["isDecInteger"] = new String("El monto ingresado no es valido\no la cantidad excede a la permitida") ;
messageIni["isValidSer4061"] = new String("Serial Invalido, verifique...") ;
messageIni["isValidAcct"] = new String("El numero de Cuenta no es valido. Por favor verifique...") ;
messageIni["isValidCta"] = new String("El numero de Cuenta no es valido. Por favor verifique...") ;
messageIni["isACDO"] = new String("Se debe digitar el número de Folio.") ;
messageIni["isBancoRem"] = new String("Opción inválida \nPor favor seleccione otro banco.") ;
messageIni["isValidDesc1523"]  = new String("Descripcion invalida.") ;
messageIni["isValidDesc1197"]  = new String("Descripcion invalida para esta transaccion.") ;
messageIni["isValidDesc5903"]  = new String("Descripcion invalida para esta transaccion.") ;
messageIni["isValidDesc4503"] = new String("Monto Inválido o Monto Rebasa el Máximo Permitido. Verifique...") ;
messageIni["isValidDate"] = new String("La fecha ingresada no es valida, utilice el formato dd/mm/aaaa\n o la fecha es mayor a la del día.") ;
messageIni["isRegistro"] = new String("Registro no válido, por favor verifique...\nLongitud de 7 posiciones, rellene con ceros.") ;
messageIni["isIDCajero"] = new String("Usuario de Cajero no válido, por favor verifique...") ;
messageIni["isCurr8"] = new String("Cantidad no válida, por favor verifique...") ;
messageIni["isRFCCF"] = new String("RFC requerido, por favor verifique...") ;
messageIni["isAbonoAcct"] = new String("Cuenta de Abono igual que Cargo o invalida, verifique...") ;
messageIni["verPrint"] = new String("") ;
messageIni["isValidChoice"] = new String("") ;
messageIni["toUpperCase"] = new String("") ;
messageIni["CtaCargo"] = new String("El numero de Cuenta no es valido\nPor favor verifique...") ;
messageIni["isValidaFirma"] = new String("Firma de Funcionario Invalido o Requiere Firma, verifique ....") ;
messageIni["isMontoChCaja"] = new String("Monto válido para expedición de cheque de caja ...") ;
messageIni["isValidMonto"] = new String("Introduzca Monto o Monto Invalido....") ;
messageIni["MtoCargo"] = new String("Cantidad no valida\nPor favor verifique.") ;
messageIni["Cheque"] = new String("Cheque, Por favor verifique.") ;
messageIni["ChequeD"] = new String("ChequeD, Por favor verifique.") ;
messageIni["isACorte"] = new String("Error .. Seleccione Antes o Despues del Corte") ;
messageIni["isValidCurrProc"] = new String("Moneda no permitida para este proceso.") ;
messageIni["isValidAuthori"] = new String("Autorización inválida.") ;
messageIni["isCodeSeg"] = new String("Codigo de Seguridad invalido...") ;
messageIni["isValidMto"] = new String("Seleccione primero el numero....") ;
messageIni["isValidNo"] = new String("Capture monto...") ;
messageIni["isTDC"] = new String("Numero de TDC no valido, verifique...") ;
messageIni["notChange"] = new String("El campo no puede ser modficado...") ;
messageIni["isValMtoRAPL"] = new String("Monto no válido...") ;
messageIni["isValMto"] = new String("Monto no válido...") ;
messageIni["isValServ"] = new String("Servicio capturado no válido...") ;
messageIni["isValMtoRAP"] = new String("Monto debe ser mayor a 0 para este Servicio ...") ;
messageIni["isValMto5503"] = new String("Monto no válido...") ;
messageIni["Valid5503"] = new String("El pago con Cobro Inmediato, solo es permitido para un servicio...") ;
messageIni["ValSum"] = new String("El Monto total no ha sido cubierto...") ;
messageIni["MtoServ"] = new String("Monto no válido...") ;
messageIni["ValRef40"] = new String("Longitud de Referencia no válida...") ;
messageIni["ValRefFija"] = new String("Longitud de Referencia no válida...") ;
messageIni["FlujoRAP"] = new String("") ;
messageIni["isMontoRAP"] = new String("Monto invalido o Monto rebasa el Monto Total...") ;
messageIni["Ref1880"] = new String("Referencia no valida...") ;
messageIni["TDCAmex"] = new String("Numero de Tarjeta no valido...") ;
messageIni["ValTDC"] = new String("Numero de Tarjeta no valido...") ;
messageIni["ValRem"] = new String("El pago con Remesas solo se aplica con un Servicio y un Pago..") ;
messageIni["MtoCOPM"] = new String("Cantidad no valida\nPor favor verifique.") ;
messageIni["wichForPago"] = new String("Campos no deben contener datos.") ;
//messageIni["wichLiqBank"] = new String("Campos no deben contener datos.") ;
messageIni["isSerial"] = new String("Serial no valido") ;
//ygx
messageIni["isSerialCheq"] = new String("Serial no valido") ;

messageIni["isMontoCargo"] = new String("Monto Invalido o Monto Rebasa el Monto Total de Cheques, verifique ....") ;
messageIni["isEmptyStr"] = new String("El campo es requerido") ;
messageIni["isnumcredito"] = new String("Número de Crédito Invalido, verifique.") ;
messageIni["iscutTDC"] = new String("Número de Crédito Invalido, verifique.") ;
messageIni["isDigVer"] = new String("Linea de Captura Invalida, verifique.") ;
messageIni["isnumdpa"] = new String("Clave DPA no valido, verifique.") ;
messageIni["isValFolioSeg"] = new String("Los numeros de folio no coinciden, verifique.") ;
messageIni["ValRel5503"] = new String("El pago en ceros no es permitido para este tipo de relacion, Verifique...") ;
messageIni["UserRACF"] = new String("Usuario inválido \nPor favor verifique.") ;
messageIni["ValCFE"] = new String("El Monto no coincide, Verificar...") ;
messageIni["valRefSat"] = new String("La referencia debe ser de 20 posiciones, Verifique...") ;

messageIni["isValidaSua"] = new String("Seleccione Tipo de Pago, verifique.") ;

//SE MODIFICA PARA CORRECCION MONTO EN CERO TXN 4455
messageIni["isMenorIgual4455"] = new String("Pago en efectivo MAYOR al Total") ;

messageIni["isSerie"] = new String("El <REGISTRO> está incorrecto, vuelva a intentarlo.") ;
messageIni["isMsgComprobante"] = new String("Para capturar los datos del cliente favor de seleccionar\n                 \"Comprobante Comisión\"");
messageIni["isTarjetaTDI"] = new String("Tarjeta TDI invalida, verifique.");
messageIni["isnumorden"] = new String("Número de Orden invalido, verifique.");
messageIni["isnum"] = new String("El numero de Folio no es valido. Por favor verifique...");
messageIni["isValidKroner"] = new String("El numero de Crédito no es valido. Por favor verifique...");
messageIni["isSIB"] = new String("Se debe digitar el número de Cuenta.") ;
messageIni["isValidARP"] = new String("Se debe digitar el número de Cuenta.") ;
messageIni["isValidDocsATC"] = new String("El campo es necesario");
messageIni["isAlphaNumeric"] = new String("El campo NO acepta espacios o caracteres especiales, verifique...");


// =============================================================================================
// Arreglo interno para búsqueda de mensajes por "Función de Validación".
// IMPORTANTE: Si requiere modificar un mensaje para alguna "Función de Validación", NO lo defina en
// el arreglo 'messageLookup', definalo en el arreglo 'messageIni' (Seccion anterior).
// =============================================================================================
var messageLookup = new Array() ;
messageLookup["isnumeric"] 	=		messageIni["isnumeric"];
messageLookup["isValidDesc1795"] 	=		messageIni["isValidDesc1795"];
messageLookup["isValidDesc1797"] 	=		messageIni["isValidDesc1797"];
messageLookup["isValidDesc1295"] 	=		messageIni["isValidDesc1295"];
messageLookup["isCurrenNoCero"] =		messageIni["isCurrenNoCero"];
messageLookup["PasswordOK"] 	=		messageIni["PasswordOK"];
messageLookup["isnumchec"] 	=		messageIni["isnumchec"];
messageLookup["isafore"]		=		messageIni["isafore"];
messageLookup["isCurrency"] 	=		messageIni["isCurrency"];
messageLookup["isleetarj"] 	=		messageIni["isleetarj"];
messageLookup["isInfonavit"] 	=		messageIni["isInfonavit"];
messageLookup["isCBA"] 		=		messageIni["isCBA"];
messageLookup["ValidNAFIN"] 	=		messageIni["ValidNAFIN"];
messageLookup["EfecSAR"] 	=		messageIni["EfecSAR"];
messageLookup["SumSAR"] 		=		messageIni["SumSAR"];
messageLookup["SumTotSAR"] 	=		messageIni["SumTotSAR"];
messageLookup["iscontrase"] 	=		messageIni["iscontrase"];
messageLookup["isValAfore"] 	=		messageIni["isValAfore"];
messageLookup["isValTarj"] 	=		messageIni["isValTarj"];
messageLookup["isValNIPs"] 	=		messageIni["isValNIPs"];
messageLookup["isaniPago"] 	=		messageIni["isaniPago"];
messageLookup["isSAAC"] 		=		messageIni["isSAAC"];
messageLookup["isIngresos"] 	=		messageIni["isIngresos"];
messageLookup["isNumReg"] 	=		messageIni["isNumReg"];
messageLookup["isEgresos"] 	=		messageIni["isEgresos"];
messageLookup["isBanMag"] 	=		messageIni["isBanMag"];
messageLookup["isReferTIP"] 	=		messageIni["isReferTIP"];
messageLookup["isRefer"] 	=		messageIni["isRefer"];
messageLookup["isBimestre"] 	=		messageIni["isBimestre"];
messageLookup["isSAR"] 		=		messageIni["isSAR"];
messageLookup["isValRFC"] 	=		messageIni["isValRFC"];
messageLookup["isBenef"] 	=		messageIni["isBenef"];
messageLookup["isTipPago"] 	=		messageIni["isTipPago"];
messageLookup["isValidDias"] 	=		messageIni["isValidDias"];
messageLookup["isFolioSerb"] 	=		messageIni["isFolioSerb"];
messageLookup["istelmex"] 	=		messageIni["istelmex"];
messageLookup["isValidHora"] 	=		messageIni["isValidHora"];
messageLookup["isAutoriCV"] 	=		messageIni["isAutoriCV"];
messageLookup["isTipoCambioCV"] =		messageIni["isTipoCambioCV"];
messageLookup["isChequeCaja"] =		messageIni["isChequeCaja"];
messageLookup["isValidCheque"] =		messageIni["isValidCheque"];
messageLookup["isValidChequeDev"] =		messageIni["isValidChequeDev"];
messageLookup["isDecInteger"] =		messageIni["isDecInteger"];
messageLookup["isValidSer4061"] =		messageIni["isValidSer4061"];
messageLookup["isValidAcct"] 	=		messageIni["isValidAcct"];
messageLookup["isValidCta"] 	=		messageIni["isValidCta"];
messageLookup["isACDO"] 		=		messageIni["isACDO"];
messageLookup["isBancoRem"] 	=		messageIni["isBancoRem"];
messageLookup["isValidDesc1523"] =		messageIni["isValidDesc1523"];
messageLookup["isValidDesc1197"] =		messageIni["isValidDesc1197"];
messageLookup["isValidDesc5903"] =		messageIni["isValidDesc5903"];
messageLookup["isValidDesc4503"] =		messageIni["isValidDesc4503"];
messageLookup["isValidDate"] 	=		messageIni["isValidDate"];
messageLookup["isRegistro"] 	=		messageIni["isRegistro"];
messageLookup["isIDCajero"] 	=		messageIni["isIDCajero"];
messageLookup["isCurr8"] 	=		messageIni["isCurr8"];
messageLookup["isRFCCF"] 	=		messageIni["isRFCCF"];
messageLookup["isAbonoAcct"] 	=		messageIni["isAbonoAcct"];
messageLookup["verPrint"] 	=		messageIni["verPrint"];
messageLookup["isValidChoice"] =		messageIni["isValidChoice"];
messageLookup["toUpperCase"] 	=		messageIni["toUpperCase"];
messageLookup["CtaCargo"] 	=		messageIni["CtaCargo"];
messageLookup["isValidaFirma"] =		messageIni["isValidaFirma"];
messageLookup["isMontoChCaja"] =		messageIni["isMontoChCaja"];
messageLookup["isValidMonto"] =		messageIni["isValidMonto"];
messageLookup["MtoCargo"] 	=		messageIni["MtoCargo"];
messageLookup["Cheque"] 		=		messageIni["Cheque"];
messageLookup["ChequeD"] 	=		messageIni["ChequeD"];
messageLookup["isACorte"] 	=		messageIni["isACorte"];
messageLookup["isValidCurrProc"] =		messageIni["isValidCurrProc"];
messageLookup["isValidAuthori"] =		messageIni["isValidAuthori"];
messageLookup["isCodeSeg"] 	=		messageIni["isCodeSeg"];
messageLookup["isValidMto"] 	=		messageIni["isValidMto"];
messageLookup["isValidNo"] 	=		messageIni["isValidNo"];
messageLookup["isTDC"] 		=		messageIni["isTDC"];
messageLookup["notChange"] 	=		messageIni["notChange"];
messageLookup["isValMto"] 	=		messageIni["isValMto"];
messageLookup["isValMtoRAPL"] 	=		messageIni["isValMtoRAPL"];
messageLookup["isValServ"] 	=		messageIni["isValServ"];
messageLookup["isValMtoRAP"] 	=		messageIni["isValMtoRAP"];
messageLookup["isValMto5503"] =		messageIni["isValMto5503"];
messageLookup["Valid5503"] 	=		messageIni["Valid5503"];
messageLookup["ValSum"] 		=		messageIni["ValSum"];
messageLookup["MtoServ"] 	=		messageIni["MtoServ"];
messageLookup["ValRef40"] 	=		messageIni["ValRef40"];
messageLookup["ValRefFija"] 	=		messageIni["ValRefFija"];
messageLookup["FlujoRAP"] 	=		messageIni["FlujoRAP"];
messageLookup["isMontoRAP"] 	=		messageIni["isMontoRAP"];
messageLookup["Ref1880"] 	=		messageIni["Ref1880"];
messageLookup["TDCAmex"] 	=		messageIni["TDCAmex"];
messageLookup["ValTDC"] 		=		messageIni["ValTDC"];
messageLookup["ValRem"] 		=		messageIni["ValRem"];
messageLookup["MtoCOPM"] 	=		messageIni["MtoCOPM"];
messageLookup["wichForPago"] 	=		messageIni["wichForPago"];
//messageLookup["wichLiqBank"] 	=		messageIni["wichLiqBank"];
messageLookup["isSerial"] 	=		messageIni["isSerial"];
//ygx
messageLookup["isSerialCheq"] 	=		messageIni["isSerialCheq"];

messageLookup["isMontoCargo"] =		messageIni["isMontoCargo"];
messageLookup["isEmptyStr"] 	=		messageIni["isEmptyStr"];
messageLookup["isnumcredito"] = 		messageIni["isnumcredito"] ;
messageLookup["iscutTDC"] 	= 		messageIni["iscutTDC"] ;
messageLookup["isDigVer"] 	= 		messageIni["isDigVer"] ;
messageLookup["isValFolioSeg"] 	= 		messageIni["isValFolioSeg"] ;
messageLookup["ValRel5503"] 	= 		messageIni["ValRel5503"] ;
messageLookup["UserRACF"] 	=		messageIni["UserRACF"];
messageLookup["ValCFE"] 	=		messageIni["ValCFE"];
messageLookup["valRefSat"] 	=		messageIni["valRefSat"];

messageLookup["isValidaSua"] 	= 		messageIni["isValidaSua"] ;
messageLookup["isnumorden"] 	= 		messageIni["isnumorden"] ;
messageLookup["isnum"] 	= 		messageIni["isnum"] ;
//SE MODIFICA PARA CORRECCION MONTO EN CERO TXN 4455
messageLookup["isMenorIgual4455"] 	=		messageIni["isMenorIgual4455"];

messageLookup["isSerie"] 	=		messageIni["isSerie"];
messageLookup["isMsgComprobante"]  = messageIni["isMsgComprobante"];
messageLookup["isTarjetaTDI"]  = messageIni["isTarjetaTDI"];
messageLookup["isValidKroner"]  = messageIni["isValidKroner"];
messageLookup["isSIB"]  = messageIni["isSIB"];
messageLookup["isValidARP"]  = messageIni["isValidARP"];
messageLookup["isValidDocsATC"]  = messageIni["isValidDocsATC"];
messageLookup["isAlphaNumeric"]  = messageIni["isAlphaNumeric"];


// =============================================================================================
// Funciones genéricas del motor de validación
// =============================================================================================


// Función de validación principal llamada desde los manejadores de eventos de las páginas
function validate(frame, field, method)
{
	if(method == ''){
		return true;
	}

	var args = validate.arguments ;

	gFrame = frame ;
	gField = eval("window." + frame.name + ".document.forms[0]." + field.name) ;
	gForm = eval("window." + frame.name + ".document.forms[0]") ;
	for (var i = 2; i < args.length; i++)
	{
	    	gMethod = args[i];
		if (!dispatchLookup[args[i]].doValidate())
		{
			alert(messageLookup[args[i]]) ;

			if (gMethod != 'isValidCheque' || gMethod != 'isValidChequeDev')
			{

				if(gField.length > 1){

					if(gField[0].type == 'radio'){
						gField[0].focus();
					}
				}else{
					if(gField.type != 'hidden')
					{
						gField.focus() ;
					}
				}

			}
			return false  ;
		}
	}
	return true ;
}

// Función que se asocia en la definición de las funciones de validación
function dispatcher(validationFunc)
{
  this.doValidate = validationFunc ;
}

// =============================================================================================
// Funciones específicas de validación
// =============================================================================================

function isnumorden(){

      if (gForm.txtNumOrden.value.length == 12){
          var strtemp = gForm.txtNumOrden.value;
          if(!isNaN(strtemp.substring(0,2)) || isNaN(strtemp.substring(2))){
              return false;
            }
      }else if (!isNaN(gForm.txtNumOrden.value) && gForm.txtNumOrden.value.length <= 8 && gForm.txtNumOrden.value.length >= 4){
             for(var i=gForm.txtNumOrden.value.length;i<8;i++){
                gForm.txtNumOrden.value = "0" + gForm.txtNumOrden.value;
             }
      }else{
          return false;
      }
 gForm.txtNumOrden.value = gForm.txtNumOrden.value.toUpperCase();
 return true;
}

function isnum(){
 if(gField.value.length > 0){
   if(!isInteger(gField.value)){
     	gField.value = "";
    	return false;
   }
 }else{
	gField.value = "";
	return false;
 }

return true
}

function isDigVer()
{
	messageLookup["isDigVer"] =	messageIni["isDigVer"];
	var linea;
	var suma = 0;
	var digito;
	var digito2;
	var digito3;

	if(gField.value.length != 3)
		return false;

	if(gForm.txtConsec0530.value.length == 1)
		linea = gForm.txtConsec0530.value;
	else
	{
		gField.value = "";
		return false;
	}
	if(gForm.txtCentroPago.value.length == 7)
		linea = linea + gForm.txtCentroPago.value;
	else
	{
		gField.value = "";
		return false;
	}
	if(gForm.txtIcefa.value.length == 3)
		linea = linea + gForm.txtIcefa.value;
	else
	{
		gField.value = "";
		return false;
	}
	if(gForm.txtRetiro.value.length == 11)
		linea = linea + gForm.txtRetiro.value;
	else
	{
		gField.value = "";
		return false;
	}
	if(gForm.txtVivienda.value.length == 11)
		linea = linea + gForm.txtVivienda.value;
	else
	{
		gField.value = "";
		return false;
	}
	if(gForm.txtTotal.value.length == 12)
		linea = linea + gForm.txtTotal.value;
	else
	{
		gField.value = "";
		return false;
	}
	if(gForm.txtTipoPago.value.length == 1)
		linea = linea + gForm.txtTipoPago.value;
	else
	{
		gField.value = "";
		return false;
	}
	if(gForm.txtBimPago.value.length == 6)
		linea = linea + gForm.txtBimPago.value;
	else
	{
		gField.value = "";
		return false;
	}

	// Primer Digito Verficador
	for (var i = 0; i < linea.length; i++)
		suma = suma + (linea.charCodeAt(i)*i);
	suma = suma % 10;
	digito = gField.value.substring(0,1);
	if(digito != suma)
	{
		gField.value = "";
		return false;
	}
	// Segundo Digito Verficador
	suma = 0;
	linea = gForm.txtRetiro.value + gForm.txtVivienda.value;
	for (var i = 0; i < linea.length; i++)
		suma = suma + (linea.charCodeAt(i)*i);
	suma = suma + digito.charCodeAt(0);
	suma = suma % 10;
	digito2 = gField.value.substring(1,2);
	if(digito2 != suma)
	{
		gField.value = "";
		return false;
	}

	// Tercer Digito Verificador
	suma = 0;
	linea = gForm.txtCentroPago.value;
	for (var i = 0; i < linea.length; i++)
		suma = suma + (linea.charCodeAt(i)*i);

	suma = suma + gForm.txtTipoPago.value.charCodeAt(0);
	suma = suma + gForm.txtConsec0530.value.charCodeAt(0);
	suma = suma + digito.charCodeAt(0);
	suma = suma + digito2.charCodeAt(0);
	suma = suma % 10;
	digito3 = gField.value.substring(2);
	if(digito3 != suma)
	{
		gField.value = "";
		return false;
	}

	return true;
}

function filler(valor, largo)
{
	while( valor.length < largo )
		valor= '0' + valor;
	return valor;
}

function isnumcredito()
{
  // Reinicia el mensaje de error en caso de que haya sido modificado
  messageLookup["isnumcredito"] = messageIni["isnumcredito"];

  if (!isnumeric())
  {
    return false;
  }

  if(gField.value.length > 0 && gForm.txtRfc.value.length > 0)
  {
    messageLookup["isnumcredito"] = new String("Solo debe Capturar un solo campo, Número de crédito o RFC");
    gField.value = '';
    return false;
  }

  if (gForm.clavedpa.value.length > 0 && gField.value.length > 0)
  {
    messageLookup["isnumcredito"] = new String("Solo debe Capturar un solo campo, Clave DPA o Numero de Credito");
    gForm.clavedpa.value = '';
    gField.value = '';
    return false;
  }

  return true;
}



function KeyCheck(window)
{
var key = window.event.keyCode;
if(key == 8)
   	return false;
  else
    return true;

}


function ValCFE()
{
   var campo = gField.name ;
   var valor_campo = gField.value ;
   var fecha = valor_campo.substring(14,20);
   var yearcfe = fecha.substring(0,2);
   var mescfe = fecha.substring(2,4);
   var diacfe = fecha.substring(4,6);
   yearcfe = "20"+yearcfe;

   var monto = valor_campo.substring(20,29);
   monto = Number(monto);
   var monto_capturado = gForm.MtoTotal.value.toString().replace(/\$|\,/g,'') ;
   var ind = monto_capturado.indexOf(".");
   monto_capturado  = monto_capturado.substring(0,ind);


   var d = new Date() ;
   var diasys = Number(d.getDate());
   if (diasys <10 )
      diasys = "0"+diasys;

   var messys = Number(d.getMonth())+1;
   if (messys <10 )
      messys = "0"+messys;

   var yearsys = Number(d.getFullYear());


 var mscfe = Number(Date.UTC(yearcfe,mescfe,diacfe));
 var ms = Number(Date.UTC(yearsys,messys,diasys));
// alert("mscfe:"+mscfe);
// alert("ms:"+ms);


 if(ms > mscfe)
   {
    messageLookup["ValCFE"] = new String("El recibo ya esta vencido");
    gField.value = '';
    return false;
   }






   if (monto_capturado != monto)
      {
      messageLookup["ValCFE"] = new String("El Monto no coincide, Verificar...");
      gField.value="";
      return false;
      }



  return true;
}




function iscutTDC()
{
  messageLookup["iscutTDC"] = messageIni["iscutTDC"];

  if (gField.value.length >= 60)
  {
    tdc = gField.value.substring(2,18);
    gField.value = "";
    return true;
  }

  if (tdc.length > 0 && gField.value.length != 16)
    gField.value = tdc;

  if (ValTDC())
    return true;
  else
    return false;
}


// Función que valida que el campo sea numérico
function isnumeric()
{
	messageLookup["isnumeric"] = messageIni["isnumeric"];

	var valor = gField.value;

 	if(!isEmpty(valor))
	{
		if (isNaN(valor))
		{
			gField.value = "";
			return false;
		}
		else
		{
			if(gField.name == "txtIcefa")
			{
				var banco = gField.value;
				if(banco != '021')
				{
					gField.value = "";
					gField.focus();
        				messageLookup["isnumeric"] = new String("Clave de ICEFA incorrecta") ;
        				return false;
        			}
			}

			if(gField.name == "txtTipoPago")
			{
				var tipo = gField.value;
				if(tipo == '1')
				{
        				alert("Verificar Fecha Limite !");
        				return true;
        			}
			}
			if(gField.name == "txtRetiro")
			{
				gField.value = filler(gField.value, 11);
			}

			if(gField.name == "txtVivienda")
			{
				gField.value = filler(gField.value, 11);
			}
			if(gField.name == "txtTotal")
			{
				gField.value = filler(gField.value, 12);
				var tmp = gField.value;
				tmp = Number(tmp);
				gForm.txtMonto.value = Currency(tmp);
			}

			if(gField.name == "txtNoNegocio")
			{
				var size = gField.value.length;
				var val = valor;
				for(i = size; i < 9; i++)
				{
					val = "0" + val;
				}
				gField.value = val;
			}


	            	if(gField.name == "txtFolioSeg")
	            	{
				if ( gField.value.length < 22 )
				{
					messageLookup[gMethod] = new String("Capture Folio númerico a 22 dígitos.\nPor favor verifique.");
					gField.value = "";
					return false;
				}
			}
			if(gField.name == "txtRefer0124")
			{
				if ( gField.value.length < 15 )
				{
					messageLookup[gMethod] = new String("Capture Referencia númerica a 15 dígitos.\nPor favor verifique.");
					gField.value = "";
					return false;
				}
				else if(gField.value.length == 15 && valor == 0 )
				{
					messageLookup[gMethod] = new String("Capture Referencia númerica diferente de cero.\nPor favor verifique.");
					gField.value = "";
					return false;
				}
			}
			if(gField.name == "txtFolioAvaluo" && valor == 0)
			  {
			   messageLookup[gMethod] = new String("Capture Número de Folio diferente de cero.\nPor favor verifique.");
			   gField.value = "";
				return false;
			  }
// VALIDACIÓN DE NÚMERO DE FOLIO 0238: ETV
			if(gField.name == "txtNumFolio" && valor <= 0)
			  {
			   messageLookup[gMethod] = new String("Capture Número de Folio mayor que cero.\nPor favor verifique.");
			   gField.value = "";
				return false;
			  }

		}
		return true;
	}

	return false;
}

// Función que valida que la cantidad capturada sea mayo que cero
function isCurrenNoCero()
{
    messageLookup["isCurrenNoCero"] =	messageIni["isCurrenNoCero"];

    var num = gField.value.toString().replace(/\$|\,/g,'') ;
    if( isNaN(Number(num)) || Number(num)==0)
    {
        gField.value = "" ;
        gField.focus() ;
        messageLookup["isCurrenNoCero"] = new String("Monto no válido...") ;
        return false ;
    }
    intVal = num.substring(0, num.indexOf(".")) ;
    decVal = num.substring(num.indexOf(".") + 1, num.length) ;
    num = intVal + decVal ;

    var wasNegative = false;
    if(isNegative(num))
    {
        num = num.toString().replace(/-/g,'') ;
        wasNegative = true;
    }

    maxlength = 13 ;
    if ( gField.name.substring(0,8) == "txtEfect")
        maxlength = 11 ;

    if ( gField.name == "txtTipoCambio1")
        maxlength = 18;

    if (num.length > maxlength)
    {
        num = num.substring(0, maxlength) ;
        if(Number(num)!=0)
            gField.value=Currency(num) ;
        else
            gField.value="" ;
        gField.focus() ;
        messageLookup["isCurrenNoCero"] = new String("Cantidad demasiado grande. Favor de verificar.") ;
        return false ;
    }

    cents = Math.floor((num)%100) ;
    num = Math.floor((num)/100).toString() ;

    if(cents < 10)
        cents = "0" + cents ;

    for (var i = 0; i < Math.floor((num.length-(1+i))/3); i++)
        num = num.substring(0,num.length-(4*i+3))+','+num.substring(num.length-(4*i+3)) ;

    gField.value = num + '.' + cents ;

    if (wasNegative)
    {
        alert("No se permiten números negativos.") ;
    }

    return true;
}

// Función que valida la confirmación de un password. Asume un nombre psswordC para el control del segundo password
function PasswordOK()
{
    messageLookup["PasswordOK"] = messageIni["PasswordOK"];

	var conf = gForm.psswordC.value;
	var pass = gField.value;
	if(conf == pass)
		return true;

	return false;
}

// Función que valida que el argumento sea un número de cheque válido
function isnumchec()
{
    messageLookup["isnumchec"] = messageIni["isnumchec"];

    var chqotros = parseInt(eval("gForm.txtChqsOtros.value").toString().replace(/\$|\,|\./g,''));
    var numchec = gField.value;

    if (parseInt(chqotros) > 0 && numchec == "")
		return false;

    if (numchec == "")
		gField.value = "0";

    if (!isnumeric())
		return false;

    return true;
}

// Función que valida que el total de documentos y efectivo sea correcto y calcula la suma.
function isafore()
{
    messageLookup["isafore"] =			messageIni["isafore"];

	if (!isCurrency()){
		gField.value = '';
		recalculaAfore();
		return false;
	}

    var efectivo = gForm.txtEfectivo.value.toString().replace(/\$|\,|\./g,'');
    efectivo = Number(efectivo);
    chqbital = gForm.txtChqsBital.value.toString().replace(/\$|\,|\./g,'');
    chqbital = Number(chqbital);
    chqotros = gForm.txtChqsOtros.value.toString().replace(/\$|\,|\./g,'');
    chqotros = Number(chqotros);

    total = efectivo + chqbital + chqotros;
    total = Currency(total);
    gForm.txtMonto.value = total;
    if (gForm.txtMonto.value <= 0)
    {
      gForm.txtMonto.value = '';
      messageLookup["isafore"] = new String("El monto no puede ser cero. Verifique...")
      return false;
    }
    return true;
}

//función para recalcular el valor del monto de la afore en la transacción 0069
function recalculaAfore(){

    var efectivo = gForm.txtEfectivo.value.toString().replace(/\$|\,|\./g,'');
    efectivo = Number(efectivo);
    var chqbital = gForm.txtChqsBital.value.toString().replace(/\$|\,|\./g,'');
    chqbital = Number(chqbital);
    var chqotros = gForm.txtChqsOtros.value.toString().replace(/\$|\,|\./g,'');
    chqotros = Number(chqotros);
    var total = efectivo + chqbital + chqotros;
    total = Currency(total);
    gForm.txtMonto.value = total;


}


////Función que Valida la referencia de la txn 0027 del SAT
function valRefSat()
{
messageLookup["valRefSat"] = messageIni["valRefSat"];
 var campo = gField.name ;
 var valor_campo = gField.value;
 valor_campo = valor_campo.toUpperCase();
 gField.value = valor_campo;
 var lon_campo = gField.length ;

 if(valor_campo.length != 20)
   {
    return false;
   }
 else
     {
    var buf = valor_campo.match("[A-Z0-9]*");
    if( buf != valor_campo)
    {
     messageLookup["valRefSat"] = new String("En Referencia no se permiten caracteres especiales, Favor de Verificar...");
    gField.value ="";
    gField.focus();
    return false;
    }
    else
    return true;

    }
}


// Función que valida que la cantidad capturada corresponda a un importe monetario
function isCurrency()
{
    messageLookup["isCurrency"] = messageIni["isCurrency"];



    var num = gField.value.toString().replace(/\$|\,/g,'') ;
    //alert (num);
    var intVal = num.substring(0, num.indexOf(".")) ;
    var decVal = num.substring(num.indexOf(".") + 1, num.length) ;
    num = intVal + decVal;
    var maxlength = 13;


    if(txn == '5179' || txn == '5183'){
    	maxlength = 18;
    	//alert(maxlength);
    }


    if(gForm.iTxn != undefined)
    {
		var txn = gForm.iTxn.value;
		if(gForm.oTxn != undefined)
			var otxn = gForm.oTxn.value;


		if(txn == '0027')
		{
            if((num.length == 12) || (num.length == 13))
              {
              messageLookup["isCurrency"] = new String("Importe No Permitido, solo se puede pagar por Medios Electronicos");
              gField.value = '';
              return false;
              }

            if(num.length == 14)
              {
              messageLookup["isCurrency"] = new String("Importe de pago no permitido");
              gField.value = '';
              return false;
              }

           if(!isCurrenNoCero())
		   {
		   gField.value = '';
           return false;
           }
       }


		if (otxn == '0124')
                   {
		   if(gForm.txtMonto0124.value.replace(/\$|\,|\./g,'') > 100000  )
		      {
		      messageLookup["isCurrency"] = new String("Cantidad no valida, el Monto excede el limite permitido, Verifique.");
		      gField.value = '';
		      return false;
		     }else if( gForm.txtMonto0124.value.replace(/\$|\,|\./g,'') < 100  )
		   		{
		   		gField.value = '';
		   		return false;
		   		}
		   }



		if(txn == '0065' || txn == '4041' || txn == '4107' || txn == '4157' || txn == '0606')
		{
    			if(num.length == 0 || Number(intVal + decVal) == 0)
    			{
    				gField.value = '';
    				return false;
    			}
    		}

		if (gForm.cTxn != undefined)
		{
		   if (gForm.cTxn.value == '1053')
		   {
             var doctos = gForm.lstTipoDocto;
             var options = gForm.lstTipoDocto.options.length;
             var montoretiro = 0;
             if (gForm.moneda.value == '01')
                montoretiro = gForm.MontoRetiroPesos.value.replace(/\$|\,|\./g,'');
             else
                montoretiro = gForm.MontoRetiroDolar.value.replace(/\$|\,|\./g,'');
             if (Number(intVal + decVal) < Number(montoretiro))
             {
               if (doctos.options[options -1].value != "IDXF")
               {
 			    doctos.options[options] = new Option();
			    doctos.options[options].text = "No Requiere" ;
		        doctos.options[options].value = "IDXF";
		       }
		     }
		     else
		     {
               if (doctos.options[options-1].value == "IDXF")
                doctos.options[options-1] = null;
             }
		   }
		   if ( (txn =='4009' && gForm.cTxn.value == '0528')  ||
			(txn == '4529' && gForm.cTxn.value == '4529') ||
			(gForm.cTxn.value == 'M015'))
			  if (num.length == 0 || Number(intVal + decVal) == 0)
			  {
			     gField.value = '';
				 return false;
			  }
		}




		if (txn == '0061' && gField.name == 'txtImporteIVA')
		{
			if (num.length > 11)
			{
				gField.value = '';
				return false;
			}
		}

		if (gField.name == 'txtImporteIVA')
		{
			var dejarpasar = 'no';
			if (gForm.ComproFiscal != undefined)
				if (gForm.ComproFiscal.selectedIndex == 0)
				{
					dejarpasar = 'si';
					gField.value = '0.00';
					num = 0;
				}

			if (gForm.cTxn != undefined)
				if (gForm.cTxn.value == '0814')
				    dejarpasar = 'si';

			if (num == 0 && dejarpasar == 'no')
			{
				gField.value = '';
			    	return false;
			}
		}

		if (gField.name == 'txtIVA')
		{
			if (gForm.lstCF != undefined)
			{
			    if( isNaN(num) )
			    {
					gField.value = "0.00";
					return false;
			    }

				if (num == 0 && gForm.lstCF.selectedIndex == 1)
				{
					gField.value = '';
					return false;
				}

				if (num > 0 && gForm.lstCF.selectedIndex == 0)
				{
				    num = 0;
					gField.value = '0.00';
				}
			}
		}

	    if (txn == '0630')
          if (gForm.oTxn != undefined)
          {
            if ( gForm.oTxn.value == '0476' && Number(intVal + decVal) > 10000000)
            {
    			 messageLookup["isCurrency"] = new String("El Monto excede el limite permitido");
			 gField.value = '';
                return false;
            }

            if ( (gForm.oTxn.value == '0476' || gForm.oTxn.value == '0482' || gForm.oTxn.value == '0492')
                && Number(intVal + decVal) == 0)
            {
			    gField.value = '';
                return false;
            }
          }


	    if (txn == '0218' || txn == '1057' || txn == '4107')
	    {
          if (gForm.oTxn != undefined)
          {
            if ( ('0490') && Number(intVal + decVal) == 0)
            {
			    gField.value = '';
                return false;
            }
          }
		}
    }

    if (( gField.name.substring(0,8) == "txtEfect") || (gField.name.substring(0,12) == "txtMonto0124"))
        maxlength = 11;

    if ( gField.name == "txtTipoCambio1")
        maxlength = 18;

    if (num.length > maxlength)
    {
		gField.value = "0.00" ;
        alert ("Cantidad demasiado grande. Favor de verificar.") ;
        num = num.substring(0, maxlength) ;
        return false ;
    }

    if( isNaN(num) )
    {
        gField.value = '' ;
        return false ;
    }

    var wasNegative = false;
    if(isNegative(num))
    {
		num = num.toString().replace(/-/g,'') ;
		wasNegative = true;
    }

    cents = Math.floor((num)%100) ;
    num = Math.floor((num)/100).toString() ;
    if(cents < 10)
        cents = "0" + cents ;

    for (var i = 0; i < Math.floor((num.length-(1+i))/3); i++)
        num = num.substring(0, num.length - (4*i+3)) + ',' + num.substring(num.length - (4*i+3)) ;

    gField.value = num + '.' + cents ;

    if(txn == '0027' && cents != '00')
      {
      gField.value = "";
      return false
      }

    if (wasNegative)
    {
		messageLookup[gMethod] = "No se permiten números negativos.";
        return false;
    }

    if(gField.name == "txtMonto" ){
	  if(txn == '0228' || txn == '0468' || txn == '1197' || txn == '1641' || txn == '0606' ||
	     txn == '0602' || txn == '1191' || txn == '5627' || txn == '5621' || txn == '5219' ||
         txn == '1653' || txn == '1651' || txn == '1173' || txn == '1175' || txn == '1177' ||
         txn == '0238' || txn == '5177' || txn == '5179' || txn == '5161' || txn == 'SDCG' ||
         txn == 'SLCG')
		 {

		 if(!isCurrenNoCero())
		   {
            gField.value = '';
           return false;
           }
           }
		 }
		 if(gField.name == "txtEfectivo")
		{
		 	if(txn == '0222' || txn == '0224')
		 {
		   if(!isCurrenNoCero())
		   {
            gField.value = '';
           return false;
           }
		 }
		 }
//-----FIN MODIFICACION
	 if(gField.name == "txtTipoCambio1")
	  {
		if(txn == '5179' || txn == '5183')
		  {


			if(!isCurrenNoCero())
			  {
        	   gField.value = '';
	           return false;
           	  }

           	if(!isTipoCambio(intVal,decVal)){
           		gField.value = '';
		        return false;
		    }
		    var limint = 8 - intVal.length;
		    var limdec = 9 - decVal.length;

		    for (i = 1; i <= limint; i++){
		    	intVal = "0" + intVal;

		    }

		    for (i = 1; i <= limdec; i++){
		    	decVal = decVal + "0";

		    }

		    gField.value = intVal + "." + decVal;

		  }
	 }

	 if(gField.name == "txtMontoEnv")
	  {
		if(txn == '5179' || txn == '5183')
		  {
			if(!isCurrenNoCero())
			  {
        	   gField.value = '';
	           return false;
           	  }
		  }
	 }

	 if(gField.name == "txtTotalSUAS")
	  {
		if(!isCurrenNoCero())
		  {
       	   gField.value = '';
           return false;
		  }
	 }

	var num_SIB = gField.value.toString().replace(/\$|\,/g,'') ;
    var intVal_SIB = num_SIB.substring(0, num_SIB.indexOf(".")) ;
    var decVal_SIB = num_SIB.substring(num_SIB.indexOf(".") + 1, num_SIB.length) ;
    num_SIB = intVal_SIB + decVal_SIB;
	 if (gForm.cTxn.value == 'CSIB' && num_SIB < 1000000)
     {
		      messageLookup["isCurrency"] = new String("El monto digitado es incorrecto, verifcar...");
		      gField.value = '';
		      return false;
	 }
	 if (gForm.cTxn.value == '0272' && num_SIB == 0)
	 {
		      messageLookup["isCurrency"] = new String("El monto digitado es incorrecto, verifcar...");
		      gField.value = '';
		      return false;
	 }
	 if (gForm.cTxn.value == '0272' && num_SIB > 100000)
	 {
		      messageLookup["isCurrency"] = new String("No se pueden cambiar más de 1,000 USD de cheques de viajero en efectivo");
		      gField.value = '';
		      return false;
	 }

	if (gForm.cTxn.value == '0814' && gField.value <= 0)
	{
		      messageLookup["isCurrency"] = new String("Monto invalido, corregir");
		      gField.value = '';
		      return false;
	 }

	if (gForm.cTxn.value == '2215' && gField.value <= 0)
	{
		      messageLookup["isCurrency"] = new String("Monto invalido, corregir");
		      gField.value = '';
		      return false;
	 }

	 if (gForm.cTxn.value == '2217' && gField.value <= 0)
	{
		      messageLookup["isCurrency"] = new String("Monto invalido, corregir");
		      gField.value = '';
		      return false;
	 }

	if (gForm.cTxn.value == '2219' && gField.value <= 0)
	{
		      messageLookup["isCurrency"] = new String("Monto invalido, corregir");
		      gField.value = '';
		      return false;
	 }
    return true ;
}


function isTipoCambio(intVal1,decVal1){

	var temp = intVal1;
	var temp2 = decVal1;

	if(temp.length > 8 || temp2.length > 9){
		return false;
	}
	else
		return true;
}

// Función para validar que se haya capturado un número de tarjeta válido
function isleetarj()
{
    messageLookup["isleetarj"] = messageIni["isleetarj"];
    var valor = gField.value;
    if (valor.length < 26 && gForm.txtRefer16.value.length == 0)
      return false;
    var tipo = valor.substring(0,1);
    var keyuno = valor.substring(1,14).toUpperCase();
    var folio  = valor.substring(14).toUpperCase();
    gForm.txtRefer16.value = folio;
    switch (Number(tipo))
	{
			case 3: //creditos fiscales
			    var ischar = keyuno.substring(0,2);
			    if (isNaN(Number(ischar)))
                  gForm.txtRfc.value = keyuno;
                else
                  gForm.numcredito.value = keyuno;
				break ;
			case 5: //DPA
			    var ischar = keyuno.substring(0,2);
			    if (isNaN(Number(ischar)))
                  gForm.txtRfc.value = keyuno;
                else
                  gForm.clavedpa.value = keyuno.substring(0,9)
				break ;
			default: //Provisionales, del Ejercicio, Coordinados
			    gForm.txtRfc.value = keyuno;
	}
    gField.value = "";
    return true;
}

// Función que valida que el número capturado sea un expediente valido de Infonavit
function isInfonavit()
{
    messageLookup["isInfonavit"] = messageIni["isInfonavit"];

	if (!isnumeric())
		return false ;

	var valor = gField.value ;
	var t = 0 ;
	var z, c, r ;
	for (z=0; z<valor.length - 1; z++)
	{
		c = valor.substring(z,z+1) ;
		t = parseInt(t) ;
		c = parseInt(c) ;
		t = t + ((9 - z) * c) ;
	}
	r = (1100- t) % 11 ;
	if (r != parseInt(valor.substring(valor.length-1))){
		gField.value = "";
		return false ;
	}

	return true ;
}

// Función con descripción pendiente
function isCBA()
{
    messageLookup["isCBA"] = messageIni["isCBA"];

    cuenta = gField.value;
    name = gField.name;
	tipcta = cuenta.substring(0,3);
    cta = "0";

	if ( (gForm.cTxn.value == "5505" && name == "txtCuentaCBA_C") ||
             (gForm.cTxn.value == "5509" && name == "txtCuentaCBA_A") )
		{cta = "1";}

    if ( (gForm.cTxn.value == "5505" && name == "txtCuentaCBA_A") ||
             (gForm.cTxn.value == "5509" && name == "txtCuentaCBA_C") )
		{cta = "2";}

	if (tipcta != "809" && cta == "1")
	{
	    gField.value="";
	    gField.focus();
	    return false;
	}

	if (tipcta == "809" && cta == "2")
	{
	    gField.value="";
	    gField.focus();
	    return false;
	}

	if (!isValidAcct())
	{
	    return false;
	}
	return true;
}

//Función que valida la longitud de un campo de NAFIN
function ValidNAFIN()
{
    messageLookup["ValidNAFIN"] = messageIni["ValidNAFIN"];

	var name = gField.name ;
	var valor = gField.value ;
	var large = 0 ;

	if(name == 'txtDepositante')
		large = 156 ;
	if(name == 'txtADisposicion')
		large = 156 ;
	if(name == 'txtDepositante2')
		large = 52 ;
	if(name == 'txtConcepto')
		large = 253 ;
	if(name == 'txtAutoridad')
		large = 123 ;

	valor = valor.toUpperCase();
	valor = valor.toString().replace(/\r|\n/g, ' ');
	Expr=/["'@^|~#]/g;

	if(valor.length > large || valor.length == 0)
	{
		return false;
	}
	else if( (name == 'txtDepositante2' || name == 'txtConcepto' || name == 'txtAutoridad') && Expr.test(valor))
	{
		alert("El campo no puede llevar caracteres especiales, se eliminaron.");
		valor = valor.replace(Expr,'');
	}
	gField.value = valor;
	return true;
}

// Función que valida que el efectivo no sea mayor que el monto capturado
function EfecSAR()
{
    messageLookup["EfecSAR"] =			messageIni["EfecSAR"];

	if (!isCurrency())
   		return false ;

    var efe = gForm.txtEfect.value.toString().replace(/\$|\,|\./g,'') ;
    efe = Number(efe) ;
    var mto = gForm.txtMto.value.toString().replace(/\$|\,|\./g,'') ;
    mto = Number(mto) ;

    if(efe > mto)
	{
		gField.value = "0.00" ;
		return false ;
	}
	return true ;
}

// Función que valida que las sumas del SAR estén correctas
function SumSAR()
{
    messageLookup["SumSAR"] = messageIni["SumSAR"];

	if (!isCurrency())
   		return false ;

	var campo = gField.name ;

	if(campo == 'txtPagIMSS' || campo == 'txtRcgIMSS')
		campo = "IMSS" ;
	if(campo == 'txtPagISSSTE' || campo == 'txtRcgISSSTE')
		campo = "ISSSTE" ;
	if(campo == 'txtPagINFONAV' || campo == 'txtRcgINFONAV')
		campo = "INFONAV" ;
	if(campo == 'txtPagFOVI' || campo == 'txtRcgFOVI')
		campo = "FOVI" ;

    var cantA = eval("gForm.txtPag" + campo + ".value").toString().replace(/\$|\,|\./g,'') ;
    cantA = Number(cantA) ;
    var cantB = eval("gForm.txtRcg" + campo + ".value").toString().replace(/\$|\,|\./g,'') ;
    cantB = Number(cantB) ;

    var total = eval("gForm.txtCve" + campo + ".value").toString().replace(/\$|\,|\./g,'') ;
    total = Number(total) ;

	totalAB = cantA + cantB ;

	if(totalAB > total)
	{
		gField.value = "0.00" ;
		return false ;
	}
	return true ;
}

// Función que valida las cantidades de SAR y las suma
function SumTotSAR()
{
    messageLookup["SumTotSAR"] =		messageIni["SumTotSAR"];

    if (!isCurrency())
   	  return false;

	var txn = gForm.cTxn.value ;
	if(txn == '0522')
    {
    	var totA = gForm.txtCveIMSS.value.toString().replace(/\$|\,|\./g,'') ;
    	totA = Number(totA) ;
    	var totB = gForm.txtCveINFONAV.value.toString().replace(/\$|\,|\./g,'') ;
    	totB = Number(totB) ;
	}
	if(txn == '0532')
    {
    	var totA = gForm.txtCveISSSTE.value.toString().replace(/\$|\,|\./g,'') ;
    	totA = Number(totA) ;
    	var totB = gForm.txtCveFOVI.value.toString().replace(/\$|\,|\./g,'') ;
    	totB = Number(totB) ;
	}

	total = totA + totB ;
    total = Currency(total) ;
	gForm.txtMto.value = total ;
	return true;
}

// Función que valida que se haya capturado una contraseña válida
function iscontrase()
{
    messageLookup["iscontrase"] =		messageIni["iscontrase"];

    if (gField.value.toUpperCase() != gForm.passw.value)
    {
	    gField.value = "" ;
		gField.focus() ;
	    return false ;
	}
	gField.value = gField.value.toUpperCase();
	return true ;
}

// Función que valida que el número de Afore capturado sea el correcto
function isValAfore()
{
    messageLookup["isValAfore"] = messageIni["isValAfore"];

	var valor = gField.value ;
	var nombre = gField.name ;

	if(nombre == 'txtAfiliacion')
	{
	   var mask = new Array(1,2,1,2,1,2,1,2,1,2,1);

	   if(mod10(valor,mask))
	      return true;
	   else
	   {
	      	gField.value = "" ;
          	gField.focus();
	      	return false;
	   }
	}


	if(nombre == 'txtCtaAfore')
	{
		if (valor.length != 15)
		{
	    	gField.value="" ;
			gField.focus() ;
	    	return false ;
		}
		valor = valor.substring(0,11) ;
	}



	if (!isLYFmodulo10(valor))
	{
	    gField.value="" ;
		gField.focus();
	    return false ;
    }
	return true ;
}

//  Función que valida un número de tarjeta
function isValTarj()
{
    messageLookup["isValTarj"] =		messageIni["isValTarj"];

	var valor = gField.value ;
    if (valor.length > 16)
    {
        valor = valor.substring(2,18) ;
        gField.value = '' ;
        alert("Verifique numero de Tarjeta :" + valor) ;
        gField.value = valor ;
    }
	if (mod10allcad(valor) != 0 || valor.length != 16)
	{
	    gField.value="" ;
        messageLookup["isValTarj"] = new String("Número de Tarjeta es Invalido. Verifique...") ;
	    return false ;
	}

	if(gForm.txttarjant!=undefined&&gForm.txttarjact.value.length==16)
	{
	    if(gForm.txttarjant.value==gForm.txttarjact.value)
	    {
	        gField.value="" ;
            messageLookup["isValTarj"] = new String("Los números de Tarjetas deben ser diferentes. Verifique...") ;
	        return false ;
	    }
	}
	return true ;
}

// Función que valida que un NIP sea válido
function isValNIPs()
{
    messageLookup["isValNIPs"] =		messageIni["isValNIPs"];

	if(gField.value.length != 4 || !isnumeric())
	{
        if(gField.name=="pssnip")
            gForm.pssconfnip.value ="" ;
	    gField.value="" ;
	    gField.focus() ;
	    messageLookup["isValNIPs"] = new String("El NIP está compuesto de 4 digitos. Por favor verifique...") ;
	    return false ;
	}

    var nip = gForm.pssnip.value ;
    var nipconf = gForm.pssconfnip.value ;
    if(nip.length==nipconf.length)
    {
	    if(nip==nipconf)
	        return true ;
        gForm.pssconfnip.value = "" ;
        gForm.pssconfnip.focus() ;
        messageLookup["isValNIPs"] = new String("Los NIPs deben ser iguales. Por favor verifique...") ;
        return false ;
	}
	return true ;
}

// Función que valida la captura de un monto de pago válido
function isaniPago()
{
    messageLookup["isaniPago"] = messageIni["isaniPago"];

	if (!isnumeric())
	{
		gField.value="" ;
		gField.focus() ;
		return false ;
	}

	var valor = gField.value ;
	if ( parseInt(valor) == 0 )
	{
		gField.value="" ;
		gField.focus() ;
		return false ;
	}
	return true ;
}

function isSAAC()
{
    messageLookup["isSAAC"] = messageIni["isSAAC"];

	campo = gField.name ;
	if (campo != "txtOpcSum")
		isCurrency() ;
	else
	{
		var valor = gField.value ;
		valor = valor.toUpperCase() ;
		gField.value = valor ;
		if (valor != 'N' && valor != 'S')
			return false ;
	}
	return true ;
}

// Valida que el campo ingresos tenga un valor correcto
function isIngresos()
{
    messageLookup["isIngresos"] = messageIni["isIngresos"];

	var cTxn = gForm.cTxn.value ;
	var	valor = gField.value ;
	if ((cTxn == '0456' && valor.length > 7) || (cTxn == '0458' && (valor.length > 10 || !isnumeric()))){

		gField.value = "";
		return false ;
	}

	return true;
}

// Valida que el numero capturado tenga el modulo 10
function isNumReg()
{
    messageLookup["isNumReg"] =	messageIni["isNumReg"];

	var valor = gField.value ;
	if (!isLYFmodulo10(valor))
		return false ;

	return true ;
}

// Valida que la referencia tenga una longitud válida
function isEgresos()
{
    messageLookup["isEgresos"] = messageIni["isEgresos"];

	var cTxn = gForm.cTxn.value ;
	var oTxn = gForm.oTxn.value ;
	var namerefer = gField.name ;
	var valuerefe = gField.value ;
	var sucursal = gForm.sucursal.value ;
	if (namerefer == 'txtRefer10630')
	{
	   if ( (cTxn == '0472' && (!isnumeric() || valuerefe.length != 10 || mod10allcad(valuerefe.substring(2) != 0))) ||
		(cTxn == '0474' && (!isnumeric() || valuerefe.length != 10)) ||
		(cTxn == '0829' && ( valuerefe.length < 13 || validnumtar(valuerefe)!= 0 )) ||
		(cTxn == '0482' && (valuerefe.length > 7 || valuerefe.length <= 0)) ||
		(cTxn == '0496' && (!isnumeric() || valuerefe.length != 10)) ||
		(cTxn == '0492' && (!isnumeric() || valuerefe.length > 10)) ){
			gField.value = "";
			return false ;
		}
		// SE AGREGA VALIDACIÓN PARA 0228-0490 (0228) 29/DIC/05
	    if ((cTxn == '0218' || cTxn == '1057' || cTxn == '4107' || cTxn=='0228') && oTxn == '0490')
		{
			if(valuerefe.length <= 0)
			{
				gField.value = "";
				return false ;
			}
		}
	}
	else
	{
	  if ((valuerefe.length > 10) || (cTxn == '0472' && !isnumeric())){
	  	gField.value = "";
	  	return false ;
	  }

	}
    if ((cTxn == '0492' || cTxn == '0482' || cTxn == '0476') && Number(valuerefe) <= 0)
    {
    		gField.value = "";
    		return false ;
    }
	// SE AGREGA VALIDACIÓN PARA 0228-0490 (0228) 29/DIC/05
    if (((cTxn == '0218' || cTxn == '1057' || cTxn == '4107' || cTxn == '0228') && oTxn == '0490') && Number(valuerefe) <= 0)
	{
		gField.value = "";
		return false ;
	}

	return true ;
}

// Valida un número de banda magnética
function isBanMag()
{
	messageLookup["isBanMag"] =	messageIni["isBanMag"];

	var d = new Date() ;
	var credito, factori, tipoinf, mesinf, resto, digver, digsum, pago, tti ;
	var referenc = gField.value ;
	var infdia = d.getDate() ;
	var infmes = (d.getMonth() + 1) ;
	var infyear = d.getFullYear() ;
	var infsmdf = gForm.cantidad.value ;

	// corta la manda si se paso por la lectora
    if (referenc.length > 19)
    {
        var r = referenc.substring(2,12) + referenc.substring(13,18) + referenc.substring(19,20) + referenc.substring(21,23) + referenc.substring(24,25) ;
        referenc = r ;
        gField.value = r ;
    }

	var f = gForm ;
	credito = referenc.substring(0,10) ;
	factori = referenc.substring(10,15) ;
	tipoinf = referenc.substring(15,16) ;
	mesinf = referenc.substring(16,18) ;
	digver = referenc.substring(18) ;
	resto = referenc.substring(10) ;

	// valida el digito verificador
	var fct = 7 ;
	var suma = 0 ;
	var px = 0 ;
	var infcaract = "" ;
	if (referenc.length != 0)
	{
		var i=referenc.length-1 ;
		while(i>0)
		{
			infcaract = referenc.substring(i-1,i) ;
			px = infcaract * fct ;
			if (px >= 10)
			{
				px = px.toString().charAt(1) ;
			}
			suma = parseInt(suma) ;
			suma = suma + parseInt(px) ;
			switch ( fct )
			{
				case 7: fct = 3; break;
				case 3: fct = 1; break;
				case 1: fct = 7; break;
			}
			i--;
		}
		digsum = suma.toString().charAt(1) ;
		digsum = 10 - digsum ;
		if (digsum==10)
			digsum = 0 ;
		if (digsum != digver)
		{
			gField.value = "";
			alert("digito verificador invalido...") ;
		 	return false;
		}

		// calcula dias moreatorios
		dayArray = new Array ("31","28","31","30","31","30","31","31","30","31","30","31");

		smdf = parseFloat(infsmdf) ;
		anio = parseInt(infyear) ;
		mes =  parseInt(infmes) ;
		dia =  parseInt(infdia) ;
		fijo = 1997 ;
		dias = 0;
		bis = (anio-1)/4 ;
		bisa = (anio/4) ;
		if (bisa != bis)
		{	dayArray[1] = "29";}
		else
		{	dayArray[1] = "28";}

		while(anio>fijo)
		{
			mes = mes + 12 ;
			fijo = fijo + 1 ;
		}
		mespago = parseInt(mesinf) + 1 ;
		mesmora = parseInt(mes) - parseInt(mespago) ;
		diamora = dia - 10 ;

		if ((mesmora > 0 && diamora > 0) || mesmora >= 0)
		{
			cmes = 0 ;
			mmora = 0 ;
			if (mesmora != " " || mesmora != 0)
			{
				while(cmes<mesmora)
				{
					while(mespago>12)
					{
						mespago = parseInt(mespago) ;
						mespago = mespago - 12 ;
					}
					mmora = parseInt(mmora) ;
					mmora = mmora + parseInt(dayArray[mespago-1]) ;
					cmes ++ ;
					mespago ++ ;
				}
			}
			dias = diamora + mmora ;
		}

		// Tabla de tasas de pago
		if (mesinf<=12)
		{
			switch ( Number(tipoinf) )
			{
				case 1: tti = 0.27; break ;
				case 3: tti = 0.35; break ;
				case 5: tti = 0.20; break ;
				default: tti = 0; break ;
			}
		}

		// calcula monto
		pago = 0; mtto = 0.00; mora = 0; total = 0 ;
		if (mesinf >= 13)
		{
			entero = factori.substring(0,4) ;
			decimal = factori.substring(4) ;
			mtto = 0.00 ;
		}
		else
		{
			entero = factori.substring(0,2) ;
			decimal = factori.substring(2) ;
		}
		Mfactori = entero + "." + decimal ;
		pago = Mfactori * smdf ;
		if (mesinf <= 12 && tti != 0)
		{
			mtto1 = redonvalor((pago/tti) * 0.01);
			mtto = parseFloat(mtto1);
		}
		mora = (pago+mtto) * (dias * (0.00/360)) ;
		total = pago + mtto + mora ;
		f.txtcredito.value	= credito ;
		Pago = redonvalor(pago) ;
		f.txtpago.value	= Pago ;
		Mtto = redonvalor(mtto) ;
		f.txtmtto.value	= Mtto ;
		Mora = redonvalor(mora) ;
		f.txtmora.value	= Mora ;
		Total = redonvalor(total) ;
		f.txttotal.value	= Total ;
		f.txtfactor.value = factori ;
		f.txttipoinf.value = tipoinf ;
		f.txtmesinf.value = mesinf ;
		f.txtverif.value = digver ;
		return true ;
	}
	else{
		gField.value = "";
		return false ;
	}

}

// Valida que el número de servicio sea válido
function isReferTIP()
{
	messageLookup["isReferTIP"] = messageIni["isReferTIP"];

	refer = gForm.txtReferTIP.value ;
	refer = refer.toUpperCase() ;
	if ( refer.length < 4 )
	{
		gField.value = '';
		return false;
	}

	servicio = refer.substring(0,4) ;
	if(isNaN(Number(servicio)))
	{
		gField.value = '';
		messageLookup["isReferTIP"] = new String("Número de Servicio Inválido!!!") ;
		return false ;
	}
	if ( Number(servicio) < 4000 || Number(servicio) > 5999)
	{
		gField.value = '';
		messageLookup["isReferTIP"] = new String("Número de Servicio Inválido!!!");
		return false;
	}

	gField.value = refer;
	return true;
}

// Valida que el número de servicio sea válido
function isRefer()
{
	messageLookup["isRefer"] = messageIni["isRefer"];

	var referencia = gForm.txtReferenc.value ;
	if (gForm.txtCtaCia.value == "101037" || gForm.txtCtaCia.value == "101036")
	{
		if (referencia.length != 12){
			gForm.txtReferenc.value = "";
			return false;
		}
		if(isLYFmodulo10(referencia)){
			return true;
		}else{
			gForm.txtReferenc.value = "";
			return false;
		}

	}
	if (gForm.txtCtaCia.value == "101026")
	{
		if (referencia.length != 23){

			gForm.txtReferenc.value = "";
			return false ;
		}

		if (isluzyfuerzanormal(referencia))
		{
			var mtopago = referencia.substring(14,19) + "." + referencia.substring(19,22) ;
			gField.value = mtopago ;
			isCurrency() ;
			gForm.txtMonto.value = gField.value ;
			gField.value = referencia ;
			return true ;
		}
		else{
			gForm.txtReferenc.value = "";
			return false ;
		}

	}
	if (gForm.txtCtaCia.value == "101067")
	{
		if (referencia.length != 25){
			gForm.txtReferenc.value = "";
			return false;
		}

		if (isluzyfuerzaespecial(referencia)){
			var mtopago = referencia.substring(14,22) + "." + referencia.substring(22,24) ;
			gField.value = mtopago ;
			isCurrency() ;
			gForm.txtMonto.value = gField.value ;
			gField.value = referencia ;
			return true;
		}else{
			gForm.txtReferenc.value = "";
			return false;
		}

	}
	if (gForm.txtCtaCia.value == "101018" || gForm.txtCtaCia.value == "101041")
	{
		if (referencia.length != 16){
			gForm.txtReferenc.value = "";
			return false;
		}

		if(isLYFmodulo10(referencia)){
			return true;
		}else{
			gForm.txtReferenc.value = "";
			return false;
		}

	}
	return true;
}

// Valida que el bimestre sea válido
function isBimestre()
{
	messageLookup["isBimestre"] = messageIni["isBimestre"];

	isnumeric() ;
	var cTxn = gForm.cTxn.value ;
	var valor = gField.value ;
	bim = valor.substring(0,1) ;
	ani	= valor.substring(1) ;

	if ( parseInt(bim) < 1 || parseInt(bim) > 6){       // Numero de bimentre erroneo
		gField.value = '';
		return false;
	}

	if ( cTxn == "0522" )
		if ( parseInt(ani) < 94 || parseInt(ani) > 97 ) {  // Año erroneo para sector privado
			gField.value = '';
			return false;
		}

	return true;
}

// Valida el monto y total
function isSAR()
{
	messageLookup["isSAR"] = messageIni["isSAR"];

	isCurrency() ;
	var instA = "" ;
	var instB = "" ;
    var efect = gForm.txtEfect.value.toString().replace(/\$|\,|\./g,'');
	var monto = gForm.txtMto.value.toString().replace(/\$|\,|\./g,'') ;
	numcamp = gForm.length;
	for(z=0; z< numcamp; z++)
	{
		if (gForm.elements[z].type == "text")
		{
			if (gForm.elements[z].name == "txtCveIMSS")
			{
				instA = "IMSS";
		 		instB = "INFONAV";
				break;
			}
			if (gForm.elements[z].name == "txtCveISSSTE")
			{
				instA = "ISSSTE";
	     		instB = "FOVI";
				break;
			}
		}
	}
	insta = eval("gForm.txtCve" + instA + ".value").toString().replace(/\$|\,|\./g,'') ;
	instb = eval("gForm.txtCve" + instB + ".value").toString().replace(/\$|\,|\./g,'') ;
	pagosa = parseInt(eval("gForm.txtPag" + instA + ".value").toString().replace(/\$|\,|\./g,'')) + parseInt(eval("gForm.txtRcg" + instA + ".value").toString().replace(/\$|\,|\./g,''));
	pagosb = parseInt(eval("gForm.txtPag" + instB + ".value").toString().replace(/\$|\,|\./g,'')) + parseInt(eval("gForm.txtRcg" + instB + ".value").toString().replace(/\$|\,|\./g,''));

	if (parseInt(pagosa) > parseInt(insta) || parseInt(pagosb) > parseInt(instb))
		return false ;
	total = parseInt(insta) + parseInt(instb) ;
	if (monto == "" || parseInt(monto) == 0)
		cantidad = efect ;
	else
		cantidad = monto ;

	if (parseInt(total) != parseInt(cantidad))
		return false ;
	isVMtoEfect(gForm.txtEfect.name,gForm.txtMto.name) ;
	return true ;
}

// Validación del RFC
function isValRFC()
{
	messageLookup["isValRFC"] = messageIni["isValRFC"];
	var comprobante ;

    if (gForm.iTxn.value == "0728" || gForm.iTxn.value == "0730")
    {
        comprobante = gForm.lstComprobante.options[gForm.lstComprobante.selectedIndex].value ;
        if (comprobante == "no" || gForm.txtCuentaCom.value != "")
			return true ;
    }
    if (gForm.iTxn.value == "0630" && gForm.oTxn.value == "0476" || gForm.iTxn.value == "0030")
    {
        comprobante = gForm.lstComprobante.options[gForm.lstComprobante.selectedIndex].value ;
        if (comprobante == "no")
			return true ;
    }
    if (gForm.cTxn.value == "0806" && gForm.iTxn.value == "0029")
    {
       if(gField.value.length > 0 && gForm.numcredito.value.length > 0)
       {
         messageLookup["isValRFC"] = new String("Solo debe Capturar un solo campo, RFC o número de crédito");
         gField.value = '';
         return false;
       }
       if(gField.value.length > 0 && gForm.clavedpa.value.length > 0)
       {
         messageLookup["isValRFC"] = new String("Solo debe Capturar un solo campo, RFC o DPA");
         gField.value = '';
         return false;
       }

       if (gForm.numcredito.value.length > 0 || gForm.clavedpa.value.length > 0)
          return true;
    }
	//validacion para 0728 y SAT
	var RFC = gField.value ;
	RFC = RFC.toUpperCase() ;
	gField.value = RFC ;
	var Nomen = "" ;
	var HomoC = "" ;
	var Fecha = "" ;
	dayArray = new Array ("31","28","31","30","31","30","31","31","30","31","30","31") ;
	lon = RFC.length ;

	if(lon < 9)
	{
        gField.value="" ;
	    gField.focus() ;
	    return false ;
	}

	if (lon == 12 || lon == 9)
	{
		RFC = " " + RFC ;
		gField.value = RFC ;
	}

	var Nomen = RFC.substring(0,4) ;
	var i=0 ;
	var caracter='' ;
    var tmp = "" ;
	for(i=0; i<Nomen.length; i++)
	{
	    caracter = Nomen.charAt(i) ;
	    if(isAlpha(caracter))
	    {
	        if (caracter == 'ñ') caracter = 'Ñ';
	        tmp=tmp+caracter ;
	    }
	    else
	    {
         if (gForm.cTxn.value != '0806')
         {
	        if(i==0 && (caracter==' ' || caracter=='0'))
	            tmp=' ' ;
	        else
	        {
	            gField.value="" ;
	            gField.focus() ;
	            return false ;
	        }
	     }
	     else
	       tmp = caracter + tmp;
	    }
	}
	Nomen = tmp ;

	Fecha = RFC.substring(4,10) ;
	if(isNaN(Number(Fecha)))
	{
	    gField.value="" ;
	    gField.focus() ;
		return false ;
	}
	var month=0 ;
    var day=0 ;
    month=Number(Fecha.substring(2,4)) ;
	day=Number(Fecha.substring(4,6)) ;
	if(month == 2)
	{
		if(Number(Fecha.substring(0,2)) % 4 == 0)
			dayArray[month-1] = 29;
		else
			dayArray[month-1] = 28;
	}

	if ((month > 12 || month < 1) || (day < 1 || day > dayArray[month-1]))
	{
	    gField.value="" ;
	    gField.focus() ;
		return false ;
	}

    HomoC = "" ;
    HomoC=RFC.substring(10) ;
    if(HomoC.length!=0 && HomoC.length!=3)
    {
        gField.value="" ;
	    gField.focus() ;
        return false ;
    }

    tmp = "" ;
    for (i = 0; i < HomoC.length; i++)
    {
        caracter = HomoC.charAt(i) ;
        if ( isNaN(Number(caracter))&& !isAlpha(caracter) )
        {
            gField.value="" ;
	        gField.focus() ;
            return false ;
        }
	    if (caracter == 'ñ') caracter = 'Ñ' ;
	    tmp=tmp+caracter ;
    }
    HomoC=tmp ;

    gField.value = Nomen + Fecha + HomoC ;
    return true ;
}

// Valida el beneficiario seleccionado
function isBenef()
{
	messageLookup["isBenef"] =	messageIni["isBenef"];

	var benef = gField.options[gField.selectedIndex].value ;
	if ((benef == 0 && gForm.txtTitular.value == "") || (benef == 1 && gForm.txtBeneficiari.value == "") ||
		(benef == 2 && gForm.txtBeneficiari2.value == "") || (benef == 3 && gForm.txtBeneficiari3.value == ""))
		return false ;
	else
		return true ;
}

// Valida el tipo de pago
function isTipPago()
{
	messageLookup["isTipPago"] = messageIni["isTipPago"];

	var seleccion = gField.options[gField.selectedIndex].value ;
	var cuenta = gForm.txtCuenta.value ;
	if (seleccion == 2 && cuenta.length == 0)
		return false;
	return true;
}

// Valida el número de días capturado
function isValidDias()
{
	messageLookup["isValidDias"] = messageIni["isValidDias"];

	var valor = gField.value;
	var size = gField.value.length;

	if (!isnumeric())
		return false;

	if ( size = 0 )
		return false;

	if ( Alltrim(valor) == '0' )
		return false;
	return true;
}

// Valida que el folio tenga seis dígitos
function isFolioSerb()
{
	messageLookup["isFolioSerb"] =	messageIni["isFolioSerb"];

	var valor = gField.value;
	var size = gField.value.length;

	if (!isnumeric())
		return false;

	if ( size != 6 )
		return false;

	return true;
}

// Valida una referencia de un pago de Telmex
function istelmex()
{
	messageLookup["istelmex"] =	messageIni["istelmex"];

	var ref = gField.value ;
	var digver = ref.substring(ref.length-1,ref.length) ;
	var total = 0 ;
	var factor = 1 ;

	if (isNaN(ref) || ref.length < 20)
	{
		gField.value = "";
		return false;
	}
	for (vi=ref.length-1; vi>0; vi--)
	{
		num = ref.substring(vi-1,vi) ;
		num =num * factor ;
		total = num + total ;
		switch (factor)
		{
			case 1:
				factor = 7 ;
				break ;
			case 7:
				factor = 3 ;
				break ;
			case 3:
				factor = 1 ;
				break ;
		}
	}
	resul = total % 9 ;
	resul ++ ;
	if (resul != digver)
	{
		gField.value = "";
		return false ;
	}
	else
	{
		var entero = ref.substring(10,17) ;
		var centavo = ref.substring(17,19) ;
		var monto = entero + "." + centavo ;
		for(vi=1; vi<monto.length; vi++)
		{
			if (monto.charAt(vi) != 0)
			{
				monto = monto.substring(vi,monto.length) ;
				break ;
			}
		}
		if (gForm.txtMonto.value == "")
		{
			gForm.txtMonto.value = monto ;
		}
	}
	return true ;
}

// Valida que sea una hora válida
function isValidHora()
{
	messageLookup["isValidHora"] = messageIni["isValidHora"];

	var hora = gField.value ;

	horas = hora.substr(0,2) ;
	minutos = hora.substr(3,2) ;
	segundos = hora.substr(6,2) ;
	if ( Number(horas) > 24 || Number(minutos) > 60 || Number(segundos) > 60 )
		return false;

	return true;
}

// Valida el código capturado para una autorización
function isAutoriCV()
{
	messageLookup["isAutoriCV"] = messageIni["isAutoriCV"];

	var inputVal = gField.value;

	if ( gForm.oTxn.value == '0778' || gForm.oTxn.value == '4389')
		if (inputVal.length == 0 )
		{
			gField.value = '';
			return false;
		}

	if (inputVal.length != 0 )
		if ( !isInteger(inputVal))
		{
			gField.value = '';
			return false;
		}

	return true;
}

// Valida el tipo de cambio
function isTipoCambioCV()
{
	messageLookup["isTipoCambioCV"] =	messageIni["isTipoCambioCV"];

	var num = gField.value.toString().replace(/\$|\,/g,'') ;
	var intVal = num.substring(0, num.indexOf(".")) ;
	var decVal = num.substring(num.indexOf(".") + 1, num.length) ;
	var maxlength = 7 ;

	num = intVal + decVal ;
	if (num.length > maxlength)
	{
		alert ("Cantidad demasiado grande. Favor de verificar.") ;
		num = num.substring(0, maxlength) ;
		gField.value = '';
		return false ;
	}

	if( isNaN(num) )
	{
		gField.value = '';
		return false ;
	}

	var cents = decVal;

	if ( cents.length > 4 )
	{
		gField.value = '';
		return false ;
	}

	while( cents.length < 4 )
		cents = cents + "0" ;

	num = intVal ;
	if ( num.length == 0 )
		num = "0"
	else
		if ( num.length > 3 )
		{
			gField.value = '';
			return false ;
		}

	while( cents.length < 4 )
		cents = cents + "0" ;

	gField.value = num + '.' + cents ;

	return true ;
}

// Valida que el número de cheque de caja sea válido
function isChequeCaja()
{
    messageLookup["isChequeCaja"] =	messageIni["isChequeCaja"];

	var moneda = gForm.moneda.value ;
	var serial ;
	var Cuenta ;
	var Codigo ;

	if (gForm.cTxn.value != "5353")
	{
		serial = gForm.txtSerial.value ;
		Codigo = gForm.txtCCodSeg.value ;
		Codigo = Alltrim(Codigo) ;

		if(Codigo.length == 0)
		{
			gForm.txtCCodSeg.value  = ''  ; // 2004-02-09 15:35 ARM: En caso de que el valor sean solamente espacios en blanco, reinicia el valor a VACIO
			return false;
		}

		if ( moneda == "01")
			Cuenta = "390111111" ;
		else
			Cuenta = "390222222" ;

		gForm.txtCCodSeg.value = Codigo ;
	}
	else
	{
		serial = gForm.txtSerial2.value ;
		Codigo = gForm.txtCodSeg.value.substring(0,3) ;
		Cuenta = gForm.txtDDACuenta2.value.substring(1,10) ;
	}

	var Div = Math.floor(Number(Cuenta * 100) / Number(serial)).toString() ;

	Div = Math.floor(Div/100).toString() ;

	var V = Number(Cuenta) - Number(serial) - Number(Div) ;
	if ( V < 0)
		V = ( -1 * V ) ;

	D = Number(serial) % 10 ;
	W = V % 1000 ;
	C = W % 10 ;
	B = Math.floor(W / 10 ) % 10 ;
	A = Math.floor( W / 100) ;

	X = D + A ;
	Y = B + D ;
	Z = D + C ;
	if ( X >= 10)
		X = X % 10 ;
	if ( Y >= 10)
		Y = Y % 10 ;
	if ( Z >= 10)
		Z = Z % 10 ;

	X = X * 100 ;
	Y = Y * 10 ;
	Suma = X + Y + Z ;

	if ( Suma != Codigo)
		gForm.txtFees.value = "2" ;

	return true;
}


// Valida que el número capturado sea de un cheque válido
function isValidChequeDev()
{
	messageLookup["isValidChequeDev"] = messageIni["isValidChequeDev"];

	var sBanda = "" ;
	var cveTran = "";

	cveTran = gForm.txtCveTran.value;

	if(gForm.txtCodSeg.value.length >= 35 || gForm.txtCodSeg.value.length == 31 || gForm.txtCodSeg.value.length == 32)
		sBanda = gForm.txtCodSeg.value ;

	if(sBanda.length >= 35 || sBanda.length == 31 || sBanda.length == 32)
	{
		Llenar_Campos(sBanda);
		gForm.txtCodSeg.focus();//gForm.txtSerial2.focus();
		if(gForm.lstChqDev.value == 24){
			   return true;
		}else{
		if( exeValid() == false )
			return false;
	}
	}
	else
	{
		// Se captura la banda del cheque manualmente o se hace alguna modificacion.
		if( (gForm.txtCodSeg.value.length>0) && (gForm.txtCveTran.value.length>0) && (gForm.txtDDACuenta2.value.length>0) && (gForm.txtSerial2.value.length>0) )
		{
			if(gForm.lstChqDev.value == 24){
				return true;
			}else{
			if( exeValid() == false )
				return false;
			}
		}
	}

	if(cveTran.length > 0)
	{
		cveTran = quitaceros(cveTran);
		if(cveTran.substring(0,3) != "148" && cveTran.substring(1,3) !=	"88")
		{
			messageLookup["isValidChequeDev"] = new String("El cheque no es de ENTIDAD BANCARIA....") ;
			gForm.txtCveTran.value = "";
			gForm.txtDDACuenta2.value = "";
			gForm.txtSerial2.value = "";
			return false ;
		}

		var patt = /(1481|(0{3}148((0[1-9])|10)))/;
		if (!patt.test(gForm.txtCveTran.value)) {
			messageLookup["isValidChequeDev"] = new String("Cve. Trans. Invalido, verifique...") ;
			gForm.txtCveTran.value = "";
			gForm.txtDDACuenta2.value = "";
			gForm.txtSerial2.value = "";
			return false ;
		}
	}

	if(!isInteger(gForm.txtDDACuenta2.value)){
			messageLookup["isValidChequeDev"] = new String("La banda capturada contiene valores incorrectos....") ;
			gForm.txtCveTran.value = "";
			gForm.txtDDACuenta2.value = "";
			gForm.txtSerial2.value = "";
			return false ;
   	}


   	if(gForm.txtDDACuenta2.value.length < 11 && gForm.txtDDACuenta2.value.length != 0)
   		gForm.txtDDACuenta2.value=filler(gForm.txtDDACuenta2.value,11);

	/* Llenado funcion Procesar DV del Cheque */
	if (!processDigVDocCheque("isValidChequeDev")) {
		return false;
	}

	return true;
 }

function isValidCheque()
{
	messageLookup["isValidCheque"] = messageIni["isValidCheque"];

	var sBanda = "" ;
	var cveTran = "";

	cveTran = gForm.txtCveTran.value;

	if(gForm.txtCodSeg.value.length >= 35 || gForm.txtCodSeg.value.length == 31 || gForm.txtCodSeg.value.length == 32)
		sBanda = gForm.txtCodSeg.value ;

	//else //desde cualquier campo se podia hacer la lectura y vaciarlos
	//	if(gForm.txtCveTran.value.length >= 35)
	//		sBanda = gForm.txtCveTran.value;
	//	else
	//		if(gForm.txtDDACuenta2.value.length >= 35)
	//			sBanda = gForm.txtDDACuenta2.value;
	//		else
	//			if(gForm.txtSerial2.value.length >= 35)
	//				sBanda = gForm.txtSerial2.value;

	if(sBanda.length >= 35 || sBanda.length == 31 || sBanda.length == 32)
	{
		Llenar_Campos(sBanda);
		gForm.txtCodSeg.focus();
		if( exeValid() == false )
			return false;
	}
	else
	{
		// Se captura la banda del cheque manualmente o se hace alguna modificacion.
		if( (gForm.txtCodSeg.value.length>0) && (gForm.txtCveTran.value.length>0) && (gForm.txtDDACuenta2.value.length>0) && (gForm.txtSerial2.value.length>0) )
		{
			if( exeValid() == false )
				return false;
		}
	}

	if(cveTran.length > 0)
	{
		cveTran = quitaceros(cveTran);
		if(cveTran.substring(0,3) != "148" && cveTran.substring(1,3) !=	"88")
		{
			messageLookup["isValidCheque"] = new String("El cheque no es de ENTIDAD BANCARIA....");
			gForm.txtCveTran.value = "";
			gForm.txtDDACuenta2.value = "";
			gForm.txtSerial2.value = "";
			return false ;
		}

		var patt = /(1481|(0{3}148((0[1-9])|10)))/;
		if (!patt.test(gForm.txtCveTran.value)) {
			messageLookup["isValidCheque"] = new String("Cve. Trans. Invalido, verifique...") ;
			gForm.txtCveTran.value = "";
			gForm.txtDDACuenta2.value = "";
			gForm.txtSerial2.value = "";
			return false ;
		}
	}

	if(!isInteger(gForm.txtDDACuenta2.value)){
			messageLookup["isValidCheque"] = new String("La banda capturada contiene valores incorrectos....") ;
			gForm.txtCveTran.value = "";
			gForm.txtDDACuenta2.value = "";
			gForm.txtSerial2.value = "";
			return false ;
   	}


   	if(gForm.txtDDACuenta2.value.length < 11 && gForm.txtDDACuenta2.value.length != 0)
   		gForm.txtDDACuenta2.value=filler(gForm.txtDDACuenta2.value,11);

	/* Llenado funcion Procesar DV del Cheque */
	if (!processDigVDocCheque("isValidCheque")) {
		return false;
	}


	return true;
}


function isDecInteger()
{
	messageLookup["isDecInteger"] =	messageIni["isDecInteger"];

	var decimal = 0 ;
	var inputVal = gField.value ;
	var inputStr = inputVal.toString() ;

	if( isEmpty(inputStr) )
		return false ;

	for (var i = 0; i < inputStr.length; i++)
	{
		var oneChar = inputStr.charAt(i) ;
		if (oneChar == "." )
			++decimal ;
	}

	if( decimal > 1 )
		return false ;

	if( inputStr.indexOf(".") >= 0 )
	{
		intVal = inputStr.substring(0, inputStr.indexOf(".")) ;
		decVal = inputStr.substring(inputStr.indexOf(".") + 1, inputStr.length) ;
	}
	else
	{
		intVal = inputStr ;
		decVal = "" ;
	}

	if( (intVal.length+decVal.length) < 1 )
		return false ;

	if( !isInteger(intVal) || !isInteger(decVal) )
		return false ;

	if( intVal.length > 9 )
		return false ;

	return true ;
}

function isValidSer4061()  // Modulo 10
{
	messageLookup["isValidSer4061"] = messageIni["isValidSer4061"];

	var sum = 0 ;
	var inputVal = gField.value ;
	var factor = 2 ;

	inputStr = inputVal.toString() ;
	if ( gField.name == 'txtFolioAvaluo')
	{
		if(inputVal.length != 13)
		{
			gField.value = '';
			messageLookup["isValidSer4061"] = new String("Número de Folio Invalido, verifique...") ;
			return false ;
		}
	}

	if(inputVal.length == 11)
	{
		inputStr = inputVal.toString().substring(1,11) ;
		inputVal = inputStr ;
	}

	for(var i=inputVal.length-2; i>=0; i--)
	{
		if( factor % 2 != 0 )
			sum += parseInt( inputStr.charAt(i) ) ;
		else
		{
			temp = parseInt( inputStr.charAt(i) * 2 ) ;
			if( temp < 10 )
				sum += temp ;
			else
				sum = sum + Math.floor(temp / 10) + (temp % 10) ;
		}
		if ( factor == 2 )
			factor = 1;
		else
			factor = 2;
	}

	if( (sum % 10 == 0 ) && (parseInt(inputStr.charAt(inputStr.length-1)) == 0) )
		return true;

	if( (10-(sum % 10)) == parseInt(inputStr.charAt(inputStr.length-1)) )
		return true
	else
	{
		if ( gField.name == 'txtFolioAvaluo')
			messageLookup["isValidSer4061"] = new String("Número de Folio Invalido, verifique...") ;

		gField.value = '';
		return false ;
	}
}


function isValidCta()
{
    messageLookup["isValidCta"] = messageIni["isValidCta"];
	var inputVal = gField.value ;
	if(inputVal.length == 0)
	  {
	   return true ;
	  }
	else if(inputVal.length == 10)
	     {
	      if( isValidDDA( inputVal ) )
			return true ;
	     }
	     else if(inputVal.length == 15)
	            {
	            if( isValidCDA( inputVal ) )
			    return true ;
	            }
	            else
	                {
	    	         gField.value = "" ;
                     return false ;

	                 }

	    gField.value = "" ;
		return false ;

}



function isValidAcct()
{
	messageLookup["isValidAcct"] = messageIni["isValidAcct"];

	var suc = gForm.sucursal.value ;
	var inputVal = gField.value ;
	var sz = gField.size;

	/* Llamado funcion Procesar DV del Cheque */
	if (!processDigVDocCheque("isValidAcct")) {
		return false;
	}

	/****** INVERSIONES CON PREFIJO 21365******** 2523 se da de baja de ventanilla*/
 	 if(gForm.iTxn.value == '3001' || gForm.iTxn.value == '3009' || gForm.iTxn.value == '3075' ||
   	    gForm.iTxn.value == '3077' || gForm.iTxn.value == '3314' || gForm.iTxn.value == '3587'){

   	    if(inputVal.substring(0,5)=='21365'){
   	      messageLookup["isValidAcct"] = 'Pagaré operable solo a través de Conexión Personal';
   	      gField.value = "" ;
   	      return false;
   	    }
   	 }
   	 if(gForm.iTxn.value == '3009' || gForm.iTxn.value == '3075' || gForm.iTxn.value == '3077' ||
   	    gForm.iTxn.value == '3314' || gForm.iTxn.value == '3587'){

   	    if(inputVal.substring(0,5)=='84000'){
   	      messageLookup["isValidAcct"] = 'Esta inversión solo permite retiro mediante traspaso a cuenta a la vista';
   	      gField.value = "" ;
   	      return false;
   	    }
   	 }
   	 if( inputVal.length == sz )
           {
			if( !isEmpty( inputVal ) )
				if( isInteger( inputVal ) )
				{
					if( inputVal.length - 1 == 10 || inputVal.length == 10)
					{
						if( isValidDDA( inputVal ) )
							return true ;
					}
					else if( isValidCDA( inputVal ) )
						return true ;
				}

			gField.value = "" ;
			return false ;
		}


		gField.value = "" ;
		return false ;
}



function ValRel5503()
{
	var op1 = gForm.opcion1.value ;
        var op2 = gForm.opcion2.value ;

        if (op1 > 2 && op2 == 1)
           return false;
		else
           return true;


}




function isACDO()
{
	messageLookup["isACDO"] = messageIni["isACDO"];

	var tempData = new String() ;
	var txn = gForm.iTxn.value ;

	gForm.txtFolio.focus() ;


	if(gField.selectedIndex == 10)
	{
		if ( txn == "5561" || txn == "5565" || txn == "5567")
		{
			messageLookup["isACDO"] = new String("Tipo de Identificación Inválida!!") ;
			return false ;
		}
		gForm.txtFolio.value = gForm.registro.value ;
		return true ;
	}
	messageLookup["isACDO"] = new String("Se debe digitar el número de Folio.") ;
	if(gForm.txtSelectedItem.value != gField.selectedIndex)
	{
		gForm.txtFolio.value = '';
		gForm.txtSelectedItem.value = gField.selectedIndex;
		return false;
	}

	return true ;
}

function isBancoRem()
{
	messageLookup["isBancoRem"] = messageIni["isBancoRem"];
	if( (gField.value == '500') || (gField.value == '600') )
		return false;

	return true;
}

function isValidDesc1523()
{
	messageLookup["isValidDesc1523"] =	messageIni["isValidDesc1523"];

   if ( gForm.txtDDACuenta.value.substring(1,0) == 8 )
																	//Agencia de Nueva York
     if ( gField.selectedIndex == 3 ||
        ( gField.selectedIndex >= 15 && gField.selectedIndex <= 19 ) ||                           // 17Comision p/Pago Servicios Kroner
          gField.selectedIndex == 21  ||                            // 19Comision p/Liberación en Firme 20Comision/Servicio Cob.Ext.
          gField.selectedIndex == 5 ||                              // 06Comision p/Operacion Internacional
          gField.selectedIndex == 22 ||
          gField.selectedIndex == 23)                               // 19Comision p/Serv suc Director cmtH
        return false ;
   return true ;
}

function isValidDesc1795(){
	messageLookup["isValidDesc1795"] =	messageIni["isValidDesc1795"];
	if(gForm.lstDGeneric1795.value==null || gForm.lstDGeneric1795.value==""){
		return false;
	}else{
		return true;
	}
}
function isValidDesc1797(){
	messageLookup["isValidDesc1797"] =	messageIni["isValidDesc1797"];
	if(gForm.lstDGeneric1797.value==null || gForm.lstDGeneric1797.value==""){
		return false;
	}else{
		return true;
	}
}
function isValidDesc1295(){
	messageLookup["isValidDesc1295"] =	messageIni["isValidDesc1295"];
	if(gForm.lstDGeneric1295.value==null || gForm.lstDGeneric1295.value==""){
		return false;
	}else{
		return true;
	}
}

function isValidDesc1197()
{
	messageLookup["isValidDesc1197"] =	messageIni["isValidDesc1197"];

   if ( gForm.txtDDARetiro.value.substring(1,0) == 8 )
   {															//Agencia de Nueva York
      if ( gField.selectedIndex >= 17 &&                        // 18Cargo/Pago de Billetes NAFIN
           gField.selectedIndex <= 23 )                         // a la
           return false ;
   }
   else
   {
      if ( gField.selectedIndex == 7 )                         // 08Cargo/Operacion Fiduciaria
         return false ;
   }
   return true ;
}

function isValidDesc5903()
{
	messageLookup["isValidDesc5903"] =	messageIni["isValidDesc5903"];

   if ( gForm.txtDDADeposito.value.substring(1,0) == 8 )
   {																//Agencia de Nueva York
     if ( gField.selectedIndex == 6 ||                              // 07, 15,16 y 17
        ( gField.selectedIndex >= 14 &&
          gField.selectedIndex <= 16 ))
        return false ;
   }

   return true ;
}

function isValidDesc4503()
{
  messageLookup["isValidDesc4503"] =	messageIni["isValidDesc4503"];

  var monto = Alltrim(gForm.txtMonto.value) ;          // Monto de la Transaccion
  monto = monto.toString().replace(/\$|\,/g,'') ;

  var intVal = monto.substring(0, monto.indexOf(".")) ;
  var decVal = monto.substring(monto.indexOf(".") + 1, monto.length) ;
  monto = intVal + decVal ;

  inputStr = gField.options[gField.selectedIndex].value ;
  inputStr = inputStr.substring(31,inputStr.length) ;           // Monto Maximo de la descripcion
  inputStr = Alltrim(inputStr) ;

  inputStr = quitaceros(inputStr) ;

  if ( (parseInt(monto) > parseInt(inputStr)) || parseInt(monto) == 0 )
  {   // Monto rebasa el maximo
     gForm.txtMonto.value = "" ;
     gForm.txtMonto.focus() ;
     return false ;
  }

  return true ;
}

// Valida una fecha
function isValidDate()
{
	messageLookup["isValidDate"] = messageIni["isValidDate"];

	var inputVal = gField.value ;
	if( inputVal.length == 10 )
	{
		if( isDate(inputVal) )
			if( isOKDate(inputVal) )
			{
				var txn = gForm.cTxn.value ;
				if (txn == '5153')
					gForm.txtDDAA.focus() ;
				return true ;
			}
	}

	gField.value = "" ;
	return false ;
}

function isOKDate(inputVal)
{
	var fecha1 ="" ;
	var fecha2 ="" ;

	fecha1 = deltrash(inputVal) ;
    	fecha2 = deltrash(sysdate()) ;

    	var fechacap = parseInt(fecha1,10) ;
    	var fechasys = parseInt(fecha2,10) ;

    	if (fechacap > fechasys)
   	 	return false ;
	else
	   	return true ;
}

function deltrash(inputVal )
{
	var dateStr  = "" ;
	var dd ="" ;
	var mm ="" ;
	var yyyy ="" ;
	var fecha ="" ;

	var inputStr = inputVal.toString() ;

	for(var i=0; i<inputStr.length; i++)
	{
		if( inputStr.charAt(i) == "-" )
			dateStr += "/" ;
		else
			dateStr += inputStr.charAt(i) ;
	}

	var delim1 = dateStr.indexOf("/") ;
	var delim2 = dateStr.lastIndexOf("/") ;

	if (delim1 != -1 && delim1 == delim2)
		return false ;

	dd   = inputStr.substring(0, delim1) ;
	mm   = inputStr.substring(delim1 + 1, delim2) ;
	yyyy = inputStr.substring(delim2 + 1, inputStr.length) ;

	fecha = yyyy + mm + dd ;

	return fecha ;
}

function isRegistro()
{
	var inputVal = gField.value ;
	var sum = 0 ;
	if( inputVal.length < 7 ){
	  messageLookup["isRegistro"] = messageIni["isRegistro"];
		gField.value="";
		return false ;
	}
    if(!isnumeric()){
		gField.value="" ;
		return false ;
	}
	if (!isSerie()){
  	  return false;
  	}else
  	  return true;
}

function isIDCajero()
{
	messageLookup["isIDCajero"] = messageIni["isIDCajero"];

	var inputVal = gField.value ;
	if( inputVal.length > 5)
    {
   	if( !isEmpty(inputVal) )
	{
    		if( isInteger( inputVal ) )
      		{
			var inputStr = inputVal.toString() ;
			var suc = "0" + inputStr.substring(0, 4) ;
                	gSuc = window.document.getElementById("sucursal").value ;

			if(gSuc == suc)
				return true ;
      		}
  	}
     }
     return false ;
}

// Valida que el campo sea un imprte válido de máximo 8 dígitos
function isCurr8()
{
	messageLookup["isCurr8"] = messageIni["isCurr8"];

	var num = gField.value.toString().replace(/\$|\,/g,'') ;

	num = num.toString().replace(/\$|\,|\./g,'') ;
	if (num.length > 8)
	{
		gField.value = "" ;
		return false ;
	}
	if(!formatCurr(num, gField.name)) // Ojo
	{
		return false ;
	}
	return true ;
}

// Valida que se haya capturado el RFC obligatorio
function isRFCCF()
{
	messageLookup["isRFCCF"] = messageIni["isRFCCF"];

	var check = gForm.lstCF[gForm.lstCF.selectedIndex].value.toString() ;

	if(check == 0)
		return true ;
	var inputVal = gField.value ;
	return !isEmpty(inputVal) ;
}

// Valida que la cuenta capturada sea válida para un abono
function isAbonoAcct()
{
	messageLookup["isAbonoAcct"] = messageIni["isAbonoAcct"];

	var inputVal = gField.value ;

	if( inputVal.length > 9 && inputVal.length < 16 )
		if( !isEmpty( inputVal ) )
			if( isInteger( inputVal ) )
				if( inputVal.length - 1 == 10 || inputVal.length == 10)
					if( isValidDDA( inputVal ) )
						if(gForm.cTxn.value == '4227' || gForm.cTxn.value == '5153')
						{
							if(inputVal != gForm.txtDDAC.value)
								return true ;
						}
						else if(gForm.cTxn.value == '4229')
						{
							tmp  = gForm.txtDDACuenta2.value ;
							tmp  = tmp.substring(1,tmp.length) ;
							if(inputVal != tmp )
								return true;
						}
	gField.value = "" ;
	return false ;
}

function verPrint()
{
	messageLookup["verPrint"] =	messageIni["verPrint"];

	var tempFaltante = -1 ;
	var aCampos = new Array() ;
	var aECampos = new Array() ;
	var i ;
	var tCampo ;
	var temp ;
	var j ;
	aECampos[0]='Nombre' ;
	aECampos[1]='RFC' ;
	aECampos[2]='Domicilio' ;
	aCampos[0] = 'txtNomb01' ;
	aCampos[1] = 'txtRFC01' ;
	aCampos[2] = 'txtDomici' ;

	if(gForm.lstRespsta.selectedIndex == 0)
	{
		j = 0 ;
		for(i = 0; i < aCampos.length; i++)
		{
			tCampo = eval('gForm.' + aCampos[i]) ;
			tCampo.value = tCampo.value.toUpperCase() ;
			if(tCampo.value == '')
			{
				tCampo.focus() ;
				j++ ;
			}
		}
		if(j > 0)
		{
			messageLookup["verPrint"] = "Los campos de Nombre, RFC y Domicilio deben contener datos." ;
			return false ;
		}
	}
	else
	{
		j = 0 ;
		for(i = 0; i < aCampos.length; i++)
		{
			tCampo = eval('gForm.' + aCampos[i]) ;
			if(tCampo.value != '')
			{
				tCampo.value = '' ;
				j++ ;
			}
		}
		gForm.lstRespsta.focus() ;
		if(j > 0)
		{
			messageLookup["verPrint"] = "Los campos de Nombre, RFC y Domicilio no deben contener datos." ;
			return false ;
		}
	}

	return true ;
}

function isValidChoice()
{
	messageLookup["isValidChoice"] = messageIni["isValidChoice"];

	if(gForm.iTxn.value != '0033')
	{
		if(gForm.lstForLiq.selectedIndex == 1 && gForm.txtDDACuenta3.value.length == 0)
		{
			gField.value = "";
			messageLookup["isValidChoice"] = new String("La Cuenta debe contener datos.") ;
			gForm.lstForLiq.selectedIndex = 1
			return false ;
		}
		if(gForm.lstForLiq.selectedIndex == 1 && gForm.txtDDACuenta3.value.length > 0)
		{
			if(!isValidDDA(gForm.txtDDACuenta3.value))
			{
				gField.value = "";
				messageLookup["isValidChoice"] = new String("La Cuenta ingresada es inválida. Por favor verifique.") ;
				gForm.lstForLiq.selectedIndex = 1
				return false ;
			}
		}
		else if(gForm.lstForLiq.selectedIndex == 0 && gForm.txtDDACuenta3.value.length > 0)
		{
			gField.value = "";
			gForm.txtDDACuenta3.focus() ;
			messageLookup["isValidChoice"] = new String("La Cuenta no debe contener datos. Por favor verifique.") ;
			gForm.lstForLiq.selectedIndex = 0
			return false ;
		}
	}

	messageLookup["isValidChoice"] = new String("") ;
	return true ;
}

function toUpperCase()
{
	messageLookup["toUpperCase"] = messageIni["toUpperCase"];

	var inputStr = Alltrim(gField.value) ;

	if (gField.name == 'txtCveRFC' || gField.name == 'txtRFC01')
	{
		if (gForm.ComproFiscal != undefined)
		{
			if (gForm.ComproFiscal.selectedIndex == 1 && gField.value.length <= 9)
			{
				messageLookup["toUpperCase"] = new String("R.F.C. es un campo requerido.") ;
				gField.value = '';
				return false ;
			}
			if (gForm.ComproFiscal.selectedIndex == 0)
			{
				inputStr = "" ;
				gField.value = "" ;
			}
		}
	}

	if (gField.name == 'txtNombre' || gField.name == 'txtDomici')
		if (gForm.iTxn != undefined && gForm.cTxn != undefined && gForm.oTxn != undefined)
			if (gForm.iTxn.value == '0630' && gForm.cTxn.value == '0829' && gForm.oTxn.value == '0476' &&
				gField.value.length == 0 && gForm.lstComprobante.selectedIndex == 1)
			{
				messageLookup["toUpperCase"] = new String("Campo requerido.") ;
				gField.focus() ;
				return false ;
			}
	gField.value = inputStr.toUpperCase() ;
	return true ;
}

function CtaCargo()
{
	messageLookup["CtaCargo"] =	messageIni["CtaCargo"];

  	var CLista = gForm.cuentas ;

  	if (CLista.options[0].value == 0)
  	{
		if (!isValidDDA(gForm.txtDDAC.value))
		{
			gForm.txtDDAC.value = "" ;
			gForm.txtDDAC.focus() ;
			return false ;
		}
		else
		{
			gForm.ctacargo.value = gForm.txtDDAC.value ;
			return true ;
		}
	}
	else
	{
		gForm.txtDDAC.value = gForm.ctacargo.value ;
		alert("Con Abonos, la cuenta de cargo no se puede modificar") ;
		return true ;
	}
}

function isValidaFirma()
{
	messageLookup["isValidaFirma"] = messageIni["isValidaFirma"];

	var inputStr = gField.value ;
	inputStr = inputStr.toUpperCase() ;

	if ( !isInteger(inputStr) && gForm.cTxn.value != '5353')
	{
		gField.value = '';
		return false;
	}

	if(gForm.cTxn != undefined)
	{
		var txn = gForm.cTxn.value ;
		if(txn !='5353')
			if(inputStr.length == 0)
				return false;
	}

	if( !gForm.txtDDACuenta2 )
		return true ;

	if ( gForm.txtDDACuenta2.value == "00103000023" || gForm.txtDDACuenta2.value == "03902222222")
		if (isEmpty(inputStr))
			return false;

	return true;
}

function isMontoChCaja()
{
	messageLookup["isMontoChCaja"] =	messageIni["isMontoChCaja"];

	if (gForm.lstTipoRet3313.selectedIndex == 2)
	{
		monto = parent.frames["panel"].Monto.toString().replace(/\$|\,/g,'') ;
		monto2 = parent.frames["panel"].txtMontoChCaja.toString().replace(/\$|\,/g,'') ;

		monto2 = Alltrim(monto2) ;
		intVal = monto.substring(0, monto.indexOf(".")) ;
		decVal = monto.substring(monto.indexOf(".") + 1, monto.length) ;
		monto  = intVal + decVal ;
		if ( Number(monto2) > Number(monto))
			return false;
	}
	return true;
}

function isValidMonto()
{
	messageLookup["isValidMonto"] =	messageIni["isValidMonto"];

	var inputStr = Alltrim(gForm.txtMontoRC.value) ;
	inputStr = inputStr.toUpperCase() ;

	if (!isCurrency())
		return false ;

	if ( gForm.moneda.value!= "03")
	{
		if ( gForm.lstTipoRet3313.selectedIndex == 0 || gForm.lstTipoRet3313.selectedIndex == 4 )
		{
			if (isEmpty(inputStr))
				return false ;
		}
		else
		{
			gForm.txtMontoRC.value = "0.00" ;
			return true ;
		}
	}
	else
		return true ;

	monto = parent.frames["panel"].Monto.toString().replace(/\$|\,|\-/g,'') ;

	inputStr = inputStr.toString().replace(/\$|\,|\-/g,'') ;

	intVal = monto.substring(0, monto.indexOf(".")) ;
	decVal = monto.substring(monto.indexOf(".") + 1, monto.length) ;
	monto = intVal + decVal ;

	intVal = inputStr.substring(0, inputStr.indexOf(".")) ;
	decVal = inputStr.substring(inputStr.indexOf(".") + 1, inputStr.length) ;
	inputStr = intVal + decVal ;

	if (( Number(inputStr) > Number(monto) ) && gForm.lstTipoRet3313.selectedIndex == 0 )
		return false ;
	isCurrency() ;

	return true;
}

function MtoCargo()
{
	messageLookup["MtoCargo"] =	messageIni["MtoCargo"];

  	var CLista = gForm.cuentas ;
  	var MLista = gForm.montos ;

  	if (CLista.options[0].value == 0)
  	{
		if (!isCurrency(gForm.txtMontoC.value))
			return false ;
		else
		{
			gForm.mtocargo.value = gForm.txtMontoC.value ;
			var txn = gForm.cTxn.value ;
			if (txn == '5153')
			    gForm.txtFechaEfect.focus() ;
			else
				gForm.txtDDAA.focus() ;
			return true ;
		}
	}
	else
	{
		gForm.txtMontoC.value = gForm.mtocargo.value ;
		alert("Con Abonos, el monto del cargo no se puede modificar") ;
	    return true ;
	}
}

function Cheque()
{
	messageLookup["Cheque"] = messageIni["Cheque"];

	var CLista = gForm.cuentas ;
	var campo = gField.name ;
	var campoHTML ;

	if (campo == 'txtCodSeg')
		campo = 'codseg' ;
	else if (campo == 'txtCveTran')
		campo = 'cvetran' ;
	else if (campo == 'txtDDACuenta2')
		campo = 'ctacargo' ;
	else if (campo == 'txtSerial2')
		campo = 'serial' ;

	if (CLista.options[0].value == 0)
	{
		if (!isValidCheque(gField.value))
			return false;
		else
		{
			campoHTML = eval("gForm.codseg") ;
			campoHTML.value = gForm.txtCodSeg.value ;
			campoHTML = eval("gForm.cvetran") ;
			campoHTML.value = gForm.txtCveTran.value ;
			campoHTML = eval("gForm.ctacargo") ;
			campoHTML.value = gForm.txtDDACuenta2.value ;
			campoHTML = eval("gForm.serial") ;
			campoHTML.value = gForm.txtSerial2.value ;
			return true ;
		}
	}
	else
	{
		campoHTML = eval("gForm." + campo) ;
		gField.value = campoHTML.value ;
		alert("Con Abonos, los datos del cheque no se pueden modificar") ;
		return true ;
	}
}

function ChequeD()
{
	messageLookup["ChequeD"] = messageIni["ChequeD"];

	campo = gField.name ;
	if (campo == 'txtCodSeg')
		campo = 'codseg' ;
	else if (campo == 'txtCveTran')
		campo = 'cvetran' ;
	else if (campo == 'txtDDACuenta2')
		campo = 'ctacargo' ;
	else if (campo == 'txtSerial2')
		campo = 'serial' ;

	campoHTML = eval("gForm." + campo) ;

	if (!isValidCheque(gField.value))
		return false ;
	else
	{
		if (!(campo == 'ctacargo'))
		{
			campoHTML.value = gField.value ;
			return true ;
		}
		else
			return true ;
	}
}

function isACorte()
{
	messageLookup["isACorte"] =	messageIni["isACorte"];

	if(gForm.txtCveTran)
	{
		var cveTran = gForm.txtCveTran.value ;
		if(cveTran.length == 9)
		{
			cveTran = cveTran.substring(5, 8) ;
			if(cveTran == '021' || cveTran == '305')
			{      // cmtH o Agencia de Nueva York
				for(var i = 0; i < gForm.elements.length; i++)
				{
					if(gForm.elements[i].name == 'rdoACorte' && gForm.elements[i].checked == true)
					{
						gForm.elements[i].checked = false;
						alert('No es necesario seleccionar esta opción para cheques ENTIDAD BANCARIA') ;
					}
				}
			}
			else
			{
				var k = 0 ;
				for(var i = 0; i < gForm.elements.length; i++)
					if ( gForm.elements[i].checked != true )
						k++;
				if ( k == i )
					return false;
			}
		}
	}
	else
	{
		var j = 0 ;
		for(var i = 0; i < gForm.elements.length; i++)
		{
			if(gForm.elements[i].name == 'rdoACorte')
				if(gForm.elements[i].checked == true)
					j++;
		}
		if(j == 0)
			return false;
	}
	return true;
}

function isValidCurrProc()
{
	messageLookup["isValidCurrProc"] =	messageIni["isValidCurrProc"];

	if(gForm.cTxn.value == '0815')
		if(gField.selectedIndex == 2)
		{
			gField.selectedIndex = 0 ;
			return false ;
		}

	if(gForm.cTxn.value == '0013' )
	  {
	   gForm.moneda.value = gForm.lstMoneda.value;

	  }
	return true ;
}


function isValidAuthori()
{
	messageLookup["isValidAuthori"] = messageIni["isValidAuthori"];

	var inputVal = gField.value ;
	var pParte ;
	var sParte ;

	if( isEmpty(inputVal) )
	{
	    gField.value = '';
		return false ;
	}

	if( !isInteger(inputVal) )
	{
		gField.value = '';
		return false ;
	}

	if( inputVal.length < 9 )
	{
		gField.value = '';
		return false ;
	}

	if( gForm.txtSerial5.value.length != 9 )
	{
		gField.value = '';
		messageLookup["isValidAuthori"] = new String("Debe ingresar primero el Serial") ;
		return false ;
	}
	pParte = gForm.txtSerial5.value.substring(2,5) ;
	sParte = gForm.txtSerial5.value.substring(5,10) ;

	if(inputVal.substring(0,1) != '9' || inputVal.length < 9)
	{
		gField.value = '';
		messageLookup["isValidAuthori"] = new String("Autorización no válida") ;
		return false ;
	}

	return true ;
}

function isCodeSeg()
{
	messageLookup["isCodeSeg"] = messageIni["isCodeSeg"];

	var inputVal = gField.value ;

	if( isEmpty(inputVal))
		return false ;

	if( !isInteger(inputVal) )
		return false ;

	if( inputVal == '020' )
		return true;
	else
	{
		gForm.txtCodeSeg.value = '' ;
		return false ;
	}
}

function isValidMto()
{
	messageLookup["isValidMto"] = messageIni["isValidMto"];

	var nombre = gField.name ;
	var mto = gField.value ;
	var num = "" ;
	var numv = "" ;
	var numd = "" ;
	var mtov = "" ;
	var mtod = "" ;

	numd = gForm.txtNoDevs.selectedIndex ;
	numv = gForm.txtNoVtas.selectedIndex ;

	mtod = gForm.txtMtoVtas.value ;
	mtov = gForm.txtMtoDevs.value ;

	if(numd == 0 && numv == 0)
	{
		gForm.txtMtoDevs.value = "0.00" ;
		gForm.txtMtoVtas.value = "0.00" ;
		return false ;
	}

	if(nombre == "txtMtoDevs")
 		num = "txtNoDevs" ;
	else
  		num = "txtNoVtas" ;
	num = eval("gForm." + num+ ".selectedIndex") ;

	if(mto == '')
		gField.value = "0.00" ;

	if(num == 0 && mto > 0.0)
	{
		gField.value = "0.00" ;
		return false ;
	}
	isCurrency() ;
	return true ;
}

function isValidNo()
{
	messageLookup["isValidNo"] = messageIni["isValidNo"];

	var num = gField.selectedIndex ;

	num = parseInt(num) ;
	var nombre = gField.name ;
	if(nombre == "txtNoDevs")
		nombre = "txtMtoDevs" ;
	else
		nombre = "txtMtoVtas" ;

	var numd = gForm.txtNoDevs.selectedIndex ;
	var numv = gForm.txtNoVtas.selectedIndex ;

	if(numd == 0 && numv == 0)
	{
		return false ;
	}

	if(num == 0)
	{
		eval("gForm."+nombre+".value = '0.00'") ;
		return true ;
	}

	return true ;
}

function isTDC()
{
	messageLookup["isTDC"] = messageIni["isTDC"];

	var mask = new Array(2,1,2,1,2,1,2,1,2,1,2,1,2,1,2,1) ;
	var tdc = gField.value ;
	var large = tdc.length ;

	if(large == 16 || large == 13)
	{
	   if(mod10(tdc,mask))
	      return true;
	   else
	      gField.value = "" ;
	      return false;
	}
	gField.value = "" ;
	return false ;
}

function notChange()
{
	messageLookup["notChange"] = messageIni["notChange"];

	var campo = gField.name ;
	var val = "" ;

	if(campo == "montoTD")
		val = gForm.MTD.value ;
	else if (campo == "montoTV")
		val = gForm.MTV.value ;
	else if (campo == "IVAD")
		val = gForm.ivaD.value ;
	else if (campo == "IVAV")
		val = gForm.ivaV.value ;
	else if (campo == "montoD")
		val = gForm.mtoD.value ;
	else if (campo == "montoV")
		val = gForm.mtoV.value ;
	else if (campo == "ComDev")
		val = gForm.cD.value ;
	else if (campo == "ComVta")
		val = gForm.cV.value ;
	else if (campo == "txtTot")
		val = gForm.tot.value ;
	else if (campo == "txtMontoT")
		val = gForm.mtoabonos.value ;
	else if (campo == "txtTotalA")
		val = gForm.noabonos.value ;

	if(gField.value == val){
		return true;
	}else{
		gField.value = val ;
		return false ;
	}
}

function isValMto()
{
	messageLookup["isValMto"] =	messageIni["isValMto"];

	var campo = gField.name ;
	var tipo = campo.substring(campo.length-2, campo.length-1) ;

	if(tipo=="1")  //para 10
		tipo = campo.substring(campo.length-3, campo.length-2) ;
	var num = campo.substring(campo.length-1, campo.length) ;
	if(num=="0")
		num=10;	//para 10
	num = Number(num) ;
	var nt = eval("gForm.no"+tipo+".value") ;
	nt = Number(nt) ;
	var val = gField.value ;

	var i = 0 ;
	var acum = 0 ;
	var tmpval = "" ;

	val = val.toString().replace(/\$|\,|\.|\-/g,'') ;
	val = Number(val) ;
	if(isNaN(val)||val==0)
	{
		gField.value = "" ;
		acum=0 ;
		for(i=1;i<=nt;i++)
		{
			tmpval = eval("gForm.monto"+tipo+i+".value") ;
			tmpval = tmpval.toString().replace(/\$|\,|\.|\-/g,'') ;
			tmpval = Number(tmpval) ;
			if(isNaN(tmpval))
				tmpval=0 ;
			acum = acum + tmpval ;
		}
		eval("gForm.txtAcum"+tipo+".value=Currency("+acum+")") ;
		return false ;
	}

	var total = eval("gForm.monto"+tipo+".value") ;
	total = total.toString().replace(/\$|\,|\.|\-/g,'') ;
	total = Number(total) ;

	tmpval = "" ;
	acum = 0 ;
	acum = Number(acum) ;
	var nvacios=0 ;
	for(i=1;i<=nt;i++)
	{
		tmpval = eval("gForm.monto"+tipo+i+".value") ;
		tmpval = tmpval.toString().replace(/\$|\,|\.|\-/g,'') ;
		tmpval = Number(tmpval) ;
		if(isNaN(tmpval)||tmpval==0) //cuando son blancos(isNAN) o Number
		{
			tmpval=0 ;
			nvacios++ ;
		}
		acum = acum + tmpval ;
	}

	if(acum>total ||(acum==total && nvacios>0) || (acum<total&&(nt==1||!nvacios)))
	{
		gField.value = "" ;
		acum = acum - val ;
		eval("gForm.txtAcum"+tipo+".value=Currency("+acum+")") ;
		return false;
	}
	else
	{
		isCurrency() ;
		eval("gForm.txtAcum"+tipo+".value=Currency("+acum+")") ;
		return true ;
	}
}
//JGA VAIDAR MONTO
function isValMtoRAPL()
{
	messageLookup["isValMtoRAPL"] =	messageIni["isValMtoRAPL"];
	var flag = 0 ; //No hay error
	var mto = gField.value ; //recupera el valor del campo
	var name = gField.name ; //recupera el nombre campo

	if (mto ==' ')
	{
		gField.value = "0.00" ;
		flag = 1 ; //Error
	}

	mto = mto.toString().replace(/\$|\,|\./g,'') ;//sustituye simbolos por espacio
	mto = Number(mto) ;
	if (isNaN(mto))
	{
		gField.value = "0.00" ;
		flag = 1 ; //Error
	}

	mto = parseInt(mto);

	if(mto <= 0)
	{
		gForm.monto.value = "0.00" ;
		gForm.txtTot.value = "0.00" ;
		gForm.tot.value = "0.00" ;
		flag = 1 ; //Error
	}
	else
	{
		eval("gForm.monto.value=Currency("+mto+")") ;
		eval("gForm.tot.value=Currency("+mto+")") ;
		eval("gForm.txtTot.value=Currency("+mto+")") ;
	}

	if(flag == 1)
		return false ;
	return true ;



}

function isValServ()
{
	messageLookup["isValServ"] = messageIni["isValServ"];

	var serv = gField.value ;
	var name = gField.name ;

	serv = serv.toString() ;
	if(isEmpty(serv))
		return false ;

    if (serv == '4376')
    {
      messageLookup["isValServ"] = new String("Error, No puede operar este servicio por RAP, Seleccione Transaccion 0029") ;
      gField.value = "";
	  return false ;
    }

    if (serv == '77777')
           {
           messageLookup["isValServ"] = new String("Error, No puede operar este servicio por RAP, Seleccione Transaccion 0027") ;
           gField.value = "";
	       return false ;
           }
	name = name.substring(name.length-1,name.length) ;
	var s = eval("gForm.s"+name+".value") ;

	if (serv != s)
	{
		var np = eval("gForm.nopagos"+name+".value=''") ;
		var mto = eval("gForm.monto"+name+".value=''") ;
	}

	if(serv == "60")
		gForm.ser60.value = "1" ;
	else
		gForm.ser60.value = "0" ;

	if(serv == "670")
		gForm.ser670.value = "1" ;
	else
		gForm.ser670.value = "0" ;

	if(serv == "480" || serv == "297")
		gForm.noefec.value = "1" ;

	var moneda = gForm.moneda.value ;
	if (moneda!="01" && (serv=="448" || serv=="480" || serv=="66" || serv=="297"))
	{
		gField.value = "" ;
		gField.focus() ;
		return false ;
	}
	eval("gForm.s"+name+".value='"+serv+"'") ;
	var i =0 ;
	var cvalor = "" ;
	var ns = gForm.nservicios.value ;
	ns = Number(ns) ;
	if (ns > 1 )
		for(i=1; i<ns+1; i++)
		{


			cvalor = eval("gForm.servicio"+i+".value") ;

			if (name != i)
				if (cvalor == serv)
				{
					eval("gForm.servicio"+i+".value=''");
					eval("gForm.servicio"+i+".focus()");
					return false ;
				}
		}



	return true ;
}

function isValMtoRAP()
{
	messageLookup["isValMtoRAP"] = messageIni["isValMtoRAP"];

	var flag = 0 ;
	var mto = gField.value ;
	var name = gField.name ;

	mto = mto.toString().replace(/\$|\,|\./g,'') ;
	mto = Number(mto) ;
	if (isNaN(mto))
	{
		gField.value = "" ;
		flag = 1 ;
	}

	name = name.substring(name.length-1,name.length) ;
	var serv = eval("gForm.servicio"+name+".value") ;

	// Solo Acepta servicio 448 y 66 en monto Cero
	if(mto <= 0 && (serv != "448" && serv != "66"))
	{
		gField.value = "";
		flag =1;
	}
	else
		isCurrency();

	var tmpval = "";
	var tmpnp = "";
	var acum = 0;
	var np = 0;
	acum = parseInt(acum);
	var i = 0;
	var no = gForm.nservicios.value;
	no = parseInt(no);

	for(i=1;i<no+1;i++)
	{
		tmpval = eval("gForm.monto"+i+".value") ;
		tmpnp = eval("gForm.nopagos"+i+".value") ;
		if(tmpval.length != 0)
		{
			tmpval = tmpval.toString().replace(/\$|\,|\./g,'') ;
			tmpval = Number(tmpval) ;
			acum = acum + tmpval ;
			tmpnp = Number(tmpnp) ;
			np = np + tmpnp ;
		}
	}

	if(acum <= 0)
	{
		gForm.txtTot.value = "0.00" ;
		gForm.tot.value = "0.00" ;
		gForm.np.value = "0" ;
	}
	else
	{
		eval("gForm.txtTot.value=Currency("+acum+")") ;
		eval("gForm.tot.value=Currency("+acum+")") ;
		gForm.np.value  = np ;
	}

	if(flag == 1)
		return false ;
	return true ;
}

function isValMto5503()
{
	messageLookup["isValMto5503"] =	messageIni["isValMto5503"];

	var flag = 0 ;
	var i = 0 ;
	var mto = gField.value ;
	mto = mto.toString().replace(/\$|\,|\./g,'') ;
	mto = Number(mto) ;

	if (isNaN(mto))
	{
		gField.value = "0.00" ;
		return false ;
	}

	var exist = 0 ;
	var acum = 0 ;
	var tmpval = 0 ;
	var total = 0 ;
	acum = parseInt(acum) ;
	var campos = new Array('txtEfectivo','txtCheque','txtCuenta','txtDocsCI','txtRemesas') ;

	total = gForm.total.value ;
	total = total.toString().replace(/\$|\,|\./g,'') ;
	total = Number(total) ;
	for(i=0; i < campos.length; i++)
	{
		exist = eval("gForm."+campos[i]) ;
		if(exist != undefined)
		{
			tmpval = eval("gForm."+campos[i]+".value") ;
			tmpval = tmpval.toString().replace(/\$|\,|\./g,'') ;
			tmpval = Number(tmpval) ;
			acum = acum + tmpval ;
			if(acum > total)
			{
				acum = acum - mto ;
				eval("gForm.subtotal.value=Currency("+acum+")") ;
				gField.value = "0.00" ;
				return false ;
			}
		}
	}

	//CONDICIÓN PARA RAP CALCULADORA EN DESCUENTOS
	if(gForm.rapcalc != undefined && gForm.descuento != undefined){
		if(gForm.rapcalc.value == "SI"){
			if(gForm.descuento.value == "SI"){
				var efetmp = eval("gForm.txtEfectivo.value");
				var chqtmp = eval("gForm.txtCheque.value");
				efetmp = efetmp.toString().replace(/\$|\,|\./g,'');
				efetmp = Number(efetmp);
				chqtmp = chqtmp.toString().replace(/\$|\,|\./g,'');
				chqtmp = Number(chqtmp);
				if(gForm.no_descuento.value == "SI" && efetmp > 0 && chqtmp > 0){
					messageLookup["isValMto5503"] = "El servicio sólo permite utilizar una forma de pago (Efectivo o Documentos)"
					gForm.txtEfectivo.value = "0.00";
					gForm.txtCheque.value = "0.00";
					return false;
				}
			}
		}
	}

	eval("gField.value=Currency("+mto+")") ;
	eval("gForm.subtotal.value=Currency("+acum+")") ;
	return true ;
}

function Valid5503()
{
	messageLookup["Valid5503"] = messageIni["Valid5503"];

	var cash = gForm.txtEfectivo.value ;
	cash = cash.toString().replace(/\$|\,|\./g,'') ;
	cash = Number(cash) ;
	var ci =  gForm.txtDocsCI.value ;
	ci = ci.toString().replace(/\$|\,|\./g,'') ;
	ci = Number(ci) ;
	var np = gForm.npagos.value ;
	np = Number(np) ;
	if(ci > 0)
	{
		var ns = gForm.nservicios.value ;
		ns = Number(ns) ;
		if (ns > 1)
			return false ;
		if (np > 1 && cash > 0)
			return false ;
		var mtosum = 0 ;
		var mto ;

		if(gForm.txtCheque != undefined)
		{
			mto = gForm.txtCheque.value ;
			mto = mto.toString().replace(/\$|\,|\./g,'') ;
			mto = Number(mto) ;
			mtosum = mtosum + mto ;
		}
		if(gForm.txtCuenta != undefined)
		{
			mto = gForm.txtCuenta.value ;
			mto = mto.toString().replace(/\$|\,|\./g,'') ;
			mto = Number(mto) ;
			mtosum = mtosum + mto ;
		}
		if(gForm.txtRemesas != undefined)
		{
			mto = gForm.txtRemesas.value ;
			mto = mto.toString().replace(/\$|\,|\./g,'') ;
			mto = Number(mto) ;
			mtosum = mtosum + mto ;
		}
//-- AGREGADO PARA RAP CALCULADORA NO PERMITIR COMBINACIÓN CI-EFECTIVO
		if(gForm.txtEfectivo != undefined && gForm.rapC != undefined)
		{
			if(gForm.rapC.value == "R")
			{
				mto = gForm.txtEfectivo.value ;
				mto = mto.toString().replace(/\$|\,|\./g,'') ;
				mto = Number(mto) ;
				mtosum = mtosum + mto ;
			}
		}
//---
		if(mtosum > 0)
		{
//--RAPC
			if(gForm.rapC != undefined)
				if(gForm.rapC.value == "R")
					messageLookup["Valid5503"] = new String("Combinación de Pago No válida,\n C.I. para este servicio se usa individualmente..") ;
				else
					messageLookup["Valid5503"] = new String("Combinación de Pago No válida,\n C.I. se usa individualmente o con Efectivo..") ;
			else
//--
				messageLookup["Valid5503"] = new String("Combinación de Pago No válida,\n C.I. se usa individualmente o con Efectivo..") ;
			return false ;
		}
	}

	return true;
}

function ValSum()
{
	messageLookup["ValSum"] = messageIni["ValSum"];

	var subtotal = gForm.subtotal.value ;
	subtotal = subtotal.toString().replace(/\$|\,|\./g,'') ;
	subtotal = Number(subtotal) ;

	var total = gForm.total.value ;
	total = total.toString().replace(/\$|\,|\./g,'') ;
	total = Number(total) ;

	if (subtotal == total)
		return true ;
	else
		return false ;
}

function MtoServ()
{
	messageLookup["MtoServ"] =	messageIni["MtoServ"];

	var zeroes = 0 ;
	var montoc = gField.value ;
 	if(isEmpty(montoc))
		return false;

	montoc = montoc.toString().replace(/\$|\,|\./g,'') ;
	montoc = Number(montoc) ;

	if(isNaN(montoc))
	{
		gField.value = "" ;
		return false ;
	}

	var no = gField.name ;
	no = no.substring(6,7) ;

	var servicio = eval("gForm.s"+no+".value") ;
	servicio = servicio.toString() ;

	if(servicio == "480")
		if(montoc > 24999999)
		{
			gField.value = "" ;
			return false ;
		}

	if(servicio == "448")
	      if(montoc > 8000000000)
		{
			gField.value = "" ;
			return false ;
		}

	if(montoc == 0 && servicio != "480" && servicio !="448" && servicio != "66")
		return false ;

	// NUMERO DE PAGOS POR SERVICIO
	var nps = eval("gForm.nps"+no+".value") ;
	nps = nps.toString().replace(/\$|\,|\./g,'') ;
	nps = Number(nps) ;

	var tmpval = "" ;
	var acum = 0 ;
	acum = parseInt(acum) ;
    var i = 0 ;

	for(i=1;i<nps+1;i++)
	{
		tmpval = eval("gForm.montos"+no+"p"+i+".value") ;
		if(tmpval.length != 0)
		{
	    	tmpval = tmpval.toString().replace(/\$|\,|\./g,'') ;
			tmpval = Number(tmpval) ;
			acum = acum + tmpval ;
		}
		else
			zeroes = zeroes +1 ;
	}

	var np = gField.name ;
	np = np.substring(8,np.length) ;
	np = Number(np) ;

	var montot = eval("gForm.mtos"+no+".value") ;
	montot = montot.toString().replace(/\$|\,|\./g,'') ;
	montot = Number(montot) ;

	if(nps == 1)
	{
		if(acum == montot)
		{
			eval("gField.value=Currency("+montoc+")") ;
			eval("gForm.montod"+no+".value='0.00'") ;
			return true ;
		}
		else
		{
			gField.value = "" ;
			eval("gForm.montod"+no+".value=Currency("+montot+")") ;
			return false ;
		}
	}

	// 	Pago > 1 hasta n
	else
	{
		if(acum < montot && zeroes > 0)
		{
			eval("gField.value=Currency("+montoc+")") ;
			montot = montot - acum ;
			eval("gForm.montod"+no+".value=Currency("+montot+")") ;
			return true ;
		}
		else if( acum == montot && zeroes == 0)
		{
			eval("gField.value=Currency("+montoc+")") ;
			montot = montot - acum ;
			eval("gForm.montod"+no+".value=Currency("+montot+")") ;
			return true ;
		}
		else
		{
			gField.value = "" ;
			montot = montot - (acum - montoc) ;
			eval("gForm.montod"+no+".value=Currency("+montot+")") ;
			return false ;
		}
	}
}

function ValRef40()
{

	messageLookup["ValRef40"] =	messageIni["ValRef40"];

	var size =Number(gField.size);
	if(size == 40)
	{
		var acum = 0 ;
		var campo = gField.name ;
		var ser = campo.substring(4,6) ;
		var pag = campo.substring(6,campo.length) ;
		for(var i=1; i<4; i++)
		{
			if(eval("gForm.ref"+i+ser+pag) != undefined)
			{
				campo = eval("gForm.ref"+i+ser+pag+".value") ;
				acum = 	acum + campo.length ;
			}
		}
		if (acum <= 40)
		{
			var valor = gField.value ;
			valor = valor.toUpperCase() ;
			gField.value = valor ;
			var n_ref = Number(gField.name.substring(3,4));
			if(n_ref==1)
			{
				if(eval("gForm."+ser+".value")==1118)
				{

					if(valor == 20586)
					{
						messageLookup["ValRef40"] = new String("Valor de Referencia 1 no valido para el servicio 1118");
						gField.value = "" ;
						return false;
					}
				}
			}
			return true ;
		}
		else
		{
			gField.value = "" ;
			return false ;
		}
	}
}

function ValRefFija()
{
	messageLookup["ValRefFija"] = messageIni["ValRefFija"];

	var size = gField.size ;
	var valor = gField.value ;

	size = Number(size) ;
	valor = valor.length ;
	if (valor == size)
	{
		var valor = gField.value ;
		valor = valor.toUpperCase() ;
		gField.value = valor ;
		return true ;
	}
	else
		{
			gField.value = "";
			return false;
		}
}

function FlujoRAP()
{
	messageLookup["FlujoRAP"] =	messageIni["FlujoRAP"];

	var flujo = "" ;
	var campo = "" ;
	var txn = "" ;
	var montoc = 0 ;
	var i = 0 ;

	for (i = 1; i < 6; i++)
	{
		switch(i)
		{
			case 1:
					campo = "txtCheque" ;
					txn = "5353*" ;
					break  ;
			case 2:
					campo = "txtCuenta" ;
					txn = "5359*" ;
					break ;
			case 3:
					campo = "txtDocsCI" ;
					txn = "0604*" ;
					break ;
			case 4:
					campo = "txtRemesas" ;
					txn = "0510*" ;
					break ;
			case 5:
					campo = "txtEfectivo" ;
					txn = "5503*" ;
		}
		if (eval("gForm."+campo) != undefined)
		{
			montoc = eval("gForm."+campo+".value") ;
			montoc = montoc.toString().replace(/\$|\,|\./g,'') ;
			montoc = Number(montoc) ;
			if (montoc != 0.0)
				flujo = flujo + txn ;
			if (campo == "txtRemesas" && montoc != 0.0)
				gForm.remesas.value = "1" ;
			if(campo == "txtRemesas" && montoc == 0.0)
				gForm.remesas.value = "0" ;
		}
	}

	var lastxn = flujo.substring(flujo.length-5, flujo.length) ;
	lastxn = lastxn.toString() ;
	if(lastxn != "5503*")
		flujo = flujo + "5503*" ;

	var firstxn = flujo.substring(0, 4) ;
	firstxn = firstxn.toString() ;
	if(firstxn == "0604")
		flujo = "5503*0604*" ;
	if(firstxn == "0510")
		flujo = "5503*0510*" ;
	gField.value = flujo ;
	return true ;
}

function isMontoRAP()
{
	messageLookup["isMontoRAP"] = messageIni["isMontoRAP"];

	var mtoacum = "" ;
	var mtotot = "" ;
	var mtocap = "" ;
	var txn = gForm.cTxn.value.toString();

	mtoacum = parent.frames["panel"].MontoPro.toString();
	if ( mtoacum == '0.00')
    	mtoacum = '000' ;
	mtoacum = mtoacum.replace(/\$|\,|\./g,'') ;
	mtoacum = Number(mtoacum) ;

	mtotot = parent.frames["panel"].MontoRAP.toString().replace(/\$|\,|\./g,'');
	mtotot = Number(mtotot) ;

	mtocap = gField.value ;
	mtocap = mtocap.replace(/\$|\,|\./g,'') ;
	if (mtocap == null || mtocap == "" || isNaN(mtocap))
	{
		gField.value = "0.00" ;
		return false ;
	}

	mtocap = Number(mtocap) ;
	suma = mtoacum + mtocap ;

  	if (suma > mtotot)
   	{
		gField.value = "0.00" ;
		return false ;
	}

	if(txn == "0604")
	{
		if(mtotot!=mtocap)
		{
			messageLookup["isMontoRAP"] = new String("El monto a pagar es incorrecto, verificar ....");
			gField.value = "0.00" ;
			return false ;
		}

	}

	if(txn == "0510")
	{
		if(mtotot!=mtocap)
		{
			messageLookup["isMontoRAP"] = new String("El monto a pagar es incorrecto, verificar ....");
			gField.value = "0.00" ;
			return false ;
		}

	}

	isCurrency() ;
	return true ;
}

function Ref1880()
{
	messageLookup["Ref1880"] =	messageIni["Ref1880"];

	var sum = 0 ;
	var mask = new Array(7,6,5,4,3,2) ;
	var inputVal = gField.value ;

	if( inputVal.length != 7 )
	{
		gField.value = "" ;
		return false ;
	}

	inputStr = inputVal.toString() ;
	for(var i=0; i<inputVal.length-1; i++)
		sum += parseInt( inputStr.charAt(i) ) * mask[i] ;

	residuo = 7 - (sum % 7) ;
	if( residuo == inputVal[6] || residuo == 7)
		return true ;
	else
	{
		gField.value = "" ;
		return false ;
	}
}

// valida que el número capturado corresponda a un número de tarjeta American Express válido
function TDCAmex()
{
	messageLookup["TDCAmex"] =	messageIni["TDCAmex"];

	var moneda = gForm.moneda.value ;
	moneda = moneda.toString() ;
	var amex = gField.value ;
	amex = amex.toString() ;
	var patron = amex.match("[0-9]{0,15}");
	if (amex.length != 15 || (patron != amex))
	{
		gField.value = "" ;
		return false ;
	}
	large = parent.frames["panel"].binesAMEX.length
	var i = 0;
   	for(j = 0; j < large; j++)
	{
		bin = parent.frames["panel"].binesAMEX[j];
		bin = bin.toString() ;
		//alert(bin);
		ok = amex.indexOf(bin) ;
		//alert(amex);
		//alert(ok);
		if (ok == 0)
		{
			i=1;
			return true ;
		}
	}
	//alert(i);
	if(i == 0)
		return false;
	return true ;
}

function ValTDC()
{
	messageLookup["ValTDC"] = messageIni["ValTDC"];

	var large ;
	var bin ;
	var ok ;
	var tdc = gField.value ;
	//alert(tdc);
	tdc = tdc.toString();

	tdc = tdc.toString() ;
	if(tdc.length != 16)
	{
		gField.value = "" ;
		return false ;
	}
	large = parent.frames["panel"].bines.length
   	for(j = 0; j < large; j++)
	{
		bin = parent.frames["panel"].bines[j];
		bin = bin.toString() ;
		ok = tdc.indexOf(bin) ;

		if (ok == 0)
		{
			var banco =  parent.frames["panel"].banco[j];
			//alert("banco"+banco);
			var pago = gField.name.substring(4, tdc.length) ;
			//alert("pago"+pago);
			var ser = pago.substring(0,2) ;
			//alert("ser"+ser);
			if(banco != "1")
			{
				 "gForm.cobrocom"+pago+".checked=true" ;
				var coms = eval("gForm.comt"+pago+".value") ;
				FormatCurr(coms,'com'+pago) ;
				var iv = eval("gForm.iva"+ser+".value") ;
				var mtoiv = coms * iv /100 ;
				mtoiv = mtoiv.toString() ;
				var punto = mtoiv.indexOf(".") ;
				if(punto != -1)
					mtoiv = mtoiv.substring(0, punto) ;
				FormatCurr(mtoiv,'iva'+pago) ;
			}
			else
			{
				"gForm.cobrocom"+pago+".checked=false";
				var comg = eval("gForm.comg"+ser+".value") ;
				var comt =0;
				var com = Number(comt) - Number(comg) ;
				if(com > 0)
				{ //se modifico por error
					FormatCurr(com,'com'+pago) ;
					var iva = eval("gForm.iva"+ser+".value") ;
					var mtoiva = com * iva/100 ;
					mtoiva = mtoiva.toString() ;
					var punto = mtoiva.indexOf(".") ;
					if(punto != -1)
						mtoiva = mtoiva.substring(0, punto) ;
					FormatCurr(mtoiva,'iva'+pago) ;
					"gForm.cobrocom"+pago+".checked=true" ;
				}
			}
			return true ;
		}
	}
	gField.value = "" ;
	return false;
}

function ValRem()
{
	messageLookup["ValRem"] = messageIni["ValRem"];

	var rem = gForm.remesas.value ;
	rem = rem.toString() ;

	if (rem == "1")
	{
		var mtosum = 0 ;
		var mto ;

		if(gForm.txtEfectivo != undefined)
		{
			mto = gForm.txtEfectivo.value ;
			mto = mto.toString().replace(/\$|\,|\./g,'') ;
			mto = Number(mto) ;
			mtosum = mtosum + mto ;
		}
		if(gForm.txtCheque != undefined)
		{
			mto = gForm.txtCheque.value ;
			mto = mto.toString().replace(/\$|\,|\./g,'') ;
			mto = Number(mto) ;
			mtosum = mtosum + mto ;
		}
		if(gForm.txtCuenta != undefined)
		{
			mto = gForm.txtCuenta.value ;
			mto = mto.toString().replace(/\$|\,|\./g,'') ;
			mto = Number(mto) ;
			mtosum = mtosum + mto ;
		}
		if(gForm.txtDocsCI != undefined)
		{
			mto = gForm.txtDocsCI.value ;
			mto = mto.toString().replace(/\$|\,|\./g,'') ;
			mto = Number(mto) ;
			mtosum = mtosum + mto ;
		}

		if(mtosum > 0)
		{
			messageLookup["ValRem"] = new String("Combinación de Pago No válida,\nRemesas se usa individualmente..") ;
			return false ;
		}
		var cash = gForm.txtRemesas.value ;
		cash = cash.toString().replace(/\$|\,|\./g,'') ;
		cash = Number(cash) ;

		var np = gForm.npagos.value ;
		np = Number(np) ;

		if(cash > 0)
		{
			var ns = gForm.nservicios.value ;
			ns = Number(ns) ;
			if (ns > 1)
				return false ;
			if (np > 1)
				return false ;
		}
		return true ;
	}
	return true ;
}

function MtoCOPM()
{
	messageLookup["MtoCOPM"] = messageIni["MtoCOPM"];

  	var CLista = gForm.nombres ;

  	if (CLista.options[0].value == 0)
  	{
		if (!isCurrency(gForm.txtMtoCOPM.value))
			return false ;
		else
		{
			gForm.mtocargo.value = gForm.txtMtoCOPM.value ;
			return true ;
		}
	}
	else
	{
		gForm.txtMtoCOPM.value = gForm.mtocargo.value ;
		alert("Con Abonos, el monto del cargo no se puede modificar") ;
	    return true ;
	}
}



function wichForPago()
{
	messageLookup["wichForPago"] =	messageIni["wichForPago"];

	if(gField.selectedIndex == 0 && (gForm.txtNomOrd.value.length == 0 || gForm.txtApeOrd.value.length == 0))
	{
		messageLookup["wichForPago"] = new String('Los campos de Nombre y Apellido deben contener información.') ;
		return false ;
	}
	else if(gField.selectedIndex == 1)
	{
		if((gForm.txtNomOrd.value.length  > 1 || gForm.txtApeOrd.value.length > 1) &&
			(Alltrim(gForm.txtNomOrd.value) != 'N.A.' || Alltrim(gForm.txtApeOrd.value) != 'N.A.'))
			alert('Los campos de Nombre y Apellido del Ordenante no deben contener información, serán omitidos.') ;
		gForm.txtNomOrd.value = 'N.A.' ;
		gForm.txtApeOrd.value = 'N.A.' ;
	}
/*	if(gForm.txtRFC01.value.length > 0)
	{
		var UppCase = Alltrim(gForm.txtRFC01.value) ;
		gForm.txtRFC01.value = UppCase.toUpperCase() ;
	}
	if(gField.selectedIndex == 1 && (gForm.txtNombre.value.length  > 0 || gForm.txtDomici.value.length > 0))
	{
		gForm.txtNombre.value = '' ;
		gForm.txtDomici.value = '' ;
		alert('Los campos de Nombre y Domicilio no deben contener información, serán omitidos.') ;
	}*/
	return true ;
}

/*function wichLiqBank()
{
	messageLookup["wichLiqBank"] =	messageIni["wichLiqBank"];

	if(gField.selectedIndex == 0 && (gForm.txtCiudad.value.length > 0 || gForm.txtInstrucc.value.length > 0))
		alert('Los campos de Ciudad e Instrucciones no deben contener información, serán omitidos.') ;
	else if(gField.selectedIndex == 1 && (gForm.txtCiudad.value.length == 0 || gForm.txtInstrucc.value.length == 0))
	{
		messageLookup["wichLiqBank"] = new String('Los campos de Ciudad e Instrucciones deben contener información.') ;
		return false ;
	}
	return true ;
}*/

function isSerial()
{
	messageLookup["isSerial"] =	messageIni["isSerial"];

	var inputVal = gField.value ;
	var maxDigits = 10 ;

	if( isEmpty(inputVal) || !isInteger(inputVal) || inputVal < 1)
	{
		gField.value = "" ;
		return false ;
	}

	/* Llanado funcion Procesar DV del Cheque */
	if (!processDigVDocCheque("isSerial")) {
		return false;
	}

	if(gForm.name == 'entry')
	{
		if(gForm.cTxn.value == '0572' || gForm.cTxn.value == '0816' ||
		gForm.oTxn.value == '0116' || gForm.cTxn.value == '0104'  )
		{
			maxDigits = 9 ;
			if(gField.value.length != 9)
			{
				if(gField.value.length == 10 && gField.value.substring(0,1) == '0')
					return true;
				messageLookup["isSerial"] = new String("Serial no valido.") ;
				gField.value = "" ;
				return false ;
			}
			if(gField.value.substring(0,3) != '251' && gField.value.substring(0,3) != '258')
			{
				messageLookup["isSerial"] = new String("Serial no valido. Debe empezar con '251' o '258'") ;
				gField.value = "" ;
				return false ;
			}
		}
	}

	while( inputVal.length < maxDigits )
		inputVal = '0' + inputVal ;
	gField.value = inputVal ;

	return true ;
}
//ygx
function isSerialCheq()
{
	messageLookup["isSerialCheq"] =	messageIni["isSerialCheq"];

	var inputVal = gField.value ;
	var maxDigits = 7 ;

	/* Llanado funcion Procesar DV del Cheque */
	if (!processDigVDocCheque("isSerialCheq")) {
		return false;
	}

	if( isEmpty(inputVal) || !isInteger(inputVal) || inputVal < 1)
	{
		gField.value = "" ;
		return false ;
	}

	if(gForm.name == 'entry')
	{
		if(gForm.cTxn.value == '0572' || gForm.cTxn.value == '0816' ||
		gForm.oTxn.value == '0116' || gForm.cTxn.value == '0104'  )
		{
			maxDigits = 9 ;
			if(gField.value.length != 9)
			{
				if(gField.value.length == 10 && gField.value.substring(0,1) == '0')
					return true;
				messageLookup["isSerialCheq"] = new String("Serial no valido.") ;
				gField.value = "" ;
				return false ;
			}
			if(gField.value.substring(0,3) != '251' && gField.value.substring(0,3) != '258')
			{
				messageLookup["isSerialCheq"] = new String("Serial no valido. Debe empezar con '251' o '258'") ;
				gField.value = "" ;
				return false ;
			}
		}
	}

	while( inputVal.length < maxDigits )
		inputVal = '0' + inputVal ;
	gField.value = inputVal ;

	return true ;
}

function isMontoCargo()
{
	messageLookup["isMontoCargo"] =	messageIni["isMontoCargo"];

	var inputStr = Alltrim(gForm.txtMontoCheque.value) ;
	inputStr = inputStr.toUpperCase() ;
	montoacu = parent.frames["panel"].MontoPro.toString() ;

	if ( montoacu == '0.00')
		montoacu = '000' ;

	montoacu = montoacu.replace(/\$|\,/g,'') ;
	montotot = parent.frames["panel"].MontoCheques.toString().replace(/\$|\,/g,'') ;

	if ( inputStr == null || inputStr == "")
	{
	  	gField.value = '0.00';
       	return false ;
    }

	if (!isCurrency())
	{
	  	return false ;
	}

	inputStr = Alltrim(gForm.txtMontoCheque.value) ;
	inputStr = inputStr.toString().replace(/\$|\,/g,'') ;
	intVal = montoacu.substring(0, montoacu.indexOf(".")) ;
	decVal = montoacu.substring(montoacu.indexOf(".") + 1, montoacu.length) ;
	montoacu = intVal + decVal ;

	intVal = montotot.substring(0, montotot.indexOf(".")) ;
	decVal = montotot.substring(montotot.indexOf(".") + 1, montotot.length) ;
	montotot = intVal + decVal ;

	intVal = inputStr.substring(0, inputStr.indexOf(".")) ;
	decVal = inputStr.substring(inputStr.indexOf(".") + 1, inputStr.length) ;
	inputStr = intVal + decVal ;

	if(gForm.cTxn.value == '5103')
	{

		if(inputStr != montotot)
		{
			messageLookup["isMontoCargo"] = new String("Monto inválido, no es igual al total, verificar...") ;
			return false;
		}
	}

	monto1 = parseInt(montoacu) + parseInt(inputStr) ;

	if ( monto1 > parseInt(montotot))
	{
		gForm.txtMontoCheque.value = "0.00" ;
		return false ;
	}

	isCurrency() ;

	return true;
}

// =============================================================================================
// Funciones auxiliares utilizadas por las de validación
// =============================================================================================

// Función que remueve los espacios en blanco de un string de ambos lados
function Alltrim(inputStr)
{
  var inStr = new String(inputStr) ;

  while(inStr.charAt(0) == ' ')
    inStr = inStr.substring(1, inStr.length) ;

  while(inStr.charAt(inStr.length) == ' ')
    inStr = inStr.substring(0, inStr.length-1) ;

  return inStr ;
}

// Función que valida si el string enviado como parametro está vacío o compuesto por blancos
function isEmpty(inStr)
{
  var inputStr = Alltrim(inStr) ;
  inputStr = inputStr.toUpperCase() ;

  if (inputStr == null || inputStr == "" || inputStr.length == 0)
  {
    gField.value = '';
    return true ;
  }

  gField.value = inputStr;
  return false ;
}

// Función que regresa si el string enviado como parámetro está vacío
function isEmptyStr()
{
  messageLookup["isEmptyStr"] = messageIni["isEmptyStr"];

  var inputVal = gField.value;
  var largo = inputVal.length;

  if(gForm.iTxn != undefined && gForm.cTxn != undefined)
  {
    if (gForm.iTxn.value == '4529' && gForm.cTxn.value == '4529' && inputVal.length == 0 && gField.name == 'txtBeneficiario')
      return true;

    if((gForm.iTxn.value == '0021' || gForm.iTxn.value == '4033') && (gField.name == "txtNomOrd" || gField.name == "txtNomBen" || gField.name == "txtApeBen" || gField.name == "txtApeOrd"))
    {
       cadtemp = gField.value;
      if(!isNaN(cadtemp.substring(0,1)) && gField.value != "" ){
		messageLookup["isEmptyStr"] = new String("Los campos de nombre y apellido deben comenzar con una letra");
		gField.value = "";
		return false;
      }

      if(gField.name == "txtNomOrd" &&  largo > 15){
	      messageLookup["isEmptyStr"] = new String("El Nombre del Ordenante solo acepta 15 caracteres");
	      gField.value = "";
	      return false;
      }else if(gField.name == "txtNomBen" &&  largo > 15){
	      messageLookup["isEmptyStr"] = new String("El Nombre del Beneficiario solo acepta 15 caracteres");
	      gField.value = "";
	      return false;
      }else if(gField.name == "txtApeBen" &&  largo > 19){
	      messageLookup["isEmptyStr"] = new String("El Apellido del Beneficiario solo acepta 19 caracteres");
	      gField.value = "";
	      return false;
      }else if(gField.name == "txtApeOrd" &&  largo > 19){
	      messageLookup["isEmptyStr"] = new String("El Apellido del ordenante solo acepta 19 caracteres");
	      gField.value = "";
	      return false;
      }else if(inputVal.indexOf('*') != -1){
	      messageLookup["isEmptyStr"] = new String("Los nombres y apellidos de Ordenante y Beneficiario no deben contener asteriscos");
	      gField.value = "";
	      return false;
      }

    }

    if((gForm.iTxn.value == '0726' || gForm.iTxn.value == '4043') && gForm.oTxn.value == '0786' && gField.name == "txtBeneficiario")
    {

      if(largo > 20)
      {
	messageLookup["isEmptyStr"] = new String("El Beneficiario solo acepta 20 caracteres");
	gField.value = "";
	return false;
      }
    }

    if(gForm.iTxn.value == "0068" && gField.name == 'txtRFCSUA'){//PARA SUAS
        cadtemp1 = gField.value;
        cadtemp2 = cadtemp1.substring(largo-1,largo)
    	if(largo <12){
    		messageLookup["isEmptyStr"] = new String("El Registro Patronal debe contener 12 caracteres");
    		gField.value = "";
    		return false;
    	}
    	else if(largo == 12)
    	    	 if( cadtemp1.charAt(11)!='1' && cadtemp1.charAt(11)!='6'){
    	    		messageLookup["isEmptyStr"] = new String("Digito Verificador Inválido");
    				gField.value = "";
		    		return false;
    			}
    }
	
	if(gForm.iTxn.value == "1197") {
    	//gField.value = gField.value.replace(/\#/g,'');
    	valor_campo = gField.value;
        var buf = valor_campo.match(/#/g);
        if( buf != null)
        {
            messageLookup["isEmptyStr"] = new String("No se permite el caracter '#', Favor de Verificar...");
            gField.value ="";
            gField.focus();
            return false;
        }

    }
	
  }

  return !isEmpty(inputVal);
}

// Función que determina si un número es negativo
function isNegative(inputVal)
{
  var num = Alltrim(inputVal) ;
  if(num.indexOf("-")==0)
    return true;
  return false;
}

// Función que da formato a un string conteniendo un número con formato de Moneda
function Currency(inputVal)
{
  var num = inputVal.toString()

  intVal = num.substring(0, num.indexOf("."))
  decVal = num.substring(num.indexOf(".") + 1, num.length)

  num = intVal + decVal;
  if( isNaN(num) )
  {
    inputVal = "0.00"
    return inputVal;
  }

  cents = Math.floor((num)%100)
  num = Math.floor((num)/100).toString()


  if(cents < 10)
    cents = "0" + cents

  for (var i = 0; i < Math.floor((num.length-(1+i))/3); i++)
    num = num.substring(0,num.length-(4*i+3))+','+num.substring(num.length-(4*i+3))

  inputVal = num + '.' + cents
  return inputVal;
}

// Función para darle formato monetario a un campo
function formatCurr(num)
{
	var punto = 0 ;

	for(var i=0; i<num.length; i++)
		if(num.charAt(i) == ".")
			punto = i;

	if (punto == 0)
		num = num.substring(0,num.length-2) + "." + num.substring(num.length-2,num.length) ;
	else if(!(num.length-2 == num.length-punto))
	{
		num = num.substring(0,punto) + num.substring(punto+1,num.length) ;
		num = num.substring(0,num.length-2) + "." + num.substring(num.length-2,num.length) ;
	}

	if( isNaN(num) )
	{
		gField.value = "" ;
		return false ;
	}

	cents = Math.floor((num*100+0.5)%100) ;
	num = Math.floor((num*100+0.5)/100).toString() ;
	if(cents < 10)
		cents = "0" + cents ;

	for (var i = 0; i < Math.floor((num.length-(1+i))/3); i++)
		num = num.substring(0,num.length-(4*i+3))+','+num.substring(num.length-(4*i+3)) ;
	gField.value = num + '.' + cents ;
	return true ;
}

// Validación del módulo 10 de referencias de Luz y Fuerza
function isLYFmodulo10(ref)
{
	var total = 0 ;
	var valor =0 ;
	var multi = 0 ;
	var factor = 0 ;
	var ctrl = 2 ;
	var z ;
	var TOTAL ;
	var digito ;

	for(z=ref.length-2; z>=0; z--)
	{
		valor = parseInt(ref.charAt(z)) ;
		multi = ctrl % 2 ;
		if (multi == 0)
			factor=2 ;
		else
			factor=1 ;
		valor = valor * factor ;
		if (valor == 10)
			valor = 1 ;
		if (valor > 10)
		{
			DIG = new String(valor) ;
			dig = parseInt(DIG.substring(0,1)) ;
			dig = dig + parseInt(DIG.substring(1)) ;
			valor = dig;
		}
		total = total + valor ;
		ctrl++ ;
	}
	TOTAL = new String(total) ;
	digito = parseInt(TOTAL.substring(1)) ;
	digito = 10 - digito ;

        if (digito == 10)
          digito = 0;
	if (digito == parseInt(ref.substring(ref.length-1)))
		return true ;
	else
		return false ;
}

// Validación del módulo 11 de referencias de Luz y Fuerza
function isLYFmodulo11(ref)
{
	var ctrl =2 ;
	var total = 0 ;
	var digver = parseInt(ref.substring(ref.length-1)) ;
	for (z=ref.length-2; z>=0; z--)
	{
		valor = parseInt(ref.charAt(z)) ;
		total = total + (valor * ctrl) ;
		ctrl ++ ;
		if (ctrl>10)
			ctrl=2 ;
	}
	digito = total % 11 ;
	if (digito == 1)
		digito = 0 ;
	else if (digito == 0)
		digito = 1 ;
	else
		digito = 11 - digito ;
	if (digver == digito)
		return true ;
	else
		return false ;
}


function mod10(tdc,vec)
{
	var sum =0;
	var restdc;
	var temp;
	var large = tdc.length;

	        cad = tdc.substring(0,large) ;
		inputStr = cad.toString() ;
		for(var i=0; i<large; i++)
		{
			temp = Number(inputStr.charAt(i)) * vec[i] ;
			if( temp < 10 )
				sum += temp ;
			else
				sum = sum + (Math.floor(temp / 10) + (temp % 10)) ;

		}

		restdc = sum % 10;

		if(restdc == 0)
		   return true;
		else
		   return false;


}



function mod10allcad(ref)
{
	var total = 0 ;
	var valor =0 ;
	var multi = 0 ;
	var factor = 0 ;
	var ctrl = 1 ;
	var DIG ;

	for(z=ref.length-1; z>=0; z--)
	{
		valor = Number(ref.charAt(z)) ;
		multi = ctrl % 2 ;
		if (multi == 0)
			factor=2 ;
		else
			factor=1 ;
		valor = valor * factor ;
		if (valor == 10 )
			valor = 1 ;
		if (valor > 10)
		{
			DIG = new String(valor) ;
			dig = Number(DIG.substring(0,1)) ;
			dig = dig + Number(DIG.substring(1)) ;
			valor = dig ;
		}
		total = total + valor ;
		ctrl ++ ;
	}
	digito = total % 10 ;
	return digito ;
}

// Función que redondea una cantidad a centavos
function redonvalor(valor)
{
  var num = valor.toString().replace(/\$|\,/g,'') ;

  if( isNaN(num) )
  {
    return canti = 0.00 ;
  }

  cents = Math.floor((num*100+0.5)%100) ;
  num = Math.floor((num*100+0.5)/100).toString() ;

  if(cents < 10)
    cents = "0" + cents ;

  for (var i = 0; i < Math.floor((num.length-(1+i))/3); i++)
    num = num.substring(0,num.length-(4*i+3))+','+num.substring(num.length-(4*i+3)) ;

  canti = num + '.' + cents ;
  return  canti ;
}

// Valida que el número de tarjeta sea válido y regresa su dígito verificador
function validnumtar(ref)
{
	if ( ref.length > 50)
	{
		ref = ref.substring(2,18) ;
		gField.value = ref ;
		alert("Presione Enter.") ;
	}
	else
		if ( ref.length > 32  && ref.length < 36)
		{
			ref = ref.substring(1,17) ;
			gField.value = ref ;
		}
	if ( isNaN(ref) )
		return 1;

	if (ref.length != 16 && ref.length != 13)			// OK 11-NOV-05

	    return 1;
	else
	{

	 if ((ref == "0000000000000000") || (ref == "0000000000000"))	// OK 11-NOV-05
	     return 1;
	 else
	digitover = mod10allcad(ref) ;

	}

	return digitover ;
}

function isluzyfuerzaespecial(refer)
{
	if (!isLYFmodulo11(refer.substring(2) + refer.substring(0,1))) //* modulo 11
		return false ;
	if (!isLYFmodulo10(refer.substring(2) + refer.substring(1,2))) //* modulo 10
		return false ;
	return true;
}

function isluzyfuerzanormal(refer)
{
	if (!isLYFmodulo10(refer.substring(2) + refer.substring(0,1))) //* modulo 10
		return false ;
	if (!isLYFmodulo11(refer.substring(2)+ refer.substring(1,2))) //* modulo 11
		return false ;
	return true ;
}

function isVMtoEfect(NameEfect,NameMto)
{
	var monto = eval("gForm." + NameMto + ".value") ;
	var efectivo = eval("gForm." + NameEfect + ".value") ;
	if (parseInt(monto) == 0 && parseInt(efectivo) > 0)
		eval("gForm." + NameMto + ".value = gForm." + NameEfect + ".value") ;
	if ((parseInt(monto) == 0 && parseInt(efectivo) == 0) || (parseInt(efectivo) >  parseInt(monto)))
		return false ;
	return true ;
}

// Valida que el caracter capturado sea una letra
function isAlpha(inputVal)
{
  if((inputVal>='A' && inputVal<='Z') || (inputVal>='a' && inputVal<='z') || inputVal=='Ñ' || inputVal=='ñ' || inputVal == "&")
    return true;

  return false;
}

// Regresa la hora del sistema
function syshour()
{
	var Fecha = new Date() ;
	var hora =  Fecha.getHours() ;
	var minutos = Fecha.getMinutes() ;
	var segundos = Fecha.getSeconds() ;

	if ( minutos < 10 )
		minutos = "0" + minutos;
	if ( segundos < 10 )
		segundos = "0" + segundos;
	if ( hora < 10 )
		hora = "0" + hora;

	hour = hora + ":" + minutos + ":" + segundos;
	return hour;
}

function RegEmpl(frame)
{
	gForm  = eval("window." + frame.name + ".document.forms[0]");
	var registro = gForm.registro.value;
		return registro;
}

function isInteger(inputVal)
{
	var inputStr = inputVal.toString() ;
	for (var i = 0; i < inputStr.length; i++)
	{
		var oneChar = inputStr.charAt(i) ;
		if (oneChar < "0" || oneChar > "9")
			return false ;
	}
	return true ;
}


function Llenar_Campos(sBanda)
{
//	gForm.txtCodSeg.value     = sBanda.substring(1,5);  // Codigo de Seguridad
//	gForm.txtCveTran.value    = sBanda.substring(6,15); // Cve. de Transito
//	gForm.txtDDACuenta2.value = sBanda.substring(16,27);// No. de Cuenta
//	gForm.txtSerial2.value    = sBanda.substring(28,35);// Serial del cheque
	gForm.txtCodSeg.value     = "";
	if (sBanda.length >= 35){
	    gForm.txtCveTran.value    = sBanda.substring(12,16) + sBanda.substring(17,21); // Cve. de Transito
	    gForm.txtDDACuenta2.value = sBanda.substring(23,34);// No. de Cuenta
	    gForm.txtSerial2.value    = sBanda.substring(2,9);// Serial del cheque*/
	}else{
		 if (sBanda.length == 31){
		    gForm.txtCveTran.value    = sBanda.substring(2,6) + sBanda.substring(7,11); // Cve. de Transito
			gForm.txtDDACuenta2.value = sBanda.substring(13,24);// No. de Cuenta
			gForm.txtSerial2.value    = "0"+sBanda.substring(25,31);// Serial del cheque

		 }else if (sBanda.length == 32){
		    gForm.txtCveTran.value    = sBanda.substring(12,16) + sBanda.substring(17,21); // Cve. de Transito
			var cuentatemp=sBanda.substring(23,32);
			var temp="";
		    for(j=0;j<=cuentatemp.length;j++)
		    {
		    	if(!(cuentatemp.substring(j,j+1) == " "))
		    		temp=temp+cuentatemp.substring(j,j+1);// No. de Cuenta
			}
			if(temp.length < 11 && temp.length != 0)
   		         temp=filler(temp,11);
			gForm.txtDDACuenta2.value = temp;
			gForm.txtSerial2.value    = sBanda.substring(2,9);// Serial del cheque
		 }
	}
}


function exeValid()
{
	// Validar que los campos correspondientes a la banda solo sean numeros.
	var temporal = '' ;
	if(istxtInt() == false)
	{
		LimpiaBanda();
		gForm.txtCodSeg.focus() ;
		return false;
	}

	// Validar la longitud de digitos para cada campo.
	if( txtLength() == false )
	{
		LimpiaBanda();
		gForm.txtCodSeg.focus() ;
		return false;
	}

//  valida  si es cheque cmtH  valida el numero de banco
	if( Validar_No_de_Banco(gForm.txtCveTran.value.substring(3,6)) == false )
	{
		LimpiaBanda() ;
		gForm.txtCodSeg.focus() ;
		return false;
	}

	// Validar No. de Cuenta
	cuenta = gForm.txtDDACuenta2.value.substring(1,2) ;
	banco = gForm.txtCveTran.value.substring(5,8) ;

	if( isValidDDA(gForm.txtDDACuenta2.value) == false && banco == "148")
	{
		messageLookup[gMethod] = new String("El numero de Cuenta no es valido. Por favor verifique...") ;
		LimpiaBanda();
		gForm.txtDDACuenta2.focus();
		return false;
	}


	plaza = gForm.txtCveTran.value.substring(2,5) ;
	tipodoc = gForm.txtCveTran.value.substring(0,2) ;

	if ( banco == "148" )
	{         // validar tipo de documento
		if ( plaza == "999" && (tipodoc!="51" && tipodoc!="55"  && tipodoc!="52" && tipodoc!="70" && tipodoc!="74" && tipodoc!="71"))
		{
			LimpiaBanda();
			gForm.txtCodSeg.focus() ;
			return false;
		}

		if ( plaza!= "999" && ( tipodoc!="51" && tipodoc!="55" && tipodoc!="70" && tipodoc!="71" && tipodoc!="74"))
		{
			LimpiaBanda();
			gForm.txtCodSeg.focus() ;
			return false;
		}
	}
//valida digito de Intercambio Valido
/*	if( Validar_Dig_de_Intercambio( gForm.txtCveTran.value.substring(5,8),gForm.txtDDACuenta2.value,gForm.txtCveTran.value.substring(8,9) ) == false ||
	Validar_digito_de_Premarcado(gForm.txtCodSeg.value,gForm.txtCveTran.value,gForm.txtDDACuenta2.value,gForm.txtSerial2.value ) == false )
	{
		if ( banco == "021" )
		{
			// validar cuentas especiales
			temporal = gForm.txtDDACuenta2.value ;
			if(temporal.length == 11)
				temporal = temporal.substring(1,11) ;

			if(LocalizarCuenta(temporal) == false)
			{
				if (gForm.txtCodSeg.value=="0225" && gForm.txtCveTran.value=="709990215"  && gForm.txtDDACuenta2.value.substring(0,3)=="071")
				{
					alert("Es una cuenta Barkclay") ;
					return true;
				}

				LimpiaBanda();
				gForm.txtCodSeg.focus() ;
				return false;
			}
			else
				return true;
		}

		LimpiaBanda();
		gForm.txtCodSeg.focus() ;
		return false;
	}*/

	if ( gForm.txtDDACuenta2.value == "00103000023" || gForm.txtDDACuenta2.value == "03902222222")
		if(!isChequeCaja())
		{
			LimpiaBanda();
			return false;
		}
	return true;
}
//FINAL VALIDA CAMPOS DE CHEQUE

function istxtInt()
{
	if( isInteger(gForm.txtCodSeg.value) == false )
	{
		return false ;
	}

	if( isInteger(gForm.txtCveTran.value) == false )
	{
		return false ;
	}

	if( isInteger(gForm.txtDDACuenta2.value) == false )
	{
		return false ;
	}

	if( isInteger(gForm.txtSerial2.value) == false )
	{
		return false ;
	}

	return true ;
}


// Funcion que limpia los campos que componen la banda magnética de un cheque
function LimpiaBanda()
{
	gForm.txtCodSeg.value = '' ;
	gForm.txtCveTran.value = '' ;
	gForm.txtDDACuenta2.value = '' ;
	gForm.txtSerial2.value = '' ;
}


// Valida la longitud de los diferntes campos que componen la banda magnética de un cheque
function txtLength()
{
	if(gForm.txtCodSeg.value.length != 0)
	{
		messageLookup["isValidCheque"] = new String("El codigo de Seguridad debe ser de Longitud Cuatro.") ;
		return false ;
	}

	if(gForm.txtCveTran.value.length != 8)
	{
		messageLookup["isValidCheque"] = new String("La Cve. de Transito debe ser de Longitud Nueve.") ;
		return false ;
	}

	if(gForm.txtDDACuenta2.value.length != 11)
	{
		messageLookup["isValidCheque"] = new String("La Cuenta debe ser de Longitud Once.") ;
		return false ;
	}

	if(gForm.txtSerial2.value.length != 7)
	{
		messageLookup["isValidCheque"] = new String("El Serial debe ser de Longitud Siete.") ;
		return false;
	}

	return true;
}


// Funcion para validar que el No. de Banco en un cheque HSBC sea el 021-->Pos(4,5,6)148
//														Banistmo  --> pos(5 y 6) 88
function Validar_No_de_Banco(sAux1)
{
	var iNoBanco = sAux1 ;
	var txn = new String(gForm.cTxn.value) ;
    var codseg = gForm.txtCodSeg.value;
    var cta    = gForm.txtDDACuenta2.value.substring(1,2);
	//Verifica si es Cheque de Agencia Anterior rzr
        if (codseg == "0200" && cta == '8')
          return true;
	// No de Banco que le corresponde a cmtH
	/*if(iNoBanco != "148" && txn.substring(0,1)!= "0" && txn != "1009" && txn != "1183" )
	{
		// No corresponde a cmtH
		messageLookup[gMethod] = new String("Error, No es cheque cmtH !!!") ;
		return false;
	}*/

	// No de Banco que le corresponde a cmtH
	if(iNoBanco =="148" && ( txn.substring(0,1)== "0" || txn == "1009" || txn == "1183"))
	{
		// No corresponde a otro Banco
		messageLookup[gMethod] = new String("Error, Es cheque ENTIDAD BANCARIA !!!") ;
		return false ;
	}

	if(iNoBanco == "148")
	{
		for(var i = 0; i < gForm.elements.length; i++)
		{
			if(gForm.elements[i].name == 'rdoACorte' && gForm.elements[i].checked == true)
			{
				gForm.elements[i].checked = false ;
				alert('No es necesario seleccionar esta opción para cheques ENTIDAD BANCARIA') ;
			}
		}
	}

	return true;
}

// Funcion para detectar que se presiono la tecla ENTER
function keyPressedHandler(frame, field, method)
{
   e = frame.event;
  if (e.keyCode == "13")
  {
    	if (field.value != "")
    		field.blur();
	if( document.all )
		e.returnValue = false;
  }
}

// Determina si un campo debe de ser validado o no, en base al contenido o falta del mismo
function estaVacio(un_campo){

	if(un_campo.value == ""){
		return true;
	}else{
		return false;
	}
}


function isValidDDA(inputVal)
{
	var sum = 0 ;
	var mask = new Array(0,0,2,1,2,1,2,1,2,0) ;
	var acct = new Array(10) ;
	var txno = '' ;
	var txni = '' ;
	var txnc = '' ;
	var compania = '' ;
	var txn = new Array("0600","S003","1191","1197","5903","4503","4139","0554","5453","1065","5203",
						"5487","5051","1523","S009","1053","1073","1193","1195","1119","1124","1125",
						"1161","1091","5353","1525","M017","4059","5261","5287","S001","5505","5507",
						"0110","0112","0114","0554","0726","4043","4057","4063","0108","0087",
						"0041","0045","5509","5511","5453","4385","4303","5503","4287","5048","4159",
						"4165","4175","4167","4135","4137","4143","4149","4163","4169","4173","4129",
						"4127","4131","4133","0312","0314","0590","4141","8000","0104") ;

	inputStr = inputVal.toString() ;

    if (gForm.diario == undefined)
    {
      txno = new String(gForm.oTxn.value) ;
	  txni = new String(gForm.iTxn.value) ;
	  txnc = new String(gForm.cTxn.value) ;
      compania = gForm.compania.value ;
    }

	if(inputVal.length == 11)
	{
		inputStr = inputVal.toString().substring(1,11) ;
		inputVal = inputStr;
	}

	if(inputVal.length < 10)
		return false ;

	for(var i=2; i<inputVal.length-1; i++)
	{
		if( i % 2 != 0 )
			sum += parseInt( inputStr.charAt(i) ) ;
		else
		{
			temp = parseInt( inputStr.charAt(i) * 2 ) ;
			if( temp < 10 )
				sum += temp ;
			else
				sum = sum + Math.floor(temp / 10) + (temp % 10) ;
		}
	}

	if( (sum % 10 == 0 ) && (parseInt(inputStr.charAt(inputStr.length-1)) == 0) )
		return true ;

	x = 10-(sum % 10) ;
	if( (10-(sum % 10)) == parseInt(inputStr.charAt(inputStr.length-1)) )
		return true ;
	else
	{
	    if (gForm.compania != undefined)
		  gForm.compania.value = compania ;

  	    messageLookup[gMethod] = new String("El número de Cuenta no es valido. Por favor verifique...") ;
		return false ;
	}
}


// Funcion para validar el digito de intercambio de un cheque.
function Validar_Dig_de_Intercambio(sNobanco, sCuenta, iDigInter)
{
       //Verifica si es Cheque de Agencia Anterior rzr
        var codseg = gForm.txtCodSeg.value;
        var cta = sCuenta.substring(1,2);
        if (codseg == "0200" && cta == '8')
          return true;

	var iItem = 0 ; var iSuma = 0 ; var iDigito = 0 ; var iDigVer = 0 ;
	var BancoCuenta = sNobanco+sCuenta ;

	/* Ejemplo            02104016322372(Banco+Cuenta)
	Multiplicar por :  37137137137137
	Sumatoria del primer d¡gito del resultado de cada multiplicacion
	(SUM), Tomar el primer digito(der-izq) y restarlo de 10.
	Si coincide con el Digito de Intercambio entonces cheque valido.
	*/
	for(var i=0; i < BancoCuenta.length; i++)
	{
		iItem = BancoCuenta.charAt(i)*3 ;
		iDigito = Digito(iItem) ;
		iSuma = iSuma + iDigito ;
		i++ ;

		iItem = BancoCuenta.charAt(i)*7 ;
		iDigito = Digito(iItem) ;
		iSuma = iSuma + iDigito ;
		i++ ;

		if(i < BancoCuenta.length)
		{
			iItem = BancoCuenta.charAt(i)*1 ;
			iDigito = Digito(iItem) ;
			iSuma = iSuma + iDigito ;
		}
	}

	iDigito = Digito(iSuma) ;

	if ( iDigito != 0 )
		iDigVer = 10 - iDigito ;

	if( iDigVer != iDigInter )
	{
		messageLookup[gMethod] = new String("Error, Digito de Intercambio Invalido.") ;
		return false ;
	}

	return true;
}

// Funcion para determinar si el digito de Premarcado es valido
// Para esto se suman el codigo de seguridad + clave de transicion + No. de Cuenta + Serial
// se obtiene el modulo 9, el residuo se resta de 9, si esta operacion anterior es igual
// al cuarto digito del codigo de seguridad entonces este digito de premarcado es valido
// en otro caso no lo es.  p.e. suponiendo que la banda es 9378  519990211 04016322372 0000275
// donde:
// 9378        : codigo de seguridad(8 digito de premarcado(dig. mas a la derecha) )
// 519990211   : No. de trancision(1 digito de intercambio(dig. mas a la derecha) )
// 04016322372 : No. de cuenta
// 0000275     : Serial
//
//Esto es :       937 + 519990211 + 04016322372 + 0000275 = 4536313795
// ( 9 % 4536313795 ) = 1
// (9 - 1) = 8 ; 8==8 => digito de premarcado valido.

function Validar_digito_de_Premarcado( sCodigoSeguridad, sClaveTran, sCuenta, sSerial )
{
       //Verifica si es Cheque de Agencia Anterior rzr
        if (sCodigoSeguridad == "0200" && sCuenta.substring(1,2) == "8")
          return true;

	var fSuma = 0 ; var iModulo = 0 ; var iDigVer = 0 ;
	var iCodSeg = sCodigoSeguridad.substring(0,3) ;

	fSuma = parseFloat(iCodSeg) + parseFloat(sClaveTran) + parseFloat(sCuenta) + parseFloat(sSerial) ;
	iModulo = ( fSuma % 9 ) ;
	iDigVer = 9 - iModulo ;
	if( iDigVer == (sCodigoSeguridad.substring(3,4)) )
		return true;
	else
	{
		messageLookup[gMethod] = new String("Error, Digito de Premarcado Invalido.") ;
		return false ;
	}
}

function LocalizarCuenta(Cuenta)
{
	var i, j ;
    	j = parent.frames["panel"].specialacct.length;

	for(i = 0; i < j; i++)
	{
        	if(parent.frames["panel"].specialacct[i] == Cuenta)
		{
			return true ;
		}
	}

	return false; // No se encontro la cuenta
}

// Funcion para obtener el digito a la derecha de una cifra
function Digito(iItems)
{
	var iMult=0;

	if(iItems < 10)
		return iItems;
	for(var l=1; l<=14; l++)
	{
		iMult = l*10;
		if ( (iItems >= iMult) && ( iItems <= (iMult+9) ) )
			return(iItems-iMult );
	}
}

function isEmptyField(inStr)
{
	var inputStr = Alltrim(inStr) ;
	var car, i, car2 ;

	inputStr = inputStr.toUpperCase() ;
	if (inputStr == null || inputStr == "")
		return true ;

	return false ;
}


function isValidCDA(inputVal)
{
	var sum = 0 ;
	var mask = new Array(5,4,3,2,7,6,5,4,3,2,1) ;
	var txn = new Array("M016","2523","2525","3001","3009","3219","3261","3313","3314","3487","3532", "3021", "2527","2529") ;
	var itxn = '' ;
	var compania = '' ;
    var moneda = '';

    if (gForm.diario == undefined)
    {
      itxn = new String(gForm.iTxn.value) ;
	  compania = gForm.compania.value ;
	  moneda = gForm.moneda.value;
    }

	if( inputVal.length != 15 )
		return false ;

	inputVal = inputVal.substring(0, 11) ;

	inputStr = inputVal.toString() ;
	for(var i=0; i<inputVal.length-1; i++)
		sum += parseInt( inputStr.charAt(i) ) * mask[i] ;

	residuo = 11 - (sum % 11) ;
	digito = inputVal.substring(0,2) ;



	if( residuo == inputVal.charAt(10) || residuo == 11)
	{
		if ( compania == "20" && ( itxn == "3021" || itxn == "2527" || itxn == "2529"))
		{
			gForm.compania.value == compania ;
			return false ;
		}
		return true;
	}
	else
	{
		if (compania != '')
		  gForm.compania.value == compania ;

		return false ;
	}

}


function quitaceros(inputVal)
{
	for(var i=0; i<inputVal.length; i++)
	{
		if( parseInt(inputVal.charAt(i)) != 0 )
			break;

		inputVal = inputVal.substring(i+1,inputVal.length);
	}

	return inputVal ;
}

function isDate(inputVal)
{
	var dateStr  = "" ;
	var inputStr = inputVal.toString() ;

	for(var i=0; i<inputStr.length; i++)
	{
		if( inputStr.charAt(i) == "-" )
			dateStr += "/" ;
		else
			dateStr += inputStr.charAt(i) ;
	}

	var delim1 = dateStr.indexOf("/") ;
	var delim2 = dateStr.lastIndexOf("/") ;

	if (delim1 != -1 && delim1 == delim2)
		return false ;

	var dd   = parseInt(inputStr.substring(0, delim1), 10) ;
	var mm   = parseInt(inputStr.substring(delim1 + 1, delim2), 10) ;
	var yyyy = parseInt(inputStr.substring(delim2 + 1, inputStr.length), 10) ;

	if( !(isInteger(dd) && isInteger(mm) && isInteger(yyyy)) )
		return false ;

	var daysInMon = new Array(0,31,0,31,30,31,30,31,31,30,31,30,31) ;

	if( yyyy % 4 != 0 )
		daysInMon[2] = 28 ;
	else if( yyyy % 400 == 0 )
		daysInMon[2] = 29 ;
	else if( yyyy % 100 == 0 )
		daysInMon[2] = 28 ;
	else
		daysInMon[2] = 29 ;

	if( mm < 1 || mm > 12 )
		return false ;
	if( dd < 1 || dd > daysInMon[mm] )
		return false ;
	if( yyyy < 1900 )
		return false ;

	return true ;
}

// De formato a un campo monetario
function FormatCurr(val, field)
{
	var num = val.toString().replace(/\$|\,/g,'') ;
	var intVal = num.substring(0, num.indexOf("."))
	var decVal = num.substring(num.indexOf(".") + 1, num.length)
	num = intVal + decVal;

	if( isNaN(num) )
		return false ;

	cents = Math.floor((num)%100) ;
	num = Math.floor((num)/100).toString() ;

	if(cents < 10)
		cents = "0" + cents ;
	if(cents.length == 1)
		cents = cents + "0" ;

	for (var i = 0; i < Math.floor((num.length-(1+i))/3); i++)
		num = num.substring(0,num.length-(4*i+3))+','+num.substring(num.length-(4*i+3)) ;

	num = num + '.' + cents ;
	eval("gForm."+field+".value = '"+ num + "'") ;
}

function addNewOption(frame)
{
	var flag ;
	var error ;

    gForm  =  eval("window." + frame.name + ".document.forms[0]") ;

	var montoC = gForm.mtocargo.value ;
	montoC = montoC.toString().replace(/\$|\,/g,'') ;
	montoC = montoC.toString().replace(/\$|\,|\./g,'') ;
	var txn = gForm.cTxn.value ;

	if (txn == '4227' || txn == '5153')
	{
		campocta = eval("gForm.txtDDAC.value") ;
		if (campocta == '' || montoC == '' || Number(montoC) <= 0.00)
			flag = 1 ;
		else
			flag = 0 ;
	}
	else if (txn == '4229')
	{
		campocta = eval("gForm.txtDDACuenta2.value") ;
		campocs = eval("gForm.txtCodSeg.value") ;
		campoct = eval("gForm.txtCveTran.value") ;
		campos = eval("gForm.txtSerial2.value") ;

		if (campocta == '' || montoC == '' || Number(montoC) <= 0.00 || campoct == '' || campos == '')
			flag = 1 ;
		else
			flag = 0 ;
	}

	if (flag == 1)
	{
		alert("Debe introducir primero los valores del Cargo..") ;
	}
	else
	{
		var CLista = gForm.cuentas ;
		var MLista = gForm.montos ;
		error = 0 ;

		cuenta = gForm.txtDDAA.value ;
		monto = gForm.txtMontoA.value ;
		monto = monto.toString().replace(/\$|\,|\./g,'') ;

		if(Number(monto) <= 0)
			error = 1 ;

		MontoT = gForm.txtMontoT.value.toString().replace(/\$|\,|\./g,'') ;
		MontoT = Number(MontoT) ;
		var mtotmp = montoC - MontoT ;
		if(!(Number(monto) <= mtotmp))
			error = 2 ;

		if(!(Number(gForm.txtTotalA.value) < Number(gForm.depnom.value)))
			error = 3 ;

		if(mtotmp == 0)
			error = 4 ;

		if(cuenta > 0 && error == 0)
		{
			flag =0 ;
		  	for(var i=0; i<CLista.length;i++)
			{
				lista = CLista.options[i].value ;
				if (CLista.options[i].value == cuenta)
				{
					flag = 1 ;
					break ;
				}
			}
			if (flag == 0)
			{
				newCuenta = new Option() ;
				newMonto = new Option() ;
				newCuenta.text = cuenta ;
				newCuenta.value = cuenta ;
				newMonto.text = Currency(monto) ;
				newMonto.value = monto ;

				if(CLista.options[0].value == 0)
				{
					CLista.options[CLista.length-1] = newCuenta ;
					MLista.options[MLista.length-1] = newMonto ;
				}
		  		else
				{
					CLista.options[CLista.length] = newCuenta ;
					MLista.options[MLista.length] = newMonto ;
				}
				CLista.options[CLista.length-1].selected = true ;
				MLista.options[MLista.length-1].selected = true ;

				TotalA = Number(gForm.txtTotalA.value) ;
				TotalA = TotalA +1 ;
				gForm.txtTotalA.value = TotalA ;
				gForm.noabonos.value = TotalA ;

				MontoT = MontoT + Number(monto) ;
				gForm.txtMontoT.value = "" ;
				gForm.txtMontoT.value = Currency(MontoT) ;
				gForm.mtoabonos.value = Currency(MontoT) ;
				gForm.txtDDAA.value = "" ;
				gForm.txtMontoA.value = "" ;
			}
			else
			{
				gForm.txtDDAA.value = "" ;
				gForm.txtMontoA.value = "" ;
				alert("Cuentas de Abono iguales, verifique...") ;
			}
  		}
		else if (error == 1)
		{
			gForm.txtMontoA.value = "0.00" ;
			alert ("No se permite monto 0.00,\n Verifique...") ;
		}
		else if (error == 2)
		{
			gForm.txtMontoA.value = "0.00" ;
			alert ("Monto de Abono superior al Cargo,\n Verifique...") ;
		}
		else if (error == 3)
		{
			gForm.txtDDAA.value = "" ;
			gForm.txtMontoA.value = "" ;
			alert ("Numero de Abonos mayor al maximo permitido,\n Verifique...") ;
		}
		else if (error == 4)
		{
			gForm.txtDDAA.value = "" ;
			gForm.txtMontoA.value = "" ;
			alert ("Nomina completada no requiere mas abonos,\n Verifique...") ;
		}
		else
		{
			gForm.txtDDAA.value = "" ;
			gForm.txtMontoA.value = "" ;
		  	alert ("Valores de Abono invalidos, Verifique...") ;
		}
	}
	gForm.txtDDAA.focus() ;
}

function modifyOption(frame)
{
    gForm  =  eval("window." + frame.name + ".document.forms[0]") ;

	var CLista = gForm.cuentas ;
	var MLista = gForm.montos ;

	var index = CLista.selectedIndex ;

	if( CLista.length >= 0 && CLista.options[0].value != 0 )
  	{
		cuenta = CLista.options[index].value ;
		monto = MLista.options[index].value ;
		monto = monto.toString().replace(/\$|\,|\./g,'') ;

  		CLista.options[index] = null ;
	  	MLista.options[index] = null ;

		gForm.txtDDAA.value = cuenta ;
  		gForm.txtMontoA.value = Currency(monto) ;

		TotalA = Number(gForm.txtTotalA.value) ;
		TotalA = TotalA - 1 ;
		gForm.txtTotalA.value = TotalA ;
		MontoT = gForm.txtMontoT.value.toString().replace(/\$|\,/g,'') ;
		MontoT = gForm.txtMontoT.value.toString().replace(/\$|\,|\./g,'') ;
		MontoT = Number(MontoT) ;
		MontoT = MontoT - Number(monto) ;
		gForm.txtMontoT.value = "" ;
		gForm.txtMontoT.value = Currency(MontoT) ;
		if(TotalA == 0)
		{
			newCuenta = new Option() ;
			newMonto = new Option() ;
			newCuenta.text = "- Cuentas de Abono -" ;
			newCuenta.value = 0 ;
			newMonto.text = "- Montos de Abono -" ;
			newMonto.value = 0 ;
			CLista.options[0] = newCuenta ;
			MLista.options[0] = newMonto ;
		  	CLista.options[0].selected = true ;
		  	MLista.options[0].selected = true ;
		}

	  	CLista.options[index-1].selected = true ;
	  	MLista.options[index-1].selected = true ;
 	}
	else if( CLista.length == 1 )
    	alert("No es posible modificar mas elementos") ;
  	else
   		alert("Seleccione la Cuenta a modificar") ;
	gForm.txtDDAA.focus() ;
}

function removeOption(frame)
{
    gForm  =  eval("window." + frame.name + ".document.forms[0]") ;

	var CLista = gForm.cuentas ;
	var MLista = gForm.montos ;
	var index = CLista.selectedIndex ;

	if( CLista.length >= 0 && CLista.options[0].value != 0)
	{
		monto = MLista.options[index].value ;
		monto = monto.toString().replace(/\$|\,|\./g,'') ;

  		CLista.options[index] = null ;
	  	MLista.options[index] = null ;

		TotalA = Number(gForm.txtTotalA.value) ;
		TotalA = TotalA - 1 ;
		gForm.txtTotalA.value = TotalA ;
		MontoT = gForm.txtMontoT.value.toString().replace(/\$|\,|\./g,'') ;
		MontoT = Number(MontoT) ;
		MontoT = MontoT - Number(monto) ;
		gForm.txtMontoT.value = "" ;
		gForm.txtMontoT.value = Currency(MontoT) ;
		if(TotalA == 0)
		{
			newCuenta = new Option() ;
			newMonto = new Option() ;
			newCuenta.text = "- Cuentas de Abono -" ;
			newCuenta.value = 0 ;
			newMonto.text = "- Montos de Abono -" ;
			newMonto.value = 0 ;
			CLista.options[0] = newCuenta ;
			MLista.options[0] = newMonto ;
		  	CLista.options[0].selected = true ;
		  	MLista.options[0].selected = true ;
		}

	  	CLista.options[index-1].selected = true ;
	  	MLista.options[index-1].selected = true ;
  }
  else if( CLista.options[0].value == 0 )
  {
    alert("No es posible eliminar mas elementos") ;
  }
  else
  {
    alert("Seleccione la Cuenta a eliminar") ;
    gForm.txtDDAA.focus() ;
   }
}

function equalmto(frame)
{
	gForm  =  eval("window." + frame.name + ".document.forms[0]") ;

	var CLista = gForm.cuentas ;
	var MLista = gForm.montos ;
	var index = CLista.selectedIndex ;

  	MLista.options[index].selected = true ;
}

function equalcta(frame)
{
    gForm  =  eval("window." + frame.name + ".document.forms[0]") ;

	var CLista = gForm.cuentas ;
	var MLista = gForm.montos ;
	var index = MLista.selectedIndex ;

  	CLista.options[index].selected = true ;
}

function verifynom(frame)
{
    gForm  =  eval("window." + frame.name + ".document.forms[0]") ;

	var txn = gForm.cTxn.value.toString() ;
	if (txn == '4227' || txn == '4229' || txn == '5153')
	{
		var total = 0 ;
		var cargo = 0 ;

		total = gForm.txtMontoT.value.toString().replace(/\$|\,|\./g,'') ;
		total = Number(total) ;

		cargo = gForm.txtMontoC.value.toString().replace(/\$|\,|\./g,'') ;
		cargo = Number(cargo) ;

		if (total == cargo && total > 0)
			return true;
		else
		{
			alert("Nomina no completada, Verifique...") ;
			return false ;
		}
	}
	if (txn == 'M007')
	{

		var ctacheque = gForm.txtDDACuenta2.value.toString() ;
		ctacheque = ctacheque.substring(1,ctacheque.length) ;
		var ctadisquete = gForm.ctacargo.value.toString() ;

		if (ctacheque == ctadisquete)
			return true;
		else
		{
			alert("Cheque no corresponde a cuenta de disquete, Verifique...") ;
			return false ;
		}
	}
	if (txn == 'M006')
	{
		var cuentas = gForm.Scuentas1.value ;
		var montos = gForm.Smontos1.value ;
		if(montos == '0' && cuentas == '0')
		{
			alert("Primero debe cargar el disquete....") ;
			return false ;
		}
		return true ;
	}
}

function verifyDisk(frame)
{
    gForm  =  eval("window." + frame.name + ".document.forms[0]") ;

	var txn = gForm.cTxn.value ;
	var cuentas ;
	var montos ;

	if(txn != '0025')
	{
		cuentas = gForm.Scuentas.value ;
		montos = gForm.Smontos.value ;
	}
	else
	{
		cuentas = gForm.montos1.value ;
		montos = gForm.nombres1.value ;
	}
		if(montos == '0' && cuentas == '0')
		{
			alert("Primero debe cargar el disquete....") ;
			return false ;
		}
	return true ;
}

function cadenas(frame)
{
    gForm  =  eval("window." + frame.name + ".document.forms[0]") ;

  	var CLista = gForm.cuentas ;
  	var MLista = gForm.montos ;
	var ListaCtas = "" ;
	var ListaMtos = "" ;
	var txn = gForm.cTxn.value ;
	var CtaCargo ;
	var MtoCargo ;

	if (txn == '4227' || txn == '5153')
	{
		CtaCargo = gForm.txtDDAC ;
		MtoCargo = gForm.txtMontoC ;
		ListaCtas = CtaCargo.value.toString() + '*' ;
	}
	else if (txn == '4229')
	{
		CtaCargo = gForm.txtDDACuenta2 ;
		MtoCargo = gForm.txtMontoC ;
		ListaCtas = CtaCargo.value.toString() ;
		ListaCtas = ListaCtas.substring(1,ListaCtas.length) ;
		ListaCtas = ListaCtas + '*' ;
	}

	ListaMtos = MtoCargo.value.toString().replace(/\$|\,/g,'') ;
	ListaMtos = ListaMtos.toString().replace(/\$|\,|\./g,'') + '*' ;

	for(i=0; i<CLista.length;i++)
	{
		ListaCtas = ListaCtas + CLista.options[i].value.toString() + '*' ;
		ListaMtos = ListaMtos + MLista.options[i].value.toString() + '*' ;
	}

	gForm.Scuentas.value = ListaCtas ;
	gForm.Smontos.value = ListaMtos ;

	return true ;
}

function addNewOptionOPM(frame)
{
    gForm  =  eval("window." + frame.name + ".document.forms[0]") ;

	var montoC = gForm.mtocargo.value ;
	montoC = montoC.toString().replace(/\$|\,|\./g,'') ;

	if (montoC == '' || Number(montoC) <= 0.00)
		flag = 1;
	else
		flag = 0;

	if (flag == 1)
		alert("Debe introducir primero el Monto del Cargo..") ;
	else
	{
		var MLista = gForm.montos ;
		var NLista = gForm.nombres ;
		var ALista = gForm.apellidos ;

		error = 0 ;
		monto = gForm.txtMtoOPM.value ;
		monto = monto.toString().replace(/\$|\,|\./g,'') ;
		nom = gForm.txtNomOPM.value ;
		ape = gForm.txtApeOPM.value ;

		if(Number(monto) <= 0)
			error = 1 ;

		MontoT = gForm.txtMontoT.value.toString().replace(/\$|\,|\./g,'') ;
		MontoT = Number(MontoT) ;
		var mtotmp = montoC - MontoT ;

		if(!(Number(monto) <= mtotmp))
			error = 2 ;

		if(mtotmp == 0)
			error = 4 ;

		if(error == 0)
		{
			newNom = new Option() ;
			newApe = new Option() ;
			newMto = new Option() ;

			newNom.text = nom ;
			newNom.value = nom ;
			newApe.text = ape ;
			newApe.value = ape ;
			newMto.text = Currency(monto) ;
			newMto.value = monto ;

			if(NLista.options[0].value == 0)
			{
				NLista.options[NLista.length-1] = newNom ;
				ALista.options[ALista.length-1] = newApe ;
				MLista.options[MLista.length-1] = newMto ;
			}
	  		else
			{
				NLista.options[NLista.length] = newNom ;
				ALista.options[ALista.length] = newApe ;
				MLista.options[MLista.length] = newMto ;
			}
			NLista.options[NLista.length-1].selected = true ;
			ALista.options[ALista.length-1].selected = true ;
			MLista.options[MLista.length-1].selected = true ;

			TotalA = Number(gForm.txtTotalA.value) ;
			TotalA = TotalA +1 ;
			gForm.txtTotalA.value = TotalA ;
			gForm.noabonos.value = TotalA ;
			MontoT = MontoT + Number(monto) ;
			gForm.txtMontoT.value = "" ;
			gForm.txtMontoT.value = Currency(MontoT) ;
			gForm.mtoabonos.value = Currency(MontoT) ;
			gForm.txtNomOPM.value = "" ;
			gForm.txtApeOPM.value = "" ;
			gForm.txtMtoOPM.value = "" ;
		}
		else if (error == 1)
		{
			gForm.txtMtoOPM.value = "" ;
			alert ("No se permite monto 0.00,\n Verifique...") ;
		}
		else if (error == 2)
		{
			gForm.txtMtoOPM.value = '0.00' ;
			alert ("Monto de Abono superior al Cargo,\n Verifique...") ;
		}
		else if (error == 3)
		{
			gForm.txtNomOPM.value = "" ;
			gForm.txtApeOPM.value = "" ;
			gForm.txtMtoOPM.value = "" ;
			alert ("Numero de Abonos mayor al maximo permitido,\n Verifique...") ;
		}
		else if (error == 4)
		{
			gForm.txtNomOPM.value = "" ;
			gForm.txtApeOPM.value = "" ;
			gForm.txtMtoOPM.value = "" ;
			alert ("Proceso completo no requiere mas abonos,\n Verifique...") ;
		}
		else
		{
			gForm.txtNomOPM.value = "" ;
			gForm.txtApeOPM.value = "" ;
			gForm.txtMtoOPM.value = "" ;
		  	alert ("Valores de Abono invalidos, Verifique...") ;
		}
	}
	gForm.txtNomOPM.focus() ;
}

function modifyOptionOPM(frame)
{
    gForm  =  eval("window." + frame.name + ".document.forms[0]") ;

	var NLista = gForm.nombres ;
	var ALista = gForm.apellidos ;
	var MLista = gForm.montos ;
	var index = NLista.selectedIndex ;

	if( NLista.length >= 0 && NLista.options[0].value != 0 )
  	{
		nom = NLista.options[index].value ;
		ape = ALista.options[index].value ;
		monto = MLista.options[index].value ;
		monto = monto.toString().replace(/\$|\,|\./g,'') ;

  		NLista.options[index] = null ;
		ALista.options[index] = null ;
	  	MLista.options[index] = null ;

		gForm.txtNomOPM.value = nom ;
		gForm.txtApeOPM.value = ape ;
  		gForm.txtMtoOPM.value = Currency(monto) ;

		TotalA = Number(gForm.txtTotalA.value) ;
		TotalA = TotalA - 1 ;
		gForm.txtTotalA.value = TotalA ;
		MontoT = gForm.txtMontoT.value.toString().replace(/\$|\,/g,'') ;
		MontoT = gForm.txtMontoT.value.toString().replace(/\$|\,|\./g,'') ;
		MontoT = Number(MontoT) ;
		MontoT = MontoT - Number(monto) ;
		gForm.txtMontoT.value = "" ;
		gForm.txtMontoT.value = Currency(MontoT) ;

		if(TotalA == 0)
		{
			newNom = new Option() ;
			newApe = new Option() ;
			newMto = new Option() ;
			newNom.text = "- Nombres Abonos -" ;
			newNom.value = 0 ;
			newApe.text = "- A p e l l i d o s - A b o n o s -" ;
			newApe.value = 0 ;
			newMto.text = "- Montos Abonos -" ;
			newMto.value = 0 ;
			NLista.options[0] = newNom ;
			ALista.options[0] = newApe ;
			MLista.options[0] = newMto ;

			NLista.options[0].selected = true ;
			ALista.options[0].selected = true ;
		  	MLista.options[0].selected = true ;
		}

		if (index == 0)
		{
	  	  NLista.options[0].selected = true ;
	  	  ALista.options[0].selected = true ;
	  	  MLista.options[0].selected = true ;
	  	}
	  	else
	  	{
	  	  NLista.options[index-1].selected = true ;
	  	  ALista.options[index-1].selected = true ;
	  	  MLista.options[index-1].selected = true ;
	  	}
	}
	else if( NLista.length == 1 )
    	alert("No es posible modificar mas elementos") ;
  	else
   		alert("Seleccione el elemento a modificar") ;
	gForm.txtNomOPM.focus() ;
}

function removeOptionOPM(frame)
{
    	gForm  =  eval("window." + frame.name + ".document.forms[0]") ;

	var NLista = gForm.nombres ;
	var ALista = gForm.apellidos ;
	var MLista = gForm.montos ;

	var index = NLista.selectedIndex ;

	if(NLista.length >= 0 && NLista.options[0].value != 0)
	{
		monto = MLista.options[index].value ;
		monto = monto.toString().replace(/\$|\,|\./g,'') ;

  		NLista.options[index] = null ;
  		ALista.options[index] = null ;
	  	MLista.options[index] = null ;

		TotalA = Number(gForm.txtTotalA.value) ;
		TotalA = TotalA - 1 ;
		gForm.txtTotalA.value = TotalA ;
		MontoT = gForm.txtMontoT.value.toString().replace(/\$|\,|\./g,'') ;
		MontoT = Number(MontoT) ;
		MontoT = MontoT - Number(monto) ;
		gForm.txtMontoT.value = "" ;
		gForm.txtMontoT.value = Currency(MontoT) ;

		if(TotalA == 0)
		{
			newNom = new Option() ;
			newApe = new Option() ;
			newMto = new Option() ;
			newNom.text = "- Nombres Abonos -" ;
			newNom.value = 0 ;
			newApe.text = "- A p e l l i d o s - A b o n o s -" ;
			newApe.value = 0 ;
			newMto.text = "- Montos Abonos -" ;
			newMto.value = 0 ;
			NLista.options[0] = newNom ;
			ALista.options[0] = newApe ;
			MLista.options[0] = newMto ;

			NLista.options[0].selected = true ;
			ALista.options[0].selected = true ;
		  	MLista.options[0].selected = true ;
		}

		if (index == 0)
		{
	  	  NLista.options[0].selected = true ;
	  	  ALista.options[0].selected = true ;
	   	  MLista.options[0].selected = true ;
	   	}
	   	else
	   	{
		  NLista.options[index-1].selected = true ;
	  	  ALista.options[index-1].selected = true ;
	   	  MLista.options[index-1].selected = true ;
	   	}
  }
  else if( NLista.options[0].value == 0 )
  {
    alert("No es posible eliminar mas elementos") ;
  }
  else
  {
    alert("Seleccione el elemento a eliminar") ;
    gForm.txtNomOPM.focus() ;
   }
}

function equalnomOPM(frame)
{
    gForm  =  eval("window." + frame.name + ".document.forms[0]") ;

	var NLista = gForm.nombres ;
	var ALista = gForm.apellidos ;
	var MLista = gForm.montos ;
	var index = NLista.selectedIndex ;

  	ALista.options[index].selected = true ;
  	MLista.options[index].selected = true ;
}

function equalapeOPM(frame)
{
    gForm  =  eval("window." + frame.name + ".document.forms[0]") ;

	var NLista = gForm.nombres ;
	var ALista = gForm.apellidos ;
	var MLista = gForm.montos ;
	var index = ALista.selectedIndex ;

  	NLista.options[index].selected = true ;
  	MLista.options[index].selected = true ;
}

function equalmtoOPM(frame)
{
    gForm  =  eval("window." + frame.name + ".document.forms[0]") ;

	var NLista = gForm.nombres ;
	var ALista = gForm.apellidos ;
	var MLista = gForm.montos ;
	var index = MLista.selectedIndex ;

  	NLista.options[index].selected = true ;
  	ALista.options[index].selected = true ;
}

function verifyOPM(frame)
{
    gForm  =  eval("window." + frame.name + ".document.forms[0]") ;

	var total = 0 ;
	var cargo = 0 ;

	total = gForm.txtMontoT.value.toString().replace(/\$|\,|\./g,'') ;
	total = Number(total) ;

	cargo = gForm.txtMtoCOPM.value.toString().replace(/\$|\,|\./g,'') ;
	cargo = Number(cargo) ;
	if ((total == cargo) && (!total == 0))
		return true ;
	else
	{
		alert("Proceso no completado, Verifique...") ;
		return false ;
	}
}

function cadenasOPM(frame)
{
    gForm  =  eval("window." + frame.name + ".document.forms[0]") ;

  	var NLista = gForm.nombres ;
  	var ALista = gForm.apellidos ;
  	var MLista = gForm.montos ;
	var ListaNoms = "" ;
	var ListaApes = "" ;
	var ListaMtos = "" ;

	for(i=0; i<NLista.length;i++)
	{
		ListaNoms = ListaNoms + NLista.options[i].value.toString() + '*' ;
		ListaApes = ListaApes + ALista.options[i].value.toString() + '*' ;
		ListaMtos = ListaMtos + MLista.options[i].value.toString() + '*' ;
	}

	gForm.Snombres.value = ListaNoms ;
	gForm.Sapellidos.value = ListaApes ;
	gForm.Smontos.value = ListaMtos ;
	return true ;
}

function deadend()
{
	if (dialogWin.win && !dialogWin.win.closed)
	{
		dialogWin.win.focus() ;
		return false ;
	}
}

function disableForms()
{
	IELinkClicks = new Array() ;
	for (var h = 0; h < frames.length; h++)
	{
		for (var i = 0; i < frames[h].document.forms.length; i++)
		{
			for (var j = 0; j < frames[h].document.forms[i].elements.length; j++)
			{
				frames[h].document.forms[i].elements[j].disabled = true ;
			}
		}
		IELinkClicks[h] = new Array() ;
		for (i = 0; i < frames[h].document.links.length; i++)
		{
			IELinkClicks[h][i] = frames[h].document.links[i].onclick ;
			frames[h].document.links[i].onclick = top.deadend ;
		}
	}
}

function enableForms()
{
	for (var h = 0; h < frames.length; h++)
	{
		for (var i = 0; i < frames[h].document.forms.length; i++)
		{
			for (var j = 0; j < frames[h].document.forms[i].elements.length; j++)
			{
				frames[h].document.forms[i].elements[j].disabled = false ;
			}
		}
		for (i = 0; i < frames[h].document.links.length; i++)
			frames[h].document.links[i].onclick = IELinkClicks[h][i] ;
	}
}

function blockEvents()
{
	top.disableForms() ;
	window.onfocus = top.checkModal ;
}

function unblockEvents()
{
	top.enableForms() ;
}

function checkModal()
{
	if (dialogWin.win && !dialogWin.win.closed)
		dialogWin.win.focus() ;
}

function containsElement(arr, ele)
{
	var found = false, index = 0 ;
	while(!found && index < arr.length)
		if(arr[index] == ele)
			found = true ;
		else
			index++ ;
	return found ;
}

function setFieldFocus(documento)
{
	if (documento.forms.length > 0)
	{
		var field = documento.forms[0] ;

		for (i = 0; i < field.length; i++)
		{
			if(field.elements[i] != null)
			    if(field.elements[i].type != null)
				if ((field.elements[i].type == "text") || (field.elements[i].type == "textarea") || (field.elements[i].type.toString().charAt(0) == "s"))
				{
					documento.forms[0].elements[i].focus() ;
					break ;
				}
		}
	}
}

function isValidRegister(form)
{
	var sum = 0 ;
	var txtRegistro = form.idNum.value ;

	for(var i=txtRegistro.length-2; i >= 0; i--)
	{
		if( i % 2 == 0 )
			sum += parseInt( txtRegistro.charAt(i) ) ;
		else
		{
			temp = parseInt( txtRegistro.charAt(i) * 2 ) ;
			if( temp < 10 )
				sum += temp ;
			else
				sum = sum + Math.floor(temp / 10) + (temp % 10) ;
		}
	}
	if( (sum % 10 == 0 ) && (parseInt(txtRegistro.charAt(txtRegistro.length-1)) == 0) )
		return true ;
	else if( (10-(sum % 10)) == parseInt(txtRegistro.charAt(txtRegistro.length-1)) )
		return true ;
	else
	{
		form.idNum.focus() ;
		form.idNum.select() ;
		alert("El registro está incorrecto, vuelva a intentarlo.") ;
		return false ;
	}
}

function isSeries(form)
{
	var txtRegistro = form.idNum.value ;
	var car = txtRegistro.substring(0,1), i, cont = 0 ;

	for(i = 1; i < txtRegistro.length; i++)
	{
		if(txtRegistro.substring(i, i + 1) == car)
			cont++ ;
	}
	if(cont == txtRegistro.length - 1)
	{
		form.idNum.focus() ;
		form.idNum.select() ;
		txtRegistro = '' ;
		alert("El <REGISTRO> está incorrecto, vuelva a intentarlo.") ;
		return false ;
	}
	return true ;
}

function fillRecord(field)
{
	var maxLength = getFieldSize(field) ;
	while( gField.value.length < maxLength )
		gField.value = '0' + gField.value;
}


// Regresa la fecha del navegador en formato dd/mm/yyyy
 function sysdate()
{
	date = new Date() ;
	day = date.getDate() ;
	month = date.getMonth()+1 ;
	year = date.getFullYear() ;
	if (day < 10)
		day = "0" + day ;
	if (month < 10)
		month = "0" + month ;
	today = day + "/" + month + "/" + year ;

	return today;
}

//Regresa la fecha del navegador en formato ddmmyyyy
function sysdatef()
{
	date = new Date() ;
	day = date.getDate() ;
	month = date.getMonth()+1 ;
	year = date.getFullYear() ;
	if (day < 10)
		day = "0" + day ;
	if (month < 10)
		month = "0" + month ;
	today = year + "" + month + "" + day ;

	return today ;
}

function FieldautoTab(input, e)
{
	var len = getFieldSize(input) ;
	var keyCode = e.keyCode ;
	var filter = [0,8,9,16,17,18,37,38,39,40,46] ;
	if(input.value.length >= len && !containsElement(filter,keyCode))
	{
		input.value = input.value.slice(0, len) ;

		if(input.form[(getIndex(input)+1) % input.form.length].type == "text")
			input.form[(getIndex(input)+1) % input.form.length].select() ;

		if (input.form[(getIndex(input)+1) % input.form.length].type != "hidden")
			input.form[(getIndex(input)+1) % input.form.length].focus() ;
	}

}

// Regresa la propiedad maxLength de un campo
function getFieldSize(field)
{
	gField = eval("window.document.forms[0]." + field.name) ;
	return field.maxLength ;
}

// =============================================================================================
// Funciones auxiliares para abrir nuevos diálogos del navegador
// =============================================================================================

dialogWin = new Object() ;
function openDialog(url, width, height, returnFunc, forma, args)
{
	if (!dialogWin.win || (dialogWin.win && dialogWin.win.closed))
	{
		top.formName = forma ;
		if (returnFunc == 'top.setPrefs()' || returnFunc == 'top.setPrefs7()' ||
			returnFunc == 'top.setPrefs8()' || returnFunc == 'top.setPrefs14()' ||
			returnFunc == 'top.setPrefs10()' || returnFunc == 'top.setPrefs12()' ||
			returnFunc == 'top.setPrefs15()')
		{
			width = 400 ;
			height = 200 ;
		}
		if(returnFunc == 'top.setPrefs()')
			dialogWin.returnFunc = new dispatcher(setPrefs) ;
		else if(returnFunc == 'top.setPrefs2()')
			dialogWin.returnFunc = new dispatcher(setPrefs2) ;
		else if(returnFunc == 'top.setPrefs3()')
			dialogWin.returnFunc = new dispatcher(setPrefs3) ;
		else if(returnFunc == 'top.setPrefs4()')
			dialogWin.returnFunc = new dispatcher(setPrefs4) ;
		else if(returnFunc == 'top.setPrefs5()')
			dialogWin.returnFunc = new dispatcher(setPrefs5) ;
		else if(returnFunc == 'top.setPrefs6()')
			dialogWin.returnFunc = new dispatcher(setPrefs6) ;
		else if(returnFunc == 'top.setPrefs7()')
			dialogWin.returnFunc = new dispatcher(setPrefs7) ;
		else if(returnFunc == 'top.setPrefs8()')
			dialogWin.returnFunc = new dispatcher(setPrefs8) ;
		else if(returnFunc == 'top.setPrefs9()')
			dialogWin.returnFunc = new dispatcher(setPrefs9) ;
		else if(returnFunc == 'top.setPrefs10()')
			dialogWin.returnFunc = new dispatcher(setPrefs10) ;
		else if(returnFunc == 'top.setPrefs11()')
			dialogWin.returnFunc = new dispatcher(setPrefs11) ;
		else if(returnFunc == 'top.setPrefs12()')
			dialogWin.returnFunc = new dispatcher(setPrefs12) ;
		else if(returnFunc == 'top.setPrefs13()')
			dialogWin.returnFunc = new dispatcher(setPrefs13) ;
		else if(returnFunc == 'top.setPrefs14()')
			dialogWin.returnFunc = new dispatcher(setPrefs14) ;
		else if(returnFunc == 'top.setPrefs15()')
			dialogWin.returnFunc = new dispatcher(setPrefs15) ;

		dialogWin.returnedValue = "" ;
		dialogWin.args = args ;
		dialogWin.url = url ;
		dialogWin.width = width ;
		dialogWin.height = height ;
		dialogWin.name = (new Date()).getSeconds().toString() ;
     	dialogWin.left = (screen.width - dialogWin.width) / 2
		dialogWin.top = (screen.height - dialogWin.height) / 2
		var attr = "left=" + dialogWin.left + ",top=" + dialogWin.top + ",resizable=no,width=" + dialogWin.width +",height=" + dialogWin.height
		dialogWin.win = window.open(dialogWin.url, dialogWin.name, attr) ;
		dialogWin.win.focus() ;
	}
	else
	{
		dialogWin.win.focus() ;
	}

  	if( returnFunc == 'top.setPrefs6()')
	  return 1;
}

// restrada: Prueba temporal de dialogo modal para ActiveX de Impresión
function openModalDialog(url, width, height, returnFunc, forma, args)
{
	var returnedValue ;

	if (!dialogWin.win || (dialogWin.win && dialogWin.win.closed))
	{
		top.formName = forma ;
		if (returnFunc == 'top.setPrefs()' || returnFunc == 'top.setPrefs7()' ||
			returnFunc == 'top.setPrefs8()' || returnFunc == 'top.setPrefs14()' ||
			returnFunc == 'top.setPrefs10()' || returnFunc == 'top.setPrefs12()' ||
			returnFunc == 'top.setPrefs15()')
		{
			width = 400 ;
			height = 200 ;
		}
		if(returnFunc == 'top.setPrefs()')
			dialogWin.returnFunc = new dispatcher(setPrefs) ;
		else if(returnFunc == 'top.setPrefs2()')
			dialogWin.returnFunc = new dispatcher(setPrefs2) ;
		else if(returnFunc == 'top.setPrefs3()')
			dialogWin.returnFunc = new dispatcher(setPrefs3) ;
		else if(returnFunc == 'top.setPrefs4()')
			dialogWin.returnFunc = new dispatcher(setPrefs4) ;
		else if(returnFunc == 'top.setPrefs5()')
			dialogWin.returnFunc = new dispatcher(setPrefs5) ;
		else if(returnFunc == 'top.setPrefs6()')
			dialogWin.returnFunc = new dispatcher(setPrefs6) ;
		else if(returnFunc == 'top.setPrefs7()')
			dialogWin.returnFunc = new dispatcher(setPrefs7) ;
		else if(returnFunc == 'top.setPrefs8()')
			dialogWin.returnFunc = new dispatcher(setPrefs8) ;
		else if(returnFunc == 'top.setPrefs9()')
			dialogWin.returnFunc = new dispatcher(setPrefs9) ;
		else if(returnFunc == 'top.setPrefs10()')
			dialogWin.returnFunc = new dispatcher(setPrefs10) ;
		else if(returnFunc == 'top.setPrefs11()')
			dialogWin.returnFunc = new dispatcher(setPrefs11) ;
		else if(returnFunc == 'top.setPrefs12()')
			dialogWin.returnFunc = new dispatcher(setPrefs12) ;
		else if(returnFunc == 'top.setPrefs13()')
			dialogWin.returnFunc = new dispatcher(setPrefs13) ;
		else if(returnFunc == 'top.setPrefs14()')
			dialogWin.returnFunc = new dispatcher(setPrefs14) ;
			else if(returnFunc == 'top.setPrefs15()')
			dialogWin.returnFunc = new dispatcher(setPrefs15) ;

		dialogWin.returnedValue = "" ;
		dialogWin.getFormData = "";
		dialogWin.args = args ;
		dialogWin.url = url ;
		dialogWin.width = width ;
		dialogWin.height = height ;
		dialogWin.name = (new Date()).getSeconds().toString() ;

		var attr = "dialogHeight:" + dialogWin.height + "px; dialogWidth: " + dialogWin.width + "px; center: Yes; resizable: No; status: No; edge: Raised; help: No; scroll: No;" ;
		window.focus() ;
		returnedValue = window.showModalDialog(dialogWin.url, "", attr) ;

		dialogWin.returnedValue = returnedValue;
		dialogWin.returnFunc.doValidate() ;
	}
  if( returnFunc == 'top.setPrefs6()') ;
	  return 1;
}


function PreparaParametros(querystring)
{
	var arr = querystring.split(';');
	var arrAux;
	var ParamArr = new Array(arr.length);

	for(var i=0;i<arr.length;i++)
	{
		arrAux = arr[i].split('=');
		ParamArr[i] = new Array(2);
		ParamArr[i][0] = arrAux[0];
		ParamArr[i][1] = arrAux[1];
	}
	return ParamArr;

}

function BusquedaValorParametro(ParamArr,NomParam)
{
	for(var i=0;i<ParamArr.length;i++)
	{
		if(ParamArr[i][0] == NomParam)
			return ParamArr[i][1];
	}
	return "";
}


// Establece las preferencias para abrir un nuevo diálogo
function setPrefs()
{

	var Cuenta ;
	if(dialogWin.returnedValue != null)
	{
		var myArray = dialogWin.returnedValue.split("&") ;
		if(myArray[1] == '1')
		{
			formName.supervisor.value = myArray[3];
			dialogWin = new Object() ;
			dialogWin.returnedValue = null ;
			formName.AuthoOK.value = '1' ;
			Cuenta = '' ;
			if(formName.SignField.value != '' && formName.SignField.value != '0')
				Cuenta = eval('formName.' + formName.SignField.value + '.value') ;
			if(formName.needSignature.value == '1' && Cuenta != '')
			{
				//Cuenta = '../../servlet/ventanilla.FirmasServlet?txtDDACuenta=' + Cuenta ;
				Cuenta = gContextPath+'/servlet/ventanilla.FirmasServlet?txtDDACuenta=' + Cuenta;
				top.openModalDialog(Cuenta, 710, 430, 'top.setPrefs2()', formName) ;
			}
			else
			{
				formName.submit() ;
			}
		}
	}
}

function setPrefs15()
{

	var Cuenta ;
	if(dialogWin.returnedValue != null)
	{
		var myArray = dialogWin.returnedValue.split("&") ;
		if(myArray[1] == '1')
		{
			formName.supervisor.value = myArray[3];
			dialogWin = new Object() ;
			dialogWin.returnedValue = null ;
			formName.AuthoOK.value = '1' ;
			Cuenta = '' ;
			CveTran = '';
			if(formName.SignField.value != '' && formName.SignField.value != '0')
				Cuenta = eval('formName.' + formName.SignField.value + '.value') ;
			if(formName.txtCveTran.value != '' && formName.txtCveTran.value != '0')
				CveTran = eval('formName.txtCveTran.value') ;
			if(formName.needSignature.value == '1' && Cuenta != '')
			{
				//Cuenta = '../../servlet/ventanilla.FirmasServlet?txtDDACuenta=' + Cuenta ;
				Cuenta = gContextPath+'/servlet/ventanilla.FirmasServlet?txtDDACuenta=' + Cuenta +'&txtCveTran='+CveTran;
				top.openModalDialog(Cuenta, 710, 430, 'top.setPrefs2()', formName) ;
			}
			else
			{
				formName.submit() ;
			}
		}
	}
}

function setPrefs2()
{
	var myArray;
	var tempValue,tempButton;

	if(dialogWin.returnedValue != null)
	{
		myArray = PreparaParametros(dialogWin.returnedValue);
		if(formName.supervisor.value != null)
			tempValue = formName.supervisor.value;
		else
			tempValue = BusquedaValorParametro(myArray,"supervisor");

		tempButton = BusquedaValorParametro(myArray,"button");
		if(tempButton == '2')
		{

			disableLinks(formName.document);
			formName.SignOK.value = '1';
			formName.supervisor.value = tempValue;
			formName.submit();
		}
	}
}

function setPrefs3()
{
	if(dialogWin.returnedValue != null)
	{
		var myArray = dialogWin.returnedValue.split("&") ;
		if(myArray[1] == '1')
		{
			formName.submit() ;
		}
	}
}


//Valida el valor del proceso de ARP

function isValidARP(){


	var valor = gField.value;
	var size = gField.value.length;

		if (!isnumeric()){
		return false;	}

		if(isDecInteger()){
		gField.value="";
		return false;}

		if(size<10)
		return false;

	gForm.action="../ventanilla/paginas/ResponseDARP.jsp";
	return true;
}

function setPrefs4()
{
	if(formName != null)
		formName.submit() ;
	else
		dialogWin.win.close() ;
}

function setPrefs5()
{
	if(dialogWin.returnedValue != null)
	{
		var myArray = dialogWin.returnedValue.split("&") ;
		if(myArray[1] == '1')
		{
			formName.supervisor.value = myArray[3] ;
			dialogWin = new Object() ;
			dialogWin.returnedValue = null ;
			formName.AuthoOK.value = '1' ;
			formName.submit() ;
		}
	}
}

function setPrefs6()
{
	if(dialogWin.returnedValue != null)
	{
		var myArray = dialogWin.returnedValue.split("&") ;
		if(myArray[1] == '1')
			top.Gte = "1";
		else
			top.Gte = "0";
		return;
	}
}

function setPrefs7()
{
	if(dialogWin.returnedValue != null)
	{
		var myArray = dialogWin.returnedValue.split("&") ;
		if(myArray[1] == '1')
		{
			formName.supervisor.value = myArray[3] ;
			formName.submit() ;
		}
	}
}

function setPrefs8()
{
	if(dialogWin.returnedValue != null)
	{
		var myArray = dialogWin.returnedValue.split("&") ;
		if(myArray[1] == '1')
		{
			formName.supervisor.value = myArray[3] ;
			formName.action = gContextPath+'/servlet/ventanilla.DataServlet' ;
			formName.submit() ;
		}
	}
}

function setPrefs9()
{
	formName.target = "panel" ;
	formName.action = "../ventanilla/paginas/Final.jsp" ;
  	formName.submit() ;
}

function setPrefs10()
{
	if(dialogWin.returnedValue != null)
	{
		var myArray = dialogWin.returnedValue.split("&") ;
		if(myArray[1] == '1')
		{
			formName.SignOK.value = '1'
			formName.supervisor.value = myArray[3]
			formName.submit()
		}
	}
}

function setPrefs11()
{
	if(dialogWin.returnedValue != null)
	{
		var myArray = dialogWin.returnedValue.split("&") ;
		if(myArray.length > 1)
		{
			formName.consecutivo.value = myArray[1];
			dialogWin = new Object() ;
			dialogWin.returnedValue = null ;
			top.openDialog('../../ventanilla/paginas/authoriz.jsp', 220, 130, 'top.setPrefs7()', formName) ;
		}
	}
}

function setPrefs12()
{
	if(dialogWin.returnedValue != null)
	{
		var myArray = dialogWin.returnedValue.split("&")
		if(formName.supervisor.value != null)
		{
			myArray[2] = '' ;
			myArray[3] = formName.supervisor.value ;
		}
		if(myArray[1] == '1')
		{
			formName.supervisor.value = myArray[3] ;
			formName.AuthoOK.value = '1';
		}
	}
}

function setPrefs13()
{
	if(formName.norev.value < formName.totalrevs.value)
		formName.submit();
}

function setPrefs14()
{
	if(dialogWin.returnedValue != null)
	{
		var myArray = dialogWin.returnedValue.split("&") ;
		if(myArray[1] == '1')
		{
			window.document.forms[0].supervisor.value = myArray[3] ;
			window.document.forms[0].submit() ;
		}
	}
}

var hrefs=new Array();

function disableLinks(documento){
  for(i=0;i<documento.links.length;i++){
    hrefs[i]=documento.links[i].href;
    documento.links[i].href="#";
    documento.links[i].className="disabled";
  }
}

function isValFolioSeg()
{
  messageLookup["isValFolioSeg"] = messageIni["isValFolioSeg"];

  if (!isnumeric())
  {
	messageLookup["isValFolioSeg"] = messageIni["isnumeric"];
	gField.value = "" ;
	return false;
  }
  else
  {
  	if(gForm.txtFolioSeg.value != gField.value)
  	{
  	   gField.focus();
  	   gField.select();
  	   gField.value = "" ;
  	   return false;
  	}
  	else
           return true;
  }
}
function isnumdpa()
{
  if(gField.value.length > 0 && gForm.txtRfc.value.length > 0)
  {
    messageLookup["isnumdpa"] = new String("Solo debe Capturar un solo campo, Clave DPA o RFC");
    gField.value = '';
    return false;
  }
  if (gForm.numcredito.value.length > 0 && gField.value.length > 0)
  {
    messageLookup["isnumdpa"] = new String("Solo debe Capturar un solo campo, Clave DPA o Numero de Credito");
    gForm.numcredito.value = '';
    gField.value = '';
    return false;
  }
  return true
}

function UserRACF()
{
	messageLookup["UserRACF"] =	messageIni["UserRACF"];

	var valor = gField.value ;

	valor = valor.toUpperCase() ;
	if(valor.length != 8)
	{
		gField.value = "" ;
		return false ;
	}

	var letra = valor.substring(0,1) ;
	var ln = valor.substring(1,2) ;
	var nums = valor.substring(2,valor.length) ;
	var buf = letra.match("[A-Z]*") ;

	if( buf != letra )
	{
		gField.value = "" ;
		return false ;
	}

	buf = ln.match("[A-Z0-9]*") ;
	if(buf != ln)
	{
		gField.value = "" ;
		return false ;
	}

	buf = nums.match("[0-9]*") ;
	if(buf != nums)
	{
		gField.value = "" ;
		return false ;
	}
  	gField.value = valor ;
  	return true ;
}
function isValidaSua()
{
 messageLookup["isValFolioSeg"] = messageIni["isValFolioSeg"];
 var formapago = gField.options[gField.selectedIndex].value;
 if (formapago == "S")
   return false
 else
   return true
}

//SE MODIFICA PARA CORRECCION MONTO EN CERO TXN 4455
function isMenorIgual4455()
{
		var numEfe = parseFloat(gForm.txtEfectivo.value.toString().replace(/\$|\,/g,''))
		var numTot = parseFloat(gForm.txttotal.value.toString().replace(/\$|\,/g,''))
		// CAST PARA COMPARAR VALORES, NO CADENAS
		if (numEfe > numTot)
		{
			messageLookup["isMenorIgual4455"] = messageIni["isMenorIgual4455"];
			gForm.txtEfectivo.value = ""
			gForm.txtEfectivo.focus()
			return false
		}
		return true
}

//SE AGREGO FUNCION PARA VALIDAR No. DE REGSITRO 13/02/2006
//EN TXN 0332,0368,0214,0212,0208,0823
function isSerie()
{
  var txtRegistro = gField.value ;
  var car = txtRegistro.substring(0,1), i, cont = 0

  for(i = 1; i < txtRegistro.length; i++){
   if(txtRegistro.substring(i, i + 1) == car)
    cont++
  }
  if(cont == txtRegistro.length - 1){
   gField.value = ""
   gField.focus()
   txtRegistro = ''
   return false;
  }else
    return true;
}

//---funciones para validación de datos de comprobante de comisión RAP
function findObject(campo){
  var i = 0
  var encontrado = "false"
  while(i<gForm.length && encontrado=="false"){
  	if(gForm.elements[i].name==campo)
    	encontrado="true"
    i++
  }
  i--
  return i
}

function isMsgComprobante()//agregado para mensaje en cobro de comisión a usuarios RAP
{
  var txtNom = "nombres"
  var txtDom = "doms"
  var txtRFC = "rfcs"
  var txtCompcom = "compcoms"
  var txtCampo = gField.name
  var i = 0
  var encontrado = "false"

  while(i<txtCampo.length && encontrado=="false"){
     if(isNaN(txtCampo.charAt(i)))
       encontrado="false"
     else
       encontrado="true"
     i++;
  }

  txtNom = txtNom + txtCampo.substring(i-1)
  txtDom = txtDom + txtCampo.substring(i-1)
  txtRFC = txtRFC + txtCampo.substring(i-1)
  txtCompcom = txtCompcom + txtCampo.substring(i-1)

  i=findObject(txtCompcom)

  if(gForm.elements[i].checked==false)
    if(gForm.elements[findObject(txtNom)].value!="" || gForm.elements[findObject(txtDom)].value !="" || gForm.elements[findObject(txtRFC)].value!=""){
      messageLookup["isMsgComprobante"] = messageIni["isMsgComprobante"];
      gForm.elements[findObject(txtNom)].value=""
      gForm.elements[findObject(txtDom)].value=""
      gForm.elements[findObject(txtRFC)].value=""
      return false
    }
    else
      return true
  else
    return true
}
//--------------- FIN
function isTarjetaTDI()
{
	var txtTDI = gField.value
	var temp = ""
	var valor = true

	if(txtTDI.length>0 && txtTDI.length<16)
	{
		messageLookup["isTarjetaTDI"] = new String("La tarjeta TDI debe ser de 16 posiciones");
		valor = false
	}
	if(txtTDI.length == 16 )
	{
		if(isNaN(txtTDI))
		{
			valor = false
		}
	}
	if(txtTDI.length > 16 )
	{
		if(txtTDI.length < 18)
		{
			messageLookup["isTarjetaTDI"] = new String("La tarjeta TDI debe ser de 16 posiciones");
			valor = false
		}
		else
		{
			txtTDI = txtTDI.substring(2,18);
			gField.value = txtTDI;
	    	if(isNaN(txtTDI))
	    	{
				valor = false
		    }
		}
	}
	if(valor){
		return true
	}
	else
	{
		gField.value=""
		gField.focus()
		return false
	}
}

function isValidKroner()
{
	var idCredito = gForm.lstCredito;
	gForm.txtCredito.value=gForm.txtCredito.value.toUpperCase();
	var valor = gField.value;
	if(gField.name == "txtCredito")
			{
				gField.value = filler(gField.value, 10);
			}

	var Credito = gForm.txtCredito.value;
	var reg1 = new RegExp("^PA[a-zA-Z0-9]{16}$");
	var reg2 = new RegExp("[a-zA-Z0-9]");
 	if(idCredito.options[idCredito.selectedIndex].value == 'BCC' && isnumeric() && Credito.length==15)
 	{
 		return true
 	}
 	else if(idCredito.options[idCredito.selectedIndex].value == 'HCC' && Credito.length==18 && reg1.test(Credito))
 	{
 		return true;
 	}
 	else if(idCredito.options[idCredito.selectedIndex].value == 'CRE' && Credito.length==10 && reg2.test(Credito))
 	{
 		return true
 	}
 	else
 	{
 		alert("Error en la captura del número de Crédito, verificar....");
 		gForm.txtCredito.value=''
 		return false
 	}
}

function isSIB(){
	var idCuenta = gForm.lstCuentaSIB;
	var Cuenta = gForm.txtCtaSIB.value;
	if(idCuenta.options[idCuenta.selectedIndex].value == '02')
 	{
			if(Cuenta.length > 0)
			{
				if( isInteger(Cuenta) )
				{
					if( Cuenta.length - 1 == 10 || Cuenta.length == 10)
					{
						if( isValidDDA( Cuenta ) )
						{
							return true ;
						}
					}
				}
			}else
				messageLookup["isSIB"] = 'Captura número de cuenta de ahorro/corriente';

			gForm.txtCtaSIB.value = '';
			return false ;
	}
 	else if(idCuenta.options[idCuenta.selectedIndex].value == '03')
 	{
			if(Cuenta.length > 0)
			{
				if( isInteger( Cuenta ) )
				{
					if( isValidCDA( Cuenta ) )
						return true ;
				}
			}
			else
				messageLookup["isSIB"] = 'Captura número de cuenta DPF';

			gForm.txtCtaSIB.value ='';
			return false ;
 	}
 	else if(idCuenta.options[idCuenta.selectedIndex].value == '04')
 	{
		if(Cuenta.length < 1)
		{
			messageLookup["isSIB"] = 'Captura número de TDC';
			return false;
		}

 		if(Cuenta.length < 13 || validnumtar(Cuenta)!= 0)
 		{
 			messageLookup["isSIB"] = 'Error, número de TDC incorrecto, verificar....';
 			gForm.txtCtaSIB.value='';
 			return false;
 		}
 	}
 	else if(idCuenta.options[idCuenta.selectedIndex].value == '01' || idCuenta.options[idCuenta.selectedIndex].value == '05')
 	{
 		if(Cuenta.length > 0)
 		{
 			messageLookup["isSIB"] = 'Error, el campo de cuenta no puede tener información para esta opción, verificar....';
 			gForm.txtCtaSIB.value='';
 			return false;
 		}
 		else
 			return true;
 	}

 	return true;
}

function isValidDocsATC()
{
	var docs = gForm.txtDocs.value;
	if (!isnumeric())
	{
		messageLookup["isValidDocsATC"] = 'Error en la captura, verificar...';
		gForm.txtDocs.value='';
		return false ;
	}
	if(docs>50)
	{
		messageLookup["isValidDocsATC"] = 'Sólo se aceptan como máximo 50 documentos, verificar...';
		gForm.txtDocs.value='';
		return false
	}

	if(docs==0)
	{
		messageLookup["isValidDocsATC"] = 'Error debes capturar al menos 1 documento, verificar...';
		gForm.txtDocs.value='';
		return false
	}

	return true;
}

function isAlphaNumeric() {
	var value = gField.value;
	var cad = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
	var i=0;
	for(i=0;i<value.length; i++)
	{
		if(cad.indexOf(value.substring(i,i+1))==-1)
		{
			gField.value="";
			gField.blur();
			return false;
		}
	}
	return true;
}

/** *********************************** */
function calcDVmod10(num) {

	var len = num.length;
	var ultimo = num.substring(len - 1, len);
	var base = num.substr(0, num.length - 1);
	var start = base.length - 1;
	var vs = [];
	var vsBase = base.split('');
	var vsForm = [ 1, 3, 7 ];
	//var vsResul;
	var j = 2;
	var aux = 0;
	var cont = 0;

	for (var i = start; i >= 0; i--) {

		vs[i] = parseInt(vsBase[i]) * parseInt(vsForm[j]);
		cont = cont + 1;
		j = j - 1;

		if (cont > 2) {
			cont = 0;
			j = 2;
		}

		aux = aux + vs[i];
	}

	//vsResul = aux.toString().split('');
	//var dvCalc = 10 - parseInt(vsResul[vsResul.length - 1]);
	var dvCalc = aux % 10;
	
	if (dvCalc > 0) {
		dvCalc = 10 - dvCalc;
	}

	if (dvCalc == ultimo)
		return (dvCalc);
	else
		return (-1);
    
}

function calcDVmod7(base, ultimo, modulo) {

	var residuo, dvCalculado, dv;

	residuo = base % modulo;

	dvCalculado = modulo - residuo;
	// setTraza("BASE= " + base + "\nultimo = " + ultimo + "\nRESIDUO =" +
	// residuo + "\ndvCalculado = " +dvCalculado);

	if (ultimo != -1) {
		if (ultimo == dvCalculado)
			return (dvCalculado);
		else
			return -1;
	} else
		return (dvCalculado);

}

/** *********************************** */

function getInstanceDate(date) {
	var parts = date.split("/");
	var date = new Date(parts[1] + "/" + parts[0] + "/" + parts[2]);
	return date.getTime();
}

function isDateValidToProcessCh() {
	return (getInstanceDate(gfeMaxVigCh) >= getInstanceDate(sysdate()));
}

function isFormatChequeNuevo(funcionPrincipal) {

	var txnGrup2 = gtxnGrup2;

	var txnVar = gForm.cTxn.value;
	var dato = txnGrup2.split('*');

	/*
	 * for( pos = 0; pos < dato.length ; pos++){ if( dato[pos] == txnVar) {
	 * return true; } }
	 */

	if (funcionPrincipal == 'isValidCheque'
			|| funcionPrincipal == 'isValidChequeDev') {
		var cveTran = gForm.txtCveTran.value;
		var txtDDACuenta = gForm.txtDDACuenta2.value;

		if (isRutaChequeNuevoBanistmo(cveTran)
				&& txtDDACuenta.length > 10) {
			dvDDACuenta = getDvField(gForm.txtDDACuenta2);
			if ((isValidDvCampo(dvDDACuenta))
					|| getDigitos(txtDDACuenta, 0, 2) != '00') {
				return true;
			}
		}
	}

	return false;
}

function isVacio(value) {
	return (value.toString() == '');
}

function isRutaChequeNuevoBanistmo(campo) {
	return quitaceros(campo).substring(0,4) == '1481';
}

function islengthField(value, length) {
	return (value.length == length);
}

function setMessageError(funcionPrincipal, mensaje) {
	if (!isEmptyField(funcionPrincipal))
		messageLookup[funcionPrincipal] = new String(mensaje);
	else
		alert(mensaje);
}

function setTraza(mensaje) {
	var habilitarTraza = false;
	if (habilitarTraza) {
		alert(mensaje);
	}

}

function setEmpty(object) {
	object.value = '';
	object.style.borderColor = '#F5F3F3';

	setEmptyCamposCheques();

	object.focus();
}

function setEmptyCamposCheques() {

	gForm.dvCuenta.value = '';
	gForm.dvCheque.value = '';
	gForm.dvRuta.value = '';
	gForm.noCheque.value = '';
	gForm.noCuenta.value = '';
	gForm.isChequeFormatoNuevo.value = '';

}

function setStyle(object, color) {
	object.style.borderColor = color;

}

function isValidDvCampo(dv) {
	return (dv != -1)
}

function getDigitos(numero, start, cant) {

	return (numero.substr(start, cant));

}

function getDvField(field) {

	var dv = -1;
	if (field.name == 'txtDDACuenta2')
		dv = calcDVmod10((field.value).slice(-11));
	else if (field.name == 'txtSerial2')
		dv = calcDVmod7(getDigitos(field.value, 0, field.value.length - 1),
				getDigitos(field.value, field.value.length - 1, 1), 7);

	return dv;

}

function processDigVDocCheque(funcionPrincipal) {
	var txnGrup1 = gtxnGrup1; // TODO consulta en main.jsp variable de contexto
	var txnGrup2 = gtxnGrup2;
	var txnGrup3 = gtxnGrup3;
	var feMaxVigCh = gfeMaxVigCh;
	var isTotalField = false;
	var blExisteGrupo1 = false;
	var blExisteGrupo2 = false;
	var blExisteGrupo3 = false;
	var txnGrup1Array = txnGrup1.split('*');
	var txnGrup2Array = txnGrup2.split('*');
	var txnGrup3Array = txnGrup3.split('*');
	var dvDDACuenta, dvTxtSerial, retornofnc = -1;
	var dvTxtCveTran = 1;
	var txtCveTran, txtDDACuenta, txtSerial, field;
	var mensajeDocumentoInvalido = "Documento Invalido";

	var txnVar = gForm.cTxn.value;
	for (pos = 0; pos < txnGrup1Array.length; pos++) {
		if (txnGrup1Array[pos] == txnVar) {
			blExisteGrupo1 = true;
			break;
		}
	}

	if (!blExisteGrupo1) {
		for (pos = 0; pos < txnGrup2Array.length; pos++) {
			if (txnGrup2Array[pos] == txnVar) {
				blExisteGrupo2 = true;
				break;
			}
		}
		if (!blExisteGrupo2) {

			for (pos = 0; pos < txnGrup3Array.length; pos++) {
				if (txnGrup3Array[pos] == txnVar) {
					blExisteGrupo3 = true;
					break;
				}
			}
		}
	}

	if (blExisteGrupo1) { // Flujo de Transacciones con validacion Completa

		setEmptyCamposCheques();

		if (!isVacio(gForm.txtCveTran.value)
				&& !isVacio(gForm.txtDDACuenta2.value)) {
			txtCveTran = gForm.txtCveTran.value;
			txtSerial = gForm.txtSerial2.value;
			var valTxtDDACuenta = (gForm.txtDDACuenta2.value).slice(-11);

			var isFormatoChequeNuevo = false;// isFormatChequeNuevo(funcionPrincipal);
			if (funcionPrincipal == 'isValidCheque' || funcionPrincipal == 'isValidChequeDev')
				isFormatoChequeNuevo = isRutaChequeNuevoBanistmo(gForm.txtCveTran.value);

			if (!isFormatoChequeNuevo) {
				var isDateValidToCheque = isDateValidToProcessCh();
				if (!isDateValidToCheque) {
					setTraza("Vencio el plazo para capturar cheques en este Formato");
					setMessageError(funcionPrincipal, mensajeDocumentoInvalido);
					setEmpty(gForm.txtSerial2);
					setEmpty(gForm.txtDDACuenta2);
					setEmpty(gForm.txtCveTran);
					return false;
				} else {
					var isCuentaValida = isValidDDA(valTxtDDACuenta);
					if (!isCuentaValida) {
						alert("La Cuenta ingresada es invalida. Por favor verifique.");
						setEmpty(gForm.txtSerial2);
						setEmpty(gForm.txtDDACuenta2);
						setEmpty(gForm.txtCveTran);

					} else {
						gForm.isChequeFormatoNuevo.value = 'N';
						gForm.noCheque.value = gForm.txtSerial2.value;
						gForm.noCuenta.value = gForm.txtDDACuenta2.value;
						gForm.txtSerial2.value = (gForm.txtSerial2.value).slice(-9);
						setTraza('CAPTURANDO CHEQUE EN FORMATO ANTERIOR'
								+ '\n-dvCuenta = ' + gForm.dvCuenta.value
								+ '\n-dvCheque = ' + gForm.dvCheque.value
								+ '\n-dvRuta = ' + gForm.dvRuta.value
								+ "\n-noCheque= " + gForm.noCheque.value
								+ "\n-noCuenta= " + gForm.noCuenta.value
								+ "\n-isChequeFormatoNuevo = "
								+ gForm.isChequeFormatoNuevo.value);
					}

				}
			} else // ES CHEQUE NUEVO
			{

				var isCuentaValida = isValidDDA(valTxtDDACuenta.substr(0,valTxtDDACuenta.length-1));
				dvDDACuenta = getDvField(gForm.txtDDACuenta2);
				if (!isCuentaValida || !isValidDvCampo(dvDDACuenta)) {
					//alert("La Cuenta ingresada es invalida. Por favor verifique.");
					setStyle(gForm.txtDDACuenta2, 'red');
					setMessageError('', mensajeDocumentoInvalido);
					setEmpty(gForm.txtSerial2);
					setEmpty(gForm.txtDDACuenta2);
					setEmpty(gForm.txtCveTran);

				} else if (!isVacio(txtSerial)) {
					txtSerial = quitaceros(txtSerial);
					dvTxtSerial = calcDVmod7(getDigitos(txtSerial, 0,
							txtSerial.length - 1), getDigitos(txtSerial,
								txtSerial.length - 1, 1), 7);
					if (!isValidDvCampo(dvTxtSerial)) {
						setStyle(gForm.txtSerial2, 'red');
						setMessageError('', mensajeDocumentoInvalido);
						setEmpty(gForm.txtSerial2);
						/*
						 * COMENTAR SI SE QUIERE IDENTIFICAR EL CAMPO
						 * ESPECIFICO
						 */
						setEmpty(gForm.txtDDACuenta2);
						setEmpty(gForm.txtCveTran);

					} else {
						gForm.isChequeFormatoNuevo.value = 'S';
						gForm.dvCuenta.value = dvDDACuenta;
						gForm.dvCheque.value = dvTxtSerial;
						gForm.dvRuta.value = '1';

						/*
						 * SE SETEAN LOS CAMPOS CUENTA Y SERIAL OMITIENDO EL
						 * DV
						 */
						txtSerial = (gForm.txtSerial2.value).slice(-9);
						gForm.noCheque.value = txtSerial;
						gForm.noCuenta.value = valTxtDDACuenta;

						gForm.txtSerial2.value = gForm.noCheque.value;
						gForm.txtDDACuenta2.value = gForm.noCuenta.value;
						/**/

						setTraza('CAPTURANDO CHEQUE EN FORMATO NUEVO SEGUN ACUERDO SUPERINTENDENCIA'
								+ '\n-dvCuenta = '
								+ gForm.dvCuenta.value
								+ '\n-dvCheque = '
								+ gForm.dvCheque.value
								+ '\n-dvRuta   = '
								+ gForm.dvRuta.value
								+ '\n-noCheque = '
								+ gForm.noCheque.value
								+ '\n-noCuenta = '
								+ gForm.noCuenta.value
								+ '\n-isChequeFormatoNuevo = '
								+ gForm.isChequeFormatoNuevo.value);
					} // fin de else DV txtserial
					//} // fin del else DV txtDDACuenta
				} // Fin del else isCuentaValida
			}

			return true;

		} // fin del si no es vacio todos los campos

	} else if (blExisteGrupo2) { // Flujo de Transacciones con validación
		// Parcial
		gForm.isChequeFormatoNuevo.value = 'S';

		if (txnVar == '0061') {
			txtSerial = gForm.txtSerialCheq.value;
			gForm.noCuenta.value = '000000100396630';
			gForm.dvCuenta.value = '2';
		} else if (txnVar == '4153') {
			txtSerial = gForm.txtSerial.value;
			gForm.noCuenta.value = '000000103000023';
			gForm.dvCuenta.value = '1';
		} else {
			txtSerial = gForm.txtSerial.value;

		}

		if (!isVacio(txtSerial)) {
			gForm.dvCheque.value = calcDVmod7(txtSerial, -1, // NO TIENE
			// ULTIMO, ES
			// VIRGEN
			7);
			gForm.noCheque.value = txtSerial;
		}

		setTraza('-dvCuenta = ' + gForm.dvCuenta.value + '\n-dvCheque = '
				+ gForm.dvCheque.value + '\n-dvRuta = ' + gForm.dvRuta.value
				+ "\n-noCheque= " + gForm.noCheque.value + "\n-noCuenta= "
				+ gForm.noCuenta.value + "\n-isChequeFormatoNuevo = "
				+ gForm.isChequeFormatoNuevo.value);

		return true;

	} else if (blExisteGrupo3) { // Flujo de transacciones sin validación

		gForm.dvCheque.value = '';
		gForm.dvCuenta.value = '';
		gForm.dvRuta.value = '';
		gForm.isChequeFormatoNuevo.value = '';
		gForm.noCuenta.value = gForm.txtDDACuenta.value;

		setTraza('-dvCuenta = ' + gForm.dvCuenta.value + '\n-dvCheque = '
				+ gForm.dvCheque.value + '\n-dvRuta = ' + gForm.dvRuta.value
				+ "\n-noCheque= " + gForm.noCheque.value + "\n-noCuenta= "
				+ gForm.noCuenta.value + "\n-isChequeFormatoNuevo = "
				+ gForm.isChequeFormatoNuevo.value);

		return true;

	}

	return true;
}
