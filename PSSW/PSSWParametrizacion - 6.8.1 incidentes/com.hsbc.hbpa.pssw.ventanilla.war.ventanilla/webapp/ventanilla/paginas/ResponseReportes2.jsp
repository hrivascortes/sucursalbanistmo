<!--
//*************************************************************************************************
//             Funcion: JSP que muestra los reportes de Administracion de Usuarios
/             Elemento: ResponseReportes2.jsp
//          Creado por: Jesus Emmanuel Lopez Rosales
//*************************************************************************************************
// CCN - 4360510 - 08/09/2006 - Pinta en pantalla el reporte de administracion de usuarios
// CCN - 4360581 - 12/03/2007 - Se convierten los estilos en jsf y se hace un include en el jsp
//*************************************************************************************************
-->

<%@page language= "java" import="java.util.Vector, ventanilla.GenericClasses" contentType="text/html"%>
<%
  Vector Datos  = (Vector)session.getAttribute("Log.Registros");
  String Tipo   = (String)session.getAttribute("Log.Tipo");
  String TipoB  = (String)session.getAttribute("Log.TipoB");
  int nopagina = 0;
  int totalpages= Integer.parseInt((String)session.getAttribute("Log.totalpages"));
  if(request.getParameter("nopagina")==null){
    nopagina = 0;
  }else{
    nopagina = Integer.parseInt(request.getParameter("nopagina").toString());
  }
  
  String printcad = "";
  int l = 0;
  int i = 0, g = 0;
  boolean coinciden = true;
  if(Tipo.equals("U"))
  {
  		Tipo = "Movimientos de Usuarios";
  		printcad = "REPORTELOG~";
  		l = 10;
  }
%>
<html>
<head>
  <title>Reportes Gerente/Supervisor</title>
  <%@include file="style.jsf"%>
</head>
<body bgcolor="white">
<form name="reportes" method="post" action="ResponseReportes2.jsp">
<%
String cajero = "";
int k=0;
int m=0;

if(Datos.size()>0){%>
  <table border="0" cellspacing="0" cellpadding="0" width="350">
    <tr>
  	  <td><font size="2"><b><%=Tipo%></b></font></td>
    </tr>
    <tr><td><br></td></tr>
  <%if(TipoB.equals("C")){
  	  cajero = (String)((Vector)Datos.get(0)).get(2);
  	  printcad+="C~"+cajero+"~";
  %>  <tr>	
        <td><font size="2"><b>Cajero: <%=cajero%></b></font></td>
      </tr>
  <%}else{
      String cadtemp = (String)((Vector)Datos.get(0)).get(0); 
      cadtemp = cadtemp.substring(0,4);
      printcad+="S~"+ cadtemp +"~";
  %>
      <tr>	
        <td><font size="2"><b>Sucursal: <% out.println(cadtemp);%></b></font></td>
      </tr>
   <%}%>
  </table><br>
  <table border="1" cellspacing="1" cellpadding="1" align='center'>
    <tr bgcolor="#CCCCCC" align='center'><td colspan=6><%=Tipo%></td></tr>
    <tr bgcolor="#CCCCCC" align='center'>
      <%if(Tipo.equals("Reversos")){%>
        <td>C&oacute;digo</td>
   	  <%}%>
      <td>Gerente/Supervisor</td>
      <td>Movimiento</td>
      <td>Cajero</td>
      <td>Dato anterior</td>
      <td>Dato Actual</td>
      <td>Hora</td>
    </tr>
    <%
    i=nopagina*10;
    g=i;
    k=0;
    String codRev = "";    
    while(i<Datos.size() && k<10)
    {
      Vector rows =  (Vector)Datos.get(i);
      out.print("<tr align='center'>");
      for (m=0;m < rows.size();m++){
         if(rows.elementAt(1).toString().equals("CAMBIO CONTRASENA") && m == 4){
            out.println("<td>********</td>");
            printcad+=" ********~";                     
         }else if(rows.elementAt(1).toString().indexOf("REACTIVACION DE PASSWORD") != -1 && m == 4){             
             String cadtempo = rows.elementAt(m).toString();
             cadtempo = cadtempo.substring(0,cadtempo.indexOf("1")) + "********"+ cadtempo.substring(cadtempo.indexOf("12345678")+8 ,cadtempo.length());
             out.println("<td>" + cadtempo + "</td>");
             printcad+= cadtempo + "~";                              
         }else if(m == 0 && rows.elementAt(m).toString().equals(rows.elementAt(2).toString())){
            out.println("<td>-</td>");
            printcad+="  -   ~";                 
         }else{ 
            out.println("<td>" + rows.elementAt(m) + "</td>");
            printcad+=rows.elementAt(m).toString()+"~";        
         }    
      } 
      out.print("</tr>");      
      i++;
      k++;
    }
    
    session.setAttribute("txtCadImpresion", printcad);
  %>
  <center>
  </table align="center"><br><br>
<%if(nopagina>0){
    session.setAttribute("Log.nopagina",String.valueOf(nopagina-1));
%>
    <a href="javascript:prevpage();submite()" onmouseover="self.status='Pagina Anterior';return true;" onmouseout="window.status='';" alt="Pagina Anterior"><img src="../imagenes/b_pagina_anterior.gif" border="0"></a>
<%}%>
<%if(nopagina+1 != totalpages){
    session.setAttribute("Log.nopagina",String.valueOf(nopagina+1));
%>
    <a href="javascript:submite()" onmouseover="self.status='Pagina Siguiente';return true;" onmouseout="window.status='';" alt="Pagina Siguiente"><img src="../imagenes/b_pagina_siguiente.gif" border="0"></a>
<%}%>
  <a href="javascript:top.openDialog('../../servlet/ventanilla.ImprimirServlet', 160, 120, 'top.setPrefs4()', document.reportes)"><img src="../imagenes/b_imprimir.gif" border="0"></a>
<%
}else
  out.println("<b>No se han realizado " +Tipo+"...</b>");
%>
<input type="hidden" name="nopagina" value="<%=nopagina+1%>">
</form>
</body>
  <script language="JavaScript">
    function prevpage(){
     <%nopagina = nopagina-1;%>
     window.document.reportes.nopagina.value=<%=nopagina%>;
    }
    
    function submite(){
      document.reportes.action = "ResponseReportes2.jsp";
      document.reportes.submit();
    }
  </script>
</html>
