<!--
//*************************************************************************************************
//             Funcion: JSP que despliega el menu de ventanilla
//            Elemento: PageBuilderIRSC.jsp
//          Creado por: Juan Carlos Gaona
//      Modificado por: Alejandro Gonzalez Castro
//*************************************************************************************************
// CCN - 4360211 - 08/10/2004 - Se crea menu de Relaciones
// CCN - 4360581 - 12/03/2007 - Se convierten los estilos en jsf y se hace un include en el jsp
//*************************************************************************************************
-->

<%@ page session="true"%>
<%
response.setHeader("Cache-Control","no-store"); //HTTP 1.1
response.setHeader("Pragma","no-cache"); //HTTP 1.0
response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
	<head>
		<title>Transmision de Registros</title>		
		<%@include file="style.jsf"%>
	</head>
	<Script language="JavaScript">
		function sendservlet()
		{
			document.entry.method = "POST"
			document.entry.action = "../servlet/ventanilla.RelacionesServlet"
			document.entry.submit();
		}
		
   </SCRIPT>	
	<body>
		<form name="entry">
			<h1><%=session.getAttribute("page.txnImage")%></h1>		
			<b><h3><%=session.getAttribute("page.cTxn")%> - <%=session.getAttribute("page.txnlabel")%></h3></b>
			<p>	
			<br>
			<input type="hidden" name="cTxn" value="<%= session.getAttribute("page.cTxn") %>">
			<input type="hidden" name="moneda" value="<%= session.getAttribute("page.moneda")%>">								
			<a href="JavaScript:sendservlet()"><img src="../ventanilla/imagenes/b_aceptar.gif" border="0"></a>
		</form>
	</body>
</html>
