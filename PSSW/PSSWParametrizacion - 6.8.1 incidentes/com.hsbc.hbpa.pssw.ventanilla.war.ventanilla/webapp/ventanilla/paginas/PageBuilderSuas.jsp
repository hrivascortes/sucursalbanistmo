<!--
//*************************************************************************************************
///            Funcion: JSP que muestra el Desglose de la txn 0837
//            Elemento: PageBuilderSuas.jsp
//          Creado por: Liliana Neyra Salazar
//*************************************************************************************************
// CCN - 4360602 - 18/05/2007 - Se realizan modificaciones para proyecto Codigo de Barras SUAS
//*************************************************************************************************
-->

<%@ page session="true" import="java.util.*, ventanilla.GenericClasses"%>
<%
response.setHeader("Cache-Control","no-store"); //HTTP 1.1
response.setHeader("Pragma","no-cache"); //HTTP 1.0
response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<%
Vector listaCampos = (Vector)session.getAttribute("page.listcampos");
Vector listaContenidos = (Vector)session.getAttribute("page.listcont");
String txnAuthLevel = (String)session.getAttribute("page.txnAuthLevel");
String txnIMGAutoriz = (String)session.getAttribute("page.txnIMGAutoriz");
int k;
String MontoPro = new String("");
Hashtable datasession = (Hashtable)session.getAttribute("page.datasession");
Stack flujotxn = new Stack();
if(session.getAttribute("page.flujotxn") != null){
	flujotxn = (java.util.Stack)session.getAttribute("page.flujotxn");
	flujotxn.removeAllElements();
}

if(session.getAttribute("page.cTxn")!=null){
	if(session.getAttribute("page.cTxn").toString().equals("S772")){
		session.setAttribute("page.cTxn","0772");
		session.setAttribute("page.oTxn","0772");
	}
}

datasession.put("txtServicio","000129");
datasession.put("txttipo","E");

GenericClasses gc = new GenericClasses();
%>
<html>
<head>
<%@include file="style.jsf"%>
  <title>Certificación</title>
<script language="JavaScript" type="text/javascript">
<!--
function validate(form)
{
	var bandera = 0;
<%
	for(int i=0; i < listaCampos.size(); i++)
	{
		Vector vCampos = (Vector)listaCampos.get(i);
		vCampos = (Vector)vCampos.get(0);
		if( vCampos.get(4).toString().length() > 0 && datasession.get(vCampos.get(0)) == null)
		{
			out.println("  if( !top.validate(window, document.Txns." + vCampos.get(0) + ", '" + vCampos.get(4) +"') )");
			out.println("return;") ;
		}
	}
%>
	var seleccion = form.lstTPSua.options[form.lstTPSua.selectedIndex].value;
  	if(seleccion == "T" || seleccion == "C" || seleccion == "O")
  		form.action = '../ventanilla/paginas/RedirectorSUAS.jsp';
  		
	if(bandera == 0){
		setIdPagos(form)
  		form.submit();
		var obj = document.getElementById('btnCont');
		obj.onclick = new Function('return false');
	}
}

function FormatCurr(val, field)
{
  var num = val.toString().replace(/\$|\,/g,'')
  
  intVal = num.substring(0, num.indexOf("."))
  decVal = num.substring(num.indexOf(".") + 1, num.length)
  num = intVal + decVal;

  if( isNaN(num) )
  {
    return false
  }

  cents = Math.floor((num)%100)
  num = Math.floor((num)/100).toString()
  
  if(cents < 10)
    cents = "0" + cents;
  if(cents.length == 1)
    cents = cents + "0";

  for (var i = 0; i < Math.floor((num.length-(1+i))/3); i++)
    num = num.substring(0,num.length-(4*i+3))+','+num.substring(num.length-(4*i+3))

  num = num + '.' + cents
  eval("document.Txns."+field+".value = '"+ num + "'");
}


function calculaMontos(){
  var Total = 0;
  var TotalBimestral = 0;
  var IMSS  = 0;
  
  IMSS = Number('<%=gc.quitap(datasession.get("txtEMI1").toString())%>');
  TotalBimestral  = Number('<%=gc.quitap(datasession.get("txtEMI2").toString())%>');
  
  
  gForm = document.Txns
  
  if(gForm.chkMensual.checked)
      Total = Total + IMSS;
  if(gForm.chkBimestral.checked)
    Total = Total+TotalBimestral;
    
  gForm.txtTotalSUAS.value= Total;
 
}

function setIdPagos(gform)
{
	var idpago = ""
	if(gForm.chkMensual.checked && !gForm.chkBimestral.checked){
    	idpago = "M";
    }
    if(gForm.chkBimestral.checked && !gForm.chkMensual.checked){
    	idpago = "N";
    }
    if(gForm.chkMensual.checked && gForm.chkBimestral.checked){
		idpago = "I";
    }
	gform.idpagosSuas.value = idpago;
}

function cancel()
{
	document.Txns.action = '../ventanilla/paginas/Final.jsp';
	document.Txns.submit();
}
//-->
</script>
</head>
<body bgcolor="#FFFFFF" onLoad ="JavaScript:calculaMontos();FormatCurr(document.Txns.txtTotalSUAS.value,'txtTotalSUAS');" >
<form name="Txns" action="../servlet/ventanilla.DataServlet" method="post" onSubmit="return validate(this)"  >
<!--VISUALIZACIÓN DE LA INFORMACIÓN OBTENIDA-->
<h1><%=session.getAttribute("page.txnImage")%></h1>
<h3>
<%out.print(session.getAttribute("page.cTxn") + "  " + (String)session.getAttribute("page.txnlabel"));%>
</h3>
<table border="0" cellspacing="0" class="normaltextblack">
<% 
String nMensual = datasession.get("txtnomen").toString();
String nBimestral = datasession.get("txtnobim").toString();
String nPeriodo = datasession.get("txtPagoPeriodo").toString();
for(int i =  0; i < listaCampos.size(); i++)
{
	Vector vCampos = (Vector)listaCampos.get(i);
	vCampos = (Vector)vCampos.get(0);
	if(!vCampos.get(0).toString().equals("txtCodSeg"))
	{
		out.println("<tr>");
    	if(vCampos.get(0).toString().equals("txtEMI1"))
    	{
    	   if (nMensual.equals("1"))
    	   	out.print("  <td><input type=\"checkbox\" name=\"chkMensual\"  disabled onClick=\"JavaScript:calculaMontos();FormatCurr(document.Txns.txtTotalSUAS.value,'txtTotalSUAS');\">&nbsp;" + vCampos.get(2) + ":</td>");
    	   else	
    	    out.print("  <td><input type=\"checkbox\" name=\"chkMensual\" checked  onClick=\"JavaScript:calculaMontos();FormatCurr(document.Txns.txtTotalSUAS.value,'txtTotalSUAS');\">&nbsp;" + vCampos.get(2) + ":</td>");
		}	
		else if(vCampos.get(0).toString().equals("txtEMI2"))
		{
		    if (nBimestral.equals("1") != nPeriodo.equals("M"))
			out.print("  <td><input type=\"checkbox\" name=\"chkBimestral\" disabled onClick=\"JavaScript:calculaMontos();FormatCurr(document.Txns.txtTotalSUAS.value,'txtTotalSUAS');\">&nbsp;" + vCampos.get(2) + ":</td>");
			else
			out.print("  <td><input type=\"checkbox\" name=\"chkBimestral\" checked onClick=\"JavaScript:calculaMontos();FormatCurr(document.Txns.txtTotalSUAS.value,'txtTotalSUAS');\">&nbsp;" + vCampos.get(2) + ":</td>");
		
		}
		else{
			out.println("  <td>" + vCampos.get(2) + ":</td>");
		}
		out.println("  <td width=\"10\">&nbsp;</td>");
		if(datasession.get(vCampos.get(0)) != null)
		{
			out.print  ("  <td>" + datasession.get(vCampos.get(0)) + "</td");
			out.println("</tr>");
		}
		else
		{
			if(!vCampos.get(0).toString().substring(0,3).equals("lst"))
			{
				if(vCampos.get(0).toString().substring(0,3).equals("pss"))
				{
					out.print  ("  <td><input maxlength=\"" + vCampos.get(3) + "\" name=\"" + vCampos.get(0) + "\" size=\"" + vCampos.get(3) +"\" type=\"Password\"" + " tabindex=\""+(i+1)+"\"");
				}
				else
				{
					if(vCampos.get(0).toString().equals("txtTotalSUAS")){
						out.print("  <td><input maxlength=\"" + vCampos.get(3) + "\" name=\"" + vCampos.get(0) + "\" size=\"" + vCampos.get(3) +"\" type=\"text\"" + " tabindex=\""+(i+1)+"\" readonly ");
						out.print("  <td><input name=\"txtimporte\" type=\"hidden\" tabindex=\""+(i+1)+"\" readonly ");
					}	
					else
						out.print("  <td><input maxlength=\"" + vCampos.get(3) + "\" name=\"" + vCampos.get(0) + "\" size=\"" + vCampos.get(3) +"\" type=\"text\"" + " tabindex=\""+(i+1)+"\"");
				}
	  
				String CampoR = (String)vCampos.get(5);
				if(CampoR != null && CampoR.length() > 0)
				{
					CampoR = CampoR.trim();
					out.print(" value=\"" + CampoR + "\"");
				}
			}
			else
			{
				out.print("  <td><select name=\"" + vCampos.get(0) + "\" tabindex=\"" + (i+1) + "\" size=\"" + "1" + "\"");
				if( vCampos.get(4).toString().length() > 0 )
				{
					if(vCampos.get(0).toString().substring(0,3).equals("rdo"))
						out.println(" onClick=\"top.validate(window, this, '" + vCampos.get(4) +"')\">");
					else
						out.println(" onChange=\"top.validate(window, this, '" + vCampos.get(4) +"')\"  >");
				}
				else
					out.println(">");
				for(k=0; k < listaContenidos.size(); k++)
				{
					Vector v1Campos = (Vector)listaContenidos.get(k);
					for(int j = 0; j < v1Campos.size(); j++)
					{
						Vector v1Camposa = (Vector)v1Campos.get(j);
						if(v1Camposa.get(0).toString().equals(vCampos.get(0).toString()))
							out.println("  <option value=\"" + v1Camposa.get(3) + "\">" + v1Camposa.get(2));
					}
				}
				out.println("  </select></td>");
			}
			if( vCampos.get(4).toString().length() > 0 && !vCampos.get(0).toString().substring(0,3).equals("lst"))
				out.println(" onBlur=\"top.estaVacio(this)||top.validate(window, this, '" + vCampos.get(4) +"')\"  onKeyPress=\"top.keyPressedHandler(window, this, '" + vCampos.get(4) +"')\"   ></td>");
			else if(!vCampos.get(0).toString().substring(0,3).equals("lst"))
				out.println(">");
			out.println("</tr>");
		}
	}
	else
	{%>
		<%@include file="fieldLector.jsf"%><%
		i = k;
	}
}%>
</table>
<table>
 <tr><td>&nbsp;</td></tr> 
 <tr><td><a id='btnCont' href="javascript:validate(document.Txns);"><img src="../ventanilla/imagenes/b_continuar.gif" border="0"></a></td>
     <td><a id='btnCan' href="javascript:cancel()" ><img src="../ventanilla/imagenes/b_cancelar.gif" border="0"></a></td>
 </tr>
</table> 
</td>
</tr>
</table>
<input type="hidden" name="iTxn" value="<%= session.getAttribute("page.iTxn") %>">
<input type="hidden" name="oTxn" value="<%= session.getAttribute("page.oTxn") %>">
<input type="hidden" name="cTxn" value="<%= session.getAttribute("page.cTxn") %>">
<input type="hidden" name="opcionSuas" value="2">
<input type="hidden" name="idpagosSuas" value="">
<input type="hidden" name="supervisor" value="<%= session.getAttribute("teller") %>">

</form>
</body>
</html>