<!--
//*************************************************************************************************
//             Funcion: JSP que despliega respuesta de txn 0520
//            Elemento: Response0520.jsp
//          Creado por: Alejandro Gonzalez Castro
//      Modificado por: Alejandro Gonzalez Castro
//*************************************************************************************************
// CCN - 4360175 - 03/08/2004 - Se eliminan displays a log / ajuste size ventana impresion
// CCN - 4360189 - 03/09/2004 - Se redirecciona a Final.jsp por Control de Efectivo
// CCN - 4360275 - 18/02/2005 - WAS 5.1
//*************************************************************************************************
-->

<%@ page session="true" import="java.util.*"%>
<%
response.setHeader("Cache-Control","no-store"); //HTTP 1.1
response.setHeader("Pragma","no-cache"); //HTTP 1.0
response.setDateHeader ("Expires", 0); //prevents caching at the proxy server*/
%>
<%!
	private String addpunto(String valor)
	{
		int longini = valor.length();
		if (longini == 1)
		{
			valor = "0.0" + valor;
			return valor;
		}
		String cents  = valor.substring(longini-2,longini);
		String entero = valor.substring(0,longini-2);
		int longente = entero.length();
		for (int i = 0; i < (longente-(1+i))/3; i++)
		{
			longente = entero.length();
			entero = entero.substring(0,longente-(4*i+3))+','+entero.substring(longente-(4*i+3));
		}
		entero = entero + '.' + cents;
		return entero;
	}

	private String getItem(String newCadNum, int newOption)
	{
		int iPIndex = newCadNum.indexOf("CONSECUTIVO:") + 12, NumSaltos, Num = 0, Ban = 0;
		String nCadNum = new String("");
		NumSaltos = newOption;
		while(Num < NumSaltos)
		{
			if(newCadNum.charAt(iPIndex) == 0x20)
			{
				while(newCadNum.charAt(iPIndex) == 0x20)
				iPIndex++;
				Num++;
			}
			else
				while(newCadNum.charAt(iPIndex) != 0x20)
			iPIndex++;
		}
   	while(newCadNum.charAt(iPIndex) != 0x20 && iPIndex < newCadNum.length())
   	{
			nCadNum = nCadNum + newCadNum.charAt(iPIndex);
			if(iPIndex < newCadNum.length())
				iPIndex++;
			if(iPIndex == newCadNum.length())
				break;
		}
		return nCadNum;
	}

	private String getString(String s)
	{
		int len = s.length();
		for(; len>0; len--)
			if(s.charAt(len-1) != ' ')
				break;
		return s.substring(0, len);
	}

	private String delCommaPointFromString(String newCadNum)
	{
		String tmp = "";
		if (newCadNum.length() == 0)
		{
			tmp="000";
			return tmp;
		}
		for (int z =0; z<newCadNum.length(); z++)
			if (newCadNum.charAt(z) != '.' && newCadNum.charAt(z) != ',')
				tmp += newCadNum.charAt(z);
		return tmp;
	}
%>
<html>
<%
	Vector resp = (Vector)session.getAttribute("response");
	Hashtable datasession = (Hashtable)session.getAttribute("page.datasession");
%>

<head>
	<title>Certificación</title>
<style type="text/css">Body {font-family: Arial, Helv; font-size: 10pt; font-weight: bold; color:#000000}</style>
</head>
<script language="javascript">
function imprime()
{
 alert("Coloque papel en Impresora y Pulse Entrar...");
}
function changeaction()
{
  document.Txns.action = "../../servlet/ventanilla.DataServlet";
}
function RFCinvalid()
{
    alert("Numero de Patron No Registrado (SAR)");
	document.Txns.action = "../paginas/Final.jsp";
}
function continuar()
{
    document.Txns.submit();
}
</script>
<body>
<form name="Txns" action="../../servlet/ventanilla.PageServlet" method="post">
 <input type="hidden" name="txtInvoqued" value="somedatahere">
<table border="0" cellspacing="0">
<tr>
<td>
  <input type="hidden" name="transaction" value="<%= session.getAttribute("page.iTxn") %>">
  <input type="hidden" name="transaction1" value="<%= session.getAttribute("page.oTxn") %>">
</td>
</tr>
</table>
<p>
<%
   String szResp = new String("");
   int codigo = 1;
   if( resp.size() != 4 )
    out.println("<b>Error de conexi&oacute;n</b><br>");
   else
   {
    try{
     codigo = Integer.parseInt((String)resp.elementAt(0));
    }
    catch(NumberFormatException nfe){
     out.println("<b>Error de conexi&oacute;n</b><br>");
    }
    if( codigo != 0 )
	  out.println("<b>Error en transaccion</b><br>");
     int lines = Integer.parseInt((String)resp.elementAt(1));
     szResp = (String)resp.elementAt(3);
     for(int i=0; ; i++){
      String line = szResp.substring( i*78, Math.min(i*78+77, szResp.length()) );
      out.println(getString(line) + "<p>");
      if(Math.min(i*78+77, szResp.length()) == szResp.length())
       break;
     }
   }

   String cTxn = (String)datasession.get("cTxn");
   if (codigo == 0)
   {
        String txtsector = "";
       String txtpatron = "";

        if (cTxn.equals("0520"))
        {
            txtsector = getItem(szResp, 2);
            if ( txtsector.equals("1"))
               txtpatron = getItem(szResp, 3);
            else
               txtpatron = getItem(szResp, 4);
            datasession.put("txtsector",txtsector);
            datasession.put("txtpatron",txtpatron);
            if (txtsector.equals("00000000000"))
            {
                datasession.put("OutOff","1");%>
                <script language="javascript">
                    RFCinvalid();
                </script>
<%          }
            if (!datasession.get("txtsector").toString().equals("0") && !datasession.get("txtsector").toString().equals("1"))
            {
                Stack flujotxn = (Stack)session.getAttribute("page.flujotxn");
                Stack flujonew = new Stack();
                String[] newflujo = {"0536","0534","S532","5353","0532"};
                for (int i=0; i<newflujo.length; i++)
                    {flujonew.push(newflujo[i]);}
                flujotxn = flujonew;
                session.setAttribute("page.flujotxn",flujotxn);
            }
        }
        if (cTxn.equals("0522") || cTxn.equals("0532"))
        {
            long efectivo = Long.parseLong(delCommaPointFromString((String)datasession.get("txtEfect")));
            long monto = Long.parseLong(delCommaPointFromString((String)datasession.get("txtMto")));
            long docs = monto - efectivo;
            datasession.put("txtCheque", addpunto(Long.toString(docs)));
            Stack flujotxn = (Stack)session.getAttribute("page.flujotxn");
            if (docs == 0)
            {
                flujotxn.pop();
                session.setAttribute("page.flujotxn",flujotxn);
            }
            long mtoA = 0;
            long mtoB = 0;
            String txnr = "";
            if (cTxn.equals("0522"))
            {
                mtoA = Long.parseLong(delCommaPointFromString((String)datasession.get("txtCveIMSS")));
                mtoB = Long.parseLong(delCommaPointFromString((String)datasession.get("txtCveINFONAV")));
            }
            if (cTxn.equals("0532"))
            {
                mtoA = Long.parseLong(delCommaPointFromString((String)datasession.get("txtCveISSSTE")));
                mtoB = Long.parseLong(delCommaPointFromString((String)datasession.get("txtCveFOVI")));
            }
            if(mtoA == 0)
            {
                if (cTxn.equals("0522"))
                    txnr = "0524";
                else
                    txnr = "0534";
                flujotxn.removeElement(txnr);
            }
            if(mtoB == 0)
            {
                if (cTxn.equals("0522"))
                    txnr = "0526";
                else
                    txnr = "0536";
                flujotxn.removeElement(txnr);
            }
            session.setAttribute("page.flujotxn",flujotxn);
}
        if (!cTxn.equals("0522") && !cTxn.equals("0532") && !cTxn.equals("0520"))
        {
            //Certificacion Inicio
            String changetxn = (String)session.getAttribute("page.cTxn");
            if (changetxn.equals("S532") || changetxn.equals("S522"))
            {
                changetxn = "0" + changetxn.substring(1);
                session.setAttribute("page.cTxn",changetxn);
            }
            else
                {session.setAttribute("page.cTxn",cTxn);}

            String isCertificable = (String)session.getAttribute("page.certifField");  // Para Certificación
            String txtCadImpresion  = ""; // Para Certificación
            if(!isCertificable.equals("N") )
            { // Para Certificación
                session.setAttribute("Consecutivo", getItem((String)resp.elementAt(3), 1)); // Para Certificación
                session.setAttribute("Tipo", isCertificable); // Para Certificación
               out.println("<script language=\"javascript\">top.openDialog('../../servlet/ventanilla.ImprimirServlet', 160, 120, 'top.setPrefs4()', document.Txns)</script>"); // Para Certificación
            }
            //Certificaion Fin

            if (!cTxn.equals("0520"))
            {%>
                <script language="javascript">
                    changeaction();
                </script>
<%          		Stack flujotxn = (Stack)session.getAttribute("page.flujotxn");
                if (!flujotxn.empty())
                {

                    String txnpop = flujotxn.peek().toString();
                    if((cTxn.equals("S522") && (txnpop.equals("0524") || txnpop.equals("0526"))) ||
                       (cTxn.equals("S532") && (txnpop.equals("0534") || txnpop.equals("0536"))))
                    {
                        datasession.put("cTxn",txnpop);
                    }
                    else
                    {
                        String newtxn = flujotxn.pop().toString();
                        if(!flujotxn.empty())
                        {
                            txnpop = flujotxn.peek().toString();
                            datasession.put("cTxn",txnpop);
                        }
                        if (flujotxn.empty())
                        {    flujotxn.push("1111");

                        }
                        if(newtxn.equals("1111"))
                        {
                            flujotxn.pop();
                        }
                    }
                }
                else
                {datasession.put("OutOff","1");}
            }
        }
        else
        {%>
            <SCRIPT language='javascript'>
                continuar();
            </SCRIPT>
<%      }
   }
%>
</form>
</body>
</html>
