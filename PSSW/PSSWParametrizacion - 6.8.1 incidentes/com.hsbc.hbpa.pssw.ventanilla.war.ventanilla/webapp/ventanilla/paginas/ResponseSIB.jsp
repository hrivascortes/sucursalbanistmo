<!--
//*************************************************************************************************
//             Funcion: JSP para despliege de Formato SIB
//            Elemento: PageBuilderSIB.jsp
//      Modificado por: Humberto Enrique Balleza Mej�a
//*************************************************************************************************
// CCN -  -  - Creaci�n de Vista
//*************************************************************************************************
-->
<%@ page session="true" import="java.util.*,ventanilla.GenericClasses"%>
<%
response.setHeader("Cache-Control","no-store"); //HTTP 1.1
response.setHeader("Pragma","no-cache"); //HTTP 1.0
response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<%
String MontoPro = new String("");
Vector listaCampos = (Vector)session.getAttribute("page.listcampos");
Vector listaContenidos = (Vector)session.getAttribute("page.listcont");
String txnAuthLevel = (String)session.getAttribute("page.txnAuthLevel");
String txnIMGAutoriz = (String)session.getAttribute("page.txnIMGAutoriz");
Vector v = (Vector)session.getAttribute("page.specialacct");
Hashtable datasession = (Hashtable)session.getAttribute("page.datasession");
String txn = session.getAttribute("page.cTxn").toString();
ventanilla.GenericClasses gc = new ventanilla.GenericClasses();
if (datasession == null)
   datasession = new Hashtable();
int k;
int i;
%>

<html>
<head>
  <title>Datos de la Transaccion</title>
  <%@include file="style.jsf"%>
<script language="JavaScript">
<!--
function Imprime()
{
	top.openDialog('../../servlet/ventanilla.ImprimirServlet', 160, 120, 'top.setPrefs4()', document.entry);
	return true;
}
//-->
</script>
</head>
<body bgcolor="#FFFFFF" onload="return Imprime()">
<br>
<form name="entry" method="post">

<input type="hidden" name="iTxn" value="<%= session.getAttribute("page.iTxn") %>">
<input type="hidden" name="oTxn" value="<%= session.getAttribute("page.oTxn") %>">
<input type="hidden" name="cTxn" value="<%= session.getAttribute("page.cTxn") %>">
<input type="hidden" name="sucursal" value="<%= session.getAttribute("branch") %>">
<input type="hidden" name="teller" value="<%= session.getAttribute("teller") %>">
<input type="hidden" name="moneda" value="<%= session.getAttribute("page.moneda")%>">
<input type="hidden" name="registro" value="<%=session.getAttribute("empno")%>">
<input type="hidden" name="tllrlvl" value="<%=session.getAttribute("tellerlevel")%>">
<input type="hidden" name="supervisor" value="<%= session.getAttribute("teller") %>">
<input type="hidden" name="AuthoOK" value="0">
<input type="hidden" name="SignOK" value="0">
<input type="hidden" name="validateFLD" value="0">
<input type="hidden" name="needSignature" value="<%out.print(txnIMGAutoriz);%>">
<input type="hidden" name="needAutho" value="<%out.print(txnAuthLevel);%>">
<input type="hidden" name="SignField" value="<%= session.getAttribute("page.SignField") %>">
<input type="hidden" name="override" value="N">
<input type="hidden" name="txtFees" value="0">
<input type="hidden" name="compania" value="20">
<input type="hidden" name="txtSelectedItem" value="0" >
<input type="hidden" name="returnform" value="N" >
<input type="hidden" name="returnform2" value="N" >
<input type="hidden" name="txtPlazaSuc" value="<%= session.getAttribute("plaza") %>" >
<input type="hidden" name="passw" value="<%=session.getAttribute("password")%>">
<input type="hidden" name="MontoRetiroPesos" value = "<%=session.getAttribute("Retiro.Pesos")%>">
<input type="hidden" name="MontoRetiroDolar" value = "<%=session.getAttribute("Retiro.Dolar")%>">
<p>
<style>
		.invisible
		{
		  display:None;
		}

		.visible
		{
		  display:"";
		}
</style>
</form>
</body>
</html>
