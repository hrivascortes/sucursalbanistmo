<!--
//*************************************************************************************************
//             Funcion: JSP que presenta las opciones de la txn 0560
//            Elemento: PageBuilder0560.jsp
//          Creado por: Alejandro Gonzalez Castro
//      Modificado por: Jesus Emmanuel Lopez Rosales
//      Modificado por: Carolina Velas
//*************************************************************************************************
// CCN - 4360576 - 09/03/2007 - Modificaciones para proyecto OPMN
// CCN - 4360581 - 12/03/2007 - Se convierten los estilos en jsf y se hace un include en el jsp
// CCN - 4360584 - 23/03/2007 - Modificaciones para proyecto OPMN
// CCN - 4620008 - 14/09/2007 - Modificaciones para tropicalizacion
// CCN - 4620024 - 24/10/2007 - Modificacione spara el campo de IVA.
// CCN - 4620024 - 26/10/2007 - Modificaciones para el campo de IVA.
//*************************************************************************************************
-->

<%@ page session="true" import="java.util.Hashtable,java.lang.*"%>
<%
response.setHeader("Cache-Control","no-store"); //HTTP 1.1
response.setHeader("Pragma","no-cache"); //HTTP 1.0
response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<%
String txnAuthLevel = (String)session.getAttribute("page.txnAuthLevel");
String txnIMGAutoriz = (String)session.getAttribute("page.txnIMGAutoriz");
Hashtable datasession = (Hashtable)session.getAttribute("page.datasession");

//Asignar valor de idflujo para flujos internos
   String stxn = session.getAttribute("page.iTxn").toString();  
   String iva = (String)datasession.get("montoIVA");
   
   if(stxn.equals("0021")){
     if(iva.equals("0") || iva.equals(null)){
     	session.setAttribute("idFlujo","07");
     }
     else  
      session.setAttribute("idFlujo","05");
   //     session.setAttribute("idFlujo","07");
   }   
%>
<%!

  private String setPointToString(String newCadNum)
  {
    int iPIndex = newCadNum.indexOf("."), iLong;
    String szTemp;

    if(iPIndex > 0)
    {
      newCadNum = new String(newCadNum + "00");
      newCadNum = newCadNum.substring(0, iPIndex + 1) + newCadNum.substring(iPIndex + 1, iPIndex + 3);
    } else {
      for(int i = newCadNum.length(); i < 3; i++)
        newCadNum = "0" + newCadNum;
      iLong = newCadNum.length();
      if(iLong == 3)
        szTemp = newCadNum.substring(0, 1);
      else
        szTemp = newCadNum.substring(0, iLong - 2);
        
      newCadNum = szTemp + "." + newCadNum.substring(iLong - 2);
    }

    return newCadNum;
  }

  private String delPointOfString(String newCadNum)
  {
    int iPIndex = newCadNum.indexOf(".");

    if(iPIndex > 0)
    {
      newCadNum = newCadNum.substring(0, iPIndex) +
                  newCadNum.substring(iPIndex + 1, newCadNum.length());
    }

    return newCadNum;
  }

  private String delCommaPointFromString(String newCadNum)
  {
    String nCad = new String(newCadNum);
    if( nCad.indexOf(".") > -1)
    {
      nCad = nCad.substring(nCad.indexOf(".") + 1, nCad.length());
      if(nCad.length() != 2)
      {
        String szTemp = new String("");
        for(int j = nCad.length(); j < 2; j++)
          szTemp = szTemp + "0";
        
        newCadNum = newCadNum + szTemp;
      }
    }

    StringBuffer nCadNum = new StringBuffer(newCadNum);
    for(int i = 0; i < nCadNum.length(); i++)
      if(nCadNum.charAt(i) == ',' || nCadNum.charAt(i) == '.')
        nCadNum.deleteCharAt(i);

    return nCadNum.toString();
  }

  private String setFormat(String newCadNum)
  {
    int iPIndex = newCadNum.indexOf(".");
    String nCadNum = new String(newCadNum.substring(0, iPIndex));
    for (int i = 0; i < (int)((nCadNum.length()-(1+i))/3); i++)
      nCadNum = nCadNum.substring(0, nCadNum.length()-(4*i+3))+','+nCadNum.substring(nCadNum.length()-(4*i+3));

    return nCadNum + newCadNum.substring(iPIndex, newCadNum.length());
  }

  private String longtoString(long newlong)
  {
    String newString = String.valueOf(newlong);
    return newString.toString();
  }
%>
<html>
<head>
  <%@include file="style.jsf"%>
  <title>Datos de la Transaccion</title>
  <script language='javascript'>
  <!--
    function validate()
    {
      document.forms[0].submit();
		var obj = document.getElementById('btnCont')
		obj.onclick = new Function('return false')
    }
  //-->
  </script>
</head>
<body>

<%
out.print("<form name=\"entry\" action=\"../servlet/ventanilla.DataServlet\" method=\"post\">");
%>

<h1><%=session.getAttribute("page.txnImage")%></h1>

<h3>
<%
  String cTxn = (String)session.getAttribute("page.cTxn");
  String txn = new String("");
  
  if(!cTxn.equals("0578"))
  {
      out.print(session.getAttribute("page.cTxn"));
  } else {
    out.print(session.getAttribute("page.cTxn"));
  }
   
  out.print("  " + (String)session.getAttribute("page.txnlabel"));
%>

</h3>
<table border="0" cellspacing="0">
 <tr>
  <td>Monto:</td>
  <td width="10">&nbsp;</td>
  <td align="right"><%=setFormat(setPointToString(delCommaPointFromString((String)datasession.get("txtMonto"))))%></td>
 </tr>
 <tr>
  <td>Comisi�n:</td>
  <td width="10">&nbsp;</td>
  <td align="right"><%=setFormat(setPointToString(delCommaPointFromString((String)datasession.get("montoComision"))))%></td>
 </tr>
<%
	if(!iva.equals("0")){
%>	 
 <tr>
  <td>ITBMS:</td>
  <td width="10">&nbsp;</td>
  <td align="right"><%=setFormat(setPointToString(delCommaPointFromString((String)datasession.get("montoIVA"))))%></td>
 </tr>
<% 
 }
 %>
 <tr>
  <td>Total:</td>
  <td width="10">&nbsp;</td>
  <td align="right"><%=setFormat(setPointToString(delCommaPointFromString((String)datasession.get("montoTotal"))))%></td>
 </tr>
</table>
<input type="hidden" name="iTxn" value="<%= session.getAttribute("page.iTxn") %>">
<input type="hidden" name="oTxn" value="<%= session.getAttribute("page.oTxn") %>">
<input type="hidden" name="cTxn" value="<%= session.getAttribute("page.cTxn") %>">
<input type="hidden" name="sucursal" value="<%= session.getAttribute("branch") %>">
<input type="hidden" name="teller" value="<%= session.getAttribute("teller") %>">
<input type="hidden" name="moneda" value="<%= session.getAttribute("page.moneda")%>">
<input type="hidden" name="registro" value="<%=session.getAttribute("empno")%>">
<input type="hidden" name="tllrlvl" value="<%=session.getAttribute("tellerlevel")%>">
<input type="hidden" name="supervisor" value="<%= session.getAttribute("teller") %>">
<input type="hidden" name="AuthoOK" value="0">
<input type="hidden" name="SignOK" value="0">
<input type="hidden" name="compania" value="20">
<input type="hidden" name="validateFLD" value="0">
<input type="hidden" name="needSignature" value="<%out.print(txnAuthLevel);%>">
<input type="hidden" name="needAutho" value="<%out.print(txnIMGAutoriz);%>">
<input type="hidden" name="txtInvoqued" value="01">
<p>

<%
  out.println("<a id='btnCont' href=\"javascript:validate();\"><img src=\"../ventanilla/imagenes/b_aceptar.gif\" border=\"0\"></a>");
  if(!cTxn.equals("0578"))
  {
    out.println("<a href=\"javascript:cancel();\" ><img src=\"../ventanilla/imagenes/b_cancelar.gif\" border=\"0\"></a>");
  }
%>

</form>
<script>
function cancel()
{
    document.entry.action="../ventanilla/paginas/Final.jsp";
    document.entry.submit();
}
</script>
</body>
</html>
