<!--
//*************************************************************************************************
//             Funcion: JSP que despliega el menu de ventanilla
//            Elemento: Response0360.jsp
//          Creado por: Hugo Gabriel Rivas Cortes
//*************************************************************************************************
// CCN - 
//*************************************************************************************************
-->

<%@ page session="true" import="ventanilla.com.bital.sfb.Response" %>
<%@ page import="java.util.*, com.bital.util.Common" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<%
response.setHeader("Cache-Control","no-store"); //HTTP 1.1
response.setHeader("Pragma","no-cache"); //HTTP 1.0
response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
Hashtable datasession = (Hashtable)session.getAttribute("page.datasession");
%>

<% 
Vector resp = (Vector)session.getAttribute("response");
String estatus = (String)resp.elementAt(0);
%>
<script language="javascript">
<!--
  sharedData = '0';

  function formSubmitter(){
    clearInterval(formSubmitter);
    if(sharedData != '0')
    {
      document.forms[0].submit()
      sharedData = '0';
    }
  }
//-->
</script>


<%if (datasession.get("txnCurr").toString().equals("0360")){%>
<html>
<head>
    <title>Certificación de 0360</title>
    <style type="text/css">Body {font-family: Arial, Helv; font-size: 10pt; font-weight: bold; color:#000000}</style>
     
    <%@include file="style.jsf"%>
</head>


<%!
  private String unformatCur(String value)
  {
    if( value == null )
      return "000";

    StringBuffer monto = new StringBuffer(value);
    for(int i=0; i<monto.length();)
    {
      if( monto.charAt(i) == ',' )
        monto.deleteCharAt(i);
      else
        ++i;
    }

    return monto.toString();
  }

  private double parseNumber(String number)
  {
    double result;
    try
    {
      result = Double.parseDouble(number);
    }
    catch(NumberFormatException e)
    {
      result = 0.00;
    }

    return result;
  }

  private String calculaIVA(String comision, String iva)
  {
    double result = parseNumber(unformatCur(comision));
    result = result * parseNumber(unformatCur(iva))/100;

    return Common.formatCur(result);
  }

  private String calculaTotal(Hashtable req)
  {
    double result = parseNumber(unformatCur( (String)req.get("txtMonto") ));
    result = result + parseNumber(unformatCur( (String)req.get("txtComision") ));
    result = result + parseNumber(unformatCur( (String)req.get("txtIVA") ));
    return Common.formatCur(result);
  }
%>

<body bgcolor="#FFFFFF" onload="top.setFieldFocus(window.document);setInterval('formSubmitter()',2000)">
<div align="center">Certificación de 0360</div>
<p>
<%
  Response rsp = new Response((Vector)session.getAttribute("response"));

  if( !rsp.getCode().equals("0") )
  {
    out.println("<b>ERROR EN LA TRANSACCION</b><br>");

    String[] msg = rsp.getLines();
    for(int i=0; i<msg.length; i++)
      out.println(msg[i] + "<br>");
  }
  else
  {
    String line = rsp.getLine(0);
    Hashtable local = (Hashtable)session.getAttribute("page.datasession");  

    String com = null;
    String iva = null;
    String domsuc = null;
    try
    {
      com = line.substring(29, 40);
      iva = line.substring(37,40);

    }
    catch(StringIndexOutOfBoundsException e1)
    {
      if( com == null )
        com = "000";
        iva="000";     
    }

    com = Common.formatCur(parseNumber(com)/100);
    iva = Common.formatCur(parseNumber(iva)/100);
    

    line = rsp.getLine(1);
    domsuc = line.trim();
    
	//iva = "05";
	com = local.get("txtMonto").toString();
//    local.put("txtComision", com);
    local.put("txtIVA", calculaIVA(com, iva));
    local.put("txtDomEsta", domsuc);
    
%>
<table border="0" cellspacing="0">
<tr>
  <td><strong>Monto de Comision</strong></td>
  <td width="10">&nbsp;</td>
  <td align="right"><%= local.get("txtMonto") %></td>
</tr>
<tr>
  <td><strong>Tasa ITBMS</strong></td>
  <td>&nbsp;</td>
  <td align="right"><%= iva %>%</td>
</tr>
<tr>
  <td><strong>ITBMS</strong></td>
  <td>&nbsp;</td>
  <td align="right"><%= local.get("txtIVA") %></td>
</tr>

<tr>
  <td><strong>Total</strong></td>
  <td>&nbsp;</td>
  <td align="right"><%= calculaTotal(local) %></td>
</tr>
</table>
<%     } %>
<script>
function cancel()
{
    document.Txns.action="../paginas/Final.jsp";
    document.Txns.submit();
}
</script>


<form name="Txns" action="../../servlet/ventanilla.DataServlet" method="post">
<%
 Stack flujotxn = (Stack)session.getAttribute("page.flujotxn");
      //aqui debo de agregar el '0360' en el Stack flujotxn 
  session.setAttribute("idFlujo","00");
                  datasession.put("flujotxn",flujotxn);
    out.print("<input type=\"image\" src=\"../imagenes/b_continuar.gif\" alt=\"\" border=\"0\">");
    out.println("<a href=\"javascript:cancel()\" ><img src=\"../imagenes/b_cancelar.gif\" border=\"0\"></a>");
%>
<input type="hidden" name="transaction" value="<%= session.getAttribute("page.iTxn") %>">
<input type="hidden" name="transaction1" value="<%= session.getAttribute("page.oTxn") %>">
</form>
</body>
</html>

 <% } 
 else if (datasession.get("txnCurr").toString().equals("0372")){%>
 <td>0372</td>
<%
response.setHeader("Cache-Control","no-store"); //HTTP 1.1
response.setHeader("Pragma","no-cache"); //HTTP 1.0
response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
//Hashtable datasession = (Hashtable)session.getAttribute("page.datasession");
String creditoKronner = (String)session.getAttribute("creditoKronner");
String P002=null;
%>
<%!
  private String getString(String s)
  {
   int len = s.length();
   for(; len>0; len--)
    if(s.charAt(len-1) != ' ')
     break;
   return s.substring(0, len);
  }

  private String getItem(String newCadNum, int newOption)
  {
   int iPIndex = newCadNum.indexOf("CONSECUTIVO:") + 12, NumSaltos, Num = 0, Ban = 0;
   String nCadNum = new String("");
   NumSaltos = newOption;
   while(Num < NumSaltos)
   {
    if(newCadNum.charAt(iPIndex) == 0x20)
    {
     while(newCadNum.charAt(iPIndex) == 0x20)
      iPIndex++;
     Num++;
    }
    else
     while(newCadNum.charAt(iPIndex) != 0x20)
      iPIndex++;
   }
   while(newCadNum.charAt(iPIndex) != 0x20 && iPIndex < newCadNum.length()){
    nCadNum = nCadNum + newCadNum.charAt(iPIndex);
    if(iPIndex < newCadNum.length())
      iPIndex++;
    if(iPIndex == newCadNum.length())
      break;
   }

   return nCadNum;
  }
  
  private String getNombreSIBDDA(String newCadNum, int newOption)
  {
   int iPIndex = newCadNum.indexOf("CONSECUTIVO:") + 12, NumSaltos, Num = 0, Ban = 0;
   String nCadNum = new String("");
   NumSaltos = newOption;
   while(Num < NumSaltos)
   {
    if(newCadNum.charAt(iPIndex) == 0x20)
    {
     while(newCadNum.charAt(iPIndex) == 0x20)
      iPIndex++;
     Num++;
    }
    else
     while(newCadNum.charAt(iPIndex) != 0x20)
      iPIndex++;
   }
   nCadNum=newCadNum.substring(iPIndex,newCadNum.length());
   return nCadNum;
  }
  
    private String getNombreSIBCDA(String newCadNum, int newOption, String cta)
  {
   //System.out.println(newCadNum);
   int inicio = newCadNum.indexOf(cta) + 15;
   int fin = newCadNum.indexOf("MONEDA");
   String nCadNum = new String("");
   nCadNum = newCadNum.substring(inicio,fin);
   //System.out.println(nCadNum);
   return nCadNum.trim();
  }
  
  private String getProveedor(String ClaveProv)//SE OBTINENE EL NOMBRE DEL PROVEEDOR
  {
  	boolean find=false;
  	int i = 0;
  	String Proveedor = "";
    ventanilla.com.bital.beans.DatosVarios rap = ventanilla.com.bital.beans.DatosVarios.getInstance();
    Vector vecProvCHQ = new Vector();        
    vecProvCHQ =(Vector)rap.getDatosV("PROVCHQ");

  	while(!find && i<vecProvCHQ.size()){
  		StringTokenizer dato = new StringTokenizer(vecProvCHQ.elementAt(i).toString(),"*");

  		if(ClaveProv.equals(dato.nextToken().toString()))
  		{
  			find = true;
			Proveedor = dato.nextToken();
  		}
  		else
  		{
  			dato.nextToken();
  		}
  		i++;
  		dato=null;
  	}	
  	return(Proveedor);
  	
  }
  

%>
<html>
<head>
  <title>Certificación</title>
<style type="text/css">Body {font-family: Arial, Helv; font-size: 10pt; font-weight: bold; color:#000000}</style>
<script language="javascript">
<!--
  sharedData = '0';

  function formSubmitter(){
    clearInterval(formSubmitter);
    if(sharedData != '0')
    {
      document.forms[0].submit()
      sharedData = '0';
    }
  }
//-->
</script>
</head>
<body bgcolor="#FFFFFF" onload="top.setFieldFocus(window.document);setInterval('formSubmitter()',2000)">
<p>
<%
  java.util.Stack flujotxn = new java.util.Stack();
  String codigo = "";
  String needOverride = "NO";
  String txn = session.getAttribute("page.cTxn").toString();
 
  if(session.getAttribute("page.flujotxn") != null)
  	flujotxn = (java.util.Stack)session.getAttribute("page.flujotxn");
  	   
   	try
   	{
		codigo = (String)resp.elementAt(0);
	 	if( codigo.equals("1"))
	   	   out.println("<b>Transaccion Rechazada</b><br><br>");
	    if( codigo.equals("0") || codigo.equals("1") ){
	    	int lines = Integer.parseInt((String)resp.elementAt(1));
	    	String szResp = (String)resp.elementAt(3);

	    	for(int i=0; i<lines; i++){
	    		   String line = szResp.substring( i*78, Math.min(i*78+77, szResp.length()) );
	     		   out.println(getString(line) + "<p>");
	     		   if(Math.min(i*78+77, szResp.length()) == szResp.length())
	      		    break;
	    	}
		}
	    else{
	    	if ( codigo.equals("3") || codigo.equals("2"))
	        	out.println("<b>Transacci&oacute;n Requiere Autorizaci&oacute;n</b>");
	        else
	            out.println("<b>Ocurri&oacute; un Error de Comunicaci&oacute;n, Verifique.</b>");
		}
  	}
  	catch(NumberFormatException nfe){
     out.println("<b>Error de conexi&oacute;n</b><br>");
    }
  
%>
<p>
<form name="Txns" action="../../servlet/ventanilla.PageServlet" method="post">
<table border="0" cellspacing="0">
<tr>
<td>
</td>
</tr>
<tr>
<td>
<%
  String isCertificable = "";
  if(session.getAttribute("page.certifField") != null)
    isCertificable = (String)session.getAttribute("page.certifField");  // Para Certificación
	
	String Suc = session.getAttribute("branch").toString();

  String txtCadImpresion  = ""; // Para Certificación


  if(!flujotxn.empty())
  {
    out.print("<div align=\"center\">");
    
    if(!isCertificable.equals("N")) // Para Certificación
    {
      session.setAttribute("Consecutivo", getItem((String)resp.elementAt(3), 1)); // Para Certificación
      session.setAttribute("Tipo", isCertificable); // Para Certificación
      
      if(codigo.equals("0"))
        out.println("<script language=\"javascript\">top.openDialog('../../servlet/ventanilla.ImprimirServlet?txtCadImpresion=" + txtCadImpresion + "', 160, 120, 'top.setPrefs4()', document.Txns)</script>"); // Para Certificación
      else
      {
        if ( codigo.equals("3") || codigo.equals("2")) 
        {
	  needOverride = "SI";
	  out.print("<script languaje=\"javascript\"> top.openDialog('authoriz.jsp', 220, 130, 'top.setPrefs8()', document.Txns) </script>");
	}
	else
	  out.println("<a href=\"javascript:top.openDialog('../../servlet/ventanilla.ImprimirServlet?txtCadImpresion=" + txtCadImpresion + "', 160, 120, 'top.setPrefs4()', document.Txns)\"><img src=\"imagenes/b_continuar.gif\" border=\"0\"></a>"); // Para Certificación
      }
    }
    else
    {
      if(codigo.equals("0"))
      {
	out.println("<script language=\"javascript\">sharedData = '1'</script>");
	out.println("<font color='red'>procesando...</font>");
      }
      else 
      {
        if ( codigo.equals("3") || codigo.equals("2")) 
        {
          needOverride = "SI";
          out.print("<script languaje=\"javascript\"> top.openDialog('authoriz.jsp', 220, 130, 'top.setPrefs8()', document.Txns) </script>");
        }
        else
      	  out.print("<input type=\"image\" src=\"../imagenes/b_aceptar.gif\" alt=\"\" border=\"0\">");
      }
    }
    
    out.print("</div>");
  }
  else
  {
    if(!isCertificable.equals("N")) // Para Certificación
    {

      session.setAttribute("Consecutivo", getItem((String)resp.elementAt(3), 1)); // Para Certificación
      session.setAttribute("Tipo", isCertificable); // Para Certificación
      
      if ( codigo.equals("3") || codigo.equals("2")) 
      {

	needOverride = "SI";
	out.print("<script languaje=\"javascript\"> top.openDialog('authoriz.jsp', 220, 130, 'top.setPrefs8()', document.Txns) </script>");
      }
      else
      {
	if (txn.equals("0096") && session.getAttribute("page.iTxn").toString().equals("4009") && codigo.equals("1")) //Transaccion 4009
        {          			
          out.println("<script language=\"javascript\">alert('Transacción Rechazada.')</script>");
        }  
        //~ITBMS~Txn~Mto Comision~Mto ITBMS~Mto Total~Descripcion~        
		ventanilla.GenericClasses gc = new ventanilla.GenericClasses();
		String montoCom=datasession.get("txtMonto").toString();

		montoCom = gc.quitap(montoCom);
		String montoImp=datasession.get("txtIVA").toString();
		montoImp = gc.quitap(montoImp);
		int total= Integer.valueOf(montoCom).intValue()+Integer.valueOf(montoImp).intValue();
		String strTotal = String.valueOf(total);
		strTotal = gc.quitap(strTotal);
		//System.out.println("referencia"+datasession.get("txtReferencia"));
		txtCadImpresion="~ITBMS~0360*0372*~"+montoCom+"~"+montoImp+"~"+strTotal+"~"+datasession.get("lstTipoComision")+"~"+datasession.get("txtReferencia")+"~";
        out.println("<script language=\"javascript\">top.openDialog('../../servlet/ventanilla.ImprimirServlet?txtCadImpresion=" + txtCadImpresion + "', 160, 120, 'top.setPrefs4()', document.Txns)</script>"); // Para Certificación
        
      }
    }
  }
%>
  <input type="hidden" name="transaction" value="<%= session.getAttribute("page.iTxn") %>">
  <input type="hidden" name="transaction1" value="<%= session.getAttribute("page.oTxn") %>">
  <input type="hidden" name="supervisor" value="<%= session.getAttribute("supervisor") %>">
  <input type="hidden" name="override" value="<%= needOverride%>">
</td>
</tr>
</table>
</form>
</body>
</html>



 <%}%>
 