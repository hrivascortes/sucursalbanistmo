<!--
//*************************************************************************************************
//             Funcion: JSP que despliega respuesta de Totales Host
//            Elemento: PageBuilderGerente.jsp
//          Creado por: Alejandro Gonzalez Castro
//      Modificado por: Alejandro Gonzalez Castro
//*************************************************************************************************
// CCN - 4360211 - 09/10/2004 - Se ajusta presentación de totales
//*************************************************************************************************
-->

<%@page %>
<html>
<head>
   <title>PageBuilder Gerente</title>
</head>
<body onload='process(document.gerente)'>
<script language='JavaScript'>
function process(f)
{
  f.submit();
}
</script>
<form name="gerente" action="../../servlet/ventanilla.PageServlet" method="post">
    <input type="hidden" name="tllrlvl" value="<%=session.getAttribute("tellerlevel")%>">
    <input type="hidden" name="transaction" value="M005">
    <input type="hidden" name="transaction1" value="0013">
    <input type="hidden" name="moneda" value="01">
</form>
<%session.setAttribute("txnProcess","16");%>
</body>
</html>
