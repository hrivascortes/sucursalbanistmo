<%@ page session="true" %>
<html>
 <head>
  <title>Calculadora</title>
 </head>
<%
  String dayNumber = "" + java.util.Calendar.getInstance().get(java.util.Calendar.DAY_OF_WEEK);
  while( dayNumber.length() < 2 )
    dayNumber = "0" + dayNumber;
%>
 <body onLoad="if (top.opener) top.opener.blockEvents()" onUnload="if (top.opener) top.opener.unblockEvents()" TOPMARGIN="0" LEFTMARGIN="0" MARGINWIDTH="0" MARGINHEIGHT="0">
 <embed type="application/x-java-applet;version=1.3"
  numUsuario="<%out.print(session.getAttribute("teller").toString() + dayNumber);%>"
  opcion="1"
  java_ARCHIVE="calculadora.jar"
  java_CODE="Calculadora.class"
  java_CODEBASE="../"
  width="470"
  height="295"
  scriptable="false"
  pluginspage="http://java.sun.com/products/plugin/1.3/plugin-install.html">
  <noembed>
   alt="No es posible cargar la aplicaci&oacute;n."
  </noembed>
 </embed>
</body>
</html>
