<!--
//*************************************************************************************************
//             Funcion: JSP que despliega respuesta de Totales Host
//            Elemento: TotalesPCentral.jsp
//          Creado por: Alejandro Gonzalez Castro
//      Modificado por: Juvenal R. Fernandez V.
//*************************************************************************************************
// CCN - 4360175 - 03/08/2004 - Se eliminan displays a log / se ajusta size ventana impresion
// CCN - 4360211 - 09/10/2004 - Se ajusta presentaci�n de totales
// CCN - 4360275 - 18/02/2005 - WAS 5.1
// CCN - 4360334 - 17/06/2005 - Cambio de importacion de clase necesaria para WAS 5.1
// CCN - 4360581 - 12/03/2007 - Se convierten los estilos en jsf y se hace un include en el jsp
//*************************************************************************************************
-->

<%@ page session="true" import="java.util.*" errorPage="/ventanilla/paginas/error.jsp"%>
<%
response.setHeader("Cache-Control","no-store"); //HTTP 1.1
response.setHeader("Pragma","no-cache"); //HTTP 1.0
response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
%>
<%
    Vector resp = (Vector)session.getAttribute("response");
%>

<html>
<head>
<title>Totales del procesador central</title>
<%@include file="style.jsf"%>
</head>
<script language="JavaScript">
function cancel()
{
    document.formTotalesPC.action="../paginas/Final.jsp";
    document.formTotalesPC.submit();
}
</script>
<body>
<%
    ventanilla.com.bital.admin.TotalesPCentral TotalesPC = new ventanilla.com.bital.admin.TotalesPCentral();
    TotalesPC.setNoLineasTxn((String)resp.elementAt(3));
    TotalesPC.setRespuestaTxn((String)resp.elementAt(3));
   if( resp.size() != 4 ){%>
    <b>Error de conexi&oacute;n</b><br>
   <%}else
   {
    %>
    <h1>Totales</h1>
    <table border='0' align="center" cellspacing="1" cellpading="1" width='100%'>
       <tr>
        <td>
         <table border='0' align='center'>
           <tr bgcolor="#CCCCCC">
             <td colspan="4" align="center"><b>D � B I T O S</b></td>
           </tr>
           <tr bgcolor="#CCCCCC">
             <td ALIGN="center"><b>Total</b></td>
             <td ALIGN="center"><b>Descripci&oacute;n</b></td>
             <td ALIGN="center"><b>Docs</b></td>
             <td ALIGN="center"><b>Importes</b></td>
           </tr>
    <%
        int noCargos = TotalesPC.getNoCargos() - 1;
        int noAbonos = TotalesPC.getNoAbonos() - 1;
        int extrac = 0;
        int extraa = 0;
        if (noCargos > noAbonos)
            extraa = noCargos - noAbonos;
        else
            extrac = noAbonos - noCargos;

        for(int i=0; i < noCargos; i++)
        {%>
            <tr>
                <td align="center"><%=TotalesPC.getNoTotalCargos(i)%></td>
       <%
       			if(TotalesPC.getDescripCargos(i).toString().equals("Cobro inmediato"))
       				out.println("<td align='left'>Cheques Locales</td>");
       			else
       				out.println("<td align='left'>"+TotalesPC.getDescripCargos(i)+"</td>");
       %>
                <td align='center'><%=TotalesPC.getDocsCargos(i)%></td>
                <td align='right'><%=TotalesPC.getImptsCargos(i)%></td>
            </tr>                 
       <%}
         while(extrac > 0)
         {%>
            <tr>
               <td colspan='4'>&nbsp;</td>
            </tr>    
         <%
           extrac--;
         }%>   
             <tr bgcolor='#CCCCCC'>
                <td align='center'><b><%=TotalesPC.getNoTotalCargos(noCargos)%></b></td>
         <%
         	   if(TotalesPC.getDescripCargos(noCargos).toString().equals("Total cargos"))
       				out.println("<td align='left'><b>Total d�bitos</b></td>");
       		   else
       				out.println("<td align='left'><b>"+TotalesPC.getDescripCargos(noCargos)+"</b></td>");
         %>
                <td align='center'><b><%=TotalesPC.getDocsCargos(noCargos)%></b></td>
                <td align='right'><b><%=TotalesPC.getImptsCargos(noCargos)%></b></td>
            </tr>
           </table>
          </td> 
          <td>&nbsp;</td> 
          <td>
           <table border='0' align='center'>
            <tr bgcolor="#CCCCCC">
              <td colspan="4" align="center"><b>C R � D I T O S</b></td>
            </tr>
            <tr  bgcolor="#CCCCCC">
              <td ALIGN="center"><b>Total</b></td>
              <td ALIGN="center"><b>Descripci&oacute;n</b></td>
              <td ALIGN="center"><b>Docs</b></td>
              <td ALIGN="center"><b>Importes</b></td>
            </tr>  
    <%
         for(int i=0; i < noAbonos; i++)
         {%>
             <tr>  
               <td align='center'><%=TotalesPC.getNoTotalAbonos(i)%></td>
               <td align='left'><%=TotalesPC.getDescripAbonos(i)%></td>
               <td align='center'><%=TotalesPC.getDocsAbonos(i)%></td>
               <td align='right'><%=TotalesPC.getImptsAbonos(i)%></td>
             </tr>
    <%  }
        while(extraa > 0)
        {%>
              <tr>
                 <td colspan='4'>&nbsp;</td>
              </tr>    
        <%
          extraa--;
        }%> 
          <tr bgcolor='#CCCCCC'>
            <td align='center'><b><%=TotalesPC.getNoTotalAbonos(noAbonos)%></b></td>
         <%
         	   if(TotalesPC.getDescripAbonos(noAbonos).toString().equals("Total abonos"))
       				out.println("<td align='left'><b>Total cr�ditos</b></td>");
       		   else
       				out.println("<td align='left'><b>"+TotalesPC.getDescripAbonos(noAbonos)+"</b></td>");
         %>
            <td align='center'><b><%=TotalesPC.getDocsAbonos(noAbonos)%></b></td>
            <td align='right'><b><%=TotalesPC.getImptsAbonos(noAbonos)%></b></td>
          </tr>
<%  }  
    String cadenaImpresion= "";
    cadenaImpresion = TotalesPC.formatStringPrinter();
    session.setAttribute("txtCadImpresion", cadenaImpresion);
%>
       </table>
     </td>
    </tr>
  </table>
<br>
     <table align="center" border=0 cellspacing="0" cellpading="1" width='20%'>
        <tr>
            <td align='center'><a href="JavaScript:cancel()"><img src="../imagenes/b_cancelar.gif" border=0 alt="Cancelar"></a></td>
            <td align='center'><a href="javascript:top.openDialog('../../servlet/ventanilla.ImprimirServlet', 160, 120, 'top.setPrefs4()',  document.formTotalesPC)"><img src="../imagenes/b_imprimir.gif" Alt="Imprimir totales procesador central" border="0"></a></td>
        </tr>
     </table>
     <form name="formTotalesPC" method="post">
        <input name="txtCadImpresion" type="hidden" value="<%=java.net.URLEncoder.encode(cadenaImpresion)%>">
     </form>
</body>
</html>
