<!--
//*************************************************************************************************
//             Funcion: JSP que presenta las opciones de la txn Digitalizacion
//            Elemento: PageBuilderDig.jsp
//          Creado por: Alejandro Gonzalez Castro
//      Modificado por: Juvenal R. Fernandez V.
//*************************************************************************************************
// CCN - 4360334 - 17/06/2005 - Se elimina variable dbQualifier
// CCN - 4360581 - 12/03/2007 - Se convierten los estilos en jsf y se hace un include en el jsp
//*************************************************************************************************
-->

<%@ page session="true" import="ventanilla.com.bital.admin.OperacionesDBBean"%>
<%
response.setHeader("Cache-Control","no-store"); //HTTP 1.1
response.setHeader("Pragma","no-cache"); //HTTP 1.0
response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
	<head>
		<%@include file="style.jsf"%>
		<title>Proceso de Digitalizacion</title>
	</head>
	<Script language="JavaScript">
		var valor = "1"
	
		function sendservlet()
		{	
                        if (valor == 1)
                            document.entry.action = "../ventanilla/paginas/PageBuilderCD1.jsp"
                        if ((valor == 2) || (valor==5))
                            document.entry.action = "../servlet/ventanilla.DigitalizarServlet"
                        if (valor == 4)
                            document.entry.action = "../ventanilla/paginas/PageBuilderCD2.jsp"
                        if (valor == 3)
                            document.entry.action = "../ventanilla/paginas/PageBuilderCD3.jsp"
			document.entry.submit();
		}

		function setValues1()
		{
	 		valor = document.entry.opcion1.options[document.entry.opcion1.selectedIndex].value
		}
		
   </SCRIPT>	
	<body bgcolor="#FFFFFF" onload="top.setFieldFocus(window.document)">   
		<form name="entry">
			<input type="hidden" name="cTxn" value="<%= session.getAttribute("page.cTxn") %>">
			<input type="hidden" name="moneda" value="<%= session.getAttribute("page.moneda")%>">
			<table border="0" cellspacing="0">       			
			<b><h3>&nbsp</b></h3>
<%						
			out.println("<b><h3>");
			out.print(session.getAttribute("page.cTxn") + "  " + (String)session.getAttribute("page.txnlabel"));
			out.println("</h3></b>");
			out.println("<tr><td>");		
			out.println("<br>");		
			out.println("</td></tr>");
			String statementPS = com.bital.util.ConnectionPoolingManager.getStatementPostFix();
			OperacionesDBBean rbQuery = new OperacionesDBBean();
			String strCajero = (String) session.getAttribute("teller");
			String strAccion = "SELECT N_NIV_DIGITALIZ FROM TC_USUARIOS_SUC WHERE C_CAJERO ='" + strCajero + "'" + statementPS;
			String strRespuesta = rbQuery.EjecutaQuery(strAccion);
			strRespuesta = strRespuesta.substring(0,1);
			if (strRespuesta.substring(0,1).equals("S"))
			{
                            out.println("<tr>");
                            out.println("	<td><b>Seleccione : </b></td>");
                            out.println("	<td>");
                            out.println("		<SELECT name=opcion1 size=1 onChange=\"setValues1()\">");
                            out.println("			<OPTION value=1 selected>Entrega de Documentacion</OPTION>");
                            out.println("			<OPTION value=2>Digitalizacion</OPTION>");
                            out.println("			<OPTION value=3>Reportes</OPTION>");
                            out.println("			<OPTION value=4>Contigencia</OPTION>");
                            out.println("			<OPTION value=5>Preparar Envio</OPTION>");
                            out.println("		</SELECT>");
                            out.println("	</td>");
                            out.println("</tr>");				
			}			
			else
			{
                            out.println("<tr>");
                            out.println("	<td><b>Seleccione : </b></td>");
                            out.println("	<td>");
                            out.println("		<SELECT name=opcion1 size=1 onChange=\"setValues1()\">");
                            out.println("			<OPTION value=1 selected>Entrega de Documentacion</OPTION>");
                            out.println("		</SELECT>");
                            out.println("	</td>");
                            out.println("</tr>");							
			}
			out.println("<tr><td>");		
			out.println("<br>");		
			out.println("</td></tr>");		
			out.println("<tr>");		
                        out.println("	<td>");
			out.print("<a href=\"JavaScript:sendservlet()\"><img src=\"../ventanilla/imagenes/b_aceptar.gif\" border=\"0\"></a>");
			out.println("	</td>");			
			out.println("</tr>");
%>			
		</table>			
		</form>
	</body>
</html>
