<!--
//*************************************************************************************************
//            Elemento: PageBuilder5361.jsp
//             Funcion: JSP de para cheques Devueltos. 
//          Creado por: Israel de Paz Mercado
//          Modificado: Fausto Rodrigo Flores Moreno
//*************************************************************************************************
// CCN - 4360455 - 07/04/2006 - Se crea PageBuilder para Cheques Devueltos.
// CCN - 4360462 - 10/04/2006 - Se genera nuevo paquete por errores en paquete anterior
// CCN - 4360474 - 05/05/2006 - Se realizan modificaciones para mostrar mensaje de espera cuando se procesan devoluciones de cheques
// CCN - 4360581 - 12/03/2007 - Se convierten los estilos en jsf y se hace un include en el jsp
//*************************************************************************************************
-->


<%@ page session="true" import="java.util.*"  %>

<%
response.setHeader("Cache-Control","no-store"); //HTTP 1.1
response.setHeader("Pragma","no-cache"); //HTTP 1.0
response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<%
String Monto = new String("");
String txnAuthLevel = (String)session.getAttribute("page.txnAuthLevel");
String txnIMGAutoriz = (String)session.getAttribute("page.txnIMGAutoriz");
Vector listaCampos = (Vector)session.getAttribute("page.listcampos");
String TxnE = (String)session.getAttribute("page.iTxn");

Hashtable datasession = (Hashtable)session.getAttribute("page.datasession");
if(datasession == null)
  datasession = new Hashtable();
%>

<html>
<head>

<style type="text/css">Body {font-family: Arial, Helv; font-size: 10pt; font-weight: bold; color:#000000}</style>

<script language="JavaScript">

function submite()
{
document.forms[0].submit();
}

</script>
</head>
<body bgcolor="#FFFFFF" onload="submite()">
<br>
<% 
    out.println("<b>Procesando Devolución de Cheques, por favor espere...</b><br>");

	String total = (String)session.getAttribute("totalChqDev");
	String conteo = (String)session.getAttribute("conteoChqDev");
	if(conteo!=null){
       if(conteo.equals(total)){
     	 out.println("<br><br>Procesando Devolución : " + conteo + " de " + total);
     	 out.println("<br><br><br>Proceso terminado...");
       }else{	   	
	   	 out.println("<br><br>Procesando Devolución : " + conteo + " de " + total);  
      }
	  }

out.print("<form name=\"entry\" action=\"../../servlet/ventanilla.DataServlet\" method=\"post\" ");
session.setAttribute("page.getoTxnView","Response5361");
%>
<input type="hidden" name="iTxn" value="5361">
<input type="hidden" name="oTxn" value="5361">
<input type="hidden" name="cTxn" value="5361">
<input type="hidden" name="sucursal" value="<%= session.getAttribute("branch") %>">
<input type="hidden" name="teller" value="<%= session.getAttribute("teller") %>">
<input type="hidden" name="moneda" value="<%= session.getAttribute("page.moneda")%>">
<input type="hidden" name="registro" value="<%=session.getAttribute("empno")%>">
<input type="hidden" name="tllrlvl" value="<%=session.getAttribute("tellerlevel")%>">
<input type="hidden" name="supervisor" value="<%= session.getAttribute("teller") %>">
<input type="hidden" name="AuthoOK" value="0">
<input type="hidden" name="SignOK" value="0">
<input type="hidden" name="validateFLD" value="0">
<input type="hidden" name="needSignature" value="<%out.print(txnIMGAutoriz);%>">
<input type="hidden" name="needAutho" value="<%out.print(txnAuthLevel);%>">
<input type="hidden" name="SignField" value="<%= session.getAttribute("page.SignField") %>">
<input type="hidden" name="override" value="N">
<input type="hidden" name="txtFees" value="0">
<input type="hidden" name="compania" value="20">
<input type="hidden" name="txtSelectedItem" value="0" >
<input type="hidden" name="returnform" value="N" >
<input type="hidden" name="returnform2" value="N" >
</form>     
</body>
</html>
