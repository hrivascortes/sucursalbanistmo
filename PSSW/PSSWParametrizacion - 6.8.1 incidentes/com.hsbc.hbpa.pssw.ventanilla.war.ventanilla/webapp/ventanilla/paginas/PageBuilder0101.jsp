<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c"%>
<!--
//*************************************************************************************************
//             Funcion: JSP para despliege de txns varias
//            Elemento: PageBuilder0101.jsp 
//      Modificado por: Jesus Emmanuel Lopez Rosales
//*************************************************************************************************
// CCN - 4360275 - 18/02/2005 - WAS 5.1
// CCN - 4360334 - 17/06/2005 - Se convierte fieldLector a fragmento
// CCN - 4360390 - 07/10/2005 - Se agrega validacion para acdo 1053.
// CCN - 4360443 - 24/02/2006 - Se realizan modificaciones para pedir autorización para el depto 9010
// CCN - 4360477 - 19/05/2006 - Se agrega validación para txn 5405 en tipo de identifiación cuando es númerico
// CCN - 4360533 - 20/10/2006 - Se realizan modificaciones para OPEE
// CCN - 4360572 - 23/02/2007 - Se realizan modificacion en validación de monto de txn 5357 y bandera de autoriz remota en txn 0041
// CCN - 4360581 - 12/03/2007 - Se convierten los estilos en jsf y se hace un include en el jsp
// CCN - 4360584 - 23/03/2007 - Modificaciones para proyecto OPMN
// CCN - 4360619 - 15/06/2007 - Se eliminan systems
// CCN - 4620018 - 12/10/2007 - Se valida monto 
// CCN - 4620021 - 17/10/2007 - Se agregan validaciones para montos mayores a 10000 dlls, Humberto Balleza
//*************************************************************************************************
-->

<%@ page session="true"
	import="java.util.*,
	java.util.Vector,
	ventanilla.GenericClasses,
	com.bital.util.Comunica,java.sql.*,
	com.bital.util.ConnectionPoolingManager,
	ventanilla.com.bital.util.PageBean"
%>

<%
response.setHeader("Cache-Control","no-store"); //HTTP 1.1
response.setHeader("Pragma","no-cache"); //HTTP 1.0
response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<%
String MontoPro = new String("");
ventanilla.GenericClasses gc = new ventanilla.GenericClasses();
Vector listaCampos = (Vector)session.getAttribute("page.listcampos");
Vector listaContenidos = (Vector)session.getAttribute("page.listcont");

//Vector listaTipochequera = (Vector)session.getAttribute("page.lstTipoCheq");
Vector listaModelo = (Vector)session.getAttribute("page.lstModeloCh");
Vector listaColor = (Vector)session.getAttribute("page.lstColorCh");
Vector listaNoCh = (Vector)session.getAttribute("page.lstNoChqs");


String txnAuthLevel = (String)session.getAttribute("page.txnAuthLevel");
String txnIMGAutoriz = (String)session.getAttribute("page.txnIMGAutoriz");
Vector v = (Vector)session.getAttribute("page.specialacct");
Hashtable datasession = (Hashtable)session.getAttribute("page.datasession");
String txn = session.getAttribute("page.cTxn").toString();
String inTxn =session.getAttribute("page.iTxn").toString();
String ctaRepago=null;
String montoKronner=null;
String P002=null;

	String sql = null;
	String PostFixUR = ConnectionPoolingManager.getPostFixUR();
	Connection pooledConnection = null;
	PreparedStatement stmt = null;
  
    pooledConnection = ConnectionPoolingManager.getPooledConnection();
    ResultSet query = null;

if (datasession == null)
   datasession = new Hashtable();
   
int k = 0;
int i =0 ;
int y=0 ;
int q=0 ;
int x=0 ;
int w=0 ;


if(inTxn.equals("0500"))
	session.setAttribute("idFlujo","01");


%>

<html>
<head>
<title>Datos de la Transaccion</title>
<%@include file="style.jsf"%> 
<script language="javascript" type="text/javascript" src="../ventanilla/js/jquery.js"></script>
<script type="text/javascript">
  $(document).ready(function(){
     disabledObject(document.getElementById("lstTipoCheq"),true);
     disabledObject(document.getElementById("lstModeloCh"),true);
     disabledObject(document.getElementById("lstColorCh"),true);
     disabledObject(document.getElementById("lstNoChqs"),true);
    $("#lstTipoCheq").change(function(){
        var seleccionado =  $(this).find(':selected').attr('value');
        var lstNumeros =  ["01", "02", "03", "04", "05"];
        var lstNoCheques = ["250","300","350","500","750","1250","1500","2000","3000","5000","10000"];
        
        if(seleccionado=='52'){
           enabledObject(document.getElementById("lstModeloCh"));
           enabledObject(document.getElementById("lstColorCh"));
           cargarLista(document.getElementById("lstModeloCh"),lstNumeros,1,5);
           cargarLista(document.getElementById("lstColorCh"),lstNumeros,1,4);
           cargarLista(document.getElementById("lstNoChqs"),lstNoCheques,1,lstNoCheques.length);
           
        }else if(seleccionado=='53'){
           enabledObject(document.getElementById("lstModeloCh"));
           enabledObject(document.getElementById("lstColorCh"));
           cargarLista(document.getElementById("lstModeloCh"),lstNumeros,1,5);
           cargarLista(document.getElementById("lstColorCh"),lstNumeros,1,1);
           cargarLista(document.getElementById("lstNoChqs"),lstNoCheques,1,lstNoCheques.length);
        
        }else if(seleccionado=='51'){
           disabledObject(document.getElementById("lstModeloCh"),true);
           disabledObject(document.getElementById("lstColorCh"),true);
           setListaMultiplo(300,900);
           
        }else if(seleccionado == '57'){
        	disabledObject(document.getElementById("lstColorCh"),true);
        	enabledObject(document.getElementById("lstModeloCh"));
        	cargarLista(document.getElementById("lstNoChqs"),lstNoCheques,1,lstNoCheques.length);
        	cargarLista(document.getElementById("lstModeloCh"),lstNumeros,1,4);
            
        }else if(seleccionado=='54' || seleccionado == '55' || seleccionado == '56'){
           disabledObject(document.getElementById("lstModeloCh"),true);
           enabledObject(document.getElementById("lstColorCh"));
           cargarLista(document.getElementById("lstColorCh"),lstNumeros,2,4);
           cargarLista(document.getElementById("lstNoChqs"),lstNoCheques,1,lstNoCheques.length);
       }
     });
    });
    
    
function cargarLista(objeto,listaObject,start,end){

   objeto.options.length=0;
   var conti = 0;
   for(var d=start;  d<=end ;d++){
       objeto.options[conti]= new Option(listaObject[d-1],listaObject[d-1]);
       conti = conti + 1;
   }
   

}

function disabledObject(objeto,disable){
  
   objeto.disabled=disable;
   objeto.options.length=0;
   objeto.options[0]= new Option("","");

}

function enabledObject(objeto){

   objeto.disabled=false;


}
    
</script>
<script type="text/javascript">



<!--

var formName = 'Mio'
var specialacct = new Array()
var bandera = 0;
<%//Asignar valor de idflujo para flujos internos
  
  
%>
<%
 if(!v.isEmpty()){
  for(int iv = 0; iv < v.size(); iv++){
   out.print("specialacct[" + iv + "]='" + v.get(iv) + "'" + '\n');
  }
 }
%>
function validate(form)
{
   
   document.entry.fecha4523.value=top.sysdatef();
   form.validateFLD.value = '0';
   inputStr = '';

   if (!form.ChequeraP.checked && !form.ChequeraC.checked) {
	   alert("Seleccione el Tipo de chequera (Personal o Comercial).");
       return;
   }

//CAMBIOSNOMBRECORTO: Se eliminan validaciones innecesarias

 var suc ='<%= (String)session.getAttribute("branch")%>'
 
 document.forms[0].submit();

}
function AuthoandSignature(entry)
{
 var Cuenta;
 var CveTransito;
 if(entry.returnform != undefined && entry.txtFolio != undefined)
   if(entry.txtFolio.value.length == 0)
     return;

 if(entry.validateFLD.value != '1')
  return

}


function Alltrim(inputStr)
{
  var inStr = new String(inputStr)

  while(inStr.charAt(0) == ' ')
    inStr = inStr.substring(1, inStr.length)

  while(inStr.charAt(inStr.length) == ' ')
    inStr = inStr.substring(0, inStr.length-1)

  return inStr;
}


function comercial(cod){


	var lstModeloCh = document.getElementById("lstModeloCh");
	
	var lstColorCh = document.getElementById("lstColorCh");
	
	var cont1 = 0;
	var tipoCheques= new Array();
	document.getElementById("lstTipoCheq").options.length=0;
    var l=0;
                
	  <c:forEach items="${lstTipoCheques}" var="tipoCheques">
	    var tipCh = '<c:out value="${tipoCheques}"/>';
	    var dato =  tipCh.split('*');
	     tipoCheques.push(new infoLista(eval(dato[1]),dato[0]));
	  </c:forEach>
	
	  for(var m=0; m < tipoCheques.length; m++){
	   
	     if(tipoCheques[m].id > cod){
	        document.getElementById("lstTipoCheq").options[l]= new Option(tipoCheques[m].descripcion,tipoCheques[m].id);
	        l=l+1;
	     }
	     
	  }
//CAMBIOSNOMBRECORTO: Se cambia los valores para 3 en 1
	  setListaMultiplo(300,900);
	  
	
       enabledObject(document.getElementById("lstTipoCheq"));
       enabledObject(document.getElementById("lstNoChqs"));
           
	
   }
	
	function infoLista(a,b){
       this.id= a;
       this.descripcion=b;  
       
    } 
    
  function multiple(valor, multiple){
	resto = valor % multiple;
	if(resto==0)
    	return true;
    else
		return false;
   }
   
   function setListaMultiplo(start,end){
   
        var cont3 = 0;
        document.getElementById("lstNoChqs").options.length=0;	
       
        var multiplos = getListMultiplos(start, end);
        
        for(var d=0; d < (end/start); d++){	
			var val = multiplos[d];
        	document.getElementById("lstNoChqs").options[cont3]=new Option(val, val); 
        	cont3 = cont3 + 1;
       	}
       	
   }

   function getListMultiplos(start,end){
	   
       var multiplos = new Array();
      
       for (var s=start; s<=end; s=s+start){
           if(multiple(s,start))
               multiplos.push(s); 
       }

       return multiplos;
   }

  
 function personal(cod){


    disabledObject(document.getElementById("lstModeloCh"),true);
    disabledObject(document.getElementById("lstColorCh"),true);
    
	var cont1 = 0;
	var tipoCheques= new Array();
	 document.getElementById("lstTipoCheq").options.length=0;
     var l=0;
     
	 <c:forEach items="${lstTipoCheques}" var="tipoCheques">
	    var tipCh = '<c:out value="${tipoCheques}"/>';
	    var dato =  tipCh.split('*');
	    tipoCheques.push(new infoLista(eval(dato[1]),dato[0]));
     </c:forEach>
	
	 for(var m=0; m < tipoCheques.length; m++){
	   
	    if(tipoCheques[m].id < cod){
	        document.getElementById("lstTipoCheq").options[l]= new Option(tipoCheques[m].descripcion,tipoCheques[m].id);
	        l=l+1;
	     }
      }
      
          setListaMultiplo(50,500);
      
          enabledObject(document.getElementById("lstTipoCheq"));
          enabledObject(document.getElementById("lstNoChqs"));

	
}
    
//-->
</script>
</head>
<body bgcolor="#FFFFFF" >
	<br>
	<% out.print("<form name=\"entry\" action=\"../servlet/ventanilla.DataServlet\" method=\"post\" >");
	  
 
%>
	<h1><%=session.getAttribute("page.txnImage")%></h1>
	<h3>
		<%




out.print(session.getAttribute("page.cTxn") + "  " + (String)session.getAttribute("page.txnlabel"));%>
	</h3>
	<table border="0" cellspacing="0" class="normaltextblack">
		<tr>
			<td>Tipo de chequera :</td>
		
		
			<td></td>
			<td>Chequera Personal <input type="radio" name="status"
 			id="ChequeraP" onclick="personal(50);"  /> 
			
			<td>Chequera Comercial: <input type="radio" name="status"
				id="ChequeraC" onclick="comercial(50);"  />
			<td></td> 
		</tr>
		</table>
	
	<table border="0" cellspacing="10" class="normaltextblack">

	<% 

	  for(i =  0; i < listaCampos.size(); i++)
	   {
	
	    Vector vCampos = (Vector)listaCampos.get(i);
	    vCampos = (Vector)vCampos.get(0);
	    if(!vCampos.get(0).toString().equals("txtCodSeg")){
	     	out.println("<tr>");
   			if (vCampos.get(0).equals("txtDDACuenta") && (txn.equals("0130") || txn.equals("0128"))){
	         	out.println("  <td>Número de Cuenta Abono:</td>");   
	        }
	        else 
	        	if(vCampos.get(0).equals("txtRefTIRE") && (txn.equals("2199") || txn.equals("4523"))){
        			out.println("  <td>Referencia:</td>");
	     		}else {
	         		out.println("  <td>" + vCampos.get(2) + ":</td>");
				}	 
    	 	out.println("  <td width=\"10\">&nbsp;</td>");
//INI: CAMBIOSNOMBRECORTO
    if (vCampos.get(0).toString().equals("txtDDACuenta")) {
        String cuenta = datasession.get("txtCtaSIB").toString();
        out.print("  <td>" + datasession.get("txtCtaSIB").toString() + "</td>");
        out.println("</tr>");
        continue;
    }
    if (vCampos.get(0).toString().equals("txtCliente")) {
        out.print("  <td>" + datasession.get("txtCliente").toString() + "</td>");			
        out.println("</tr>");
	continue;
    }
//FIN: CAMBIOSNOMBRECORTO		    
		    if(datasession.get(vCampos.get(0)) != null) {
				if(vCampos.get(0).equals("txtTipoCambio")){
		     		String TipCamb = (String)datasession.get(vCampos.get(0));
	     	  	    java.text.DecimalFormat reformattedValue = new java.text.DecimalFormat("#,###.0000");
		     	    String returnTipCamb = reformattedValue.format((double)(java.lang.Double.parseDouble(TipCamb) / 10000.00));
		     	    out.print  ("  <td>" + returnTipCamb + "</td>");
		     	}
			    else 
			    	out.print  ("  <td>" + datasession.get(vCampos.get(0)) + "</td>");
	  	 		out.println("</tr>");				  
			}
	        
	        else{
	        	
	        	if(!vCampos.get(0).toString().substring(0,3).equals("lst"))
		       	{
		       		if(vCampos.get(0).toString().substring(0,3).equals("pss"))
		            {
		                out.print  ("  <td><input maxlength=\"" + vCampos.get(3) + "\" name=\"" + vCampos.get(0) + "\" size=\"" + vCampos.get(3) +"\" type=\"Password\"" + " tabindex=\""+(i+1)+"\"");
		           
		            }
		         //checkbox cheque de gerencia
		            else 
		            	if (vCampos.get(0).toString().equals("chkChqGerencia")){
		      	        	out.print  ("  <td><input name=\"" + vCampos.get(0) + "\"  type=\"checkbox\"" + " tabindex=\""+(i+1)+"\" onclick=\"\"" );          
		            	}
		            	else{
		                	out.print("  <td><input maxlength=\"" + vCampos.get(3) + "\" name=\"" + vCampos.get(0) + "\" size=\"" + vCampos.get(3) +"\" type=\"text\"" + " tabindex=\""+(i+1)+"\"");
		            	}	
		            	
		            	String CampoR = (String)vCampos.get(5);
				      	if(CampoR != null && CampoR.length() > 0){
				        	CampoR = CampoR.trim();
				        	//System.out.println(vCampos.get(0) + " " + ctaRepago + " " + CampoR + " " + CampoR.length());
				        	out.print(" value=\"" + CampoR + "\"");
					    }
			         
			     }
			     
				else{
	
		      		out.print("  <td><select id='"+ vCampos.get(0) +"'  name=\"" + vCampos.get(0) + "\" tabindex=\"" + (i+1) + "\" size=\"" + "1" + "\"");
		     	
		//	      	if( vCampos.get(3).toString().length() > 0 ){
			      	if( vCampos.get(4).toString().length() > 0 ){
		     
				       if(vCampos.get(0).toString().substring(0,3).equals("rdo"))
				           out.println(" onClick=\"top.validate(window, this, '" + vCampos.get(4) +"')\">");
					   else
					       out.println(" onChange=\"top.validate(window, this, '" + vCampos.get(4) +"')\">");
		      		}
		      		else
		       			out.println(">");
		      		
		      		for(k=0; k < (listaContenidos.size()); k++){
		  
				       Vector v1Campos = (Vector)listaContenidos.get(k);
				       for(int j = 0; j < v1Campos.size(); j++){
				         Vector v1Camposa = (Vector)v1Campos.get(j);
				         if(v1Camposa.get(0).toString().equals(vCampos.get(0).toString()))
				         	out.println("  <option value=\"" + v1Camposa.get(3) + "\">" + v1Camposa.get(2) + "</option>");
				       }
		     		}
		      		
		      		out.println("  </select></td>");
	   	 
		     		}
			     	if( vCampos.get(4).toString().length() > 0 && !vCampos.get(0).toString().substring(0,3).equals("lst"))
						System.out.println("");
				    else 
				    if(!vCampos.get(0).toString().substring(0,3).equals("lst"))
						out.println("/>");
	 					out.println("</tr>");
		     
		    	}
		  
			}
	
		  	else{
			     %><%@include file="fieldLector.jsf"%>
				<%
			     i = k;
		    }
	  		%>
			<script>
//  		   		personal();
//  		   		comercial();
		   </script>
			<%   
	   }

	%>
	
	<script>	 

// function validaChequeras($campo) {
 
//     $db = conectar();
 
//     $sql = "SELECT * FROM PSPP101.TC_CNT_CAMPOS_L WHERE C_CAMPO = :campo"; 
     
//     $query = $db->prepare($sql);
//     $query-> bindParam(':C_CAMPO', $campo);
//     $query-> execute();
//     $contador = $query -> rowCount();
 
//     if ($contador > 0) {
//         // el usuario existe
//         return true;
//     } else {
//         return false;
//     }
// }

 </script>	
	
	

	</table>
	<input type="hidden" name="iTxn"
		value="<%= session.getAttribute("page.iTxn") %>">
	<input type="hidden" name="oTxn"
		value="<%= session.getAttribute("page.oTxn") %>">
	<input type="hidden" name="cTxn"
		value="<%= session.getAttribute("page.cTxn") %>">
	<input type="hidden" name="sucursal"
		value="<%= session.getAttribute("branch") %>">
	<input type="hidden" name="teller"
		value="<%= session.getAttribute("teller") %>">
	<input type="hidden" name="moneda"
		value="<%= session.getAttribute("page.moneda")%>">
	<input type="hidden" name="registro"
		value="<%=session.getAttribute("empno")%>">
	<input type="hidden" name="tllrlvl"
		value="<%=session.getAttribute("tellerlevel")%>">
	<input type="hidden" name="supervisor"
		value="<%= session.getAttribute("teller") %>">
	<input type="hidden" name="AuthoOK" value="0">
	<input type="hidden" name="SignOK" value="0">
	<input type="hidden" name="validateFLD" value="0">
	<input type="hidden" name="needSignature"
		value="<%out.print(txnIMGAutoriz);%>">
	<input type="hidden" name="needAutho"
		value="<%out.print(txnAuthLevel);%>">
	<input type="hidden" name="SignField"
		value="<%= session.getAttribute("page.SignField") %>">
	<input type="hidden" name="override" value="N">
	<input type="hidden" name="txtFees" value="0">
	<input type="hidden" name="compania" value="20">
	<input type="hidden" name="txtSelectedItem" value="0">
	<input type="hidden" name="returnform" value="N">
	<input type="hidden" name="returnform2" value="N">
	<input type="hidden" name="txtPlazaSuc"
		value="<%= session.getAttribute("plaza") %>">
	<input type="hidden" name="hdCreditoIni" value="">
	<input type="hidden" name="specialCveTran" value="">
	<input type="hidden" name="isChequeFormatoNuevo" value="N">
	<input type="hidden" name="dvCuenta" value="">
	<input type="hidden" name="dvCheque" value="">
	<input type="hidden" name="dvRuta" value="">

	<input type="hidden" name="txtDDACuenta"
		value="<%= datasession.get("txtCtaSIB")%>">

	<input type="hidden" name="passw"
		value="<%=session.getAttribute("password")%>">
	<input type="hidden" name="MontoRetiroPesos"
		value="<%=session.getAttribute("Retiro.Pesos")%>">
	<input type="hidden" name="MontoRetiroDolar"
		value="<%=session.getAttribute("Retiro.Dolar")%>">
	<input type="hidden" name="fecha4523" values="">
	<p>
		<script>
 if (document.entry.txtFechaEfect)
 {
  document.writeln("<input type=hidden name=txtFechaSys>");
  document.entry.txtFechaEfect.value=top.sysdate();
 }
 if (document.entry.txtFechaError)
 {
  document.writeln("<input type=hidden name=txtFechaSys>");
  document.entry.txtFechaError.value=top.sysdate();
 }
 if (document.entry.txtFechaCarta)
 {
  document.writeln("<input type=hidden name=txtFechaSys>");
  document.entry.txtFechaCarta.value=top.sysdate();
 }
</script>
		<style>
.invisible {
	display: None;
}

.visible {
	display: "";
}
</style>


		<% if(txnAuthLevel.equals("1") || txnIMGAutoriz.equals("1"))
    out.println("<a id='btnCont' tabindex=\"" + (i+1) + "\" href=\"javascript:document.entry.returnform.value='S';validate(document.entry);javascript:AuthoandSignature(document.entry)\" ><img src=\"../ventanilla/imagenes/b_continuar.gif\" border=\"0\"></a>");
   else
    out.println("<a id='btnCont' tabindex=\"" + (i+1) + "\" href=\"javascript:document.entry.returnform.value='S';validate(document.entry)\"><img src=\"../ventanilla/imagenes/b_continuar.gif\" border=\"0\"></a>");
%>
	
	</form>
</body>
</html>
