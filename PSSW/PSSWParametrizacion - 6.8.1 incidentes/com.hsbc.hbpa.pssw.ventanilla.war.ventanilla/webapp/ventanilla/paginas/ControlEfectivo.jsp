<!--
//*************************************************************************************************
//             Funcion: JSP que muestra la lista de usuarios bloqueado
//            Elemento: ControlEfectivo.jsp
//          Creado por: Fausto Rodrigo Flores Moreno
//*************************************************************************************************
// CCN - 4360589 - 16/04/2007 - Se realizan modificaciones para el monitoreo de efectivo en las sucursales
//*************************************************************************************************
-->
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<HTML>
<HEAD>
<%@ page session="true" language="java" import="java.util.*,ventanilla.com.bital.util.MonitorEfectivo,ventanilla.com.bital.beans.DatosVarios,ventanilla.GenericClasses,javax.servlet.http.HttpSession"%>
<%--@ page language="java" import="java.util.*,MonitorEfectivo,GenericClasses"--%>
<% 
   MonitorEfectivo monitoreo = new MonitorEfectivo();
   Vector resultado = monitoreo.getBloqueados();
   String userPIF = "";
   
   if(session.getAttribute("userPIF")!=null)
   	   userPIF = (String)session.getAttribute("userPIF");
   else
	   userPIF = (String)request.getParameter("userC");
       
   session.setAttribute("userPIF",userPIF);
   
   for(int y = 0; y<resultado.size();y++)
   if(resultado.size()>0){
   		if(!resultado.elementAt(0).toString().equals("30000")){
	    	int i=0;
	    	String data = "";      
		    String dataTmp= "";
	    	DatosVarios mot = DatosVarios.getInstance();                   
		    Vector timeRefresh = new Vector();
	    	timeRefresh =(Vector)mot.getDatosV("PIFfactor");
		    
		    if(timeRefresh.size()>0){
		    	String timeR = timeRefresh.get(0).toString();
	        	StringTokenizer time = new StringTokenizer(timeR,"*");
			    time.nextToken();
			    data = time.nextToken().toString().trim();
			    time.nextToken();
				data = String.valueOf((Integer.parseInt(data) * 60));
			}else
				data = "120";
	   		out.println("<META http-equiv='refresh' content='"+data+"' URL='ControlEfectivo.jsp'>");
	  }else{
   		out.println("<META http-equiv='Content-Type' content='text/html; charset=ISO-8859-1'>");
  	  }
   }
%>
<link rel="StyleSheet" type="text/css" href="../estilos/style.css">
<TITLE>Bloqueo de Cajero.jsp</TITLE>
<script type="text/JavaScript">
	var newDatos = new Array();
	
<%
	if(resultado.size()>0){
		if(!resultado.get(0).toString().equals("30000"))// && !resultado.get(0).toString().equals("-0"))
		{
			for(int k = 0; k<resultado.size(); k++)
			{
				out.println("newDatos[" + (k) + "] = '" + resultado.get(k).toString().trim() + "'");
			}
		}
		
	}
	
%>
    function actualizar()
	{
		document.informacion.action = "ControlEfectivo.jsp";
		document.informacion.submit();
	}
	function setBloq()
	{
		var strDatos = "";
		var pos = 0;
		var x = 0;
		var find = false;
		if(newDatos.length > 1){
			while( x < newDatos.length && !find){
				if(document.informacion.Desbloq[x].checked){
					pos = x;
					find = true;
				}				
				x++;
			}
		}
		else{
			if(document.informacion.Desbloq.checked)
				find = true;
		}
		if(find){
			strDatos = newDatos[pos];
			var res = confirm("�Esta seguro de que desea desbloquear este usuario?")
			if(res){
				document.informacion.usersBloq.value = strDatos;
				document.informacion.submit();
			}else{
				return;
			}
		}else{
			alert("No a seleccionado ningun usuario");
			return;
		}
		
	}
	
	function Detalle(llave)
	{
		document.informacion.action = "DetalleBloqueado.jsp";
		document.informacion.usersBloq.value = llave;
		document.informacion.submit();
	}
</script>
</HEAD>
<BODY>
<form name="informacion" action = "../../servlet/ventanilla.Desbloqueo" method="post">
<%
	GenericClasses gc = new GenericClasses();
	if(resultado.size()>0){
   		if(!resultado.elementAt(0).toString().equals("30000") && !resultado.elementAt(0).toString().equals("-0")){
%>
		<table align="center">
		<tr><td>
			<table align="center" width="750px">
			<tr>
			<th bgcolor='#999999' align="center"><FONT size="2" color="#ffffff" face="Arial">SUCURSAL</font></th>
			<th bgcolor='#999999' align="center"><FONT size="2" color="#ffffff" face="Arial">CAJERO</font></th>
			<th bgcolor='#999999' align="center"><FONT size="2" color="#ffffff" face="Arial">FECHA DE BLOQUEO</font></th>
			<th bgcolor='#999999' align="center"><FONT size="2" color="#ffffff" face="Arial">HORA DE BLOQUEO</font></th>
			<th bgcolor='#999999' align="center"><FONT size="2" color="#ffffff" face="Arial">MONEDA</font></th>
			<th bgcolor='#999999' align="center"><FONT size="2" color="#ffffff" face="Arial">MONTO LIMITE</font></th>
			<th bgcolor='#999999' align="center"><FONT size="2" color="#ffffff" face="Arial">MONTO NO CONCENTRADO</font></th>
			<th bgcolor='#999999' align="center"><FONT size="2" color="#ffffff" face="Arial">DESBLOQUEAR</font></th>
			</tr>
			<tr><td colspan="8">
				<div id="Transactions" style="overflow: auto; width: 100%; height: 170px;">
					<table align="center" width="100%" cellspacing="0">
					<tr>
<%					StringTokenizer row;
					int j = 0;
					String valor = "";
					boolean casilla = false;
					for(int i=0; i<resultado.size();i++){
						casilla = false;
						String dato = resultado.elementAt(i).toString();
						row = new StringTokenizer(dato.substring(1,dato.length()-1),"|");
						j = 0;
						while(row.hasMoreElements()){
							valor = row.nextToken().toString();
							if(j == 0){%>
								<td align="center" width="80px"><FONT size="2" color="black" face="Arial"><%=valor.substring(0,4)%></font></td>
								<td align="center" width="60px"><FONT size="2" color="black" face="Arial"><a href="javascript:Detalle('<%=valor%>');" title="Detalle"; onmouseover="self.status='Detalle';return true;" onmouseout="window.status='';" alt="Detalle"><%=valor%></a></font></td>
							<%}
							if(j == 1){%>
								<td align="center" width="110px"><FONT size="2" color="black" face="Arial"><%=gc.FormatDate(valor)%></font></td>
							<%}else if(j == 2){%>
								<td align="center" width="130px"><FONT size="2" color="black" face="Arial"><%=gc.FormatHour(valor)%></font></td>
							<%}else if(j == 3){%>
								<td align="center" width="50px"><FONT size="2" color="black" face="Arial"><%=gc.getDivisa(valor)%></font></td>
							<%}else if(j == 5){%>
								<td align="center" width="120px"><FONT size="2" color="black" face="Arial"><%=gc.formatMonto(valor)%></font></td>
							<%}else if(j == 6){%>
								<td align="center" width="130px"><FONT size="2" color="black" face="Arial"><%=gc.formatMonto(valor)%></font></td>
							<%	casilla = true;
							}
							if (casilla){%>
								<td align="center" width="120px"><input name="Desbloq" type="radio"></td></tr>
							<%	casilla = false;
							}
							j++;
						}
					}
%>					</table>
				</div>
			</td>
			</tr>
			<tr><td colspan="8"><hr color="red"></hr></td></tr>
			</table>
		</td>
		<td>&nbsp;</td>
		<td valign="middle">
			<table align="center">
			<tr><td align="center" valign="middle">
				<a id='btnAct' href="javascript:actualizar();" onmouseover="self.status='Actualizar Pantalla';return true;" onmouseout="window.status='';"><img src="../imagenes/b_acpa2.gif" alt="Actualizar Pantalla" border="0"></a>
			</td></tr>
			<tr><td>&nbsp;</td></tr>
			<tr><td align="center" valign="middle">
				<a id='btnDesB' href="javascript:setBloq();" onmouseover="self.status='Desbloquear Usuario (s)';return true;" onmouseout="window.status='';"><img src="../imagenes/b_desb2.gif" alt="Desbloquear Usuario (s)" border="0"></a>
			</td>
			</tr>
			</table>
		</td>
		</tr>
		</table>	
<%		}
		else if(resultado.elementAt(0).toString().equals("-0")){
%>			<table align="center">
			<tr>
			<td>&nbsp;</td>
			</tr>
			<tr>
			<td align="center">
				<div id="Transactions" style="overflow: auto; width: 80%; height: 50%;border-style: double;border-color: red;border-width: thin;" align="center">
					<b>NO EXISTEN CAJEROS BLOQUEADOS</b>
				</div>
			</td>			
			</tr>
			<tr><td align="center" valign="middle">&nbsp;</td></tr>
			<tr><td align="center" valign="middle">
				<a id='btnAct' href="javascript:actualizar();" onmouseover="self.status='Actualizar Pantalla';return true;" onmouseout="window.status='';"><img src="../imagenes/b_acpa2.gif" alt="Actualizar Pantalla" border="0"></a>
			</td></tr>

			</table>
<%		}
		else
		{
%>			<table align="center">
			<tr>
			<td>&nbsp;</td>
			</tr>
			<tr>
			<td align="center">
				<div id="Transactions" style="overflow: auto; width: 80%; height: 50%;border-style: double;border-color: red;border-width: thin;" align="center">
					<b>ESTA VISUALIZACI�N NO SE ENCUENTRA DISPONIBLE POR EL MOMENTO</b>
				</div>
			</td>
			</tr>
			</table>
<%		}
	}
%>
<input type="hidden" name="usersBloq" value="0">
<input type="hidden" name="userC" value="<%=userPIF%>">
</form>
</BODY>
</HTML>
