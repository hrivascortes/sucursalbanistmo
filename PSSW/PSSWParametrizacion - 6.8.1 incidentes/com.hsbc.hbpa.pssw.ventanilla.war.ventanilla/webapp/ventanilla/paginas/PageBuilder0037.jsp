<!--
//*************************************************************************************************
//             Funcion: JSP que despliega captura de txn 0037
//            Elemento: PageBuilder0037.jsp
//          Creado por: Alejandro Gonzalez Castro
// Ultima Modificacion: 03/08/2004
//      Modificado por: Alejandro Gonzalez Castro
//*************************************************************************************************
// CCN - 4360175 - 03/08/2004 - Ajuste size de ventana de impresion
// CCN - 4360581 - 12/03/2007 - Se convierten los estilos en jsf y se hace un include en el jsp
//*************************************************************************************************
-->

<%@ page session="true" import="java.util.Vector"%>
<%
response.setHeader("Cache-Control","no-store"); //HTTP 1.1
response.setHeader("Pragma","no-cache"); //HTTP 1.0
response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<%
 java.util.Vector listaCampos = (Vector)session.getAttribute("page.listcampos");
 java.util.Vector listaContenidos = (Vector)session.getAttribute("page.listcont");
 int k=0;
 int i=0;
%>
<html>
<head>
 <%@include file="style.jsf"%>
 <title>Datos de la Transaccion</title>
 <script type="text/javascript" language="javascript">
 <!--
  var bancosDisponibles = new Array();
  bancosDisponibles[0] = new String('CAD CANADIAN DOLLAR CITIBANK CANADA 123               FRONT ST. W TORONT. M5J 2M3 A/C2183509007 TRANSITO NO. 00082-260');
  bancosDisponibles[1] = new String('CHF SWISS FRANC     CITIBANK, N.A. (ZURICH)           FOR CRS A/C 0981722019');
  bancosDisponibles[2] = new String('DKK DANISH KRONE    CITIBANK INTERNATIONAL, PLC       DENMARK BRANCH                            FOR CRS ACCOUNT NO. 811160016');
  bancosDisponibles[3] = new String('EUR EURO            CITIBANK INTERNATIONAL PLC        AUSTRIA BRANCH (VIENNA)                   FOR CRS A/C 781046-027');
  bancosDisponibles[4] = new String('EUR EURO            CITIBANK N.A. (BRUSSELS)          FOR CRS A/C. 570-8020855-80');
  bancosDisponibles[5] = new String('EUR EURO            CITIBANK A.G. (FRANKFURT)         502 109 00                                FOR CRS ACCOUNT NO. 4120060029');
  bancosDisponibles[6] = new String('EUR EURO            CITIBANK INTERNATIONAL PLC, SPAIN JOSE ORTEGA Y GASSETT 29, MADRID CRS      A/C 14740000140008179018');
  bancosDisponibles[7] = new String('EUR EURO            OKOBANK (HELSINKI, FINLAND)       AS AGENT FOR CITIBANK, N.A.               GCMS A/C 31260-95906850');
  bancosDisponibles[8] = new String('EUR EURO            CITIBANK INTERNATIONAL PLC        PARIS CRS A/C 0141674048');
  bancosDisponibles[9] = new String('EUR EURO            CITIBANK N.A. (DUBLIN)            SORT 99-00-52 CRS A/C 4944-038');
  bancosDisponibles[10] = new String('EUR EURO            CITIBANK N.A. (MILAN)             FOR CRS A/C 4/107116/023                  AS AGENT FOR CITIBANK N.A. NEW YORK');
  bancosDisponibles[11] = new String('EUR EURO            CITIBANK N.A. (AMSTERDAM)         CRS ACCOUNT 266099971');
  bancosDisponibles[12] = new String('EUR EURO            CITIBANK PORTUGAL S.A.            FOR CRS A/C 0-801001-017');
  bancosDisponibles[13] = new String('GBP POUND STERLING  CITIBANK N.A. (REMITANCE SERVICE) MCP ACCOUNT 336 STRAND                    LONDON WC2R 1HB, SORTT 08-61-03');
  bancosDisponibles[14] = new String('JPY JAPANESE YEN    CITIBANK N.A. (TOKIO)             FOR CRS A/C 0-221704-401');
  bancosDisponibles[15] = new String('SEK SWEDISH KRONA   CITIBANK INTERNATIONAL, PLC       SWENDEN BRANCH (STOCKHOLM)                FOR CRS ACCOUNT');

  function buildSelector(){
    var arrayIndex = 0;
    document.forms[0].lstDivisaGCB.length = 0;
    for(; arrayIndex < bancosDisponibles.length; arrayIndex++){
	document.forms[0].lstDivisaGCB[arrayIndex] = new Option();
      document.forms[0].lstDivisaGCB[arrayIndex].text = bancosDisponibles[arrayIndex].substring(0,80);
      document.forms[0].lstDivisaGCB[arrayIndex].value = bancosDisponibles[arrayIndex].substring(0,10);
    }
    document.forms[0].lstDivisaGCB.selectedIndex = 0;
  }

  function obtainString(inputString, position){
    var initialPosition = 0;
    var finalPosition = 0;
    var outputString = new String('');
    switch(position){
      case 0: initialPosition = 0; finalPosition = 3; break;
      case 1: initialPosition = 4; finalPosition = 19; break;
      case 2: initialPosition = 20; finalPosition = 53; break;
      case 3: initialPosition = 54; finalPosition = 95; break;
      case 4: initialPosition = 96; finalPosition = 131; break;
    }
    if(finalPosition > inputString.length)
      finalPosition = inputString.length;
    if(initialPosition > inputString.length)
      initialPosition = inputString.length;
    outputString = inputString.substring(initialPosition,finalPosition);
    return outputString;
  }

  function replaceWhiteSpaces(inputString){
    var inputLength = inputString.length;
    var outputString = new String();
    var tempChar = ' ';
    var i = 0;
    for(; i < inputLength; i++){
      tempChar = inputString.charAt(i);
      if(tempChar == ' ')
        tempChar = '%20';
//        tempChar = unescape('%20');
      outputString += tempChar;
    }
    return outputString;
  }

  function submitPrint(){
   var formName = document.forms[0];
   var cadImpresion = new String('GIROCITI2~EXP~');
   var regularExpression = /,/g;
   var txtMonto = formName.txtMonto.value.replace(regularExpression, '');
   regularExpression = /\./g;
   txtMonto = txtMonto.replace(regularExpression, '');
   cadImpresion += formName.txtSerial5.value + '~' + txtMonto + '~';
//   cadImpresion += formName.txtApeBen.value + ' ' + formName.txtNomBen.value + '~';
   cadImpresion += formName.txtNomBen.value + ' ' + formName.txtApeBen.value + '~';
//   cadImpresion += formName.txtApeOrd.value + ' ' + formName.txtNomOrd.value + '~';
   cadImpresion += formName.txtNomOrd.value + ' ' + formName.txtApeOrd.value + '~';
   cadImpresion += obtainString(bancosDisponibles[formName.lstDivisaGCB.selectedIndex], 0) + '~';
   cadImpresion += obtainString(bancosDisponibles[formName.lstDivisaGCB.selectedIndex], 1) + '~';
   cadImpresion += obtainString(bancosDisponibles[formName.lstDivisaGCB.selectedIndex], 2) + '~';
   cadImpresion += obtainString(bancosDisponibles[formName.lstDivisaGCB.selectedIndex], 3) + '~';
   cadImpresion += obtainString(bancosDisponibles[formName.lstDivisaGCB.selectedIndex], 4) + '~';
   cadImpresion = '../servlet/ventanilla.ImprimirServlet?txtCadImpresion=' + replaceWhiteSpaces(cadImpresion);

   top.openDialog(cadImpresion, 160, 120, 'top.setPrefs4()', document.forms[0]);
  }

function validate(form)
{
   
<%
  for(i=0; i < listaCampos.size(); i++)
   {
    Vector vCampos = (Vector)listaCampos.get(i);
    vCampos = (Vector)vCampos.get(0);
    if( vCampos.get(4).toString().length() > 0 )
    {
      out.println("  if( !top.validate(window, document.girosCitiBank." + vCampos.get(0) + ", '" + vCampos.get(4) +"') )");
      if (vCampos.get(0).toString().equals("txtFolio") && vCampos.get(4).toString().equals("isEmptyStr")) 
    {        
     %>
      if(document.entry.returnform.value == "N"  ) {
         return false;
       }
      else {
         document.entry.returnform.value = "N";
         return true;
      }
 <% }
     else
        out.println("return false;") ;
    }
   }
%>
return true;
}

 //-->
 </script>
</head>
<body bgcolor="#FFFFFF" onload="top.setFieldFocus(window.document); buildSelector();">
<br>
<form name="girosCitiBank">
<h1><%=session.getAttribute("page.txnImage")%></h1>
<h3>
<%out.print(session.getAttribute("page.cTxn") + "  " + (String)session.getAttribute("page.txnlabel"));%>
</h3>
<table border="0" cellspacing="0">
<%
	for(i =  0; i < listaCampos.size(); i++){
    if(i == 2){
     out.print("<tr>");
     out.print(" <td>");
     out.print("  <i><b>Beneficiario</b></i>");
     out.print(" </td>");
     out.println("</tr>");
    }
    else if(i == 4){
     out.print("<tr>");
     out.print(" <td>");
     out.print("  <i><b>Ordenante</b></i>");
     out.print(" </td>");
     out.println("</tr>");
    }

	  java.util.Vector vCampos = (java.util.Vector)listaCampos.get(i);
	  vCampos = (Vector)vCampos.get(0);
    if(!vCampos.get(0).toString().equals("txtCodSeg")){
	    out.println("<tr>");
	    out.println("  <td>" + vCampos.get(2) + ":</td>");
	    out.println("  <td width=\"10\">&nbsp;</td>");
	    if(!vCampos.get(0).toString().substring(0,3).equals("lst")){
			  if(vCampos.get(0).toString().substring(0,3).equals("pss")){
			    out.print  ("  <td><input maxlength=\"" + vCampos.get(3) + "\" name=\"" + vCampos.get(0) + "\" size=\"" + vCampos.get(3) +"\" type=\"Password\"" + "\" tabindex=\""+ (i+1) +"\"");
			  }
        		  else{
          		    out.print("  <td><input maxlength=\"" + vCampos.get(3) + "\" name=\"" + vCampos.get(0) + "\" size=\"" + vCampos.get(3) +"\" type=\"text\"" + "\" tabindex=\""+ (i+1) +"\"");        		  
			}
	      String CampoR = (String)vCampos.get(5);
	      if(CampoR != null && CampoR.length() > 0){
	        CampoR = CampoR.trim();
	        out.print(" value=\"" + CampoR + "\"");
	      }
	     }
     else{
	      out.print("  <td><select style =\"font-family:Courier; font-size:12px;\" name=\"" + vCampos.get(0) + "\" size=\"" + "1" + "\" tabindex=\""+ (i+1) +"\"");
	      if( vCampos.get(4).toString().length() > 0 ){
	       if(vCampos.get(0).toString().substring(0,3).equals("rdo"))
           out.println(" onClick=\"top.validate(window, this, '" + vCampos.get(4) +"')\">");
	       else
	         out.println(" onChange=\"top.validate(window, this, '" + vCampos.get(4) +"')\">");
	      }
	      else
	       out.println(">");
	      
	for(k=0; k < listaContenidos.size(); k++){
	       Vector v1Campos = (Vector)listaContenidos.get(k);
	       for(int j = 0; j < v1Campos.size(); j++){
	         Vector v1Camposa = (Vector)v1Campos.get(j);
	         if(v1Camposa.get(0).toString().equals(vCampos.get(0).toString()))
	           out.println("  <option value=\"" + v1Camposa.get(2) + "\">" + v1Camposa.get(3));
	       }
	      }
	      out.println("  </select></font></td>");
	     }
	     if( vCampos.get(4).toString().length() > 0 && !vCampos.get(0).toString().substring(0,3).equals("lst"))
	      out.println(" onBlur=\"top.estaVacio(this)||top.validate(window, this, '" + vCampos.get(4) +"')\"" + "onKeyPress=\"top.keyPressedHandler(window, this, '" + vCampos.get(4) +"')\"></td>");
	     else if(!vCampos.get(0).toString().substring(0,3).equals("lst"))
	      out.println(">");
	     out.println("</tr>");
	    }
	    else{
	     %><%--@include file="fieldLector.jsp"--%><%
	     //i = k;
	    }
	   }
	%>
 </table>
 <p>
 <a tabIndex="<%=(i+1)%>" href=" javascript: if (validate(document.girosCitiBank)) submitPrint();"><img src="../ventanilla/imagenes/b_continuar.gif" border="0"></a>
 <input type="hidden" name="sucursal" value="<%= session.getAttribute("branch") %>">
 </form>
 </body>
</html>
