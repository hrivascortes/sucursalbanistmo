<!--
//*************************************************************************************************
//             Funcion: JSP que despliega respuesta de txn Vouchers
//            Elemento: ResponseVouchers4.jsp
//          Creado por: Alejandro Gonzalez Castro
// Ultima Modificacion: 03/08/2004
//      Modificado por: Alejandro Gonzalez Castro
//*************************************************************************************************
// CCN - 4360175 - 03/08/2004 - Ajuste size de ventana de impresion
//*************************************************************************************************
-->
<%@ page session="true" import="java.util.*"%>
<%
response.setHeader("Cache-Control","no-store"); //HTTP 1.1
response.setHeader("Pragma","no-cache"); //HTTP 1.0
response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
<title>Results</title>
<style type="text/css">Body {font-family: Arial, Helv; font-size: 10pt; font-weight: bold; color:#000000}</style>
</head>
<body>
Proceso de Vouchers Terminado...
<form name="ImprimeDiario">
<%
	Hashtable data = (Hashtable)session.getAttribute("page.datasession");
	String afiliacion =  (String)data.get("txtNoNegocio");
	String cuenta =  (String)session.getAttribute("cta");
	String nd =  (String)data.get("txtNoDevs");
	String nv =  (String)data.get("txtNoVtas");
	String mtod =  (String)session.getAttribute("montod");
	String mtov =  (String)session.getAttribute("montov");
	String comd = (String)session.getAttribute("comds");
	String comv = (String)session.getAttribute("comvs");
	String ivad = (String)session.getAttribute("ivads");
	String ivav = (String)session.getAttribute("ivavs");
	String totald = (String)session.getAttribute("sumads");
	String totalv = (String)session.getAttribute("sumavs");
	String sbcd = "0";
	String sbcv =  (String)session.getAttribute("sbc");
	String vouchers = "VOUCHERS~" + afiliacion + "~" + cuenta + "~" + nd + "~" + mtod + "~" + comd + "~" + ivad + "~" + totald + "~"  + sbcd + "~" + nv + "~" + mtov + "~" + comv + "~" + ivav + "~" + totalv + "~"  + sbcv + "~" ;
	String codetxn = (String)data.get("codetxn");
     if(!codetxn.equals("0"))
         vouchers="VOUCHERS~";
	if(session.getAttribute("ctrlCiclo") != null)
    session.removeAttribute("ctrlCiclo");
	if(session.getAttribute("ctrlConsecutivo") != null)
    session.removeAttribute("ctrlConsecutivo");
	if(session.getAttribute("ctrl") != null)
    session.removeAttribute("ctrl");
  out.println("<a href=\"javascript:top.openDialog('../../servlet/ventanilla.ImprimirServlet?txtCadImpresion=" + vouchers + "', 160, 120, 'top.setPrefs4()', document.Txns)\"><img src=\"../imagenes/b_continuar.gif\" border=\"0\"></a>");
%>
</form>
</body>
</html>
