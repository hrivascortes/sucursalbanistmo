<!--
//*************************************************************************************************
//             Funcion: JSP que despliega respuesta M021
//            Elemento: ResponseM021.jsp
//          Creado por: Marco Lara Palacios
// Ultima Modificacion: 06/08/2004
//      Modificado por: Marco Lara Palacios
//*************************************************************************************************
// CCN - 4360176 - 06/08/2004 - Se habilita control de usuarios de apoyo
// CCN - 4360581 - 12/03/2007 - Se convierten los estilos en jsf y se hace un include en el jsp
//*************************************************************************************************
-->

<%@ page session="true"%>
<%
response.setHeader("Cache-Control","no-store"); //HTTP 1.1
response.setHeader("Pragma","no-cache"); //HTTP 1.0
response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
  <title>Confirmación de Datos</title>
  <%@include file="style.jsf"%>
</head>
<body bgcolor="white">
<p><br>
<%
  String msg = (String)session.getAttribute("error.msg");
  if( msg != null )
  {%>
<font color="red"><b>** Error ***</b></font><p><br>
<%
  out.println(msg);
  }
  else
  {%>
<b>Alta efectuada satisfactoriamente</b>
<%}%>
</body>
</html>