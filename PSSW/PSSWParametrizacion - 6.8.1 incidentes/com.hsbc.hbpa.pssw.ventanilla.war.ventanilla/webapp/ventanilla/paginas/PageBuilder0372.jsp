<!--
// CCN - 4360581 - 12/03/2007 - Se convierten los estilos en jsf y se hace un include en el jsp
-->

<%@ page session="true" import="java.util.Hashtable,java.lang.*"%>
<%
response.setHeader("Cache-Control","no-store"); //HTTP 1.1
response.setHeader("Pragma","no-cache"); //HTTP 1.0
response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<%
String txnAuthLevel = (String)session.getAttribute("page.txnAuthLevel");
String txnIMGAutoriz = (String)session.getAttribute("page.txnIMGAutoriz");
Hashtable datasession = (Hashtable)session.getAttribute("page.datasession");
%>
<%!
  private String setPointToString(String newCadNum)
  {
   int iPIndex = newCadNum.indexOf("."), iLong;
   String szTemp;

   if(iPIndex > 0)
   {
    newCadNum = new String(newCadNum + "00");
    newCadNum = newCadNum.substring(0, iPIndex + 1) + newCadNum.substring(iPIndex + 1, iPIndex + 3);
   }
   else
   {
    for(int i = newCadNum.length(); i < 3; i++)
     newCadNum = "0" + newCadNum;
    iLong = newCadNum.length();
    if(iLong == 3)
     szTemp = newCadNum.substring(0, 1);
    else
     szTemp = newCadNum.substring(0, iLong - 2);
    newCadNum = szTemp + "." + newCadNum.substring(iLong - 2);
   }

   return newCadNum;
  }

  private String delPointOfString(String newCadNum)
  {
   int iPIndex = newCadNum.indexOf(".");

   if(iPIndex > 0)
   {
    newCadNum = newCadNum.substring(0, iPIndex) +
                newCadNum.substring(iPIndex + 1, newCadNum.length());
   }

   return newCadNum;
  }

  private String delCommaPointFromString(String newCadNum)
  {
   String nCad = new String(newCadNum);
   if( nCad.indexOf(".") > -1)
   {
    nCad = nCad.substring(nCad.indexOf(".") + 1, nCad.length());
    if(nCad.length() != 2)
    {
     String szTemp = new String("");
     for(int j = nCad.length(); j < 2; j++)
      szTemp = szTemp + "0";
     newCadNum = newCadNum + szTemp;
    }
   }

   StringBuffer nCadNum = new StringBuffer(newCadNum);
   for(int i = 0; i < nCadNum.length(); i++)
    if(nCadNum.charAt(i) == ',' || nCadNum.charAt(i) == '.')
     nCadNum.deleteCharAt(i);

   return nCadNum.toString();
  }

  private String setFormat(String newCadNum)
  {
    int iPIndex = newCadNum.indexOf(".");
    String nCadNum = new String(newCadNum.substring(0, iPIndex));
    for (int i = 0; i < (int)((nCadNum.length()-(1+i))/3); i++)
     nCadNum = nCadNum.substring(0, nCadNum.length()-(4*i+3))+','+nCadNum.substring(nCadNum.length()-(4*i+3));

    return nCadNum + newCadNum.substring(iPIndex, newCadNum.length());
  }

  private String longtoString(long newlong){
    String newString = String.valueOf(newlong);
    return newString.toString();
  }
%>
<html>
<head>
  <%@include file="style.jsf"%>
  <title>Datos de la Transaccion</title>
<script language="javascript">
<!--
  function formSubmitter()
  {
      	document.forms[0].submit();
  }
//-->
</script>
</head>
<body onload="top.setFieldFocus(window.document);formSubmitter();">
<% out.print("<form name=\"entry\" action=\"../../servlet/ventanilla.DataServlet\" method=\"post\">");
 long montoComision = Long.parseLong(datasession.get("txtComision").toString());
 long montoIVA = Long.parseLong(datasession.get("txtPIVA").toString()) * montoComision / 100;
%>

<h1><%=session.getAttribute("page.txnImage")%></h1>
<h3>
<%out.print(session.getAttribute("page.cTxn") + "  " + (String)session.getAttribute("page.txnlabel"));%>
</h3>
                
<table>
	<tr>
		<td>N&uacute;mero de Servicio: </td>
		<%if (session.getAttribute("page.iTxn").toString().equals("0728"))
		{%>
		<td align="right">101041</td>
	<%	}
		else if(session.getAttribute("page.iTxn").toString().equals("0730"))
		{%>
		<td align="right"><%=datasession.get("txtCtaCia").toString()%></td>
	<%	}%>
	</tr>
	<tr>
		<td align="right">Importe de IVA: </td>
		<td align="right"><%=setFormat(setPointToString(longtoString(montoIVA)))%></td>
	</tr>
</table>
<input type="hidden" name="iTxn" value="<%= session.getAttribute("page.iTxn") %>">
<input type="hidden" name="oTxn" value="<%= session.getAttribute("page.oTxn") %>">
<input type="hidden" name="cTxn" value="<%= session.getAttribute("page.cTxn") %>">
<input type="hidden" name="sucursal" value="<%= session.getAttribute("branch") %>">
<input type="hidden" name="teller" value="<%= session.getAttribute("teller") %>">
<input type="hidden" name="moneda" value="<%= session.getAttribute("page.moneda")%>">
<input type="hidden" name="registro" value="<%=session.getAttribute("empno")%>">
<input type="hidden" name="tllrlvl" value="<%=session.getAttribute("tellerlevel")%>">
<input type="hidden" name="supervisor" value="<%= session.getAttribute("teller") %>">
<input type="hidden" name="AuthoOK" value="0">
<input type="hidden" name="compania" value="20">
<input type="hidden" name="SignOK" value="0">
<input type="hidden" name="validateFLD" value="0">
<input type="hidden" name="needSignature" value="<%out.print(txnAuthLevel);%>">
<input type="hidden" name="needAutho" value="<%out.print(txnIMGAutoriz);%>">
<input type="hidden" name="txtInvoqued" value="01">
<p>
</form>
</body>
</html>
