<!--
//*************************************************************************************************
//             Funcion: JSP que despliega respuesta de txn 0860
//            Elemento: Response0860.jsp
//          Creado por: Jesus Emmanuel Lopez Rosales
//*************************************************************************************************
// CCN - 4360175 - 03/08/2004 - Ajuste size de ventana de impresion
// CCN - 4360189 - 03/09/2004 - Se redirecciona a Final.jsp por Control de Efectivo
// CCN - 4360275 - 18/02/2005 - WAS 5.1
// CCN - 4360275 - 18/02/2005 - Se realizan modificaciones para la presentacion correcta
//                              de los datos del beneficiario y ordenante.
// CCN - 4360576 - 09/03/2007 - Modificaciones para proyecto OPMN
//*************************************************************************************************
-->
<%@ page session="true" import="java.util.*"%>
<%
response.setHeader("Cache-Control","no-store"); //HTTP 1.1
response.setHeader("Pragma","no-cache"); //HTTP 1.0
response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<%!
private String getString(String s)
{
    int len = s.length();
    
    for(; len>0; len--)
	if(s.charAt(len-1) != ' ')
	    break;

    return s.substring(0, len);
}

private String setPointToString(String newCadNum)
{
   int iPIndex = newCadNum.indexOf("."), iLong;
   String szTemp;

   if(iPIndex > 0)
   {
     newCadNum = new String(newCadNum + "00");
     newCadNum = newCadNum.substring(0, iPIndex + 1) + newCadNum.substring(iPIndex + 1, iPIndex + 3);
   } else {
     for(int i = newCadNum.length(); i < 3; i++)
       newCadNum = "0" + newCadNum;
       
     iLong = newCadNum.length();
     
     if(iLong == 3)
       szTemp = newCadNum.substring(0, 1);
     else
       szTemp = newCadNum.substring(0, iLong - 2);
       
     newCadNum = szTemp + "." + newCadNum.substring(iLong - 2);
   }

   return newCadNum;
}

private String delPointOfString(String newCadNum)
{
   int iPIndex = newCadNum.indexOf(".");

   if(iPIndex > 0)
   {
    newCadNum = newCadNum.substring(0, iPIndex) +
                newCadNum.substring(iPIndex + 1, newCadNum.length());
   }

   return newCadNum;
}


private String delCommaPointFromString(String newCadNum)
{
   String nCad = new String(newCadNum);
   
   if( nCad.indexOf(".") > -1)
   {
     nCad = nCad.substring(nCad.indexOf(".") + 1, nCad.length());
     if(nCad.length() != 2)
     {
       String szTemp = new String("");
       
       for(int j = nCad.length(); j < 2; j++)
         szTemp = szTemp + "0";
         
       newCadNum = newCadNum + szTemp;
     }
   }

   StringBuffer nCadNum = new StringBuffer(newCadNum);
   for(int i = 0; i < nCadNum.length(); i++)
     if(nCadNum.charAt(i) == ',' || nCadNum.charAt(i) == '.')
       nCadNum.deleteCharAt(i);

   return nCadNum.toString();
}


private String setFormat(String newCadNum)
{
    int iPIndex = newCadNum.indexOf(".");
    String nCadNum = new String(newCadNum.substring(0, iPIndex));
    
    for (int i = 0; i < (int)((nCadNum.length()-(1+i))/3); i++)
      nCadNum = nCadNum.substring(0, nCadNum.length()-(4*i+3))+','+nCadNum.substring(nCadNum.length()-(4*i+3));

    return nCadNum + newCadNum.substring(iPIndex, newCadNum.length());
}
%>
<%
    Vector resp = (Vector)session.getAttribute("response");
    Hashtable datasession = (Hashtable)session.getAttribute("page.datasession");
    String Continuar = "SI";
    String ordenSuspendida = "NO";
%>
<html>
<head>
    <title>Certificación</title>
    <style type="text/css">Body {font-family: Arial, Helv; font-size: 10pt; font-weight: bold; color:#000000}</style>
    <script language="javascript">
        function doSubmit()
        {
          if(document.forms[0].borrarPagina.value=='SI'){
            document.forms[0].action = 'Final.jsp';           
          }
          else if(document.forms[0].liberarOrden.value=='SI'){
            document.forms[0].action = '../../servlet/ventanilla.PageServlet';
          }
          document.forms[0].submit();            
        }
    </script>
</head>
<body bgcolor="#FFFFFF">

<%
    out.print("<form name=\"Txns\" action=\"../../servlet/ventanilla.DataServlet\" method=\"post\">");
    //out.print("<form name=\"Txns\" action="PageBuilder4083.jsp" method=\"post\">");
    String iTxn = (String)session.getAttribute("page.iTxn");    
%>
    <input type="hidden" name="transaction" value="<%= session.getAttribute("page.iTxn") %>">
    <input type="hidden" name="transaction1" value="<%= session.getAttribute("page.oTxn") %>">
<p>
<%
    String statusOrden = new String("");
    if(datasession.get("statusOrden") != null)
      statusOrden = datasession.get("statusOrden").toString();
  
    String szResp = "  ";
    String txtStatus = "";
    ventanilla.GenericClasses gc = new ventanilla.GenericClasses();
    if( resp.size() != 4 ) 
      out.println("<b>Error de conexi&oacute;n</b><br>");
    else
    {
        try
        {
            int codigo = Integer.parseInt((String)resp.elementAt(0));
            
            if( codigo != 0 )
            {
                String msgResp = (String)resp.elementAt(3);
                out.println("<b>Error en transaccion</b><br>");
                if ( datasession.get("cTxn").toString().equals("0860"))
                {
                    out.println(msgResp);  
                    statusOrden = "LIBERADA";
                }
                else
                {
       	           int linesM = Integer.parseInt((String)resp.elementAt(1));
       	           
     	           for(int i=0; i<linesM; i++) 
     	           {
    		       String linea = msgResp.substring( i*78, Math.min(i*78+77, msgResp.length()) );
     		       out.println(getString(linea) + "<p>");
     		       
     		       if(Math.min(i*78+77, msgResp.length()) == msgResp.length())
      		         break;
    	           }
                }
            } 
            else 
            {
                int lines = Integer.parseInt((String)resp.elementAt(1));
                szResp = (String)resp.elementAt(3);
                String itxn = (String)datasession.get("iTxn");
                String txn = (String)datasession.get("cTxn");
                String bBoton = (String)datasession.get("Cancelar.y");
                
                if(txn.equals("0860"))
                {
                    if(szResp.substring(24, 25).equals("0"))
                    {  String  txtStatusOrd ="";
                       String txtMonto="";
                       String txtNomOrd = "";
                       String txtNomBen = "";
                       switch(Integer.parseInt(szResp.substring(247, 248))){
						    case 1: 
						            txtStatusOrd = "EXP";  break;
						    case 2: 
						            txtStatusOrd = "LIQ";  break;
						    case 3: 
						            txtStatusOrd = "CAN";  break;
						    case 4: 
						            txtStatusOrd = "REA";  break;
						    case 5: 
						            txtStatusOrd = "REV";  break;						            
						    case 7: 
						            txtStatusOrd = "NO COINCIDE EL MONTO";  break;
						    case 8: 
						            txtStatusOrd = "STATUS NO VALIDO PARA CAN - LIQ";  break;   
						    case 9: 
						            txtStatusOrd = "NO EXISTE NUMERO DE ORDEN";  break;
					   }
					   if(txtStatusOrd == "EXP" || txtStatusOrd == "REA"){  
                         txtMonto = szResp.substring(312, 327);
                         txtNomOrd = szResp.substring(390,468);
                         txtNomBen = szResp.substring(468);
                       }else{
                         Continuar = "NO";    
                         out.print("<H3>**********Status: " + txtStatusOrd + "**********</H3>");                          
                       }
                        
                  
                        if ( Continuar.equals("SI"))
                        {
                                out.println("<table border=\"0\">");
                                out.println(" <tr>");
                                out.print("  <td>Estatus:</td>" + "<td width=\"10\">&nbsp;</td><td>"  + txtStatusOrd + "</td><td width=\"10\">&nbsp;</td>");
                                out.println(" </tr>");
                                out.println(" <tr>");
                                out.print("  <td>Monto:</td><td width=\"10\">&nbsp;</td><td>" + gc.FormatAmount(txtMonto.trim()) + "</td>");
                                out.println(" </tr>");
                                out.println(" <tr>");
                                out.println("<td><i><b>Ordenante:</b></i></td>");
                                out.println(" </tr>");
                                out.println(" <tr>");
                                out.println(" <td>Nombre:</td><td width=\"10\">&nbsp;</td><td>" + txtNomOrd.trim() + "</td>");
                                out.println(" </tr>");                            
                                out.println(" <tr>");
                                out.println("<td><i><b>Beneficiario:</b></i></td>");
                                out.println(" </tr>");
                                out.println(" <tr>");
                                out.println(" <td>Nombre:</td><td width=\"10\">&nbsp;</td><td>" + txtNomBen.trim() + "</td>");
                                out.println(" </tr>");
                                out.println("</table>");
                                datasession.put("txtMonto", gc.FormatAmount(txtMonto.trim()));
                                datasession.put("txtNomOrd", txtNomOrd.trim());
                                datasession.put("txtNomBen", txtNomBen.trim());
                                datasession.put("txtStatusOrden", txtStatusOrd.substring(0,3));
                                session.setAttribute("page.datasession", datasession);                                  
                                datasession.put("cTxn", "freeOrder");
                                session.setAttribute("page.datasession", datasession);
                                String Txne = (String)session.getAttribute("page.iTxn");
                                if(Txne.equals("0022") || Txne.equals("0128"))
                                  datasession.put("txtStatusOrden", "1");
                                else if(Txne.equals("0023") || Txne.equals("0130"))
                                  datasession.put("txtStatusOrden", "2");
                                  
                                session.setAttribute("page.datasession", datasession);                                
                           
                        }
                    }
                    else
                    {
                        out.print(szResp.trim().substring(10,szResp.length()));
                    }
                }
            }    
                
        }
        catch(NumberFormatException nfe)
        {
          out.println("<b>Error de conexi&oacute;n</b><br>");
        }
    }
    
    Stack flujotxn = (Stack)session.getAttribute("page.flujotxn");
    if(iTxn.equals("4045"))
    {
        if(!flujotxn.empty())
        {
            flujotxn.pop();
            session.setAttribute("page.flujotxn", flujotxn);
            datasession.put("cTxn","4045");
            session.setAttribute("page.datasession", datasession);
        }
    }
%>
  <input type="hidden" name="transaction" value="<%= session.getAttribute("page.iTxn") %>">
  <input type="hidden" name="transaction1" value="<%= session.getAttribute("page.oTxn") %>">
<%
    String isCertificable = (String)session.getAttribute("page.certifField");
    if(!flujotxn.empty())
    {
        out.print("<div align=\"center\">");
        
        if(isCertificable.equals("1"))
          out.println("<a href=\"javascript:top.openDialog('../../servlet/ventanilla.ImprimirServlet', 160, 120, 'top.setPrefs4()', document.Txns)\"><img src=\"imagenes/b_continuar.gif\" border=\"0\"></a>"); // Para Certificación
        else
        {
        }
        
        out.print("</div>");
    }
%>
  <br>
  <% 
  String liberarOrden = new String("SI");
  
  if(Continuar.equals("NO"))
    liberarOrden = new String("NO");
%>
  <input type="hidden" name="liberarOrden" value="NO">
  <input type="hidden" name="borrarPagina" value="NO">
<%
  
if(Continuar.equals("SI"))
{ liberarOrden="SI";
%>
        <a href="javascript:document.forms[0].liberarOrden.value='SI';doSubmit()"><img name="Aceptar" src="../imagenes/b_aceptar.gif" alt="Aceptar" border="0"></a>
<%
}


%>
  <a href="javascript:document.forms[0].borrarPagina.value='SI';document.forms[0].liberarOrden.value='<%=liberarOrden%>';doSubmit()"><img name="Cancelar" src="../imagenes/b_cancelar.gif" alt="Cancelar" border="0"></a>

</form>
</body>
</html>
