<%
//*************************************************************************************************
//             Funcion: JSP que despliega respuesta de txn 0604
//            Elemento: Response0604.jsp
//          Creado por: Alejandro Gonzalez Castro
//      Modificado por: Alejandro Gonzalez Castro
//*************************************************************************************************
// CCN - 4360175 - 03/08/2004 - Ajuste size de ventana de impresion
// CCN - 4360211 - 08/10/2004 - Se elimina mensaje de documentos pendientes por digitalizar
// CCN - 4360275 - 18/02/2005 - WAS 5.1
//*************************************************************************************************
 %>

<%@ page session="true" import="java.util.* , ventanilla.GenericClasses"%>
<%
response.setHeader("Cache-Control","no-store"); //HTTP 1.1
response.setHeader("Pragma","no-cache"); //HTTP 1.0
response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<%!
  private String getString(String s)
  {
    int len = s.length();
    for(; len>0; len--)
	  if(s.charAt(len-1) != ' ')
	    break;
    return s.substring(0, len);
  }

    private String formatMonto(String s)
  {
    int len = s.length();
    if (len < 3)
    	s = "0." + s;
    else
    {
      int n = (len - 3 ) / 3;
    	s = s.substring(0, len - 2) + "." + s.substring(len - 2 , len);
      for (int i = 0; i < n; i++)
      {
      	len = s.length();
    		s = s.substring(0, (len -(i*3+6+i))) + "," + s.substring((len -(i*3+6+i)) , len);
    	}
    }
    return s;
  }
%>
<%
Hashtable datasession= (Hashtable)session.getAttribute("page.datasession");
Vector resp = (Vector)session.getAttribute("response");
  Stack flujotxn = (Stack)session.getAttribute("page.flujotxn");
  GenericClasses gc = new GenericClasses();

//IDDEVA.- Se agrega bandera
String muestraForm = (String)datasession.get("formulariaUAF");
	 	if(null == muestraForm)
	 	   muestraForm = "NO";  
  
if(datasession.get("efectivoCSIB") != null){
  	String strEfectivo = (String)datasession.get("efectivoCSIB")==null?"0.00":(String)datasession.get("efectivoCSIB");	  	
 	long efectivo = Long.parseLong(gc.quitap(strEfectivo));
 	//IDDEVA.-Bandera de muestra formulario UAF
  	if( (efectivo >= 1000000) 
  	 && ( datasession.get("servicio1").toString().equals("60")  || 
  	      datasession.get("servicio1").toString().equals("670") ||  
  	      muestraForm.trim().equals("SI") 
  	      )
  	  )
  	 {	
         flujotxn.add("CSIB");
 	}
}

String montopro;
  if ( (String)datasession.get("MontoTotal") == null)
       montopro = "0";
  else
       montopro = (String)datasession.get("MontoTotal");

String montotot = (String)datasession.get("txtCheque");
if ( montotot != null)
{
   montopro = formatMonto(montopro);
}
%>

<html>
<head>
  <title>Certificación

<%!
   private String getItem(String newCadNum, int newOption)
  {
   int iPIndex = newCadNum.indexOf("CONSECUTIVO:") + 12, NumSaltos, Num = 0, Ban = 0;
   String nCadNum = new String("");
   NumSaltos = newOption;
   while(Num < NumSaltos)
   {
    if(newCadNum.charAt(iPIndex) == 0x20)
    {
     while(newCadNum.charAt(iPIndex) == 0x20)
      iPIndex++;
     Num++;
    }
    else
     while(newCadNum.charAt(iPIndex) != 0x20)
      iPIndex++;
   }
   while(newCadNum.charAt(iPIndex) != 0x20 && iPIndex < newCadNum.length()){
    nCadNum = nCadNum + newCadNum.charAt(iPIndex);
    if(iPIndex < newCadNum.length())
      iPIndex++;
    if(iPIndex == newCadNum.length())
      break;
   }

   return nCadNum;
  }
 %>

  </title>
<style type="text/css">Body {font-family: Arial, Helv; font-size: 10pt; font-weight: bold; color:#000000}</style>
<script  language="JavaScript">
function continuar()
{
    document.Txns.submit();
}

</script>
</head>
<body bgcolor="#FFFFFF">
<form name="Txns" action="../../servlet/ventanilla.PageServlet" method="post">
<table border="0" cellspacing="0">
<tr>
<td>
<%
  String isCertificable = (String)session.getAttribute("page.certifField");  // Para Certificación
  String txtCadImpresion  = ""; // Para Certificación
//  Stack flujotxn = (Stack)session.getAttribute("page.flujotxn");

  String txtCasoEsp = (String)datasession.get("txtCasoEsp");
  if(!flujotxn.empty()){
   out.print("<div align=\"center\">");
    if(!isCertificable.equals("N")){ // Para Certificación
    session.setAttribute("Consecutivo", getItem((String)resp.elementAt(3), 1)); // Para Certificación
    session.setAttribute("Tipo", isCertificable); // Para Certificación
    session.setAttribute("txtCasoEsp",txtCasoEsp);
  //  out.println("<a href=\"#\" onClick=\"top.openDialog('../../servlet/ventanilla.ImprimirServlet?txtCadImpresion=" + txtCadImpresion + "', 160, 120, 'top.setPrefs4()', Txns)\"><img src=\"../imagenes/b_continuar.gif\" border=\"0\"></a>"); // Para Certificación
   }
   else
    out.print("<input type=\"image\" src=\"../imagenes/b_continuar.gif\" alt=\"\" border=\"0\">");
   out.print("</div>");
  }
  else{
   session.setAttribute("Consecutivo", getItem((String)resp.elementAt(3), 1)); // Para Certificación
   session.setAttribute("Tipo", isCertificable); // Para Certificación
   session.setAttribute("txtCasoEsp",txtCasoEsp);
   //out.println("<a href=\"#\" onClick=\"top.openDialog('../../servlet/ventanilla.ImprimirServlet?txtCadImpresion=" + txtCadImpresion + "', 160, 120, 'top.setPrefs4()', Txns)\"><img src=\"../imagenes/b_continuar.gif\" border=\"0\"></a>");
  }
  
%>
</td>
</tr>
<tr>
<td>
<input type="hidden" name="transaction" value="<%= session.getAttribute("page.iTxn") %>">
<%

  if ( montopro.equals(montotot))  {

      out.println("<input type=\"hidden\" name=\"transaction1\" value=\""+ session.getAttribute("page.oTxn")+"\">");
   }
  else{
      out.println("<input type=\"hidden\" name=\"transaction1\" value=\""+ session.getAttribute("page.iTxn")+"\">");
     }
 %>
  <input type="hidden" name="moneda" value="<%= session.getAttribute("moneda") %>">

</td>
</tr>
</table>
</form>
<p>
<%
String codigo = "";
  if( resp.size() != 4 )
   out.println("<b>Error de conexi&oacute;n</b><br>");
  else
  {
   	try
   	{
     	codigo = (String)resp.elementAt(0);
 	if( codigo.equals("1"))
   	   out.println("<b>Transaccion Rechazada</b><br><br>");
        if( codigo.equals("0") || codigo.equals("1") ) {
     	   int lines = Integer.parseInt((String)resp.elementAt(1));
    	   String szResp = (String)resp.elementAt(3);
    	   for(int i=0; i<lines; i++) {
    		   String line = szResp.substring( i*78, Math.min(i*78+77, szResp.length()) );
     		   out.println(getString(line) + "<p>");
     		   if(Math.min(i*78+77, szResp.length()) == szResp.length())
      		    break;
    	   }
       }
       else {
         if ( codigo.equals("3") || codigo.equals("2"))
            out.println("<b>Transacci&oacute;n Requiere Autorizaci&oacute;n</b>");
         else
            out.println("<b>Ocurri&oacute; un Error de Comunicaci&oacute;n, Verifique.</b>");
       }
  	}
  	catch(NumberFormatException nfe){
     out.println("<b>Error de conexi&oacute;n</b><br>");
    }
  }
%>
</body>
</html>
