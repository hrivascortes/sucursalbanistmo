<!--
//*************************************************************************************************
//             Funcion: JSP que presenta las opciones de la txn 0854
//            Elemento: Response0854.jsp
//          Creado por: Alejandro Gonzalez Castro
//		  Modificado por: Juvenal Fernandez
//*************************************************************************************************
// CCN - 4360163 - 06/07/2004 - Se controla click sobre el boton aceptar
// CCN - 4360189 - 03/09/2004 - Se redirecciona a Final.jsp por Control de Efectivo
// CCN - 4360268 - 04/02/2005 - Se realizan adecuaciones para eliminar Agencia NY
// CCN - 4360581 - 12/03/2007 - Se convierten los estilos en jsf y se hace un include en el jsp
//*************************************************************************************************
-->
<%@ page session="true" import="java.util.*"%>
<%
response.setHeader("Cache-Control","no-store"); //HTTP 1.1
response.setHeader("Pragma","no-cache"); //HTTP 1.0
response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<%!
  private String getString(String s)
  {
    int len = s.length();
    for(; len>0; len--)
	  if(s.charAt(len-1) != ' ')
	    break;

    return s.substring(0, len);
  }
%>
<%
  Vector resp = (Vector)session.getAttribute("response");
  Hashtable datasession = (Hashtable)session.getAttribute("page.datasession");
%>
<html>
<head>
  <title>Certificación</title>
  <%@include file="style.jsf"%>
<script language="JavaScript">
function aceptar(forma)
{
	forma.submit();
	var obj = document.getElementById('btnCont')
	.onclick = new Function('return false')	
}

function cancelar(forma)
{
	  document.Txns.action = "../paginas/Final.jsp";
	  document.Txns.submit();
}

</script>
</head>
<body>

<form name="Txns" action="../../servlet/ventanilla.PageServlet" method="post">
<table border="0" cellspacing="0">
<tr>
<td>
</td>
</tr>
<tr>
<td>
  <input type="hidden" name="transaction" value="<%= session.getAttribute("page.iTxn") %>">
  <input type="hidden" name="transaction1" value="<%= session.getAttribute("page.oTxn") %>">
</td>
</tr>
</table>
<p>
<%
   String szResp = new String("");
   int codigo = 1;
   if( resp.size() != 4 )
    out.println("<b>Error de conexi&oacute;n</b><br>");
   else
   {
    try{
     codigo = Integer.parseInt((String)resp.elementAt(0));
    }
    catch(NumberFormatException nfe){
     out.println("<b>Error de conexi&oacute;n</b><br>");
    }
    if( codigo != 0 ) {
      out.println("<b>Error en transaccion</b><br>");
     int lines = Integer.parseInt((String)resp.elementAt(1));
     szResp = (String)resp.elementAt(3);
     for(int i=0; ; i++){
      String line = szResp.substring( i*78, Math.min(i*78+77, szResp.length()) );
      out.println(getString(line) + "<p>");
      if(Math.min(i*78+77, szResp.length()) == szResp.length())
       break;
     }
    }
   }
%>
<%
if (codigo == 0)
{
%>  
	<table border="0" cellspacing="0">
        <tr>
	<td>Servicio:</td>
	<td></td>
	<td  align="right"><%
	 out.print(datasession.get("txtServicio"));
	%></td>
        </tr>
	<tr>
	<td>Referencia:</td>
	<td></td>
	<td  align="right"><%
	 out.print(datasession.get("txtRefer"));
	%></td>
	</tr>
        <tr>
	<td>Monto:</td>
	<td></td>
	<td  align="right"><%
	 out.print(datasession.get("txtMonto"));
	%></td>
	<tr>
	<td>Beneficiario:</td>
	<td></td>
	<td align="right"><%out.print(datasession.get("txtBeneficiario"));%></td>
	</tr>
	<tr>
	<td>Ordenante:</td>
	<td></td>
	<td align="right"><%out.print(datasession.get("txtOrdenante"));%></td>
	</tr>
        <tr>
        </tr>
	<tr>
	<td><a id='btnCont' onclick="this.onclick = new Function('return false');" href="JavaScript:aceptar(document.Txns) "><img src="../imagenes/b_continuar.gif" border="0"></a></td>
	<td><a onclick="this.onclick = new Function('return false');" href="JavaScript:cancelar(document.Txns) "><img src="../imagenes/b_cancelar.gif" border="0"></a></td>
	</tr>
	</table>
<%
}%>
</form>
</body>
</html>