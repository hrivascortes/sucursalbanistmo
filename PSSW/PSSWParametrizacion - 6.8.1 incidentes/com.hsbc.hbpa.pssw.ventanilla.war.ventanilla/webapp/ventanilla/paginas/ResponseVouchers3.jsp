<!-- 
180 
// CCN - 4360581 - 12/03/2007 - Se convierten los estilos en jsf y se hace un include en el jsp
-->

<%@ page session="true" import="java.util.Hashtable"%>
<%
response.setHeader("Cache-Control","no-store"); //HTTP 1.1
response.setHeader("Pragma","no-cache"); //HTTP 1.0
response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<META HTTP-EQUIV="REFRESH" content="5">
<html>
<head>
  <title>Results</title>
  <%@include file="style.jsf"%>
</head>
<Script language="JavaScript">
function chgPag()
{
	document.vouchers.submit();
}
</Script>
<body>
<%
	String status = "*";
	out.println("<br>Procesando Vouchers, por favor espere...");
	Hashtable datos = (Hashtable)session.getAttribute("page.datasession");

	Integer count = (Integer)datos.get("conteo");
	Integer size  = (Integer)datos.get("numero");
	String codet = (String)datos.get("codetxn");
		if (codet.equals("0"))
   		    status = "Aceptado";
   	        else {
                    if (codet.equals("3") || codet.equals("2")) {
                       status = "Autorizacion";%>
                       <script language="javascript">
                          top.openDialog('../paginas/authoriz.jsp', 220, 130, 'top.setPrefs10()', document.entry)
                       </script>
            <%      }
                    else
			status = "Rechazado"; }

			out.println("<br><br>Transacciones procesadas: " + count + " de " + size + " - " + status );
%>
	<form name="vouchers" action="../../servlet/ventanilla.Group03VOU" method="Post">
		<input type="hidden" name="inpSubmit">
                <input type="hidden" name="supervisor" value="<%= session.getAttribute("teller") %>">
                <input type="hidden" name="SignOK" value="0">
	</form>
	<script language="JavaScript" >
		chgPag();
	</script>
</body>
</html>