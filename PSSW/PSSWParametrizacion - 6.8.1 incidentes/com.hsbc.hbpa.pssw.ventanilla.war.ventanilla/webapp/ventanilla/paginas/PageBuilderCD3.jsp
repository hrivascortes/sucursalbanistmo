<!--
// CCN - 4360581 - 12/03/2007 - Se convierten los estilos en jsf y se hace un include en el jsp
-->

<%@ page session="true"%>
<%
response.setHeader("Cache-Control","no-store"); //HTTP 1.1
response.setHeader("Pragma","no-cache"); //HTTP 1.0
response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
    <head>
        <%@include file="style.jsf"%>
        <title>Proceso de Digitalizacion Contigencia</title>
    </head>
    <Script language="JavaScript">   
    var valor = "1"

    function sendservlet()
    {	        
        document.entry.action = "../../servlet/ventanilla.DigitalizarServlet"
        document.entry.submit();
    }
		
    function setValues1()
    {
        valor = document.entry.opcion.options[document.entry.opcion.selectedIndex].value
    }

    </SCRIPT>	
	<body onload="top.setFieldFocus(window.document)">   
		<form name="entry">
                    <input type="hidden" name="cTxn" value="<%= session.getAttribute("page.cTxn") %>">
                    <input type="hidden" name="moneda" value="<%= session.getAttribute("page.moneda")%>">
                    <input type="hidden" name="opcion1" value="3">
                    <table border="0" cellspacing="0">       			
			<b><h3>&nbsp</b></h3>
<%						
			out.println("<b><h3>");
			out.print(session.getAttribute("page.cTxn") + "  " + (String)session.getAttribute("page.txnlabel"));
			out.println("</h3></b>");
%>
			<tr><td>
                            <br>		
			</td></tr>
                        <tr>
                            <td><b>Seleccione : </b></td>
                            <td>
                            <SELECT name=opcion size=1 onChange="setValues1()">
                                <OPTION value=1 selected> Digitalizados del Dia</OPTION>
                                <OPTION value=2>Digitalizados otra Sucursal</OPTION>
                                <OPTION value=3>Digitalizados otra Fecha</OPTION>
                                <OPTION value=4>Mal Validados (Monto Incorrecto)</OPTION>
                                <OPTION value=5>No Digitalizados</OPTION>
                                <OPTION value=6>No Entregados</OPTION>
                            </SELECT>
                            </td>
                        </tr>
			<tr>
                            <td>
                                <a href="JavaScript:sendservlet()"><img src="../imagenes/b_aceptar.gif" border="0"></a>
                            </td>
			</tr>			
		</table>			
		</form>
	</body>
</html> 