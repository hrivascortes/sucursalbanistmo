<!--
//*************************************************************************************************
//             Funcion: JSP que despliega el menu de ventanilla
//            Elemento: rsp0810.jsp
//          Creado por: Alejandro Gonzalez Castro
//      Modificado por: Juvenal R Fernandez V.
//*************************************************************************************************
// CCN - 4360178 - 20/08/2004 - Se agrega boton de cancelar
// CCN - 4360189 - 03/09/2004 - Se redirecciona a Final.jsp por Control de Efectivo
// CCN - 4360275 - 18/02/2005 - WAS 5.1
// CCN - 4360581 - 12/03/2007 - Se convierten los estilos en jsf y se hace un include en el jsp
//*************************************************************************************************
-->

<%@ page session="true" import="ventanilla.com.bital.sfb.Response" %>
<%@ page import="java.util.*, com.bital.util.Common" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<%
response.setHeader("Cache-Control","no-store"); //HTTP 1.1
response.setHeader("Pragma","no-cache"); //HTTP 1.0
response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
%>
<html>
<head>
    <title>Certificación de 0810</title>
    <%@include file="style.jsf"%>
</head>
<%!
  private String unformatCur(String value)
  {
    if( value == null )
      return "000";

    StringBuffer monto = new StringBuffer(value);
    for(int i=0; i<monto.length();)
    {
      if( monto.charAt(i) == ',' )
        monto.deleteCharAt(i);
      else
        ++i;
    }

    return monto.toString();
  }

  private double parseNumber(String number)
  {
    double result;
    try
    {
      result = Double.parseDouble(number);
    }
    catch(NumberFormatException e)
    {
      result = 0.00;
    }

    return result;
  }

  private String calculaIVA(String comision, String iva)
  {
    double result = parseNumber(unformatCur(comision));
    result = result * parseNumber(unformatCur(iva));

    return Common.formatCur(result);
  }

  private String calculaTotal(Hashtable req)
  {
    double result = parseNumber(unformatCur( (String)req.get("txtMonto") ));
    result = result + parseNumber(unformatCur( (String)req.get("txtComision") ));
    result = result + parseNumber(unformatCur( (String)req.get("txtIVA") ));

    return Common.formatCur(result);
  }
%>
<body>
<div align="center">EXP. CHQ. GERENCIA CON PAGO EN EFECTIVO</div>
<p>
<%
  Response rsp = new Response((Vector)session.getAttribute("response"));

  if( !rsp.getCode().equals("0") )
  {
    out.println("<b>ERROR EN LA TRANSACCION</b><br>");

    String[] msg = rsp.getLines();
    for(int i=0; i<msg.length; i++)
      out.println(msg[i] + "<br>");
  }
  else
  {
    String line = rsp.getLine(0);
    Hashtable local = (Hashtable)session.getAttribute("page.datasession");

    String com = null;
    String iva = null;
    String domsuc = null;
    try
    {
      com = line.substring(29, 40);
      iva = line.substring(41);
    }
    catch(StringIndexOutOfBoundsException e1)
    {
      if( com == null )
        com = "000";
      iva = "000";
    }

    com = Common.formatCur(parseNumber(com)/100);
    iva = Common.formatCur(parseNumber(iva)/100);

    line = rsp.getLine(1);
    domsuc = line.trim();

    local.put("txtComision", com);
    local.put("txtIVA", calculaIVA(com, iva));
    local.put("txtDomEsta", domsuc);
%>
<table border="0" cellspacing="0">
<tr>
  <td><strong>Monto</strong></td>
  <td width="10">&nbsp;</td>
  <td align="right"><%= local.get("txtMonto") %></td>
</tr>
<tr>
  <td><strong>Comision</strong></td>
  <td>&nbsp;</td>
  <td align="right"><%= local.get("txtComision") %></td>
</tr>
<tr>
  <td><strong>ITBMS</strong></td>
  <td>&nbsp;</td>
  <td align="right"><%= local.get("txtIVA") %></td>
</tr>

<tr>
  <td><strong>Total</strong></td>
  <td>&nbsp;</td>
  <td align="right"><%= calculaTotal(local) %></td>
</tr>
</table>
<% } %>
<script>
function cancel()
{
    document.Txns.action="../paginas/Final.jsp";
    document.Txns.submit();
}
</script>
<form name="Txns" action="../../servlet/ventanilla.PageServlet" method="post">
<%
  Stack flujotxn = (Stack)session.getAttribute("page.flujotxn");
  if( !flujotxn.empty() )
  {
    out.print("<input type=\"image\" src=\"../imagenes/b_continuar.gif\" alt=\"\" border=\"0\">");
    out.println("<a href=\"javascript:cancel()\" ><img src=\"../imagenes/b_cancelar.gif\" border=\"0\"></a>");
  }
%>
<input type="hidden" name="transaction" value="<%= session.getAttribute("page.iTxn") %>">
<input type="hidden" name="transaction1" value="<%= session.getAttribute("page.oTxn") %>">
</form>
</body>
</html>
