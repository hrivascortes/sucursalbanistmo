<!-- 12 -->

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
 <head>
  <title>Error Page</title>
 </head>
 <body>
  <H3><font color="red">*** Error! ***</font></H3><%
  if(session.getAttribute("page.Builder.Error") != null){
   out.println(session.getAttribute("page.Builder.Error").toString());
   session.removeAttribute("page.Builder.Error");
  }
  if(session.getAttribute("NoError") != null){
   out.println(session.getAttribute("NoError").toString());
   session.removeAttribute("NoError");
  }
  if(session.getAttribute("menu.Builder.Error") != null){
   out.println(session.getAttribute("menu.Builder.Error").toString());
   session.removeAttribute("menu.Builder.Error");
  }
  if(session.getAttribute("page.Builder.missingField.Error") != null){
   java.util.Vector tempVector = (java.util.Vector)session.getAttribute("page.Builder.missingField.Error");
   byte counter = -1;
   out.print("Hacen falta los siguientes campos:<br>");
   while(++counter < tempVector.size()){
    out.print("<br>" + tempVector.get(counter));
   }
   session.removeAttribute("page.Builder.missingField.Error");
  }
  %>
 </body>
</html>
