<!--
//*************************************************************************************************
//             Funcion: JSP que despliega la ayuda
//            Elemento: ResponseCatTxns.jsp
//          Creado por: Israel De Paz Mercado
//	Modificado por: Oscar Lopez Gutierrez
//*************************************************************************************************
// CCN - 4360314 - 06/05/2005 - Se crea jsp para el Catalogo de Txns de PSSW.
// CCN - 4360347 - 22/07/2005 - Se elimina ayuda para la txn 4453
// CCN - 4360364 - 19/08/2005 - Se elimina ayuda para la txn 0530
// CCN - 4360500 - 04/08/2006 - Se incluyen txns para depositadores 0230,0232,0234,0236,5005,5007
// CCN - 4360512 - 08/09/2006 - Se agrega ayuda para la txn 0238
// CCN - 4360581 - 12/03/2007 - Se convierten los estilos en jsf y se hace un include en el jsp
//*************************************************************************************************
-->
<%@ page session="true" import="java.util.*" %>
<% 
response.setHeader("Cache-Control","no-store"); //HTTP 1.1
response.setHeader("Pragma","no-cache"); //HTTP 1.0
response.setDateHeader ("Expires", 0); //prevents caching at the proxy server

%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
<title>Datos de la Transaccion</title>
<%@include file="style.jsf"%>
</head>
<body bgcolor="#FFFFFF" onload="top.setFieldFocus(window.document)">
<form name="Help" >

<h1>Catalogo de Transacciones de PSSW</h1>


<table  align="center" cellspacing='5' border='1'>
<tr><td colspan='7'  align="center" bgcolor="#CCCCCC"><a id="Procesos"><b>P R O C E S O S - D I S P O N I B L E S</b></a></td></tr>
<tr>
    <td align="left"><a href="#Depositos"><b>Depositos</b></a></td>
    <td align="left"><a href="#Retiros"><b>Retiros</b></a></td>
    <td align="left"><a href="#Transferencias"><b>Transferencias</b></a></td>
    <td align="left"><a href="#Compra-Venta"><b>Compra-Venta</b></a></td>
    <td align="left"><a href="#Ordenes de Pago"><b>Ordenes de Pago</b></a></td>
    <td align="left"><a href="#Cheques de Caja"><b>Cheques de Caja</b></a></td>
    <td align="left"><a href="#Giros Citibank"><b>Giros Citibank</b></a></td>
</tr>
<tr>
    <td align="left"><a href="#Western Union"><b>Western Union</b></a></td>
    <td align="left"><a href="#RAP"><b>RAP</b></a></td>
    <td align="left"><a href="#DAP"><b>DAP</b></a></td>
    <td align="left"><a href="#Remesas"><b>Remesas</b></a></td>
    <td align="left"><a href="#Fiduciario"><b>Fiduciario</b></a></td>
    <td align="left"><a href="#Nomina"><b>Nomina</b></a></td>
    <td align="left"><a href="#Pago de Servicios"><b>Pago de Servicios</b></a></td>
</tr>
<tr>
    <td align="left"><a href="#Vouchers"><b>Vouchers</b></a></td>
    <td align="left"><a href="#Inversiones"><b>Inversiones</b></a></td>
    <td align="left"><a href="#Traveler Cheques"><b>Traveler Cheques</b></a></td>
    <td align="left"><a href="#CBA"><b>CBA</b></a></td>
    <td align="left"><a href="#Cheques Certificados"><b>Cheques Certificados</b></a></td>
    <td align="left"><a href="#Avaluos"><b>Avaluos</b></a></td>
    <td align="left"><a href="#Varias"><b>Varias</b></a></td>
</tr>
<tr>
    <td align="left"><a href="#Relaciones"><b>Relaciones</b></a></td>
</tr>
</table>
<p>
<table border='1' align="center">

        <tr>
            <td colspan="6" align="center" bgcolor="#CCCCCC"><a id="DAP"><b>DAP</b></a></td>
            <td align="center"><a href="#Procesos">Procesos</a></td>
        </tr>
        <tr>
            <td align="center" bgcolor="#CCCCCC">Txn</td>
            <td align="center" bgcolor="#CCCCCC">Descripcion</td>
            <td align="center" bgcolor="#CCCCCC">N$</td>
            <td align="center" bgcolor="#CCCCCC">US$</td>
            <td align="center" bgcolor="#CCCCCC">UDI</td>
            <td align="center" bgcolor="#CCCCCC">Caja</td>
        </tr>

        <tr>
            <td align="center">0067</td>
            <td align="left">Pago Devoluci�n de Impuestos</td>
            <td align="center">S</td>
            <td align="center">N</td>
            <td align="center">N</td>
            <td align="center">N</td>
        </tr>

        <tr>
            <td align="center">4011</td>
            <td align="left">Cargo por dispersi�n con Cheque de Caja</td>
            <td align="center">S</td>
            <td align="center">S</td>
            <td align="center">N</td>
            <td align="center">N</td>
        </tr>

        <tr>
            <td align="center">4529</td>
            <td align="left">Cargo por dispersi�n de efectivo / Personas F�sicas</td>
            <td align="center">S</td>
            <td align="center">S</td>
            <td align="center">N</td>
            <td align="center">S</td>
        </tr>

        <tr>
            <td align="center">4531</td>
            <td align="left">Cargo por dispersi�n a cuenta de cheques</td>
            <td align="center">S</td>
            <td align="center">S</td>
            <td align="center">N</td>
            <td align="center">N</td>
        </tr>

        <tr>
            <td align="center">5561</td>
            <td align="left">Cargo por Liquidacion de TIP</td>
            <td align="center">S</td>
            <td align="center">S</td>
            <td align="center">N</td>
            <td align="center">N</td>
        </tr>

        <tr>
            <td align="center">5565</td>
            <td align="left">Liquidacion TIP en efectivo</td>
            <td align="center">S</td>
            <td align="center">S</td>
            <td align="center">N</td>
            <td align="center">N</td>
        </tr>


        <tr>
            <td colspan="6" align="center" bgcolor="#CCCCCC"><a id="Ordenes de Pago"><b>Ordenes de Pago</b></a></td>
            <td align="center"><a href="#Procesos">Procesos</a></td>
        </tr>
        <tr>
            <td align="center" bgcolor="#CCCCCC">Txn</td>
            <td align="center" bgcolor="#CCCCCC">Descripcion</td>
            <td align="center" bgcolor="#CCCCCC">N$</td>
            <td align="center" bgcolor="#CCCCCC">US$</td>
            <td align="center" bgcolor="#CCCCCC">UDI</td>
            <td align="center" bgcolor="#CCCCCC">Caja</td>
        </tr>

        <tr>
            <td align="center">0021</td>
            <td align="left">Expedici�n de Orden de Pago</td>
            <td align="center">S</td>
            <td align="center">S</td>
            <td align="center">N</td>
            <td align="center">N</td>
        </tr>

        <tr>
            <td align="center">0022</td>
            <td align="left">Liquidaci�n de Orden de Pago</td>
            <td align="center">S</td>
            <td align="center">N</td>
            <td align="center">N</td>
            <td align="center">N</td>
        </tr>

        <tr>
            <td align="center">0023</td>
            <td align="left">Cancelaci�n / Liquidaci�n Orden de Pago Suspendida</td>
            <td align="center">S</td>
            <td align="center">S</td>
            <td align="center">N</td>
            <td align="center">N</td>
        </tr>

        <tr>
            <td align="center">0587</td>
            <td align="left">Retransmisi�n O.P. otros Bancos</td>
            <td align="center">S</td>
            <td align="center">N</td>
            <td align="center">N</td>
            <td align="center">N</td>
        </tr>

        <tr>
            <td align="center">4045</td>
            <td align="left">Suspensi�n de Orden de Pago</td>
            <td align="center">S</td>
            <td align="center">N</td>
            <td align="center">N</td>
            <td align="center">N</td>
        </tr>

        <tr>
            <td align="center">9099</td>
            <td align="left">Relaci�n de Retransmitidos</td>
            <td align="center">S</td>
            <td align="center">N</td>
            <td align="center">N</td>
            <td align="center">N</td>
        </tr>


        <tr>
            <td colspan="6" align="center" bgcolor="#CCCCCC"><a id="CBA"><b>CBA</b></a></td>
            <td align="center"><a href="#Procesos">Procesos</a></td>
        </tr>
        <tr>
            <td align="center" bgcolor="#CCCCCC">Txn</td>
            <td align="center" bgcolor="#CCCCCC">Descripcion</td>
            <td align="center" bgcolor="#CCCCCC">N$</td>
            <td align="center" bgcolor="#CCCCCC">US$</td>
            <td align="center" bgcolor="#CCCCCC">UDI</td>
            <td align="center" bgcolor="#CCCCCC">Caja</td>
        </tr>

        <tr>
            <td align="center">5505</td>
            <td align="left">Cargo a Cuenta CBA</td>
            <td align="center">N</td>
            <td align="center">S</td>
            <td align="center">N</td>
            <td align="center">N</td>
        </tr>

        <tr>
            <td align="center">5509</td>
            <td align="left">Cargo a Cuenta Nueva York</td>
            <td align="center">N</td>
            <td align="center">S</td>
            <td align="center">N</td>
            <td align="center">N</td>
        </tr>


        <tr>
            <td colspan="6" align="center" bgcolor="#CCCCCC"><a id="Cheques de Caja"><b>Cheques de Caja</b></a></td>
            <td align="center"><a href="#Procesos">Procesos</a></td>
        </tr>
        <tr>
            <td align="center" bgcolor="#CCCCCC">Txn</td>
            <td align="center" bgcolor="#CCCCCC">Descripcion</td>
            <td align="center" bgcolor="#CCCCCC">N$</td>
            <td align="center" bgcolor="#CCCCCC">US$</td>
            <td align="center" bgcolor="#CCCCCC">UDI</td>
            <td align="center" bgcolor="#CCCCCC">Caja</td>
        </tr>

        <tr>
            <td align="center">0061</td>
            <td align="left">Cargo por expedici�n de cheque de caja</td>
            <td align="center">S</td>
            <td align="center">S</td>
            <td align="center">N</td>
            <td align="center">N</td>
        </tr>

        <tr>
            <td align="center">0065</td>
            <td align="left">Cancelaci�n pago cheque caja suspendido</td>
            <td align="center">S</td>
            <td align="center">S</td>
            <td align="center">N</td>
            <td align="center">N</td>
        </tr>

        <tr>
            <td align="center">4041</td>
            <td align="left">Cheque de caja recibido otros bancos</td>
            <td align="center">S</td>
            <td align="center">S</td>
            <td align="center">N</td>
            <td align="center">N</td>
        </tr>

        <tr>
            <td align="center">4107</td>
            <td align="left">Cancelaci�n cheque de caja para liquidaci�n de obligaciones</td>
            <td align="center">S</td>
            <td align="center">S</td>
            <td align="center">N</td>
            <td align="center">N</td>
        </tr>

        <tr>
            <td align="center">4145</td>
            <td align="left">Suspensi�n de cheque de caja</td>
            <td align="center">S</td>
            <td align="center">S</td>
            <td align="center">N</td>
            <td align="center">N</td>
        </tr>

        <tr>
            <td align="center">4151</td>
            <td align="left">Cheque Recibido de otros Bancos Locales</td>
            <td align="center">S</td>
            <td align="center">N</td>
            <td align="center">N</td>
            <td align="center">N</td>
        </tr>

        <tr>
            <td align="center">4153</td>
            <td align="left">Cheque de caja pagado en efectivo</td>
            <td align="center">S</td>
            <td align="center">S</td>
            <td align="center">N</td>
            <td align="center">N</td>
        </tr>

        <tr>
            <td align="center">4155</td>
            <td align="left">Expedici�n Cheque de Caja con Pago en Efectivo</td>
            <td align="center">S</td>
            <td align="center">S</td>
            <td align="center">N</td>
            <td align="center">N</td>
        </tr>

        <tr>
            <td align="center">4157</td>
            <td align="left">Expedici�n cheque de caja por liquidaci�n de obligaciones</td>
            <td align="center">S</td>
            <td align="center">S</td>
            <td align="center">N</td>
            <td align="center">N</td>
        </tr>


        <tr>
            <td colspan="6" align="center" bgcolor="#CCCCCC"><a id="Compra-Venta"><b>Compra-Venta</b></a></td>
            <td align="center"><a href="#Procesos">Procesos</a></td>
        </tr>
        <tr>
            <td align="center" bgcolor="#CCCCCC">Txn</td>
            <td align="center" bgcolor="#CCCCCC">Descripcion</td>
            <td align="center" bgcolor="#CCCCCC">N$</td>
            <td align="center" bgcolor="#CCCCCC">US$</td>
            <td align="center" bgcolor="#CCCCCC">UDI</td>
            <td align="center" bgcolor="#CCCCCC">Caja</td>
        </tr>

        <tr>
            <td align="center">C726</td>
            <td align="left">Ajuste de Centavos</td>
            <td align="center">S</td>
            <td align="center">S</td>
            <td align="center">N</td>
            <td align="center">N</td>
        </tr>

        <tr>
            <td align="center">0104</td>
            <td align="left">Cancelaci�n giro US$ CitiBank</td>
            <td align="center">N</td>
            <td align="center">S</td>
            <td align="center">N</td>
            <td align="center">N</td>
        </tr>

        <tr>
            <td align="center">0108</td>
            <td align="left">Cobro inmediato</td>
            <td align="center">S</td>
            <td align="center">S</td>
            <td align="center">N</td>
            <td align="center">N</td>
        </tr>

        <tr>
            <td align="center">0112</td>
            <td align="left">Orden de Pago</td>
            <td align="center">S</td>
            <td align="center">N</td>
            <td align="center">N</td>
            <td align="center">N</td>
        </tr>

        <tr>
            <td align="center">0114</td>
            <td align="left">Cancelacion Orden de Pago</td>
            <td align="center">S</td>
            <td align="center">N</td>
            <td align="center">N</td>
            <td align="center">N</td>
        </tr>

        <tr>
            <td align="center">0600</td>
            <td align="left">Remesas</td>
            <td align="center">S</td>
            <td align="center">S</td>
            <td align="center">N</td>
            <td align="center">N</td>
        </tr>

        <tr>
            <td align="center">0726</td>
            <td align="left">Efectivo</td>
            <td align="center">S</td>
            <td align="center">S</td>
            <td align="center">N</td>
            <td align="center">N</td>
        </tr>

        <tr>
            <td align="center">4043</td>
            <td align="left">Cargo a cuenta de cheques</td>
            <td align="center">S</td>
            <td align="center">S</td>
            <td align="center">N</td>
            <td align="center">N</td>
        </tr>

        <tr>
            <td align="center">4057</td>
            <td align="left">Cheque de Caja</td>
            <td align="center">S</td>
            <td align="center">S</td>
            <td align="center">N</td>
            <td align="center">N</td>
        </tr>

        <tr>
            <td align="center">4059</td>
            <td align="left">Cheque Personal</td>
            <td align="center">S</td>
            <td align="center">S</td>
            <td align="center">N</td>
            <td align="center">N</td>
        </tr>

        <tr>
            <td align="center">4063</td>
            <td align="left">Cancelacion Cheque de Caja</td>
            <td align="center">S</td>
            <td align="center">S</td>
            <td align="center">N</td>
            <td align="center">N</td>
        </tr>


        <tr>
            <td colspan="6" align="center" bgcolor="#CCCCCC"><a id="Fiduciario"><b>Fiduciario</b></a></td>
            <td align="center"><a href="#Procesos">Procesos</a></td>
        </tr>
        <tr>
            <td align="center" bgcolor="#CCCCCC">Txn</td>
            <td align="center" bgcolor="#CCCCCC">Descripcion</td>
            <td align="center" bgcolor="#CCCCCC">N$</td>
            <td align="center" bgcolor="#CCCCCC">US$</td>
            <td align="center" bgcolor="#CCCCCC">UDI</td>
            <td align="center" bgcolor="#CCCCCC">Caja</td>
        </tr>

        <tr>
            <td align="center">1181</td>
            <td align="left">Ingresos Fiduciario Efectivo y/o Documentos</td>
            <td align="center">S</td>
            <td align="center">S</td>
            <td align="center">N</td>
            <td align="center">S</td>
        </tr>

        <tr>
            <td align="center">1183</td>
            <td align="left">Ingresos Fiduciario Cobro Inmediato</td>
            <td align="center">S</td>
            <td align="center">S</td>
            <td align="center">N</td>
            <td align="center">S</td>
        </tr>

        <tr>
            <td align="center">1185</td>
            <td align="left">Cargo por Operaci�n Fiduciaria</td>
            <td align="center">S</td>
            <td align="center">S</td>
            <td align="center">N</td>
            <td align="center">S</td>
        </tr>

        <tr>
            <td align="center">1189</td>
            <td align="left">Egresos Fiduciario con Cheque de Caja</td>
            <td align="center">S</td>
            <td align="center">S</td>
            <td align="center">N</td>
            <td align="center">S</td>
        </tr>

        <tr>
            <td align="center">1387</td>
            <td align="left">Cargo a Fideicomisos o Mandatos</td>
            <td align="center">S</td>
            <td align="center">S</td>
            <td align="center">N</td>
            <td align="center">S</td>
        </tr>

        <tr>
            <td colspan="6" align="center" bgcolor="#CCCCCC"><a id="Transferencias"><b>Transferencias</b></a></td>
            <td align="center"><a href="#Procesos">Procesos</a></td>
        </tr>
        <tr>
            <td align="center" bgcolor="#CCCCCC">Txn</td>
            <td align="center" bgcolor="#CCCCCC">Descripcion</td>
            <td align="center" bgcolor="#CCCCCC">N$</td>
            <td align="center" bgcolor="#CCCCCC">US$</td>
            <td align="center" bgcolor="#CCCCCC">UDI</td>
            <td align="center" bgcolor="#CCCCCC">Caja</td>
        </tr>

        <tr>
            <td align="center">0041</td>
            <td align="left">Transferencia de Vista a Vista</td>
            <td align="center">S</td>
            <td align="center">S</td>
            <td align="center">N</td>
            <td align="center">S</td>
        </tr>

        <tr>
            <td align="center">0043</td>
            <td align="left">Transferencia de Vista a Plazo</td>
            <td align="center">S</td>
            <td align="center">S</td>
            <td align="center">N</td>
            <td align="center">S</td>
        </tr>

        <tr>
            <td align="center">0044</td>
            <td align="left">Transferencia de Plazo a Vista</td>
            <td align="center">S</td>
            <td align="center">S</td>
            <td align="center">N</td>
            <td align="center">S</td>
        </tr>

        <tr>
            <td align="center">0047</td>
            <td align="left">Transferencia de Vista a UDIs</td>
            <td align="center">S</td>
            <td align="center">N</td>
            <td align="center">N</td>
            <td align="center">S</td>
        </tr>

        <tr>
            <td align="center">0051</td>
            <td align="left">Pago de Seguro Accidentes Personales</td>
            <td align="center">S</td>
            <td align="center">N</td>
            <td align="center">N</td>
            <td align="center">S</td>
        </tr>


        <tr>
            <td colspan="6" align="center" bgcolor="#CCCCCC"><a id="Remesas"><b>Remesas</b></a></td>
            <td align="center"><a href="#Procesos">Procesos</a></td>
        </tr>
        <tr>
            <td align="center" bgcolor="#CCCCCC">Txn</td>
            <td align="center" bgcolor="#CCCCCC">Descripcion</td>
            <td align="center" bgcolor="#CCCCCC">N$</td>
            <td align="center" bgcolor="#CCCCCC">US$</td>
            <td align="center" bgcolor="#CCCCCC">UDI</td>
            <td align="center" bgcolor="#CCCCCC">Caja</td>
        </tr>

        <tr>
            <td align="center">4487</td>
            <td align="left">Cargo devolucion remesas de cobranza</td>
            <td align="center">S</td>
            <td align="center">S</td>
            <td align="center">N</td>
            <td align="center">S</td>
        </tr>

        <tr>
            <td align="center">5103</td>
            <td align="left">Depositos remesas</td>
            <td align="center">S</td>
            <td align="center">S</td>
            <td align="center">N</td>
            <td align="center">S</td>
        </tr>

        <tr>
            <td align="center">5261</td>
            <td align="left">Cargo por devolucion de remesas</td>
            <td align="center">S</td>
            <td align="center">S</td>
            <td align="center">N</td>
            <td align="center">S</td>
        </tr>


        <tr>
            <td colspan="6" align="center" bgcolor="#CCCCCC"><a id="Nomina"><b>Nomina</b></a></td>
            <td align="center"><a href="#Procesos">Procesos</a></td>
        </tr>
        <tr>
            <td align="center" bgcolor="#CCCCCC">Txn</td>
            <td align="center" bgcolor="#CCCCCC">Descripcion</td>
            <td align="center" bgcolor="#CCCCCC">N$</td>
            <td align="center" bgcolor="#CCCCCC">US$</td>
            <td align="center" bgcolor="#CCCCCC">UDI</td>
            <td align="center" bgcolor="#CCCCCC">Caja</td>
        </tr>

        <tr>
            <td align="center">M006</td>
            <td align="left">Disquete Pago con Carta</td>
            <td align="center">S</td>
            <td align="center">N</td>
            <td align="center">N</td>
            <td align="center">N</td>
        </tr>

        <tr>
            <td align="center">M007</td>
            <td align="left">Disquete Pago con Cheque</td>
            <td align="center">S</td>
            <td align="center">N</td>
            <td align="center">N</td>
            <td align="center">N</td>
        </tr>

        <tr>
            <td align="center">1019</td>
            <td align="left">Abono por Deposito de Nomina</td>
            <td align="center">S</td>
            <td align="center">N</td>
            <td align="center">N</td>
            <td align="center">N</td>
        </tr>

        <tr>
            <td align="center">4227</td>
            <td align="left">Listado Pago con Carta</td>
            <td align="center">S</td>
            <td align="center">N</td>
            <td align="center">N</td>
            <td align="center">N</td>
        </tr>

        <tr>
            <td align="center">4229</td>
            <td align="left">Listado Pago con Cheque</td>
            <td align="center">S</td>
            <td align="center">N</td>
            <td align="center">N</td>
            <td align="center">N</td>
        </tr>


        <tr>
            <td colspan="6" align="center" bgcolor="#CCCCCC"><a id="Depositos"><b>Depositos</b></a></td>
            <td align="center"><a href="#Procesos">Procesos</a></td>
        </tr>
        <tr>
            <td align="center" bgcolor="#CCCCCC">Txn</td>
            <td align="center" bgcolor="#CCCCCC">Descripcion</td>
            <td align="center" bgcolor="#CCCCCC">N$</td>
            <td align="center" bgcolor="#CCCCCC">US$</td>
            <td align="center" bgcolor="#CCCCCC">UDI</td>
            <td align="center" bgcolor="#CCCCCC">Caja</td>
        </tr>

        <tr>
            <td align="center">1001</td>
            <td align="left">Dep�sito cuenta nueva</td>
            <td align="center">S</td>
            <td align="center">S</td>
            <td align="center">N</td>
            <td align="center">S</td>
        </tr>

        <tr>
            <td align="center">1003</td>
            <td align="left">Dep�sito Mixto</td>
            <td align="center">S</td>
            <td align="center">S</td>
            <td align="center">N</td>
            <td align="center">S</td>
        </tr>

        <tr>
            <td align="center">1005</td>
            <td align="left">Dep�sito instrucciones especiales</td>
            <td align="center">S</td>
            <td align="center">S</td>
            <td align="center">N</td>
            <td align="center">S</td>
        </tr>

        <tr>
            <td align="center">1009</td>
            <td align="left">Dep�sito con documentos cobro inmediato</td>
            <td align="center">S</td>
            <td align="center">S</td>
            <td align="center">N</td>
            <td align="center">S</td>
        </tr>

        <tr>
            <td align="center">1073</td>
            <td align="left">Dep�sito por reapertura de cuenta</td>
            <td align="center">S</td>
            <td align="center">S</td>
            <td align="center">N</td>
            <td align="center">S</td>
        </tr>

        <tr>
            <td align="center">1191</td>
            <td align="left">Abono por cancelaci�n cuenta con saldo negativo</td>
            <td align="center">S</td>
            <td align="center">S</td>
            <td align="center">N</td>
            <td align="center">S</td>
        </tr>

        <tr>
            <td align="center">4009</td>
            <td align="left">Deposito Global Cobro Inmediato</td>
            <td align="center">S</td>
            <td align="center">S</td>
            <td align="center">N</td>
            <td align="center">S</td>
        </tr>

        <tr>
            <td align="center">4503</td>
            <td align="left">Abono por cobro de comisi�n indebido</td>
            <td align="center">S</td>
            <td align="center">S</td>
            <td align="center">N</td>
            <td align="center">S</td>
        </tr>
        
        <tr>
            <td align="center">5005</td>
            <td align="left">Bonificacion por aclaracion de deposito a cuenta <%=(String)session.getAttribute("identidadApp")%>/RAP</td>
            <td align="center">S</td>
            <td align="center">N</td>
            <td align="center">N</td>
            <td align="center">S</td>
        </tr>        

        <tr>
            <td align="center">5023</td>
            <td align="left">Abono por tarjeta de debito</td>
            <td align="center">S</td>
            <td align="center">S</td>
            <td align="center">N</td>
            <td align="center">S</td>
        </tr>

        <tr>
            <td align="center">5603</td>
            <td align="left">Abono por orden de pago recibida por otros bancos</td>
            <td align="center">S</td>
            <td align="center">N</td>
            <td align="center">N</td>
            <td align="center">S</td>
        </tr>

        <tr>
            <td align="center">5901</td>
            <td align="left">Abono por transferencia para apertura de cuenta</td>
            <td align="center">S</td>
            <td align="center">S</td>
            <td align="center">N</td>
            <td align="center">S</td>
        </tr>

        <tr>
            <td align="center">5903</td>
            <td align="left">Dep�sito miscel�neo</td>
            <td align="center">S</td>
            <td align="center">S</td>
            <td align="center">N</td>
            <td align="center">S</td>
        </tr>


        <tr>
            <td colspan="6" align="center" bgcolor="#CCCCCC"><a id="Western Union"><b>Western Union</b></a></td>
            <td align="center"><a href="#Procesos">Procesos</a></td>
        </tr>
        <tr>
            <td align="center" bgcolor="#CCCCCC">Txn</td>
            <td align="center" bgcolor="#CCCCCC">Descripcion</td>
            <td align="center" bgcolor="#CCCCCC">N$</td>
            <td align="center" bgcolor="#CCCCCC">US$</td>
            <td align="center" bgcolor="#CCCCCC">UDI</td>
            <td align="center" bgcolor="#CCCCCC">Caja</td>
        </tr>

        <tr>
            <td align="center">0020</td>
            <td align="left">Western Union (Dinero en Minutos)</td>
            <td align="center">S</td>
            <td align="center">N</td>
            <td align="center">N</td>
            <td align="center">S</td>
        </tr>


        <tr>
            <td colspan="6" align="center" bgcolor="#CCCCCC"><a id="Retiros"><b>Retiros</b></a></td>
            <td align="center"><a href="#Procesos">Procesos</a></td>
        </tr>
        <tr>
            <td align="center" bgcolor="#CCCCCC">Txn</td>
            <td align="center" bgcolor="#CCCCCC">Descripcion</td>
            <td align="center" bgcolor="#CCCCCC">N$</td>
            <td align="center" bgcolor="#CCCCCC">US$</td>
            <td align="center" bgcolor="#CCCCCC">UDI</td>
            <td align="center" bgcolor="#CCCCCC">Caja</td>
        </tr>

        <tr>
            <td align="center">1027</td>
            <td align="left">Cargo sobre Importe Suspendido</td>
            <td align="center">S</td>
            <td align="center">S</td>
            <td align="center">N</td>
            <td align="center">S</td>
        </tr>

        <tr>
            <td align="center">1051</td>
            <td align="left">Cheque recibido de otros bancos locales</td>
            <td align="center">S</td>
            <td align="center">S</td>
            <td align="center">N</td>
            <td align="center">S</td>
        </tr>

        <tr>
            <td align="center">1053</td>
            <td align="left">Cheque pagado, retiro en efectivo</td>
            <td align="center">S</td>
            <td align="center">S</td>
            <td align="center">N</td>
            <td align="center">S</td>
        </tr>

        <tr>
            <td align="center">1057</td>
            <td align="left">Retiro por Instrucciones Especiales</td>
            <td align="center">S</td>
            <td align="center">S</td>
            <td align="center">N</td>
            <td align="center">S</td>
        </tr>

        <tr>
            <td align="center">1075</td>
            <td align="left">Cargo sobre cuenta bloqueda</td>
            <td align="center">S</td>
            <td align="center">S</td>
            <td align="center">N</td>
            <td align="center">S</td>
        </tr>

        <tr>
            <td align="center">1077</td>
            <td align="left">Cargo sobre fondos congelados</td>
            <td align="center">S</td>
            <td align="center">S</td>
            <td align="center">N</td>
            <td align="center">S</td>
        </tr>

        <tr>
            <td align="center">1079</td>
            <td align="left">Pago de cheques sobregirando la cuenta</td>
            <td align="center">S</td>
            <td align="center">S</td>
            <td align="center">N</td>
            <td align="center">S</td>
        </tr>

        <tr>
            <td align="center">1081</td>
            <td align="left">Pago de cheque perdido o robado</td>
            <td align="center">S</td>
            <td align="center">S</td>
            <td align="center">N</td>
            <td align="center">S</td>
        </tr>

        <tr>
            <td align="center">1091</td>
            <td align="left">Cancelaci�n forzada cuenta con saldo positivo</td>
            <td align="center">S</td>
            <td align="center">S</td>
            <td align="center">N</td>
            <td align="center">S</td>
        </tr>

        <tr>
            <td align="center">1093</td>
            <td align="left">Pago de cheque suspendido</td>
            <td align="center">S</td>
            <td align="center">S</td>
            <td align="center">N</td>
            <td align="center">S</td>
        </tr>

        <tr>
            <td align="center">1179</td>
            <td align="left">Cargo sobre importe bloqueado</td>
            <td align="center">S</td>
            <td align="center">S</td>
            <td align="center">N</td>
            <td align="center">S</td>
        </tr>

        <tr>
            <td align="center">1193</td>
            <td align="left">Cancelaci�n de cuenta con pago de intereses</td>
            <td align="center">S</td>
            <td align="center">S</td>
            <td align="center">N</td>
            <td align="center">S</td>
        </tr>

        <tr>
            <td align="center">1195</td>
            <td align="left">Cancelaci�n de cuenta sin pago de intereses</td>
            <td align="center">S</td>
            <td align="center">S</td>
            <td align="center">N</td>
            <td align="center">S</td>
        </tr>

        <tr>
            <td align="center">1197</td>
            <td align="left">Retiros Miscelaneos</td>
            <td align="center">S</td>
            <td align="center">S</td>
            <td align="center">N</td>
            <td align="center">S</td>
        </tr>

        <tr>
            <td align="center">1207</td>
            <td align="left">Cargo por dotaci�n de chequera</td>
            <td align="center">S</td>
            <td align="center">S</td>
            <td align="center">N</td>
            <td align="center">S</td>
        </tr>

        <tr>
            <td align="center">1240</td>
            <td align="left">Disposici�n en efectivo tarjeta de d�bito</td>
            <td align="center">S</td>
            <td align="center">S</td>
            <td align="center">N</td>
            <td align="center">S</td>
        </tr>

        <tr>
            <td align="center">1275</td>
            <td align="left">Comisi�n por transferencia de fondos</td>
            <td align="center">S</td>
            <td align="center">S</td>
            <td align="center">N</td>
            <td align="center">S</td>
        </tr>

        <tr>
            <td align="center">1523</td>
            <td align="left">Comisiones miscel�neos</td>
            <td align="center">S</td>
            <td align="center">S</td>
            <td align="center">N</td>
            <td align="center">S</td>
        </tr>

        <tr>
            <td align="center">1550</td>
            <td align="left">Comisi�n por dep�sito voucher T.D.C.</td>
            <td align="center">S</td>
            <td align="center">S</td>
            <td align="center">N</td>
            <td align="center">S</td>
        </tr>

        <tr>
            <td align="center">4251</td>
            <td align="left">Cheque recibido de otros bancos locales</td>
            <td align="center">S</td>
            <td align="center">N</td>
            <td align="center">N</td>
            <td align="center">S</td>
        </tr>

        <tr>
            <td align="center">4271</td>
            <td align="left">Cheque recibido de otros bancos foraneos</td>
            <td align="center">S</td>
            <td align="center">N</td>
            <td align="center">N</td>
            <td align="center">S</td>
        </tr>

        <tr>
            <td align="center">5007</td>
            <td align="left">Cargo por aclaracion de deposito a cuenta <%=(String)session.getAttribute("identidadApp")%>/RAP</td>
            <td align="center">S</td>
            <td align="center">N</td>
            <td align="center">N</td>
            <td align="center">S</td>
        </tr>

        <tr>
            <td align="center">5051</td>
            <td align="left">Cheque recibido de otros bancos</td>
            <td align="center">S</td>
            <td align="center">S</td>
            <td align="center">N</td>
            <td align="center">S</td>
        </tr>

        <tr>
            <td align="center">5357</td>
            <td align="left">Retiro en efectivo con ficha m�ltiple</td>
            <td align="center">S</td>
            <td align="center">S</td>
            <td align="center">N</td>
            <td align="center">S</td>
        </tr>

        <tr>
            <td align="center">5487</td>
            <td align="left">Cargo por dev. cheque cobro inm. dep.</td>
            <td align="center">S</td>
            <td align="center">S</td>
            <td align="center">N</td>
            <td align="center">S</td>
        </tr>

        <tr>
            <td align="center">5571</td>
            <td align="left">Cargo Dotaciones de Efectivo</td>
            <td align="center">S</td>
            <td align="center">S</td>
            <td align="center">N</td>
            <td align="center">S</td>
        </tr>

        <tr>
            <td align="center">5573</td>
            <td align="left">Cheque Pago de Dotaciones de Efectivo</td>
            <td align="center">S</td>
            <td align="center">S</td>
            <td align="center">N</td>
            <td align="center">S</td>
        </tr>


        <tr>
            <td colspan="6" align="center" bgcolor="#CCCCCC"><a id="Giros Citibank"><b>Giros Citibank</b></a></td>
            <td align="center"><a href="#Procesos">Procesos</a></td>
        </tr>
        <tr>
            <td align="center" bgcolor="#CCCCCC">Txn</td>
            <td align="center" bgcolor="#CCCCCC">Descripcion</td>
            <td align="center" bgcolor="#CCCCCC">N$</td>
            <td align="center" bgcolor="#CCCCCC">US$</td>
            <td align="center" bgcolor="#CCCCCC">UDI</td>
            <td align="center" bgcolor="#CCCCCC">Caja</td>
        </tr>

        <tr>
            <td align="center">0031</td>
            <td align="left">Expedici�n de Giros CitiBank</td>
            <td align="center">N</td>
            <td align="center">S</td>
            <td align="center">N</td>
            <td align="center">S</td>
        </tr>

        <tr>
            <td align="center">0033</td>
            <td align="left">Cancelaci�n de Giros CitiBank</td>
            <td align="center">N</td>
            <td align="center">S</td>
            <td align="center">N</td>
            <td align="center">S</td>
        </tr>

        <tr>
            <td align="center">0037</td>
            <td align="left">Impresi�n de Giros Divisas</td>
            <td align="center">N</td>
            <td align="center">S</td>
            <td align="center">N</td>
            <td align="center">S</td>
        </tr>


        <tr>
            <td colspan="6" align="center" bgcolor="#CCCCCC"><a id="RAP"><b>RAP</b></a></td>
            <td align="center"><a href="#Procesos">Procesos</a></td>
        </tr>
        <tr>
            <td align="center" bgcolor="#CCCCCC">Txn</td>
            <td align="center" bgcolor="#CCCCCC">Descripcion</td>
            <td align="center" bgcolor="#CCCCCC">N$</td>
            <td align="center" bgcolor="#CCCCCC">US$</td>
            <td align="center" bgcolor="#CCCCCC">UDI</td>
            <td align="center" bgcolor="#CCCCCC">Caja</td>
        </tr>

        <tr>
            <td align="center">RAPM</td>
            <td align="left">Cobranza Personalizada</td>
            <td align="center">S</td>
            <td align="center">S</td>
            <td align="center">N</td>
            <td align="center"> </td>
        </tr>

        <tr>
            <td align="center">0029</td>
            <td align="left">Pago de Electronico de Impuestos SAT</td>
            <td align="center">S</td>
            <td align="center">N</td>
            <td align="center">N</td>
            <td align="center">N</td>
        </tr>

        <tr>
            <td align="center">5503</td>
            <td align="left">Dep�sito Cobranza Especial</td>
            <td align="center">S</td>
            <td align="center">S</td>
            <td align="center">N</td>
            <td align="center">S</td>
        </tr>


        <tr>
            <td colspan="6" align="center" bgcolor="#CCCCCC"><a id="Avaluos"><b>Avaluos</b></a></td>
            <td align="center"><a href="#Procesos">Procesos</a></td>
        </tr>
        <tr>
            <td align="center" bgcolor="#CCCCCC">Txn</td>
            <td align="center" bgcolor="#CCCCCC">Descripcion</td>
            <td align="center" bgcolor="#CCCCCC">N$</td>
            <td align="center" bgcolor="#CCCCCC">US$</td>
            <td align="center" bgcolor="#CCCCCC">UDI</td>
            <td align="center" bgcolor="#CCCCCC">Caja</td>
        </tr>

        <tr>
            <td align="center">0606</td>
            <td align="left">Pago por Aval�o en Efectivo</td>
            <td align="center">S</td>
            <td align="center">N</td>
            <td align="center">N</td>
            <td align="center">S</td>
        </tr>

        <tr>
            <td align="center">1641</td>
            <td align="left">Pago por Aval�o Cargo a Cuenta</td>
            <td align="center">S</td>
            <td align="center">N</td>
            <td align="center">N</td>
            <td align="center">S</td>
        </tr>


        <tr>
            <td colspan="6" align="center" bgcolor="#CCCCCC"><a id="Pago de Servicios"><b>Pago de Servicios</b></a></td>
            <td align="center"><a href="#Procesos">Procesos</a></td>
        </tr>
        <tr>
            <td align="center" bgcolor="#CCCCCC">Txn</td>
            <td align="center" bgcolor="#CCCCCC">Descripcion</td>
            <td align="center" bgcolor="#CCCCCC">N$</td>
            <td align="center" bgcolor="#CCCCCC">US$</td>
            <td align="center" bgcolor="#CCCCCC">UDI</td>
            <td align="center" bgcolor="#CCCCCC">Caja</td>
        </tr>

        <tr>
            <td align="center">0068</td>
            <td align="left">Pago de Suas</td>
            <td align="center">S</td>
            <td align="center">N</td>
            <td align="center">N</td>
            <td align="center">N</td>
        </tr>

        <tr>
            <td align="center">0069</td>
            <td align="left">Aportaciones Voluntarias Deposito Afore</td>
            <td align="center">S</td>
            <td align="center">N</td>
            <td align="center">N</td>
            <td align="center"> </td>
        </tr>

        <tr>
            <td align="center">0070</td>
            <td align="left">Aportaciones Voluntarias Retirio Afore</td>
            <td align="center">S</td>
            <td align="center">N</td>
            <td align="center">N</td>
            <td align="center">N</td>
        </tr>

        <tr>
            <td align="center">0087</td>
            <td align="left">Solicitud de Chequeras</td>
            <td align="center">S</td>
            <td align="center">S</td>
            <td align="center">N</td>
            <td align="center">S</td>
        </tr>

        <tr>
            <td align="center">0090</td>
            <td align="left">Reposiciones de Tarjetas</td>
            <td align="center">S</td>
            <td align="center">N</td>
            <td align="center">N</td>
            <td align="center">N</td>
        </tr>

        <tr>
            <td align="center">0200</td>
            <td align="left">Balance Dotaciones y Concentraciones</td>
            <td align="center">S</td>
            <td align="center">S</td>
            <td align="center">N</td>
            <td align="center">S</td>
        </tr>

        <tr>
            <td align="center">0201</td>
            <td align="left">Dotaciones</td>
            <td align="center">S</td>
            <td align="center">S</td>
            <td align="center">N</td>
            <td align="center">S</td>
        </tr>

        <tr>
            <td align="center">0202</td>
            <td align="left">Concentraciones</td>
            <td align="center">S</td>
            <td align="center">S</td>
            <td align="center">N</td>
            <td align="center">S</td>
        </tr>

        <tr>
            <td align="center">0204</td>
            <td align="left">Dotaciones ATM</td>
            <td align="center">S</td>
            <td align="center">S</td>
            <td align="center">N</td>
            <td align="center">S</td>
        </tr>

        <tr>
            <td align="center">0206</td>
            <td align="left">Dotaciones en Efectivo ATM</td>
            <td align="center">S</td>
            <td align="center">S</td>
            <td align="center">N</td>
            <td align="center">S</td>
        </tr>
        
        <tr>
            <td align="center">0208</td>
            <td align="left">Faltante en ATM</td>
            <td align="center">S</td>
            <td align="center">S</td>
            <td align="center">N</td>
            <td align="center">N</td>
        </tr>
        
        <tr>
            <td align="center">0212</td>
            <td align="left">Sobrante en ATM</td>
            <td align="center">S</td>
            <td align="center">S</td>
            <td align="center">N</td>
            <td align="center">N</td>
        </tr>

        <tr>
            <td align="center">0214</td>
            <td align="left">Salidas de Efectivo o Remanente</td>
            <td align="center">S</td>
            <td align="center">S</td>
            <td align="center">N</td>
            <td align="center">N</td>
        </tr>
        
        <tr>
            <td align="center">0230</td>
            <td align="left">Extraccion de efectivo del CDM</td>
            <td align="center">S</td>
            <td align="center">N</td>
            <td align="center">N</td>
            <td align="center">N</td>
        </tr>   
        
        <tr>
            <td align="center">0232</td>
            <td align="left">Dotacion de efectivo del CDM</td>
            <td align="center">S</td>
            <td align="center">N</td>
            <td align="center">N</td>
            <td align="center">S</td>
        </tr>                     
                
        <tr>
            <td align="center">0234</td>
            <td align="left">Faltantes CDM</td>
            <td align="center">S</td>
            <td align="center">N</td>
            <td align="center">N</td>
            <td align="center">N</td>
        </tr>        
                        
        <tr>
            <td align="center">0236</td>
            <td align="left">Sobrantes CDM</td>
            <td align="center">S</td>
            <td align="center">N</td>
            <td align="center">N</td>
            <td align="center">N</td>
        </tr>                                

        <tr>
            <td align="center">0238</td>
            <td align="left">Depositos de clientes por ETV</td>
            <td align="center">S</td>
            <td align="center">S</td>
            <td align="center">N</td>
            <td align="center">N</td>
        </tr>        
        
        <tr>
            <td align="center">0332</td>
            <td align="left">Deudores Varios</td>
            <td align="center">S</td>
            <td align="center">S</td>
            <td align="center">N</td>
            <td align="center">S</td>
        </tr>

        <tr>
            <td align="center">0368</td>
            <td align="left">Acreedores Varios</td>
            <td align="center">S</td>
            <td align="center">S</td>
            <td align="center">N</td>
            <td align="center">S</td>
        </tr>

       
        <tr>
            <td align="center">0540</td>
            <td align="left">Pagos Extemporaneos INFONAVIT</td>
            <td align="center">S</td>
            <td align="center">N</td>
            <td align="center">N</td>
            <td align="center">S</td>
        </tr>

        <tr>
            <td align="center">0630</td>
            <td align="left">Egresos Varios</td>
            <td align="center">S</td>
            <td align="center">S</td>
            <td align="center">N</td>
            <td align="center">N</td>
        </tr>

        <tr>
            <td align="center">0728</td>
            <td align="left">Pago de tel�fono</td>
            <td align="center">S</td>
            <td align="center">N</td>
            <td align="center">N</td>
            <td align="center"> </td>
        </tr>

        <tr>
            <td align="center">0730</td>
            <td align="left">Servicios Especiales</td>
            <td align="center">S</td>
            <td align="center">N</td>
            <td align="center">N</td>
            <td align="center">S</td>
        </tr>

        <tr>
            <td align="center">0732</td>
            <td align="left">Ingresos Varios</td>
            <td align="center">S</td>
            <td align="center">S</td>
            <td align="center">N</td>
            <td align="center">N</td>
        </tr>

        <tr>
            <td align="center">0820</td>
            <td align="left">Pago Electronico de Impuestos</td>
            <td align="center">S</td>
            <td align="center">N</td>
            <td align="center">N</td>
            <td align="center">S</td>
        </tr>

        <tr>
            <td align="center">4455</td>
            <td align="left">Pago de Infonavit Banda Magnetica</td>
            <td align="center">S</td>
            <td align="center">N</td>
            <td align="center">N</td>
            <td align="center">S</td>
        </tr>

        <tr>
            <td align="center">5405</td>
            <td align="left">Pagos de Loteria Nacional</td>
            <td align="center">S</td>
            <td align="center">N</td>
            <td align="center">N</td>
            <td align="center">S</td>
        </tr>

        <tr>
            <td align="center">5407</td>
            <td align="left">Pagos del Poder Judicial</td>
            <td align="center">S</td>
            <td align="center">N</td>
            <td align="center">N</td>
            <td align="center">S</td>
        </tr>


        <tr>
            <td colspan="6" align="center" bgcolor="#CCCCCC"><a id="Traveler Cheques"><b>Traveler Cheques</b></a></td>
            <td align="center"><a href="#Procesos">Procesos</a></td>
        </tr>
        <tr>
            <td align="center" bgcolor="#CCCCCC">Txn</td>
            <td align="center" bgcolor="#CCCCCC">Descripcion</td>
            <td align="center" bgcolor="#CCCCCC">N$</td>
            <td align="center" bgcolor="#CCCCCC">US$</td>
            <td align="center" bgcolor="#CCCCCC">UDI</td>
            <td align="center" bgcolor="#CCCCCC">Caja</td>
        </tr>

        <tr>
            <td align="center">V353</td>
            <td align="left">A la Par American Express Traveler Cheque Cheque <%=(String)session.getAttribute("identidadApp")%></td>
            <td align="center">N</td>
            <td align="center">S</td>
            <td align="center">N</td>
            <td align="center">S</td>
        </tr>

        <tr>
            <td align="center">0602</td>
            <td align="left">Tramite American Express Traveler Cheques</td>
            <td align="center">N</td>
            <td align="center">S</td>
            <td align="center">N</td>
            <td align="center">S</td>
        </tr>

        <tr>
            <td align="center">0735</td>
            <td align="left">A la Par American Express Traveler Cheque en Efectivo</td>
            <td align="center">N</td>
            <td align="center">S</td>
            <td align="center">N</td>
            <td align="center">S</td>
        </tr>

        <tr>
            <td align="center">5359</td>
            <td align="left">A la Par American Express Traveler Cheque Cargo a Cuenta</td>
            <td align="center">N</td>
            <td align="center">S</td>
            <td align="center">N</td>
            <td align="center">S</td>
        </tr>


        <tr>
            <td colspan="6" align="center" bgcolor="#CCCCCC"><a id="Cheques Certificados"><b>Cheques Certificados</b></a></td>
            <td align="center"><a href="#Procesos">Procesos</a></td>
        </tr>
        <tr>
            <td align="center" bgcolor="#CCCCCC">Txn</td>
            <td align="center" bgcolor="#CCCCCC">Descripcion</td>
            <td align="center" bgcolor="#CCCCCC">N$</td>
            <td align="center" bgcolor="#CCCCCC">US$</td>
            <td align="center" bgcolor="#CCCCCC">UDI</td>
            <td align="center" bgcolor="#CCCCCC">Caja</td>
        </tr>

        <tr>
            <td align="center">4537</td>
            <td align="left">Efectivo / Pago de cheques certificados</td>
            <td align="center">S</td>
            <td align="center">S</td>
            <td align="center">N</td>
            <td align="center">S</td>
        </tr>

        <tr>
            <td align="center">4539</td>
            <td align="left">Abono a Cuenta / Pago de cheques certificados</td>
            <td align="center">S</td>
            <td align="center">S</td>
            <td align="center">N</td>
            <td align="center">S</td>
        </tr>

        <tr>
            <td align="center">4543</td>
            <td align="left">Cargo por cheque certificado recibido de otros bancos</td>
            <td align="center">S</td>
            <td align="center">S</td>
            <td align="center">N</td>
            <td align="center">S</td>
        </tr>

        <tr>
            <td align="center">4545</td>
            <td align="left">Cargo por certificaci�n de cheque</td>
            <td align="center">S</td>
            <td align="center">S</td>
            <td align="center">N</td>
            <td align="center">S</td>
        </tr>


        <tr>
            <td colspan="6" align="center" bgcolor="#CCCCCC"><a id="Inversiones"><b>Inversiones</b></a></td>
            <td align="center"><a href="#Procesos">Procesos</a></td>
        </tr>
        <tr>
            <td align="center" bgcolor="#CCCCCC">Txn</td>
            <td align="center" bgcolor="#CCCCCC">Descripcion</td>
            <td align="center" bgcolor="#CCCCCC">N$</td>
            <td align="center" bgcolor="#CCCCCC">US$</td>
            <td align="center" bgcolor="#CCCCCC">UDI</td>
            <td align="center" bgcolor="#CCCCCC">Caja</td>
        </tr>

        <tr>
            <td align="center">2503</td>
            <td align="left">Dep�sitos Miscel�neos</td>
            <td align="center">S</td>
            <td align="center">S</td>
            <td align="center">N</td>
            <td align="center">S</td>
        </tr>

        <tr>
            <td align="center">2523</td>
            <td align="left">Abono por Transferencia</td>
            <td align="center">S</td>
            <td align="center">S</td>
            <td align="center">N</td>
            <td align="center">S</td>
        </tr>

        <tr>
            <td align="center">2525</td>
            <td align="left">Cargo por transferencia</td>
            <td align="center">S</td>
            <td align="center">S</td>
            <td align="center">N</td>
            <td align="center">S</td>
        </tr>

        <tr>
            <td align="center">3001</td>
            <td align="left">Deposito en efectivo y/o documentos</td>
            <td align="center">S</td>
            <td align="center">S</td>
            <td align="center">N</td>
            <td align="center">S</td>
        </tr>

        <tr>
            <td align="center">3009</td>
            <td align="left">Reinversi�n</td>
            <td align="center">S</td>
            <td align="center">S</td>
            <td align="center">N</td>
            <td align="center">S</td>
        </tr>

        <tr>
            <td align="center">3021</td>
            <td align="left">Pago Intereses y Renovaci�n en L�nea</td>
            <td align="center">N</td>
            <td align="center">S</td>
            <td align="center">N</td>
            <td align="center">S</td>
        </tr>

        <tr>
            <td align="center">3075</td>
            <td align="left">Retiro sobre cuenta bloqueada</td>
            <td align="center">S</td>
            <td align="center">S</td>
            <td align="center">N</td>
            <td align="center">S</td>
        </tr>

        <tr>
            <td align="center">3077</td>
            <td align="left">Retiro sobre fondos congelados</td>
            <td align="center">S</td>
            <td align="center">S</td>
            <td align="center">N</td>
            <td align="center">S</td>
        </tr>

        <tr>
            <td align="center">3105</td>
            <td align="left">Retiro Miscel�neos</td>
            <td align="center">S</td>
            <td align="center">S</td>
            <td align="center">N</td>
            <td align="center">S</td>
        </tr>

        <tr>
            <td align="center">3313</td>
            <td align="left">Consulta para Retiro</td>
            <td align="center">S</td>
            <td align="center">S</td>
            <td align="center">S</td>
            <td align="center">S</td>
        </tr>

        <tr>
            <td align="center">3314</td>
            <td align="left">Retiro en efectivo</td>
            <td align="center">S</td>
            <td align="center">S</td>
            <td align="center">N</td>
            <td align="center">S</td>
        </tr>

        <tr>
            <td align="center">3487</td>
            <td align="left">Cargo por cheque cobro inmediato depositado devuelto</td>
            <td align="center">S</td>
            <td align="center">S</td>
            <td align="center">N</td>
            <td align="center">S</td>
        </tr>

        <tr>
            <td align="center">3587</td>
            <td align="left">Cargo por remesa depositada devuelta</td>
            <td align="center">S</td>
            <td align="center">S</td>
            <td align="center">N</td>
            <td align="center">S</td>
        </tr>


        <tr>
            <td colspan="6" align="center" bgcolor="#CCCCCC"><a id="Varias"><b>Varias</b></a></td>
            <td align="center"><a href="#Procesos">Procesos</a></td>
        </tr>
        <tr>
            <td align="center" bgcolor="#CCCCCC">Txn</td>
            <td align="center" bgcolor="#CCCCCC">Descripcion</td>
            <td align="center" bgcolor="#CCCCCC">N$</td>
            <td align="center" bgcolor="#CCCCCC">US$</td>
            <td align="center" bgcolor="#CCCCCC">UDI</td>
            <td align="center" bgcolor="#CCCCCC">Caja</td>
        </tr>

        <tr>
            <td align="center">N081</td>
            <td align="left">Emisi�n Billete de Dep�sito NAFINSA</td>
            <td align="center">S</td>
            <td align="center">S</td>
            <td align="center">N</td>
            <td align="center"> </td>
        </tr>

        <tr>
            <td align="center">N083</td>
            <td align="left">Cancelaci�n Billete de Dep�sito NAFINSA</td>
            <td align="center">S</td>
            <td align="center">S</td>
            <td align="center">N</td>
            <td align="center"> </td>
        </tr>

        <tr>
            <td align="center">0250</td>
            <td align="left">Consulta de la ultima Transacci�n</td>
            <td align="center">S</td>
            <td align="center">S</td>
            <td align="center">N</td>
            <td align="center">S</td>
        </tr>

        <tr>
            <td align="center">0800</td>
            <td align="left">Impresion de la Ultima Certificacion</td>
            <td align="center">S</td>
            <td align="center">S</td>
            <td align="center">N</td>
            <td align="center">S</td>
        </tr>

        <tr>
            <td align="center">8000</td>
            <td align="left">Visualizacion de Firmas</td>
            <td align="center">S</td>
            <td align="center">S</td>
            <td align="center">N</td>
            <td align="center">S</td>
        </tr>

        <tr>
            <td align="center">9645</td>
            <td align="left">Rechazo de Cheques de Cobro Inmediato</td>
            <td align="center">S</td>
            <td align="center">N</td>
            <td align="center">N</td>
            <td align="center">S</td>
        </tr>

        <tr>
            <td align="center">9646</td>
            <td align="left">Protestos Cheques Devueltos</td>
            <td align="center">S</td>
            <td align="center">N</td>
            <td align="center">N</td>
            <td align="center">S</td>
        </tr>

        <tr>
            <td align="center">9816</td>
            <td align="left">Cambio de Contrase�a</td>
            <td align="center">S</td>
            <td align="center">N</td>
            <td align="center">N</td>
            <td align="center"> </td>
        </tr>


        <tr>
            <td colspan="6" align="center" bgcolor="#CCCCCC"><a id="Vouchers"><b>Vouchers</b></a></td>
            <td align="center"><a href="#Procesos">Procesos</a></td>
        </tr>
        <tr>
            <td align="center" bgcolor="#CCCCCC">Txn</td>
            <td align="center" bgcolor="#CCCCCC">Descripcion</td>
            <td align="center" bgcolor="#CCCCCC">N$</td>
            <td align="center" bgcolor="#CCCCCC">US$</td>
            <td align="center" bgcolor="#CCCCCC">UDI</td>
            <td align="center" bgcolor="#CCCCCC">Caja</td>
        </tr>

        <tr>
            <td align="center">0066</td>
            <td align="left">Dep�sitos / Devoluciones Vouchers TDC</td>
            <td align="center">S</td>
            <td align="center">N</td>
            <td align="center">N</td>
            <td align="center">N</td>
        </tr>


        <tr>
            <td colspan="6" align="center" bgcolor="#CCCCCC"><a id="Relaciones"><b>Relaciones</b></a></td>
            <td align="center"><a href="#Procesos">Procesos</a></td>
        </tr>
        <tr>
            <td align="center" bgcolor="#CCCCCC">Txn</td>
            <td align="center" bgcolor="#CCCCCC">Descripcion</td>
            <td align="center" bgcolor="#CCCCCC">N$</td>
            <td align="center" bgcolor="#CCCCCC">US$</td>
            <td align="center" bgcolor="#CCCCCC">UDI</td>
            <td align="center" bgcolor="#CCCCCC">Caja</td>
        </tr>

        <tr>
            <td align="center">N082</td>
            <td align="left">Relacion Billetes NAFINSA</td>
            <td align="center">S</td>
            <td align="center">S</td>
            <td align="center">N</td>
            <td align="center">S</td>
        </tr>

        <tr>
            <td align="center">RDIG</td>
            <td align="left">Relacion Digitalizacion</td>
            <td align="center">S</td>
            <td align="center">S</td>
            <td align="center">N</td>
            <td align="center">S</td>
        </tr>

        <tr>
            <td align="center">9009</td>
            <td align="left">Relaci�n y Volantes Cobro Inmediato </td>
            <td align="center">S</td>
            <td align="center">S</td>
            <td align="center">N</td>
            <td align="center">S</td>
        </tr>

        <tr>
            <td align="center">9029</td>
            <td align="left">Relacion Formularios 13 y 13A</td>
            <td align="center">S</td>
            <td align="center">N</td>
            <td align="center">N</td>
            <td align="center">S</td>
        </tr>

        <tr>
            <td align="center">9251</td>
            <td align="left">Relacion Cheques Recibidos Otros Bancos</td>
            <td align="center">S</td>
            <td align="center">S</td>
            <td align="center">N</td>
            <td align="center">S</td>
        </tr>

        <tr>
            <td align="center">9503</td>
            <td align="left">Relacion Pagos Cobranza (5503)</td>
            <td align="center">S</td>
            <td align="center">N</td>
            <td align="center">N</td>
            <td align="center">S</td>
        </tr>

        <tr>
            <td align="center">9520</td>
            <td align="left">Relacion Aportaciones SAR</td>
            <td align="center">S</td>
            <td align="center">N</td>
            <td align="center">N</td>
            <td align="center">S</td>
        </tr>

        <tr>
            <td align="center">9540</td>
            <td align="left">Relacion de Pagos (INFONAVIT)</td>
            <td align="center">S</td>
            <td align="center">N</td>
            <td align="center">N</td>
            <td align="center">S</td>
        </tr>

        <tr>
            <td align="center">9630</td>
            <td align="left">Relaci�n Servicios (Egresos)</td>
            <td align="center">S</td>
            <td align="center">S</td>
            <td align="center">N</td>
            <td align="center">S</td>
        </tr>

        <tr>
            <td align="center">9640</td>
            <td align="left">Relaci�n Deposito de Remesas</td>
            <td align="center">S</td>
            <td align="center">S</td>
            <td align="center">N</td>
            <td align="center">S</td>
        </tr>

        <tr>
            <td align="center">9642</td>
            <td align="left">Relaci�n Cheques Certificados</td>
            <td align="center">S</td>
            <td align="center">S</td>
            <td align="center">N</td>
            <td align="center">S</td>
        </tr>

        <tr>
            <td align="center">9730</td>
            <td align="left">Relaci�n Servicios Especiales</td>
            <td align="center">S</td>
            <td align="center">S</td>
            <td align="center">N</td>
            <td align="center">S</td>
        </tr>

        <tr>
            <td align="center">9732</td>
            <td align="left">Relaci�n Servicios (Ingresos)</td>
            <td align="center">S</td>
            <td align="center">S</td>
            <td align="center">N</td>
            <td align="center">S</td>
        </tr>

        <tr>
            <td align="center">9820</td>
            <td align="left">Relacion Transferencias LD / CE (SELLO)</td>
            <td align="center">S</td>
            <td align="center">N</td>
            <td align="center">N</td>
            <td align="center">S</td>
        </tr>

        <tr>
            <td align="center">9821</td>
            <td align="left">Relacion Pago Loteria / Poder Judicial</td>
            <td align="center">S</td>
            <td align="center">S</td>
            <td align="center">N</td>
            <td align="center">S</td>
        </tr>
</table>
</form>



</body>
</html>
