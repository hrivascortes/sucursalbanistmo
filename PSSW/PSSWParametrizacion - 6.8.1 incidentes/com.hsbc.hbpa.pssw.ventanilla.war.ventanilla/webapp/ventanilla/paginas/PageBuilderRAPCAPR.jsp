<%
//*************************************************************************************************
///            Funcion: JSP que despliega las formas de pago de un servicio RAP
//            Elemento: PageBuilderRAPCAPR.jsp
//          Creado por: Alejandro Gonzalez Castro
//      Modificado por: Fausto Rodrigo Flores Moreno
//*************************************************************************************************
// CCN - 4360581 - 12/03/2007 - Se convierten los estilos en jsf y se hace un include en el jsp
// CCN - 4360585 - 23/03/2007 - Se realizan modificaciones para RAP Calculadora
// CCN - 4620021 - 17/10/2007 - Se agregan validaciones para montos mayores a 10000 dlls, Humberto Balleza
//*************************************************************************************************
%>

<%@ page session="true" import="java.util.Vector,java.util.Hashtable"%>
<%
response.setHeader("Cache-Control","no-store"); //HTTP 1.1
response.setHeader("Pragma","no-cache"); //HTTP 1.0
response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<%
String MontoPro = new String("");
String txnAuthLevel = (String)session.getAttribute("page.txnAuthLevel");
String txnIMGAutoriz = (String)session.getAttribute("page.txnIMGAutoriz");
Vector v = (Vector)session.getAttribute("page.specialacct");
Hashtable info = (Hashtable)session.getAttribute("info");

String pcdOK = (String)session.getAttribute("pcd");
Hashtable datasession = (Hashtable)session.getAttribute("page.datasession");

int k;
String noefec = (String)datasession.get("noefec");
%>
<html>
<head>
<%@include file="style.jsf"%>
<title>Datos de la Transaccion</title>
<script language="JavaScript">
var formName = 'Mio'
var specialacct = new Array()
var bandera = 0;
<%
 if(!v.isEmpty())
 {
  for(int iv = 0; iv < v.size(); iv++)
	  {out.print("specialacct[" + iv + "]='" + v.get(iv) + "'" + '\n');}
 }
%>
function validate(form)
{
if (document.entry.txtEfectivo)
{<%
      out.println("  if( !top.validate(window, document.entry.txtEfectivo, 'isValMto5503') )");
      out.println("    return");%>
}
if (document.entry.txtCheque)
{<%
      out.println("  if( !top.validate(window, document.entry.txtCheque, 'isValMto5503') )");
      out.println("    return");%>
}
if (document.entry.txtCuenta)
{<%
      out.println("  if( !top.validate(window, document.entry.txtCuenta, 'isValMto5503') )");
      out.println("    return");%>
}
if (document.entry.txtDocsCI)
{<%
      out.println("  if( !top.validate(window, document.entry.txtDocsCI, 'isValMto5503') )");
      out.println("    return");%>
      //if(!(document.entry.txtDocsCI.value > 0 && document.entry.txtNumPropios.value > 0))
      if(document.entry.txtDocsCI.value > 0)
      {
      	if(document.entry.txtNumPropios.value == 0)
      	{
      		alert("Debes de capturar al menos un documento de cheques locales");
      		return;
      	}
      }
   	
}
if (document.entry.txtRemesas)
{<%
      out.println("  if( !top.validate(window, document.entry.txtRemesas, 'isValMto5503') )");
      out.println("    return");%>
      //if(!(document.entry.txtRemesas.value > 0 && document.entry.txtNumExtranjeros.value > 0))
      if(document.entry.txtRemesas.value > 0)
      {
      	if(document.entry.txtNumExtranjeros.value == 0)
      	{
      		alert("Debes de capturar al menos un documento de cheques extranjeros");
      		return;
      	}
      }
}

if(document.entry.txtEfectivo && document.entry.txtDocsCI)
{
<%
      out.println("  if( !top.validate(window, document.entry.txtEfectivo, 'Valid5503') )");
      out.println("    return");%>
}
<%
      out.println("  if( !top.validate(window, document.entry.subtotal, 'ValSum') )");
      out.println("    return");
      out.println("  if( !top.validate(window, document.entry.txns, 'FlujoRAP') )");
      out.println("    return");%>

if (document.entry.txtRemesas)
{<%
      out.println("  if( !top.validate(window, document.entry.txtRemesas, 'ValRem') )");
      out.println("    return");%>
}


if (document.entry.txtEfectivo)
{
  iMonto = document.entry.txtEfectivo.value
  iMonto = iMonto.toString().replace(/\$|\,/g,'');   
  intVal = iMonto.substring(0, iMonto.indexOf("."));
  decVal = iMonto.substring(iMonto.indexOf(".") + 1, iMonto.length); 
    
  Monto = Number(intVal + decVal);
     if ( Monto >= 1000000) 
         bandera = 1;
     else
     {
         bandera = 0;
         entry.needAutho.value='0';
     }
}


    form.validateFLD.value = '1'
    
<% //if(!txnAuthLevel.equals("1") && !txnIMGAutoriz.equals("1"))
	//{
    //out.println("  form.submit();");
    //out.println("var obj = document.getElementById('btnCont');"); 
    //out.println("obj.onclick = new Function('return false');");   
	//}   
%>

  if ( bandera == 1 ) {                       // Pedir autorizacion
        form.onSubmit="return validate(document.entry);"
        document.entry.needAutho.value = '1';
        AuthoandSignature(document.entry);
  }

  if ( bandera == 0) {
 <%
 if(!txnAuthLevel.equals("1") && !txnIMGAutoriz.equals("1")){
    out.println("  form.submit()");
    out.println("var obj = document.getElementById('btnCont');");
    out.println("obj.onclick = new Function('return false');");
    }
    
%> }

}
function AuthoandSignature(entry)
{
 var Cuenta;
 if(entry.validateFLD.value != '1')
  return
 if(entry.needAutho.value == '1' && entry.AuthoOK.value != '1')
 {
  top.openDialog('authoriz.jsp', 220, 130, 'top.setPrefs()', entry)
 }
 if(entry.needSignature.value == '1' && entry.SignField.value != '')
{
  Cuenta = eval('entry.' + entry.SignField.value + '.value')
  top.openModalDialog('<%=request.getContextPath()%>/servlet/ventanilla.FirmasServlet?txtDDACuenta=' + Cuenta, 710, 430, 'top.setPrefs2()', entry)
 }
}
</script>
</head>

<body bgcolor="#FFFFFF" onload="top.setFieldFocus(window.document)">
<br>
<% out.print("<form name=\"entry\" action=\"RedirectorRAPCARP.jsp\" method=\"post\" ");
   if(txnAuthLevel.equals("1"))
    out.print("onSubmit=\"return validate(this)\"");
   out.println(">");
%>

<h1><%=session.getAttribute("page.txnImage")%></h1>

<h3>
5503  Cobranza
</h3>


<% if (pcdOK.equals("1"))
{%>
<table border="0">
<%if(!(noefec.equals("1")))
{%>
	<tr><td align="right">Efectivo :</td><td>&nbsp;&nbsp;</td>
	<td><input type="text" name="txtEfectivo" size="16" maxlength="13" onBlur="top.validate(window, this, 'isValMto5503')" onKeyPress="top.keyPressedHandler(window, this, 'isValMto5503')"></td><td>&nbsp;&nbsp;</td><td>&nbsp;&nbsp;</td><td>&nbsp;&nbsp;</td></tr>
<%}%>
<tr><td align="right">Documentos <%=(String)session.getAttribute("identidadApp")%> :</td><td>&nbsp;&nbsp;</td>
<td><input type="text" name="txtCheque" size="16" maxlength="13" onBlur="top.validate(window, this, 'isValMto5503')" onKeyPress="top.keyPressedHandler(window, this, 'isValMto5503')"></td><td>&nbsp;&nbsp;</td><td>&nbsp;&nbsp;</td><td>&nbsp;&nbsp;</td>
<tr><td align="right">Cheques Locales :</td><td>&nbsp;&nbsp;</td>
<td><input type="text" name="txtDocsCI" size="16" maxlength="13" onBlur="top.validate(window, this, 'isValMto5503')" onKeyPress="top.keyPressedHandler(window, this, 'isValMto5503')"></td><td>N�mero de cheques :</td><td>&nbsp;&nbsp;</td><td><input type="text" name="txtNumPropios" size="4" maxlength="4" value="0" onBlur="top.validate(window, this, 'isnumeric')""></td>
<tr><td align="right">Cheques Extranjeros :</td><td>&nbsp;&nbsp;</td>
<td><input type="text" name="txtRemesas" size="16" maxlength="13" onBlur="top.validate(window, this, 'isValMto5503')" onKeyPress="top.keyPressedHandler(window, this, 'isValMto5503')"></td><td>N�mero de cheques :</td><td>&nbsp;&nbsp;</td><td><input type="text" name="txtNumExtranjeros" size="4" maxlength="4" value="0" onBlur="top.validate(window, this, 'isnumeric')""></td>
<%}%>

<%
String Desc_Recargo = (String)datasession.get("Desc_Recargo");
String no_descuento = (String)request.getParameter("no_descuento");
String rapcalc = (String)request.getParameter("rapcalc");

if(Desc_Recargo!=null){
	if(Desc_Recargo.equals("D") && no_descuento.equals("SI")){
		info.put("FP2","N");
		info.put("FP3","N");
	}
}

if (pcdOK.equals("2"))
{
String bital    =  info.get("FP1").toString();
String cobroi   =  info.get("FP2").toString();
String remesas  =  info.get("FP3").toString();
%>

<table border="0">
<%	if(bital.equals("S"))
	{%>
		<tr><td align="right">Efectivo :</td><td>&nbsp;&nbsp;</td>
		<td><input type="text" name="txtEfectivo" size="16" maxlength="13" onBlur="top.validate(window, this, 'isValMto5503')" onKeyPress="top.keyPressedHandler(window, this, 'isValMto5503')"></td><td>&nbsp;&nbsp;</td><td>&nbsp;&nbsp;</td><td>&nbsp;&nbsp;</td></tr>
		<tr><td align="right">Documentos <%=(String)session.getAttribute("identidadApp")%> :</td><td>&nbsp;&nbsp;</td>
		<td><input type="text" name="txtCheque" size="16" maxlength="13" onBlur="top.validate(window, this, 'isValMto5503')" onKeyPress="top.keyPressedHandler(window, this, 'isValMto5503')"></td><td>&nbsp;&nbsp;</td><td>&nbsp;&nbsp;</td><td>&nbsp;&nbsp;</td></tr>
<%	}
	if(cobroi.equals("S"))
	{%>
		<tr><td align="right">Cheques Locales :</td><td>&nbsp;&nbsp;</td>
		<td><input type="text" name="txtDocsCI" size="16" maxlength="13" onBlur="top.validate(window, this, 'isValMto5503')" onKeyPress="top.keyPressedHandler(window, this, 'isValMto5503')"></td><td>N�mero de cheques :</td><td>&nbsp;&nbsp;</td><td><input type="text" name="txtNumPropios" size="4" maxlength="4" value="0" onBlur="top.validate(window, this, 'isnumeric')""></td></tr><%
	}
	if(remesas.equals("S"))
	{%>
		<tr><td align="right">Cheques Extranjeros :</td><td>&nbsp;&nbsp;</td>
		<td><input type="text" name="txtRemesas" size="16" maxlength="13" onBlur="top.validate(window, this, 'isValMto5503')" onKeyPress="top.keyPressedHandler(window, this, 'isValMto5503')"></td><td>N�mero de cheques :</td><td>&nbsp;&nbsp;</td><td><input type="text" name="txtNumExtranjeros" size="4" maxlength="4" value="0" onBlur="top.validate(window, this, 'isnumeric')""></td>
		</tr><%
	}
}%>

<tr><td align="right"><b>Total :</b></td><td>&nbsp;&nbsp;</td><%
ventanilla.GenericClasses gc = new ventanilla.GenericClasses();

if(Desc_Recargo!=null){
	if((Desc_Recargo.equals("D") || Desc_Recargo.equals("R")) && no_descuento.equals("SI")){%>
		<td><b><%=gc.formatMonto(datasession.get("MontoPagar").toString())%></b></td></tr><%
	}else if(Desc_Recargo.equals("D") && no_descuento.equals("NO")){%>
		<td><b><%=(String)datasession.get("tot")%></b></td></tr><%
	}else if(Desc_Recargo.equals("N")){%>
		<td><b><%=(String)datasession.get("tot")%></b></td></tr><%
	}
}else{%>
	<td><b><%=(String)datasession.get("tot")%></b></td></tr>
<%}%>
</table>
<p>
<p>
<%if((noefec.equals("1")))
{%>
	<b>Los Servicios 297 y 480 no pueden pagarse con Efectivo ...</b>
<%}%>
<input type="hidden" name="rapcalc" value="<%=rapcalc%>">
<input type="hidden" name="no_descuento" value="<%=no_descuento%>">
<%	if(Desc_Recargo!=null){
		if(Desc_Recargo.equals("D")){%>
<input type="hidden" name="descuento" value="SI">
<%		}
	}%>
<input type="hidden" name="subtotal">
<input type="hidden" name="txns">
<input type="hidden" name="remesas" value="0">
<input type="hidden" name="nservicios" value="<%=(String)datasession.get("nservicios")%>">
<%if(Desc_Recargo!=null){%>
	<input type="hidden" name="rapC" value="<%=Desc_Recargo%>"><%	
	if((Desc_Recargo.equals("D") || Desc_Recargo.equals("R")) && no_descuento.equals("SI")){%>
		<input type="hidden" name="total" value="<%=gc.formatMonto(datasession.get("MontoPagar").toString())%>"><%
	}else if(Desc_Recargo.equals("D") && no_descuento.equals("NO")){%>
		<input type="hidden" name="total" value="<%=(String)datasession.get("tot")%>"><%
	}else if(Desc_Recargo.equals("N")){%>
		<input type="hidden" name="total" value="<%=(String)datasession.get("tot")%>"><%
	}
}else{%>
	<input type="hidden" name="total" value="<%=(String)datasession.get("tot")%>">
<%}%>

<input type="hidden" name="npagos" value="<%=(String)datasession.get("np")%>">
<%Vector FRAP  = new Vector();
  session.setAttribute("FRAP",FRAP);%>

<input type="hidden" name="iTxn" value="<%= session.getAttribute("page.iTxn") %>">
<input type="hidden" name="oTxn" value="<%= session.getAttribute("page.oTxn") %>">
<input type="hidden" name="cTxn" value="<%= session.getAttribute("page.cTxn") %>">
<input type="hidden" name="sucursal" value="<%= session.getAttribute("branch") %>">
<input type="hidden" name="teller" value="<%= session.getAttribute("teller") %>">
<input type="hidden" name="moneda" value="<%= session.getAttribute("page.moneda")%>">
<input type="hidden" name="registro" value="<%=session.getAttribute("empno")%>">
<input type="hidden" name="tllrlvl" value="<%=session.getAttribute("tellerlevel")%>">
<input type="hidden" name="supervisor" value="<%= session.getAttribute("teller") %>">
<input type="hidden" name="AuthoOK" value="0">
<input type="hidden" name="SignOK" value="0">
<input type="hidden" name="validateFLD" value="0">
<input type="hidden" name="needSignature" value="<%out.print(txnIMGAutoriz);%>">
<input type="hidden" name="needAutho" value="<%out.print(txnAuthLevel);%>">
<input type="hidden" name="SignField" value="<%= session.getAttribute("page.SignField") %>">
<input type="hidden" name="servicio1" value="<%= (String)datasession.get("servicio1") %>">
<p>
<%
 if(txnAuthLevel.equals("1") || txnIMGAutoriz.equals("1"))
    out.println("<a id='btnCont' href=\"javascript:validate(document.entry);javascript:AuthoandSignature(document.entry)\" ><img src=\"../imagenes/b_continuar.gif\" border=\"0\"></a>");
   else
    out.println("<a id='btnCont' href=\"javascript:validate(document.entry)\"><img src=\"../imagenes/b_continuar.gif\" border=\"0\"></a>");
%>
</form>
</body>
</html>
