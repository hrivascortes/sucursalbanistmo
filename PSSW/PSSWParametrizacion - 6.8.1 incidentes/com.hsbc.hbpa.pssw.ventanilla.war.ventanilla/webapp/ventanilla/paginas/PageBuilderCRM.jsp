<!--
//*************************************************************************************************
//             Funcion: JSP que presentaCRM
//            Elemento: PageBuilderCRM.jsp
//          Creado por: Alejandro Gonzalez Castro
//      Modificado por: Alejandro Gonzalez Castro
//*************************************************************************************************
// CCN - 4360189 - 03/09/2004 - Se redirecciona a Final.jsp por Control de Efectivo
//*************************************************************************************************
-->
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
	<head>
		<title>C R M</title>
		<SCRIPT   language="JavaScript">

		function Terminar()
		{			
			document.signature.action = "Final.jsp";
			document.signature.target = "panel";
			document.signature.submit();
		}

		</SCRIPT>		
	</head>
	<body>	
	<font face="helvetica"><H3><font color="red"> C R M </font></H3>
<% 
	if(session.getAttribute("crmInfo") != null)
	{
		java.util.Hashtable datasession = (java.util.Hashtable)session.getAttribute("crmInfo");
		String strMensaje1 =  "<B>" + (String)datasession.get("D_LINEA_MENSAJE_1") + "</B>" + "<br>";
		String strMensaje2 = "<B>" + (String)datasession.get("D_LINEA_MENSAJE_2") + "</B>" + "<br>";
		String strMensaje3 = "<B>" + (String)datasession.get("D_LINEA_MENSAJE_3") + "</B>" + "<br>";
		String strNumeroCis = "<B>" + (String)datasession.get("C_NUMERO_CTE_CIS") + "</B>" + "<br>";
		String strNombreCte = "<B>" + (String)datasession.get("D_NOMBRE_CTE") + "</B>" + "<br>";    
		strNumeroCis = "Numero CIS del cliente : " + strNumeroCis;
		strNombreCte = "Nombre del cliente : " + strNombreCte;
   	out.println(strNombreCte);
   	out.println(strMensaje1);
   	out.println(strMensaje2);
   	out.println(strMensaje3);
		session.removeAttribute("crmInfo");
	}
%>
	</font>
	<FORM name="signature">
		<input type="hidden" name="SignOK" value="1">
		<DIV STYLE="text-align:right">
<%
	out.println("<a href=\"javascript:Terminar()\"><img src=\"../imagenes/b_continuar.gif\" border=\"0\"></a>");
%>
		</DIV>
	</FORM>
 </body>
</html>
