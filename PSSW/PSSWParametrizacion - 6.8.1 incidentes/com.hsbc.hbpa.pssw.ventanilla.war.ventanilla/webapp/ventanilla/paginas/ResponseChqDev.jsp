<!--
//*************************************************************************************************
//             Funcion: JSP que muestra mensaje de cheques devueltos.
//            Elemento: ResponseChqDev.jsp
//          Creado por: Israel De Paz Mercado
//*************************************************************************************************
// CCN - 4360455 - 07/04/2005 - Se crea JSP para el esquema de cheques devueltos.
// CCN - 4360462 - 10/04/2005 - Se genera nuevo paquete por errores en paquete anterior
//*************************************************************************************************
-->
 
<%@ page session="true" import="java.util.*"%>
<%
response.setHeader("Cache-Control","no-store"); //HTTP 1.1
response.setHeader("Pragma","no-cache"); //HTTP 1.0
response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<%!
   private String delCommaPointFromString(String newCadNum)
    {
        String nCad = new String(newCadNum);
        if( nCad.indexOf(".") > -1) {
            nCad = nCad.substring(nCad.indexOf(".") + 1, nCad.length());
            if(nCad.length() != 2) {
                String szTemp = new String("");
                for(int j = nCad.length(); j < 2; j++)
                    szTemp = szTemp + "0";
                newCadNum = newCadNum + szTemp;
            }
        }
        
        StringBuffer nCadNum = new StringBuffer(newCadNum);
        for(int i = 0; i < nCadNum.length(); i++)
            if(nCadNum.charAt(i) == ',' || nCadNum.charAt(i) == '.')
                nCadNum.deleteCharAt(i);
                
        return nCadNum.toString();
    }
%>

<%
Hashtable datasession = (Hashtable)session.getAttribute("page.datasession");

String Tot =(String)datasession.get("txtCheque");
long Total = 0;
Total =   Long.parseLong(delCommaPointFromString(Tot));

String Monto = (String)datasession.get("MontoTotal");
long MontoTot = 0;
MontoTot = Long.parseLong(delCommaPointFromString(Monto));
%>

<html>
<head>
<title>
</title>
<style type="text/css">Body {font-family: Arial, Helv; font-size: 10pt; font-weight: bold; color:#000000}</style>
<script language="javascript" >
function validate(form)
{
form.submit();
}
</script>
</head>
<body bgcolor="#FFFFFF">
<%
   if (MontoTot < Total)
      {%>
      <form name="entry" action="../../servlet/ventanilla.PageServlet" method="post"> 
      <input type="hidden" name="transaction" value="5353">
      <input type="hidden" name="transaction1" value="5353">
      <input type="hidden" name="supervisor" value="<%= session.getAttribute("supervisor") %>">
      <%}
   else if(((String)datasession.get("iTxn")).equals("1003") && MontoTot == Total)
       {
        datasession.put("txtDDACuenta2",session.getAttribute("CuentaChq")); 
        datasession.put("txtSerial2",session.getAttribute("SerialChq"));
       session.setAttribute("page.datasession", datasession);
       %>
             <form name="entry" action="../../servlet/ventanilla.PageServlet" method="post"> 
             <input type="hidden" name="transaction" value="5353">
             <input type="hidden" name="transaction1" value="5353">
             <input type="hidden" name="supervisor" value="<%= session.getAttribute("supervisor") %>">
     <%} 
   else{%>
             <form name="entry" action="RedirectorRAP.jsp" method="post">  
           <%}%>
           
<br><b><center>Devolucion Registrada, Continuar Operacion</center></b>
<br><center><a href="javascript:validate(document.entry)"><img src="../imagenes/b_continuar.gif" border="0"></a></center>

</form>
</body>
</html>
