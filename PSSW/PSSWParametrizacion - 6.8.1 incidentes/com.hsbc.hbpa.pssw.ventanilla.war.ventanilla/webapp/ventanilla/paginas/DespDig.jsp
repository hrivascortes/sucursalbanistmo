<!--
//*************************************************************************************************
//             Funcion: JSP que carga applet de digitalizacion
//            Elemento: DespDig.jsp
//          Creado por: Juan Carlos Gaona
//      Modificado por: Alejandro Gonzalez Castro
//*************************************************************************************************
// CCN - 4360189 - 03/09/2004 - Se redirecciona a Final.jsp por Control de Efectivo
//*************************************************************************************************
-->

<%@ page session="true"%>
<%
response.setHeader("Cache-Control","no-store"); //HTTP 1.1
response.setHeader("Pragma","no-cache"); //HTTP 1.0
response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
%>
<%!
  private String stringFormat(int option, int sum)
  {
    java.util.Calendar now = java.util.Calendar.getInstance();
    String temp = new Integer(now.get(option) + sum).toString();
    if( temp.length() != 2 )
      temp = "0" + temp;
    return temp;
  }
%>
<HTML>
<HEAD>
 <TITLE>Impresión</TITLE>
</HEAD>
<Script language="JavaScript">
	function submiteSalir()
	{	
		document.Digitalizacion.action = "Final.jsp";
		document.Digitalizacion.target = "panel";
		document.Digitalizacion.submit();
		window.close();
		return;
	}
	
</script>
<%
	String txtTipoDoc = new String("");
	String txtCajero = new String("");
	if(session.getAttribute("TipoDoc") != null)
		txtTipoDoc = session.getAttribute("TipoDoc").toString();
	if(session.getAttribute("teller") != null)
		txtCajero = session.getAttribute("teller").toString();

%>
<BODY TOPMARGIN="0" LEFTMARGIN="0" MARGINWIDTH="0" MARGINHEIGHT="0">

<OBJECT classid="clsid:8AD9C840-044E-11D1-B3E9-00805F499D93" codebase="http://java.sun.com/products/plugin/1.2.2/jinstall-1_2_2-win.cab#Version=1,2,2,0" WIDTH ="750" HEIGHT = "380">
		<PARAM name="type" value="application/x-java-applet;version=1.2">
		<PARAM name="java_ARCHIVE" value="digitalizar.jar">
		<PARAM name="java_CODE" value="ls100Main">
		<PARAM name="java_NAME" value="Digitalizacion">
		<PARAM name="java_CODEBASE" value="../">
		<PARAM name="TipoDoc" value="<%out.print(txtTipoDoc);%>">
		<PARAM name="strCajero" value="<%out.print(txtCajero);%>">
		<PARAM name="RemoteIP" value="<%=request.getServerName()%>">
		<PARAM name="RemotePort" value="<%=request.getServerPort()%>">
		<PARAM name="Ruta" value="<%=request.getContextPath()%>">
		<COMMENT>
<EMBED type="application/x-java-applet;version=1.2" 
		java_ARCHIVE="digitalizar.jar"
		java_CODE ="ls100Main"
		java_NAME ="Digitalizacion"
		java_CODEBASE ="../"
		WIDTH ="750"
		HEIGHT = "380"
		TipoDoc = "<%out.print(txtTipoDoc);%>"
		strCajero = "<%out.print(txtCajero);%>"
		RemoteIP = "<%=request.getServerName()%>"
		RemotePort = "<%=request.getServerPort()%>"
		Ruta = "<%=request.getContextPath()%>"
		MAYSCRIPT>
</EMBED>
		</COMMENT>
</OBJECT>
	<FORM NAME="Digitalizacion" method="POST">
   	<input type="hidden" name="nothing" value="nothing">
   </FORM>    
</BODY>
</HTML>
