<!--
//*************************************************************************************************
//             Funcion: JSP que arma la pagina principal
//            Elemento: index.jsp
//          Creado por: Alejandro Gonzalez Castro
// Ultima Modificacion: 25/02/2005
//      Modificado por: Jesus E. Lopez Rosales
//*************************************************************************************************
// CCN - 4360297 - 18/03/2005 - Se crea funcion openWindow para poder abrir una nueva ventana.
// CCN - 4360508 - 08/09/2006 - Se realiza modificacion de la liga de la intranet.
//*************************************************************************************************
-->
<%  
 response.setHeader("Cache-Control","no-store"); //HTTP 1.1
 response.setHeader("Pragma","no-cache"); //HTTP 1.0
 response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
%>
<HTML>
<HEAD>
<TITLE>index.jsp</TITLE>
</HEAD>
<SCRIPT language="JavaScript">
function openWindow()
{
<%if (session.getAttribute("Login.On") == null)
  {%>
	direccion = "main.jsp";
	options = "resizable=0,toolbar=0,scrollbars=0,menubar=0,width=790,height=515,top=0,left=0,status=1";
	window.open(direccion,"PSSW",options);
<%}else{%>	
    alert("No es Posible Abrir Mas de una session en PSSW-VENTANILLA");
<%}%>
    document.intranet.submit();
}
</SCRIPT>

<BODY onload="openWindow()">
<form name="intranet" action="http://intranet.banistmo.corp/" method="post">
  <input type="hidden" name="dummy" value="0">
</form>

</BODY>
</HTML>
