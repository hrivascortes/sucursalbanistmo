
<!-- 
//***********************************************************************************************************************
//             Funcion: JSP que arma la pagina principal
//            Elemento: main.jsp
//          Creado por: Alejandro Gonzalez Castro
//      Modificado por: Jes�s Emmanuel L�pez Rosales
//*************************************************************************************************
// CCN - 4360297 - 23/03/2005 - Se cambia CCN por peticion de QA, CCN anterior 4360291
// CCN - 4360323 - 24/05/2005 - Se envian nuevamente los elementos con el cambio del tama�o de la letra
// CCN - 4360342 - 08/07/2005 - Se realizan modificaciones para departamentos.
// CCN - 4360364 - 19/08/2005 - Se controlan departamentos por Centro de Costos.
// CCN - 4360437 - 24/02/2006 - Se realizan modifiaciones para mostrar mensajes de ultima hora
// CCN - 4360510 - 08/09/2006 - Se almacena en sesion el perfil con el que se esta trabajando
// IR - Estandarizaci�n de cheques 20/05/2014 - Se crear variables de contexto
//*************************************************************************************************
-->  


<%
 response.setHeader("Cache-Control","no-store"); //HTTP 1.1
 response.setHeader("Pragma","no-cache"); //HTTP 1.0
 response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
%>
<%
String perfil = request.getParameter("perf")==null?"":request.getParameter("perf").toString().trim();

if(!perfil.equals(""))
	session.setAttribute("tellerperfil",perfil);

session.setAttribute("Login.On","true");
%>
<html>
<script language="javascript"> 

var gContextPath = '<%=request.getContextPath()%>';
var gtxnGrup1 ='<%= session.getAttribute("TxnGrup1")%>'; 
var gtxnGrup2 ='<%= session.getAttribute("TxnGrup2")%>'; 
var gtxnGrup3 ='<%= session.getAttribute("TxnGrup3")%>'; 
var gfeMaxVigCh ='<%= session.getAttribute("feMaxVigCh")%>'; 		


<%-- var gFeMaxVigCh = '<%=request.getFeMaxVigCh()%>'; --%>

</script>    
<script language="javascript" src="ventanilla/paginas/validate.js"></script>


<script language="javascript">   

function msg()
{
    alert("El cambio de Contrase�a fu� exitoso...");
}

</script>
<head>

<%
    boolean login = false;

    if( session.getAttribute("error") != null )
    {
        if( session.getAttribute("error").toString().equals("ok") )
            login = true;
    }

    if( !login )
    {%>
        <%@ include file="ventanilla/paginas/firma.jsp"%>
<%      out.println("</html>");
        return;
    }%> 
<%

java.util.Hashtable userinfo = null;
if(session.getAttribute("userinfo") != null)
{
     userinfo = (java.util.Hashtable)session.getAttribute("userinfo");

     if(userinfo.get("CambioOK") != null)
     {
        userinfo.remove("CambioOK");%>

        <script>msg();</script> 
     <%}
}

%> 

<title>PSSW::Ventanilla</title>
    <frameset cols="*,140" style="color:#FFFFFF" border="0">
    
        <frameset rows="51,100,0,*,30" border="0">
        
            <frame name="datossucursal" src="ventanilla/paginas/datossucursal.jsp" frameborder="0" marginwidth="0" marginheight="0" noresize scrolling="no" tabindex="-1">

            <frame name="procesosdisponibles" src="ventanilla/paginas/procesosdisponibles.jsp" frameborder="0" marginwidth="0" marginheight="0" noresize scrolling="no" tabindex="-1">
            
            <frame name="Calculadora" src="ventanilla/paginas/final.html" frameborder="0" marginwidth="0" marginheight="0" noresize scrolling="no" tabindex="-1">


            <frame name="panel" src="ventanilla/paginas/panelcaptura.html" frameborder="0" marginwidth="0" marginheight="0" noresize scrolling="auto">
            
       
            <frameset cols="390,250" style="color:#FFFFFF" border="1">

                <frame name="avisosultimahora" src="ventanilla/paginas/avisosultimahora.jsp" frameborder="0" marginwidth="0" marginheight="0" noresize scrolling="no" tabindex="-1">

<%              if(((String)session.getAttribute("isForeign")).equals("S") || userinfo.get("N_NIVEL").equals("3") || userinfo.get("N_NIVEL").equals("4")){%>

					<frame name="transaccionejecutar" src="ventanilla/paginas/panelcaptura.html" frameborder="0" marginwidth="0" marginheight="0" noresize scrolling="no">

<%              }else{%>
                    <frame name="transaccionejecutar" src="ventanilla/paginas/transaccionejecutar.jsp" frameborder="0" marginwidth="0" marginheight="0" noresize scrolling="no">


<%              }%>


            </frameset>
	
        </frameset>

        <frame name="seccioninformativa" src="ventanilla/paginas/seccioninformativa.jsp" frameborder="0" marginwidth="0" marginheight="0" noresize scrolling="no" tabindex="-1">
        

    </frameset>

 </head>

 <body>

 </body>

</html> 
