
package com.bital.cics;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import com.ibm.ctg.client.ECIRequest;
import com.ibm.ctg.client.JavaGateway;


public class CicsCtgImpl implements ICicsCtgImpl {


	private void execute(CicsCtgBean ctgBean) throws IOException{
		String progress = "";
		JavaGateway javagateway = null;

		ctgBean.setLoadedDate(getDateFormat(new Date(),"dd/MM/yyyy"));
		ctgBean.setLoadedHour(getDateFormat(new Date(), "HH:mm:ss"));

		verificaUser(ctgBean);

	
		try {
			
			progress += "Connecting to Gateway <BR> ";
			
			//System.out.println(progress);
			// create and open the CICS Gateway	
			javagateway = openCicsConnection(ctgBean);
			
			ECIRequest eciRequest = null;

			byte[] abCommarea = getByteCommAreaEncoding(ctgBean);

			// create the ECI Request						
			eciRequest =
				new ECIRequest(
					ECIRequest.ECI_SYNC,
					ctgBean.getCicsServer(),
					ctgBean.getUserId(),
					ctgBean.getPassword(),
					ctgBean.getCicsProgramName(),
					ctgBean.getTransId(),
					abCommarea,
					ctgBean.getCommAreaLength(),
					ECIRequest.ECI_NO_EXTEND,
					ECIRequest.ECI_LUW_NEW);

			// indicate progess
			progress += "Flowing ECI request to server <BR> ";
			//System.out.println("Configurando valores::::");

			javagateway.flow(eciRequest);

			if (eciRequest.getCicsRc() == ECIRequest.ECI_NO_ERROR) {
				if (eciRequest.Commarea != null) {
					// set result string in default encoding attribute
					String result = getStrCommAreaEncoding(ctgBean, eciRequest.Commarea);
					ctgBean.setCommAreaOutput(result);
					ctgBean.setResultCode(0);
					ctgBean.setErrorMessage("La conexi�n ha sido exitosa");
				}
			} else {
				
				ctgBean.setErrorMessage("Error en el CICS");
				ctgBean.setResultCode(1);
				
			}

			// set return codes
			System.out.println("Rc: " + eciRequest.getRc());
			//System.out.println("Rc: " + eciRequest.getCicsRcString());
			//System.out.println("Rc: " + eciRequest.getRcString());

			ctgBean.setReturnCodeECI(eciRequest.getCicsRc());
			ctgBean.setErrorMsgECI(eciRequest.getCicsRcString());
			ctgBean.setAbendCode(eciRequest.Abend_Code);
		} catch (Exception ex) {
			
			ctgBean.setErrorMessage("::: Excepci�n al conectar con el CICS");
			ctgBean.setErrorException(ex.getMessage());
			ctgBean.setResultCode(2);
			
		} finally {
			progress += "Closing Gateway <BR> ";
			ctgBean.setProgress(progress);
			if (javagateway != null) {
				javagateway.close();
			}
		}
	}

	
	private void verificaUser(CicsCtgBean ctgBean) {
		if (!(ctgBean.getUserId() == null)) {
			if (ctgBean.getUserId().trim().equals("")) {
				ctgBean.setUserId(null);
			}
		}

		if (!(ctgBean.getPassword() == null)) {
			if (ctgBean.getPassword().trim().equals("")) {
				ctgBean.setPassword(null);
			}
		}
	}
	
	public void execCics(CicsCtgBean ctgBean) {
		try {
			execute(ctgBean);
		} catch (IOException e) {
			
			ctgBean.setErrorMessage("Excepci�n al conectar con el CICS");
			ctgBean.setErrorException(e.getMessage());
			ctgBean.setResultCode(2);
		}
	}

	
	public JavaGateway openCicsConnection(CicsCtgBean ctgBean) throws IOException {
		JavaGateway javagateway = new JavaGateway();
		
		//Asigna el puerto
		if (ctgBean.getGatewayPort() != 0) {
			javagateway.setPort(ctgBean.getGatewayPort());
		}
		
		//Asigna la URL
		if (ctgBean.getGatewayURL() == null) {
			javagateway.setURL(new String("local:"));
		} else if (ctgBean.getGatewayURL().trim().equals("")) {
			javagateway.setURL(new String("local:"));
		} else {
			javagateway.setURL(ctgBean.getGatewayURL());
		}
		

		//Asigna el protocolo	
		if (ctgBean.getGatewayProtocol() == null) {
			javagateway.setProtocol(new String("local"));
		} else if (ctgBean.getGatewayProtocol().trim().equals("")) {
			javagateway.setProtocol(new String("local"));
		} else {
			javagateway.setProtocol(ctgBean.getGatewayProtocol());
		}
		
		javagateway.open();	
		return javagateway;
	}

	
	private byte[] getByteCommAreaEncoding(CicsCtgBean ctgBean) throws UnsupportedEncodingException {

		byte[] abCommarea = new byte[ctgBean.getCommAreaLength()];
		byte[] temp = null;

		if (ctgBean.getEncoding() == null) {
			temp = ctgBean.getCommAreaInput().getBytes();
		} else if (ctgBean.getEncoding().trim().equals("")) {
			temp = ctgBean.getCommAreaInput().getBytes();
		} else {
			temp = ctgBean.getCommAreaInput().getBytes(ctgBean.getEncoding());
		}

		System.arraycopy(temp, 0, abCommarea, 0, temp.length);

		return abCommarea;
	} 

	
	private String getStrCommAreaEncoding(CicsCtgBean ctgBean, byte[] bytes) throws UnsupportedEncodingException {
		String result = null;

		if (ctgBean.getEncoding() == null) {
			result = new String(bytes);
		} else if (ctgBean.getEncoding().trim().equals("")) {
			result = new String(bytes);
		} else {
			result = new String(bytes, ctgBean.getEncoding());
		}

		return result;
	} 
	
	
	public static String getDateFormat(Date date, String formato){
		Locale lcMx = new Locale("es", "MX"); 
		SimpleDateFormat formateador = new SimpleDateFormat(formato, lcMx);
		return formateador.format(date);
	}

} 