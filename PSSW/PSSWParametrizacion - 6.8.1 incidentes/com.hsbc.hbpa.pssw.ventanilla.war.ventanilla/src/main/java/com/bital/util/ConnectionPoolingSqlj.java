//*************************************************************************************************
//             Funcion: Clase que realiza Pool de Conexiones para SQLJ
//            Elemento: ConnectionPoolingSqlj.java
//          Creado por: Alejandro Gonzalez Castro
//*************************************************************************************************
// CCN - 4360229 - 26/10/2004 - Se incluye clase para trabajar con SQLJ
//*************************************************************************************************
 
package com.bital.util;


public class ConnectionPoolingSqlj {
    private static String statementPostFix = new String("");
    private static String dbQualifier = new String("idpsw");
    
    public ConnectionPoolingSqlj() {
    }
    
    public static java.sql.Connection getPooledConnection( ){
        java.util.Hashtable jdbcParameters = null;
        javax.naming.Context jdbcContext = null;
        javax.sql.DataSource jdbcDataSource = null;
        java.sql.Connection pooledConnection = null;
        jdbcParameters = new java.util.Hashtable();
        try{
            String tmpString = getdbQualifier();
            //1. Create parameter list to access naming system
            jdbcParameters.put(javax.naming.Context.INITIAL_CONTEXT_FACTORY, "com.ibm.ejs.ns.jndi.CNInitialContextFactory" );
            
            //2. Access naming system
            jdbcContext = new javax.naming.InitialContext( jdbcParameters );
            
            //3. Get DataSource Factory object from naming system
            jdbcDataSource = ( javax.sql.DataSource ) jdbcContext.lookup( "jdbc/" +  dbQualifier);

            pooledConnection = jdbcDataSource.getConnection( );

            pooledConnection.setAutoCommit(false);
        }
        catch( javax.naming.NamingException namingException ){
            System.out.println( "Error ConnectionPoolingManager::getPooledConnection: [" + namingException.toString() + "]");
        }
        catch( java.sql.SQLException sqlException ){
            //processDB2Diagnosable(sqlException);
        }
        return pooledConnection;
    }
    
    public static String getStatementPostFix(){
        if( System.getProperties().getProperty("os.arch").toString().indexOf("x86") < 0)
            statementPostFix = new String(" WITH CS");
        return statementPostFix;
    }
    
    public static String getdbQualifier(){
        if(System.getProperties().getProperty("dbQualifier") != null){
            char qualifier = System.getProperties().getProperty("dbQualifier").toString().toLowerCase().charAt(0);
            System.getProperties().getProperty("dbQualifier").toString().toLowerCase().charAt(0);
            dbQualifier = dbQualifier.substring(0,1) + qualifier + dbQualifier.substring(2);
        }
        return dbQualifier;
    }
    

}
