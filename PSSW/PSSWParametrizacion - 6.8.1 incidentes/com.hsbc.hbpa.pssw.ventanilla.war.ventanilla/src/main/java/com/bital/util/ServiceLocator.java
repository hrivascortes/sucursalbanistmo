//*************************************************************************************************
//             Funcion: Clase que hace la lectura de variables de ambiente
//            Elemento: ServiceLocator.java
//			Creado por: Marco Antonio Lara Palacios
//      Modificado por: Fausto Rodrigo Flores Moreno
//*************************************************************************************************
// CCN - 4360858 - 16/04/2009 - FFM - Se genera clase para recuperar variables y recursos definidos en el WAS
//*************************************************************************************************

package com.bital.util;

import java.util.Collections;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;

import javax.jms.Queue;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

public final class ServiceLocator {
	private static ServiceLocator seLocator = null;
	private InitialContext context = null;
	private DataSource ds = null;
	private Map cache = null;

	private ServiceLocator() {
		try {
			Hashtable params = new Hashtable();
			params.put(javax.naming.Context.INITIAL_CONTEXT_FACTORY,
					"com.ibm.websphere.naming.WsnInitialContextFactory");
			context = new InitialContext(params);
			ds = (DataSource) context.lookup("java:comp/env/PSSW");
			context = new InitialContext();
			cache = Collections.synchronizedMap(new HashMap());
		} catch (NamingException e) {
		}
	}

	public static ServiceLocator getInstance() {
		if (seLocator == null){
			seLocator = new ServiceLocator();
		}

		return seLocator;
	}

	public DataSource getDataSource() {
		return ds;
	}

	public String getResourceValue(String key) {
		String result = null;
		try {
			if (cache.containsKey(key))
			{
				result = (String) cache.get(key);
			}
			else {
				result = (String) context.lookup("java:comp/env/" + key);
				cache.put(key, result);
			}
		} catch (NamingException e) {
		}

		return result;
	}

	public Queue getResourceQueue(String key) {
		Queue result = null;
		try {
			if (cache.containsKey(key))
				result = (Queue) cache.get(key);
			else {
				result = (Queue) context.lookup("java:comp/env/" + key);
				cache.put(key, result);
			}
		} catch (NamingException e) {
		}

		return result;
	}
}