//*************************************************************************************************
//             Funcion: Clase que realiza Pool de Conexiones
//            Elemento: ConnectionPoolingManager.java
//          Creado por: Alejandro Gonzalez Castro
//      Modificado por: Juvenal Fernandez
//*************************************************************************************************
// CCN - 4360189 - 03/09/2004 - Se agrega getPostFixUR() para usar postfijo WITH UR
// CCN - 4360229 - 26/10/2004 - Se adecua clase para trabajar con SQLJ
// CCN - 4360324 - 03/06/2005 - Se adecua clase para ambiente Piloto PSSW
// CCN - 4360324 - 03/06/2005 - Se adecua clase para ambiente Piloto PSSW
//*************************************************************************************************
 
package com.bital.util; 

public class ConnectionPoolingManager
{
    private static String statementPostFix = new String("");
    private static String PostFixUR = new String("");
    private static String dbQualifier = new String("");
    
    public static java.sql.Connection getPooledConnection( ){
        java.util.Hashtable jdbcParameters = null;
        javax.naming.Context jdbcContext = null;
        javax.sql.DataSource jdbcDataSource = null;
        java.sql.Connection jdbcConnection = null;
        try{
            String tmpString = getdbQualifier();
            jdbcParameters = new java.util.Hashtable();
            //1. Create parameter list to access naming system
			jdbcParameters.put(javax.naming.Context.INITIAL_CONTEXT_FACTORY, "com.ibm.websphere.naming.WsnInitialContextFactory" );
			
            //2. Access naming system
            jdbcContext = new javax.naming.InitialContext( jdbcParameters );
            
            //3. Get DataSource Factory object from naming system
   			jdbcDataSource = ( javax.sql.DataSource )jdbcContext.lookup("java:comp/env/PSSW");
            
            //4. Finally try to get the Connection Object
           jdbcConnection = jdbcDataSource.getConnection( );
           jdbcConnection.setAutoCommit(true);
        }
        catch( javax.naming.NamingException namingException ){
            System.out.println( "Error ConnectionPoolingManager::jdbcContext.lookup: [" + namingException.toString() + "]");
        }
        catch( java.sql.SQLException sqlException ){
            System.out.println( "Error ConnectionPoolingManager::jdbcContext.lookup: [" + sqlException.toString() + "]");
        }
        
        return jdbcConnection;
    }
    
    public static String getStatementPostFix(){
        if( System.getProperties().getProperty("os.arch").toString().indexOf("x86") < 0)
            statementPostFix = new String(" WITH CS");
        return statementPostFix;
    }
  
    public static String getPostFixUR(){
        if( System.getProperties().getProperty("os.arch").toString().indexOf("x86") < 0)
            PostFixUR = new String(" WITH UR");
        return PostFixUR;
    }
    
    public static String getdbQualifier(){
        if(System.getProperties().getProperty("dbQualifier") != null){
			dbQualifier = System.getProperties().getProperty("dbQualifier").toString().toLowerCase();
        }
        return dbQualifier;
    }
}
