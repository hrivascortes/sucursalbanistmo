package com.bital.cics;

public class CicsCtgBean {
	/** Datos de entrada **/
	private String cicsProgramName = null; //CICS program name
	private String commAreaInput = null; //COMMAREA input
	private int commAreaLength = 0; //COMMAREA length
	private String gatewayURL = null; //Gateway daemon URL
	private int gatewayPort = 0; //Gateway daemon Port		 
	private String encoding = null; //Encoding
	private String cicsServer = null; //CICS Server
	private String userId = null; //User ID
	private String rolKroner = null; //Rol Kroner
	private String password = null; //Password		
	private String transId = null; //Mirror transaction
	private String gatewayProtocol = null; //Gateway Protocol
	//private boolean		trace				= false;	//Trace

	/** Datos de salida **/
	private int resultCode = 0; //C�digo: 0=OK, 1=Error, 2=Excepci�n
	private String progress = null; //Progreso de comunicaci�n
	private String loadedDate = null; //Fecha en que se ejecuto la interface
	private String loadedHour = null; //Hora en que se ejecuto la interface	
	private String commAreaOutput = null; //COMMAREA output
	private int returnCodeECI = 0; //ECI return code
	private String errorMsgECI = null; //ECI error msg
	private String abendCode = null; //Abend Code
	private String errorException = null; //Mensaje de excepci�n
	private String errorMessage = null; //Mensaje de error
	//private Trace traceES = null; //TraceES


	{
	}

	/** 
	 * Password  		
	 * @return	String
	 */
	public String getPassword() {
		return password;
	}

	/** 
	 * Mirror transaction 	 
	 * @return	String
	 */
	public String getTransId() {
		return transId;
	}

	/** 
	 * CICS program name 
	 * @return	String
	 */
	public String getCicsProgramName() {
		return cicsProgramName;
	}

	/** 
	 * CICS Server 	 
	 * @return	String
	 */
	public String getCicsServer() {
		return cicsServer;
	}

	/** 
	 * COMMAREA input 	 
	 * @return	String
	 */
	public String getCommAreaInput() {
		return commAreaInput;
	}

	/** 
	 * COMMAREA length 	 
	 * @return	int
	 */
	public int getCommAreaLength() {
		return commAreaLength;
	}

	/** 
	 * Encoding 	 
	 * @return	String
	 */
	public String getEncoding() {
		return encoding;
	}

	/** 
	 * Gateway daemon Port 
	 * @return	int
	 */
	public int getGatewayPort() {
		return gatewayPort;
	}

	/** 
	 * Gateway daemon URL 	 
	 * @return	String
	 */
	public String getGatewayURL() {
		return gatewayURL;
	}

	/** User ID 	 
	 * @return	String
	 */
	public String getUserId() {
		return userId;
	}
	
	/** Rol Kroner 	 
	 * @return	String
	 */
	public String RolKroner() {
		return rolKroner;
	}

	/** 
	 * Abend Code 	 
	 * @return	String
	 */
	public String getAbendCode() {
		return abendCode;
	}

	/** 
	 * COMMAREA output 	 
	 * @return	String
	 */
	public String getCommAreaOutput() {
		return commAreaOutput;
	}

	/** 
	 * ECI error msg 	 
	 * @return	String
	 */
	public String getErrorMsgECI() {
		return errorMsgECI;
	}

	/** 
	 * Fecha en que se ejecuto la interface 	 
	 * @return	String
	 */
	public String getLoadedDate() {
		return loadedDate;
	}

	/** 
	 * Hora en que se ejecuto la interface 	 
	 * @return	String
	 */
	public String getLoadedHour() {
		return loadedHour;
	}

	/** 
	 * Progreso de comunicaci�n 	 
	 * @return	String
	 */
	public String getProgress() {
		return progress;
	}

	/** 
	 * C�digo: 0=OK, 1=Error, 2=Excepci�n 	 
	 * @return	int
	 */
	public int getResultCode() {
		return resultCode;
	}

	/** 
	 * ECI return code 	 
	 * @return	int
	 */
	public int getReturnCodeECI() {
		return returnCodeECI;
	}

	/** 
	 * Mensaje de excepci�n 	 
	 * @return	String
	 */
	public String getErrorException() {
		return errorException;
	}

	/** 
	 * Mensaje de error 	 
	 * @return	String
	 */
	public String getErrorMessage() {
		return errorMessage;
	}

	/** 
	 * Trace ES 	 
	 * @return	Trace
	 */
	//public Trace getTraceES() {
	//	return traceES;
	//}

	/** Gateway Protocol 	 
	 * @return	String
	 */
	public String getGatewayProtocol() {
		return gatewayProtocol;
	}

	/**	 	<SET> DE LOS DATOS 	**/
	{
	}

	/** 
	 * Password  			 	 
	 * @param	string password
	 */
	public void setPassword(String string) {
		password = string;
	}

	/** 
	 * Mirror transaction   			 	 
	 * @param	string transId
	 */
	public void setTransId(String string) {
		transId = string;
	}

	/** 
	 * Encoding   			 	 
	 * @param	string encoding
	 */
	public void setEncoding(String string) {
		encoding = string;
	}

	/** 
	 * CICS program name    cicsProgramName			 	 
	 * @param	string cicsProgramName
	 */
	public void setCicsProgramName(String string) {
		cicsProgramName = string;
	}

	/** 
	 * CICS Server   			 	 
	 * @param	string cicsServer
	 */
	public void setCicsServer(String string) {
		cicsServer = string;
	}

	/** 
	 * COMMAREA input   			 	 
	 * @param	string commAreaInput
	 */
	public void setCommAreaInput(String string) {
		commAreaInput = string;
	}

	/** 
	 * COMMAREA length   			 	 
	 * @param	i commAreaLength
	 */
	public void setCommAreaLength(int i) {
		commAreaLength = i;
	}

	/** 
	 * Gateway daemon Port   			 	 
	 * @param	i gatewayPort
	 */
	public void setGatewayPort(int i) {
		gatewayPort = i;
	}

	/** 
	 * Gateway daemon URL   			 	 
	 * @param	string gatewayURL
	 */
	public void setGatewayURL(String string) {
		gatewayURL = string;
	}

	/** 
	 * User ID   			 	 
	 * @param	string userId
	 */
	public void setUserId(String string) {
		userId = string;
	}

	/** 
	 * Rol Kroner   			 	 
	 * @param	string RolKroner
	 */
	public void setRolKroner(String string) {
		rolKroner = string;
	}

	/** 
	 * Abend Code   			 	 
	 * @param	string abendCode
	 */
	public void setAbendCode(String string) {
		abendCode = string;
	}

	/** 
	 * COMMAREA output   			 	 
	 * @param	string commAreaOutput
	 */
	public void setCommAreaOutput(String string) {
		commAreaOutput = string;
	}

	/** 
	 * ECI error msg   			 	 
	 * @param	string errorMsgECI
	 */
	public void setErrorMsgECI(String string) {
		errorMsgECI = string;
	}

	/** 
	 * Fecha en que se ejecuto la interface   			 	 
	 * @param	string loadedDate
	 */
	public void setLoadedDate(String string) {
		loadedDate = string;
	}

	/** 
	 * Hora en que se ejecuto la interface 	 
	 * @param	string loadedHour
	 */
	public void setLoadedHour(String string) {
		loadedHour = string;
	}

	/** 
	 * Progreso de comunicaci�n 	 
	 * @param	string progress
	 */
	public void setProgress(String string) {
		progress = string;
	}

	/** 
	 * C�digo: 0=OK, 1=Error, 2=Excepci�n 	 
	 * @param	i resultCode
	 */
	public void setResultCode(int i) {
		resultCode = i;
	}

	/** 
	 * ECI return code 	 
	 * @param	i returnCodeECI
	 */
	public void setReturnCodeECI(int i) {
		returnCodeECI = i;
	}

	/** 
	 * Mensaje de excepci�n 	 
	 * @param	string errorException
	 */
	public void setErrorException(String string) {
		errorException = string;
	}

	/** 
	 * Mensaje de error 	 
	 * @param	string errorMessage
	 */
	public void setErrorMessage(String string) {
		errorMessage = string;
	}

	/** 
	 * Trace ES 	 
	 * @param	trace  traceES
	 */
	//public void setTraceES(Trace trace) {
	//	traceES = trace;
	//}

	/** 
	 * Gateway Protocol 	 
	 * @param	string gatewayProtocol
	 */
	public void setGatewayProtocol(String string) {
		gatewayProtocol = string;
	}

}
