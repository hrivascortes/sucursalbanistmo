package com.bital.util;

public class GatewayException extends java.lang.Exception
{
  public GatewayException()
  {
    super();
  }

  public GatewayException(String msg)
  {
    super(msg);
  }
}
