
package com.bital.util;

import com.bital.cics.CicsCtgBean;
import com.bital.cics.CicsCtgImpl;

public class Comunica {
	String salida=null;
	
/*	public static void main(String[] args) { 
		Comunica executa = new Comunica();
		CicsCtgBean cicsCtgObj = new CicsCtgBean();		
		//cicsCtgObj.setCicsProgramName("KPHO075"); //"rpc"	
		cicsCtgObj.setCicsProgramName("KPNO003"); //"rpc"	
		cicsCtgObj.setCommAreaLength(4096); //"long"
		cicsCtgObj.setUserId("DPSPUSR2");//("IDDAGJ"); //"user"		
		cicsCtgObj.setPassword("cmtH2345");//("ANAKIN69"); //"password"	
		cicsCtgObj.setCicsServer("CIPADRK1"); //"cics"
		cicsCtgObj.setGatewayURL("web53.mx.cmtH"); //"url" LOCAL
		//cicsCtgObj.setGatewayURL("local:"); //"url"
		cicsCtgObj.setGatewayPort(2006); //"puerto" --LOCAL
		cicsCtgObj.setGatewayProtocol("tcp"); //"protocolo"		
		cicsCtgObj.setEncoding("IBM037"); //"encoding"
		cicsCtgObj.setTransId("KPNO"); //"trans"
		cicsCtgObj.setCommAreaInput("RECUPIDDRDE__01A000010233023020080107000000000100_00000100901020100000000000000000IDDRDE__000000000000000000000000000000____________0001002000182133014001110211010000000000000000");
		executa.executale(cicsCtgObj);		
	}*/
	
	public String executale(CicsCtgBean cicsCtgObj) {
	
		CicsCtgImpl cicsCtg = new CicsCtgImpl();
		cicsCtg.execCics(cicsCtgObj);

		if (cicsCtgObj.getResultCode() > 0) {
			String error = cicsCtgObj.getErrorMessage() + " ::: "  + cicsCtgObj.getErrorException() + " ::: " +cicsCtgObj.getErrorMsgECI() + " ::: ";
			System.out.println(error);
			
			
		}else{
			//System.out.println("MSG SALIDA  "+cicsCtgObj.getCommAreaOutput());
			salida=cicsCtgObj.getCommAreaOutput();
		}
		return salida;
	
	}
}
