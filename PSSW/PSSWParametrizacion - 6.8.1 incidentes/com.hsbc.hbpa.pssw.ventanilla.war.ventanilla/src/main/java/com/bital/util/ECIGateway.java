//*************************************************************************************************
//             Funcion: Clase que realiza conexion a Gateway
//          Creado por: Marco A. Lara Palacios
//      Modificado por: Rutilo Zarco Reyes
//*************************************************************************************************
// CCN -4360176 - 06/08/2004 - Se habilita com a X65W
// CCN -4360189 - 03/09/2004 - Se inhabilita com a X65W y se corrige llamado para VSAM
// CCN - 4360334 - 17/06/2005 - Se cambia el codigo de pagina a 037 por especificacion de WAS5
// CCN - 4360336 - 17/06/2005 - Se cambia la obtenci�n de variables de ambiente
// CCN - 4360407 - 15/12/2005 - Se agrega condicion para cambio de caracteres en SAT V4.0
//*************************************************************************************************

package com.bital.util;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

import com.ibm.ctg.client.ECIRequest;
import com.ibm.ctg.client.JavaGateway;

public class ECIGateway implements Gateway {
    private String url  ;
    private int    port = 2006;

    private JavaGateway jgaConn    = null;
    private ECIRequest  eciRequest = null;
    private byte[]      abCommarea = new byte[4096];

    public ECIGateway() {}

    public ECIGateway(String url, int port) {
        this.url  = url;
        this.port = port;
    }


    private boolean open() {

        boolean result = true;

        try {
            if( jgaConn == null ) {
                jgaConn = new JavaGateway();
                jgaConn.setURL(url);
                if( port != 0 )
                    jgaConn.setPort(port);
            }

            if( !jgaConn.isOpen() ){
                jgaConn.open();
            }
        }
        catch(IOException e) {
            result = false;
            System.out.println("Exception open:" + e.toString());
        }

        return result;
    }

    private void close() {
        try {
            if( jgaConn != null ){
                jgaConn.close();
            }

            jgaConn = null;
        }
        catch(IOException e) {
            System.out.println("Exception close:" + e.toString());
        }
    }

    protected void finalize() throws Throwable {
        close();
        abCommarea = null;
    }

    public void write(String msg) throws GatewayException { 	
        if( !open() )
            throw new GatewayException("Unable to open CicsGateway");

        String prefix = new String("IW000002 ");
        if( msg.substring(0, 4).equals("DSLZ") )
            prefix = "IW000003";
        else if( msg.substring(0, 4).equals("DSLO") )
            prefix = "IW000004";
        else if( msg.substring(0, 4).equals("FIRM") ) {
            prefix = "IW000005";
            abCommarea = new byte[12284];
        }
        else if( msg.substring(0, 4).equals("XPAN") ) {
            prefix = "IW000001";
            abCommarea = new byte[12284];
        }
        else if( msg.substring(0, 4).equals("VSAM") )
        {
            prefix = "IW000008";
            abCommarea = new byte[4104];            
        }
        else if( msg.substring(0, 4).equals("VSEC") )
            prefix = "VSECUTIL";
        else if( msg.substring(0, 4).equals("ODWU") )
            prefix = "IW000006";
        
        
        try {
            // Limpiar area de intercambio
            for(int i=0; i<abCommarea.length; i++)
                abCommarea[i] = 0;

            // Transformacion de caracteres en el envio
            StringBuffer buffer = new StringBuffer(msg.length());
            int sat = 0;
            if (msg.indexOf("5503A") > 0)
                sat = verifica(msg);
            if( msg.indexOf("0806G") < 0 && sat == 0) {
                for(int i=0; i<msg.length(); i++) {
                    char ch = msg.charAt(i);
                    if( ch == '�' )
                        buffer.append('#');
                    else
                        buffer.append(ch);
                }
            }
            else
                buffer.append(msg);

            System.arraycopy(buffer.toString().getBytes("Cp037"), 0, abCommarea, 4, msg.length());
            //String ctgID = "CICSDRG1";
            String ctgID ="";
	  	javax.naming.Context initialContext = null;
	  	
	    try
	    {
	    	initialContext = new javax.naming.InitialContext();
		ctgID = (String)initialContext.lookup("java:comp/env/CICS");
		
	    }
	    catch(javax.naming.NamingException e)
	    {
	    	System.out.println("ECIGateway::Lookup::java:comp/env/CICS");
	    	System.out.println(e);
	    }
            
            
            if( prefix != null ) {        	
                eciRequest = new ECIRequest(ctgID, null, null, prefix, abCommarea, ECIRequest.ECI_NO_EXTEND, ECIRequest.ECI_LUW_NEW);
                jgaConn.flow(eciRequest);
            }
        }
        catch( Exception e ) {
            e.printStackTrace(System.out);
            throw new GatewayException(e.getMessage());
        }
    }

    public String readLine() throws GatewayException {
        String response = null;
        StringBuffer buffer = null;
		try{
        response = new String(abCommarea, "Cp037").trim();
		}
		catch(UnsupportedEncodingException ueException){
		  throw new GatewayException(ueException.getMessage());
		}

        buffer = new StringBuffer( response.length() );

        if( response.indexOf("WSAT_RS") < 0 ) {
            for(int i=0; i<response.length(); i++) {
                char ch = response.charAt(i);
				if( ch == '#' )
					buffer.append('�');    
                else
                    buffer.append(ch);
            }
        }
        else
            buffer.append(response);
        close();
        return new String(buffer);
    }


    public byte[] read() throws GatewayException {
        if( abCommarea == null )
            throw new GatewayException("Commarea is undefined");
        close();

        return abCommarea;
    }
    public int verifica(String msg)
    {
      int status = 0;
      int indice = msg.indexOf("*");
      if (indice > -1)
      {
       String serv = msg.substring(indice,indice+7);
       if (serv.equals("**4376*"))
        status = 1;
      }
      return status;
    }
}