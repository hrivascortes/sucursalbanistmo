package com.bital.util;  

public class ParameterNotFoundException extends java.lang.Exception
{
  public ParameterNotFoundException()
  {
    super();
  }

  public ParameterNotFoundException(String msg)
  {
    super(msg);
    //System.out.println("El parametro: " + msg + " NO EXISTE!");
  } 
}
