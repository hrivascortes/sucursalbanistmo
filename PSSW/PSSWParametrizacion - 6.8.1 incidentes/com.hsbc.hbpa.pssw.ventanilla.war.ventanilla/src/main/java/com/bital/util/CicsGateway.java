package com.bital.util;

import com.bital.util.Gateway;
import com.bital.util.GatewayException;
import com.bital.util.ECIGateway;

public class CicsGateway implements Gateway
{
  private Gateway gw = null;

  public CicsGateway()
  {
    gw = new ECIGateway("local:", 0);
//    gw = new ECIGateway();
  }

  public void write(String str) throws GatewayException
  {
    gw.write(str);
  }

  public String readLine() throws GatewayException
  {
    return gw.readLine();
  }

  public byte[] read() throws GatewayException
  {
    return gw.read();
  }
}
