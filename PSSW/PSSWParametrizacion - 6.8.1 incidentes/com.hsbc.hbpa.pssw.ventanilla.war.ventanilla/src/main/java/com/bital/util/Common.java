package com.bital.util;

import java.util.Calendar;

public final class Common
{

    public Common()
    {
    }

    public static String number(String s, int i)
    {
        s = s.trim();
        for(int j = s.length(); j < i; j++)
            s = "0" + s;

        return s;
    }

    public static String getCurrentDay()
    {
        Calendar calendar = Calendar.getInstance();
        int i = calendar.get(5);
        int j = calendar.get(2) + 1;
        int k = calendar.get(1);
        String s = i + "/";
        if(i < 10)
            s = "0" + s;
        if(j < 10)
            s = s + "0";
        s = s + j + "/" + k;
        return s;
    }

    public static String formatCur(double d)
    {
        String s = Long.toString((long)Math.floor((d * 100D + 0.5D) % 100D));
        String s1 = Long.toString((long)Math.floor((d * 100D + 0.5D) / 100D));
        if(s.length() < 2)
            s = "0" + s;
        for(int i = 0; (double)i < Math.floor((s1.length() - (1 + i)) / 3); i++)
            s1 = s1.substring(0, s1.length() - (4 * i + 3)) + ',' + s1.substring(s1.length() - (4 * i + 3));

        return s1 + "." + s;
    }

    public static String unformatCur(String s)
    {
        if(s == null)
            return "000";
        StringBuffer stringbuffer = new StringBuffer();
        for(int i = 0; i < s.length(); i++)
        {
            char c = s.charAt(i);
            if(c != ',' && c != '.')
                stringbuffer.append(c);
        }

        return stringbuffer.toString();
    }
}
