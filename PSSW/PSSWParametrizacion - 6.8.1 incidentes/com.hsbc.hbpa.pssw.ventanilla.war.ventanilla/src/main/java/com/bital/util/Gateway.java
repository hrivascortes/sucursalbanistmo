package com.bital.util;

public interface Gateway {
    void write(String msg) throws GatewayException;
    String readLine() throws GatewayException;
    byte[] read() throws GatewayException;
}
