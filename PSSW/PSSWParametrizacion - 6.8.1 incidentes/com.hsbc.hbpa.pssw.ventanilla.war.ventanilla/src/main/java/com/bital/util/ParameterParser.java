package com.bital.util; 

import com.bital.util.ParameterNotFoundException;

import java.util.Hashtable;

public class ParameterParser
{
  private Hashtable req;

  public ParameterParser(Hashtable req)
  {
    this.req = req;
  }
   
  // Regresar el valor de tipo String
  public String getString(String name) throws ParameterNotFoundException 
  {
    // Obtener el valor del parametro
    String value = (String)req.get(name);

    if( value == null )
      throw new ParameterNotFoundException(name);
    else
    return value;
  }

  public String getString(String name, String def)
  {  
    try
    {
      return getString(name);
    }
    catch( Exception e )
    {
      return def;
    }
  }

  // Regresar el valor de tipo String sin formato numerico [,.]
  public String getCString(String name) throws ParameterNotFoundException
  {
    // Obtener el valor del parametro
    String value = (String)req.get(name);

    if( value == null )
      throw new ParameterNotFoundException(name);
    else
    {
      StringBuffer buffer = new StringBuffer();
      for(int i=0; i<value.length(); i++)
      {
        char cLetter = value.charAt(i);
        if( cLetter == ',' || cLetter == '.' )
          continue;
        
        buffer.append(cLetter);
      }
      
      return new String(buffer);
    }
  }

  public String getCString(String name, String def)
  {
    try
    {
      return getCString(name);
    }
    catch( Exception e )
    {
      return def;
    }
  }

  // Regresar el valor de tipo int
  public int getInt(String name) throws ParameterNotFoundException, NumberFormatException
  {
    return Integer.parseInt(getString(name));
  }

  public int getInt(String name, int def)
  {
    try
    {
      return getInt(name);
    }
    catch( Exception e )
    {
      return def;
    }
  }
  
  // Regresar el valor de tipo long
  public long getLong(String name) throws ParameterNotFoundException, NumberFormatException
  {
    return Long.parseLong(getString(name));
  }

  public long getLong(String name, long def)
  {
    try
    {
      return getLong(name);
    }
    catch( Exception e )
    {
      return def;
    }
  }
  
  public String getStringNULL(String name)
  {
    // Obtener el valor del parametro
    String value = (String)req.get(name);

    return value;
  }
}
