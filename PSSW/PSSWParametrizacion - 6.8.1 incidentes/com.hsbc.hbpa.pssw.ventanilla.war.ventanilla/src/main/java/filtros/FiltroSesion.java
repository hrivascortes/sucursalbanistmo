//**************************************************************************************
//            Funcion: clase que verifica si la session ha caducado
//            Elemento: FiltroSesion.java
//          Creado por: Yair Jesus Chavez Antonel
//      Modificado por: Fausto Rodrigo Flores Moreno
//**************************************************************************************
// CCN - 4360590 - 16/04/2007 - Se realizan modificaciones para el minitoreo de efectivo en sucursales
//**************************************************************************************

package filtros;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class FiltroSesion extends HttpServlet implements Filter {
	private String Url = null;
	public void destroy() {

	}


    public int validaSesion(HttpServletRequest rq)throws ServletException, IOException{
    	HttpSession session = rq.getSession();
    	int flag = 0;
    	// 0 -> Caduco session
    	// 1 -> No Caduco session
    	// 2 -> No hizo concentración efectivo Pesos
    	// 3 -> No hizo concentración efectivo DLLS

    	String bloqueadoPesos = "N";
    	String bloqueadoDLLS = "N";
    	//System.out.println(session.getAttribute("tellerperfil").toString());
    	if(session.getAttribute("tellerperfil") != null){
    		if(session.getAttribute("PIFAlertCash")!=null){
    			bloqueadoPesos = (String)session.getAttribute("PIFAlertCash");
    		}
    		if(session.getAttribute("PIFAlertCashDlls")!=null){
    			bloqueadoDLLS = (String)session.getAttribute("PIFAlertCashDlls");
    		}
    		if(bloqueadoPesos.equals("1"))
	    		flag = 2;
	    	else if(bloqueadoDLLS.equals("1"))
		    	flag = 3;
		    else
		    	flag = 1;
    	}
    	
    	return flag;
    }

	public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain)
		throws ServletException, IOException {
		java.util.Stack flujotxn =null;
		HttpServletRequest httpS = (HttpServletRequest)req;
		HttpSession session = httpS.getSession();		        
		flujotxn = (java.util.Stack)session.getAttribute("page.flujotxn");		
		
		
		if(validaSesion((HttpServletRequest)req) == 1){
			if(isCalculadora((HttpServletRequest)req)){
				RequestDispatcher disp = req.getRequestDispatcher(Url);
				disp.forward(req,resp);
				}
				else{
					chain.doFilter(req, resp);
				}
		}else	if(validaSesion((HttpServletRequest)req) == 2){
			RequestDispatcher disp = req.getRequestDispatcher("/ventanilla/paginas/BloqueoConPesos.jsp");
			disp.forward(req,resp);
		}else if(validaSesion((HttpServletRequest)req) == 3){
			RequestDispatcher disp = req.getRequestDispatcher("/ventanilla/paginas/BloqueoConDlls.jsp");
			disp.forward(req,resp);
		}else{
			RequestDispatcher disp = req.getRequestDispatcher("/ventanilla/paginas/CaducaSesion.jsp");        
			disp.forward(req,resp);
		}
	}
	private boolean isCalculadora(HttpServletRequest rq){
		boolean status = false;
		HttpSession session = rq.getSession();
		String ReqURL = rq.getRequestURL().toString();
		String strCalc = session.getAttribute("viewCSIB")==null?"0":(String)session.getAttribute("viewCSIB");
		if(strCalc.equals("1") && (!ReqURL.contains("CurrencyServlet"))){
			setUrl2();			
			status = true;
		}
		return status;
	}
	
	public void setUrl2 (){
		Url = null;
		Url = "/ventanilla/paginas/PageBuilderSIB.jsp";
	}

	public void init(FilterConfig config) throws ServletException {

	}

}
