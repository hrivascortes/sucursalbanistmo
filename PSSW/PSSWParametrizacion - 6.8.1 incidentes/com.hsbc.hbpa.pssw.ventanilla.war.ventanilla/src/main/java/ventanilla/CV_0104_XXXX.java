//*************************************************************************************************
//             Funcion: Clase que realiza txn 0104
//            Elemento: CV_0104_XXXX.java
//          Creado por: Alejandro Gonzalez Castro
//		Modificado por: Jes�s Emmanuel L�pez Rosales
//*************************************************************************************************
// CCN - 4360189 - 03/09/2004 - Se cambia invoke al Diario para Control de Efectivo
// CCN - 4360268 - 04/02/2005 - Se realizan adecuaciones para eliminar Agencia NY 
// CCN - 4360368 - 02/09/2005 - Se elimina sleep
// CCN - 4360374 - 08/09/2005 - Se genera nuevo paquete a peticion de QA.
// CCN - 4360532 - 20/10/2006 - Se realizan modificaciones para enviar el numero de cajero, consecutivo txn entrada y salida en txn 0821
//*************************************************************************************************

package ventanilla;

import java.io.IOException;
import java.util.Hashtable;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import ventanilla.com.bital.admin.Diario;
import ventanilla.com.bital.sfb.CompraVenta;

public class CV_0104_XXXX extends HttpServlet {
	
    public void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String TE, TO, Currency;
        int Resultado = -1;
        GenericClasses GC = new GenericClasses();

        CompraVenta oTxn = new CompraVenta();
        Diario diario;
        String sMessage = null;

        HttpSession session = req.getSession(false);
        Hashtable datasession = (Hashtable)session.getAttribute("page.datasession");
        TE = (String)datasession.get("cTxn");
        TO = (String)datasession.get("oTxn");
        Currency = (String)datasession.get("moneda");
        String txtPlaza = (String)datasession.get("txtPlaza");
        String msg = null;
        String dirip =  (String)session.getAttribute("dirip");
        String txtCasoEsp = new String("");
        String txtCadImpresion = new String("");
        String txtSupervisor = (String)datasession.get("teller");
        String txtRegistro = (String)datasession.get("txtRegistro");
        String txtOverride = "0";
        String Currency2 = "";
        String Monto = "";
        String ConsecutivoIN = "";
        String ConsecutivoOUT = "";        
        if ( Currency.equals("01"))
            Currency2 = "02";
        else
            Currency2 = "01";

        if ( (String)datasession.get("supervisor") != null )       //pidio autorizacisn de supervisor
            txtSupervisor =  (String)datasession.get("supervisor");
        
         if ( datasession.get("posteo") == null ) {
            datasession.put("posteo","1");
            session.setAttribute("page.datasession",(Hashtable)datasession);
        }
        
        if ( session.getAttribute("txtCasoEsp") != null )
            txtCasoEsp = session.getAttribute("txtCasoEsp").toString();
        
        if ( (String)datasession.get("override") != null )
            if (datasession.get("override").toString().equals("SI"))
                  txtOverride = "3";

        oTxn.setTxnCode(TE);
        oTxn.setBranch((String)datasession.get("sucursal"));
        oTxn.setTeller((String)datasession.get("teller"));
        oTxn.setSupervisor(txtSupervisor);
        oTxn.setOverride(txtOverride);

        if( TE.equals( "0104" ) )
        {
            //ODCT 0104G 00001000131  000131  000**WCH*US$*0251000001*3*021000089*100000*0*0*0449470*0449470***951000001*
            if ( datasession.get("posteo").toString().equals("1")){
                oTxn.setFormat( "G" );
                oTxn.setService("WCH");
                oTxn.setAcctNo( "0104" );
                oTxn.setToCurr("US$");
                oTxn.setCheckNo("0" + (String)datasession.get("txtSerial5"));
                oTxn.setGStatus("3");
                oTxn.setBcoTrnsf("021000089");
                oTxn.setTranAmt( GC.delCommaPointFromString((String)datasession.get( "txtMonto1" )) );
                oTxn.setComAmt( "0" );
                oTxn.setIVAAmt( "0" );
                oTxn.setAcctNo( (String)datasession.get("registro") );
                oTxn.setCtaBenef( txtRegistro );
                oTxn.setInstruc((String)datasession.get("txtAutori"));
                
                
                diario = new Diario();
                sMessage = diario.invokeDiario(oTxn, (String)session.getAttribute("d_ConsecLiga"), dirip, Currency, session);
                
                Resultado = oTxn.getStatus();
                
                Monto = GC.delCommaPointFromString((String)datasession.get("txtMonto1"));
                
                if(sMessage.startsWith("0") || sMessage.startsWith("1")) {
                    txtCasoEsp = txtCasoEsp + TE + "~" + GC.getString(oTxn.getMessage(),1) + "~";
                    ConsecutivoIN = GC.getString(oTxn.getMessage(),1);
                    if (ConsecutivoIN.length()<6){
                    	ConsecutivoIN = "0" + ConsecutivoIN; 
                    }                    
                }
            }
            else
                Resultado = 0;
        }
        if ( TE.equals("0104") && Resultado == 0 )
        {
            if ( datasession.get("posteo").toString().equals("1")) {
                 datasession.put("override","NO");
                 datasession.put("posteo","2");
                 txtOverride = "0";
            }
           /*try{Thread.sleep( 1500 );}
            catch (InterruptedException e){}*/
            oTxn = new CompraVenta();
            oTxn.setBranch((String)datasession.get("sucursal"));
            oTxn.setTeller((String)datasession.get("teller"));
            oTxn.setSupervisor(txtSupervisor);
            oTxn.setOverride(txtOverride);
            
            //ODCT 4111A 00001000131  000131  000**4000000000*1000000*N$*0251000001**000**00100100000*100000**ABO.CTA.CHQS.  M.N.************************************0104**
            oTxn.setTxnCode( TO );
            oTxn.setFormat( "A" );
            oTxn.setAcctNo( (String)datasession.get( "txtDDACuenta" ) );
            oTxn.setTranAmt( GC.delCommaPointFromString((String)datasession.get( "txtMontoC" )) );
            oTxn.setTranCur( "N$" );
            oTxn.setMoAmoun( GC.delCommaPointFromString((String)datasession.get( "txtMonto1" )) );
            oTxn.setTranDesc( "ABO.CTA.CHQS.  M.N." );
            oTxn.setCashIn( "000" );
            String txtTipoCambio = GC.StringFiller((String)datasession.get( "txtTipoCambio" ), ((String)datasession.get( "txtTipoCambio" )).length()+3, false, "0");
            txtTipoCambio = "0000" + txtTipoCambio;
            txtTipoCambio = txtTipoCambio.substring(txtTipoCambio.length()-8);
            oTxn.setDraftAm(txtPlaza + txtTipoCambio);
            oTxn.setCheckNo( "0" + (String)datasession.get("txtSerial5") );
            oTxn.setTrNo5( "0104" );

            diario = new Diario();
            sMessage = diario.invokeDiario(oTxn, (String)session.getAttribute("d_ConsecLiga"), dirip, Currency2, session);

            Resultado = oTxn.getStatus();

            Monto = GC.delCommaPointFromString((String)datasession.get("txtMonto"));

            if(sMessage.startsWith("0") || sMessage.startsWith("1"))
            {
                txtCasoEsp = txtCasoEsp + TO + "~" + GC.getString(oTxn.getMessage(),1) + "~";
                ConsecutivoOUT = GC.getString(oTxn.getMessage(),1);
                if (ConsecutivoOUT.length()<6){
                  	ConsecutivoOUT = "0" + ConsecutivoOUT; 
                }                
            }
        }

        if (  TE.equals("0104") && Resultado == 0)
        {   // Cargo y Abono Aceptado
            // ODCT 0821G 00001000131  000131  000**C*US$*0000000000*2*021000089*100000*010000000*0*0449470*0449470*0104**951000001*

            oTxn = new CompraVenta();
            oTxn.setBranch((String)datasession.get("sucursal"));
            oTxn.setTeller((String)datasession.get("teller"));
            oTxn.setSupervisor(txtSupervisor);
            oTxn.setTxnCode("0821");
            oTxn.setFormat( "G" );
            oTxn.setToCurr( "US$" );
            oTxn.setService( "C" );
            oTxn.setGStatus( "2" );
            oTxn.setBcoTrnsf("021000089");
            String txtAutoriCV = (String)datasession.get("txtAutoriCV");
            if ( txtAutoriCV == null )
                txtAutoriCV = "00000";
            oTxn.setCheckNo( "00000" + txtAutoriCV );
            oTxn.setTranAmt( GC.delCommaPointFromString((String)datasession.get( "txtMonto1" )) );
            String txtTipoCambio = GC.StringFiller((String)datasession.get( "txtTipoCambio" ), ((String)datasession.get( "txtTipoCambio" )).length()+2, false, "0") + "00";
            txtTipoCambio = "0000" + txtTipoCambio;
            txtTipoCambio = txtTipoCambio.substring(txtTipoCambio.length()-9);
            oTxn.setComAmt(txtTipoCambio);
            oTxn.setIVAAmt( "0" );
            oTxn.setAcctNo( (String)datasession.get( "registro" ) );
            oTxn.setCtaBenef( (String)datasession.get( "txtRegistro" ) );
            oTxn.setInstruc((String)datasession.get( "txtAutori" ));
            oTxn.setBenefic("0104      " +  ConsecutivoIN +ConsecutivoOUT + (String)datasession.get("teller"));

            diario = new Diario();
            sMessage = diario.invokeDiario(oTxn, (String)session.getAttribute("d_ConsecLiga"), dirip, "US$", session);

            Monto = GC.delCommaPointFromString((String)datasession.get("txtMonto"));
            if(oTxn.getMessage().substring(0,1).equals("0") || oTxn.getMessage().substring(0,1).equals("1")){
                txtCasoEsp = txtCasoEsp + "0821" + "~" + GC.getString(oTxn.getMessage(),1) + "~";
            }
        }
        if(txtCasoEsp.length() > 0){
            session.setAttribute("txtCasoEsp", txtCasoEsp);
        }
        session.setAttribute("txtCadImpresion", sMessage);
        if(txtCadImpresion.length() > 0){
            session.setAttribute("txtCadImpresion", txtCadImpresion);
        }
        session.setAttribute("page.txnresponse", sMessage);
        getServletContext().getRequestDispatcher("/servlet/ventanilla.Redirector").forward(req,resp);
    }

    public void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doPost(req, resp);
    }
}
