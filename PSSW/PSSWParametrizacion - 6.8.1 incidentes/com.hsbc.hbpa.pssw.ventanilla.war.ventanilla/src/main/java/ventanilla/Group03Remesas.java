//*************************************************************************************************
//             Funcion: Clase que realiza Pago de Remesas txn 5453
//            Elemento: Group03Remesas.java
//          Creado por: Rutilo Zarco Reyes
//      Modificado por: Fausto R. Flores Moreno
//*************************************************************************************************
// CCN -4360160- 30/06/2004 - Se agrega persistencia de datos en consulta de Bines
// CCN -4360189- 03/09/2004 - Se cambia invoke al Diario para Control de Efectivo
// CCN -4360259- 21/01/2005 - Se modifica el acceso a los bines
// CCN -4360456- 07/04/2006 - Se corrige campo lstbanco para txn 4487 y txn 5453.
//*************************************************************************************************
package ventanilla;

import java.io.IOException;
import java.util.Hashtable;
import java.util.Stack;
import java.util.Vector;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import ventanilla.com.bital.admin.Diario;
import ventanilla.com.bital.beans.BinesAMEX;
import ventanilla.com.bital.beans.BinesTDC;
import ventanilla.com.bital.sfb.Remesas;

public class Group03Remesas extends HttpServlet {
    private String getMonto(String nvoMonto) {
        StringBuffer txtMonto = new StringBuffer(nvoMonto);
        for(int j=0; j<txtMonto.length();) {
            if(txtMonto.charAt(j) == ',' || txtMonto.charAt(j) == '.')
                txtMonto.deleteCharAt(j);
            else
                ++j;
        }
        
        return txtMonto.toString();
    }
    
   
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession(false);
        Hashtable datasession = (Hashtable)session.getAttribute("page.datasession");
        Vector  bines = new Vector();
		Vector  binesAMEX = new Vector();
		GenericClasses gc = new GenericClasses();
        
        String compara = (String)datasession.get("cTxn");
        String txtTxn = (String)datasession.get("cTxn");
        
        //Variables de diario electronico
        String dirip =  (String)session.getAttribute("dirip");
        String txtServicio = "";
        
        if(txtTxn.equals("4487"))
            txtServicio = (String)datasession.get("txtCta4487");
        else
            txtServicio = (String)datasession.get("txtServicio");
        
        if ( txtServicio.equals("60")) {
            
            BinesTDC tdcbines = BinesTDC.getInstance();
            bines = tdcbines.getBines();
            session.setAttribute("bines",bines);
            
            String bin = "";
            int ok  = 1;
            String txttdc = (String)datasession.get("txtRefer1");
            if(txttdc.length() != 16)
                ok = 1;
            else {
                int large = bines.size();
                Vector row = new Vector();
                for(int j = 0; j < large; j++) {
                    row = (Vector)bines.get(j);
                    bin = row.get(0).toString();
                    if ( txttdc.startsWith(bin) ) {
                        ok = 0;
                        break;
                    }
                }
            }
            if ( ok != 0 ) {
                String msgrem =  "1~01~2~Numero de Tarjeta Invalida~";
                Stack flujotxn = (Stack)session.getAttribute("page.flujotxn");
                if (!flujotxn.empty()) {
                    flujotxn.removeAllElements();
                    session.setAttribute("page.flujotxn", flujotxn);
                }
                session.setAttribute("page.certifField","N");
                session.setAttribute("page.txnresponse", msgrem);
                getServletContext().getRequestDispatcher("/servlet/ventanilla.Redirector").forward(request,response);
                return;
            }
        }
        
		if ( txtServicio.equals("670")) {
            
			BinesAMEX tdcbinesAMEX = BinesAMEX.getInstance();
			binesAMEX = tdcbinesAMEX.getBines();
			session.setAttribute("bines",bines);
            
			String bin = "";
			int ok  = 1;
			String txttdc = (String)datasession.get("txtRefer1");
			if(txttdc.length() != 15)
				ok = 1;
			else {
				int large = binesAMEX.size();
				Vector row = new Vector();
				for(int j = 0; j < large; j++) {
					row = (Vector)binesAMEX.get(j);
					bin = row.get(0).toString();
					if ( txttdc.startsWith(bin) ) {
						ok = 0;
						break;
					}
				}
			}
			if ( ok != 0 ) {
				String msgrem =  "1~01~2~Numero de Tarjeta Invalida~";
				Stack flujotxn = (Stack)session.getAttribute("page.flujotxn");
				if (!flujotxn.empty()) {
					flujotxn.removeAllElements();
					session.setAttribute("page.flujotxn", flujotxn);
				}
				session.setAttribute("page.certifField","N");
				session.setAttribute("page.txnresponse", msgrem);
				getServletContext().getRequestDispatcher("/servlet/ventanilla.Redirector").forward(request,response);
				return;
			}
		}
        
        Remesas remesas = new Remesas();
        String txtTeller = (String)datasession.get("teller");
        remesas.setTxnCode(txtTxn);
        remesas.setTeller(txtTeller);
        remesas.setBranch((String)datasession.get("sucursal"));
        remesas.setSupervisor(txtTeller);
        
        if ( (String)datasession.get("override") != null )
            if (datasession.get("override").toString().equals("SI"))
                remesas.setOverride("3");
        
        if ( (String)datasession.get("supervisor") != null )
            remesas.setSupervisor((String)datasession.get("supervisor"));
        
        if(txtTxn.equals("4487"))
            remesas.setAcctNo((String)datasession.get("txtCta4487"));
        else
            remesas.setAcctNo((String)datasession.get("txtServicio"));
        remesas.setTranAmt(getMonto((String)datasession.get("txtMonto")));
        remesas.setReferenc1((String)datasession.get("txtRefer1"));
        remesas.setReferenc2((String)datasession.get("txtRefer2"));
        remesas.setReferenc3((String)datasession.get("txtRefer3"));
        remesas.setFromCurr(gc.getDivisa((String)datasession.get("moneda")));
        
        if(txtTxn.equals("4487")){
         	//remesas.setTrNo((String)datasession.get("lstBancoRem") + (String)datasession.get("lstCausa"));
			remesas.setTrNo("000" + (String)datasession.get("lstCausa"));
        }else if(txtTxn.equals("5487")){
         	remesas.setTrNo("000" + (String)datasession.get("lstCausa2"));
        }else{
			remesas.setTrNo((String)datasession.get("lstBanco") + (String)datasession.get("lstCausa2"));
        }
        
        remesas.setCheckNo((String)datasession.get("txtSerial"));
        String txtMoneda = (String)datasession.get("moneda");
        
        Diario diario = new Diario();
        String sMessage = diario.invokeDiario(remesas, (String)session.getAttribute("d_ConsecLiga"), dirip, txtMoneda, session);
        
        session.setAttribute("page.txnresponse", sMessage);
        getServletContext().getRequestDispatcher("/servlet/ventanilla.Redirector").forward(request,response);
        
        
    }
    
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}
