//*************************************************************************************************
//             Funcion: Clase que realiza txn Servicios
//            Elemento: Group02Serv.java
//          Creado por: Alejandro Gonzalez Castro
//      Modificado por: Alejandro Gonzalez Castro
//*************************************************************************************************
// CCN - 4360189 - 03/09/2004 - Se cambia invoke al Diario para Control de Efectivo
// CCN - 4360211 - 08/10/2004 - Concepto SAR 92
//*************************************************************************************************
package ventanilla;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.util.Hashtable;
import ventanilla.com.bital.sfb.ServiciosB;
import ventanilla.com.bital.sfb.ServiciosA;

import ventanilla.com.bital.admin.Diario;

public class Group02Serv extends HttpServlet
{
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession(false);
        Hashtable datasession = (Hashtable)session.getAttribute("page.datasession");
        String txn = (String)datasession.get("cTxn");
        String txtMoneda     = (String)datasession.get("moneda");
        String dirip =  (String)session.getAttribute("dirip");
        
        ServiciosB servicio = new ServiciosB();
        servicio.setBranch((String)datasession.get("sucursal"));
        servicio.setTeller((String)datasession.get("teller"));
        servicio.setSupervisor((String)datasession.get("teller"));
        
        ServiciosA servicioA = new ServiciosA();
        servicioA.setBranch((String)datasession.get("sucursal"));
        servicioA.setTeller((String)datasession.get("teller"));
        
        String txtCadImpresion = "";
        String sMessage = "";
        char status = '1';
        
        
       
        if( datasession.get("cTxn").toString().equals("0070") ) {
            txn = "0826";
            servicio.setFormat("B");
            servicio.setTxnCode(txn);
            servicio.setAcctNo((String)datasession.get("txtAfiliacion"));
            servicio.setTranAmt("000");
            servicio.setReferenc("VER210");
            servicio.setFromCurr("N$");
            servicio.setCashIn(request.getParameter("lstTipRet"));
            Diario diario = new Diario();
            sMessage = diario.invokeDiario(servicio, (String)session.getAttribute("d_ConsecLiga"), dirip, txtMoneda, session);
            if(sMessage.charAt(0) =='0')
               txtCadImpresion = "NOIMPRIMIRNOIMPRIMIR";
            datasession.put("posteo","1");
        }
        if (datasession.get("cTxn").toString().equals("0826")) 
        {
            String txtCasoEsp = "";
            if ( session.getAttribute("txtCasoEsp") != null )
              txtCasoEsp = session.getAttribute("txtCasoEsp").toString();
        
            String Tipopago       = datasession.get("lstTipPago").toString();
            String TipoRetiro     = datasession.get("lstTipRet").toString();
            String PersonaRetira  = datasession.get("lstPersona").toString();
            String txtFolioDoc    = datasession.get("txtNumfolio").toString();
            String txtAfore       = datasession.get("txtAfore").toString();
            String txtCURP        = datasession.get("txtCURP").toString();
            String txtISR         = datasession.get("txtISR").toString();
            String txtLeyenda     = datasession.get("txtLeyenda" ).toString();
            String txtLeyAport    = datasession.get("txtLeyAport" ).toString();
            String txtTipoID      = tipodoc(datasession.get("lstDocto").toString());
            String txtPersonaRetira = "";
            String numeroretiro   = datasession.get("retiro_selec").toString();
            String txtDescRecibo  = "";
            String txtDescripcion = "";
            String txtAfiliacion = datasession.get("txtAfiliacion").toString();
            String txtNumFolio = datasession.get("txtFolio" + numeroretiro).toString();
            String txtFecha = datasession.get("txtFecha" + numeroretiro).toString();
            String txtValorTitulo = datasession.get("txtValorTitulo" + numeroretiro).toString();
            String txtTitulos = datasession.get("txtTitulos" + numeroretiro).toString();
            String txtMontoT = datasession.get("txtMonto" + numeroretiro).toString();
            String txtMonto = datasession.get("txtMonto" + numeroretiro).toString();
            String txtMonto1 = datasession.get("txtMonto0" + numeroretiro).toString();
            String txtMonto2 = datasession.get("txtMonto1" + numeroretiro).toString();
            long txtmonto2 = 0;
            try{
               txtmonto2 = Long.parseLong(txtMonto2);
            }catch(NumberFormatException e){};

            String txtMontoDes1 = datasession.get("txtMonCar0" + numeroretiro).toString();
            String txtMontoDes2 = datasession.get("txtMonCar1" + numeroretiro).toString();            
            String txtTipo = "";
            String txtTipoPersona = "";
            if (PersonaRetira.equals("0")) {
                txtPersonaRetira = datasession.get("txtTitular").toString();
                txtTipoPersona = "TITULAR";
            }
            else {
                 txtPersonaRetira = datasession.get("txtBeneficiari" + PersonaRetira).toString();
                 txtTipoPersona = "BENEFICIARIO";
            }
            long monto1 = 0;
            if (TipoRetiro.equals("6") || TipoRetiro.equals("3")){
                try{
                   monto1 = Long.parseLong(txtMonto1);
                }
                catch(NumberFormatException e)
                {};
                txtMonto = txtMonto1;
                txtTipo = "1";
            }
            String Txn = "";
            if ( Tipopago.equals("1") ) {
                Txn = "4307";
                servicioA.setAcctNo("4004019428");
                servicioA.setCashOut(txtMonto);
            }
            else {
                Txn = "0071";
                servicioA.setCashOut("000");
                servicioA.setAcctNo1("4004019428");
                servicioA.setAcctNo2(datasession.get("txtCuenta").toString());
            }
            servicioA.setSupervisor((String)datasession.get("teller"));
            if ( (String)datasession.get("override") != null )
                 if (datasession.get("override").toString().equals("SI"))
                     servicioA.setOverride("3");
            if ( (String)datasession.get("supervisor") != null )
                 servicioA.setSupervisor((String)datasession.get("supervisor"));
            servicioA.setTxnCode(Txn);
            servicioA.setFormat( "A" );
            servicioA.setTranCur("N$");
            servicioA.setCheckNo("0000000000");
            servicioA.setCheckNo1("000");
            txtDescripcion = txtMontoDes1 + "-" + txtAfiliacion + "-" + txtNumFolio;
            servicioA.setTranDesc(txtDescripcion);
            servicioA.setTranAmt(txtMonto);
            servicioA.setAcctNo3(txtFolioDoc + txtTipoID + PersonaRetira);
            servicioA.setAcctNo4(txtAfiliacion + "0" + TipoRetiro + txtTipo);
            servicioA.setAcctNo5(txtFecha + txtAfore);
            String DescRev = "REV." + txtDescripcion;
            if(DescRev.length() > 40)
                    DescRev = DescRev.substring(0,40);
            servicioA.setDescRev(DescRev);
            if ( datasession.get("posteo").toString().equals("1")) {
               if ((TipoRetiro.equals("6") || TipoRetiro.equals("3")) && monto1 == 0)
                   status = '0';
               else
               {
                 Diario diario = new Diario();
                 sMessage = diario.invokeDiario(servicioA, (String)session.getAttribute("d_ConsecLiga"), dirip, txtMoneda, session);
                 status = sMessage.charAt(0);
                 if ( status == '0' || status == '1' )
                    txtCasoEsp = txtCasoEsp + Txn + "~"+ getString(sMessage,1) + "~";
               }
            }
            else
                status = '0';
               
            if( status == '0' )  {
               if ( datasession.get("posteo").toString().equals("1")) {
                  datasession.put("override","NO");
                  datasession.put("posteo","2");
               }
               try{Thread.sleep( 1500 );}
               catch (InterruptedException e){}
               if (TipoRetiro.equals("6") || TipoRetiro.equals("3")){
                   servicioA.setSupervisor((String)datasession.get("teller"));
                   servicioA.setOverride("0");
                   
                   if ( (String)datasession.get("override") != null )
                       if (datasession.get("override").toString().equals("SI"))
                           servicioA.setOverride("3");
                   if ( (String)datasession.get("supervisor") != null )
                       servicioA.setSupervisor((String)datasession.get("supervisor"));
                   
//                   if ( !txtMonto2.equals("")) {                    // Segundo envio
                   if ( txtmonto2 > 0) {                    // Segundo envio
                       txtMonto = txtMonto2;
                       txtDescripcion = txtMontoDes2 + "-" + txtAfiliacion + "-" + txtNumFolio;
                       servicioA.setTranDesc(txtDescripcion);
                       servicioA.setTranAmt(txtMonto);
                       if (Txn.equals("4307"))
                          servicioA.setCashOut(txtMonto);                           
                       DescRev = "REV." + txtDescripcion;
                       if(DescRev.length() > 40)
                          DescRev = DescRev.substring(0,40);
                       servicioA.setDescRev(DescRev);
                       Diario diario = new Diario();
                       sMessage = diario.invokeDiario(servicioA, (String)session.getAttribute("d_ConsecLiga"), dirip, txtMoneda, session);
                       status = sMessage.charAt(0);
                       if ( status == '0' || status == '1' )
                           txtCasoEsp = txtCasoEsp + Txn + "~" + getString(sMessage,1) + "~";
                   }
               }
            }
            if ( status == '0') {
                // colocar la cadena de impresion
                String txtR1 = "";
               String txtV1 = "";
               String txtR2 = "";
               String txtV2 = "";
               String txtR3 = "";
               String txtV3 = "";
               String txtR4 = "";
               String txtV4 = "";
               if (TipoRetiro.equals("3")) {
                   txtLeyenda = "SAR 92 Y VIVIENDA 92 ";
                   txtR1 = "IMPORTE SAR 92"; 
                   txtR2 = "IMPORTE VIVIENDA 92"; 
                   txtR3 = "ISR RETENIDO"; 
                   txtV1 = txtMonto1;
                   txtV2 = txtMonto2;
                   txtV3 = txtISR;
                   txtR4 = "TOTAL A PAGAR";
                   txtV4 = txtMontoT;
               }
               else
               if (TipoRetiro.equals("6")) {
                   txtLeyenda = "NEGATIVA DE PENSION";
                   txtR1 = "IMPORTE RCV"; 
                   txtR2 = "IMPORTE VIVIENDA 97"; 
                   txtR3 = "ISR RETENIDO"; 
                   txtV1 = txtMonto1;
                   txtV2 = txtMonto2;
                   txtV3 = txtISR;
                   txtR4 = "TOTAL A PAGAR";
                   txtV4 = txtMontoT;                   
               }
               else{
                   txtR1 = "ISR RETENIDO"; 
                   txtR2 = "TOTAL A PAGAR"; 
                   txtV1 = txtISR;
                   txtV2 = txtMontoT;
                   int Retiro = new Integer(TipoRetiro).intValue();
                   switch (Retiro) { 
                       case 1 : { txtLeyenda = "APORTACIONES VOLUNTARIAS PATRON"; 
                                  txtR4 = leyendaAport(txtLeyAport); 
                                  break;
                                 }
                       case 2 : { txtLeyenda = "APORTACIONES VOLUNTARIAS TRABAJADOR"; 
                                  txtR4 = leyendaAport(txtLeyAport);
                                  break;
                                }
                       case 4 : txtLeyenda = "DESEMPLEO"; break;
                       case 5 : txtLeyenda = "MATRIMONIO"; break;
                       case 7 : txtLeyenda = "NEGATIVA DE PENSION (PS)"; break;
                       case 8 : txtLeyenda = "PLAN PRIVADO DE PENSION"; break;
                   }
               }
               String txtCuenta = "";
               if ( datasession.get("txtCuenta") != null)
                   txtCuenta = datasession.get("txtCuenta").toString();
               txtCadImpresion = "~RECDISP~" + txtLeyenda + "~" + txtTipoPersona + "~" + (String)datasession.get("sucursal") +
                                 "~" + txtNumFolio + "~" + txtCURP + "~" +  txtAfiliacion + "~" + txtPersonaRetira + "~" + txtCuenta + 
                                 "~" + txtMontoT + "~" + txtTitulos + "~" + txtValorTitulo + "~" + txtR1 + "~" + txtV1 + "~" +
                                  txtR2 + "~" + txtV2 + "~" + txtR3 + "~" + txtV3 + "~" + txtR4 + "~" + txtV4 + "~";
            }
            else
                session.setAttribute("txtCadImpresion", "");       // Limpia el NOIMPRIMIRNOIMPRIMIR
            
            if (txtCasoEsp.length() > 0){
               session.setAttribute("txtCasoEsp", txtCasoEsp);
            }
            
            
        }
        session.setAttribute("page.datasession", datasession);
        if (txtCadImpresion.length() > 0){
           session.setAttribute("txtCadImpresion", txtCadImpresion);
        }
 
        session.setAttribute("page.txnresponse", sMessage);
        getServletContext().getRequestDispatcher("/servlet/ventanilla.Redirector").forward(request,response);
    }
    
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
    
    
    private String tipodoc(String doc) {
        String docu = "";
        if (doc.equals("IDLI"))
        {docu = "1";}
        else if(doc.equals("IDPA"))
        {docu = "2";}
        else if(doc.equals("IDCP"))
        {docu = "3";}
        else if(doc.equals("IDNC"))
        {docu = "4";}
        else if(doc.equals("IDCA"))
        {docu = "5";}
        else if(doc.equals("IDCE"))
        {docu = "6";}
        else if(doc.equals("IDXF"))
        {docu = "7";}
        return docu;
    }
    
  private String leyendaAport(String doc) {
        String docu = "";
        if (doc.startsWith("T"))
        {docu = "RETIRO TOTAL";}
        else if(doc.startsWith("P"))
        {docu = "RETIRO PARCIAL";}
        else if(doc.startsWith("N"))
        {docu = "NO APLICA";}
        return docu;
    }    
    
  private String getString(String newCadNum, int newOption)
  {
   int iPIndex = newCadNum.indexOf("CONSECUTIVO:") + 12, NumSaltos, Num = 0, Ban = 0;
   String nCadNum = new String("");
   NumSaltos = newOption;
   while(Num < NumSaltos)
   {
    if(newCadNum.charAt(iPIndex) == 0x20)
    {
     while(newCadNum.charAt(iPIndex) == 0x20)
      iPIndex++;
     Num++;
    }
    else
     while(newCadNum.charAt(iPIndex) != 0x20)
      iPIndex++;
   }
   while(newCadNum.charAt(iPIndex) != 0x20 && iPIndex < newCadNum.length()){
    nCadNum = nCadNum + newCadNum.charAt(iPIndex);
    if(iPIndex < newCadNum.length())
      iPIndex++;
    if(iPIndex == newCadNum.length())
      break;
   }
   if ( nCadNum.endsWith("~"))
       nCadNum = nCadNum.substring(0,nCadNum.length()-1);
   return nCadNum;
  }    
    
}
