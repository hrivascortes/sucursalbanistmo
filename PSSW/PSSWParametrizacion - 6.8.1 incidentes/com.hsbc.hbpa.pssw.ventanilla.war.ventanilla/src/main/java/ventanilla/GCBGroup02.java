//*************************************************************************************************
//             Funcion: Clase que realiza txn 0816 - 0576
//            Elemento: GCBGroup02.java
//          Creado por: Alejandro Gonzalez Castro
//      Modificado por: Alejandro Gonzalez Castro
//*************************************************************************************************
// CCN - 4360189 - 03/09/2004 - Se cambia invoke al Diario para Control de Efectivo
// CCN - 4360368 - 02/09/2005 - Se elimina sleep
// CCN - 4360374 - 08/09/2005 - Se genera nuevo paquete a peticion de QA.
//*************************************************************************************************
package ventanilla;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.util.Calendar;
import java.util.Hashtable;

import ventanilla.com.bital.sfb.CompraVenta;
import ventanilla.com.bital.admin.Diario;

public class GCBGroup02 extends HttpServlet {
    private String stringFormat(int option) {
        Calendar now = Calendar.getInstance();
        String temp = new Integer(now.get(option)).toString();
        if( temp.length() != 2 )
            temp = "0" + temp;

        return temp;
    }

    private String delCommaPointFromString(String newCadNum) {
        String nCad = new String(newCadNum);
        if( nCad.indexOf(".") > -1) {
            nCad = nCad.substring(nCad.indexOf(".") + 1, nCad.length());
            if(nCad.length() != 2) {
                String szTemp = new String("");
                for(int j = nCad.length(); j < 2; j++)
                    szTemp = szTemp + "0";
                newCadNum = newCadNum + szTemp;
            }
        }

        StringBuffer nCadNum = new StringBuffer(newCadNum);
        for(int i = 0; i < nCadNum.length(); i++)
            if(nCadNum.charAt(i) == ',' || nCadNum.charAt(i) == '.')
                nCadNum.deleteCharAt(i);

        return nCadNum.toString();
    }

    private String getString(String newCadNum, int NumSaltos) {
        int iPIndex = newCadNum.indexOf("CONSECUTIVO:") + 12, Num = 0, Ban = 0;
        String nCadNum = new String("");
/*   NumSaltos = 4;
   if(newOption == 0)
    NumSaltos = 3;*/
        while(Num < NumSaltos) {
            if(newCadNum.charAt(iPIndex) == 0x20) {
                while(newCadNum.charAt(iPIndex) == 0x20)
                    iPIndex++;
                Num++;
            }
            else
                while(newCadNum.charAt(iPIndex) != 0x20)
                    iPIndex++;
        }
        while(newCadNum.charAt(iPIndex) != 0x20) {
            nCadNum = nCadNum + newCadNum.charAt(iPIndex++);
            if(newCadNum.charAt(iPIndex) == 0x20 && newCadNum.charAt(iPIndex + 1) == 0x2A) {
                nCadNum = nCadNum + newCadNum.charAt(iPIndex) + newCadNum.charAt(iPIndex + 1);
                iPIndex += 2;
            }
        }

        return nCadNum;
    }

    private String getTipoCambio(String newTipo, int newZeroes) {
        int newCadLen = newTipo.length();
        for(int i = newCadLen; i < newCadLen + newZeroes; i++)
            newTipo = "0" + newTipo;

        return newTipo;
    }

    public void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String time = stringFormat(Calendar.HOUR_OF_DAY) + stringFormat(Calendar.MINUTE) + stringFormat(Calendar.SECOND);
        String TE, TO, Currency, Currency2, rspDiario;
        String lstForPago = new String("");
        String txtCasoEsp = new String("");
        String txtCadImpresion = new String("");

        CompraVenta oTxn = new CompraVenta();

        HttpSession session = req.getSession(false);
        Hashtable datasession = (Hashtable)session.getAttribute("page.datasession");
        TE = (String)datasession.get("cTxn");
        TO = (String)datasession.get("oTxn");
        Currency = (String)datasession.get("lstMoneda");
        Currency2 = (String)datasession.get("moneda");
        String dirip =  (String)session.getAttribute("dirip");

        oTxn.setTxnCode(TE);
        oTxn.setBranch((String)datasession.get("sucursal"));
        oTxn.setTeller((String)datasession.get("teller"));
        oTxn.setSupervisor((String)datasession.get("teller"));

        Diario diario = new Diario();
        String sMessage="";

        if( TE.equals( "0816" ) ){
            oTxn.setFormat( "G" );
            oTxn.setService("WCH");
            oTxn.setToCurr("US$");
            oTxn.setCheckNo(new String((String)datasession.get("txtSerial5")));
            oTxn.setTranAmt("1");
            oTxn.setAcctNo("0");
            oTxn.setInstruc((String)datasession.get("txtAutori"));

            sMessage = diario.invokeDiario(oTxn, (String)session.getAttribute("d_ConsecLiga"), dirip, Currency2, session);
            session.setAttribute("page.txnresponse", sMessage);
        }
        else if( TE.equals( "0576" ) )
        {
            String lstForLiq = (String)datasession.get("lstForLiq");
            String txtCuenta = "0000000000~";
            oTxn.setFormat( "G" );
            oTxn.setService("WCH");
            oTxn.setToCurr("US$");
            oTxn.setCheckNo(new String((String)datasession.get("txtSerial5")));
            oTxn.setGStatus("3");
            oTxn.setBcoTrnsf("021000089");
            oTxn.setTranAmt(delCommaPointFromString((String)datasession.get("txtMonto")));
            oTxn.setAcctNo("0576");
            if(lstForLiq.equals("02"))
            {
                txtCuenta = (String)datasession.get("txtDDACuenta");
                TE = "4093";
                oTxn.setTxnCode(TE);
                oTxn.setAcctNo((String)datasession.get("txtDDACuenta"));
            }
            oTxn.setBenefic((String)datasession.get("txtBenef"));
            oTxn.setOrdenan((String)datasession.get("txtNomOrd"));
            oTxn.setInstruc((String)datasession.get("txtAutori"));

            sMessage = diario.invokeDiario(oTxn, (String)session.getAttribute("d_ConsecLiga"), dirip, Currency2, session);
            session.setAttribute("page.txnresponse", sMessage);

            if(oTxn.getMessage().substring(0,1).equals("0")){

                txtCasoEsp = txtCasoEsp + TE + "~" + getString(oTxn.getMessage(),1) + "~";
                String Monto = delCommaPointFromString((String)datasession.get("txtMonto"));
                String txtSerial = (String)datasession.get("txtSerial5");
                String txtMonto = delCommaPointFromString((String)datasession.get("txtMonto"));
                String txtOrdenante = (String)datasession.get("txtNomOrd");
                String txtBeneficiario = (String)datasession.get("txtBenef");

                txtCadImpresion = "~" + "GIROCITI1~CAN" + "~" + txtSerial + "~" + txtMonto +
                "~" + "000" + "~" + "000" + "~" + "000" +
                "~" + txtOrdenante + "~" + txtBeneficiario + "~" + txtCuenta;
            }
        }
        if(txtCasoEsp.length() > 0){
            session.setAttribute("txtCasoEsp", txtCasoEsp);
        }
        session.setAttribute("txtCadImpresion", oTxn.getMessage());
        if(txtCadImpresion.length() > 0){
            session.setAttribute("txtCadImpresion", txtCadImpresion);
        }
        if(lstForPago != null)
            if(lstForPago.equals("02") && TE.equals("0816"))
                session.setAttribute("page.txnresponse", "0~03~0152522~CONSECUTIVO:  152522    0    00000000000 00                                                                            00000000000~");
            else{
                session.setAttribute("page.txnresponse", oTxn.getMessage());
            }
        else{
            session.setAttribute("page.txnresponse", oTxn.getMessage());
        }

        getServletContext().getRequestDispatcher("/servlet/ventanilla.Redirector").forward(req,resp);
    }

    public void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doPost(req, resp);
    }
}
