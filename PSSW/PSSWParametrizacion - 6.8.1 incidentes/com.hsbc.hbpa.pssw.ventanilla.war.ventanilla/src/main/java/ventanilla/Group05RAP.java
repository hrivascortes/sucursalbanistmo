//*************************************************************************************************
//             Funcion: Clase que realiza txn 0510 Remesas
//            Elemento: Group05RAP.java
//          Creado por: Alejandro Gonzalez Castro
//      Modificado por: Alejandro Gonzalez Castro
//*************************************************************************************************
// CCN - 4360189 - 03/09/2004 - Se cambia invoke al Diario para Control de Efectivo
//*************************************************************************************************
package ventanilla;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import ventanilla.com.bital.admin.Diario;
import ventanilla.com.bital.sfb.RAPB;

import java.util.Hashtable;
import java.util.Stack;

public class Group05RAP extends HttpServlet
{

    private String getString(String newCadNum, int newOption) {
        int iPIndex = newCadNum.indexOf("CONSECUTIVO:") + 12, NumSaltos, Num = 0, Ban = 0;
        String nCadNum = new String("");
        NumSaltos = newOption;
        while(Num < NumSaltos) {
            if(newCadNum.charAt(iPIndex) == 0x20) {
                while(newCadNum.charAt(iPIndex) == 0x20)
                    iPIndex++;
                Num++;
            }
            else
                while(newCadNum.charAt(iPIndex) != 0x20)
                    iPIndex++;
        }
        while(newCadNum.charAt(iPIndex) != 0x20 && iPIndex < newCadNum.length()){
            nCadNum = nCadNum + newCadNum.charAt(iPIndex);
            if(iPIndex < newCadNum.length())
                iPIndex++;
            if(iPIndex == newCadNum.length())
                break;
        }

        return nCadNum;
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/plain");
        HttpSession session = request.getSession(false);
        Hashtable datasession = (Hashtable)session.getAttribute("page.datasession");
		GenericClasses gc = new GenericClasses();

        String dirip =  (String)session.getAttribute("dirip");
        String monto = "";

        RAPB rapb = new RAPB();
        rapb.setFormat("B");
        rapb.setBranch((String)datasession.get("sucursal"));
        rapb.setTeller((String)datasession.get("teller"));
        rapb.setSupervisor((String)datasession.get("supervisor"));
        rapb.setTxnCode("0510");
        
        rapb.setAcctNo("0510");

        StringBuffer montotmp = new StringBuffer((String)datasession.get("txtMonto"));
        for(int i=0; i<montotmp.length();)
        {
            if( montotmp.charAt(i) == ',' || montotmp.charAt(i) == '.' )
                montotmp.deleteCharAt(i);
            else
                ++i;
        }
        
        monto = montotmp.toString();
        
        rapb.setTranAmt(monto);
        //rapb.setReferenc((String)datasession.get("txtSerial"));
		rapb.setReferenc("0");
        rapb.setFeeAmoun((String)datasession.get("lstBancoRemCV"));

        String moneda = (String)datasession.get("moneda");
		rapb.setFromCurr(gc.getDivisa(moneda));
        String txtMontoTot = "";
        long MontoREM = 0;
        long MontoTotal = 0;

        txtMontoTot = (String)datasession.get("MontoTotal");
        if( txtMontoTot != null )
            MontoTotal = Long.parseLong(txtMontoTot);

        StringBuffer montorem = new StringBuffer((String)datasession.get("txtRemesas"));
        for(int i=0; i<montorem.length();) {
            if( montorem.charAt(i) == ',' || montorem.charAt(i) == '.' )
                montorem.deleteCharAt(i);
            else
                ++i;
        }

        MontoREM= Long.parseLong(montorem.toString());
        
        rapb.setIntern((String)session.getAttribute("ConsecREM"));
        rapb.setIntWh((String)session.getAttribute("ServREM"));

        Diario diario = new Diario();
        String sMessage = diario.invokeDiario(rapb, (String)session.getAttribute("d_ConsecLiga"), dirip, moneda, session);

        if ( sMessage.charAt(0) == '0' )
        {
            MontoTotal = MontoTotal + Long.parseLong(monto.toString());
            datasession.put("MontoTotal",new Long(MontoTotal).toString());
        }
        
        Stack flujotxn = (Stack)session.getAttribute("page.flujotxn");

        if (MontoTotal != MontoREM) 
        {
            flujotxn.push("0510");
            session.setAttribute("page.flujotxn", flujotxn);
            datasession.remove("txtSerial");
            datasession.remove("txtMonto");
        }

        session.setAttribute("page.txnresponse", rapb.getMessage());
        getServletContext().getRequestDispatcher("/servlet/ventanilla.Redirector").forward(request,response);
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}