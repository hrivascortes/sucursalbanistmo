//*************************************************************************************************
//             Funcion: Clase que realiza txn Inversiones
//            Elemento: Group04I.java
//          Creado por: Alejandro Gonzalez Castro
//      Modificado por: Alejandro Gonzalez Castro
//*************************************************************************************************
// CCN - 4360189 - 03/09/2004 - Se cambia invoke al Diario para Control de Efectivo
//*************************************************************************************************
package ventanilla;

import java.io.IOException;
import java.util.Hashtable;
import java.util.Stack;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import ventanilla.com.bital.sfb.InversionesB;
import ventanilla.com.bital.admin.Diario;

public class Group04I extends HttpServlet {
    private String delCommaPointFromString(String newCadNum) {
        String nCad = new String(newCadNum);
        if( nCad.indexOf(".") > -1) {
            nCad = nCad.substring(nCad.indexOf(".") + 1, nCad.length());
            if(nCad.length() != 2) {
                String szTemp = new String("");
                for(int j = nCad.length(); j < 2; j++)
                    szTemp = szTemp + "0";
                newCadNum = newCadNum + szTemp;
            }
        }

        StringBuffer nCadNum = new StringBuffer(newCadNum);
        for(int i = 0; i < nCadNum.length(); i++)
            if(nCadNum.charAt(i) == ',' || nCadNum.charAt(i) == '.')
                nCadNum.deleteCharAt(i);

        return nCadNum.toString();
    }

    public void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {
        InversionesB oFormatoB = new InversionesB();

        resp.setContentType("text/plain");

        HttpSession session = req.getSession(false);
        Hashtable datasession = (Hashtable)session.getAttribute("page.datasession");
        String dirip = (String)session.getAttribute("dirip");
        String txtTxnCode    = (String)datasession.get("cTxn");
        String txtFormat     = "B";
        String txtBranch     = (String)datasession.get("sucursal");
        String txtTeller     = (String)datasession.get("teller");//"000108";
        String txtSupervisor = (String)datasession.get("teller");//"000101";
        String txtBackOut    = "0";
        String txtOverride   = "0";
        String txtCurrentDay = "0";

        if ((String)datasession.get("supervisor")!= null)
            txtSupervisor = (String)datasession.get("supervisor");

        oFormatoB.setTxnCode(txtTxnCode);
        oFormatoB.setFormat(txtFormat);
        oFormatoB.setBranch(txtBranch);
        oFormatoB.setTeller(txtTeller);
        oFormatoB.setSupervisor(txtSupervisor);
        oFormatoB.setBackOut(txtBackOut);
        oFormatoB.setOverride(txtOverride);
        oFormatoB.setCurrentDay(txtCurrentDay);
        String txtMonto      = delCommaPointFromString((String)datasession.get("txtMontoCheque"));
        String txtMoneda     = (String)datasession.get("moneda");
        String txtSerial     = (String)datasession.get("txtSerial");
        String lstBanco      = (String)datasession.get("lstBanco");
        String txtEfectivo   = "000";

        if(txtMoneda.equals("01"))
            txtMoneda = "N$"; //Nuevos pesos
        else {
            if(txtMoneda.equals("02"))
                txtMoneda = "US$"; //Dolares
            else
                if(txtMoneda.equals("03"))
                    txtMoneda = "UDI"; //Unidades de Inversion
        }

        String txtMontoTot   = (String)datasession.get("MontoTotalR");
        long MontoRemesa     = Long.parseLong(delCommaPointFromString((String)datasession.get("txtMontoRemesa")));
        long MontoTotalR      = 0;

        if( txtMontoTot != null )
            MontoTotalR = Long.parseLong(txtMontoTot);

        oFormatoB.setAcctNo(txtTxnCode);
        oFormatoB.setReferenc(txtSerial);
        oFormatoB.setTranAmt(txtMonto);                   //Monto Total de la Txn.
        oFormatoB.setFromCurr(txtMoneda);                  //Tipo de Moneda con que se opera la Txn.
        oFormatoB.setFeeAmoun(lstBanco);

        Diario diario = new Diario();
        String sMessage = diario.invokeDiario(oFormatoB, (String)session.getAttribute("d_ConsecLiga"), dirip, txtMoneda, session);

        long Monto     = Long.parseLong(delCommaPointFromString(txtMonto));

        if ( sMessage.charAt(0) == '0' )
        {
            MontoTotalR = MontoTotalR + Monto;
            datasession.put("MontoTotalR",new Long(MontoTotalR).toString());
        }

        Stack flujotxn = (Stack)session.getAttribute("page.flujotxn");
        if ( MontoTotalR != MontoRemesa )
        {
            flujotxn.push("0544");
            session.setAttribute("page.flujotxn", flujotxn);
            session.setAttribute("page.txnresponse", sMessage);
            getServletContext().getRequestDispatcher("/servlet/ventanilla.Redirector").forward(req,resp);
        }
        else
        {
            session.setAttribute("page.txnresponse", sMessage);
            getServletContext().getRequestDispatcher("/servlet/ventanilla.Redirector").forward(req,resp);
        }
    }
    public void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doPost(req, resp);
    }
}
