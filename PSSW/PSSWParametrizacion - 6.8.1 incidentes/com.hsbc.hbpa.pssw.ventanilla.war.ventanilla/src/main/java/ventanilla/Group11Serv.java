//*************************************************************************************************
//             Funcion: Clase que realiza txn Servicios
//            Elemento: Group11Serv.java
//          Creado por: Alejandro Gonzalez Castro
//      Modificado por: Alejandro Gonzalez Castro
//*************************************************************************************************
// CCN - 4360189 - 03/09/2004 - Se cambia invoke al Diario para Control de Efectivo
//*************************************************************************************************
package ventanilla;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.util.Hashtable;

import ventanilla.com.bital.sfb.ServiciosA;
import ventanilla.com.bital.admin.Diario;

public class Group11Serv extends HttpServlet 
{
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession(false);
        Hashtable datasession = (Hashtable)session.getAttribute("page.datasession");
        String txn = (String)datasession.get("cTxn");
		GenericClasses gc = new GenericClasses();
        String moneda = gc.getDivisa((String)datasession.get("moneda"));
        String[] invoqued = request.getParameterValues("txtInvoqued");
        String invTxn = "";
        if(invoqued != null)
            invTxn = (String)invoqued[0];
        String dirip =  (String)session.getAttribute("dirip");
        String msg = "";
        String Monto = "";
        String txtDescrip="";
        
        ServiciosA servicioA = new ServiciosA();
        servicioA.setBranch((String)datasession.get("sucursal"));
        servicioA.setTeller((String)datasession.get("teller"));
        servicioA.setSupervisor((String)datasession.get("teller"));
        servicioA.setFormat("A");
        servicioA.setTxnCode(txn);
        
        if ( (String)datasession.get("override") != null )
                 if (datasession.get("override").toString().equals("SI"))
                     servicioA.setOverride("3");
            if ( (String)datasession.get("supervisor") != null )
                 servicioA.setSupervisor((String)datasession.get("supervisor"));
        
        servicioA.setAcctNo((String)datasession.get("txtDDACuenta"));
        Monto = quitap((String)datasession.get("txtMontoNoCero"));
        servicioA.setTranAmt(Monto);
        servicioA.setTranCur(moneda);
        servicioA.setCheckNo((String)datasession.get("txtnumiden"));
        servicioA.setCashOut(Monto);
        servicioA.setMoamoun("000");
        if (txn.equals("5405")) {
            txtDescrip="FACT" + (String)datasession.get("txtfact") + "FRACC" + (String)datasession.get("txtfracc") + "No" + (String)datasession.get("txtNo") + "-" + (String)datasession.get("lsttipiden") + (String)datasession.get("lstsigno");
        }
        if (txn.equals("5407")) {
            txtDescrip=(String)datasession.get("txtNumFolio") + "-" + (String)datasession.get("lsttipiden");
        }       
        servicioA.setTranDesc(txtDescrip);
        String DescRev = "REV. " + txtDescrip;
        if(DescRev.length() > 40)
            DescRev = DescRev.substring(0,40);
        servicioA.setDescRev(DescRev);       
        
        
        Diario diario = new Diario();
        String sMessage = diario.invokeDiario(servicioA, (String)session.getAttribute("d_ConsecLiga"), dirip, moneda, session);
        
        session.setAttribute("page.txnresponse", sMessage);
        getServletContext().getRequestDispatcher("/servlet/ventanilla.Redirector").forward(request,response);
    }
    
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
    
    private String quitap(String Valor) {
        StringBuffer Cantidad = new StringBuffer(Valor);
        for(int i=0; i<Cantidad.length();) {
            if( Cantidad.charAt(i) == ',' || Cantidad.charAt(i) == '.' )
                Cantidad.deleteCharAt(i);
            else
                ++i;
        }
        return Cantidad.toString();
    }
    
    public String getmoneda(String cmoneda) {
        String newmoneda = "";
        if (cmoneda.equals("N$"))
        { newmoneda = "01";}
        else if (cmoneda.equals("US$"))
        { newmoneda = "02";}
        else
        { newmoneda = "03";}
        return newmoneda;
    }
    
   
    public String removeAsterisk(String campo) {
        StringBuffer result = new StringBuffer(campo);
        String caracter = result.substring(result.length()-1,result.length());
        if (caracter.equals("*"))
            result.deleteCharAt(result.length()-1);
        return result.toString();
    }
    
}
