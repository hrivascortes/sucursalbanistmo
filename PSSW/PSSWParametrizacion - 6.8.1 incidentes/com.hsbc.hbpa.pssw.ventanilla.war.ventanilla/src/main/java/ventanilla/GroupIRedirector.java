package ventanilla;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.util.Stack;
import java.util.Hashtable;


public class GroupIRedirector extends HttpServlet
{
  private String formatMonto(String s)
  {
    int len = s.length();
    if (len < 3){
    	s = "0." + s;
    }
    else
    {
      int n = (len - 3 ) / 3;
    	s = s.substring(0, len - 2) + "." + s.substring(len - 2 , len);
      for (int i = 0; i < n; i++)
      {
      	len = s.length();
    		s = s.substring(0, (len -(i*3+6+i))) + "," + s.substring((len -(i*3+6+i)) , len);
    	}
    }
    return s;
  }

  private String delCommaPointFromString(String newCadNum)
  {
    String nCad = new String(newCadNum);
    if ( nCad.indexOf(".") > -1) {
       nCad = nCad.substring(nCad.indexOf(".") + 1, nCad.length());
       if (nCad.length() != 2) {
           String szTemp = new String("");
           for (int j = nCad.length(); j < 2; j++)
               szTemp = szTemp + "0";
           newCadNum = newCadNum + szTemp;
       }
    }

    StringBuffer nCadNum = new StringBuffer(newCadNum);
    for (int i = 0; i < nCadNum.length(); i++)
        if (nCadNum.charAt(i) == ',' || nCadNum.charAt(i) == '.')
            nCadNum.deleteCharAt(i);

    return nCadNum.toString();
  }

  public void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
  {
       HttpSession session = req.getSession(false);
       Stack flujotxn = (Stack)session.getAttribute("page.flujotxn");
       Hashtable ht = (Hashtable)session.getAttribute("page.datasession");
       session.setAttribute("page.flujotxn", flujotxn);
       String txtTipoRet3313 = (String)ht.get("lstTipoRet3313");
       String txtMoneda = (String)ht.get("moneda");
       String txtCDACuenta = (String)ht.get("txtCDACuenta");
       String txtNivelCajero = (String)ht.get("tllrlvl");                   // Nivel de usuario

       if ( txtMoneda.equals("01") || txtMoneda.equals("02"))                         // Moneda Nacional o Dolares
       {
       	    if ( txtTipoRet3313.equals("06"))                                    // Terminar
       	    {
  //             getServletContext().setAttribute("page.txnresponse", oFiduciario.getMessage());
               getServletContext().getRequestDispatcher("/servlet/ventanilla.Redirector").forward(req,resp);
       	    }
       	    else {
            if ( txtTipoRet3313.equals("01") || txtTipoRet3313.equals("05") ) { // Retiro Parcial o Complementario
            	flujotxn.push("3009");
            	flujotxn.push("3314");
              session.setAttribute("page.flujotxn", flujotxn);
              long MontoInver       = Long.parseLong(delCommaPointFromString((String)ht.get("txtMontoRetiro")));
              long MontoReinver      = Long.parseLong(delCommaPointFromString((String)ht.get("txtMontoRC")));
              if ( txtTipoRet3313.equals("01"))                                // Retiro Parcial
                  MontoReinver = MontoInver - MontoReinver;
              else
                  MontoReinver = MontoInver + MontoReinver;                     // Complementario
              String txtMonto = formatMonto(new Long(MontoReinver).toString());
              ht.put("txtMontoRC",txtMonto);
            }

            if ( txtTipoRet3313.equals("02") ) {                               // Retiro Total
            	flujotxn.push("3314");
                session.setAttribute("page.flujotxn", flujotxn);
            }
            if ( txtTipoRet3313.equals("03") ) {                               // Retiro con Cheque de Caja
                flujotxn.push("FMC1");
            	flujotxn.push("0063");
                session.setAttribute("page.flujotxn", flujotxn);
            }
            if ( txtTipoRet3313.equals("04") ) {                               // Retiro con Cheque de Caja
            	flujotxn.push("0044");
                session.setAttribute("page.flujotxn", flujotxn);
                ht.put("txtCDAO",txtCDACuenta);
            }
            getServletContext().getRequestDispatcher("/servlet/ventanilla.PageServlet?transaction=" + ht.get("iTxn") +
                                              "&transaction1=" + ht.get("oTxn")).forward(req,resp);
            }
       }
       else {                                       // UDIS
           if ( txtTipoRet3313.equals("01")  ) {    // Transferencia UIDS a vista
              if ( txtNivelCajero.equals("1")) {     // Cajero sucursal
              	flujotxn.push("9803");
              	flujotxn.push("9815");
              }
              else {                                // Cajero departamental
                flujotxn.push("9711");
              	flujotxn.push("9809");
              }
              session.setAttribute("page.flujotxn", flujotxn);
              getServletContext().getRequestDispatcher("/servlet/ventanilla.PageServlet?transaction=" + ht.get("iTxn") +
                                              "&transaction1=" + ht.get("oTxn")).forward(req,resp);
            }
            else
            {
//               getServletContext().setAttribute("page.txnresponse", oFiduciario.getMessage());
               getServletContext().getRequestDispatcher("/servlet/ventanilla.Redirector").forward(req,resp);
            }

       }
  }
  public void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
  {
    doPost(req, resp);
  }
}
