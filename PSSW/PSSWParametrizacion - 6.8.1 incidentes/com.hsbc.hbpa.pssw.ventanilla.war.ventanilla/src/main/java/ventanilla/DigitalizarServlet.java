//*************************************************************************************************
//             Funcion: Clase Digitalizacion de cheques
//            Elemento: DigitalizarServlet.java
//          Creado por: Alejandro Gonzalez Castro
//      Modificado por: Juvenal Fernandez
//*************************************************************************************************
// CCN - 4360334 - 17/06/2005 - Se elimina variable dbQualifier
//*************************************************************************************************

package ventanilla;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import ventanilla.com.bital.admin.OperacionesDBBean;

public class DigitalizarServlet extends HttpServlet 
{
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, NullPointerException 
	{
		javax.servlet.http.HttpSession sessions = request.getSession(false);
		String statementPS = com.bital.util.ConnectionPoolingManager.getStatementPostFix();
		String strOpcion = new String("");
		String strCajero = new String("000101");
		String strError = new String(" ");
		String strRespuesta = new String("");
		String strAccion = new String("");
		String strMoneda = new String("");
		OperacionesDBBean rbQuery = new OperacionesDBBean();
        
		if(sessions.getAttribute("opcion1") != null)
			strOpcion = (String) sessions.getAttribute("opcion1");
		else
			if(request.getParameter("opcion1") != null)
				strOpcion = request.getParameter("opcion1").toString();
		if(sessions.getAttribute("teller")!= null)
			strCajero = sessions.getAttribute("teller").toString();
		if(sessions.getAttribute("moneda") != null)
			strMoneda = (String) sessions.getAttribute("moneda");
		else
			if(request.getParameter("moneda") != null)
				strMoneda = request.getParameter("moneda").toString();
		if (strOpcion.equals("1")) 
		{
			String strCajeroTmp = new String("");
			String strPasswordTmp = new String("");
			String strCeros = new String("0000000000");
			if(sessions.getAttribute("cajero") != null)
				strCajeroTmp = (String) sessions.getAttribute("cajero");
			else
				if(request.getParameter("cajero") != null)
					strCajeroTmp = request.getParameter("cajero").toString();
			if(sessions.getAttribute("passwordtmp") != null)
				strPasswordTmp = (String) sessions.getAttribute("passwordtmp");
			else
				if(request.getParameter("passwordtmp") != null)
					strPasswordTmp = request.getParameter("passwordtmp").toString();
			strPasswordTmp = strPasswordTmp.toUpperCase();
			if (strCajeroTmp.length() < 6)
				strCajeroTmp = strCeros.substring(0,6-strCajeroTmp.length()) + strCajeroTmp;
			strAccion = "SELECT  N_NIV_DIGITALIZ, D_CLAVE_ACC FROM TC_USUARIOS_SUC WHERE C_CAJERO ='" + strCajeroTmp + "'" + statementPS;
			strRespuesta = rbQuery.EjecutaQuery(strAccion);
			if (strRespuesta.length() > 1)
			{
				if (strRespuesta.substring(0,1).equals("S"))
				{
					strRespuesta = strRespuesta.substring(2,strRespuesta.length()-1);
					if (strRespuesta.equals(strPasswordTmp))
					{
						strAccion = "SELECT  D_CONSEC_HOGAN, D_DATOS_CHEQUE , D_IMPORTE_CHEQUE, D_CORTE FROM TW_CONSEC_VOL_DDA WHERE C_NUMERO_CAJERO ='" + strCajero.substring(4,6) +"' AND C_NUMERO_SUCURSAL = '" + strCajero.substring(0,4) +  "' AND D_DOCUM_ENTREGADO = 'N' AND D_STATUS = 'A'" + statementPS;
						strRespuesta = rbQuery.EjecutaQuery(strAccion);
						if (strRespuesta.length() > 30)
						{
							strRespuesta = "DIGITA~1~" + strCajeroTmp + "~" + strMoneda + "~" + strRespuesta;
							sessions.setAttribute("txtCadImpresion", strRespuesta);							
							strAccion = "UPDATE TW_CONSEC_VOL_DDA set D_DOCUM_ENTREGADO = 'E', D_TIPO_DOCUMENTO = '1',  D_CAJERO_SUCURSAL = '" + strCajeroTmp + "' WHERE C_NUMERO_CAJERO = '" + strCajero.substring(4,6) + "' AND C_NUMERO_SUCURSAL ='" + strCajero.substring(0,4) + "' AND D_DOCUM_ENTREGADO = 'N' AND D_STATUS = 'A'";
							rbQuery.InsertUpdateRegistro(strAccion);
							if( rbQuery.getStatus() < 0)
								strError = "No se pudo Realizar la Entrega de Documento (TW_CONSEC_VOL_DDA)";						
							/*else 
								if( rbQuery.getStatus() > 0)
								{
									strAccion = "SELECT  D_CONSEC_HOGAN, D_DATOS_CHEQUE , D_IMPORTE_CHEQUE, D_CORTE FROM "+ dbQualifier + ".TW_CONSEC_VOL_DDA WHERE C_NUMERO_CAJERO ='" + strCajero.substring(4,6) +"' AND C_NUMERO_SUCURSAL = '" + strCajero.substring(0,4) +  "' AND D_DOCUM_ENTREGADO = 'E'" + statementPS;
									strRespuesta = "DIGITA~1~" + strCajeroTmp + "~" + strMoneda + "~" + rbQuery.EjecutaQuery(strAccion);
									sessions.setAttribute("txtCadImpresion", strRespuesta);
								}*/							
						}
						else
							strError = "No Existen Documentos por entregar ";
					}
					else
						strError = "Password Incorrecto del Cajero " + strCajeroTmp ;
				}
				else
					strError = "El Cajero " + strCajeroTmp + " No Tiene Permisos para Digitalizar";
			}
			else
				strError = "No existe el Cajero " + strCajeroTmp;
			sessions.setAttribute("NoError",strError);
		}
		else if (strOpcion.equals("2")) 
		{
			sessions.setAttribute("TipoDoc","1");
		}
		else if (strOpcion.equals("3")) 
		{
			String strOpc = new String("");
			String strCajeroTmp = new String("000000");            
			strCajeroTmp = strCajero;
			if(sessions.getAttribute("opcion") != null)
				strOpc = (String) sessions.getAttribute("opcion");
			else
				if(request.getParameter("opcion") != null)
					strOpc = request.getParameter("opcion").toString();					
			if (strOpc.equals("1")) 
			{
				strAccion = "SELECT  D_CONSEC_HOGAN, D_DATOS_CHEQUE , D_IMPORTE_CHEQUE, D_CORTE, C_NUMERO_CAJERO, C_NUMERO_SUCURSAL FROM TW_CONSEC_VOL_DDA WHERE D_CAJERO_SUCURSAL ='" + strCajero + "' AND D_DOCUM_DIGITALIZ = 'D' AND D_TIPO_DOCUMENTO ='1' AND D_STATUS = 'A'" + statementPS;
				strRespuesta = "DIGITA~2~" + strCajeroTmp + "~" + strMoneda + "~" + rbQuery.EjecutaQuery(strAccion);
			}
			else if (strOpc.equals("2")) 
			{
				strAccion = "SELECT  D_CONSEC_HOGAN, D_DATOS_CHEQUE , D_IMPORTE_CHEQUE, D_CORTE, C_NUMERO_CAJERO, D_CAJERO_SUCURSAL FROM TW_CONSEC_VOL_DDA WHERE C_NUMERO_CAJERO ='" + strCajero.substring(4,6) +"' AND C_NUMERO_SUCURSAL = '" + strCajero.substring(0,4) + "' AND D_DOCUM_DIGITALIZ = 'D' AND D_TIPO_DOCUMENTO ='2'" + statementPS;
				strRespuesta = "DIGITA~3~" + strCajeroTmp + "~" + strMoneda + "~" + rbQuery.EjecutaQuery(strAccion);
			}
			else if (strOpc.equals("3")) 
			{				
				strAccion = "SELECT  D_CONSEC_HOGAN, D_DATOS_CHEQUE , D_IMPORTE_CHEQUE, D_CORTE, C_NUMERO_CAJERO, D_CAJERO_SUCURSAL FROM TW_CONSEC_VOL_DDA WHERE C_NUMERO_CAJERO ='" + strCajero.substring(4,6) +"' AND C_NUMERO_SUCURSAL = '" + strCajero.substring(0,4) + "' AND D_DOCUM_DIGITALIZ = 'D' AND D_TIPO_DOCUMENTO ='3'" + statementPS;
				strRespuesta = "DIGITA~4~" + strCajeroTmp + "~" + strMoneda + "~" + rbQuery.EjecutaQuery(strAccion);
			}
			else if (strOpc.equals("4")) 
			{
				strAccion = "SELECT  D_CONSEC_HOGAN, D_DATOS_CHEQUE , D_IMPORTE_CHEQUE, D_CORTE, C_NUMERO_CAJERO, C_NUMERO_SUCURSAL FROM TW_CONSEC_VOL_DDA WHERE D_CAJERO_SUCURSAL ='" + strCajero + "' AND D_DOCUM_ENTREGADO = 'I' AND D_STATUS = 'A'" + statementPS;
				strRespuesta = "DIGITA~5~" + strCajeroTmp + "~" + strMoneda + "~" + rbQuery.EjecutaQuery(strAccion);
			}
			else if (strOpc.equals("5"))
			{
				strAccion = "SELECT  D_CONSEC_HOGAN, D_DATOS_CHEQUE , D_IMPORTE_CHEQUE, D_CORTE, C_NUMERO_CAJERO, C_NUMERO_SUCURSAL FROM TW_CONSEC_VOL_DDA WHERE D_CAJERO_SUCURSAL ='" + strCajero  + "' AND D_DOCUM_ENTREGADO = 'E' AND D_DOCUM_DIGITALIZ != 'D' AND D_STATUS = 'A'" + statementPS;
				strRespuesta = "DIGITA~6~" + strCajeroTmp + "~" + strMoneda + "~" + rbQuery.EjecutaQuery(strAccion);
			}
			else if (strOpc.equals("6"))
			{
				strAccion = "SELECT  D_CONSEC_HOGAN, D_DATOS_CHEQUE , D_IMPORTE_CHEQUE, D_CORTE, C_NUMERO_CAJERO, C_NUMERO_SUCURSAL FROM TW_CONSEC_VOL_DDA WHERE C_NUMERO_SUCURSAL ='" + strCajero.substring(0,4) + "' AND D_DOCUM_ENTREGADO != 'E' AND D_DOCUM_ENTREGADO != 'I' AND D_STATUS = 'A'" + statementPS;
				strRespuesta = "DIGITA~7~" + strCajeroTmp + "~" + strMoneda + "~" + rbQuery.EjecutaQuery(strAccion);
			}
			sessions.setAttribute("txtCadImpresion", strRespuesta);
		}
		else if (strOpcion.equals("4"))
		{
			String strOpc = new String("");
			String strDato = new String("");
			if(sessions.getAttribute("opcion") != null)
				strOpc = (String) sessions.getAttribute("opcion");
			else
				if(request.getParameter("opcion") != null)
					strOpc = request.getParameter("opcion").toString();
			if(sessions.getAttribute("dato") != null)
				strDato = (String) sessions.getAttribute("dato");
			else
				if(request.getParameter("dato") != null)
					strDato = request.getParameter("dato").toString();
			if (strOpc.equals("1"))
				strOpc = "2" + strDato;
			else
				strOpc = "3" + strDato;
			sessions.setAttribute("TipoDoc",strOpc);
		}
		else if (strOpcion.equals("5")) 
		{
			String strCajeroTmp = new String("000000");            			
			strAccion = "SELECT  D_CONSEC_HOGAN, D_DATOS_CHEQUE , D_IMPORTE_CHEQUE, D_CORTE FROM TW_CONSEC_VOL_DDA WHERE C_NUMERO_CAJERO ='" + strCajero.substring(4,6) + "' AND C_NUMERO_SUCURSAL ='" + strCajero.substring(0,4) + "' AND D_DOCUM_DIGITALIZ = 'D' AND D_STATUS = 'A'" + statementPS;
			strRespuesta = "DIGITA~8~" + strCajeroTmp + "~" + strMoneda + "~" + rbQuery.EjecutaQuery(strAccion);
			//sessions.setAttribute("txtCadImpresion", strRespuesta);			
			strError = "Operacion No disponible";
         sessions.setAttribute("NoError",strError);			
		}		
		if (!strError.equals(" "))
			response.sendRedirect("../ventanilla/paginas/NoOperacion.jsp");
		else 
		{
			if ((strOpcion.equals("2")) || (strOpcion.equals("4")))
				response.sendRedirect("../ventanilla/paginas/DespDig.jsp");
			else
				response.sendRedirect("../ventanilla/paginas/ResponseCHQ.jsp");
		}
	}
    
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		doPost(request, response);
	}
}
