//*************************************************************************************************
//             Funcion: Clase que realiza txn Servicios
//            Elemento: Group07Serv.java
//          Creado por: Alejandro Gonzalez Castro
//      Modificado por: Alejandro Gonzalez Castro
//*************************************************************************************************
// CCN - 4360189 - 03/09/2004 - Se cambia invoke al Diario para Control de Efectivo
//*************************************************************************************************
package ventanilla;
 
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.util.Hashtable;
import java.util.Stack;

import ventanilla.com.bital.sfb.ServiciosB;
import ventanilla.com.bital.admin.Diario;

public class Group07Serv extends HttpServlet 
{
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
    {
        HttpSession session = request.getSession(false);
        Hashtable datasession = (Hashtable)session.getAttribute("page.datasession");
        String txn = (String)datasession.get("cTxn");
		GenericClasses gc = new GenericClasses();
        String moneda = gc.getDivisa((String)datasession.get("moneda"));
        
        long efectivo = Long.parseLong(quitap((String)datasession.get("txtEfectivo")));
        long monto = Long.parseLong(quitap((String)datasession.get("txtMtoSAAC")));
        String ftime = (String)session.getAttribute("ftime");
        if ((efectivo == 0 || efectivo < monto) && ftime.equals("1")) {
            long docs = monto - efectivo;
            datasession.put("txtCheque", addpunto(Long.toString(docs)));
            session.setAttribute("ftime","2");
            Stack flujotxn = (Stack)session.getAttribute("page.flujotxn");
            String[] newflujo = {"S" + txn.substring(1),"5353"};
            for (int i=0; i<newflujo.length; i++)
            { flujotxn.push(newflujo[i]);}
            session.setAttribute("page.flujotxn",flujotxn);
            getServletContext().getRequestDispatcher("/servlet/ventanilla.PageServlet?transaction=" + datasession.get("iTxn") + "&transaction1=" + datasession.get("oTxn")).forward(request,response);
        }
        else {
            if (txn.substring(0,1).equals("S"))
            { txn = "0" + txn.substring(1);}
            
            String dirip =  (String)session.getAttribute("dirip");
            String sMessage = "";
            
            ServiciosB servicio = new ServiciosB();
            servicio.setBranch((String)datasession.get("sucursal"));
            servicio.setTeller((String)datasession.get("teller"));
            servicio.setSupervisor((String)datasession.get("teller"));
            servicio.setFormat("B");
            String Monto = "";
            
            if( txn.equals("0488") ) 
            {
                servicio.setTxnCode(txn);
                servicio.setAcctNo((String)datasession.get("txtRfc"));
                Monto = quitap((String)datasession.get("txtMtoSAAC"));
                servicio.setTranAmt(Monto);
                String exp = (String)datasession.get("txtNumExp");
                String bim = (String)datasession.get("txtBimestre");
                String doc = (String)datasession.get("txtNumDocto");
                String infofovi = "1";
                String opcsum = (String)datasession.get("txtOpcSum");
                if (opcsum.equals("S") || opcsum.equals("s"))
                {opcsum = "1";}
                else
                {opcsum = "0";}
                servicio.setReferenc(exp + bim + doc + infofovi + opcsum);
                Monto ="";
                Monto = quitap((String)datasession.get("txtSumSaac"));
                servicio.setFeeAmoun(Monto);
                Monto ="";
                Monto = quitap((String)datasession.get("txtEfectivo"));
                servicio.setCashIn(Monto);
                servicio.setFromCurr(moneda);
            }

            Diario diario = new Diario();
            sMessage = diario.invokeDiario(servicio, (String)session.getAttribute("d_ConsecLiga"), dirip, moneda, session);
            session.setAttribute("page.txnresponse", sMessage);
            getServletContext().getRequestDispatcher("/servlet/ventanilla.Redirector").forward(request,response);
        }
    }
    
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
    
    private String quitap(String Valor) {
        StringBuffer Cantidad = new StringBuffer(Valor);
        for(int i=0; i<Cantidad.length();) {
            if( Cantidad.charAt(i) == ',' || Cantidad.charAt(i) == '.' )
                Cantidad.deleteCharAt(i);
            else
                ++i;
        }
        return Cantidad.toString();
    }
    
    public String getmoneda(String cmoneda) {
        String newmoneda = "";
        if (cmoneda.equals("N$"))
        { newmoneda = "01";}
        else if (cmoneda.equals("US$"))
        { newmoneda = "02";}
        else
        { newmoneda = "03";}
        return newmoneda;
    }
    
    
    public String removeAsterisk(String campo) {
        StringBuffer result = new StringBuffer(campo);
        String caracter = result.substring(result.length()-1,result.length());
        if (caracter.equals("*"))
            result.deleteCharAt(result.length()-1);
        return result.toString();
    }
    
    private String addpunto(String valor) {
        int longini = valor.length();
        String cents  = valor.substring(longini-2,longini);
        String entero = valor.substring(0,longini-2);
        int longente = entero.length();
        for (int i = 0; i < (longente-(1+i))/3; i++) {
            longente = entero.length();
            entero = entero.substring(0,longente-(4*i+3))+','+entero.substring(longente-(4*i+3));
        }
        entero = entero + '.' + cents;
        return entero;
    }
    
}
