//*************************************************************************************************
//             Funcion: Clase que pone los datos necesarios en sesion para el PageBuilder correspondiente
//          Creado por: Alejandro Gonzalez Castro.
//      Modificado por: Fasuto Rodrigo Flores Moreno
//*************************************************************************************************
// CCN - 4360334 - 17/06/2005 - Se corrige reenvio a error.jsp a ruta relativa para WAS5
// CCN - 4360342 - 08/07/2005 - Se realizan modificaciones para departamentos.
// CCN - 4360347 - 22/07/2005 - Se envia el proceso para las transacciones correctamente
// CCN - 4360364 - 19/08/2005 - Se controlan departamentos por Centro de Costos.
// CCN - 4360425 - 03/02/2006 - Tarjeta TDI
// CCN - 4360576 - 09/03/2007 - Modificaciones para proyecto OPMN
// CCN - 4360593 - 20/04/2007 - Se elimina variable de session idFlujo
//*************************************************************************************************

package ventanilla;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Vector;
	
public class PageServlet extends HttpServlet 
{
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
    {
       		
        HttpSession session = request.getSession(false);
		//session.setAttribute("blagP001","1");
		//session.setAttribute("blagP002","0");
        if( session == null )
            return;
        // PROCESO RAP --- AGC
        if(request.getParameter("noservicios") != null){
            session.setAttribute("page.noservicios",request.getParameter("noservicios").toString());
        }
        
        if(request.getParameter("quickaccess") != null){
                session.removeAttribute("txtCadImpresion");        
                session.removeAttribute("idFlujo");        
        }
        if(request.getParameter("isTarjetaTDI") == null) 
           session.removeAttribute("Tarjeta.TDI");
        String inputTxn = request.getParameter("transaction");
        String outputTxn  = request.getParameter("transaction1");	
        String txnaislada = request.getParameter("txnaislada");
        if(((String)session.getAttribute("isForeign")).equals("S") || session.getAttribute("tipomenu").equals("T"))
        {
        	if(request.getParameter("txnProcess") != null)
			session.setAttribute("txnProcess", request.getParameter("txnProcess"));
        }        
        
        if(txnaislada != null) 
        {
            String timestamp = stringFormat(java.util.Calendar.HOUR_OF_DAY, 2) + stringFormat(java.util.Calendar.MINUTE, 2) + stringFormat(java.util.Calendar.SECOND, 2);
            session.setAttribute("d_ConsecLiga", timestamp);
            session.removeAttribute("page.iTxn");
            java.util.Stack flujotxn = new java.util.Stack();
            session.setAttribute("page.flujotxn", flujotxn);
            String txnProcess  = request.getParameter("txnProcess");
            session.setAttribute("txnProcess", txnProcess);
            session.removeAttribute("page.datasession");
            String moneda  = request.getParameter("moneda");
            session.setAttribute("page.moneda", moneda);
        }
        
        String tempinputTxn = (String)session.getAttribute("page.iTxn");
        int temp =0;
		Hashtable datasession = (Hashtable)session.getAttribute("page.datasession");
        if(inputTxn== null){
        	if(tempinputTxn.equals("P001") || tempinputTxn.equals("P002")){
        	
				
				if(datasession != null)
					{
						datasession.put("txtDDACuenta",(String)session.getAttribute("ctaRepago"));
						datasession.put("rdoTipoPago",(String)request.getParameter("rdoTipoPago"));
						String montoFinal =(String)request.getParameter("hdMtoLetra");
						datasession.put("txtMonto",montoFinal);
						session.setAttribute("page.datasession",datasession);
						session.setAttribute("montoKronner",montoFinal);
		
					}
				tempinputTxn = null;
				inputTxn=(String)session.getAttribute("page.oTxn");
				session.setAttribute("blagP001","0");
				session.setAttribute("blagP002","psP002");
				temp=1;
        
		 }
        }else{
        	if(temp==0){
				session.setAttribute("blagP002","1");
				session.setAttribute("blagP001","1");
        	}
        }
        if(tempinputTxn == null) 
        {
            session.setAttribute("page.iTxn", inputTxn);
            if(outputTxn == null || outputTxn.equals("ND"))  
                outputTxn = inputTxn;

            session.setAttribute("page.oTxn", outputTxn);
            String moneda  = request.getParameter("moneda");
            session.setAttribute("page.moneda", moneda);
			String moneda_s = request.getParameter("moneda_s");
			session.setAttribute("page.moneda_s", moneda_s);
        }
        if( inputTxn == null)
        {
			response.sendRedirect("../ventanilla/paginas/error.jsp");
            return;
        }
        ventanilla.com.bital.util.PageBean pag = new ventanilla.com.bital.util.PageBean();
        
        java.util.Stack flujotxn = (java.util.Stack)session.getAttribute("page.flujotxn");
        if(flujotxn == null || flujotxn.isEmpty()) 
        {
            ventanilla.com.bital.beans.Flujotxn ft = ventanilla.com.bital.beans.Flujotxn.getInstance();
            if(outputTxn == null)
                outputTxn = inputTxn;
            flujotxn = ft.getFlujotxn(inputTxn, outputTxn, (String)session.getAttribute("page.moneda"));                                  
        }
        String currentTxn = new String("");
        
        if(!flujotxn.isEmpty()) {
            currentTxn = (String)flujotxn.pop(); 
            pag.setTransaction( currentTxn );
            session.setAttribute("page.flujotxn", flujotxn);
            session.setAttribute("page.cTxn", currentTxn);
            
            if(outputTxn.equals("4525"))
            	pag.setData((String)session.getAttribute("page.iTxn"),
            	(String)session.getAttribute("page.oTxn"),
            	(String)session.getAttribute("page.moneda_s"));
            else 
				pag.setData((String)session.getAttribute("page.iTxn"),
				(String)session.getAttribute("page.oTxn"),
				(String)session.getAttribute("page.moneda"));
            pag.execute();
            if(pag.gettxnInfo() != null){
                if(pag.getmissingFields() == null){
                    session.setAttribute("page.listcampos", pag.getfieldTxnContent());
                    session.setAttribute("page.listcont", pag.getContentListing());
                    session.setAttribute("page.txnlabel", pag.gettxnInfo().get("D_DESCRIPCION").toString().trim());
                    session.setAttribute("page.txnAuthLevel", pag.gettxnInfo().get("D_NVL_AUTORIZ").toString().trim());
                    session.setAttribute("page.txnImage", pag.gettxnInfo().get("D_TITULO_IMAGEN").toString().trim());
                    session.setAttribute("page.txnIMGAutoriz", pag.gettxnInfo().get("D_RQ_AUTO_FIRMA").toString().trim());
                    session.setAttribute("page.getiTxnView", pag.gettxnInfo().get("D_VISTA_ENTRADA").toString().trim());
                    session.setAttribute("page.getoTxnView", pag.gettxnInfo().get("D_VISTA_SALIDA").toString().trim());
                    session.setAttribute("page.SignField", pag.gettxnInfo().get("D_CMPO_RQ_FIRMA").toString().trim());
                    session.setAttribute("page.certifField", pag.gettxnInfo().get("D_REQ_CERTIFICA").toString().trim());
					session.setAttribute("page.specialacct", pag.getSpecialAccounts());


                    if (currentTxn.equals("0101")) {
	                    session.setAttribute("page.lstModeloCh", pag.getLstModelo());
	                    session.setAttribute("page.lstColorCh", pag.getLstColor());
	                    session.setAttribute("page.lstNoChqs", pag.getLstNroCh());
	                    request.setAttribute("lstTipoCheques", getListTipoCheques(pag.getLstTipoChequera()));
                    }

                }
                else{
                    session.setAttribute("page.Builder.missingField.Error", pag.getmissingFields());
					response.sendRedirect("../ventanilla/paginas/error.jsp");
                    return;
                }
            }
            else{
            	if(session.getAttribute("page.cTxn").toString().equals("4101")){
            	   response.sendRedirect("../ventanilla/paginas/Final.jsp"); 
            	}else{            	
                session.setAttribute("page.Builder.Error", "No esta definida la transacci&oacute;n <b>" +
                currentTxn + "</b> en la aplicaci&oacute;n.");
				response.sendRedirect("../ventanilla/paginas/error.jsp");
                return;
            	}
            }
        }
        else{
            response.sendRedirect("../ventanilla/paginas/error.jsp");
            return;
        }
        String PageBuilder = new String("PageBuilder");
        if(!pag.gettxnInfo().get("D_VISTA_ENTRADA").toString().equals(""))
            PageBuilder = pag.gettxnInfo().get("D_VISTA_ENTRADA").toString();
       
		getServletContext().getRequestDispatcher("/ventanilla/paginas/" + PageBuilder + ".jsp").forward(request, response);	
			
    }
    private String stringFormat(int option, int NumDig) {
        java.util.Calendar now = java.util.Calendar.getInstance();
        String temp = new Integer(now.get(option)).toString();
        for(int i = temp.length(); i < NumDig; i++)
            temp = "0" + temp;
        
        return temp;
    }
    
    private ArrayList getListTipoCheques(Vector lstTipoChequera){
    	
    	Vector v1 = null;
    	Vector v2 = null;
    	ArrayList<String> tipoCheques = new ArrayList<String>();
    	
    	try{
    	 
       for(int i=0; i < lstTipoChequera.size(); i++)
 	   	{
 		
		     v1 = (Vector)lstTipoChequera.get(i);
		     
 		    tipoCheques.add((String)v1.get(2) + "*" +(String)v1.get(3));
 			
 		}
    		
    	}catch( Exception e){
    		System.out.println(e.getMessage());
    	}
    	
    	return tipoCheques;
    	
    }
    
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}
