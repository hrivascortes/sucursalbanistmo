//*************************************************************************************************
//             Funcion: Clase que realiza txn CBA
//            Elemento: Group01CBA.java
//          Creado por: Alejandro Gonzalez Castro
//      Modificado por: Alejandro Gonzalez Castro
//*************************************************************************************************
// CCN - 4360189 - 03/09/2004 - Se cambia invoke al Diario para Control de Efectivo
//*************************************************************************************************
package ventanilla;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.util.Hashtable;
import ventanilla.com.bital.sfb.CBA;
import ventanilla.com.bital.admin.Diario;

public class Group01CBA extends HttpServlet
{
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession(false);
        Hashtable datasession = (Hashtable)session.getAttribute("page.datasession");
        String txn = (String)datasession.get("cTxn");
		GenericClasses gc = new GenericClasses();
        String moneda = gc.getDivisa((String)datasession.get("moneda"));
        String[] invoqued = request.getParameterValues("txtInvoqued");
        String invTxn = "";
        if(invoqued != null)
            invTxn = (String)invoqued[0];
        //Variables para diario electronico
        String dirip =  (String)session.getAttribute("dirip");
        String msg = "";
        String txtDescrip="";
        //Variables para diario electronico

        CBA cba = new CBA();
        cba.setBranch((String)datasession.get("sucursal"));
        cba.setTeller((String)datasession.get("teller"));
        cba.setSupervisor((String)datasession.get("teller"));
        cba.setTranCur(moneda);
        cba.setTxnCode(txn);
        cba.setMoamoun("000");
        cba.setTranAmt(quitap((String)datasession.get("txtMonto")));

        if ( (String)datasession.get("supervisor") != null)
           cba.setSupervisor((String)datasession.get("supervisor"));

        if ( (String)datasession.get("override") != null )
                if (datasession.get("override").toString().equals("SI"))
                    cba.setOverride("3");

        if (txn.equals("5505") || txn.equals("5509")) {
            cba.setAcctNo((String)datasession.get("txtCuentaCBA_C"));
            txtDescrip="TRANSFER TO " + (String)datasession.get("txtCuentaCBA_A");
        }

        if (txn.equals("5507") || txn.equals("5511")) {
            cba.setAcctNo((String)datasession.get("txtCuentaCBA_A"));
            txtDescrip="TRANSFER FROM " + (String)datasession.get("txtCuentaCBA_C");
        }
        cba.setTranDesc(txtDescrip);

        // Descripcion para Reverso
        String DescRev = "REV. " + txtDescrip;
        if(DescRev.length() > 40)
            DescRev = DescRev.substring(0,40);
        cba.setDescRev(DescRev);

        Diario diario = new Diario();
        String sMessage = diario.invokeDiario(cba, (String)session.getAttribute("d_ConsecLiga"), dirip, moneda, session);
        session.setAttribute("page.txnresponse", sMessage);

        getServletContext().getRequestDispatcher("/servlet/ventanilla.Redirector").forward(request,response);
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }

    private String quitap(String Valor) {
        StringBuffer Cantidad = new StringBuffer(Valor);
        for(int i=0; i<Cantidad.length();) {
            if( Cantidad.charAt(i) == ',' || Cantidad.charAt(i) == '.' )
                Cantidad.deleteCharAt(i);
            else
                ++i;
        }
        return Cantidad.toString();
    }
    public String getmoneda(String cmoneda) {
        String newmoneda = "";
        if (cmoneda.equals("N$"))
        { newmoneda = "01";}
        else if (cmoneda.equals("US$"))
        { newmoneda = "02";}
        else
        { newmoneda = "03";}
        return newmoneda;
    }

    public String removeAsterisk(String campo) {
        StringBuffer result = new StringBuffer(campo);
        String caracter = result.substring(result.length()-1,result.length());
        if (caracter.equals("*"))
            result.deleteCharAt(result.length()-1);
        return result.toString();
    }

}