//*************************************************************************************************
//             Funcion: Clase que realiza txn Inversiones
//            Elemento: Group06I.java
//          Creado por: Alejandro Gonzalez Castro
//      Modificado por: Alejandro Gonzalez Castro
//*************************************************************************************************
// CCN - 4360189 - 03/09/2004 - Se cambia invoke al Diario para Control de Efectivo
//*************************************************************************************************
package ventanilla;

import java.io.IOException;
import java.util.Hashtable;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import ventanilla.com.bital.sfb.Inversiones;
import ventanilla.com.bital.admin.Diario;

public class Group06I extends HttpServlet {

    private String delCommaPointFromString(String newCadNum) {
        String nCad = new String(newCadNum);
        if( nCad.indexOf(".") > -1) {
            nCad = nCad.substring(nCad.indexOf(".") + 1, nCad.length());
            if(nCad.length() != 2) {
                String szTemp = new String("");
                for(int j = nCad.length(); j < 2; j++)
                    szTemp = szTemp + "0";
                newCadNum = newCadNum + szTemp;
            }
        }

        StringBuffer nCadNum = new StringBuffer(newCadNum);
        for(int i = 0; i < nCadNum.length(); i++)
            if(nCadNum.charAt(i) == ',' || nCadNum.charAt(i) == '.')
                nCadNum.deleteCharAt(i);

        return nCadNum.toString();
    }

    public void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Inversiones oInversiones = new Inversiones();

        resp.setContentType("text/plain");
        HttpSession session = req.getSession(false);
        Hashtable datasession = (Hashtable)session.getAttribute("page.datasession");

        String dirip = (String)session.getAttribute("dirip");
        String txtTxnCode    = (String)datasession.get("cTxn");
        String txtFormat     = "A";
        String txtBranch     = (String)datasession.get("sucursal");
        String txtTeller     = (String)datasession.get("teller");//"000108";
        String txtSupervisor = (String)datasession.get("teller");//"000101";
        String txtBackOut    = "0";
        String txtOverride   = "0";
        String txtCurrentDay = "0";
        String txtFechEfect  = "";

        if ((String)datasession.get("supervisor")!= null)
            txtSupervisor = (String)datasession.get("supervisor");

        if ( (String)datasession.get("override") != null )
            if (datasession.get("override").toString().equals("SI"))
                txtOverride = "3";


        oInversiones.setTxnCode(txtTxnCode);
        oInversiones.setFormat(txtFormat);
        oInversiones.setBranch(txtBranch);
        oInversiones.setTeller(txtTeller);
        oInversiones.setSupervisor(txtSupervisor);
        oInversiones.setBackOut(txtBackOut);
        oInversiones.setOverride(txtOverride);
        oInversiones.setCurrentDay(txtCurrentDay);
        String txtMoneda     = (String)datasession.get("moneda");                                //Moneda
        String txtCDACuenta  = (String)datasession.get("txtCDACuenta");                          //No. de cuenta
        String txtMonto      = delCommaPointFromString((String)datasession.get("txtMonto"));     //Monto de la Txn.
        String txtBeneficiario  = (String)datasession.get("txtBeneficiario");                    //Nombre del Beneficiario
        String txtSerial  = (String)datasession.get("txtSerial");                                //No. de cheque
        String txtEfectivo   = "000";
        String txtDDACuenta = "";

        if(txtMoneda.equals("01"))
            txtDDACuenta = "0103000023";
        else
            txtDDACuenta = "3902222222";

        oInversiones.setTranCur(txtMoneda);     //Tipo de Moneda con que se opera la Txn.
        oInversiones.setAcctNo1(txtCDACuenta);   //No de Cuenta
        oInversiones.setTranAmt(txtMonto);      //Monto Total de la Txn.
        oInversiones.setCheckNo(txtSerial);     //Serial de la txn
        oInversiones.setTranDesc(txtBeneficiario);      //Nombre del Beneficiario
        oInversiones.setAcctNo2(txtDDACuenta);      //Monto Total de la Txn.

        Diario diario = new Diario();
        String sMessage = diario.invokeDiario(oInversiones, (String)session.getAttribute("d_ConsecLiga"), dirip, txtMoneda, session);

        session.setAttribute("page.txnresponse", oInversiones.getMessage());
        getServletContext().getRequestDispatcher("/servlet/ventanilla.Redirector").forward(req,resp);

    }

    public void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doPost(req, resp);
    }
}
