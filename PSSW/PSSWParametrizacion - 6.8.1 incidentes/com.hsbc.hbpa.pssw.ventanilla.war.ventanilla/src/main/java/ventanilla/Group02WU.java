//*************************************************************************************************
//             Funcion: Clase que realiza txn Western Union
//            Elemento: Group02WU.java
//          Creado por: Alejandro Gonzalez Castro
//      Modificado por: Alejandro Gonzalez Castro
//*************************************************************************************************
// CCN - 4360189 - 03/09/2004 - Se cambia invoke al Diario para Control de Efectivo
// CCN - 4360371 - 02/09/2005 - Se incrementa tiempo de espera de comunicacion con WU
// CCN - 4360374 - 08/09/2005 - Se genera nuevo paquete a peticion de QA.
// CCN - 4360378 - 19/09/2005 - Se agrega log para WesterUnion
// CCN - 4360383 - 26/09/2005 - Se incrementa el numero de intentos con WU
// CCN - 4360384 - 28/09/2005 - Se eliminan displays
// CCN - 4360392 - 07/10/2005 - Se modifica para visualizar mensajes de WU
//*************************************************************************************************
package ventanilla; 

import java.io.IOException;
import java.util.Hashtable;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import ventanilla.com.bital.admin.Diario;
import ventanilla.com.bital.sfb.Deposito;
import ventanilla.com.bital.sfb.FormatoE;
import ventanilla.com.bital.sfb.WUCD;
import ventanilla.com.bital.sfb.WUCanBus;
import ventanilla.com.bital.sfb.WUPago;
import ventanilla.com.bital.sfb.WUVSAM;

public class Group02WU extends HttpServlet 
{
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
    {
        response.setContentType("text/plain");
        HttpSession session = request.getSession(false);
        Hashtable datasession = (Hashtable)session.getAttribute("page.datasession");
        String sMessage = "";
        String action = "";
        String code = "";
		GenericClasses Genericas = new GenericClasses();
        
        if(request.getParameter("WUaction") != null)
            action = (String)request.getParameter("WUaction");
        
        Hashtable data = new Hashtable();
        data = (Hashtable)session.getAttribute("WUdata");

        if(action.equals("pagar"))
        {
            WUPago pago = new WUPago();
            pago.setBranch((String)datasession.get("sucursal"));
            pago.setRegistro((String)datasession.get("registro"));
            pago.setTeller((String)datasession.get("teller"));
            pago.setNoSecuencia("02");
            pago.setNoTerminal((String)data.get("noterm"));
            pago.setNoSecAnt((String)data.get("nosec"));
            pago.setCveDespierta((String)data.get("cvedesp"));
            pago.setCuenta((String)data.get("cta"));
            pago.setLU((String)data.get("lu"));
            pago.setTipoApp((String)data.get("app"));
            pago.setConsecutivo((String)data.get("consec"));
            pago.setResultado("00");
            String nosec = (String)data.get("nosec");
            if(nosec.equals("01"))
                pago.setPaginacion("1");
            else
                pago.setPaginacion("2");
            pago.setDBkey((String)data.get("dbkey"));
            String monto = (String)data.get("mto");
            monto = monto.substring(0, monto.length()-2);
            pago.setMonto(ClearAmount(monto));
            pago.setAcct("4020442505");
            pago.execute();
            sMessage = pago.getMessage();
        }
        else if(action.equals("canorden"))
        {
            WUCD cano = new WUCD();
            cano.setBranch((String)datasession.get("sucursal"));
            cano.setRegistro((String)datasession.get("registro"));
            cano.setTeller((String)datasession.get("teller"));
            cano.setNoSecuencia("03");
            cano.setNoTerminal((String)data.get("noterm"));
            cano.setNoSecAnt((String)data.get("nosec"));
            cano.setCveDespierta((String)data.get("cvedesp"));
            cano.setCuenta((String)data.get("cta"));
            cano.setLU((String)data.get("lu"));
            cano.setTipoApp((String)data.get("app"));
            cano.setConsecutivo((String)data.get("consec"));
            cano.setResultado("00");
            cano.setPaginacion("2");
            cano.setMTCN((String)data.get("dbkey"));
            cano.execute();
            sMessage = cano.getMessage();
            session.setAttribute("txtCadImpresion", "");
        }
        else
        {
            WUCanBus canb =  new WUCanBus();
            canb.setBranch((String)datasession.get("sucursal"));
            canb.setRegistro((String)datasession.get("registro"));
            canb.setTeller((String)datasession.get("teller"));
            canb.setNoSecuencia("06");
            canb.setNoTerminal((String)data.get("noterm"));
            canb.setNoSecAnt((String)data.get("nosec"));
            canb.setCveDespierta((String)data.get("cvedesp"));
            canb.setCuenta((String)data.get("cta"));
            canb.setLU((String)data.get("lu"));
            canb.setTipoApp((String)data.get("app"));
            canb.setConsecutivo((String)data.get("consec"));
            canb.setResultado("00");
            canb.setPaginacion("2");
            canb.execute();
            sMessage = canb.getMessage();
            session.setAttribute("txtCadImpresion", "");
        }
        
        code = sMessage.substring(8,10);
        if(code.equals("00"))
        {
            WUVSAM vsam = new WUVSAM();
            vsam.setOPSucursal((String)datasession.get("sucursal"));
            vsam.setTxnId("VSAM");
            vsam.setFuncion("R");
            vsam.setFile("PASWESTE");
            vsam.setLenKey("7");
            String key = sMessage.substring(70,sMessage.length()-1);
            key = key + "2";
            vsam.setKey(key);
            vsam.setLenReg("2300");
            for(int i=0; i < 19; i++)
            {
                vsam.execute();
                sMessage = vsam.getMessage();
                code = sMessage.substring(0,2);
                // CODIGO 13 - NO SE ENCONTRO LA LLAVE => REINTENTAR TXN
                if(!code.equals("13"))
                    break;
                try{Thread.sleep(15000);}
                catch (InterruptedException e){}
            }
        }
        
        if(code.equals("00"))
        {
            String res = sMessage.substring(199,201);
            if(res.equals("03") && action.equals("pagar"))
            {
                String dirip = (String)session.getAttribute("dirip");
                String txtSupervisor = null;
                String txtMoneda = null;
                
                txtMoneda = (String)datasession.get("moneda");
				txtMoneda = Genericas.getDivisa(txtMoneda);
                
                if ( (String)datasession.get("supervisor") != null )
                    txtSupervisor = (String)datasession.get("supervisor");

                FormatoE ftoe = new FormatoE();
                ftoe.setTxnCode("4453");
                ftoe.setFormat("A");
                ftoe.setBranch((String)datasession.get("sucursal"));
                ftoe.setTeller((String)datasession.get("teller"));
                ftoe.setSupervisor(txtSupervisor);
                ftoe.setAcctNo("400");
                ftoe.setTranAmt(ClearAmount((String)data.get("mto")));
                if(((String)request.getParameter("txtCuenta")).length() > 0)
                    ftoe.setReferenc1((String)data.get("mtcn")+"-"+(String)request.getParameter("txtCuenta"));
                else
                    ftoe.setReferenc1((String)data.get("mtcn")+"-4020442505");
                ftoe.setCashOut(ClearAmount((String)data.get("mto")));
                ftoe.setFromCurr(txtMoneda);
                Diario diario = new Diario();
                sMessage = diario.invokeDiario(ftoe, (String)session.getAttribute("d_ConsecLiga"), dirip, txtMoneda, session);
                String txtCasoEsp = "4453~"+Genericas.getString(sMessage, 1)+"~";
                String txtCadImpresion ="";
                if(sMessage.startsWith("0"))
                {
                    txtCadImpresion = "~WU1~" + ((String)data.get("ben")).trim() + "~" + ClearAmount((String)data.get("mto")) + "~~";
                    session.setAttribute("txtCadImpresion", txtCadImpresion);
                }
                if(((String)request.getParameter("txtCuenta")).length() > 0 && sMessage.startsWith("0"))
                {
                    try{Thread.sleep( 1500 );}
                    catch (InterruptedException e){}
                    Deposito dep = new Deposito();
                    dep.setTxnCode("1003");
                    dep.setFormat("A");
                    dep.setBranch((String)datasession.get("sucursal"));
                    dep.setTeller((String)datasession.get("teller"));
                    dep.setSupervisor(txtSupervisor);
                    dep.setAcctNo((String)request.getParameter("txtCuenta"));
                    dep.setTranAmt(ClearAmount((String)data.get("mto")));
                    dep.setTranCur(txtMoneda);
                    dep.setCashIn(ClearAmount((String)data.get("mto")));
                    dep.setTranDesc("DEPOSITO DE DINERO EN MINUTOS" +(String)data.get("mtcn"));
                    
                    // Descripcion para Reverso
                    String DescRev = "REV. DEP. DINERO EN MINUTOS" + (String)data.get("mtcn");
                    if(DescRev.length() > 40)
                        DescRev = DescRev.substring(0,40);
                    dep.setDescRev(DescRev);
                    diario = new Diario();
                    sMessage = diario.invokeDiario(dep, (String)session.getAttribute("d_ConsecLiga"), dirip, txtMoneda, session);
                    txtCasoEsp = txtCasoEsp + "1003~"+Genericas.getString(sMessage, 1)+"~";
                    txtCadImpresion = txtCadImpresion.substring(0,txtCadImpresion.length()-1);
                    txtCadImpresion = txtCadImpresion + (String)request.getParameter("txtCuenta")+ "~";
                    if(sMessage.startsWith("0"))
                        session.setAttribute("txtCadImpresion", txtCadImpresion);
                }
                session.setAttribute("txtCasoEsp", txtCasoEsp);
            }
            else
            {
                if(action.equals("canorden") || action.equals("pagar"))
                    sMessage = "0~01~0000000~"+sMessage.substring(206,sMessage.length()).trim()+"~";
                else
                    sMessage = "0~01~0000000~CANCELACION DE BUSQUEDA EXITOSA~";
                session.setAttribute("txtCadImpresion", "");
            }
        }
        else
            sMessage = "1~01~0000000~"+sMessage.substring(2,42).trim()+"~";

        session.setAttribute("page.getoTxnView", "Response");
        session.setAttribute("page.txnresponse", sMessage);
        getServletContext().getRequestDispatcher("/servlet/ventanilla.Redirector").forward(request,response);
    }
    
    public String ClearAmount(String mto)
    {
        StringBuffer monto = new StringBuffer(mto);
        for(int i=0; i<monto.length();) 
        {
            if( monto.charAt(i) == ',' || monto.charAt(i) == '.' )
                monto.deleteCharAt(i);
            else
                ++i;
        }
        mto = monto.toString();
        return mto;
    }
    
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
    {
        doPost(request, response);
    }
}
