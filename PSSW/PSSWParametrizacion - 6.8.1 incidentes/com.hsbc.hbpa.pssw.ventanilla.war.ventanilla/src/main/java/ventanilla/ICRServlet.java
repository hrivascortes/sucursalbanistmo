//*************************************************************************************************
//             Funcion: Clase para obtener el ICR
//            Elemento: ICRServlet.java
//          Creado por: Humberto Enrique Balleza Mej�a
//*************************************************************************************************
// CCN -  - 03/09/2004 - Se crea el elemento
//*************************************************************************************************
package ventanilla;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import ventanilla.com.bital.sfb.FormatoB;

public class ICRServlet extends HttpServlet 
{
	private String resp = null;
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
    {
        response.setContentType("text/plain");
        HttpSession session = request.getSession(false);

		FormatoB formatob = new FormatoB();
		GenericClasses gc = new GenericClasses();
		formatob.setBranch(request.getParameter("branch"));
		formatob.setTeller(request.getParameter("teller"));
		formatob.setTxnCode("0830");
		formatob.setFormat("B");
		formatob.setAcctNo(gc.rellenaZeros(request.getParameter("txtCuentaICR"),15));
		formatob.setTranAmt("1");
		formatob.setReferenc(request.getParameter("lstTipoCuenta"));
		formatob.setFeeAmoun("0");
		formatob.setIntern("0");
		formatob.setIntWh("0");
		formatob.setCashIn("000");
		String moneda = gc.getDivisa(request.getParameter("moneda"));
		formatob.setFromCurr(moneda);
		formatob.execute();
		String respuesta = formatob.getMessage();
		String sICR = extractResponce(respuesta,(String)request.getParameter("lstTipoCuenta"));
		session.setAttribute("ctaICR", sICR);
		session.setAttribute("flagICR", resp);
        session.setAttribute("updateICR", "1"); 
        session.setAttribute("PRESSTC","S");
        getServletContext().getRequestDispatcher("/ventanilla/paginas/seccioninformativa.jsp").forward(request,response);
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
    
	public String extractResponce(String returnStream, String tipoCta){
		int consecutiveIndex = returnStream.indexOf("CONSECUTIVO");
		if(returnStream.substring(0,1).equals("1"))
		{
			returnStream="No existe la cuenta";
			resp="1";
		}else if(returnStream.substring(0,1).equals("0"))
		{
			resp="0";
			String Cuenta = returnStream.substring(consecutiveIndex + 86, consecutiveIndex + 105);
			if(tipoCta.equals("BCC") || tipoCta.equals("HCC"))
				returnStream=Cuenta.substring(9,Cuenta.length());
			else
				returnStream=Cuenta;	
		}
		return returnStream;
	}
	
	public String ICRforFirma(String respuesta,String tipo_cta)
	{
		String sICR = extractResponce(respuesta,tipo_cta);
   		return sICR;
	}
}
