//************************************************************************************************
//			   Funcion: Clase que realiza la interconexion
//						con Pagos Voluntarios
//			  Elemento: GroupPV.java
//			Creado por: Carolina Vela Seraf�n
//		
//*************************************************************************************************
// CCN -     -       -  Pago Voluntario
// CR -  CR11593 - 14/02/2011 - Correcci�n en impresi�n de pago voluntario
//*************************************************************************************************
package ventanilla;

import java.io.IOException;
import java.util.Hashtable;
import java.util.Vector;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import ventanilla.com.bital.admin.Diario;
import ventanilla.com.bital.sfb.FormatoB;

import com.bital.cics.CicsCtgBean;
import com.bital.cics.CicsCtgImpl;
import com.bital.util.Comunica;


public class GroupPV extends HttpServlet {
	 
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		HttpSession session = request.getSession(false);
		Hashtable datasession = (Hashtable)session.getAttribute("page.datasession");
		Hashtable userinfo = (Hashtable)session.getAttribute("userinfo");
		GenericClasses gc = new GenericClasses();
		FormatoB formB = new FormatoB();

		String idFlujo = (String)session.getAttribute("idFlujo");
		String dirip =  (String)session.getAttribute("dirip");
		
		//		variables del ciclo
			  int intAcep=0;
			  int indice=0;
			  int blag=0;
			  String sMessage="0~CUENTA:4000000000";
			  String ctaRepago="";
			  Diario diario = new Diario();
			  int blagCronner=0;
		
		
		//Variables para trace
		String txtTxn 		= (String)datasession.get("cTxn");
		String txtCode    	= ("0830");
	    String txtFormat    = "B";
		String txtCredito  	= (String)datasession.get("txtCredito");
		String txtTranAmt	= "1";
		String lstCredito  	= (String)datasession.get("lstCredito");
		String txtFeeAmount	= "0";
		String txtIntern	= "0";
		String txtInWh		= "0";
		String txtCashIn	= "000";
		String moneda 		= gc.getDivisa((String)datasession.get("moneda"));
	    String txtBranch    = (String)datasession.get("sucursal");
	    String txtTeller    = (String)datasession.get("teller");
	    String txtSupervisor= (String)datasession.get("teller");
	    String txtBackOut   = "0";
	    String txtOverride  = "0";
	    String txtCurrentDay= "0";
	    
	    //Settear valores al bean del Formato
		formB.setTxnCode(txtCode);
		formB.setFormat(txtFormat);
		formB.setAcctNo(txtCredito);
		formB.setTranAmt(txtTranAmt);
		formB.setReferenc(lstCredito);
		formB.setFeeAmoun(txtFeeAmount);
		formB.setIntern(txtIntern);
		formB.setIntWh(txtInWh);
		formB.setCashIn(txtCashIn);
		formB.setFromCurr(moneda);
		formB.setBranch(txtBranch);
		formB.setTeller(txtTeller);
		formB.setSupervisor(txtSupervisor);
		formB.setBackOut(txtBackOut);
		formB.setOverride(txtOverride);
		formB.setCurrentDay(txtCurrentDay);
		String newCta = "";
		
   
		if(lstCredito.equals("BCC")|| (lstCredito.equals("HCC")))
			{
				//Quitar 
				sMessage = diario.invokeDiario(formB,(String)session.getAttribute("d_ConsecLiga"), dirip, moneda, session);
				intAcep=Integer.parseInt(sMessage.substring(0,sMessage.indexOf("~")));
				if(intAcep==0){
					blagCronner=1;
					indice= sMessage.indexOf("CUENTA:");
					newCta=sMessage.substring(indice+7,indice+27).trim();
					
					if (newCta != null && newCta.length() >= 10)
						{
							newCta = newCta.substring(newCta.length()-10,newCta.length());
						}
	    	    }
				else	
				{
							session.setAttribute("page.txnresponse", sMessage);
							getServletContext().getRequestDispatcher("/servlet/ventanilla.Redirector").forward(request,response);
				}   
			}
											
	
		if(intAcep==0){    
			
			ventanilla.com.bital.beans.DatosVarios datosv = ventanilla.com.bital.beans.DatosVarios.getInstance();
			Vector selectu = (Vector)datosv.getDatosV("USERKRONER");
			String UserId = selectu.get(0).toString(); 
			
			Vector selectc = (Vector)datosv.getDatosV("CICSSERVER");
			String CicsServer = selectc.get(0).toString(); 
			Vector selectg = (Vector)datosv.getDatosV("GATEWAYURL");
			String GatewayURL = selectg.get(0).toString(); 
			Comunica c = new Comunica();
			CicsCtgBean cicsCtgObj = new CicsCtgBean();		
			cicsCtgObj.setCicsProgramName("KPHO075"); //"rpc"	
			cicsCtgObj.setCommAreaLength(4096); //"long"
			cicsCtgObj.setUserId(UserId); //"user"
			//cicsCtgObj.setUserId("DPSPUSR2");		
			//System.out.println("Usuario para Kroner: "+UserId);	
			//cicsCtgObj.setPassword("PUMAS17"); //"password"
			cicsCtgObj.setCicsServer(CicsServer); //"cics"
			//cicsCtgObj.setCicsServer("CIPADTW2");
			//System.out.println("CICS de conexi�n: "+CicsServer);
			cicsCtgObj.setGatewayURL("local:"); //"url"
			//cicsCtgObj.setGatewayURL("web53.mx.cmtH"); //"url" LOCAL
			//System.out.println("Gateway de conecci�n "+GatewayURL);
			cicsCtgObj.setGatewayPort(0);
			//cicsCtgObj.setGatewayPort(2006); //"puerto" --LOCAL
			cicsCtgObj.setGatewayProtocol("local"); //"protocolo"		
			//cicsCtgObj.setGatewayProtocol("tcp"); //"protocolo"
			cicsCtgObj.setEncoding("IBM037"); //"encoding"
			cicsCtgObj.setTransId("KPHO"); //"trans"
			

	
			//SE ARMA CADENA PARA KRONER
			String user = (String)userinfo.get("D_EJCT_CTA");
			String asignado = "EDOCC";
			String fchConsulta= gc.getDate(1);
			int cveMov=1;
			if(blagCronner == 1)
			{
				txtCredito=newCta;
				datasession.put("txtCredito",txtCredito);
			}
			
			//String cadFInalCronner= asignado+user+ctaRepago+fchConsulta+cveMov;
			if(UserId.length()==6)
				UserId=UserId+"__";
			String cadFInalCronner= asignado+UserId+"00C"+txtCredito.substring(2,10)+fchConsulta+cveMov;
			//String cadFInalCronner= asignado+"ZASZAS__"+"00C"+gc.rellenaZeros(txtCredito,8)+fchConsulta+cveMov;
			                          //EDOCCIDDRDE__00C02330230200804091
									  //EDOCCIDDRDE__00C01838366200709281
			//String cadFInalCronner="EDOCCUPSPKUSE01C6886530200804081";

			//System.out.println(cadFInalCronner);
			System.out.println("MSG PAgo POST KRONER: "+cadFInalCronner);
			
			cicsCtgObj.setCommAreaInput(cadFInalCronner);
			CicsCtgImpl cicsCtg = new CicsCtgImpl();
				
 			String res=c.executale(cicsCtgObj);	
			System.out.println("MSG Pago RESPONSE KRONER: "+res);						
			//String res = "001000001660071130000099000000000000001JUAN ROBERTO *TORRES ESPINOSA           0001500000000000000020071102024cmtH BANK (PANAMA) S20110202102008020402USDDOLAR AMERICANO  1305030000200712030001452125487500000000                 1000114160000000000000000000150000000000000000000000150000000000000000000000000000000000000000000000000000000000000000000125000000000000000000000008319450000000000000000000000000000000000000000000000000000133319450000000000000000000000000000000000000000000000000001508319450000000000000000000000000000TRADICIONAL         00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000010000000000000                    00000000000000000000000000000000VIGENTE   SUCURSAL DEFAULT                        * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *                                                             * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 00                                                                                                    00000000000410000000000000000000000000000000000________________________________________________________________________________________________________________________0000000           1201102020000000000000000000000000000000000000000000000000001400000000000000000000000000000000000000013331945000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000012500000000000000000000000000000000000000000000000000000000000000000000000000000000000000000200711150000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000";
			//String 
			//res = "000233023020080409000011000000000000095JOE_*MCENROE____________________________0000025000002005121520051123024cmtH_BANK_(PANAMA)_S20110131102008041502DOLAR_AMERICANO_____1009010201200804150057400000046320060613_________________1000601000000000000000000000001442666000000000000000001442666000000000000000000000000000000000000000000000000009794990000000206450000001212760000000010820000000000000000000000000000000603070000000000000000000233880000012652070000000003000000000000000000000000000000027106160000012652070000000000000700PAGOS_NIVELADOS_____00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000010000000000000____________________00000000000000000000000000000000VIGENTE___PUNTA_PAITILLA__________________________PRESTAMOS_PERSONALES_(PCON)_____________________________________________________________________________________________*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_01____________________________________________________________________________________________________00000000000410000000000000000000000000000000000________________________________________________________________________________________________________________________0000501___________72011013100000000000000000000000000000000017160000000000000006000000000000000000000000000000000000000000217270000011007750000000013000000000637000000000000000000000000000000000000000000000000000000000000000000000224480000000003610000000404250000000000000000000000000000000000000000000000000000000000002045010100000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000PRESTAMOS_PERSONALES__________000000000000_____________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________";
			//System.out.println("MSG OUT KRONER: "+res);
			
			//validaciones kronner
			
			String flagNoPago = null;
			
			flagNoPago=res.substring(227,228);
			
			if(res != null)
			{
			if(res.length()>=2 && !res.substring(0,2).equals("00"))
			{
				session.setAttribute("page.txnresponse", res);
				if(res.substring(0,6).equals("020174")){
					
					session.setAttribute("respKronner",res);			
				}
				Vector resKroner = new Vector();
				resKroner.add("1");
				resKroner.add("01");
				resKroner.add(res.substring(0,5));
				resKroner.add(res);
				session.setAttribute("response",resKroner);
				response.sendRedirect("../ventanilla/paginas/Response.jsp");
				//getServletContext().getRequestDispatcher("/servlet/ventanilla.Redirector").forward(request,response);
			}}
			if(res == null)
			{
				Vector resKroner = new Vector();
				resKroner.add("1");
				resKroner.add("01");
				resKroner.add("000000");
				resKroner.add("ERROR DE COMUNICACI�N CON EL HOST DE KRONER");
				session.setAttribute("response",resKroner);
				response.sendRedirect("../ventanilla/paginas/Response.jsp");
			}
			if(res != null && res.length()>=2 && res.substring(0,2).equals("00") && flagNoPago.equals("0")) 
			{
				Vector resKroner = new Vector();
				resKroner.add("1");
				resKroner.add("01");
				resKroner.add("000000");
				resKroner.add("CR�DITO BLOQUEADO PARA PAGOS, NO SE PUEDE OPERAR LA TRANSACCI�N");
				session.setAttribute("response",resKroner);
				response.sendRedirect("../ventanilla/paginas/Response.jsp");
			}

				if(txtTxn.equals("P001")){
						//indice= sMessage.indexOf("CUENTA:");
						session.setAttribute("page.oTxn","1003");	
						session.setAttribute("page.cTxn","1003");
						ctaRepago=res.substring(192,202);
						//ctaRepago=sMessage.substring(indice+7,indice+27).trim(); 
						//ctaRepago=ctaRepago.substring(ctaRepago.length()-10,ctaRepago.length());
				}
				if(txtTxn.equals("P002")){
						//indice= sMessage.indexOf("CUENTA:");
						ctaRepago=res.substring(192,202);
						//ctaRepago=sMessage.substring(indice+7,indice+27).trim(); 
						//ctaRepago=ctaRepago.substring(ctaRepago.length()-10,ctaRepago.length());
				}
				
			

			session.setAttribute("respuestaKronner", res);  
			session.setAttribute("page.txnresponse", sMessage);
			session.setAttribute("ctaRepago",ctaRepago);
			//datasession.put("txtDDACuenta",ctaKronner);
			session.setAttribute("page.datasession", datasession);	
			String creditoIni=(String)request.getParameter("hdCreditoIni");
			session.setAttribute("creditoIni",creditoIni);
			if(txtTxn.equals("P001")){
				String RS_SDO_CAPVIG = res.substring(277,288);
				String RS_SDO_VENC = res.substring(333,344);
				String RS_SDO_INTS = res.substring(369,380);
				String RS_INT_VENC = res.substring(357,368);
				String RS_IMP_FECI_VIG = res.substring(1583,1594);
				String RS_IMP_FECI_VEN = res.substring(1595,1606);
				String RS_SDO_MORA = res.substring(405,416);
				String RS_GASTOS = res.substring(574,585);
				String RS_COMISIONES = res.substring(453,464);
				//String RS_SEGURO = res.substring(610,621);
				String RS_IVA_CAP_VEN = res.substring(1387,1398);
				String RS_IMP_SEG_VIG = res.substring(1487,1498);
				String RS_IMP_SEG_VEN = res.substring(1499,1510);
				String RS_IMP_SEGOVIG = res.substring(1733,1744);
				String RS_IMP_SEGOVEN = res.substring(1745,1756);
				String RS_TOT_VIGENTE = res.substring(429,440);
				String RS_TOT_ADEUDO = res.substring(489,500);
				String RS_GASTOSVIG = res.substring(1721,1732);
				String RS_SDO_SEGVEN = res.substring(586,597);
				String RS_COMISIONESVIG = res.substring(1709,1720);
				String RS_TOT_VENCIDO = res.substring(501,512);
				
				//Cambio ITBMS

                                String RS_ITBM_VIG = res.substring(1510,1522);
				String RS_ITBM_VEN = res.substring(1522,1534);
		        
				String RS_ITBM_VIG_GAS = res.substring(1920,1932);
				String RS_ITBM_VEN_GAS = res.substring(1932,1944);
				
				
				session.setAttribute("RS_SDO_CAPVIG",RS_SDO_CAPVIG);
				session.setAttribute("RS_SDO_VENC",RS_SDO_VENC);
				session.setAttribute("RS_SDO_INTS",RS_SDO_INTS);
				session.setAttribute("RS_INT_VENC",RS_INT_VENC);
				session.setAttribute("RS_IMP_FECI_VIG",RS_IMP_FECI_VIG);
				session.setAttribute("RS_IMP_FECI_VEN",RS_IMP_FECI_VEN);
				session.setAttribute("RS_SDO_MORA",RS_SDO_MORA);
				session.setAttribute("RS_GASTOS",RS_GASTOS);
				session.setAttribute("RS_COMISIONES",RS_COMISIONES);
				//session.setAttribute("RS_SEGURO",RS_SEGURO);
				session.setAttribute("RS_IVA_CAP_VEN",RS_IVA_CAP_VEN);
				session.setAttribute("RS_IMP_SEG_VIG",RS_IMP_SEG_VIG);
				session.setAttribute("RS_IMP_SEG_VEN",RS_IMP_SEG_VEN);
				session.setAttribute("RS_IMP_SEGOVIG",RS_IMP_SEGOVIG);
				session.setAttribute("RS_IMP_SEGOVEN",RS_IMP_SEGOVEN);
				session.setAttribute("RS_TOT_VIGENTE",RS_TOT_VIGENTE);
				session.setAttribute("RS_TOT_ADEUDO",RS_TOT_ADEUDO);
				session.setAttribute("RS_GASTOSVIG",RS_GASTOSVIG);
				session.setAttribute("RS_SDO_SEGVEN",RS_SDO_SEGVEN);
				session.setAttribute("RS_COMISIONESVIG",RS_COMISIONESVIG);
				session.setAttribute("RS_TOT_VENCIDO",RS_TOT_VENCIDO);
				//ITBMS
				session.setAttribute("RS_ITBM_VIG",RS_ITBM_VIG);
				session.setAttribute("RS_ITBM_VEN",RS_ITBM_VEN);
				session.setAttribute("RS_ITBM_VIG_GAS",RS_ITBM_VIG_GAS);
				session.setAttribute("RS_ITBM_VEN_GAS",RS_ITBM_VEN_GAS);

				response.sendRedirect("../ventanilla/paginas/PageBuilderP001.jsp");
			}
			else 
				response.sendRedirect("../ventanilla/paginas/PageBuilderP002.jsp");
				session.setAttribute("blagP002","P002");
		}
		else{
			session.setAttribute("page.txnresponse", sMessage);
			getServletContext().getRequestDispatcher("/servlet/ventanilla.Redirector").forward(request,response);
				
		}
        
	}
    
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}
}
