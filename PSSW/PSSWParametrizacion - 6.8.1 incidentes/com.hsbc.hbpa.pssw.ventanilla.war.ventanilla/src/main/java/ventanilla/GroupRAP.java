//*************************************************************************************************
//			   Funcion: Clase que realiza txn 5503 - 0736 - 0372
//			  Elemento: GroupRAP.java
//			Creado por: Alejandro Gonzalez Castro
//		Modificado por: Fausto Rodrigo Flores Moreno
//*************************************************************************************************
// CCN - 4360189 - 03/09/2004 - Se cambia invoke al Diario para Control de Efectivo
// CCN - 4360237 - 05/10/2004 - Se corrige problema de pago RAP en Efectivo y cheque para que solo
//								descuente el efectivo en un pago y no en todos
// CCN - 4360259 - 21/01/2005 - Se evita crear una tabla de bines que no se ocupa
// CCN - 4360319 - 13/05/2005 - Se envia tipo de pago en el servicio 4254 de RAP
// CCN - 4360334 - 17/06/2005 - Se elimina variable dbQualifier
// CCN - 4360364 - 19/08/2005 - Se realiza correcion a la cadena de envio de la txn 5503 cuando se paga en efectivo y en C.I. 
//								Se realizan correciones para la combinacion de varios pagos con efectivo y cheques
// CCN - 4360368 - 02/09/2005 - Se elimina sleep
// CCN - 4360374 - 08/09/2005 - Se genera nuevo paquete a peticion de QA.
// CCN - 4360425 - 03/02/2006 - Proyecto Tarjeta de Identificacion TDI
// CCN - 4360455 - 07/04/2006 - Se realizan modificaciones para el esquema de cheques devueltos.
// CCN - 4360462 - 10/04/2006 - Se genera nuevo paquete por errores en paquete anterior
// CCN - 4360474 - 05/05/2006 - Se realizan modificaciones para quitar el num cta del servicio RAP del diario
// CCN - 4360492 - 23/06/2006 - Se realizan modificaciones para el esquema de Cobro Comisi�n Usuarios RAP
// CCN - 4360574 - 09/03/2007 - Se realizan modificaciones para el esquema de RAP Calculadora
//*************************************************************************************************
package ventanilla;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Hashtable;
import java.util.StringTokenizer;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import ventanilla.com.bital.admin.Diario;
import ventanilla.com.bital.sfb.RAPB;
import ventanilla.com.bital.sfb.RAPE;
import ventanilla.com.bital.sfb.Transaction;
import ventanilla.com.bital.util.TransactionFactory;

import com.bital.util.ConnectionPoolingManager;
import com.bital.util.ParameterNotFoundException;

public class GroupRAP extends HttpServlet
{

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		HttpSession session = null;
		Hashtable datasession = null;
        
		response.setContentType("text/plain");
		session = request.getSession(false);
		datasession = (Hashtable)session.getAttribute("page.datasession");
		String dirip = (String)session.getAttribute("dirip");
		String ConLiga = (String)session.getAttribute("d_ConsecLiga");

		String CurrServ = (String)datasession.get("CurrServ");
		String CurrPago = (String)datasession.get("CurrPago");
        
		String Cajero = (String)datasession.get("teller");
		String conRAP = "";
		String tipo = "";
		String acepta = "";
		String cobrocom = "";
		String compcom = "";
		String txn = "";
		String codigo = "";
		String TipoCom = "";
		String fPagoCal = (String)datasession.get("RapCCheque");
		String DescRec = (String)datasession.get("Desc_Recargo");
		String fRAPCal = (String)datasession.get("RAPCalculadora");
		String no_descuento = (String)datasession.get("no_descuento");
		if(fPagoCal == null)
			fPagoCal = "E";//EFECTIVO
    		
		long mtotmp = 0;
		int consecrap = 0;

		boolean flagCobCom = false;
		
		if(CurrServ.equals("0") && CurrPago.equals("0"))
		{
			// CLEAR A TABLA CERTIF
			clearcertif(Cajero);
			// OBTENCION DE IMPORTES DE EFECTIVO ????
			mtotmp = getpago(datasession, "efe");
			CurrServ = "1";
			CurrPago = "1";
			datasession.put("ConRAP", "0");
		}
		else
		{
			// OBTENCION DE IMPORTES DE PAGOS POR RUBRO
			//mtotmp = pagos(datasession);

			String tem = (String)datasession.get("mtoRAP");
			mtotmp = Long.parseLong(tem);
			conRAP = (String)datasession.get("ConRAP");
			// Consecutivo de RAP para Certificaciones
			consecrap = Integer.parseInt(conRAP) +1;

			// Tipo de Servicio 1 o 2
			tipo = (String)datasession.get("tiposerv");

			// CONTROL DE PAGOS - ACEPTADO O RECHAZADO
			acepta = (String)datasession.get("aceptas"+CurrServ+"p"+CurrPago);

			TipoCom = (String)datasession.get("ComServRAP"+CurrServ);
			// AJUSTE PARA SERVICIOS EXCEPTO LOS QUE COBRAN COMISION
			if(tipo.equals("2"))
			{
				if(acepta == null)
					acepta = "0";
				else
					acepta = "1";
			}
			else
				acepta = "1";
           
			cobrocom = (String)datasession.get("cobrocoms"+CurrServ+"p"+CurrPago);
			if(cobrocom == null)
				cobrocom = "0";

			compcom = (String)datasession.get("compcoms"+CurrServ+"p"+CurrPago);
			if(compcom == null)
				compcom = "0";

			if(acepta.equals("1"))
			{
				// TXN 5503
				txn = "5503";
				codigo = RAP5503(datasession, txn, CurrServ, CurrPago, mtotmp, dirip, consecrap, ConLiga, session);


				String ChqDev = (String)datasession.get("chqDev");
				if(ChqDev != null)
				   {  
				   if (ChqDev.equals("SI"))
				   session.setAttribute("codigo5503",codigo);
						session.setAttribute("numServ5361",(String)datasession.get("s"+CurrServ));
				   }      


				if(codigo.equals("0") && tipo.equals("2") && cobrocom.equals("1"))
				{
                    
					if(TipoCom.equals("0")){
						txn = "0736";
						RAPBtxn(datasession, txn, CurrServ, CurrPago, dirip, consecrap, ConLiga, session);
					}else{
						txn = "0792";
						RAPBtxn(datasession, txn, CurrServ, CurrPago, dirip, consecrap, ConLiga, session);
					}
					txn = "0372";
					RAPBtxn(datasession, txn, CurrServ, CurrPago, dirip, consecrap, ConLiga, session);
				}
                
				//RAP CALCULADORA
			   if(fRAPCal != null){
					if(fRAPCal.equals("S") && codigo.equals("0")){
						if(no_descuento.equals("SI")){
							if(DescRec.equals("D")){
								if(fPagoCal.equals("S")){//CHEQUE
									txn = "5691";
									RAPCalculadora(datasession, txn, CurrServ, CurrPago, dirip, consecrap, ConLiga, session);
								}else{//EFECTIVO
									txn = "5689";
									RAPCalculadora(datasession, txn, CurrServ, CurrPago, dirip, consecrap, ConLiga, session);
								}
							}else if(DescRec.equals("R")){
								txn = "5687";
								RAPCalculadora(datasession, txn, CurrServ, CurrPago, dirip, consecrap, ConLiga, session);
							}
						}
					}
				}
              
			}

			String nservicios = (String)datasession.get("nos");
			String npagos = (String)datasession.get("nps"+CurrServ);

			if(CurrServ.equals(nservicios) && CurrPago.equals(npagos))
			{
				datasession.put("end", "1");
			}
			else
			{
				int Npago = Integer.parseInt(npagos);
				int Nserv = Integer.parseInt(nservicios);
				int Cpago = Integer.parseInt(CurrPago);
				int Cserv = Integer.parseInt(CurrServ);

				if(Cpago == Npago && Nserv > Cserv)
					Cserv = Cserv + 1;

				// Incfremento en el numero de pagos X servicio
				if(Npago > Cpago)
					Cpago = Cpago + 1;
				else
					Cpago = 1;

				CurrServ = Integer.toString(Cserv);
				CurrPago = Integer.toString(Cpago);
			}
			datasession.put("codigo", codigo);
		}

		conRAP = Integer.toString(consecrap);
		String tmp = Long.toString(mtotmp);
		String chqcmtH = (String)datasession.get("Cheque."+(String)session.getAttribute("identidadApp"));
		if (chqcmtH == null)
		   datasession.put("mtoRAP", tmp);
		datasession.put("ConRAP", conRAP);
		datasession.put("CurrServ", CurrServ);
		datasession.put("CurrPago", CurrPago);
		session.setAttribute("page.datasession",datasession);
		session.setAttribute("page.getoTxnView","ResponseRAP");

		session.setAttribute("page.txnresponse", "0~02~0151515~PROCESANDO RAP VERIFIQUE CERTIFICACION~");
		getServletContext().getRequestDispatcher("/servlet/ventanilla.Redirector").forward(request,response);
	}

	private void RAPBtxn(Hashtable datos, String Trxn, String serv, String pago, String dir, int consecutivorap,  String Conliga, HttpSession ses)
	{

		RAPB rapb = new RAPB();
		rapb.setFormat("B");
		rapb.setBranch((String)datos.get("sucursal"));
		rapb.setTeller((String)datos.get("teller"));
		rapb.setSupervisor((String)datos.get("supervisor"));
		GenericClasses gc = new GenericClasses();

		rapb.setTxnCode(Trxn);
		rapb.setAcctNo(Trxn);

		String cuenta = Trxn;
		String monto = "";
		String Teller = (String)datos.get("teller");

		if(Trxn.equals("0736") || Trxn.equals("0792"))
			monto = (String)datos.get("coms"+serv+"p"+pago);
		if(Trxn.equals("0372"))
			monto = (String)datos.get("ivas"+serv+"p"+pago);

		monto = clearmto(monto);

		rapb.setTranAmt(monto);
		rapb.setReferenc((String)datos.get("s"+serv));
		rapb.setCashIn(monto);
		String refs = "&&&";
		String moneda = (String)datos.get("moneda");

		rapb.setFromCurr(gc.getDivisa(moneda));
		Diario diario = new Diario();
		String sMessage = diario.invokeDiario(rapb, Conliga, dir, moneda, ses);

		String respuesta = sMessage;

		String codigo = respuesta.substring(0,1);

		if(Trxn.equals("0736")||Trxn.equals("0792")) 
		{
			String nombre = (String)datos.get("nombres"+serv+"p"+pago);
			if(nombre == null)
				nombre = "&";
			else
				nombre = nombre + "&";
			String dom = (String)datos.get("doms"+serv+"p"+pago);
			if(dom == null)
				dom = "&";
			else
				dom = dom + "&";
			String rfc = (String)datos.get("rfcs"+serv+"p"+pago);
			if(rfc == null)
				rfc = "&";
			else
				rfc = rfc + "&";
			refs = nombre + dom + rfc;
		}

		String amount = rapb.getTranAmt().toString().substring(0,rapb.getTranAmt().length() - 1);
		String efectivo = monto;

		certif(respuesta, Teller, cuenta, amount, efectivo, Trxn, moneda, consecutivorap, refs, codigo);
	}

	private String RAPCalculadora(Hashtable datos, String Trxn, String serv, String pago, String dir, int consecutivorap,  String Conliga, HttpSession ses)
	{
    	
		Transaction generic = null;
		String moneda = (String)datos.get("moneda");
		String monto = (String)datos.get("Monto_D_R");
		monto = clearmto(monto);

		String Teller = (String)datos.get("teller");
		String ref = (String)datos.get("Refere");
		String servicio = (String)datos.get("s"+serv);
		String cuenta = Trxn;
		String refs = (String)datos.get("ref1s"+serv+"p"+pago);
      
		generic = TransactionFactory.getTransaction(Trxn);
        
		if( generic == null )
			return "NO EXISTE LA TXN";
        
		try {// Obtener referencia a la transaccion
			generic.perform(datos);
		}
		catch( ParameterNotFoundException e ) {
			return "1~1~0000000~ERROR EN LA TRANSACCION~";
		}

		
		Diario diario = new Diario();
		String sMessage = diario.invokeDiario(generic.getProcess(), Conliga, dir, moneda, ses);        

		String respuesta = sMessage;				
		String codigo = respuesta.substring(0,1);
		
		if(codigo.equals("1"))
			codigo = "0";
		
/*   PARA CERTIFICAR LA TXN DE RECARGO - DESCUENTO*/
		String amount = generic.getProcess().getTranAmt().toString().substring(0,generic.getProcess().getTranAmt().toString().length()-1);
		String efectivo = "000";
		long mtoOrg=0;
		long mtoefe=0;
		long mtotem = 0;
		if(datos.get("montos1p1") != null)
			mtoOrg = Long.parseLong(clearmto(datos.get("montos1p1").toString()));
		
		if(datos.get("txtEfectivo") != null)
			mtoefe = Long.parseLong(clearmto(datos.get("txtEfectivo").toString()));
		
		if(Trxn.equals("5687")){
			if (mtoefe > mtoOrg){
				mtotem = mtoefe - mtoOrg;
				mtoOrg = 0;
			}else{
				mtoOrg = mtoOrg - mtoefe;
				mtotem = 0;
			}
			if(mtotem > 0){
				efectivo = new Long(mtotem).toString();
			}
			if(mtoOrg > 0){
				efectivo = "000";
			}
		}
		else if(Trxn.equals("5689"))
			efectivo = monto;
		else if(Trxn.equals("5691"))
			efectivo = "000";
	    
		certif(respuesta, Teller, cuenta, amount, efectivo, Trxn, moneda, consecutivorap, refs, codigo);
		
		return respuesta;
	}

	private void certif(String nrespuesta, String Teller, String Cuenta, String Amount, String Efectivo, String TXN, String Moneda, int Consecrap, String Refs, String Codigo)
	{

		StringTokenizer resp = new StringTokenizer(nrespuesta, "~");
		String respuesta = "";
		GenericClasses gc = new GenericClasses();
		while (resp.hasMoreElements()) {
			respuesta = respuesta + resp.nextElement().toString();
		}

		int lineas = Integer.parseInt(respuesta.substring(1,3));
		String consec = respuesta.substring(3,10);
		respuesta = respuesta.substring(10,respuesta.length());
        
		Moneda=gc.getDivisa(Moneda);
		Connection pooledConnection = ConnectionPoolingManager.getPooledConnection();
		Statement insert = null;
		String sql ="";
		try
		{
			if(pooledConnection != null)
			{
				int j = 0;
				for(int k=0; k<lineas; k++)
				{
					insert = pooledConnection.createStatement();
					String lresp = respuesta.substring(j,Math.min(j+78, respuesta.length()));
					String codresp = "";
					if(Codigo.equals("0"))
						codresp = "A";
					else
						codresp = "R";

					sql = "INSERT INTO TW_CERTIFICACIONES VALUES('"+Teller+
					"','"+codresp+Cuenta+"',"+Amount+","+Efectivo+",'"+TXN+"','"+Moneda+"',"+Consecrap+","+
					(k+1)+",'"+lresp.trim()+"','"+consec+"','"+Refs+"')";
					int code = 0;
					code = insert.executeUpdate(sql);
					j = j+78;
					if(insert != null){
						insert.close();
						insert = null;
					}
				}
			}
		}
		catch(SQLException sqlException)
		{
			System.out.println("Error GroupRAP::certif [" + sqlException + "]");
			System.out.println("Error GroupRAP::certif::SQL [" + sql + "]");
		}
		finally{
			try{
				if(insert != null){
					insert.close();
					insert = null;
				}
				if(pooledConnection != null){
					pooledConnection.close();
					pooledConnection = null;
				}
			}
			catch(java.sql.SQLException sqlException)
			{
				System.out.println("Error FINALLY GroupRAP::certif [" + sqlException + "]");
				System.out.println("Error FINALLY GroupRAP::certif::SQL [" + sql + "]");
			}
		}
	}

	private String RAP5503(Hashtable info, String Txn, String Serv, String Pago, long mtotem, String dirIP, int ConsecRAP, String conliga, HttpSession sesion)
	{
		String ref1 = "";
		String ref2 = "";
		String ref3 = "";
		String beneficiario = "";
		String refs = "";
		String servicio = "";
		String moneda = "";
		GenericClasses gc = new GenericClasses();
		Hashtable datasession = new Hashtable();
		
		datasession=(Hashtable)sesion.getAttribute("page.datasession");

		RAPE rape = new RAPE();
		rape.setFormat("A");
		rape.setBranch((String)info.get("sucursal"));
		rape.setTeller((String)info.get("teller"));
		rape.setSupervisor((String)info.get("supervisor"));
		if (sesion.getAttribute("Tarjeta.TDI") != null) //Loch Ness
			rape.setFiller((String)sesion.getAttribute("Tarjeta.TDI"));
//System.out.println("Tarjeta TDI : " + rape.getFiller());
		rape.setTxnCode(Txn);
		servicio = gc.rellenaZeros((String)info.get("s"+Serv),5);
		rape.setAcctNo(servicio);
		String cuenta = servicio;

		String monto = (String)info.get("montos"+Serv+"p"+Pago);

		monto = clearmto(monto);
        
		String tipo = (String)info.get("tiposerv");

		// OBTENCION DE MONTOS DE PAGO
		long mtoefe = 0;
		long mtorem = 0;
		long mtocta = 0;
		long mtochq = 0;
		long mtocoi = 0;
        
		mtoefe = getpago(info, "efe");
		mtorem = getpago(info, "rem");
		mtocta = getpago(info, "cta");
		mtochq = getpago(info, "chq");
		mtocoi = getpago(info, "coi");
        
		// CASO REMESAS 1 SERVICIO - 1 PAGO
		if(mtorem > 0 && mtoefe == 0 && mtocta == 0 && mtochq == 0 && mtocoi == 0) 
		{
			
			rape.setTranAmt(monto);
			rape.setDraftAm(monto);
			rape.setCashIn("000");
			rape.setTrNo("9");
			rape.setCheckNo(NumCheques((String)datasession.get("txtNumExtranjeros")));
		}
        
		// CASO CI Y EFECTIVO - 1 SERVICIO 1 PAGO
		else if(mtocoi > 0 && mtoefe > 0 && mtocta == 0 && mtochq == 0 && mtorem == 0)
		{
			rape.setTranAmt(new Long(mtocoi+mtoefe).toString());
			rape.setMoAmoun(new Long(mtocoi).toString());
			rape.setCashIn(new Long(mtoefe).toString());
            
			//agregan    
			rape.setDraftAm("000");
			String corte = (String)info.get("rdoACorte");
			corte = corte.substring(1);
			rape.setTrNo(corte);
			rape.setCheckNo(NumCheques((String)datasession.get("txtNumPropios")));
            
		}

		//CASO SOLO COI
		else if(mtocoi > 0 && mtoefe == 0 && mtocta == 0 && mtochq == 0 && mtorem == 0)
		{
			rape.setTranAmt(monto);
			rape.setMoAmoun(monto);
			rape.setCashIn("000");
			rape.setDraftAm("000");
			String corte = (String)info.get("rdoACorte");
			corte = corte.substring(1);
			rape.setTrNo(corte);
			rape.setCheckNo(NumCheques((String)datasession.get("txtNumPropios")));
		}
        
		// CASO SOLO EFECTIVO
		else if(mtoefe > 0 && mtocta == 0 && mtochq == 0 && mtorem == 0 && mtocoi == 0) 
		{
			rape.setTranAmt(monto);
			rape.setCashIn(monto);
		}
        
		// CASO EFECTIVO Y CTA O CHQ
		else if(mtoefe > 0 && mtocta >= 0 && mtochq >= 0 && mtorem == 0 && mtocoi == 0)
		{
			if (mtotem > Long.parseLong(monto)) 
			{
				rape.setCashIn(monto);
				rape.setTranAmt(monto);
				mtotem = mtotem - Long.parseLong(monto);
				if (mtochq > 0 && mtocta == 0)
				{
					info.put("mtoRAP",Long.toString(mtotem));
					info.put("Cheque."+(String)sesion.getAttribute("identidadApp"),"1");
				}
			}
			else if(mtotem == 0)
			{
				rape.setCashIn("000");
				rape.setTranAmt(monto);
			}
			else // (mtotmp < Long.parseLong(monto))
			{
				rape.setCashIn(new Long(mtotem).toString());
				rape.setTranAmt(monto);
				mtotem = 0;
				info.put("txtEfectivo", "0.00");
			}
		}
		// CASO CHQ Y / O CTA
		else if(mtochq >= 0 && mtocta >= 0 && mtoefe == 0 && mtorem == 0 && mtocoi == 0) 
		{
			rape.setTranAmt(monto);
			rape.setCashIn("000");
		}
		else 
		{
			System.out.println("No entra a ningun caso de combinacion de Pagos");
		}
		if(servicio.equals("4254"))
			rape.setTrNo("01");
		if(servicio.equals("480") || servicio.equals("448") || servicio.equals("297") || servicio.equals("66")) 
		{
			String RFC = (String)info.get("ref1s"+Serv+"p"+Pago);

			if(RFC != null) 
			{
				RFC = RFC.trim();
				int large = RFC.length();
				if (large == 10) {
					ref1 = RFC.substring(0,4);
					ref2 = RFC.substring(4,RFC.length());
					ref3 = "000";
				}
				else if (large == 13) {
					ref1 = RFC.substring(0,4);
					ref2 = RFC.substring(4,10);
					ref3 = RFC.substring(10,RFC.length());
				}
				else {
					ref1 = RFC.substring(0,3);
					ref2 = RFC.substring(3,9);
					ref3 = RFC.substring(9,RFC.length());
				}
				rape.setReferenc1(ref1);
				rape.setReferenc2(ref2);
				rape.setReferenc3(ref3);
			}
		}
		else 
		{
			ref1 = (String)info.get("ref1s"+Serv+"p"+Pago);
			if(ref1 != null)
				rape.setReferenc1(ref1);
			else
				ref1 = "";
			ref2 = (String)info.get("ref2s"+Serv+"p"+Pago);
			if(ref2 != null)
				rape.setReferenc2(ref2);
			else
				ref2 = "";
			ref3 = (String)info.get("ref3s"+Serv+"p"+Pago);
			if(ref3 != null)
				rape.setReferenc3(ref3);
			else
				ref3 = "";
		}

		refs = ref1 + "&" + ref2 + "&" + ref3 + "&";

		moneda = (String)info.get("moneda");
		rape.setFromCurr(gc.getDivisa(moneda));
		Diario diario = new Diario();
		String sMessage = diario.invokeDiario(rape, conliga, dirIP, moneda, sesion);

		String respuesta = sMessage;

		String codigo = respuesta.substring(0,1);

		String ConsecI = "0";
		String ConsecF = "0";
		String ConsecREM = "0";
        
		if(codigo.equals("0") && mtorem > 0)
		{
			ConsecREM = sMessage.substring(5,12);
			sesion.setAttribute("ConsecREM",ConsecREM);
			sesion.setAttribute("ServREM",servicio);
		}

		if(codigo.equals("0") && Pago.equals("1"))
		{
			if(respuesta.length() > 234)
			{
				beneficiario = respuesta.substring(235,respuesta.length()-1).trim();
				int y = 0;
				int count = 0;
				StringBuffer Ben = new StringBuffer(beneficiario);
				for(y = 0; y < Ben.length(); y++)
					if(Ben.charAt(y) == 0x26) 
					{
						count ++;
						Ben.replace(y,y+1,"%26");
					}
				beneficiario = Ben.toString();
				info.put("beneficiario"+Serv,beneficiario);
			}
			else
				beneficiario = "";

			if(tipo.equals("2"))
				info.put("comppago"+Serv,(String)info.get("comppago"+Serv));
		}

		if(codigo.equals("0") && Serv.equals("1") && Pago.equals("1"))
		{
			ConsecI = sMessage.substring(5,12);
			sesion.setAttribute("ConsecI",ConsecI);
		}

		if(codigo.equals("0") && Serv.equals("1") && !Pago.equals("1"))
		{
			ConsecF = sMessage.substring(5,12);
		}
		sesion.setAttribute("ConsecF",ConsecF);

		String amount = rape.getTranAmt().toString().substring(0,rape.getTranAmt().length() - 1);
		String efectivo = rape.getCashIn().toString().substring(0,rape.getCashIn().length() - 1);

		if(efectivo.length() == 0)
			efectivo = "000";

		String teller = (String)info.get("teller");
        
		certif(respuesta, teller, cuenta, amount, efectivo, Txn, moneda, ConsecRAP, refs, codigo);
		return codigo;
	}

	private void clearcertif(String cajero)
	{
		String statementPS = ConnectionPoolingManager.getStatementPostFix();
		Connection pooledConnection = ConnectionPoolingManager.getPooledConnection();
		PreparedStatement delete = null;
		try
		{
			if(pooledConnection != null)
			{
				delete = pooledConnection.prepareStatement("DELETE FROM TW_CERTIFICACIONES WHERE D_CAJERO=?" + statementPS);
				delete.setString(1,cajero);
				int i = delete.executeUpdate();
			}
		}
		catch(SQLException sqlException)
			{System.out.println("Error GroupRAP::clearcertif [" + sqlException + "]");}
		finally
		{
			try
			{
				if(delete != null)
				{
					delete.close();
					delete = null;
				}
				if(pooledConnection != null)
				{
					pooledConnection.close();
					pooledConnection = null;
				}
			}
			catch(java.sql.SQLException sqlException){System.out.println("Error FINALLY GroupRAP::clearcertif [" + sqlException + "]");}
		}
	}

	private long getpago(Hashtable data, String pago)
	{
		String monto = new String();
		long mto = 0;
        
		if(pago.equals("efe"))
		{
			monto = (String)data.get("txtEfectivo");
			if(monto != null)
			{
				monto = clearmto(monto);
				mto = Long.parseLong(monto);
			}
		}

		if(pago.equals("chq"))
		{
			monto = (String)data.get("txtCheque");
			if(monto != null)
			{
				monto = clearmto(monto);
				mto = Long.parseLong(monto);
			}
		}
        
		if(pago.equals("cta"))
		{
			monto = (String)data.get("txtCuenta");
			if(monto != null)
			{
				monto = clearmto(monto);
				mto = Long.parseLong(monto);
			}   
		}
        
		if(pago.equals("coi"))
		{
			monto = (String)data.get("txtDocsCI");
			if(monto != null)
			{
				monto = clearmto(monto);
				mto = Long.parseLong(monto);
				if(mto > 0)
					data.put("CIrap", "1");
			}
		}

		if(pago.equals("rem"))
		{
			monto = (String)data.get("txtRemesas");
			if(monto != null)
			{
				monto = clearmto(monto);
				mto = Long.parseLong(monto);
				if(mto > 0)
					data.put("REMrap", "1");
			}
		}
        
		return mto;
	}

	private String clearmto(String mto)
	{
		StringBuffer montotmp = new StringBuffer(mto);
		for(int k=0; k<montotmp.length();) {
			if(montotmp.charAt(k) == ',' || montotmp.charAt(k) == '.')
				montotmp.deleteCharAt(k);
			else
				++k;
		}
		mto = montotmp.toString();
		return mto;
	}
	
	private String NumCheques(String numCheques)
	{
		String cheques=null;
		if(numCheques.length() == 1)
			cheques="000"+numCheques;
		else if(numCheques.length() == 2)
		     cheques="00"+numCheques;
		else if(numCheques.length() == 3)
			 cheques="0"+numCheques;
		else
			cheques=numCheques;
		return cheques;
	}

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		doPost(request, response);
	}
}

