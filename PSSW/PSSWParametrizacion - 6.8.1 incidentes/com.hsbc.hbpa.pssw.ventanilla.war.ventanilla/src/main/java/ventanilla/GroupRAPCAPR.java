//*************************************************************************************************
//             Funcion: Clase que realiza txn 5503 para RAPL
//            Elemento: GroupRAPCAPR.java
//*************************************************************************************************
// 
//*************************************************************************************************
package ventanilla;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.Vector;
import java.util.Hashtable;
import java.util.StringTokenizer;
import com.bital.util.ConnectionPoolingManager;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import ventanilla.com.bital.admin.Diario;
import ventanilla.com.bital.beans.BinesTDC;
import ventanilla.com.bital.beans.DatosVarios;
import ventanilla.com.bital.sfb.RAPE;
import ventanilla.com.bital.sfb.RAPB;
import ventanilla.com.bital.sfb.Transaction;
import ventanilla.com.bital.util.TransactionFactory;
import com.bital.util.ParameterNotFoundException;
import ventanilla.GenericClasses; //JGA


public class GroupRAPCAPR extends HttpServlet
{
	
	
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        HttpSession session = null;
        Hashtable datasession = null;
        
        response.setContentType("text/plain");
        session = request.getSession(false);
        datasession = (Hashtable)session.getAttribute("page.datasession");
        
        String dirip = (String)session.getAttribute("dirip");
        String ConLiga = (String)session.getAttribute("d_ConsecLiga");
        String CurrServ = (String)datasession.get("CurrServ");
        String CurrPago = (String)datasession.get("CurrPago");
        
        String Cajero = (String)datasession.get("teller");
        
        String conRAP = "";
        String tipo = "";
        String acepta = "";
        String cobrocom = "";
        String compcom = "";
        String txn = "";
        String codigo = "";
        String TipoCom = "";
        String fPagoCal = (String)datasession.get("RapCCheque");
        String fPagoCalTDC = "";
        String DescRec = (String)datasession.get("Desc_Recargo");
        String fRAPCal = (String)datasession.get("RAPCalculadora");
    	String no_descuento = (String)datasession.get("no_descuento");
    	
    	           
		GenericClasses GC = new GenericClasses();
    	if(fPagoCal == null){
    		fPagoCal = "E";//EFECTIVO
    	}	
        long mtotmp = 0;
        int consecrap = 0;
        
        if(CurrServ.equals("0") && CurrPago.equals("0"))
        {
        	
            // CLEAR A TABLA CERTIF
            clearcertif(Cajero);
            // OBTENCION DE IMPORTES DE EFECTIVO ????
            mtotmp = getpago(datasession, "efe");
            CurrServ = "1";
            CurrPago = "1";
            datasession.put("ConRAP", "0");
        }
        else
        {
        	
             // OBTENCION DE IMPORTES DE PAGOS POR RUBRO

            String tem = (String)datasession.get("mtoRAP");
            mtotmp = Long.parseLong(tem);
            conRAP = (String)datasession.get("ConRAP");
            // Consecutivo de RAP para Certificaciones
            consecrap = Integer.parseInt(conRAP) +1;

            // Tipo de Servicio 1 o 2
            tipo = (String)datasession.get("tiposerv");

            // CONTROL DE PAGOS - ACEPTADO O RECHAZADO
            acepta = (String)datasession.get("aceptas"+CurrServ+"p"+CurrPago);

			TipoCom = (String)datasession.get("ComServRAP"+CurrServ);
            // AJUSTE PARA SERVICIOS EXCEPTO LOS QUE COBRAN COMISION
			
			
            if(tipo.equals("2"))
            {
                if(acepta == null){
                    acepta = "0";
                }
                else
                {
                    acepta = "1";
                }
            }
            else{
            	
                acepta = "1";
            }
           
            cobrocom = (String)datasession.get("cobrocoms"+CurrServ+"p"+CurrPago);
        	
            if(cobrocom == null){
                cobrocom = "0";
            }
            compcom = (String)datasession.get("compcoms"+CurrServ+"p"+CurrPago);
           
            if(compcom == null){
                compcom = "0";
            }
            if(acepta.equals("1"))
            {
            	
                codigo = "0";
				String NumServ = (String) datasession.get("s" + CurrServ);
				BinesTDC tdcbines = BinesTDC.getInstance();
				Vector bines = tdcbines.getBines();
				DatosVarios servrap = DatosVarios.getInstance();
				String servMQ = "1";
				if(servrap.getDatosv("SERVMQ")!=null){
					
					servMQ = servrap.getDatosv("SERVMQ");
				}
				String fpago = getfPago(datasession);
				if(NumServ.equals("1926")){
					
					String tipoPago = datasession.get("rdoPagoAD1") == null? "0":(String)datasession.get("rdoPagoAD1");
					datasession.put("fPagoKroner", GC.getFormaPago(tipoPago, getPago(datasession)));
				}
                if(NumServ.equals("60") && GC.isHSBC(NumServ,(String)datasession.get("ref1s"+CurrServ+"p"+CurrPago),bines) && servMQ.equals("1") && (fpago.equals("efe") || fpago.equals("chq"))){
					// TXN WPAG PARA CONNEX-WHIRL
                	
                	txn = "WPAG";
                	codigo = RAPTDC(datasession, txn, CurrServ, CurrPago, dirip, consecrap, ConLiga, session);
                }
                if(datasession.get("PagoTDC")!= null && datasession.get("PagoTDC").toString().equals("SI") && datasession.get("txtTDC")!=null && !datasession.get("txtTDC").toString().equals("0.00")){
                	
                	txn = "PTCR";
                	codigo = POSTDC(datasession, txn, CurrServ, CurrPago, dirip, consecrap, ConLiga, session);
                	if(codigo.equals("0")){
                		
                		fPagoCalTDC =(String)datasession.get("RapCTDC");
                		 
                		txn = "0132";
                		codigo = RAP0132(datasession, txn, CurrServ, CurrPago, dirip, consecrap, ConLiga, session);
                	}
                }
                if(codigo.equals("0")){
                	
                	// TXN 5503
	                txn = "5503";
	                codigo = RAP5503(datasession, txn, CurrServ, CurrPago, mtotmp, dirip, consecrap, ConLiga, session);


	                String ChqDev = (String)datasession.get("chqDev");
	                
	                if(ChqDev != null)
	                   {
	                	
	                   if (ChqDev.equals("SI")){
	                	   
							long MontoRecibido= 0;
							String MontoDevuelto="";
						
							String TotalCheque = (String)datasession.get("txtCheque");
							 
							TotalCheque=GC.delCommaPointFromString(TotalCheque);				   		
							if (datasession.get("MontoDevuelto") != null){
								MontoDevuelto =(String)datasession.get("MontoDevuelto");
								MontoRecibido = Long.parseLong(TotalCheque) - Long.parseLong(MontoDevuelto); 			
								session.setAttribute("MontoRecibido",String.valueOf(MontoRecibido));
							}
	                   		session.setAttribute("codigo5503",codigo);
	                   		session.setAttribute("numServ5361",(String)datasession.get("s"+CurrServ));
	                   	}      
	                   }
	                
	                if(codigo.equals("0") && NumServ.equals("1926") && (fpago.equals("efe") || fpago.equals("chq") || fpago.equals("cert"))){
	                	txn = "KPAG";
	                	codigo = RAPKRONER(datasession, txn, CurrServ, CurrPago, dirip, consecrap, ConLiga, session);
	                }
	                
	                if(codigo.equals("0") && tipo.equals("2") && cobrocom.equals("1"))
	                {
	                	String comsSer = (String)datasession.get("coms"+CurrServ+"p"+CurrPago);
	                	String ivasSer = (String)datasession.get("coms"+CurrServ+"p"+CurrPago);
	                	boolean cobrar = true;
	                    if(NumServ.equals("60") && comsSer.equals("0.00") && ivasSer.equals("0.00")){
	                    	cobrar = false;
	                     }
	                    if(cobrar){
		                	if(TipoCom.equals("0")){
		                		
		                    	txn = "0736";
		                    	RAPBtxn(datasession, txn, CurrServ, CurrPago, dirip, consecrap, ConLiga, session);
		                    }else{
		                    	
								txn = "0792";
				                RAPBtxn(datasession, txn, CurrServ, CurrPago, dirip, consecrap, ConLiga, session);
		                    }
		                    txn = "0372";
		                    RAPBtxn(datasession, txn, CurrServ, CurrPago, dirip, consecrap, ConLiga, session);
	                    }
	                }
	                
	                //RAP CALCULADORA
	               	if(fRAPCal != null){
	               		
	                	if(fRAPCal.equals("S") && codigo.equals("0")){
	                		
	                		if(no_descuento.equals("SI")){
	                			
		                		if(DescRec.equals("D")){
		                			
			   		        		if(fPagoCal.equals("S") || fPagoCalTDC.equals("S")){//CHEQUE
			   		        			
			   		        			txn = "5691";
	        		        			RAPCalculadora(datasession, txn, CurrServ, CurrPago, dirip, consecrap, ConLiga, session);
	   		    		    		}else{//EFECTIVO
	   		    		    			
	       		        				txn = "5689";
		                				RAPCalculadora(datasession, txn, CurrServ, CurrPago, dirip, consecrap, ConLiga, session);
	        		        		}
		                		}else if(DescRec.equals("R")){
		                			
	    	            			txn = "5687";
		    	            		RAPCalculadora(datasession, txn, CurrServ, CurrPago, dirip, consecrap, ConLiga, session);
		                		}
		               		}
	                	}
	                }
                }
            }

            String nservicios = (String)datasession.get("nos");
            String npagos = (String)datasession.get("nps"+CurrServ);

            if(CurrServ.equals(nservicios) && CurrPago.equals(npagos))
            {
                datasession.put("end", "1");
            }
            else
            {
                int Npago = Integer.parseInt(npagos);
                int Nserv = Integer.parseInt(nservicios);
                int Cpago = Integer.parseInt(CurrPago);
                int Cserv = Integer.parseInt(CurrServ);

                if(Cpago == Npago && Nserv > Cserv)
                    Cserv = Cserv + 1;

                // Incfremento en el numero de pagos X servicio
                if(Npago > Cpago)
                    Cpago = Cpago + 1;
                else
                    Cpago = 1;

                CurrServ = Integer.toString(Cserv);
                CurrPago = Integer.toString(Cpago);
            }
            datasession.put("codigo", codigo);            
        }

        conRAP = Integer.toString(consecrap);
        String tmp = Long.toString(mtotmp);
        String chqcmtH = (String)datasession.get("Cheque."+(String)session.getAttribute("identidadApp"));
        
        if (chqcmtH == null){
           datasession.put("mtoRAP", tmp);
        }
        datasession.put("ConRAP", conRAP);
        datasession.put("CurrServ", CurrServ);
        datasession.put("CurrPago", CurrPago);
        session.setAttribute("page.datasession",datasession);
        session.setAttribute("page.getoTxnView","ResponseRAPCAPR");
        
        session.setAttribute("page.txnresponse", "0~02~0151515~PROCESANDO RAP VERIFIQUE CERTIFICACION~");
        getServletContext().getRequestDispatcher("/servlet/ventanilla.Redirector").forward(request,response);
    }

    private void RAPBtxn(Hashtable datos, String Trxn, String serv, String pago, String dir, int consecutivorap,  String Conliga, HttpSession ses)
    {
    	
        RAPB rapb = new RAPB();
        rapb.setFormat("B");
        rapb.setBranch((String)datos.get("sucursal"));
        rapb.setTeller((String)datos.get("teller"));
        rapb.setSupervisor((String)datos.get("supervisor"));

        rapb.setTxnCode(Trxn);
        rapb.setAcctNo(Trxn);

        String cuenta = Trxn;
        String monto = "";
        String Teller = (String)datos.get("teller");

        if(Trxn.equals("0736") || Trxn.equals("0792")){
            monto = (String)datos.get("coms"+serv+"p"+pago);
        }
        if(Trxn.equals("0372")){
            monto = (String)datos.get("ivas"+serv+"p"+pago);
        }    
        monto = clearmto(monto);

        rapb.setTranAmt(monto);
        rapb.setReferenc((String)datos.get("s"+serv));
        rapb.setCashIn(monto);
        String refs = "&&&";
        String moneda = (String)datos.get("moneda");
            	String strEfecTotalCalc = datos.get("EfecTotalCalc") == null? "0":(String)datos.get("EfecTotalCalc");
    	long EfecTotalCalc = Long.parseLong(strEfecTotalCalc);
        long mtoefec = Long.parseLong(monto);
        
        EfecTotalCalc = EfecTotalCalc + mtoefec;
        datos.put("EfecTotalCalc", String.valueOf(EfecTotalCalc));
        
        if(moneda.equals("01")) 
            rapb.setFromCurr("N$");
        else if(moneda.equals("02")) 
            rapb.setFromCurr("US$");
        else 
            rapb.setFromCurr("UDI");
        
        Diario diario = new Diario();
        String sMessage = diario.invokeDiario(rapb, Conliga, dir, moneda, ses);

        String respuesta = sMessage;
       
        String codigo = respuesta.substring(0,1);

        if(Trxn.equals("0736")||Trxn.equals("0792")) 
        {
            String nombre = (String)datos.get("nombres"+serv+"p"+pago);
            if(nombre == null)
                nombre = "&";
            else
                nombre = nombre + "&";
            String dom = (String)datos.get("doms"+serv+"p"+pago);
            if(dom == null)
                dom = "&";
            else
                dom = dom + "&";
            String rfc = (String)datos.get("rfcs"+serv+"p"+pago);
            if(rfc == null)
                rfc = "&";
            else
                rfc = rfc + "&";
            refs = nombre + dom + rfc;
        }

        String amount = rapb.getTranAmt().toString().substring(0,rapb.getTranAmt().length() - 1);
        String efectivo = monto;

        certif(respuesta, Teller, cuenta, amount, efectivo, Trxn, moneda, consecutivorap, refs, codigo);
    }
    
    private String POSTDC(Hashtable datos, String Trxn, String serv, String pago, String dir, int consecutivorap,  String Conliga, HttpSession ses){
    	Transaction generic = null;
    	String moneda = (String)datos.get("moneda");
		String monto = (String)datos.get("montos"+serv+"p"+pago);
		String servicio = (String)datos.get("s"+serv);
		monto = clearmto(monto);
				
		String RapCalTDC = "";
		if(datos.get("RAPCalculadora")!=null){
			RapCalTDC = (String)datos.get("RAPCalculadora");
			if(RapCalTDC.equals("S"))
				datos.put("RapCTDC","S");//Se pago con cheque
			else
				datos.put("RapCTDC","N");//No se pago con cheque
		}else
			datos.put("RapCTDC","N");//No se pago con cheque
		
		
		datos.put("servTDC",servicio);
      	datos.put("montoTDC",monto);
		
            	
    	generic = TransactionFactory.getTransaction(Trxn);
        
		if( generic == null ){
			return "NO EXISTE LA TXN";
		}
        
		try {// Obtener referencia a la transaccion
			generic.perform(datos);
		}
		catch( ParameterNotFoundException e ) {
			return "1~1~0000000~ERROR EN LA TRANSACCION~";
		}
		
		
		Diario diario = new Diario();
		String sMessage = diario.invokeDiario(generic.getProcess(), Conliga, dir, moneda, ses);
    	
		String respuesta = sMessage;				
		String codigo = respuesta.substring(0,1);
		
    	return codigo;
    }
    
    private String RAP0132(Hashtable datos, String Trxn, String serv, String pago, String dir, int consecutivorap,  String Conliga, HttpSession ses){

    	Transaction generic = null;
    	GenericClasses gc = new GenericClasses();
    	String moneda = (String)datos.get("moneda");
    	String strTDC = (String)datos.get("txtTrack2TDC");
    	strTDC = strTDC.substring(0,16);
    	String strAfiliacion = (String) datos.get("NumAfiliacionTDC");
    	String refer1 = "";
    	String refer2 = "";
    	String refer3 = "";    	
    	String AutorizTDC = (String)ses.getAttribute("AutorizTDC");
    	
        if(datos.get("ref1s"+serv+"p"+pago) != null){
            refer1 = (String)datos.get("ref1s"+serv+"p"+pago);
        }
        if(datos.get("ref2s"+serv+"p"+pago) != null){
            refer2 = (String)datos.get("ref2s"+serv+"p"+pago);
        }
        if(datos.get("ref3s"+serv+"p"+pago) != null){
            refer3 = (String)datos.get("ref3s"+serv+"p"+pago); 	
        }
    	datos.put("tarjetaTDC",strTDC);
    	datos.put("refer0",gc.StringFiller(AutorizTDC,8,false," ") + gc.StringFiller(strAfiliacion,8,false," "));
      	datos.put("refer1",refer1);
      	datos.put("refer2",refer2);
      	datos.put("refer3",refer3);
    	
        generic = TransactionFactory.getTransaction(Trxn);
        
		if( generic == null ){
			return "NO EXISTE LA TXN";
		}
        
		try {// Obtener referencia a la transaccion
			generic.perform(datos);
		}
		catch( ParameterNotFoundException e ) {
			return "1~1~0000000~ERROR EN LA TRANSACCION~";
		}
		Diario diario = new Diario();
		String sMessage = diario.invokeDiario(generic.getProcess(), Conliga, dir, moneda, ses);
    	
		String respuesta = sMessage;				
		String codigo = respuesta.substring(0,1);
        
        return codigo;
    }

	private String RAPTDC(Hashtable datos, String Trxn, String serv, String pago, String dir, int consecutivorap,  String Conliga, HttpSession ses)
	{
		
		Transaction generic = null;
		String moneda = (String)datos.get("moneda");
		String monto = (String)datos.get("montos"+serv+"p"+pago);
		String Teller = (String)datos.get("teller");
		String servicio = (String)datos.get("s"+serv);
		String cuenta = (String)datos.get("s"+serv);//Trxn;
		String refs = (String)datos.get("ref1s"+serv+"p"+pago);
		monto = clearmto(monto);

		
      	datos.put("servTDC",servicio);
      	datos.put("refTDC",refs);
      	datos.put("montoTDC",monto);

		generic = TransactionFactory.getTransaction(Trxn);
        
		if( generic == null ){
			
			return "NO EXISTE LA TXN";
		}
        
		try {// Obtener referencia a la transaccion
			generic.perform(datos);
		}
		catch( ParameterNotFoundException e ) {
			return "1~1~0000000~ERROR EN LA TRANSACCION~";
		}
		String amount = generic.getProcess().getTranAmt().toString().substring(0,generic.getProcess().getTranAmt().toString().length()-1);

		String efectivo = "000";
		
		
		Diario diario = new Diario();
		String sMessage = diario.invokeDiario(generic.getProcess(), Conliga, dir, moneda, ses);        

		String respuesta = sMessage;				
		String codigo = respuesta.substring(0,1);
		if (codigo.equals("0")){
			
		   String nomCliente  = (String)ses.getAttribute("nomCortoConnex") == null ? "" : (String)ses.getAttribute("nomCortoConnex");		
		   ses.removeAttribute("nomCortoConnex");	    
		   refs = refs+"&"+nomCliente;
		}	    
		certif(respuesta, Teller, cuenta, amount, efectivo, Trxn, moneda, consecutivorap, refs, codigo);
		
		return codigo;
	}

	private String RAPKRONER(Hashtable datos, String Trxn, String serv, String pago, String dir, int consecutivorap,  String Conliga, HttpSession ses)
	{
		
		Transaction generic = null;
		String moneda = (String)datos.get("moneda");
		String monto = (String)datos.get("montos"+serv+"p"+pago);
		String Teller = (String)datos.get("teller");
		String cuenta = (String)datos.get("s"+serv);//Trxn;
		String refs = (String)datos.get("ref1s"+serv+"p"+pago);
		monto = clearmto(monto);
		datos.put("montoKroner",monto);
		generic = TransactionFactory.getTransaction(Trxn);
		
        
		if( generic == null ){
			
			return "NO EXISTE LA TXN";
		}
        
		try {// Obtener referencia a la transaccion
			generic.perform(datos);
		}
		catch( ParameterNotFoundException e ) {
			return "1~1~0000000~ERROR EN LA TRANSACCION~";
		}
		String amount = generic.getProcess().getTranAmt().toString().substring(0,generic.getProcess().getTranAmt().toString().length()-1);

		String efectivo = "000";
		
		
		Diario diario = new Diario();
		String sMessage = diario.invokeDiario(generic.getProcess(), Conliga, dir, moneda, ses);        

		String respuesta = sMessage;				
		String codigo = respuesta.substring(0,1);
			    
		certif(respuesta, Teller, cuenta, amount, efectivo, Trxn, moneda, consecutivorap, refs, codigo);
		
		return codigo;
	}
	
	private String RAPCalculadora(Hashtable datos, String Trxn, String serv, String pago, String dir, int consecutivorap,  String Conliga, HttpSession ses)
    {
		
    	
	    Transaction generic = null;
        String moneda = (String)datos.get("moneda");
        String monto = (String)datos.get("Monto_D_R");
        monto = clearmto(monto);

		String Teller = (String)datos.get("teller");
		String ref = (String)datos.get("Refere");
		String servicio = (String)datos.get("s"+serv);
	    String cuenta = Trxn;
	    String refs = (String)datos.get("ref1s"+serv+"p"+pago);
	    
	  	    
	    if(Trxn.equals("5691") && datos.get("PagoTDC")!= null && datos.get("PagoTDC").toString().equals("SI") && datos.get("txtTDC")!=null && !datos.get("txtTDC").toString().equals("0.00")){
	    	datos.put("flagTDC", "TDC");
	    	
	    }else{
	    	
	    	datos.put("flagTDC", "");
	 	}
	   	String strEfecTotalCalc = datos.get("EfecTotalCalc") == null? "0":(String)datos.get("EfecTotalCalc");
    	long EfecTotalCalc = Long.parseLong(strEfecTotalCalc);
        long mtoefec = Long.parseLong(monto);
        
        if(Trxn.equals("5689")){
        	EfecTotalCalc = EfecTotalCalc - mtoefec;
        }else if (Trxn.equals("5687")){
        	EfecTotalCalc = EfecTotalCalc + mtoefec;
        }
        datos.put("EfecTotalCalc", String.valueOf(EfecTotalCalc));
	    
	    
        generic = TransactionFactory.getTransaction(Trxn);
        
        if( generic == null ){
        	
            return "NO EXISTE LA TXN";
        }
        
        try {// Obtener referencia a la transaccion
            generic.perform(datos);
        }
        catch( ParameterNotFoundException e ) {
            return "1~1~0000000~ERROR EN LA TRANSACCION~";
        }

        Diario diario = new Diario();
        String sMessage = diario.invokeDiario(generic.getProcess(), Conliga, dir, moneda, ses);        

        String respuesta = sMessage;				
		String codigo = respuesta.substring(0,1);
		
		if(codigo.equals("1")){
			codigo = "0";
		}
/*   PARA CERTIFICAR LA TXN DE RECARGO - DESCUENTO*/
        String amount = generic.getProcess().getTranAmt().toString().substring(0,generic.getProcess().getTranAmt().toString().length()-1);
        
        String efectivo = "000";
        long mtoOrg=0;
        long mtoefe=0;
        long mtotem = 0;
        if(datos.get("montos1p1") != null){
			mtoOrg = Long.parseLong(clearmto(datos.get("montos1p1").toString()));
			
        }
		
		if(datos.get("txtEfectivo") != null){
			
			mtoefe = Long.parseLong(clearmto(datos.get("txtEfectivo").toString()));
		}
		
        if(Trxn.equals("5687")){
        	
   			if (mtoefe > mtoOrg){
	        	mtotem = mtoefe - mtoOrg;
    	    	mtoOrg = 0;
        	}else{
        		mtoOrg = mtoOrg - mtoefe;
	        	mtotem = 0;
			}
            if(mtotem > 0){
				efectivo = new Long(mtotem).toString();
	        }
    	    if(mtoOrg > 0){
        		efectivo = "000";
	        }
        }
	    else if(Trxn.equals("5689")){
	    	
	    	efectivo = monto;
	    }
	    else if(Trxn.equals("5691")){
	    	
	    	efectivo = "000";
	    }
	    
        certif(respuesta, Teller, cuenta, amount, efectivo, Trxn, moneda, consecutivorap, refs, codigo);
		
		return respuesta;
    }

    private void certif(String nrespuesta, String Teller, String Cuenta, String Amount, String Efectivo, String TXN, String Moneda, int Consecrap, String Refs, String Codigo)
    {
    	
    	
        StringTokenizer resp = new StringTokenizer(nrespuesta, "~");
        String respuesta = "";
        while (resp.hasMoreElements()) {
            respuesta = respuesta + resp.nextElement().toString();
        }
        int lineasTmp = 0;
        int residuo = 0;
        int lineas = Integer.parseInt(respuesta.substring(1,3));
        String consec = respuesta.substring(3,10);
        respuesta = respuesta.substring(10,respuesta.length());
        lineasTmp = respuesta.length()/78;
        residuo = respuesta.length()%78;

        if(lineas != lineasTmp){
        	if(residuo > 0){
        		lineas = lineasTmp + 1;
        	}else{
        		lineas = lineasTmp;
        	}
        }
        if(Moneda.equals("01"))
            Moneda = "N$";
        else
            Moneda = "US$";

        Connection pooledConnection = ConnectionPoolingManager.getPooledConnection();
        Statement insert = null;
        String sql ="";
        try
        {
            if(pooledConnection != null)
            {
            	
                int j = 0;
                for(int k=0; k<lineas; k++)
                {
                	
                    insert = pooledConnection.createStatement();
                    String lresp = respuesta.substring(j,Math.min(j+78, respuesta.length()));
                    String codresp = "";
                    if(Codigo.equals("0")){
                    	codresp = "A";
                    }
                    else{
                    	
                        codresp = "R";
                    }

                    sql = "INSERT INTO TW_CERTIFICACIONES VALUES('"+Teller+
                    "','"+codresp+Cuenta+"',"+Amount+","+Efectivo+",'"+TXN+"','"+Moneda+"',"+Consecrap+","+
                    (k+1)+",'"+lresp.trim()+"','"+consec+"','"+Refs+"')";
                    
                    int code = 0;
                    code = insert.executeUpdate(sql);
                    j = j+78;
                    if(insert != null){
                    	
                        insert.close();
                        insert = null;
                    }
                }
            }
        }
        catch(SQLException sqlException)
        {
            System.out.println("Error GroupRAPCAPR::certif [" + sqlException + "]");
            System.out.println("Error GroupRAPCAPR::certif::SQL [" + sql + "]");
        }
        finally{
            try{
                if(insert != null){
                    insert.close();
                    insert = null;
                }
                if(pooledConnection != null){
                    pooledConnection.close();
                    pooledConnection = null;
                }
            }
            catch(java.sql.SQLException sqlException)
            {
                System.out.println("Error FINALLY GroupRAPCAPR::certif [" + sqlException + "]");
                System.out.println("Error FINALLY GroupRAPCAPR::certif::SQL [" + sql + "]");
            }
        }
    }

    private String RAP5503(Hashtable info, String Txn, String Serv, String Pago, long mtotem, String dirIP, int ConsecRAP, String conliga, HttpSession sesion)
    {
    	
        String ref1 = "";
        String ref2 = "";
        String ref3 = "";
        String beneficiario = "";
        String refs = "";
        String servicio = "";
        String moneda = "";

        RAPE rape = new RAPE();
        rape.setFormat("A");
        rape.setBranch((String)info.get("sucursal"));
        rape.setTeller((String)info.get("teller"));
        rape.setSupervisor((String)info.get("supervisor"));
        if (sesion.getAttribute("Tarjeta.TDI") != null){ //Loch Ness
        	
           	rape.setFiller((String)sesion.getAttribute("Tarjeta.TDI"));
    }

        rape.setTxnCode(Txn);
        servicio = (String)info.get("s"+Serv);
        rape.setAcctNo(servicio);
        String cuenta = servicio;

        String monto = (String)info.get("montos"+Serv+"p"+Pago);

        monto = clearmto(monto);
        
        String tipo = (String)info.get("tiposerv");
        GenericClasses gc = new GenericClasses();
    	String strEfecTotalCalc = info.get("EfecTotalCalc") == null? "0":(String)info.get("EfecTotalCalc");
    	strEfecTotalCalc = gc.quitap(strEfecTotalCalc);
    	long EfecTotalCalc = Long.parseLong(strEfecTotalCalc); 
        
        // OBTENCION DE MONTOS DE PAGO
        long mtoefe = 0;
        long mtorem = 0;
        long mtocta = 0;
        long mtochq = 0;
        long mtocoi = 0;
        long mtotdc = 0;
        long mtochqcert = 0;
        
        mtoefe = getpago(info, "efe");
        mtorem = getpago(info, "rem");
        mtocta = getpago(info, "cta");
        mtochq = getpago(info, "chq");
        mtocoi = getpago(info, "coi");
        mtotdc = getpago(info, "tdc");
        mtochqcert = getpago(info, "cert");
        
       
        //JGA tipos de pagos
        Hashtable datasession = null;
        datasession = (Hashtable)sesion.getAttribute("page.datasession");
        
        
        String tipoPago = getfPago(datasession);
        
        datasession.put("TipoPago", tipoPago);
        sesion.setAttribute("page.datasession",datasession);
        
    
        
        // CASO REMESAS 1 SERVICIO - 1 PAGO
        if(mtorem > 0 && mtoefe == 0 && mtocta == 0 && mtochq == 0 && mtocoi == 0 && mtotdc == 0 && mtochqcert == 0) 
        {
        	
            rape.setTranAmt(monto);
            rape.setDraftAm(monto);
            rape.setCashIn("000");
        }
        
        // CASO CI Y EFECTIVO - 1 SERVICIO 1 PAGO
        else if(mtocoi > 0 && mtoefe > 0 && mtocta == 0 && mtochq == 0 && mtorem == 0 && mtotdc == 0 && mtochqcert == 0)
        {
        	
            rape.setTranAmt(new Long(mtocoi+mtoefe).toString());
            rape.setMoAmoun(new Long(mtocoi).toString());
            rape.setCashIn(new Long(mtoefe).toString());
            
            //agregan    
            rape.setDraftAm("000");
            String corte = (String)info.get("rdoACorte");
            corte = corte.substring(1);
            rape.setTrNo(corte);
           
            
        }

        //CASO SOLO COI
        else if(mtocoi > 0 && mtoefe == 0 && mtocta == 0 && mtochq == 0 && mtorem == 0 && mtotdc == 0 && mtochqcert == 0)
        {
        	
            rape.setTranAmt(monto);
            rape.setMoAmoun(monto);
            rape.setCashIn("000");
            rape.setDraftAm("000");
            String corte = (String)info.get("rdoACorte");
            corte = corte.substring(1);
            rape.setTrNo(corte);
            
        }
        
        // CASO SOLO EFECTIVO
        else if(mtoefe > 0 && mtocta == 0 && mtochq == 0 && mtorem == 0 && mtocoi == 0 && mtotdc == 0 && mtochqcert == 0) 
        {
        	
            rape.setTranAmt(monto);
            rape.setCashIn(monto);
            EfecTotalCalc = EfecTotalCalc + Long.parseLong(monto);
            info.put("EfecTotalCalc", String.valueOf(EfecTotalCalc));
        }
        
        // CASO EFECTIVO Y CTA Y CHQ O CHQCERT
        else if(mtoefe > 0 && mtocta >= 0 && mtochq >= 0 && mtochqcert >= 0&& mtorem == 0 && mtocoi == 0 && mtotdc == 0)
        {
        	
            if (mtotem > Long.parseLong(monto)) 
            {
            	
                rape.setCashIn(monto);
                rape.setTranAmt(monto);
                mtotem = mtotem - Long.parseLong(monto);
                if (mtochq > 0 && mtocta == 0)
                {
                	
                	info.put("mtoRAP",Long.toString(mtotem));
                	info.put("Cheque."+(String)sesion.getAttribute("identidadApp"),"1");
                }
            }
            else if(mtotem == 0)
            {
            	
                rape.setCashIn("000");
                rape.setTranAmt(monto);
            }
            else // (mtotmp < Long.parseLong(monto))
            {
            	
                rape.setCashIn(new Long(mtotem).toString());
                rape.setTranAmt(monto);
                mtotem = 0;
                info.put("txtEfectivo", "0.00");
            }
        }
        // CASO CHQ Y / O CTA
        else if(mtochq >= 0 && mtocta >= 0 && mtoefe == 0 && mtorem == 0 && mtocoi == 0 && mtotdc == 0 && mtochqcert == 0) 
        {
        	
            rape.setTranAmt(monto);
            rape.setCashIn("000");
        }// CASO CHQ CERT Y / O CTA
        else if(mtochqcert >= 0 && mtocta >= 0 && mtoefe == 0 && mtorem == 0 && mtocoi == 0 && mtotdc == 0 && mtochq == 0) 
        {
        	
            rape.setTranAmt(monto);
            rape.setCashIn("000");
        }        
        else if(mtotdc > 0 && mtocoi == 0 && mtoefe == 0 && mtocta == 0 && mtochq == 0 && mtorem == 0 && mtochqcert == 0)//tdc
        {
        	
        	rape.setTranAmt(monto);
            rape.setCashIn("000");
            rape.setCardType("TC");
        }        
        else 
        {
        	
            System.out.println("No entra a ningun caso de combinacion de Pagos");
        }
		if(servicio.equals("4254")){
			
			rape.setTrNo("01");
		}
        if(servicio.equals("480") || servicio.equals("448") || servicio.equals("297") || servicio.equals("66")) 
        {
        	
            String RFC = (String)info.get("ref1s"+Serv+"p"+Pago);
            
            if(RFC != null) 
            {
            	
                RFC = RFC.trim();
                int large = RFC.length();
                if (large == 10) {
                	
                    ref1 = RFC.substring(0,4);
                    ref2 = RFC.substring(4,RFC.length());
                    ref3 = "000";
                }
                else if (large == 13) {
                	
                    ref1 = RFC.substring(0,4);
                    ref2 = RFC.substring(4,10);
                    ref3 = RFC.substring(10,RFC.length());
                }
                else {
                	
                    ref1 = RFC.substring(0,3);
                    ref2 = RFC.substring(3,9);
                    ref3 = RFC.substring(9,RFC.length());
                }
                rape.setReferenc1(ref1);
                rape.setReferenc2(ref2);
                rape.setReferenc3(ref3);
            }
        }
        else 
        {
        	
            ref1 = (String)info.get("ref1s"+Serv+"p"+Pago);
            if(ref1 != null){
            	rape.setReferenc1(ref1);
            }
            else{
            	
                ref1 = "";
            }
            ref2 = (String)info.get("ref2s"+Serv+"p"+Pago);
            if(ref2 != null){
            	
                rape.setReferenc2(ref2);
            }
            else{
            	
                ref2 = "";
            }
            ref3 = (String)info.get("ref3s"+Serv+"p"+Pago);
            if(ref3 != null){
            	
                rape.setReferenc3(ref3);
            }
            else{
            	
                ref3 = "";
            }
        }

        refs = ref1 + "&" + ref2 + "&" + ref3 + "&";

        moneda = (String)info.get("moneda");
        
        if(moneda.equals("01"))
            rape.setFromCurr("N$");
        else if(moneda.equals("02")) 
            rape.setFromCurr("US$");
        else 
            rape.setFromCurr("UDI");
        
        
        Diario diario = new Diario();
        String sMessage = diario.invokeDiario(rape, conliga, dirIP, moneda, sesion);
        
        

        String respuesta = sMessage;

        String codigo = respuesta.substring(0,1);

        String ConsecI = "0";
        String ConsecF = "0";
        String ConsecREM = "0";
        
        if(codigo.equals("0") && mtorem > 0)
        {
        	
            ConsecREM = sMessage.substring(5,12);
            sesion.setAttribute("ConsecREM",ConsecREM);
            sesion.setAttribute("ServREM",servicio);
        }

        if(codigo.equals("0") && Pago.equals("1"))
        {
        	
            if(respuesta.length() > 234)
            {
            	beneficiario = respuesta.substring(235,respuesta.length()-1).trim();
                int y = 0;
                int count = 0;
                StringBuffer Ben = new StringBuffer(beneficiario);
                for(y = 0; y < Ben.length(); y++)
                	if(Ben.charAt(y) == 0x26) 
                    {
                    	
                        count ++;
                        Ben.replace(y,y+1,"%26");
                    }
                beneficiario = Ben.toString();
                if(beneficiario.trim().length() > 0){
                	
                	info.put("beneficiario"+Serv,beneficiario);
                }
                else{
                	
                	if(info.get("nameServ" + Serv) != null && !info.get("nameServ" + Serv).toString().equals("S/NOMBRE")){
                		info.put("beneficiario"+Serv,(String)info.get("nameServ" + Serv));
                		
                	}
                	else{
                		
                		info.put("beneficiario"+Serv,"");
                	}
                }
            }
            else{
            	
            	if(info.get("nameServ" + Serv) != null && !info.get("nameServ" + Serv).toString().equals("S/NOMBRE")){
            		info.put("beneficiario"+Serv,(String)info.get("nameServ" + Serv));
            	}
            	else{
            		
            		beneficiario = "";
            	}
            }

            if(tipo.equals("2")){
            	
                info.put("comppago"+Serv,(String)info.get("comppago"+Serv));
            }
        }

        if(codigo.equals("0") && Serv.equals("1") && Pago.equals("1"))
        {
        
            ConsecI = sMessage.substring(5,12);
            sesion.setAttribute("ConsecI",ConsecI);
        }

        if(codigo.equals("0") && Serv.equals("1") && !Pago.equals("1"))
        {
        	
            ConsecF = sMessage.substring(5,12);
            
        }
        sesion.setAttribute("ConsecF",ConsecF);

        String amount = rape.getTranAmt().toString().substring(0,rape.getTranAmt().length() - 1);
        String efectivo = rape.getCashIn().toString().substring(0,rape.getCashIn().length() - 1);
                
        if(efectivo.length() == 0){
            efectivo = "000";
        }
        String teller = (String)info.get("teller");
                
        certif(respuesta, teller, cuenta, amount, efectivo, Txn, moneda, ConsecRAP, refs, codigo);
        return codigo;
    }

    private void clearcertif(String cajero)
    {
        String statementPS = ConnectionPoolingManager.getStatementPostFix();
        Connection pooledConnection = ConnectionPoolingManager.getPooledConnection();
        PreparedStatement delete = null;
        try
        {
            if(pooledConnection != null)
            {
            	delete = pooledConnection.prepareStatement("DELETE FROM TW_CERTIFICACIONES WHERE D_CAJERO=?" + statementPS);
                delete.setString(1,cajero);
                int i = delete.executeUpdate();
            }
        }
        catch(SQLException sqlException)
            {System.out.println("Error GroupRAPCAPR::clearcertif [" + sqlException + "]");}
        finally
        {
            try
            {
                if(delete != null)
                {
                	
                    delete.close();
                    delete = null;
                }
                if(pooledConnection != null)
                {
                	
                    pooledConnection.close();
                    pooledConnection = null;
                }
            }
            catch(java.sql.SQLException sqlException){System.out.println("Error FINALLY GroupRAPCAPR::clearcertif [" + sqlException + "]");}
        }
    }

    private long getpago(Hashtable data, String pago)
    {
        String monto = new String();
        long mto = 0;
        
        if(pago.equals("efe"))
        {

            monto = (String)data.get("txtEfectivo");
            if(monto != null)
            {

                monto = clearmto(monto);
                mto = Long.parseLong(monto);
            }
        }

        if(pago.equals("chq"))
        {

            monto = (String)data.get("txtCheque");
            if(monto != null)
            {
            	monto = clearmto(monto);
                mto = Long.parseLong(monto);
            }
        }
        
        if(pago.equals("cert")) //Se agrega para proyecto de Cheque Certificado
        {
        	
            monto = (String)data.get("txtCHQCert");
            if(monto != null)
            {
            	
                monto = clearmto(monto);
                mto = Long.parseLong(monto);
            }
        }
        
        if(pago.equals("cta"))
        {

            monto = (String)data.get("txtCuenta");
            if(monto != null)
            {

                monto = clearmto(monto);
                mto = Long.parseLong(monto);
            }   
        }
        
        if(pago.equals("coi"))
        {
            monto = (String)data.get("txtDocsCI");
            if(monto != null)
            {

                monto = clearmto(monto);
                mto = Long.parseLong(monto);
                if(mto > 0){
                    data.put("CIrap", "1");
                }
            }
        }

        if(pago.equals("rem"))
        {

            monto = (String)data.get("txtRemesas");
            if(monto != null)
            {
 
                monto = clearmto(monto);
                mto = Long.parseLong(monto);
                if(mto > 0){
                    data.put("REMrap", "1");
                }
            }
        }
        
        if(pago.equals("tdc"))
        {

            monto = (String)data.get("txtTDC");
            if(monto != null)
            {

                monto = clearmto(monto);
                mto = Long.parseLong(monto);
                if(mto > 0){
                    data.put("TDCrap", "1");
                }
            }
        }

        return mto;
    }

    private String clearmto(String mto)
    {
        StringBuffer montotmp = new StringBuffer(mto);
        for(int k=0; k<montotmp.length();) {
            if(montotmp.charAt(k) == ',' || montotmp.charAt(k) == '.')
                montotmp.deleteCharAt(k);
            else
                ++k;
        }
        mto = montotmp.toString();

        return mto;
    }
    
    private String getfPago (Hashtable info){
    	String fpago = "";
		// OBTENCION DE MONTOS DE PAGO
		long mtoefe = 0;
		long mtorem = 0;
		long mtochq = 0;
		long mtocoi = 0;
		long mtocert = 0;
		long mtotdc = 0;
        
		mtoefe = getpago(info, "efe");
		mtorem = getpago(info, "rem");
		mtochq = getpago(info, "chq");
		mtocert = getpago(info, "cert");
		mtocoi = getpago(info, "coi");
		mtotdc = getpago(info, "tdc");
		
				
		
		if(mtoefe != 0)
			fpago = "efe";
		else if(mtorem != 0)
			fpago = "rem";
		else if(mtochq != 0)
			fpago = "chq";
		else if(mtocoi != 0)
			fpago = "coi";
		else if(mtocert != 0)
			fpago = "cert";
		else if(mtotdc != 0)
			fpago = "tdc";
		
		
		return fpago;
    }
    
    private int getPago(Hashtable data)
    {
        int tipo = 0;
       
        if((String)data.get("txtEfectivo") != null && !((String)data.get("txtEfectivo")).equals("0.00")){
        	tipo = 0;
        }else if((String)data.get("txtCheque") != null && !((String)data.get("txtCheque")).equals("0.00")){
        	tipo = 1;
        }else if((String)data.get("txtCHQCert") != null && !((String)data.get("txtCHQCert")).equals("0.00")){
        	tipo = 1;
        }else if((String)data.get("txtDocsCI") != null && !((String)data.get("txtDocsCI")).equals("0.00")){
        	tipo = 2;
        }        
        
        return tipo;
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        doPost(request, response);
    }
}

