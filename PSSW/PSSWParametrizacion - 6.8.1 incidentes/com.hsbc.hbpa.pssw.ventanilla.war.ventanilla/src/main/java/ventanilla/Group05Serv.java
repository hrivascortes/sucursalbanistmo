//*************************************************************************************************
//             Funcion: Clase que realiza txn Servicios
//            Elemento: Group05Serv.java
//          Creado por: Alejandro Gonzalez Castro
//      Modificado por: Fausto Rodrigo Flores Moreno
//*************************************************************************************************
// CCN - 4360189 - 03/09/2004 - Se cambia invoke al Diario para Control de Efectivo
// CCN - 4360312 - 26/04/2005 - Se eliminan referencias a opcion 2 de ATM
// CCN - 4360508 - 08/09/2006 - Se realiza modifiación para que viaje supervisor en txn 0492
// CCN - 4360567 - 02/02/2007 - Se realiza modifiación para no permitir que se generen ivas duplicados
//*************************************************************************************************
package ventanilla;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.util.Hashtable;

import ventanilla.com.bital.sfb.ServiciosB;
import ventanilla.com.bital.admin.Diario;
import ventanilla.com.bital.util.VolanteoDDA;
import java.util.Stack;

public class Group05Serv extends HttpServlet
{
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        
        GenericClasses GC = new GenericClasses();
        HttpSession session = request.getSession(false);
        Hashtable datasession = (Hashtable)session.getAttribute("page.datasession");
        String txn = (String)datasession.get("cTxn");
        String moneda = GC.getDivisa((String)datasession.get("moneda"));
        String[] invoqued = request.getParameterValues("txtInvoqued");
        String invTxn = "";
        String txtCasoEsp = "";
                
        if(invoqued != null)
            invTxn = (String)invoqued[0];

        String dirip =  (String)session.getAttribute("dirip");
        String msg = "";
        Hashtable Truncamiento = (Hashtable)session.getAttribute("truncamiento");
        long TruncamientoMN = 500000L;
        long TruncamientoUSD = 50000L;
        if (!Truncamiento.isEmpty() ){
           TruncamientoMN = Long.parseLong(Truncamiento.get("TruncamientoMN").toString());
           TruncamientoUSD = Long.parseLong(Truncamiento.get("TruncamientoUSD").toString());
        }        
        VolanteoDDA oVolanteoDDA = null;
        String txtVolante = "NO";

        ServiciosB serviciob = new ServiciosB();
        serviciob.setBranch((String)datasession.get("sucursal"));
        serviciob.setTeller((String)datasession.get("teller"));
        serviciob.setSupervisor((String)datasession.get("teller"));

        //agregar supervisor en trace 11-08-2006
        if ((String)datasession.get("supervisor") != null )
            serviciob.setSupervisor((String)datasession.get("supervisor"));

        serviciob.setFormat("B");
        String sucursal = (String)session.getAttribute("branch");
        String Monto = "";
        
        if( txn.equals("0829") ) {
            String plaza =  (String)session.getAttribute("plaza");
            serviciob.setTxnCode(txn);
            serviciob.setAcctNo(plaza);
            Monto = GC.quitap((String)datasession.get("txtMonto"));
            serviciob.setTranAmt(Monto);
            serviciob.setReferenc("DT");
            serviciob.setFromCurr(moneda);
        }

        if( txn.equals("0474") || txn.equals("0476") || txn.equals("0482") /*|| txn.equals("0492")*/)
        {
            serviciob.setTxnCode(txn);
            serviciob.setAcctNo(txn);
            Monto = GC.quitap((String)datasession.get("txtMonto"));
            serviciob.setTranAmt(Monto);
            
            String refer1 = "";
            String refer2 = "";
            if (txn.equals("0474")) {
                refer1 = (String)datasession.get("txtRefer10630");
                refer2 = (String)datasession.get("txtRefer20630");
                serviciob.setReferenc(refer1 + refer2);
            }
            /*else if (txn.equals("0492")) {
                refer1 = (String)datasession.get("txtRefer10630");
                serviciob.setReferenc(sucursal + setzeros(refer1));
            }*/
            else
            { serviciob.setReferenc((String)datasession.get("txtRefer10630")); }
            
            serviciob.setIntWh("");
            serviciob.setCashOut(Monto);
            serviciob.setFromCurr(moneda);
        }
        
        if( txn.equals("0332") ) {
            serviciob.setTxnCode(txn);
            serviciob.setAcctNo(txn);
            Monto = GC.quitap((String)datasession.get("txtMontoNoCero"));
            serviciob.setTranAmt(Monto);
            String regist = (String)session.getAttribute("empno");
            String respon = (String)datasession.get("txtRegResp");
            String causas = (String)datasession.get("lstReferenc");
            serviciob.setReferenc(regist + respon + causas);
            serviciob.setCashOut(Monto);
            serviciob.setFromCurr(moneda);
        }

        Diario diario = new Diario();
        String sMessage = diario.invokeDiario(serviciob, (String)session.getAttribute("d_ConsecLiga"), dirip, moneda, session);

        if (txn.equals("0476") && datasession.get("txtComision") != null){
        	if(Double.parseDouble((String)datasession.get("txtComision"))>0){
			   txtCasoEsp = txn + "~"+ GC.getString(sMessage,1) + "~";
   		       sMessage = ComIVA(datasession, "0764","COM POR DISP EN EFEC", dirip, (String)session.getAttribute("d_ConsecLiga"), session);
   	    	   txtCasoEsp = txtCasoEsp + "0764"+ "~"+ GC.getString(sMessage,1) + "~";      		    
	           sMessage = ComIVA(datasession, "0372","IVA", dirip, (String)session.getAttribute("d_ConsecLiga"), session);
		       txtCasoEsp = txtCasoEsp + "0372" + "~"+ GC.getString(sMessage,1) + "~";
        	   session.setAttribute("txtCasoEsp", txtCasoEsp);              
	           datasession.put("cTxn","0372");
    	       Stack flujotxn = (Stack)session.getAttribute("page.flujotxn");
        	   if (!flujotxn.empty()){
    	    	   flujotxn.removeAllElements();
	               session.setAttribute("page.flujotxn", flujotxn);
    	       }
        	}
        }else if(txn.equals("0476") && sMessage.substring(0,1).equals("0")){
           Stack flujotxn = (Stack)session.getAttribute("page.flujotxn");
           if (!flujotxn.empty()){
		      flujotxn.removeAllElements();
           	  session.setAttribute("page.flujotxn", flujotxn);
           }
		}
        
        session.setAttribute("page.txnresponse", sMessage);
        getServletContext().getRequestDispatcher("/servlet/ventanilla.Redirector").forward(request,response);
        
    }
    
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
    
    public String removeAsterisk(String campo) {
        StringBuffer result = new StringBuffer(campo);
        String caracter = result.substring(result.length()-1,result.length());
        if (caracter.equals("*"))
            result.deleteCharAt(result.length()-1);
        return result.toString();
    }   
    
    private String setzeros(String campo)
    {
      while(campo.length() < 10)
          campo = "0" + campo;
      return campo;
    }
  
    private String ComIVA(Hashtable datos, String Trxn, String serv, String dir, String Conliga, HttpSession ses)
    {

        GenericClasses gc = new GenericClasses();
        ServiciosB Serv = new ServiciosB();      
        Serv.setFormat("B");
        Serv.setBranch((String)datos.get("sucursal"));
        Serv.setTeller((String)datos.get("teller"));
        Serv.setSupervisor((String)datos.get("teller"));
        Serv.setTxnCode(Trxn);
        Serv.setAcctNo(Trxn);
        String moneda = (String)datos.get("moneda");
        String monto = "";

        if(Trxn.equals("0764"))
            monto = gc.quitap((String)datos.get("txtComision"));
        if(Trxn.equals("0372"))
            monto = gc.quitap((String)datos.get("montoIVA"));
			
        Serv.setTranAmt(monto);
        Serv.setCashIn(monto);
        Serv.setReferenc(serv);
        Serv.setIntWh("0");
	    Serv.setFromCurr("N$");

        Diario diario = new Diario();
/*        try{Thread.sleep( 1500 );}
	      catch (InterruptedException e){}*/
        String sMessage = diario.invokeDiario(Serv, Conliga, dir, moneda, ses);
	
        return sMessage;
    }  
}
