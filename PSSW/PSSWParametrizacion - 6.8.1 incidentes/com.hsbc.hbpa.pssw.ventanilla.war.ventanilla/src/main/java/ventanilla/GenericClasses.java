//*************************************************************************************************
//             Funcion: Metodos Genericos
//            Elemento: GenericClasses.java
//          Creado por: Alejandro Gonzalez Castro
//		Modificado por: Fredy Pe�a Moreno
//*************************************************************************************************
// CCN - 4360189 - 03/09/2004 - Se agrega metodo stringFormat()
// CCN - 4360253 - 07/01/2005 - Se agregan metodos addpunto(), FillerString() y ClearString()
// CCN - 4360268 - 04/02/2005 - Se agregan metodos StringFiller(), getPositionalString() y ValEffDate()
// CCN - 4360291 - 18/03/2005 - Se agregan opciones al metodo getDate() 
// CCN - 4360296 - 23/03/2005 - Se cambia CCN por peticion de QA, CCN anterior 4360291
// CCN - 4360315 - 06/05/2005 - Se corrige el envio de milisegundos
// CCN - 4360334 - 17/06/2005 - Se agrega metodo delCerosBinarios para WAS5
// CCN - 4360402 - 11/11/2005 - Se agregan nuevos metodos
// CCN - 4360533 - 20/10/2006 - Se realizan modificaciones para OPEE
// CCN - 4360567 - 02/02/2007 - Se agrega funci�n modulo10 para correcci�n de txn 0730
// CCN - 4360589 - 16/04/2007 - Se realizan modificaciones para el monitoreo de efectivo en las sucursales
// CCN - 4360619 - 15/06/2007 - Se eliminan systems
// CCN - 4620006 - 30/08/2007 - Se realiza modificaci�n para multimoneda Panam�
//*************************************************************************************************

package ventanilla;
import java.util.Vector;
import java.text.DecimalFormat;
import java.lang.reflect.Method;

public class GenericClasses{

    public GenericClasses(){
    }
    
    public String quitap(String Valor) {
        StringBuffer Cantidad = new StringBuffer(Valor);
        for(int i=0; i<Cantidad.length();) {
            if( Cantidad.charAt(i) == ',' || Cantidad.charAt(i) == '.' )
                Cantidad.deleteCharAt(i);
            else
                ++i;
        }
        return Cantidad.toString();
    }
    
    public String delLeadingfZeroes(String newString){
    	StringBuffer newsb = new StringBuffer(newString);   
      
       	int i = 0;
       	while((newsb.length()>0) && (newsb.charAt(i) == '0'))
    	   newsb.deleteCharAt(0);
   	
   		if(newsb.length()==0)
   			return "0";
   		else
   			return newsb.toString();
    }
    
    public String delCommaPointFromString(String newCadNum) {
        String nCad = new String(newCadNum);
        if( nCad.indexOf(".") > -1) {
            nCad = nCad.substring(nCad.indexOf(".") + 1, nCad.length());
            if(nCad.length() != 2) {
                String szTemp = new String("");
                for(int j = nCad.length(); j < 2; j++)
                    szTemp = szTemp + "0";
                newCadNum = newCadNum + szTemp;
            }
        }
        StringBuffer nCadNum = new StringBuffer(newCadNum);
        for(int i = 0; i < nCadNum.length(); i++)
            if(nCadNum.charAt(i) == ',' || nCadNum.charAt(i) == '.'){
                nCadNum.deleteCharAt(i);
            }
        return nCadNum.toString();
    }
    
    public String delCommaFromString(String newCadNum) {
		StringBuffer nCadNum = new StringBuffer(newCadNum);
		for(int i = 0; i < nCadNum.length(); i++)
			if(nCadNum.charAt(i) == ','){
				nCadNum.deleteCharAt(i);
			}
		return nCadNum.toString();
	}
    
    public String getString(String newCadNum, int newOption) {
        int iPIndex = newCadNum.indexOf("CONSECUTIVO:") + 12, NumSaltos, Num = 0, Ban = 0;
        String nCadNum = new String("");
        NumSaltos = newOption;
        while(Num < NumSaltos) {
            if(newCadNum.charAt(iPIndex) == 0x20) {
                while(newCadNum.charAt(iPIndex) == 0x20)
                    iPIndex++;
                Num++;
            }
            else
                while(newCadNum.charAt(iPIndex) != 0x20)
                    iPIndex++;
        }
        while(newCadNum.charAt(iPIndex) != 0x20 && iPIndex < newCadNum.length()){
            nCadNum = nCadNum + newCadNum.charAt(iPIndex);
            if(iPIndex < newCadNum.length()){
                iPIndex++;
            }
            if(iPIndex == newCadNum.length()){
                break;
            }
        }
        
        return nCadNum;
    }
    
    public String formatMonto(String s) {
        int len = s.length();
        if (len < 3)
            s = "0." + s;
        else {
            int n = (len - 3 ) / 3;
            s = s.substring(0, len - 2) + "." + s.substring(len - 2 , len);
            for (int i = 0; i < n; i++) {
                len = s.length();
                s = s.substring(0, (len -(i*3+6+i))) + "," + s.substring((len -(i*3+6+i)) , len);
            }
        }
        
        return s;
    }
    
    public String getDate(int formato) 
    {
        java.util.Date fecha = new java.util.Date();
        java.text.SimpleDateFormat formatof = null;
        switch (formato) 
        {
            case 1:
                formatof = new java.text.SimpleDateFormat("yyyyMMdd");
                break;
            case 2:
                formatof = new java.text.SimpleDateFormat("MMdd");
                break;
            case 3:
                formatof = new java.text.SimpleDateFormat("HHmmss");
                break;
			case 4:
				formatof = new java.text.SimpleDateFormat("yyMMdd");
				break;
			case 5:
				formatof = new java.text.SimpleDateFormat("HHmm");
				break;
			case 6:
				formatof = new java.text.SimpleDateFormat("dd");
				break;
			case 7:
				formatof = new java.text.SimpleDateFormat("HHmmssSSS");
				break;
			case 8:	
				formatof = new java.text.SimpleDateFormat("ddMMyyyy");
				break;    
        }
        String fechacom = formatof.format(fecha);
        return fechacom;
    }

    public String getDivisa(String newDivisa)
    {
		ventanilla.com.bital.beans.MonedasInt div = ventanilla.com.bital.beans.MonedasInt.getInstance();
		String divisa = "";
		java.util.Vector vDiv = div.getInfoMoneda(newDivisa);
		divisa = (String)vDiv.get(1);
		return divisa;
    }
    
    public String stringFormat(int option, int NumDig) 
    {
        java.util.Calendar now = java.util.Calendar.getInstance();
        String temp = new Integer(now.get(option)).toString();
        for(int i = temp.length(); i < NumDig; i++)
            temp = "0" + temp;
        return temp;
    }
    
    public String addpunto(String valor) 
    {
        int longini = valor.length();
        String cents  = valor.substring(longini-2,longini);
        String entero = valor.substring(0,longini-2);
        int longente = entero.length();
        for (int i = 0; i < (longente-(1+i))/3; i++) {
            longente = entero.length();
            entero = entero.substring(0,longente-(4*i+3))+','+entero.substring(longente-(4*i+3));
        }
        entero = entero + '.' + cents;
        return entero;
    }

    public String ClearString(String cadena)
    {
        int y = 0;
        StringBuffer cadenaSB = new StringBuffer(cadena);
        for(y = 0; y < cadenaSB.length(); y++)
        if(cadenaSB.charAt(y) == '\'')
        {
            cadenaSB.replace(y,y+1," ");
        }
        cadena = cadenaSB.toString();
        return cadena;
    }
    
	public String StringFiller(String newString, int longitud, boolean side, String Filler) 
	{
		int CadLen = newString.length();
		int i=0;
		if(side)
			for(i = CadLen; i < longitud; i++)
				newString = newString + Filler;
		else
			for(i = CadLen; i < longitud; i++)
				newString = Filler + newString;
		return newString;
	}
    
    
    public String cutString(String cadena, int longitud, boolean side){    	
    	String newCad = "";
    	
    	if(side)//lado izquierdo
    		newCad = cadena.substring(longitud,cadena.length());	
    	else
    		newCad = cadena.substring(0,(cadena.length()-longitud));
    	
    	return newCad;
    }
    
	public String getPositionalString(String inputString, int initialPosition, int maxNumberOfBytes, String withSpaces){
		String outputString = new String("");
		int i = 0;
		if(maxNumberOfBytes == 0){
			maxNumberOfBytes = inputString.length();
		}
		if(withSpaces.equals("YES"))
			while(initialPosition < inputString.length() && inputString.charAt(initialPosition) != 0x20 && i++ < maxNumberOfBytes)
				outputString += inputString.charAt(initialPosition++);
		else
			while(initialPosition < inputString.length() && i++ < maxNumberOfBytes && inputString.charAt(initialPosition) != '~')
				outputString += inputString.charAt(initialPosition++);
		
		if(outputString.length() == 0)
			outputString = new String("000");
		return outputString;
	}
	
	public String ValEffDate(String newCadNum, String newCadNum2) {
		String nCad = new String(newCadNum);
		String nCad2 = new String(newCadNum2);
		nCad = nCad.substring(6,10) + nCad.substring(3,5) +  nCad.substring(0,2);

		long Fecha1 = Long.parseLong(nCad);
		long Fecha2 = Long.parseLong(nCad2);

		if ( Fecha1 != Fecha2 )
			nCad  = newCadNum.substring(3,5) + newCadNum.substring(0,2)+ newCadNum.substring(8,10); //Fecha en que se Opera la Txn.
		else
			nCad = "";

		return nCad;
	}
	
	public String delCerosBinarios(String newStr) 
	{
		//	Limpia Ceros Binarios de la Respuesta de Hogan
		StringBuffer tmp = new StringBuffer(newStr);
		for(int y = 0; y < tmp.length(); y++)
			if(tmp.charAt(y) == 0x00){
				tmp.replace(y,y+1," ");
			}
		newStr = tmp.toString();
		return newStr;
	} 

	public String EmptyString(String newStr) 
	{
		if(newStr.length() == 0){
			newStr = "NA";
		}
		return newStr;
	}
	
    public String FormatDate(String fecha)
    {
        fecha = fecha.substring(6) + "-" + fecha.substring(4,6) + "-" + fecha.substring(0,4);
        return fecha;
    }	

    public String FormatHour (String hora)
    {
    	String Hour = "";
    	Hour = hora.substring(0,2) + ":" + hora.substring(2,4) + ":" + hora.substring(4,6) + "." + hora.substring(6);
    	return Hour;
    }

    public String FormatAmount(String monto)
    {
        double number = Double.parseDouble(monto)/100.00;
        DecimalFormat DecFormat = new DecimalFormat("###,##0.00");
        String FormatMto = DecFormat.format(number);
        return FormatMto;
    }
    
	public String getBeanData(Object BeanData, String metodo) {
        Object Valor = null;
        try {
            // obtiene la clase
            Class Bean = BeanData.getClass();
            // Tipo de los argumentos que recibe el metodo en un arreglo de clases
            Class[] TipoArgs = new Class[] {};
            // obtiene el metodo del Bean
            Method Metodo = null;
            Metodo = Bean.getMethod(metodo, TipoArgs);
            // argumentos de la clase en un arreglo de Objects
            Object[] Args = new Object[] {};
            // Invocacion dinamica
            Valor = Metodo.invoke(BeanData, Args);
            if(Valor==null){
                Valor = "";
            }
            if(!metodo.equals("toString"))
            {
                if(Valor.toString().endsWith("*")){
                    Valor = Valor.toString().substring(0,Valor.toString().length()-1);
                }
            }
            else if(Valor.toString().startsWith("DSLZ")){
                Valor = Valor.toString().trim();
            }    
        }
        catch( Exception e ) {
            System.out.println("Error GenericClasses::getBeanData [" + e.getMessage() + "]");
            e.printStackTrace();
            return null;
        }
        return Valor.toString();
    }    

	public String getFormatoMoneda(String moneda){
		return moneda;
	}		
	
	public String modulo10(String campos) {
		int total = 0;
		int valor = 0;
		int multi = 0;
		int factor = 0;
		int ctrl = 2;
		for (int i=campos.length()-1; i>=0; i--) {
			Character c = new Character(campos.charAt(i));
			valor = Integer.parseInt(c.toString());
			multi = ctrl % 2;
			if (multi == 0)
			{factor = 2;}
			else
			{factor = 1;}
			valor = valor * factor;
			if (valor == 10) {valor = 1;}
			if (valor > 10) {
				String DIG = new String(Integer.toString(valor));
				int dig = Integer.parseInt(DIG.substring(1));
				dig = dig + Integer.parseInt(DIG.substring(0,1));
				valor = dig;
			}
			total = total + valor;
			ctrl++;
		}
		String cadena = new String(Integer.toString(total));
		int digito = Integer.parseInt(cadena.substring(cadena.length()-1,cadena.length()));
		digito = 10 - digito;
		String digver = new String(Integer.toString(digito));
		return digver;
	}	
	
	public String rellenaZeros(String valor, int longitud){
		String cadena = null;
		cadena=valor;
		if(valor.length() < longitud)
		{
			int i=0;
			for(i=0;i<longitud-valor.length();i++)
			{
				cadena="0"+cadena;
			}	
		}
		return cadena;
	}
	
	public boolean validateModulo10(String cta)
	{
		int sum = 0;
		int temp = 0;
		if(cta.length()==11){
			cta=cta.substring(1,cta.length());
		}
		for(int i=2; i<cta.length()-1; i++)
		{
			if( i % 2 != 0 )
				sum += Integer.parseInt(cta.substring(i,i+1));
			else
			{
				temp = Integer.parseInt(cta.substring(i,i+1)) * 2 ;
				if( temp < 10 )
					sum += temp ;
				else
					sum = sum + (int)Math.floor(temp / 10) + (temp % 10) ;
			}
		}

		if( (sum % 10 == 0 ) && (Integer.parseInt(cta.substring(cta.length()-1,cta.length())) == 0) ){
			return true ;
		}			
		if( (10-(sum % 10)) == Integer.parseInt(cta.substring(cta.length()-1,cta.length())) )
			return true ;
		else 
		{
			return false;
		}
	}
		public String getFormaPago(String tipo, int forma){
		String fPago = "";
		
		if(tipo.equals("pd") && forma == 0) // efectivo
			fPago = "30";
		if(tipo.equals("pd") && forma == 1) //cheques cmtH
			fPago = "31";
		if(tipo.equals("pd") && forma == 2) //CI
			fPago = "32";
		if(tipo.equals("pa") && forma == 0) // efectivo
			fPago = "33";
		if(tipo.equals("pa") && forma == 1) //cheques cmtH
			fPago = "34";
		if(tipo.equals("pa") && forma == 2) //CI
			fPago = "35";
		/*
		30	PAGO EN EFECTIVO PSSW  
		31	PAGO CHEQUE cmtH PSSW  
		32	PAGO CHEQUE OTROBCO PSSW  
		33	PAGO ADELANTADO EN EFECTIVO PSSW  
		34	PAGO ADELANTADO CHEQUE cmtH PSSW  
		35	PAGO ADELANTADO CHEQUE OTROS BANCOS PSSW 
		*/
		return fPago; 
	}
	
	public String getFormaPago(int forma){
		String fPago = "";
	    ventanilla.com.bital.beans.DatosVarios insIdentidad = ventanilla.com.bital.beans.DatosVarios.getInstance();                   


		
		switch(forma){
			case 30: fPago = "Pago Directo en Efectivo"; break;
			case 31: fPago = "Pago Directo con Cheque "+(String)insIdentidad.getDatosv("IDENTIDAD"); break;
			case 32: fPago = "Pago Directo con Cheque Otro Banco"; break;
			case 33: fPago = "Pago Adelantado en Efectivo"; break;
			case 34: fPago = "Pago Adelantado con Cheque "+(String)insIdentidad.getDatosv("IDENTIDAD"); break;
			case 35: fPago = "Pago Adelantado con Cheque Otro Banco"; break;			
		}
		
		/*
		30	PAGO EN EFECTIVO PSSW  
		31	PAGO CHEQUE cmtH PSSW  
		32	PAGO CHEQUE OTROBCO PSSW  
		33	PAGO ADELANTADO EN EFECTIVO PSSW  
		34	PAGO ADELANTADO CHEQUE cmtH PSSW  
		35	PAGO ADELANTADO CHEQUE OTROS BANCOS PSSW 
		*/
		return fPago; 
	}
	public boolean isHSBC(String serv, String tdc, Vector bines){
		
		boolean tdcHSBC = false;
		if(serv.equals("60")){
			Vector row = null;
			int x = 0;
			boolean find = false;
			while(x<bines.size() && !find)
			{
				row = (Vector) bines.get(x);
				if(tdc.substring(0,6).equals(row.get(0).toString().trim())){
					if(row.get(1).toString().trim().equals("1")){
						tdcHSBC = true;
					}
					find = true;
				}
				x++;
			}
		}else{
			tdcHSBC = false;
		}
		return tdcHSBC;
	}


}
