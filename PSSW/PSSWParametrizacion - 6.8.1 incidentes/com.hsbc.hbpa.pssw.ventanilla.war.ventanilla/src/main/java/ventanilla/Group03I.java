//*************************************************************************************************
//             Funcion: Clase que realiza txn Inversiones
//            Elemento: Group03I.java
//          Creado por: Alejandro Gonzalez Castro
//		Modificado por: Juvenal Fernandez
//*************************************************************************************************
// CCN - 4360189 - 03/09/2004 - Se cambia invoke al Diario para Control de Efectivo
// CCN - 4360268 - 04/02/2005 - Se realizan adecuaciones para eliminar Agencia NY
//*************************************************************************************************
package ventanilla;

import java.io.IOException;
import java.util.Hashtable;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import ventanilla.com.bital.sfb.Inversiones;
import ventanilla.com.bital.admin.Diario;

public class Group03I extends HttpServlet 
{
    public void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException 
    {
        Inversiones oInversiones = new Inversiones();

        resp.setContentType("text/plain");

        HttpSession session = req.getSession(false);
        Hashtable datasession = (Hashtable)session.getAttribute("page.datasession");

        String dirip = (String)session.getAttribute("dirip");

        String getMsg = "";
        String txtTxnCode    = (String)datasession.get("cTxn");
        String txtFormat     = "A";
        String txtBranch     = (String)datasession.get("sucursal");
        String txtTeller     = (String)datasession.get("teller");//"000108";
        String txtSupervisor = (String)datasession.get("teller");//"000101";
        String txtBackOut    = "0";
        String txtOverride   = "0";
        String txtCurrentDay = "0";
        String txtFechEfect  = "";
        String txtDescripcion= "";

        if ((String)datasession.get("supervisor")!= null)
            txtSupervisor = (String)datasession.get("supervisor");

        if ( txtTxnCode.equals("3021"))
            oInversiones.setTranAmt("1");

        if ( (String)datasession.get("override") != null )
            if (datasession.get("override").toString().equals("SI"))
                txtOverride = "3";

        oInversiones.setTxnCode(txtTxnCode);
        oInversiones.setFormat(txtFormat);
        oInversiones.setBranch(txtBranch);
        oInversiones.setTeller(txtTeller);
        oInversiones.setSupervisor(txtSupervisor);
        oInversiones.setBackOut(txtBackOut);
        oInversiones.setOverride(txtOverride);
        oInversiones.setCurrentDay(txtCurrentDay);
        String txtMoneda     = (String)datasession.get("moneda");    //Moneda
        String txtCDACuenta  = (String)datasession.get("txtCDACuenta"); //No. de cuenta
        if(txtMoneda.equals("03"))
            txtMoneda = "01";
        oInversiones.setTranCur(txtMoneda);     //Tipo de Moneda con que se opera la Txn.
        oInversiones.setAcctNo(txtCDACuenta);   //No de Cuenta

        Diario diario = new Diario();
        String sMessage = diario.invokeDiario(oInversiones, (String)session.getAttribute("d_ConsecLiga"), dirip, txtMoneda, session);

        if ( txtTxnCode.equals("3021"))                                             // No certificable
            session.setAttribute("txtCadImpresion", "NOIMPRIMIRNOIMPRIMIR");

        session.setAttribute("page.txnresponse", sMessage);
        getServletContext().getRequestDispatcher("/servlet/ventanilla.Redirector").forward(req,resp);

    }

    public void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doPost(req, resp);
    }
}
