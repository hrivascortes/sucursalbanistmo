//*************************************************************************************************
//             Funcion: Clase que realiza el posteo de las txns- 0810, 0124, 0372 para el cobro de 
//                      comison por impresion de estado de cuenta CDA
//            Elemento: Group18Serv.java
//          Creado por: Juvenal R. Fernandez Varela
//      Modificado por: Juvenal R. Fernandez Varela
//*************************************************************************************************
// CCN - 4360171 - 23/07/2004 - Se crea nueva txn 0124
// CCN - 4360189 - 03/09/2004 - Se cambia invoke al Diario para Control de Efectivo
// CCN - 4360302 - 01/04/2005 - Se valida monto m�nimo de un peso
// CCN - 4360368 - 02/09/2005 - Se elimina sleep
// CCN - 4360374 - 08/09/2005 - Se genera nuevo paquete a peticion de QA.
//*************************************************************************************************

package ventanilla; 

import java.io.IOException;
import java.util.Hashtable;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import ventanilla.com.bital.admin.Diario;
import ventanilla.com.bital.sfb.ServiciosB;

public class Group18Serv extends HttpServlet {
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession(false);
        Hashtable datasession = (Hashtable)session.getAttribute("page.datasession");
		GenericClasses GC = new GenericClasses();
		
        String moneda = GC.getDivisa((String)datasession.get("moneda"));
        String dirip =  (String)session.getAttribute("dirip");
        String msg = "";
        String plaza = (String)session.getAttribute("plaza");
        
        String iTxn = (String)datasession.get("iTxn");
        String oTxn = (String)datasession.get("oTxn");
        String cTxn = (String)datasession.get("cTxn");  
        
        ServiciosB serviciob = new ServiciosB();
        serviciob.setBranch((String)datasession.get("sucursal"));
        serviciob.setTeller((String)datasession.get("teller"));
        serviciob.setSupervisor((String)datasession.get("teller"));
        serviciob.setFormat("B");
         
        Diario diario = new Diario();
        String sMessage = "";
         
        if(cTxn.equals("0732")&& iTxn.equals("0732"))
        {        
            serviciob.setTxnCode("0810");
            serviciob.setAcctNo(plaza);
            serviciob.setTranAmt("000");
            serviciob.setReferenc("1");
            serviciob.setFromCurr(moneda);
                   
            
            sMessage = diario.invokeDiario(serviciob, (String)session.getAttribute("d_ConsecLiga"), dirip, moneda, session);
               
            if (sMessage.startsWith("0"))
            {        
                long iva = Long.parseLong(getItem(sMessage, 4));
                long monto = Long.parseLong(delCommaPointFromString((String)datasession.get("txtMonto0124")));
                long montoIVA = (monto * iva) / 100;
                long montoTotal = montoIVA + monto;
                session.setAttribute("page.MontoIVA",Long.toString(montoIVA));
				String MtoIVA = Long.toString(montoIVA);
				String MtoTotal = Long.toString(montoTotal);
				if(MtoIVA.length()< 3)
					MtoIVA = GC.StringFiller(MtoIVA, 3, false, "0");
				if(MtoTotal.length()< 3)
					MtoTotal = GC.StringFiller(MtoTotal, 3, false, "0");
                datasession.put("txtIVA0124",GC.addpunto(MtoIVA));
                datasession.put("txtMontoT",GC.addpunto(MtoTotal));
                session.setAttribute("page.datasession",datasession);
            }      
            session.setAttribute("page.txnresponse", sMessage);
            getServletContext().getRequestDispatcher("/servlet/ventanilla.Redirector").forward(request,response);
        }
        else if (cTxn.equals("0124")&& iTxn.equals("0732"))
        {
            serviciob.setTxnCode("0124");
            serviciob.setAcctNo("0124");
            serviciob.setTranAmt(delCommaPointFromString((String)datasession.get("txtMonto0124")));
            serviciob.setReferenc((String)datasession.get("txtRefer0124"));
            serviciob.setCashIn(delCommaPointFromString((String)datasession.get("txtMonto0124")));
            serviciob.setFromCurr(moneda);
            
            sMessage = diario.invokeDiario(serviciob, (String)session.getAttribute("d_ConsecLiga"), dirip, moneda, session);
            String txtCasoEsp = "0124" + "~"+ getItem(sMessage,1)+ "~";
            
            /*if (sMessage.startsWith("0"))
            {                    
                serviciob.setTxnCode("0372");
                serviciob.setAcctNo("0372");
                serviciob.setTranAmt(delCommaPointFromString((String)datasession.get("txtIVA0124")));
                serviciob.setReferenc("IVA");
                serviciob.setCashIn(delCommaPointFromString((String)datasession.get("txtIVA0124")));
                serviciob.setFromCurr(moneda);

                sMessage = diario.invokeDiario(serviciob, (String)session.getAttribute("d_ConsecLiga"), dirip, moneda, session);
                txtCasoEsp = txtCasoEsp + "0372" + "~"+ getItem(sMessage,1) + "~";
                session.setAttribute("txtCasoEsp", txtCasoEsp);
            }*/
            
            session.setAttribute("page.txnresponse", sMessage);
            getServletContext().getRequestDispatcher("/servlet/ventanilla.Redirector").forward(request,response);
        }
        
    }
    
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
  
    private String getItem(String newCadNum, int newOption) 
    {
        int iPIndex = newCadNum.indexOf("CONSECUTIVO:") + 12, NumSaltos, Num = 0, Ban = 0;
        String nCadNum = new String("");
        NumSaltos = newOption;
        while(Num < NumSaltos) 
        {
            if(newCadNum.charAt(iPIndex) == 0x20) 
            {
                while(newCadNum.charAt(iPIndex) == 0x20)
                    iPIndex++;
                Num++;
            }
            else
                while(newCadNum.charAt(iPIndex) != 0x20)
                    iPIndex++;
        }
        while(newCadNum.charAt(iPIndex) != 0x20 && iPIndex < newCadNum.length())
        {
            nCadNum = nCadNum + newCadNum.charAt(iPIndex);
            if(iPIndex < newCadNum.length())
                iPIndex++;
            if(iPIndex == newCadNum.length())
                break;
        }
        return nCadNum;
    }
  
      
     private String delCommaPointFromString(String newCadNum) 
     {
        String nCad = new String(newCadNum);
        if( nCad.indexOf(".") > -1) 
        {
            nCad = nCad.substring(nCad.indexOf(".") + 1, nCad.length());
            if(nCad.length() != 2) 
            {
                String szTemp = new String("");
                for(int j = nCad.length(); j < 2; j++)
                    szTemp = szTemp + "0";
                newCadNum = newCadNum + szTemp;
            }
        }
        
        StringBuffer nCadNum = new StringBuffer(newCadNum);
        for(int i = 0; i < nCadNum.length(); i++)
            if(nCadNum.charAt(i) == ',' || nCadNum.charAt(i) == '.')
                nCadNum.deleteCharAt(i);
        
        return nCadNum.toString();
    }
}
