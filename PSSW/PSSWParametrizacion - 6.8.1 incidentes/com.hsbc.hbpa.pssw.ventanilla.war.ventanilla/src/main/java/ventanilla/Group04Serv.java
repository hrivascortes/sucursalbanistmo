//*************************************************************************************************
//             Funcion: Clase para las txns 0730, 0728
//            Elemento: Group04Serv.java
//          Creado por: Alejandro Gonzalez Castro
//      Modificado por: Fausto R. Flores Moreno
//*************************************************************************************************
// CCN - 4360164 - 07/07/2004 - Se controla envio de moneda
// CCN - 4360189 - 03/09/2004 - Se cambia invoke al Diario para Control de Efectivo
// CCN - 4360349 - 22/07/2005 - Se agrega la txn 0832 para el cobro de comisiones
// CCN - 4360364 - 19/08/2005 - Se modifican textos "Comision Cliente/Comision no Cliente" para telmex 
// CCN - 4360425 - 03/02/2006 - Proyecto Tarjeta de Identificacion TDI
// CCN - 4360477 - 19/05/2006 - Se modifica el flujo de la txn 0728 para no duplicar posteo de IVA
// CCN - 4360567 - 02/02/2007 - Se incluye validación de comision mayor a 0
//*************************************************************************************************
package ventanilla;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.util.Hashtable;
import ventanilla.com.bital.sfb.ServiciosA;
import ventanilla.com.bital.sfb.ServiciosB;
import ventanilla.com.bital.admin.Diario;
import ventanilla.com.bital.sfb.RAPG;

import java.util.Stack;

public class Group04Serv extends HttpServlet {
    
    private String getPositionalString(String inputString, int initialPosition, int maxNumberOfBytes, String withSpaces) {
        String outputString = new String("");
        int i = 0;
        if(maxNumberOfBytes == 0)
            maxNumberOfBytes = inputString.length();
        if(withSpaces.equals("YES"))
            while(initialPosition < inputString.length() && inputString.charAt(initialPosition) != 0x20 && i++ < maxNumberOfBytes)
                outputString += inputString.charAt(initialPosition++);
        else
            while(initialPosition < inputString.length() && i++ < maxNumberOfBytes)
                outputString += inputString.charAt(initialPosition++);
        if(outputString.length() == 0)
            outputString = new String("000");
        return outputString;
    }
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession(false);
        Hashtable datasession = (Hashtable)session.getAttribute("page.datasession");
        String txn = (String)datasession.get("cTxn");
        String[] invoqued = request.getParameterValues("txtInvoqued");
        String invTxn =(String)session.getAttribute("VC");
        Diario diario = new Diario();
        String dirip =  (String)session.getAttribute("dirip");
        String sMessage = "";
        String txtMoneda= (String)datasession.get("moneda");
        
        ServiciosA servicioA = new ServiciosA();
        servicioA.setBranch((String)datasession.get("sucursal"));
        servicioA.setTeller((String)datasession.get("teller"));
        servicioA.setSupervisor((String)datasession.get("teller"));
        ServiciosB servicio = new ServiciosB();
        servicio.setBranch((String)datasession.get("sucursal"));
        servicio.setTeller((String)datasession.get("teller"));
        servicio.setSupervisor((String)datasession.get("teller"));
        servicio.setFormat("B");
        String Monto = "";
        String txtServicio = "101041";
        datasession.put("txtServicio", txtServicio);
        datasession.put("flagCuenta","0");//Bandera para mostrar el mensaje "La Cuenta no Existe"
        session.setAttribute("page.datasession", datasession);
        String txtCasoEsp="";
        GenericClasses GC = new GenericClasses();
        datasession.put("fTime", "2");
        if((txn.equals("0730") || txn.equals("0728")) && invTxn.equals("1")) {
            String cuenta = (String)datasession.get("txtCuentaCom");
            datasession.put("fTime", "1");
            datasession.put("txtInvoqued","some");
            if (cuenta.equals("")) 
            {
            	if ( txn.equals("0728") || (txn.equals("0730") && (txtServicio.equals("101026")))  )
            	  	{
            		RAPG rapg = new RAPG();
        
					rapg.setBranch((String)datasession.get("sucursal"));
					rapg.setTeller((String)datasession.get("teller"));
					rapg.setSupervisor((String)datasession.get("supervisor"));
					rapg.setTxnCode("0832");
					rapg.setFormat("G");
					rapg.setToCurr("N$");
        			rapg.setTranAmt("1");
					rapg.setAcctNo("1");
					rapg.setBenefic(txtServicio);
					sMessage = diario.invokeDiario(rapg, (String)session.getAttribute("d_ConsecLiga"), dirip, txtMoneda, session);
            	}
            	else
            	{
    	            txn = "0808";
	                servicio.setTxnCode(txn);
                	servicio.setAcctNo(txtServicio);
            	    servicio.setTranAmt("1");
        	        servicio.setReferenc("1");
    	            servicio.setFromCurr("N$");
	                sMessage = diario.invokeDiario(servicio, (String)session.getAttribute("d_ConsecLiga"), dirip, txtMoneda, session);
                	
            	}
            session.setAttribute("page.datasession", datasession);	
            }
            else 
            {
                String monedaCta = (String)datasession.get("lstDivisa");
				monedaCta=GC.getDivisa(monedaCta);
                
                servicio.setFormat("A");
                if (cuenta.length() == 10)
                    txn = "0080";
                else
                    txn = "0089";
                servicioA.setTxnCode(txn);
                servicioA.setAcctNo(cuenta);
                servicioA.setTranCur(monedaCta);
                servicioA.setCashIn("000");
                sMessage = diario.invokeDiario(servicioA, (String)session.getAttribute("d_ConsecLiga"), dirip, txtMoneda, session);
 
              
                   java.util.Vector cics_resp = new java.util.Vector(); 
                   String respuesta = sMessage;
                   StringBuffer respuestaSB = new StringBuffer(sMessage);
                   if(respuestaSB.charAt(0) != 'O' )
                      {
                       ventanilla.com.bital.util.NSTokenizer parser = new ventanilla.com.bital.util.NSTokenizer(respuesta, "~");
                       while( parser.hasMoreTokens() ) {
                            String token = parser.nextToken();
                            if( token == null )
                               continue;
                            cics_resp.addElement(token);
                            }
            
                       String codigo = (String)cics_resp.elementAt(0); 
                       
                        if (codigo.equals("1"))
                          datasession.put("flagCuenta","1");//Bandera para mostrar el mensaje "La Cuenta no Existe"

                        
                        if (codigo.equals("0"))
                           {
                           RAPG rapg = new RAPG();
    				       rapg.setBranch((String)datasession.get("sucursal"));
					       rapg.setTeller((String)datasession.get("teller"));
					       rapg.setSupervisor((String)datasession.get("supervisor"));
					  	   rapg.setTxnCode("0832");
					       rapg.setFormat("G");
					       rapg.setToCurr("N$");
        			       rapg.setTranAmt("1");
					       rapg.setAcctNo("1");
					       rapg.setBenefic(txtServicio);
		   			       sMessage = diario.invokeDiario(rapg, (String)session.getAttribute("d_ConsecLiga"), dirip, txtMoneda, session);
                           }	
                      }    
          }
        }
        if (invTxn.equals("2")) {
            servicio.setTxnCode(txn);
            if (txn.equals("0728") || txn.equals("S728")) {
                txn = "0728";
                datasession.put("cTxn",txn);
                if (session.getAttribute("Tarjeta.TDI") != null) //Loch Ness
                 	servicio.setIntern((String)session.getAttribute("Tarjeta.TDI"));
                servicio.setTxnCode(txn);
                servicio.setAcctNo(txtServicio);
                Monto = GC.quitap((String)datasession.get("txtMonto"));
                servicio.setTranAmt(Monto);
                String refer = (String)datasession.get("txtReferencia2");
                String status = val_tel(refer);
                if (status.equals("1")) {
                    String Msg = "1~01~2~Referencia Invalida ... Verifique ";
                    session.setAttribute("page.txnresponse", Msg);
                    getServletContext().getRequestDispatcher("/servlet/ventanilla.Redirector").forward(request,response);
                    return;
                }
                String valor = telmex(refer);
                refer = refer + valor;
                servicio.setReferenc(refer);
                Monto ="";
                Monto = GC.quitap((String)datasession.get("txtEfectivo"));
                servicio.setCashIn(Monto);
                servicio.setFromCurr("N$");
                sMessage = diario.invokeDiario(servicio, (String)session.getAttribute("d_ConsecLiga"), dirip, txtMoneda, session);
				if (txn.equals("0728") && datasession.get("montoComision") != null){
					if(Double.parseDouble((String)datasession.get("montoComision"))>0){
					   txtCasoEsp = txn + "~"+ GC.getString(sMessage,1) + "~";
	        	       sMessage = ComIVA(datasession, "0360",txtServicio, dirip, (String)session.getAttribute("d_ConsecLiga"), session);
	   		  	       txtCasoEsp = txtCasoEsp + "0360"+ "~"+ GC.getString(sMessage,1) + "~";      		    
	           		   sMessage = ComIVA(datasession, "0372",txtServicio, dirip, (String)session.getAttribute("d_ConsecLiga"), session);
			           txtCasoEsp = txtCasoEsp + "0372" + "~"+ GC.getString(sMessage,1) + "~";
	    	           session.setAttribute("txtCasoEsp", txtCasoEsp);              
		               datasession.put("cTxn","0372");
	    	           Stack flujotxn = (Stack)session.getAttribute("page.flujotxn");
	           		   if (!flujotxn.empty()){
			              flujotxn.removeAllElements();
	                  	  session.setAttribute("page.flujotxn", flujotxn);
	           		   }
					}
            	}else if(txn.equals("0728") && sMessage.substring(0,1).equals("0")){
            	   Stack flujotxn = (Stack)session.getAttribute("page.flujotxn");
           		   if (!flujotxn.empty()){
		              flujotxn.removeAllElements();
                  	  session.setAttribute("page.flujotxn", flujotxn);
           		   }
				}
				
            }
		}
        if(txn.equals("0808")){
            String domicilioSuc = new String();
            String respuesta = new String();
            if(txn.equals("0808"))
                respuesta = sMessage;
            else
                respuesta = sMessage;
            if(respuesta.substring(0,1).equals("0")){
                domicilioSuc = getPositionalString(respuesta,91,40, "NO");
                session.setAttribute("domicilioSuc", domicilioSuc);
            }
        }
       
        session.setAttribute("page.txnresponse", sMessage);
        getServletContext().getRequestDispatcher("/servlet/ventanilla.Redirector").forward(request,response);
    }
    
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
    
    private String telmex(String ref) {
        long valida = Long.parseLong(ref.substring(0,3));
        String valores ="";
        int factor;
        if (valida == 00) {
            factor = 3;
            valores = ref.substring(2,10);
        }
        else {
            factor = 1;
            valores = ref.substring(0,10);
        }
        long suma = 0;
        long num  = 0;
        for (int i=7; i>=0;) {
            num = Long.parseLong(valores.substring(i,i+1));
            if (num < 0 || num > 9) {
                break;
            }
            num = num * factor;
            suma = suma + num;
            switch (factor) {
                case 3: factor = 1; break;
                case 1: factor = 7; break;
                case 7: factor = 3; break;
            }
            i--;
        }
        suma = suma + 49;
        long res = suma % 9;
        res++;
        return Long.toString(res);
    }
    
    private String val_tel(String re) {
        String status = "0";
        java.util.Date fecha = new java.util.Date();
        java.text.SimpleDateFormat formatof = new java.text.SimpleDateFormat("yyyyMMdd");
        String FS = formatof.format(fecha);
        long fech_min = Long.parseLong(FS);
        String LC = re.substring(0, 3);
        long campo = Long.parseLong(LC);
        if ((fech_min < 20011110 || fech_min > 20020110) && campo == 00)
            status = "1";
        return status;
    }
//AGREGADO PARA POSTEAR COMISIÓN E IVA EN EL MISMO SERVLET PARA TXN 0728
/*    private String getString(String newCadNum, int newOption) {
        int iPIndex = newCadNum.indexOf("CONSECUTIVO:") + 12, NumSaltos, Num = 0, Ban = 0;
        String nCadNum = new String("");
        NumSaltos = newOption;
        while(Num < NumSaltos) {
            if(newCadNum.charAt(iPIndex) == 0x20) {
                while(newCadNum.charAt(iPIndex) == 0x20)
                    iPIndex++;
                Num++;
}
            else
                while(newCadNum.charAt(iPIndex) != 0x20)
                    iPIndex++;
        }
        while(newCadNum.charAt(iPIndex) != 0x20 && iPIndex < newCadNum.length()){
            nCadNum = nCadNum + newCadNum.charAt(iPIndex);
            if(iPIndex < newCadNum.length())
                iPIndex++;
            if(iPIndex == newCadNum.length())
                break;
        }
        
        if ( nCadNum.endsWith("~"))
            nCadNum = nCadNum.substring(0,nCadNum.length()-1);
        return nCadNum;
    }*/
    
    private String ComIVA(Hashtable datos, String Trxn, String serv, String dir, String Conliga, HttpSession ses)
    {

        GenericClasses gc = new GenericClasses();
        ServiciosB Serv = new ServiciosB();      
        Serv.setFormat("B");
        Serv.setBranch((String)datos.get("sucursal"));
        Serv.setTeller((String)datos.get("teller"));
        Serv.setSupervisor((String)datos.get("teller"));

        Serv.setTxnCode(Trxn);
        Serv.setAcctNo(Trxn);
        
        String moneda = (String)datos.get("moneda");
        String monto = "";
		
        if(Trxn.equals("0360"))
            monto = gc.quitap((String)datos.get("montoComision"));
        if(Trxn.equals("0372"))
            monto = gc.quitap((String)datos.get("montoIVA"));
		
        Serv.setTranAmt(monto);
        Serv.setCashIn(monto);
        Serv.setReferenc(serv);
	    Serv.setFromCurr("N$");

        Diario diario = new Diario();
/*        try{Thread.sleep( 1500 );}
        catch (InterruptedException e){}*/
        String sMessage = diario.invokeDiario(Serv, Conliga, dir, moneda, ses);

        return sMessage;
   }
}

