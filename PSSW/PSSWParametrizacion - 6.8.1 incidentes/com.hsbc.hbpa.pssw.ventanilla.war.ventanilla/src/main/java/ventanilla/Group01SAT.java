//*************************************************************************************************
//             Funcion: Clase que consulta el pago de SAT txn 0027
//            Elemento: Group01SAT.java
//          Creado por: Israel De paz Mercado
// CCN - 4360409 - 06/01/2006 - Se crea servlet para la consulta del pago de Linea de Captura del SAT.
//*************************************************************************************************
package ventanilla;

import java.io.IOException;
import java.util.Hashtable;
import java.util.Vector;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import ventanilla.com.bital.admin.Diario;
import ventanilla.com.bital.sfb.RAPG;

public class Group01SAT extends HttpServlet {
    
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	response.setContentType("text/plain");
        HttpSession session = request.getSession(false);
        Hashtable datasession = (Hashtable)session.getAttribute("page.datasession");
        String dirip = (String)session.getAttribute("dirip");
        
        Vector bines = new Vector();
		GenericClasses gc = new GenericClasses();
        RAPG rapg = new RAPG();
        
        rapg.setBranch((String)datasession.get("sucursal"));
        rapg.setTeller((String)datasession.get("teller"));
        rapg.setSupervisor((String)datasession.get("supervisor"));
        
        rapg.setTxnCode("0832");
        rapg.setFormat("G");
        String moneda = (String)datasession.get("moneda");
		rapg.setToCurr(gc.getDivisa(moneda));
        
        rapg.setTranAmt("1");
        rapg.setAcctNo("1");
        Vector servs   = new Vector();
        String ns = (String)datasession.get("nservicios");
        int nos = 1;
        String servicios = "";
        String ser60 = "";
        servs.addElement((String)datasession.get("servicio"+Integer.toString(1)));
        session.setAttribute("page.datasession", datasession);
        servicios="77777";
        rapg.setBenefic(servicios);
        Diario diario = new Diario();
        String sMessage = diario.invokeDiario(rapg, (String)session.getAttribute("d_ConsecLiga"), dirip, moneda, session);
        session.setAttribute("page.txnresponse", sMessage);
  
        getServletContext().getRequestDispatcher("/servlet/ventanilla.Redirector").forward(request,response);
    }
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}