//*************************************************************************************************
//             Funcion: Clase que realiza txn 0041 - 0043 - 0044 - 0045 - 0046 - 0051
//            Elemento: Group01TR.java
//          Creado por: Alejandro Gonzalez Castro
//		Modificado por: Juvenal Fernandez
//*************************************************************************************************
// CCN - 4360189 - 03/09/2004 - Se cambia invoke al Diario para Control de Efectivo
// CCN - 4360204 - 24/09/2004 - Se iguala cadena de envio de la txn 0051 a la de CT y se postea 
//				txn 0827 para verificar que el folio de la txn 0051 no este pagado
// CCN - 4360268 - 04/02/2005 - Se realizan adecuaciones para eliminar Agencia NY
// CCN - 4360368 - 02/09/2005 - Se elimina sleep
// CCN - 4360374 - 08/09/2005 - Se genera nuevo paquete a peticion de QA.
//*************************************************************************************************
package ventanilla;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.util.Hashtable;
import ventanilla.com.bital.admin.Diario;
import ventanilla.com.bital.sfb.Transferencia;
import ventanilla.com.bital.sfb.Acdo;

public class Group01TR extends HttpServlet {
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/plain");
        HttpSession session = request.getSession(false);
        Hashtable datasession = (Hashtable)session.getAttribute("page.datasession");
		GenericClasses gc = new GenericClasses();

        //Variables para diario electronico
        String dirip =  (String)session.getAttribute("dirip");
        String mensaje = "";

        Transferencia transferencia = new Transferencia();

        transferencia.setBranch((String)datasession.get("sucursal"));
        transferencia.setTeller((String)datasession.get("teller"));
        transferencia.setSupervisor((String)datasession.get("supervisor"));

        if ( (String)datasession.get("override") != null )
             if (datasession.get("override").toString().equals("SI"))
                transferencia.setOverride("3");

        if ( (String)datasession.get("supervisor") != null )
             transferencia.setSupervisor((String)datasession.get("supervisor"));

        String txn = (String)datasession.get("cTxn");
        transferencia.setTxnCode(txn);

        String suc = (String)datasession.get("sucursal");
        if (txn.equals("0041") ) 
        {
            transferencia.setAcctNo1((String)datasession.get("txtDDAO"));
            transferencia.setAcctNo2((String)datasession.get("txtDDAD"));
        }
        /*	
            // RFC A 19 POSICIONES JUSTIFICADO A LA IZQ
            // IVA 14 POSICIONES INCLUYENDO COMAS Y PUNTO DECIMAL JUSTIFICADO A LA DER
            String CF = (String)datasession.get("lstCF");
            if (CF.equals("1"))
            {
                String RFC = (String)datasession.get("txtRFC");
                int longitud = RFC.length();
                for (int i=longitud; i<19; i++) {
                    RFC = RFC + " ";
                }
                String IVA = (String)datasession.get("txtIVA");
                longitud = IVA.length();
                for (int i=longitud; i<14; i++) {
                    IVA = " " + IVA;
                }
                String Desc = "-TRF "+ RFC + " " + IVA;
                transferencia.setTranDesc(Desc);
                String DescRev = "REV. " + Desc;
                if(DescRev.length() > 40)
                    DescRev = DescRev.substring(0,40);
                transferencia.setDescRev(DescRev);
            }
            else
            {
                transferencia.setTranDesc("TRANSF A " + (String)datasession.get("txtDDAD"));
                transferencia.setDescRev("REV. TRANSF A " + (String)datasession.get("txtDDAD"));
            }
        }*/
        if (txn.equals("0043") || txn.equals("0045")) {
            transferencia.setAcctNo1((String)datasession.get("txtDDAO"));
            transferencia.setAcctNo2((String)datasession.get("txtCDAD"));
        }
        if (txn.equals("0044") || txn.equals("0046")) {
            transferencia.setAcctNo1((String)datasession.get("txtCDAO"));
            transferencia.setAcctNo2((String)datasession.get("txtDDAD"));
        }
       
        if ( txn.equals("0051")){
            Acdo    oAcdo    = new Acdo();
            oAcdo.setTxnCode("0827");
            oAcdo.setFormat("B");
            oAcdo.setBranch(datasession.get("sucursal").toString());
            oAcdo.setTeller(datasession.get("teller").toString());
            oAcdo.setSupervisor(datasession.get("teller").toString());
            oAcdo.setBackOut("0");
            oAcdo.setOverride("0");
            oAcdo.setCurrentDay("0");
            oAcdo.setAcctNo(datasession.get("txtFolioSeg").toString().substring(5,20));
            Diario diario = new Diario();
            mensaje = diario.invokeDiario(oAcdo, (String)session.getAttribute("d_ConsecLiga"), dirip, datasession.get("moneda").toString(), session);
            
            
            if (mensaje.startsWith("0"))
            {  

                transferencia.setAcctNo1((String)datasession.get("txtDDACuenta"));
                transferencia.setAcctNo2("4001261114");           // Cuenta concentradora de seguros
                transferencia.setTranDesc("CARGO SEG AP CT REF " + datasession.get("txtFolioSeg").toString().substring(5,20));
                transferencia.setDescRev("REV CGO SEG AP CREF " + datasession.get("txtFolioSeg").toString().substring(5,20));
                transferencia.setMoamoun("000");
                transferencia.setVer0051("VER372");
            }
        }

        StringBuffer monto = new StringBuffer((String)datasession.get("txtMonto") );
        for(int i=0; i<monto.length();) {
            if( monto.charAt(i) == ',' || monto.charAt(i) == '.' )
                monto.deleteCharAt(i);
            else
                ++i;
        }

        transferencia.setTranAmt(monto.toString());

        String moneda = (String)datasession.get("moneda");
		transferencia.setTranCur(gc.getDivisa(moneda));

        if((String)datasession.get("txtFechaEfect") != null ) {
            StringBuffer fechatmp = new StringBuffer((String)datasession.get("txtFechaEfect"));
            for(int i=0; i<fechatmp.length();) {
                if( fechatmp.charAt(i) == '/' || fechatmp.charAt(i) == '-' )
                    fechatmp.deleteCharAt(i);
                else
                    ++i;
            }

            String fechacap = fechatmp.substring(4,fechatmp.length()) + fechatmp.substring(2,4) + fechatmp.substring(0,2);

            String fechasys = (String)datasession.get("txtFechaSys");

            if (!fechasys.equals(fechacap)) {
                String fecha = fechacap.substring(4,6) + fechacap.substring(6,fechacap.length()) + fechacap.substring(2,4);
                transferencia.setEffDate(fecha);
            }
        }

        if( mensaje.equals("") || mensaje.startsWith("0")){
            Diario diario = new Diario();
            mensaje = diario.invokeDiario(transferencia, (String)session.getAttribute("d_ConsecLiga"), dirip, moneda, session);
        }

        session.setAttribute("page.txnresponse", mensaje);
        getServletContext().getRequestDispatcher("/servlet/ventanilla.Redirector").forward(request,response);
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}
