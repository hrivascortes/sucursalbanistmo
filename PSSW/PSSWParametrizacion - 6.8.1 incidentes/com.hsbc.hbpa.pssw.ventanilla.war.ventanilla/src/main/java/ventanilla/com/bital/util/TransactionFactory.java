//*************************************************************************************************
//             Funcion: Clase que clasifica txns
//           Elemento : TransactionFactory.java
//      Modificado por: Fausto Rodrigo Flores Moreno
//*************************************************************************************************
// CCN - 4360374 - 08/09/2005 - Se genera nuevo paquete a peticion de QA.
// CCN - 4360424 - 03/02/2006 - Se agregan txn para combinadas y divisas.
// CCN - 4360437 - 24/02/2006 - Se realizan correcciones para txns de SPEI
// CCN - 4360493 - 23/06/2006 - Se realizan modificaciones para txns depto 9005
// CCN - 4360576 - 09/03/2007 - Modificaciones para proyecto OPMN
// CCN - 4360593 - 20/04/2007 - Se realizan modificaciones para corregir ivas duplicados
//*************************************************************************************************


package ventanilla.com.bital.util;

import ventanilla.com.bital.sfb.Transaction;

public class TransactionFactory
{
  // Codigos de transacciones disponibles
  private static String code[][] =
  {
    // Depositos
    {"1001", "1003"},
    {"1011", "1017", /*"1119",*/ "1124", "1191", "1234", "1236", "5023", "5901", "0096"},
    {"1009"},
    {"1029"},
    {"1073"},
    {"4251", "4271"},
    {"5048", "5061", "5063", "5303", "5403", "5603"},
    {"1051", "5051"},
    {"5903"},
    {"1503", "1505", "1507", "1511", "1533"},
    {"5203", "5703"},
    {"4503", "4505"},
    // Cheques de Caja
    {"0810"},
    {"0061"},
    {"S165", "S365"},
    {"S265", "S465"},
    {"4041"},
    {"4103"},
    {"4107"},
    {"4145"},
    {"4151"},
    {"4153"},
    {"4157"},
    {"4011"},
    {"4015"},
    // Cobro Inmediato Masivo
    {"0528"},
    {"4009"}
  };

  // Busqueda de transaccion solicitada
  private static int search(String txn)
  {
    for(int i=0; i<code.length; i++)
    {
      String[] obj = code[i];
      for(int j=0; j<obj.length; j++)
      {
        if( txn.equals(obj[j]) )
          return i;
      }
    }

    // No se encontro la transaccion solicitada
    return 99;
  }

  public static Transaction getTransaction(String txn)
  {
    Transaction txClass = null;

try{
    switch( search(txn) )
    {
      case  0 : txClass = new ventanilla.com.bital.sfb.Group01(); break;
      case  1 : txClass = new ventanilla.com.bital.sfb.Group02(); break;
      case  2 : txClass = new ventanilla.com.bital.sfb.Group03(); break;
      case  3 : txClass = new ventanilla.com.bital.sfb.Group04(); break;
      case  4 : txClass = new ventanilla.com.bital.sfb.Group05(); break;
      case  5 : txClass = new ventanilla.com.bital.sfb.Group06(); break;
      case  6 : txClass = new ventanilla.com.bital.sfb.Group07(); break;
      case  7 : txClass = new ventanilla.com.bital.sfb.Group08(); break;
      case  8 : txClass = new ventanilla.com.bital.sfb.Group09(); break;
      case  9 : txClass = new ventanilla.com.bital.sfb.Group10(); break;
      case 10 : txClass = new ventanilla.com.bital.sfb.Group11(); break;
      case 11 : txClass = new ventanilla.com.bital.sfb.Group12(); break;

      case 12 : txClass = new ventanilla.com.bital.sfb.Group30A(); break;
      case 13 : txClass = new ventanilla.com.bital.sfb.Group31(); break;
      case 14 : txClass = new ventanilla.com.bital.sfb.Group32A(); break;
      case 15 : txClass = new ventanilla.com.bital.sfb.Group32B(); break;
      case 16 : txClass = new ventanilla.com.bital.sfb.Group33(); break;
      case 17 : txClass = new ventanilla.com.bital.sfb.Group34(); break;
      case 18 : txClass = new ventanilla.com.bital.sfb.Group35(); break;
      case 19 : txClass = new ventanilla.com.bital.sfb.Group36(); break;
      case 20 : txClass = new ventanilla.com.bital.sfb.Group37(); break;
      case 21 : txClass = new ventanilla.com.bital.sfb.Group38(); break;
      case 22 : txClass = new ventanilla.com.bital.sfb.Group39(); break;
      case 23 : txClass = new ventanilla.com.bital.sfb.Group31X(); break;
      case 24 : txClass = new ventanilla.com.bital.sfb.Group31Y(); break;
      case 25 : txClass = new ventanilla.com.bital.sfb.Group40(); break;
      case 26 : txClass = new ventanilla.com.bital.sfb.Group41(); break;
            
      default : Class Txn = null; 
				Txn = Class.forName("ventanilla.com.bital.sfb.Txn_"+txn.toString());
				txClass = (Transaction)Txn.newInstance(); break;
      }
	}catch(Exception e){
		System.out.print("TRANSACCION NO DISPONIBLE EN TRANSACTIONFACTORY");
    }
    return txClass;
  }
}

