//*************************************************************************************************
//             Funcion: Bean Txn 5621
//           Elemento : Txn_5621.java
//          Creado por: Fausto Rodrigo Flores Moreno
//*************************************************************************************************
// CCN - 4360493 - 23/06/2006 - Se crea Bean para txn Depto 9005
//*************************************************************************************************
package ventanilla.com.bital.sfb;

import com.bital.util.ParameterParser;
import com.bital.util.ParameterNotFoundException;
import ventanilla.GenericClasses;

public class Txn_5621 extends Transaction
{
	public Header doAction(ParameterParser param)
		throws ParameterNotFoundException
	{
		FormatoA formatoa = new FormatoA();
		GenericClasses gc = new GenericClasses();

		formatoa.setTxnCode("5621");
		formatoa.setAcctNo(param.getString("txtDDADeposito"));
		formatoa.setTranAmt(param.getCString("txtMonto"));
		String moneda = gc.getDivisa(param.getString("moneda"));
		formatoa.setTranCur(moneda);
		formatoa.setTranDesc("01LIQUIDACION/COBRANZA ENVIADA EXT");
		formatoa.setDescRev("01REV.LIQUIDACION/COBRANZA ENVIA");
		formatoa.setMoAmoun("000");
        	return formatoa;
	}
}
