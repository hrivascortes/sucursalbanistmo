//*************************************************************************************************
//		Funcion: 		Bean Txn 3025
//		Creado por: 	Guillermo Rodrigo Escand�n Soto
//*************************************************************************************************
//CCN -  - 02/06/2008 - Creaci�n de bean
//*************************************************************************************************

package ventanilla.com.bital.sfb;

import ventanilla.GenericClasses;
import com.bital.util.ParameterNotFoundException;
import com.bital.util.ParameterParser;


public class Txn_3025 extends Transaction
{
	public Header doAction(ParameterParser param)
		throws ParameterNotFoundException
	{
		FormatoA formatoa = new FormatoA();
		GenericClasses gc = new GenericClasses();
		
		String moneda = gc.getDivisa(param.getString("moneda"));
		
		formatoa.setTxnCode("3025");
		formatoa.setFormat("A");
		formatoa.setAcctNo(param.getCString("txtCDACuenta"));
		formatoa.setTranAmt(param.getCString("txtMonto"));

		if (param.getString("override") != null)
			if (param.getString("override").equals("SI"))
				formatoa.setOverride("3");

		formatoa.setTranCur(moneda);

		return formatoa;
	}
}