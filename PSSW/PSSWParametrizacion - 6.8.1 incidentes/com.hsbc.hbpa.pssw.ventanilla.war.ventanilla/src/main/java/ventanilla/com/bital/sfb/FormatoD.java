package ventanilla.com.bital.sfb;

public class FormatoD extends Header
{
  private String txtEffDate  = "";
  private String txtPswd     = "";
  private String txtTotalsId = "";
  private String txtCashIn   = "";
  private String txtCashOut  = "";
  private String txtCash1    = "";
  private String txtCurrCod1 = "";
  private String txtCash2    = "";
  private String txtCurrCod2 = "";
  private String txtCash3    = "";
  private String txtCurrCod3 = "";
  private String txtCash4    = "";
  private String txtCurrCod4 = "";
  private String txtCash5    = "";
  private String txtCurrCod5 = "";
  private String txtCash6    = "";
  private String txtCurrCod6 = "";
  private String txtCash7    = "";
  private String txtCurrCod7 = "";
  private String txtCash8    = "";
  private String txtCurrCod8 = "";
  private String txtCash9    = "";
  private String txtCurrCod9 = "";


  public String getEffDate()
  {
	return txtEffDate + "*";
  }
  public void setEffDate(String newEffDate)
  {
	txtEffDate = newEffDate;
  }
  
  public String getPswd()
  {
	return txtPswd + "*";
  }
  public void setPswd(String newPswd)
  {
	txtPswd = newPswd;
  }

  public String getTotalsId()
  {
	return txtTotalsId + "*";
  }
  public void setTotalsId(String newTotalsId)
  {
	txtTotalsId = newTotalsId;
  }
  
  public String getCashIn()
  {
	return txtCashIn + "*";
  }
  public void setCashIn(String newCashIn)
  {
	txtCashIn = newCashIn;
  }
  
  public String getCashOut()
  {
	return txtCashOut + "*";
  }
  public void setCashOut(String newCashOut)
  {
	txtCashOut = newCashOut;
  }
  
  public String getCash1()
  {
	return txtCash1 + "*";
  }
  public void setCash1(String newCash1)
  {
	txtCash1 = newCash1;
  }
  
  public String getCurrCod1()
  {
	return txtCurrCod1 + "*";
  }
  public void setCurrCod1(String newCurrCod1)
  {
	txtCurrCod1 = newCurrCod1;
  }
  
  public String getCash2()
  {
	return txtCash2 + "*";
  }
  public void setCash2(String newCash2)
  {
	txtCash2 = newCash2;
  }
  
  public String getCurrCod2()
  {
	return txtCurrCod2 + "*";
  }
  public void setCurrCod2(String newCurrCod2)
  {
	txtCurrCod2 = newCurrCod2;
  }
  
  public String getCash3()
  {
	return txtCash3 + "*";
  }
  public void setCash3(String newCash3)
  {
	txtCash3 = newCash3;
  }
  
  public String getCurrCod3()
  {
	return txtCurrCod3 + "*";
  }
  public void setCurrCod3(String newCurrCod3)
  {
	txtCurrCod3 = newCurrCod3;
  }
  
  public String getCash4()
  {
	return txtCash4 + "*";
  }
  public void setCash4(String newCash4)
  {
	txtCash4 = newCash4;
  }
  
  public String getCurrCod4()
  {
	return txtCurrCod4 + "*";
  }
  public void setCurrCod4(String newCurrCod4)
  {
	txtCurrCod4 = newCurrCod4;
  }
  
  public String getCash5()
  {
	return txtCash5 + "*";
  }
  public void setCash5(String newCash5)
  {
	txtCash5 = newCash5;
  }
  
  public String getCurrCod5()
  {
	return txtCurrCod5 + "*";
  }
  public void setCurrCod5(String newCurrCod5)
  {
	txtCurrCod5 = newCurrCod5;
  }
  
  public String getCash6()
  {
	return txtCash6 + "*";
  }
  public void setCash6(String newCash6)
  {
	txtCash6 = newCash6;
  }
  
  public String getCurrCod6()
  {
	return txtCurrCod6 + "*";
  }
  public void setCurrCod6(String newCurrCod6)
  {
	txtCurrCod6 = newCurrCod6;
  }
  
  public String getCash7()
  {
	return txtCash7 + "*";
  }
  public void setCash7(String newCash7)
  {
	txtCash7 = newCash7;
  }
  
  public String getCurrCod7()
  {
	return txtCurrCod7 + "*";
  }
  public void setCurrCod7(String newCurrCod7)
  {
	txtCurrCod7 = newCurrCod7;
  }
  
  public String getCash8()
  {
	return txtCash8 + "*";
  }
  public void setCash8(String newCash8)
  {
	txtCash8 = newCash8;
  }
  
  public String getCurrCod8()
  {
	return txtCurrCod8 + "*";
  }
  public void setCurrCod8(String newCurrCod8)
  {
	txtCurrCod8 = newCurrCod8;
  }
  
  public String getCash9()
  {
	return txtCash9 + "*";
  }
  public void setCash9(String newCash9)
  {
	txtCash9 = newCash9;
  }
  
  public String getCurrCod9()
  {
	return txtCurrCod9 + "*";
  }
  public void setCurrCod9(String newCurrCod9)
  {
	txtCurrCod9 = newCurrCod9;
  }
  
  public String toString()
  {
	return super.toString()
	+ getEffDate()	+ getPswd()		+ getTotalsId()
	+ getCashIn()	+ getCashOut()
	+ getCash1()	+ getCurrCod1()
	+ getCash2()	+ getCurrCod2()
	+ getCash3()	+ getCurrCod3()
	+ getCash4()	+ getCurrCod4()
	+ getCash5()	+ getCurrCod5()
	+ getCash6()	+ getCurrCod6()
	+ getCash7()	+ getCurrCod7()
	+ getCash8()	+ getCurrCod8()
	+ getCash9()	+ getCurrCod9();
  }    
}
