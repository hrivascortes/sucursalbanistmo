// Transaccion 4103 - Cheques de Caja
package ventanilla.com.bital.sfb;

import com.bital.util.ParameterParser;
import com.bital.util.ParameterNotFoundException;

public class Group34 extends Transaction
{
  public Header doAction(ParameterParser param)
    throws ParameterNotFoundException
  {
    // Instanciar el bean a utilizar
    Cheques cheque = new Cheques();
    
    // Ingresar datos requeridos    
    cheque.setTranAmt( param.getCString("txtMonto") );
    int moneda = param.getInt("moneda");
    if( moneda == 1 )
    {
      cheque.setTranCur("N$");
      cheque.setAcctNo ("0103000023");
    }
    else if( moneda == 1 )
    {
      cheque.setTranCur("US$");
      cheque.setAcctNo ("3902222222");
    }

    //cheque.setCheckNo( param.getString("txtSerial") );
    cheque.setMoAmount("000");
    cheque.setFees("0");
    return cheque;
  }               
}
