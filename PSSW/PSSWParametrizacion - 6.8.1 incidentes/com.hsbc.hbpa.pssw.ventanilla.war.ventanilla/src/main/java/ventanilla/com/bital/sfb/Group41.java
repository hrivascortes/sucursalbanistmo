//*************************************************************************************************
//			 Funcion: Clase que realiza txn 4009
//			Elemento: Group16Serv.java
//		  Creado por: Alejandro Gonzalez Castro
//	  Modificado por: Juvenal Fernandez
//*************************************************************************************************
//CCN - 4360268 - 04/02/2005 - Se realizan adecuaciones para eliminar Agencia NY
//*************************************************************************************************
package ventanilla.com.bital.sfb; 

import com.bital.util.ParameterParser;
import com.bital.util.ParameterNotFoundException;
import ventanilla.GenericClasses;

public class Group41 extends Transaction
{
  public Header doAction(ParameterParser param)
    throws ParameterNotFoundException
  {
    // Instanciar el bean a utilizar
    Deposito deposito = new Deposito();

    deposito.setAcctNo  ( param.getString("txtDDACuenta") );
    deposito.setCashIn  ("000");
    deposito.setMoamoun("000");
    deposito.setTranAmt( param.getCString("txtMontoT") );
	GenericClasses gc = new GenericClasses();

    int moneda = param.getInt("moneda");
	deposito.setTranCur( gc.getDivisa(param.getString("moneda")));
    deposito.setTrNo( param.getString("rdoACorte").substring(1) );
    String NoCheques = param.getString("txtNoCheques");
    while( NoCheques.length() < 4 )
      NoCheques = " " + NoCheques;
    
    
    if(param.getString("txtCodSeg", null) != null )
    {
       String descrip="DEP. CHEQUES C.I. GLOBAL " +  NoCheques + " T" +   param.getString("rdoACorte").substring(1);
       deposito.setTranDesc(descrip);
       // Descripcion para Reverso
       String DescRev = "REV. " + descrip;
       if(DescRev.length() > 40)
           DescRev = DescRev.substring(0,40);
       deposito.setDescRev(DescRev);
    }   
     
    if( param.getInt("rdoACorte") == 1 )
    {
      deposito.setHoldays ("2");
      deposito.setHolDays2("2");
    }
    else
    {
      deposito.setHoldays ("4");
      deposito.setHolDays2("4");
    }
     
    deposito.setAmount3(param.getString("txtPrimerConsec"));
    
    
    
    return deposito;
  }               
}