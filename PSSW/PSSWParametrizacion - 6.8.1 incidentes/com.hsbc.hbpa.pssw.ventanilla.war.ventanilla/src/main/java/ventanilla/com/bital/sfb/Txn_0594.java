//*************************************************************************************************
//             Funcion: Bean Txn 0594
//           Elemento : Txn_0594.java
//          Creado por: Yair J. Chavez Antonel
//*************************************************************************************************
// CCN - 4360533 - 20/10/2006 - Se crea bean para proyecto OPEE
//*************************************************************************************************
package ventanilla.com.bital.sfb;

import ventanilla.GenericClasses;
import com.bital.util.ParameterNotFoundException;
import com.bital.util.ParameterParser;


public class Txn_0594 extends Transaction{

	public static String opee = new String();
	public Header doAction(ParameterParser param)
		throws ParameterNotFoundException
	{

		FormatoB formatob = new FormatoB();
		GenericClasses gc = new GenericClasses();
		
		formatob.setTxnCode("0594");
		formatob.setFormat("B");
		formatob.setAcctNo(param.getString("txtDDACuenta"));
		formatob.setTranAmt(param.getCString("txtMonto1"));
		String moneda = gc.getDivisa(param.getString("moneda"));
		formatob.setFromCurr(moneda);
		formatob.setReferenc(opee); 
		
		return formatob;
	}
}
