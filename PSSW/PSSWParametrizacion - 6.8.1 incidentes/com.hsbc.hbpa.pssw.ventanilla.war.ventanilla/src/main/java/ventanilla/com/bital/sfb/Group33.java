// Transaccion 4041 - Cheques de Caja
package ventanilla.com.bital.sfb;

import com.bital.util.ParameterParser;
import com.bital.util.ParameterNotFoundException;

public class Group33 extends Transaction
{
  public Header doAction(ParameterParser param)
    throws ParameterNotFoundException
  {
    // Instanciar el bean a utilizar
    Cheques cheque = new Cheques();
    
    // Ingresar datos requeridos    
    cheque.setTranAmt( param.getCString("txtMonto") );

    int moneda = param.getInt("moneda");
    if( moneda == 1 )
    {
      cheque.setAcctNo ("0103000023");
      cheque.setTranCur("N$");
    }
    else if( moneda == 2 )
    {
      cheque.setAcctNo ("3902222222");
      cheque.setTranCur("US$");
    }

    cheque.setCheckNo( param.getString("txtSerial") );
    cheque.setTrNo   ( param.getString("lstBanco") );
    
    return cheque;
  }               
}
