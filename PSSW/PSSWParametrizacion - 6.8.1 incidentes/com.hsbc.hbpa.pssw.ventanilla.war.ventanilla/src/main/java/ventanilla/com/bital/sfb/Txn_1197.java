//*************************************************************************************************
//             Funcion: Bean Txn 1197
//           Elemento : Txn_1197.java
//      Modificado por: Alejandro Gonzalez Castro
//*************************************************************************************************
// CCN - 4360268 - 04/02/2005 - Se realizan adecuaciones para eliminar Agencia NY
// CCN - 4360424 - 03/02/2006 - Se agrega para txn combinada
//*************************************************************************************************
package ventanilla.com.bital.sfb;

import com.bital.util.ParameterParser;
import com.bital.util.ParameterNotFoundException;
import ventanilla.GenericClasses;

public class Txn_1197 extends Transaction
{
	public Header doAction(ParameterParser param)
		throws ParameterNotFoundException
	{
		FormatoA formatoa = new FormatoA();
		GenericClasses gc = new GenericClasses();

		formatoa.setTxnCode("1197");
		formatoa.setAcctNo(param.getString("txtDDARetiro"));
		formatoa.setTranAmt(param.getCString("txtMonto"));
		String moneda = gc.getDivisa(param.getString("moneda"));
		formatoa.setTranCur(moneda);
		formatoa.setTranDesc(param.getString("txtDesc1197"));
		String Desc = param.getString("txtDesc1197");
		String DescRev = "";
        DescRev = Desc.substring(0,2) + "REV." + Desc.substring(2,Desc.length());
        if(DescRev.length() > 40)
        	DescRev = DescRev.substring(0,40);
        formatoa.setDescRev(DescRev);
        String FEfect = param.getString("txtFechaEfect");
		String FSys = param.getString("txtFechaSys");
        FEfect =  gc.ValEffDate(FEfect, FSys);
		formatoa.setEffDate(FEfect);
		return formatoa;
	}
}
