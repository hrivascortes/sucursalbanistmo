package ventanilla.com.bital.sfb;

public interface Command
{
  void execute();
}
