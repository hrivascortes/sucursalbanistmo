package ventanilla.com.bital.sfb;

public class DAPA extends Header
{
  private String txtAcctNo   = "";//2
  private String txtTranAmt  = "";//4
  private String txtTranCur  = "";//5
  private String txtCheckNo  = "";//6
  private String txtCashIn   = "";//8
  private String txtCashOut  = "";//9
  private String txtMoamoun  = "";//11
  private String txtTranDesc = "";//13
  private String txtAcctNo1  = "";

  private String txtDescRev  = "";        

  public String getAcctNo()
  {
	return txtAcctNo + "*";
  }
  public void setAcctNo(String newAcctNo)
  {
	txtAcctNo = newAcctNo;
  }

  public String getTranAmt()
  {
	return txtTranAmt + "*";
  }
  public void setTranAmt(String newTranAmt)
  {
	txtTranAmt = newTranAmt;
  }
  
  public String getTranCur()
  {
	return txtTranCur + "*";
  }
  public void setTranCur(String newTranCur)
  {
	txtTranCur = newTranCur;
  }
  
  public String getCheckNo()
  {
	return txtCheckNo + "*";
  }
  public void setCheckNo(String newCheckNo)
  {
	txtCheckNo = newCheckNo;
  }

  public String getCashIn()
  {
	return txtCashIn + "*";
  }
  public void setCashIn(String newCashIn)
  {
	txtCashIn = newCashIn;
  }

  public String getCashOut()
  {
	return txtCashOut + "*";
  }
  public void setCashOut(String newCashOut)
  {
	txtCashOut = newCashOut;
  }
  
  public String getMoamoun()
  {
	return txtMoamoun + "*";
  }
  public void setMoamoun(String newMoamoun)
  {
	txtMoamoun = newMoamoun;
  }
  
  public String getTranDesc()
  {
	return txtTranDesc + "*";
  }
  public void setTranDesc(String newTranDesc)
  {
	txtTranDesc = newTranDesc;
  }

  public String getAcctNo1()
  {
	return txtAcctNo1 + "*";
  }
  public void setAcctNo1(String newAcctNo1)
  {
	txtAcctNo1 = newAcctNo1;
  }
  
  public String getDescRev()
  {
	return txtDescRev;
  }    
  
  public void setDescRev(String newDescRev)
  {
	txtDescRev = newDescRev;
  }     
  
 public String toString()
  {
	return super.toString()
	+ "*"	+ getAcctNo()	+ "*"	+ getTranAmt()
	+ getTranCur()	+ getCheckNo()	+ "*"		+ getCashIn()
	+ getCashOut()	+ "*" + getMoamoun()	+ "*"
	+ getTranDesc()	+ "*"	+ "*"
	+ "*"	+ "*"	+ "*"	+ getAcctNo1()	+ "*"	+ "*"	+ "*"
	+ "*"	+ "*"	+ "*"	+ "*"	+ "*"	+ "*"	+ "*"
	+ "*"	+ "*"	+ "*"	+ "*"	+ "*"	+ "*"	+ "*"
	+ "*"	+ "*"	+ "*"	+ "*"	+ "*"	+ "*"	+ "*"
	+ "*"	+ "*"	+ "*"	+ "*"	+ "*"	+ "*"	+ "*";
  }    
}
