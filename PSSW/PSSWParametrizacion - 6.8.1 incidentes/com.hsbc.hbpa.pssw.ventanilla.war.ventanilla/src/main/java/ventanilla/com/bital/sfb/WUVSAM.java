//************************************************************************************************* 
//             Funcion: Bean para WU VSAM
//            Elemento: WUVSAM.jav
//          Creado por: Alejandro Gonzalez Castro
//      Modificado por: Rutilo Zarco Reyes
//*************************************************************************************************
// CCN - 4360189 - 03/09/2004 - Se habilita WU
//*************************************************************************************************

package ventanilla.com.bital.sfb;

public class WUVSAM extends Header2
{
    private String txtTxnId     = "";
    private String txtResCode   = "00";
    private String txtFuncion   = "";
    private String txtFile      = "";
    private String txtLenKey    = "";
    private String txtKey       = "";
    private String txtLenReg    = "";

    public String getTxnId() 
    {
        return txtTxnId + " ";
    }
    public void setTxnId(String newTxnId) 
    {
        txtTxnId = newTxnId;
    }
    
    public String getResCode() 
    {
        return txtResCode;
    }
    public void setResCode(String newResCode) 
    {
        txtResCode = newResCode;
    }
    
    public String getFuncion() 
    {
        return txtFuncion;
    }
    public void setFuncion(String newFuncion) 
    {
        txtFuncion = newFuncion;
    }
    
    public String getFile() 
    {
        return txtFile;
    }
    public void setFile(String newFile) 
    {
        txtFile = newFile;
    }
    
    public String getLenKey() 
    {
        return Filler(txtLenKey,4, "0", "L");
    }
    public void setLenKey(String newLenKey) 
    {
        txtLenKey = newLenKey;
    }
    
    public String getKey() 
    {
        return Filler(txtKey,60, " ", "R");
    }
    public void setKey(String newKey) 
    {
        txtKey = newKey;
    }
    
    public String getLenReg() 
    {
        return txtLenReg;
    }
    public void setLenReg(String newLenReg) 
    {
        txtLenReg = newLenReg;
    }
    
    public String Filler(String cadena, int largo, String filler, String side)
    {
        int cadlong = cadena.length();
        for(int i=cadlong; i<largo; i++)
        {
            if(side.equals("R"))
                cadena = cadena + filler;
            else
                cadena = filler + cadena;
        }
        return cadena;
    }
    
    public String toString() 
    {
        return getTxnId()   +   getResCode()    +   getFuncion()    +
               getFile()    +	getLenKey()	+   getKey()        +
               getLenReg();
    }
}
