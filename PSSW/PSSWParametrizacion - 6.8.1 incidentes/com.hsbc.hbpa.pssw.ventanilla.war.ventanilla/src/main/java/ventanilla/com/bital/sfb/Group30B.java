// Transaccion 4155 - Cheques de Caja
package ventanilla.com.bital.sfb;

import com.bital.util.ParameterParser;
import com.bital.util.ParameterNotFoundException;
import ventanilla.GenericClasses;

public class Group30B extends Transaction
{
  public Header doAction(ParameterParser param)
    throws ParameterNotFoundException
  {
    // Instanciar el bean a utilizar
    Cheques cheque = new Cheques();
    
    // Ingresar datos requeridos    
    int moneda = param.getInt("moneda");
	GenericClasses gc = new GenericClasses();
	cheque.setTranCur(gc.getDivisa(param.getString("moneda")));
    if( moneda == 1 )
    {
      cheque.setAcctNo ("0103000023");
    }
    else if( moneda == 2 )
    {
      cheque.setAcctNo ("3902222222");
    }
    
    cheque.setTranAmt( param.getCString("txtMonto") );
    cheque.setCashIn ( param.getCString("txtMonto") );
    cheque.setCheckNo( param.getString("txtSerial") );
    cheque.setFees   ( "000" );
    cheque.setTranDesc(param.getString("txtBeneficiario"));

    return cheque;
  }               
}