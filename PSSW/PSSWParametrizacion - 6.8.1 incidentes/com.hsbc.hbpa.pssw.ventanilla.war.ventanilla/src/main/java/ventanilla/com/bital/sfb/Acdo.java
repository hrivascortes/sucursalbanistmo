package ventanilla.com.bital.sfb;

public class Acdo extends Header
{ // Variables para el FORMATO B
  private String txtEffDate  = "";    //6
  private String txtAcctNo   = "";    //21 *  // Documento y referencia del ACDO
  private String txtTranAmt  = "000"; //13 *
  private String txtReferenc = "0";   //21 *
  private String txtFeeAmoun = "0";   //11
  private String txtIntern   = "0";   //11
  private String txtIntWh    = "0";   //11
  private String txtCashIn   = "000"; //11
  private String txtCashOut  = "";    //11
  private String txtFromCurr = "N$";  //3  *
  private String txtToCurr   = "";    //3


   public String getEffDate()
   { return txtEffDate + "*"; }

   public String getAcctNo()
   { return txtAcctNo + "*";  }

   public String getTranAmt()
   { return txtTranAmt + "*"; }

   public String getReferenc()
   { return txtReferenc + "*";}

   public String getFeeAmoun()
   { return txtFeeAmoun + "*";}

   public String getIntern()
   { return txtIntern + "*";  }

   public String getIntWh()
   { return txtIntWh + "*";   }

   public String getCashIn()
   { return txtCashIn + "*";  }

   public String getCashOut()
   { return txtCashOut + "*"; }

   public String getFromCurr()
   { return txtFromCurr + "*";}

   public String getToCurr()
   { return txtToCurr + "*";  }

   public void setEffDate(String newEffDate )
   { txtEffDate = newEffDate;
    }

   public void setAcctNo(String newAcctNo)
   { txtAcctNo = newAcctNo; }

   public void setTranAmt(String newTranAmt)
   { txtTranAmt = newTranAmt; }

   public void setReferenc(String newReferenc)
   { txtReferenc = newReferenc;

     }

   public void setFeeAmoun(String newFeeAmoun)
   { txtFeeAmoun =newFeeAmoun; }

   public void setIntern(String newIntern)
   { txtIntern = newIntern; }

   public void setIntWh(String newIntWh)
   { txtIntWh = newIntWh; }

   public void setCashIn(String newCashIn)
   { txtCashIn = newCashIn; }

   public void setCashOut(String newCashOut)
   { txtCashOut = newCashOut; }

   public void setFromCurr(String newFromCurr)
   { txtFromCurr =newFromCurr; }

   public void setToCurr(String newToCurr)
   { txtToCurr = newToCurr; }

  public String toString()
  {
      return super.toString() + getEffDate() + getAcctNo()  + getTranAmt() +
                                getReferenc()+ getFeeAmoun()+ getIntern()  +
                                getIntWh()   + getCashIn()  + getCashOut() +
                                getFromCurr()+ getToCurr();
  }
}
