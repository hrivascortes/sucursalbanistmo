//*************************************************************************************************
//             Funcion: Bean Txn 5215
//      Modificado por: Alejandro Gonzalez Castro
//*************************************************************************************************
// CCN - 4360437 - 24/02/2006 - Se realizan modificaciones para transacciones SPEI
//*************************************************************************************************

package ventanilla.com.bital.sfb;

import com.bital.util.ParameterParser;
import com.bital.util.ParameterNotFoundException;
import ventanilla.GenericClasses;

public class Txn_5215 extends Transaction
{
	public Header doAction(ParameterParser param)
		throws ParameterNotFoundException
	{
		FormatoA formatoa = new FormatoA();
		GenericClasses gc = new GenericClasses();
		
		formatoa.setTxnCode("5215");
		formatoa.setFormat("A");
		formatoa.setAcctNo(param.getString("txtDDACuenta"));
		formatoa.setTranAmt(param.getCString("txtMonto"));
		String moneda = gc.getDivisa(param.getString("moneda"));
		formatoa.setTranCur(moneda);
		formatoa.setCheckNo(param.getString("txtSerial4"));
		formatoa.setMoAmoun("000");
		
		return formatoa;
	}
}
