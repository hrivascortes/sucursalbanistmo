//*************************************************************************************************
//             Funcion: Bean Txn 4155
//          Creado por: Fausto Rodrigo Flores Moreno
//      Modificado por: Fredy Pe�a Moreno
//*************************************************************************************************
// CCN - 4360593 - 20/04/2007 - Se crea bean para corregir ivas duplicados
// CCN - 4620039 - 28/11/2007 - Se modifica cta
//*************************************************************************************************
package ventanilla.com.bital.sfb;

import com.bital.util.ParameterParser;
import com.bital.util.ParameterNotFoundException;
import ventanilla.GenericClasses;

public class Txn_4155 extends Transaction
{
  public Header doAction(ParameterParser param)
    throws ParameterNotFoundException
  {
    FormatoA formatoa = new FormatoA();
    GenericClasses gc = new GenericClasses();

	formatoa.setTxnCode("4155");
	formatoa.setTranAmt(param.getCString("txtMonto"));
	formatoa.setCashIn(param.getCString("txtMonto"));
	formatoa.setCheckNo(param.getString("txtSerialCheq"));
	formatoa.setFees("000");
	formatoa.setTranDesc(param.getString("txtBeneficiario"));
	
    String moneda = gc.getDivisa(param.getString("moneda"));
    formatoa.setTranCur(moneda);
    formatoa.setAcctNo("0103000023");

    return formatoa;
  }               
}
