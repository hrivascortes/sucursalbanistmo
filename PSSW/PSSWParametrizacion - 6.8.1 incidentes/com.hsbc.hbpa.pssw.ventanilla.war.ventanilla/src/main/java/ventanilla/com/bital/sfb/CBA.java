package ventanilla.com.bital.sfb;

public class CBA extends Header
{
  private String txtAcctNo   = "";
  private String txtTranAmt  = "";
  private String txtTranCur  = "";
  private String txtMoamoun = "";
  private String txtTranDesc = "";
  
  private String txtDescRev  = "";  

  public String getAcctNo()
  {
	return txtAcctNo + "*";
  }
  public void setAcctNo(String newAcctNo)
  {
	txtAcctNo = newAcctNo;
  }

  public String getTranAmt()
  {
	return txtTranAmt + "*";
  }
  public void setTranAmt(String newTranAmt)
  {
	txtTranAmt = newTranAmt;
  }
  
  public String getTranCur()
  {
	return txtTranCur + "*";
  }
  public void setTranCur(String newTranCur)
  {
	txtTranCur = newTranCur;
  }

  public String getMoamoun()
  {
	return txtMoamoun + "*";
  }
  public void setMoamoun(String newMoamoun)
  {
	txtMoamoun = newMoamoun;
  }
  
  public String getTranDesc()
  {
	return txtTranDesc + "*";
  }
  public void setTranDesc(String newTranDesc)
  {
	txtTranDesc = newTranDesc;
  }
  
  public String getDescRev()
  {
	return txtDescRev;
  }    
  
  public void setDescRev(String newDescRev)
  {
	txtDescRev = newDescRev;
  }   
 
  public String toString()
  {
	return super.toString()
	+ "*"	+ getAcctNo()	+ "*"	+ getTranAmt()
	+ getTranCur()	+ "*"	+ "*"		+ "*"
	+ "*"	+ "*"	+ getMoamoun()	+ "*"
	+ getTranDesc()	+ "*"	+ "*"
	+ "*"	+ "*"	+ "*"	+ "*"	+ "*"	+ "*"	+ "*"
	+ "*"	+ "*"	+ "*"	+ "*"	+ "*"	+ "*"	+ "*"
	+ "*"	+ "*"	+ "*"	+ "*"	+ "*"	+ "*"	+ "*"
	+ "*"	+ "*"	+ "*"	+ "*"	+ "*"	+ "*"	+ "*"
	+ "*"	+ "*"	+ "*"	+ "*"	+ "*"	+ "*"	+ "*";
  }    
}
