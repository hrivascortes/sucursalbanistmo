// Transaccion 0528
package ventanilla.com.bital.sfb; 

import com.bital.util.ParameterParser;
import com.bital.util.ParameterNotFoundException;
import ventanilla.GenericClasses; 

public class Group40 extends Transaction
{
  public Header doAction(ParameterParser param)
    throws ParameterNotFoundException
  {
    // Instanciar el bean a utilizar
    FormatoB oFormatoB = new FormatoB();
    oFormatoB.setFormat("B");
	GenericClasses gc = new GenericClasses();
    String txtMonto      = param.getCString("txtMonto");
    String txtMoneda     = param.getString("moneda");
    String txtEfectivo   = "000";
    String txtCodSeg  = param.getString("txtCodSeg");
    String txtCveTran = param.getString("txtCveTran");
    String txtSerial  = param.getString("txtSerial2");
    String txtDepFirm = param.getString("rdoACorte");
    String txtDDACuenta = param.getString("txtDDACuenta2");
    
    if ( txtDepFirm.equals("01"))                           // Antes del Corte
        txtDepFirm  = "1";
    else
        txtDepFirm  = "2";                                     // Despues del Corte
    txtMoneda=gc.getDivisa(txtMoneda);
   
    // Se establesen por medio del BEAN los datos para la transacci"n.
    
    oFormatoB.setAcctNo( "0528" + txtCodSeg + txtCveTran + txtDDACuenta.substring(0,4));
    oFormatoB.setReferenc(txtDDACuenta.substring(4,11) + txtSerial + "T" + txtDepFirm);
    oFormatoB.setTranAmt(txtMonto);                   //Monto Total de la Txn.
    oFormatoB.setFromCurr(txtMoneda);                  //Tipo de Moneda con que se opera la Txn.
    oFormatoB.setIntern(param.getString("txtDDACuenta"));
    if ( param.getString("txtPrimerConsec",null) != null)
        oFormatoB.setCashIn(param.getString("txtPrimerConsec"));
    String NoCheques = new Integer(param.getInt("txtNoCheques") + 1 ).toString();
    oFormatoB.setCashOut(NoCheques);
 
     if ( param.getCString("txtVolante").equals("SI") )
        oFormatoB.setFeeAmoun(param.getString("Volante"));
   
    return oFormatoB;
  }               
}