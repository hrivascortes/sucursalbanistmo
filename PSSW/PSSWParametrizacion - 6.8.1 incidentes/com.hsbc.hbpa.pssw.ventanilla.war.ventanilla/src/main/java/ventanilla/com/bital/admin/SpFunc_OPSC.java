//*************************************************************************************************
//          Funcion: Clase que inserta los datos de las solicitudes de Cheque de Gerencia(Status 1)
//          Elemento: SpFunc_OPSC.java
//          Creado por: YGX
//          Modificado por: Jesus Emmanuel Lopez Rosales
//*************************************************************************************************


package ventanilla.com.bital.admin;

/**
 * @author YGX 
 * Fecha:05/10/07
 * Clase qye inserta los datos de las solicitudes de Cheque de Gerencia (Status 1)
 */

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Hashtable;
import java.util.Vector;

import javax.servlet.http.HttpSession;

import ventanilla.GenericClasses;

import com.bital.util.ConnectionPoolingManager;



public class SpFunc_OPSC implements InterfazSF{
			
	public SpFunc_OPSC(){	}
	
	public int SpFuncionBD(Object bean, Vector Respuesta, HttpSession session, String Connureg){
		int messagecode = 0;
		return messagecode;
	}

	public int SpFuncionAD(Object bean, Vector Respuesta, HttpSession session, String Connureg){
		int messagecode = 0;
		GenericClasses GC = new GenericClasses();				
		Hashtable datasession = (Hashtable)session.getAttribute("page.datasession");		
		Hashtable usrinfo = (Hashtable)session.getAttribute("userinfo");
		
		//Datos para insertar la tabla
		String txn       = GC.getBeanData(bean,"getTxnCode").trim();
		//System.out.println("txn SPFunc_OPSC:"+txn);

		String sucDestino="";
		String cta	     = "";	
		String monto	 = GC.getBeanData(bean,"getTranAmt").trim();
		
		
		String canal     = "01";	
		String sucOrigen = GC.getBeanData(bean,"getBranch").trim();
		
		if(txn.equals("0134"))
		{
		 sucDestino= (String)datasession.get("lstSucursal");
		 cta  = GC.getBeanData(bean,"getAcctNo").trim();	
		}
		else
		{
		 
		 cta  = GC.getBeanData(bean,"getAcctNo").trim().substring(10);	
		// sucDestino= (String)datasession.get("lstSucursal");
		 sucDestino= (String)datasession.get("lstSucursalDep");
		}
		//System.out.println(sucDestino);
		String fecha_sol = GC.getDate(1).trim();	
		
		//Datos llave de busqueda
		String benefi	 = (String)datasession.get("txtBenefiCheq");
		String adress	 = (String)datasession.get("txtDomiciB").toString().trim();
		String detalle	 = (String)datasession.get("txtDetallePago").toString().trim();
		
		//Datos de la respuesta de HOGAN
		String consec    = Respuesta.get(2).toString().trim();//.substring(1).trim();	
		//System.out.println("Consecutivoo: "+consec);
		//System.out.println("direccion: "+adress);
		//System.out.println("detalle: "+detalle);
		
		//Connureg
		//String fecha = GC.getDate(6);
        //String hora  = GC.getDate(7);
        //String teller = GC.getBeanData(bean,"getTeller").trim();
        //getBeanData("getTeller");
        //String ConsecutivoDiario = teller + fecha + hora;
        String consecutivoDiario = (String)session.getAttribute("page.diario_consecutivo");
    	//System.out.println("ConsecutivoDiario: "+consecutivoDiario);
		
		if(adress== null||adress.equals(""))
		  {adress=" ";}
		if(detalle ==null||detalle.equals(""))
		  {detalle=" ";}
		
		//Insert de Solictud de Cheque	Gerencia
		String statementPS = com.bital.util.ConnectionPoolingManager.getStatementPostFix();			
		Connection pooledConnection = ConnectionPoolingManager.getPooledConnection();
		PreparedStatement insert = null;
		
		int ResultInsert = 0;									
				try{							 
					if(pooledConnection != null){	
					  try{						 
					//	insert = pooledConnection.prepareStatement("INSERT INTO TW_CHQGER (C_FECHA_SOL,C_SUC_SOL,C_CONCARGA,D_SUC_EXP,D_MONTO,D_CANAL,D_SERIAL,D_CUENTA ,D_FECHA_EXP ,D_STATUS ,D_BENEFICIARIO ,D_DETALLE ,D_DIRECCION ,D_CEDULA,D_AUTORIZADO ,D_CONUREG) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
					    insert = pooledConnection.prepareStatement("INSERT INTO TW_CHQGER VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
						//System.out.println(insert);
						insert.setString(1,fecha_sol);
						insert.setString(2,sucOrigen);
						insert.setString(3,consec); 
						insert.setString(4,sucDestino);
						insert.setInt(5,Integer.parseInt(monto)); 
						insert.setString(6,"01");
						insert.setString(7," ");
						insert.setString(8,cta); 
						insert.setString(9," ");
						insert.setString(10,"1"); 
						insert.setString(11,benefi); 
						insert.setString(12,detalle);
						insert.setString(13,adress);
						insert.setString(14," ");
						insert.setString(15," ");						
						insert.setString(16,consecutivoDiario);
						ResultInsert= insert.executeUpdate();
			    		//if(ResultInsert > 0){	    			
			    			
							//System.out.println("SpFunc_OPSC::InsertSolCheque::Se insert� ["+ResultInsert+"] registro en la tabla TW_CHQGER");
							//System.out.println("SpFunc_OPSC::InsertSolCheque::Se actualiz� el status a \"1\"");
						//}						
					 //insert.executeUpdate();						
					 }catch(SQLException sqlException){
					 	
					 	sqlException.printStackTrace();
					 	
				            //System.err.println("SpFunc_OPSC::InsertLog [" + sqlException + "]");		 			 			
					 }
					}        
			   	}catch( Exception e ){
			   		//System.err.println("SpFunc_OPSC:: " + e.getMessage());
					e.printStackTrace();		 
			   	}
		return messagecode;
	}
	
	public int SpFuncionBR(Hashtable txnRev, Hashtable txnOrg, HttpSession session, String Connureg, String Supervisor){
		int messagecode = 0;
		return messagecode;
	}
	
	public int SpFuncionAR(Hashtable txnRev, Hashtable txnOrg, HttpSession session, String Connureg, String Cosecutivo, String Supervisor, Vector VResp){
		int messagecode = 0;
		return messagecode;
	}
}

