//*************************************************************************************************
//		Funcion: Formato B alterno para las transacciones 0768 y 0770
//		Creado por: Guillermo Rodrigo Escand�n Soto
//*************************************************************************************************

package ventanilla.com.bital.sfb;

public class FormatoB2 extends Header
{
  private String txtEffDate  = "";
  private String txtAcctNo   = "";
  private String txtTranAmt  = "";
  private String txtReferenc = "";
  private String txtFeeAmoun = "";
  private String txtIntern   = "";
  private String txtIntWh    = "";
  private String txtCashIn   = "";
  private String txtCashOut  = "";
  private String txtFromCurr = "";
  private String txtToCurr   = "";
  private String txtDesc	 = "";


  public FormatoB2()
  {
	super();
  }

  private String getValue(String value)
  {
	if( value == null )
	  return "*";
	else
	  return value + "*";  
  }

  public String getAcctNo()
  {
	return getValue(txtAcctNo);
  }

  public String getEffDate()
  {
	return getValue(txtEffDate);
  }
  
  public String getCashIn()
  {
	return getValue(txtCashIn);
  }

  public String getCashOut()
  {
	return getValue(txtCashOut);
  }

  public String getIntWh()
  {
	return getValue(txtIntWh);
  }


  public String getIntern()
  {
	return getValue(txtIntern);
  }

  public String getFeeAmoun()
  {
	return getValue(txtFeeAmoun);
  }

  public String getTranAmt()
  {
	return getValue(txtTranAmt);
  }

  public String  getReferenc()
  {
	return getValue(txtReferenc);
  }
  
  public String getFromCurr()
  {
	return getValue(txtFromCurr);
  }

  public String getToCurr()
  {
	return getValue(txtToCurr);
  }
  
  public String getDesc()
  {
	return getValue(txtDesc);
  }

  public void setEffDate(String newEffDate)
  {
	txtEffDate = newEffDate;
  }
  
  public void setAcctNo(String newAcctNo)
  {
	txtAcctNo = newAcctNo;
  }
  
  public void setTranAmt(String newTranAmt)
  {
	txtTranAmt = newTranAmt;
  }
  
  public void setReferenc(String newReferenc)
  {
	txtReferenc = newReferenc;
  }

  public void setFeeAmoun(String newFeeAmoun)
  {
	txtFeeAmoun = newFeeAmoun;
  }
  
  public void setIntern(String newIntern)
  {
	txtIntern = newIntern;
  }
  
  public void setIntWh(String newIntWh)
  {
	txtIntWh = newIntWh;
  }
  
  public void setCashIn(String newCashIn)
  {
	txtCashIn = newCashIn;
  }
  
  public void setCashOut(String newCashOut)
  {
	txtCashOut = newCashOut;
  }
  
  public void setFromCurr(String newFromCurr)
  {
	txtFromCurr = newFromCurr;
  }
  
  public void setToCurr(String newToCurr)
  {
	txtToCurr = newToCurr;
  }
  
  public void setDesc(String newDesc)
  {
	txtDesc = newDesc;
  }

  public String toString()
  {
	return super.toString()
	+ getEffDate()	+ getAcctNo()	+ getTranAmt()
	+ getReferenc()	+ getFeeAmoun()	+ getIntern()	+ getIntWh()
	+ getCashIn()	+ getCashOut()	+ getFromCurr()	+ getToCurr() + getDesc();
  }    
  
}
