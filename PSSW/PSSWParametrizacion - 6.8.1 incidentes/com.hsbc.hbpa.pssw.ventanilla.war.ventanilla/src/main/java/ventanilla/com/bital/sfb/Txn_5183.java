//*************************************************************************************************
//             Funcion: Bean Txn 5183
//      Creado por: Jesus Emmanuel Lopez Rosales
//                  Yair Jesus Chavez Antonel 
//*************************************************************************************************
// CCN - 4360533 - 20/10/2006 - Se crea bean para proyecto OPEE
// CCN - 4360542 - 10/11/2006 - Se realizan modificaciones para OPEE y txn 0821
//*************************************************************************************************
package ventanilla.com.bital.sfb;

import com.bital.util.ParameterParser;
import com.bital.util.ParameterNotFoundException;
import ventanilla.GenericClasses;

public class Txn_5183 extends Transaction
{	
	public static String tipoCambioO = "";
	
	public Header doAction(ParameterParser param)
		throws ParameterNotFoundException
	{
		FormatoA formatoa = new FormatoA();
		GenericClasses gc = new GenericClasses();
		String tipoCambio = "";
		tipoCambioO = param.getCString("txtTipoCambio");
		
		formatoa.setTxnCode("5183");
		formatoa.setFormat("A");
		formatoa.setAcctNo(param.getString("txtDDACuenta"));
		String moneda = gc.getDivisa(param.getString("moneda"));
		if (moneda.equals("N$"))
			formatoa.setTranAmt(param.getCString("txtMonto"));	
		else
			formatoa.setTranAmt(param.getCString("txtMonto1"));
		formatoa.setTranCur(moneda);
		tipoCambio = tipoCambioO;
		//ODDT 5183A 00002000250  000250  000**4000000000**422  *US$*****00000231231*422   ***************12123CAD*************************
		//ODPA 5183A 00002000251  000251  000**0100000331**52812*N$ *****      18935*100000***************00000No existe la moneda:null*************************
		//String entero = tipoCambio.substring(0,8);
		//String decimal = tipoCambio.substring(8);
		//tipoCambio = entero.substring(entero.length()-4) + decimal.substring(0,4);		
		//formatoa.setDraftAm(param.getString("txtPlaza")+tipoCambio);
		formatoa.setDraftAm(gc.rellenaZeros(tipoCambio,11));
		//formatoa.setMoAmoun(param.getCString("txtMontoEnv"));
		if (moneda.equals("N$"))
			formatoa.setMoAmoun(param.getCString("txtMonto1"));	
		else
			formatoa.setMoAmoun(param.getCString("txtMonto"));
		//formatoa.setAcctNo2(gc.StringFiller(param.getString("txtFolioIT5"),5,false,"0") + param.getString("lstCatDivisa"));
		//System.out.println(gc.getDivisa(param.getString("moneda2")));
		formatoa.setAcctNo2("00000" + gc.getDivisa(param.getString("moneda2")));        		
		return formatoa;
	}
}

                 
