//*************************************************************************************************
//             Funcion: Bean Txn 0476
//          Creado por: Yessica G Garcia
//*************************************************************************************************
// CCN - 46200080 - 14/09/2007 - Se agrega TXN
//*************************************************************************************************

package ventanilla.com.bital.sfb;

import com.bital.util.ParameterNotFoundException;
import com.bital.util.ParameterParser;

import ventanilla.GenericClasses;

public class Txn_0476 extends Transaction{
	
	public Header doAction(ParameterParser param)
		throws ParameterNotFoundException
	{
		
		FormatoB formatob = new FormatoB();
		GenericClasses gc = new GenericClasses();
		
		ServiciosB serviciob = new ServiciosB();
        serviciob.setBranch(param.getCString("sucursal"));
        //((String)datasession.get("sucursal"));
        
        serviciob.setTeller(param.getCString("teller"));
        //((String)datasession.get("teller"));
        
        serviciob.setSupervisor(param.getCString("teller"));
        //((String)datasession.get("teller"));
        
         serviciob.setSupervisor(param.getCString("supervisor"));
         //((String)datasession.get("supervisor"));
		
		serviciob.setFormat("B");
		
	    serviciob.setTxnCode("0476");
	    
	   	serviciob.setAcctNo("0476");
	   	
	   	serviciob.setTranAmt(gc.delCommaPointFromString(param.getString("txtMonto")));
	   	
	   	serviciob.setReferenc(param.getString("txtRefer10630"));
		
		serviciob.setIntWh("");
        
        serviciob.setCashOut(gc.delCommaPointFromString(param.getString("txtMonto")));
                
        String moneda = gc.getDivisa(param.getString("moneda"));
        
        serviciob.setFromCurr(moneda);
		
		
		return serviciob;
	}

}
