// Transaccion 4011 para 0061 - Cheques de Caja
package ventanilla.com.bital.sfb;

import com.bital.util.ParameterParser;
import com.bital.util.ParameterNotFoundException;
import ventanilla.GenericClasses;

public class Group31X extends Transaction
{
  public Header doAction(ParameterParser param)
    throws ParameterNotFoundException
  {
    // Instanciar el bean a utilizar
    Cheques cheque = new Cheques();
    cheque.setTxnCode("4011");
	GenericClasses gc = new GenericClasses();
    
    // Ingresar datos requeridos
    cheque.setAcctNo ( param.getString("txtDDACuenta") );
    cheque.setTranAmt( param.getCString("txtMonto") );
    int moneda = param.getInt("moneda");
	cheque.setTranCur( gc.getDivisa(param.getString("moneda")));
   
    String desc = "";
    String DescRev = "";
    /*if( param.getString("txtCveRFC").length() > 0 )
    {
      desc = "-CGO " + param.getString("txtCveRFC");
      
      String iva = param.getString("txtImporteIVA");
      int longdesc = desc.length();
      int longiva = iva.length();
      int dif = longdesc + longiva;
      dif = 39 - dif;
      
      for (int i=longdesc; i<longdesc+dif; i++) {
           desc = desc + " ";
      }      
      
      DescRev = "REV.  " + desc;
      desc = desc + param.getString("txtImporteIVA");
    }
    else
    {*/
        desc = "EXPED.CHEQUE DE GERENCIA";
        DescRev = "REV.  " + desc;
   // }
        // Descripcion para Reverso
        if(DescRev.length() > 40)
            DescRev = DescRev.substring(0,40);
        cheque.setDescRev(DescRev);
    

    cheque.setTranDesc( desc );

    cheque.setCheckNo ( param.getString("txtSerialCheq") );    
    cheque.setCashIn  ( "000" );
    cheque.setMoAmount( "000" );
    
    return cheque;
  }
}