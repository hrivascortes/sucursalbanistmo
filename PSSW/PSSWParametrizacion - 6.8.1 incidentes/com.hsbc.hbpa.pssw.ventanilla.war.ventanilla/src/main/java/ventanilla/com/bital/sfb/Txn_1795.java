//*************************************************************************************************
//             Funcion: Bean Txn 1795
//      Modificado por: Fredy Pe�a Moreno
//*************************************************************************************************
// CCN - 4360607 - 18/05/2007 - Bean para TXNS Genericas Depositos Generico
//*************************************************************************************************

package ventanilla.com.bital.sfb;

import com.bital.util.ParameterParser;
import com.bital.util.ParameterNotFoundException;
import ventanilla.GenericClasses;

public class Txn_1795 extends Transaction{
	public Header doAction(ParameterParser param)
		throws ParameterNotFoundException
	{
		FormatoA formatoa = new FormatoA();
		GenericClasses gc = new GenericClasses();
		formatoa.setTxnCode("1795");
		formatoa.setFormat("A");		
		formatoa.setAcctNo(param.getString("txtDDACuenta"));		
		formatoa.setTranAmt(param.getCString("txtEfectivo"));			
		formatoa.setCashIn(param.getCString("txtEfectivo"));
	
		formatoa.setTranDesc(param.getString("lstDGeneric1795"));		
	        
        if (param.getString("override") != null)
            if (param.getString("override").equals("SI"))
                formatoa.setOverride("3");
     
        formatoa.setMoAmoun("000"); 
        formatoa.setHolDays("0");
        formatoa.setHolDays2("0");
			
		String moneda = gc.getDivisa(param.getString("moneda"));
		formatoa.setTranCur(moneda);		
	
		return formatoa;
	}
}
