//*************************************************************************************************
//             Funcion: Bean Txn 4193 SOLICTUD DE CHEQUES DE GERENCIA 
//             Elaborado por: YGX
//*************************************************************************************************

package ventanilla.com.bital.sfb;


import ventanilla.GenericClasses;
import com.bital.util.ParameterNotFoundException;
import com.bital.util.ParameterParser;


public class Txn_0270 extends Transaction
{		 
	public Header doAction(ParameterParser param)
		throws ParameterNotFoundException
	{				
        
        GenericClasses gc = new GenericClasses();   
        FormatoH formatoh = new FormatoH();         				
		ventanilla.com.bital.sfb.Cheques Cheques = new ventanilla.com.bital.sfb.Cheques();
		
		formatoh.setTxnId("ODPA");
		formatoh.setTxnCode("0270");
	    formatoh.setFormat("H");   
	    
	    //Cuenta de Cheques de Gerencia
	    //formatoh.setAcctNo("0");
	    
	    
	    
	    //1*Cuenta de Cheques de Gerencia
	    formatoh.setAcctNo("85003700000000026666");
	    
	    //formatoh.setNatCurr("");
	    String benefi = param.getString("txtBenefiCheq").trim();
	    if (benefi.length() > 40)
			benefi = benefi.substring(0, 40);
	    formatoh.setTranDesc(benefi);
	    
	    //1*Cuenta de Cuenta informativa se env�a la cta del cargo
	    formatoh.setAcctDeb("20030101000000001");	    
	    
	    formatoh.setTranAmt(gc.delCommaPointFromString(param.getString("txtMonto")).trim());
	    
	    int moneda = param.getInt("moneda"); 
	    formatoh.setTranCur(gc.getDivisa(param.getString("moneda")));
	    //formatoh.setCheckNo(param.getCString("txtSerial"));
	    //formatoh.setCheckNo("000");
	    formatoh.setCashIn("000");
	    formatoh.setCashOut("");
	    formatoh.setDraftAm("");
	    formatoh.setFees("");
	    formatoh.setDescRev("REV SOLICITUD CG DEPTAL");
    
    	//System.out.println(formatoh);
    	return formatoh;
	}
}
