package ventanilla.com.bital.util;

import ventanilla.com.bital.util.Usuario;

public final class Base extends Segment
{
  private String[] items = null;
  private Usuario user = null;
  //private int status = 1;

  public Base(Usuario user)
  {
    this.user = user;
  }

  /*public int getStatus()
  {
    return this.status;
  }*/

  public String getMessage()
  {
    StringBuffer msg = new StringBuffer(128);
    msg.append("XPAN ~00~PWEB_MM~").append("60026~~~");
    /*  .append(user.getSucursal()).append("~")
      .append(user.getPromotor()).append("~")
      .append(user.getPromotor()).append("~")*/
    
    return msg.toString();
  }

  public void parse(ventanilla.com.bital.util.PASSResponse response){
//	setStatus(-1);
    // Obtener 6 elementos de la respuesta
    items = response.parseItems(6);

    // Mostrar los elementos obtenidos
    //com.bital.util.Debug.StringArray(items, System.out);

    // Validar codigo de respuesta del segmento
    if( items.length != 6 )
      return;
    
    setStatus(0);
  }
}
