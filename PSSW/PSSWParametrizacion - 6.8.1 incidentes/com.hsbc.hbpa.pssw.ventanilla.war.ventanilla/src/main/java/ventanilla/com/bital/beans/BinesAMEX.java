//*************************************************************************************************
//             Funcion: Clase que realiza la consulta los bines de la tabla
//          Creado por: Humberto Enrique Balleza Mej�a
//      Modificado por: 
//*************************************************************************************************
// CCN -          - 20/01/2009 - Se crea la clase para realizar select y mantener los datos en persistencia
//*************************************************************************************************

package ventanilla.com.bital.beans;

import java.util.Vector;

import ventanilla.com.bital.util.DBQuery;

public class BinesAMEX
{
    private Vector bines = null;
    private static BinesAMEX instance = null;
    
    private BinesAMEX() 
    {
        bines = new Vector();
        execute();
    }
    
    public static synchronized BinesAMEX getInstance()
    {
        if(instance == null)
            instance = new BinesAMEX();
        return instance;
    }

    public Vector getBines() 
    {
        return bines;
    }    
    
    public void execute() 
    {
        String sql = null;
        sql = "SELECT C_BIN, D_ID_BANCO, LENGTH(RTRIM(C_BIN)) AS LEN FROM ";
        String clause = null;
        clause = " ORDER BY LEN DESC, C_BIN ASC";
        
        DBQuery dbq = new DBQuery(sql,"TC_BINES_AMEX", clause);
        bines = dbq.getRows();
                
    }
}
