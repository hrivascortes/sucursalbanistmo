//*************************************************************************************************
//             Funcion: Bean Txn 5007
//          Creado por: Oscar L�pez Gutierrez
//*************************************************************************************************
// CCN - 4360500 - 04/08/2006 - Se agrega para Depositdores
//*************************************************************************************************

package ventanilla.com.bital.sfb;

import com.bital.util.ParameterParser;
import com.bital.util.ParameterNotFoundException;
import ventanilla.GenericClasses;

public class Txn_5007 extends Transaction
{
	public Header doAction(ParameterParser param)
		throws ParameterNotFoundException
	{
		FormatoA formatoa = new FormatoA();
		GenericClasses gc = new GenericClasses();
		formatoa.setTxnCode("5007");
		formatoa.setFormat("A");
		formatoa.setAcctNo(param.getString("txtDDACuenta"));
		formatoa.setTranAmt(param.getCString("txtMontoNoCero"));
		String moneda = gc.getDivisa(param.getString("moneda"));
		formatoa.setTranCur(moneda);
		String txtFechaEfect = param.getCString("txtFechaEfect");
		txtFechaEfect = txtFechaEfect.substring(6,10) + txtFechaEfect.substring(3,5) +  txtFechaEfect.substring(0,2);
        	String DescRev    = "REVERSO " + txtFechaEfect;		
	        if(DescRev.length() > 40)
        	   DescRev = DescRev.substring(0,40);
	        formatoa.setDescRev(DescRev);
		formatoa.setTranDesc("CAR.CDM" + txtFechaEfect);        
		formatoa.setCheckNo(param.getCString("txtSecCDM"));		
		formatoa.setAcctNo1(param.getCString("txtHora"));
		formatoa.setAcctNo2(param.getCString("txtNemo"));
		formatoa.setTrNo(param.getCString("txtNumFolioATM"));
		
		return formatoa;
	}
}


