// Transaccion 4011 para 0061 - Cheques de Caja
package ventanilla.com.bital.sfb;

import com.bital.util.ParameterParser;
import com.bital.util.ParameterNotFoundException;
import ventanilla.GenericClasses;

public class Txn_2201 extends Transaction
{
  public Header doAction(ParameterParser param)
    throws ParameterNotFoundException
  {
    
    FormatoA formatoa = new FormatoA();
	GenericClasses gc = new GenericClasses();
	formatoa.setTxnCode("2201");
	
	formatoa.setAcctNo(param.getString("txtDDACuenta"));

	
	String moneda = gc.getDivisa(param.getString("moneda"));
	String moneda_s =  gc.getDivisa(param.getString("moneda2"));
	
	if(!moneda.equals("N$"))
		formatoa.setTranAmt(param.getCString("txtMonto"));
	else
		formatoa.setTranAmt(param.getCString("txtMonto1"));
	
	
	formatoa.setTranCur(moneda_s);
	//"ABONO X TRANSF RECIBIDA" +param.getCString("lstBanCorresp1")
	//CR X TRANSF REC C/V
	//formatoa.setTranDesc(param.getCString("lstBanCorresp1"));
	//formatoa.setTranDesc("ABONO X TRANSF RECIBIDA " +param.getCString("lstBanCorresp1"));
	
	String ceros="";
	if(moneda.equals("N$"))
		//formatoa.setTranDesc(param.getString("lstBanCorresp1")+gc.StringFiller( ceros,12-param.getString("lstBanCorresp1").length(),false," ")+"USD000000000       00");
		formatoa.setTranDesc(param.getString("lstBanCorresp1") + gc.StringFiller(ceros,12-param.getString("lstBanCorresp1").length(),false," ") + "USD" + param.getString("txtRefTIRE"));
	else
		//formatoa.setTranDesc(param.getString("lstBanCorresp1")+gc.StringFiller( ceros,12-param.getString("lstBanCorresp1").length(),false," ")+moneda+"000000000       00");
		formatoa.setTranDesc(param.getString("lstBanCorresp1") + gc.StringFiller(ceros,12-param.getString("lstBanCorresp1").length(),false," ") + moneda + param.getString("txtRefTIRE"));
	
	
    //System.out.println(formatoa);
    return formatoa;
  }
}