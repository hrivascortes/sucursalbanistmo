//*************************************************************************************************
//             Funcion: Bean Txn 0860
//      Elaborado por: Jesus Emmanuel Lopez Rosales
//*************************************************************************************************
// CCN - 4360576 - 09/03/2007 - Modificaciones para proyecto OPMN
// CCN - 4360584 - 23/03/2007 - Modificaciones para proyecto OPMN
//*************************************************************************************************
package ventanilla.com.bital.sfb;



import java.util.Vector;

import com.bital.util.ParameterNotFoundException;
import com.bital.util.ParameterParser;
import ventanilla.GenericClasses;
import ventanilla.com.bital.util.DBQuery;

public class Txn_0860 extends Transaction
{		 
	public Header doAction(ParameterParser param)
		throws ParameterNotFoundException
	{				
        GenericClasses gc = new GenericClasses();                				
		FormatoG formatog = new FormatoG();
		 
	   formatog.setTxnCode("0860");
       formatog.setFormat("G");	
       formatog.setToCurr("N$");
       formatog.setTranAmt("000");	
       formatog.setAcctNo("1");
              
       String strOrden = param.getString("txtNumOrden");
       if (strOrden.length() == 8){
       	   String sql = null;       	           
           sql = "SELECT C_NUMORD_NVO FROM ";
           String clause = null;
           clause = " WHERE C_NUMORD_ANT='" + strOrden + "'";
           DBQuery dbq = new DBQuery(sql,"TC_OPMN",clause);
           Vector Regs = dbq.getRows();
           String  Orden ="";
           if(Regs.size()>0){
             Vector row = (Vector)Regs.get(0);
             Orden = row.get(0).toString().trim();           
           }else{
             Orden = strOrden;            
           }
           formatog.setCtaBenef(Orden);  
       }else{       
	   formatog.setCtaBenef(strOrden.toUpperCase());	   	   
  	   }
	   String strciudad =param.getString("ciudad");
	   if(strciudad.length() > 20){
	      strciudad = strciudad.substring(0,20);
	   }else{
	      strciudad =gc.StringFiller(param.getString("ciudad"),20,true," ");
	   }	
	   String strip =gc.StringFiller(param.getString("dirip"),16,true," ");
	   formatog.setInstruc(strciudad + strip);
	   formatog.setOrdenan(gc.StringFiller(param.getString("lstTipoDocto"),4,true," ")+gc.StringFiller(param.getString("txtFolio"),17,true," "));	   	   	   
		return formatog;
	}
	

}
