//*************************************************************************************************
//             Funcion: Bean Txn 0770
//*************************************************************************************************
// CCN - XXXXXXX - XXXXXXXXXX -                                                       
//*************************************************************************************************

package ventanilla.com.bital.sfb;

import com.bital.util.ParameterParser;
import com.bital.util.ParameterNotFoundException;

import ventanilla.GenericClasses;
 

public class Txn_0770 extends Transaction
{
	public Header doAction(ParameterParser param)
		throws ParameterNotFoundException
	{
		FormatoB2 formatob = new FormatoB2();
		GenericClasses gc = new GenericClasses();
		String moneda = gc.getDivisa(param.getString("moneda"));
		String moneda_s =  gc.getDivisa(param.getString("moneda2"));
		
		//ODCT 0770B 09004900403  900403  000**0770MRMDUS33    USD*50000*227405173******US$**

		formatob.setTxnCode("0770");
		formatob.setFormat("B");
		String ceros = "";
		if(!moneda.equals("N$"))
				formatob.setAcctNo("0770" + param.getString("lstBanCorresp1")+gc.StringFiller( ceros,12-param.getString("lstBanCorresp1").length(),false," ")+"USD");
		else
				formatob.setAcctNo("0770" + param.getString("lstBanCorresp1")+gc.StringFiller( ceros,12-param.getString("lstBanCorresp1").length(),false," ")+moneda_s);
				
		if(!moneda.equals("N$"))
			formatob.setTranAmt(param.getCString("txtMonto"));
		else
			formatob.setTranAmt(param.getCString("txtMonto1"));
		
		formatob.setReferenc("000000000");
		formatob.setCashIn("0");
		formatob.setFromCurr(moneda_s);
		formatob.setDesc(param.getCString("txtRefTIRE"));
		
		return formatob;
	}
}
