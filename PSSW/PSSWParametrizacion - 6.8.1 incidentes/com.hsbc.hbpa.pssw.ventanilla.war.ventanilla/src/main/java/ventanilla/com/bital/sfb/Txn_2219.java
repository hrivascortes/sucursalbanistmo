//*************************************************************************************************
//			   Funcion: Bean Txn 2217
//		Creado por: Carolina Vela 
//*************************************************************************************************

//*************************************************************************************************
package ventanilla.com.bital.sfb;

import com.bital.util.ParameterParser;
import com.bital.util.ParameterNotFoundException;
import ventanilla.GenericClasses;

public class Txn_2219 extends Transaction
{
	public Header doAction(ParameterParser param)
		throws ParameterNotFoundException
	{
		FormatoA formatoa = new FormatoA();
		GenericClasses gc = new GenericClasses();
		formatoa.setTxnCode("2219");
		formatoa.setFormat("A");
		formatoa.setAcctNo(param.getString("txtDDACuenta"));
		formatoa.setTranAmt(param.getCString("txtMonto"));
		
		String moneda = gc.getDivisa(param.getString("moneda"));
		formatoa.setTranCur(moneda);

		formatoa.setTranDesc(param.getString("lstDescSGiro"));

		return formatoa;
	}
}