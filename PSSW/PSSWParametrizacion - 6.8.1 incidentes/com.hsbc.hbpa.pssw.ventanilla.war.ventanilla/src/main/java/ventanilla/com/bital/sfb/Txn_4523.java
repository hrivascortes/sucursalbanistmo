//*************************************************************************************************
//			 Funcion: Bean Txn 4523
//	  Creado por: Humberto Enrique Balleza Mej�a
//*************************************************************************************************
//CCN -         - 20/10/2006 - Se crea bean para proyecto TIRE
//*************************************************************************************************

package ventanilla.com.bital.sfb;

import com.bital.util.ParameterParser;
import com.bital.util.ParameterNotFoundException;
import ventanilla.GenericClasses;

public class Txn_4523 extends Transaction
{
  public Header doAction(ParameterParser param)
    throws ParameterNotFoundException
  {
    
        FormatoA formatoa = new FormatoA();
		GenericClasses gc = new GenericClasses();
		formatoa.setTxnCode("4523");
		
		
		
		//ODCT 4523A 09004900406  900406  000**7000000000**5000  *US$********MRMDUS33    USD999589PRU98     11**************0000000600*******0000000090*******798798******1061001*850027***
		//ODPA 4523A 00002000251  000251  000**0100000331**100000*N$ ********MRMDUS33    USD000000000       00**************0000000000*******0000000000*******000234******1080425*000000***
		formatoa.setAcctNo(param.getString("txtDDACuenta"));
		formatoa.setTranAmt(param.getCString("txtMonto"));
	    formatoa.setCheckNo2("0000000000");
		formatoa.setCheckNo3("0000000000");
		formatoa.setCheckNo5("000000");
		String ceros="";
		//formatoa.setCashIn("000");
	    formatoa.setCheckNo4(param.getString("registro").substring(1,7));
	 	if( param.getString("fecha4523", null) != null )
		{
		  String fecha1 = param.getString("fecha4523");
		  fecha1 = fecha1.substring(2, 8);
		  formatoa.setAcctNo5("1"+fecha1);
		}
		
		String moneda = gc.getDivisa(param.getString("moneda"));
		formatoa.setTranCur(moneda);
	    if(moneda.equals("N$"))
			//formatoa.setTranDesc(param.getString("lstBanCorresp1")+gc.StringFiller( ceros,12-param.getString("lstBanCorresp1").length(),false," ")+"USD000000000       00");
			formatoa.setTranDesc(param.getString("lstBanCorresp1") + gc.StringFiller(ceros, 12-param.getString("lstBanCorresp1").length(),false," ") + "USD" + param.getString("txtRefTIRE"));
	    else
			//formatoa.setTranDesc(param.getString("lstBanCorresp1")+gc.StringFiller( ceros,12-param.getString("lstBanCorresp1").length(),false," ")+moneda+"000000000       00");
			formatoa.setTranDesc(param.getString("lstBanCorresp1") + gc.StringFiller(ceros, 12-param.getString("lstBanCorresp1").length(),false," ") + moneda + param.getString("txtRefTIRE"));
		    
    //System.out.println(formatoa);
    
    return formatoa;
  }
}