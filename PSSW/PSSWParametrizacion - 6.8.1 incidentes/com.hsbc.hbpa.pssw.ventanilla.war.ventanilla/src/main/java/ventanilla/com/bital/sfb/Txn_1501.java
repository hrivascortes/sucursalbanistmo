//*************************************************************************************************
//             Funcion: Bean Txn 0566
//      Elaborado por: Jesus Emmanuel Lopez Rosales
//*************************************************************************************************
// CCN - 4360576 - 09/03/2007 - Modificaciones para proyecto OPMN
//*************************************************************************************************
package ventanilla.com.bital.sfb;

import com.bital.util.ParameterNotFoundException;
import com.bital.util.ParameterParser;


public class Txn_1501 extends Transaction
{		 
	public Header doAction(ParameterParser param)
		throws ParameterNotFoundException
	{
		FormatoB formatob = new FormatoB();
        formatob.setTxnCode("1501");
        formatob.setFromCurr("N$");
        formatob.setFormat("B");
        formatob.setAcctNo("0372");
        formatob.setTranAmt(param.getCString("txtIvaMN"));
        formatob.setReferenc("IVA COM.CHQ.CAJA");
        formatob.setCashIn(param.getCString("txtIvaMN"));

		return formatob;
	}
	

}
