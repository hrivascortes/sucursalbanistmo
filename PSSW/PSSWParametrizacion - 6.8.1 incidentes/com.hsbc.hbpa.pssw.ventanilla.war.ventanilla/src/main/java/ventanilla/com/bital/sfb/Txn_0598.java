//*************************************************************************************************
//             Funcion: Bean Txn 0598
//           Elemento : Txn_0598.java
//          Creado por: Yair J. Chavez Antonel
//*************************************************************************************************
// CCN - 4360533 - 20/10/2006 - Se crea bean para proyecto OPEE
//*************************************************************************************************
package ventanilla.com.bital.sfb;

import com.bital.util.ParameterNotFoundException;
import com.bital.util.ParameterParser;

import ventanilla.GenericClasses;

public class Txn_0598 extends Transaction{

	
	public Header doAction(ParameterParser param)
		throws ParameterNotFoundException
	{
		
		FormatoB formatob = new FormatoB();
		GenericClasses gc = new GenericClasses();
		
		formatob.setTxnCode("0598");
		formatob.setFormat("B");
		formatob.setAcctNo(param.getString("txtDDACuenta"));
		String moneda = gc.getDivisa(param.getString("moneda"));
		if (!moneda.equals("N$"))
			formatob.setTranAmt(param.getCString("txtMonto"));
		else
			formatob.setTranAmt(param.getCString("txtMonto1"));
		if (moneda.equals("N$"))
			formatob.setFromCurr(gc.getDivisa(param.getString("moneda2")));
		else
			formatob.setFromCurr("N$");
		//formatob.setReferenc(param.getString("lstCatDivisa")+param.getCString("txtMonto1"));
		if (moneda.equals("N$"))
			formatob.setReferenc(gc.getDivisa(param.getString("moneda2"))+param.getCString("txtMonto1"));
		else
		formatob.setReferenc(gc.getDivisa(param.getString("moneda2"))+param.getCString("txtMonto"));
		return formatob;
	}
}
