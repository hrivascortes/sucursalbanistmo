//************************************************************************************************* 
//             Funcion: Clase de enviar valores por Referencia a un SQL
//            Elemento: SQLRefer.java
//          Creado por: Fausto Rodrigo Flores Moreno
//*************************************************************************************************
// CCN - 4360845 - 13/02/2009 - FFM - Clase para paso de valores por referencia al SLQ
//*************************************************************************************************

package ventanilla.com.bital.util;

public class SQLRefer {
	private int type = 0;
	private Object val = null;
	public static final int STRING = 1;
	public static final int LONG   = 2;
	public static final int INT    = 3;
	
	public SQLRefer (int t, Object v){
		type = t;
		if(t == LONG){val = new Long((String)v);}
			
		if(t == STRING){val = v;}
					
		if(t == INT){val =  Integer.valueOf((String)v);}
			
	}
	
	public int getType (){
		return type;		
	}
	
	public Integer getInt (){
		return (Integer)val;
	}
	
	public Long getLong(){
		return (Long)val;
	}
	
	public String getString(){
		return (String)val;
	}	
}
