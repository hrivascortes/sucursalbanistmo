//*************************************************************************************************
//          Funcion: Clase especial para transacciones de SUAS
//          Elemento: SpFunc_SUAS.java
//          Creado por: Fausto Rodrigo Flores Moreno
//          Modificado por: Jesus Emmanuel Lopez Rosales
//*************************************************************************************************
// CCN - 4360602 - 18/05/2007 - Se realizan modificaciones para proyecto Codigo de Barras SUAS
// CCN - 4360615 - 08/06/2007 - Se genera paquete a petici�n de QA
//*************************************************************************************************
package ventanilla.com.bital.admin;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Hashtable;
import java.util.StringTokenizer;
import java.util.Vector;

import javax.servlet.http.HttpSession;

import ventanilla.GenericClasses;
import ventanilla.com.bital.beans.DatosVarios;

import com.bital.util.ConnectionPoolingManager;

public class SpFunc_SUAS implements InterfazSF{
	
	public SpFunc_SUAS(){			
	}		
	
	public int SpFuncionBD(Object bean, Vector Respuesta, HttpSession session, String Connureg){
		int messagecode = 0;
		return messagecode;
	}

	public int SpFuncionAD(Object bean, Vector Respuesta, HttpSession session, String Connureg){
		int messagecode = 0;								

		GenericClasses gc = new GenericClasses();				
		Hashtable datasession = (Hashtable)session.getAttribute("page.datasession");
		java.util.Stack flujotxn = new java.util.Stack();
		String ambiente = "";
		javax.naming.Context initialContext = null;
		
		try{	
			initialContext = new javax.naming.InitialContext();
			ambiente = (String)initialContext.lookup("java:comp/env/AMB");
		}catch( javax.naming.NamingException namingException ){
			System.out.println( "Error SpFunc_SUAS::Context.lookup: [" + namingException.toString() + "]");
		}

		if(session.getAttribute("page.flujotxn") != null)
		  	flujotxn = (java.util.Stack)session.getAttribute("page.flujotxn");
		
		String status = "";
		String szResp = (String)Respuesta.elementAt(3);
   	   	status = szResp.substring(29,30);
		
   	   	String mensaje="";
		int lineas = Integer.parseInt((String)Respuesta.elementAt(1));

   	   	if(lineas == 3)
	   	{
			if (status.equals("N"))
		    	{mensaje = "Registro Patronal no Localizado en la Base de Datos. <br> Verificar en la Subdirecci�n del IMSS correspondiente";}
		    else if (status.equals("B"))
		   	    {mensaje = "Base de Datos no Disponible.";}
		    else if (status.equals("D"))
		        {mensaje = "Datos no Disponibles.";}
			else if (status.equals("E"))
		    	{mensaje = "Error al Leer el Archivo.";}
		    else if (status.equals("M"))
		        {mensaje = "Diferencia de Montos.";}
		   	else if (status.equals("A"))
		        {mensaje = "Pago ya Realizado.";}
	   	}
	   	else if(lineas == 9)
	   	{
			String status2 =szResp.substring(398,399);
			String status3 =szResp.substring(399,400);
			String nobim="0";
			String nomen="0";
			if (status2.equals("A"))
			nomen = "1"; 			
			if (status3.equals("A"))
			nobim = "1";
			datasession.put("txtnobim",nobim);
			datasession.put("txtnomen",nomen);
       	    String date = gc.getDate(1);
			
    	    int traRes = Integer.parseInt(szResp.substring(390,395));
    	  	int diaActual = Integer.parseInt(date.substring(6,8));
    	  	String AnioActual =(date.substring(2,4));
    	  	int perRes = Integer.parseInt(szResp.substring(33,35));
    	  	int mesActual = 0;
    	  	if(ambiente.toLowerCase().equals("q")){
				mesActual = getFechaQA();
				if(mesActual == 0){
					mesActual = Integer.parseInt(date.substring(4,6));
					System.err.println("NO EXISTE FECHA EN TC_DATOS_VARIOS PARA AMB DE QA");
				}
    	  	}else
    	  		mesActual = Integer.parseInt(date.substring(4,6)); 

    	    int i=0;
    	    int traPCD=0;
    	    int diaLimite=0;
    	    
		    String data = "";      
		    String dataTmp= "";
		    String datos ="";
		    String datosdef ="";
		    String periodo=" ";
		    String numeroControl="";
		    /******** Informaci�n de Datos Varios - PCD **********/
		    DatosVarios dV = DatosVarios.getInstance();                   
		    Vector vec = new Vector();        
		    vec =(Vector)dV.getDatosV("SUASPCD");
			data = (String)vec.get(0);
		    StringTokenizer dato = new StringTokenizer(data,"*");
		    boolean find = false;
		   	i = 1;
    	 	while (dato.hasMoreTokens())
    	 	{ 
    	 	   datos = dato.nextToken().toString(); //al finalizar el recorrido guarda el numero de trabajadores
    	 	   if (i == mesActual)
    	 	        datosdef = datos;
    	 	   i++;
    	 	}
    	 	
    	   	traPCD = Integer.parseInt(datos);
    	 	diaLimite = Integer.parseInt(datosdef.substring(2,4));
    	 	int resta = mesActual-perRes;

    	 	if (traRes > traPCD ){
    	 		mensaje = "El n�mero de trabajadores es mayor al permitido";
    	 		status = "NOK";
    	 	}
            else
            {
            	if (diaActual > diaLimite){/*11*/
              		mensaje = "Fecha de pago inv�lida - Pas� Fecha L�mite";
              		status = "NOK";
		 		}
            	else
            	{
	            	if ((mesActual-perRes)!= 1 && (mesActual-perRes)!= -11){
			   	 		mensaje = "Fecha de pago inv�lida - Fuera de periodo";
			   	 		status = "NOK";
					}
		            else
		            {
						if (mesActual == 1 || mesActual == 3 || mesActual == 5 || mesActual == 7 || mesActual == 9 || mesActual == 11)
						   periodo ="B";
						else	
						   periodo ="M";
						if (periodo == "M" && nomen == "1"){
							mensaje = "Pago Mensual ya realizado";
							status = "NOK";
						}
						else
						{  
							datasession.put("txtPagoPeriodo",periodo);
           			 		datasession.put("txtPeriodo",szResp.substring(29,35));
					 		datasession.put("suasConsec",szResp.substring(41,47));
					 		datasession.put("txtNombre",szResp.substring(234,274)+szResp.substring(312,317));
					 		datasession.put("txtRCV",gc.FormatAmount(szResp.substring(468,481)));
					 		datasession.put("txtVIV",gc.FormatAmount(szResp.substring(484,497)));
					 		datasession.put("txtACV",gc.FormatAmount(szResp.substring(546,559)));
					 		datasession.put("txtEMI1",gc.FormatAmount(szResp.substring(562,575)));

            	 			/***** Suma montos Infonavit **********************/
	 						long SumaBimestral= (Long.parseLong(szResp.substring(468,481))+Long.parseLong(szResp.substring(484,497))+
	 						Long.parseLong(szResp.substring(546,559)));
	 						datasession.put("txtEMI2",gc.FormatAmount(Long.toString(SumaBimestral)));
            	 						
	 						/******  Numero de Control *************************/
	 						String juliana = gc.stringFormat(java.util.Calendar.DAY_OF_YEAR, 0);
	 						juliana = gc.StringFiller(juliana,3, false, "0");
	 						numeroControl = '7'+AnioActual+juliana+szResp.substring(41,47);            	 					
	 						datasession.put("txtnumctrl",numeroControl);
	        	 			status = "OK";
					 		if(!flujotxn.isEmpty())
	            	 		{
						 		flujotxn.removeAllElements();
	            	 			flujotxn.push("S772");
					 			session.setAttribute("page.flujotxn", flujotxn);
	            	 		}
						}
 			        }
		       	}
			}
   	   }
	   datasession.put("mensajeSUAS",mensaje);
	   datasession.put("statusSUAS",status);
	   session.setAttribute("page.datasession",datasession);
	   return messagecode;
	}
	
	public int SpFuncionBR(Hashtable txnRev, Hashtable txnOrg, HttpSession session, String Connureg, String Supervisor){
		int messagecode = 0;
		return messagecode;
	}
	
	public int SpFuncionAR(Hashtable txnRev, Hashtable txnOrg, HttpSession session, String Connureg, String Cosecutivo, String Supervisor, Vector VResp){
		int messagecode = 0;
		return messagecode;
	}
	
	private int getFechaQA(){
		int newMes = 0;
		Connection pooledConnection = ConnectionPoolingManager.getPooledConnection();
		PreparedStatement select = null;
		ResultSet res = null;
		String Mestmp = "";
		try {
			if(pooledConnection != null){				
				select = pooledConnection.prepareStatement("SELECT D_DATOS FROM TC_DATOS_VARIOS WHERE C_CLAVE = ?");
				select.setString(1, "FSUASQA"); 
				res = select.executeQuery();
				if(res != null){
					if(res.next())
						Mestmp = res.getString("D_DATOS").trim();
				}
				else{
					Mestmp = "";
				}
			}
		}catch(SQLException sqlException){
			System.err.println("SpFunc_SUAS::getFechaQA [" + sqlException.toString() + "]");
		}finally {
			try {
				if(select != null) {
					select.close();
					select = null;
				}
				if(pooledConnection != null) {
					pooledConnection.close();
					pooledConnection = null;
				}
			}
			catch(java.sql.SQLException sqlException){
				System.err.println("SpFunc_SUAS::getFechaQA:Finally [" + sqlException.toString() + "]");
			}
		}
		if(!Mestmp.equals(""))
			newMes = Integer.parseInt(Mestmp);

		return newMes;
	}
}							
