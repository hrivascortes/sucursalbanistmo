//*************************************************************************************************
//             Funcion: Clase que verifica los montos maximos para txns varias
//          Creado por: Rutilo Zarco Reyes
//      Modificado por: Juvenal Fernandez
//*************************************************************************************************
// CCN 4360160- 30/06/2004 - Se incluye presistencia de datos sobre tabla TC_MTS_MXMOS_X_TXN
// CCN - 4360334 - 17/06/2005 - Se elimina variable dbQualifier
// CCN - 4360390 - 07/10/2005 - Se realiza validacion para txn 1053.
//*************************************************************************************************

package ventanilla.com.bital.admin;

import java.util.Vector;
import ventanilla.com.bital.beans.MtosMax;

public class LimiteTransacciones 
{
    private String update = "";
    private String StatusCons = "";
    String statementPS = com.bital.util.ConnectionPoolingManager.getStatementPostFix();
    
    private String delCommaPointFromString(String newCadNum) 
    {
        String nCad = new String(newCadNum);
        if( nCad.indexOf(".") > -1)
        {
            nCad = nCad.substring(nCad.indexOf(".") + 1, nCad.length());
            if(nCad.length() != 2)
            {
                String szTemp = new String("");
                for(int j = nCad.length(); j < 2; j++)
                    szTemp = szTemp + "0";
                newCadNum = newCadNum + szTemp;
            }
        }
        
        StringBuffer nCadNum = new StringBuffer(newCadNum);
        for(int i = 0; i < nCadNum.length(); i++)
            if(nCadNum.charAt(i) == ',' || nCadNum.charAt(i) == '.')
                nCadNum.deleteCharAt(i);
        
        return nCadNum.toString();
    }
    
    public String getMontosLimite(String Proceso, String itxn, String otxn, String ctxn, String monedatxn, String montotxn) 
    {
        long MontoMax = 0;
        String MontoAux = "";
        
        String Codtxn = "";
        if  (ctxn.equals("1053"))
           return StatusCons;
        if (Proceso.equals("03")){                                            // Compra / Venta
            if (ctxn.equals("0825") || ctxn.equals("0821") || ctxn.equals("0823"))                    // ACDO , Tipo de cambio
                return StatusCons;
            Codtxn = itxn + "*" + otxn + "*";                                  // Transaccion de entrada y salida
        }
        else
            Codtxn = ctxn+ "*";
        
        MtosMax mtos = MtosMax.getInstance();
        Vector Rselect = mtos.getMtosM(Codtxn);
       
        if(!(Rselect.isEmpty()))
        {
            montotxn = delCommaPointFromString(montotxn);                         // elimina puntos y comas
            long Monto = Long.parseLong(new String(montotxn));
            if (monedatxn.equals("01"))
                MontoAux =  (String)Rselect.get(1);
            else if (monedatxn.equals("02") || Proceso.equals("03"))
                MontoAux = (String)Rselect.get(2);
            else
                return StatusCons;
            
            MontoMax =  Long.parseLong(MontoAux.trim());
            if ( Monto > MontoMax ) 
            {
                StatusCons = "1~01~2~Transaccion Rechazada: Monto Rebasa el limite permitido para la Transaccion~";
                return StatusCons;
            }
        }
        return StatusCons;
    }
}
