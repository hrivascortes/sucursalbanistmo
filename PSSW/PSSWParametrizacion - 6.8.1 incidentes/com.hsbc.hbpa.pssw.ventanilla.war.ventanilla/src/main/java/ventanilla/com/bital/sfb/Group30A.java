// Transaccion 0810 - Cheques de Caja
package ventanilla.com.bital.sfb;

import com.bital.util.ParameterParser;
import com.bital.util.ParameterNotFoundException;
import ventanilla.GenericClasses;

public class Group30A extends Transaction
{
  public Header doAction(ParameterParser param)
    throws ParameterNotFoundException
  {
    // Instanciar el bean a utilizar
    Cheques cheque = new Cheques();
    
    // Ingresar datos requeridos
    cheque.setFormat("B");
	GenericClasses gc = new GenericClasses();

    cheque.setAcctNo  ( "01" );
    cheque.setTranAmt ( param.getCString("txtMonto") );
    cheque.setReferenc( "1" );

    int moneda = param.getInt("moneda");
	cheque.setFromCurr( gc.getDivisa(param.getString("moneda")));

    return cheque;
  }               
}