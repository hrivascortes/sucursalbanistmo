//*************************************************************************************************
//             Funcion: Bean consulta comisiones
//          Creado por: Rutilo Zarco Reyes.
//      Modificado por: Fausto Rodrigo Flores Moreno
//*************************************************************************************************
// CCN - 4360446 - 10/03/2006 - Proyecto TDI fase 2 cobro de comision
// CCN - 4360492 - 23/06/2006 - Se realizan modificaciones para el esquema de Cobro Comisi�n Usuarios RAP
//*************************************************************************************************

package ventanilla.com.bital.admin;
import java.util.Hashtable;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import ventanilla.GenericClasses;
import ventanilla.com.bital.beans.BinesAMEX;
import ventanilla.com.bital.beans.BinesTDC;
import ventanilla.com.bital.sfb.RAPG;
import ventanilla.com.bital.util.DBQuery;

public class ConsultaTDI {

    public String TarjetaTDI = null;
    public Vector pcdcode = null;
    public int pcdok = 0;
    public String respuesta = "0~01~010101~Lectura de Tarjeta TDI Exitosa~";;
    
	public ConsultaTDI() {
	}

    public void execute(HttpSession session, HttpServletRequest request)
    {
		GenericClasses gc = new GenericClasses();
        Hashtable datasession = (Hashtable)session.getAttribute("page.datasession");
        String dirip = (String)session.getAttribute("dirip");
       //Realiza lectura de Tabla de Transacciones Frecuetes Loch Ness
//       String tarjeta = request.getParameter("tarjetaTDI");
       String tarjeta = (String)datasession.get("tarjetaTDI");
       session.setAttribute("Tarjeta.TDI",tarjeta);
       Vector TxnTdi = execute(tarjeta);
       Vector dataxTxn = new Vector();
       datasession.put("Txn.Tdi",TxnTdi);
       if (TxnTdi.size() == 0)
          respuesta = "0~01~010101~La Tarjeta TDI no tiene Transacciones Frecuentes, Seleccione un servicio.~";
       //Consultar Comision e Iva por Servicio
       for(int i=0; i<TxnTdi.size(); i++)
       {
       	 Hashtable info = new Hashtable();
       	 pcdcode = new Vector();
       	 Vector fields = (Vector)TxnTdi.get(i);
       	 String servicio = (String)fields.get(4);
       	 servicio = servicio.trim();
       	 String refer1 = (String)fields.get(6);
       	 refer1 = refer1.trim();
         int  spaces = 7 - servicio.length();
         for (int j=1; j < spaces+1; j++)
            servicio = servicio + " ";
         RAPG rapg = new RAPG();
         rapg.setBranch((String)datasession.get("sucursal"));
         rapg.setTeller((String)datasession.get("teller"));
         rapg.setSupervisor((String)datasession.get("supervisor"));
         rapg.setTxnCode("0832");
         rapg.setFormat("G");
         String moneda = (String)datasession.get("moneda");
		 rapg.setToCurr(gc.getDivisa(moneda));
         rapg.setTranAmt("1");
         rapg.setAcctNo("1");
         rapg.setBenefic(servicio);
         Diario diario = new Diario();
         String sMessage = diario.invokeDiario(rapg, (String)session.getAttribute("d_ConsecLiga"), dirip, moneda, session);
         String codigo = null;
         StringBuffer respuestaSB = new StringBuffer(sMessage);
         respuesta = sMessage;
         if( respuestaSB.charAt(0) == 'O' )
         {
            respuesta = "1~01~010101~ERROR GENERAL DE COMUNICACIONES~";
            break;
         }
         Vector cics_resp = new Vector();
         ventanilla.com.bital.util.NSTokenizer parser = new ventanilla.com.bital.util.NSTokenizer(respuesta, "~");
         while( parser.hasMoreTokens() ) {
           String token = parser.nextToken();
           if( token == null )
               continue;
           cics_resp.addElement(token);
        }
    	try
	    {
          codigo = (String)cics_resp.elementAt(0);
          int lines = Integer.parseInt((String)cics_resp.elementAt(1));
          String szResp = (String)cics_resp.elementAt(3);
          if( !codigo.equals("0") )
          {
          	 String line = CheckResp(lines,szResp);
             respuesta = "1~01~010101~"+line+"~";
             break;
          }
          else
          {
     		// OBTENCION DE CODIGOS DE PCD - 4 SERVICIOS
	        pcdcode.addElement(szResp.substring(156,157));
     	    pcdcode.addElement(szResp.substring(169,170));
	        pcdcode.addElement(szResp.substring(182,183));
     	    pcdcode.addElement(szResp.substring(234,235));
     	    validaservicio();
            if (respuesta.startsWith("1"))
              break;
    		// LONGITUDES DE REFERENCIA 1 - 2 - 3
			int pini = 156; // servicio 1
            info.put("Long1",szResp.substring(pini+1,pini+3));
            info.put("Long2",szResp.substring(pini+3,pini+5));
            info.put("Long3",szResp.substring(pini+5,pini+7));
     		// OBTENCION DE FORMAS DE PAGO
	    	String dato = "";
			int NNN = 0;
			for (int z=7; z<10; z++)
		    {
			  dato = szResp.substring(pini+z,pini+(z+1));
			  if (dato.equals("S") || dato.equals("N"))
			  {
                info.put("FP"+(z-6), dato);
			    if(dato.equals("N"))
			      NNN = NNN + 1;
			  }
		      else
              {
                info.put("FP"+(z-6), "S");
              }
			}
           // VALIDACION DE FORMA DE PAGO -NNN-
           if (NNN == 3)
  			 pcdok = 8;
           // OBTENCION DE COMPROBANTE DE PAGO
           dato = szResp.substring(pini+10,pini+11);
		   if (!dato.equals("S") && !dato.equals("N"))
              dato = "N";
           info.put("CompPago", dato);
           // OBTENCION DE VALIDACION DE LONGITUDES DE REFERENCIAS
           dato = szResp.substring(pini+11,pini+12);
  		   if (!dato.equals("S") && !dato.equals("N"))
              dato = "N";
           info.put("ValRefs", dato);
           // OBTENCION DE COMISION E IVA DE COMISION
		   pini = 312;  //Servicio 1
           info.put("Com", szResp.substring(pini,pini+9)); //Comision General de Servicio
           info.put("Iva", szResp.substring(pini+9,pini+11));
           // OBTENCION DE COBRO COMISION, COMISION CLIENTE, COMISION NO CLIENTE Y FLAG DE REFERENCIAS
           pini = 546;
           info.put("CobCom", szResp.substring(pini,pini+1)); 
           info.put("ComCli", szResp.substring(pini+1,pini+5)); //Comision Cliente
           info.put("ComNoCli", szResp.substring(pini+5,pini+9)); //Comision No Cliente
           info.put("FlagRef", szResp.substring(pini+9,pini+10)); //1 Servicio
           info.put("pcdok", Integer.toString(pcdok));
           
           //OBTENCI�N DE FLAG PARA DETERMINAR TXN DE COMISI�N A UTILIZAR (0736 o 0792)
           pini = 556;
           if(szResp.substring(pini,pini+1).equals("2")){
		     info.put("ComServRAP", "1");//txn 792
		   }else{
		     info.put("ComServRAP", "0");//txn 736
           }
           
           //Calculo de comision e iva.
           int mtocom  = 0;
           int mtocom1 = 0;
           int mtocom2 = 0;
           int mtoiva  = 0;
           int iva     = 0;
           String cobrocom = null;
           if (pcdok == 2)
           {
           	 mtocom1 = Integer.parseInt((String)info.get("Com"));
//System.out.println("Comision General por Servicio :" + mtocom1);
           	 String CobCom = (String)info.get("CobCom");
             if(CobCom.equals("S"))
             { //Comision cliente
               mtocom2 = Integer.parseInt((String)info.get("ComCli"));
             }
              // SUMA DE COMISIONES
             mtocom = mtocom1 + mtocom2;
              // CONTROL PARA COBRO DE COMISION
         //Valida si el bin es cmtH
//System.out.println("Comision Total por Servicio :" + mtocom);
//System.out.println("servicio -" + servicio + "-");
//System.out.println("refer 1 -" + refer1 + "-");
          String serv = servicio.trim();
          if (serv.equals("60"))
          {
            if (serv60(refer1, "isbin")) //No es Bin HSBC
               mtocom = mtocom - mtocom1;
          }
//
             if (mtocom > 0)
               cobrocom = "S";
             else
               cobrocom = "N";
             mtoiva = mtocom;
             iva = Integer.parseInt((String)info.get("Iva"));
             mtoiva = mtocom * iva / 100;
             info.put("Monto.Comision",Integer.toString(mtocom));
             info.put("Monto.Iva",Integer.toString(mtoiva));
             info.put("Cobra.Comision",cobrocom);
           }
           dataxTxn.add(i,info);
          }
  	    }
     	catch(NumberFormatException nfe)
	    {
            respuesta = "1~01~010101~ERROR GENERAL DE DATOS~";
	    	break;
	    }
       }
       datasession.put("DataxTxn.Tdi",dataxTxn);
    }

    public String CheckResp(int lines, String szResp)
    {
      String line = null;
      for(int i=0; i<lines; i++)
      {
        line = szResp.substring( i*78, Math.min(i*78+77, szResp.length()) );
        if(Math.min(i*78+77, szResp.length()) == szResp.length())
           break;
      }
      return line;
    }	
    public String getrespuesta()
    {
    	return respuesta;
    } 
    public void validaservicio()
    {
       for (int x=0; x<pcdcode.size(); x++)
       {
         if ((pcdcode.elementAt(x).toString()).equals("3"))
         {
            respuesta = "1~01~010101~NO EXISTE SERVICIO~";
            break;
         }
         if ((pcdcode.elementAt(x).toString()).equals("4"))
         {
           respuesta = "1~01~010101~SERVICIO EN AMBAS PCDS~";
           break;
         }
       }
       if((pcdcode.elementAt(0).toString()).equals("1"))
          pcdok = 1;
       else
          pcdok = 2;
    }
    
    public Vector execute(String tarjeta)
    {
        String sql = null;
        sql = "SELECT D_TARJETA_TDI,D_FRECUENCIA,D_NUM_CIS,D_TXN,D_SERVICIO,D_NOM_SERV,D_REFER1,D_REFER2,D_REFER3,D_FECHULT_TXN,D_STATUS,D_CONTA_TXN,D_MONTO,D_COMISION,D_IVA,D_TOTAL FROM ";
        String clause = " WHERE D_TARJETA_TDI='" + tarjeta + "' ORDER BY D_FRECUENCIA ASC,D_FECHULT_TXN ASC";
        DBQuery dbq = new DBQuery(sql,"TW_TXNFREC_LN", clause);
        Vector Regs = dbq.getRows(); //All records
        Vector fiverecord = new Vector();
        if (Regs.size() > 0)
        {
          int txns = 3; //5
          if (Regs.size() <= (txns-1)) 
            txns = Regs.size();
          int numrecord = 0;
          for(int i=0; i<txns; i++) //Solo 3 transacciones frecuentes
          {
          	Vector record = (Vector)Regs.get(i);
          	String servicio = (String)record.get(4);
          	String refer1 = (String)record.get(6);
          	for (int x=0; x<servicio.length(); x++)
          	{
              if(servicio.charAt(x) != '0')
              {
              	servicio = servicio.substring(x);
              	break;
              }
          	}
          	record.set(4,servicio);
            String serv60 = servicio.trim();
//System.out.println("Es servicio 60 :" + serv60);
            if (serv60.equals("60")) //Agrega servicio 60 con Bin que exista
            {
               if (serv60(refer1, "isfind"))
               {
                 fiverecord.add(numrecord,record);
                 numrecord++;
               }
            }
            else
            {
               fiverecord.add(i,record);
               numrecord++;
            }
          }
        }
     return fiverecord;
   }
   
   public boolean serv60(String refer1, String validar)
   {
   	 boolean status = false;
       BinesTDC tdcbines = BinesTDC.getInstance();
       Vector bines = tdcbines.getBines();
       for (int zz=0; zz<bines.size(); zz++)
       {
        	Vector record = (Vector)bines.get(zz);
         	String bin = (String)record.get(0);
         	int banco = Integer.parseInt((String)record.get(1));
         	if (refer1.indexOf(bin) > -1)
         	{
         	   if(banco == 1 || validar.equals("isfind")) //bin cmtH
         	   {
         	   	 status = true;
                 break;
         	   }
           	}
       }
       return status;
   }
   
   public boolean serv670(String refer1, String validar)
   {
	 boolean status = false;
	   BinesAMEX tdcbines = BinesAMEX.getInstance();
	   Vector bines = tdcbines.getBines();
	   for (int zz=0; zz<bines.size(); zz++)
	   {
			Vector record = (Vector)bines.get(zz);
			String bin = (String)record.get(0);
			int banco = Integer.parseInt((String)record.get(1));
			if (refer1.indexOf(bin) > -1)
			{
			   if(banco == 1 || validar.equals("isfind")) //bin AMEX
			   {
				 status = true;
				 break;
			   }
			}
	   }
	   return status;
   }
}

