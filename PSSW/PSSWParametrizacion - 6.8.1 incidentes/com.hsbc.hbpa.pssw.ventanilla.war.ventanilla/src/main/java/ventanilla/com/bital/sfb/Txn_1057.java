//*************************************************************************************************
//             Funcion: Bean Txn 1057
//      Modificado por: Alejandro Gonzalez Castro
//*************************************************************************************************
// CCN - 4360268 - 04/02/2005 - Se realizan adecuaciones para eliminar Agencia NY
// CCN - 4360424 - 03/02/2006 - Se agrega para txn combinada
//*************************************************************************************************
package ventanilla.com.bital.sfb;

import com.bital.util.ParameterParser;
import com.bital.util.ParameterNotFoundException;
import ventanilla.GenericClasses;

public class Txn_1057 extends Transaction
{
	public Header doAction(ParameterParser param)
		throws ParameterNotFoundException
	{
		FormatoA formatoa = new FormatoA();
		GenericClasses gc = new GenericClasses();
		formatoa.setTxnCode("1057");
		
		formatoa.setAcctNo(param.getString("txtDDARetiro"));
		formatoa.setTranAmt(param.getCString("txtMonto"));

	    if (param.getString("override") != null)
       		if (param.getString("override").equals("SI"))
           		formatoa.setOverride("3");

		String moneda = gc.getDivisa(param.getString("moneda"));
		formatoa.setTranCur(moneda);

		return formatoa;
	}
}
