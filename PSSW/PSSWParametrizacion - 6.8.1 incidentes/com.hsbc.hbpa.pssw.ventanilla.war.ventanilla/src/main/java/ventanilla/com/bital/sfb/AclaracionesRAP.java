package ventanilla.com.bital.sfb;

public class AclaracionesRAP extends Header
{
  private String txtEffDate = "";
  private String txtAcctNo = "";
  private String txtTranAmt = "";
  private String txtTranCur = "";
  private String txtTranDesc = "";
  private String txtAcctNo5 = "";
  private String txtDescRev = "";
  
  public String getEffDate()
  {
	return txtEffDate + "*";
  }    

  public String getAcctNo()
  {
	return txtAcctNo + "*";
  }
  
  public String getTranAmt()
  {
	return txtTranAmt + "*";
  }    
  
  public String getTranCur()
  {
	return txtTranCur + "*";
  }    
  
  public String getTranDesc()
  {
	return txtTranDesc + "*";
  }    
  
  public String getAcctNo5()
  {
	return txtAcctNo5 + "*";
  }
  
  
  
  public void setEffDate(String newEffDate)
  {
	txtEffDate = newEffDate;
  }
  
  public void setAcctNo(String newAcctNo)
  {
	txtAcctNo = newAcctNo;
  }  
  
  public void setTranAmt(String newTranAmt)
  {
	txtTranAmt = newTranAmt;
  }
  
  public void setTranCur(String newTranCur)
  {
	txtTranCur = newTranCur;
  }
  
  public void setTranDesc(String newTranDesc)
  {
	txtTranDesc = newTranDesc;
  }
  
  public void setAcctNo5(String newAcctNo5)
  {
	txtAcctNo5 = newAcctNo5;
  }  

  public String getDescRev()
  {
	return txtDescRev;
  }    
  
  public void setDescRev(String newDescRev)
  {
	txtDescRev = newDescRev;
  }
  
  public String toString()
  {
	return super.toString() + getEffDate() + getAcctNo() + "*"
	  + getTranAmt() + getTranCur() + "*******" + getTranDesc()
	  + "*********************************" + getAcctNo5()
	  + "***";
  }    
}
