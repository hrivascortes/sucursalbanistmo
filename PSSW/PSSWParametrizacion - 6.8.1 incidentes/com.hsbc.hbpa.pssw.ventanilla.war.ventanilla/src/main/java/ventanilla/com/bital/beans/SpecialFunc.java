//*************************************************************************************************
//             Funcion: Clase que realiza la consulta de funciones especiales y las conserva en memoria
//          Creado por: Fausto Rodrigo Flores Moreno
//          Modificado por: Jesus Emmanuel Lopez Rosales
//*************************************************************************************************
// CO - CR7564 - 26/01/2011 - Se crea clase para cargar en persistencia las funciones especiales para el esquema de After y before posting
// CO - CR7564 - 26/01/2011 - Cambios Proyecto camp
//*************************************************************************************************

package ventanilla.com.bital.beans;

import ventanilla.com.bital.util.DBQuery;

import java.util.Vector;
import java.util.Hashtable;

public class SpecialFunc
{
     private Hashtable funciones = null;
     private static SpecialFunc instance = null;
     
    private SpecialFunc() 
    {
        funciones = new Hashtable();
        execute();
    }
    
    public static SpecialFunc getInstance()
    {
        if(instance == null){
            synchronized (SpecialFunc.class){
            	if(instance == null)
            		instance = new SpecialFunc();
            }
        }
        return instance;
    }
    
    public Hashtable getFunciones()
    {
        return this.funciones;
    }
    
    public void execute() 
    {
        String sql = null;
        sql = "SELECT C_CODE_FUNC, D_NOMBRE_FUNC FROM ";
        String clause = null;
        clause = " ORDER BY C_CODE_FUNC";
        
        DBQuery dbq = new DBQuery(sql,"TC_SPFUNC", clause);
        Vector Regs = dbq.getRows();
        
        int size = Regs.size();
        Vector row = new Vector();
        String code = null;
        String name = null;
        
        for(int i=0; i<size; i++)
        {
            row = (Vector)Regs.get(i);
            code = row.get(0).toString();
            name = row.get(1).toString().trim();
            row.remove(0);
            row.remove(0);
            funciones.put(code, name);
            
        }
    }
}
