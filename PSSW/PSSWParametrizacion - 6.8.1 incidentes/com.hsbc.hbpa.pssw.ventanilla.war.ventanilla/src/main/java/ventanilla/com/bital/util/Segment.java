package ventanilla.com.bital.util;

public abstract class Segment
{
  private static String ERROR_CODE = "64372";
  private static String ERROR_DESC = "ERROR DE COMUNICACION CON HOST";
  
  private int    status      = -1;
  private String code        = null;
  private String description = null;
  
  public abstract String getMessage();
  public abstract void   parse(PASSResponse response);
  
  public void update()
  {
  }
  
  public int getStatus()
  {
    return status;
  }
  
  public void setStatus(int status)
  {
    this.status = status;
  }
  
  public String getCode()
  {
    if( status < 0 )
      return ERROR_CODE;
    return code;
  }
  
  public void setCode(String code)
  {
    this.code = code;
  }
  
  public String getDescription()
  {
    if( status < 0 )
      return ERROR_DESC;
    return description;
  }
  
  public void setDescription(String description)
  {
    this.description = description;
  }
}