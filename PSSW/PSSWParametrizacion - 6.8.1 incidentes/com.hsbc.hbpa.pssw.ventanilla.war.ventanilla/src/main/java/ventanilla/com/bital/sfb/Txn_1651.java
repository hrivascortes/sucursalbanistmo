//*************************************************************************************************
//             Funcion: Bean Txn 1651
//           Elemento : Txn_1651.java
//          Creado por: Fausto Rodrigo Flores Moreno
//*************************************************************************************************
// CCN - 4360493 - 23/06/2006 - Se crea Bean para txn Depto 9005
//*************************************************************************************************
package ventanilla.com.bital.sfb;

import com.bital.util.ParameterParser;
import com.bital.util.ParameterNotFoundException;
import ventanilla.GenericClasses;

public class Txn_1651 extends Transaction
{
	public Header doAction(ParameterParser param)
		throws ParameterNotFoundException
	{
		FormatoA formatoa = new FormatoA();
		GenericClasses gc = new GenericClasses();

		formatoa.setTxnCode("1651");
		formatoa.setAcctNo(param.getString("txtDDACuenta"));
		formatoa.setTranAmt(param.getCString("txtMonto"));
		formatoa.setCheckNo(param.getCString("txtSerial"));
		String moneda = gc.getDivisa(param.getString("moneda"));
		formatoa.setTranCur(moneda);
		formatoa.setTranDesc("01 COMISION SERVICIO COB DOCUMENTARIA");
		formatoa.setDescRev("01REV. COMISION SERVICIO COB DOCUMENTAR");
		formatoa.setMoAmoun("000");
        	return formatoa;
	}
}
