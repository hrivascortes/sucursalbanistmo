package ventanilla.com.bital.admin;

import ventanilla.GenericClasses; 

public class PresentaTotalesC {
  String sql = "";
  String tmp = "";
  String txtCajero = "";
  String txtMoneda = "";
  String StringFormat = "TOTALES~";
  String statementPS = com.bital.util.ConnectionPoolingManager.getStatementPostFix();
  String dbQualifier = com.bital.util.ConnectionPoolingManager.getdbQualifier();
  
  int flag1 = 1;
  int flag2 = 1;
  
  int Cont1 = 0;
  int Cont2 = 0;
  int NoFilasCargos = 0;
  int NoFilasAbonos = 0;
  
  public void setCajero(String newCajero) {
    txtCajero = newCajero;
  }
  
  public void setMoneda(String newMoneda) {
    txtMoneda = newMoneda;
  }
  
  public String getCajero() {
    return txtCajero;
  }
  
  public String getMoneda() {
    return txtMoneda;
  }
  
  //***
  // FORMATEA NUMEROS PARA PRESENTARLOS CON COMAS Y PUNTOS
  //***
  public String comas(String nvacantidad, int nvaveces) {
    StringBuffer tmp1 = new StringBuffer();
    int posicionini = nvacantidad.length() - 2 ;
    int posicion1 = 0;
    int posicion2 = 0;
    while(posicionini > 3)
      posicionini = posicionini - 3;
    
    tmp1.append(nvacantidad.substring(0,posicionini)+ ",");
    nvaveces = nvaveces - 1;
    
    if (nvaveces >=1){
      posicion1 = posicionini;
      posicion2 = posicionini + 3;
      while(nvaveces >= 1){
        tmp1.append(nvacantidad.substring(posicion1,posicion2) + ",");
        nvaveces = nvaveces - 1;
        posicion1 = posicion2;
        posicion2 = posicion2 + 3;
      }
      tmp1.append(nvacantidad.substring(posicion1));
    }
    
    else
      tmp1.append(nvacantidad.substring(posicionini));
    nvacantidad = tmp1.toString();
    return nvacantidad;
  }// fin metodo comas
  
  public String puntodecimal(String nvacantidad) {
    int length = nvacantidad.length() ;
    StringBuffer tmp = new StringBuffer();
    if (length > 2){
      tmp.append(nvacantidad.substring(0,length - 2));
      tmp.append("." + nvacantidad.substring(length - 2));
    }
    else{
      if(length == 2)
        tmp.append("0." + nvacantidad);
      else
        tmp.append("0.0" + nvacantidad);
    }
    
    nvacantidad = tmp.toString();
    return nvacantidad;
  }//fin metodo puntodecimal
  
  public String formatoNumero(String Numero) {
    
    if(!Numero.equals("0")){
      int NCaracteres = Numero.length();
      int nveces = (NCaracteres - 3) / 3 ;
      if (nveces != 0){
        Numero = comas(Numero,nveces);
        Numero = puntodecimal(Numero);
      }
      else
        Numero = puntodecimal(Numero);
    }
    else
      Numero = "0.00";
    return Numero;
    
  }//fin metodo formatoNumero
  
  //***
  //OBTIENE EL NUMERO DE CARGOS
  public int getNoTotalCargos() {
    sql = "SELECT COUNT (*) FROM " + dbQualifier + ".TC_CATLOGO_TOTALES ";
    sql = sql + "WHERE D_TIPO='C' AND D_DESCRIPCION <> ''" + statementPS;
    java.sql.Connection pooledConnection = com.bital.util.ConnectionPoolingManager.getPooledConnection( );
    java.sql.Statement stmt = null;
    java.sql.ResultSet rs = null;
    try{
      if(pooledConnection != null){
        stmt = pooledConnection.createStatement();
        rs = stmt.executeQuery(sql);
        if(rs.next())
          NoFilasCargos = rs.getInt(1);
      }
    }
    catch(java.sql.SQLException sqlException){
      System.out.println("Error PresentaTotalesC::getNoTotalCargos [" + sqlException.toString() + "]");
    }
    finally{
      try{
        if(rs != null){
          rs.close();
          rs = null;
        }
        if(stmt != null){
          stmt.close();
          stmt = null;
        }
        if(pooledConnection != null){
          pooledConnection.close();
          pooledConnection = null;
        }
      }
      catch(java.sql.SQLException sqlException){
        System.out.println("Error FINALLY PresentaTotalesC::getNoTotalCargos [" + sqlException.toString() + "]");
      }
    }
    return NoFilasCargos;
  }
  
  //***
  //OBTIENE EL NUMERO DE ABONOS
  public int getNoTotalAbonos() {
    sql = "SELECT COUNT (*) FROM " + dbQualifier + ".TC_CATLOGO_TOTALES ";
    sql = sql + "WHERE D_TIPO='A' AND D_DESCRIPCION <> ''" + statementPS;
    java.sql.Connection pooledConnection = com.bital.util.ConnectionPoolingManager.getPooledConnection( );
    java.sql.Statement stmt = null;
    java.sql.ResultSet rs = null;
    try{
      if(pooledConnection != null){
        stmt = pooledConnection.createStatement();
        rs = stmt.executeQuery(sql);
        if(rs.next())
          NoFilasAbonos = rs.getInt(1);
      }
    }
    catch(java.sql.SQLException sqlException){
      System.out.println("Error PresentaTotalesC::getNoTotalAbonos [" + sqlException.toString() + "]");
    }
    finally{
      try{
        if(rs != null){
          rs.close();
          rs = null;
        }
        if(stmt != null){
          stmt.close();
          stmt = null;
        }
        if(pooledConnection != null){
          pooledConnection.close();
          pooledConnection = null;
        }
      }
      catch(java.sql.SQLException sqlException){
        System.out.println("Error FINALLY PresentaTotalesC::getNoTotalAbonos [" + sqlException.toString() + "]");
      }
    }
    return NoFilasAbonos;
  }
  
  //***
  //OBTIENE NO TOTALES Y NOMBRES DE CARGOS EN LA BD TOTALES1
  String[] NoCargos = new String[getNoTotalCargos()];
  String[] cargos = new String[getNoTotalCargos()];
  private void getCargos() {
    int i = 0;
    sql = "SELECT * FROM " + dbQualifier + ".TC_CATLOGO_TOTALES ";
    sql = sql + "WHERE D_TIPO='C' AND D_DESCRIPCION <> ''" + statementPS;
    java.sql.Connection pooledConnection = com.bital.util.ConnectionPoolingManager.getPooledConnection( );
    java.sql.Statement stmt = null;
    java.sql.ResultSet rs = null;
    try{
      if(pooledConnection != null){
        stmt = pooledConnection.createStatement();
        rs = stmt.executeQuery(sql);
        while(rs.next()){
          NoCargos[i] = rs.getString("C_NUMERO_DE_TOTAL");
          cargos[i] = rs.getString("D_DESCRIPCION");
          i++;
        }
      }
    }
    catch(java.sql.SQLException sqlException){
      System.out.println("Error PresentaTotalesC::getCargos [" + sqlException.toString() + "]");
    }
    finally{
      try{
        if(rs != null){
          rs.close();
          rs = null;
        }
        if(stmt != null){
          stmt.close();
          stmt = null;
        }
        if(pooledConnection != null){
          pooledConnection.close();
          pooledConnection = null;
        }
      }
      catch(java.sql.SQLException sqlException){
        System.out.println("Error FINALLY PresentaTotalesC::getCargos [" + sqlException.toString() + "]");
      }
    }
  }
  
  //***
  // OBTIENE DOCUMENTOS E IMPORTES DE CARGOS
  String[] DocsCargos = new String[getNoTotalCargos()];
  String[] ImportesCargos = new String[getNoTotalCargos()];
  
  public void getDICargos() {
    String ColDocs = "T2_NO_DOCUMENTOS";
    String ColImporte = "T2_IMPORTE";
    for(int i=0; i<getNoTotalCargos(); i++){
      sql = "SELECT N_NUM_DOCUMENTOS,N_IMPORTE FROM " + dbQualifier + ".TW_T_NMDCTO_IMPRTE ";
      sql = sql + "WHERE C_ID_TELLER='" + getCajero() + "' AND C_DIVISA='" + getMoneda() + "' AND N_NUM_TOTAL= " + NoCargos[i] + statementPS;
      //			DocsCargos[i] = formatoNumero("0");
      DocsCargos[i] = "0";
      ImportesCargos[i] = formatoNumero("000");
      java.sql.Connection pooledConnection = com.bital.util.ConnectionPoolingManager.getPooledConnection( );
      java.sql.Statement stmt = null;
      java.sql.ResultSet rs = null;
      try{
        if(pooledConnection != null){
          stmt = pooledConnection.createStatement();
          rs = stmt.executeQuery(sql);
          if(rs.next()){
            DocsCargos[i] = rs.getString("N_NUM_DOCUMENTOS");
            ImportesCargos[i] = formatoNumero(rs.getString("N_IMPORTE"));
          }
        }
      }
      catch(java.sql.SQLException sqlException){
        System.out.println("Error PresentaTotalesC::getDICargos [" + sqlException.toString() + "]");
      }
      finally {
        try {
          if(rs != null){
            rs.close();
            rs = null;
          }
          if(stmt != null){
            stmt.close();
            stmt = null;
          }
          if(pooledConnection != null){
            pooledConnection.close();
            pooledConnection = null;
          }
        }
        catch(java.sql.SQLException sqlException){
          System.out.println("Error FINALLY ReversaDiario::SelectTxn [" + sqlException.getErrorCode() +  sqlException.getMessage() + "]");
        }
      }
    }
  }
  
  public String getNoCargo(int tmp) {
    if(flag1 == 1){
      getCargos();
      getDICargos();
      flag1 = 0;
    }
    return NoCargos[tmp];
  }
  
  public String getNombreCargo(int tmp) {
    return cargos[tmp];
  }
  
  public String getDocsCargo(int tmp) {
    return DocsCargos[tmp];
  }
  
  public String getImportesCargo(int tmp) {
    return ImportesCargos[tmp];
  }
  
  //***
  //OBTIENE NO TOTALES Y NOMBRES DE ABONOS EN LA BD TOTALES1
  String[] NoAbonos = new String[getNoTotalAbonos()];
  String[] abonos = new String[getNoTotalAbonos()];
  private void getAbonos() {
    int i = 0;
    sql = "SELECT * FROM " + dbQualifier + ".TC_CATLOGO_TOTALES ";
    sql = sql + "WHERE D_TIPO='A' AND D_DESCRIPCION <> ''" + statementPS;
    java.sql.Connection pooledConnection = com.bital.util.ConnectionPoolingManager.getPooledConnection( );
    java.sql.Statement stmt = null;
    java.sql.ResultSet rs = null;
    try{
      if(pooledConnection != null){
        stmt = pooledConnection.createStatement();
        rs = stmt.executeQuery(sql);
        while(rs.next()){
          NoAbonos[i] = rs.getString("C_NUMERO_DE_TOTAL");
          abonos[i] = rs.getString("D_DESCRIPCION");
          i++;
        }
      }
    }
    catch(java.sql.SQLException sqlException){
      System.out.println("Error PresentaTotales::getAbonos [" + sqlException.toString() + "]");
    }
    finally{
      try{
        if(rs != null){
          rs.close();
          rs = null;
        }
        if(stmt != null){
          stmt.close();
          stmt = null;
        }
        if(pooledConnection != null){
          pooledConnection.close();
          pooledConnection = null;
        }
      }
      catch(java.sql.SQLException sqlException){
        System.out.println("Error FINALLY PresentaTotales::getAbonos [" + sqlException.toString() + "]");
      }
    }
  }
  
  //***
  // OBTIENE DOCUMENTOS E IMPORTES DE ABONOS
  String[] DocsAbonos = new String[getNoTotalAbonos()];
  String[] ImportesAbonos = new String[getNoTotalAbonos()];
  public void getDIAbonos() {
    for(int i=0; i<getNoTotalAbonos(); i++){
      sql = "SELECT N_NUM_DOCUMENTOS,N_IMPORTE FROM " + dbQualifier + ".TW_T_NMDCTO_IMPRTE ";
      sql = sql + "WHERE C_ID_TELLER='" + getCajero() + "' AND C_DIVISA='" + getMoneda() + "' AND N_NUM_TOTAL= " + NoAbonos[i] + statementPS;
      //			DocsAbonos[i] = formatoNumero("0");
      DocsAbonos[i] = "0";
      ImportesAbonos[i] = formatoNumero("000");
      java.sql.Connection pooledConnection = com.bital.util.ConnectionPoolingManager.getPooledConnection( );
      java.sql.Statement stmt = null;
      java.sql.ResultSet rs = null;
      try{
        if(pooledConnection != null){
          stmt = pooledConnection.createStatement();
          rs = stmt.executeQuery(sql);
          if(rs.next()){
            DocsAbonos[i] = rs.getString("N_NUM_DOCUMENTOS");
            ImportesAbonos[i] = formatoNumero(rs.getString("N_IMPORTE"));
          }
        }
      }
      catch(java.sql.SQLException sqlException){
        System.out.println("Error PresentaTotalesC::getDIAbonos: [" + sqlException.toString() + "]");
      }
      
      try {
        if(rs != null){
          rs.close();
          rs = null;
        }
        if(stmt != null){
          stmt.close();
          stmt = null;
        }
        if(pooledConnection != null){
          pooledConnection.close();
          pooledConnection = null;
        }
        
      }
      catch(java.sql.SQLException sqlException){
        System.out.println("Error FINALLY ReversaDiario::SelectTxn [" + sqlException.getErrorCode() +  sqlException.getMessage() + "]");
      }
      
    }
  }
  
  public String getNoAbono(int tmp) {
    if(flag2 == 1){
      getAbonos();
      getDIAbonos();
      flag2 = 0;
    }
    return NoAbonos[tmp];
  }
  
  public String getNombreAbono(int tmp) {
    return abonos[tmp];
  }
  
  public String getDocsAbono(int tmp) {
    return DocsAbonos[tmp];
  }
  
  public String getImportesAbono(int tmp) {
    return ImportesAbonos[tmp];
  }
  
  //***
  // I N I C I O    I M P R E S I O N    T O T A L E S
  //***
  public String cadenaImpresion(String newSucursal,String newCajero, String newMoneda) {
    //primeras 8 posiciones descripcion si es cargo o abono justificado a la izquierda
    //de la 9 a la 12 no de total  justificado a la izquierda
    //13 a la 50 descripcion de total justificado a la izquierda
    //51 a la 61 no de documentos justificado a la derecha
    //65 a la 82 importe justificado a la izquierda
    boolean control1 = true;
    boolean ctrlCargos = true;
    boolean ctrlAbonos = false;
    int contador1 = 0;
    int contador2 = 0;
    int ctrlSwitch = 1;
    while(control1 == true){
      switch(ctrlSwitch){
        case 1:
          StringFormat =  StringFormat + "                   B A N C O     I N T E R N A C I O N A L          ~";
          StringFormat =  StringFormat + "                                TOTALES LOCALES~ ~ ~";
          StringFormat = StringFormat + formatHeaderPrinter(newSucursal,newCajero,newMoneda);
          StringFormat = StringFormat + "CARGOS   " + formatNoTotal(getNoCargo(contador1)) + formatDescripTotal(getNombreCargo(contador1)) + formatDocsTotal(getDocsCargo(contador1)) + formatImptsTotal(getImportesCargo(contador1)) + "~";
          ctrlSwitch = 2;
          contador1++;
          break;
        case 2:
          if(contador1 >= getNoTotalCargos())
            ctrlSwitch = 3;
          else{
            StringFormat = StringFormat + "         " + formatNoTotal(getNoCargo(contador1)) + formatDescripTotal(getNombreCargo(contador1)) + formatDocsTotal(getDocsCargo(contador1)) + formatImptsTotal(getImportesCargo(contador1)) + "~";
            contador1++;
          }
          break;
        case 3:
          StringFormat = StringFormat + " ~";
          StringFormat = StringFormat + "ABONOS   " + formatNoTotal(getNoAbono(contador2)) + formatDescripTotal(getNombreAbono(contador2)) + formatDocsTotal(getDocsAbono(contador2)) + formatImptsTotal(getImportesAbono(contador2)) + "~";
          contador2++;
          ctrlSwitch = 4;
          break;
        case 4:
          if(contador2 >= getNoTotalAbonos())
            control1 = false;
          else{
            StringFormat = StringFormat + "         " + formatNoTotal(getNoAbono(contador2)) + formatDescripTotal(getNombreAbono(contador2)) + formatDocsTotal(getDocsAbono(contador2)) + formatImptsTotal(getImportesAbono(contador2)) + "~";
            contador2++;
          }
          break;
        default:
          control1 = false;
          break;
      }
    }
    return StringFormat;
  }
  
  private String formatHeaderPrinter(String newSucursal,String newCajero, String newMoneda) {
    String txtHeaderPrinter = "SUCURSAL            CAJERO              MONEDA              FECHA               ~";
    String txtSucursal = newSucursal;
    String txtCajero = newCajero;
    String txtMoneda = "";
    String txtFecha = "";
	GenericClasses gc = new GenericClasses();
    java.util.Date fecha = new java.util.Date();
    java.text.SimpleDateFormat  formatoFecha = new java.text.SimpleDateFormat("dd/MM/yyyy");
    txtFecha = formatoFecha.format(fecha);
    
	txtMoneda = gc.getDivisa(newMoneda);

    if(txtSucursal.length() > 4)
      txtSucursal = txtSucursal.substring(txtSucursal.length() - 4);
    for(int i=txtSucursal.length(); i<20; i++)
      txtSucursal = txtSucursal + " ";
    
    for(int i=txtCajero.length(); i<20; i++)
      txtCajero = txtCajero + " ";
    
    for(int i=txtMoneda.length(); i<20; i++)
      txtMoneda = txtMoneda + " ";
    
    for(int i=txtFecha.length(); i<20; i++)
      txtFecha = txtFecha + " ";
    
    
    
    txtHeaderPrinter = txtHeaderPrinter + "" + txtSucursal + txtCajero + txtMoneda + txtFecha + "~";
    txtHeaderPrinter = txtHeaderPrinter + " ~"; //newLinew
    
    return txtHeaderPrinter;
  }
  
  private String formatNoTotal(String newNoTotal) {
    String formatNoTotal = "";
    int tmplength = newNoTotal.length();
    if(tmplength == 1)
      formatNoTotal = "(0" + newNoTotal.substring(0,1) + ") ";
    else{
      newNoTotal = newNoTotal.trim();
      formatNoTotal = "(" + newNoTotal + ") ";
    }
    return formatNoTotal;
  }
  
  private String formatDescripTotal(String newDescrip) {
    int longitud = 27 - newDescrip.length();
    for(int i=1; i<=longitud; i++)
      newDescrip = newDescrip + " ";
    return newDescrip;
  }
  
  private String formatDocsTotal(String newDocs) {
    int longitud = 11 - newDocs.length();
    for(int i=1; i<=longitud; i++)
      newDocs = " " + newDocs;
    return newDocs;
  }
  
  private String formatImptsTotal(String newImporte) {
    int longitud = 21 - newImporte.length();
    for(int i=1; i<=longitud; i++)
      newImporte = " " + newImporte;
    return newImporte;
  }
  //***
  // F I N    I M P R E S I O N    T O T A L E S
  //***
}