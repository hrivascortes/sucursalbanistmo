//*************************************************************************************************
//             Funcion: Bean Txn 0230
//          Creado por: Oscar L�pez Gutierrez
//*************************************************************************************************
// CCN - 4360500 - 04/08/2006 - Se agrega para extraccion de efectivo CDM
//*************************************************************************************************
package ventanilla.com.bital.sfb;

import com.bital.util.ParameterParser;
import com.bital.util.ParameterNotFoundException;
import ventanilla.GenericClasses;

public class Txn_0230 extends Transaction
{
	public Header doAction(ParameterParser param)
		throws ParameterNotFoundException
	{
		FormatoB formatob = new FormatoB();
		GenericClasses gc = new GenericClasses();
		formatob.setTxnCode("0230");
		formatob.setAcctNo("0230");		
		formatob.setFormat("B");
		formatob.setTranAmt(param.getCString("txtMontoNoCero"));
	        String moneda = gc.getDivisa((String)param.getString("moneda"));
	        formatob.setFromCurr(moneda);
	        formatob.setCashIn(param.getCString("txtMontoNoCero"));
        	String numSuc = (String)param.getCString("sucursal");
	        if(numSuc.length()>4)
        	    numSuc = numSuc.substring(numSuc.length()-4);		
	        formatob.setBranch((String)param.getCString("sucursal"));
		String empno = (String)param.getCString("teller");
		String txtRegResp = (String)param.getCString("txtRegResp");
		formatob.setReferenc(txtRegResp + txtRegResp + "D"+numSuc+(String)param.getCString("lstNemo") + (String)param.getString("lst0230"));
	
		return formatob;
	}
}
