//*************************************************************************************************
//             Funcion: Bean Txn 0568
//      Elaborado por: Jesus Emmanuel Lopez Rosales
//*************************************************************************************************
// CCN - 4360576 - 09/03/2007 - Modificaciones para proyecto OPMN
// CCN - 4360584 - 23/03/2007 - Modificaciones para proyecto OPMN
//*************************************************************************************************
package ventanilla.com.bital.sfb;

import ventanilla.GenericClasses;
import com.bital.util.ParameterNotFoundException;
import com.bital.util.ParameterParser;


public class Txn_0568 extends Transaction
{		 
	public Header doAction(ParameterParser param)
		throws ParameterNotFoundException
	{				
        GenericClasses gc = new GenericClasses();                				
		FormatoG formatog = new FormatoG();
		 
	   formatog.setTxnCode("0568");
       formatog.setFormat("G");	
       formatog.setToCurr("N$");
       formatog.setCheckNo("0");
       formatog.setGStatus(param.getString("txtStatusOrden"));
       formatog.setTranAmt(param.getCString("txtMonto"));	
       formatog.setComAmt("0");       
	   formatog.setIVAAmt("0");
	   formatog.setAcctNo("1");
	   formatog.setCtaBenef(param.getString("txtNumOrden"));
	   
	   String strciudad =param.getString("ciudad");
	   if(strciudad.length() > 20){
	      strciudad = strciudad.substring(0,20);
	   }else{
	      strciudad =gc.StringFiller(param.getString("ciudad"),20,true," ");
	   }	
	   String strip =gc.StringFiller(param.getString("dirip"),16,true," ");
	   formatog.setInstruc(strciudad + strip);
	   formatog.setOrdenan(gc.StringFiller(param.getString("lstTipoDocto"),4,true," ")+gc.StringFiller(param.getString("txtFolio"),17,true," "));	   	   
		return formatog;
	}
	

}
