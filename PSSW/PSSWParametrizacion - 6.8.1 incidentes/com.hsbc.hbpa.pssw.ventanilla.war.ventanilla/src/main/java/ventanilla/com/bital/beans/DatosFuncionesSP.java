//*************************************************************************************************
//	 	Funcion: Clase que realiza la consulta de los datos del Pais y las conserva en memoria
//		Nombre: DatosPais.java			
//		Creado por: Humberto Enrique Balleza Mej�a
//	    Modificado por: 
//*************************************************************************************************
//CCN -XXXXXXX- 13/02/2007 - Se crea la clase para realizar select y mantener los datos en memoria
//*************************************************************************************************

package ventanilla.com.bital.beans;

import java.util.Hashtable;
import java.util.Vector;

public class DatosFuncionesSP {
	
	private Hashtable DEBP = null;
	private Hashtable DEAP = null;
	private Hashtable DRBP = null;
	private Hashtable DRAP = null;
	private static DatosFuncionesSP instance = null;
    
	private DatosFuncionesSP() 
	{   
		DEBP = new Hashtable();
		DEAP = new Hashtable();
		DRBP = new Hashtable();
		DRAP = new Hashtable();
		execute();
	}

	public static synchronized DatosFuncionesSP getInstance()
	{
		if(instance == null)
			instance = new DatosFuncionesSP();
		return instance;
	}
	
	public boolean isSPFunc(String key, String tipo)
	{
		if(tipo.equals("DEAP"))
		{
			if(DEAP.containsKey(key))
				return true;
		}
		else if(tipo.equals("DEBP"))
		{
			if(DEBP.containsKey(key))
				return true;
		}
		else if(tipo.equals("DRAP"))
		{
			if(DRAP.containsKey(key))
				return true;
		}
		else if(tipo.equals("DRBP"))
		{
			if(DRBP.containsKey(key))
				return true;
		}
		return false;				
	}
	
	public Vector getSPFuncs(String key, String tipo)
	{
		Vector vSP = new Vector();
		int i=0;
		int j=0;
		if(tipo.equals("DEAP"))
		{
			if(DEAP.containsKey(key))
			{
				java.util.StringTokenizer dato = new java.util.StringTokenizer((String)DEAP.get(key),"*");
				j=dato.countTokens();
				for(i=0;i<j;i++)
				{
					vSP.add(dato.nextToken());
				}
			}
		}
		else if(tipo.equals("DEBP"))
		{
			if(DEBP.containsKey(key))
			{
				java.util.StringTokenizer dato = new java.util.StringTokenizer((String)DEBP.get(key),"*");
				j=dato.countTokens();
				for(i=0;i<j;i++)
				{
					vSP.add(dato.nextToken());
				}
			}
		}
		else if(tipo.equals("DRAP"))
		{
			if(DRAP.containsKey(key))
			{
				java.util.StringTokenizer dato = new java.util.StringTokenizer((String)DRAP.get(key),"*");
				j=dato.countTokens();
				for(i=0;i<j;i++)
				{
					vSP.add(dato.nextToken());
				}
			}
		}
		else if(tipo.equals("DRBP"))
		{
			if(DRBP.containsKey(key))
			{
				java.util.StringTokenizer dato = new java.util.StringTokenizer((String)DRBP.get(key),"*");
				j=dato.countTokens();
				for(i=0;i<j;i++)
				{
					vSP.add(dato.nextToken());
				}
			}
		}
		return vSP;
	}

	public void execute() 
	   {
	   		DatosVarios dv = DatosVarios.getInstance();
			Vector Rselect = dv.getDatosV("PROCESPS");
			int i = 0;
			int j = 0;
			int con = 0;
			for(j=0;j<Rselect.size();j++)
			{
				String tipoSPFunc = "";
				String SPFunc = "";
				String temp="";	
	
			    java.util.StringTokenizer dato = new java.util.StringTokenizer((String)Rselect.get(j),"*");
				SPFunc=dato.nextToken();
				tipoSPFunc=dato.nextToken();
				con = dato.countTokens();
				for(i=0;i<con;i++)
				{
					temp=dato.nextToken();
					if(tipoSPFunc.equals("DEAP"))
					{	
						if(DEAP.containsKey(temp))
							DEAP.put(temp,(String)DEAP.get(temp)+SPFunc+"*");
						else
							DEAP.put(temp,SPFunc+"*");
					}
					else if(tipoSPFunc.equals("DEBP"))
					{
						if(DEBP.containsKey(temp))
							DEBP.put(temp,(String)DEBP.get(temp)+SPFunc+"*");
						else
							DEBP.put(temp,SPFunc+"*");
					}
					else if(tipoSPFunc.equals("DRAP"))
					{
						if(DRAP.containsKey(temp))
							DRAP.put(temp,(String)DRAP.get(temp)+SPFunc+"*");
						else
							DRAP.put(temp,SPFunc+"*");
					}
					else if(tipoSPFunc.equals("DRBP"))
					{
						if(DRBP.containsKey(temp))
							DRBP.put(temp,(String)DRBP.get(temp)+SPFunc+"*");
						else
							DRBP.put(temp,SPFunc+"*");
					}
				}
				con=0;
			}
	   }
}
