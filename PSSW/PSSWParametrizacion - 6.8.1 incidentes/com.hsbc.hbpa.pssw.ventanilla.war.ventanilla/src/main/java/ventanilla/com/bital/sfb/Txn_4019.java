//*************************************************************************************************
//			   Funcion: Bean Txn 4019
//			 Elemento : Txn_4019.java
//		Modificado por: Humberto Enrique Balleza Mej�a
//*************************************************************************************************
// CCN -         - 07/01/2008 - Se crea el bean de la transacci�n 4019
//*************************************************************************************************
package ventanilla.com.bital.sfb;

import com.bital.util.ParameterParser;
import com.bital.util.ParameterNotFoundException;
import ventanilla.GenericClasses;

public class Txn_4019 extends Transaction
{
	public Header doAction(ParameterParser param)
		throws ParameterNotFoundException
	{
		FormatoA formatoa = new FormatoA();
		GenericClasses gc = new GenericClasses();
		formatoa.setTxnCode("4019");
		
		formatoa.setAcctNo(param.getString("txtDDACuenta"));
		formatoa.setCashIn("000");
		formatoa.setTranAmt(param.getCString("txtMonto"));
		formatoa.setTrNo("1");
		formatoa.setMoAmoun("000");
		String temp = null;
		formatoa.setTxtDocs(param.getCString("txtDocsLoc"));
		temp="DEP. CHQ BANCOS LOCALES    "+param.getCString("txtDocsLoc")+" T3";
		formatoa.setTranDesc(temp);
		formatoa.setDescRev("REV. "+temp);
		formatoa.setHolDays("3");
		formatoa.setHolDays2("3");
		formatoa.setAmount3("0096601");
		
	    if (param.getString("override") != null)
       		if (param.getString("override").equals("SI"))
           		formatoa.setOverride("3");
		String moneda = gc.getDivisa(param.getString("moneda"));
		formatoa.setTranCur(moneda);

		return formatoa;
	}
}
