package ventanilla.com.bital.util;

//public class QueryDBColumnsPerRow implements java.io.Serializable
public class QueryDBColumnsPerRow
{
  private java.sql.ResultSet  newqueryResultSet = null;
  private java.util.Hashtable newqueryHashtable = null;

  public QueryDBColumnsPerRow(java.sql.ResultSet newqueryResultSet){
    this.newqueryResultSet = newqueryResultSet;
  }
 
  public void execute()
  {
   java.sql.ResultSetMetaData rsmd = null;
   try{
    rsmd = newqueryResultSet.getMetaData();
    int numberOfColumns = rsmd.getColumnCount();
   	newqueryHashtable = new java.util.Hashtable();
    for(int i = 1; i <= numberOfColumns; i++)
      this.newqueryHashtable.put(rsmd.getColumnName(i), this.newqueryResultSet.getString(i));
    }
   catch(java.sql.SQLException sqlException){
    System.out.println("QueryDBColumnsPerRow::executeQuery: [" + sqlException.toString() + "] ");
   }
   finally{
     try{
       if(rsmd != null)
         rsmd = null;
       if(newqueryResultSet != null){
         newqueryResultSet.close();
         newqueryResultSet = null;
       }
     }
     catch(java.sql.SQLException sqlException){
       System.out.println("Error FINALLY QueryDBColumnsPerRow::executeQuery: [" + sqlException.toString() + "] ");
     }
   }
  }

  public java.util.Hashtable getColumns(){
  	return newqueryHashtable;
  }
}
