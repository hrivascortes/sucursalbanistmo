//*************************************************************************************************
//             Funcion: Clase que realiza la consulta de Montos Maximos por transaccion
//          Creado por: Juvenal R. Fernandez V.
// Ultima Modificacion: 22/06/2004
//      Modificado por: Juvenal R. Fernandez V.
//*************************************************************************************************
// CCN -4360160- 30/06/2004 - Se crea la clase para realizar select y mantener los datos en memoria
// CCN - 4360334 - 17/06/2005 - Se elimina variable dbQualifier evitando enviar "." antes del nombre de la tabla
//*************************************************************************************************

package ventanilla.com.bital.beans;

import ventanilla.com.bital.util.DBQuery;

import java.util.Vector;
import java.util.Hashtable;

public class MtosMax
{
    private Hashtable montosm = null;
    private static MtosMax instance = null;
    
    private MtosMax() 
    {   
        montosm = new Hashtable();
        execute();
    }
    
    public static synchronized MtosMax getInstance()
    {
        if(instance == null)
            instance = new MtosMax();
        return instance;
    }
    
    public Vector getMtosM(String txn)
    {
        Vector mtosm = new Vector();
        if(montosm.containsKey(txn))
            mtosm = (Vector)this.montosm.get(txn);
       
        return mtosm;
    }
    
    public void execute()
    {
        String sql = null;
        sql = "SELECT C_TRANSACCIONES, N_MONTO_PESOS, N_MONTO_DOLARES FROM ";        
        String clause = null;
        clause = " ORDER BY C_TRANSACCIONES";
        
        DBQuery dbq = new DBQuery(sql,"TC_MTS_MXMOS_X_TXN", clause);
        Vector Regs = dbq.getRows();
        
        int size = Regs.size();
        Vector row = new Vector();
        String txn = null;

        for(int i=0; i<size; i++)
        {
            row = (Vector)Regs.get(i);
            txn = (String)row.get(0);
            montosm.put(txn.trim(), row);
        }
    }    
}
