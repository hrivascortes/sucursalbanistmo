//*************************************************************************************************
//              Funcion: Bean Txn 0272
//      Modificado por: Humberto Enrique Balleza Mej�a
//*************************************************************************************************
// CCN -        - 04/02/2005 - Se crea la transacci�n 0272
//*************************************************************************************************
package ventanilla.com.bital.sfb;

import com.bital.util.ParameterParser;
import com.bital.util.ParameterNotFoundException;
import ventanilla.GenericClasses;

public class Txn_0272 extends Transaction
{
	public Header doAction(ParameterParser param)
		throws ParameterNotFoundException
	{
		FormatoB formatob = new FormatoB();
		GenericClasses gc = new GenericClasses();
		//txtDocs*txtMonto*lstBancoATC*
		formatob.setTxnCode("0272");
		formatob.setFormat("B");
		formatob.setAcctNo(param.getCString("txtDocs"));
		formatob.setTranAmt(param.getCString("txtMonto"));
		formatob.setReferenc(param.getString("lstBancoATC"));
		String moneda = gc.getDivisa(param.getString("moneda"));
		formatob.setCashOut(param.getCString("txtMonto"));
		formatob.setFromCurr(moneda);

		return formatob;
	}
}
