//*************************************************************************************************
//			 Funcion: Clase que realiza txn Abono a DDA 4129
//			Elemento: Txn_4129.java
//		  Creado por: Hugo Gabriel Rivas Cortes
//*************************************************************************************************
//CR - 13700 - 11/10/2011 - Creacion de elemento LF 
//*************************************************************************************************

package ventanilla.com.bital.sfb;





import ventanilla.GenericClasses;
import com.bital.util.ParameterParser;
import com.bital.util.ParameterNotFoundException;

public class Txn_4129 extends Transaction
{
  public Header doAction(ParameterParser param)
    throws ParameterNotFoundException
  {
	  //ODPA 4129A 05030503007  503001  000**0152500949**213  *N$******000**01 DEVOLUCION DE INTERESES L.F.*0***************0**********************
	  GenericClasses gc = new GenericClasses();
	  FormatoA format = new FormatoA();

      format.setBranch(param.getString("sucursal"));
      format.setTeller(param.getString("teller"));
      format.setSupervisor(param.getString("teller"));
      format.setCashIn("000");
      //format.setMoAmoun("000");
      //format.setHolDays("0");
      //format.setHolDays2("0");

      if ( param.getString("supervisor") != null )
           format.setSupervisor(param.getString("supervisor"));

      format.setTxnCode("4129");
      format.setTranDesc(param.getString("lstDevLF"));
      
      if(param.getString("lstDevLF").toString().substring(0,2).equals("02")){
    	  format.setTxnCode("4131");
    	  
      }
      
      format.setAcctNo(param.getString("txtCuentaDDA"));
      format.setTranAmt(param.getCString("txtMonto"));
      String moneda = param.getString("moneda");
		format.setTranCur(gc.getDivisa(moneda));
		
      
      return format;
  }               
}
