package ventanilla.com.bital.sfb;

public class DAPG extends Header
{
  private String txtToCurr   = "";
  private String txtCheckNo  = "";
  private String txtTranAmt  = "";
  private String txtAcctNo   = "";
  private String txtBenefic  = "";
  private String txtInstruc  = "";

  public String getToCurr()
  {
	return txtToCurr + "*";
  }
  public void setToCurr(String newToCurr)
  {
	txtToCurr = newToCurr;
  }

  public String getCheckNo()
  {
	return txtCheckNo + "*";
  }
  public void setCheckNo(String newCheckNo)
  {
	txtCheckNo = newCheckNo;
  }

  public String getTranAmt()
  {
	return txtTranAmt + "*";
  }
  public void setTranAmt(String newTranAmt)
  {
	txtTranAmt = newTranAmt;
  }
  
  public String getAcctNo()
  {
	return txtAcctNo + "*";
  }
  public void setAcctNo(String newAcctNo)
  {
	txtAcctNo = newAcctNo;
  }
  
  public String getBenefic()
  {
	return txtBenefic + "*";
  }
  public void setBenefic(String newBenefic)
  {
	txtBenefic = newBenefic;
  }
  
  public String getInstruc()
  {
	return txtInstruc + "*";
  }
  public void setInstruc(String newInstruc)
  {
	txtInstruc = newInstruc;
  }

  public String toString()
  {
	return super.toString()
	+ "*"	+ "*"	+ getToCurr()	+ getCheckNo()	+ "*"
	+ "*"	+ getTranAmt()	+ "*"	+ "*"	+ getAcctNo()
	+ "*"	+ getBenefic()	+ "*"	+ getInstruc();
  }    
}
