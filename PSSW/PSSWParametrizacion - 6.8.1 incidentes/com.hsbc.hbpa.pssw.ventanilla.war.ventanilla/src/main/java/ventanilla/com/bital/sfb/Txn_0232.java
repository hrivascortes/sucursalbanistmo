//*************************************************************************************************
//             Funcion: Bean Txn 0232
//          Creado por: Oscar L�pez Gutierrez
//*************************************************************************************************
// CCN - 4360500 - 04/08/2006 - Se agrega para dotaci�n de efectivo CDM
//*************************************************************************************************
package ventanilla.com.bital.sfb;

import com.bital.util.ParameterParser;
import com.bital.util.ParameterNotFoundException;
import ventanilla.GenericClasses;

public class Txn_0232 extends Transaction
{
	public Header doAction(ParameterParser param)
		throws ParameterNotFoundException
	{
		FormatoB formatob = new FormatoB();
		GenericClasses gc = new GenericClasses();
		formatob.setTxnCode("0232");
		formatob.setAcctNo("0232");		
		formatob.setFormat("B");
		formatob.setTranAmt(param.getCString("txtMontoNoCero"));
		formatob.setCashOut(param.getCString("txtMontoNoCero"));
        	String moneda = gc.getDivisa((String)param.getString("moneda"));
	        formatob.setFromCurr(moneda);
        	String numSuc = (String)param.getCString("sucursal");
	        if(numSuc.length()>4)
        	    numSuc = numSuc.substring(numSuc.length()-4);		
	        formatob.setBranch((String)param.getCString("sucursal"));
		String empno = (String)param.getCString("teller");
		String txtRegResp = (String)param.getCString("txtRegResp");
		formatob.setReferenc("              D"+numSuc+(String)param.getCString("lstNemo"));
		
		return formatob;
	}
}
