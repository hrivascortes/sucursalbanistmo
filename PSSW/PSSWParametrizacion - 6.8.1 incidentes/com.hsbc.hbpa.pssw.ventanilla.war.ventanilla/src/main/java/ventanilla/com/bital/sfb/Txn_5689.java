//*************************************************************************************************
//             Funcion: Bean Txn 5689
//          Creado por: Fausto Rodrigo Flores Moreno
//*************************************************************************************************
// CCN - 4360574 - 09/03/2007 - Se crea bean para la consulta de servicios marcados para RAP Calculadora
//*************************************************************************************************
package ventanilla.com.bital.sfb;

import com.bital.util.ParameterParser;
import com.bital.util.ParameterNotFoundException;
import ventanilla.GenericClasses;

public class Txn_5689 extends Transaction
{
	public Header doAction(ParameterParser param)
		throws ParameterNotFoundException
	{
		FormatoA formatoa = new FormatoA();
		GenericClasses gc = new GenericClasses();

		formatoa.setTxnCode("5689");
		formatoa.setAcctNo(param.getString("servicio1"));
		formatoa.setTranAmt(param.getString("MtoDes_Rec"));
		formatoa.setTrNo("9");
		formatoa.setTranDesc(param.getString("ref1s1p1"));
		String moneda = gc.getDivisa(param.getString("moneda"));
		formatoa.setTranCur(moneda);
		formatoa.setCashOut(param.getString("MtoDes_Rec"));
		
		return formatoa;
	}
}