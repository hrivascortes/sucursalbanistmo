//*************************************************************************************************
//             Funcion: Bean Txn 5017 Impresion de Cheque de Gerencia.
//             Elaborado por: YGX
//*************************************************************************************************

package ventanilla.com.bital.sfb;


import ventanilla.GenericClasses;
import com.bital.util.ParameterNotFoundException;
import com.bital.util.ParameterParser;


public class Txn_5017 extends Transaction
{		 
	public Header doAction(ParameterParser param)
		throws ParameterNotFoundException
	{				
        GenericClasses gc = new GenericClasses();   
        FormatoH formatoh = new FormatoH();         				
		ventanilla.com.bital.sfb.Cheques Cheques = new ventanilla.com.bital.sfb.Cheques();
		
		formatoh.setTxnId("ODPA");
		formatoh.setTxnCode("5017");
	    formatoh.setFormat("H");   
	    
	    formatoh.setTranDesc(param.getString("txtBenefiCheq").trim());
	    
	    //Cuenta de Cheques de Gerencia
	    formatoh.setAcctNo("0103000023");
	    //Cuenta de Cuenta informativa se env�a la cta del cargo
	    formatoh.setAcctDeb("0103000023");	    
	    formatoh.setTranAmt(gc.delCommaPointFromString(param.getString("txtMonto")).trim());
	    
	    int moneda = param.getInt("moneda"); 
	    formatoh.setTranCur(gc.getDivisa(param.getString("moneda")));	    
	    formatoh.setCheckNo(param.getCString("txtSerialCheq"));	    
	    
	    formatoh.setCashIn("000");
	    
	    formatoh.setDescRev("REV 5017" );
	    	   	
	   //System.out.println(formatoh);
	   	
		return formatoh;
	}
	

}
