//*************************************************************************************************
//             Funcion: Clase que realiza la consulta de Datos a insertar en el Diario y los conserva en memoria
//          Creado por: Juvenal R. Fernandez V.
//      Modificado por: Juvenal R. Fernandez V.
//*************************************************************************************************
// CCN -4360152- 21/06/2004 - Se crea la clase para realizar select y mantener los datos en memoria
// CCN - 4360334 - 17/06/2005 - Se elimina variable dbQualifier evitando enviar "." antes del nombre de la tabla
// CO - CR7564 - 26/01/2011 - Cambios Proyecto camp
//*************************************************************************************************

package ventanilla.com.bital.beans;

import ventanilla.com.bital.util.DBQuery;

import java.util.Vector;
import java.util.Hashtable;

public class DataDiario
{
    private Hashtable datosd = null;
    private static DataDiario instance = null;
    
    private DataDiario() 
    {
        datosd = new Hashtable();
        execute();
    }
    
    public static synchronized DataDiario getInstance()
    {
        if(instance == null)
            instance = new DataDiario();
        return instance;
    }

    public Vector getDatosd(String txn, String moneda)
    {
        Vector tmp = (Vector)this.datosd.get(txn+moneda);
        return tmp;
    }    
    
    public void execute() 
    {
        String sql = null;
        sql = "SELECT C_TXN, C_MONEDA, C_AFECTA_DIARIO, C_VISIBLE_DIARIO, C_TXN_REVERSABLE, C_TOTALES_CARGO, C_TOTALES_ABONO, D_CAMPOS_DIARIO, D_DEBP, D_DEAP, D_DRBP, D_DRAP FROM ";
        String clause = null;
        clause = " ORDER BY C_TXN, C_MONEDA";
        
        DBQuery dbq = new DBQuery(sql,"TC_DATOS_DIARIO", clause);
        Vector Regs = dbq.getRows();
        
        int size = Regs.size();
        Vector row = new Vector();
        String txn= null;
        String moneda= null;
        
        for(int i=0; i<size; i++)
        {
            row = (Vector)Regs.get(i);
            txn = row.get(0).toString();
            moneda = row.get(1).toString().trim();
            row.remove(0);
            row.remove(0);

            datosd.put(txn+moneda, row);
            
        }
    }
}
