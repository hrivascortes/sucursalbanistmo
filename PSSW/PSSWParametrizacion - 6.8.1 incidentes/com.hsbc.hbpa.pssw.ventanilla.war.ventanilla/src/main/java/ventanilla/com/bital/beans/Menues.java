//*************************************************************************************************
//             Funcion: Clase que realiza la consulta de Menues a desplgar y los conserva en memoria
//            Elemento: Menues.java
//          Creado por: Juvenal R. Fernandez V.
//      Modificado por: Jesus Emmanuel L�pez Rosales
//*************************************************************************************************
// CCN - 4360152 - 21/06/2004 - Se crea la clase para realizar select y mantener los datos en memoria
// CCN - 4360175 - 06/08/2004 - Se corrige inclusionn elemento ultimo de select
// CCN - 4360334 - 17/06/2005 - Se elimina variable dbQualifier evitando enviar "." antes del nombre de la tabla
// CCN - 4360342 - 08/07/2005 - Se realizan modificaciones para departamentos.
// CCN - 4360456 - 07/04/2006 - Se agrega codigo para control de txn secundarias para deptos.
// CCN - 4360510 - 08/09/2006 - Obtener las transacciones por proceso de un perfil
//*************************************************************************************************

package ventanilla.com.bital.beans;

import ventanilla.com.bital.util.DBQuery;

import java.util.*;

public class Menues
{
    private Hashtable menues = null;
	private Hashtable TxnsXDepto = null;
	private Hashtable Txns = null;
	private Hashtable TxnPerfil = null;
    private static Menues instance = null;
    
    private Menues() 
    {
        menues = new Hashtable();
        Txns = new Hashtable();
		TxnsXDepto = new Hashtable();
		TxnPerfil = new Hashtable();
        execute();
    }
    
    private Vector newVectorTemp(Vector txn, String txnPar)
    {
       Vector Vtemp = new Vector(txn.size());
       for (int h=0;h<txn.size();h++){
         if(h!=1)
            Vtemp.addElement(txn.get(h));
         else
            Vtemp.addElement(txn.get(h).toString()+txnPar);
       }
       return Vtemp;
    }
    
    public static synchronized Menues getInstance()
    {
        if(instance == null)
            instance = new Menues();
        return instance;
    }

    public Vector getMenues(String proceso)
    {
        Vector tmp = (Vector)this.menues.get(proceso);
        return tmp;
    }    
    
	public Vector getMenuesDep(String depto)
	{
		Vector tmp = new Vector();
		  tmp = (Vector)this.TxnsXDepto.get(depto);
		
		return tmp;
	}
	
	public Vector getMenuesPerfil(String idperfil,boolean tipomenu, String numproc)
	{
		Vector tmp = new Vector();
		Vector tmproc = new Vector();
		tmp = (Vector)this.TxnPerfil.get(idperfil);
		if(!tipomenu){
		  for(int i=0;i<tmp.size();i++){
		     String cad = tmp.get(i).toString();	
		     if(cad.substring(1,3).equals(numproc)){
		        
		        tmproc.addElement(tmp.elementAt(i));
		     }   
		  }	
		  return tmproc; 
		}
		else{  
		return tmp;
		}
	}
	
	public void setMenuesDep()
	{
		DatosVarios deptos = DatosVarios.getInstance();
		Monedas monedas = Monedas.getInstance();
		
		Vector Monedas = monedas.getMonedas();
		Hashtable Depto = deptos.getDatosH("DEPTOS");
		Enumeration keysd = Depto.keys();
		Vector tmp = new Vector();
		String TxnMoneda = "";
		int s=0;
		while (keysd.hasMoreElements())
		{
			String NumDepto = keysd.nextElement().toString();
			String TxnsDept = "";
			Vector txnsD = (Vector)Depto.get(NumDepto);
			int size=0;
			for(int j=0; j<txnsD.size();j++)
			{
				for(int i=0; i<Monedas.size(); i++)
				{
 					Vector TxnPares = new Vector();
 					TxnsDept = txnsD.elementAt(j).toString();
 					if(TxnsDept.indexOf('-')>-1){
 					  TxnMoneda = TxnsDept.substring(0,TxnsDept.indexOf('-'))+((Vector)Monedas.elementAt(i)).elementAt(0)+TxnsDept.substring(TxnsDept.indexOf('-'),TxnsDept.length());
 					}else{
 					  TxnMoneda = TxnsDept+((Vector)Monedas.elementAt(i)).elementAt(0);
 					}
					size=TxnMoneda.length();
					if(size>6){
  					  if(Txns.get(TxnMoneda.substring(0,TxnMoneda.indexOf('-')))!= null){
					   	 TxnPares = newVectorTemp((Vector)Txns.get(TxnMoneda.substring(0,TxnMoneda.indexOf('-'))),TxnMoneda.substring(TxnMoneda.indexOf('-'),TxnMoneda.length()));
					   	 tmp.addElement(TxnPares);
					   	 TxnPares = null;
  					  }
					}else
					  if(Txns.get(TxnMoneda)!=null){
				   		tmp.addElement(Txns.get(TxnMoneda));
				}
			}	
			}	
			TxnsXDepto.put(NumDepto,tmp);
			tmp = new Vector();
		}		
	}

	public void setMenuesPerfil()
	{
		DatosVarios perfiles = DatosVarios.getInstance();
		Monedas monedas = Monedas.getInstance();
		
		Vector Monedas = monedas.getMonedas();		
		Hashtable hPerfil = perfiles.getDatosH("PERFILT");
		Enumeration keysd = hPerfil.keys();
		Vector tmp = new Vector();
		String TxnMoneda = "";
		int s=0;
		while (keysd.hasMoreElements())
		{
			String NumPerfil = keysd.nextElement().toString();			
			String TxnsDept = "";
			Vector txnsD = (Vector)hPerfil.get(NumPerfil);			
			int size=0;
			for(int j=0; j<txnsD.size();j++)
			{
				for(int i=0; i<Monedas.size(); i++)
				{
 					Vector TxnPares = new Vector();
 					TxnsDept = txnsD.elementAt(j).toString();
 					if(TxnsDept.indexOf('-')>-1){
 					  TxnMoneda = TxnsDept.substring(0,TxnsDept.indexOf('-'))+((Vector)Monedas.elementAt(i)).elementAt(0)+TxnsDept.substring(TxnsDept.indexOf('-'),TxnsDept.length());
 					}else{
 					  TxnMoneda = TxnsDept+((Vector)Monedas.elementAt(i)).elementAt(0);
 					}
					size=TxnMoneda.length();
					if(size>6){
  					  if(Txns.get(TxnMoneda.substring(0,TxnMoneda.indexOf('-')))!= null){
					   	 TxnPares = newVectorTemp((Vector)Txns.get(TxnMoneda.substring(0,TxnMoneda.indexOf('-'))),TxnMoneda.substring(TxnMoneda.indexOf('-'),TxnMoneda.length()));
					   	 tmp.addElement(TxnPares);
					   	 TxnPares = null;
  					  }
					}else
					  if(Txns.get(TxnMoneda)!=null){
				   	     tmp.addElement(Txns.get(TxnMoneda));
					  }
				}
			}	
			TxnPerfil.put(NumPerfil,tmp);
			tmp = new Vector();
		}		
	}
    
    public void execute() 
    {
        String sql = null;
        sql = "SELECT C_PROCESO, C_TRANSACCION, C_MONEDA, D_DESCRIPCION, N_NIVEL_CAJERO, D_TXN_DSP_SUC_N, D_TXN_DSP_SUC_E, D_TXN_DSP_CAJA FROM ";
        String clause = null;
        clause = " ORDER BY C_PROCESO, C_TRANSACCION";
        
        DBQuery dbq = new DBQuery(sql,"TC_MENUES_TXNS", clause);
        Vector Regs = dbq.getRows();
        
        int size = Regs.size();
        Vector row = new Vector();
        Vector rows = new Vector();
        Vector rqtxns = new Vector();
        String proceso = null;
        String newproceso = null;
        for(int i=0; i<size; i++)
        {
            row = (Vector)Regs.get(i);
            proceso = row.get(0).toString();
            
            if(newproceso == null)
                newproceso = proceso;
            
            if( (!proceso.equals(newproceso)) || (i == size-1) )
            {
                if (i == size-1)
                    rows.add(row);
                menues.put(newproceso, rows);
                rows = new Vector();
                newproceso = proceso;
            }
            
            if (row.get(7).equals("S"))
                rqtxns.add(row);
            
            rows.add(row);
            Txns.put(row.get(1).toString()+row.get(2).toString(),row);
        }
        menues.put("S",rqtxns);
        
		setMenuesDep();
		setMenuesPerfil();
    }
    
}
