//*************************************************************************************************
//             Funcion: Servlet para obtener las paginas del flujo de las txns
//            Elemento: PageStream.java
//          Creado por: Alejandro Gonzalez Castro
//      Modificado por: Juvenal R. Fernandez V.
//*************************************************************************************************
// CCN - 4360334 - 17/06/2005 - Se elimina variable dbQualifier
//*************************************************************************************************
package ventanilla.com.bital.util;

//public class PageStream implements java.io.Serializable
public class PageStream
{
  private String iTxn = null;
  private String oTxn = null;
  private String iMon = null;
  private java.util.Vector vflujo = null;
  private java.util.Stack  sflujo = null;
  String statementPS = com.bital.util.ConnectionPoolingManager.getStatementPostFix();

  public PageStream()
  {
  }

  public void setData(String newiTxn, String newoTxn, String newMon)
  {
    iTxn = newiTxn;
    oTxn = newoTxn;
    iMon = newMon;
  }

  public String getiTxn()
  {
    return iTxn;
  }

  public String getoTxn()
  {
    return oTxn;
  }

  public String getCurrency()
  {
    return iMon;
  }

  public void executeQuery()
  {
    for(int i = oTxn.length(); i < 4; i++)
      oTxn += " ";
    java.sql.Connection pooledConnection = com.bital.util.ConnectionPoolingManager.getPooledConnection( );
    java.sql.PreparedStatement newPreparedStatement = null;
    java.sql.ResultSet queryStream = null;

    try{
     if(pooledConnection != null){
      newPreparedStatement =
          pooledConnection.prepareStatement("SELECT * FROM tc_Flujo_Txns WHERE c_Txn_Entrada LIKE ? AND c_Txn_Salida LIKE ? AND c_Moneda LIKE ?" + statementPS);
      newPreparedStatement.setString(1,iTxn);
      newPreparedStatement.setString(2,oTxn);
      newPreparedStatement.setString(3,iMon);
      queryStream = newPreparedStatement.executeQuery();
      vflujo = new java.util.Vector();
      sflujo = new java.util.Stack();
      if(queryStream.next()){
       String newString = queryStream.getString("d_Flujo_Txn");
       ventanilla.com.bital.util.NSTokenizer parser = new ventanilla.com.bital.util.NSTokenizer(newString, "*");
       while( parser.hasMoreTokens() ){
        String token = parser.nextToken();
        if( token == null )
          continue;
        vflujo.add(token);
       }
      }
      if(!vflujo.isEmpty())
       invertFlujo();
      else
       sflujo.push(iTxn);
     }
    }
    catch(java.sql.SQLException sqlException){
     System.out.println("Error PageStream::executeQuery: [" + sqlException.toString() + "] ");
    }
    finally{
      try{
        if(queryStream != null){
          queryStream.close();
          queryStream = null;
        }
        if(newPreparedStatement != null){
          newPreparedStatement.close();
          newPreparedStatement = null;
        }
        if(pooledConnection != null){
          pooledConnection.close();
          pooledConnection = null;
        }
      }
      catch(java.sql.SQLException sqlException){
        System.out.println("Error FINALLY PageStream::executeQuery: [" + sqlException.toString() + "] ");
      }
    }
  }

  public void invertFlujo()
  {
   for(int i = vflujo.size() - 1; i >= 0; i--)
    sflujo.push(vflujo.get(i));
  }

  public java.util.Stack getFlujo()
  {
   return sflujo;
  }
}
