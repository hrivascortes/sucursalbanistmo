//*************************************************************************************************
//             Funcion: Clase que realiza la consulta de Cuentas especiales y la conserva en memoria
//          Creado por: Rutilo Zarco Reyes
//      Modificado por: Juvenal Fernandez
//*************************************************************************************************
// CCN -4360160- 30/06/2004 - Se agregan cuentas tipo 1 a tabla hash para verificar cuentas especiales en firmas
// CCN - 4360334 - 17/06/2005 - Se elimina variable dbQualifier evitando enviar "." antes del nombre de la tabla
//*************************************************************************************************

package ventanilla.com.bital.beans;

import ventanilla.com.bital.util.DBQuery;

import java.util.Vector;
import java.util.Hashtable;

public class CuentasEsp
{
    private Vector cuentasesp = null;
    private Hashtable ctas = null;
    private static CuentasEsp instance = null;
    
    private CuentasEsp() 
    {   
        ctas = new Hashtable();
        cuentasesp = new Vector();
        execute();
    }
    
    public static synchronized CuentasEsp getInstance()
    {
        if(instance == null)
            instance = new CuentasEsp();
        return instance;
    }
    
    public Vector getcuentasesp()
    {  
        return this.cuentasesp;
    }
    
    public String getCuentaEsp(String cta)
    {
        String tmp = "";
        if(ctas.containsKey(cta))
            tmp = (String)this.ctas.get(cta);
        return tmp;
    }
    
    public void execute() 
    {
        String sql = null;
        sql = "SELECT C_NUMERO_CUENTA, C_TIPO FROM ";
        String clause = null;
        clause = " ";
        
        DBQuery dbq = new DBQuery(sql,"TC_CUENTAS_ESPLS", clause);
        Vector Regs = dbq.getRows();
        
        int size = Regs.size();
        Vector row = new Vector();
        String tipo = null;
        String cta = null;
        for(int i=0; i<size; i++)
        {
            row  = (Vector)Regs.get(i);
            tipo = row.get(1).toString();
            cta  = row.get(0).toString();
            if (tipo.equals("02"))
            {
               if(Long.parseLong(row.get(0).toString()) != 0)
                  cuentasesp.add(row.get(0).toString());
            }               
            else if (tipo.equals("01"))
               ctas.put(cta, cta.trim()+"~");
        }
    }
}
