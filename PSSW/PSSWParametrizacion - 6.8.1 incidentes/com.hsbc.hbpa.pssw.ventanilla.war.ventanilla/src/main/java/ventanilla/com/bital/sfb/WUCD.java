//************************************************************************************************* 
//             Funcion: Bean para WU Consulta D
//            Elemento: WUCD.java
//          Creado por: Alejandro Gonzalez Castro
//      Modificado por: Rutilo Zarco Reyes
//*************************************************************************************************
// CCN - 4360189 - 03/09/2004 - Se habilita WU
//*************************************************************************************************

package ventanilla.com.bital.sfb;

public class WUCD extends Header3 
{
    private String txtPaginacion    = "";
    private String txtMTCN          = "";

    public String getPaginacion() 
    {
        return txtPaginacion;
    }
    public void setPaginacion(String newPaginacion) 
    {
        txtPaginacion = newPaginacion;
    }

    public String getMTCN() 
    {
        return Filler(txtMTCN,10, "0", "L");
    }
    public void setMTCN(String newMTCN) 
    {
        txtMTCN = newMTCN;
    }

    public String Filler(String cadena, int largo, String filler, String side)
    {
        int cadlong = cadena.length();
        for(int i=cadlong; i<largo; i++)
        {
            if(side.equals("R"))
                cadena = cadena + filler;
            else
                cadena = filler + cadena;
        }
        return cadena;
    }
    
    public String toString() 
    {
        return super.toString() + getPaginacion() + getMTCN();
    }
}

