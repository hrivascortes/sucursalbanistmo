//*************************************************************************************************
//             Funcion: Bean Txn 4033
//      Elaborado por: Jesus Emmanuel Lopez Rosales
//*************************************************************************************************
// CCN -  - 12/01/07 - Se agrega para txn 0021
// CCN - 4360576 - 09/03/2007 - Modificaciones para proyecto OPMN
//*************************************************************************************************
package ventanilla.com.bital.sfb;

import ventanilla.GenericClasses;
import com.bital.util.ParameterNotFoundException;
import com.bital.util.ParameterParser;


public class Txn_0128 extends Transaction
{		 
	public Header doAction(ParameterParser param)
		throws ParameterNotFoundException
	{				
        GenericClasses gc = new GenericClasses();                				
		FormatoB formatob = new FormatoB();
		//String TE = param.getString("iTxn");
		
	    formatob.setTxnCode("0128");		
	    formatob.setFormat( "B" );
        formatob.setFromCurr("N$");
        formatob.setTranAmt(param.getCString( "txtMonto" ));
        formatob.setAcctNo("0128");
        formatob.setReferenc(param.getString("txtNumOrden").toUpperCase());
		return formatob;
	}
	

}
