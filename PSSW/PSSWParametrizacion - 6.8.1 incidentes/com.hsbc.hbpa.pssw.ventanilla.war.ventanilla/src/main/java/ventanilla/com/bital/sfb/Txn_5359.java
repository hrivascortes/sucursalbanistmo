//*************************************************************************************************
//             Funcion: Bean Txn 5359
//          Creado por: Liliana Neyra Salazar
//*************************************************************************************************
// CCN - 4360602 - 18/05/2007 - Se realizan modificaciones para proyecto Codigo de Barras SUAS
//*************************************************************************************************
package ventanilla.com.bital.sfb;

import com.bital.util.ParameterParser;
import com.bital.util.ParameterNotFoundException;
import ventanilla.GenericClasses;

public class Txn_5359 extends Transaction
{
	public Header doAction(ParameterParser param)
		throws ParameterNotFoundException
	{
		FormatoA formatoa = new FormatoA();
		GenericClasses gc = new GenericClasses();
		String Monto = "";
		formatoa.setTxnCode("5359");
		formatoa.setFormat("A");
		
		formatoa.setAcctNo(param.getString("txtDDACuenta"));
		Monto = gc.quitap(param.getString("txtMonto"));
		formatoa.setTranAmt(Monto);
		formatoa.setMoAmoun("000");
		String txtDescripcion = "CARGO PARA PAGO SERV. X COBRANZA";
		formatoa.setTranDesc(txtDescripcion);
		String DescRev = "REV." + txtDescripcion;
		if(DescRev.length() > 40)
			DescRev = DescRev.substring(0,40);
		formatoa.setDescRev(DescRev);
		if ( param.getString("override") != null )
			if (param.getString("override").toString().equals("SI"))
				formatoa.setOverride("3");
			if (param.getString("supervisor") != null )
				formatoa.setSupervisor(param.getString("supervisor"));
		String moneda = gc.getDivisa(param.getString("moneda"));
		formatoa.setTranCur(moneda);

		return formatoa;
	}
}
