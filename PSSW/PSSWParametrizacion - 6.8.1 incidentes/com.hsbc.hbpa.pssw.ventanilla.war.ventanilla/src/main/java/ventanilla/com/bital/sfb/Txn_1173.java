//*************************************************************************************************
//             Funcion: Bean Txn 0216
//      Modificado por: Alejandro Gonzalez Castro
//*************************************************************************************************
// CCN - 4360268 - 04/02/2005 - Se realizan adecuaciones para eliminar Agencia NY
// CCN - 4360424 - 03/02/2006 - Se agrega txn para divisas.
//*************************************************************************************************
package ventanilla.com.bital.sfb;

import com.bital.util.ParameterParser;
import com.bital.util.ParameterNotFoundException;
import ventanilla.GenericClasses;

public class Txn_1173 extends Transaction
{
	public Header doAction(ParameterParser param)
		throws ParameterNotFoundException
	{
		FormatoA formatoa = new FormatoA();
		GenericClasses gc = new GenericClasses();
		
		formatoa.setTxnCode("1173");
		formatoa.setFormat("A");
		formatoa.setAcctNo(param.getString("txtDDACuenta2"));
		formatoa.setTranAmt(param.getCString("txtMonto"));
		String moneda = gc.getDivisa(param.getString("moneda"));
		formatoa.setTranCur(moneda);
		formatoa.setCheckNo(param.getString("txtSerial2"));
		formatoa.setTrNo(param.getString("txtFolioIT5"));
		formatoa.setCashOut(param.getCString("txtMonto"));
		formatoa.setMoAmoun("000");
		formatoa.setFees("8");
		String cvetran = param.getString("txtCveTran");
		formatoa.setTrNo1(cvetran.substring(2,5));
		String codseg = param.getString("txtCodSeg");
		formatoa.setTrNo2(codseg.substring(0,3));
		
		return formatoa;
	}
}
