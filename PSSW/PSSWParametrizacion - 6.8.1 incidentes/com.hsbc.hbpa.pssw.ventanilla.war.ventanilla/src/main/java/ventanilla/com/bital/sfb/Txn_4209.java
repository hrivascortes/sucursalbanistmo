//*************************************************************************************************
//             Funcion: Bean Txn 4209 Impresion de Cheque de Gerencia.
//             Elaborado por: YGX
//*************************************************************************************************

package ventanilla.com.bital.sfb;


import ventanilla.GenericClasses;
import com.bital.util.ParameterNotFoundException;
import com.bital.util.ParameterParser;


public class Txn_4209 extends Transaction
{		 
	public Header doAction(ParameterParser param)
		throws ParameterNotFoundException
	{				
        GenericClasses gc = new GenericClasses();   
        FormatoH formatoh = new FormatoH();         				
		ventanilla.com.bital.sfb.Cheques Cheques = new ventanilla.com.bital.sfb.Cheques();
		
		formatoh.setTxnId("ODPA");
		formatoh.setTxnCode("4209");
	    formatoh.setFormat("H");   
	    
	    //Cuenta de Cheques de Gerencia
	    formatoh.setAcctNo("0103000023");
	    formatoh.setNatCurr("");
	    formatoh.setTranDesc(param.getString("txtBeneficiario").trim());
	    
	    //Cuenta de Cuenta informativa se env�a la cta del cargo
	    formatoh.setAcctDeb(param.getString("txtCuenta").trim());	    
	    formatoh.setTranAmt(gc.delCommaPointFromString(param.getString("txtMonto")).trim());
	    
	    int moneda = param.getInt("moneda"); 
	    formatoh.setTranCur(gc.getDivisa(param.getString("moneda")));
	    formatoh.setCheckNo(param.getCString("txtSerial"));	    
	    formatoh.setCashIn("000");
	    formatoh.setCashOut("");
	    formatoh.setDraftAm("");
	    formatoh.setFees("");
	    
	    String connureg= (String)param.getString("txtConureg").trim();
	    //System.out.println("connureg_tw_chqger" + connureg);
	    formatoh.setDescRev("REV IMPRESION CG"  + connureg);
	    	   	
	   	//System.out.println(formatoh);
	   	
		return formatoh;
	}
	

}
