//*************************************************************************************************
//             Funcion: Clase que realiza la consulta los bines de la tabla
//          Creado por: Juvenal R. Fernandez V.
//      Modificado por: Juvenal R. Fernandez V.
//*************************************************************************************************
// CCN - 4360160 - 30/06/2004 - Se crea la clase para realizar select y mantener los datos en memoria
// CCN - 4360259 - 21/01/2005 - Se modifica la lectura de los bines como mejora al proceso
// CCN - 4360334 - 17/06/2005 - Se elimina variable dbQualifier evitando enviar "." antes del nombre de la tabla
//*************************************************************************************************

package ventanilla.com.bital.beans;

import java.util.Vector;

import ventanilla.com.bital.util.DBQuery;

public class BinesTDC
{
    private Vector bines = null;
    private static BinesTDC instance = null;
    
    private BinesTDC() 
    {
        bines = new Vector();
        execute();
    }
    
    public static synchronized BinesTDC getInstance()
    {
        if(instance == null)
            instance = new BinesTDC();
        return instance;
    }

    public Vector getBines() 
    {
        return bines;
    }    
    
    public void execute() 
    {
        String sql = null;
        sql = "SELECT C_BIN, D_ID_BANCO, LENGTH(RTRIM(C_BIN)) AS LEN FROM ";
        String clause = null;
        clause = " ORDER BY LEN DESC, C_BIN ASC";
        
        DBQuery dbq = new DBQuery(sql,"TC_BINES_TDC", clause);
        bines = dbq.getRows();
                
    }
}
