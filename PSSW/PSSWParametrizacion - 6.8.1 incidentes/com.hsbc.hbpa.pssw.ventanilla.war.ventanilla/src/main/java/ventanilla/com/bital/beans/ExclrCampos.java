//*************************************************************************************************
//             Funcion: Clase que realiza la consulta de Campos a desplegar de la txn y los conserva en memoria
//          Creado por: Rutilo Zarco Reyes
//      Modificado por: Juvenal R. Fernandez V.
//*************************************************************************************************
// CCN -4360152- 21/06/2004 - Se crea la clase para realizar select y mantener los datos en memoria
// CCN - 4360334 - 17/06/2005 - Se elimina variable dbQualifier evitando enviar "." antes del nombre de la tabla
//*************************************************************************************************

package ventanilla.com.bital.beans;

import ventanilla.com.bital.util.DBQuery;

import java.util.Vector;
import java.util.Hashtable;

public class ExclrCampos
{
    private Hashtable exclrcampos = null;
    private static ExclrCampos instance = null;
    
    private ExclrCampos() 
    {   
        exclrcampos = new Hashtable();
        execute();
    }
    
    public static synchronized ExclrCampos getInstance()
    {
        if(instance == null)
            instance = new ExclrCampos();
        return instance;
    }
    
    public Vector getexclrcampos(String iTxn, String oTxn, String transaction, String iCurrency)
    {
        Vector excampos = new Vector();
        String dcampos = null;
        if(exclrcampos.containsKey(iTxn + oTxn + transaction + iCurrency))
            dcampos = (String)this.exclrcampos.get(iTxn + oTxn + transaction + iCurrency);
        if (dcampos != null)
        {
          ventanilla.com.bital.util.NSTokenizer newParser = new ventanilla.com.bital.util.NSTokenizer(dcampos, "*");
          while( newParser.hasMoreTokens() ){
            String newToken = newParser.nextToken();
            if( newToken == null )
              continue;
            excampos.add(newToken);
          }
        }
        return excampos;
    }
    
    public void execute() 
    {
        String sql = null;
        sql = "SELECT C_TXN_ENTRADA,C_TXN_SALIDA,C_TXN_ACTUAL,C_MONEDA,D_CAMPOS FROM ";
        String clause = null;
        clause = " ";
        
        DBQuery dbq = new DBQuery(sql,"TC_EXCLR_CAMPOS", clause);
        Vector Regs = dbq.getRows();
        
        int size = Regs.size();
        Vector row = new Vector();
        String txne = null;
        String txns = null;
        String txna = null;
        String mone = null;

        for(int i=0; i<size; i++)
        {
            row = (Vector)Regs.get(i);
            txne = fillField(row.get(0).toString(),4);
            txns = fillField(row.get(1).toString(),4);
            txna = fillField(row.get(2).toString(),4);
            mone = fillField(row.get(3).toString(),2);

            exclrcampos.put(txne + txns + txna + mone, row.get(4).toString());
        }
    }
    
    private String fillField(String Data, int length){
       int newTemp = Data.length();
       for(int i = newTemp; i < length; i++)
        Data = Data + " ";
       return Data;
    }
}
