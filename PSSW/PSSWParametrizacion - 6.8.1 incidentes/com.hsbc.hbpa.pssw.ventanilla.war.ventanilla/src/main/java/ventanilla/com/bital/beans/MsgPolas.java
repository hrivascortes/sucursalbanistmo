//*************************************************************************************************
//             Funcion: Bean que contiene los mensajes de entrada y salida de la comunicacion con el web Service de POLAS
//            Elemento: MsgPolas.java
//*************************************************************************************************
package ventanilla.com.bital.beans;

import com.hbmx.cppcore.model.Payment;
import com.hbmx.cppcore.model.communication.responses.NotificationResponseMessage;
import com.hbmx.cppcore.model.communication.responses.QueryResponseMessage;
import com.hbmx.cppcore.model.communication.responses.CancellationResponseMessage;


public class MsgPolas  {
	
   
	  private String codigo  = "";
	  private String mensaje   = "";
	  private String folio   = "";
	  private String monto = "";
	  private String estatus = "";
	  private Payment pago = null;
	  private QueryResponseMessage msgResCon=null;
	  private NotificationResponseMessage msgResNot=null;
	  private CancellationResponseMessage msgResCan=null;
	
	  
   public MsgPolas(Payment pago) {
			this.pago=pago;
   }
   
   public void setMsgOutCon(QueryResponseMessage msgResCon) {
		this.msgResCon = msgResCon;
	  }
    
   public void setMsgOutNot(NotificationResponseMessage msgResNot) {
		this.msgResNot = msgResNot;
	  }
   
   public void setMsgOutCan(CancellationResponseMessage msgResCan) {
		this.msgResCan = msgResCan;
	  }
   
   public QueryResponseMessage getMsgOutCon() {
		return msgResCon;
	  }
   
  public NotificationResponseMessage getMsgOutNot( ) {
		return msgResNot;
	  }
  
  public CancellationResponseMessage getMsgOutCan() {
		return msgResCan;
	  }
	  
   public String getMonto() {
		return monto;
	}

	public void setMonto(String monto) {
		this.monto = monto;
	}

	public String getCodigo()
	  {
		return codigo;
	  }
	  
	  public String getMensaje() {
		return mensaje;
	  }
	  
	  public void setCodigo(String codigo) {
		this.codigo = codigo;
	  }
	  
	 	  
	  public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	  }
	  
	public String getEstatus() {
		return estatus;
	}

	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}
	
	public String getFolio() {
		return folio;
	}

	public void setFolio(String folio) {
		this.folio = folio;
	}
	
  
	public StringBuffer getMsgIn(){
	        StringBuffer sbResultado=new StringBuffer();
	        
	    	sbResultado.append("canal=");
	    	if(pago.getChannel()==null){
	    		sbResultado.append("ND");
	    	}else{
	    		sbResultado.append(pago.getChannel());
	    		}
	    	sbResultado.append("*"); 
	    	
	    	sbResultado.append("cuenta=");
	    	sbResultado.append(pago.getAccount());
	    	sbResultado.append("*"); 
	    	
	    	sbResultado.append("servicio=");
	    	sbResultado.append(pago.getService());
	    	sbResultado.append("*"); 
	    	
	    	sbResultado.append("referencia=");
	    	if(pago.getReference()==null){
	    		sbResultado.append("ND");
	    	}else{
	    		sbResultado.append(pago.getReference());
	    	}
	    	sbResultado.append("*");  
	    	
	    	sbResultado.append("monto=");
	    	if (pago.getAmount()==null){
	    		sbResultado.append("ND");
	    	}else{
	    		sbResultado.append(pago.getAmount());
	    	}
	    	sbResultado.append("*"); 
	    	
	    	sbResultado.append("fecha=");
	    	if (pago.getChannelDate()==null){
	    		sbResultado.append("ND");
	    	}else{
	    		sbResultado.append(pago.getChannelDate());
	    	}
	    	sbResultado.append("*");
	    
	    	sbResultado.append("notransaccion=");
	    	sbResultado.append(pago.getTransactionNumber());
	    	sbResultado.append("*"); 
	    	
	    	sbResultado.append("sucursal=");
	    	sbResultado.append(pago.getBranchNumber());
	    	sbResultado.append("*");
	    	
	    	sbResultado.append("tipopago=");
	    	if (pago.getType()==null){
	    		sbResultado.append("ND");
	    	}else{
	    	   	sbResultado.append(pago.getType());
	    	}
	    	sbResultado.append("*");
	    	
	    	if(sbResultado.toString().length()>700){
	    		StringBuffer sbAux=new StringBuffer(sbResultado.toString().substring(0,700));
	    		sbResultado=sbAux;
				}
	    	
	    	return sbResultado;
	    	
	 }

	
	  
	  
}
