//************************************************************************************************* 
//             Funcion: Clase que controla la emision de volantes para cheques C.I.
//          Creado por: Alejandro Gonzalez Castro
//      Modificado por: Juvenal R. Fernandez V.
//*************************************************************************************************
// CCN - 4360159 - 27/05/2004 - se cierran correctamente cursores a DB2 / netscape
// CCN - 4360164 - 07/07/2004 - se cierran correctamente cursores a DB2 / explorer
// CCN - 4360211 - 08/10/2004 - Se elimina mensaje de Documentos pendientes por Digitalizar y se agrega PosFixUR
// CCN - 4360334 - 17/06/2005 - Se elimina variable dbQualifier
//*************************************************************************************************

package ventanilla.com.bital.util; 

public class VolanteoDDA extends Object{
  private String statementPS = com.bital.util.ConnectionPoolingManager.getStatementPostFix();
  private String PostFixUR = com.bital.util.ConnectionPoolingManager.getPostFixUR();
  private String inputCurrency = new String("N$");
  private String inputBranch   = new String("0001");
  private String inputTeller   = new String("01");
  private String inputBank     = new String("001");
  private String chequeData    = new String("");
  private int    volanteoConsecutive = 1;
  private int    chequeConsecutive   = 0;
  private String inputCorte    = new String("");
  private String buildConsecutive = new String("");
  private String volanteCerrado = new String("N");
  private char   existeVolante = 'N';
  private int    lastVolanteoConsecutive = 0;
  private int NumChequesPosteados = 0;
  private String chequeAmount = "000";

  public VolanteoDDA(){
  }

  public void setCurrency(String inputCurrency){
    this.inputCurrency = inputCurrency;
  }

  public String getCurrency(){
    return this.inputCurrency;
  }

  public void setBranch(String inputBranch){
    this.inputBranch = inputBranch;
  }

  public String getBranch(){
    return this.inputBranch;
  }

  public void setTeller(String inputTeller){
    this.inputTeller = inputTeller;
  }

  public String getTeller(){
    return this.inputTeller;
  }

  public void setBank(String inputBank){
    this.inputBank = inputBank;
  }

  public String getBank(){
    return this.inputBank;
  }

  public void setCorte(String inputCorte){
    this.inputCorte = inputCorte;
  }

  public String getCorte(){
    return this.inputCorte;
  }

  public void setChequeData(String chequeData){
    this.chequeData = chequeData;
  }

  public String getChequeData(){
    return this.chequeData;
  }

  public void searchPreviousVolanteo(){
    java.sql.Connection pooledConnection = com.bital.util.ConnectionPoolingManager.getPooledConnection( );
    java.sql.PreparedStatement newPreparedStatement = null;
    java.sql.ResultSet newResultSet = null;

    try{
      if(pooledConnection != null){
        newPreparedStatement =
        pooledConnection.prepareStatement("SELECT c_consec_volanteo FROM tw_volanteo_dda where c_numero_sucursal = ? and c_numero_cajero = ? and c_numero_banco = ? ORDER BY c_consec_volanteo DESC" + PostFixUR);        
        newPreparedStatement.setString(1,this.getBranch());
        newPreparedStatement.setString(2,this.getTeller());
        newPreparedStatement.setString(3,this.getBank());
        newResultSet = newPreparedStatement.executeQuery();
        if(newResultSet.next()){
          this.lastVolanteoConsecutive = Integer.parseInt(newResultSet.getString("c_consec_volanteo"));
        }
        if(newResultSet != null){
          newResultSet.close();
          newResultSet = null;
        }
        if(newPreparedStatement != null){
          newPreparedStatement.close();
          newPreparedStatement = null;
        }        
        this.volanteoConsecutive = this.lastVolanteoConsecutive;
        newPreparedStatement =
        pooledConnection.prepareStatement("SELECT c_consec_volanteo, c_consec_cheque, d_volante_cerrado FROM tw_volanteo_dda where c_numero_sucursal = ? and c_numero_cajero = ? and c_numero_banco = ? and c_moneda = ? and d_corte = ? ORDER BY c_consec_volanteo DESC" + PostFixUR);        
        newPreparedStatement.setString(1,this.getBranch());
        newPreparedStatement.setString(2,this.getTeller());
        newPreparedStatement.setString(3,this.getBank());
        newPreparedStatement.setString(4,this.getCurrency());
        newPreparedStatement.setString(5,this.getCorte());
        newResultSet = newPreparedStatement.executeQuery();
        this.volanteoConsecutive = this.lastVolanteoConsecutive + 1;
        if(newResultSet.next()){
          this.volanteoConsecutive = Integer.parseInt(newResultSet.getString("c_consec_volanteo"));
          this.chequeConsecutive = Integer.parseInt(newResultSet.getString("c_consec_cheque"));
          this.volanteCerrado = newResultSet.getString("d_volante_cerrado");
          this.existeVolante = 'S';
          if(this.chequeConsecutive == 99 || this.volanteCerrado.equals("S")){
            this.volanteoConsecutive = ++this.lastVolanteoConsecutive;
            this.chequeConsecutive = 0;
            this.existeVolante = 'N';
          }
        }
      }
    }
    catch(java.sql.SQLException sqlException){
      System.out.println("Error volanteoDDA::searchPreviousVolanteo ++" + sqlException.toString() + "++");
    }
    finally{
      try{
        if(newResultSet != null){
          newResultSet.close();
          newResultSet = null;
        }
        if(newPreparedStatement != null){
          newPreparedStatement.close();
          newPreparedStatement = null;
        }
        if(pooledConnection != null){
          pooledConnection.close();
          pooledConnection = null;
        }
      }
      catch(java.sql.SQLException sqlException){
        System.out.println("Error FINALLY volanteoDDA::searchPreviousVolanteo ++" + sqlException.toString() + "++");
      }
    }
  }

  public void buildConsecutive(){
    this.searchPreviousVolanteo();
    this.chequeConsecutive++;
  }

  public String getConsecutive(){
    this.buildConsecutive();
    this.buildConsecutive = this.getBranch() + this.getTeller() +
    this.volanteoConsecutive;
    return this.buildConsecutive;
  }

  public void setChequeAmount(String chequeAmount){
    this.chequeAmount = chequeAmount;
  }

  public String getChequeAmount(){
    return this.chequeAmount;
  }

  public int getNumDoctosNotGiven(){  
    return this.NumChequesPosteados;
  }
  
  private void updateVolanteoDDA(){
    java.sql.Connection pooledConnection = com.bital.util.ConnectionPoolingManager.getPooledConnection( );
    java.sql.PreparedStatement newPreparedStatement = null;
    int updateResponse = 0;
    try{
      if(pooledConnection != null){
        if(this.existeVolante == 'N'){
          newPreparedStatement =
          pooledConnection.prepareStatement("INSERT INTO tw_volanteo_dda VALUES(?,?,?,?,?,?,?,?)");
          newPreparedStatement.setString(1,this.getBranch());
          newPreparedStatement.setString(2,this.getTeller());
          newPreparedStatement.setInt(3,this.volanteoConsecutive);
          newPreparedStatement.setString(4,this.getBank());
          newPreparedStatement.setInt(5,this.chequeConsecutive);
          newPreparedStatement.setString(6,this.getCurrency());
          newPreparedStatement.setString(7,this.getCorte());
          newPreparedStatement.setString(8,"N");
          updateResponse = newPreparedStatement.executeUpdate();
        }
        else{ 
          newPreparedStatement =
          pooledConnection.prepareStatement("UPDATE tw_volanteo_dda set  c_consec_cheque = ? where c_numero_sucursal = ? and c_numero_cajero = ? and c_numero_banco = ? and c_moneda = ? and d_corte = ? and c_consec_volanteo = ?" + statementPS);          
          newPreparedStatement.setInt(1,this.chequeConsecutive);
          newPreparedStatement.setString(2,this.getBranch());
          newPreparedStatement.setString(3,this.getTeller());
          newPreparedStatement.setString(4,this.getBank());
          newPreparedStatement.setString(5,this.getCurrency());
          newPreparedStatement.setString(6,this.getCorte());
          newPreparedStatement.setInt(7,this.volanteoConsecutive);
          updateResponse = newPreparedStatement.executeUpdate();
        }
      }
    }
    catch(java.sql.SQLException sqlException){
      System.out.println("Error volanteoDDA::updateConsecutive ++" + sqlException.toString() + "++");
    }
    finally{
      try{
        if(newPreparedStatement != null){
          newPreparedStatement.close();
          newPreparedStatement = null;
        }
        if(pooledConnection != null){
          pooledConnection.close();
          pooledConnection = null;
        }
      }
      catch(java.sql.SQLException sqlException){
      }
    }
  }

  private void insertConsecVolDDA(String hoganConsecutive, String GenVolante){
    java.sql.Connection pooledConnection = com.bital.util.ConnectionPoolingManager.getPooledConnection( );
    java.sql.PreparedStatement newPreparedStatement = null;
    int updateResponse = 0;
    try{
      if(pooledConnection != null){
        newPreparedStatement =
        pooledConnection.prepareStatement("INSERT INTO tw_consec_vol_dda VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
        String txtConsecutive = " ";
        if ( GenVolante.equals("SI"))
            txtConsecutive =   this.getConsecutive();
        newPreparedStatement.setString(1,this.getBranch());
        newPreparedStatement.setString(2,this.getTeller());
        newPreparedStatement.setString(3,this.getBank() + txtConsecutive);
        newPreparedStatement.setString(4,this.getChequeData().trim());
        newPreparedStatement.setString(5,hoganConsecutive.trim());
        newPreparedStatement.setString(6,this.getCurrency());
        newPreparedStatement.setString(7,this.getCorte());
        newPreparedStatement.setString(8,"N");
        newPreparedStatement.setString(9,this.getChequeAmount());
        newPreparedStatement.setString(10,"A");
        newPreparedStatement.setString(11,"N");
        newPreparedStatement.setString(12,"N");
        newPreparedStatement.setString(13,"");
        newPreparedStatement.setString(14,"");
        updateResponse = newPreparedStatement.executeUpdate();
      }
    }
    catch(java.sql.SQLException sqlException){
      System.out.println("Error volanteoDDA::insertConsecVolDDA ++" + sqlException.toString() + "++");
    }
    finally{
      try{
        if(newPreparedStatement != null){
          newPreparedStatement.close();
          newPreparedStatement = null;
        }
        if(pooledConnection != null){
          pooledConnection.close();
          pooledConnection = null;
        }
      }
      catch(java.sql.SQLException sqlException){
      }
    }
  }

  public void updateConsecutive(String hoganConsecutive, String GenVolante){
    if ( GenVolante.equals("SI"))
        this.updateVolanteoDDA();
    hoganConsecutive = "000" + hoganConsecutive;
    //System.out.println("VolanteoDDA::Hogan consecutivo <"+hoganConsecutive+">"); 
    hoganConsecutive = hoganConsecutive.substring(hoganConsecutive.length()-7);
    this.insertConsecVolDDA(hoganConsecutive, GenVolante);
   // this.searchDoctosNotGiven();
  }
  
  public void searchDoctosDig(String Opcion){
    java.sql.Connection pooledConnection = com.bital.util.ConnectionPoolingManager.getPooledConnection( );
    java.sql.PreparedStatement newPreparedStatement = null;
    java.sql.ResultSet newResultSet = null;

    try{
      if(pooledConnection != null)
      {
       if ( Opcion.equals("2") ) {                                              // No digitalizados de los entregados
             newPreparedStatement = pooledConnection.prepareStatement("SELECT count(*) as numberDoctos FROM tw_consec_vol_dda where c_numero_sucursal = ? and c_numero_cajero = ? " + PostFixUR);
             newPreparedStatement.setString(1,this.getBranch());
             newPreparedStatement.setString(2,this.getTeller());
        }
        
        if(Opcion.equals("1"))
        {
             newPreparedStatement = pooledConnection.prepareStatement("SELECT count(*) as numberDoctos FROM tw_cheques_dig where c_numero_sucursal = ? and c_numero_cajero = ? " + PostFixUR);
             newPreparedStatement.setString(1,this.getBranch());
             newPreparedStatement.setString(2,this.getTeller());
        }

        
        newResultSet = newPreparedStatement.executeQuery();
        if(newResultSet.next()){
          this.NumChequesPosteados = Integer.parseInt(newResultSet.getString("numberDoctos"));
        }
        if(newResultSet != null){
          newResultSet.close();
          newResultSet = null;
        }
      }
    }
    catch(java.sql.SQLException sqlException){
      System.out.println("Error volanteoDDA::searchDoctosDig ++" + sqlException.toString() + "++");
    }
    finally{
      try{
        if(newResultSet != null){
          newResultSet.close();
          newResultSet = null;
        }
        if(newPreparedStatement != null){
          newPreparedStatement.close();
          newPreparedStatement = null;
        }
        if(pooledConnection != null){
          pooledConnection.close();
          pooledConnection = null;
        }
      }
      catch(java.sql.SQLException sqlException){
        System.out.println("Error FINALLY volanteoDDA::searchDoctosDig ++" + sqlException.toString() + "++");
      }
    }
  }
}
