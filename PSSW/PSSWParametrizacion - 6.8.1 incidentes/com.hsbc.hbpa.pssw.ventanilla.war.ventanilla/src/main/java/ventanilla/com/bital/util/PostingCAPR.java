//************************************************************************************************* 
//             Funcion: Clase para posteo de txn 0200, 0210, 0420 y 0430
//            Elemento: PostingCAPR.java
//          Creado por: EVA
//*************************************************************************************************
// CO - CR 270926	 -   Proyecto POLAS
//*************************************************************************************************

package ventanilla.com.bital.util;

import java.sql.Timestamp;
import javax.servlet.http.HttpSession;
import com.hbmx.cppcore.model.Payment;

import ventanilla.com.bital.admin.InterfasesLog;
import ventanilla.com.bital.beans.DatosVarios;
import ventanilla.com.bital.beans.MsgPolas;
import ventanilla.com.bital.sfb.ServicioRAPL;
import ventanilla.com.bital.util.ServiciosPolas;
import java.util.Hashtable;
import java.util.Date;
import java.util.StringTokenizer;

import ventanilla.GenericClasses;

public class PostingCAPR {
	
	public PostingCAPR (){

	}
	
	public int excutePosting(Object bean,HttpSession session, String Connureg){
		int iResultado=0;
		
		MsgPolas mp=null;
    	String sErrorSP=null;
    	String sStatusRefe=null;
    	String sMonto=null;
    	String sMensaje = null;
    	
    	GenericClasses GC = new GenericClasses();
		Hashtable htCamposPagina = (Hashtable)session.getAttribute("page.datasession");
    	  	
    	String sCveServicio = (String)htCamposPagina.get("servicio1");
    	//Obtengo bean del servicio
    	DatosVarios rapl = DatosVarios.getInstance();                   
        Hashtable htRAPL = null;        
        htRAPL =(Hashtable)rapl.getDatosH("SERVICRAPL");
        ServicioRAPL srapl = (ServicioRAPL)htRAPL.get(sCveServicio.trim());        	
    	//Este If es para identificar si el servicio tiene consulta: Mto_X_Referencia
    	if(srapl.isConsulta()){
    		//Entra en este if cuando el servicio tiene consulta (Mto_X_Referencia)
    		           	 
        	String sReferencia =  (String)htCamposPagina.get("txtReferencia");
        	Hashtable UserInfo = (Hashtable)session.getAttribute("userinfo");
              	String sCveCajero = (String)UserInfo.get("C_CAJERO");       
        	
    		//Se arma el pago.
    		Payment pago = new Payment();
   		    pago.setAccount(0);
 	        pago.setChannel("PSSWPA");     	   
 	        pago.setService(Long.parseLong(sCveServicio));
	        pago.setReference(sReferencia.trim());
	        Date date = new java.util.Date();
	        java.sql.Timestamp timeStampDate = new Timestamp(date.getTime());
	        pago.setChannelDate(timeStampDate);
	        pago.setBranchNumber(Long.parseLong(sCveCajero.substring(0, 4)));	        
	        mp = new MsgPolas(pago);
	        InterfasesLog log = new InterfasesLog();
	        Hashtable datosCom = new Hashtable();
   	    	datosCom.put("D_SUCURSAL",(String)session.getAttribute("branch"));
   	    	datosCom.put("D_OPERADOR",(String)session.getAttribute("teller"));
   	    	datosCom.put("D_DIVISA",GC.getDivisa((String)session.getAttribute("page.moneda")));
   	    	datosCom.put("D_TXN", (String)session.getAttribute("page.cTxn"));        		
   	    	datosCom.put("D_CONNUREG", (String)session.getAttribute("page.diario_consecutivo"));
   	    	//datosCom.put("D_CONNUREG", "");
   	    	datosCom.put("D_CONSECLIGA", GC.getDate(3));
   	    	datosCom.put("D_INTERFASE", "POLAS");
   	    	datosCom.put("D_SPFUNC_TXN", "CAPR");
   	    	datosCom.put("D_ETAPA_TXN", "P");
   	    	datosCom.put("D_ESTATUS", "E");
   	    	datosCom.put("D_MOTIVO", "C");
   	    	log.setInfo(datosCom);
   	    	
   	    	String metodos="canal=getChannel&cuenta=getAccount&servicio=getService&referencia=getReference&monto=getAmount&fecha=getChannelDate&notransaccion=getTransactionNumber&sucursal=getBranchNumber&tipopago=getType&";
   	    	log.insertLog(pago, metodos);

  	    	
    		ServiciosPolas servipolas = new ServiciosPolas(); //Se crea objeto para hacer la consulta      	    	
  	    	servipolas.executeConsultaRef(pago,mp, session);//consulta referencia
  	    		        
  	    	
  	    	StringBuffer MsgOut=new StringBuffer();
  	    	MsgOut.append(createResponse(mp,"codigo=getCodigo&"));
  	    	Object objeto=mp.getMsgOutCon();
  	    	if(objeto==null){
  	    		metodos="mensaje=getMensaje&";
      	    	objeto=mp;
      	    	MsgOut.append(createResponse(objeto,metodos));
  	    	}
  	    	else{
  	    		metodos="mensaje=getMessage&monto=getAmount&statusRef=getError&";
  	    		MsgOut.append(createResponse(objeto,metodos));
  	    	}
  	    	
  	    	
  	        iResultado = Integer.parseInt(mp.getCodigo());
  	        sErrorSP = mp.getCodigo();
  	        sStatusRefe = mp.getEstatus();
  	        sMonto = mp.getMonto();
  	        sMensaje = mp.getMensaje();
			
        	log.updateLog((String)session.getAttribute("page.diario_consecutivo"),"R",MsgOut.toString(),"C",(String)session.getAttribute("page.cTxn"));
        	
        }else{
        	iResultado=1;
        	sErrorSP = "0";
        	sStatusRefe = "0";
        	sMonto="0";
        	sMensaje = "";
        	
        }
    	//Bandera de error de Servicio Polas {0 .- Exitoso, cualquier otro error}
    	session.setAttribute("bnderrorSP",sErrorSP);
    	//Estatus de referencia {0 .- Procede el pago, cualquier otro no procede el pago}
    	session.setAttribute("statusrefeSP",sStatusRefe);
    	//Monto de la referencia.
    	session.setAttribute("mtoSP",sMonto);
    	session.setAttribute("mensajeSP", sMensaje);
    	Hashtable datasession = (Hashtable)session.getAttribute("page.datasession");
    	//datasession.put("txtMonto",sMonto);
    	session.setAttribute("page.datasession", datasession);
		return iResultado;	
	}
	
	private String createResponse(Object obj, String method){
		GenericClasses GC = new GenericClasses();
		StringBuffer result = new StringBuffer();
		StringTokenizer datos = new StringTokenizer(method,"&");
		while(datos.hasMoreTokens()){
			StringTokenizer values = new StringTokenizer(datos.nextToken().trim(),"=");
			while(values.hasMoreTokens()){
				result.append(values.nextToken())
					  .append("=")
					  .append(GC.getBeanData(obj, values.nextToken()));
			}
			result.append("|");
		}
		
		return result.toString();
	}
	
	
}

