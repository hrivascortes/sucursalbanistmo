// Transaccion 1503, 1505, 1507, 1509, 1511 & 1533
package ventanilla.com.bital.sfb;

import com.bital.util.ParameterParser;
import com.bital.util.ParameterNotFoundException;

public class Group10 extends Transaction
{
  public Header doAction(ParameterParser param)
    throws ParameterNotFoundException
  {
    // Instanciar el bean a utilizar
    Deposito deposito = new Deposito();

    // Ingresar datos requeridos
    deposito.setAcctNo(param.getString("txtDDACuenta"));
    deposito.setTranAmt(param.getCString("txtMonto"));

    int moneda = param.getInt("moneda");
    if( moneda == 1 )
      deposito.setTranCur("N$");
    else if( moneda == 2 )
      deposito.setTranCur("US$");
    else
      deposito.setTranCur("UDI");

    if( param.getString("txtFechaEfect", null) != null )
    {
      String fecha1 = param.getString("txtFechaEfect");
      String fecha2 = param.getString("txtFechaSys");
      fecha1 = fecha1.substring(6) + fecha1.substring(3, 5) + fecha1.substring(0, 2);

      if( !fecha1.equals(fecha2) )
      {
        fecha2 = fecha1.substring(4, 6) + fecha1.substring(6) + fecha1.substring(2, 4);
        deposito.setEffDate(fecha2);
      }
    }
    
    return deposito;
  }               
}
