//*************************************************************************************************
//             Funcion: Clase que realiza la consulta de Monedas a desplgar y los conserva en memoria
//          Creado por: Juvenal R. Fernandez V.
// Ultima Modificacion: 18/06/2004
//      Modificado por: Juvenal R. Fernandez V.
//*************************************************************************************************
// CCN -4360152- 21/06/2004 - Se crea la clase para realizar select y mantener los datos en memoria
// CCN - 4360334 - 17/06/2005 - Se elimina variable dbQualifier evitando enviar "." antes del nombre de la tabla
// CO - CR7564 - 26/01/2011 - Cambios Proyecto camp
//*************************************************************************************************

package ventanilla.com.bital.beans;

import ventanilla.com.bital.util.DBQuery;

import java.util.Vector;

public class Monedas
{
     protected Vector monedas = null;
	 protected Vector monedas_SYB = null;
     private static Monedas instance = null;
     
    private Monedas() 
    {
        monedas = new Vector();
        execute();
    }
    
    public static synchronized Monedas getInstance()
    {
        if(instance == null)
            instance = new Monedas();
        return instance;
    }
    
    public Vector getMonedas()
    {
        return this.monedas;
    }
    
    public Vector getMonedas_Syb()
    {
    	return this.monedas_SYB;
    }
    public void execute() 
    {
        String sql = null;
        sql = "SELECT C_NOM_MONEDA, D_DESCRIPCION, D_INTCODE FROM ";
        String clause = null;
        clause = " ORDER BY C_NOM_MONEDA";
        
        DBQuery dbq = new DBQuery(sql,"TC_MONEDAS", clause);
        monedas = dbq.getRows();
        
        sql = "SELECT D_INTCODE, D_DESCRIPCION FROM ";
		clause = " ORDER BY C_NOM_MONEDA";
		DBQuery dbq_2 = new DBQuery(sql,"TC_MONEDAS", clause);
		monedas_SYB = dbq_2.getRows();
        
    }
}
