// Transaccion 0350 & 0372 - Cheques de Caja
package ventanilla.com.bital.sfb;

import com.bital.util.ParameterParser;
import com.bital.util.ParameterNotFoundException;
import ventanilla.GenericClasses;

public class Group30C extends Transaction
{
  public Header doAction(ParameterParser param)
    throws ParameterNotFoundException
  {
    // Instanciar el bean a utilizar
    Cheques cheque = new Cheques();
    
    // Ingresar datos requeridos    
    cheque.setFormat("B");
    cheque.setAcctNo  (param.getString("cTxn"));
    cheque.setTranAmt (param.getCString("txtEfectivo"));
    cheque.setReferenc(param.getString("txtReferenc"));
    cheque.setCashIn  (param.getCString("txtEfectivo"));
	GenericClasses gc = new GenericClasses();
    
    int moneda = param.getInt("moneda");
	cheque.setFromCurr( gc.getDivisa(param.getString("moneda")));

    return cheque;
  }               
}