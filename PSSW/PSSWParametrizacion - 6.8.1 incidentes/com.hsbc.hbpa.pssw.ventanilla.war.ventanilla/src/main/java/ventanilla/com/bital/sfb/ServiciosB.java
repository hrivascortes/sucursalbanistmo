package ventanilla.com.bital.sfb;

public class ServiciosB extends Header
{
  private String txtEffDate = "";
  private String txtAcctNo = "";
  private String txtTranAmt = "";
  private String txtReferenc = "";
  private String txtFeeAmoun = "";
  private String txtIntern = "";
  private String txtIntWh = "";
  private String txtCashIn = "";
  private String txtCashOut = "";
  private String txtFromCurr = "";
  private String txtToCurr = "";

  public String getAcctNo()
  {
	return txtAcctNo + "*";
  }
  public String getEffDate()
  {
	return txtEffDate + "*";
  }
  public String getTranAmt()
  {
	return txtTranAmt + "*";
  }
  public String getReferenc()
  {
    return txtReferenc + "*";
  }
  public String getFeeAmoun()
  {
    return txtFeeAmoun + "*";
  }
  public String getIntern()
  {
    return txtIntern + "*";
  }
  public String getIntWh()
  {
    return txtIntWh + "*";
  }
  public String getCashIn()
  {
   return txtCashIn + "*";
  }
  public String getCashOut()
  {
   return txtCashOut + "*";
  }
  public String getFromCurr()
  {
   return txtFromCurr + "*";
  }
  public String getToCurr()
  {
   return txtToCurr + "*";
  }
  public void setAcctNo(String newAcctNo)
  {
	txtAcctNo = newAcctNo;
  }
  public void setEffDate(String newEffDate)
  {
	txtEffDate = newEffDate;
  }
  public void setTranAmt(String newTranAmt)
  {
	txtTranAmt = newTranAmt;
  }
  public void setReferenc(String newReferenc)
  {
    txtReferenc = newReferenc;
  }
  public void setFeeAmoun(String newFeeAmoun)
  {
    txtFeeAmoun = newFeeAmoun;
  }
  public void setIntern(String newIntern)
  {
    txtIntern = newIntern;
  }
  public void setIntWh(String newIntWh)
  {
    txtIntWh = newIntWh;
  }
  public void setCashIn(String newCashIn)
  {
    txtCashIn = newCashIn;
  }
  public void setCashOut(String newCashOut)
  {
    txtCashOut = newCashOut;
  }
  public void setFromCurr(String newFromCurr)
  {
    txtFromCurr = newFromCurr;
  }
  public void setToCurr(String newToCurr)
  {
    txtToCurr = newToCurr;
  }

  public String toString()
  {
      /*Formato B */
      return super.toString() + getEffDate() + getAcctNo() + getTranAmt()
      + getReferenc() + getFeeAmoun() + getIntern() + getIntWh() + getCashIn()
	  + getCashOut() + getFromCurr() + getToCurr();
  }

}
