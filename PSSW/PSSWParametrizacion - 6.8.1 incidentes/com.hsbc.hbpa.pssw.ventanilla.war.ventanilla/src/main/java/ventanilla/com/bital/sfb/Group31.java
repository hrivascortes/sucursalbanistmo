// Transaccion 0061 - Cheques de Caja
package ventanilla.com.bital.sfb;

import com.bital.util.ParameterParser;
import com.bital.util.ParameterNotFoundException;

public class Group31 extends Transaction
{
  public Header doAction(ParameterParser param)
    throws ParameterNotFoundException
  {
    // Instanciar el bean a utilizar
    Cheques cheque = new Cheques();
    
    // Ingresar datos requeridos
    cheque.setTranAmt( param.getCString("txtMonto") );
    int moneda = param.getInt("moneda");
    if( moneda == 1 )
    {
      cheque.setTranCur("N$");
      cheque.setAcctNo2 ( "0103000023" );
    }
    else if( moneda == 2 )
    {
      cheque.setTranCur("US$");
      cheque.setAcctNo2 ( "3902222222" );
    }
    else
      cheque.setTranCur("UDI");

    cheque.setCheckNo ( param.getString("txtSerial") );
    cheque.setTranDesc( param.getString("txtBeneficiario") );
    cheque.setAcctNo1 ( param.getString("txtDDACuenta") );
//    cheque.setAcctNo2 ( "3901111111" );
    
    return cheque;
  }
}
