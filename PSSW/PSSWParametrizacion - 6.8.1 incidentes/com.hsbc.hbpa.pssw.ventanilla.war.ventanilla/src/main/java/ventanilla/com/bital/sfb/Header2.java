//************************************************************************************************* 
//             Funcion: Header para WU
//            Elemento: Header3.java
//          Creado por: Alejandro Gonzalez Castro
//      Modificado por: Juvenal Fernandez
//*************************************************************************************************
// CCN - 4360334 - 17/06/2005 - Se elimina import de protomatter
//*************************************************************************************************

package ventanilla.com.bital.sfb; 

import com.bital.util.Gateway;
import com.bital.util.GatewayFactory;
import com.bital.util.GatewayException;
//import com.protomatter.syslog.Syslog;

public class Header2 implements Command {
    private int    cStatus = 0;
    private String txtMessage = null;
    private String txtReverso = null;
    private String message = null;
    private String OPSucursal = null;
    
    public void execute() {
        Gateway cg = GatewayFactory.getGateway("");
        
        try {
            message = this.toString();
            if(txtReverso != null)
                message = txtReverso;
            /*System.out.println("Mensaje de Envio Header2 -"+ message+"-");*/
            cg.write(message);
            String result = cg.readLine();
            txtMessage = result;
            /*System.out.println("Mensaje de Res Header2 -"+txtMessage+"-");*/
        }
        catch( GatewayException e ) {
            System.out.println("GATE:" + e.toString());
        }
    }
    
    public int getStatus() {
        return cStatus;
    }
    
    public String getMessage() {
        return txtMessage;
    }
    
    public void setMessage(String mensaje) {
        txtMessage = mensaje;
    }
    
    public void setReverso(String newMessage) {
        txtReverso = newMessage;
    }
    public void setOPSucursal(String newOPSucursal) {
        
        OPSucursal = newOPSucursal.substring(1);
    }
    public String toString() {
        return "DSLZ " + OPSucursal + "    ";
    }
}
