package ventanilla.com.bital.util;

//public class ExtractMenu implements java.io.Serializable
public class ExtractMenu
{
 private String    txnProcess = "";
 private java.util.Vector MenuesTxn = null;
 private java.util.Vector MenuesTxnSec = null;
 private java.util.Vector Monedas = null;
 String statementPS = com.bital.util.ConnectionPoolingManager.getStatementPostFix();
 String dbQualifier = com.bital.util.ConnectionPoolingManager.getdbQualifier();
 
 public ExtractMenu()
 {
 }

 public void setProcess(String newProcess)
 {
  txnProcess = newProcess;
 }

 public String getProcess()
 {
  return txnProcess;
 }

 public void executeQuery()
 {
   java.sql.Connection pooledConnection = com.bital.util.ConnectionPoolingManager.getPooledConnection( );
   java.sql.ResultSet newResultSet = null;
   java.sql.PreparedStatement newPreparedStatement = null;
   try{
    if(pooledConnection != null){
     newPreparedStatement =
          pooledConnection.prepareStatement("SELECT * FROM "+ dbQualifier + ".tc_Menues_Txns WHERE c_Proceso LIKE ? ORDER BY c_Proceso, c_Transaccion" + statementPS);
     newPreparedStatement.setString(1,txnProcess);
     newResultSet = newPreparedStatement.executeQuery();
     MenuesTxn = convertRStoVector(newResultSet);
     if(newResultSet != null) {
       newResultSet.close();
       newResultSet = null;
     }     
     if(newPreparedStatement != null) {
       newPreparedStatement.close();
       newPreparedStatement = null;
     }
     newPreparedStatement =
          pooledConnection.prepareStatement("SELECT * FROM "+ dbQualifier +".tc_Menues_Txn_S WHERE c_Proceso LIKE ? ORDER BY c_Proceso, c_Transaccion" + statementPS);
     newPreparedStatement.setString(1,txnProcess);
     newResultSet = newPreparedStatement.executeQuery();
     MenuesTxnSec = convertRStoVector(newResultSet);
     if(newResultSet != null) {
       newResultSet.close();
       newResultSet = null;
     }     
     if(newPreparedStatement != null) {
       newPreparedStatement.close();
       newPreparedStatement = null;
     }
     newPreparedStatement =
          pooledConnection.prepareStatement("SELECT C_NOM_MONEDA, D_DESCRIPCION FROM "+ dbQualifier +".tc_Monedas ORDER BY c_Nom_Moneda" + statementPS);
     newResultSet = newPreparedStatement.executeQuery();
     Monedas = convertRStoVector(newResultSet);
    }
    else{
     System.out.println("Error ExtractMenu::executeQuery conexisn NULA");
    }
   }
   catch(java.sql.SQLException sqlException){
    System.out.println("Error ExtractMenu::executeQuery [" + sqlException.toString() + "]");
   }
   finally{
     try{
      if(newResultSet != null){
       newResultSet.close();
       newResultSet = null;
      }
      if(newPreparedStatement != null){
       newPreparedStatement.close();
       newPreparedStatement = null;
      }
      if(pooledConnection != null){
        pooledConnection.close();
        pooledConnection = null;
      }
     }
     catch(java.sql.SQLException sqlException){
       System.out.println("Error FINALLY ExtractMenu::executeQuery [" + sqlException.toString() + "]");
     }
   }
 }

 public void executeQueryIsolated()
 {
  java.sql.Connection pooledConnection = com.bital.util.ConnectionPoolingManager.getPooledConnection( );
  java.sql.Statement newStatement = null;
  java.sql.ResultSet newResultSet = null;
  try{
   if(pooledConnection != null){
    newStatement = pooledConnection.createStatement( );
    newResultSet = newStatement.executeQuery("SELECT * FROM "+ dbQualifier +".tc_Menues_Txns where d_Txn_Dsp_Caja='S' ORDER BY c_Proceso, c_Transaccion,c_Moneda" + statementPS);
    MenuesTxn = convertRStoVector(newResultSet);
   }
   else{
    System.out.println("Error en ExtractMenu::executeQueryIsolated conexisn NULA");
   }
  }
  catch(java.sql.SQLException sqlException){
   System.out.println("Error en ExtractMenu::executeQueryIsolated [" + sqlException.toString() + "]");
  }
  finally{
    try{
      if(newResultSet != null){
       newResultSet.close();
       newResultSet = null;
      }
      if(newStatement != null){
        newStatement.close();
        newStatement = null;
      }
      if(pooledConnection != null){
        pooledConnection.close();
        pooledConnection = null;
      }
    }
    catch(java.sql.SQLException sqlException){
     System.out.println("Error FINALLY ExtractMenu::executeQueryIsolated [" + sqlException.toString() + "]");
    }
  }
 }

 public java.util.Vector getMenues()
 {
   return (java.util.Vector)MenuesTxn;
 }

 public java.util.Vector getMenuesSec()
 {
   return (java.util.Vector)MenuesTxnSec;
 }

 public java.util.Vector getMonedas()
 {
   return (java.util.Vector)Monedas;
 }

 public java.util.Vector convertRStoVector(java.sql.ResultSet newResultSet){
  java.util.Vector newVector = new java.util.Vector();
  java.sql.ResultSetMetaData rsmd = null;

  try{
   rsmd = newResultSet.getMetaData();
   int numberOfColumns = rsmd.getColumnCount();
   while(newResultSet.next()){
    java.util.Vector newdeepVector = new java.util.Vector();
    for(int i = 1; i <= numberOfColumns; i++)
     newdeepVector.add(newResultSet.getString(i));
    newVector.add(newdeepVector);
   }
  }
  catch(java.sql.SQLException sqlException){
   System.out.println("Error en ExtractMenu::convertRStoVector [" + sqlException.toString() + "]");
  }
  finally{
    try{
      if(rsmd != null)
      	rsmd = null;
      if(newResultSet != null){
      	newResultSet.close();
      	newResultSet =  null;
      }
    }
    catch(java.sql.SQLException sqlException){
      System.out.println("Error FINALLY ExtractMenu::convertRStoVector [" + sqlException.toString() + "]");
    }
  }
  return newVector;
 }
}
