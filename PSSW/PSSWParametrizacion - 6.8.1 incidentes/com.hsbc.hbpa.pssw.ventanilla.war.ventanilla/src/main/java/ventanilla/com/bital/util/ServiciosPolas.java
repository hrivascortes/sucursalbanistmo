//*************************************************************************************************
//             Funcion: Bean que contiene los mensajes de entrada y salida de la comunicacion con el web Service de POLAS
//            Elemento: MsgPolas.java
//*************************************************************************************************
package ventanilla.com.bital.util;

import org.apache.log4j.BasicConfigurator;
import com.bital.util.ServiceLocator;
import ventanilla.com.bital.beans.MsgPolas;
import com.hbmx.communicationcore.exception.ServiceUnavailableException;
import com.hbmx.cppc.serviceimplementation.OnlinePaymentsService;
import com.hbmx.cppcore.model.Payment;
import com.hbmx.cppcore.model.communication.NotificationMessage;
import com.hbmx.cppcore.model.communication.QueryMessage;
import com.hbmx.cppcore.model.communication.responses.NotificationResponseMessage;
import com.hbmx.cppcore.model.communication.responses.QueryResponseMessage;
import com.hbmx.cppcore.model.communication.CancellationMessage;
import com.hbmx.cppcore.model.communication.responses.CancellationResponseMessage;
import javax.servlet.http.HttpSession;
import java.util.Hashtable;

public class ServiciosPolas {
	//final static String url="http://164.2.43.224:9080";
	//final static String url="https://mxf5drpw01ch.mx.cmtH";
	private static String url = ServiceLocator.getInstance().getResourceValue("POLAS_URL"); 
	
    //Notificacion Reverso
  
public void enviarReverso(Payment pago,MsgPolas mp){
	

	BasicConfigurator.configure();
    OnlinePaymentsService cancellationService;
	try {
		cancellationService = new OnlinePaymentsService(url);
        CancellationResponseMessage cancellationResponseMessage = cancellationService
                .cancell(crearCancelacion(pago));
       
               
        if (cancellationResponseMessage.isSuccessful()) {
        	mp.setCodigo("0");
        	
        	
        	if(cancellationResponseMessage.getMessage()!=null){
        		if(cancellationResponseMessage.getMessage()!=""){
        			mp.setMensaje("NOTIFICACION REVERSO OK, MSG DE CLIENTE: " + cancellationResponseMessage.getMessage());
        		}
        	}
        	else{
        		mp.setMensaje("NOTIFICACION REVERSO OK");
        	}
        	
        	
        } else {
           
        	mp.setCodigo("2");
    		mp.setMensaje("ERROR EN LA NOTIFICACION DE REVERSO"); 
        }
    } catch (ServiceUnavailableException e) {
    	mp.setCodigo("3");
		mp.setMensaje("PROBLEMA CON EL WEB SERVICE: "+e.getMessage());
		System.out.println("*** Error en la notificacion reverso E=***"+e.getMessage());
     } catch (Exception e) {
    	 mp.setCodigo("3");
 		mp.setMensaje("PROBLEMA CON EL WEB SERVICE: "+e.getMessage());
 		System.out.println("*** Error en la notificacion reverso E=***"+e.getMessage());
    }
	
}


private CancellationMessage crearCancelacion(Payment pago) {
	CancellationMessage message = new CancellationMessage();
    message.setPayment(pago);
    return message;
}
    
    //Notificacion
      
public void enviarNotificacion(Payment pago,MsgPolas mp, HttpSession session ){
    
    	BasicConfigurator.configure();
        OnlinePaymentsService notificationService;
        
    	try {
            notificationService = new OnlinePaymentsService(url);
            NotificationResponseMessage notificationResponseMessage = notificationService
                    .notify(crearNotificacion(pago, session));
	        


            if (notificationResponseMessage.isSuccessful()) {
            	////Checa si hay folio
            	if(notificationResponseMessage.getExternalPaymentNumber() != null){
            		if(notificationResponseMessage.getExternalPaymentNumber()!="") {
	            		 mp.setCodigo("0");
	            		 mp.setFolio(notificationResponseMessage.getExternalPaymentNumber());
	            		 if(notificationResponseMessage.getMessage()!=null){
	            			 if(notificationResponseMessage.getMessage()!=""){
	            			 	mp.setMensaje(notificationResponseMessage.getMessage());  
	            			 }
	            		 }
	            		 else{
	            			 mp.setMensaje("OK SIN MENSAJE RESPUESTA");
	            		 }
	            	 }
            	}              
                else{
                	mp.setCodigo("1");
                   	 if(notificationResponseMessage.getMessage()!=null){
                   		 if(notificationResponseMessage.getMessage()!=""){
                   		 	mp.setMensaje(notificationResponseMessage.getMessage());
                   		 }
            		 }
                	 else{
                		mp.setMensaje("OK SIN FOLIO PARA IMPRESION, NI MENSAJE RESPUESTA");
                	 }
                }
         
             } else {
            		mp.setCodigo("2");
            		mp.setMensaje("ERROR EN LA NOTIFICACION");     	
            }
            
        } catch (ServiceUnavailableException e) {
        	
        	mp.setCodigo("3");
    		mp.setMensaje("PROBLEMA CON EL WEB SERVICE: "+e.getMessage());
    		System.out.println("*** Error en la notificacion E=***"+e.getMessage());

        } catch (Exception e) {
        	mp.setCodigo("3");
    		mp.setMensaje("PROBLEMA CON EL WEB SERVICE: "+e.getMessage());
             System.out.println("*** Error en la notificacion E=***"+e.getMessage());
           
        }

    }
    
    private NotificationMessage crearNotificacion(Payment pago, HttpSession session) {
    	Hashtable datasession = (Hashtable)session.getAttribute("page.datasession");
    	NotificationMessage message = new NotificationMessage();        
        message.setPayment(pago);   
        if(datasession.get("clientInformation") != null ){
        	message.setClientInformation((String)datasession.get("clientInformation").toString());
        	datasession.remove("clientInformation");
        	session.setAttribute("page.datasession", datasession);
        }
        return message;
    }
    
    //Consulta
   
	public void executeConsultaRef(Payment pago, MsgPolas mp, HttpSession session){
		Hashtable datasession = (Hashtable)session.getAttribute("page.datasession");
		OnlinePaymentsService paymentsService;
         try {
            paymentsService = new OnlinePaymentsService(url);            
            QueryMessage message = new QueryMessage();
            message.setPayment(pago);
            QueryResponseMessage responseMessage = paymentsService.query(message);
            switch (Integer.parseInt(responseMessage.getError())) {
            case 0:
                mp.setCodigo("0");
                mp.setMonto(responseMessage.getAmount().toString());
                mp.setEstatus("0");
                mp.setMensaje(responseMessage.getMessage());

                break;
            case 1:
       		mp.setCodigo("1");
       		mp.setMensaje(responseMessage.getMessage());
                mp.setMonto("0.00");
        	mp.setEstatus("");
            	
               break;
            case 2:
                mp.setCodigo("2");
                mp.setMensaje(responseMessage.getMessage());
                mp.setMonto("0.00");
                mp.setEstatus("");
                
                break;
            }
            if(responseMessage.getClientInformation() != null ){
        		datasession.put("clientInformation", responseMessage.getClientInformation());
        		session.setAttribute("page.datasession", datasession);
            }
            //System.out.println("responseMessage: " + responseMessage.getMessage());
        } catch (ServiceUnavailableException e) {
        	mp.setCodigo("3");
            mp.setMensaje("PROBLEMA CON EL WEB SERVICE: "+e.getMessage());
            mp.setEstatus("");
            System.out.println("Error: " + e.getMessage());
            e.printStackTrace();
            
        } catch (Exception e) {
        	mp.setCodigo("3");
        	mp.setEstatus("");
            mp.setMensaje("PROBLEMA CON EL WEB SERVICE: "+e.getMessage());        	
            System.out.println("Error: " + e.getMessage());
            e.printStackTrace();
        }		
	}
	

}
