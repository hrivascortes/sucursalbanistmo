//*************************************************************************************************
//      Funcion:	Bean Txn 0101 Solicitud de chequeras
//      Creado por: Guillermo Rodrigo Escand�n Soto
//*************************************************************************************************
// CCN - 4620057 - 11/01/2008 - Creaci�n de bean
//*************************************************************************************************

package ventanilla.com.bital.sfb;

import ventanilla.GenericClasses;

import com.bital.util.ParameterNotFoundException;
import com.bital.util.ParameterParser;

public class Txn_0101 extends Transaction {
	public Header doAction(ParameterParser param)
			throws ParameterNotFoundException {
		FormatoA formatoa = new FormatoA();
		GenericClasses gc = new GenericClasses();

		String moneda = gc.getDivisa(param.getString("moneda"));

		formatoa.setTxnCode("0101");
		formatoa.setFormat("A");
		String cuenta10 = param.getCString("txtDDACuenta");
		if (cuenta10.length() > 10) {
			cuenta10 = cuenta10.substring(cuenta10.length() - 10,
					cuenta10.length());
			formatoa.setAcctNo(cuenta10);
		} else {
			formatoa.setAcctNo(gc.rellenaZeros(
					param.getCString("txtDDACuenta"), 10));
		}
		formatoa.setTranAmt(param.getString("lstNoChqs"));
		formatoa.setTranCur(moneda);
		formatoa.setCheckNo(param.getString("lstTipoCheq"));
		formatoa.setMoAmoun("000");
		formatoa.setTranDesc((param.getString("lstSucursal").substring(1, param
				.getString("lstSucursal").length()))
				+ convertNullToValidField(param.getStringNULL("lstModeloCh"))
				+ convertNullToValidField(param.getStringNULL("lstColorCh")));

		return formatoa;
	}

	public String convertNullToValidField(String str) {

		return ((str == null) ? "00" : str);
	}
}