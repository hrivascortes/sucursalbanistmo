package ventanilla.com.bital.sfb;

public class BTotales extends Header
{

    private String txtTrNo = null;

    public BTotales()
    {
        txtTrNo = "";
    }

    public void setTrNo(String s)
    {
        txtTrNo = s;
    }

    public String getTrNo()
    {
        return txtTrNo + "*";
    }

    public String toString()
    {
        return super.toString() + "******" + getTrNo() + "****************";
    }
}
