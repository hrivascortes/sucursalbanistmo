//*************************************************************************************************
//			   Funcion: Bean Txn 0492
//			Creado por: Guillermo Rodrigo Escand�n Soto
//*************************************************************************************************
// CCN - 
//*************************************************************************************************
package ventanilla.com.bital.sfb;

import ventanilla.GenericClasses;

import com.bital.util.ParameterNotFoundException;
import com.bital.util.ParameterParser;

public class Txn_0492 extends Transaction
{
	public Header doAction(ParameterParser param)
		throws ParameterNotFoundException
	{
		FormatoB formatob = new FormatoB();
		GenericClasses gc = new GenericClasses();
		
		String refer1 = "";
		String sucursal = param.getCString("sucursal");
		formatob.setTxnCode("0492");
		formatob.setFormat("B");
		formatob.setAcctNo("0492");
		formatob.setTranAmt(param.getCString("txtMonto"));
		refer1 = param.getString("txtRefer10630");
		formatob.setReferenc(sucursal + setzeros(refer1));
		formatob.setCashOut(param.getCString("txtMonto"));
		String moneda = gc.getDivisa(param.getString("moneda"));
		formatob.setFromCurr(moneda);

		return formatob;
	}
	
	private String setzeros(String campo)
	{
	  while(campo.length() < 10)
		  campo = "0" + campo;
	  return campo;
	}
}