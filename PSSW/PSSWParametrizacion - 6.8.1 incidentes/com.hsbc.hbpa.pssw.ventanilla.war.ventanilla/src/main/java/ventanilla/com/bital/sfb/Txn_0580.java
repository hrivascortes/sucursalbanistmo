//*************************************************************************************************
//             Funcion: Bean Txn 0580
//          Creado por: Liliana Neyra Salazar
//*************************************************************************************************
// CCN - 4360602 - 18/05/2007 - Se realizan modificaciones para proyecto Codigo de Barras SUAS
//*************************************************************************************************
package ventanilla.com.bital.sfb;

import com.bital.util.ParameterParser;
import com.bital.util.ParameterNotFoundException;
import ventanilla.GenericClasses;
import ventanilla.com.bital.util.VolanteoDDA;

public class Txn_0580 extends Transaction
{
	public Header doAction(ParameterParser param)
		throws ParameterNotFoundException
	{
		FormatoB formatob = new FormatoB();
		GenericClasses gc = new GenericClasses();
		String Monto = "";
		VolanteoDDA oVolanteoDDA = null;
		String txtVolante = "NO";
		
		formatob.setTxnCode("0580");
		formatob.setFormat("B");
		formatob.setAcctNo("0580" + param.getString("txtCodSeg") + param.getString("txtCveTran") + param.getString("txtDDACuenta2").toString().substring(0,4));
		Monto = gc.quitap(param.getString("txtimporte"));
		formatob.setTranAmt(Monto);
		String rdoACorte = param.getString("rdoACorte");
		if (rdoACorte.equals("01"))
		   {rdoACorte = "T1";}
		else
			{rdoACorte = "T2";}
		formatob.setReferenc(param.getString("txtDDACuenta2").toString().substring(4) + param.getString("txtSerial2") + rdoACorte);
		formatob.setIntern(param.getString("txtnumctrl").toString().substring(0,10));
		formatob.setIntWh(param.getString("txtnumctrl").toString().substring(10,12));
		formatob.setCashIn("000");
		String moneda = gc.getDivisa(param.getString("moneda"));
		formatob.setFromCurr(moneda);
		long TruncamientoMN =Long.parseLong(param.getString("trunc0580")); 
		oVolanteoDDA = new VolanteoDDA();
		String txtBanco = param.getString("txtCveTran");
		txtBanco = txtBanco.substring(5,8);
		oVolanteoDDA.setTeller(param.getString("teller").toString().substring(4,6));
		oVolanteoDDA.setBranch(param.getString("sucursal").toString().substring(1));
		oVolanteoDDA.setBank(txtBanco);
		oVolanteoDDA.setCurrency(param.getString("moneda"));
		oVolanteoDDA.setCorte(param.getString("rdoACorte"));
		oVolanteoDDA.setChequeData(param.getString("txtSerial2") +param.getString("txtDDACuenta2") + param.getString("txtCveTran") +
		param.getString("txtCodSeg"));
		oVolanteoDDA.setChequeAmount(Monto);
		if ( Long.parseLong(Monto) >= TruncamientoMN ) {              // Monto Mayor al de Truncamiento
			txtVolante = "SI";
			formatob.setFeeAmoun(oVolanteoDDA.getConsecutive());
			}
   
		return formatob;
	}
}
