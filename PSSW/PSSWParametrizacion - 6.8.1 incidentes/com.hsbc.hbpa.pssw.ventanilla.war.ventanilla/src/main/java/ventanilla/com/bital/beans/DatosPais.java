//*************************************************************************************************
//	 	Funcion: Clase que realiza la consulta de los datos del Pais y las conserva en memoria
//		Nombre: DatosPais.java			
//		Creado por: Humberto Enrique Balleza Mej�a
//	    Modificado por: 
//*************************************************************************************************
//CCN -XXXXXXX- 13/02/2007 - Se crea la clase para realizar select y mantener los datos en memoria
//*************************************************************************************************

package ventanilla.com.bital.beans;

import ventanilla.com.bital.util.DBQuery;

import java.util.Vector;
import java.util.Hashtable;

public class DatosPais {
	
	private Hashtable pais = null;
	private static DatosPais instance = null;
    
	private DatosPais() 
	{   
		pais = new Hashtable();
		execute();
	}

	public static synchronized DatosPais getInstance()
	{
		if(instance == null)
			instance = new DatosPais();
		return instance;
	}
	
	public String getInfoPais(String key)
	{     
		String mon = null;
		if(pais.containsKey(key))
		{
			return (String)this.pais.get(key);
		}else
			return "No existe el registro";
	}
	
	public void execute() 
	   {
		   String sql = null;
		   sql = "SELECT D_DATOS FROM ";
		   String clause = null;
		   clause = " WHERE C_CLAVE='PAIS'";
        
		   DBQuery dbq = new DBQuery(sql,"TC_DATOS_VARIOS", clause);
		   Vector Regs = dbq.getRows();
        
		   int size = Regs.size();
		   Vector row = new Vector();
		   String datos = null;

		   for(int i=0; i<size; i++)
		   {
			row = (Vector)Regs.get(i);
			datos = row.get(0).toString().trim();
			java.util.StringTokenizer renglon = new java.util.StringTokenizer(datos,"*");
			pais.put("cia",renglon.nextToken());
			pais.put("pais",renglon.nextToken());
			pais.put("leyenda",renglon.nextToken());
			pais.put("siglas",renglon.nextToken());
			pais.put("currency",renglon.nextToken());
			pais.put("web",renglon.nextToken());
		   }
	   }
}
