//*************************************************************************************************
//             Funcion: Bean Txn 5148
//      Modificado por: Humberto Enrique Balleza Mej�a
//*************************************************************************************************
//      06/07/2007 - Se crea el bean de la transacci�n 5148
//*************************************************************************************************
package ventanilla.com.bital.sfb; 

import com.bital.util.ParameterParser;
import com.bital.util.ParameterNotFoundException;
import ventanilla.GenericClasses;

public class Txn_5148 extends Transaction
{
  public Header doAction(ParameterParser param)
	throws ParameterNotFoundException
  {
	// Instanciar el bean a utilizar
	Deposito deposito = new Deposito();
    
	// Ingresar datos requeridos    
	deposito.setAcctNo ( param.getString("txtDDACta5148") );
	deposito.setCashIn ( "000" );
	GenericClasses gc = new GenericClasses();
    //deposito.setTranAmt("1");
	deposito.setTranAmt( param.getCString("txtMonto") );
       
	if (param.getString("override") != null)
	   if (param.getString("override").equals("SI"))
		   deposito.setOverride("3");

	int moneda = param.getInt("moneda");
	deposito.setTranCur( gc.getDivisa(param.getString("moneda")));

	if( param.getString("txtFechaEfect", null) != null )
	{
	  String fecha1 = param.getString("txtFechaEfect");
	  String fecha2 = param.getString("txtFechaSys");
	  fecha1 = fecha1.substring(6) + fecha1.substring(3, 5) + fecha1.substring(0, 2);

	  if( !fecha1.equals(fecha2) )
	  {
		fecha2 = fecha1.substring(4, 6) + fecha1.substring(6) + fecha1.substring(2, 4);
		deposito.setEffDate(fecha2);
	  }
	}

	return deposito;
  }               
}