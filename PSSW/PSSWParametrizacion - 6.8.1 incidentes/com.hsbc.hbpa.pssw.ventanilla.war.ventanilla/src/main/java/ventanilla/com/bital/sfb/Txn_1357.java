//*************************************************************************************************
//             Funcion: Bean Txn 1357
//      Modificado por: Alejandro Gonzalez Castro
//*************************************************************************************************
// CCN - 4360437 - 24/02/2006 - Se realizan modificaciones para transacciones SPEI
//*************************************************************************************************

package ventanilla.com.bital.sfb;

import com.bital.util.ParameterParser;
import com.bital.util.ParameterNotFoundException;
import ventanilla.GenericClasses;

public class Txn_1357 extends Transaction
{
	public Header doAction(ParameterParser param)
		throws ParameterNotFoundException
	{
		FormatoA formatoa = new FormatoA();
		GenericClasses gc = new GenericClasses();
		
		formatoa.setTxnCode("1357");
		formatoa.setFormat("A");
		formatoa.setAcctNo(param.getString("txtDDACuenta"));
		//System.out.println("txtComision: "+param.getCString("txtComision1357"));
		formatoa.setTranAmt(param.getCString("txtComision1357"));
		String moneda = gc.getDivisa(param.getString("moneda"));
		formatoa.setTranCur(moneda);
		formatoa.setCheckNo(param.getString("txtSerial4"));
		formatoa.setMoAmoun("000");
		
		return formatoa;
	}
}
