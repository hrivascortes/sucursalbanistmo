//*************************************************************************************************
//             Funcion: Clase que realiza la consulta del flujo de la txn y lo conserva en memoria
//          Creado por: Rutilo Zarco Reyes
//      Modificado por: Juvenal R. Fernandez V.
//*************************************************************************************************
// CCN -4360152- 21/06/2004 - Se crea la clase para realizar select y mantener los datos en memoria
// CCN - 4360334 - 17/06/2005 - Se elimina variable dbQualifier evitando enviar "." antes del nombre de la tabla
//*************************************************************************************************

package ventanilla.com.bital.beans;

import ventanilla.com.bital.util.DBQuery;

import java.util.Vector;
import java.util.Hashtable;

public class Flujotxn
{
    private Hashtable flujotxn = null;
    private static Flujotxn instance = null;
    
    private Flujotxn() 
    {   
        flujotxn = new Hashtable();
        execute();
    }
    
    public static synchronized Flujotxn getInstance()
    {
        if(instance == null)
            instance = new Flujotxn();
        return instance;
    }
    
    public java.util.Stack getFlujotxn(String txne, String txns, String moneda)
    {     
       for(int i = txns.length(); i < 4; i++)
         txns += " ";
        String dflujotxn = null;
        java.util.Vector vflujo = new java.util.Vector();
        java.util.Stack sflujo = new java.util.Stack();
        if(flujotxn.containsKey(txne + txns + moneda))
        {
            dflujotxn = (String)this.flujotxn.get(txne + txns + moneda);
            ventanilla.com.bital.util.NSTokenizer parser = new ventanilla.com.bital.util.NSTokenizer(dflujotxn, "*");
            while( parser.hasMoreTokens() ){
              String token = parser.nextToken();
              if( token == null )
               continue;
              vflujo.add(token);
            }
        }
        if(!vflujo.isEmpty())
        {
          for(int i = vflujo.size() - 1; i >= 0; i--)
            sflujo.push(vflujo.get(i));
        }
        else
         sflujo.push(txne);
        return sflujo;
    }
    
    public void execute() 
    {
        String sql = null;
        sql = "SELECT C_TXN_ENTRADA,C_TXN_SALIDA,C_MONEDA,D_FLUJO_TXN FROM ";
        String clause = null;
        clause = " ";
        
        DBQuery dbq = new DBQuery(sql,"TC_FLUJO_TXNS", clause);
        Vector Regs = dbq.getRows();
        
        int size = Regs.size();
        Vector row = new Vector();
        String txne = null;
        String txns = null;
        String moneda = null;
        String flujo = null;

        for(int i=0; i<size; i++)
        {
            row = (Vector)Regs.get(i);
            txne = row.get(0).toString();
            txns = row.get(1).toString();
            moneda = row.get(2).toString();
            flujo = row.get(3).toString();

            flujotxn.put(txne+txns+moneda, flujo);
        }
    }
}
