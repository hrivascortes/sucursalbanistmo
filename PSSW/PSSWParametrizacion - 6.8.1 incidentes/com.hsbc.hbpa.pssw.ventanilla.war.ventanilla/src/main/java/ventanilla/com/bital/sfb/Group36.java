// Transaccion 4145 - Cheques de Caja
package ventanilla.com.bital.sfb;

import com.bital.util.ParameterParser;
import com.bital.util.ParameterNotFoundException;
import ventanilla.GenericClasses;

public class Group36 extends Transaction
{
  public Header doAction(ParameterParser param)
    throws ParameterNotFoundException
  {
    // Instanciar el bean a utilizar
    Cheques cheque = new Cheques();
    
    // Ingresar datos requeridos    
    cheque.setFormat("B");
	GenericClasses gc = new GenericClasses();
    int moneda = param.getInt("moneda");
	cheque.setFromCurr( gc.getDivisa(param.getString("moneda")));
    if( moneda == 1 )
    {
      cheque.setAcctNo  ("0103000023");
    }
    else if( moneda == 2 )
    {
      cheque.setAcctNo  ("3902222222");
    }

    cheque.setTranAmt ( param.getCString("txtMonto") );
    cheque.setReferenc( param.getString("txtSerial") );
    
    if( param.getString("txtFechaEfect", null) != null )
    {
      String fecha1 = param.getString("txtFechaEfect");
      String fecha2 = param.getString("txtFechaSys");
      fecha1 = fecha1.substring(6) + fecha1.substring(3, 5) + fecha1.substring(0, 2);

      if( !fecha1.equals(fecha2) )
      {
        fecha2 = fecha1.substring(4, 6) + fecha1.substring(6) + fecha1.substring(2, 4);
        cheque.setEffDate(fecha2);
      }
    }
    return cheque;
  }               
}
