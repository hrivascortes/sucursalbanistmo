//*************************************************************************************************
//             Funcion: Bean Txn 1295
//      Modificado por: Fredy Pe�a Moreno
//*************************************************************************************************
// CCN - 4360607 - 18/05/2007 - Bean para TXNS Genericas  Comision Generica
//*************************************************************************************************

package ventanilla.com.bital.sfb;

import com.bital.util.ParameterParser;
import com.bital.util.ParameterNotFoundException;
import ventanilla.GenericClasses;

public class Txn_1295 extends Transaction{
	public Header doAction(ParameterParser param)
		throws ParameterNotFoundException
	{
		FormatoA formatoa = new FormatoA();
		GenericClasses gc = new GenericClasses();
		formatoa.setTxnCode("1295");
		formatoa.setFormat("A");		
		formatoa.setAcctNo(param.getString("txtDDACuenta"));		
		formatoa.setTranAmt(param.getCString("txtMonto"));		
		formatoa.setTranDesc(param.getString("lstDGeneric1295"));					
		String moneda = gc.getDivisa(param.getString("moneda"));
		formatoa.setTranCur(moneda);		
	
		return formatoa;
	}
}