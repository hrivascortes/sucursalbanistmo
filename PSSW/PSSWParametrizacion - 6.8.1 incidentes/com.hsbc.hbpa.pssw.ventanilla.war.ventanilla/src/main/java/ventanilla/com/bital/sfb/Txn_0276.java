//*************************************************************************************************
//             Funcion: Bean Txn 4209 Impresion de Cheque de Gerencia.
//             Elaborado por: YGX
//*************************************************************************************************

package ventanilla.com.bital.sfb;


import ventanilla.GenericClasses;
import com.bital.util.ParameterNotFoundException;
import com.bital.util.ParameterParser;


public class Txn_0276 extends Transaction
{		 
	public Header doAction(ParameterParser param)
		throws ParameterNotFoundException
	{				
        GenericClasses gc = new GenericClasses();   
        FormatoH formatoh = new FormatoH();         				
		ventanilla.com.bital.sfb.Cheques Cheques = new ventanilla.com.bital.sfb.Cheques();
		
		formatoh.setTxnId("ODPA");
		formatoh.setTxnCode("0276");
	    formatoh.setFormat("H");   
	    
	    //Cuenta de Cheques de Gerencia
	    formatoh.setAcctNo("0");
	    //Cuenta de Cuenta informativa se env�a la cta del cargo
	    formatoh.setAcctDeb("85490000000000027777");	    
	    formatoh.setTranDesc(param.getString("txtBenefiCheq").trim());	    
	    formatoh.setTranAmt(gc.delCommaPointFromString(param.getString("txtMonto")).trim());
	    
	    int moneda = param.getInt("moneda"); 
	    formatoh.setTranCur(gc.getDivisa(param.getString("moneda")));
	    
	    formatoh.setCheckNo(param.getCString("txtSerialCheq"));	    	    
	    formatoh.setCashIn("000");	    
	    formatoh.setDescRev("REV CTA CONT DEPTO");	    	   	

	   //	System.out.println(formatoh);
	   	
		return formatoh;
	}
	

}
