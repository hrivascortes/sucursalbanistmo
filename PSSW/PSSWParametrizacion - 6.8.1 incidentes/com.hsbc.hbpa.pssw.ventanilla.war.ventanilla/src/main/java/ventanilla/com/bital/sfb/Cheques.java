package ventanilla.com.bital.sfb;

public class Cheques extends Header
{
  private String txtDescRev  = null;
  private String txtEffDate  = null;
  private String txtAcctNo   = null;
  private String txtTranAmt  = null;
  private String txtTranCur  = null;
  private String txtCheckNo  = null;
  private String txtTrNo     = null;
  private String txtCashIn   = null;
  private String txtCashOut  = null;
  private String txtMoAmount = null;
  private String txtFees     = null;
  private String txtTranDesc = null;
  private String txtHolDays  = null;
  private String txtAcctNo1  = null;
  private String txtTrNo1    = null;
  private String txtAcctNo2  = null;
  private String txtTrNo2    = null;
  private String txtHolDays2 = null;
  private String txtAmount3  = null;
  private String txtCheckNo3 = null;
  private String txtTrNo3    = null;
  private String txtAmount4  = null;
  private String txtCheckNo4 = null;
  private String txtAmount5  = null;
  private String txtCheckNo5 = null;

  //-----------------------------------
  //  Campos del formato B
  //-----------------------------------
  private String txtReferenc = null;
  private String txtFeeAmoun = null;
  private String txtIntern   = null;
  private String txtIntWh    = null;
  private String txtFromCurr = null;

  private String emptyValue(String value)
  {
    if( value == null )
      return "*";
    else
      return value + "*";
  }
  
  public String getAcctNo()
  {
	return emptyValue(txtAcctNo);
  }    

  public String getAmount3()
  {
	return emptyValue(txtAmount3);
  }    
  
  public String getAmount4()
  {
	return emptyValue(txtAmount4);
  }    

  public String getAmount5()
  {
	return emptyValue(txtAmount5);
  }    

  public String getCashIn()
  {
	return emptyValue(txtCashIn);
  }
  
  public String getCashOut()
  {
    return emptyValue(txtCashOut);
  }    

  public String getCheckNo()
  {
	return emptyValue(txtCheckNo);
  }    

  public String getCheckNo3()
  {
	return emptyValue(txtCheckNo3);
  }    

  public String getCheckNo4()
  {
	return emptyValue(txtCheckNo4);
  }    

  public String getCheckNo5()
  {
	return emptyValue(txtCheckNo5);
  }    

  public String getEffDate()
  {
	return emptyValue(txtEffDate);
  }

  public String getMoAmount()
  {
    return emptyValue(txtMoAmount);
  }

  public String getFees()
  {
	return emptyValue(txtFees);
  }    

  public String getHolDays()
  {
	return emptyValue(txtHolDays);
  }
  
  public String getAcctNo1()
  {
    return emptyValue(txtAcctNo1);
  }    

  public String getTrNo1()
  {
    return emptyValue(txtTrNo1);
  }    
  
  public String getAcctNo2()
  {
    return emptyValue(txtAcctNo2);
  }    
  
  public String getTrNo2()
  {
    return emptyValue(txtTrNo2);
  }    
  
  public String getHolDays2()
  {
	return emptyValue(txtHolDays2);
  }    
  
  public String getTranAmt()
  {
	return emptyValue(txtTranAmt);
  }    

  public String getTranCur()
  {
	return emptyValue(txtTranCur);
  }    
  
  public String getTranDesc()
  {
	return emptyValue(txtTranDesc);
  }    

  public String getTrNo()
  {
	return emptyValue(txtTrNo);
  }    

  public String getTrNo3()
  {
	return emptyValue(txtTrNo3);
  }

  public String getReferenc()
  {
    return emptyValue(txtReferenc);
  }
  
  public String getFeeAmoun()
  {
    return emptyValue(txtFeeAmoun);
  }
  
  public String getIntern()
  {
    return emptyValue(txtIntern);
  }
  
  public String getIntWh()
  {
    return emptyValue(txtIntWh);
  }

  public String getFromCurr()
  {
    return emptyValue(txtFromCurr);
  }

  public void setAcctNo(String newAcctNo)
  {
	txtAcctNo = newAcctNo;
  }  

  public void setAmount3(String newAmount3)
  {
	txtAmount3 = newAmount3;
  }  

  public void setAmount4(String newAmount4)
  {
	txtAmount4 = newAmount4;
  }  

  public void setAmount5(String newAmount5)
  {
	txtAmount5 = newAmount5;
  }  

  public void setCashIn(String newCashIn)
  {
	txtCashIn = newCashIn;
  }
  
  public void setCashOut(String newCashOut)
  {
    txtCashOut = newCashOut;
  }  

  public void setCheckNo(String newCheckNo)
  {
	txtCheckNo = newCheckNo;
  }  

  public void setCheckNo3(String newCheckNo3)
  {
	txtCheckNo3 = newCheckNo3;
  }  

  public void setCheckNo4(String newCheckNo4)
  {
	txtCheckNo4 = newCheckNo4;
  }  

  public void setCheckNo5(String newCheckNo5)
  {
	txtCheckNo5 = newCheckNo5;
  }  

  public void setEffDate(String newEffDate)
  {
	txtEffDate = newEffDate;
  }

  public void setMoAmount(String newMoAmount)
  {
    txtMoAmount = newMoAmount;
  }

  public void setFees(String newFees)
  {
	txtFees = newFees;
  }  

  public void setHolDays(String newHolDays)
  {
	txtHolDays = newHolDays;
  }

  public void setAcctNo1(String newAcctNo1)
  {
    txtAcctNo1 = newAcctNo1;
  }  

  public void setTrNo1(String newTrNo1)
  {
    txtTrNo1 = newTrNo1;
  }    
  
  public void setAcctNo2(String newAcctNo2)
  {
    txtAcctNo2 = newAcctNo2;
  }  
  
  public void setTrNo2(String newTrNo2)
  {
    txtTrNo2 = newTrNo2;
  }  
  
  public void setHolDays2(String newHolDays2)
  {
	txtHolDays2 = newHolDays2;
  }  

  public void setTranAmt(String newTranAmt)
  {
	txtTranAmt = newTranAmt;
  }  

  public void setTranCur(String newTranCur)
  {
	txtTranCur = newTranCur;
  }  

  public void setTranDesc(String newTranDesc)
  {
	txtTranDesc = newTranDesc;
  }  

  public void setTrNo(String newTrNo)
  {
	txtTrNo = newTrNo;
  }  

  public void setTrNo3(String newTrNo3)
  {
	txtTrNo3 = newTrNo3;
  }

  public void setReferenc(String newReferenc)
  {
    txtReferenc = newReferenc;
  }
  
  public void setFeeAmoun(String newFeeAmoun)
  {
    txtFeeAmoun = newFeeAmoun;
  }
  
  public void setIntern(String newIntern)
  {
    txtIntern = newIntern;
  }
  
  public void setIntWh(String newIntWh)
  {
    txtIntWh = newIntWh;
  }

  public void setFromCurr(String newFromCurr)
  {
    txtFromCurr = newFromCurr;
  }

  public String getDescRev()
  {
	return txtDescRev;
  }    
  
  public void setDescRev(String newDescRev)
  {
	txtDescRev = newDescRev;
  }

  public String toString()
  {
    if( getFormat().equals("B") )
    {
      return super.toString() + getEffDate() + getAcctNo() + getTranAmt()
        + getReferenc() + getFeeAmoun() + getIntern() + getIntWh() + getCashIn()
        + "*" + getFromCurr() + "*";
    }
    else
      return super.toString() + getEffDate() + getAcctNo() + "*"
        + getTranAmt() + getTranCur() + getCheckNo() + getTrNo()
        + getCashIn() + getCashOut() + "*" + getMoAmount() + getFees() + getTranDesc()
        + getHolDays() + "****" + getAcctNo1() + "*" + getTrNo1()
        + "****" + getAcctNo2() + "*" + getTrNo2()
        + getHolDays2() + "*" + getAmount3() + "**" + getCheckNo3()
        + getTrNo3() + "**" + getAmount4() + "**" + getCheckNo4()
        + "***" + getAmount5() + "**" + getCheckNo5() + "**";
  }    
}
