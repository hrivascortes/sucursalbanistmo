package ventanilla.com.bital.sfb;

import java.util.Hashtable;

import com.bital.util.ParameterParser;
import com.bital.util.ParameterNotFoundException;

public abstract class Transaction
{
  protected Header proceso = null;

  public Transaction()
  {
    super();
  }

  public Header getProcess()
  {
    return proceso;
  }

  public String toString()
  {
    return proceso.getMessage();
  }

  public void setHeader(ParameterParser param)
      throws ParameterNotFoundException
  {
    if( proceso.getTxnCode() == null )
      proceso.setTxnCode(param.getString("cTxn"));
    proceso.setBranch(param.getString("sucursal"));
    proceso.setTeller(param.getString("teller"));

    if( param.getString("supervisor") != null )
      proceso.setSupervisor(param.getString("supervisor"));

    String needOvrr = "NO";
    if (param.getStringNULL("override") != null)
        needOvrr = param.getString("override");

    if (needOvrr.startsWith("S"))
        proceso.setOverride("3");
  }

  public void perform(Hashtable request) throws ParameterNotFoundException
  {
    if( request == null )
      throw new ParameterNotFoundException("Parametros no disponibles");

    // Obtener lista de parametros
    ParameterParser param = new ParameterParser(request);


    // Ejecutar proceso
    proceso = doAction(param);
    // Llenar datos del encabezado


    setHeader(param);
  }

  public void execute()
  {
    // Ejecutar el requerimiento
    proceso.execute();
  }

  public abstract Header doAction(ParameterParser param)
      throws ParameterNotFoundException;
}
