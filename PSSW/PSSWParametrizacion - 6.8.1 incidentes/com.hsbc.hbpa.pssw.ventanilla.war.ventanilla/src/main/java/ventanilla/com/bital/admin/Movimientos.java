package ventanilla.com.bital.admin;

import java.io.StringReader;
import java.util.List;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;

/**
 * @author YGX
 * Lee una cadena XML y la imprime, si fuera de un archivo lo genera..
 */
public class Movimientos {
	
    private String fechOper = "";
	private String numCheque = "";
	private String tranId= "";
	private String montoTran= "";
	private String tipoTran = "";
	private String saldo = "";
	private String rastrId   = "";
	private String descripcion = "";
	
	private List movimiento;
	
	private String codigo = "";
	private boolean cta_found = false;
	private String descrip = "";
	
	
	public List getMovimiento() {
	return movimiento;   }
	
	public void setMovimiento(List movimiento) {
	this.movimiento = movimiento;	}
	
	public void addMovimientos(Movimientos object) {
    this.movimiento.add(object);    }
		
	public String getFechOper() {
    return fechOper;	}
     	
	public String getNumCheque() {
		return numCheque ;	}
	
	public String getTranId() {
		return tranId;	}
    
	public String getMontoTran() {
		return montoTran;	}
	
	public String getTipoTran() {
		return tipoTran;	}
    
	public String getSaldo() {
		return saldo;	}
	
	public String getRastrId() {
		return rastrId;	}
	
	public String getDescripcion() {
		return descripcion;	}
 
	public String getCodigoerror() {
		return codigo;	}
		
	public String getDescriperror() {
		return descrip;	}

	/**
	 * @param string Cadena que contiene los datos de la cuenta en formato xml
	 */
	public void leePerfil(String string) {
	 try {
			/*System.out.println("Movimientos:");
			System.out.println(string);*/
			SAXBuilder builder = new SAXBuilder(false);

			//usar el parser Xerces y no queremos que valide el documento
			Document doc = builder.build(new StringReader(string));

			//construyo el arbol en memoria desde el fichero que se lo pasar� por parametro.			
			Element raiz = doc.getRootElement(); //tomo el elemento raiz

		   if (raiz.getName().equals("movimientos")) 	{
				movimiento = raiz.getChildren("movimiento");				
			} else if (raiz.getName().equals("error")) {
				codigo = raiz.getChildText("codigo");
				descrip = raiz.getChildText("descrip");
			}
		}catch (JDOMException e) {
			System.out.println(e.getMessage());
	  	}
	}
	
	
	
	
	 

		
}

