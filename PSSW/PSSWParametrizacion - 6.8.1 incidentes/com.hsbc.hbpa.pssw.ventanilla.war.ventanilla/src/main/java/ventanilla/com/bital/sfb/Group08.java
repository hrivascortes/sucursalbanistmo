// Transaccion 1051 & 5051
package ventanilla.com.bital.sfb;

import com.bital.util.ParameterNotFoundException;
import com.bital.util.ParameterParser;

public class Group08 extends Transaction
{
  public Header doAction(ParameterParser param)
    throws ParameterNotFoundException
  {
    // Instanciar el bean a utilizar
    Deposito deposito = new Deposito();

    // Ingresar datos requeridos
    deposito.setAcctNo  (param.getString("txtDDACuenta2"));
    deposito.setMoamoun("000");
    deposito.setTranAmt (param.getCString("txtMonto"));
    if (param.getString("override") != null)
       if (param.getString("override").equals("SI"))
           deposito.setOverride("3");

    int moneda = param.getInt("moneda");
    if( moneda == 1 )
      deposito.setTranCur("N$");
    else if( moneda == 2 )
      deposito.setTranCur("US$");
    else
      deposito.setTranCur("UDI");

    deposito.setCheckNo(param.getString("txtSerial2"));
    deposito.setFees   ("8");
    //System.out.println("Group01::txtCveTran:: " + param.getString("txtCveTran").toString());
    if (param.getString("txtCveTran").equals("1481")) {
    	deposito.setTrNo1(param.getString("txtCveTran"));
    } else {
        deposito.setTrNo1(param.getString("txtCveTran").substring(2,5));
    }
    //deposito.setTrNo2  (param.getString("txtCodSeg").substring(0,3));
    String txtCodSeg = "0000";
	deposito.setTrNo2  (txtCodSeg.substring(0,3));
    return deposito;
  }
}
