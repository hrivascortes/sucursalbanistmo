//*************************************************************************************************
//			 Funcion: Clase que realiza txn 0069
//			Elemento: Group03.java
//		  Creado por: Alejandro Gonzalez Castro
//	  Modificado por: Juvenal Fernandez
//*************************************************************************************************
//CCN - 4360268 - 04/02/2005 - Se realizan adecuaciones para eliminar Agencia NY
//*************************************************************************************************

package ventanilla.com.bital.sfb;

import com.bital.util.ParameterParser;
import com.bital.util.ParameterNotFoundException;
import ventanilla.GenericClasses;

public class Group03 extends Transaction {
    public Header doAction(ParameterParser param)
    throws ParameterNotFoundException {
        Deposito deposito = new Deposito();
		GenericClasses gc = new GenericClasses();
        
        if(param.getString("iTxn").equals("0069")) 
        {
            deposito.setAcctNo("4004019386");
            deposito.setCheckNo("000" + param.getString("txtSerial2"));
            deposito.setAcctNo2("AFORE"+param.getString("txtAfiliacion")+"0000");
        }
        else 
        {
            deposito.setAcctNo  ( param.getString("txtDDACuenta") );
        }
        deposito.setCashIn  ("000");
        deposito.setMoamoun("000");
        deposito.setTranAmt( param.getCString("txtMontoCheque") );
        
        int moneda = param.getInt("moneda");
		deposito.setTranCur( gc.getDivisa(param.getString("moneda")));
        
        deposito.setTrNo( param.getString("rdoACorte").substring(1) );
        
        if(param.getString("txtCodSeg", null) != null )
        {
            deposito.setCheckNo("000" + param.getString("txtSerial2"));
            String descrip = "CHEQUE" + param.getString("txtCodSeg") + param.getString("txtCveTran")
                + param.getString("txtDDACuenta2") + param.getString("txtSerial2") + "T" + param.getInt("rdoACorte");
            deposito.setTranDesc(descrip);
            // Descripcion para Reverso
            String DescRev = param.getString("txtCodSeg") + param.getString("txtCveTran");
            DescRev = DescRev + param.getString("txtDDACuenta2") + param.getString("txtSerial2") + "T" + param.getInt("rdoACorte");
            DescRev = "REV.  " + DescRev;           
            if(DescRev.length() > 40)
                DescRev = DescRev.substring(0,40);
            deposito.setDescRev(DescRev);
        }
        
        if( param.getInt("rdoACorte") == 1 ) 
        {
            deposito.setHoldays("2");
            deposito.setHolDays2("2");
        }
        else 
        {
            deposito.setHoldays("4");
            deposito.setHolDays2("4");
        }
        
        if ( param.getCString("txtVolante").equals("SI") )
            deposito.setAmount2(param.getString("Volante"));

        return deposito;
    }
}