//*************************************************************************************************
//             Funcion: Clase para las txns 1001 y 1003
//          Creado por: Alejandro Gonzalez Castro
//      Modificado por: Juvenal Fernandez
//*************************************************************************************************
// CCN -4360178 - 20/08/2004 - se modifica envio de no. de cuenta del cheque
// CCN - 4360268 - 04/02/2005 - Se realizan adecuaciones para eliminar Agencia NY
//*************************************************************************************************

package ventanilla.com.bital.sfb;

import com.bital.util.ParameterParser;
import com.bital.util.ParameterNotFoundException;
import ventanilla.GenericClasses;

public class Group01 extends Transaction {
    public Header doAction(ParameterParser param)
    throws ParameterNotFoundException {
       
        Deposito deposito = new Deposito();
        deposito.setAcctNo(param.getString("txtDDACuenta"));
        deposito.setCashIn(param.getCString("txtEfectivo"));
        deposito.setMoamoun("000");
		GenericClasses gc = new GenericClasses();
		ventanilla.com.bital.beans.DatosVarios insIdentidad = ventanilla.com.bital.beans.DatosVarios.getInstance();                   
		

        
        if (param.getString("override") != null)
            if (param.getString("override").equals("SI"))
                deposito.setOverride("3");
        
        long monto = Long.parseLong( param.getCString("txtEfectivo"));
        monto += Long.parseLong( param.getCString("txtCheque"));
        deposito.setTranAmt( Long.toString(monto) );
        
        int moneda = param.getInt("moneda");
		deposito.setTranCur( gc.getDivisa(param.getString("moneda")));
        
        if( param.getString("cTxn").equals("1003") && !param.getCString("txtCheque").equals("000") ) {
            deposito.setAcctNo2((param.getString("txtDDACuenta2")).substring(1,11));            
            deposito.setCheckNo2("000"+param.getString("txtSerial2"));
        }
        
            String descrip = "";
            String DescRev = "";
            
            if( param.getString("cTxn").equals("1001") ) 
            {
                descrip = "DEPOSITO POR CUENTA NUEVA";
                
                if( param.getCString("txtCheque").equals("000") )
                    deposito.setDesc2("PAGO EN EFECTIVO");
                else if( param.getCString("txtEfectivo").equals("000") )
                    deposito.setDesc2("PAGO CON CHEQUE "+(String)insIdentidad.getDatosv("IDENTIDAD"));
                else
                    deposito.setDesc2("PAGO MIXTO");
            } 
            else 
            {
                if( param.getCString("txtCheque").equals("000") )
                    descrip ="DEPOSITO EN EFECTIVO";
                else if( param.getCString("txtEfectivo").equals("000") )
                    descrip ="DEPOSITO CHEQUE "+(String)insIdentidad.getDatosv("IDENTIDAD");
                else
                    descrip ="DEPOSITO EN EFECTIVO Y CHEQUE "+(String)insIdentidad.getDatosv("IDENTIDAD");
            }
            deposito.setTranDesc(descrip);            
            deposito.setHoldays("0");
            deposito.setHolDays2("0");
            
            // Descripcion para Reverso
             DescRev = "REV." + descrip;
             if(DescRev.length() > 40)
                DescRev = DescRev.substring(0,40);
             deposito.setDescRev(DescRev);
        return deposito;
    }
}