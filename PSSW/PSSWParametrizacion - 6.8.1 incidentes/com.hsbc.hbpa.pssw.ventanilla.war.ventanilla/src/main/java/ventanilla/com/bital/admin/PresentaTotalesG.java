package ventanilla.com.bital.admin;

import java.sql.SQLException;

public class PresentaTotalesG {
  int intNoCargos = 0;
  int intNoAbonos = 0;
  String StringFormat = "TOTALES~";
  
  private String sql= "";
  private String txtimporte = "";
  
  private int NoCargos = getNoCargosAbonos("C");
  private int NoAbonos = getNoCargosAbonos("A");
  private	int documentos = 0;
  private	int importe = 0;
  
  
  //Arreglo para Cargos
  private String[] NoTotCargo   = new String[NoCargos];
  private String[] DescTotCargo = new String[NoCargos];
  private int[] DocsTotCargo = new int[NoCargos];
  private String[] ImporteTotCargo = new String[NoCargos];
  
  //Arreglo para Abonos
  private String[] NoTotAbono   = new String[NoAbonos];
  private String[] DescTotAbono = new String[NoAbonos];
  private int[] DocsTotAbono = new int[NoAbonos];
  private String[] ImporteTotAbono = new String[NoAbonos];
  
  //conection
  String statementPS = com.bital.util.ConnectionPoolingManager.getStatementPostFix();
  
  
  //*********************************************************************************
  // FORMATEA NUMEROS PARA PRESENTARLOS CON COMAS Y PUNTOS	***********************
  //*********************************************************************************
  private String comas(String nvacantidad, int nvaveces) {
    StringBuffer tmp1 = new StringBuffer();
    int posicionini = nvacantidad.length() - 2 ;
    int posicion1 = 0;
    int posicion2 = 0;
    while(posicionini > 3) {
      posicionini = posicionini - 3;
    }
    tmp1.append(nvacantidad.substring(0,posicionini)+ ",");
    nvaveces = nvaveces - 1;
    
    if (nvaveces >=1) {
      posicion1 = posicionini;
      posicion2 = posicionini + 3;
      while(nvaveces >= 1) {
        tmp1.append(nvacantidad.substring(posicion1,posicion2) + ",");
        nvaveces = nvaveces - 1;
        posicion1 = posicion2;
        posicion2 = posicion2 + 3;
      }
      tmp1.append(nvacantidad.substring(posicion1));
    }
    
    else {
      tmp1.append(nvacantidad.substring(posicionini));
    }
    nvacantidad = tmp1.toString();
    return nvacantidad;
  }// fin metodo comas
  
  
  private String puntodecimal(String nvacantidad) {
    int length = nvacantidad.length() ;
    StringBuffer tmp = new StringBuffer();
    if (length > 2) {
      tmp.append(nvacantidad.substring(0,length - 2));
      tmp.append("." + nvacantidad.substring(length - 2));
    }
    
    else {
      if(length == 2) {
        
        tmp.append("0." + nvacantidad);
      }
      else {
        tmp.append("0.0" + nvacantidad);
      }
    }
    
    nvacantidad = tmp.toString();
    return nvacantidad;
  }//fin metodo puntodecimal
  
  
  
  private String formatoNumero(String Numero) {
    
    if(!Numero.equals("0")) {
      int NCaracteres = Numero.length();
      int nveces = (NCaracteres - 3) / 3 ;
      if (nveces != 0) {
        Numero = comas(Numero,nveces);
        Numero = puntodecimal(Numero);
      }
      else {
        Numero = puntodecimal(Numero);
      }
    }
    else {
      Numero = "0.00";
    }
    return Numero;
    
  }//fin metodo formatoNumero
  
  
  //********************************************************
  //OBTIENE Numero DE CARGOS O ABONOS DISPONIBLES EN TOTALES
  //********************************************************
  private int getNoCargosAbonos(String nvoTipo) {
    java.sql.Connection pooledConnection = com.bital.util.ConnectionPoolingManager.getPooledConnection( );
    String dbQualifier = com.bital.util.ConnectionPoolingManager.getdbQualifier();
    java.sql.Statement sqlStatement = null;
    java.sql.ResultSet rs = null;
    //    java.sql.ResultSetMetaData rsmd = null;
    int noFilas = 0;
    try {
      if(pooledConnection != null) {
        sql = "SELECT COUNT(*) FROM " + dbQualifier + ".TC_CATLOGO_TOTALES ";
        sql = sql + "WHERE  D_TIPO='" + nvoTipo + "' AND D_DESCRIPCION <> 'nulo'";
        sqlStatement = pooledConnection.createStatement();
        rs = sqlStatement.executeQuery(sql);
        try {
          rs.next();
          noFilas = rs.getInt(1);
          if(nvoTipo.equals("C")) {
            intNoCargos = noFilas;
          }
          else {
            intNoAbonos = noFilas;
          }
          
        }
        catch(SQLException sqlex) {
          System.out.println("Error en la conexi�n a la BD: " + sqlex);
        }
        //**//
      }
    }
    catch(java.sql.SQLException sqlex) {
      System.out.println("Error Total1::ExecuteSql [" + sqlex.toString() + "]");
    }
    finally {
      try {
        if(rs != null){
          rs.close();
          rs = null;
        }
        if(sqlStatement != null){
          sqlStatement.close();
          sqlStatement = null;
        }
        if(pooledConnection != null){
          pooledConnection.close();
          pooledConnection = null;
        }
      }
      catch(java.sql.SQLException sqlException){
        System.out.println("Error FINALLY ::SelectTxn [" + sqlException.getErrorCode() +  sqlException.getMessage() + "]");
      }
    }
    return noFilas;
  }
  
  public int NoCargos() {
    return intNoCargos;
  }
  
  public int NoAbonos() {
    return intNoAbonos;
  }
  
  public void setNoDescCargos(String nvoTipo) {
    java.sql.Connection pooledConnection = com.bital.util.ConnectionPoolingManager.getPooledConnection( );
    String dbQualifier = com.bital.util.ConnectionPoolingManager.getdbQualifier();
    java.sql.Statement sqlStatement = null;
    java.sql.ResultSet rs = null;
    //  java.sql.ResultSetMetaData rsmd = null;
    try {
      if(pooledConnection != null) {
        int i=0;
        String ColNoTotal = "C_NUMERO_DE_TOTAL";
        String ColDescTotal = "D_DESCRIPCION";
        sql = "SELECT " + ColNoTotal + ", " + ColDescTotal + " FROM " + dbQualifier + ".TC_CATLOGO_TOTALES ";
        sql = sql + "WHERE  D_TIPO='" + nvoTipo + "' AND D_DESCRIPCION <> 'nulo'" + statementPS;
        sqlStatement = pooledConnection.createStatement();
        rs = sqlStatement.executeQuery(sql);
        try {
          if(nvoTipo.equals("C")) {
            while(rs.next()) {
              NoTotCargo[i] = rs.getString(ColNoTotal);
              DescTotCargo[i] = rs.getString(ColDescTotal);
              i++;
            }//fin while
          }// fin if
          else {
            while(rs.next()) {
              NoTotAbono[i] = rs.getString(ColNoTotal);
              DescTotAbono[i] = rs.getString(ColDescTotal);
              i++;
            }//fin while
          }//fin else
        }//fin try
        catch(SQLException sqlex) {
          System.out.println("Error en la conexi�n a la BD: " + sqlex);
        }//fin catch
        //****//
      }
    }
    catch(java.sql.SQLException sqlex){
      System.out.println("Error NNototal::ExecuteSql [" + sqlex.toString() + "]");
    }
    finally{
      try {
        if(rs != null){
          rs.close();
          rs = null;
        }
        if(sqlStatement != null){
          sqlStatement.close();
          sqlStatement = null;
        }
        if(pooledConnection != null){
          pooledConnection.close();
          pooledConnection = null;
        }
      }
      catch(java.sql.SQLException sqlException){
        System.out.println("Error FINALLY ::SelectTxn [" + sqlException.getErrorCode() +  sqlException.getMessage() + "]");
      }
    }
  }
  
  //*********************************************************
  // OBTIENE NUMEROS Y DESCRIPCIONES DE LOS TOTALES ACTIVOS *
  //*********************************************************
  public String getNoTotCargo(int newindex) {
    return NoTotCargo[newindex];
  }
  
  public String getDescTotCargo(int newindex) {
    return DescTotCargo[newindex];
  }
  
  public String getNoTotAbono(int newindex) {
    return NoTotAbono[newindex];
  }
  
  public String getDescTotAbono(int newindex) {
    return DescTotAbono[newindex];
  }
  //*********************************************************
  //        FIN DE OBTENCION DE No. Y DESCRIPCIONES	      *
  //*********************************************************
  
  private int cambiaEntero(String newString) {
    String tmpString = new String(newString);
    int chgInt = Integer.parseInt(tmpString);
    return chgInt;
  }
  
  
  private String cambiaCadena(int newint) {
    Integer tmpint = new Integer(newint);
    String chgString = tmpint.toString();
    return chgString;
  }
  
  
  public void setDITotales(String nvoTipo,String Moneda) 
  {
      java.sql.Connection pooledConnection = null;
      
      String dbQualifier = com.bital.util.ConnectionPoolingManager.getdbQualifier();
      java.sql.Statement sqlStatement = null;
      java.sql.ResultSet rs = null;
      //        java.sql.ResultSetMetaData rsmd = null;
      String ColDocs = "N_NUM_DOCUMENTOS";
      String ColImporte = "N_IMPORTE";
        if(nvoTipo.equals("C")) 
        {
            for(int i=0; i < NoCargos; i++) 
            {
                pooledConnection = com.bital.util.ConnectionPoolingManager.getPooledConnection( );
                try
                {
                    if(pooledConnection != null)
                    {
                        documentos = 0;
                        importe = 0;
                        txtimporte = "";
                        sql = "SELECT " + ColDocs + ", " + ColImporte + " FROM " + dbQualifier + ".TW_T_NMDCTO_IMPRTE ";
                        sql = sql + "WHERE C_DIVISA='" + Moneda + "' AND N_NUM_TOTAL=" + getNoTotCargo(i) + " ORDER BY C_ID_TELLER ASC" + statementPS;
                      
                        sqlStatement = pooledConnection.createStatement();
                        rs = sqlStatement.executeQuery(sql);
                        try 
                        {
                            while(rs.next()) 
                            {
                                documentos = documentos + rs.getInt(ColDocs);
                                importe = importe + rs.getInt(ColImporte);
                            }
                          
                            txtimporte = cambiaCadena(importe);
                            txtimporte = formatoNumero(txtimporte);
                            DocsTotCargo[i] =  documentos;
                            ImporteTotCargo[i] = txtimporte;
                        }
                        catch(SQLException sqlex) 
                            {System.out.println("Error en conexi�n de BD : " + sqlex);}
                    }
                }
                catch(SQLException sqlException)
                {System.out.println("Error PresentaTotalesG::setDITotales [" + sqlException + "]");}
                finally {
                    try {
                        if(rs != null) {
                            rs.close();
                            rs = null;
                        }
                        if(sqlStatement != null) {
                            sqlStatement.close();
                            sqlStatement = null;
                        }
                        if(pooledConnection != null) {
                            pooledConnection.close();
                            pooledConnection = null;
                        }
                    }
                    catch(java.sql.SQLException sqlException){System.out.println("Error FINALLY Group01Nafin::Post [" + sqlException + "]");}
                }
            }// fin del ciclo for
        }
        else
        {
            for(int i=0; i < NoAbonos; i++) 
            {
                pooledConnection = com.bital.util.ConnectionPoolingManager.getPooledConnection( );
                try
                {
                    if(pooledConnection != null)
                    {
                        documentos = 0;
                        importe = 0;
                        txtimporte = "";
                        sql = "SELECT " + ColDocs + ", " + ColImporte + " FROM " + dbQualifier + ".TW_T_NMDCTO_IMPRTE ";
                        sql = sql + "WHERE C_DIVISA='" + Moneda + "' AND N_NUM_TOTAL=" + getNoTotAbono(i) + " ORDER BY C_ID_TELLER ASC" + statementPS;
                        sqlStatement = pooledConnection.createStatement();
                        rs = sqlStatement.executeQuery(sql);
                        try
                        {
                            while(rs.next()) 
                            {
                                documentos = documentos + rs.getInt(ColDocs);
                                importe = importe + rs.getInt(ColImporte);
                            }
                            txtimporte = cambiaCadena(importe);
                            txtimporte = formatoNumero(txtimporte);
                            DocsTotAbono[i] = documentos;
                            ImporteTotAbono[i] = txtimporte;
                        }
                        catch(SQLException sqlex)
                            {System.out.println("Error en conexi�n de BD : " + sqlex);}
                      
                    }
                }
                catch(SQLException sqlException)
                {System.out.println("Error PresentaTotalesG::setDITotales [" + sqlException + "]");}
                finally {
                    try {
                        if(rs != null) {
                            rs.close();
                            rs = null;
                        }
                        if(sqlStatement != null) {
                            sqlStatement.close();
                            sqlStatement = null;
                        }
                        if(pooledConnection != null) {
                            pooledConnection.close();
                            pooledConnection = null;
                        }
                    }
                    catch(java.sql.SQLException sqlException){System.out.println("Error FINALLY Group01Nafin::Post [" + sqlException + "]");}
                }
            }// fin del ciclo for
        }
  }// fin metodo setDITotales()
  
  public int getDocsTotCargo(int newindex) {
    return DocsTotCargo[newindex];
  }
  
  public String getImporteTotCargo(int newindex) {
    return ImporteTotCargo[newindex];
  }
  
  public int getDocsTotAbono(int newindex) {
    return DocsTotAbono[newindex];
  }
  
  public String getImporteTotAbono(int newindex) {
    return ImporteTotAbono[newindex];
  }
  
  
  //***************************************************************************************
  // I N I C I O    I M P R E S I O N    T O T A L E S ************************************
  //***************************************************************************************

  public String cadenaImpresion(String suc, String mon)
  {
    //primeras 8 posiciones descripcion si es cargo o abono justificado a la izquierda
    //de la 9 a la 12 Numero de total  justificado a la izquierda
    //13 a la 50 descripcion de total justificado a la izquierda
    //51 a la 61 no de documentos justificado a la derecha
    //65 a la 82 importe justificado a la izquierda
    boolean control1 = true;
    boolean ctrlCargos = true;
    boolean ctrlAbonos = false;
    int contador1 = 0;
    int contador2 = 0;
    int ctrlSwitch = 1;
    while(control1 == true)
    {
      switch(ctrlSwitch)
      {
      case 1:
          StringFormat =  StringFormat + "                   B A N C O   I N T E R N A C I O N A L          ~";
          StringFormat =  StringFormat + "                          TOTALES LOCALES SUCURSAL                ~";
          StringFormat = StringFormat + "                    SUCURSAL : "+ suc + "  MONEDA : " + mon + "    ~ ~ ~";
          StringFormat = StringFormat + "    CARGOS   " + formatNoTotal(getNoTotCargo(contador1)) + formatDescripTotal(getDescTotCargo(contador1)) + formatDocsTotal(cambiaCadena(getDocsTotCargo(contador1)))+ formatImptsTotal(getImporteTotCargo(contador1))+"~";
          StringFormat = StringFormat.toUpperCase();
        ctrlSwitch = 2;
        contador1++;
        break;
 
      case 2:
        if(contador1 >= NoCargos)
        {
          ctrlSwitch = 3;
        }
        else
        {
          StringFormat = StringFormat + "             " + formatNoTotal(getNoTotCargo(contador1)) + formatDescripTotal(getDescTotCargo(contador1)) + formatDocsTotal(cambiaCadena(getDocsTotCargo(contador1)))+ formatImptsTotal(getImporteTotCargo(contador1))+"~";
          StringFormat = StringFormat.toUpperCase();
          contador1++;
        }
        break;
 
      case 3:
        StringFormat = StringFormat + "~ ~    ABONOS   " + formatNoTotal(getNoTotAbono(contador2)) + formatDescripTotal(getDescTotAbono(contador2)) + formatDocsTotal(cambiaCadena(getDocsTotAbono(contador2)))+ formatImptsTotal(getImporteTotAbono(contador2))+"~";
          StringFormat = StringFormat.toUpperCase();
        contador2++;
        ctrlSwitch = 4;
        break;
 
      case 4:
        if(contador2 >= NoAbonos)
        {
          control1 = false;
        }
        else
        {
          StringFormat = StringFormat + "             " + formatNoTotal(getNoTotAbono(contador2)) + formatDescripTotal(getDescTotAbono(contador2)) + formatDocsTotal(cambiaCadena(getDocsTotAbono(contador2)))+ formatImptsTotal(getImporteTotAbono(contador2))+"~";
          StringFormat = StringFormat.toUpperCase();
          contador2++;
        }
        break;
 
      default:
        control1 = false;
        break;
 
      }
    }
 
    return StringFormat;
  }
 
 
  private String formatNoTotal(String newNoTotal)
  {
    String formatNoTotal = "";
    int tmplength = newNoTotal.length();
    if(tmplength == 1)
    {
      formatNoTotal = "(0" + newNoTotal.substring(0,1) + ") ";
    }
    else
    {
      newNoTotal = newNoTotal.trim();
      formatNoTotal = "(" + newNoTotal + ") ";
    }
    return formatNoTotal;
  }
 
  private String formatDescripTotal(String newDescrip)
  {
    int longitud = 27 - newDescrip.length();
    for(int i=1; i<=longitud; i++)
    {
      newDescrip = newDescrip + " ";
    }
    return newDescrip;
  }
 
  private String formatDocsTotal(String newDocs)
  {
    int longitud = 11 - newDocs.length();
    for(int i=1; i<=longitud; i++)
    {
      newDocs = " " + newDocs;
    }
    return newDocs;
  }
 
  private String formatImptsTotal(String newImporte)
  {
    int longitud = 21 - newImporte.length();
    for(int i=1; i<=longitud; i++)
    {
      newImporte = " " + newImporte;
    }
    return newImporte;
  }
 
 
  //***************************************************************************************
  // F I N    I M P R E S I O N    T O T A L E S ******************************************
  //***************************************************************************************
}
