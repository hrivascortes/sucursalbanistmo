//*************************************************************************************************
//             Funcion: Bean Txn 5159
//      Creado por: Jesus Emmanuel Lopez Rosales
//                  Yair Jesus Chavez Antonel 
//*************************************************************************************************
// CCN - 4360533 - 20/10/2006 - Se crea bean para proyecto OPEE
//*************************************************************************************************
package ventanilla.com.bital.sfb;

import ventanilla.GenericClasses;

import com.bital.util.ParameterNotFoundException;
import com.bital.util.ParameterParser;

public class Txn_5159 extends Transaction
{
	public Header doAction(ParameterParser param)
		throws ParameterNotFoundException
	{

		FormatoA formatoa = new FormatoA();
		GenericClasses gc = new GenericClasses();
		
		formatoa.setTxnCode("5159");
		formatoa.setFormat("A");
		formatoa.setAcctNo(param.getString("txtDDACuenta"));
		formatoa.setTranAmt(param.getCString("txtMonto"));		
		String moneda = gc.getDivisa(param.getString("moneda"));		
		formatoa.setTranCur(moneda);

		String txtTipoCambio = param.getString("txtTC");
		String entero = txtTipoCambio.substring(0,7);
		String decimal = txtTipoCambio.substring(7);
		txtTipoCambio = entero.substring(entero.length()-4) + decimal.substring(0,4);
		
		formatoa.setDraftAm(param.getString("txtPlaza")+txtTipoCambio);
		formatoa.setMoAmoun(param.getCString("txtMonto1"));
	
		return formatoa;
	}
}

                 