//*************************************************************************************************
//             Funcion: Bean Txn 0216
//      Modificado por: Jesus Emmanuel Lopez Rosales
//*************************************************************************************************
// CCN - 4360268 - 04/02/2005 - Se realizan adecuaciones para eliminar Agencia NY
// CCN - 4360424 - 03/02/2006 - Se agrega txn para divisas.
// CCN - 4360576 - 09/03/2007 - Modificaciones para proyecto OPMN
//*************************************************************************************************
package ventanilla.com.bital.sfb;

import com.bital.util.ParameterParser;
import com.bital.util.ParameterNotFoundException;

public class Txn_0354 extends Transaction
{
	public Header doAction(ParameterParser param)
		throws ParameterNotFoundException
	{
		FormatoB formatob = new FormatoB();
	    formatob.setTxnCode("0354");
	    formatob.setFormat("B");
	    formatob.setAcctNo( "0354" );
	    formatob.setTranAmt(param.getCString("montoComision" ));
	    formatob.setReferenc("COMISION O.P.");
	    formatob.setFeeAmoun("0");
	    formatob.setIntern("0");
	    formatob.setIntWh("0");
	    formatob.setCashIn(param.getCString("montoComision" ));
	    formatob.setFromCurr("N$");

		return formatob;
	}
}
