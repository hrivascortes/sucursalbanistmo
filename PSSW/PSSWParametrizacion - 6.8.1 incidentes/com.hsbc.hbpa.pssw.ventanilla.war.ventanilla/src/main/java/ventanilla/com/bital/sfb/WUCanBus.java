//************************************************************************************************* 
//             Funcion: Bean para WU Cancelacion Busqueda
//            Elemento: WUCanBus.java
//          Creado por: Alejandro Gonzalez Castro
//      Modificado por: Rutilo Zarco Reyes
//*************************************************************************************************
// CCN - 4360189 - 03/09/2004 - Se habilita WU
//*************************************************************************************************

package ventanilla.com.bital.sfb;

public class WUCanBus extends Header3 
{
    private String txtPaginacion    = "";

    public String getPaginacion() 
    {
        return txtPaginacion;
    }
    public void setPaginacion(String newPaginacion) 
    {
        txtPaginacion = newPaginacion;
    }

    public String toString() 
    {
        return super.toString() + getPaginacion();
    }
}
