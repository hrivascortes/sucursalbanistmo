package ventanilla.com.bital.util;

public class Usuario
{
  private String cajero   = null;
  private String registro = null;
  private String nombre   = null;
  private String nivel    = null;
  private String menu     = null;
  private String horario  = null;
  private String promotor = null;
  private String sucursal = null;

  public Usuario()
  {
    this.cajero = "000143";
    this.registro = "0431866";
    this.nombre = "ALEJANDRO MEDINA PALACIOS";
    this.nivel = "2";
    this.menu = "GERENTE";
    this.horario = "AM";
    this.promotor = "W0001";
    this.sucursal = "0001";
  }

  public String toString()
  {
    return "*" + getSucursal() + "/" + cajero + "/" + registro + "/" + nombre
      + "/" + nivel + "/" + menu + "/" + horario + "/" + promotor + "*";
  }

  private void reset()
  {
  }

  public String getCajero()
  {
    return this.cajero;
  }

  public void setCajero(String cajero)
  {
    this.cajero = cajero;
  }

  public String getRegistro()
  {
    return this.registro;
  }

  public void setRegistro(String registro)
  {
    this.registro = registro;
  }

  public String getNombre()
  {
    return this.nombre;
  }

  public void setNombre(String nombre)
  {
    this.nombre = nombre;
  }

  public String getNivel()
  {
    return this.nivel;
  }

  public void setNivel(String nivel)
  {
    this.nivel = nivel;
  }

  public String getMenu()
  {
    return this.menu;
  }

  public void setMenu(String menu)
  {
    this.menu = menu;
  }

  public String getHorario()
  {
    return this.horario;
  }

  public void setHorario(String horario)
  {
    this.horario = horario;
  }

  public String getPromotor()
  {
    return this.promotor;
  }

  public void setPromotor(String promotor)
  {
    this.promotor = promotor;
  }

  public void setSucursal(String sucursal)
  {
    this.sucursal = sucursal;
  }

  public String getSucursal()
  {
    return this.sucursal;
  }
}