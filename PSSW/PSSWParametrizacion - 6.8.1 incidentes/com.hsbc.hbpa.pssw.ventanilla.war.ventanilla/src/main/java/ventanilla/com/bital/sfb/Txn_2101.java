//*************************************************************************************************
//             Funcion: Bean Txn 2101
//      Modificado por: Humberto Enrique Balleza Mej�a
//*************************************************************************************************
// CCN - 4360607 - 18/05/2007 - Bean para txn 2101
//*************************************************************************************************

package ventanilla.com.bital.sfb;

import com.bital.util.ParameterParser;
import com.bital.util.ParameterNotFoundException;
import ventanilla.GenericClasses;

public class Txn_2101 extends Transaction{
	public Header doAction(ParameterParser param)
		throws ParameterNotFoundException
	{
		FormatoA formatoa = new FormatoA();
		GenericClasses gc = new GenericClasses();
		formatoa.setTxnCode("2101");
		formatoa.setFormat("A");		
		formatoa.setAcctNo(param.getString("txtDDACuenta"));		
		formatoa.setTranAmt(param.getCString("txtMonto"));		
		formatoa.setTranDesc(param.getString("txtDetalle"));					
		String moneda = gc.getDivisa(param.getString("moneda"));
		formatoa.setTranCur(moneda);		
	
		return formatoa;
	}
}