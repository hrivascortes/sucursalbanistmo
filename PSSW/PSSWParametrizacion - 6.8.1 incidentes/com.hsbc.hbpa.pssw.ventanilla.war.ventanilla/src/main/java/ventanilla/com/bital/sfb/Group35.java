//*************************************************************************************************
//		   Funcion: Transaccion 4107 - Cheques de Caja
//		  Elemento: Group35.java
//	    Modificado por: Alejandro Gonzalez Castro
//*************************************************************************************************
////CCN - 4360424 - 03/02/2006 - Se realizan adecuaciones para combinadas
//*************************************************************************************************

package ventanilla.com.bital.sfb;

import com.bital.util.ParameterParser;
import com.bital.util.ParameterNotFoundException;

public class Group35 extends Transaction
{
  public Header doAction(ParameterParser param)
    throws ParameterNotFoundException
  {
    Cheques cheque = new Cheques();
	cheque.setTxnCode("4107");    
    cheque.setTranAmt( param.getCString("txtMonto") );
    int moneda = param.getInt("moneda");
    if( moneda == 1 )
    {
      cheque.setAcctNo ("0103000023");
      cheque.setTranCur("N$");
    }
    else if( moneda == 2 )
    {
      cheque.setAcctNo ("3902222222");
      cheque.setTranCur("EUR");
    }

    cheque.setCheckNo( param.getString("txtSerialCan") );
    return cheque;
  }               
}
