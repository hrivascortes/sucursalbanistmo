//*************************************************************************************************
//             Funcion: Bean Txn 0821
//           Elemento : Txn_0821.java
//          Creado por: Yair J. Chavez Antonel
//      Modificado por: Jesus Emmanuel Lopez Rosales
//*************************************************************************************************
// CCN - 4360533 - 20/10/2006 - Se crea bean para proyecto OPEE
// CCN - 4360542 - 10/11/2006 - Se realizan modificaciones para OPEE y txn 0821
// CCN - 4360576 - 09/03/2007 - Modificaciones para proyecto OPMN
// CCN - 4360592 - 20/04/2007 - Modificaciones para proyecto OPMN
//*************************************************************************************************

package ventanilla.com.bital.sfb;

import com.bital.util.ParameterNotFoundException;
import com.bital.util.ParameterParser;
import ventanilla.GenericClasses;

public class Txn_0821 extends Transaction{

	public static String cajero ="";
	public static String consecin ="";
    public static String consecout ="";
	
	public Header doAction(ParameterParser param) throws ParameterNotFoundException
	{
		GenericClasses gc = new GenericClasses();
		CompraVenta oTxn = new CompraVenta();
        String ConsecutivoIN =  "";
		String ConsecutivoOUT = "";
		String txtSupervisor = (String)param.getString("teller");
		String cTxn = param.getString("cTxn");
		String iTxn = param.getString("iTxn");
		String OT = param.getString("oTxn");
		FormatoG formatog = new FormatoG();
		formatog.setReversable("N");
		if(cTxn.equals("4033") || cTxn.equals("4101")){
		  formatog.setReversable("S");
		}	
		formatog.setTxnCode("0821");
		formatog.setFormat("G");
		
		if(cTxn.equals("5159"))
		{
			formatog.setService("V");
			formatog.setGStatus("3");			
			formatog.setTranAmt(param.getCString("txtMonto1"));		             
		}
		if(cTxn.equals("5183"))
		{
			//ODPA 0821G 00002000251  000240  000**V*EUR*00000*1**59000*001693500**0000234*0000234*0726      145411145413000251***
			String moneda = gc.getDivisa(param.getString("moneda"));
			if(moneda.equals("N$"))
				formatog.setService("V");
			else
				formatog.setService("C");
			formatog.setGStatus("3");			
			formatog.setTranAmt(param.getCString("txtMonto1"));
		}
		if(cTxn.equals("4525"))
		{
			//ODPA 0821G 00002000251  000240  000**V*EUR*00000*1**59000*001693500**0000234*0000234*0726      145411145413000251***
			String moneda = gc.getDivisa(param.getString("moneda"));
			if(moneda.equals("N$"))
				formatog.setService("V");
			else
				formatog.setService("C");
			formatog.setGStatus("5");			
			formatog.setTranAmt(param.getCString("txtMonto1"));
		}
		if(cTxn.equals("2201"))
		{
			//ODPA 0821G 00002000251  000240  000**V*EUR*00000*1**59000*001693500**0000234*0000234*0726      145411145413000251***
			String moneda = gc.getDivisa(param.getString("moneda"));
			if(moneda.equals("N$"))
				formatog.setService("V");
			else
				formatog.setService("C");
			formatog.setGStatus("5");			
			formatog.setTranAmt(param.getCString("txtMonto1"));
		}

		if(cTxn.equals("4033") || cTxn.equals("4101"))
		{
			if(cTxn.equals("4033")){
			 formatog.setService("C");
 	         formatog.setOrdenan(param.getString("txtNomOrd"));
			}else{
			 formatog.setService("B");
			 formatog.setOrdenan(param.getString("txtNomOrd"));
			} 
			formatog.setGStatus("5");			
			formatog.setTranAmt(param.getCString("txtMonto1"));

	        formatog.setIVAAmt("0");
		}		
				
		//formatog.setToCurr("US$");
		String moneda = gc.getDivisa(param.getString("moneda"));
		if(moneda.equals("N$"))
			formatog.setToCurr(gc.getDivisa(param.getString("moneda2")));
		else
			formatog.setToCurr(gc.getDivisa(param.getString("moneda")));
			
			
			
        String txtAutoriCV = param.getString("txtAutoriCV");
        if (txtAutoriCV == null || (txtAutoriCV.equals("") && cTxn.equals("4033")))
        	txtAutoriCV = "00000";
		if(!txtAutoriCV.equals("00000") && !txtAutoriCV.equals("") && iTxn.equals("2201"))
			oTxn.setGStatus( "5" );
			
		if(!txtAutoriCV.equals("00000") && !txtAutoriCV.equals("") && iTxn.equals("4525"))
				oTxn.setGStatus( "5" );

		if(!txtAutoriCV.equals("00000") && !txtAutoriCV.equals("") && iTxn.equals("5183"))
					oTxn.setGStatus( "3" );
		if(!txtAutoriCV.equals("00000") && !txtAutoriCV.equals("") && OT.equals("0106"))
					oTxn.setGStatus( "1" );
		if(!txtAutoriCV.equals("00000") && !txtAutoriCV.equals("") && OT.equals("4111"))
					oTxn.setGStatus( "5" );

		formatog.setCheckNo( "00000" + txtAutoriCV );
		
		String txtTipoCambio="";
				
	    txtTipoCambio = param.getString("txtTC");

		
		String entero = txtTipoCambio.substring(0,7);
		String decimal = txtTipoCambio.substring(7);	//4						  4
		txtTipoCambio = entero.substring(entero.length()-3) + decimal.substring(0,6);
		
        formatog.setComAmt(txtTipoCambio);
        
        if(!cTxn.equals("4033")){
          formatog.setIVAAmt(param.getCString("txtIvaMN"));
        }
        
        formatog.setAcctNo(param.getString("registro"));
        formatog.setCtaBenef(param.getString("txtRegistro"));        
        
        ConsecutivoIN = consecin ;        
        ConsecutivoOUT = consecout;        
        if (ConsecutivoIN.length()<6){
           	ConsecutivoIN = "0" + ConsecutivoIN; 
        }
        
		if (ConsecutivoOUT == null || ConsecutivoOUT.length()==0)
			ConsecutivoOUT = "000000";
        else if (ConsecutivoOUT.length()<6)
           	ConsecutivoOUT = "0" + ConsecutivoOUT;  
           
		/*
        if(cTxn.equals("4033")){            
         formatog.setBenefic(param.getString("txtDDACuenta") + ConsecutivoIN + ConsecutivoOUT + cajero);
	    }else{
	       if(iTxn.equals("0128") || iTxn.equals("0130")){
	          int lon =  iTxn.length();
	          for(int j = lon;j<10;j++){
	          	iTxn = iTxn + " "; 
	          }                  
	          formatog.setBenefic(iTxn + ConsecutivoIN + ConsecutivoOUT + cajero);	       	
	       }else{  
	          int lon =  cTxn.length();
	          for(int j = lon;j<10;j++){
	          	cTxn = cTxn + " "; 
	          }                  
	          formatog.setBenefic(cTxn + ConsecutivoIN + ConsecutivoOUT + cajero);
	       }   
        }
        */

       
        formatog.setIVAAmt(param.getCString("txtMonto"));
        
		if(cTxn.equals("5183"))
		{
			if(moneda.equals("N$"))
		        formatog.setBenefic("05985183  " + ConsecutivoIN + ConsecutivoOUT + cajero);
			else
		        formatog.setBenefic("51830598  " + ConsecutivoIN + ConsecutivoOUT + cajero);
		}
		if(cTxn.equals("4525"))
		{
			if(moneda.equals("N$"))
				formatog.setBenefic("07704525  " + ConsecutivoIN + ConsecutivoOUT + cajero);
			else
				formatog.setBenefic("45250770  " + ConsecutivoIN + ConsecutivoOUT + cajero);
		}
		if(cTxn.equals("2201"))
		{
			if(moneda.equals("N$"))
				formatog.setBenefic("22010768  " + ConsecutivoIN + ConsecutivoOUT + cajero);
			else
				formatog.setBenefic("07682201  " + ConsecutivoIN + ConsecutivoOUT + cajero);
		}
		
/*		Catalogo de GStatus 
		1 - Efectivo
		3 - Orden de Pago
		5 - Documentos
*/
		return formatog;
	}
}
