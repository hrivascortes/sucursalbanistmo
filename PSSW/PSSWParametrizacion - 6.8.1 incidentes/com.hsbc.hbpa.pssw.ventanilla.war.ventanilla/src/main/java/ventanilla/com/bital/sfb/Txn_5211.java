//*************************************************************************************************
//             Funcion: Bean Txn 5211
//      Modificado por: Alejandro Gonzalez Castro
//*************************************************************************************************
// CCN - 4360437 - 24/02/2006 - Se realizan modificaciones para transacciones SPEI
//*************************************************************************************************

package ventanilla.com.bital.sfb;

import com.bital.util.ParameterParser;
import com.bital.util.ParameterNotFoundException;
import ventanilla.GenericClasses;

public class Txn_5211 extends Transaction
{
	public Header doAction(ParameterParser param)
		throws ParameterNotFoundException
	{
		FormatoA formatoa = new FormatoA();
		GenericClasses gc = new GenericClasses();
		
		formatoa.setTxnCode("5211");
		formatoa.setFormat("A");
		formatoa.setAcctNo(param.getString("txtDDACuenta"));
		formatoa.setTranAmt(param.getCString("txtMonto"));
		String moneda = gc.getDivisa(param.getString("moneda"));
		formatoa.setTranCur(moneda);
		formatoa.setCheckNo(param.getString("txtSerial"));
		formatoa.setMoAmoun("000");
		
		if( param.getString("txtFechaEfect", null) != null ){
	      String fecha1 = param.getString("txtFechaEfect");
    	  String fecha2 = param.getString("txtFechaSys");
	      fecha1 = fecha1.substring(6) + fecha1.substring(3, 5) + fecha1.substring(0, 2);

	      if( !fecha1.equals(fecha2) ){
	        fecha2 = fecha1.substring(4, 6) + fecha1.substring(6) + fecha1.substring(2, 4);
    	    formatoa.setEffDate(fecha2);
      		}
	    }
		
		return formatoa;
	}
}
