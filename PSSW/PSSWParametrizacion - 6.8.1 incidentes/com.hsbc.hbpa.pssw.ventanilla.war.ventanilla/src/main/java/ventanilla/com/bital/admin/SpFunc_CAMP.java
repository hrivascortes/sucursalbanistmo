//************************************************************************************************* 
//             Funcion: Spfunc para CAMP
//            Elemento: SpFunc_CAMP.java
//          Creado por: Jesus Emmanuel Lopez Rosales
//          Modificado por: Jesus Emmanuel Lopez Rosales
//*************************************************************************************************
// CO - CR7564 - 26/01/2011 - Cambios Proyecto camp
//*************************************************************************************************

package ventanilla.com.bital.admin;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Hashtable;
import java.util.Vector;

import javax.servlet.http.HttpSession;
import ventanilla.GenericClasses;
import com.bital.util.ConnectionPoolingManager;



public class SpFunc_CAMP implements InterfazSF {

	public int SpFuncionBD(Object bean,Vector Respuesta,HttpSession session,String Connureg) {
		
		return 0; 
	}

	public int SpFuncionAD(Object bean,Vector Respuesta,HttpSession session,String Connureg) {
		
		int messagecode = 0;		
		GenericClasses gc = new GenericClasses();				

		Hashtable datasession = (Hashtable)session.getAttribute("page.datasession");						
		Hashtable usrinfo = (Hashtable)session.getAttribute("userinfo");		
		
		String cTxn = (String)datasession.get("cTxn");
		
		

		Connection pooledConnection = ConnectionPoolingManager.getPooledConnection();
		PreparedStatement insert = null;	
		int ResultInsert = 0;
		
		try{			       								 	
			 if(pooledConnection != null){	
				insert = pooledConnection.prepareStatement("INSERT INTO TW_CAMP_LOG VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
				insert.setString(1, "16"+datasession.get("teller")+Respuesta.elementAt(2).toString().substring(1)); //D_TXN_ID
				insert.setString(2, Connureg);//D_CONNUREG
				insert.setString(3, "PSSW");//C_APP_CODE
				
				insert.setString(4, gc.StringFiller(datasession.get("txtDDACuenta").toString(), 18, false, "0"));//D_CUENTA
				
				String sFecha=gc.getDate(1);
				insert.setString(5,sFecha);//D_FECHA
				insert.setString(6,sFecha);//D_FECHA_POST
				insert.setString(7,"0"+cTxn);//C_TXN
				
				if(cTxn.equals("2199") || cTxn.equals("2201"))
					insert.setString(8, "");//D_NUM_REFE
				else
					if(session.getAttribute("numOPEE") != null)
						insert.setString(8, session.getAttribute("numOPEE").toString());//D_NUM_REFE
					else
						insert.setString(8, "");//D_NUM_REFE
				
				insert.setString(9,"00000"+ datasession.get("sucursal").toString());//D_SUCURSAL
				insert.setString(10,"0000" + datasession.get("teller").toString());//D_CAJERO
				insert.setString(11,"0000000000");//D_CHECK_NUM
				insert.setString(12,"000");//D_NUM_CHECKS
				
				if(cTxn.equals("2199") || cTxn.equals("2201"))
					insert.setString(13,"C");//D_INDICADOR
				else//5177,5183			  
					insert.setString(13,"D");//D_INDICADOR
				
				if(cTxn.equals("5177") || cTxn.equals("2199") || cTxn.equals("5183"))
					insert.setString(14,gc.StringFiller(gc.delCommaPointFromString(datasession.get("txtMonto").toString()), 18, false, "0"));//D_MONTO
				else//2201
					insert.setString(14,gc.StringFiller(gc.delCommaPointFromString(datasession.get("txtTotalMN").toString()), 18, false, "0"));//D_MONTO				
				
				insert.setString(15,"000000000000000000");//D_CASH
				
				Vector monedas = (Vector) session.getAttribute("monedas.info");
				for(int i=0;i<monedas.size();i++){
					Vector moneda = (Vector) monedas.elementAt(i);
					if(moneda.elementAt(0).equals(datasession.get("moneda").toString()))
						if(moneda.elementAt(0).equals("01"))
							insert.setString(16,"USD");//D_MONEDA
						else
							insert.setString(16,moneda.elementAt(2).toString());//D_MONEDA
				}
				
				if(cTxn.equals("2199") || cTxn.equals("2201"))
					insert.setString(17,"Y");//D_OVERSEAS
				else//5177,5183
					insert.setString(17,"N");//D_OVERSEAS
				
				insert.setString(18,"0");//D_CUENTA_B
				insert.setString(19,"DESCONOCIDO");//D_NOM_B
				
				if(cTxn.equals("5177") || cTxn.equals("5183")){
					insert.setString(20,"");//D_ORIGINATOR
					insert.setString(21,"HBPA");//D_BANCO_O
					insert.setString(22,"HBPA");//D_BANCO_S
					insert.setString(23,"DESCONOCIDO");//D_BANCO_B
					insert.setString(24,"DESCONOCIDO");//D_BENEFICIARIO
					insert.setString(25,"DESCONOCIDO");//D_BANCO_INTER
					insert.setString(26,"DF");//D_TIPO_PAGO
					insert.setString(27,"N");//D_BTOB_CODE
					insert.setString(28,"DESCONOCIDO");//D_DETALLE
					insert.setString(29,"PA");//D_SBANK_CODE
					insert.setString(30,"PA");//D_OBANK_CODE
					insert.setString(31,"");//D_BENEF_CODE
					insert.setString(32,"");//D_BBANK_CODE
					insert.setString(33,"PA");//D_ORIG_CODE
					insert.setString(34,"");//D_IBANK_CODE
				}else{//2199,2201
					insert.setString(20,"DESCONOCIDO");//D_ORIGINATOR
					insert.setString(21,"");//D_BANCO_O
					insert.setString(22,"DESCONOCIDO");//D_BANCO_S
					insert.setString(23,"HBPA");//D_BANCO_B
					insert.setString(24,"");//D_BENEFICIARIO
					insert.setString(25,"DESCONOCIDO");//D_BANCO_INTER
					insert.setString(26,"DF");//D_TIPO_PAGO
					insert.setString(27,"N");//D_BTOB_CODE
					insert.setString(28,"DESCONOCIDO");//D_DETALLE
					insert.setString(29,"");//D_SBANK_CODE
					insert.setString(30,"");//D_OBANK_CODE
					insert.setString(31,"PA");//D_BENEF_CODE
					insert.setString(32,"PA");//D_BBANK_CODE
					insert.setString(33,"");//D_ORIG_CODE
					insert.setString(34,"");//D_IBANK_CODE
				}
							
				ResultInsert = insert.executeUpdate();
			 }		 			 			
		}catch(SQLException sqlException){
			System.err.println("SpFunc_CAMP::InsertLog [" + sqlException + "]");								
		}catch( Exception e ){
			System.err.println("SpFunc_CAMP:: " + e.getMessage());
			e.printStackTrace();		 
		}finally {		 			
    		try{
    			if(insert != null) {
    				insert.close();
    				insert = null;
            	}
            	if(pooledConnection != null) {
                	pooledConnection.close();
                	pooledConnection = null;
            	}
        	}catch(java.sql.SQLException sqlException){
        		
        		sqlException.printStackTrace();
            	System.out.println("Error FINALLY SPFunc_camp::deleteLog [" + sqlException + "]");	            	
          		
        	}
	 	}
		
		return messagecode;	
	}   
	
	public int SpFuncionBR(Hashtable txnRev, Hashtable txnOrg, HttpSession session, String Connureg, String Supervisor){
		int messagecode = 0;
		return messagecode;
	}
	
	public int SpFuncionAR(Hashtable txnRev, Hashtable txnOrg, HttpSession session, String Connureg, String Cosecutivo, String Supervisor, Vector VResp){
		int messagecode = 0;
		
		Connection pooledConnection = ConnectionPoolingManager.getPooledConnection();
		PreparedStatement update = null;
		String statementPS = com.bital.util.ConnectionPoolingManager.getStatementPostFix();
        
        	int ResultUpdate = 0;
	        try{
	        	pooledConnection = ConnectionPoolingManager.getPooledConnection();
		 		update = pooledConnection.prepareStatement("DELETE FROM TW_CAMP_LOG WHERE D_CONNUREG = ?" + statementPS);
			    update.setString(1, txnOrg.get("D_CONNUREG").toString());			   			    
			    ResultUpdate= update.executeUpdate();
			
	        }catch(SQLException sqlException){
				System.err.println("SpFunc_CAMP::DeleteLog [" + sqlException + "]");								
			}catch( Exception e ){
				System.err.println("SpFunc_CAMP:: " + e.getMessage());
				e.printStackTrace();		 
			}finally {		 			
	    		try{
	    			if(update != null) {
	            		update.close();
	                	update = null;
	            	}
	            	if(pooledConnection != null) {
	                	pooledConnection.close();
	                	pooledConnection = null;
	            	}
	        	}catch(java.sql.SQLException sqlException){
	        		
	        		sqlException.printStackTrace();
	            	System.out.println("Error FINALLY SPFunc_camp::deleteLog [" + sqlException + "]");	            	
              		
	        	}
		 	}
		 	
		return messagecode;
	}			
}