//*************************************************************************************************
//             Funcion: Clase que ejecuta el SQL solicitado y regresa el resultado en un vector
//          Creado por: Alejandro Gonzalez Castro.
//      Modificado por: Juvenal R. Fernandez V.
//*************************************************************************************************
// CCN -4360152 - 21/06/2004 - Se crea la clase para realizar select y mantener los datos en memoria
// CCN -4360315 - 06/05/2005 - Se corrige problema con sqls de Persistencia 
// CCN -4360321 - 13/05/2005 - Se envia la clase para Z/OS 1.6
// CCN - 4360334 - 17/06/2005 - Se elimina variable dbQualifier
//*************************************************************************************************

package ventanilla.com.bital.util;

import java.sql.*;
import com.bital.util.ConnectionPoolingManager;
import java.util.Vector;

public class DBQuery 
{
    private Vector Rows = null;
    
    public DBQuery(String sql, String table, String clause)
    {
        Connection conn = ConnectionPoolingManager.getPooledConnection();
        ResultSet rs = null;
        PreparedStatement stmt = null;
        String schema = null;
        
        try 
        {
            if(conn != null) 
            {
                StringBuffer sqltmp = null;
                sqltmp = new StringBuffer(200);
                sqltmp.append(sql);
                sqltmp.append(table);
                sqltmp.append(clause);
                sql = sqltmp.toString();
                
                stmt = conn.prepareStatement(sql);
                
                rs = stmt.executeQuery();
                ResultSetMetaData rsmd = null;
                rsmd = rs.getMetaData();
                Rows = new Vector(100,50);
                int cols = rsmd.getColumnCount();
                while(rs.next()) 
                {
                    Vector tmp = new Vector(cols);
                    for(int i = 1; i <= cols; i++)
                        tmp.add(rs.getString(i));
                    Rows.add(tmp);
                }
            }
        }
        catch(SQLException sqlException)
        {
            System.out.println("Error DBQuery::executeQuery [" + sqlException + "]");
            System.out.println("Error DBQuery::SQL[" + sql + "]");
        }
        finally {
            try {
                if(rs != null) {
                    rs.close();
                    rs = null;
                }
                if(stmt != null) {
                    stmt.close();
                    stmt = null;
                }
                if(conn != null) {
                    conn.close();
                    conn = null;
                }
            }
            catch(java.sql.SQLException sqlException)
            {System.out.println("Error FINALLY DBQuery::executeQuery [" + sqlException + "]");}
        }
    }
    
    public Vector getRows()
    {
        return Rows;
    }
}
