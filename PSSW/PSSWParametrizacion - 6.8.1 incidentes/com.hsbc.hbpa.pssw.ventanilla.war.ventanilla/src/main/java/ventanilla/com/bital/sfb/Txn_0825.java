//*************************************************************************************************
//             Funcion: Bean Txn 0825
//      Modificado por: Alejandro Gonzalez Castro
//*************************************************************************************************
// CCN - 4360268 - 04/02/2005 - Se realizan adecuaciones para eliminar Agencia NY
// CCN - 4360424 - 03/02/2006 - Se agrega para txn combinada
//*************************************************************************************************
package ventanilla.com.bital.sfb;

import com.bital.util.ParameterParser;
import com.bital.util.ParameterNotFoundException;
import ventanilla.GenericClasses;

public class Txn_0825 extends Transaction
{
	public Header doAction(ParameterParser param)
		throws ParameterNotFoundException
	{
		FormatoB formatob = new FormatoB();
		GenericClasses gc = new GenericClasses();

		formatob.setTxnCode("0825");
		formatob.setFormat("B");
		formatob.setAcctNo(param.getString("lstTipoDocto") + param.getString("txtFolio"));
		formatob.setTranAmt("000");
		formatob.setReferenc("0");
		formatob.setFeeAmoun("0");
		formatob.setIntern("0");
		formatob.setIntWh("0");
		formatob.setCashIn("000");
		String moneda = gc.getDivisa(param.getString("moneda"));
		formatob.setFromCurr(moneda);

		return formatob;
	}
}
