//*************************************************************************************************
//             Funcion: Bean Txn 5161
//      Creado por: Jesus Emmanuel Lopez Rosales
//                  Yair Jesus Chavez Antonel 
//*************************************************************************************************
// CCN - 4360533 - 20/10/2006 - Se crea bean para proyecto OPEE
//*************************************************************************************************
package ventanilla.com.bital.sfb;

import com.bital.util.ParameterParser;
import com.bital.util.ParameterNotFoundException;
import ventanilla.GenericClasses;

public class Txn_5161 extends Transaction
{
	public Header doAction(ParameterParser param)
		throws ParameterNotFoundException
	{
		FormatoA formatoa = new FormatoA();
		GenericClasses gc = new GenericClasses();
		formatoa.setTxnCode("5161");
		formatoa.setFormat("A");
		formatoa.setAcctNo(param.getString("txtDDACuenta"));
		formatoa.setTranAmt(param.getCString("txtMonto"));		

		String moneda = gc.getDivisa(param.getString("moneda"));
		formatoa.setTranCur(moneda);       
	
		return formatoa;
	}
}

                 