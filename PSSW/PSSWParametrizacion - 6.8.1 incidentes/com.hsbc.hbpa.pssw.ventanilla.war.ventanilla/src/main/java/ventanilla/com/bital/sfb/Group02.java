//*************************************************************************************************
//			 Funcion: Clase para las txns 1005, 1011, 1017, 1119, 1124, 1191, 1234, 1236, 5023, 5148 & 5901
//			Elemento: Group02.java
//		  Creado por: Alejandro Gonzalez Castro
//	  Modificado por: Juvenal Fernandez
//*************************************************************************************************
//CCN -4360178 - 20/08/2004 - se modifica envio de no. de cuenta del cheque
//CCN - 4360268 - 04/02/2005 - Se realizan adecuaciones para eliminar Agencia NY
//*************************************************************************************************

package ventanilla.com.bital.sfb; 

import com.bital.util.ParameterParser;
import com.bital.util.ParameterNotFoundException;
import ventanilla.GenericClasses;

public class Group02 extends Transaction
{
  public Header doAction(ParameterParser param)
    throws ParameterNotFoundException
  {
    // Instanciar el bean a utilizar
    Deposito deposito = new Deposito();
    
    // Ingresar datos requeridos    
    deposito.setAcctNo ( param.getString("txtDDACuenta") );
    deposito.setCashIn ( "000" );
	GenericClasses gc = new GenericClasses();
    /*if(!param.getString("cTxn").equals("0096"))
       deposito.setTranAmt( param.getCString("txtMonto") );
    else{
       deposito.setTranAmt("1");
    }*/
       
    if (param.getString("override") != null)
       if (param.getString("override").equals("SI"))
           deposito.setOverride("3");

    int moneda = param.getInt("moneda");
	deposito.setTranCur( gc.getDivisa(param.getString("moneda")));

    if( param.getString("txtFechaEfect", null) != null )
    {
      String fecha1 = param.getString("txtFechaEfect");
      String fecha2 = param.getString("txtFechaSys");
      fecha1 = fecha1.substring(6) + fecha1.substring(3, 5) + fecha1.substring(0, 2);

      if( !fecha1.equals(fecha2) )
      {
        fecha2 = fecha1.substring(4, 6) + fecha1.substring(6) + fecha1.substring(2, 4);
        deposito.setEffDate(fecha2);
      }
    }

    return deposito;
  }               
}