package ventanilla.com.bital.sfb;

import com.bital.util.ParameterNotFoundException;
import com.bital.util.ParameterParser;
import ventanilla.GenericClasses;

public class Txn_0216
  extends Transaction
{
  public Header doAction(ParameterParser param)
    throws ParameterNotFoundException
  {
    FormatoB2 formatob = new FormatoB2();
    GenericClasses gc = new GenericClasses();
    
    formatob.setTxnCode("0216");
    formatob.setFormat("B");
    formatob.setAcctNo("0216 " + param.getString("txtConceptoMCC"));
    formatob.setTranAmt(param.getCString("txtMonto"));
    formatob.setReferenc(param.getString("txtReferMCC"));
    String moneda = gc.getDivisa(param.getString("moneda"));
    formatob.setFromCurr(moneda);
    if ("1197".equals(param.getString("cTxn"))) {
      formatob.setDesc(param.getString("txtDesc1197"));
    } else {
      formatob.setDesc(param.getString("txtDepMisc"));
    }
    formatob.setCashIn("0");
    formatob.setCashOut("0");
    
    return formatob;
  }
}
