//*************************************************************************************************
//             Funcion: Clase que realiza las consultas del Detalle al Diario
//            Elemento: DiarioDetalle.java
//      Modificado por: Alejandro Gonzalez Castro
//*************************************************************************************************
// CCN - 4360178 - 20/08/2004 - Se habilita guardado en el diario de la txn CON para consultas al Diario
//*************************************************************************************************

package ventanilla.com.bital.admin;

import java.sql.*;
import com.bital.util.ConnectionPoolingManager;
import java.util.*;
import ventanilla.GenericClasses;


public class DiarioDetalle
{
	public Hashtable executeDQuery(String cajero, String suc, String connureg)
	{
		String sql = null;
		String PostFixUR = com.bital.util.ConnectionPoolingManager.getPostFixUR();
		GenericClasses GC = new GenericClasses();
		Hashtable Data = new Hashtable();
		
		Connection pooledConnection = ConnectionPoolingManager.getPooledConnection();
        ResultSet Rselect = null;
        Statement select = null;
        try
        {
            if(pooledConnection != null)
            {
                select = pooledConnection.createStatement();
                sql = "SELECT D_TXN, D_SUPERVISOR, D_FECHOPER, D_CUENTA, D_SERIAL, D_REFER, ";
                sql = sql + "D_REFER1, D_REFER2, D_REFER3, D_EFECTIVO, D_MONTO, ";
                sql = sql + "D_FECHEFEC, D_DIVISA, D_DESCRIP, D_ESTATUS, ";
                sql = sql + "D_CONSECUTIVO, D_CARGO, D_ABONO, D_RESPUESTA ";
                sql = sql + "FROM TW_DIARIO_ELECTRON WHERE D_SUCURSAL = '"+ suc +"' AND D_OPERADOR ='"+cajero+"' AND D_CONNUREG = '"+connureg+"' ";
                sql += PostFixUR;
                Rselect = select.executeQuery(sql);

                //System.out.println("SQL "+ sql);
                

                while(Rselect.next())
                {
                    Data.put("txn", Rselect.getString("D_TXN").trim());
                    Data.put("supervisor", Rselect.getString("D_SUPERVISOR").trim());
                    Data.put("fechaoper", GC.FormatDate(Rselect.getString("D_FECHOPER").trim()));
                    Data.put("cuenta", GC.EmptyString(Rselect.getString("D_CUENTA").trim()));
                    Data.put("serial", GC.EmptyString(Rselect.getString("D_SERIAL").trim()));
                    Data.put("refer", GC.EmptyString(Rselect.getString("D_REFER").trim()));
                    Data.put("refer1", GC.EmptyString(Rselect.getString("D_REFER1").trim()));
                    Data.put("refer2", GC.EmptyString(Rselect.getString("D_REFER2").trim()));
                    Data.put("refer3", GC.EmptyString(Rselect.getString("D_REFER3").trim()));
                    Data.put("efectivo", GC.FormatAmount(Rselect.getString("D_EFECTIVO").trim()));
                    Data.put("monto", GC.FormatAmount(Rselect.getString("D_MONTO").trim()));
                    Data.put("fechaefe", GC.EmptyString(Rselect.getString("D_FECHEFEC").trim()));
                    Data.put("divisa", Rselect.getString("D_DIVISA").trim());
                    Data.put("descripcion", GC.EmptyString(Rselect.getString("D_DESCRIP").trim()));
                    Data.put("estatus", Rselect.getString("D_ESTATUS").trim());
                    Data.put("consecutivo", Rselect.getString("D_CONSECUTIVO").trim());
                    Data.put("cargo", Rselect.getString("D_CARGO").trim());
                    Data.put("abono", Rselect.getString("D_ABONO").trim());
                    Data.put("respuesta", Rselect.getString("D_RESPUESTA"));
                    //System.out.println("HT Servlet" + Data.toString());
                }
            }
        }
        catch(SQLException sqlException)
        {
        	System.out.println("Error DiarioDetalle::executeDQuery [" + sqlException + "]");
            System.out.println("Error DiarioDetalle::executeDQuery::SQL ["+sql+"]");
        }
        finally
        {
            try 
            {
                if(Rselect != null) 
                {
                    Rselect.close();
                    Rselect = null;
                }
                if(select != null) 
                {
                    select.close();
                    select = null;
                }
                if(pooledConnection != null) 
                {
                    pooledConnection.close();
                    pooledConnection = null;
                }
            
            }
            catch(java.sql.SQLException sqlException)
            {
                System.out.println("Error FINALLY DiarioDetalle::executeDQuery [" + sqlException + "]");
                System.out.println("Error FINALLY DiarioDetalle::executeDQuery::SQL ["+sql+"]");
            }
        }
        return Data;
	}

}
