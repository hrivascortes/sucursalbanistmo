package ventanilla.com.bital.sfb;

public class FormatoG extends Header
{
  private String txtService  = "";
  private String txtGStatus  = "";
  private String txtBcoTrnsf = "";
  private String txtComAmt   = "";
  private String txtIVAAmt   = "";
  private String txtCtaBenef = "";
  private String txtBenefic  = "";
  private String txtOrdenan  = "";
  private String txtInstruc  = "";
  private String txtEffDate  = "";
  private String txtAcctNo  = "";
  private String txtTranAmt  = "";
  private String txtToCurr  = "";      
  private String txtCheckNo  = "";
  private String txtReversable = "";  

  public String getEffDate()
  {
	return txtEffDate + "*";
  }
  public void setEffDate(String newEffDate)
  {
	txtEffDate = newEffDate;
  }
  
  public String getAcctNo()
  {
	return txtAcctNo + "*";
  }
  public void setAcctNo(String newAcctNo)
  {
	txtAcctNo = newAcctNo;
  }

  public String getTranAmt()
  {
	return txtTranAmt + "*";
  }
  public void setTranAmt(String newTranAmt)
  {
	txtTranAmt = newTranAmt;
  }
  
  public String getToCurr()
  {
	return txtToCurr + "*";
  }
  public void setToCurr(String newToCurr)
  {
	txtToCurr = newToCurr;
  }
  
  public String getCheckNo()
  {
	return txtCheckNo + "*";
  }
  public void setCheckNo(String newCheckNo)
  {
	txtCheckNo = newCheckNo;
  }

 public void setService(String newService)
  {
        txtService = newService;
  }

  public String getService()
  {
        return txtService + "*";
  }

  public void setGStatus(String newGStatus)
  {
        txtGStatus = newGStatus;
  }

  public String getGStatus()
  {
        return txtGStatus + "*";
  }

  public void setComAmt(String newComAmt)
  {
        txtComAmt = newComAmt;
  }

  public String getComAmt()
  {
        return txtComAmt + "*";
  }
 
  public void setIVAAmt(String newIVAAmt)
  {
        txtIVAAmt = newIVAAmt;
  }

  public String getIVAAmt()
  {
        return txtIVAAmt + "*";
  }

  public void setCtaBenef(String newCtaBenef)
  {
        txtCtaBenef = newCtaBenef;
  }

  public String getCtaBenef()
  {
        return txtCtaBenef + "*";
  }

  public void setBenefic(String newBenefic)
  {
        txtBenefic = newBenefic;
  }

  public String getBenefic()
  {
        return txtBenefic + "*";
  }


  public void setInstruc(String newInstruc)
  {
        txtInstruc = newInstruc;
  }

  public String getInstruc()
  {
        return txtInstruc + "*";
  }       

  public void setBcoTrnsf(String newBcoTrnsf)
  {
        txtBcoTrnsf = newBcoTrnsf;
  }

  public String getBcoTrnsf()
  {
        return txtBcoTrnsf + "*";
  }

  public void setOrdenan(String newOrdenan)
  {
        txtOrdenan = newOrdenan;
  }

  public String getOrdenan()
  {
        return txtOrdenan + "*";
  }  
  
  public String getReversable()
  {
	return txtReversable;
  }    
  
  public void setReversable(String newReverso)
  {
	txtReversable = newReverso;
  }  

  public String toString()
  {
    return super.toString() + getEffDate()  + getService() + getToCurr()
       + getCheckNo()    + getGStatus()  + getBcoTrnsf()+ getTranAmt()
       + getComAmt()     + getIVAAmt()   + getAcctNo()  + getCtaBenef()
       + getBenefic()    + getOrdenan()    + getInstruc();
  }    
}
