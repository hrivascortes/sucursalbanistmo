//************************************************************************************************* 
//             Funcion: Clase de para loguear datos de PLD's
//            Elemento: PLDsLog.java
//          Creado por: Fausto Rodrigo Flores Moreno
//*************************************************************************************************
//*************************************************************************************************

package ventanilla.com.bital.admin;

import java.util.StringTokenizer;
import java.util.Hashtable;
import java.util.ArrayList;
import ventanilla.GenericClasses;
import ventanilla.com.bital.util.DBQueryUM;
import ventanilla.com.bital.util.SQLRefer;
import ventanilla.com.bital.util.Segment;

public class InterfasesLog {
	private GenericClasses gc = new GenericClasses();
	private Hashtable info = null;
	
	/**
	 * @param Info = datos a guardar en el log, la key del hashtable debe ser el nombre de la columna a guargar.
	 */
	public InterfasesLog (Hashtable info){
		this.info = info;
	}
	
	public InterfasesLog (){
	}
	
	/**
	 * @param info = datos a guardar en el log, la key del hashtable debe ser el nombre de la columna a guargar.
	 */
	public void setInfo (Hashtable info){
		this.info = info;
	}
	
	/**
	 * @param msg = mensaje de envio a guardar, metodo usado solo para PLD's
	 */
	public int insertLog(Segment[] msg) {

		StringBuffer mensaje = new StringBuffer(512);	
		String columnas = "(D_SUCURSAL,D_OPERADOR,D_FECHA,D_CONNUREG,D_TXN,D_INTERFASE,D_DIVISA,D_SPFUNC_TXN,D_ETAPA_TXN,D_ESTATUS,D_CONSECLIGA,D_MSGENVIO,D_MOTIVO)";
		
		for(int i=0; i<msg.length; i++){
			mensaje.append(msg[i].getMessage());
		}

		ArrayList values = new ArrayList();
		values.add(new SQLRefer (1,info.get("D_SUCURSAL")));
		values.add(new SQLRefer (1,info.get("D_OPERADOR")));
		values.add(new SQLRefer (1,gc.getDate(1)));
		values.add(new SQLRefer (1,info.get("D_CONNUREG")));
		values.add(new SQLRefer (1,info.get("D_TXN")));
		values.add(new SQLRefer (1,info.get("D_INTERFASE")));
		values.add(new SQLRefer (1,info.get("D_DIVISA")));
		values.add(new SQLRefer (1,info.get("D_SPFUNC_TXN")));
		values.add(new SQLRefer (1,info.get("D_ETAPA_TXN")));
		values.add(new SQLRefer (1,info.get("D_ESTATUS")));
		values.add(new SQLRefer (1,info.get("D_CONSECLIGA")));
		values.add(new SQLRefer (1,mensaje.toString()));
		values.add(new SQLRefer (1,info.get("D_MOTIVO")));
			
		DBQueryUM dbq = new DBQueryUM(1,"INSERT INTO ",columnas," TW_DIARIO_INTERFAZ "," VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?) ","", values);
		 
		return dbq.getRows();
	}
	
	/**
	 * @param msg = mensaje de envio a guardar
	 */
	public int insertLog(String msg) {

		String columnas = "(D_SUCURSAL,D_OPERADOR,D_FECHA,D_CONNUREG,D_TXN,D_INTERFASE,D_DIVISA,D_SPFUNC_TXN,D_ETAPA_TXN,D_ESTATUS,D_CONSECLIGA,D_MSGENVIO,D_MOTIVO)";

		ArrayList values = new ArrayList();
		values.add(new SQLRefer (1,info.get("D_SUCURSAL")));
		values.add(new SQLRefer (1,info.get("D_OPERADOR")));
		values.add(new SQLRefer (1,gc.getDate(1)));
		values.add(new SQLRefer (1,info.get("D_CONNUREG")));
		values.add(new SQLRefer (1,info.get("D_TXN")));
		values.add(new SQLRefer (1,info.get("D_INTERFASE")));
		values.add(new SQLRefer (1,info.get("D_DIVISA")));
		values.add(new SQLRefer (1,info.get("D_SPFUNC_TXN")));
		values.add(new SQLRefer (1,info.get("D_ETAPA_TXN")));
		values.add(new SQLRefer (1,info.get("D_ESTATUS")));
		values.add(new SQLRefer (1,info.get("D_CONSECLIGA")));
		values.add(new SQLRefer (1,msg));	
		values.add(new SQLRefer (1,info.get("D_MOTIVO")));
		
		DBQueryUM dbq = new DBQueryUM(1,"INSERT INTO ",columnas," TW_DIARIO_INTERFAZ "," VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?) ","", values);
		 
		return dbq.getRows();
	}
	
	
	public int insertLog(Object oClase, String metodosGet) {
		
		String msg = createResponse(oClase, metodosGet);

		String columnas = "(D_SUCURSAL,D_OPERADOR,D_FECHA,D_CONNUREG,D_TXN,D_INTERFASE,D_DIVISA,D_SPFUNC_TXN,D_ETAPA_TXN,D_ESTATUS,D_CONSECLIGA,D_MSGENVIO,D_MOTIVO)";

		ArrayList values = new ArrayList();
		System.err.println("info"+info);
		values.add(new SQLRefer (1,info.get("D_SUCURSAL")));
		values.add(new SQLRefer (1,info.get("D_OPERADOR")));
		values.add(new SQLRefer (1,gc.getDate(1)));
		values.add(new SQLRefer (1,info.get("D_CONNUREG")));
		values.add(new SQLRefer (1,info.get("D_TXN")));
		values.add(new SQLRefer (1,info.get("D_INTERFASE")));
		values.add(new SQLRefer (1,info.get("D_DIVISA")));
		values.add(new SQLRefer (1,info.get("D_SPFUNC_TXN")));
		values.add(new SQLRefer (1,info.get("D_ETAPA_TXN")));
		values.add(new SQLRefer (1,info.get("D_ESTATUS")));
		values.add(new SQLRefer (1,info.get("D_CONSECLIGA")));
		values.add(new SQLRefer (1,msg));	
		values.add(new SQLRefer (1,info.get("D_MOTIVO")));
		
		DBQueryUM dbq = new DBQueryUM(1,"INSERT INTO ",columnas," TW_DIARIO_INTERFAZ "," VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?) ","", values);
		 
		return dbq.getRows();
	}
	
	/**
	 * @param oClase = objeto instanciado
	 * @param metodosGet = metodos a invocar de objeto, estrucutra ejemplo: cuenta=getCta&cis=getCIS&
	 * @param Conurreg = conurreg de la txn que hace la invocación
	 * @param Status = si se tiene respuesta de la interfase se coloca R
	 * @param sInterfase = nombre de la interfase o clase que se invoca
	 * @return int = con el numero de registros actualizados
	 */
	public int updateLog(Object oClase, String metodosGet, String connureg, String status, String sInterfase) {
		
		String msg = createResponse(oClase, metodosGet);	
		ArrayList values = new ArrayList();
		values.add(new SQLRefer (1,status));
		values.add(new SQLRefer (1,msg));
		values.add(new SQLRefer (1,connureg));
		values.add(new SQLRefer (1,sInterfase));
		
		DBQueryUM dbq = new DBQueryUM(2,"UPDATE ","","TW_DIARIO_INTERFAZ ", " SET D_ESTATUS = ? , D_RESPUESTA = ? ","WHERE D_CONNUREG = ? AND D_INTERFASE = ? ",values);
		
		return dbq.getRows();
	}

	/**
	 * @param oClase = objeto instanciado
	 * @param metodosGet = metodos a invocar de objeto, estrucutra ejemplo: cuenta=getCta&cis=getCIS&
	 * @param Conurreg = conurreg de la txn que hace la invocación
	 * @param Status = si se tiene respuesta de la interfase se coloca R
	 * @param sInterfase = nombre de la interfase o clase que se invoca
	 * @return int = con el numero de registros actualizados
	 */
	public int updateLog(Object oClase, String metodosGet, String connureg, String status, String motivo, String txn) {
		
		String msg = createResponse(oClase, metodosGet);	
		ArrayList values = new ArrayList();
		values.add(new SQLRefer (1,status));
		values.add(new SQLRefer (1,msg));
		values.add(new SQLRefer (1,connureg));
		values.add(new SQLRefer (1,txn));
		values.add(new SQLRefer (1,motivo));
		
		
		DBQueryUM dbq = new DBQueryUM(2,"UPDATE ","","TW_DIARIO_INTERFAZ ", " SET D_ESTATUS = ? , D_RESPUESTA = ? ","WHERE D_CONNUREG = ? AND D_TXN = ? AND D_MOTIVO = ? ",values);
		
		return dbq.getRows();
	}
	
	
	
	/**
	 * @param Conurreg = conurreg de la txn que hace la invocación
	 * @param etapa =
	 * @param Status = si se tiene respuesta de la interfase se coloca R
	 * @param msgRes = respuesta que regresa la interfase, para el caso de que no se una PLD
	 * @return int = con el numero de registros actualizados
	 */
	public int updateLog(String connureg, String status, String msgRes,String motivo, String txn) {

		ArrayList values = new ArrayList();
		values.add(new SQLRefer (1,status));
		values.add(new SQLRefer (1,msgRes));
		values.add(new SQLRefer (1,connureg));
		values.add(new SQLRefer (1,txn));
		values.add(new SQLRefer (1,motivo));
		
		DBQueryUM dbq = new DBQueryUM(2,"UPDATE ","","TW_DIARIO_INTERFAZ ", " SET D_ESTATUS = ?, D_RESPUESTA = ? ","WHERE D_CONNUREG = ? AND D_TXN = ? AND D_MOTIVO = ?",values);
		return dbq.getRows();
	}
	
	/**
	 * @param Conurreg = conurreg de la txn que original
	 * @return Hastable con la información del registro a buscar
	 */
	public Hashtable getInfoTxn(String connureg, String txn, String motivo){
		Hashtable infoTxn = new Hashtable();
		
		StringBuffer sql = new StringBuffer(256);
		sql.append("SELECT D_INTERFASE,D_SUCURSAL,D_OPERADOR,D_FECHA,D_DIVISA,D_SPFUNC_TXN,D_ETAPA_TXN,D_ESTATUS,D_CONSECLIGA,D_MSGENVIO,D_RESPUESTA  FROM ");
		
		ArrayList values = new ArrayList();
		values.add(new SQLRefer (1,connureg));
		values.add(new SQLRefer (1,txn));
		values.add(new SQLRefer (1,motivo));
		
		DBQueryUM dbq = new DBQueryUM(2,sql.toString(),"TW_DIARIO_INTERFAZ ","WHERE D_CONNUREG = ? AND D_TXN = ? AND D_MOTIVO = ? ",values,"");
		ArrayList regs = dbq.getDatos();
        
        int size = regs.size();
        ArrayList row = null;
        
        for(int i=0; i<size; i++)
        {
            row = (ArrayList)regs.get(i);
            infoTxn.put("D_INTERFASE",row.get(0)); 
            infoTxn.put("D_SUCURSAL",row.get(1));  
            infoTxn.put("D_OPERADOR",row.get(2));  
            infoTxn.put("D_FECHA",row.get(3));     
            infoTxn.put("D_DIVISA",row.get(4));    
            infoTxn.put("D_SPFUNC_TXN",row.get(5));
            infoTxn.put("D_ETAPA_TXN",row.get(6)); 
            infoTxn.put("D_ESTATUS",row.get(7));   
            infoTxn.put("D_CONSECLIGA",row.get(8));
            infoTxn.put("D_MSGENVIO",row.get(9));  
            infoTxn.put("D_RESPUESTA",row.get(10)); 
        }
		return infoTxn;
	}

	
	/**
	 * @param Conurreg = conurreg de la txn que original
	 * @return Hastable con la información del registro a buscar
	 */
	public Hashtable getInfoTxn(String connureg, String interfase){
		Hashtable infoTxn = new Hashtable();
		
		StringBuffer sql = new StringBuffer(256);
		sql.append("SELECT D_TXN,D_MOTIVO,D_SUCURSAL,D_OPERADOR,D_FECHA,D_DIVISA,D_SPFUNC_TXN,D_ETAPA_TXN,D_ESTATUS,D_CONSECLIGA,D_MSGENVIO,D_RESPUESTA  FROM ");
		
		ArrayList values = new ArrayList();
		values.add(new SQLRefer (1,connureg));
		values.add(new SQLRefer (1,interfase));
		
		DBQueryUM dbq = new DBQueryUM(2,sql.toString(),"TW_DIARIO_INTERFAZ ","WHERE D_CONNUREG = ? AND D_INTERFASE = ? ",values,"");
		ArrayList regs = dbq.getDatos();
        
        int size = regs.size();
        ArrayList row = null;
        
        for(int i=0; i<size; i++)
        {
            row = (ArrayList)regs.get(i);
            infoTxn.put("D_TXN",row.get(0));
            infoTxn.put("D_MOTIVO",row.get(1)); 
            infoTxn.put("D_SUCURSAL",row.get(2));  
            infoTxn.put("D_OPERADOR",row.get(3));  
            infoTxn.put("D_FECHA",row.get(4));     
            infoTxn.put("D_DIVISA",row.get(5));    
            infoTxn.put("D_SPFUNC_TXN",row.get(6));
            infoTxn.put("D_ETAPA_TXN",row.get(7)); 
            infoTxn.put("D_ESTATUS",row.get(8));   
            infoTxn.put("D_CONSECLIGA",row.get(9));
            infoTxn.put("D_MSGENVIO",row.get(10));  
            infoTxn.put("D_RESPUESTA",row.get(11)); 
        }
		return infoTxn;
	}
	
	/**
	 * @param obj = objeto instanciado, por lo regualar es un bean con setters y getters
	 * @param method = metodos a invocar de objeto, estrucutra ejemplo: cuenta=getCta&cis=getCIS&, deben ir siempre en pares
	 */
	private String createResponse(Object obj, String method){
		StringBuffer result = new StringBuffer();
		StringTokenizer datos = new StringTokenizer(method,"&");
		while(datos.hasMoreTokens()){
			StringTokenizer values = new StringTokenizer(datos.nextToken().trim(),"=");
			while(values.hasMoreTokens()){
				result.append(values.nextToken())
					  .append("=")
					  .append(gc.getBeanData(obj, values.nextToken()));
			}
			result.append("|");
		}
		
		return result.toString();
	}
	
}