// Transaccion 0065 - Cheques de Caja
package ventanilla.com.bital.sfb;

import com.bital.util.ParameterParser;
import com.bital.util.ParameterNotFoundException;
import ventanilla.GenericClasses;

public class Group32B extends Transaction
{
  public Header doAction(ParameterParser param)
    throws ParameterNotFoundException
  {
    // Instanciar el bean a utilizar
    Cheques cheque = new Cheques();
    
    // Ingresar datos requeridos    
    cheque.setTxnCode("0065");
    cheque.setTranAmt( param.getCString("txtMonto") );
    cheque.setCheckNo( param.getString("txtSerial") );
    cheque.setAcctNo2( param.getString("txtDDACuenta") );
	GenericClasses gc = new GenericClasses();
    
    int moneda = param.getInt("moneda");
	cheque.setTranCur( gc.getDivisa(param.getString("moneda")));
    if( moneda == 1 )
    {
      cheque.setAcctNo1("0103000023");
    }
    else if( moneda == 2 )
    {
      cheque.setAcctNo1("3902222222");
    }

    if( param.getString("cTxn").equals("S265") )
      cheque.setTrNo("1");
    else
      cheque.setTrNo("3");
    
    return cheque;
  }               
}
