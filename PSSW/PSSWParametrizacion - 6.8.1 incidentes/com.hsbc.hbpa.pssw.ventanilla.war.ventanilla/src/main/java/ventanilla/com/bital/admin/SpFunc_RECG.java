//*************************************************************************************************
//          Funcion: Clase que actualiza el status de Cheque de gerencia (Status 2 impreso)
//          Elemento: SpFunc_CGER.java
//          Creado por: YGX
//          Modificado por: Jesus Emmanuel Lopez Rosales
//*************************************************************************************************


package ventanilla.com.bital.admin;

/**
 * @author YGX 
 * Fecha:05/10/07
 * Clase qye inserta los datos de las solicitudes de Cheque de Gerencia
 */

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.Hashtable;
import java.util.StringTokenizer;
import java.util.Vector;
import javax.servlet.http.HttpSession;
import ventanilla.GenericClasses;
import com.bital.util.ConnectionPoolingManager;
//import ventanilla.com.bital.sfb.Txn_4011;


public class SpFunc_RECG implements InterfazSF{
	
		
	public SpFunc_RECG(){	}
	
	
	public int SpFuncionBD(Object bean, Vector Respuesta, HttpSession session, String Connureg){
		int messagecode = 0;
		return messagecode;
	}

	public int SpFuncionAD(Object bean, Vector Respuesta, HttpSession session, String Connureg){
		int messagecode = 0;
		GenericClasses GC = new GenericClasses();						
		Hashtable datasession = (Hashtable)session.getAttribute("page.datasession");		
		Hashtable usrinfo = (Hashtable)session.getAttribute("userinfo");
		
		//Campos llave para actualizar la tabla de tw_chq_ger
		String llave = (String)datasession.get("val_datos");
	    //System.out.println(" Txn_RECG " + llave );
	    String datatmp="";	    
	    String datatmp1="";
	    String datatmp2="";
	    String datatmp3="";
	    String datatmp4="";
	    String datatmp5="";
	    String datatmp6="";
	    String datatmp7="";  
	    String datatmp8="";
	    String datatmp9="";

	   StringTokenizer tok = new StringTokenizer(llave,"~");             
	   while(tok.hasMoreTokens()) 
	   { 
	     datatmp  = tok.nextToken();
	     datatmp1 = tok.nextToken();
	     datatmp2 = tok.nextToken();
	     datatmp3 = tok.nextToken();
	     datatmp4 = tok.nextToken();
	     datatmp5 = tok.nextToken();
	     datatmp6 = tok.nextToken();
	     datatmp7 = tok.nextToken();
	     datatmp8 = tok.nextToken();
	     datatmp9 = tok.nextToken();
	   }
	    
		String sucDestino = datatmp8;
		String fecha_sol  = datatmp2;
		String concarga   = datatmp7;
						
		//Cambio de Status del Cheque
		Connection pooledConnection = ConnectionPoolingManager.getPooledConnection();
		PreparedStatement update = null;
		String statementPS = com.bital.util.ConnectionPoolingManager.getStatementPostFix();
        
        	int ResultUpdate = 0;
	        try{
	        	System.out.println("SpFunc_RECG:UpdateReacCheque::");			
		 		pooledConnection = ConnectionPoolingManager.getPooledConnection();
		 		update = pooledConnection.prepareStatement("UPDATE TW_CHQGER SET D_STATUS=? WHERE C_FECHA_SOL=? AND D_SUC_EXP=? AND D_CONNUREG=?" + statementPS);
			    update.setString(1, "1");			   
			    update.setString(2, fecha_sol);
			    update.setString(3, sucDestino);
			    update.setString(4, concarga);
			    ResultUpdate= update.executeUpdate();
			    //if(ResultUpdate > 0){
				//	System.out.println("SPFunc_RECG::UpdateReacChequeDep::Se actualiz� ["+ResultUpdate+"] registro en la tabla TW_CHQGER");
				//	System.out.println("SPFunc_RECG::UpdateReacChequeDep::Se actualiz� el status a \"1\"");
				//}
		 	}catch(Exception e){
		 		e.printStackTrace();
		 		//System.out.println(e.getMessage());
		 			 		
		 	}finally {		 			
	    		try{
	    			if(update != null) {
	            		update.close();
	                	update = null;
	            	}
	            	if(pooledConnection != null) {
	                	pooledConnection.close();
	                	pooledConnection = null;
	            	}
	        	}catch(java.sql.SQLException sqlException){
	        		
	        		sqlException.printStackTrace();
	            	//System.out.println("Error FINALLY SPFunc_RECG::UpdateLog [" + sqlException + "]");	            	
              		
	        	}
		 	}	
		 return messagecode;			
	}
	
	public int SpFuncionBR(Hashtable txnRev, Hashtable txnOrg, HttpSession session, String Connureg, String Supervisor){
		int messagecode = 0;
		return messagecode;
	}
	
	public int SpFuncionAR(Hashtable txnRev, Hashtable txnOrg, HttpSession session, String Connureg, String Cosecutivo, String Supervisor, Vector VResp){
		int messagecode = 0;
		return messagecode;
	}
		
}

