//*************************************************************************************************
//             Funcion: Bean Txn 0089
//      Elaborado por: Humberto Enrique Balleza Mej�a
//*************************************************************************************************
// CCN - 4620021 - 17/10/2007
//*************************************************************************************************
package ventanilla.com.bital.sfb;

import ventanilla.GenericClasses;
import com.bital.util.ParameterNotFoundException;
import com.bital.util.ParameterParser;


public class Txn_0089 extends Transaction
{		 
	public Header doAction(ParameterParser param)
		throws ParameterNotFoundException
	{				
        GenericClasses gc = new GenericClasses();                				
		ServiciosA servicioA = new ServiciosA();
		servicioA.setBranch(param.getCString("sucursal"));
		servicioA.setTeller(param.getCString("teller"));
		servicioA.setSupervisor(param.getCString("teller"));
		servicioA.setTxnCode("0089");
		servicioA.setAcctNo(param.getCString("txtCtaSIB"));
		servicioA.setTranCur(gc.getDivisa(param.getCString("moneda")));
		servicioA.setCashIn("000");
		return servicioA;
	}
}
