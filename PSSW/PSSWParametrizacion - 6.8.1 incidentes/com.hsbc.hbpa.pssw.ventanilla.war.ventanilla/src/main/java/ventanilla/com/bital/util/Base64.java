package ventanilla.com.bital.util;

public class Base64 
{ 
	private char[] map = 
	{
		'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H',
		'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P',
		'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X',
		'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f',
		'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n',
		'o', 'p', 'q', 'r', 's', 't', 'u', 'v',
		'w', 'x', 'y', 'z', '0', '1', '2', '3',
		'4', '5', '6', '7', '8', '9', '+', '/'
	};


	public String encode(byte[] ba) 
	{
		int sz = ba.length;
		StringBuffer sb = new StringBuffer();
		int j;
		int i;
		for (i = 0, j = 0; i < (sz -2); i += 3) 
		{
			sb.append(map[(ba[i] >>> 2) & 0x3F]);
      	sb.append(map[((ba[i] << 4) & 0x30) + ((ba[i+1] >>> 4) & 0xf)]);
	      sb.append(map[((ba[i+1] << 2) & 0x3c) + ((ba[i+2] >>> 6) & 0x3)]);
   	   sb.append(map[ba[i+2] & 0X3F]);
		}
		switch (sz - i) 
		{
			case 0:
				break;
			case 1:
				sb.append(map[(ba[i] >>> 2) & 0x3F]);
				sb.append(map[((ba[i] << 4) & 0x30)]);
				sb.append('='); 
				sb.append('=');
				break;
			case 2:
				sb.append(map[(ba[i] >>> 2) & 0x3F]);
				sb.append(map[((ba[i] << 4) & 0x30) + ((ba[i+1] >>> 4) & 0xf)]);
				sb.append(map[((ba[i+1] << 2) & 0x3c)]);
				sb.append('=');
				break;
		}
		return sb.toString();
	}

	public byte[] decode(String s) 
	{
		int nb;
		byte[] bo = new byte[s.length()];
		int boi = 0;
		byte [] b = s.getBytes();
		byte b1 = (byte)-1, b2 = (byte)-1, b3 = (byte)-1, b4 = (byte)-1;
		for (int i = 0; i < s.length(); i += 4) 
		{
			for (int j = 0; j < 64; ++j) 
			{
				if (b[i] == map[j])
					b1 = (byte)j;
				if (b[i+1] == map[j])
					b2 = (byte)j;
				if (b[i+2] == map[j])
					b3 = (byte)j;
				if (b[i+3] == map[j])
					b4 = (byte)j;
			}
			if ((b1 <0) || (b2 < 0) )
				System.out.println("Descodificacion  Caracter Invalido. ");
			nb = 3;
			if (b[i+3] == '=')
				nb = 2;
			if (b[i+2] == '=')
				nb = 1;
			switch (nb) 
			{
				case 1:
					bo[boi++] = (byte)((b1 << 2) | (b2 >>> 4));
					break;
				case 2:
					bo[boi++] = (byte)((b1 << 2) | (b2 >>> 4));
					bo[boi++] = (byte)((b2 << 4) | (b3 >>> 2));
					break;
				case 3:
					bo[boi++] = (byte)((b1 << 2) | (b2 >>> 4));
					bo[boi++] = (byte)((b2 << 4) | (b3 >>> 2));
					bo[boi++] = (byte)((b3 << 6) | b4);
					break;
			}
		}
		byte[] br = new byte[boi];
		for (int i = 0; i < boi; ++i)
			br[i] = bo[i];
		return br;
	}  
}
