//*************************************************************************************************
//             Funcion: Bean para Posteo de TDI
//          Creado por: Rutilo Zarco Reyes
//      Modificado por: Fausto Rodrigo Flores Moreno
//*************************************************************************************************
// CCN - 4360446 - 10/03/2006 - Proyecto TDI fase 2 cobro de comision
// CCN - 4360492 - 23/06/2006 - Se realizan modificaciones para el esquema de Cobro Comisi�n Usuarios RAP
// CCN - 4360567 - 02/02/2007 - Se realiza correcci�n de iva con el mismo monto que la comisi�n
//*************************************************************************************************

package ventanilla.com.bital.admin;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Hashtable;
import java.util.StringTokenizer;
import java.util.Vector;

import javax.servlet.http.HttpSession;

import ventanilla.GenericClasses;
import ventanilla.com.bital.sfb.RAPB;
import ventanilla.com.bital.sfb.RAPE;
import ventanilla.com.bital.sfb.ServiciosB;

import com.bital.util.ConnectionPoolingManager;

public class PostingTDI {

  public PostingTDI()
  {}
  public String codigo = null;
  public String tipo = "";
  public void execute(HttpSession session)
  {
    Hashtable datasession = (Hashtable)session.getAttribute("page.datasession");
    int NumTxnFre = Integer.parseInt((String)datasession.get("NumTxnFre"));
    Vector txntdi = (Vector)datasession.get("Txn.Tdi");
    Vector datatdi = (Vector)datasession.get("DataxTxn.Tdi");
    String cajero = (String)datasession.get("teller");
    String moneda = (String)datasession.get("moneda");
    String conliga = (String)session.getAttribute("d_ConsecLiga");
    String dirip = (String)session.getAttribute("dirip");
    String sucursal = (String)datasession.get("sucursal");
    String supervisor = (String)datasession.get("supervisor");
    clearcertif(cajero);
    String txtCasoEsp = "";
    Vector bines = new Vector();
   boolean flagCobCom = false;
    int consecrap = 0;
    for (int i=0; i<NumTxnFre; i++) //Transacciones Frecuentes
    {
      String accion = (String)datasession.get("accion" + (i+1));
      if (accion != null) //Transacciones seleccionadas
      {
        Vector fields = (Vector)txntdi.get(i);
        Hashtable info = (Hashtable)datatdi.get(i);
        String comision = clearmto((String)info.get("Monto.Comision"));
        String iva = clearmto((String)info.get("Monto.Iva"));
        String CobraComision = (String)info.get("Cobra.Comision");
        String TipoCom = (String)info.get("ComServRAP");
        if (info.get("pcdok") != null)
          tipo = (String)info.get("pcdok");
      	String monto = clearmto((String)datasession.get("monto" + (i+1)));
      	String txn = (String)fields.get(3);
      	String servicio = (String)fields.get(4);
      	servicio = servicio.trim();
      	String refer1 = (String)fields.get(6);
      	refer1 = refer1.trim();
      	String refer2 = (String)fields.get(7);
      	refer2 = refer2.trim();
      	String refer3 = (String)fields.get(8);
      	refer3 = refer3.trim();
        //arma txn
        if (txn.equals("5503"))
        {
          consecrap++;
          txtCasoEsp = txtCasoEsp + execute5503(sucursal, cajero, supervisor, txn, servicio, monto, refer1,refer2,refer3, moneda, conliga, dirip, i, session);
          if (CobraComision.equals("S") && tipo.equals("2") && codigo.equals("0"))
          {
 
			if(TipoCom.equals("0")){
            txn = "0736";
            txtCasoEsp = txtCasoEsp + execomiva(sucursal, cajero, supervisor, txn, comision, servicio, moneda, conliga, dirip, consecrap, session);
	            flagCobCom=true;
             }else{
 	            txn = "0792";
		        txtCasoEsp = txtCasoEsp + execomiva(sucursal, cajero, supervisor, txn, comision, servicio, moneda, conliga, dirip, consecrap, session);
		        flagCobCom=true;
             }
             if(flagCobCom){
            txn = "0372";
    	        txtCasoEsp = txtCasoEsp + execomiva(sucursal, cajero, supervisor, txn, iva, servicio, moneda, conliga, dirip, consecrap, session);
          	 }
          }
          try{Thread.sleep( 1500 );}
          catch (InterruptedException e){}
        }
        else //if (txn.equals("0728") || txn.equals("0730"))
        {
           Vector txns = new Vector();
           txns.add(txn);
           if (CobraComision.equals("S") && tipo.equals("2"))
           {
             txns.add("0360");
             txns.add("0372");
           }
           for (int x=0; x<txns.size(); x++)
           {
             ServiciosB serviciob = new ServiciosB();
             serviciob.setBranch(sucursal);
             serviciob.setTeller(cajero);
             serviciob.setSupervisor(cajero);
             serviciob.setFormat("B");
             serviciob.setAcctNo(servicio);
             txn = (String)txns.get(x);
             if (txn.equals("0728"))
             {
             	String refer = refer1.substring(0,10);
             	String dv = refer1.substring(refer1.length()-2, refer1.length()-1);
//System.out.println("digito :" + dv);
             	String mto = monto;
             	for (int xx=mto.length(); xx<9; xx++)
             	  mto = "0" + mto;
             	String dvt = DigitoTelmex(refer+ mto);
//System.out.println("digitot:" + dvt);
             	refer1 = refer+mto+dvt;
                String valor = telmex(refer1);
                refer1 = refer1 + valor;
                serviciob.setReferenc(refer1);
             }
             if (txn.equals("0730"))
				serviciob.setReferenc(refer1);
             serviciob.setTxnCode(txn);
             if (txn.equals("0360") || txn.equals("0372")) 
             {
               serviciob.setReferenc(servicio);
               serviciob.setAcctNo(txn);
               if (txn.equals("0360")) // Comision
               {
                 serviciob.setTranAmt(comision);
                 serviciob.setCashIn(comision);
               }
               else
               {
                 serviciob.setTranAmt(iva);
                 serviciob.setCashIn(iva);
               }
             } 
             else
             {
               serviciob.setTranAmt(monto);
               serviciob.setCashIn(monto);
             }
             serviciob.setFromCurr("N$");
//System.err.println(serviciob.toString());           
             Diario diario = new Diario();
             String sMessage = diario.invokeDiario(serviciob, conliga, dirip, moneda, session);
             if (x==0)
             {
               codigo = sMessage.substring(0,1);
               if (!codigo.equals("0"))
               {
               	txns.remove(x+1);
               	txns.remove(x+2);
               }
             }
             txtCasoEsp = txtCasoEsp + txn + "~" + getString(sMessage,1) + "~";
             if (txn.equals("0372"))
             {
              try{Thread.sleep( 1500 );}
              catch (InterruptedException e){}
             }
           }
        }
      }
    }
    datasession.put("NumRaps",Integer.toString(consecrap));
    if(txtCasoEsp.length() > 0)
       session.setAttribute("txtCasoEsp", txtCasoEsp);
  }

  private String execomiva(String sucursal, String cajero, String supervisor, String txn, String monto, String servicio, String moneda, String conliga, String dirip, int consecrap, HttpSession session)
  {
          RAPB rapb = new RAPB();
	      GenericClasses gc = new GenericClasses();
          rapb.setFormat("B");
          rapb.setBranch(sucursal);
          rapb.setTeller(cajero);
          rapb.setSupervisor(supervisor);
          rapb.setTxnCode(txn);
          rapb.setAcctNo(txn);
          String cuenta = txn;
          rapb.setTranAmt(monto);
          rapb.setReferenc(servicio);
          rapb.setCashIn(monto);
          String refs = "&&&";
          rapb.setFromCurr(gc.getDivisa(moneda));
          Diario diario = new Diario();
          String sMessage = diario.invokeDiario(rapb, conliga, dirip, moneda, session);
          codigo = sMessage.substring(0,1);
   //       certif(sMessage, cajero, cuenta, monto, monto, txn, moneda, consecrap, refs, codigo);
          return txn + "~" + getString(sMessage,1) + "~";
  }
  private String execute5503(String sucursal, String cajero, String supervisor, String txn, String servicio, String monto, String refer1,String refer2,String refer3, String moneda, String conliga, String dirip, int consecrap, HttpSession session)
  {
          RAPE rape = new RAPE();
		  GenericClasses gc = new GenericClasses();
          rape.setFormat("A");
          rape.setBranch(sucursal);
          rape.setTeller(cajero);
          rape.setSupervisor(supervisor);
          rape.setTxnCode(txn);
          rape.setAcctNo(servicio);
          String cuenta = servicio;
          rape.setTranAmt(monto);
          rape.setCashIn(monto);
          rape.setReferenc1(refer1);
          rape.setReferenc2(refer2);
          rape.setReferenc3(refer3);
          if(servicio.equals("4254"))
			rape.setTrNo("01");
          String refs = refer1 + "&" + refer2 + "&" + refer3 + "&";
          rape.setFromCurr(gc.getDivisa(moneda));
          Diario diario = new Diario();
          String sMessage = diario.invokeDiario(rape, conliga, dirip, moneda, session);
          String respuesta = sMessage;
          codigo = respuesta.substring(0,1);
          String beneficiario = null;
          Hashtable datasession = (Hashtable)session.getAttribute("page.datasession");          
          if(codigo.equals("0") && respuesta.length() > 234)
          {
                beneficiario = respuesta.substring(235,respuesta.length()-1).trim();
                int y = 0;
                int count = 0;
                StringBuffer Ben = new StringBuffer(beneficiario);
                for(y = 0; y < Ben.length(); y++)
                    if(Ben.charAt(y) == 0x26) 
                    {
                        count ++;
                        Ben.replace(y,y+1,"%26");
                    }
                beneficiario = Ben.toString();
                datasession.put("beneficiario"+consecrap,beneficiario);
           }
           else
                beneficiario = "";
            if(tipo.equals("2"))
                datasession.put("comppago"+consecrap,Integer.toString(consecrap));
//          certif(respuesta, cajero, cuenta, monto, monto, txn, moneda, consecrap, refs, codigo);
          return txn + "~" + getString(respuesta,1) + "~";
  }
    private void clearcertif(String cajero)
    {
        String statementPS = ConnectionPoolingManager.getStatementPostFix();
        Connection pooledConnection = ConnectionPoolingManager.getPooledConnection();
        PreparedStatement delete = null;
        try
        {
            if(pooledConnection != null)
            {
                delete = pooledConnection.prepareStatement("DELETE FROM TW_CERTIFICACIONES WHERE D_CAJERO=?" + statementPS);
                delete.setString(1,cajero);
                int i = delete.executeUpdate();
            }
        }
        catch(SQLException sqlException)
            {System.out.println("Error GroupRAP::clearcertif [" + sqlException + "]");}
        finally
        {
            try
            {
                if(delete != null)
                {
                    delete.close();
                    delete = null;
                }
                if(pooledConnection != null)
                {
                    pooledConnection.close();
                    pooledConnection = null;
                }
            }
            catch(java.sql.SQLException sqlException){System.out.println("Error FINALLY GroupRAP::clearcertif [" + sqlException + "]");}
        }
    }

    private String clearmto(String mto)
    {
        StringBuffer montotmp = new StringBuffer(mto);
        for(int k=0; k<montotmp.length();) {
            if(montotmp.charAt(k) == ',' || montotmp.charAt(k) == '.')
                montotmp.deleteCharAt(k);
            else
                ++k;
        }
        mto = montotmp.toString();
        return mto;
    }
    
    private void certif(String nrespuesta, String Teller, String Cuenta, String Amount, String Efectivo, String TXN, String Moneda, int Consecrap, String Refs, String Codigo)
    {
        StringTokenizer resp = new StringTokenizer(nrespuesta, "~");
        String respuesta = "";
        while (resp.hasMoreElements()) {
            respuesta = respuesta + resp.nextElement().toString();
        }

        int lineas = Integer.parseInt(respuesta.substring(1,3));
        String consec = respuesta.substring(3,10);
        respuesta = respuesta.substring(10,respuesta.length());
		GenericClasses gc = new GenericClasses();
		Moneda = gc.getDivisa( Moneda);
        Connection pooledConnection = ConnectionPoolingManager.getPooledConnection();
        Statement insert = null;
        String sql ="";
        try
        {
            if(pooledConnection != null)
            {
                int j = 0;
                for(int k=0; k<lineas; k++)
                {
                    insert = pooledConnection.createStatement();
                    String lresp = respuesta.substring(j,Math.min(j+78, respuesta.length()));
                    String codresp = "";
                    if(Codigo.equals("0"))
                        codresp = "A";
                    else
                        codresp = "R";

                    sql = "INSERT INTO TW_CERTIFICACIONES VALUES('"+Teller+
                    "','"+codresp+Cuenta+"',"+Amount+","+Efectivo+",'"+TXN+"','"+Moneda+"',"+Consecrap+","+
                    (k+1)+",'"+lresp.trim()+"','"+consec+"','"+Refs+"')";
                    int code = 0;
                    code = insert.executeUpdate(sql);
                    j = j+78;
                    if(insert != null){
                        insert.close();
                        insert = null;
                    }
                }
            }
        }
        catch(SQLException sqlException)
        {
            System.out.println("Error GroupRAP::certif [" + sqlException + "]");
            System.out.println("Error GroupRAP::certif::SQL [" + sql + "]");
        }
        finally{
            try{
                if(insert != null){
                    insert.close();
                    insert = null;
                }
                if(pooledConnection != null){
                    pooledConnection.close();
                    pooledConnection = null;
                }
            }
            catch(java.sql.SQLException sqlException)
            {
                System.out.println("Error FINALLY GroupRAP::certif [" + sqlException + "]");
                System.out.println("Error FINALLY GroupRAP::certif::SQL [" + sql + "]");
            }
        }
    }
    private String getString(String newCadNum, int NumSaltos) {
        int iPIndex = newCadNum.indexOf("CONSECUTIVO:") + 12, Num = 0, Ban = 0;
        String nCadNum = new String("");
        while(Num < NumSaltos) {
            if(newCadNum.charAt(iPIndex) == 0x20) {
                while(newCadNum.charAt(iPIndex) == 0x20)
                    iPIndex++;
                Num++;
            }
            else
                while(newCadNum.charAt(iPIndex) != 0x20)
                    iPIndex++;
        }
        while(newCadNum.charAt(iPIndex) != 0x20) {
            nCadNum = nCadNum + newCadNum.charAt(iPIndex++);
            if(newCadNum.charAt(iPIndex) == 0x20 && newCadNum.charAt(iPIndex + 1) == 0x2A) {
                nCadNum = nCadNum + newCadNum.charAt(iPIndex) + newCadNum.charAt(iPIndex + 1);
                iPIndex += 2;
            }
        }
        return nCadNum;
    }
    private String telmex(String ref) {
        long valida = Long.parseLong(ref.substring(0,3));
        String valores ="";
        int factor;
        if (valida == 00) {
            factor = 3;
            valores = ref.substring(2,10);
        }
        else {
            factor = 1;
            valores = ref.substring(0,10);
        }
        long suma = 0;
        long num  = 0;
        for (int i=7; i>=0;) {
            num = Long.parseLong(valores.substring(i,i+1));
            if (num < 0 || num > 9) {
                break;
            }
            num = num * factor;
            suma = suma + num;
            switch (factor) {
                case 3: factor = 1; break;
                case 1: factor = 7; break;
                case 7: factor = 3; break;
            }
            i--;
        }
        suma = suma + 49;
        long res = suma % 9;
        res++;
        return Long.toString(res);
    }
	public String DigitoTelmex(String tel)
	{
//System.out.println("tel -" + tel + "-");
	  int total = 0;
	  int factor = 1;
	  int num = 0;
	  for(int vi=tel.length(); vi>0; vi--)
	  {
	  	num = Integer.parseInt(tel.substring(vi-1,vi));
	  	num = num * factor;
	  	total = num + total;
	  	switch(factor)
		{
	  	  case 1:
	  	  	factor = 7;
	  	  	break;
	  	  case 7:
	  	  	factor = 3;
	  	  	break;
	  	  case 3:
	  	  	factor = 1;
	  	  	break;
	  	}
	  }
      int result = total % 9;
	  result ++;
	  return Integer.toString(result);
	}

}


