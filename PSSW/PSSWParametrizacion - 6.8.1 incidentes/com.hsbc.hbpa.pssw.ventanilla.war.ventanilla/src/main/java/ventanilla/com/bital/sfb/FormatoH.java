package ventanilla.com.bital.sfb;


public class FormatoH extends Header {
  
  private String txtEffDate  = ""; 
  private String txtAcctNo   = ""; 
  private String txtNatCurr  = ""; 
  private String txtTranDesc = ""; 
  private String txtAcctDeb = ""; 
  private String txtTranAmt  = ""; 
  private String txtTranCur  = "";	
  private String txtCheckNo  = ""; 
  private String txtCashIn   = ""; 
  private String txtCashOut  = ""; 
  private String txtDraftAm  = ""; 
  private String txtFees     = ""; 
  private String txtDescRev  = "";
  
  public String getEffDate()
  {
	return txtEffDate + "*";
  }
  public void setEffDate(String newEffDate)
  {
	txtEffDate = newEffDate;
  }
  
  public String getAcctNo()
  {
	return txtAcctNo + "*";
  }
  public void setAcctNo(String newAcctNo)
  {
	txtAcctNo = newAcctNo;
  }

  public String getNatCurr()
  {
	return txtNatCurr + "*";
  }
  public void setNatCurr(String newNatCurr)
  {
	txtNatCurr = newNatCurr;
  }

  public String getTranAmt()
  {
	return txtTranAmt + "*";
  }
  public void setTranAmt(String newTranAmt)
  {
	txtTranAmt = newTranAmt;
  }
  
  public String getTranCur()
  {
	return txtTranCur + "*";
  }
  public void setTranCur(String newTranCur)
  {
	txtTranCur = newTranCur;
  }
  
  public String getCheckNo()
  {
	return txtCheckNo + "*";
  }
  public void setCheckNo(String newCheckNo)
  {
	txtCheckNo = newCheckNo;
  }
  
  public String getCashIn()
  {
	return txtCashIn + "*";
  }
  public void setCashIn(String newCashIn)
  {
	txtCashIn = newCashIn;
  }
  
  public String getCashOut()
  {
	return txtCashOut + "*";
  }
  public void setCashOut(String newCashOut)
  {
	txtCashOut = newCashOut;
  }
  
  public String getDraftAm()
  {
	return txtDraftAm + "*";
  }
  public void setDraftAm(String newDraftAm)
  {
	txtDraftAm = newDraftAm;
  }

  public String getFees()
  {
	return txtFees + "*";
  }
  public void setFees(String newFees)
  {
	txtFees = newFees;
  }
  
  public String getTranDesc()
  {
	return txtTranDesc + "*";
  }
  public void setTranDesc(String newTranDesc)
  {
	txtTranDesc = newTranDesc;
  }
  
  public String getAcctDeb()
  {
    return txtAcctDeb + '*';
  }  
  
  public void setAcctDeb(String newAcctDeb)
  {
     txtAcctDeb = newAcctDeb;
  }
  
   public String getDescRev()
  {
	return txtDescRev;
  }    
  
  public void setDescRev(String newDescRev)
  {
	txtDescRev = newDescRev;
  }
  
   public String toString()
  {
	return super.toString()
	+ getEffDate()	+ getAcctNo()	+ getNatCurr()	+ getTranDesc()	
	+ getAcctDeb()  + getTranAmt()	+ getTranCur()	+ getCheckNo()	
	+ getCashIn()	+ getCashOut()	+ getDraftAm()	+ getFees();
  }  
  
}
