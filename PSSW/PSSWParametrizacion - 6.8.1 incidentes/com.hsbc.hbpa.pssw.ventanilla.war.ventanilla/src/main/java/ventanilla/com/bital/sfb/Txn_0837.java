//*************************************************************************************************
//             Funcion: Bean Txn 0837
//          Creado por: Liliana Neyra Salazar
//*************************************************************************************************
// CCN - 4360602 - 18/05/2007 - Se realizan modificaciones para proyecto Codigo de Barras SUAS
//*************************************************************************************************
package ventanilla.com.bital.sfb;

import com.bital.util.ParameterParser;
import com.bital.util.ParameterNotFoundException;
import ventanilla.GenericClasses;

public class Txn_0837 extends Transaction
{
	public Header doAction(ParameterParser param)
		throws ParameterNotFoundException
	{
		FormatoB formatob = new FormatoB();
		GenericClasses gc = new GenericClasses();
		formatob.setTxnCode("0837");
		formatob.setFormat("B");
		formatob.setAcctNo(param.getString("txtRFCSUA").substring(0,11));
		formatob.setTranAmt("1");
		formatob.setReferenc("0772");
		String moneda = gc.getDivisa(param.getString("moneda"));
		formatob.setFromCurr(moneda);

		return formatob;
	}
}
