//*************************************************************************************************
//             Funcion: Bean Txn 0814
//      Creado por: Jesus Emmanuel Lopez Rosales
//*************************************************************************************************
// CCN - 4360576 - 09/03/2007 - Modificaciones para proyecto OPMN
//*************************************************************************************************
package ventanilla.com.bital.sfb;

import com.bital.util.ParameterParser;
import com.bital.util.ParameterNotFoundException;

public class Txn_0814 extends Transaction
{
	public Header doAction(ParameterParser param)
		throws ParameterNotFoundException
	{
		FormatoB formatob = new FormatoB();
        formatob.setFormat("B");
        formatob.setAcctNo(param.getString("plaza"));
        formatob.setTranAmt(param.getString("txtMonto"));
        formatob.setReferenc("1");   //Requerido por ODS
        formatob.setFeeAmoun("0");
        formatob.setIntern("0");
        formatob.setIntWh("0");
        formatob.setCashIn("000");
        formatob.setFromCurr("N$");
		return formatob;
	}
}
