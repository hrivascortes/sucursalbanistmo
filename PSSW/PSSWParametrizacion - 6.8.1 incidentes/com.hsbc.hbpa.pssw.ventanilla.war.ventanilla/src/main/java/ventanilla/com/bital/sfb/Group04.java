// Transaccion 1029
package ventanilla.com.bital.sfb;

import com.bital.util.ParameterParser;
import com.bital.util.ParameterNotFoundException;

public class Group04 extends Transaction
{
  public Header doAction(ParameterParser param)
    throws ParameterNotFoundException
  {
    // Instanciar el bean a utilizar
    Deposito deposito = new Deposito();

    // Ingresar datos requeridos
    deposito.setFormat("F");
    deposito.setAcctNo( param.getString("txtDDACuenta") );
    deposito.setCashIn ( "000" );
    //deposito.setTranAmt(unformatCur((String)params.get("txtMonto")));

    int amount = Integer.parseInt(param.getCString("txtMonto"));
    amount -= Integer.parseInt(param.getCString("txtMontod"));
    deposito.setTranAmt( Integer.toString(amount) );

    int moneda = param.getInt("moneda");
    if( moneda == 1 )
      deposito.setTranCur("N$");
    else if( moneda == 2 )
      deposito.setTranCur("US$");
    else
      deposito.setTranCur("UDI");

    deposito.setHoldays (param.getString("txtDiasSbc"));
    deposito.setAmount3 (param.getCString("txtMonto"));
    deposito.setCheckNo3(param.getString("txtNumDocv"));
    deposito.setTrNo3   (param.getString("txtNumDocd"));
    deposito.setAmount4 (param.getCString("txtMontod"));
    deposito.setCheckNo4(param.getCString("txtIva"));
    deposito.setAmount5 (param.getCString("txtComision"));
    deposito.setCheckNo5(param.getString("txtNegocio"));

    return deposito;
  }
}
