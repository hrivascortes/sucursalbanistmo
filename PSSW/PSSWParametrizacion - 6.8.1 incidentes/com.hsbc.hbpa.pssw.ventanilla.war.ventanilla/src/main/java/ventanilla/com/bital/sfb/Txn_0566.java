//*************************************************************************************************
//             Funcion: Bean Txn 0566
//      Elaborado por: Jesus Emmanuel Lopez Rosales
//*************************************************************************************************
// CCN -  - 12/01/07 - Se agrega para txn 0021
// CCN - 4360576 - 09/03/2007 - Modificaciones para proyecto OPMN
// CCN - 4360584 - 23/03/2007 - Modificaciones para proyecto OPMN
//*************************************************************************************************
package ventanilla.com.bital.sfb;

import ventanilla.GenericClasses;
import com.bital.util.ParameterNotFoundException;
import com.bital.util.ParameterParser;

 
public class Txn_0566 extends Transaction
{		 
	public Header doAction(ParameterParser param)
		throws ParameterNotFoundException
	{				
        GenericClasses gc = new GenericClasses();                				
		FormatoG formatog = new FormatoG();
		 
	   formatog.setTxnCode("0566");
       formatog.setFormat("G");	
       formatog.setToCurr("N$");
       formatog.setCheckNo("0");	
       formatog.setTranAmt(param.getCString("txtMonto"));	
       formatog.setComAmt(param.getCString("montoComision"));       
	   formatog.setIVAAmt(param.getCString("montoIVA"));
	   formatog.setAcctNo("1");
	   formatog.setBenefic(param.getString("txtNomBen") + " " + param.getString("txtApeBen"));	   
	   formatog.setOrdenan(param.getString("txtNomOrd") + " " + param.getString("txtApeOrd"));
	   String strciudad =param.getString("ciudad");
	   if(strciudad.length() > 20){
	      strciudad = strciudad.substring(0,20);
	   }else{
	      strciudad =gc.StringFiller(param.getString("ciudad"),20,true," ");
	   }	
	   String strip =gc.StringFiller(param.getString("dirip"),16,true," ");
	   formatog.setInstruc(strciudad + strip);
	   formatog.setCtaBenef(gc.StringFiller(param.getString("lstTipoDocto"),4,true," ")+gc.StringFiller(param.getString("txtFolio"),17,true," "));	   
		return formatog;
	}
	

}
