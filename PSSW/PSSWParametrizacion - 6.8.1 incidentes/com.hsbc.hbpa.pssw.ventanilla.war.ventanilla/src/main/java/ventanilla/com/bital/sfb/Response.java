package ventanilla.com.bital.sfb;

import java.util.Vector;

import ventanilla.com.bital.util.NSTokenizer;

public class Response
{

    private static final int LINE_LENGTH = 78;
    private String code = null;
    private int lines = 0;
    private String serial = null;
    private String msg = null;

    public Response(String s)
    {
        code = null;
        lines = 0;
        serial = null;
        msg = null;
        msg = s;
        parseString(); 
    }

    public Response(Vector vector)
    {
        code = null;
        lines = 0;
        serial = null;
        msg = null;
        code = (String)vector.get(0);
        lines = Integer.parseInt((String)vector.get(1));
        serial = (String)vector.get(2);
        msg = (String)vector.get(3);
    }

    private void parseString()
    {
        String s = msg;
        NSTokenizer nstokenizer = new NSTokenizer(s, "~");
        int i = 0;
        while(nstokenizer.hasMoreTokens())
        {
            String s1 = nstokenizer.nextToken();
            if(s1 != null)
            {
                switch(i)
                {
                case 0: // '\0'
                    code = s1;
                    break;

                case 1: // '\001'
                    lines = Integer.parseInt(s1);
                    break;

                case 2: // '\002'
                    serial = s1;
                    break;

                case 3: // '\003'
                    msg = s1;
                    break;

                }
                i++;
            }
        }

    }

    public String getCode()
    {
        return code;
    }

    public String getSerial()
    {
        return serial;
    }

    public String[] getLines()
    {
        int i = getLineNumber();
        String as[] = new String[i];
        for(int j = 0; j < i; j++)
            as[j] = getLine(j);

        return as;
    }

    public int getLineNumber()
    {
        int i = msg.length() / 78 + 1;
        if(i > lines)
            i = lines;
        return i;
    }

    public String getLine(int i)
    {
        int j = getLineNumber() - 1;
        if(i < 0 || i > j)
        {
            return null;
        }
        else
        {
            int k = i * 78;
            return msg.substring(k, Math.min(msg.length(), k + 78)).trim();
        }
    }
}
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 
