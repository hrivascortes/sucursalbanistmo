//*************************************************************************************************
//             Funcion: Servlet que da formato a la respuesta de totales de Hogan
//            Elemento: TotalesPCentral.java
//          Creado por: Alejandro Gonzalez Castro
//      Modificado por: Juvenal R. Fernandez Varela
//*************************************************************************************************
// CCN - 4360211 - 09/10/2004 - Se envia descripcion de totales en Mayusculas y minusculas, 
//				y el numero del total sin parentesis
//*************************************************************************************************
package ventanilla.com.bital.admin;

public class TotalesPCentral {
    private String txtRespuestaTxn = "";
    
    private int NoLineasTxn = 0;
    private int residuo = 0;
    private int elementosNoCargos = 0;
    private int elementosNoAbonos = 0;
    private int posicionIniCargos = 0;
    private int posicionFinCargos = 0;
    private int posicionIniAbonos = 0;
    private int posicionFinAbonos = 0;
    
    private String[] strRespuesta = new String[getNoLineas()];
    public String[] noTotalCargos = new String[9];
    public String[] descCargos    = new String[9];
    public String[] docsCargos    = new String[9];
    public String[] importeCargos = new String[9];
    public String[] noTotalAbonos = new String[9];
    public String[] descAbonos    = new String[9];
    public String[] docsAbonos    = new String[9];
    public String[] importeAbonos = new String[9];
    
    public void setNoLineasTxn(String newRespuesta) {
        int noLineas = newRespuesta.length();
        residuo = noLineas % 78;
        noLineas = noLineas / 78;
        if(residuo > 0) {
            noLineas = noLineas + 1;
        }
        NoLineasTxn = noLineas;
    }
        
    public void setRespuestaTxn(String newRespuesta) {
        txtRespuestaTxn = newRespuesta;
        executeTotales();
    }
    
    public int getNoLineas() {
        return NoLineasTxn;
    }
    
    private void executeTotales() {
        int control = 0;
        int noLineas = 0;
        strRespuesta = new String[getNoLineas()];
        
        for(int i=0; i<getNoLineas()-1; i++) {
            strRespuesta[i] = txtRespuestaTxn.substring(control,control+78);
            control = control + 78;
        }
        
        if(residuo > 0) {
            strRespuesta[getNoLineas()-1] = txtRespuestaTxn.substring(control,control+residuo);
            int tmp = 78-residuo;
            for(int i=1; i<tmp; i++) {
                strRespuesta[getNoLineas()-1] = strRespuesta[getNoLineas()-1] + " ";
            }
        }
        else {
            strRespuesta[getNoLineas()-1] = txtRespuestaTxn.substring(control,control+78);
        }
        
        posicionCargosAbonos();
        Cargos();
        Abonos();
    }
    
    private void posicionCargosAbonos() {
        String cadena = "";
        int columna=0;
        int fila=0;
        String strCompara = "";
        boolean ctrl = false;
        for(int i=1; i<=2; i++) {
            if(i == 1) {
                strCompara = "CARGOS";
                while(ctrl == false) {
                    
                    cadena = strRespuesta[fila].substring(columna,columna+6);
                    columna++;
                    if(cadena.equals(strCompara)) {
                        ctrl = true;
                        posicionIniCargos = fila;
                    }
                    if(columna >= 72) {
                        fila++;
                        columna = 0;
                    }
                    if(fila >= getNoLineas()) {
                        ctrl = true;
                    }
                }
                ctrl = false;
            }
            else {
                strCompara = "ABONOS";
                while(ctrl == false) {
                    cadena = strRespuesta[fila].substring(columna,columna+6);
                    columna++;
                    if(cadena.equals(strCompara)) {
                        ctrl = true;
                        posicionFinCargos = fila-1;
                        posicionIniAbonos = fila;
                        posicionFinAbonos = getNoLineas()-1;
                    }
                    if(columna >= 72) {
                        fila++;
                        columna = 0;
                    }
                    if(fila >= getNoLineas()) {
                        ctrl = true;
                    }
                }//while abonos
            }
        }//for
    }
    
    
    private void Cargos() {
        String noTotal = "";
        String strTemp = "";
        boolean ctrl2;
        int indexArray = 0;
        int indexStr = 0;
        int iniStr = 0;
        int finStr = 0;
        
        int noArrayCargos = posicionFinCargos - posicionIniCargos + 1;
        noTotalCargos = new String[noArrayCargos];
        descCargos    = new String[noArrayCargos];
        docsCargos    = new String[noArrayCargos];
        importeCargos = new String[noArrayCargos];
        
        for(int i = posicionIniCargos; i <= posicionFinCargos; i++) {
            ctrl2 = false;
            int cadenaProcesar = 1;
            boolean flag1 = false;
            while(ctrl2 == false) {
                strTemp = strRespuesta[i].substring(indexStr,indexStr+1);
                indexStr++;
                switch(cadenaProcesar) {
                    //no de totales
                    case 1:
                        
                        if(strTemp.equals("(")) {
                            iniStr = indexStr;
                        }
                        else if(strTemp.equals(")")) {
                            finStr = indexStr;
                            noTotalCargos[indexArray] = strRespuesta[i].substring(iniStr-1,finStr);
                            if(noTotalCargos[indexArray].length() == 3) {
                                String strTmp1 = noTotalCargos[indexArray].substring(0,1);
                                strTmp1 = strTmp1 + "0" + noTotalCargos[indexArray].substring(1);
                                noTotalCargos[indexArray] = strTmp1;
                            }
                            cadenaProcesar = 2;
                            iniStr = 0;
                            finStr = 0;
                            flag1 = false;
                        }
                        
                        break;
                        
                        
                        //Descripcion
                    case 2:
                        
                        if (flag1 == false) {
                            if(strTemp.equals(" ") && !strRespuesta[i].substring(indexStr+1,indexStr+2).equals(" ")) {
                                iniStr = indexStr + 1;
                                flag1 = true;
                            }
                        }
                        else {
                            if(strTemp.equals(" ") && strRespuesta[i].substring(indexStr+1,indexStr+2).equals(" ")  && strRespuesta[i].substring(indexStr+2,indexStr+3).equals(" ")) {
                                finStr = indexStr-1;
                                descCargos[indexArray] = strRespuesta[i].substring(iniStr-1,finStr);
                                cadenaProcesar = 3;
                                iniStr = 0;
                                finStr = 0;
                                flag1 = false;
                            }
                        }
                        
                        break;
                        
                        //documentos
                    case 3:
                        
                        if(flag1 == false && !strTemp.equals(" ")) {
                            iniStr = indexStr;
                            flag1 = true;
                        }
                        else if(flag1 == true && strTemp.equals(" ")) {
                            finStr = indexStr-1;
                            docsCargos[indexArray] = strRespuesta[i].substring(iniStr-1,finStr);
                            iniStr = 0;
                            finStr = 0;
                            flag1 = false;
                            cadenaProcesar = 4;
                        }
                        
                        if(indexStr == 55) {
                            docsCargos[indexArray] = "0";
                            cadenaProcesar = 4;
                            flag1 = false;
                        }
                        break;
                        
                        //importes
                    case 4:
                        
                        if(flag1 == false && !strTemp.equals(" ")) {
                            iniStr = indexStr;
                            flag1 = true;
                        }
                        else if(flag1 == true && strTemp.equals(" ")) {
                            finStr = indexStr-1;
                            importeCargos[indexArray] = strRespuesta[i].substring(iniStr-1,finStr);
                            if(importeCargos[indexArray].length() == 3) {
                                importeCargos[indexArray] = "0" + importeCargos[indexArray];
                            }
                            iniStr = 0;
                            finStr = 0;
                            ctrl2 = true;
                            indexStr = 0;
                        }
                        
                        break;
                        
                        //DEFAULT
                    default:
                        ctrl2 = true;
                        indexStr = 0;
                        break;
                }
                
                if(indexStr >= 78) {
                    ctrl2 = true;
                    indexStr = 0;
                }
            }
            indexArray++;
        }
        setNoCargos(noArrayCargos);
        
    }
    
    
    //metodos para regresar los valores de los cargos
    private void setNoCargos(int noCargos) {
        elementosNoCargos = noCargos;
    }
    
    public int getNoCargos() {
        return elementosNoCargos;
    }
    
    public String getNoTotalCargos(int indexArray1) {
        String strTotalNum = noTotalCargos[indexArray1];
        return strTotalNum.substring(1,strTotalNum.length()-1);
    }
    
    public String getDescripCargos(int indexArray1) {
        return descCargos[indexArray1].charAt(0) + descCargos[indexArray1].substring(1,descCargos[indexArray1].length()).toLowerCase();
    }
    
    public String getDocsCargos(int indexArray1) {
        return docsCargos[indexArray1];
    }
    
    public String getImptsCargos(int indexArray1) {
        return importeCargos[indexArray1];
    }
    
    private void Abonos() {
        String noTotal = "";
        String strTemp = "";
        boolean ctrl2;
        int indexArray = 0;
        int indexStr = 0;
        int iniStr = 0;
        int finStr = 0;
        
        int noArrayAbonos = posicionFinAbonos - posicionIniAbonos + 1;
        noTotalAbonos = new String[noArrayAbonos];
        descAbonos    = new String[noArrayAbonos];
        docsAbonos    = new String[noArrayAbonos];
        importeAbonos = new String[noArrayAbonos];
        
        for(int i = posicionIniAbonos; i <= posicionFinAbonos; i++) {
            ctrl2 = false;
            int cadenaProcesar = 1;
            boolean flag1 = false;
            while(ctrl2 == false) {
                strTemp = strRespuesta[i].substring(indexStr,indexStr+1);
                indexStr++;
                switch(cadenaProcesar) {
                    //no de totales
                    case 1:
                        
                        if(strTemp.equals("(")) {
                            iniStr = indexStr;
                        }
                        else if(strTemp.equals(")")) {
                            finStr = indexStr;
                            noTotalAbonos[indexArray] = strRespuesta[i].substring(iniStr-1,finStr);
                            if(noTotalAbonos[indexArray].length() == 3) {
                                String strTmp1 = noTotalAbonos[indexArray].substring(0,1);
                                strTmp1 = strTmp1 + "0" + noTotalAbonos[indexArray].substring(1);
                                noTotalAbonos[indexArray] = strTmp1;
                            }
                            cadenaProcesar = 2;
                            iniStr = 0;
                            finStr = 0;
                            flag1 = false;
                        }
                        
                        break;
                        
                        
                        //Descripcion
                    case 2:
                        
                        if (flag1 == false) {
                            if(strTemp.equals(" ") && !strRespuesta[i].substring(indexStr+1,indexStr+2).equals(" ")) {
                                iniStr = indexStr + 1;
                                flag1 = true;
                            }
                        }
                        else {
                            if(strTemp.equals(" ") && strRespuesta[i].substring(indexStr+1,indexStr+2).equals(" ")  && strRespuesta[i].substring(indexStr+2,indexStr+3).equals(" ")) {
                                finStr = indexStr-1;
                                descAbonos[indexArray] = strRespuesta[i].substring(iniStr-1,finStr);
                                cadenaProcesar = 3;
                                iniStr = 0;
                                finStr = 0;
                                flag1 = false;
                            }
                        }
                        
                        break;
                        
                        //documentos
                    case 3:
                        
                        if(flag1 == false && !strTemp.equals(" ")) {
                            iniStr = indexStr;
                            flag1 = true;
                        }
                        else if(flag1 == true && strTemp.equals(" ")) {
                            finStr = indexStr-1;
                            docsAbonos[indexArray] = strRespuesta[i].substring(iniStr-1,finStr);
                            iniStr = 0;
                            finStr = 0;
                            flag1 = false;
                            cadenaProcesar = 4;
                        }
                        
                        if(indexStr == 55) {
                            docsAbonos[indexArray] = "0";
                            cadenaProcesar = 4;
                            flag1 = false;
                        }
                        break;
                        
                        //importes
                    case 4:
                        
                        if(flag1 == false && !strTemp.equals(" ")) {
                            iniStr = indexStr;
                            flag1 = true;
                        }
                        else if(flag1 == true && strTemp.equals(" ")) {
                            finStr = indexStr-1;
                            importeAbonos[indexArray] = strRespuesta[i].substring(iniStr-1,finStr);
                            if(importeAbonos[indexArray].length() == 3) {
                                importeAbonos[indexArray] = "0" + importeAbonos[indexArray];
                            }
                            iniStr = 0;
                            finStr = 0;
                            ctrl2 = true;
                            indexStr = 0;
                        }
                        
                        break;
                        
                        //DEFAULT
                    default:
                        ctrl2 = true;
                        indexStr = 0;
                        break;
                }
                
                if(indexStr >= 78) {
                    ctrl2 = true;
                    indexStr = 0;
                }
            }
            indexArray++;
        }
        setNoAbonos(noArrayAbonos);
    }
    
    //metodos para regresar los valores de los cargos
    private void setNoAbonos(int noAbonos) {
        elementosNoAbonos = noAbonos;
    }
    
    public int getNoAbonos() {
        return elementosNoAbonos;
    }
    
    public String getNoTotalAbonos(int indexArray1) {
        String strTotalNum = noTotalAbonos[indexArray1];
        return strTotalNum.substring(1,strTotalNum.length()-1);
    }
    
    public String getDescripAbonos(int indexArray1) {
        //return descAbonos[indexArray1];
        return descAbonos[indexArray1].charAt(0) + descAbonos[indexArray1].substring(1,descAbonos[indexArray1].length()).toLowerCase();
    }
    
    public String getDocsAbonos(int indexArray1) {
        return docsAbonos[indexArray1];
    }
    
    public String getImptsAbonos(int indexArray1) {
        return importeAbonos[indexArray1];
    }
    
    public String formatStringPrinter() {
    	ventanilla.com.bital.beans.DatosVarios insIdentidad = ventanilla.com.bital.beans.DatosVarios.getInstance();                   
    	String strIdentidad = (String)insIdentidad.getDatosv("IDENTIDAD");
        String txtStringPrinter = "";
        txtStringPrinter = "TOTALES~";
        int noVeces = txtRespuestaTxn.length() / 78;
        txtStringPrinter =  txtStringPrinter + "                                  "+strIdentidad+"                           ~";
        txtStringPrinter =  txtStringPrinter + "                           TOTALES PROCESADOR CENTRAL~ ~ ~";
        for(int i=1; i<=noVeces; i++) {
            if(i<noVeces)
                txtStringPrinter = txtStringPrinter + "" + txtRespuestaTxn.substring(78*i,78*(i+1)) + "~";
            else
                txtStringPrinter = txtStringPrinter + "" + txtRespuestaTxn.substring(78*i,txtRespuestaTxn.length()) + "~";
        }
        String newStrPrt = txtStringPrinter.replace(' ',' ');//remplaza ceros binarios por espacio en forma hexadecimal
        return newStrPrt;
    }
}
