//*************************************************************************************************
//			Funcion: Clase especial para transacciones de OPEE
//			Elemento: SpFunc_OPEE.java
//			Creado por: Yair Jesus Ch�vez Antonel.
//			Modificado por: Jesus Emmanuel Lopez Rosales.
//*************************************************************************************************
// CCN - 4360533 - 20/10/2006 - Se crea elemento para funcion especial de OPEE
// CCN - 4360543 - 13/11/2006 - Se realizan modificaciones para recibir de variable de sesion la ruta de Digidoc
// CCN - 4360554 - 05/01/2007 - Se realizan encriptaci�n de usuario
// CCN - 4360562 - 19/01/2007 - Cambios para lectura de ruta de archivo OPEE desde variable de entorno
// CCN - 4360576 - 09/03/2007 - Modificaciones para proyecto OPMN
// CCN - 4360619 - 15/06/2007 - Se eliminan systems
//*************************************************************************************************
package ventanilla.com.bital.admin;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Hashtable;
import java.util.Vector;

import javax.servlet.http.HttpSession;
import javax.xml.namespace.QName;

import org.apache.axis.client.Call;
import org.apache.axis.client.Service;
import ventanilla.GenericClasses;
import ventanilla.com.bital.beans.DatosVarios;
import ventanilla.com.bital.sfb.Txn_0594;
import ventanilla.com.bital.sfb.Txn_5179;
import ventanilla.com.bital.sfb.Txn_5183;
import ventanilla.com.bital.util.Base64;
import com.bital.util.ConnectionPoolingManager;

public class SpFunc_OPEE implements InterfazSF{
	
	public SpFunc_OPEE(){			
	}		
	
	public int SpFuncionBD(Object bean, Vector Respuesta, HttpSession session, String Connureg){
		int messagecode = 0;
		return messagecode;
	}

	public int SpFuncionAD(Object bean, Vector Respuesta, HttpSession session, String Connureg){//DIARIO
		int messagecode = 0;					

		GenericClasses GC = new GenericClasses();				

		Hashtable datasession = (Hashtable)session.getAttribute("page.datasession");
		
		Hashtable usrinfo = (Hashtable)session.getAttribute("userinfo");
		
		//Variable para conexi�n a webservice OPEE
		String urlOPEE = "";
		javax.naming.Context initialContext = null;
				
		String sucursal 	= GC.getBeanData(bean,"getBranch").trim();
		String txn 			= GC.getBeanData(bean,"getTxnCode").trim();		
		String divisa_o 	= "";//GC.getBeanData(bean,"getAcctNo2");//getFormatoMoneda(GC.getBeanData(bean,"getTranCur").trim());				
		String monto_o 		= "";//GC.getBeanData(bean,"getMoAmoun");//GC.getBeanData(bean,"getTranAmt").trim();
		String usuario 		= usrinfo.get("D_EJCT_CTA").toString().trim();
		String fechaoper 	= GC.getDate(1).trim();
		String hora 		= Respuesta.get(2).toString().substring(1).trim();
		String cuenta 		= GC.getBeanData(bean,"getAcctNo").trim();	
		final String canal	= "00001";
		String status = "";
				
		// DATOS A OBTENER DE LA RESPUESTA DE HOGAN		
		String comision 	= Respuesta.get(3).toString().substring(27,40).trim();
		session.setAttribute("comision",comision);	
		String iva			= Respuesta.get(3).toString().substring(105,118).trim();
		session.setAttribute("Iva",iva);
		String numOPEE		= Respuesta.get(3).toString().substring(156,172).trim();
		Txn_0594.opee 		= numOPEE;
		String callback		= Respuesta.get(3).toString().substring(176,177).trim();
		String esquema 		= Respuesta.get(3).toString().substring(177,178).trim();
		String gasto		= Respuesta.get(3).toString().substring(178,180).trim();
		String nomOrdenante = Respuesta.get(3).toString().substring(78,100).trim();
		session.setAttribute("ordenante",nomOrdenante);

		String IT5Folio = "";
		String divisa_d = "";
		String monto_d  = "";
		String tc_divisa = "";
		
		numOPEE = numOPEE.substring(6);		
		session.setAttribute("numOPEE",numOPEE);
		
		// DATOS DEL PAIS		
		ventanilla.com.bital.beans.DatosPais pais = ventanilla.com.bital.beans.DatosPais.getInstance();
		
		String siglas=pais.getInfoPais("siglas").substring(2,4).trim();
		
		//System.out.println(siglas);
		if(siglas.length()<0)
		{
		  siglas="";
		}
		if(txn.equals("5159")){			
			numOPEE = GC.StringFiller(numOPEE, 15, false, "0");
			divisa_d = GC.getFormatoMoneda(GC.getBeanData(bean,"getTranCur"));
			if(divisa_d.equals("N$"))
			{
			  divisa_d="USD";
			}
			divisa_o = "USD";
			monto_d = GC.getBeanData(bean,"getTranAmt").trim();
			monto_o = GC.getBeanData(bean,"getMoAmoun");
		}
		
		if(txn.equals("5161") || txn.equals("5177")){			
			numOPEE = GC.StringFiller(numOPEE, 15, false, "0");
			
			divisa_d = GC.getFormatoMoneda(GC.getBeanData(bean,"getTranCur"));
			if(divisa_d.equals("N$"))
			{
			  divisa_d="USD";
			}
			divisa_o = divisa_d;
			monto_d = GC.getBeanData(bean,"getTranAmt");
			monto_o = monto_d;
		}
		if(txn.equals("5179") || txn.equals("5183")){
			divisa_o = GC.getBeanData(bean,"getAcctNo2");
			divisa_o = divisa_o.substring(5);
			monto_o = GC.getBeanData(bean,"getTranAmt");
			IT5Folio = GC.getBeanData(bean,"getAcctNo2");
			IT5Folio = IT5Folio.substring(0,5);
			
			numOPEE = numOPEE.substring(numOPEE.length()-5);
			numOPEE = sucursal + IT5Folio + numOPEE;
			
			divisa_d = GC.getFormatoMoneda(GC.getBeanData(bean,"getTranCur"));
			
			if(divisa_d.equals("N$"))
			{
			  divisa_d="USD";
			}
			//divisa_d = divisa_d.substring(5);
			
			
			if(txn.equals("5183") && divisa_d.equals("USD"))
			{
				monto_d = GC.delCommaPointFromString((String)datasession.get("txtMontoC"));
				divisa_d=divisa_o;
				divisa_o="USD";
			}else if(txn.equals("5183") && !divisa_d.equals("USD"))
			{
//				monto_d = GC.getBeanData(bean,"getTranAmt").trim();//GC.getBeanData(bean,"getMoAmoun");
				monto_d = GC.getBeanData(bean,"getMoAmoun");
				String monto_temp=monto_d;
				
				//monto_o=monto_d;
				//monto_o=monto_temp;
				
				divisa_o=divisa_d;
				divisa_d="USD";
			}
			else
				monto_d = GC.getBeanData(bean,"getTranAmt").trim();//GC.getBeanData(bean,"getMoAmoun");
			
			
			if(txn.equals("5183")){
				tc_divisa = Txn_5183.tipoCambioO;//GC.getBeanData(bean,"getDraftAm");
				String p_entera = "";
				String p_decimal = "";
				if(tc_divisa.length() > 4){
					p_entera = tc_divisa.substring(0,tc_divisa.length()-4);
					p_decimal = tc_divisa.substring(p_entera.length(),tc_divisa.length());
					p_entera = GC.StringFiller(p_entera, 7, false, "0");
					p_decimal = p_decimal + "00000";
					tc_divisa = p_entera + p_decimal;
				}
				else{
					p_entera = "0000000";
					p_decimal = GC.StringFiller(tc_divisa, 9, true, "0");
					tc_divisa = p_entera + p_decimal;
				}
			}	
			if(txn.equals("5179")){
				tc_divisa = Txn_5179.tipoCambio;//GC.getBeanData(bean,"getDraftAm");				
			}
			
		}
		
		String tc_dolar = "";
		if(txn.equals("5159") || txn.equals("5183")){			
			tc_dolar = (String)datasession.get("txtTC");
		}

		String sDatos = "|"+siglas+"|"+numOPEE+"|"+sucursal+"|"+usuario+"|"+fechaoper+"|"+hora+"|"+txn+"|"+cuenta+
						"|"+divisa_d+"|"+monto_d+"|"+divisa_o+"|"+monto_o+"|"+comision+"|"+iva+"|"+tc_dolar+
						"|"+tc_divisa+"|"+gasto+"|"+callback+"|"+canal+"|"+status+"|"+esquema+"|";
			
		//System.out.println("Esta es la cadena de la OPEE: "+sDatos);	
				

		Connection pooledConnection = ConnectionPoolingManager.getPooledConnection();
		PreparedStatement insert = null;	
		int ResultInsert = 0;
		
		try{			       
			
			try
			{	
				initialContext = new javax.naming.InitialContext();
				urlOPEE = (String)initialContext.lookup("java:comp/env/URLOPEE");
			}
			catch( javax.naming.NamingException namingException ){
				System.out.println( "Error SpFunc_OPEE::Context.lookup: [" + namingException.toString() + "]");
			}	

			String endpoint = urlOPEE;

			Service service = new Service();
			Call call = (Call) service.createCall();
			call.setTargetEndpointAddress( new java.net.URL(endpoint) );
			call.setOperationName(new QName("http://soapinterop.org/", "createOPI"));
		 	
			DatosVarios Pwds = DatosVarios.getInstance();
			Hashtable htPWDS = Pwds.getDatosH("PWDS");		 			
			String sKey = htPWDS.get("OPEE").toString();				
			Base64 b64 = new Base64();
			String cadDecode = "";
			byte[] bytecad = b64.decode(sKey);
			for (int i=0;i<bytecad.length;i++){
				 cadDecode = cadDecode + (char)bytecad[i];  	
			}		 		 
			String res = (String)call.invoke( new Object[] {sDatos, cadDecode} );	
		 	
			if(pooledConnection != null){	
				insert = pooledConnection.prepareStatement("INSERT INTO TW_OPEE VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
				insert.setString(1, numOPEE); 
				insert.setString(2, sucursal);
				insert.setString(3, usuario);
				insert.setString(4, fechaoper);
				insert.setString(5, hora);
				insert.setString(6, txn);
				insert.setString(7, cuenta);
				insert.setString(8, divisa_o);
				insert.setLong(9, Long.parseLong(monto_o));				
				insert.setString(10, divisa_d);
				insert.setLong(11, Long.parseLong(monto_d));						
				insert.setLong(12, Long.parseLong(comision));
				insert.setLong(13, Long.parseLong(iva));				
				insert.setString(14, tc_dolar);
				insert.setString(15, tc_divisa);
				insert.setString(16, gasto);
				insert.setString(17, callback);
				insert.setString(18, canal);
				insert.setString(20, esquema);
				insert.setString(21,siglas);
			}		 			 			
			if(res.equals("1")){
				try {					
					insert.setString(19, "R");						
					ResultInsert = insert.executeUpdate();					
				}catch(SQLException sqlException){
					System.err.println("SpFunc_OPEE::InsertLog [" + sqlException + "]");            
				}catch(Exception e){
					System.err.println("SpFunc_OPEE:: " + e.getMessage());
					e.printStackTrace();
				}        
				finally {
					try {
						if(insert != null) {
							insert.close();
							insert = null;
						}
						if(pooledConnection != null) {
							pooledConnection.close();
							pooledConnection = null;
						}
					}	
					catch(java.sql.SQLException sqlException){
						System.out.println("Error FINALLY SpFunc_OPEE::InsertLog [" + sqlException + "]");                
					}
				}			  				
			}else{
				try {					
					insert.setString(19, "E");
					ResultInsert= insert.executeUpdate();										    
				}catch(SQLException sqlException){
					System.err.println("SpFunc_OPEE::InsertLog [" + sqlException + "]");            
				}catch(Exception e){
					System.err.println("SpFunc_OPEE::" + e.getMessage());
					e.printStackTrace();
				}        
				finally {
					try {
						if(insert != null) {
							insert.close();
							insert = null;
						}
						if(pooledConnection != null) {
							pooledConnection.close();
							pooledConnection = null;
						}
					}
					catch(java.sql.SQLException sqlException){
						System.out.println("Error FINALLY SpFunc_OPEE::InsertLog [" + sqlException + "]");                
					}
				}
			}
		}catch( Exception e ){
			System.err.println("SpFunc_OPEE:: " + e.getMessage());
			e.printStackTrace();		 
			try {
				if(pooledConnection != null){				
					insert = pooledConnection.prepareStatement("INSERT INTO TW_OPEE VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
					insert.setString(1, numOPEE); 
					insert.setString(2, sucursal);
					insert.setString(3, usuario);
					insert.setString(4, fechaoper);
					insert.setString(5, hora);
					insert.setString(6, txn);
					insert.setString(7, cuenta);
					insert.setString(8, divisa_o);
					insert.setLong(9, Long.parseLong(monto_o));				
					insert.setString(10, divisa_d);
					insert.setLong(11, Long.parseLong(monto_d));						
					insert.setLong(12, Long.parseLong(comision));
					insert.setLong(13, Long.parseLong(iva));				
					insert.setString(14, tc_dolar);
					insert.setString(15, tc_divisa);
					insert.setString(16, gasto);
					insert.setString(17, callback);
					insert.setString(18, canal);
					insert.setString(19, "E");
					insert.setString(20, esquema);				
					insert.setString(21, siglas);
					ResultInsert= insert.executeUpdate();					
				}    
			}catch(SQLException sqlException){
				System.err.println("SpFunc_OPEE::InsertLog [" + sqlException + "]");            
			}catch(Exception ex){
				System.err.println("SpFunc_OPEE:: " + e.getMessage());
				e.printStackTrace();
			}        
			finally {
				try {
					if(insert != null) {
						insert.close();
						insert = null;
					}
					if(pooledConnection != null) {
						pooledConnection.close();
						pooledConnection = null;
					}
				}
				catch(java.sql.SQLException sqlException){
					System.out.println("Error FINALLY SpFunc_OPEE::InsertLog [" + sqlException + "]");                
				}
			}
		}
		return messagecode;
	}
	
	public int SpFuncionBR(Hashtable txnRev, Hashtable txnOrg, HttpSession session, String Connureg, String Supervisor){
		int messagecode = 0;
		return messagecode;
	}
	
	public int SpFuncionAR(Hashtable txnRev, Hashtable txnOrg, HttpSession session, String Connureg, String Cosecutivo, String Supervisor, Vector VResp){
		int messagecode = 0;
		return messagecode;
	}
}							
