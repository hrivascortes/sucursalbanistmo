//*************************************************************************************************
//             Funcion: Bean Txn 0476
//          Creado por: Humberto Balleza Mej�a
//*************************************************************************************************
// CCN - 46200080 - 10/06/2008 - Se agrega TXN
//*************************************************************************************************

package ventanilla.com.bital.sfb;

import com.bital.util.ParameterNotFoundException;
import com.bital.util.ParameterParser;

import ventanilla.GenericClasses;

public class Txn_0332 extends Transaction{
	
	public Header doAction(ParameterParser param)
		throws ParameterNotFoundException
	{
		
		FormatoB formatob = new FormatoB();
		GenericClasses gc = new GenericClasses();
		//String[] invoqued = request.getParameterValues("txtInvoqued");
		
		//ODPA 0332B 00002000251  000240  000**0332*1000*0000123997000501*****1000*N$**
		//ODPA 0332B 00002000251  000240  000**0332*654 *0000123997000501*****654 *N$**
		
		
		ServiciosB serviciob = new ServiciosB();
        serviciob.setBranch(param.getCString("sucursal"));
        //((String)datasession.get("sucursal"));
        
        serviciob.setTeller(param.getCString("teller"));
        //((String)datasession.get("teller"));
        
        serviciob.setSupervisor(param.getCString("teller"));
        //((String)datasession.get("teller"));
        
		//agregar supervisor en trace 11-08-2006
		//if ((String)datasession.get("supervisor") != null )
			//serviciob.setSupervisor((String)datasession.get("supervisor"));
        
        serviciob.setSupervisor(param.getCString("supervisor"));
         //((String)datasession.get("supervisor"));
		
		String regist = param.getString("registro");
		String respon = param.getString("txtRegResp");
		String causas = param.getString("lstReferenc");
		serviciob.setFormat("B");
		serviciob.setTxnCode("0332");
	   	serviciob.setAcctNo("0332");
	   	serviciob.setTranAmt(gc.delCommaPointFromString(param.getString("txtMontoNoCero")));
		serviciob.setReferenc(regist + respon + causas);
		
      
        serviciob.setCashOut(gc.delCommaPointFromString(param.getString("txtMontoNoCero")));
                
        String moneda = gc.getDivisa(param.getString("moneda"));
        
        serviciob.setFromCurr(moneda);
		
		
		return serviciob;
	}

}
