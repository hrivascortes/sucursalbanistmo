//*************************************************************************************************
//			   Funcion: Bean Txn 5217
//			 Elemento : Txn_5217.java
//		Modificado por: Humberto Enrique Balleza Mej�a
//*************************************************************************************************
// CCN -         - 07/01/2008 - Se crea el bean de la transacci�n 5217
//*************************************************************************************************
package ventanilla.com.bital.sfb;

import com.bital.util.ParameterParser;
import com.bital.util.ParameterNotFoundException;
import ventanilla.GenericClasses;

public class Txn_5217 extends Transaction
{
	public Header doAction(ParameterParser param)
		throws ParameterNotFoundException
	{
		//txtDDACuenta*txtMonto*lstTipoCargo*txtFechaEfect*

		FormatoA formatoa = new FormatoA();
		GenericClasses gc = new GenericClasses();
		formatoa.setTxnCode("5217");
		formatoa.setAcctNo(param.getString("txtDDACuenta"));
		formatoa.setTranAmt(param.getCString("txtMonto"));
		formatoa.setTranDesc(param.getString("lstTipoCargo"));
		String fecha1 = param.getString("txtFechaEfect");
		String fecha2 = param.getString("txtFechaSys");
		String txtFechaEfect =  gc.ValEffDate(fecha1, fecha2);
		formatoa.setEffDate(txtFechaEfect);
	    if (param.getString("override") != null)
       		if (param.getString("override").equals("SI"))
           		formatoa.setOverride("3");
		String moneda = gc.getDivisa(param.getString("moneda"));
		formatoa.setTranCur(moneda);

		return formatoa;
	}
}
