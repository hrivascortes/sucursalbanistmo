//*************************************************************************************************
//             Funcion: Bean Txn 0814
//      Creado por: Jesus Emmanuel Lopez Rosales
//*************************************************************************************************
// CCN - 4360576 - 09/03/2007 - Modificaciones para proyecto OPMN
//*************************************************************************************************
package ventanilla.com.bital.sfb;

import ventanilla.GenericClasses;

import com.bital.util.ParameterParser;
import com.bital.util.ParameterNotFoundException;

public class Txn_1695 extends Transaction
{
	public Header doAction(ParameterParser param)
		throws ParameterNotFoundException
	{
		//ODPA 1695A 00005000540  000540  000**0199989246**100*N$******000****************************************
		
		FormatoA formatoa = new FormatoA();
		GenericClasses gc = new GenericClasses();
        formatoa.setAcctNo(param.getString("txtDDACuenta"));
        formatoa.setTranAmt(gc.quitap(param.getString("txtMonto")));
        String divisa = gc.getDivisa(param.getString("moneda"));
        formatoa.setTranCur(divisa);
        formatoa.setMoAmoun("000");
        formatoa.setTranDesc(param.getString("lstTipoComision"));
        String descRev = param.getString("lstTipoComision");
        descRev = descRev.substring(0,2) + "REV " + descRev.substring(2);
        formatoa.setDescRev(descRev);
		
		return formatoa;
	}
}
