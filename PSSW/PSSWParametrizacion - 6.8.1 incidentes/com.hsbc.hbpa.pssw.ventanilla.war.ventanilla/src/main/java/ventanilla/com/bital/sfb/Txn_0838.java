//*************************************************************************************************
//             Funcion: Bean Txn 0825
//          Creado por: Fausto Rodrigo Flores Moreno
//*************************************************************************************************
// CCN - 4360574 - 09/03/2007 - Se crea bean para la consulta de servicios marcados para RAP Calculadora
//*************************************************************************************************
package ventanilla.com.bital.sfb;

import com.bital.util.ParameterParser;
import com.bital.util.ParameterNotFoundException;
import ventanilla.GenericClasses;

public class Txn_0838 extends Transaction
{
	public Header doAction(ParameterParser param)
		throws ParameterNotFoundException
	{
		FormatoG formatog = new FormatoG();
		GenericClasses gc = new GenericClasses();
		String Reftmp = "";

		formatog.setTxnCode("0838");
		formatog.setFormat("G");
		
		Reftmp = param.getString("txtReferRapCal");
		if(Reftmp.length()>20){
			formatog.setAcctNo(Reftmp.substring(0,20));
			formatog.setCtaBenef(Reftmp.substring(20,Reftmp.length()));
		}else{
			formatog.setAcctNo(Reftmp);
		}
		formatog.setTranAmt(gc.quitap(param.getString("monto1")));
		formatog.setBenefic(param.getString("servicio1"));
			
		String moneda = gc.getDivisa(param.getString("moneda"));
		formatog.setToCurr(moneda);
		
		return formatog;
	}
}