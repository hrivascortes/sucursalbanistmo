//*************************************************************************************************
//             Funcion: Bean de servicios formato E
//            Elemento: ServiciosE.java
//          Creado por: Rutilo Zarco Reyes
// Ultima Modificacion: 02/08/2004
//      Modificado por: Alejandro Gonzalez Castro
//*************************************************************************************************
// CCN -4360174- 02/08/2004 - Se agrega metodo FormatoSAT para que no imprima null en certificacion
//*************************************************************************************************

package ventanilla.com.bital.sfb;

public class ServiciosE extends Header
{
  private String txtEffDate   = "";
  private String txtAcctNo    = "";
  private String txtTranAmt   = "";
  private String txtReferenc1 = "";
  private String txtReferenc2 = "";
  private String txtReferenc3 = "";
  private String txtCashIn    = "";
  private String txtCashOut   = "";
  private String txtFromCurr  = "";
  private String txtToCurr    = "";
  private String txtCardType  = "";
  private String txtFiller    = "";
  private String txtFiller1   = "";
  private String txtHoldays   = "";
  private String txtTrNo      = "";
  private String txtCheckNo   = "";
  private String txtMoamoun   = "";
  private String txtDraftAm   = "";
  private String txtFormatoSAT= "";

  public String getFormatoSAT()
  {
      return txtFormatoSAT + "*";
  }
  public void setFormatoSAT(String newFormatoSAT)
  {
      txtFormatoSAT = newFormatoSAT;
  }

  public String getEffDate()
  {
	return txtEffDate + "*";
  }
  public void setEffDate(String newEffDate)
  {
	txtEffDate = newEffDate;
  }

  public String getAcctNo()
  {
	return txtAcctNo + "*";
  }
  public void setAcctNo(String newAcctNo)
  {
	txtAcctNo = newAcctNo;
  }

  public String getTranAmt()
  {
	return txtTranAmt + "*";
  }
  public void setTranAmt(String newTranAmt)
  {
	txtTranAmt = newTranAmt;
  }

  public String getReferenc1()
  {
	return txtReferenc1 + "*";
  }
  public void setReferenc1(String newReferenc1)
  {
	txtReferenc1 = newReferenc1;
  }

  public String getReferenc2()
  {
	return txtReferenc2 + "*";
  }
  public void setReferenc2(String newReferenc2)
  {
	txtReferenc2 = newReferenc2;
  }

  public String getReferenc3()
  {
	return txtReferenc3 + "*";
  }
  public void setReferenc3(String newReferenc3)
  {
	txtReferenc3 = newReferenc3;
  }

  public String getCashIn()
  {
	return txtCashIn + "*";
  }
  public void setCashIn(String newCashIn)
  {
	txtCashIn = newCashIn;
  }

  public String getCashOut()
  {
	return txtCashOut + "*";
  }
  public void setCashOut(String newCashOut)
  {
	txtCashOut = newCashOut;
  }

  public String getFromCurr()
  {
	return txtFromCurr + "*";
  }
  public void setFromCurr(String newFromCurr)
  {
	txtFromCurr = newFromCurr;
  }

  public String getToCurr()
  {
	return txtToCurr + "*";
  }
  public void setToCurr(String newToCurr)
  {
	txtToCurr = newToCurr;
  }

  public String getCardType()
  {
	return txtCardType + "*";
  }
  public void setCardType(String newCardType)
  {
	txtCardType = newCardType;
  }

  public String getFiller()
  {
	return txtFiller + "*";
  }
  public void setFiller(String newFiller)
  {
	txtFiller = newFiller;
  }

  public String getFiller1()
  {
	return txtFiller1 + "*";
  }
  public void setFiller1(String newFiller1)
  {
	txtFiller1 = newFiller1;
  }

  public String getHoldays()
  {
	return txtHoldays + "*";
  }
  public void setHoldays(String newHoldays)
  {
	txtHoldays = newHoldays;
  }

  public String getTrNo()
  {
	return txtTrNo + "*";
  }
  public void setTrNo(String newTrNo)
  {
	txtTrNo = newTrNo;
  }

  public String getCheckNo()
  {
	return txtCheckNo + "*";
  }
  public void setCheckNo(String newCheckNo)
  {
	txtCheckNo = newCheckNo;
  }

  public String getMoamoun()
  {
	return txtMoamoun + "*";
  }
  public void setMoamoun(String newMoamoun)
  {
	txtMoamoun = newMoamoun;
  }

  public String getDraftAm()
  {
	return txtDraftAm + "*";
  }
  public void setDraftAm(String newDraftAm)
  {
	txtDraftAm = newDraftAm;
  }

  public String toString()
  {
	return super.toString()
	+ getEffDate()	+ getAcctNo()		+ getTranAmt()
	+ getReferenc1()+ getReferenc2()	+ getReferenc3()
	+ getCashIn()	+ getCashOut()		+ getFromCurr()	+ getToCurr()
	+ getCardType()	+ getFiller()		+ getFiller1()	+ getHoldays()
	+ getTrNo()		+ getCheckNo()		+ getMoamoun()	+ getDraftAm();
  }
}
