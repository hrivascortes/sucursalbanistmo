//*************************************************************************************************
//             Funcion: Bean Txn 0768
//*************************************************************************************************
// CCN - XXXXXXX - XXXXXXXXXX -                                                       
//*************************************************************************************************

package ventanilla.com.bital.sfb;

import com.bital.util.ParameterParser;
import com.bital.util.ParameterNotFoundException;

import ventanilla.GenericClasses;
 

public class Txn_0768 extends Transaction
{
	public Header doAction(ParameterParser param)
		throws ParameterNotFoundException
	{
		FormatoB2 formatob = new FormatoB2();
		GenericClasses gc = new GenericClasses();
		
		//ODCT 0768B 09004900403  900403  000**0768MRMDUS33    USD*50000*227405173******US$**
		//ODPA 0768B 05030503007  503007  000**0768MILDGB22       *18181*000000000******N$**

		String moneda = gc.getDivisa(param.getString("moneda"));
		formatob.setTxnCode("0768");
		formatob.setFormat("B");
		String ceros="";
		if(moneda.equals("N$"))
				formatob.setAcctNo("0768" + param.getString("lstBanCorresp1")+gc.StringFiller( ceros,12-param.getString("lstBanCorresp1").length(),false," ")+"USD");
		else
				formatob.setAcctNo("0768" + param.getString("lstBanCorresp1")+gc.StringFiller( ceros,12-param.getString("lstBanCorresp1").length(),false," ")+moneda);
				
		if(moneda.equals("N$"))
			formatob.setTranAmt(param.getCString("txtMonto"));
		else
			formatob.setTranAmt(param.getCString("txtMonto1"));
		
		formatob.setReferenc("000000000");
		formatob.setCashIn("0");
		formatob.setFromCurr(moneda);
		formatob.setDesc(param.getCString("txtRefTIRE"));
		
		return formatob;
	}
}
