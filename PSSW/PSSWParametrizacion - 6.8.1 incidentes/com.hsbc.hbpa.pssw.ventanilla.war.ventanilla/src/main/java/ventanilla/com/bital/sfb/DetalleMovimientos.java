package ventanilla.com.bital.sfb;

/**
 * @author YGX
 * 
 */
import java.io.IOException;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.namespace.QName;

import org.apache.axis.client.Call;
import org.apache.axis.client.Service;
import org.jdom.Element;

import ventanilla.GenericClasses;
import ventanilla.com.bital.admin.Movimientos;
import ventanilla.com.bital.admin.PerfilCuenta;
import ventanilla.com.bital.beans.DatosVarios;
import ventanilla.com.bital.util.Base64;



public class DetalleMovimientos extends HttpServlet
{
	 
	 
	 public void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {
    	GenericClasses GC = new GenericClasses();
    	List prueba;
    	resp.setContentType("text/plain");
        HttpSession session   = req.getSession(false);        
        Hashtable datasession = (Hashtable)session.getAttribute("page.datasession");
       
        //Parametros 
        String cuenta  = (String)datasession.get("txtDDACuenta");
        String fecha   = (String)datasession.get("txtFechaInicial");
        String renglon = (String)datasession.get("txtNumReglon");
        session.setAttribute("page.renglon",renglon);
                 
        
        //Datos
        String cia       = "60";
        String producto  = "DDA"; 
        String fecha_sol = "1" + GC.getDate(1).substring(2,8);
        
        String f="";
        
        f = "1" + fecha.substring(8,10) + fecha.substring(3,5)+fecha.substring(0,2);        
        
                
        //Systems
        /*
        System.out.println("cta    " + cuenta);
        System.out.println("fecha  " + fecha);
        System.out.println("renglon" + renglon);
        System.out.println("cia    " + cia);
        System.out.println("producto"+ producto);
        System.out.println("fecha_ " + fecha_sol);
        */
        //Codigo de errores
        String error_perfil="0";
        String error_mov="0";
        String descripcion_mov="0";
        String descripcion_perfil="0";
        
        //Respuesta xml movimientos
        	
		String fecha_xml      =" ";
    	String cheq_xml       =" ";
    	String tran_id_xml    =" ";
    	String tipTran_xml    =" ";
    	String monto_xml      =" ";
    	String saldo_xml      =" ";
    	String rastrId_xml    =" ";
    	String tipoTran_xml    =" ";
    	String descripcion_xml=" ";	
    	String cta_xml      =" ";		   		   
		String sucursal_xml =" ";	   
		String nombre_xml   =" ";
		String nombre1_xml   =" ";
		String nombre2_xml   =" ";
		String nombre3_xml   =" ";
		String nombre4_xml   =" ";
                
        //Url a la que se va ha conectar el servicio 
        DatosVarios Urls = DatosVarios.getInstance();
        Hashtable htURL = Urls.getDatosH("URLS");	
        String endpoint = htURL.get("DMOV").toString();
        //System.out.println("URL"+endpoint);	
        
        //Pwd que se guarda en la tabla
        DatosVarios Pwds = DatosVarios.getInstance();
        Hashtable htPWDS = Pwds.getDatosH("PWDS");		 			
        String sKey = htPWDS.get("DMOV").toString();
        
        //Desncripcion
        //String sKey = "SEZTSE9HUEEqaW5nYTAz";				
		String cadDecode = "";			
		String code = "";
		String code2 = "";
		Base64 b64 = new Base64();
		byte[] bytecad = b64.decode(sKey);
		for (int i=0;i<bytecad.length;i++){
			 cadDecode = cadDecode + (char)bytecad[i];  	
		}		
			 		 
		//System.out.println(cadDecode);	
		if(!cadDecode.equals(""))
		{
		 
		 /*StringTokenizer tok = new StringTokenizer(cadDecode,"*");             
	      while(tok.hasMoreTokens()) 
	      {  
	        code  = tok.nextToken();     
		    code2 = tok.nextToken();
	      }*/
	      
	      code = cadDecode.substring(0,8);
          code2   = cadDecode.substring(8);
        
        //System.out.println("code  " + code);
        //System.out.println("code2 " + code2);
		  
		}
             
                	   
        //Servicio que consulta el Perfil de la Cuenta       
      
          try{        
          
           Service service = new Service();
		   Call call = (Call) service.createCall();
		   call.setTargetEndpointAddress( new java.net.URL(endpoint) );
		   call.setOperationName(new QName("http://soapinterop.org/", "perfil"));
           //call.setUsername("HFSHOGPA");
           //call.setPassword(sKey);
           call.setUsername(code);
           call.setPassword(code2);
           
         	 try{
	           String textoXML = (String)call.invoke( new Object[] {cia, producto, cuenta});
	           PerfilCuenta cta = new PerfilCuenta();
			   cta.leePerfil(textoXML);
			   		   
			   if(cta.getCodigoerror().length()>0)
			   {
			   		error_perfil= (String)cta.getCodigoerror();   		        		
	        		descripcion_perfil=(String)cta.getDescriperror();        		
	        		session.setAttribute("page.error_perfil",error_perfil);
				    session.setAttribute("page.descripcion_perfil",descripcion_perfil);
			   }
			   else{
			   	   //Respuesta Del Servicio		   		   
				    cta_xml      = (String)cta.getCuenta().toString();		   		   
				    sucursal_xml = (String)cta.getSucursal().toString();		   
				    
				    nombre1_xml  = (String)cta.getNombre1().toString().trim();
				   	nombre2_xml	 = (String)cta.getNombre2().toString().trim();
				   	nombre3_xml	 = (String)cta.getNombre3().toString().trim();
				   	nombre4_xml	 = (String)cta.getNombre4().toString().trim();
				   	
				   	nombre_xml = nombre1_xml+" "+nombre2_xml+" "+nombre3_xml+" "+ nombre4_xml;
				   //Set�s en session
				   session.setAttribute("page.cta_xml",cta_xml);
				   session.setAttribute("page.sucursal_xml",sucursal_xml);
				   session.setAttribute("page.nombre_xml",nombre_xml); 
		   		   session.setAttribute("page.error_perfil",error_perfil);
				   session.setAttribute("page.descripcion_perfil",descripcion_perfil);
				   
			   		/*/Respuesta
				   //System.out.println("cta_xml:"+ cta_xml);
				   //System.out.println("nombre_xml:"+ nombre_xml);
				   //System.out.println("sucursal_xml:"+ sucursal_xml);*/
				   
			   }
	
	           
	         }catch( Exception e ){
      			
      			System.err.println("Error DetalleMovimientos::Perfil Cuenta::["+e.toString()+"]");  
      			getServletContext().getRequestDispatcher("/ventanilla/paginas/error.jsp").forward(req,resp);
      			return;   
      		  }    			      			
	           
	           	   
		 
          }catch( Exception e )
        	  {
      			
      			System.err.println("Error DetalleMovimientos::Perfil Cuenta::["+e.toString()+"]");      			      			
      			getServletContext().getRequestDispatcher("/ventanilla/paginas/error.jsp").forward(req,resp);
      			return;   
    	    } 
    	    
    	 
      
         
         try{
           	
	           Service service = new Service();
			   Call call = (Call) service.createCall();
			   call.setTargetEndpointAddress( new java.net.URL(endpoint) );
			   call.setOperationName(new QName("http://soapinterop.org/", "movimientos"));
	           call.setUsername(code);
	           call.setPassword(code2);
	           
	           //ret para desarrollo
	           //String ret = (String)call.invoke( new Object[] {cia, producto, cuenta, f, fecha_sol });           
	           //String ret = (String)call.invoke( new Object[] {cia, producto, cuenta, "1060904", "1060905" });
	           //String ret = (String)call.invoke( new Object[] {cia, producto, cuenta, f, "1071126" });  
	                   
	          try{
	              //YGX Cambio Consulta servicio
	              //String ret = (String)call.invoke( new Object[] {cia, producto, cuenta, f, fecha_sol });           
	              
	                String ret = (String)call.invoke( new Object[] {cia, producto, cuenta, f });           
	              //System.out.println("ret: "+ret);
	           
	           Movimientos mov = new Movimientos();      
	      	   mov.leePerfil(ret);
	      	   
	      	   if(mov.getCodigoerror().length()>0)
			   {
			   		error_mov= (String)mov.getCodigoerror();   		        		
	        		descripcion_mov=(String)mov.getDescriperror();
	
					session.setAttribute("page.error_mov",error_mov);
			        session.setAttribute("page.descripcion_mov",descripcion_mov);
			   }
			   else{
		      	   prueba = mov.getMovimiento();		      
		           Hashtable hs = new Hashtable();
		           Iterator i = prueba.iterator();
			  	   
			  	   int x = prueba.size();
			  	   int k=0;						
			  	   //System.out.println("Prueba.size:" +x);
		  
			  		while (i.hasNext())
			  		{	  		  
			  		  Element e = (Element)i.next();
			  		  Element fechOper  = (Element) e.getChild("fechaOper");			  		  	
			  		  Element numCheque = (Element) e.getChild("numCheque");
			  		  Element tranId    = (Element) e.getChild("tranId");
			  		  Element montoTran = (Element) e.getChild("montoTran");
			  		  Element tipoTran  = (Element) e.getChild("tipoTran");
			  		  Element saldo     = (Element) e.getChild("saldo");
			  		  Element rastrId   = (Element) e.getChild("rastrId");
			  		  Element descripcion = (Element) e.getChild("descripcion");	
			  		  
			  		  	
					   fecha_xml      = (String)fechOper.getText();
					   	if(fecha_xml.length()==0) fecha_xml =" .";
    				   cheq_xml       = (String)numCheque.getText();
    				   	if(cheq_xml.length()==0) cheq_xml =" .";
    				   tran_id_xml    = (String)tranId.getText();
    				   	if(tran_id_xml.length()==0) tran_id_xml =" .";
    				   monto_xml      = (String)montoTran.getText();
    				   	if(monto_xml.length()==0) monto_xml =" .";
                       tipTran_xml    = (String)tipoTran.getText();                       
                       	if(tipTran_xml.length()==0) tipTran_xml =" .";
                       saldo_xml      = (String)saldo.getText();
                       	if(saldo_xml.length()==0) saldo_xml =" .";
                       rastrId_xml    = (String)rastrId.getText();
                       	if(rastrId_xml.length()==0) rastrId_xml =" .";
                       descripcion_xml= (String)descripcion.getText();
                       	if(descripcion_xml.length()==0) descripcion_xml =" .";
			  		  	  		  
			  		  k++;	  		  
			  		  
					  /*hs.put("key" + k ,fechOper.getText() +"~"+ numCheque.getText()   
					  		   +"~"+ tranId.getText()  +"~"+ montoTran.getText() 
					  		   +"~"+ tipoTran.getText()+"~"+ saldo.getText()	
					  		   +"~"+ rastrId.getText() +"~"+ descripcion.getText());      */  
					  	hs.put("key" + k, fecha_xml   +"&"+ cheq_xml +"&"
					  				 	+ tran_id_xml +"&"+ monto_xml+"&"
					  				 	+ tipTran_xml +"&"+ saldo_xml+"&"
					  				 	+ rastrId_xml +"&"+ descripcion_xml); 
			  		}
			  
					//System.out.println("Size hash"+hs.size());
					session.setAttribute("page.movimientos",hs);
					session.setAttribute("page.bandera",renglon);
					session.setAttribute("page.error_mov",error_mov);
			        session.setAttribute("page.descripcion_mov",descripcion_mov);
			   
				  }
    
	          } catch( Exception e ){
      			System.err.println("Error DetalleMovimientos::Moviemientos::["+e.toString()+"]");
      			
      			getServletContext().getRequestDispatcher("/ventanilla/paginas/error.jsp").forward(req,resp);
      			return;     
	          }
	          
	              	
         }catch( Exception e ){
      			System.err.println("Error DetalleMovimientos::Moviemientos::["+e.toString()+"]");      			
      			getServletContext().getRequestDispatcher("/ventanilla/paginas/error.jsp").forward(req,resp);
      			return;   
      	
    	    }
    	        	    
    	
    	String sMessage = "0~1~0000000~BUSQUEDA ACEPTADA~";
    	session.setAttribute("page.txnresponse", sMessage);
        getServletContext().getRequestDispatcher("/servlet/ventanilla.Redirector").forward(req,resp);
        
     }
        
     public void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {
        doPost(req, resp);
    }
    


}
