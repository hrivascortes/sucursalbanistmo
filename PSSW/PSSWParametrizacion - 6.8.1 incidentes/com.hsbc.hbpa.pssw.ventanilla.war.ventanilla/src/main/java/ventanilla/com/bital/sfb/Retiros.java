//*************************************************************************************************
//             Funcion: Bean para Retiros
//            Elemento: Retiros.java
//          Creado por: Alejandro Gonzalez Castro
//*************************************************************************************************
// CCN - 4360221 - 22/10/2004 - Inclusion en la impresion de la certificacion del nombre corto del 
//				cliente a quien se le deposita
//*************************************************************************************************
 
package ventanilla.com.bital.sfb;

import ventanilla.GenericClasses; 
 
public class Retiros extends Header
{
  private String txtBeneficiario = "";
  private String txtReversable = "";
  private String txtDescRev = "";
  private String txtEffDate  = "";     
  private String txtAcctNo   = "";     
  private String txtNatcurr  = "";     
  private String txtTranAmt  = "";     
  private String txtTranCur  = "";     
  private String txtCheckNo  = "";
  private String txtTrNo     = "";
  private String txtCashIn   = "";
  private String txtCashOut  = "";
  private String txtDraftAm  = "";
  private String txtMoAmoun  = "";
  private String txtFees     = "";
  private String txtTranDesc = "";
  private String txtHolDays  = "";
  private String txtHoldAmt  = "";
  
  private String txtGrptyp1  = "";
  private String txtAmount1  = "";
  private String txtApplid1  = "";
  private String txtAcctNo1  = "";
  private String txtCheckNo1 = "";
  private String txtTrNo1    = "";
  private String txtHolDays1 = "";
  
  private String txtGrptyp2  = "";
  private String txtAmount2  = "";
  private String txtApplid2  = "";
  private String txtAcctNo2  = "";
  private String txtCheckNo2 = "";
  private String txtTrNo2    = "";
  private String txtHolDays2 = "";
  
  private String txtGrptyp3  = "";
  private String txtAmount3  = "";
  private String txtApplid3  = "";
  private String txtAcctNo3  = "";
  private String txtCheckNo3 = "";
  private String txtTrNo3    = "";
  private String txtHolDays3 = "";
  
  private String txtGrptyp4  = "";
  private String txtAmount4  = "";
  private String txtApplid4  = "";
  private String txtAcctNo4  = "";
  private String txtCheckNo4 = "";
  private String txtTrNo4    = "";
  private String txtHolDays4 = "";
  
  private String txtGrptyp5  = "";
  private String txtAmount5  = "";
  private String txtApplid5  = "";
  private String txtAcctNo5  = "";
  private String txtCheckNo5 = "";
  private String txtTrNo5    = "";
  private String txtHolDays5 = "";
   
  public String getEffDate()    
  {return txtEffDate + "*";}     
  public String getAcctNo()  
  {return txtAcctNo + "*";}    
  public String getNatCurr() 
  {return txtNatcurr + "*";}    
  public String getTranAmt() 
  {return txtTranAmt + "*";}    
  public String getTranCur()      
  {return txtTranCur + "*";}    
  public String getCheckNo()
  {return txtCheckNo + "*";}    
  public String getTrNo()
  {return txtTrNo + "*";}    
  public String getCashIn()
  {return txtCashIn + "*";}    
  public String getCashOut()
  {return txtCashOut + "*";}    
  public String getDraftAm()
  {return txtDraftAm + "*";}    
  public String getMoAmoun()
  {return txtMoAmoun+"*";}
  public String getFees()
  {return txtFees + "*";}    
  public String getTranDesc()
  {return txtTranDesc + "*";}    
  public String getHolDays()
  {return txtHolDays + "*";}    
  public String getHoldAmt()
  {return txtHoldAmt + "*";} 
  
  public String getGrpTyp1()
  {return txtGrptyp1 + "*";} 
  public String getAmount1()
  {return txtAmount1 + "*";}    
  public String getApplid1()
  {return txtApplid1 + "*";}    
  public String getAccNo1()
  {return txtAcctNo1 + "*";}    
  public String getCheckNo1()
  {return txtCheckNo1 + "*";}    
  public String getTrNo1()
  {return txtTrNo1 + "*";}    
  public String getHolDays1()
  {return txtHolDays1 + "*";}    
  
  public String getGrpTyp2()
  {return txtGrptyp2 + "*";} 
  public String getAmount2()
  {return txtAmount2 + "*";}    
  public String getApplid2()
  {return txtApplid2 + "*";}    
  public String getAccNo2()
  {return txtAcctNo2 + "*";}    
  public String getCheckNo2()
  {return txtCheckNo2 + "*";}    
  public String getTrNo2()
  {return txtTrNo2 + "*";}    
  public String getHolDays2()
  {return txtHolDays2 + "*";}    
  
  public String getGrpTyp3()
  {return txtGrptyp3 + "*";} 
  public String getAmount3()
  {return txtAmount3 + "*";}    
  public String getApplid3()
  {return txtApplid3 + "*";}    
  public String getAccNo3()
  {return txtAcctNo3 + "*";}    
  public String getCheckNo3()
  {return txtCheckNo3 + "*";}    
  public String getTrNo3()
  {return txtTrNo3 + "*";}    
  public String getHolDays3()
  {return txtHolDays3 + "*";}    
 
  public String getGrpTyp4()
  {return txtGrptyp4 + "*";} 
  public String getAmount4()
  {return txtAmount4 + "*";}    
  public String getApplid4()
  {return txtApplid4 + "*";}    
  public String getAccNo4()
  {return txtAcctNo4 + "*";}    
  public String getCheckNo4()
  {return txtCheckNo4 + "*";}    
  public String getTrNo4()
  {return txtTrNo4 + "*";}    
  public String getHolDays4()
  {return txtHolDays4 + "*";}    
 
  public String getGrpTyp5()
  {return txtGrptyp5 + "*";} 
  public String getAmount5()
  {return txtAmount5 + "*";}    
  public String getApplid5()
  {return txtApplid5 + "*";}    
  public String getAccNo5()
  {return txtAcctNo5 + "*";}    
  public String getCheckNo5()
  {return txtCheckNo5 + "*";}    
  public String getTrNo5()
  {return txtTrNo5 + "*";}    
  public String getHolDays5()
  {return txtHolDays5 + "*";}    
////////////////////////SET ///////////////////////////
  public void setEffDate(String newEffDate) 
  {
    txtEffDate = newEffDate;
  }
  public void setAcctNo(String newAcctNo)  
  {txtAcctNo = newAcctNo;}
  public void setNatCurr(String newAcctNo)  
  {txtNatcurr = newAcctNo;}
  public void setTranAmt(String newTranAmt)
  {
    txtTranAmt = newTranAmt;
  }
  public void setTranCur(String newTranCur){
   GenericClasses gc = new GenericClasses();
   txtTranCur = gc.getDivisa(newTranCur);
  }
  public void setCheckNo(String newCheckNo)
  {txtCheckNo = newCheckNo;}
  public void setTrNo(String newTrNo)
  {txtTrNo = newTrNo;}
  public void setCashIn(String newCashIn)
  {txtCashIn = newCashIn;}
  public String setCashOut(String newCashOut)
  {
    return txtCashOut = newCashOut;
  }    
  public String setDraftAm(String newDraftAm)
  {return txtDraftAm = newDraftAm;}    
  public void setMoAmoun(String newMoAmoun)
  {txtMoAmoun = newMoAmoun;}
  public void setFees(String newFees)
  {txtFees = newFees;}
  public void setTranDesc(String newTranDesc)
  {txtTranDesc = newTranDesc;}
  public void setHolDays(String newHolDays)
  {txtHolDays = newHolDays;}
  public void setHoldAmt(String newHoldAmt)
  {txtHoldAmt = newHoldAmt;}
  
  public void setGrptyp1(String newGrptyp1)
  {txtGrptyp1 = newGrptyp1;}
  public void setAmount1(String newAmount1)
  {txtAmount1 = newAmount1;}
  public void setApplid1(String newApplid1)
  {txtApplid1 = newApplid1;}
  public void setAcctNo1(String newAcctNo1)
  {txtAcctNo1 = newAcctNo1;}
  public void setCheckNo1(String newCheckNo1)
  {txtCheckNo1 = newCheckNo1;}
  public void setTrNo1(String newTrNo1)
  {txtTrNo1 = newTrNo1;}
  public void setHolDays1(String newHolDays1)
  {txtTrNo1 = newHolDays1;}

  public void setGrptyp2(String newGrptyp2)
  {txtGrptyp2 = newGrptyp2;}
  public void setAmount2(String newAmount2)
  {txtAmount2 = newAmount2;}
  public void setApplid2(String newApplid2)
  {txtApplid2 = newApplid2;}
  public void setAcctNo2(String newAcctNo2)
  {txtAcctNo2 = newAcctNo2;}
  public void setCheckNo2(String newCheckNo2)
  {txtCheckNo2 = newCheckNo2;}
  public void setTrNo2(String newTrNo2)
  {txtTrNo2 = newTrNo2;}
  public void setHolDays2(String newHolDays2)
  {txtTrNo2 = newHolDays2;}

  public void setGrptyp3(String newGrptyp3)
  {txtGrptyp3 = newGrptyp3;}
  public void setAmount3(String newAmount3)
  {txtAmount3 = newAmount3;}
  public void setApplid3(String newApplid3)
  {txtApplid3 = newApplid3;}
  public void setAcctNo3(String newAcctNo3)
  {txtAcctNo3 = newAcctNo3;}
  public void setCheckNo3(String newCheckNo3)
  {txtCheckNo3 = newCheckNo3;}
  public void setTrNo3(String newTrNo3)
  {txtTrNo3 = newTrNo3;}
  public void setHolDays3(String newHolDays3)
  {txtTrNo3 = newHolDays3;}
   
  public void setGrptyp4(String newGrptyp4)
  {txtGrptyp4 = newGrptyp4;}
  public void setAmount4(String newAmount4)
  {txtAmount4 = newAmount4;}
  public void setApplid4(String newApplid4)
  {txtApplid4 = newApplid4;}
  public void setAcctNo4(String newAcctNo4)
  {txtAcctNo4 = newAcctNo4;}
  public void setCheckNo4(String newCheckNo4)
  {txtCheckNo4 = newCheckNo4;}
  public void setTrNo4(String newTrNo4)
  {txtTrNo4 = newTrNo4;}
  public void setHolDays4(String newHolDays4)
  {txtTrNo4 = newHolDays4;}
 
  public void setGrptyp5(String newGrptyp5)
  {txtGrptyp5 = newGrptyp5;}
  public void setAmount5(String newAmount5)
  {txtAmount5 = newAmount5;}
  public void setApplid5(String newApplid5)
  {txtApplid5 = newApplid5;}
  public void setAcctNo5(String newAcctNo5)
  {txtAcctNo5 = newAcctNo5;}
  public void setCheckNo5(String newCheckNo5)
  {txtCheckNo5 = newCheckNo5;}
  public void setTrNo5(String newTrNo5)
  {txtTrNo5 = newTrNo5;}
  public void setHolDays5(String newHolDays5)
  {txtTrNo5 = newHolDays5;}
 
  public String getDescRev()
  {
	return txtDescRev;
  }    
  
  public void setDescRev(String newDescRev)
  {
	txtDescRev = newDescRev;
  }

  public String getReversable()
  {
	return txtReversable;
  }    
  
  public void setReversable(String newReverso)
  {
	txtReversable = newReverso;
  }

  public String getBeneficiario()
  {
	return txtBeneficiario;
  }    
  
  public void setBeneficiario(String newBeneficiario)
  {
	txtBeneficiario = newBeneficiario;
  }

  public String toString()
  {  // Formato "A"
  	return super.toString()
         + getEffDate() + getAcctNo() + getNatCurr() + getTranAmt()+ getTranCur()
         + getCheckNo() + getTrNo()   + getCashIn()  + getCashOut()+ getDraftAm()
         + getMoAmoun() + getFees()   + getTranDesc()+ getHolDays()+ getHoldAmt()
         + getGrpTyp1() + getAmount1()+ getApplid1()+ getAccNo1()  + getCheckNo1()
         + getTrNo1()   + getHolDays1()
         + getGrpTyp2() + getAmount2()+ getApplid2()+ getAccNo2()+ getCheckNo2()
         + getTrNo2()   + getHolDays2()
         + getGrpTyp3() + getAmount3()+ getApplid3()+ getAccNo3()+ getCheckNo3()
         + getTrNo3()   + getHolDays3()
         + getGrpTyp4() + getAmount4()+ getApplid4()+ getAccNo4()+ getCheckNo4()
         + getTrNo4()   + getHolDays4()
         + getGrpTyp5() + getAmount5()+ getApplid5()+ getAccNo5()+ getCheckNo5()
         + getTrNo5()   + getHolDays5();
   }      
}
