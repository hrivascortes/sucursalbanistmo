//*************************************************************************************************
//			   Funcion: Bean Txn 5219
//			 Elemento : Txn_5219.java
//		Modificado por: Humberto Enrique Balleza Mej�a
//*************************************************************************************************
// CCN -         - 07/01/2008 - Se crea el bean de la transacci�n 5219
//*************************************************************************************************
package ventanilla.com.bital.sfb;

import com.bital.util.ParameterParser;
import com.bital.util.ParameterNotFoundException;
import ventanilla.GenericClasses;

public class Txn_5219 extends Transaction
{
	public Header doAction(ParameterParser param)
		throws ParameterNotFoundException
	{
		//txtDDACuenta*txtMonto*lstTipoAbono*txtFechaEfect*
	
		FormatoA formatoa = new FormatoA();
		GenericClasses gc = new GenericClasses();
		formatoa.setTxnCode("5219");
		formatoa.setAcctNo(param.getString("txtDDACuenta"));
		formatoa.setCashIn("000");
		formatoa.setTranAmt(param.getCString("txtMonto"));
		formatoa.setTranDesc(param.getString("lstTipoAbono"));
		String fecha1 = param.getString("txtFechaEfect");
		String fecha2 = param.getString("txtFechaSys");
		String txtFechaEfect =  gc.ValEffDate(fecha1, fecha2);
		formatoa.setEffDate(txtFechaEfect);
	    if (param.getString("override") != null)
       		if (param.getString("override").equals("SI"))
           		formatoa.setOverride("3");
		String moneda = gc.getDivisa(param.getString("moneda"));
		formatoa.setTranCur(moneda);

		return formatoa;
	}
}
