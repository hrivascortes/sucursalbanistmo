//*************************************************************************************************
//			 Funcion: Clase que realiza la consulta de las Monedas y las conserva en memoria
//		  Creado por: Humberto Enrique Balleza Mej�a
//	  Modificado por: 
//*************************************************************************************************
//CCN -XXXXXXX- 13/02/2007 - Se crea la clase para realizar select y mantener los datos en memoria
//*************************************************************************************************

package ventanilla.com.bital.beans;

import ventanilla.com.bital.util.DBQuery;

import java.util.Vector;
import java.util.Hashtable;

public class MonedasInt {
	
	private Hashtable monedas = null;
	private static MonedasInt instance = null;
    
	private MonedasInt() 
	{   
		monedas = new Hashtable();
		execute();
	}

	public static synchronized MonedasInt getInstance()
	{
		if(instance == null)
			instance = new MonedasInt();
		return instance;
	}
	
	public java.util.Vector getInfoMoneda(String moneda)
	{     
		String mon = null;
		java.util.Vector vMonedas = new java.util.Vector();
		if(moneda.length()==1 && !moneda.equals("0"))
			moneda="0"+moneda;
		if(monedas.containsKey(moneda))
		{
			mon = (String)this.monedas.get(moneda);
			ventanilla.com.bital.util.NSTokenizer parser = new ventanilla.com.bital.util.NSTokenizer(mon, "*");
			while( parser.hasMoreTokens() ){
			  String token = parser.nextToken();
			  if( token == null )
			   continue;
			  vMonedas.add(token);
			}
		}
		if(vMonedas.isEmpty())
		{
			vMonedas.add(moneda);
			vMonedas.add("No existe la moneda:"+moneda);
		}
		return vMonedas;
	}
	
	public void execute() 
	   {
		   String sql = null;
		   sql = "SELECT C_NOM_MONEDA, D_DESCRIPCION, D_INTCODE FROM ";
		   String clause = null;
		   clause = " ";
        
		   DBQuery dbq = new DBQuery(sql,"TC_MONEDAS", clause);
		   Vector Regs = dbq.getRows();
        
		   int size = Regs.size();
		   Vector row = new Vector();
		   String id_moneda = null;
		   String nombre_moneda = null;
		   String id_int = null;


		   for(int i=0; i<size; i++)
		   {
			   row = (Vector)Regs.get(i);
			   id_moneda = row.get(0).toString().trim();
			   nombre_moneda = row.get(1).toString().trim();
			   id_int = row.get(2).toString().trim();
			   monedas.put(id_moneda,nombre_moneda+"*"+id_int);
			   monedas.put(id_int,nombre_moneda+"*"+id_int);
		   }
	   }

}
