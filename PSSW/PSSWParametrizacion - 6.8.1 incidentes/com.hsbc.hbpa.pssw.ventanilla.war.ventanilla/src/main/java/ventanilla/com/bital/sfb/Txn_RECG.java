//*************************************************************************************************
//             Funcion: Bean Txn 4193 SOLICTUD DE CHEQUES DE GERENCIA 
//             Elaborado por: YGX
//*************************************************************************************************

package ventanilla.com.bital.sfb;


import java.util.StringTokenizer;

import ventanilla.GenericClasses;
import com.bital.util.ParameterNotFoundException;
import com.bital.util.ParameterParser;


public class Txn_RECG extends Transaction
{		 
	public Header doAction(ParameterParser param)
		throws ParameterNotFoundException
	{				
        
        GenericClasses gc = new GenericClasses();   
        FormatoH formatoh = new FormatoH();         				
		ventanilla.com.bital.sfb.Cheques Cheques = new ventanilla.com.bital.sfb.Cheques();
		
		formatoh.setTxnId("ODPA");
		formatoh.setTxnCode("RECG");
	    formatoh.setFormat("H");   
	    
	    String llave = param.getString("val_datos");
	    //System.out.println(" Txn_RECG " + llave );
	    String datatmp="";	    
	    String datatmp1="";
	    String datatmp2="";
	    String datatmp3="";
	    String datatmp4="";
	    String datatmp5="";
	    String datatmp6="";
	    String datatmp7="";  
	    String datatmp8="";
	    String datatmp9="";

	   StringTokenizer tok = new StringTokenizer(llave,"~");             
	   while(tok.hasMoreTokens()) 
	   { 
	     datatmp  = tok.nextToken();
	     datatmp1 = tok.nextToken();
	     datatmp2 = tok.nextToken();
	     datatmp3 = tok.nextToken();
	     datatmp4 = tok.nextToken();
	     datatmp5 = tok.nextToken();
	     datatmp6 = tok.nextToken();
	     datatmp7 = tok.nextToken();
	     datatmp8 = tok.nextToken();
	     datatmp9 = tok.nextToken();
	   }
	    
	    formatoh.setAcctNo(datatmp9);
	    formatoh.setTranAmt(datatmp1);
	    formatoh.setCheckNo(datatmp7);
	    formatoh.setTranDesc("Reactivacion de Cheques de Gerencia");
	    
    	return formatoh;
	}
}
