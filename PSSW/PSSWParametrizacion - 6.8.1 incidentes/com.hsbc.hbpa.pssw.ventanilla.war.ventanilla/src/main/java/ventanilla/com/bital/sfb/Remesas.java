package ventanilla.com.bital.sfb;
 
public class Remesas extends Header
{
  private String txtDescRev = "";
  private String txtEffDate = "";	
  private String txtAcctNo = "";	
  private String txtSerial = "";
  private String txtTranAmt = "";
  private String txtFeeAmoun = "";
  private String txtReferenc1 = "";
  private String txtReferenc2 = "";
  private String txtReferenc3 = "";
  private String txtTranCur = "";
  private String txtFromCurr = "";
  private String txtCheckNo = "";
  private String txtTrNo = "";
  private String txtDraftAm = "";
  private String txtMoamoun = "";
  private String txtTranDesc = "";
  private String txtHoldays = "";
  private String txtTipoCambio = "";
  private String txtTrNo5 = "";
  private String txttoCurr = "";
  private String txtService = "";
  private String txtComAmt = "";	  
  private String txtGStatus = "";
  private String txtCheckNo5 = "";  

  public void iniVar()
  {
	txtEffDate = "";
	txtAcctNo = "";	
	txtSerial = "";
	txtTranAmt = "";
	txtFeeAmoun = "";
	txtReferenc1 = "";
	txtReferenc2 = "";
	txtReferenc3 = "";
	txtTranCur = "";
	txtFromCurr = "";
	txtCheckNo = "";
	txtTrNo = "";
	txtDraftAm = "";
	txtMoamoun = "";
	txtTranDesc = "";
	txtHoldays = "";
	txtTipoCambio = "";
	txtTrNo5 = "";
	txttoCurr = "";
	txtService = "";
	txtComAmt = "";
	txtGStatus = "";
	txtCheckNo5 = "";	
  }

  
  public void setEffDate(String newEffDate)
  {
	txtEffDate = newEffDate;
  }  
  
  public String getEffDate()
  {
	return txtEffDate + "*";
  }


  public void setAcctNo(String newAcctNo)
  {
	txtAcctNo = newAcctNo;
  }  

  public String getAcctNo()
  {
	return txtAcctNo + "*";
  }    

  /*public void setSerial(String newSerial)
  {
	txtSerial = newSerial;
  }  

  public String getSerial()
  {
	return txtSerial + "*";
  } */     
  
  public void setTranAmt(String newTranAmt)
  {
	txtTranAmt = newTranAmt;
  }  
  
  public String getTranAmt()
  {
	return txtTranAmt + "*";
  }    

  public void setReferenc1(String newReferenc1)
  {
	  txtReferenc1 = newReferenc1;
  }
  
  public String getReferenc1()
  {
	  return txtReferenc1 + "*";
  }
  
  public void setReferenc2(String newReferenc2)
  {
	  txtReferenc2 = newReferenc2;
  }
  
  public String getReferenc2()
  {
	  return txtReferenc2 + "*";
  }

  public void setReferenc3(String newReferenc3)
  {
	  txtReferenc3 = newReferenc3;
  }
  
  public String getReferenc3()
  {
	  return txtReferenc3 + "*";
  }
  
  public void setTranCur(String newTranCur)
  {
	txtTranCur = newTranCur;
  }    

  public String getTranCur()
  {
	return txtTranCur + "*";
  }    

  public void setFromCurr(String newFromCurr)
  {
	txtFromCurr = newFromCurr;
  }    

  public String getFromCurr()
  {
	return txtFromCurr + "*";
  }      

  
// Cheque  
  public void setCheckNo(String newCheckNo)
  {
	  txtCheckNo = newCheckNo;
  }
  
  public String getCheckNo()
  {
	  return txtCheckNo + "*";
  }

//Causa
  public void setTrNo(String newTrNo)
  {
	  txtTrNo = newTrNo;
  }
  
  public String getTrNo()
  {
	  return txtTrNo + "*";
  }

  
// DraftAm
  public void setDraftAm(String newDraftAm)
  {
	  txtDraftAm = newDraftAm;
  }
  
  public String getDraftAm()
  { 
	  return txtDraftAm + "*";
  }
  
// Banco 
  public void setFeeAmoun(String newFeeAmoun)
  {
	  txtFeeAmoun = newFeeAmoun;
  }
  
  public String getFeeAmoun()
  {
	  return txtFeeAmoun + "*";
  }
  
//MoAmoun
  public void setMoamoun(String newMoamoun)
  {
	  txtMoamoun = newMoamoun;
  }
  
  public String getMoamoun()
  {
	  return txtMoamoun + "*";
  }
  

//Referencia  
  public void setTranDesc(String newTranDesc)
  {
	  txtTranDesc = newTranDesc;
  }
  
  public String getTranDesc()
  {
	  return txtTranDesc + "*";
  }    
  
//Dias diferidos  
  public void setHoldays(String newHoldays)
  {
	  txtHoldays = newHoldays;
  }
  
  public String getHoldays()
  {
	  return txtHoldays + "*";
  }
  
  
// Tipo de cambio  
  public void setTipoCambio(String newTipoCambio)
  {
	  txtTipoCambio = newTipoCambio;
  }
  
  public String getTipoCambio()
  {
	  return txtTipoCambio + "*";
  }
  
// Txn Anterior
  public void setTrNo5(String newTrNo5)
  {
	  txtTrNo5 = newTrNo5;
  }
  
  public String getTrNo5()
  {
	  return txtTrNo5 + "*";
  }
  
  public void settoCurr(String newtoCurr)
  {
	  txttoCurr = newtoCurr;
  }
  
  public String gettoCurr()
  {
	  return txttoCurr + "*";
  }
  
  public void setService(String newService)
  {
	  txtService = newService;
  }
  
  public String getService()
  {
	  return txtService + "*";
  }
  
  public void setComAmt(String newComAmt)
  {
	  txtComAmt = newComAmt;
  }
  
  public String getComAmt()
  {
	  return txtComAmt + "*";
  }
  
  public void setGStatus(String newGStatus)
  {
	  txtGStatus = newGStatus ;
  }
  
  public String getGStatus()
  {
	  return txtGStatus +  "*";
  }
  
  public void setCheckNo5(String newCheckNo5)
  {
	  txtCheckNo5 = newCheckNo5;
  }
  
  public String getCheckNo5()
  {
	  return txtCheckNo5 + "*";
  }

  public String getDescRev()
  {
	return txtDescRev;
  }    
  
  public void setDescRev(String newDescRev)
  {
	txtDescRev = newDescRev;
  }
  
  public String toString()
  {
	if(getTxnCode().equals("4487") || getTxnCode().equals("5453"))								//transaccion
	{
      return super.toString() + "*" + getAcctNo() + getTranAmt() + getReferenc1() + getReferenc2() + getReferenc3() + "**" + getFromCurr() + "*****" + getTrNo() + getCheckNo() + "**";
	}
	
	else if (getFormat().equals("B"))
	{
		return super.toString() + "*" + getAcctNo() + getTranAmt() + getReferenc1() + getFeeAmoun() + "****" + getFromCurr() + "*";
	}

	else if (getFormat().equals("G"))
	{
		return super.toString() + "*" + gettoCurr() + getService() + gettoCurr() + getCheckNo() + "*";
	}
	
	else
	{
		return super.toString() + "*" + getAcctNo() + "*" + getTranAmt() + getTranCur() + getCheckNo() + getTrNo() + "**" + getDraftAm() + getMoamoun() + "*" + getTranDesc() + getHoldays() + "*********************************" + getCheckNo5() + getTrNo5()  + "*";
	}
  }    
}
