//*************************************************************************************************
//             Funcion: Bean Txn 1797
//      Modificado por: Fredy Pe�a Moreno
//*************************************************************************************************
// CCN - 4360607 - 18/05/2007 - Bean para TXNS Genericas Retiros Generico
//*************************************************************************************************

package ventanilla.com.bital.sfb;

import com.bital.util.ParameterParser;
import com.bital.util.ParameterNotFoundException;
import ventanilla.GenericClasses;

public class Txn_1797 extends Transaction{
	public Header doAction(ParameterParser param)
		throws ParameterNotFoundException
	{	
		FormatoA formatoa = new FormatoA();
		GenericClasses gc = new GenericClasses();
		formatoa.setTxnCode("1797");
		formatoa.setFormat("A");
		formatoa.setAcctNo(param.getString("txtDDACuenta"));		
		formatoa.setTranAmt(param.getCString("txtMonto"));
		formatoa.setCashOut(param.getCString("txtMonto"));		
		formatoa.setTranDesc(param.getString("lstDGeneric1797"));
		formatoa.setDescRev("REV Retiro Efectivo con Ficha Multiple");
		
		if (param.getString("override") != null)
            if (param.getString("override").equals("SI"))
                formatoa.setOverride("3");
		
		String moneda = gc.getDivisa(param.getString("moneda"));
		formatoa.setTranCur(moneda);
		return formatoa;
	}
}