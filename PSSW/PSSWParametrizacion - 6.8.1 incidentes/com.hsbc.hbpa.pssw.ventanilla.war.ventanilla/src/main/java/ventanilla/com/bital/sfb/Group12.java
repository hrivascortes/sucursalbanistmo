//*************************************************************************************************
//		   	   Funcion: Clase que realiza txn 4503
//	  		  Elemento: Group16Serv.java
//		    Creado por: Alejandro Gonzalez Castro
//		Modificado por: Juvenal Fernandez
//*************************************************************************************************
//CCN - 4360268 - 04/02/2005 - Se realizan adecuaciones para eliminar Agencia NY
//*************************************************************************************************
package ventanilla.com.bital.sfb;

import com.bital.util.ParameterParser;
import com.bital.util.ParameterNotFoundException;
import ventanilla.GenericClasses;

public class Group12 extends Transaction
{
  public Header doAction(ParameterParser param)
    throws ParameterNotFoundException
  {
    // Instanciar el bean a utilizar
    Deposito deposito = new Deposito();
     
    String descrip="";
    deposito.setAcctNo  (param.getString("txtDDACuenta"));
    
    if(param.getString("cTxn").equals("4503"))
    deposito.setCheckNo (param.getString("txtSerial"));
    
    deposito.setTranAmt (param.getCString("txtMonto"));
	GenericClasses gc = new GenericClasses();
    
    int moneda = param.getInt("moneda");
	deposito.setTranCur( gc.getDivisa(param.getString("moneda")));

    if( param.getString("txtFechaEfect", null) != null )
    {
      String fecha1 = param.getString("txtFechaEfect");
      String fecha2 = param.getString("txtFechaSys");
      fecha1 = fecha1.substring(6) + fecha1.substring(3, 5) + fecha1.substring(0, 2);
   
      if(param.getString("cTxn").equals("4503")){
    	  if(param.getString("lstDesc4503").length() > 31){
    		  descrip = param.getString("lstDesc4503").substring(0, 31);
    	  }else{
    		  descrip = param.getString("lstDesc4503");
    	  }
      }
      else{
    	  descrip = param.getString("lstDesc4505");
      }
    	  
      String fecha   = param.getString("txtFechaEfect");
      //HEMT
      StringBuffer fechaFormateada = new StringBuffer();
      fechaFormateada.append(fecha.substring(0, 2)).append(fecha.substring(3, 5)).append(fecha.substring(6));
      
      if(descrip.length() > 32){
    	  //Fechaformateada mide 8
    	  descrip = descrip.substring(0,32) + fechaFormateada;
      }else{
    	  descrip = descrip + fechaFormateada;
      }
      fechaFormateada.delete(0, fechaFormateada.length());
      
      deposito.setTranDesc(descrip);

      // Descripcion para Reverso
      String DescRev = descrip.substring(0,2) + "REV." + descrip.substring(2,descrip.length());
      if(DescRev.length() > 40)
        DescRev = DescRev.substring(0,40);
      deposito.setDescRev(DescRev);

      if( !fecha1.equals(fecha2) )
      {
        fecha2 = fecha1.substring(4, 6) + fecha1.substring(6) + fecha1.substring(2, 4);
        deposito.setEffDate(fecha2);
      }
    }

    return deposito;
  }               
}