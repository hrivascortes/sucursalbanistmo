//*************************************************************************************************
//             Funcion: Clase que realiza la consulta de Menues Secundarios a desplgar y los conserva en memoria
//            Elemento: MenuesS.java
//          Creado por: Juvenal R. Fernandez V.
//      Modificado por: Rutilo Zarco Reyes
//*************************************************************************************************
// CCN - 4360152 - 21/06/2004 - Se crea la clase para realizar select y mantener los datos en memoria
// CCN - 4360175 - 06/08/2004 - Se corrige inclusionn elemento ultimo de select
// CCN - 4360334 - 17/06/2005 - Se elimina variable dbQualifier evitando enviar "." antes del nombre de la tabla
// CCN - 4360342 - 08/07/2005 - Se realizan modificaciones para departamentos.
// CCN - 4360456 - 07/04/2006 - Se realizan modifiaciones para txns secundarias por nivel.
//*************************************************************************************************

package ventanilla.com.bital.beans;

import ventanilla.com.bital.util.DBQuery;

import java.util.*;

public class MenuesS
{
    private Hashtable menuess = null;
	private Vector menuessD = null;
    private static MenuesS instance = null;
    
    private MenuesS() 
    {   
        menuess = new Hashtable();
		menuessD = new Vector();
        execute();
    }
    
    public static synchronized MenuesS getInstance()
    {
        if(instance == null)
            instance = new MenuesS();
        return instance;
    }
    
    public Vector getMenuesS(String proceso)
    {
        Vector tmp = new Vector();
        if(menuess.containsKey(proceso))
            tmp = (Vector)this.menuess.get(proceso);
        return tmp;
    }
    
	public Vector getMenuesSDept()
	{
		return this.menuessD;
	}
    
    public void execute() 
    {
        String sql = null;
        sql = "SELECT C_PROCESO, C_TRANSACCION, C_MONEDA, C_TRANSACCION_S, D_DESCRIPCION,N_NIVEL_CAJERO FROM ";
        String clause = null;
        clause = " ORDER BY C_PROCESO, C_TRANSACCION, C_TRANSACCION_S";
        
        DBQuery dbq = new DBQuery(sql,"TC_MENUES_TXN_S", clause);
        Vector Regs = dbq.getRows();
        
        int size = Regs.size();
        Vector row = new Vector();
        Vector rows = new Vector();
		Vector rowsd = new Vector();
        String proceso = null;
        String newproceso = null;
        
        for(int i=0; i<size; i++)
        {
            row = (Vector)Regs.get(i);
            proceso = row.get(0).toString();
            
            if(newproceso == null)
                newproceso = proceso;
            
            if( (!proceso.equals(newproceso)) || (i == size-1) )
            {    
                if (i == size-1)
                    rows.add(row);
                menuess.put(newproceso, rows);
                rows = new Vector();
                newproceso = proceso;
            }
            rows.add(row);
			menuessD.add(row);
        }
    }
}
