//*************************************************************************************************
//          Funcion: Clase que maneja la transaccion 5355
//          Elemento: SpFunc_CQDV.java
//          Creado por: Humberto Enrique Balleza Mej�a
//          Modificado por: Jesus Emmanuel Lopez Rosales
//*************************************************************************************************

package ventanilla.com.bital.admin;

import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;
import javax.servlet.http.HttpSession;
import ventanilla.GenericClasses;
import ventanilla.com.bital.sfb.FormatoA;
import java.math.BigInteger;

public class SpFunc_CQDV  implements InterfazSF{
	
	public SpFunc_CQDV(){			
	}		
	
	GenericClasses GC = new GenericClasses();
	
	public int SpFuncionBD(Object bean, Vector Respuesta, HttpSession session, String Connureg){
		int messagecode = 0;
		return messagecode;
	}

	public int SpFuncionAD(Object bean, Vector Respuesta, HttpSession session, String Connureg){
		int messagecode = 0;
		
		//ODDT 5361A 00005000597  000597  000**4011834959**10033*N$*0000257*02113**10033**000**31312313********3******************************
        FormatoA fa = new FormatoA();
        Hashtable datasession = (Hashtable)session.getAttribute("page.datasession");
        
        // Hashtable chequesdevueltos =
        // (Hashtable)session.getAttribute("ChequesDevueltos");
		Hashtable chequesdevueltos = new Hashtable();
		int size;
		if (session.getAttribute("ChequesDevueltos") != null) {
			chequesdevueltos = (Hashtable) session
					.getAttribute("ChequesDevueltos");
			size = chequesdevueltos.size();
		} else {
			size = 0;
		}

		session.setAttribute("CountChequesT", String.valueOf(size));
        ventanilla.GenericClasses gc = new ventanilla.GenericClasses();
        Enumeration Keys = (Enumeration)chequesdevueltos.keys();
        int i=0;
        BigInteger montoTotal = new BigInteger("0");
        String txtCasoEsp = new String("");
        while(!chequesdevueltos.isEmpty())
        {
        String Key = Keys.nextElement().toString();
        String Cuenta = (String)datasession.get("txtDDACuenta");
		String Serial = Key.substring(11,Key.length()); 
		Serial = gc.rellenaZeros(Serial,7);
        String valor = (String)chequesdevueltos.get(Key);
        int pos = valor.indexOf("*");
        String monto = valor.substring(0,pos); 
        monto = gc.delCommaPointFromString(monto);
        String rechazo = valor.substring(pos+1,valor.length());              
        String moneda = (String)datasession.get("moneda");
        fa.setBranch((String)datasession.get("sucursal"));
        fa.setTeller((String)datasession.get("teller"));
        fa.setSupervisor((String)datasession.get("supervisor"));
        fa.setTxnCode("5355");
        fa.setFormat("A");
        fa.setAcctNo((String)Cuenta);
        fa.setTranAmt(monto);
		fa.setTranCur(gc.getDivisa(moneda));
        fa.setCheckNo((String)Serial);	
        fa.setMoAmoun("000");
        fa.setTrNo("021"+rechazo); 
        fa.setCashOut(monto);
        fa.setTrNo1("");
        fa.setTranDesc("");
        String dirip = (String)session.getAttribute("dirip");
        try{//delay para evitar txns duplicada
		   	 Thread.sleep(1500);
		   }catch (InterruptedException e){
		   }
        Diario diario = new Diario();
        String sMessage = diario.invokeDiario(fa, (String)session.getAttribute("d_ConsecLiga"), dirip, moneda, session);
        chequesdevueltos.remove(Key);
        i++;
        BigInteger montoParcial = new BigInteger(monto);
        montoTotal = montoTotal.add(montoParcial);
        txtCasoEsp = txtCasoEsp + "5355" + "~" + getString(sMessage,1) + "~";
        }
        session.setAttribute("txtCasoEsp", txtCasoEsp);
        session.setAttribute("CountChequesOK",String.valueOf(i));
        session.setAttribute("montoChqDev1003",montoTotal.toString());
        return messagecode;
	}
	
	public int SpFuncionBR(Hashtable txnRev, Hashtable txnOrg, HttpSession session, String Connureg, String Supervisor){
		int messagecode = 0;
		return messagecode;
	}
	
	public int SpFuncionAR(Hashtable txnRev, Hashtable txnOrg, HttpSession session, String Connureg, String Cosecutivo, String Supervisor, Vector VResp){
		int messagecode = 0;
		return messagecode;
	}
	
    private String getString(String newCadNum, int NumSaltos) {
        int iPIndex = newCadNum.indexOf("CONSECUTIVO:") + 12, Num = 0, Ban = 0;
        String nCadNum = new String("");
        while(Num < NumSaltos) {
            if(newCadNum.charAt(iPIndex) == 0x20) {
                while(newCadNum.charAt(iPIndex) == 0x20)
                    iPIndex++;
                Num++;
            }
            else
                while(newCadNum.charAt(iPIndex) != 0x20)
                    iPIndex++;
        }
        while(newCadNum.charAt(iPIndex) != 0x20) {
            nCadNum = nCadNum + newCadNum.charAt(iPIndex++);
            if(newCadNum.charAt(iPIndex) == 0x20 && newCadNum.charAt(iPIndex + 1) == 0x2A) {
                nCadNum = nCadNum + newCadNum.charAt(iPIndex) + newCadNum.charAt(iPIndex + 1);
                iPIndex += 2;
            }
        }
        return nCadNum;
    }
}							
