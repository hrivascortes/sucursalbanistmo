//*************************************************************************************************
//             Funcion: Bean para captura de datos txn 0082 y 0083
//            Elemento: GerenteD.java
//          Creado por: Alejandro Gonzalez Casttro
//      Modificado por: Juvenal R. Fernandez V.
//*************************************************************************************************
// CCN - 4360178 - 20/08/2004 - Se agregan metodos get y setDescrip para journailzar en el Diario
// CCN - 4360364 - 19/08/2005 - Se activa Control de Efectivo en D�lares
//*************************************************************************************************

package ventanilla.com.bital.sfb;

public class GerenteD extends Header
{
  private String txtCurrCod1 = "";
  private String txtTotalsId = "";
  private String txtDescrip  = "";
  private String txtDivisa   = "";

  public String getCurrCod1()
  {
	return txtCurrCod1 + "*";
  }
  
  public void setCurrCod1(String newCurrCod1)
  {
	txtCurrCod1 = newCurrCod1;
  }
  
  public String getTotalsId()
  {
	return txtTotalsId + "*";
  }
  
  public void setTotalsId(String newTotalsId)
  {
	txtTotalsId = newTotalsId;
  }    
  
  public String getDescrip()
  {
	return txtDescrip + "*";
  }
  
  public void setDescrip(String newDescrip)
  {
	txtDescrip = newDescrip;
  }
  
  public String getFromCurr()
  {
	return txtDivisa + "*";
  }
  
  public void setFromCurr(String newDivisa)
  {
      txtDivisa = newDivisa;
  }
  
  public String toString()
  {
	return super.toString() + "**" + getTotalsId()+ "***" + getCurrCod1()
	  + "****************" ;
  }    
}
