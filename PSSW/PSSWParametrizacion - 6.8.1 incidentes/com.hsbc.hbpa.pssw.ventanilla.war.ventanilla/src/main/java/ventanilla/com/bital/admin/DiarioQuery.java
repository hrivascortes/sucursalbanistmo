//*************************************************************************************************
//             Funcion: Clase que realiza las consultas al Diario
//            Elemento: DiarioQuery.java
//          Creado por: Alejandro Gonzalez Castro
//      Modificado por: Juvenal R. Fernandez V.
//*************************************************************************************************
// CCN - 4360178 - 20/08/2004 - Se habilita guardado en el diario de la txn CON para consultas al Diario
// CCN - 4360189 - 03/09/2004 - Se cambia invoke al Diario para Control de Efectivo
// CCN - 4360204 - 24/09/2004 - Se agrega moneda a la consulta para el Diario Electronico
// CCN - 4360221 - 22/10/2004 - Consultas por Status "C" Cancelada
// CCN - 4360291 - 18/03/2005 - Se unifican las tabas de TW_DIARIO Y TW_DIARIO_RESPUEST
// CCN - 4360296 - 23/03/2005 - Se cambia CCN por peticion de QA, CCN anterior 4360291
// CCN - 4360334 - 17/06/2005 - Se elimina variable dbQualifier
// CCN - 4360364 - 19/08/2005 - Se activa Control de Efectivo en D�lares
// CCN - 4360395 - 21/10/2005 - Se realizan modificaciones para utilizar el D_CONNUREG
// CCN - 4360402 - 11/11/2005 - Se realizan modificaciones para incluir el detalle del en el diario electronico
//*************************************************************************************************
package ventanilla.com.bital.admin; 

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Calendar;
import java.util.Hashtable;
import java.util.Vector;

import javax.servlet.http.HttpSession;

import ventanilla.GenericClasses;
import ventanilla.com.bital.sfb.GerenteD;

import com.bital.util.ConnectionPoolingManager;

public class DiarioQuery
{
    private String txn = "";
    private Object TxnBean = null;
    private String sql = "";
    private String ConsecutivoDiario = "";
    private Vector Tellers = new Vector();
    private Vector Names = new Vector();
	private GenericClasses GC = new GenericClasses();
	
    // Variables para display diario
    private String teller       = "";
    private String typejournal  = "";
    private String order        = "";
    private String currency     = "";
    private int rows            = 0;
    private String account      = "";
    private String amount       = "";
    private String totalC       = "";
    private String totalA       = "";
    private String consecFirst  = "";
    private String consecLast   = "";
    private String dateFirst    = "";
    private String dateLast     = "";
    private String txncode      = "";
    private String status       = "";
    private int page            = 0;
    private int selectrows      = 0;
    private int nopages         = 0;
    private int firstrow        = 0;
    private int lastrow         = 0;
    private int nivel           = 1;
    private String cajerocon    = "";
    private String dirip        = "";

    private Vector ColTxn       = new Vector();
    private Vector ColStatus    = new Vector();
    private Vector ColConsec    = new Vector();
    private Vector ColDate      = new Vector();
    private Vector ColAccount   = new Vector();
    private Vector ColAmount    = new Vector();
    private Vector ColCash      = new Vector();
    private Vector ColTotalC    = new Vector();
    private Vector ColTotalA    = new Vector();
    private Vector ColSerial    = new Vector();
    private Vector ColRef       = new Vector();
    private Vector ColRev       = new Vector();
    private Vector ColConLink   = new Vector();
    private Vector ColMoneda    = new Vector();
    private Vector ColConnureg  = new Vector();

    String PostFixUR = com.bital.util.ConnectionPoolingManager.getPostFixUR();
    // SETS

    public void setIDTellers(Vector cajeros)
    {
        Tellers = cajeros;
    }

    public void setNameTellers(Vector nombres)
    {
        Names = nombres;
    }

    public void setTeller(String cajero)
    {
        teller = cajero;
    }

    public void setTypeJournal(String tipodiario)
    {
        typejournal = tipodiario;
    }

    public void setOrder(String orden)
    {
        order = orden;
    }

    public void setCurrency(String moneda)
    {
        currency = moneda;
    }

    public void setRows(int registros)
    {
        rows = registros;
    }

    public void setAccount(String cuenta)
    {
        account = cuenta;
    }

    public void setAmount(String importe)
    {
        amount = importe;
    }

    public void setTotalC(String TotalC)
    {
        totalC = TotalC;
    }

    public void setTotalA(String TotalA)
    {
        totalA = TotalA;
    }

    public void setConsecFirst(String consecutivoI)
    {
        consecFirst = consecutivoI;
    }

    public void setConsecLast(String consecutivoF)
    {
        consecLast = consecutivoF;
    }

    public void setDateFirst(String dateI)
    {
        dateFirst = dateI;
    }

    public void setDateLast(String dateF)
    {
        dateLast = dateF;
    }

    public void setTxnCode(String codigotxn)
    {
        txncode = codigotxn;
    }

    public void setStatus(String estatus)
    {
        status = estatus;
    }

    public void setColTxn(Vector ColumnTxn)
    {
        ColTxn = ColumnTxn;
    }

    public void setColStatus(Vector ColumnStatus)
    {
        ColStatus = ColumnStatus;
    }

    public void setColConsec(Vector ColumnConsec)
    {
        ColConsec = ColumnConsec;
    }

    public void setColDate(Vector ColumnDate)
    {
        int size = ColumnDate.size();
        for(int i=0; i<size; i++)
        {
            String datetmp = GC.FormatDate((String)ColumnDate.get(i));
            ColumnDate.set(i, datetmp);
        }
        ColDate = ColumnDate;
    }

    public void setColAccount(Vector ColumnAccount)
    {
        ColAccount = ColumnAccount;
    }

    public void setColAmount(Vector ColumnAmount)
    {
        int size = ColumnAmount.size();
        for(int i=0; i<size; i++)
        {
            String montotmp = GC.FormatAmount((String)ColumnAmount.get(i));
            ColumnAmount.set(i, montotmp);
        }
        ColAmount = ColumnAmount;
    }

    public void setColCash(Vector ColumnCash)
    {
        int size = ColumnCash.size();
        for(int i=0; i<size; i++)
        {
            String efectivotmp = GC.FormatAmount((String)ColumnCash.get(i));
            ColumnCash.set(i, efectivotmp);
        }
        ColCash = ColumnCash;
    }

    public void setColTotalC(Vector ColumnTotalC)
    {
        ColTotalC = ColumnTotalC;
    }

    public void setColTotalA(Vector ColumnTotalA)
    {
        ColTotalA = ColumnTotalA;
    }

    public void setColSerial(Vector ColumnSerial)
    {
        ColSerial = ColumnSerial;
    }

    public void setColRef(Vector ColumnRef)
    {
        ColRef = ColumnRef;
    }

    public void setColRev(Vector ColumnRev)
    {
        ColRev = ColumnRev;
    }

    public void setColConLink(Vector ColumnConLink)
    {
        ColConLink = ColumnConLink;
    }

    public void setColMoneda(Vector ColumnMoneda)
    {
        ColMoneda = ColumnMoneda;
    }

    public void setColConnureg(Vector ColumnConnureg)
    {
        ColConnureg = ColumnConnureg;
    }
    public void setPage(int npagina)
    {
        page = npagina;
    }

    public void setSelectRows(int selregs)
    {
        selectrows = selregs;
    }

    public void setNoPages()
    {
        int totalrows = getSelectRows();
        int norows  = getRows();
        int res = totalrows % norows;
        nopages = totalrows / norows;
        if(res > 0)
            nopages++;
    }

    private void setFirstRow()
    {
        firstrow = getPage()*getRows();
    }

    private void setLastRow()
    {
        lastrow = getRows()+getPage()*getRows();
        if( lastrow > getSelectRows())
            lastrow = getSelectRows();
    }

    public void setLevel(String level)
    {
        nivel = Integer.parseInt(level);
    }
    
    public void setTellerQ(String cajeroq)
    {
        cajerocon = cajeroq;
    }

    public void setIP(String ip)
    {
        dirip = ip;
    }

    // GETS

    public Vector getIDTellers()
    {
        return Tellers;
    }

    public Vector getNameTellers()
    {
        return Names;
    }

    public String getTeller()
    {
        return teller;
    }

    public String getTypeJournal()
    {
        if(dateFirst.equals("00000000") && dateLast.equals("00000000"))
        {
            if(typejournal.equals("1"))
                return new String(" AND D_FECHOPER = '"+GC.getDate(1)+"'");
            else
                return new String("");
        }
        else
        {
            typejournal = "2";
            return new String("");
        }
    }

    public String getOrder()
    {
        if(order.equals("1"))
            order = "ASC";
        else if(order.equals("2"))
            order = "DESC";
        else
            order = "D_MONTO DESC";

        if(order.length() > 4)
        {
            return new String(order);
        }
        else
        {    
        if(typejournal.equals("1"))
            return new String("D_CONNUREG " + order);
        else
            return new String("D_FECHOPER " + order +", D_CONNUREG " + order);
    }
    }

    public String getCurrency()
    {
        if(currency.equals("0"))
            return new String("");
        else
            return new String(" AND D_DIVISA = '" + currency + "'");
    }

    public int getRows()
    {
        return rows;
    }

    public String getAccount()
    {
        if(account.length() == 0)
            return new String("");
        else
            return new String(" AND D_CUENTA = '" + account + "'");
    }

    public String getAmount()
    {
        if(amount.length() == 0)
            return new String("");
        else
        {
            amount = delCommaPoint(amount);
            return new String(" AND D_MONTO = " + amount );
        }
    }

    public String getTotalC()
    {
        if(totalC.equals("0"))
            return new String("");
        else if(totalC.equals("1"))
            return new String(" AND (D_CARGO = '"+ totalC + "' OR D_CARGO LIKE '"+totalC+",%')" );
        else
            return new String(" AND D_CARGO LIKE '%"+ totalC + "%'");
    }

    public String getTotalA()
    {
        if(totalA.equals("0"))
            return new String("");
        else
            return new String(" AND D_ABONO LIKE '%"+ totalA + "%'");
    }

    public String getConsecFirst()
    {
        if(consecFirst.length() == 0)
            return new String("");
        else
            return new String(" AND D_CONSECUTIVO >= '" + consecFirst + "'");
    }

    public String getConsecLast()
    {
        if(consecLast.length() == 0)
            return new String("");
        else
            return new String(" AND D_CONSECUTIVO <= '" + consecLast + "'");
    }

    public String getDateFirst()
    {
        if(dateFirst.equals("00000000"))
            return new String("");
        else
            return new String(" AND D_FECHOPER >= '" + dateFirst + "'");
    }

    public String getDateLast()
    {
        if(dateLast.equals("00000000"))
            return new String("");
        else
            return new String(" AND D_FECHOPER <= '" + dateLast + "'");
    }

    public String getTxnCode()
    {
        if(txncode.length() == 0)
            return new String("");
        else
            return new String(" AND D_TXN = '" + txncode + "'");
    }

    public String getStatus()
    {
        if(status.equals("T"))
            return new String("");
        else
            return new String(" AND D_ESTATUS = '" + status + "'");
    }

    public int getPage()
    {
        return page;
    }

    public int getSelectRows()
    {
        return selectrows;
    }

    public int getNoPages()
    {
        return nopages;
    }

    public int getFirstRow()
    {
        return firstrow;
    }

    public int getLastRow()
    {
        return lastrow;
    }

    public Vector getColTxn()
    {
        Vector tmp = new Vector();
        for(int i=getFirstRow(); i<getLastRow(); i++)
            tmp.add(ColTxn.get(i));
        return tmp;
    }

    public Vector getColStatus()
    {
        Vector tmp = new Vector();
        for(int i=getFirstRow(); i<getLastRow(); i++)
            tmp.add(ColStatus.get(i));
        return tmp;
    }

    public Vector getColConsec()
    {
        Vector tmp = new Vector();
        for(int i=getFirstRow(); i<getLastRow(); i++)
            tmp.add(ColConsec.get(i));
        return tmp;
    }

    public Vector getColDate()
    {
        Vector tmp = new Vector();
        for(int i=getFirstRow(); i<getLastRow(); i++)
            tmp.add(ColDate.get(i));
        return tmp;
    }

    public Vector getColAccount()
    {
        Vector tmp = new Vector();
        for(int i=getFirstRow(); i<getLastRow(); i++)
            tmp.add(ColAccount.get(i));
        return tmp;
    }

    public Vector getColAmount()
    {
        Vector tmp = new Vector();
        for(int i=getFirstRow(); i<getLastRow(); i++)
            tmp.add(ColAmount.get(i));
        return tmp;
    }

    public Vector getColCash()
    {
        Vector tmp = new Vector();
        for(int i=getFirstRow(); i<getLastRow(); i++)
            tmp.add(ColCash.get(i));
        return tmp;
    }

    public Vector getColTotalC()
    {
        Vector tmp = new Vector();
        for(int i=getFirstRow(); i<getLastRow(); i++)
            tmp.add(ColTotalC.get(i));
        return tmp;
    }

    public Vector getColTotalA()
    {
        Vector tmp = new Vector();
        for(int i=getFirstRow(); i<getLastRow(); i++)
            tmp.add(ColTotalA.get(i));
        return tmp;
    }

    public Vector getColSerial()
    {
        Vector tmp = new Vector();
        for(int i=getFirstRow(); i<getLastRow(); i++)
            tmp.add(ColSerial.get(i));
        return tmp;
    }

    public Vector getColRef()
    {
        Vector tmp = new Vector();
        for(int i=getFirstRow(); i<getLastRow(); i++)
            tmp.add(ColRef.get(i));
        return tmp;
    }

    public Vector getColRev()
    {
        Vector tmp = new Vector();
        for(int i=getFirstRow(); i<getLastRow(); i++)
            tmp.add(ColRev.get(i));
        return tmp;
    }

    public Vector getColConLink()
    {
        Vector tmp = new Vector();
        for(int i=getFirstRow(); i<getLastRow(); i++)
            tmp.add(ColConLink.get(i));
        return tmp;
    }

    public Vector getColMoneda()
    {
        Vector tmp = new Vector();
        for(int i=getFirstRow(); i<getLastRow(); i++)
            tmp.add(ColMoneda.get(i));
        return tmp;
    }
    
    public Vector getColConnureg()
    {
        Vector tmp = new Vector();
        for(int i=getFirstRow(); i<getLastRow(); i++)
            tmp.add(ColConnureg.get(i));
        return tmp;
    }

    public int getLevel()
    {
        return nivel;
    }

    public String getTellerQ()
    {
        return cajerocon;
    }

    public String getIP()
    {
        return dirip;
    }

    // METHODS


    public String delCommaPoint(String Amount)
    {
        StringBuffer tmp = new StringBuffer(Amount);
        for (int i = 0; i < tmp.length(); i++)
            if (tmp.charAt(i) == ',' || tmp.charAt(i) == '.')
                tmp.deleteCharAt(i);
        return tmp.toString();
    }

    
    public String getBeforeDate(Calendar Fecha, int dias)
    {
        Fecha.add(Calendar.DATE, -(dias));
        String year = String.valueOf(Fecha.get(Calendar.YEAR));
        String month = String.valueOf(Fecha.get(Calendar.MONTH)+1);
        if(month.length() == 1)
            month = "0" + month;
        String day = String.valueOf(Fecha.get(Calendar.DATE));
        if(day.length() == 1)
            day = "0" + day;

        String beforedate = year + month + day;
        Fecha.add(Calendar.DATE, dias);
        return beforedate;
    }

    public Hashtable getData(int pagina)
    {
        setPage(pagina);
        setFirstRow();
        setLastRow();

        Hashtable Data = new Hashtable();
        Data.put("Txn", getColTxn());
        Data.put("Status", getColStatus());
        Data.put("Consec", getColConsec());
        Data.put("Date", getColDate());
        Data.put("Account", getColAccount());
        Data.put("Amount", getColAmount());
        Data.put("Cash", getColCash());
        Data.put("TotalC", getColTotalC());
        Data.put("TotalA", getColTotalA());
        Data.put("Serial", getColSerial());
        Data.put("Ref", getColRef());
        Data.put("Rev", getColRev());
        Data.put("ConLink", getColConLink());
        Data.put("Moneda", getColMoneda());
        Data.put("Connureg", getColConnureg());
        return Data;
    }

    public void getTellers(String sucursal, boolean fuente)
    {
        Connection pooledConnection = ConnectionPoolingManager.getPooledConnection();
        Vector cajeros = new Vector();
        Vector nombres = new Vector();
        ResultSet Rselect = null;
        Statement select = null;
        try
        {
            if(pooledConnection != null)
            {
                select = pooledConnection.createStatement();
                if(fuente)
                    sql = "SELECT C_CAJERO, D_NOMBRE FROM TC_USUARIOS_SUC WHERE C_CAJERO LIKE '" + sucursal+ "%' AND N_NIVEL > 0 AND N_NIVEL < 3 ORDER BY C_CAJERO ASC";
                else
                    sql = "SELECT C_CAJERO, D_NOMBRE FROM TC_USUARIOS_SUC WHERE C_CAJERO LIKE '" + sucursal+ "%' ORDER BY C_CAJERO ASC";
                sql += PostFixUR;
                Rselect = select.executeQuery(sql);

                while(Rselect.next())
                {
                    cajeros.add(Rselect.getString(1).trim());
                    nombres.add(Rselect.getString(2).trim());
                }
            }
        }
        catch(SQLException sqlException)
            {
                System.out.println("Error DiarioQuery::getTellers [" + sqlException + "]");
                System.out.println("Error DiarioQuery::getTellers::SQL ["+sql+"]");
            }
        finally
        {
            try {
                if(Rselect != null) {
                    Rselect.close();
                    Rselect = null;
                }
                if(select != null) {
                    select.close();
                    select = null;
                }
                if(pooledConnection != null) {
                    pooledConnection.close();
                    pooledConnection = null;
                }
            }
            catch(java.sql.SQLException sqlException)
            {
                System.out.println("Error FINALLY DiarioQuery::getTellers [" + sqlException + "]");
                System.out.println("Error FINALLY DiarioQuery::getTellers::SQL ["+sql+"]");
            }
        }
        setIDTellers(cajeros);
        setNameTellers(nombres);
    }

    public void executeQuery(HttpSession session)
    {
        Connection pooledConnection = ConnectionPoolingManager.getPooledConnection();
        Vector coltxn       = new Vector();
        Vector colstatus    = new Vector();
        Vector colconsec    = new Vector();
        Vector colfecha     = new Vector();
        Vector colcuenta    = new Vector();
        Vector colmonto     = new Vector();
        Vector colefectivo  = new Vector();
        Vector colcargo     = new Vector();
        Vector colabono     = new Vector();
        Vector colserial    = new Vector();
        Vector coltxnrev    = new Vector();
        Vector colconliga   = new Vector();
        Vector colref       = new Vector();
        Vector colmoneda    = new Vector();
        Vector colconnureg  = new Vector();
        
        ResultSet Rselect = null;
        Statement select = null;
        
        if(getLevel() > 1 && !(getTeller().equals(getTellerQ())))
        {
            GerenteD GD = new GerenteD();
            GD.setTxnCode("CON ");
            GD.setBranch(getTeller().substring(0,4));
            GD.setTeller(getTellerQ());
            GD.setSupervisor(getTellerQ());
            GD.setTotalsId(getTeller());
            GD.setCurrCod1("CONSULTA DIARIO DE CAJERO");
            GD.setFromCurr("N$");
            String consecliga =  stringFormat(java.util.Calendar.HOUR_OF_DAY, 2) + stringFormat(java.util.Calendar.MINUTE, 2) + stringFormat(java.util.Calendar.SECOND, 2);
            Diario diario = new Diario();
            String sMessage = diario.invokeDiario(GD, consecliga, getIP(), "01", session);
        }
        
        try
        {
            if(pooledConnection != null)
            {
                select = pooledConnection.createStatement();
                sql = "SELECT D_TXN, D_ESTATUS, D_CONSECUTIVO, D_FECHOPER, D_CUENTA, ";
                sql = sql + "D_MONTO, D_EFECTIVO, D_CARGO, D_ABONO, D_SERIAL, ";
                sql = sql + "C_TXN_REVERSABLE, D_CONSECLIGA, ";
                sql = sql + "D_DESCRIP, D_REFER, D_REFER1, D_REFER2, D_REFER3, ";
                sql = sql + "D_DIVISA, D_CONNUREG ";
                sql = sql + "FROM TW_DIARIO_ELECTRON WHERE ";
                sql = sql + "D_OPERADOR = '" + getTeller() + "'";
                sql = sql + " AND C_TXN_VISIBLE = 'S'";
                sql = sql + getTypeJournal();
                sql = sql + getCurrency();
                sql = sql + getAccount();
                sql = sql + getAmount();
                sql = sql + getTotalC();
                sql = sql + getTotalA();
                sql = sql + getConsecFirst();
                sql = sql + getConsecLast();
                sql = sql + getDateFirst();
                sql = sql + getDateLast();
                sql = sql + getTxnCode();
                sql = sql + getStatus();
                sql = sql + " ORDER BY " + getOrder() + PostFixUR;

                //System.out.println("SQL ["+sql+"]");
                Rselect = select.executeQuery(sql);

                String tmp = "";
                String sabado = "";
                String domingo = "";

                String FechaSys = GC.getDate(1);
                
                Calendar date = Calendar.getInstance();
                // int dia de la semana  1 2 3 4 5 6 7
                int today = date.get(Calendar.DAY_OF_WEEK);
                
                if(today == 1) // Domingo
                {
                    sabado = getBeforeDate(date, 1);
                }
                else if(today == 2) // Lunes
                {
                    sabado = getBeforeDate(date, 2);
                    domingo = getBeforeDate(date, 1);
                }
                
                while(Rselect.next())
                {
                    coltxn.add(Rselect.getString("D_TXN").trim());
                    colstatus.add(Rselect.getString("D_ESTATUS").trim());
                    colconsec.add(Rselect.getString("D_CONSECUTIVO").trim());
                    colfecha.add(Rselect.getString("D_FECHOPER").trim());
                    colcuenta.add(Rselect.getString("D_CUENTA").trim());
                    colmonto.add(Rselect.getString("D_MONTO").trim());
                    colefectivo.add(Rselect.getString("D_EFECTIVO").trim());
                    colcargo.add(Rselect.getString("D_CARGO").trim());
                    colabono.add(Rselect.getString("D_ABONO").trim());
                    colserial.add(Rselect.getString("D_SERIAL").trim());
       
                    String fechaDB = Rselect.getString("D_FECHOPER").trim();
                    
                    if(today == 1) // Domingo
                    {
                        if(fechaDB.equals(FechaSys) || fechaDB.equals(sabado))
                            coltxnrev.add(Rselect.getString("C_TXN_REVERSABLE").trim());
                        else
                            coltxnrev.add("N");
                    }

                    else if(today == 2) // Lunes
                    {
                        if(fechaDB.equals(FechaSys) || fechaDB.equals(sabado) || fechaDB.equals(domingo))
                            coltxnrev.add(Rselect.getString("C_TXN_REVERSABLE").trim());
                        else
                            coltxnrev.add("N");
                    }
                    else
                    {
                        if(fechaDB.equals(FechaSys))
                            coltxnrev.add(Rselect.getString("C_TXN_REVERSABLE").trim());
                        else
                            coltxnrev.add("N");
                    }
                        
                    colconliga.add(Rselect.getString("D_CONSECLIGA").trim());

                    tmp = Rselect.getString("D_DESCRIP").trim();
                    if(tmp.length()==0)
                        tmp = Rselect.getString("D_REFER").trim();
                    if(tmp.length()==0)
                        tmp = Rselect.getString("D_REFER1").trim();
                    if(tmp.length()==0)
                        tmp = Rselect.getString("D_REFER2").trim();
                    if(tmp.length()==0)
                        tmp = Rselect.getString("D_REFER3").trim();
                    colref.add(tmp);
                    colmoneda.add(Rselect.getString("D_DIVISA").trim());
                    colconnureg.add(Rselect.getString("D_CONNUREG").trim());
                }
            }
        }
        catch(SQLException sqlException)
            {
                System.out.println("Error DiarioQuery::executeQuery [" + sqlException + "]");
                System.out.println("Error DiarioQuery::executeQuery::SQL ["+sql+"]");
            }
        finally
        {
            try {
                if(Rselect != null) {    
                    Rselect.close();
                    Rselect = null;
                }
                if(select != null) {
                    select.close();
                    select = null;
                }
                if(pooledConnection != null) {
                    pooledConnection.close();
                    pooledConnection = null;
                }
            }
            catch(java.sql.SQLException sqlException)
            {
                System.out.println("Error FINALLY DiarioQuery::executeQuery [" + sqlException + "]");
                System.out.println("Error FINALLY DiarioQuery::executeQuery::SQL ["+sql+"]");
            }
        }

        setSelectRows(coltxn.size());
        setColTxn(coltxn);
        setColStatus(colstatus);
        setColConsec(colconsec);
        setColDate(colfecha);
        setColAccount(colcuenta);
        setColAmount(colmonto);
        setColCash(colefectivo);
        setColTotalC(colcargo);
        setColTotalA(colabono);
        setColSerial(colserial);
        setColRef(colref);
        setColRev(coltxnrev);
        setColConLink(colconliga);
        setColMoneda(colmoneda);
        setColConnureg(colconnureg);
        setNoPages();
    }
    
  private String stringFormat(int option, int NumDig)
  {
    java.util.Calendar now = java.util.Calendar.getInstance();
    String temp = new Integer(now.get(option)).toString();
    for(int i = temp.length(); i < NumDig; i++)
     temp = "0" + temp;

    return temp;
  } 
}
