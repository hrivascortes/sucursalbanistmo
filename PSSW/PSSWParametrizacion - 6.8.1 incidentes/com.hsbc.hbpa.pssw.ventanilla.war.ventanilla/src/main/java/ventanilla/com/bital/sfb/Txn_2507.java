//*************************************************************************************************
//			 Funcion: Clase que realiza txn Abono a DDA 2507
//			Elemento: Txn_2507.java
//		  Creado por: Hugo Gabriel Rivas Cortes
//*************************************************************************************************
//CR - 13700 - 05/04/2005 - Creacion de elemento
//*************************************************************************************************
package ventanilla.com.bital.sfb;

import ventanilla.GenericClasses;

import com.bital.util.ParameterParser;
import com.bital.util.ParameterNotFoundException;

public class Txn_2507 extends Transaction
{
  public Header doAction(ParameterParser param)
    throws ParameterNotFoundException
  {
	  
	  GenericClasses gc = new GenericClasses();
	  FormatoA format = new FormatoA();

      format.setBranch(param.getString("sucursal"));
      format.setTeller(param.getString("teller"));
      format.setSupervisor(param.getString("supervisor"));
      format.setMoAmoun("000");
      format.setHolDays("0");
      format.setHolDays2("0");

      if ( param.getString("supervisor") != null )
           format.setSupervisor(param.getString("supervisor"));

      format.setTxnCode("2507");
      String suc = param.getString("sucursal");
  	   format.setAcctNo(param.getString("txtDebitoPF"));
        
      format.setTranAmt(param.getCString("txtMonto"));
      String moneda = param.getString("moneda");
		format.setTranCur(gc.getDivisa(moneda));
      
      return format;
  }               
}
