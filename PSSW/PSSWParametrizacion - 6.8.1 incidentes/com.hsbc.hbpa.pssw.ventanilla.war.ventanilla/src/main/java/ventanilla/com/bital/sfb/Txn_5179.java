//*************************************************************************************************
//             Funcion: Bean Txn 5179
//      Creado por: Jesus Emmanuel Lopez Rosales
//                  Yair Jesus Chavez Antonel 
//*************************************************************************************************
// CCN - 4360533 - 20/10/2006 - Se crea bean para proyecto OPEE
//*************************************************************************************************
package ventanilla.com.bital.sfb;

import com.bital.util.ParameterParser;
import com.bital.util.ParameterNotFoundException;
import ventanilla.GenericClasses;

public class Txn_5179 extends Transaction
{
	public static String tipoCambio = "";
	public Header doAction(ParameterParser param)
		throws ParameterNotFoundException
	{		
		FormatoA formatoa = new FormatoA();
		GenericClasses gc = new GenericClasses();		
		
		formatoa.setFormat("A");
		formatoa.setAcctNo(param.getString("txtDDACuenta"));
		formatoa.setTranAmt(param.getCString("txtMonto"));		
		String moneda = gc.getDivisa(param.getString("moneda"));
		formatoa.setTranCur(moneda);				
		tipoCambio = param.getCString("txtTipoCambio1");
		//tipoCambio = gc.cutString(tipoCambio, 3, false);		
		//formatoa.setDraftAm(tipoCambio);
		formatoa.setDraftAm(param.getString("txtPlazaSuc"));
		formatoa.setMoAmoun(param.getCString("txtMontoEnv"));		
		formatoa.setAcctNo2(gc.StringFiller(param.getString("txtFolioIT5"),5,false,"0") + param.getString("lstCatDivisa"));
       	
		return formatoa;
	}
}

                 