//*************************************************************************************************
//             Funcion: Clase que controla el acceso al sistema de los usuarios
//          Creado por: Rutilo Zarco Reyes
//      Modificado por: Fausto Rodrigo Flores Moreno
//*************************************************************************************************
// CCN - 4360258- 13/01/2005 - Se permite acceso a usuarios de banca premier BPXXXXXX
// CCN - 4360291- 18/03/2005 - Se unifican las tabas de TW_DIARIO Y TW_DIARIO_RESPUEST
// CCN - 4360296 - 23/03/2005 - Se cambia CCN por peticion de QA, CCN anterior 4360291
// CCN - 4360315 - 06/05/2005 - Se corrige error de validacion para ambiente de capacitaci�n
// CCN - 4360334 - 17/06/2005 - Se elimina variable dbQualifier
// CCN - 4360336 - 17/06/2005 - Se cambia la obtenci�n de variables de ambiente
// CCN - 4360347 - 22/07/2005 - Validar usuario capturado de RACF(Auditoria)
// CCN - 4360361 - 05/08/2005 - Corregir error de Insert a TW_VENTA_LOG
// CCN - 4360364 - 19/08/2005 - Se modifica el control de acceso y se almacena el usuario de GTE la primera vez.
// CCN - 4360395 - 21/10/2005 - Se realizan modificaciones para poder que usuarios de RACF DG y DP puedan accesar.
// CCN - 4360589 - 16/04/2007 - Se realizan modificaciones para el monitoreo de efectivo en las sucursales
// CCN - 4360640 - 12/07/2007 - Se cambia mensaje de alerta cuando el usuario ha sido bloqueado
//*************************************************************************************************

package ventanilla.com.bital.util; 
import java.util.Vector;
import ventanilla.com.bital.beans.DatosVarios;
import ventanilla.GenericClasses;
import com.bital.util.*;
import java.sql.*;

public class AdminUsers 
{
    String statementPS = com.bital.util.ConnectionPoolingManager.getStatementPostFix();
    String PSUR = com.bital.util.ConnectionPoolingManager.getPostFixUR();   
	private GenericClasses GC = new GenericClasses();
	private String ambiente;
    private javax.naming.Context initialContext = null;
    private String RACF = "";
	
    public AdminUsers() 
    {
    	try
    	{
    		initialContext = new javax.naming.InitialContext();
    		ambiente = (String)initialContext.lookup("java:comp/env/AMB");
    	}
    	catch( javax.naming.NamingException namingException ){
            System.out.println( "Error ConnectionPoolingManager::Context.lookup: [" + namingException.toString() + "]");
        }
    }
    
    public void setRACF(String newRACF)
    {
    	RACF = newRACF.toUpperCase();
    }
    
    public String getRACF()
    {	
		return RACF;
    }
    
    
    public java.util.Hashtable getUser(String newUser, String newPassword, String newIP, String newSchedule, String ConfPass, String newEmpno, String Cambio, String newRACF, String band) {
        java.util.Vector dbAccessMssgStatus = new java.util.Vector();
        java.util.Hashtable userInfo = new java.util.Hashtable();
        int dbAccessStatus = 1;
        java.sql.PreparedStatement newPreparedStatement = null;
        boolean cambiopsswd = false;
        setRACF(newRACF);
        if(!ConfPass.equals("X")) 
        {
            // Cambio de password
            java.sql.Connection pooledConnection = com.bital.util.ConnectionPoolingManager.getPooledConnection( );
            try 
            {
                if(pooledConnection != null) 
                {
					newPreparedStatement = pooledConnection.prepareStatement("UPDATE TC_USUARIOS_SUC SET D_CLAVE_ACC ='"+ConfPass+"', D_FECHA='"+GC.getDate(8)+"' WHERE c_Cajero = ?" + statementPS);
                    newPreparedStatement.setString(1,newUser);
                    int updateUser = newPreparedStatement.executeUpdate();
                    if(updateUser > 0)
                    {
                        cambiopsswd =  true;
                    }
                }
                else
                    userInfo.put("ERROR_DATOS", "No hay conexiones disponibles para la aplicaci&oacute;n. Por favor intente despu&eacute;s.");
            }
            catch(java.sql.SQLException sqlException) 
                {System.out.println("Error AdminUsers::getUser: [" + sqlException.toString() + "] ");}
            finally 
            {
                try {
                    if(newPreparedStatement != null) {
                        newPreparedStatement.close();
                        newPreparedStatement = null;
                    }
                    if(pooledConnection != null){
                        pooledConnection.close();
                        pooledConnection = null;
                    }
                }
                catch(java.sql.SQLException sqlException){
                    System.out.println("Error AdminUsers::getUser FINALLY: [" + sqlException.toString() + "] ");
                }
            }
            
            String sql = "INSERT INTO TW_VENTA_LOG VALUES(?,?,?,?,?,?,?,?,?) ";
            pooledConnection = ConnectionPoolingManager.getPooledConnection();
            PreparedStatement insert = null;
            try {
                if(pooledConnection != null) 
                {
                    insert = pooledConnection.prepareStatement(sql);
                    insert.setString(1,newUser.substring(0,4));
                    insert.setString(2,newUser);
                    insert.setString(3,newRACF);                    
                    insert.setString(4,GC.getDate(1));
                    insert.setString(5,GC.getDate(3));
                    insert.setString(6,"CAMBIO CONTRASENA-");
                    insert.setString(7,newUser);
					insert.setString(8,"********");                    					       
					insert.setString(9,ConfPass);                    					       
                    insert.executeUpdate();

                }
            }
            catch(SQLException sqlException)
            {
            	sql = "INSERT INTO TW_VENTA_LOG VALUES('"+newUser.substring(0,4)+"','"+newUser+"','"+newRACF+"','"+GC.getDate(1)+"','"+GC.getDate(3)+"','CAMBIO CONTRASE�A-','"+newUser+"','********','"+ConfPass+" )";
            	System.out.println("Error AdminUsers::getUserC [" + sql + "]");
            	System.out.println("Error AdminUsers::getUserC [" + sqlException + "]");}
            finally {
                try {
                    if(insert != null) {
                        insert.close();
                        insert = null;
                    }
                    if(pooledConnection != null) {
                        pooledConnection.close();
                        pooledConnection = null;
                    }
                }
                catch(java.sql.SQLException sqlException)
                {System.out.println("Error FINALLY DBUsers::M013C [" + sqlException + "]");}
            }
        }
        
        java.sql.ResultSet queryUser = null;
        java.sql.Connection pooledConnection = com.bital.util.ConnectionPoolingManager.getPooledConnection( );
        try {
            if(pooledConnection != null) {
                newPreparedStatement =
                pooledConnection.prepareStatement("SELECT * FROM tc_Usuarios_Suc WHERE c_Cajero = ?" + PSUR);
                newPreparedStatement.setString(1,newUser);
                queryUser = newPreparedStatement.executeQuery();
                if(queryUser.next()) {
                    ventanilla.com.bital.util.QueryDBColumnsPerRow newColumnsPerRow = new ventanilla.com.bital.util.QueryDBColumnsPerRow(queryUser);
                    newColumnsPerRow.execute();
                    userInfo = newColumnsPerRow.getColumns();
                }
            }
            else
                userInfo.put("ERROR_DATOS", "No hay conexiones disponibles para la aplicaci&oacute;n. Por favor intente despu&eacute;s.");
        }
        catch(java.sql.SQLException sqlException){
            System.out.println("Error AdminUsers::getUser: [" + sqlException.toString() + "] ");
        }
        finally {
            try {
                if(queryUser != null){
                    queryUser.close();
                    queryUser = null;
                }
                if(newPreparedStatement != null){
                    newPreparedStatement.close();
                    newPreparedStatement = null;
                }
                if(pooledConnection != null){
                    pooledConnection.close();
                    pooledConnection = null;
                }
            }
            catch(java.sql.SQLException sqlException){
                System.out.println("Error AdminUsers::getUser FINALLY: [" + sqlException.toString() + "] ");
            }
        }
        
        dbAccessMssgStatus.add("");
        dbAccessMssgStatus.add("Usuario " + newUser + " no registrado. Intente de nuevo.");
        dbAccessMssgStatus.add("Contrase&ntilde;a inv&aacute;lida para el usuario " + newUser + ". Intente de nuevo.");
        dbAccessMssgStatus.add("Usuario " + newUser + " firmado en otra terminal. Intente despu&eacute;s.");
        dbAccessMssgStatus.add("El Usuario " + newUser + " requiere cambio de Contrase&ntilde;a.");
        dbAccessMssgStatus.add("La Contrase&ntilde;a del usuario ha expirado, por favor cambiela.");
        dbAccessMssgStatus.add("El Usuario " + newUser + " solicit&oacute; cambio de Contrase&ntilde;a.");
        dbAccessMssgStatus.add("Usuario " + newUser + " Revocado. Solicite reactivaci&oacute;n al Gerente.");
        dbAccessMssgStatus.add("La terminal de la cual intenta accesar, no se encuentra registrada para esta Sucursal.");
        dbAccessMssgStatus.add("El Cajero " + newUser + " intenta firmarse en otra Sucursal. Acceso denegado");
        dbAccessMssgStatus.add("El nivel de operaci&oacute;n del Cajero " + newUser +" no corresponde con el usuario de RACF. Acceso denegado");
		dbAccessMssgStatus.add("Haz excedido tu l�mite de pesos y/o d�lares en ventanilla, tu usuario ha sido bloqueado,<br>espera llamada del area facultada para desbloquearte");//para control de efectivo
        
        if(userInfo != null && !userInfo.isEmpty()) 
        {
            String dbUserName   = (String)userInfo.get("C_CAJERO");
            String dbUserPsswd  = (String)userInfo.get("D_CLAVE_ACC");
            String dbUserStatus = (String)userInfo.get("D_SIGN_STATUS");
            String dbUserIPAdd  = (String)userInfo.get("D_DIRECCION_IP");
            String dbUserROL    = (String)userInfo.get("N_NIVEL");
            String dbUserDate   = (String)userInfo.get("D_FECHA");
            String dbUserIntentos = (String)userInfo.get("N_SIGN_INTENTOS");
                          
            java.util.Calendar sysdate = java.util.Calendar.getInstance();
            sysdate.add(java.util.Calendar.DATE, -30);
            java.util.Calendar dbdate = java.util.Calendar.getInstance();
            
            if(dbUserDate.trim().length() == 0)
                dbUserDate = "00000000";
            
            int year = Integer.parseInt(dbUserDate.substring(4,8));
            int month = Integer.parseInt(dbUserDate.substring(2,4));
            month = month -1;
            int day = Integer.parseInt(dbUserDate.substring(0,2));
            dbdate.set(year, month, day);
            boolean change = false;
            if(sysdate.after(dbdate))
                change = true;
            
            if(dbUserIPAdd.length() > 0)
                dbUserIPAdd = dbUserIPAdd.trim();
            
            String suc = dbUserName.substring(0,4);
            int IP =  getIP(newIP,suc);
            
            if(!dbUserName.equals(newUser))
            {
                // Usuario NO existe en tabla DB2
                dbAccessStatus = 1;
            }
            else if(IP == 0) 
            {
                // Dir IP NO existe en DB2
                dbAccessStatus = 8;
            }
            else if(IP == 2) 
            {
                // El cajero intenta firmase desde un terminal asignada a otra sucursal
                dbAccessStatus = 9;
            }
            else if(!Verifica_Nivel(newRACF,dbUserROL))
            {
                // El Usuario RACF no corresponde al Nivel de Operacion del Cajero
                dbAccessStatus = 10;
            }
 
            else if(dbUserStatus.equals("R") || dbUserIntentos.equals("3")) 
            {
                // El cajero esta revocado por estatus o por intentos fallidos
                dbAccessStatus = 7;
            }
            else if(dbUserStatus.equals("B"))//para control efectivo
            {
            	//El cajero esta bloqueado por no realizar concentraci�n
            	dbAccessStatus = 11;
            }
            else if(!dbUserPsswd.equals(newPassword))
            {
                // Password invalido y contabilizacion de intentos de acceso
                int nintentos = setIntentos(dbUserIntentos, newUser);
                if(nintentos == 3)
                    dbAccessStatus = 7;
                else
                    dbAccessStatus = 2;
            }
            else if(dbUserPsswd.equals("12345678"))
            {
                // Cajero requiere cambio de Password por Reactivacion
                dbAccessStatus = 4;
            }    
            else if(change)
            {
                // Cajero requiere cambio de Password por Expiracion
                dbAccessStatus = 5;
            }
            else if(Cambio.equals("S")) 
            {
                // Cajero solicito cambio de Password
                dbAccessStatus = 6;
            }
            else if(dbUserStatus.equals("F") && !dbUserIPAdd.equals(newIP))
            {
                // Cajero firmado en otra terminal
                dbAccessStatus = 3;
            }
            else
            {
                // Todo OK
                setUserIP(newUser, newIP, newSchedule, newEmpno,band);
                dbAccessStatus = 0;
            }
           
        }
        
        if(dbAccessStatus != 0)
            userInfo.put("ERROR_DATOS", dbAccessMssgStatus.elementAt(dbAccessStatus));
        
        if(cambiopsswd)
            userInfo.put("CambioOK", "S");

        return (java.util.Hashtable) userInfo;
    }

    public int setIntentos(String Intentos, String teller) 
    {
        java.sql.Connection pooledConnection = com.bital.util.ConnectionPoolingManager.getPooledConnection( );
        java.sql.PreparedStatement newPreparedStatement = null;
        int updatedRecord = 0;
        int noIntentos = Integer.parseInt(Intentos);
        noIntentos++;
        String Anterior ="";
        String Posterior ="";
        
        try{
            if(pooledConnection != null)
            {
                if(noIntentos == 3)
                    newPreparedStatement =  pooledConnection.prepareStatement("UPDATE tc_Usuarios_Suc SET d_sign_status='R', n_sign_intentos= ? where c_Cajero = ?" + statementPS);
                else
                    newPreparedStatement =  pooledConnection.prepareStatement("UPDATE tc_Usuarios_Suc SET n_sign_intentos= ? where c_Cajero = ?" + statementPS);
                newPreparedStatement.setInt(1, noIntentos);
                newPreparedStatement.setString(2,teller);
                
                int irs = newPreparedStatement.executeUpdate();
            }
            else
                updatedRecord = 1;
        }
        catch(java.sql.SQLException sqlException){
            System.out.println("Error AdminUsers::setIntentos: [" + sqlException.toString() + "]");
            updatedRecord = 1;
        }
        finally{
            try{
                if(newPreparedStatement != null){
                    newPreparedStatement.close();
                    newPreparedStatement = null;
                }
                if(pooledConnection != null){
                    pooledConnection.close();
                    pooledConnection = null;
                }
            }
            catch(java.sql.SQLException sqlException){
                System.out.println("Error AdminUsers::setIntentos FINALLY: [" + sqlException.toString() + "] ");
            }
        }
        if(noIntentos == 3)
        {
        	Intentos = "REVOCA USUARIO-";
        	Anterior = "ESTATUS N";
        	Posterior = "ESTATUS R";
        }
        else
        {
        	Anterior = Intentos;
        	Intentos = "Intento: "+Intentos;
        	Posterior = String.valueOf(noIntentos);
        }	
        	String sql = "INSERT INTO TW_VENTA_LOG VALUES(?,?,?,?,?,?,?,?,?) ";
            pooledConnection = ConnectionPoolingManager.getPooledConnection();
            PreparedStatement insert = null;
            try {
                if(pooledConnection != null) 
                {
                    insert = pooledConnection.prepareStatement(sql);
                    insert.setString(1,teller.substring(0,4));
                    insert.setString(2,teller);
                    insert.setString(3,getRACF());                    
                    insert.setString(4,GC.getDate(1));
                    insert.setString(5,GC.getDate(3));
                    insert.setString(6,Intentos);
                    insert.setString(7,teller);
					insert.setString(8,Anterior);                    					       
					insert.setString(9,Posterior);                    					       
                    insert.executeUpdate();

                }
            }
            catch(SQLException sqlException)
            {
            	sql = "INSERT INTO TW_VENTA_LOG VALUES('"+teller.substring(0,4)+"','"+teller+"','"+getRACF()+"','"+GC.getDate(1)+"','"+GC.getDate(3)+"','"+Intentos+"'"+teller+"','"+Anterior+"','"+Posterior+"' )";
            	System.out.println("Error AdminUsers::setIntentosC [" + sql + "]");
            	System.out.println("Error AdminUsers::setIntentosC [" + sqlException + "]");}
            finally {
                try {
                    if(insert != null) {
                        insert.close();
                        insert = null;
                    }
                    if(pooledConnection != null) {
                        pooledConnection.close();
                        pooledConnection = null;
                    }
                }
                catch(java.sql.SQLException sqlException)
                {System.out.println("Error FINALLY AdminUsers::setIntentosC  [" + sqlException + "]");}
            }
        
        
        return noIntentos;
    }
    
    public int setUserIP(String newUser, String newIP, String newSchedule, String newEmpNo, String band) {
        java.sql.Connection pooledConnection = com.bital.util.ConnectionPoolingManager.getPooledConnection( );
        java.sql.PreparedStatement newPreparedStatement = null;
        int updatedRecord = 0;
        
        
        try{
            if(pooledConnection != null){
            	if(band.equalsIgnoreCase("S"))
            	{
             		newPreparedStatement = pooledConnection.prepareStatement("UPDATE tc_Usuarios_Suc SET d_Direccion_IP = ?, d_Horario = ?, d_registro = ?, d_ejct_cta = ?, d_Sign_Status = 'F', n_sign_intentos=0 where c_Cajero = ?" + statementPS);
            		newPreparedStatement.setString(1,newIP);
        	    	newPreparedStatement.setString(2,newSchedule);
    	        	newPreparedStatement.setString(3,newEmpNo);
                	newPreparedStatement.setString(4,getRACF());
                	newPreparedStatement.setString(5,newUser);
	                int irs = newPreparedStatement.executeUpdate();
            	}
            	else
            	{
					newPreparedStatement = pooledConnection.prepareStatement("UPDATE tc_Usuarios_Suc SET d_Direccion_IP = ?, d_Horario = ?, d_registro = ?, d_Sign_Status = 'F', n_sign_intentos=0 where c_Cajero = ?" + statementPS);
            		newPreparedStatement.setString(1,newIP);
        	    	newPreparedStatement.setString(2,newSchedule);
    	        	newPreparedStatement.setString(3,newEmpNo);
                	newPreparedStatement.setString(4,newUser);
  	                int irs = newPreparedStatement.executeUpdate();
            	}
                
            }
            else
                updatedRecord = 1;
        }
        catch(java.sql.SQLException sqlException){
            System.out.println("Error AdminUsers::setUserIP: [" + sqlException.toString() + "]");
            updatedRecord = 1;
        }
        finally{
            try{
                if(newPreparedStatement != null){
                    newPreparedStatement.close();
                    newPreparedStatement = null;
                }
                if(pooledConnection != null){
                    pooledConnection.close();
                    pooledConnection = null;
                }
            }
            catch(java.sql.SQLException sqlException){
                System.out.println("Error AdminUsers::setUserIP FINALLY: [" + sqlException.toString() + "] ");
            }
        }
        
        return updatedRecord;
    }
    
    public java.util.Hashtable getBranchInfo(String newUser) {
        java.sql.Connection pooledConnection = com.bital.util.ConnectionPoolingManager.getPooledConnection( );
        java.sql.PreparedStatement newPreparedStatement = null;
        java.sql.ResultSet queryBranch = null;
        java.util.Hashtable branchInfo = new java.util.Hashtable();
        
        try{
            if(pooledConnection != null){
                newPreparedStatement = pooledConnection.prepareStatement("SELECT * FROM tc_Info_Sucursal WHERE c_Num_Sucursal = ?" + PSUR);
                newPreparedStatement.setString(1,newUser.substring(0,4));
                queryBranch = newPreparedStatement.executeQuery();
                if(queryBranch.next())
                {
                    ventanilla.com.bital.util.QueryDBColumnsPerRow newColumnsPerRow = new ventanilla.com.bital.util.QueryDBColumnsPerRow(queryBranch);
                    newColumnsPerRow.execute();
                    branchInfo = newColumnsPerRow.getColumns();
                }
                else
                    branchInfo.put("ERROR_DATOS", "No existe informaci&oacute;n registrada de la sucursal " + newUser.substring(0,4) + ".");
            }
            else{
                branchInfo.put("ERROR_DATOS", "No hay conexiones disponibles para la aplicaci&oacute;n. Por favor intente despu&eacute;s.");
            }
        }
        catch(java.sql.SQLException sqlException){
            branchInfo.put("ERROR_DATOS", sqlException.toString());
            System.out.println("Error AdminUsers::getBranchInfo: [" + sqlException.toString() + "] ");
        }
        finally{
            try{
                if(queryBranch != null){
                    queryBranch.close();
                    queryBranch = null;
                }
                if(newPreparedStatement != null){
                    newPreparedStatement.close();
                    newPreparedStatement = null;
                }
                if(pooledConnection != null){
                    pooledConnection.close();
                    pooledConnection = null;
                }
            }
            catch(java.sql.SQLException sqlException){
                System.out.println("Error AdminUsers::getBranchInfo FINALLY: [" + sqlException.toString() + "] ");
            }
        }
        return branchInfo;
    }
    
    public java.util.Hashtable getMontTruncamiento() 
    {
        java.util.Hashtable Truncamiento = new java.util.Hashtable();
        DatosVarios datosv = DatosVarios.getInstance();
        Vector Rselect = datosv.getDatosV("IMPTRUNCAM");        
               
        if(!(Rselect.isEmpty()))
        {
            String Montos = (String)Rselect.get(1);
            java.util.StringTokenizer MontosTrun = new java.util.StringTokenizer(Montos,"*");
            
            if (MontosTrun.countTokens()==2) 
            {
                Truncamiento.put("TruncamientoMN",MontosTrun.nextToken());
                Truncamiento.put("TruncamientoUSD",MontosTrun.nextToken());
            }
        }
        else
          Truncamiento.put("ERROR_DATOS", "No existe informaci&oacute;n de montos truncados " );
        
        return Truncamiento;
    }
    
    public int getIP(String IP, String Suc) {

        ambiente= ambiente.toUpperCase();
		if(ambiente.equals("D") || ambiente.equals("C"))
			return 1;
        java.sql.Connection pooledConnection = com.bital.util.ConnectionPoolingManager.getPooledConnection( );
        java.sql.PreparedStatement newPreparedStatement = null;
        java.sql.ResultSet query = null;
        int result = 0;
        
        try{
            if(pooledConnection != null) {
                newPreparedStatement = pooledConnection.prepareStatement("SELECT D_NUM_SUCURSAL FROM TC_IP_SUCURSALES WHERE  C_DIRECCION_IP = ? "+PSUR);
                newPreparedStatement.setString(1,IP);
                
                query = newPreparedStatement.executeQuery();
                if(query.next()) {
                    result = 1;
                    String sucursal = query.getString("D_NUM_SUCURSAL");
                    if(!Suc.equals(sucursal))
                        result = 2;
                }
                else
                    result = 0;
            }
        }
        catch(java.sql.SQLException sqlException) {
            System.out.println("Error AdminUsers::getIP: [" + sqlException.toString() + "] ");
        }
        finally {
            try{
                if(query != null){
                    query.close();
                    query = null;
                }
                if(newPreparedStatement != null){
                    newPreparedStatement.close();
                    newPreparedStatement = null;
                }
                if(pooledConnection != null){
                    pooledConnection.close();
                    pooledConnection = null;
                }
            }
            catch(java.sql.SQLException sqlException){
                System.out.println("Error AdminUsers::getIP FINALLY: [" + sqlException.toString() + "] ");
            }
        }
        return result;
    }
    

    public boolean Verifica_Nivel(String newRACF, String dbUserROL)
    {
        ambiente= ambiente.toUpperCase();
        if(ambiente.equals("D") || ambiente.equals("C"))
                 return true;
                  
        boolean tipo= false;
        newRACF = newRACF.toUpperCase();
        
        if( (dbUserROL.equals("1")||dbUserROL.equals("2")) && (newRACF.startsWith("I")) )
            tipo = true;
        else if( (dbUserROL.equals("3")||dbUserROL.equals("4")) && (newRACF.startsWith("SP") || newRACF.startsWith("SG")|| newRACF.startsWith("BP") || newRACF.startsWith("DP") || newRACF.startsWith("DG") ))
            tipo = true;
        
        return tipo;
    }
}
