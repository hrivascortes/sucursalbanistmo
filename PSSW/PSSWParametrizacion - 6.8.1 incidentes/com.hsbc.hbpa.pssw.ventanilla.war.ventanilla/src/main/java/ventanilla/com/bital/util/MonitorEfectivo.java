//*************************************************************************************************
//             Funcion: Clase que realiza el monitoreo de sucursales para PIF
//            Elemento: MonitorEfectivo.java
//          Creado por: Fausto Rodrigo Flores Morneo
//*************************************************************************************************
// CCN - 4360587 - 03/04/2007 - Se agrega para el monitoreo de efectivo
//*************************************************************************************************

package ventanilla.com.bital.util;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.Vector;

import ventanilla.GenericClasses;

import com.bital.util.ConnectionPoolingManager;


public class MonitorEfectivo{
private String sql = "";

  public MonitorEfectivo() {
  }
  
  public Vector getBloqueados(){
	Vector result= new Vector();
	String strRow = "";
	Connection pooledConnection = ConnectionPoolingManager.getPooledConnection();
	String PSUR = ConnectionPoolingManager.getPostFixUR();   
	PreparedStatement newPreparedStatement = null;
	ResultSet querySelect = null;
	ResultSetMetaData mData = null;
	try {
        if(pooledConnection != null) {
            newPreparedStatement = pooledConnection.prepareStatement("SELECT * FROM TH_MONPIF where D_STATUS=? " + PSUR);
            newPreparedStatement.setString(1,"B");
            querySelect = newPreparedStatement.executeQuery();
            mData = querySelect.getMetaData();
            while(querySelect.next()) {
				for (int i = 1; i<= mData.getColumnCount(); i++){
					String strTemp = querySelect.getString(i).trim();
					if(strTemp.length()==0)
						strRow += "-|";
					else
						strRow += strTemp + "|";
				}
				result.add("|"+ strRow);
				strRow="";
            }
            
            if(result.isEmpty())
            	result.add("-0");
        }
        else
            result.add("ERROR_DATOS: No hay conexiones disponibles para la aplicaci&oacute;n. Por favor intente despu&eacute;s.");
    }
    catch(java.sql.SQLException sqlException){
        System.out.println("Error MonitorEfectivo::getBloqueados: [" + sqlException.toString() + "] ");
        result.add(String.valueOf(sqlException.getErrorCode()));
    }
    finally {
        try {
            if(querySelect != null){
                querySelect.close();
                querySelect = null;
            }
            if(newPreparedStatement != null){
                newPreparedStatement.close();
                newPreparedStatement = null;
            }
            if(pooledConnection != null){
                pooledConnection.close();
                pooledConnection = null;
            }
        }
        catch(java.sql.SQLException sqlException){
            System.out.println("Error MonitorEfectivo::getBloqueados FINALLY: [" + sqlException.toString() + "] ");
        }
    }
  	return result;
  }
  
  public String getBloqueadoDtll(String Cajero){
	String strRow = "";
  	Connection pooledConnection = ConnectionPoolingManager.getPooledConnection();
  	String PSUR = ConnectionPoolingManager.getPostFixUR();   
  	PreparedStatement newPreparedStatement = null;
  	ResultSet querySelect = null;
  	ResultSetMetaData mData = null;
  	try {
        if(pooledConnection != null) {
            newPreparedStatement = pooledConnection.prepareStatement("SELECT D_EJCT_CTA,D_NOMBRE,D_DIRECCION_IP FROM TC_USUARIOS_SUC where C_CAJERO = ? AND D_SIGN_STATUS = ? " + PSUR);
            newPreparedStatement.setString(1,Cajero);
            newPreparedStatement.setString(2,"B");
            querySelect = newPreparedStatement.executeQuery();
            mData = querySelect.getMetaData();
            while(querySelect.next()) {
				for (int i = 1; i<= mData.getColumnCount(); i++){
					String strTemp = querySelect.getString(i).trim();
					if(strTemp.length()==0)
						strRow += "-|";
					else
						strRow += strTemp + "|";
				}
				strRow = "|"+strRow;
            }
        }
        else
            strRow = "No hay conexiones disponibles para la aplicaci&oacute;n. Por favor intente despu&eacute;s.";
    }
    catch(java.sql.SQLException sqlException){
        System.out.println("Error MonitorEfectivo::getBloqueadoDtll: [" + sqlException.toString() + "] ");
        strRow = "ERROR SQL";
    }
    finally {
        try {
            if(querySelect != null){
                querySelect.close();
                querySelect = null;
            }
            if(newPreparedStatement != null){
                newPreparedStatement.close();
                newPreparedStatement = null;
            }
            if(pooledConnection != null){
                pooledConnection.close();
                pooledConnection = null;
            }
        }
        catch(java.sql.SQLException sqlException){
            System.out.println("Error MonitorEfectivo::getBloqueadoDtll FINALLY: [" + sqlException.toString() + "] ");
        }
    }
  	return strRow;
  }
  
  public void setDesbloquear(String user, String fecha, String hora, String racf, String fechpif, String horapif){
  	Connection pooledConnection = ConnectionPoolingManager.getPooledConnection();
  	String statementPS = ConnectionPoolingManager.getStatementPostFix();  
  	PreparedStatement newPreparedStatement = null;
  	String status = "NOK";
  	int code = 0;
  	
  	try {
        if(pooledConnection != null) {
			
            newPreparedStatement = pooledConnection.prepareStatement("UPDATE TH_MONPIF SET D_STATUS = ?, D_RACF_PIF = ?, D_FECHA_DESBLOQ = ?, D_HORA_DESBLOQ = ? WHERE D_CAJERO = ? AND D_FECHA_EVENT = ? AND D_HORA_EVENT = ?" + statementPS);
            newPreparedStatement.setString(1,"N");
            newPreparedStatement.setString(2,racf);
            newPreparedStatement.setString(3,fechpif);
            newPreparedStatement.setString(4,horapif);
            newPreparedStatement.setString(5,user);
            newPreparedStatement.setString(6,fecha);
            newPreparedStatement.setString(7,hora);
            code = newPreparedStatement.executeUpdate();
            status = "OK";
        }
        else
            System.err.println("ERROR_DATOS: No hay conexiones disponibles para la aplicaci&oacute;n. Por favor intente despu&eacute;s.");
    }
    catch(java.sql.SQLException sqlException){
        System.err.println("Error MonitorEfectivo::setDesbloquear: [" + sqlException.toString() + "] ");
    }
    finally {
        try {
            if(newPreparedStatement != null){
                newPreparedStatement.close();
                newPreparedStatement = null;
            }
            if(pooledConnection != null){
                pooledConnection.close();
                pooledConnection = null;
            }
        }
        catch(java.sql.SQLException sqlException){
            System.err.println("Error MonitorEfectivo::setDesbloquear FINALLY: [" + sqlException.toString() + "] ");
        }
    }
    if(status.equals("OK")){
    	pooledConnection = ConnectionPoolingManager.getPooledConnection();
	  	try {
	        if(pooledConnection != null) {
	
	            newPreparedStatement = pooledConnection.prepareStatement("UPDATE TC_USUARIOS_SUC SET D_SIGN_STATUS = ? WHERE C_CAJERO = ?" + statementPS);
	            newPreparedStatement.setString(1,"N");
	            newPreparedStatement.setString(2,user);
	            code = newPreparedStatement.executeUpdate();   
	        }
	        else
	            System.err.println("ERROR_DATOS: No hay conexiones disponibles para la aplicaci&oacute;n. Por favor intente despu&eacute;s.");
	    }
	    catch(java.sql.SQLException sqlException){
	        System.err.println("Error MonitorEfectivo::getInfoSuc: [" + sqlException.toString() + "] ");
	    }
	    finally {
	        try {
	            if(newPreparedStatement != null){
	                newPreparedStatement.close();
	                newPreparedStatement = null;
	            }
	            if(pooledConnection != null){
	                pooledConnection.close();
	                pooledConnection = null;
	            }
	        }
	        catch(java.sql.SQLException sqlException){
	            System.err.println("Error MonitorEfectivo::setDesbloquear FINALLY: [" + sqlException.toString() + "] ");
	        }
	    }
    }
  }
  
//  public int InsertPIF(double DMontoFactor,String Monto, String Moneda, String Cajero) {
  public int InsertPIF(String DMontoFactor,String Monto, String Moneda, String Cajero) {
	  Connection pooledConnection = ConnectionPoolingManager.getPooledConnection();
	  PreparedStatement select = null;
	  ResultSet Rselect = null;
	  GenericClasses GC = new GenericClasses();
			
	  String FechaEvent   = "";
	  String HoraEvent    = "";
	  String Status       = "";
	  String RacfPIF      = "";
	  String FechaDesbloq = "";
	  String HoraDesbloq  = "";
	  FechaEvent          = GC.getDate(1);
	  HoraEvent           = GC.getDate(7);
	  Status              = "B";
		
	  PreparedStatement insert = null;
	  int ResultInsert = 0;
	  pooledConnection = ConnectionPoolingManager.getPooledConnection();
    
	  sql = "INSERT INTO TH_MONPIF VALUES('"+Cajero+"','"+FechaEvent+"','"+HoraEvent+"','"+Moneda+"','"+Status+"','"+DMontoFactor+"','"+Monto+"','"+RacfPIF+"','"+FechaDesbloq+"','"+HoraDesbloq+"')";
																													//MontoFactor
	  try {
		  if(pooledConnection != null) 
		  {
			  insert = pooledConnection.prepareStatement("INSERT INTO TH_MONPIF VALUES(?,?,?,?,?,?,?,?,?,?)");
			  insert.setString(1,  Cajero);
			  insert.setString(2,  FechaEvent);
			  insert.setString(3,  HoraEvent);
			  insert.setString(4,  Moneda);
			  insert.setString(5,  Status);
			  insert.setString(6,  DMontoFactor);
			  insert.setString(7,  Monto);
			  insert.setString(8,  RacfPIF);
			  insert.setString(9,  FechaDesbloq);
			  insert.setString(10, HoraDesbloq);
			
			  ResultInsert= insert.executeUpdate();
			
			  if(ResultInsert > 1)
			  {
				  System.out.println("Diario::InsertPif::ResInsert>1 ["+ResultInsert+"]");
				  System.out.println("Diario::InsertPif::SQL ["+sql+"]");
			  }
		  }
	  }
	  catch(SQLException sqlException)
	  {
		  System.out.println("Error Diario::InsertPif [" + sqlException + "]");
		  System.out.println("Error Diario::InsertPif::SQL [" + sql + "]");
	  }
	  finally {
		  try {
			  if(insert != null) {
				  insert.close();
				  insert = null;
			  }
			  if(pooledConnection != null) {
				  pooledConnection.close();
				  pooledConnection = null;
			  }
		  }
		  catch(java.sql.SQLException sqlException)
		  {
			  System.out.println("Error FINALLY Diario::InsertPif [" + sqlException + "]");
			  System.out.println("Error FINALLY Diario::InsertPif::SQL [" + sql + "]");
		  }
	  }
	  return ResultInsert;
  }
  public int UpdateUsuarios(String Cajero) 
	  {
		       
		  Connection pooledConnection = ConnectionPoolingManager.getPooledConnection();
		  String statementPS = ConnectionPoolingManager.getStatementPostFix();
		  PreparedStatement update = null;
		  int ResultUpdate = 0;
		  try {
			  if(pooledConnection != null) {
				  update = pooledConnection.prepareStatement("UPDATE TC_USUARIOS_SUC SET D_SIGN_STATUS = ? WHERE C_CAJERO = ?" + statementPS);
				  update.setString(1,"B");
				  update.setString(2,Cajero);
				  ResultUpdate= update.executeUpdate();
				  if(ResultUpdate > 1)
				  {
					  System.out.println("Diario::UpdateUsuario::ResUpdate>1 ["+ResultUpdate+"]");
					  System.out.println("Diario::UpdateUsuario::SQL ["+sql+"]");
				  }
			  }
		  }
		  catch(SQLException sqlException)
		  {
			  System.out.println("Error Diario::UpdateUsuario [" + sqlException + "]");
			  System.out.println("Error Diario::UpdateUsuario::SQL ["+sql+"]");
		  }
		  finally {
			  try {
				  if(update != null) {
					  update.close();
					  update = null;
				  }
				  if(pooledConnection != null) {
					  pooledConnection.close();
					  pooledConnection = null;
				  }
			  }
			  catch(java.sql.SQLException sqlException)
			  {
				  System.out.println("Error FINALLY Diario::UpdateUsuario[" + sqlException + "]");
				  System.out.println("Error FINALLY Diario::UpdateUsuario::SQL ["+sql+"]");
			  }
		  }
		  return ResultUpdate;
	  }

  public String getHistorico(){
  	String Consulta = "";
  	
  	return Consulta;
  }

}
