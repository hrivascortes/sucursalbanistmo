//*************************************************************************************************
//             Funcion: Bean Txn 0566
//      Elaborado por: Jesus Emmanuel Lopez Rosales
//*************************************************************************************************
// CCN - 4360576 - 09/03/2007 - Modificaciones para proyecto OPMN
//*************************************************************************************************
package ventanilla.com.bital.sfb;

import com.bital.util.ParameterNotFoundException;
import com.bital.util.ParameterParser;


public class Txn_1569 extends Transaction
{		 
	public Header doAction(ParameterParser param)
		throws ParameterNotFoundException
	{
		FormatoB formatob = new FormatoB();
	    formatob.setTxnCode("1569");
	    formatob.setFormat("B");
	    formatob.setAcctNo( "1569" );
	    formatob.setTranAmt(param.getCString("txtComisionMN" ));
	    formatob.setReferenc("COMISION O.P.");
	    formatob.setFeeAmoun("0");
	    formatob.setIntern("0");
	    formatob.setIntWh("0");
	    formatob.setCashIn(param.getCString("txtComisionMN" ));
	    formatob.setFromCurr("N$");

		return formatob;
	}
	

}
