// Transaccion 4015 para 0061 - Cheques de Caja
package ventanilla.com.bital.sfb;

import com.bital.util.ParameterParser;
import com.bital.util.ParameterNotFoundException;
import ventanilla.GenericClasses;

public class Group31Y extends Transaction
{
  public Header doAction(ParameterParser param)
    throws ParameterNotFoundException
  {
    // Instanciar el bean a utilizar
    Cheques cheque = new Cheques();
    cheque.setTxnCode("4015");
	GenericClasses gc = new GenericClasses();
    
    // Ingresar datos requeridos
    cheque.setTranAmt( param.getCString("txtMonto") );
    int moneda = param.getInt("moneda");
	cheque.setTranCur( gc.getDivisa(param.getString("moneda")));

    cheque.setCheckNo ( param.getString("txtSerialCheq") );
    cheque.setTranDesc( param.getString("txtBeneficiario") );
    cheque.setAcctNo  ( "0103000023" );
    if ( moneda == 2)
        cheque.setAcctNo  ( "3902222222" );
    cheque.setAcctNo1 ( param.getString("txtDDACuenta") );    
    cheque.setCashIn  ( "000" );
    cheque.setMoAmount( "000" );
    
    return cheque;
  }
}
