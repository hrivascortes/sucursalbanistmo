//************************************************************************************************* 
//             Funcion: Header para WU
//            Elemento: Header3.java
//          Creado por: Alejandro Gonzalez Castro
//      Modificado por: Juvenal Fernandez
//*************************************************************************************************
// CCN - 4360189 - 03/09/2004 - Se habilita WU
// CCN - 4360334 - 17/06/2005 - Se elimina import de protomatter
//*************************************************************************************************

package ventanilla.com.bital.sfb;

import com.bital.util.Gateway;
import com.bital.util.GatewayFactory;
import com.bital.util.GatewayException;

public class Header3 implements Command 
{
    private String txtMessage = null;
    private String txtTxnId         = "ODPA ";
    private String txtPgmCom        = "ACTI0010";
    private String txtServId        = "WESU";
    private String txtPgmIdeal      = "        ";
    private String txtReturnCode    = "00";
    private String txtBranch        = "";
    private String txtRegistro      = "";
    private String txtTeller        = "";
    private String txtNoSecuencia   = "";
    private String txtNoTerminal    = "";
    private String txtNoSecAnt      = "";
    private String txtCveDespierta  = "";
    private String txtCuenta        = "";
    private String txtLU            = "";
    private String txtTipoApp       = "";
    private String txtConsecutivo   = "";
    private String txtResultado     = "";
    private String txtCodeRespuesta = "     ";
    private String txtMsgRespuesta  = "                                                                                ";

    public Header3() 
    {
    }
    
    public void execute() 
    {
        Gateway cg = GatewayFactory.getGateway("");
        try 
        {
            String message = this.toString();
            cg.write(message);
            String result = cg.readLine();
            txtMessage = result;
        }
        catch( GatewayException e )
        {
            txtMessage = "1~01~0000000~ERROR DE ENLACE A LA APLICACION CICS~";
        }
    }
    
    public String getMessage() 
    {
        return txtMessage;
    }
    
    public void setMessage(String newMessage) 
    {
        txtMessage = newMessage;
    }
    
    public String getTxnId() 
    {
        return txtTxnId;
    }
    public void setTxnId(String newTxnId) 
    {
        txtTxnId = newTxnId;
    }

    public String getPgmCom() 
    {
        return txtPgmCom;
    }
    public void setPgmCom(String newPgmCom) 
    {
        txtPgmCom = newPgmCom;
    }

    public String getServId() 
    {
        return txtServId;
    }
    public void setServId(String newServId) 
    {
        txtServId = newServId;
    }

    public String getPgmIdeal() 
    {
        return txtPgmIdeal;
    }
    public void setPgmIdeal(String newPgmIdeal) 
    {
        txtPgmIdeal = newPgmIdeal;
    }

    public String getReturnCode() 
    {
        return txtReturnCode;
    }
    public void setReturnCode(String newReturnCode) 
    {
        txtReturnCode = newReturnCode;
    }

    public String getBranch() 
    {
        return txtBranch;
    }
    public void setBranch(String newBranch) 
    {
        txtBranch = newBranch;
    }

    public String getRegistro() 
    {
        return "0" + txtRegistro;
    }
    public void setRegistro(String newRegistro) 
    {
        txtRegistro = newRegistro;
    }

    public String getTeller() 
    {
        return txtTeller;
    }
    public void setTeller(String newTeller) 
    {
        txtTeller = newTeller;
    }

    public String getNoSecuencia() 
    {
        return txtNoSecuencia;
    }
    public void setNoSecuencia(String newNoSecuencia) 
    {
        txtNoSecuencia = newNoSecuencia;
    }

    public String getNoTerminal() 
    {
        return txtNoTerminal;
    }
    public void setNoTerminal(String newNoTerminal) 
    {
        txtNoTerminal = newNoTerminal;
    }

    public String getNoSecAnt() 
    {
        return txtNoSecAnt;
    }
    public void setNoSecAnt(String newNoSecAnt) 
    {
        txtNoSecAnt = newNoSecAnt;
    }

    public String getCveDespierta() 
    {
        return txtCveDespierta;
    }
    public void setCveDespierta(String newCveDespierta) 
    {
        txtCveDespierta = newCveDespierta;
    }

    public String getCuenta() 
    {
        return txtCuenta;
    }
    public void setCuenta(String newCuenta) 
    {
        txtCuenta = newCuenta;
    }

    public String getLU() 
    {
        return txtLU;
    }
    public void setLU(String newLU) 
    {
        txtLU = newLU;
    }

    public String getTipoApp() 
    {
        return txtTipoApp;
    }
    public void setTipoApp(String newTipoApp) 
    {
        txtTipoApp = newTipoApp;
    }

    public String getConsecutivo() 
    {
        return txtConsecutivo;
    }
    public void setConsecutivo(String newConsecutivo) 
    {
        txtConsecutivo = newConsecutivo;
    }

    public String getResultado() 
    {
        return txtResultado;
    }
    public void setResultado(String newResultado) 
    {
        txtResultado = newResultado;
    }

    public String getCodeRespuesta() 
    {
        return txtCodeRespuesta;
    }
    public void setCodeRespuesta(String newCodeRespuesta) 
    {
        txtCodeRespuesta = newCodeRespuesta;
    }

    public String getMsgRespuesta() 
    {
        return txtMsgRespuesta;
    }
    public void setMsgRespuesta(String newMsgRespuesta) 
    {
        txtMsgRespuesta = newMsgRespuesta;
    }

    public String toString() 
    {
        return getTxnId()  +   getPgmCom() +   getServId() +   getPgmIdeal()   +   getReturnCode() +
            getBranch() +   getRegistro()   +   getTeller() +   getNoSecuencia()+   getNoTerminal() +
            getNoSecAnt()   +  getCveDespierta()    +   getCuenta() +   getLU() +   getTipoApp() +
            getConsecutivo()    +   getResultado()  +   getCodeRespuesta()  +   getMsgRespuesta();
    }
}
