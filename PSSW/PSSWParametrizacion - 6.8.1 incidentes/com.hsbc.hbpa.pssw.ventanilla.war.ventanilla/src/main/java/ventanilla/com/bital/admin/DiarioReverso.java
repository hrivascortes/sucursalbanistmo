//*************************************************************************************************
//             Funcion: Clase para reversos del Diario Electronico
//            Elemento: DiarioReverso.java
//          Creado por: Alejandro Gonzalez Castro
//      Modificado por: Jesus Emmanuel Lopez Roslaes
//*************************************************************************************************
// CCN - 4360291 - 18/03/2005 - Se unifican las tabas de TW_DIARIO Y TW_DIARIO_RESPUEST
// CCN - 4360296 - 23/03/2005 - Se cambia CCN por peticion de QA, CCN anterior 4360291
// CCN - 4360310 - 24/04/2005 - Se cambia la insercion de Int a Long para evitar problemas con Montos grandes
// CCN - 4360315 - 06/05/2005 - Se evita insertar respuesta en el reverso de las txns
// CCN - 4360334 - 17/06/2005 - Se elimina variable dbQualifier
// CCN - 4360347 - 22/07/2005 - Se incluye supervisor que autoriza reversos de las txns 5353 y 1003
// CCN - 4360364 - 19/08/2005 - Se activa Control de Efectivo en D�lares
//				Se inserta en la tabla TW_CTRL_LOG para reportes de GTE
//				Se realiza modificacion a la txn 0730 servicio 304379 para no postear el reverso cuando sea  monto 0.00
// CCN - 4360368 - 02/09/2005 - Se eliminan sleep
// CCN - 4360374 - 08/09/2005 - Se genera nuevo paquete a peticion de QA.
// CCN - 4360395 - 21/10/2005 - Se realizan modificaciones a SQl para ordenar por D_CONNUREG
// CCN - 4360576 - 09/03/2007 - Modificaciones para proyecto OPMN
// CCN - 4360584 - 23/03/2007 - Modificaciones para proyecto OPMN
// CCN - 4360587 - 03/04/2007 - Modificaciones para el monitoreo de efectivo
// CCN - 4360592 - 20/04/2007 - Modificaciones para proyecto OPMN
// CCN - 4360619 - 15/06/2007 - Se eliminan systems
// CO - CR7564 - 26/01/2011 - Cambios Proyecto camp
//*************************************************************************************************


package ventanilla.com.bital.admin;  

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.StringTokenizer;
import java.util.Vector;

import javax.servlet.http.HttpSession;

import ventanilla.GenericClasses;
import ventanilla.com.bital.beans.DataDiario;
import ventanilla.com.bital.beans.DatosVarios;
import ventanilla.com.bital.beans.SpecialFunc;
import ventanilla.com.bital.util.NSTokenizer;

import com.bital.util.ConnectionPoolingManager;

public class DiarioReverso {
    private String teller   = "";
    private String branch = "";
    private String supervisor = "";
    private String date     = "";
    private String conlink  = "";
    private String consecrev = "";
    private String txncode = "";
    private String consecutivo = "";
    private String sql = "";
    private Vector Data = new Vector();
    private String ConsecutivoDiario = "";
    private String respuesta = "";
    private String consecorig = "";
    private String StatusDSLZ = "";
    private String MsgDSLZ = "";
    private String MsgOPMN = "";    
    private String SpecialKey = "";
    private String TraceResp = "";
    private String connureg_orig="";
    private String banEsp = "NO";    
    private int numrevs = 0;
    private int messagecode = 0;
    String statementPS = com.bital.util.ConnectionPoolingManager.getStatementPostFix();    
    String PostFixUR = com.bital.util.ConnectionPoolingManager.getPostFixUR();    
    private String TotA = null;
    private String TotC = null;
    private HttpSession session;
    private long monto = 0;
    private Hashtable DatosDiario = null;
	private GenericClasses GC = new GenericClasses();
    
    public void invokeReverso(String Cajero, String Supervisor, String Fecha, String ConsecLiga) 
    {
        Fecha = Fecha.substring(6) + Fecha.substring(3,5) + Fecha.substring(0,2);
        setTeller(Cajero);
        setSupervisor(Supervisor);
        setDate(Fecha);
        setConLink(ConsecLiga);
        SelectTxns();
    }

    // SETS

    public void setTeller(String cajero) {
        teller = cajero;
        setBranch(teller.substring(0,4));
    }

    public void setSupervisor(String Supervisor) {
        supervisor = Supervisor;
    }

    public void setBranch(String sucursal) {
        branch = sucursal;
    }

    public void setDate(String fecha) {
        date = fecha;
    }

    public void setConLink(String consecliga) {
        conlink = consecliga;
    }

    public void setData(Vector datos) {
        Data = datos;
    }

    public void setRespuesta(String resp) {
        respuesta = resp;
    }

    public void setConsecRev(String consec) {
        consecrev = consec;
    }

    public void setNumRevs(int number) {
        numrevs = number;
    }

    public void setTxnCode(String transaccion) {
        txncode = transaccion;
    }

    public void setConsecutivo(String consec) {
        consecutivo = consec;
    }

    public void setConsecOrig(String ConsecOrig) {
        consecorig = ConsecOrig;
    }

    public void setStatusDSLZ(String status) {
        StatusDSLZ = status;
    }

    public void setMsgDSLZ(String msgdslz) {
        MsgDSLZ = msgdslz;
    }
    
    public void setMsgOPMN(String msgopmn) {
       MsgOPMN = msgopmn;
    }
    
    public void setSesion(HttpSession sesion)
    {
        session = sesion;
    }
    
    public void setFuncEsp(String key){
    	SpecialKey = key;
    }
    
    public String getFuncEsp(){
    	return SpecialKey;
    }

    // GETS

    public String getTeller() {
        return teller;
    }

    public String getSupervisor() {
        return supervisor;
    }

    public String getBranch() {
        return branch;
    }

    public String getDate() {
        return date;
    }

    public String getConLink() {
        return conlink;
    }

    public Vector getData() {
        return Data;
    }

    public String getRespuesta() {
        return respuesta;
    }

    public int getNumRevs() {
        return numrevs;
    }

    public Hashtable getTxn(int indice) {
        Vector tmp  = new Vector();
        tmp = getData();
        Hashtable temp = new Hashtable();
        temp = (Hashtable)tmp.get(indice);
        Enumeration Keys = temp.keys();
        Hashtable Txn = new Hashtable();
        while(Keys.hasMoreElements()) {
            String Key = Keys.nextElement().toString();
            String Valor = (String)temp.get(Key);
            Txn.put(Key, Valor);
        }
        return Txn;
    }

    public String getConsecRev() {
        return consecrev;
    }

    public String getStatus() {
        if(getConsecRev().equals("0"))
            return new String("A");
        else
            return new String("?");
    }

    public String getTxnCode() {
        return txncode;
    }

    public String getConsecutivo() {
        return consecutivo;
    }

    public String getConsecOrig() {
        return consecorig;
    }

    public String getStatusDSLZ() 
    {
        if(StatusDSLZ.equals("EXP"))
            StatusDSLZ = "EXPEDIDA";
        if(StatusDSLZ.equals("LIQ"))
            StatusDSLZ = "LIQUIDADA";
        if(StatusDSLZ.equals("SUS"))
            StatusDSLZ = "SUSPENDIDA";
        if(StatusDSLZ.equals("CAN"))
            StatusDSLZ = "CANCELADA";
        return StatusDSLZ;
    }

    public String getMsgDSLZ() {
        return MsgDSLZ;
    }

    public String getMsgOPMN() {
        return MsgOPMN;
    }    
    
    public HttpSession getSesion()
    {
        return session;
    }

    // METHODS

    public void setDiarioConsecutivo() {  	
        String fecha = GC.getDate(6);
        String hora  = GC.getDate(7);
        String teller = getTeller();
        ConsecutivoDiario = teller + fecha + hora;
    }

    public String getDiarioConsecutivo() {
        return ConsecutivoDiario;
    }

    public void SelectTxns() {
        Connection pooledConnection = ConnectionPoolingManager.getPooledConnection();
        ResultSet Rselect = null;
        ResultSetMetaData Rsmd = null;
		PreparedStatement select = null;
        Hashtable Registro = null;
        Vector Datos = new Vector();
        try {
            if(pooledConnection != null) {
            	select = pooledConnection.prepareStatement("SELECT * FROM TW_DIARIO_ELECTRON WHERE D_OPERADOR = ? AND D_FECHOPER = ? AND D_CONSECLIGA = ?  AND D_ESTATUS = ?  AND C_TXN_REVERSABLE = ? ORDER BY D_CONNUREG DESC"  + PostFixUR);
            	select.setString(1,getTeller());
            	select.setString(2,getDate());
            	select.setString(3,getConLink());
            	select.setString(4,getStatus());
            	select.setString(5,"S");
            	Rselect = select.executeQuery();
                
                Rsmd = Rselect.getMetaData();
                String columna  = "";
                String valor    = "";
                while(Rselect.next()) {
                    Registro = new Hashtable();
                    for(int i=1; i<Rsmd.getColumnCount()+1; i++) {
                        columna = Rsmd.getColumnName(i);
                        // Se realiza este ajuste por la txn 0520 en la que viaja
                        // el RFC con espacio al inicio
                        if(columna.equals("D_DESCRIP"))
                            valor   = Rselect.getString(i);
                        else{
                        	if(Rselect.getString(i) == null)
                            	valor = "";  
                            else
                            	valor = Rselect.getString(i).trim();
                            if(columna.equals("D_TXN")){
                               if(valor.equals("0566")){
                                  banEsp = "0566";     
                               }
                               if(valor.equals("4033")){
                                  banEsp = "4033";     
                               }                                         	                               
                            }
                            if(columna.equals("D_RESPUESTA") && (banEsp.equals("0566")||banEsp.equals("4033"))){
                               TraceResp = valor;
                            }                                                        	
                            //YGX
                            if(columna.equals("D_CONNUREG"))
                            {
								connureg_orig= valor;
								//String connureg = (String)valor;
                	            //System.out.println("Imprime el connureg"+ connureg_orig);                
					            
		                        
                            } 
                            
                        }
                            
                        if(valor.length() != 0 && valor != null)
                            Registro.put(columna, valor);
                    }
                    Datos.add(Registro);
                }
                setNumRevs(Datos.size());                	
                setData(Datos);
            }
        }
        catch(SQLException sqlException)
        {
            System.out.println("Error DiarioReverso::SelectTxns [" + sqlException + "]");
            System.out.println("Error DiarioReverso::SelectTxns::SQL [" + sql + "]");
        }
        finally {
            try {
                if(Rselect != null) {
                    Rselect.close();
                    Rselect = null;
                }
                if(select != null) {
                    select.close();
                    select = null;
                }
                if(pooledConnection != null) {
                    pooledConnection.close();
                    pooledConnection = null;
                }
            }
            catch(java.sql.SQLException sqlException)
            {
                System.out.println("Error FINALLY DiarioReverso::SelectTxns [" + sqlException + "]");
                System.out.println("Error FINALLY DiarioReverso::SelectTxns::SQL [" + sql + "]");
            }
        }
    }

    public int executeReverso(int nrev, HttpSession sesion) 
    {
        setSesion(sesion);


        setDiarioConsecutivo();
        int result = 0;
        int codigo = 0;
        int codeVol = 0;
        Vector VResp = new Vector();

        Hashtable txn = new Hashtable();
        String FuncEspAP = "";
		String FuncEspBP = "";
        Diario  postDiario = new Diario();      
        String mensaje ="";
        String codigotxn ="";
        String mensajeResp="";
        
        if(banEsp.equals("0566") || banEsp.equals("4033")){
          GenericClasses gc = new GenericClasses();               				
          setTxnCode("0860");
          codigotxn = getTxnCode();
          String NoOrden ="";
          if(banEsp.equals("0566")){
             NoOrden = TraceResp.substring(234,246);
          }else if(banEsp.equals("4033")){
             NoOrden = TraceResp.substring(314,326);
          }	
	      String strciudad =session.getAttribute("ciudad").toString();
	      if(strciudad.length() > 20){
	         strciudad = strciudad.substring(0,20);
	      }else{
	         strciudad =gc.StringFiller(session.getAttribute("ciudad").toString(),20,true," ");
	      }	
	      String strip =gc.StringFiller(session.getAttribute("dirip").toString(),16,true," ");          
   	      String sucursal =session.getAttribute("branch").toString();          
	      String cajero =session.getAttribute("teller").toString();          	      	      
          mensaje = "ODPA 0860G "+sucursal+cajero+"  "+cajero+"  000***N$****000***1*"+ NoOrden + "***"+ strciudad + strip+"*";
          ventanilla.com.bital.sfb.Header header = new ventanilla.com.bital.sfb.Header();
          header.setReverso(mensaje);
          header.execute();
          String Resptxn = header.getMessage().trim();
          if(Resptxn.substring(260,261).equals("2") || Resptxn.substring(260,261).equals("3")){
            messagecode = 19;
            if(Resptxn.substring(260,261).equals("2"))
              respuesta = "LIQUIDADA";	
            else if(Resptxn.substring(260,261).equals("3"))
              respuesta = "CANCELADA";	
          
            setMsgOPMN(respuesta);	                      	                             
          }	
          banEsp ="NO";
        }
        
        if(messagecode ==0){
          txn = getTxn(nrev);
          setTxnCode(txn.get("D_TXN").toString());
          codigotxn = getTxnCode();
          mensaje = txn.get("D_MSGENVIO").toString();
          if(txn.get("D_RESPUESTA") == null)
		  mensajeResp="";
          else
          	mensajeResp = txn.get("D_RESPUESTA").toString();
        }
        // Caso General de Reverso

        String Moneda = txn.get("D_DIVISA").toString();
        DatosDiario = getDatosDiario(txn.get("D_TXN").toString(), Moneda);
        
        FuncEspBP = ((String)DatosDiario.get("DRBP"));
        if(FuncEspBP==null)
            FuncEspBP="NA";
        else
        	FuncEspBP=FuncEspBP.trim();
        
        FuncEspAP = ((String)DatosDiario.get("DRAP"));
        if (FuncEspAP==null)
            FuncEspAP="NA";
        else
        	FuncEspAP=FuncEspAP.trim();
      
        
        if(!codigotxn.equals("0812") && !codigotxn.equals("DSLZ") && !codigotxn.equals("5353")&& !codigotxn.equals("1003") &&
           !codigotxn.equals("0860")) 
        {
            mensaje = setMessageRev(txn, mensaje);
        }

        // Txns con Descripcion Especial para Reverso
        if(txn.containsKey("D_DESCRIP_REVERSO")) {
            mensaje = setDescripRev(txn, mensaje);
        }

        // Casos Especiales de Reverso

        if(codigotxn.equals("5353")) 
        {
            mensaje = setReverso5353(mensaje, txn);
        }
        
                
        
        if(codigotxn.equals("1003")) 
        {
            mensaje = setReverso1003(mensaje, txn);
        }
        
        if(codigotxn.equals("0812")) {
            mensaje = setReverso0812(mensaje);
            txn.put("D_CUENTA", "99998");
        }
        
        if(codigotxn.equals("0566")) {
            mensaje = setReverso0566(mensaje, mensajeResp );
        }        

        if(codigotxn.equals("4097")) {
            mensaje = setReverso4097(mensaje, mensajeResp );
        }

        if(codigotxn.equals("4033")) {
            mensaje = setReverso4033(mensaje, mensajeResp );
        }                

        if(codigotxn.equals("4111") || codigotxn.equals("4113") || codigotxn.equals("4115")
        || codigotxn.equals("4385") || codigotxn.equals("4063") || codigotxn.equals("4059")
        || codigotxn.equals("4057") || codigotxn.equals("4043")) {
            mensaje = setReversoCV(txn, mensaje);
        }
        
        if(codigotxn.equals("0821"))
			mensaje = setReverso0821(mensaje, txn);

        if(codigotxn.equals("DSLZ")) 
        {
            boolean action = ValidateDSLZ(txn);
            if(action)
            {
                mensaje = setReversoDSLZ(txn, mensaje);
                if(!mensaje.startsWith("DSLZ"))
                {
                    setMsgDSLZ(mensaje);
                    messagecode = 16;
                }
            }
            else
                messagecode = 15;
        }

        StringBuffer msgtmp = null;
		if(!FuncEspBP.equals("NA")){
			msgtmp = new StringBuffer(mensaje);
			messagecode = SpecialFunc("DRBP",txn, getTxn(nrev), msgtmp, FuncEspBP, VResp, getDiarioConsecutivo());
			mensaje = msgtmp.toString();
		}
        
        String consecutivoOriginal = "";
        if(messagecode == 0)
        {
            txn.put("D_MSGENVIO", mensaje.trim());
            txn.put("D_ESTATUS", "P");
            txn.put("D_FECHOPER", GC.getDate(1));
            txn.put("D_CONNUREG", getDiarioConsecutivo());
            txn.put("D_SUPERVISOR",getSupervisor());
            
            result = UpdateSourceTxnR(nrev);
            if(result != 1)
                messagecode = 17;

            result = insertReverso(txn);
            if(result != 1)
                messagecode = 6;
            consecutivoOriginal = getConsecOrig();
        }
        
        if(messagecode == 0)
        {
            if(codigotxn.equals("0528"))
            {
                codeVol = UpdateVolanteo(consecutivoOriginal);
                String resp = "";
                if(codeVol == 0)
                    resp = "0~01~"+consecutivoOriginal+"~TRANSACCION ACEPTADA~";
                else {
                    resp = "1~01~"+consecutivoOriginal+"~TRANSACCION RECHAZADA~";
                }
                setRespuesta(resp);

                StringTokenizer resptmp= new StringTokenizer(resp, "~");
                while(resptmp.hasMoreTokens()) {
                    String token = resptmp.nextToken();
                    VResp.add(token);
                }
            }
            else 
            {
                VResp = postingReverso(mensaje, codigotxn, txn);

                String code = (String)VResp.get(0);
      		
                
                // Borrado de Ordenes de Pago Transmitidas
                if(codigotxn.equals("0587") && code.equals("0"))
                {
                    DeleteOrden(txn);
                }
                
                if(codigotxn.equals("4193") && code.equals("0"))
                {
                	                    
                	//System.out.println("4193");
                    DeleteSolCger(connureg_orig,txn);
                }

                
                if(codigotxn.equals("0270") && code.equals("0"))
                {
                	                    
                	//System.out.println("0270");
                    DeleteSolCger(connureg_orig,txn);
                }
                
                 if(codigotxn.equals("4209") && code.equals("0"))
                {
                	//System.out.println("4209 DiarioReverso");
                    UpdateStatusCger(txn);
                }
                
				if(codigotxn.equals("4019") && code.equals("0"))
			   {
					DeleteP002(connureg_orig,txn);
			   }
                
                try
                {codigo = Integer.parseInt(code);}
                catch(NumberFormatException nfe) 
                {
                    messagecode = 7;
                }
            }
        }

        if(messagecode == 0) 
        {
            if(codigo >= 0 || codigo <= 3) 
            {
               
		int res = UpdateReverso(VResp);
                if(res != 1)
                    messagecode = 9;
                else if(codigo == 0)
                {
            		res = InsertLog((String)txn.get("D_CONSECUTIVO")) ;
	            	if(res != 1)
    	                messagecode = 18;	
                }
               
            }
        }

        if(messagecode == 0 && codigo == 0) 
        {
            int resp = UpdateSourceTxn(nrev);
            if(resp != 1)
                messagecode = 14;
        }
        
        if(messagecode == 0 && codigo == 0)
        {
            // Afectacion de Totales para Control de Efectivo Pesos
            long efe = Long.parseLong(getSesion().getAttribute("efectivo").toString());
            long efe2 = Long.parseLong(getSesion().getAttribute("efectivo2").toString());
            Moneda = txn.get("D_DIVISA").toString();
            if(Moneda.equals("N$") && efe>0)
                DoTotales(getSesion(),Moneda);
            else if(Moneda.equals("US$") && efe2>0)
                DoTotales(getSesion(),Moneda);
            if(!FuncEspAP.equals("NA")){                                      // Despu�s de postear reverso
				msgtmp = new StringBuffer(mensaje);
				messagecode = SpecialFunc("DRAP",txn, getTxn(nrev), msgtmp, FuncEspAP, VResp, getDiarioConsecutivo());
				mensaje = msgtmp.toString();
            }
        }
        

        if(messagecode == 0 && codigo == 0) {
            if (codigotxn.equals("1009") || codigotxn.equals("0108") || codigotxn.equals("1183") ||
            codigotxn.equals("0460") || codigotxn.equals("0604") || codigotxn.equals("0580") ||
            codigotxn.equals("0542"))
                codeVol = UpdateVolanteo(consecutivoOriginal);
        }

        if(codigotxn.equals("0528") && codeVol != 0) {
            messagecode = codeVol;
        }
        return messagecode;
    }

    public String setMessageRev(Hashtable Txn, String firstMessage) {
        // REVERSO F9
        // Se obtinene el consecutivo capturado
        String tmpconsec = getConsecRev();
        if(tmpconsec.equals("0")) {
        tmpconsec = Txn.get("D_CONSECUTIVO").toString();
            setConsecOrig(tmpconsec);
        }


        tmpconsec = "200" + tmpconsec;
        firstMessage = firstMessage.substring(0,32) + tmpconsec + firstMessage.substring(35);

        // Inclusion de Supervisor
        int position = 24;
        firstMessage = firstMessage.substring(0,position) + getSupervisor() + firstMessage.substring(position+6);

        return firstMessage;
    }

    public String setDescripRev(Hashtable HTtxn, String Mensaje) {
        String descrev =  HTtxn.get("D_DESCRIP_REVERSO").toString();
        if(descrev.equals("EMPTY"))
            descrev = "";

        String codetxn = getTxnCode();

        NSTokenizer STmensaje = new NSTokenizer(Mensaje, "*");
        Mensaje = "";
        int tokencount = 0;
        while(STmensaje.hasMoreTokens()) {
            String token = "";
            token = STmensaje.nextToken();
            if(token != null) {
                tokencount++;
                if(codetxn.equals("4089")) {
                    if(tokencount == 15)
                        Mensaje = Mensaje + descrev + "*";
                    else
                        Mensaje = Mensaje + token + "*";
                }
                else {
                    if(tokencount == 14)
                        Mensaje = Mensaje + descrev + "*";
                    else
                        Mensaje = Mensaje + token + "*";
                }
            }
        }
        HTtxn.put("D_DESCRIP", descrev);

        return Mensaje;
    }

    public String setReverso5353(String mensaje5353, Hashtable Txn) 
    {
        String tmpconsec = getConsecRev();
        if(tmpconsec.equals("0")) {
        tmpconsec = Txn.get("D_CONSECUTIVO").toString();
            setConsecOrig(tmpconsec);
        }
         tmpconsec = "200" + tmpconsec;
        mensaje5353 = mensaje5353.substring(0,32) + tmpconsec + mensaje5353.substring(35);
        
        //		Inclusion de Supervisor
		int position = 24;
		mensaje5353 = mensaje5353.substring(0,position) + getSupervisor() + mensaje5353.substring(position+6);

        
        NSTokenizer STmensaje = new NSTokenizer(mensaje5353, "*");
        mensaje5353 = "";
        int tokencount = 0;
        while(STmensaje.hasMoreTokens()) 
        {
            String token = "";
            token = STmensaje.nextToken();
            if(token != null) {
                tokencount++;
                if(tokencount == 27)
                    mensaje5353 = mensaje5353 + Txn.get("D_SERIAL").toString() + "*";
                else
                    mensaje5353 = mensaje5353 + token + "*";
            }
        }
        return mensaje5353;
    }
    
	public String setReverso0821(String mensaje0821, Hashtable Txn) 
	{
       
		NSTokenizer STmensaje = new NSTokenizer(mensaje0821, "*");
		System.out.println("Este es el mensaje original de la 0821 reversada: " + mensaje0821);
		mensaje0821 = "";
		int tokencount = 0;
		while(STmensaje.hasMoreTokens()) 
		{
			String token = "";
			token = STmensaje.nextToken();
			if(token != null) {
				tokencount++;
				if(tokencount == 3)
				{
					if(token.equals("V"))
						token="C";
					else
						token="V";
					mensaje0821 = mensaje0821 + token + "*";
				}
				else
					mensaje0821 = mensaje0821 + token + "*";
			}
		}
		System.out.println("Este es el mensaje final de la 0821 reversada: " + mensaje0821);
		return mensaje0821;
	}
    
    public String setReverso1003(String mensaje1003, Hashtable Txn) 
    {
        String tmpconsec = getConsecRev();
        if(tmpconsec.equals("0")) {
        tmpconsec = Txn.get("D_CONSECUTIVO").toString();
            setConsecOrig(tmpconsec);
        }
       
        tmpconsec = "200" + tmpconsec; 
        mensaje1003 = mensaje1003.substring(0,32) + tmpconsec + mensaje1003.substring(35);
        
        //		Inclusion de Supervisor
		int position = 24;
		mensaje1003 = mensaje1003.substring(0,position) + getSupervisor() + mensaje1003.substring(position+6);

        
        NSTokenizer STmensaje = new NSTokenizer(mensaje1003, "*");
        mensaje1003 = "";
        int tokencount = 0;
        while(STmensaje.hasMoreTokens()) 
        {
            String token = "";
            token = STmensaje.nextToken();
            if(token != null) {
                tokencount++;
                if(/*(tokencount == 14) ||*/ (tokencount == 30))
                    mensaje1003 = mensaje1003 + "" + "*";
                else
                    mensaje1003 = mensaje1003 + token + "*";
            }
        }
        return mensaje1003;
    }
    
public String setReverso0566(String mensaje0566, String mensajeResp0566 ) {
        NSTokenizer STmensaje = new NSTokenizer(mensaje0566, "*");
        mensaje0566 = "";      
        String strOrdenP1 =mensajeResp0566.substring(234,237);      
        String strOrdenP2 =mensajeResp0566.substring(237,246);        
        int tokencount = 0;
        while(STmensaje.hasMoreTokens()) {
            String token = "";
            token = STmensaje.nextToken();
            if(token != null) {
                tokencount++;
                if(tokencount == 3)
                    mensaje0566 = mensaje0566 + strOrdenP1 + "*";
                else if(tokencount == 7)
                    mensaje0566 = mensaje0566 + strOrdenP2 + "*";
                else
                    mensaje0566 = mensaje0566 + token + "*";
            }
        }
        return mensaje0566;
    }

public String setReverso4097(String mensaje4097, String mensajeResp4097 ) {
        NSTokenizer STmensaje = new NSTokenizer(mensaje4097, "*");
        mensaje4097 = "";  
        String strOrden =mensajeResp4097.substring(314,326);      
        int tokencount = 0;
        while(STmensaje.hasMoreTokens()) {
            String token = "";
            token = STmensaje.nextToken();
            if(token != null) {
                tokencount++;
                if(tokencount == 12)
                    mensaje4097 = mensaje4097 + strOrden + "*";
                else
                    mensaje4097 = mensaje4097 + token + "*";
            }
        }
        return mensaje4097;
    }    

public String setReverso4033(String mensaje4033, String mensajeResp4033 ) {
        NSTokenizer STmensaje = new NSTokenizer(mensaje4033, "*");
        mensaje4033 = "";  
        String strOrden =mensajeResp4033.substring(314,326);      
        int tokencount = 0;
        while(STmensaje.hasMoreTokens()) {
            String token = "";
            token = STmensaje.nextToken();
            if(token != null) {
                tokencount++;
                if(tokencount == 12)
                    mensaje4033 = mensaje4033 + strOrden + "*";
                else
                    mensaje4033 = mensaje4033 + token + "*";
            }
        }
        return mensaje4033;
    }
    
    public String setReverso0812(String mensaje0812) {
        NSTokenizer STmensaje = new NSTokenizer(mensaje0812, "*");
        mensaje0812 = "";
        int tokencount = 0;
        while(STmensaje.hasMoreTokens()) {
            String token = "";
            token = STmensaje.nextToken();
            if(token != null) {
                tokencount++;
                if(tokencount == 3)
                    mensaje0812 = mensaje0812 + "99998" + "*";
                else
                    mensaje0812 = mensaje0812 + token + "*";
            }
        }
        return mensaje0812;
    }

    public String setReversoCV(Hashtable HTtxn, String mensajeCV) {
        NSTokenizer STmensaje = new NSTokenizer(mensajeCV, "*");
        mensajeCV = "";
        int tokencount = 0;
        String temp = "";
        while(STmensaje.hasMoreTokens()) {
            String token = "";
            token = STmensaje.nextToken();
            if(token != null) {
                tokencount++;
                if(tokencount == 50) {
                    int txnrev = Integer.parseInt(token);
                    txnrev = txnrev+1;
                    temp  = String.valueOf(txnrev);
                    if(temp.length() == 3)
                        temp = "0" + temp;
                    mensajeCV = mensajeCV + temp + "*";
                }
                else
                    mensajeCV = mensajeCV + token + "*";
            }
        }
        HTtxn.put("D_SERIAL", temp);

        return mensajeCV;
    }

    public String setReversoDSLZ(Hashtable HTtxn, String mensajeDSLZ) {
        int indice = 0;
        String tipo = HTtxn.get("D_SERIAL").toString();

        if(tipo.equals("LIQ") || tipo.equals("CAN") || tipo.equals("SUS"))
        {
            indice = mensajeDSLZ.indexOf(tipo);
            mensajeDSLZ = mensajeDSLZ.substring(0,indice) + "REA" + mensajeDSLZ.substring(indice+3,mensajeDSLZ.length());
            indice = mensajeDSLZ.indexOf(tipo);
            mensajeDSLZ = mensajeDSLZ.substring(0,indice) + "REA" + mensajeDSLZ.substring(indice+3,mensajeDSLZ.length());
            HTtxn.put("D_SERIAL","REA");
        }

        if(tipo.equals("LSU"))
        {
            indice = mensajeDSLZ.indexOf(tipo);
            mensajeDSLZ = mensajeDSLZ.substring(0,indice) + "SUS" + mensajeDSLZ.substring(indice+3,mensajeDSLZ.length());
            indice = mensajeDSLZ.indexOf(tipo);
            mensajeDSLZ = mensajeDSLZ.substring(0,indice) + "SUS" + mensajeDSLZ.substring(indice+3,mensajeDSLZ.length());
            HTtxn.put("D_SERIAL","SUS");
        }

        if(tipo.equals("REA"))
        {
            indice = mensajeDSLZ.indexOf(tipo);
            mensajeDSLZ = mensajeDSLZ.substring(0,indice) + "LIQ" + mensajeDSLZ.substring(indice+3,mensajeDSLZ.length());
            indice = mensajeDSLZ.indexOf(tipo);
            mensajeDSLZ = mensajeDSLZ.substring(0,indice) + "LIQ" + mensajeDSLZ.substring(indice+3,mensajeDSLZ.length());
            HTtxn.put("D_SERIAL","LIQ");
        }

        if(tipo.equals("EXP")) 
        {
            String orden = HTtxn.get("D_DESCRIP").toString();
            String banco = HTtxn.get("D_REFER").toString().trim();
            if(banco.equals((String)session.getAttribute("identidadApp")))
                banco = "    ";
            else
                banco = "0001";
            String inqDSLZ = "DSLZ " + getBranch() + "    INQ1" + orden;
            inqDSLZ = inqDSLZ + "                             "+ banco + "00000000";
            inqDSLZ = Filler(inqDSLZ, 520);

            ventanilla.com.bital.sfb.Header2 header2 = new ventanilla.com.bital.sfb.Header2();
            header2.setReverso(inqDSLZ);
            header2.execute();
            String resp = header2.getMessage();
            if(resp.startsWith("003")) 
            {
                String OSN = resp.substring(11,19);
                String bank = resp.substring(320,324);
                mensajeDSLZ = "DSLZ " + getBranch() + "    REV "+ orden;
                mensajeDSLZ = Filler(mensajeDSLZ, 50);
                mensajeDSLZ = mensajeDSLZ + bank + OSN + "REV " + GC.getDate(4) + GC.getDate(5);
                mensajeDSLZ = mensajeDSLZ + getTeller() + "  " + getBranch();
                mensajeDSLZ = Filler(mensajeDSLZ, 520);
            }
            else
            {
                mensajeDSLZ = resp;
            }
        }
        return mensajeDSLZ;
    }

    public int insertReverso(Hashtable datostxn) 
    {
        String TotC = (String)datostxn.get("D_CARGO");
        String TotA = (String)datostxn.get("D_ABONO");
        setTotales(TotC, TotA);
		datostxn.remove("D_RESPUESTA");
		
		DatosVarios TDiario = DatosVarios.getInstance();
		Connection pooledConnection = ConnectionPoolingManager.getPooledConnection();
		PreparedStatement insert = null;
		int ResultUpdate = 0;
		sql="INSERT INTO TW_DIARIO_ELECTRON (D_SUCURSAL,D_OPERADOR,D_CONNUREG,D_TXN,C_TXN_REVERSABLE,C_TXN_VISIBLE,D_SUPERVISOR,D_FECHOPER,D_CUENTA,D_SERIAL,D_REFER,D_REFER1,D_REFER2,D_REFER3,D_EFECTIVO,D_MONTO,D_FECHEFEC,D_DIVISA,D_DESCRIP,D_DESCRIP_REVERSO,D_ESTATUS,D_CONSECUTIVO,D_CONSECLIGA,D_CARGO,D_ABONO,D_MSGENVIO) VALUES(";
		Vector Valores = new Vector();
		
		try 
		 {
			 if(pooledConnection != null) 
			 {
				Hashtable TablaDiario = TDiario.getTablaDiario();
				insert = pooledConnection.prepareStatement("INSERT INTO TW_DIARIO_ELECTRON (D_SUCURSAL,D_OPERADOR,D_CONNUREG,D_TXN,C_TXN_REVERSABLE,C_TXN_VISIBLE,D_SUPERVISOR,D_FECHOPER,D_CUENTA,D_SERIAL,D_REFER,D_REFER1,D_REFER2,D_REFER3,D_EFECTIVO,D_MONTO,D_FECHEFEC,D_DIVISA,D_DESCRIP,D_DESCRIP_REVERSO,D_ESTATUS,D_CONSECUTIVO,D_CONSECLIGA,D_CARGO,D_ABONO,D_MSGENVIO) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");

				for(int i=1; i<TablaDiario.size();i++)				//inserta los datos en blanco (de 1 a 26)
				{	
					if(i==15||i==16)
						insert.setInt(i,0);
					else
						insert.setString(i,"");
					Valores.addElement("''");
				}

		        Enumeration Keys = datostxn.keys();
				int pos=0;
		        while(Keys.hasMoreElements()) 						//inserta los datos con valores 
		        {
		            String Key = Keys.nextElement().toString();
		            String ColumnTable = Key;
		            String Valor = (String)datostxn.get(Key);
					if(Valor == null)
						Valor = "nulo";
					pos = Integer.parseInt((String)TablaDiario.get(ColumnTable));
		            if(ColumnTable.equals("D_MONTO") || ColumnTable.equals("D_EFECTIVO"))
		            {
		               // Control de Efectivo
		                if(ColumnTable.equals("D_EFECTIVO"))
		                    setMonto(Valor);
						insert.setLong(pos,Long.parseLong(Valor));
		            }
		            else
						insert.setString(pos,Valor);
					Valores.setElementAt("'"+Valor+"'",pos-1);   
		        }
				sql = sql + Valores + ")";
				ResultUpdate= insert.executeUpdate();
                if(ResultUpdate > 1)
                {
                    System.out.println("Error DiarioReverso::InsertReverso::ResUpdate>1 -"+ResultUpdate+"-");
                    System.out.println("Error DiarioReverso::InsertReverso::SQL -"+sql+"-");
                }
            }
        }
        catch(SQLException sqlException)
        {
            System.out.println("Error DiarioReverso::InsertReverso [" + sqlException + "]");
            System.out.println("Error DiarioReverso::InsertReverso::SQL [" + sql + "]");
        }
        finally {
            try {
                if(insert != null) {
                    insert.close();
                    insert = null;
                }
                if(pooledConnection != null) {
                    pooledConnection.close();
                    pooledConnection = null;
                }
            }
            catch(java.sql.SQLException sqlException)
            {
                System.out.println("Error FINALLY DiarioReverso::InsertReverso [" + sqlException + "]");
                System.out.println("Error FINALLY DiarioReverso::InsertReverso::SQL [" + sql + "]");
            }
        }
        return ResultUpdate;
    }

    public Vector postingReverso(String cadena, String txn, Hashtable HTtxn) 
    {
        if(txn.equals("DSLZ")) 
        {
            ventanilla.com.bital.sfb.Header2 header2 = new ventanilla.com.bital.sfb.Header2();
            header2.setReverso(cadena);
            header2.execute();
            respuesta = header2.getMessage();
            if(respuesta.startsWith("0")) 
            {
                respuesta = "0~1~0"+GC.getDate(3)+"~";
                String tmp = "CONSECUTIVO:  "+GC.getDate(3)+" "+header2.getMessage();
                int tmpl = tmp.length();
                for(int i=tmpl; i<78; i++)
                    tmp = tmp + " ";
                tmp = tmp + Filler("*+ TRANSACCION ACEPTADA POR MERVA +*", 78);
                respuesta = respuesta + tmp;

                String tipoDSLZ = HTtxn.get("D_SERIAL").toString();
                /*System.out.println("status ["+tipoDSLZ+"]");*/
                if(tipoDSLZ.equals("EXP"))
                    respuesta = respuesta + "ORDEN REVERSADA~";
                else if(tipoDSLZ.equals("SUS"))
                    respuesta = respuesta + "ORDEN SUSPENDIDA~";
                else
                    respuesta = respuesta + "ORDEN REACTIVADA~";
            }
            else
            {
                // TXN RECHAZADA
                respuesta = "1~1~0"+GC.getDate(3)+"~*+ TRANSACCION RECHAZADA POR MERVA +*~";
            }
        }
        else 
        {
            if(txn.equals("5503"))
            {
                String servicio = HTtxn.get("D_CUENTA").toString();
                String mto = HTtxn.get("D_MONTO").toString();
                if((servicio.endsWith("448") || servicio.equals("66") || servicio.equals("4376")) && mto.equals("0"))
                {
                    respuesta = "0~01~0"+GC.getDate(3)+"~*+ TRANSACCION ACEPTADA +*";
                    
                }
                else
                {
                    ventanilla.com.bital.sfb.Header header = new ventanilla.com.bital.sfb.Header();
                    header.setReverso(cadena);
                    header.execute();
                    respuesta = header.getMessage();
                }
            }
            
            else if(txn.equals("0730"))
              {
              	String servicio = HTtxn.get("D_CUENTA").toString();
              	String mto = HTtxn.get("D_MONTO").toString();
              	if (servicio.equals("304379") && mto.equals("0")) 
            	   {
   	               respuesta = "0~03~0"+GC.getDate(3)+"~CONSECUTIVO:  0"+GC.getDate(3)+"    0     *+ TRANSACCION     ACEPTADA  +*~";
   	               }
                   else
                       {
   	                   ventanilla.com.bital.sfb.Header header = new ventanilla.com.bital.sfb.Header();
                       header.setReverso(cadena);
                       header.execute();
                       respuesta = header.getMessage();
                       }
            
              }
            else
            {
                ventanilla.com.bital.sfb.Header header = new ventanilla.com.bital.sfb.Header();
                header.setReverso(cadena);
                header.execute();
                respuesta = header.getMessage();
			}
        }

        setRespuesta(respuesta);

        StringTokenizer STRespuesta = new StringTokenizer(respuesta, "~");
        Vector VRespuesta= new Vector();
        while(STRespuesta.hasMoreTokens()) {
            String token = STRespuesta.nextToken();
            VRespuesta.add(token);
        }

        setConsecutivo(VRespuesta.get(2).toString());
        return VRespuesta;
    }

    public Vector getLinesResp(String lineas, int nolineas) {
        Vector Lineas = new Vector();
        for(int i=0; i<nolineas; i++) {
            Lineas.add(lineas.substring( i*78, Math.min(i*78+77, lineas.length())));
            if(Math.min(i*78+77, lineas.length()) == lineas.length())
                break;
        }

        return Lineas;
    }

    public int UpdateReverso(Vector VRespuesta) {
        int coderesp = Integer.parseInt((String)VRespuesta.get(0));
        String consec = (String)VRespuesta.get(2);
		String respuest = (String)VRespuesta.get(3);
        String status = "";
        if(coderesp == 0)
            status = "C";
        else
            status = "R";
		respuest = GC.delCerosBinarios(respuest);
		
        Connection pooledConnection = ConnectionPoolingManager.getPooledConnection();
		PreparedStatement update = null;
        int ResultUpdate = 0;
        
        /*System.out.println("SQL - Reverso::Update ["+sql+"]");*/

        try {
            if(pooledConnection != null) 
            {
				update = pooledConnection.prepareStatement("UPDATE TW_DIARIO_ELECTRON SET D_ESTATUS = ?, D_CONSECUTIVO = ?, D_RESPUESTA = ? WHERE D_CONNUREG = ?" + statementPS);
				update.setString(1, status);
				update.setString(2, consec);
				update.setString(3, respuest.trim());
				update.setString(4, getDiarioConsecutivo());
				ResultUpdate= update.executeUpdate();
				sql= "UPDATE TW_DIARIO_ELECTRON SET D_ESTATUS = "+status+", D_CONSECUTIVO = "+consec+", D_RESPUESTA = "+respuest.trim()+" WHERE D_CONNUREG = "+getDiarioConsecutivo() + statementPS;
                if(ResultUpdate > 1)
                {
                    System.out.println("DiarioReverso::UpdateReverso::ResUpdate>1 ["+ResultUpdate+"]");
                    System.out.println("DiarioReverso::UpdateReverso::SQL ["+sql+"]");
                }
            }
        }
        catch(SQLException sqlException)
        {
            System.out.println("Error DiarioReverso::UpdateReverso [" + sqlException + "]");
            System.out.println("Error DiarioReverso::UpdateReverso::SQL [" + sql + "]");
        }
        finally {
            try {
                if(update != null) {
                    update.close();
                    update = null;
                }
                if(pooledConnection != null) {
                    pooledConnection.close();
                    pooledConnection = null;
                }
            }
            catch(java.sql.SQLException sqlException)
            {
                System.out.println("Error FINALLY DiarioReverso::UpdateReverso [" + sqlException + "]");
                System.out.println("Error FINALLY DiarioReverso::UpdateReverso::SQL [" + sql + "]");
            }
        }
        return ResultUpdate;
    }

	public int InsertLog(String consec) {
        Connection pooledConnection = ConnectionPoolingManager.getPooledConnection();
		PreparedStatement select = null;
		ResultSet Rselect = null;
        Hashtable UserInfo = (Hashtable)getSesion().getAttribute("userinfo");
        Hashtable supervisores = (Hashtable)getSesion().getAttribute("user.par");
        String indice = (String)getSesion().getAttribute("autho.indice");
		String Nom_Sup = (String)supervisores.get("D_NOMBRE"+indice);
		String RACF_Sup = (String)supervisores.get("D_EJCT_CTA"+indice);
		String IP_Sup = (String)supervisores.get("D_DIRECCION_IP"+indice);		
		
		String tmpconsec = getConsecRev();
        if(tmpconsec.equals("0")) 
	        tmpconsec = consec;

		PreparedStatement update = null;
		int ResultUpdate = 0;
        pooledConnection = ConnectionPoolingManager.getPooledConnection();
        sql = "INSERT INTO TW_CTRL_LOG VALUES('"+"0"+this.getBranch()+"','"+this.getTeller()+"','"+this.getDiarioConsecutivo()+"','"+(String)UserInfo.get("D_NOMBRE")+"','"+(String)UserInfo.get("D_EJCT_CTA")+"','"+(String)UserInfo.get("D_DIRECCION_IP")+"','"+Nom_Sup+"','"+RACF_Sup+"','"+IP_Sup+",'R','"+tmpconsec+"')";
               
        try {
            if(pooledConnection != null) 
            {
				update = pooledConnection.prepareStatement("INSERT INTO TW_CTRL_LOG VALUES(?,?,?,?,?,?,?,?,?,?,?)");
				update.setString(1, "0"+this.getBranch());
				update.setString(2, this.getTeller());
				update.setString(3, this.getDiarioConsecutivo());
				update.setString(4, (String)UserInfo.get("D_NOMBRE"));
				update.setString(5, (String)UserInfo.get("D_EJCT_CTA"));
				update.setString(6, (String)UserInfo.get("D_DIRECCION_IP"));
				update.setString(7, Nom_Sup);
				update.setString(8, RACF_Sup);
				update.setString(9, IP_Sup);				
				update.setString(10, "R");
				update.setString(11,tmpconsec.substring(1));
				ResultUpdate= update.executeUpdate();
				
                if(ResultUpdate > 1)
                {
                    System.out.println("DiarioReverso::InsertLog::ResUpdate>1 ["+ResultUpdate+"]");
                    System.out.println("DiarioReverso::InsertLog::SQL ["+sql+"]");
                }
            }
        }
        catch(SQLException sqlException)
        {
            System.out.println("Error DiarioReverso::InsertLog [" + sqlException + "]");
            System.out.println("Error DiarioReverso::InsertLog::SQL [" + sql + "]");
        }
        finally {
            try {
                if(update != null) {
                    update.close();
                    update = null;
                }
                if(pooledConnection != null) {
                    pooledConnection.close();
                    pooledConnection = null;
                }
            }
            catch(java.sql.SQLException sqlException)
            {
                System.out.println("Error FINALLY DiarioReverso::InsertLog [" + sqlException + "]");
                System.out.println("Error FINALLY DiarioReverso::InsertLog::SQL [" + sql + "]");
            }
        }
        return ResultUpdate;
    }
    
    public Vector getMessage(int code) {
        String respuesta = "";
        switch (code) {
            case 0:
                respuesta = getRespuesta();
                break;
            case 5:
                respuesta = "1~01~0101010~TRANSACCION NO EXISTE EN TABLA TC_DATOS_DIARIO";
                break;
            case 6:
                respuesta = "1~01~0101010~ERROR AL INTENTAR INSERTAR EN EL DIARIO ELECTRONICO";
                break;
            case 7:
                respuesta = "1~01~0101010~ERROR DE ENLACE A LA APLICACION CICS~";
                break;
            case 8:
                respuesta = "1~01~0101010~ERROR AL INTENTAR INSERTAR LA RESPUESTA DE HOST EN EL DIARIO ELECTRONICO";
                break;
            case 9:
                respuesta = "1~01~0101010~ERROR AL INTENTAR ACTUALIZAR EN EL DIARIO ELECTRONICO";
                break;
            case 10:
                respuesta = "1~01~0101010~ERROR AL INTENTAR SELECCIONAR REGISTRO EN EL CONTROL DE CHEQUES";
                break;
            case 11:
                respuesta = "1~01~0101010~ERROR AL INTENTAR ACTUALIZAR REGISTRO EN EL CONTROL DE CHEQUES";
                break;
            case 12:
                respuesta = "1~01~0101010~ERROR AL INTENTAR SELECCIONAR EN EL CONTROL DE VOLANTEO";
                break;
            case 13:
                respuesta = "1~01~0101010~ERROR AL INTENTAR ACTUALIZAR EN EL CONTROL DE VOLANTEO";
                break;
            case 14:
                respuesta = "1~01~0101010~ERROR AL INTENTAR ACTUALIZAR TXN FUENTE";
                break;
            case 15:
                respuesta = "1~01~0101010~ERROR ORDEN CON  ESTATUS DE " + getStatusDSLZ();
                break;
            case 16:
                respuesta = "1~01~0101010~" + getMsgDSLZ();
                break;
            case 17:
                respuesta = "1~01~0101010~ERROR AL INTENTAR ACTUALIZAR TXN ORIGINAL PARA REVERSO";
                break;
			case 18:
                respuesta = "1~01~0101010~ERROR AL INTENTAR ACTUALIZAR REGISTRO EN EL CONTROL DE REVERSOS";
                break;                
            case 19:
                respuesta = "1~01~0101010~ERROR ORDEN CON  ESTATUS DE " + getMsgOPMN();
                break;                
        }

        NSTokenizer resp = new NSTokenizer(respuesta, "~");
        Vector response = new Vector();
        while(resp.hasMoreTokens()) {
            String token = resp.nextToken();
            if( token == null )
                continue;

            response.addElement(token);
        }
        return response;
    }

    public int UpdateSourceTxn(int norev) {
        Hashtable Stxn = new Hashtable();
        Stxn = getTxn(norev);
        Enumeration Keys = Stxn.keys();
        Stxn.remove("D_ESTATUS");
		String conrev = getConsecRev();
		        
        Connection pooledConnection = ConnectionPoolingManager.getPooledConnection();
		PreparedStatement update = null;
        int ResultUpdate = 0;

        /*System.out.println("SQL Update::SourceTxn ["+sql+"]");*/
        try {
            if(pooledConnection != null) {
				
		
		if(conrev.equals("0"))					//necesario para no borrar el consecutivo de la txn original
		{  							
			update = pooledConnection.prepareStatement("UPDATE TW_DIARIO_ELECTRON SET D_ESTATUS = ? WHERE D_CONNUREG = ? " + statementPS);
			update.setString(1,"X");
			update.setString(2,(String)Stxn.get("D_CONNUREG"));
		}
		else{
			update = pooledConnection.prepareStatement("UPDATE TW_DIARIO_ELECTRON SET D_ESTATUS = ?, D_CONSECUTIVO = ? WHERE D_CONNUREG = ? " + statementPS);
			update.setString(1,"X");
			update.setString(2,conrev);
			update.setString(3,(String)Stxn.get("D_CONNUREG"));
		}	

		ResultUpdate= update.executeUpdate();
                if(ResultUpdate > 1)
                {
                    System.out.println("DiarioReverso::UpdateSourceTxn::ResUpdate>1 ["+ResultUpdate+"]");
                    System.out.println("DiarioReverso::UpdateSourceTxn::SQL ["+sql+"]");
                }
            }
        }
        catch(SQLException sqlException)
        {
            System.out.println("Error DiarioReverso::UpdateSourceTxn [" + sqlException + "]");
            System.out.println("Error DiarioReverso::UpdateSourceTxn::SQL [" + sql + "]");
        }
        finally {
            try {
                if(update != null) {
                    update.close();
                    update = null;
                }
                if(pooledConnection != null) {
                    pooledConnection.close();
                    pooledConnection = null;
                }
            }
            catch(java.sql.SQLException sqlException)
            {
                System.out.println("Error FINALLY DiarioReverso::UpdateSourceTxn [" + sqlException + "]");
                System.out.println("Error FINALLY DiarioReverso::UpdateSourceTxn::SQL [" + sql + "]");
            }
        }
        return ResultUpdate;
    }

    private int UpdateVolanteo(String consec) {
        String cajero = getTeller();
        String sucursal = cajero.substring(0,4);
        String nocajero = cajero.substring(4);

        String sql = "";

        sql = "SELECT D_CONSECUTIVO FROM TW_CONSEC_VOL_DDA";
        sql = sql + " WHERE D_CONSEC_HOGAN='" + consec + "' AND C_NUMERO_SUCURSAL ='" + sucursal + "'";
        sql = sql + " AND C_NUMERO_CAJERO='" + nocajero +"'" + PostFixUR;

        String ConsecVolanteo = SQLSelectVol(sql);
        ConsecVolanteo = ConsecVolanteo.trim();
        if (ConsecVolanteo.equals("EMPTY"))
            return 10;

        sql = "DELETE FROM TW_CONSEC_VOL_DDA";
        sql = sql + " WHERE D_CONSEC_HOGAN='" + consec + "' AND C_NUMERO_SUCURSAL ='" + sucursal + "'";
        sql = sql + " AND C_NUMERO_CAJERO='" + nocajero +"'";

        int update = SQLUpdateVol(sql);
        if (update != 1)
            return 11;

        if(ConsecVolanteo.length() > 3) {

            String banco  = ConsecVolanteo.substring(0,3);
            String numvol = ConsecVolanteo.substring(9);

            sql = "SELECT C_CONSEC_CHEQUE FROM TW_VOLANTEO_DDA";
            sql = sql + " WHERE C_NUMERO_SUCURSAL ='" + sucursal + "'";
            sql = sql + " AND C_NUMERO_CAJERO ='" + nocajero + "'";
            sql = sql + " AND C_NUMERO_BANCO ='" + banco + "'";
            sql = sql + " AND C_CONSEC_VOLANTEO=" + numvol + PostFixUR;

            String ConsecChq = SQLSelectVol(sql);
            if (ConsecChq.equals("EMPTY"))
                return 12;

            int cheque = Integer.parseInt(ConsecChq);
            cheque --;

            sql = "UPDATE TW_VOLANTEO_DDA";
            sql = sql + " SET C_CONSEC_CHEQUE=" + cheque;
            sql = sql + " WHERE C_NUMERO_SUCURSAL ='" + sucursal + "'";
            sql = sql + " AND C_NUMERO_CAJERO ='" + nocajero + "'";
            sql = sql + " AND C_NUMERO_BANCO ='" + banco + "'";
            sql = sql + " AND C_CONSEC_VOLANTEO=" + numvol;

            update = SQLUpdateVol(sql);
            if (update != 1)
                return 13;
        }


        return 0;
    }

    public int SQLUpdateVol(String sqlUpdate) {
        sqlUpdate += statementPS;
        Connection pooledConnection = ConnectionPoolingManager.getPooledConnection();
        Statement update = null;
        int ResultUpdate = 0;

        try {
            if(pooledConnection != null) {
                update = pooledConnection.createStatement();
                ResultUpdate= update.executeUpdate(sqlUpdate);
                if(ResultUpdate > 1)
                {
                    System.out.println("Error DiarioReverso::SQLUpdateVol::ResUpdate>1 [" + ResultUpdate + "]");
                    System.out.println("Error DiarioReverso::SQLUpdateVol::SQL [" + sql + "]");
                }
            }
        }
        catch(SQLException sqlException)
        {
            System.out.println("Error DiarioReverso::SQLUpdateVol [" + sqlException + "]");
            System.out.println("Error DiarioReverso::SQLUpdateVol::SQL [" + sql + "]");
        }
        finally {
            try {
                if(update != null) {
                    update.close();
                    update = null;
                }
                if(pooledConnection != null) {
                    pooledConnection.close();
                    pooledConnection = null;
                }
            }
            catch(java.sql.SQLException sqlException)
            {
                System.out.println("Error FINALLY DiarioReverso::SQLUpdateVol [" + sqlException + "]");
                System.out.println("Error FINALLY DiarioReverso::SQLUpdateVol::SQL [" + sql + "]");
            }
        }
        return ResultUpdate;
    }

    public String SQLSelectVol(String sqlSelect) {
        Connection pooledConnection = ConnectionPoolingManager.getPooledConnection();
        ResultSet Rselect = null;
        Statement select = null;
        String valor = "";
        try {
            if(pooledConnection != null) {
                select = pooledConnection.createStatement();
                Rselect = select.executeQuery(sqlSelect);
                if(Rselect.next()) {
                    valor = Rselect.getString(1);
                }
                else
                    valor = "EMPTY";
            }
        }
        catch(SQLException sqlException)
        {
            System.out.println("Error DiarioReverso::SQLSelectVol [" + sqlException + "]");
            System.out.println("Error DiarioReverso::SQLSelectVol::SQL [" + sqlSelect + "]");
        }
        finally {
            try {
                if(Rselect != null) {
                    Rselect.close();
                    Rselect = null;
                }
                if(select != null) {
                    select.close();
                    select = null;
                }
                if(pooledConnection != null) {
                    pooledConnection.close();
                    pooledConnection = null;
                }
            }
            catch(java.sql.SQLException sqlException)
            {
                System.out.println("Error FINALLY DiarioReverso::SQLSelectVol [" + sqlException + "]");
                System.out.println("Error FINALLY DiarioReverso::SQLSelectVol::SQL [" + sqlSelect + "]");
            }
        }
        return valor;
    }

    public String Filler(String cadena, int largo) {
        int cadlong = cadena.length();
        for(int i=cadlong; i<largo; i++)
            cadena = cadena + " ";
        return cadena;
    }
    
    public boolean ValidateDSLZ(Hashtable Datos)
    {
        String tipo = Datos.get("D_SERIAL").toString().trim();
        String orden = Datos.get("D_DESCRIP").toString();
        String banco = Datos.get("D_REFER").toString().trim();
        if(banco.equals((String)session.getAttribute("identidadApp")))
            banco = "    ";
        else
            banco = "0001";
        String inqDSLZ = "DSLZ " + getBranch() + "    INQ1" + orden;
        inqDSLZ = inqDSLZ + "                             "+ banco + "00000000";
        inqDSLZ = Filler(inqDSLZ, 520);
        
        ventanilla.com.bital.sfb.Header2 header2 = new ventanilla.com.bital.sfb.Header2();
        header2.setReverso(inqDSLZ);
        header2.execute();
        String resp = header2.getMessage().trim();
        if(resp.startsWith("003")) 
        {
            int lcad = resp.length();
            if (lcad > 328)
                lcad = 328;
            String status = resp.substring(324, lcad).trim();
            setStatusDSLZ(status);
            String OSN = resp.substring(11,19);
            // LIBERACION ORDEN DE PAGO
            inqDSLZ = "DSLZ " + getBranch() + "    LIB ";
            
            String tmporden = orden;
            int refill = 33;
            int len = tmporden.length();
            for(int i = len; i < refill; i++)
                tmporden = tmporden + " ";
            
            inqDSLZ = inqDSLZ + tmporden + banco + OSN;
            inqDSLZ = Filler(inqDSLZ, 520);
            header2 = new ventanilla.com.bital.sfb.Header2();
            header2.setReverso(inqDSLZ);
            header2.execute();
            String resplib = header2.getMessage().trim();
            if(resplib.startsWith("003"))
            {
                if(status.equals(tipo))
                    return true;
                else if(status.equalsIgnoreCase("REA") && tipo.equalsIgnoreCase("EXP"))
                    return true;
            }
        }
        return false;
    }
    
    public void DeleteOrden(Hashtable datostxn)
    {
        String orden = datostxn.get("D_REFER1").toString().trim();
        String sqlUpdate = "DELETE from TW_ORD_PAG_RETRANS where C_NUM_ORDEN = " + orden + statementPS;
        
        Connection pooledConnection = ConnectionPoolingManager.getPooledConnection();
        Statement update = null;
        int ResultUpdate = 0;

        try {
            if(pooledConnection != null) {
                update = pooledConnection.createStatement();
                ResultUpdate= update.executeUpdate(sqlUpdate);
                if(ResultUpdate > 1)
                {
                    System.out.println("Error DiarioReverso::DeleteOrden::ResUpdate>1 [" + ResultUpdate + "]");                
                    System.out.println("Error DiarioReverso::DeleteOrden::SQL [" + sqlUpdate + "]");                
                }
                    
            }
        }
        catch(SQLException sqlException)
        {
            System.out.println("Error DiarioReverso::DeleteOrden [" + sqlException + "]");
            System.out.println("Error DiarioReverso::DeleteOrden::SQL [" + sqlUpdate + "]");                
        }
        finally {
            try {
                if(update != null) {
                    update.close();
                    update = null;
                }
                if(pooledConnection != null) {
                    pooledConnection.close();
                    pooledConnection = null;
                }
            }
            catch(java.sql.SQLException sqlException)
            {
                System.out.println("Error FINALLY DiarioReverso::DeleteOrden [" + sqlException + "]");
                System.out.println("Error FINALLY DiarioReverso::DeleteOrden::SQL [" + sqlUpdate + "]");                
            }
        }
    }

    public int UpdateSourceTxnR(int notxn) 
    {
        Hashtable Stxnr = new Hashtable();
        Stxnr = getTxn(notxn);
        Stxnr.remove("D_ESTATUS");
        
        Connection pooledConnection = ConnectionPoolingManager.getPooledConnection();
		PreparedStatement update = null;
        int ResultUpdate = 0;

        /*System.out.println("SQL Update::SourceTxnR ["+sql+"]");*/
        try {
            if(pooledConnection != null) {
				update = pooledConnection.prepareStatement("UPDATE TW_DIARIO_ELECTRON SET D_ESTATUS = ? WHERE D_CONNUREG = ?");
                update.setString(1,"?");
                update.setString(2,(String)Stxnr.get("D_CONNUREG"));
                ResultUpdate= update.executeUpdate();
                if(ResultUpdate > 1)
                {
                    System.out.println("DiarioReverso::UpdateSourceTxnR::ResUpdate>1 ["+ResultUpdate+"]");
                    System.out.println("DiarioReverso::UpdateSourceTxnR::SQL ["+sql+"]");
                }
            }
        }
        catch(SQLException sqlException)
        {
            System.out.println("Error DiarioReverso::UpdateSourceTxnR [" + sqlException + "]");
            System.out.println("Error DiarioReverso::UpdateSourceTxnR::SQL ["+sql+"]");
        }
        finally {
            try {
                if(update != null) {
                    update.close();
                    update = null;
                }
                if(pooledConnection != null) {
                    pooledConnection.close();
                    pooledConnection = null;
                }
            }
            catch(java.sql.SQLException sqlException)
            {
                System.out.println("Error FINALLY DiarioReverso::UpdateSourceTxnR [" + sqlException + "]");
                System.out.println("Error FINALLY DiarioReverso::UpdateSourceTxnR::SQL ["+sql+"]");
            }
        }
        return ResultUpdate;
    }

    public void DoTotales(HttpSession sesion, String Moneda)
    {
        String totalC = getTotal(1);
        String totalA = getTotal(2);

        boolean totc = false;
        boolean tota = false;

        StringTokenizer ST = new StringTokenizer(totalC, ",");
        String token = null;
        
        while(ST.hasMoreTokens()) 
        {
            token = ST.nextToken();
            if(token.equals("1"))
            {
                totc = true;
                break;
            }
        }
        
        ST = new StringTokenizer(totalA, ",");
        token = null;
        
        while(ST.hasMoreTokens()) 
        {
            token = ST.nextToken();
            if(token.equals("7"))
            {
                tota = true;
                break;
            }
        }

        if(totc || tota)
        {
        
            long EfeR = 0;
            long EfeP = 0;
            long efectivo = 0;
            long EfeLim = 0;
            long MontoFactor = 0;
            String mtoPif = "MontoFactor";
            String Rec = "EfeRec";
            String Pag = "EfePag";
            String efec = "efectivo";
            String alert = "AlertCash";
            String pifalert= "PIFAlertCash";
            
            if(Moneda.equals("US$"))
            {
                Rec = Rec + "2";
                Pag = Pag + "2";
                efec = efec + "2";
                alert = alert + "Dlls";
                pifalert =pifalert +"Dlls";
				mtoPif = mtoPif + "2";
            }
            
            EfeR = Long.parseLong(sesion.getAttribute(Rec).toString());
            EfeP = Long.parseLong(sesion.getAttribute(Pag).toString());
        
            if(totc)
                EfeR -= getMonto();
        
            if(tota)
                EfeP -= getMonto();

            efectivo = EfeR - EfeP;
            sesion.setAttribute(Rec, String.valueOf(EfeR));
            sesion.setAttribute(Pag, String.valueOf(EfeP));
        
            EfeLim = Long.parseLong(sesion.getAttribute(efec).toString());
            
            //CAMBIOPIF
			MontoFactor = Long.parseLong(sesion.getAttribute(mtoPif).toString());
			if(efectivo> MontoFactor )
			{
				sesion.setAttribute(pifalert, "1");
				sesion.setAttribute("efectivoPIF",String.valueOf(efectivo));
			}
			else
				sesion.setAttribute(pifalert, "0");

            if(efectivo > EfeLim)
                sesion.setAttribute(alert, "1");
            else
                sesion.setAttribute(alert, "0");
        }
    }

    public void setTotales(String C, String A)
    {
        TotC = C;
        TotA = A;
    }
    
    public String getTotal(int tot)
    {
        if(tot == 1)
            return TotC;
        else
            return TotA;
    }

    public void setMonto(String mto)
    {
        monto = Long.parseLong(mto);
    }
    
    public long getMonto()
    {
        return monto;
    }
    
  //******* OBTIENE LAS FUNCIONES ESPECIALES PARA CADA TXNS
    public Hashtable getDatosDiario(String Txn, String Moneda) {
        String DRBP = "";
        String DRAP = "";
        Hashtable Datos = new Hashtable();

        DataDiario datosd = DataDiario.getInstance();
        Vector Rselect = datosd.getDatosd(Txn,Moneda);
        if(Rselect != null)
        {
	        DRBP = Rselect.get(8).toString();;
	        DRAP = Rselect.get(9).toString();;
	        Datos.put("DRBP",DRBP);
	        Datos.put("DRAP",DRAP);        
        }
        return Datos;
    }
    
    public int SpecialFunc(String invoke, Hashtable txnRev, Hashtable txnOrg, StringBuffer Mensaje, String func, Vector Resp, String Connureg){
    	int msgcode = 0;
    	try{
    		StringTokenizer sFunc = new StringTokenizer(func, "*");
    		SpecialFunc Funciones = SpecialFunc.getInstance();
    		Hashtable functions = Funciones.getFunciones();
    		while(sFunc.hasMoreTokens()){
    			String codFunc = null;
    			String newFunc = null;
    			codFunc = sFunc.nextToken().toString().trim();
    			newFunc = (String)functions.get(codFunc);
	    		Class Funcion = null;
    			InterfazSF inter = null;
    			Funcion = Class.forName("ventanilla.com.bital.admin.SpFunc_"+ newFunc.toString());
				inter = (InterfazSF)Funcion.newInstance();
				if(invoke.equals("DRBP"))
					msgcode = inter.SpFuncionBR(txnRev, txnOrg, session, Connureg, supervisor);
				else
					msgcode = inter.SpFuncionAR(txnRev, txnOrg, session, Connureg, consecrev, supervisor, Resp);
    		}
    	}catch(Exception e){
    		System.out.println(e.getMessage());
    		e.printStackTrace();
    	}
    	return msgcode;
    }
    
    
    private boolean isTxnEspecial(String llaveOPP){
    	DatosVarios mot = DatosVarios.getInstance();                   
    	Vector vTxnEsp = new Vector();        
    	vTxnEsp = (Vector)mot.getDatosV("PROCESP");
    	StringTokenizer st = null;
    	StringTokenizer st2 = null;
    	String codigotxn = getTxnCode();//fpm codigotxn
    	String llave = "";
    	String llave2 = "";
    	boolean txnSpecial = false;

		for(int x = 0; x < vTxnEsp.size();x++){
			String cad = (String)vTxnEsp.elementAt(x);
			st = new StringTokenizer(cad,"*");
			llave = st.nextToken();		
			while(st.hasMoreElements()){
				String token = st.nextToken();
				st2 = new StringTokenizer(token,"-");
				while(st2.hasMoreElements()){						
					String token2 = st2.nextToken();
				   	if(token2.equals(llaveOPP)){
						llave2=token2;
						break;	
					}
				}
				if(token.equals(codigotxn)&&llave2.equals(llaveOPP)){
					txnSpecial = true;
					setFuncEsp(llave);
					break;
				}
			}				
		}				
    	return txnSpecial;    	
    }
    
    public void UpdateStatusCger(Hashtable datostxn)
    {
        String conureg_cger = datostxn.get("D_DESCRIP_REVERSO").toString().substring(16).trim();
        //System.out.println("impresion_reversa"+conureg_cger);
                
        String sucursal_destino =  datostxn.get("D_SUCURSAL").toString().trim();
        //System.out.println("suc exp "+ sucursal_destino);
        //System.out.println("connureg "+ conureg_cger);

		//Cambio de Status del Cheque en tw_chqger
				
		Connection pooledConnection = ConnectionPoolingManager.getPooledConnection();
		PreparedStatement update = null;
		String statementPS = com.bital.util.ConnectionPoolingManager.getStatementPostFix();
        
        	int ResultUpdate = 0;
	        try{
		 		pooledConnection = ConnectionPoolingManager.getPooledConnection();
		 		update = pooledConnection.prepareStatement("UPDATE TW_CHQGER SET D_STATUS=? WHERE D_SUC_EXP=? AND D_CONNUREG=?" + statementPS);
			    update.setString(1, "1");			    
			    update.setString(2, sucursal_destino.trim());
			    update.setString(3, conureg_cger.trim());
			    ResultUpdate= update.executeUpdate();
			    if(ResultUpdate > 0){
					System.out.println("RevImpresionCheque::UpdateImpCheque::Se actualiz� ["+ResultUpdate+"] registro");
					System.out.println("RevImpresionCheque::UpdateImpCheque::Se actualiz� el status a \"1\"");
				}
		 	}catch(Exception e){
		 		System.out.println(e.getMessage());
		 			 		
		 	}finally {		 			
	    		try{
	    			if(update != null) {
	            		update.close();
	                	update = null;
	            	}
	            	if(pooledConnection != null) {
	                	pooledConnection.close();
	                	pooledConnection = null;
	            	}
	        	}catch(java.sql.SQLException sqlException){
	            	System.out.println("Error FINALLY RevImpresionCheque::UpdateLog [" + sqlException + "]");	            	
              		
	        	}
		 	}	

    }

    public void DeleteSolCger(String connureg_orig, Hashtable datostxn)    
    {
      
        //String cuenta = datostxn.get("D_CUENTA").toString().trim();
        //String monto = datostxn.get("D_MONTO").toString().trim();
        String fecha_solicitud = datostxn.get("D_FECHOPER").toString().trim();        
        String sucursal_sol =  datostxn.get("D_SUCURSAL").toString().trim();
        //System.out.println("suc sol del "+ sucursal_sol);
        //System.out.println("connureg del"+ connureg_orig);
        //System.out.println("valor del"+ connureg_orig);

		//Eliminacion de registro de cheque de gerencia
				
		Connection pooledConnection = ConnectionPoolingManager.getPooledConnection();
		PreparedStatement delete = null;
		String statementPS = com.bital.util.ConnectionPoolingManager.getStatementPostFix();
        
        	int ResultDelete = 0;
	        try{
		 		pooledConnection = ConnectionPoolingManager.getPooledConnection();		 		
		 		//delete = pooledConnection.prepareStatement("DELETE FROM TW_CHQGER WHERE C_FECHA_SOL=? AND C_SUC_SOL=? AND D_CONNUREG=? AND D_CUENTA=? AND D_MONTO=?" + statementPS);
		 		delete = pooledConnection.prepareStatement("DELETE FROM TW_CHQGER WHERE C_FECHA_SOL=? AND C_SUC_SOL=? AND D_CONNUREG=?" + statementPS);
			    delete.setString(1, fecha_solicitud.trim());			    
			    delete.setString(2, sucursal_sol.trim());
			    delete.setString(3, connureg_orig.trim());
			    //delete.setString(4, cuenta.trim());
			    //delete.setString(5, monto.trim());
			    //delete.setString(4, monto.trim());
			    ResultDelete= delete.executeUpdate();
			    if(ResultDelete > 0){
					System.out.println("ReversoSolCG::DeleteSolCheque::Se borro ["+ResultDelete+"] registro");
					System.out.println("ReversoSolCG::DeleteSolCheque::Se borro el registro de la tabla TW_CHQGER ");
				}
		 	}catch(Exception e){
		 		System.out.println(e.getMessage());
		 			 		
		 	}finally {		 			
	    		try{
	    			if(delete != null) {
	            		delete.close();
	                	delete = null;
	            	}
	            	if(pooledConnection != null) {
	                	pooledConnection.close();
	                	pooledConnection = null;
	            	}
	        	}catch(java.sql.SQLException sqlException){
	            	System.out.println("Error FINALLY ReversoSolCheque::DeleteLog [" + sqlException + "]");	            	
              		
	        	}
		 	}	

    }
    
    
	public void DeleteP002(String connureg_orig, Hashtable datostxn)    
	{
		String cosecutivo= null;
		     
		cosecutivo = (String)datostxn.get("D_REFER3");
		
		if(cosecutivo == null)
			cosecutivo="";
		else
			cosecutivo = datostxn.get("D_REFER3").toString();

		//Eliminacion de registro de PAGO VOLUNTARIO
				
		if(!cosecutivo.equals(""))
		{		
		Connection pooledConnection = ConnectionPoolingManager.getPooledConnection();
		PreparedStatement delete = null;
		String statementPS = com.bital.util.ConnectionPoolingManager.getStatementPostFix();
        
			int ResultDelete = 0;
			try{
				pooledConnection = ConnectionPoolingManager.getPooledConnection();		 		
				delete = pooledConnection.prepareStatement("DELETE FROM TW_KRON WHERE D_NUM_TRANS=? AND D_FILLER=?" + statementPS);
				delete.setString(1, cosecutivo.trim());		
				delete.setString(2, connureg_orig);	    
				ResultDelete= delete.executeUpdate();
				if(ResultDelete > 0){
					System.out.println("DeleteP002::Se borro ["+ResultDelete+"] registro");
					System.out.println("DeleteP002::Se borro el registro de la tabla TW_KRON ");
				}
			}catch(Exception e){
				System.out.println(e.getMessage());
		 			 		
			}finally {		 			
				try{
					if(delete != null) {
						delete.close();
						delete = null;
					}
					if(pooledConnection != null) {
						pooledConnection.close();
						pooledConnection = null;
					}
				}catch(java.sql.SQLException sqlException){
					System.out.println("Error FINALLY ReversoP002::DeleteLog [" + sqlException + "]");	            	
              		
				}
			}
		}	

	}
	
    
}
