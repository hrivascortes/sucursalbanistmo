package ventanilla.com.bital.admin;


public class OperacionesDBBean
{
	String statementPS = com.bital.util.ConnectionPoolingManager.getStatementPostFix();
	int 	 intStatus = 0;

	public void setStatus(int newStatus)
	{
		intStatus = newStatus;
	}

	public int getStatus()
	{
		return intStatus;
	}

	public String EjecutaQuery(String strAccion)
	{
		java.sql.Connection pooledConnection = com.bital.util.ConnectionPoolingManager.getPooledConnection( );

		java.sql.Statement stmt = null;
		java.sql.ResultSet rs = null;
		String columnsData = new String("");
		try
		{
			if(pooledConnection != null)
			{
				stmt = pooledConnection.createStatement();
		//		strAccion+= statementPS;
				rs = stmt.executeQuery(strAccion);
				while(rs.next())
				{
					for(int columnNumber = 1; columnNumber <= rs.getMetaData().getColumnCount(); columnNumber++)
					{
						String columnData = rs.getString(columnNumber);
						if(columnData == null)
							columnData = "";
						columnsData += columnData.trim() + "~";
					}
				}
			}
		}
		catch(java.sql.SQLException sqlException)
		{
			System.out.println("OperacionesDBBean::EjecutaQuery [" + sqlException + "]");
		}
		finally
		{
			try
			{
				if(rs != null)
				{
					rs.close();
					rs = null;
				}
				if (stmt != null){
					stmt.close();
					stmt = null;
				}
				if(pooledConnection != null)
				{
					pooledConnection.close();
					pooledConnection = null;
				}
			}
			catch(java.sql.SQLException sqlException)
			{
		   	System.out.println("OperacionesDBBean::EjecutaQuery Error FINALLY [" + sqlException + "]");
			}
		}
		return columnsData;
	}

	public void InsertUpdateRegistro (String strAccion)
	{
		java.sql.Connection pooledConnection = com.bital.util.ConnectionPoolingManager.getPooledConnection( );
		java.sql.Statement stmt = null;
		try
		{
			if(pooledConnection != null)
			{
				stmt = pooledConnection.createStatement();
				intStatus = stmt.executeUpdate(strAccion);
				if (intStatus < 0)
					setStatus(-1);
			}
		}
		catch(java.sql.SQLException sqlException)
		{
			System.out.println("OperacionesDBBean::InsertUpdateRegistro Error [" + sqlException + "]");
			setStatus(-1);
		}
		finally
		{
			try
			{
				if(stmt != null)
				{
					stmt.close();
					stmt = null;
				}
				if(pooledConnection != null)
				{
					pooledConnection.close();
					pooledConnection = null;
				}
			}
			catch(java.sql.SQLException sqlException)
			{
		   	System.out.println("OperacionesDBBean::InsertUpdateRegistro Error FINALLY [" + sqlException + "]");
				setStatus(-1);
			}
		}
	}
}
