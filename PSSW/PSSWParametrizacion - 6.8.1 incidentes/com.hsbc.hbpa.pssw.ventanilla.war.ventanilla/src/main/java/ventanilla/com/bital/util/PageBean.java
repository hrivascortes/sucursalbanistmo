//*************************************************************************************************
//             Funcion: Clase que obtiene los campos que lleva una txn
//          Creado por: Alejandro Gonzalez Castro.
// Ultima Modificacion: 18/06/2004
//      Modificado por: Rutilo Zarco Reyes.
//*************************************************************************************************
// CCN -4360152- 21/06/2004 - Se modifica el acceso a los datos para obtenerlos en memoria
//*************************************************************************************************

package ventanilla.com.bital.util;

import java.util.Vector;

public class PageBean {
	private String transaction = null;
	private String iTxn = null;
	private String oTxn = null;
	private String iCurrency = null;
	private java.util.Hashtable txnInfo = null;
	private java.util.Vector<String> missingFields = null;
	private java.util.Vector<String> fieldsPerTxn = null;
	private java.util.Vector<Vector> fieldTxnContent = null;
	private java.util.Vector<Vector> fieldContent = null;
	private java.util.Vector<Vector> lstTipoChequera = null;
	private java.util.Vector<Vector> lstModelo = null;
	private java.util.Vector<Vector> lstColor = null;
	private java.util.Vector<Vector> lstNroCh = null;
	private java.util.Vector fieldContentListing = null;

	public PageBean() {
	}

	public PageBean(String txn) {
		transaction = txn;
	}

	public void setTransaction(String txn) {
		transaction = txn;
	}

	public String getTransaction() {
		return transaction;
	}


	public java.util.Hashtable getTxnInfo() {
		return txnInfo;
	}

	public void setTxnInfo(java.util.Hashtable txnInfo) {
		this.txnInfo = txnInfo;
	}

	public java.util.Vector<Vector> getLstTipoChequera() {
		return lstTipoChequera;
	}

	public void setLstTipoChequera(java.util.Vector<Vector> lstTipoChequera) {
		this.lstTipoChequera = lstTipoChequera;
	}

	public java.util.Vector<Vector> getLstModelo() {
		return lstModelo;
	}

	public void setLstModelo(java.util.Vector<Vector> lstModelo) {
		this.lstModelo = lstModelo;
	}

	public java.util.Vector<Vector> getLstColor() {
		return lstColor;
	}

	public void setLstColor(java.util.Vector<Vector> lstColor) {
		this.lstColor = lstColor;
	}

	public java.util.Vector<Vector> getLstNroCh() {
		return lstNroCh;
	}

	public void setLstNroCh(java.util.Vector<Vector> lstNroCh) {
		this.lstNroCh = lstNroCh;
	}

	public void setData(String newiTxn, String newoTxn, String newCurrency) {
		iTxn = newiTxn;
		oTxn = newoTxn;
		iCurrency = newCurrency;
	}

	public String fillField(String Data, int length) {
		int newTemp = Data.length();
		for (int i = newTemp; i < length; i++)
			Data = Data + " ";

		return Data;
	}

	public void removeNotWantedFields() {
		ventanilla.com.bital.beans.ExclrCampos cxl = ventanilla.com.bital.beans.ExclrCampos
				.getInstance();
		java.util.Vector notWantedFields = null;
		notWantedFields = new java.util.Vector();
		notWantedFields = cxl.getexclrcampos(fillField(iTxn, 4),
				fillField(oTxn, 4), fillField(transaction, 4),
				fillField(iCurrency, 2));

		for (int i = 0; i < notWantedFields.size(); i++)
			fieldsPerTxn.remove(notWantedFields.get(i));
	}

	public boolean checkRequiredFields(java.util.Hashtable newTxnInfo) {
		java.util.Vector<String> requiredFields = new java.util.Vector<String>();
		byte i = 0;
		boolean notmissingField = true;
		requiredFields.add("d_Campos_de_Txn");
		requiredFields.add("d_Descripcion");
		requiredFields.add("d_Nvl_Autoriz");
		requiredFields.add("d_Titulo_Imagen");
		while (i < requiredFields.size()) {
			if (newTxnInfo.get(requiredFields.get(i).toString().toUpperCase())
					.toString().trim().length() == 0) {
				if (missingFields == null)
					missingFields = new java.util.Vector<String>();
				missingFields.add(requiredFields.get(i));
				notmissingField = false;
			}
			i++;
		}
		return notmissingField;
	}

	public void execute() {
		ventanilla.com.bital.beans.CamposXTxn cxt = ventanilla.com.bital.beans.CamposXTxn
				.getInstance();
		txnInfo = cxt.getcamposxtxn(transaction);

		if (txnInfo != null) {
			boolean completeTxnInfo = checkRequiredFields(txnInfo);
			if (completeTxnInfo == true) {
				fieldsPerTxn = new java.util.Vector<String>();
				String txnFields = txnInfo.get(
						new String("d_Campos_de_Txn").toUpperCase()).toString();
				NSTokenizer newParser = new NSTokenizer(txnFields, "*");
				while (newParser.hasMoreTokens()) {
					String newToken = newParser.nextToken();
					if (newToken == null)
						continue;
					fieldsPerTxn.add(newToken);
				}
				removeNotWantedFields();
			}
		}

		if (fieldsPerTxn == null)
			fieldsPerTxn = new java.util.Vector<String>();
		for (int i = 0; i < fieldsPerTxn.size(); i++) {
			String nextField = fieldsPerTxn.get(i).toString().trim();
			ventanilla.com.bital.beans.CamposDisp cd = ventanilla.com.bital.beans.CamposDisp
					.getInstance();
			java.util.Vector fieldListing = cd.getcamposdisp(nextField,
					iCurrency);

			if (fieldTxnContent == null)
				fieldTxnContent = new java.util.Vector<Vector>();
			fieldTxnContent.add(fieldListing);

			if (nextField.length() > 0)




				if (nextField.substring(0, 3).equals("lst")
						|| nextField.substring(0, 3).equals("rdo")) {
					ventanilla.com.bital.beans.CamposL cl = ventanilla.com.bital.beans.CamposL
							.getInstance();
					java.util.Vector fieldContentListing = cl.getcamposl(
							nextField, iCurrency);

					if (fieldContent == null)
						fieldContent = new java.util.Vector<Vector>();
					fieldContent.add(fieldContentListing);

					if (transaction != null && transaction.equals("0101")) {
						if (nextField.equals("lstTipoCheq")) {
							if (lstTipoChequera == null)
								lstTipoChequera = new java.util.Vector<Vector>();
							setLstTipoChequera(cl.getcamposl(nextField, iCurrency));

						} else if (nextField.equals("lstModeloCh")) {
							if (lstModelo == null)
								lstModelo = new java.util.Vector<Vector>();
							setLstModelo(cl.getcamposl(nextField, iCurrency));

						} else if (nextField.equals("lstColorCh")) {
							if (lstColor == null)
								lstColor = new java.util.Vector<Vector>();
							setLstColor(cl.getcamposl(nextField, iCurrency));

						} else if (nextField.equals("lstNoChqs")) {
							if (lstNroCh == null)
								lstNroCh = new java.util.Vector<Vector>();
							setLstNroCh(cl.getcamposl(nextField, iCurrency));
						}
					}
				}

		}

		if (fieldContent == null)
			fieldContent = new java.util.Vector<Vector>();
		if (fieldContentListing == null)
			fieldContentListing = new java.util.Vector();
	}

	public java.util.Vector getSpecialAccounts() {
		ventanilla.com.bital.beans.CuentasEsp ctaesp = ventanilla.com.bital.beans.CuentasEsp
				.getInstance();
		java.util.Vector acctVector = ctaesp.getcuentasesp();
		return acctVector;
	}

	public java.util.Vector<String> getmissingFields() {
		return (java.util.Vector<String>) missingFields;
	}

	public java.util.Hashtable gettxnInfo() {
		return (java.util.Hashtable) txnInfo;
	}

	public java.util.Vector<Vector> getfieldTxnContent() {
		return (java.util.Vector<Vector>) fieldTxnContent;
	}

	public java.util.Vector<Vector> getContentListing() {
		return (java.util.Vector<Vector>) fieldContent;
	}
}
