//*************************************************************************************************
//             Funcion: Bean Txn 4193 SOLICTUD DE CHEQUES DE GERENCIA 
//             Elaborado por: YGX
//*************************************************************************************************

package ventanilla.com.bital.sfb;


import ventanilla.GenericClasses;
import com.bital.util.ParameterNotFoundException;
import com.bital.util.ParameterParser;


public class Txn_4193 extends Transaction
{		 
	public Header doAction(ParameterParser param)
		throws ParameterNotFoundException
	{				
        GenericClasses gc = new GenericClasses();                				
		ventanilla.com.bital.sfb.Cheques Cheques = new ventanilla.com.bital.sfb.Cheques();	   	
		   
		   Cheques.setTxnId("ODPA");
		   Cheques.setTxnCode("4193");
		   Cheques.setFormat("A");	


	       Cheques.setAcctNo(param.getString("txtDDACuenta"));
	       Cheques.setTranAmt(gc.delCommaPointFromString(param.getString("txtMonto")));  
	       
	       int moneda = param.getInt("moneda");    
	       Cheques.setTranCur( gc.getDivisa(param.getString("moneda")));	
		   
		   Cheques.setTranDesc("CARGO SOLICITUD EMISION CG");	
		   Cheques.setBranch(param.getCString("sucursal"));
		   Cheques.setTeller(param.getCString("teller"));	
		   Cheques.setMoAmount("000");
		   
		   ////Reverso
		   String desc = "";                     
           desc = " REV CARGO SOLICITUD EMISION CG";
           Cheques.setDescRev("REV CARGO SOLICITUD EMISION CG");
           
           //System.out.println(Cheques.getDescRev());
    	  
       return Cheques;
	}
}
