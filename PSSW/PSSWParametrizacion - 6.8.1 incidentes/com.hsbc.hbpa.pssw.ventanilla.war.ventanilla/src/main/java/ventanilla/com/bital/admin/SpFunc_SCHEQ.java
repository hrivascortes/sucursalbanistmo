//*************************************************************************************************
//          Funcion: Clase especial para las funciones especiales de Solicitud de Chequera.
//          Elemento: SpFunc_SCHEQ.java
//          Creado por: yulani.barrazadifilippo@tcs.com
//          Modificado por:
//*************************************************************************************************
package ventanilla.com.bital.admin;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Hashtable;
import java.util.Vector;

import javax.servlet.http.HttpSession;

import com.bital.util.ConnectionPoolingManager;

public class SpFunc_SCHEQ implements InterfazSF {

	public SpFunc_SCHEQ() {
	}

	public int SpFuncionBD(Object bean, Vector Respuesta, HttpSession session,
			String Connureg) {
		int messagecode = 0;

		Hashtable datasession = (Hashtable) session
				.getAttribute("page.datasession");

		long consecutivoChequera = obtenerUltConsecutivoCheq((String) datasession
				.get("txtCtaSIB")) + 1;
		datasession.put("txtConsecutCheq", consecutivoChequera);

		session.setAttribute("page.datasession", datasession);
		return messagecode;
	}

	public int SpFuncionAD(Object bean, Vector Respuesta, HttpSession session,
			String Connureg) {

		int messagecode = 0;

		Hashtable datasession = (Hashtable) session
				.getAttribute("page.datasession");

		String oTxn = (String) datasession.get("oTxn");

		if (oTxn.equals("0101")) {
			long consecutivoChequera = obtenerUltConsecutivoCheq((String) datasession
					.get("txtCtaSIB")) + 1;
			datasession.put("txtConsecutCheq", consecutivoChequera);

			session.setAttribute("page.datasession", datasession);
		}
		return messagecode;

	}

	public int SpFuncionBR(Hashtable txnRev, Hashtable txnOrg,
			HttpSession session, String Connureg, String Supervisor) {
		int messagecode = 0;
		return messagecode;
	}

	public int SpFuncionAR(Hashtable txnRev, Hashtable txnOrg,
			HttpSession session, String Connureg, String Cosecutivo,
			String Supervisor, Vector VResp) {
		int messagecode = 0;
		return messagecode;
	}

	public long obtenerUltConsecutivoCheq(String numCuenta) {
		long ultConsecutivoCheq = 0;

		Connection pooledConnection = ConnectionPoolingManager
				.getPooledConnection();
		PreparedStatement select = null;
		ResultSet Rselect = null;
		try {
			if (pooledConnection != null) {
				select = pooledConnection
						.prepareStatement("SELECT MAX(CTRLCHQ_CHQ_FINAL) CTRLCHQ_CHQ_FINAL FROM TW_CTRLCHQ_SEC WHERE CTRLCHQ_CUENTA = ? ");
				select.setString(1,
						String.format("%015d", Integer.parseInt(numCuenta)));
				Rselect = select.executeQuery();
				if (Rselect.next()) {
					ultConsecutivoCheq = Rselect.getLong("CTRLCHQ_CHQ_FINAL");
				}
			}
		} catch (SQLException sqlException) {
			System.out.println("TW_CTRLCHQ_SEC::obtenerUltConsecutivoCheq ["
					+ sqlException + "]");
			sqlException.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (Rselect != null) {
					Rselect.close();
					Rselect = null;
				}
				if (select != null) {
					select.close();
					select = null;
				}
				if (pooledConnection != null) {
					pooledConnection.close();
					pooledConnection = null;
				}
			} catch (java.sql.SQLException sqlException) {
				System.out
						.println("Error FINALLY TW_CTRLCHQ_SEC::obtenerUltConsecutivoCheq ["
								+ sqlException + "]");
				sqlException.printStackTrace();
			}
		}

		System.out.println("obtenerUltConsecutivoCheq(" + numCuenta + "):: "
				+ ultConsecutivoCheq);

		return ultConsecutivoCheq;
	}
}
