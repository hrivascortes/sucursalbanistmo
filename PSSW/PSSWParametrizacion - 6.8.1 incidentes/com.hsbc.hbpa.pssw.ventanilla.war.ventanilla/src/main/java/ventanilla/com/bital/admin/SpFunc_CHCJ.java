//*************************************************************************************************
//          Funcion: Spfunc para CHCJ
//          Elemento: SpFunc_CHCJ.java
//          Creado por: TCS 26/05/2015
//          Modificado por:
//*************************************************************************************************
/*
 Implementar el m�todo SpFuncionAD con la l�gica para realizar la persistencia sobre la
 tabla TW_CHEQUE_CANJE de los campos requeridos, (ver campos reporte) obtenidos a trav�s del campo en Sesi�n
 "page.datasession�, si solo si, la transacci�n se encuentra en el listado de transacciones para reporte,
 almacenada en la variable txnCheqR en el contexto de la aplicaci�n.
 '1053*4011*4019*4103*4153*4537*4539*4545'
 */
//*************************************************************************************************

package ventanilla.com.bital.admin;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Hashtable;
import java.util.Vector;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpSession;

import ventanilla.GenericClasses;

import com.bital.util.ConnectionPoolingManager;

public class SpFunc_CHCJ extends HttpServlet implements InterfazSF {

	public int SpFuncionAD(Object bean, Vector Respuesta, HttpSession session,
			String Connureg) {

		int messagecode = 0;
		GenericClasses gc = new GenericClasses();

		Hashtable datasession = (Hashtable) session
				.getAttribute("page.datasession");
		Hashtable usrinfo = (Hashtable) session.getAttribute("userinfo");

		String SUCURSAL = convertCampo((String) datasession.get("sucursal"), 5);
		String NUM_OPERADOR = convertCampo((String) datasession.get("teller"),6);
		
		//Transaccion
		String cTxn = (String) datasession.get("cTxn");
		if (cTxn.equals("0061")) {
			cTxn = "4011";
		}
		
		//end Transaccion
		//Cuenta
		String noCuenta = "";
    if (("0604".equals(cTxn)) && (datasession.get("DDAARP") != null)) {
      noCuenta = ((String)datasession.get("DDAARP")).trim();
    } else if (datasession.get("txtDDACuenta2") != null) {
      noCuenta = (String)datasession.get("txtDDACuenta2");
    } else if (datasession.get("txtDDACuenta") != null) {
      noCuenta = (String)datasession.get("txtDDACuenta");
    } else {
      noCuenta = "0103000023";
    }

		
		
		String CUENTA = convertCampo(noCuenta, 15);
		String COD_TRX = convertCampo(cTxn, 4);
		//endCuenta
		//MontoCheque
		String monto = null;
		if (datasession.get("txtMonto") != null) {
			monto = (String) datasession.get("txtMonto");

		} else if (datasession.get("txtMontoCheque") != null) {
			monto = (String) datasession.get("txtMontoCheque");
		}
		Object IMPORTE = convertCampo(monto, 15);
		//endMontoCheque
		//NumCheque
		String numCheque = "0";
		String NUM_CHEQUE = convertCampo(numCheque, 10);
		if (datasession.get("noCheque") != null) {
			NUM_CHEQUE = convertCampo((String) datasession.get("noCheque"), 10);
			if (cTxn.equals("5103")) {
				NUM_CHEQUE	 = convertCampo((String) datasession.get("txtNoCheqExt"),10);
			}
		}
		//endNumCheque
		//Secuencial
		Date date = new Date();
		DateFormat hourFormat = new SimpleDateFormat("HHmmss");
		DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
		Object NUM_SEC = hourFormat.format(date);		
		//EndSecuencial
		//DVCuenta
		String NUM_DV_CTA = "";
		if (datasession.get("dvCuenta") != null) {
			NUM_DV_CTA = (String) datasession.get("dvCuenta");
		}
		//EndDVCuenta
		//DVSerial
		String NUM_DV_CHEQ = "";
		if (datasession.get("dvCheque") != null) {
			NUM_DV_CHEQ = (String) datasession.get("dvCheque");
		}
		//EndDVSerial

		String DIVISA = (String) datasession.get("moneda");
		String TIPO_CHEQUERA = "Z";
		String TIPO_CHEQUE ="N";
		if (datasession.get("isChequeFormatoNuevo") != null && !(cTxn.equals("4019") || cTxn.equals("5103"))) {
			TIPO_CHEQUE = convertCampo(
					(String) datasession.get("isChequeFormatoNuevo"), 0);
		}
		String TIPO_OPER = "Z";
		String TIPO_BANDA = "";
		String USUARIO_ULT_MOD = NUM_OPERADOR;
		
		if (cTxn.equals("5353") && noCuenta.contains("103000023")) {
			COD_TRX = "4103";
			NUM_DV_CTA ="1";
			String tmp = (String) datasession.get("noCheque");
			NUM_DV_CHEQ = modulo7(Integer.parseInt(tmp));
		}
		String cadena = SUCURSAL + "-" + NUM_OPERADOR + "-" + COD_TRX + "-"
				+ CUENTA + "-" + NUM_DV_CTA + "-" + IMPORTE + "-" + DIVISA
				+ "-" + NUM_CHEQUE + "-" + NUM_DV_CHEQ + "-" + NUM_SEC + "-"
				+ TIPO_CHEQUERA + "-" + TIPO_CHEQUE + "-" + TIPO_OPER + "-"
				+ TIPO_BANDA;
		

		/*
		 * Validaci�n de campos null - el nombre de las variables pueden tomar
		 * un nombre difierente por cada transacci�n
		 */

		if (NUM_DV_CTA == null) {
			TIPO_CHEQUE = "A";
			TIPO_BANDA = "A";
		} else {
			TIPO_CHEQUE = "N";
			TIPO_BANDA = "N";
		}
		Connection pooledConnection = ConnectionPoolingManager
				.getPooledConnection();
		PreparedStatement insert = null;

		int ResultInsert = 0;

		try {
			if (pooledConnection != null) {
				/*
				 * SUCURSAL NUM_OPERADOR COD_TRX CUENTA NUM_DV_CTA IMPORTE
				 * DIVISA NUM_CHEQUE NUM_DV_CHEQ NUM_SEC FECHA TIPO_CHEQUERA
				 * TIPO_CHEQUE TIPO_OPER TIPO_BANDA USUARIO_ULT_MOD
				 */
				insert = pooledConnection
						.prepareStatement("INSERT INTO PSPP101.TW_CHEQUE_CANJE (SUCURSAL,NUM_OPERADOR,COD_TRX,CUENTA,NUM_DV_CTA,IMPORTE,DIVISA,NUM_CHEQUE,NUM_DV_CHEQ,"
								+ "NUM_SEC,FECHA,TIPO_CHEQUERA,TIPO_CHEQUE,TIPO_OPER,TIPO_BANDA,USUARIO_ULT_MOD,FECHA_ULT_OPER, D_CONNUREG) "
								+ "VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");

				insert.setString(1, SUCURSAL);
				insert.setString(2, NUM_OPERADOR);
				insert.setString(3, COD_TRX);
				insert.setString(4, CUENTA);
				insert.setString(5, NUM_DV_CTA);
				insert.setObject(6, IMPORTE);
				insert.setString(7, DIVISA);
				insert.setString(8, NUM_CHEQUE);
				insert.setString(9, NUM_DV_CHEQ);
				insert.setObject(10, NUM_SEC);
				insert.setString(11, dateFormat.format(new Date()));
				insert.setString(12, TIPO_CHEQUERA);
				insert.setString(13, TIPO_CHEQUE);
				insert.setString(14, TIPO_OPER);
				insert.setString(15, TIPO_BANDA);
				insert.setString(16, USUARIO_ULT_MOD);
				insert.setTimestamp(17, new Timestamp(new Date().getTime()));
				insert.setString(18, Connureg);

				ResultInsert = insert.executeUpdate();
			}
		} catch (SQLException sqlException) {
			System.err.println("SpFunc_CHCJ::SpFuncionAD [" + sqlException
					+ "]");
		} catch (Exception e) {
			System.err.println("SpFunc_CHCJ:: " + e.getMessage());
			e.printStackTrace();
		} finally {
			try {
				if (insert != null) {
					insert.close();
					insert = null;
				}
				if (pooledConnection != null) {
					pooledConnection.close();
				}
			} catch (java.sql.SQLException sqlException) {

				sqlException.printStackTrace();
				System.out.println("Error FINALLY SpFunc_CHCJ::SpFuncionAD ["
						+ sqlException + "]");
			}
		}

		return messagecode;
	}

	public int SpFuncionBR(Hashtable txnRev, Hashtable txnOrg,
			HttpSession session, String connureg, String supervisor) {
		return 0;
	}

	public int SpFuncionAR(Hashtable txnRev, Hashtable txnOrg,
			HttpSession session, String Connureg, String Cosecutivo,
			String Supervisor, Vector VResp) {
		int messagecode = 0;

		Connection pooledConnection = ConnectionPoolingManager
				.getPooledConnection();
		PreparedStatement update = null;
		String statementPS = com.bital.util.ConnectionPoolingManager
				.getStatementPostFix();

		int ResultUpdate = 0;
		try {
			
			String connRegRecortado = txnOrg.get("D_CONNUREG").toString();
			connRegRecortado = connRegRecortado.substring(0,14)+"%";
			
			pooledConnection = ConnectionPoolingManager.getPooledConnection();
			update = pooledConnection
					.prepareStatement("DELETE FROM PSPP101.TW_CHEQUE_CANJE WHERE D_CONNUREG like ?"
							+ statementPS);
			update.setString(1, connRegRecortado);
			ResultUpdate = update.executeUpdate();

		} catch (SQLException sqlException) {
			System.err.println("SpFunc_CHCJ::DeleteLog [" + sqlException + "]");
		} catch (Exception e) {
			System.err.println("SpFunc_CHCJ:: " + e.getMessage());
			e.printStackTrace();
		} finally {
			try {
				if (update != null) {
					update.close();
					update = null;
				}
				if (pooledConnection != null) {
					pooledConnection.close();
					pooledConnection = null;
				}
			} catch (java.sql.SQLException sqlException) {

				sqlException.printStackTrace();
				System.out.println("Error FINALLY SpFunc_CHCJ::deleteLog ["
						+ sqlException + "]");

			}
		}

		return messagecode;
	}

	public int SpFuncionBD(Object bean, Vector respuesta, HttpSession session,
			String connureg) {
		return 0;
	}

	public String convertCampo(String str, int longitud) {
		str = str.replace(".", "").replace(",", "");
		while (str.length() < longitud)
			str = "0" + str;

		return str;
	}

		public static String modulo7(int DVch) {
			int res = DVch % 7;
			DVch = 7 - res;
			String DvCh = Integer.toString(DVch);
			return DvCh;
		}

}