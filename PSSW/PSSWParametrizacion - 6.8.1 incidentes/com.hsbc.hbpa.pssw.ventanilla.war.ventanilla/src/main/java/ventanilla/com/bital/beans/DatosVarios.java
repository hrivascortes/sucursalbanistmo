//*************************************************************************************************
//			   Funcion: Clase que realiza la consulta a Datos Varios
//			Creado por: Juvenal R. Fernandez V.
//		Modificado por: Fausto Rodrigo Flores Moreno
//		Modificado por: Carolina Vela
//*************************************************************************************************
// CCN - 4360160- 30/06/2004 - Se crea la clase para realizar select y mantener los datos en memoria
// CCN - 4360260 21/01/2005 - Se agrega codigo para colocar los datos de CRM en memoria(tabla hash)
// CCN - 4360276 17/02/2005 - Se agrega codigo para colocar los datos de Motivator y Rap cobranza personalizada.
// CCN - 4360291 - 18/03/2005 - Se unifican las tabas de TW_DIARIO Y TW_DIARIO_RESPUEST
// CCN - 4360296 - 23/03/2005 - Se cambia CCN por peticion de QA, CCN anterior 4360291
// CCN - 4360334 - 17/06/2005 - Se elimina variable dbQualifier evitando enviar "." antes del nombre de la tabla
// CCN - 4360342 - 08/07/2005 - Se realizan modificaciones para departamentos.
// CCN - 4360356 - 19/08/2005 - Se agregan SubProductos de Inversiones en Persistencia.
// CCN - 4360387 - 19/08/2005 - Se agregan Cuentas compa�ia de CFE en Persistencia y Salaraio minimo.
// CCN - 4360425 - 03/02/2006 - Proyecto Loch Ness
// CCN - 4360437 - 24/02/2006 - Se realizan modificaciones para mensajes de ultima hora
// CCN - 4360456 - 07/04/2006 - Se agraga c�digo para control txns secundarias para departamentos.
// CCN - 4360493 - 23/06/2006 - Se agrega codigo para poner en persistencia datos de proveedores en txn 1053
// CCN - 4360510 - 08/09/2006 - Se agrega codigo para poner en persistencia datos de los perfiles de usuarios y procesos dsiponibles
// CCN - 4360533 - 20/10/2006 - Se realizan modificaciones para OPEE
// CCN - 4360576 - 09/03/2007 - Modificaciones para proyecto OPMN
// CCN - 4360587 - 03/04/2007 - Modificaciones para monitoreo de efectivo
// CCN - 4360602 - 18/05/2007 - Se agrega llave para lectura de fechas limite de pago suas codigo de barras
// CCN - 4620008 - 14/09/2007 - 
// CCN - 4620021 - 17/10/2007 - Se agrega llave para nuevo esquema de funciones especiales, Humberto Balleza
// CCN - 4620043 - 17/10/2007 - Se agrega llave para nuevo esquema de funciones especiales, Yessica Garcia
//*************************************************************************************************

package ventanilla.com.bital.beans;

import ventanilla.com.bital.sfb.ServicioRAPL;
import ventanilla.com.bital.util.DBQuery;

import java.util.*;

public class DatosVarios
{
	
	private Hashtable deptos = null;
	private Hashtable datosv = null;
	private static DatosVarios instance = null;
	private Hashtable TablaDiario = null;
	
	private DatosVarios() 
	{
		datosv = new Hashtable();
		execute();
	}
    
	private boolean hasMoredata(String cadena){
		StringTokenizer dat = new StringTokenizer(cadena,"*");
		boolean more=false;
		dat.nextToken();
		if(dat.nextToken().toString().length()<4){
		  more = true;
		}
		return more;
	}
    
	private Vector searchMoreLines(int pos , String llave, Vector regs){
	   Vector infodepto = new Vector();
	   String cadTemp = "";
	   String cadRowdat = "";
	   String deptrow="";
	   String dept = ((Vector)regs.get(pos)).get(1).toString().substring(0,4);
	   int total_reg = 0;
	   int newpos = 4;
	   int j=pos;
	   int cont=0;
	   int g=0;
	   boolean nomore=true;
              
	   while(j<regs.size() && nomore){
		 deptrow = ((Vector)regs.get(j)).get(1).toString().substring(0,4);
		 if(llave.equals(((Vector)regs.get(j)).get(0).toString().trim()) && dept.equals(deptrow)){
		   cadRowdat = ((Vector)regs.get(j)).get(1).toString();       
		   if(hasMoredata(cadRowdat)){
			 while(g<cadRowdat.length() && cont<2){
				if(cadRowdat.charAt(g)=='*'){
				   cont++;
				}
			   g++;
			 }
			 newpos = g;
		   }
		   g=0;cont=0;
		   if(total_reg<1){
			 if(newpos>4)
			   cadTemp =  cadRowdat.substring(0,4)+"*"+cadRowdat.substring(newpos);
		   else
			   cadTemp =  cadRowdat.substring(0,4)+cadRowdat.substring(newpos);
		   }else{
			   cadTemp = cadTemp + cadRowdat.substring(newpos);
			   }
		   total_reg++;
		 }else
		   nomore=false;
		 j++;
	   }
	   j=j-2;
	   infodepto.addElement(cadTemp) ;
	   infodepto.addElement(String.valueOf(j));
	   return infodepto;
	}
    	
	public static synchronized DatosVarios getInstance()
	{
		if(instance == null){
			instance = new DatosVarios();
		}
		return instance;
	}

	public Hashtable getDatosH(String clave)
	{   
		Hashtable tmp = new Hashtable();
		if(datosv.containsKey(clave)){
		 tmp = (Hashtable)this.datosv.get(clave);
		}
		return tmp;
	}
	public String getDatosv(String clave)
	{
		String dato = null;
		if(datosv.containsKey(clave)){
		  dato = (String)datosv.get(clave);
		}
		return dato;
	}
	public Vector getDatosV(String clave)
	{   
		Vector tmp = new Vector();
		if(datosv.containsKey(clave)){
		 tmp = (Vector)this.datosv.get(clave);
		}
		return tmp;
	}    
    
	public void execute() 
	{
		String sql = null;
		sql = "SELECT C_CLAVE, D_DATOS FROM ";
		String clause = null;
		clause = " ORDER BY C_CLAVE,D_DATOS";
        
		DBQuery dbq = new DBQuery(sql,"TC_DATOS_VARIOS", clause);
		Vector Regs = dbq.getRows();
        
		int size = Regs.size();
		Vector row = new Vector();
		Vector DatV = new Vector();
		Vector DatVec = new Vector();
		Vector MotivatorTxn = new Vector();
		Vector VecDatosCp = new Vector();
		Vector DatCFE = new Vector();
		Vector DatAVISOS = new Vector();
		Vector ProvChequeras = new Vector();
		Vector vecEsp = new Vector();
		Vector vecEsps = new Vector();
		Vector infoDep;
		Vector perfilDesc = new Vector(); 
		Vector procesos = new Vector();
		Vector MontosOPMN = new Vector();
		Vector pif = new Vector();
		Vector suas = new Vector();
		Vector carp = new Vector();
		Vector krou = new Vector();
		Vector kror = new Vector();
		Vector kroc = new Vector();
		Vector krog = new Vector();
		Hashtable crmrt = new Hashtable();
		Hashtable perfilTxn = new Hashtable();
		Hashtable motiv = new Hashtable();
		Hashtable htPWDS = new Hashtable();
		Hashtable htURL = new Hashtable();		
		String clave = null; 
		String siguiente = null;
		String keyp = "";
		String keyant = "";
		String ptemp = null;
        Vector paises = new Vector();
    	String rubro = "";
        Hashtable htDatosRAPL = new Hashtable();
        String ident = "";
        String ident2 = "";
        String ident1 = "";
        String ident3 = "";

		
		for(int i=0; i<size; i++)
		{
			row = (Vector)Regs.get(i);
			clave = row.get(0).toString().trim();
			String data = row.get(1).toString();
            
			if(i < size-1)
			{
			   Vector next = (Vector)Regs.get(i+1);
			   siguiente = next.get(0).toString().trim();
			}
			else 
				siguiente = null;
            	
		                
			if (clave.startsWith("MOT"))
			{
				DatV.addElement(data);
			}  	
			else if(clave.startsWith("LOC")) 
				datosv.put(clave,data.trim());
			else if(clave.equals("CRMRTV")||clave.equals("DEPTOS"))
			{   
                
				if(clave.equals("DEPTOS")){
				  infoDep = searchMoreLines(i,clave, (Vector)Regs);
				  data = infoDep.get(0).toString().trim();
				  i = Integer.parseInt(infoDep.get(1).toString());
				  siguiente = ((Vector)Regs.get(i+1)).get(0).toString().trim();
				}
                
				StringTokenizer DatosCRM = new StringTokenizer(data,"*");
				String key = DatosCRM.nextToken();
				Vector DatosTxn = new Vector();
				while(DatosCRM.hasMoreTokens())
				{
					DatosTxn.addElement(DatosCRM.nextToken());
				}
				crmrt.put(key,DatosTxn);
			} 
			else if(clave.startsWith("RAPCP"))
			 {
				StringTokenizer DatCp = new StringTokenizer(data,"*");
				Vector DatosCp = new Vector();
				while(DatCp.hasMoreTokens())
				{
					DatosCp.addElement(DatCp.nextToken());
				}
				VecDatosCp.addElement(DatosCp);
                
			 }else if(clave.equals("RAPBLOQCFE"))
					{
					DatCFE.addElement(data);	
			 }else if(clave.equals("AVISOS")){
					DatAVISOS.addElement(data);
			 }else if(clave.equals("PROVCHQ")){
					ProvChequeras.addElement(data);
			 }else if(clave.equals("PROCESP")){
					vecEsp.addElement(data); 
			 }else if(clave.equals("PROCESPS")){
					vecEsps.addElement(data);   
			 }else if(clave.equals("PIFFACTOR")){
					pif.addElement(data);
			 }else if(clave.equals("SUASPCD")){
					suas.addElement(data);		
			 }else if(clave.equals("ARP")){
					carp.addElement(data);	
			 }else if(clave.equals("USERKRONER")){
					krou.addElement(data);	
			}else if(clave.equals("ROLEKRONER")){
					kror.addElement(data);
			}else if(clave.equals("CICSSERVER")){
					kroc.addElement(data);	
			}else if(clave.equals("GATEWAYURL")){
					krog.addElement(data);               	    
			}else if(clave.equals("PERFIL")){             	    
             	    
				if(data.substring(3,7).equals("0000")){
				   perfilDesc.addElement(data);
				}else{
					 keyant = keyp;
					 keyp = data.substring(0,2);         	               	       	         	         

					 Vector DatosTxn = new Vector();
					 if(keyp.equals(keyant)){
					   DatosTxn = (Vector)perfilTxn.get(keyp);
					   data = data.substring(3,data.length());
					 }	                 
					 StringTokenizer datosperfil = new StringTokenizer(data,"*");          	       	          	       	     	                 
					 while(datosperfil.hasMoreTokens())
					 {
						DatosTxn.addElement(datosperfil.nextToken());
					 }
					   perfilTxn.put(keyp,DatosTxn);           		   	           	  	   
				}               		   
         	    
			 }else if(clave.equals("PROCESOS")){
					procesos.addElement(data);
			 }else if(clave.equals("MONTOSOPMN")){
					StringTokenizer montosMax = new StringTokenizer(data,"*");             	    
					MontosOPMN.addElement(String.valueOf(Integer.parseInt(montosMax.nextToken())));
					MontosOPMN.addElement(String.valueOf(Integer.parseInt(montosMax.nextToken())));
			 }else if (clave.equals("PAISES")){
                	  paises.addElement(data);
             }else if(clave.startsWith("SERVICRAPL"))
		 {
			StringTokenizer DatRAPL = new StringTokenizer(data,"*");
			ServicioRAPL srapl = new ServicioRAPL();
			//1700*Sabritas*S*N*N*T*F*
			//1239*sky*CREF,NPAG,NREV,*S*S*
			srapl.setNoServicio(DatRAPL.nextToken());
			srapl.setDescServicio(DatRAPL.nextToken());
		
			String servs=(String)DatRAPL.nextElement();
				StringTokenizer DatServRAPL = new StringTokenizer(servs,",");
				while(DatServRAPL.hasMoreTokens()){
					String stserv=DatServRAPL.nextToken();
					if(stserv.compareTo("CREF")==0){
						srapl.setConsulta(true);
					}
					if(stserv.compareTo("NPAG")==0){
						srapl.setNotificacion(true);
					}
					if(stserv.compareTo("NREV")==0){
						srapl.setCancelacion(true);
					}
				}
		
			
			if(0==DatRAPL.nextToken().compareTo("S")){
				srapl.setTipoPago("P");
			}
			else{
				srapl.setTipoPago("T");
			}
			
			if(0==DatRAPL.nextToken().compareTo("S")){
				srapl.setFolio(true);
			}
			
			htDatosRAPL.put(srapl.getNoServicio(),srapl);
		 }
            else if (clave.equals("RUBRO")){
    	            rubro = data;
                    
			 }else if(clave.equals("PWDS")){
					StringTokenizer PWDS = new StringTokenizer(data,"*");  
					while(PWDS.hasMoreTokens()){
						String llave = PWDS.nextToken();
						String valor = PWDS.nextToken();
						htPWDS.put(llave,valor);
					}
			 }else if(clave.equals("URLS")){
					StringTokenizer URLS = new StringTokenizer(data,"*");  
					while(URLS.hasMoreTokens()){
						String llave = URLS.nextToken();
						String valor = URLS.nextToken();
						htURL.put(llave,valor);
					}
			 }
			  else if (clave.equals("IDENTIDAD")){
  	            ident = data;
			  }
			  else if (clave.equals("IDENTIDAD1")){
	  	            ident1 = data;
				  }
			  else if (clave.equals("IDENTIDAD2")){
	  	            ident2 = data;
				  }
			  else if (clave.equals("IDENTIDAD3")){
	  	            ident3 = data;
				  }
            
            
            
            
            
			if( (siguiente == null) || !clave.equals(siguiente))
			{
				if (clave.startsWith("MOT"))
				{
					DatVec.addElement(clave.substring(9,10));
					DatVec.addElement(DatV);
				DatV = new Vector();	
					datosv.put(clave.substring(0,9),DatVec);
				}
				else if(clave.equals("CRMRTV")||clave.equals("DEPTOS"))
				{
					datosv.put(clave,crmrt);
					crmrt = new Hashtable();
				}
				else if(clave.equals("IMPTRUNCAM"))
					datosv.put(clave, row); 
				else if(clave.startsWith("RAPCP"))
					datosv.put("RAPCP", VecDatosCp); 	
				else if(clave.startsWith("SUBPROD"))
					datosv.put("SUBPROD", row);
				else if(clave.equals("RAPBLOQCFE"))
					datosv.put("RAPBLOQCFE", DatCFE);
				else if(clave.equals("TB")) // Salario Minimo
					datosv.put("Salario.Minimo", row);
				else if(clave.equals("AVISOS")) // Avisos Alerta
					datosv.put("AVISOS", DatAVISOS);
				else if(clave.equals("PROVCHQ")) //Proveedor Chequeras
					datosv.put("PROVCHQ",ProvChequeras);
				else if(clave.equals("PROCESP")) 
					datosv.put("PROCESP",vecEsp);
				else if(clave.equals("PROCESPS")) 
					datosv.put("PROCESPS",vecEsps);            		
				else if(clave.equals("PIFFACTOR")) 
					datosv.put("PIFfactor",pif);	
				else if(clave.equals("SUASPCD")) 
					datosv.put("SUASPCD",suas);
				else if(clave.equals("ARP")) 
					datosv.put("ARP",carp);
				else if(clave.equals("PERFIL")){				
					datosv.put("PERFIL", perfilDesc);
					datosv.put("PERFILT", perfilTxn);					
					perfilTxn = new Hashtable();
				}	
				else if(clave.equals("PROCESOS"))
					datosv.put("PROCESOS", procesos);
				else if(clave.equals("MONTOSOPMN")){
					datosv.put("MontosOPMN", MontosOPMN);	
				}else if(clave.equals("PWDS")){				
					datosv.put("PWDS", htPWDS);
				}else if(clave.equals("ARP")){
					datosv.put("CARP", carp);	
				}else if(clave.equals("USERKRONER")){
					datosv.put("USERKRONER", krou);	
				}else if(clave.equals("ROLEKRONER")){
					datosv.put("ROLEKRONER", kror);	
				}else if(clave.equals("CICSSERVER")){
					datosv.put("CICSSERVER", kroc);	
				}else if(clave.equals("GATEWAYURL")){
					datosv.put("GATEWAYURL", krog);	
				}else if(clave.equals("URLS")){
					datosv.put("URLS", htURL);
				} else if (clave.equals("PAISES")){
                    datosv.put("PAISES", paises); 
                } else if (clave.equals("RUBRO")){
                    datosv.put("RUBRO", rubro);
                }else if(clave.equals("SERVICRAPL")){
     				datosv.put("SERVICRAPL",htDatosRAPL);
      			}
                else if (clave.equals("IDENTIDAD")){
                	System.out.println("Datos Varios --Data-->"+data);
                	datosv.put("IDENTIDAD", data);
    			  }
                else if (clave.equals("IDENTIDAD1")){
             	datosv.put("IDENTIDAD1", data);
    			  }
                else if (clave.equals("IDENTIDAD2")){
                	datosv.put("IDENTIDAD2", data);
    			  }
                else if (clave.equals("IDENTIDAD3")){
                	datosv.put("IDENTIDAD3", data);
    			}else if (clave.equals("TXN_XMONTO")){ //Requerimiento 011 Se adiciona campo para validar TXN por Monto
                	datosv.put("TXN_XMONTO", data);
                }else if (clave.equals("TXNGRUP1")){ 
                	datosv.put("TxnGrup1", data);
    			 }
                else if (clave.equals("TXNGRUP2")){ 
                	datosv.put("TxnGrup2", data);
    			  }
                else if (clave.equals("TXNGRUP3")){ 
                	datosv.put("TxnGrup3", data);
    			  }
                else if (clave.equals("FEMAXVIGCH")){ 
                	datosv.put("feMaxVigCh", data);
    			  }
			}	
		}
	}
    
	public Hashtable getTablaDiario() {
		 if (TablaDiario == null){
			InicializaTabla();
		 }
		return TablaDiario;
	}	
	
	public void InicializaTabla() {
			TablaDiario = new Hashtable();
			TablaDiario.put("D_SUCURSAL","1");       
			TablaDiario.put("D_OPERADOR","2");       
			TablaDiario.put("D_CONNUREG","3");       
			TablaDiario.put("D_TXN","4");            
			TablaDiario.put("C_TXN_REVERSABLE","5"); 
			TablaDiario.put("C_TXN_VISIBLE","6");    
			TablaDiario.put("D_SUPERVISOR","7");     
			TablaDiario.put("D_FECHOPER","8");       
			TablaDiario.put("D_CUENTA","9");         
			TablaDiario.put("D_SERIAL","10");         
			TablaDiario.put("D_REFER","11");          
			TablaDiario.put("D_REFER1","12");         
			TablaDiario.put("D_REFER2","13");         
			TablaDiario.put("D_REFER3","14");         
			TablaDiario.put("D_EFECTIVO","15");       
			TablaDiario.put("D_MONTO","16");          
			TablaDiario.put("D_FECHEFEC","17");       
			TablaDiario.put("D_DIVISA","18");         
			TablaDiario.put("D_DESCRIP","19");        
			TablaDiario.put("D_DESCRIP_REVERSO","20");
			TablaDiario.put("D_ESTATUS","21");     
			TablaDiario.put("D_CONSECUTIVO","22");     
			TablaDiario.put("D_CONSECLIGA","23");     
			TablaDiario.put("D_CARGO","24");          
			TablaDiario.put("D_ABONO","25");          
			TablaDiario.put("D_MSGENVIO","26");
			TablaDiario.put("D_RESPUESTA","27");              
  }
}

