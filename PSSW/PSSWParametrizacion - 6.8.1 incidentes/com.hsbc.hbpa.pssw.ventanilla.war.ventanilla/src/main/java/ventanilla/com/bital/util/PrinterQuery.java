//*************************************************************************************************
//             Funcion: Clase que realiza la consulta para la impresion de certificacion y docuemntacion
//          Creado por: Juan Carlos Gaona
//      Modificado por: Fausto Rodrigo Flores Moreno
//*************************************************************************************************
// CCN - 4360291 - 18/03/2005 - Se unifican las tabas de TW_DIARIO Y TW_DIARIO_RESPUEST
// CCN - 4360296 - 23/03/2005 - Se cambia CCN por peticion de QA, CCN anterior 4360291
// CCN - 4360315 - 06/05/2005 - Se realiza relacion de ordenes de pago por sucursal
// CCN - 4360334 - 17/06/2005 - Se elimina variable dbQualifier
// CCN - 4360446 - 10/03/2006 - Proyecto TDI fase 2 cobro de comision
// CCN - 4360532 - 20/10/2006 - Se realizan modificaciones impresión de giros
// CCN - 4360574 - 09/03/2007 - Se ajusta orden de impresión para RAP cuando existe recargo o descuento (RAP Calculadora)
//*************************************************************************************************

package ventanilla.com.bital.util;

import java.util.Vector;
import ventanilla.com.bital.beans.CamposL;
import com.bital.util.ConnectionPoolingManager;

public class PrinterQuery implements java.io.Serializable {
    private Vector queryList = null;
    private Vector resultSet = null;
    private java.util.Hashtable requestParameters = null;
    private String tellerID = new String("");
    private String consecutiveID = new String("");
    private String transactionID = new String("");
    private String printParameters = new String("");
    private Vector processType = new Vector();
    private Vector camposID = new Vector();
    private String[][] txnandConsecutive = new String[20][2];
    private String casoEspecial = new String("");
    
    private Vector Banco = new Vector();
    private Vector Campol = new Vector();
    
    private int querysNumber = 0;
    String PostFixUR = ConnectionPoolingManager.getPostFixUR();
    
    public Vector Campos(String cadena, String delimitador) {
        NSTokenizer parser = new NSTokenizer(cadena, delimitador);
        Vector campos = new Vector();
        while(parser.hasMoreTokens()) {
            String token = parser.nextToken();
            if( token == null )
                continue;
            campos.addElement(token);
        }
        return campos;
    }
    
    private String stringFormat(int option, int sum) {
        java.util.Calendar now = java.util.Calendar.getInstance();
        String temp = new Integer(now.get(option) + sum).toString();
        if( temp.length() == 1 )
            temp = "0" + temp;
        return temp;
    }
    
    private void buildQuerys() {
        int processIndex = 0;
        String strDate = new String(stringFormat(java.util.Calendar.YEAR, 0) + stringFormat(java.util.Calendar.MONTH, 1) + stringFormat(java.util.Calendar.DAY_OF_MONTH, 0));
        if(printParameters.length() == 0 || printParameters.startsWith("~") || casoEspecial.length() != 0) {        	
			String tipoProceso = new String("");
			String tipoProceso1 = new String("");
			if(printParameters.length() > 0)
			{
				camposID = Campos(printParameters,"~");
				tipoProceso = (String)camposID.elementAt(0);
				tipoProceso1 = (String)camposID.elementAt(1);
			}			
            int i = 0;
            int j = 0;
            while( i < querysNumber) {
				if(tipoProceso.equals("LOCH") || tipoProceso1.equals("LOCH")||tipoProceso.equals("LCFISCAL") || tipoProceso1.equals("LCFISCAL")) {
					j = i;
					queryList.add("SELECT D_CUENTA, D_MONTO, D_TXN, D_RESPUESTA, D_REFER, D_REFER1, D_REFER2, D_REFER3, D_EFECTIVO, D_DIVISA, D_CONSECUTIVO, D_ESTATUS FROM TW_DIARIO_ELECTRON WHERE D_OPERADOR ='" + tellerID + "' AND D_FECHOPER='" + strDate + "' AND D_TXN ='" + txnandConsecutive[i][0] + "' AND D_CONSECUTIVO = '" + txnandConsecutive[i++][1] + "'" + PostFixUR);
					processType.add("MOVIMIENTOS_LOCH");					
				}
				else
				{
                	queryList.add("SELECT D_SUCURSAL, D_CUENTA, D_DIVISA, D_SERIAL, D_REFER, D_REFER1, D_REFER2, D_REFER3, D_DESCRIP, D_EFECTIVO, D_ESTATUS, D_MONTO, D_RESPUESTA FROM TW_DIARIO_ELECTRON WHERE D_OPERADOR ='" + tellerID + "' AND D_FECHOPER='" + strDate + "' AND D_TXN ='" + txnandConsecutive[i][0] + "' AND D_CONSECUTIVO = '" + txnandConsecutive[i++][1] + "'" + PostFixUR);
                	processType.add("CERTIFICACION");
				}
            }
        }
        if(printParameters.length() > 0) {
            if(printParameters.substring(0,1).equals("~"))
                printParameters = printParameters.substring(1,printParameters.length());
            String tipoProceso = new String("");
            camposID = Campos(printParameters,"~");
            tipoProceso = (String)camposID.elementAt(0);
            
            if(tipoProceso.equals("NOMINA")) {
                queryList.add("SELECT D_CUENTA, D_MONTO, D_STATUS, D_CONSEC FROM TW_NOMINA WHERE D_CAJERO = '" + tellerID + "' Order by D_CONSEC " + PostFixUR);
                processType.add("MOVIMIENTOS_NOMINA");
                queryList.add("SELECT D_CUENTA, D_CONSECUTIVO, D_MONTO, D_TXN, D_NO_LINEA, D_LINEA_RESPUESTA FROM TW_CERTIFICACIONES WHERE D_CAJERO = '"  + tellerID + "'" + PostFixUR);                
                processType.add("CERTIFICACION_NOMINA");
            }
            else if(tipoProceso.equals("RAP") || tipoProceso.equals("SAT")) {
                queryList.add("SELECT D_CUENTA, D_MONTO, D_TXN, D_CONSEC_RAP, D_NO_LINEA, D_LINEA_RESPUESTA, D_CONSECUTIVO, D_REFER5503, D_EFECTIVO, D_MONEDA FROM TW_CERTIFICACIONES WHERE D_CAJERO = '" + tellerID + "' AND ( D_TXN <> '5687' AND D_TXN <> '5689' AND D_TXN <> '5691') Order by D_CONSEC_RAP, D_TXN DESC, D_NO_LINEA " + PostFixUR );
                processType.add("MOVIMIENTOS_RAP");
                queryList.add("SELECT D_CUENTA, D_MONTO, D_TXN, D_CONSEC_RAP, D_NO_LINEA, D_LINEA_RESPUESTA, D_CONSECUTIVO, D_REFER5503, D_EFECTIVO, D_MONEDA FROM TW_CERTIFICACIONES WHERE D_CAJERO = '" + tellerID + "' AND ( D_TXN = '5687' OR D_TXN = '5689' OR D_TXN = '5691') Order by D_CONSEC_RAP, D_TXN DESC, D_NO_LINEA " + PostFixUR );
                processType.add("MOVIMIENTOS_RAP");
            }
            else if(tipoProceso.equals("NAFINSA")) {
                String strBillete = (String)camposID.elementAt(1);
                queryList.add("SELECT N_TIPO, C_BILLETE, D_DEPOSITANTE, C_CLAVE, D_NOM_PLAZA, D_MONTO, D_CONCEPTO, D_AUTORIDAD, C_NUM_SUCURSAL, D_NOM_SUCURSAL, C_MONEDA FROM TW_NAFINSA WHERE D_STATUS = 'E' AND C_BILLETE = '" + strBillete + "'"+ PostFixUR );                
                processType.add("COMPROBANTE_NAFINSA");
            }
            else if(tipoProceso.equals("GIROCITI3")) {
                String strGiro = (String)camposID.elementAt(2);
                queryList.add("SELECT C_MONEDA, D_BENEFICIARIO, D_MONTO, D_BANCO, D_PLAZA FROM TW_GIROS WHERE D_STATUS = 'E' AND C_GIRO='" + strGiro + "'"+ PostFixUR );
                processType.add("COMPROBANTE_GIRO");
            }
            else if(tipoProceso.equals("RELORDENPAGO")) {
                String strBanco = (String)camposID.elementAt(1);
                if (strBanco.equals("")) {
                    queryList.add("SELECT D_NUM_BANCO, N_MONTO_ORDEN FROM TW_ORD_PAG_RETRANS WHERE D_CAJERO LIKE '"+ tellerID.substring(0,4) +"%' AND D_FECHA = '" + strDate + "' Order by D_NUM_BANCO " + PostFixUR );
                    processType.add("MOVIMIENTOS_OP1");
                }
                else {
                    queryList.add("SELECT D_NUM_BANCO, D_FECHA, C_NUM_ORDEN, D_ORDENANTE, D_BENEFICIARIO, D_CIUDAD, D_INSTRUCCIONES, N_MONTO_ORDEN FROM TW_ORD_PAG_RETRANS WHERE D_NUM_BANCO = '" + strBanco + "' AND D_CAJERO LIKE'"+ tellerID.substring(0,4) +"%' AND D_FECHA = '" + strDate + "' Order by D_NUM_BANCO " + PostFixUR );
                    processType.add("MOVIMIENTOS_OP2");
                }
            }
            else if(tipoProceso.equals("VOUCHERS")) {
                queryList.add("SELECT D_CUENTA, D_MONTO, D_TXN, D_NO_LINEA, D_LINEA_RESPUESTA, D_CONSECUTIVO, D_MONEDA FROM TW_CERTIFICACIONES WHERE D_CAJERO = '" + tellerID + "'" + " Order by D_CONSEC_RAP, D_TXN DESC, D_NO_LINEA " + PostFixUR );                
                processType.add("MOVIMIENTOS_VOUCHERS");
            }
            else if(tipoProceso.equals("FICDEPCOB")) {
                consecutiveID = (String)camposID.elementAt(3);
                queryList.add("SELECT D_DIVISA, D_CUENTA, D_CONSECUTIVO, D_DESCRIP, D_MONTO, D_SUCURSAL, D_SERIAL, D_REFER3 FROM TW_DIARIO_ELECTRON WHERE D_CONSECLIGA = '" + consecutiveID + "' AND D_ESTATUS = 'A' AND D_FECHOPER='" + strDate + "' AND D_OPERADOR ='" + tellerID + "'" + PostFixUR); // modificado 15/12/2003                
                processType.add("DATOS_FICHA");
            }
            else if(tipoProceso.equals("REMESA")) {
                consecutiveID = (String)camposID.elementAt(3);
                queryList.add("SELECT D_DIVISA, D_CUENTA, D_CONSECUTIVO, D_DESCRIP, D_MONTO, D_SUCURSAL, D_SERIAL, D_REFER3 FROM TW_DIARIO_ELECTRON WHERE D_CONSECLIGA = '" + consecutiveID + "' AND D_ESTATUS ='A' AND D_FECHOPER='" + strDate + "' AND D_OPERADOR ='" + tellerID + "'"  + PostFixUR); // modificado 15/12/2003                
                processType.add("DATOS_REMESA");
            }
            else if(tipoProceso.equals("AFOREDEP")) {
                consecutiveID = (String)camposID.elementAt(1);
                queryList.add("SELECT D_CONSECUTIVO, D_CUENTA, D_MONTO, D_DESCRIP, D_SERIAL  FROM TW_DIARIO_ELECTRON WHERE D_ESTATUS ='A' AND D_TXN = '1009' AND D_CONSECLIGA = '" + consecutiveID + "' AND D_FECHOPER='" + strDate + "' AND D_OPERADOR ='" + tellerID + "' Order By D_CONSECUTIVO" + PostFixUR);   // modificado 15/12/2003
                processType.add("DATOS_AFORE");
            }
            else if(tipoProceso.equals("REMESACV")) {
                String strBanco = (String)camposID.elementAt(1);
                Banco.add(strBanco);
                Campol.add("lstBancoRemCV");
                queryList.add("VACIO");
                processType.add("DATOS_BANCO");
            }
        }
    }
    
    private void extractData() {
        if(requestParameters.get("tellerID") != null)
            tellerID = requestParameters.get("tellerID").toString();
        if(requestParameters.get("consecutiveID") != null)
            consecutiveID = requestParameters.get("consecutiveID").toString();
        if(requestParameters.get("transactionID") != null)
            transactionID = requestParameters.get("transactionID").toString();
        if(requestParameters.get("printParameters") != null)
            printParameters = requestParameters.get("printParameters").toString();
        if(requestParameters.get("casoEspecial") != null) {
            casoEspecial = requestParameters.get("casoEspecial").toString();
            java.util.StringTokenizer st = new java.util.StringTokenizer(casoEspecial,"~");
            while(st.hasMoreElements()) {
                txnandConsecutive[querysNumber][0] = st.nextElement().toString();
                txnandConsecutive[querysNumber++][1] = st.nextElement().toString();
                String strCeros = "0000000000";
                txnandConsecutive[querysNumber-1][1] = strCeros.substring(0,7-txnandConsecutive[querysNumber-1][1].length()) + txnandConsecutive[querysNumber-1][1];
            }
        }
        else {
            txnandConsecutive[querysNumber][0] = transactionID;
            if (consecutiveID.length() > 7)
                consecutiveID = consecutiveID.substring(0,7);
            txnandConsecutive[querysNumber++][1] = consecutiveID;
        }
        String strCeros = "0000000000";
        txnandConsecutive[querysNumber-1][1] = strCeros.substring(0,7-txnandConsecutive[querysNumber-1][1].length()) + txnandConsecutive[querysNumber-1][1];
    }
    
    public PrinterQuery( java.util.Hashtable requestParameters ) {
        resultSet = new Vector();
        queryList = new Vector();
        this.requestParameters = requestParameters;
        extractData();
        buildQuerys();
        makeQuery();
    }
    
    private void makeQuery() {
        java.sql.Statement newStatement = null;
        java.sql.ResultSet queryResultSet = null;
        querysNumber = 0;
        int nbancos = 0;
        for(int rowNumber = 0; rowNumber < queryList.size(); rowNumber++) {
            String rowsData = new String("");
            if(!queryList.get(rowNumber).toString().equals("VACIO")) {
                java.sql.Connection pooledConnection = com.bital.util.ConnectionPoolingManager.getPooledConnection( );
                try {
                    if(pooledConnection != null) {
                        newStatement = pooledConnection.createStatement();
                        queryResultSet = newStatement.executeQuery(queryList.get(rowNumber).toString());
                        int contador = 0;
                        while(queryResultSet.next()) {
                            contador++;
                            String columnsData = new String("");
                            for(int columnNumber = 1; columnNumber <= queryResultSet.getMetaData().getColumnCount(); columnNumber++) {
                                String columnData = queryResultSet.getString(columnNumber);
                                if(columnData == null)
                                    columnData = "";
                                
                                columnsData += columnData + "~";
                            }
                            String aditionalData =  new String("");
                            if(processType.get(rowNumber).toString().equals("CERTIFICACION")) {
                                aditionalData += txnandConsecutive[querysNumber][0] + "~" + txnandConsecutive[querysNumber++][1] + "~";
                                aditionalData += tellerID + "~";
                            }
                            
                            if(processType.get(rowNumber).toString().equals("DATOS_FICHA") || processType.get(rowNumber).toString().equals("DATOS_AFORE") ) {
                                camposID = Campos(columnsData,"~");
                                String strBanco = (String)camposID.elementAt(3);
                                if (strBanco != null)
                                    if (strBanco.length() > 17) {
                                        strBanco = strBanco.substring(15,18);
                                        Banco.add(strBanco);
                                        Campol.add("lstBanco");
                                        queryList.add("VACIO");
                                        processType.add("DATOS_BANCO");
                                    }
                            }
                            if(processType.get(rowNumber).toString().equals("DATOS_REMESA")) {
                                camposID = Campos(columnsData,"~");
                                String strBanco = (String)camposID.elementAt(7);
                                Banco.add(strBanco);
                                Campol.add("lstBancoRem");
                                queryList.add("VACIO");
                                processType.add("DATOS_BANCO");
                            }
                            if(processType.get(rowNumber).toString().equals("MOVIMIENTOS_OP1") || processType.get(rowNumber).toString().equals("MOVIMIENTOS_OP2")) {
                                camposID = Campos(columnsData,"~");
                                String strBanco = (String)camposID.elementAt(0);
                                Banco.add(strBanco);
                                Campol.add("lstBanco");
                                queryList.add("VACIO");
                                processType.add("DATOS_BANCO");
                            }
                            rowsData += columnsData + aditionalData + "|";
                        }
                        if(newStatement != null)
                            newStatement.close();
                        if(queryResultSet != null)
                            queryResultSet.close();
                    }
                }
                catch(java.sql.SQLException sqlException) {
                    rowsData += "ERRORCOMUNICACION@CONSULTA|";
                    System.out.println("PrinterQuery::makeQuery::Exception-" + sqlException+"-");
                }
                finally{
                    try{
                        if(newStatement != null){
                            newStatement.close();
                            newStatement = null;
                        }
                        if(queryResultSet != null){
                            queryResultSet.close();
                            queryResultSet = null;
                        }
                        if(pooledConnection != null){
                            pooledConnection.close();
                            pooledConnection = null;
                        }
                    }
                    catch(java.sql.SQLException sqlException){
                        System.out.println("Error FINALLY PrinterQuery::makeQuery [" + sqlException.toString() + "]");
                    }
                }
            }
            else {
                CamposL campos = CamposL.getInstance();
                rowsData += campos.getDValor((String)Campol.get(nbancos),((String)Banco.get(nbancos)).trim()) + "|";
                nbancos++;
            }
            if(rowsData.trim().length() > 0)
                resultSet.add(processType.get(rowNumber).toString() + "@" + rowsData);
        }
    }
    
    public java.util.Vector getData() {
        return resultSet;
    }
}
