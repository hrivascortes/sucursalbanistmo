//*************************************************************************************************
//             Funcion: BEAN para transacciones de RAP formato G
//            Elemento: RAPG.java
//          Creado por: Alejandro Gonzalez  
//      Modificado por: Juvenal R. Fernandez Varela
//*************************************************************************************************
// CCN - 4360237 - 05/11/2004 - Se agrega metodo getFormatoSAT para pagos 13 y 13A en Ceros
//*************************************************************************************************

package ventanilla.com.bital.sfb;

public class RAPG extends Header
{
  private String txtEffDate  = "";
  private String txtService  = "";
  private String txtToCurr   = "";
  private String txtCheckNo  = "";
  private String txtGStatus  = "";
  private String txtBcoTrnsf = "";
  private String txtTranAmt  = "";
  private String txtComAmt   = "";
  private String txtIvaAmt   = "";
  private String txtAcctNo   = "";
  private String txtAcctBen  = "";
  private String txtBenefic  = "";
  private String txtOrdenan  = "";
  private String txtInstruc  = "";
  private String txtFormatoSAT= "";
  private String txtDocs  = "";
  private String txtRAPservice  = "";


  public String getTxtRAPservice()
  {
	  return txtRAPservice + "*";
  }
  public void setTxtRAPservice(String newtxtRAPservice)
  {
	 txtRAPservice = newtxtRAPservice;
  }  
  public String getFormatoSAT()
  {
      return txtFormatoSAT + "*";
  }
  public void setTxtDocs(String newtxtDocs)
  {
	  txtDocs = newtxtDocs;
  }
  public String getTxtDocs()
  {
	return txtDocs + "*";
  }
  public void setFormatoSAT(String newFormatoSAT)
  {
      txtFormatoSAT = newFormatoSAT;
  }

  public String getEffDate()
  {
	return txtEffDate + "*";
  }
  public void setEffDate(String newEffDate)
  {
	txtEffDate = newEffDate;
  }

  public String getService()
  {
	return txtService + "*";
  }
  public void setService(String newService)
  {
	txtService = newService;
  }

  public String getToCurr()
  {
	return txtToCurr + "*";
  }
  public void setToCurr(String newToCurr)
  {
	txtToCurr = newToCurr;
  }

  public String getCheckNo()
  {
	return txtCheckNo + "*";
  }
  public void setCheckNo(String newCheckNo)
  {
	txtCheckNo = newCheckNo;
  }

  public String getGStatus()
  {
	return txtGStatus + "*";
  }
  public void setGStatus(String newGStatus)
  {
	txtGStatus = newGStatus;
  }

  public String getBcoTrnsf()
  {
	return txtBcoTrnsf + "*";
  }
  public void setBcoTrnsf(String newBcoTrnsf)
  {
	txtBcoTrnsf = newBcoTrnsf;
  }

  public String getTranAmt()
  {
	return txtTranAmt + "*";
  }
  public void setTranAmt(String newTranAmt)
  {
	txtTranAmt = newTranAmt;
  }

  public String getComAmt()
  {
	return txtComAmt + "*";
  }
  public void setComAmt(String newComAmt)
  {
	txtComAmt = newComAmt;
  }

  public String getIvaAmt()
  {
	return txtIvaAmt + "*";
  }
  public void setIvaAmt(String newIvaAmt)
  {
	txtIvaAmt = newIvaAmt;
  }

  public String getAcctNo()
  {
	return txtAcctNo + "*";
  }
  public void setAcctNo(String newAcctNo)
  {
	txtAcctNo = newAcctNo;
  }

  public String getAcctBen()
  {
	return txtAcctBen + "*";
  }
  public void setAcctBen(String newAcctBen)
  {
	txtAcctBen = newAcctBen;
  }

  public String getBenefic()
  {
	return txtBenefic + "*";
  }
  public void setBenefic(String newBenefic)
  {
	txtBenefic = newBenefic;
  }

  public String getOrdenan()
  {
	return txtOrdenan + "*";
  }
  public void setOrdenan(String newOrdenan)
  {
	txtOrdenan = newOrdenan;
  }

  public String getInstruc()
  {
	return txtInstruc + "*";
  }
  public void setInstruc(String newInstruc)
  {
	txtInstruc = newInstruc;
  }

  public String toString()
  {
	return super.toString()
	+ getEffDate()	+ getService()	+ getToCurr()	+ getCheckNo()	+ getGStatus()
	+ getBcoTrnsf()	+ getTranAmt()	+ getComAmt()	+ getIvaAmt()	+ getAcctNo()
	+ getAcctBen()	+ getBenefic()	+ getOrdenan()	+ getInstruc();
  }
}
