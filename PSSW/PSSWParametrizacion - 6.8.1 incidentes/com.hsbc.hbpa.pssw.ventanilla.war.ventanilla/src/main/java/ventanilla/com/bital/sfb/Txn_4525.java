// Transaccion 4011 para 0061 - Cheques de Caja
package ventanilla.com.bital.sfb;

import com.bital.util.ParameterParser;
import com.bital.util.ParameterNotFoundException;
import ventanilla.GenericClasses;

public class Txn_4525 extends Transaction
{
  public Header doAction(ParameterParser param)
    throws ParameterNotFoundException
  {
    

	GenericClasses gc = new GenericClasses();
	FormatoA formatoa = new FormatoA();
	formatoa.setTxnCode("4525");
	
	formatoa.setAcctNo(param.getString("txtDDACuenta"));
	//formatoa.setTranAmt(param.getCString("txtMonto"));

	
	String moneda = gc.getDivisa(param.getString("moneda"));
	
	if(moneda.equals("N$"))
		formatoa.setTranAmt(param.getCString("txtMonto"));
	else
		formatoa.setTranAmt(param.getCString("txtMonto1"));
	
	String moneda_s =  gc.getDivisa(param.getString("moneda2"));
	formatoa.setTranCur(moneda);
	String ceros="";
	if(!moneda.equals("N$"))
		//formatoa.setTranDesc(param.getString("lstBanCorresp1")+gc.StringFiller( ceros,12-param.getString("lstBanCorresp1").length(),false," ")+"USD000000000       00");
		formatoa.setTranDesc(param.getString("lstBanCorresp1")+gc.StringFiller(ceros,12-param.getString("lstBanCorresp1").length(),false," ") + "USD" + param.getString("txtRefTIRE"));
	else
		//formatoa.setTranDesc(param.getString("lstBanCorresp1")+gc.StringFiller( ceros,12-param.getString("lstBanCorresp1").length(),false," ")+moneda_s+"000000000       00");
		formatoa.setTranDesc(param.getString("lstBanCorresp1")+gc.StringFiller(ceros,12-param.getString("lstBanCorresp1").length(),false," ") + moneda_s + param.getString("txtRefTIRE"));
	//formatoa.setTranDesc("MRMDUS33    USD227405173       11");
	formatoa.setCheckNo2("0000000000");
	formatoa.setCheckNo3("0000000000");
	formatoa.setCheckNo5("000000");
	formatoa.setCashIn("000");
	formatoa.setCheckNo4(param.getString("registro").substring(1,7));
	formatoa.setMoAmoun(param.getCString("txtTotalUSD"));
	if( param.getString("fecha4525", null) != null )
	{
	  String fecha1 = param.getString("fecha4525");
	  fecha1 = fecha1.substring(2, 8);
	  formatoa.setAcctNo5("1"+fecha1);
	}
	//formatoa.setAcctNo5(param.getString("fecha4525"));
	//formatoa.setMoAmoun(param.getCString("txtMonto1"));
	
	//formatoa.setDraftAm(param.getString("txtPlazaSuc") + param.getString("txtPlazaSuc"));
	String txtTipoCambio = param.getCString("txtTipoCambio");
	String txtPlaza = param.getCString("txtPlazaSuc");
	if(txtTipoCambio.length() < 6)
		txtTipoCambio=gc.StringFiller(txtTipoCambio,6,false,"0");
	txtTipoCambio = "0000" + txtTipoCambio;
	txtTipoCambio = txtTipoCambio.substring(txtTipoCambio.length()-8);
	formatoa.setDraftAm("0"+txtPlaza + txtTipoCambio);
	
	//ODCT 4525A 09004900403  900403  000**6000000000**544000*N$***000**00100108800*50000**MRMDUS33    USD227405173       11**************0000006528*******0000000979*******446096******1070820*022344***
	//ODPA 4525A 05030503007  503007  000**0100000331**1000000*N$***000**00100018935*528122**MRMDUS33    USD000000000       01**************0000000000*******0000000000*******000234******1080423*000000***
    
    //System.out.println(formatoa);
    return formatoa;
  }
}