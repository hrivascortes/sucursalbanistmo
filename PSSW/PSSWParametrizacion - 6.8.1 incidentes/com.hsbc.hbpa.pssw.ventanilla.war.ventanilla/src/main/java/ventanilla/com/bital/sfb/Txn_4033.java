//*************************************************************************************************
//             Funcion: Bean Txn 4033
//      Elaborado por: Jesus Emmanuel Lopez Rosales
//*************************************************************************************************
// CCN - 4360576 - 09/03/2007 - Modificaciones para proyecto OPMN
// CCN - 4360584 - 23/03/2007 - Modificaciones para proyecto OPMN
//*************************************************************************************************
package ventanilla.com.bital.sfb;

import ventanilla.GenericClasses;
import com.bital.util.ParameterNotFoundException;
import com.bital.util.ParameterParser;


public class Txn_4033 extends Transaction
{		 
	public Header doAction(ParameterParser param)
		throws ParameterNotFoundException
	{				
        GenericClasses gc = new GenericClasses();                				
		FormatoG formatog = new FormatoG();
		 
	   formatog.setTxnCode("4033");
       formatog.setFormat("G");	
       formatog.setToCurr("US$");
       formatog.setTranAmt(param.getCString("txtMonto1"));	
       String TC = param.getCString("txtTipoCambio");
       TC = "00" + TC.substring(0,4);
       formatog.setComAmt(TC);
       formatog.setAcctNo(param.getString("txtDDACuenta"));       
	   //formatog.setCtaBenef(param.getString("txtNumOrden"));
	   formatog.setBenefic(param.getString("txtNomBen") + " " + param.getString("txtApeBen"));       
       //formatog.setGStatus(param.getString("txtStatusOrden"));       
	   
	   String strciudad =param.getString("ciudad");
	   if(strciudad.length() > 20){
	      strciudad = strciudad.substring(0,20);
	   }else{
	      strciudad =gc.StringFiller(param.getString("ciudad"),20,true," ");
	   }	
	   String strip =gc.StringFiller(param.getString("dirip"),16,true," ");
	   formatog.setInstruc(strciudad + strip);
   	   formatog.setOrdenan(gc.StringFiller(param.getString("lstTipoDocto"),4,true," ")+gc.StringFiller(param.getString("txtFolio"),17,true," "));
		return formatog;
	}
	

}
