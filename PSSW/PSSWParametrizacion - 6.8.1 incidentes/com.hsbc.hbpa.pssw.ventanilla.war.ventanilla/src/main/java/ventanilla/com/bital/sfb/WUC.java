//************************************************************************************************* 
//             Funcion: Bean para WU Consulta
//            Elemento: WUC.java
//          Creado por: Alejandro Gonzalez Castro
//      Modificado por: Rutilo Zarco Reyes
//*************************************************************************************************
// CCN - 4360189 - 03/09/2004 - Se habilita WU
//*************************************************************************************************

package ventanilla.com.bital.sfb;

public class WUC extends Header3 
{
    private String txtMTCN          = "";
    private String txtNomBenS       = "";
    private String txtApePBenS      = "";
    private String txtNomOrdS       = "";
    private String txtApePOrdS      = "";
    private String txtNomBen        = "";
    private String txtApePBen       = "";
    private String txtApeMBen       = "";
    private String txtCalle         = "";
    private String txtCiudad        = "";
    private String txtEstado        = "";
    private String txtCP            = "";
    private String txtTelefono      = "";
   
    public String getMTCN() 
    {
        return Filler(txtMTCN,10, "0", "L");
    }
    public void setMTCN(String newMTCN) 
    {
        txtMTCN = newMTCN;
    }

    public String getNomBenS() 
    {
        return Filler(txtNomBenS,40, " ", "R");
    }
    public void setNomBenS(String newNomBenS) 
    {
        txtNomBenS = newNomBenS;
    }

    public String getApePBenS() 
    {
        return Filler(txtApePBenS,40, " ", "R");
    }
    public void setApePBenS(String newApePBenS) 
    {
        txtApePBenS = newApePBenS;
    }

    public String getNomOrdS() 
    {
        return Filler(txtNomOrdS,40, " ", "R");
    }
    public void setNomOrdS(String newNomOrdS) 
    {
        txtNomOrdS = newNomOrdS;
    }

    public String getApePOrdS() 
    {
        return Filler(txtApePOrdS,40, " ", "R");
    }
    public void setApePOrdS(String newApePOrdS) 
    {
        txtApePOrdS = newApePOrdS;
    }

    public String getNomBen() 
    {
        return Filler(txtNomBen,40, " ", "R");
    }
    public void setNomBen(String newNomBen) 
    {
        txtNomBen = newNomBen;
    }

    public String getApePBen() 
    {
        return Filler(txtApePBen,40, " ", "R");
    }
    public void setApePBen(String newApePBen) 
    {
        txtApePBen = newApePBen;
    }

    public String getApeMBen() 
    {
        return Filler(txtApeMBen,40, " ", "R");
    }
    public void setApeMBen(String newApeMBen) 
    {
        txtApeMBen = newApeMBen;
    }

    public String getCalle() 
    {
        return Filler(txtCalle,80, " ", "R");
    }
    public void setCalle(String newCalle) 
    {
        txtCalle = newCalle;
    }

    public String getCiudad() 
    {
        return Filler(txtCiudad, 24, " ", "R");
    }
    public void setCiudad(String newCiudad) 
    {
        txtCiudad = newCiudad;
    }

    public String getEstado() 
    {
        return Filler(txtEstado,12, " ", "R");
    }
    public void setEstado(String newEstado) 
    {
        txtEstado = newEstado;
    }

    public String getCP() 
    {
        return Filler(txtCP, 5, "0", "R");
    }
    public void setCP(String newCP) 
    {
        txtCP = newCP;
    }

    public String getTelefono() 
    {
        return Filler(txtTelefono, 10, "0", "R");
    }
    public void setTelefono(String newTelefono) 
    {
        txtTelefono = newTelefono;
    }

    public String Filler(String cadena, int largo, String filler, String side)
    {
        int cadlong = cadena.length();
        for(int i=cadlong; i<largo; i++)
        {
            if(side.equals("R"))
                cadena = cadena + filler;
            else
                cadena = filler + cadena;
        }
        return cadena;
    }
    
    public String toString() 
    {
        return super.toString() + 
            getMTCN()   +   getNomBenS()    +   getApePBenS()   +   getNomOrdS()    +   getApePOrdS()   +
            getNomBen() +   getApePBen()    +   getApeMBen()    +   getCalle()  +   getCiudad() +
            getEstado() +   getCP() +   getTelefono();
    }
}

