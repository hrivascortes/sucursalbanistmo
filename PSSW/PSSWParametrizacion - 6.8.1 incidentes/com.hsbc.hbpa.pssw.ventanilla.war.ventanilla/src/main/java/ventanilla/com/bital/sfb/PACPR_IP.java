package ventanilla.com.bital.sfb;

import ventanilla.com.bital.util.Segment;
import ventanilla.com.bital.util.PASSResponse;

public class PACPR_IP extends Segment{

	
	/*
	private String apertura      = "";
    private String promotor1     = "";
    private String promotor2     = "";
    private String sucursal      = "";
    private String estatus        = "";
    private String fechaMantto   = "";
    private String restriccion   = "";
    private String saldo         = "";
    private String moneda        = "";
    private String[] titular     = new String[5];
    private String relacion      = "";
    private String cis           = "";
    private String rfc           = "";
    private String curp          = "";
    private String liga          = "";
    private String[] calle       = new String[2];
    private String colonia       = "";
    private String ciudad        = "";
    private String estado        = "";
    private String cp            = "";
    private String pais          = "";
    private String saldo2        = "";
	private String sector		 = "";
	*/
	private String companyNumber = "";
	private String productID     = "";
	private String accountNumber = "";
	private String[] Items = null;
	private String subproducto   = "";
	private String numeroCYS     = "";
	private String RUC   = "";

// SETTERS
	public void setSubproducto (String newSubproducto){
		subproducto = newSubproducto;
	}
	public void setNumeroCYS (String numeroCYS){
		subproducto = numeroCYS;
	}
	public void setRUC (String RUC){
		subproducto = RUC;
	}
	public void setCompanyNumber(String CompanyNumber){
		companyNumber = CompanyNumber;
	}
	public void setProductID(String ProductID){
		productID = ProductID;
	}
	public void setAccountNumber(String AccountNumber){
		accountNumber = AccountNumber;
	}
	/*
	public void setCis(String Cis){
		cis = Cis;
	}
	public void setSubproducto (String newSubproducto){
		subproducto = newSubproducto;
	}
	public void setApertura(String newApertura){
		apertura= newApertura;
	}
	public void setPromotor1(String newPromotor1){
		promotor1= newPromotor1;
	}
	public void setPromotor2(String newPromotor2){
		promotor2= newPromotor2;
	}
	public void setSucursal(String newSucursal){
		sucursal= newSucursal;
	}
	public void setEstatus (String newStatus){
		estatus = newStatus;
	}
	public void setFechaMantto(String newFechaMantto){
		fechaMantto = newFechaMantto;
	}
	public void setRestriccion(String newRestriccion){
		restriccion = newRestriccion;
	}
	public void setSaldo(String newSaldo){
		saldo= newSaldo;
	}
	public void setMoneda(String newMoneda){
		moneda= newMoneda;
	}
	public void setTitular (int pos, String newTitular){
		titular[pos]= newTitular;
	}
	public void setRelacion(String newRelacion){
		relacion= newRelacion;
	}
	public void setRfc (String newRfc){
		rfc = newRfc;
	}
	public void setCurp(String newCurp){
		curp= newCurp;
	}
	public void setLiga(String newLiga){
		liga= newLiga;
	}
	public void setCalle(int pos, String newCalle){
		calle[pos]= newCalle;
	}
	public void setColonia(String newColonia){
		colonia = newColonia;
	}
	public void setCiudad(String newCiudad){
		ciudad= newCiudad;
	}
	public void setEstado(String newEstado){
		estado= newEstado;
	}
	public void setCp(String newCp){
		cp= newCp;
	}
	public void setPais(String newPais){
		pais= newPais;
	}
	public void setSaldo2(String newSaldo2){
		saldo2= newSaldo2;
	}
	public void setSector(String newSector){
		sector= newSector;
	}*/
// GETTERS	
	public String getSubproducto(){
		return subproducto;
	}
	public String getNumeroCYS(){
		return numeroCYS;
	}
	public String getRUC(){
		return RUC;
	}
	public String getCompanyNumber(){
		return companyNumber;
	}
	public String getProductID(){
		return productID;
	}
	public String getAccountNumber(){
		return accountNumber;
	}
	/*
	public String getCis(){
		return cis;
	}
	public String getSubproducto (){
		return subproducto;
	}
	public String getApertura(){
		return apertura;
	}
	public String getPromotor1(){
		return promotor1;
	}
	public String getPromotor2(){
		return promotor2;
	}
	public String getSucursal(){
		return sucursal;
	}
	public String getEstatus(){
		return estatus;
	}
	public String getFechaMantto(){
		return fechaMantto;
	}
	public String getRestriccion(){
		return restriccion;
	}
	public String getSaldo(){
		return saldo;
	}
	public String getMoneda(){
		return moneda;
	}
	public String getTitular(int pos){
		return titular[pos];
	}
	public String getRelacion(){
		return relacion;
	}
	public String getRfc(){
		return rfc;
	}
	public String getCurp(){
		return curp;
	}
	public String getLiga(){
		return liga;
	}
	public String getCalle(int pos){
		return calle[pos];
	}
	public String getColonia(){
		return colonia;
	}
	public String getCiudad(){
		return ciudad;
	}
	public String getEstado(){
		return estado;
	}
	public String getCp(){
		return cp;
	}
	public String getPais(){
		return pais;
	}
	public String getSaldo2(){
		return saldo2;
	}
	public String getSector(){
		return sector;
	}*/
// ----

	public String getMessage(){
	    StringBuffer msg = new StringBuffer(128);
	    msg.append("PACPR_IP~00~")
	       .append(companyNumber).append("~")
	       .append(productID).append("~")
	       //.append("DDA").append("~")
	       .append(accountNumber).append("~")
	       .append("INQ~");

    
	    return msg.toString();
	}
	
	public void parse(PASSResponse response){
		
		Items = response.parseItems(47);
		if( Items.length != 47 )
			return;
		/*if( !Items[0].equals("PACPR_IP") )
		    return;*/
		if( !Items[0].equals("WACPR_IS") )
	    return;
		if( !Items[1].equals("00") ){
		    setStatus(1);
		    setCode  (Items[3]);
		    setDescription(Items[4]);
		    return;
		}

		setStatus(0);

		subproducto = Items[8];
		numeroCYS   = Items[43];
		RUC         = Items[45];
		/*apertura    = Items[9];
		promotor1   = Items[10];
		promotor2   = Items[11];
		sucursal    = Items[13];
		estatus     = Items[14];
		fechaMantto = Items[16];
		restriccion = Items[19];
		saldo       = Items[21];
		moneda      = Items[22];
		titular[0]  = Items[23];
		titular[1]  = Items[24];
		titular[2]  = Items[25];
		titular[3]  = Items[26];
		titular[4]  = Items[27];
		relacion    = Items[42];
		cis         = Items[43];
		rfc         = Items[44];
		curp        = Items[45];
		liga        = Items[29];
		calle[0]    = Items[30];
		calle[1]    = Items[31];
		colonia     = Items[32];
		ciudad      = Items[33];
		estado      = Items[34];
		cp          = Items[35];
		pais        = Items[36];
		sector		= Items[38];
		saldo2      = Items[46];*/
	} 
}
