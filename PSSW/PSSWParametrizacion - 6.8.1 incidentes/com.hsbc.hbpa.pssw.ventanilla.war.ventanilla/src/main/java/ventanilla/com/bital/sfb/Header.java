//************************************************************************************************* 
//             Funcion: Header para WU
//            Elemento: Header3.java
//          Creado por: Alejandro Gonzalez Castro
//      Modificado por: Juvenal Fernandez
//*************************************************************************************************
// CCN - 4360334 - 17/06/2005 - Se elimina import de protomatter
//*************************************************************************************************

package ventanilla.com.bital.sfb;
 
import com.bital.util.Gateway;
import com.bital.util.GatewayFactory;
import com.bital.util.GatewayException;
//import com.protomatter.syslog.Syslog;

public class Header implements Command
{
  private String txtMessage = null;
  private String txtReverso = null;
  private int    status     = -1;

  //  Atributos  
  //-----------------------------------
  private String txtTxnId      = "ODPA";
  private String txtTxnCode    = null;
  private String txtFormat     = "A";
  private String txtBranch     = null;
  private String txtTeller     = null;
  private String txtSupervisor = null;
  private String txtBackOut    = null;
  private String txtOverride   = null;
  private String txtCurrentDay = null;
  private String ambiente = null;
  javax.naming.Context initialContext = null;

  public Header()
  {
    super();
  }

  //  Metodo para comunicacion
  //-----------------------------------
  public void execute()
  {
    //Gateway cg = GatewayFactory.getGateway();
    Gateway cg = GatewayFactory.getGateway(getTxnCode());

    String msg = null;
    if( txtReverso != null )
      msg = txtReverso;
    else
      msg = this.toString();
      
	try
	{
		initialContext = new javax.naming.InitialContext();
		ambiente = (String)initialContext.lookup("java:comp/env/AMB");
	}
	catch(javax.naming.NamingException e)
	{
		System.out.println("Error Header::Lookup::java:comp/env/AMB");
		System.out.println(e);
	}
	
	if(!ambiente.toLowerCase().equals("p"))    
    	System.out.println("MSG IN: "+msg);
 //   Syslog.debug(this, msg);
    
    try
    {
      cg.write(msg);
      String result = cg.readLine();
      
	  if(!ambiente.toLowerCase().equals("p"))
	  	System.out.println("MSG OUT: "+result+"/////////");
      
      txtMessage = result.substring(0, 1) + "~" + result.substring(2, 4) + "~"
        + result.substring(4, 11) + "~" + result.substring(12, result.length()) + "~";
      
      status = Integer.parseInt(result.substring(0, 1));
    }
    catch( GatewayException e )
    {
      txtMessage = "1~01~0000000~ERROR DE ENLACE A LA APLICACION CICS~";
    }
    catch( NumberFormatException e )
    {
      status = -1;
    }

//    System.out.println(txtMessage);
//    Syslog.debug(this, txtMessage);
  }

  public String getMessage()
  {
    return txtMessage;
  }
  
  public int getStatus()
  {
    return status;
  }

  //  Getters
  //-----------------------------------
  public String getTxnId()
  {
    return txtTxnId;
  }

  public String getBackOut()
  {
    if( txtBackOut == null )
      return "0";
    else
      return txtBackOut;
  }      

  public String getBranch()
  {    
    return txtBranch;
  }      

  public String getCurrentDay()
  {
    if( txtCurrentDay == null )
      return "0";
    else
      return txtCurrentDay;
  }      

  public String getFormat()
  {
    return txtFormat;
  }  

  public String getOverride()
  {
    if( txtOverride == null )
      return "0";
    else
      return txtOverride;
  }  

  public String getSupervisor()
  {
    if( txtSupervisor == null )
      return txtTeller;//Common.number(txtTeller, 6);
    else
      return txtSupervisor;//Common.number(txtSupervisor, 6);
  }  

  public String getTeller()
  {
    return txtTeller;//Common.number(txtTeller, 6);
  }  

  public String getTxnCode()
  {
    return txtTxnCode;//Common.number(txtTxnCode, 4);
  }  

  //  Setters
  //-----------------------------------
  public void setTxnId(String newTxnId)
  {
    txtTxnId = newTxnId;
  }

  public void setBackOut(String newBackOut)
  {
    txtBackOut = newBackOut;
  }  

  public void setBranch(String newBranch)
  {
    txtBranch = newBranch;
  }  

  public void setCurrentDay(String newCurrentDay)
  {
    txtCurrentDay = newCurrentDay;
  }

  public void setFormat(String newFormat)
  {
    txtFormat = newFormat;
  }

  public void setOverride(String newOverride)
  {
    txtOverride = newOverride;
  }  

  public void setSupervisor(String newSupervisor)
  {
    txtSupervisor = newSupervisor;
  }  

  public void setTeller(String newTeller)
  {
    txtTeller = newTeller;
  }  

  public void setTxnCode(String newTxnCode)
  {
    txtTxnCode = newTxnCode;
  }
 
  public void setMessage(String newMessage)
  {
    txtMessage = newMessage;
  }
   
  //  Metodos para diario
  //-----------------------------------
  public void setReverso(String newReverso)
  {
    txtReverso = newReverso;
  }
  
  public String getReverso()
  {
    return txtReverso;
  }

  //  Funcion de conversion / mensaje
  //-----------------------------------
  public String toString()
  {
    return getTxnId() + " " + getTxnCode() + getFormat() + " "
          + getBranch() + getTeller() + "  " + getSupervisor() + "  "
          + getBackOut() + getOverride() + getCurrentDay() + "*";
  }      
  
  public String getTranCur()
  { return "*"; }
  
  public String getCashIn()
  { return "*"; }
  
  public String getCashOut()
  { return "*"; }
  
  public String getTranAmt()
  { return "*"; }
  
  public String getFromCurr()
  { return "*"; }
  
}
