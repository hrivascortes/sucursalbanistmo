//*************************************************************************************************
//             Funcion: Clase que ejecuta el SQL solicitado
//          Creado por: Fausto Rodrigo Flores Moreno
//*************************************************************************************************
// CCN - 4360845 - 13/02/2009 - FFM - Optimización de Query para TDC
// CCn - 4360845 - 13/02/2009 - FFM - 18889 - Se elimina definición de variable stmt
// CCN - 4360858 - 16/04/2009 - FPM FFM - Optimización de PSSW (quitar toUpperCase, uso de preparestatement, quitar synchronized)
// CCN - 4360890 - 17/07/2009 - FFM - Se hace mejora en DBQueryUM
//*************************************************************************************************

package ventanilla.com.bital.util;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;

import com.bital.util.ConnectionPoolingManager;

public class DBQueryUM 
{
    private int numRows = 0;
    private ArrayList Rows = null;
	private Connection conn = ConnectionPoolingManager.getPooledConnection();
	private PreparedStatement stmt = null;
	private String strRespuesta = null;

	public DBQueryUM(int Type, String statementQ, String Columnas, String table, String Datos, String clause, ArrayList Ref)
    {
        StringBuffer sqltmp = new StringBuffer(1024);

        try 
        {
            if(conn != null) 
            {
                
                sqltmp.append(statementQ);
                switch (Type) {
                	case 1:		sqltmp.append(table);
								sqltmp.append(Columnas);
				                sqltmp.append(Datos);
				                break;
				    case 2:		sqltmp.append(table);
				                sqltmp.append(Datos);
				                sqltmp.append(clause);
				                break;
				    case 3:		sqltmp.append(table);
				                sqltmp.append(clause);
				                break;
                }
                stmt = conn.prepareStatement(sqltmp.toString());
                if(Ref != null){ 	setReferencias(Ref);       }
                conn.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);
                numRows = stmt.executeUpdate();
            }
        }
        catch(SQLException sqlException)
        {
            System.out.println("Error DBQueryUM::executeQuery [" + sqlException + "]");
            System.out.println("Error DBQueryUM::SQL[" + getSQL(Ref,sqltmp.toString()) + "]");
        }
        finally {
            try {
                if(stmt != null) {
                    stmt.close();
                    stmt = null;
                }
                if(conn != null) {
                    conn.close();
                    conn = null;
                }
            }
            catch(java.sql.SQLException sqlException)
            {System.out.println("Error FINALLY DBQueryUM::executeQuery [" + sqlException + "]");}
        }
    }
    
    public DBQueryUM(int type,String sql, String table, String clause, ArrayList Ref, String token)
    {
        ResultSet rs = null;
        StringBuffer sqltmp = new StringBuffer(1024);
        try 
        {
            if(conn != null) 
            {
                
                sqltmp.append(sql);
                sqltmp.append(table);
                sqltmp.append(clause);
                stmt = conn.prepareStatement(sqltmp.toString());
                if(Ref != null){setReferencias(Ref);}
                	

                conn.setTransactionIsolation(Connection.TRANSACTION_READ_UNCOMMITTED);
                rs = stmt.executeQuery();
                ResultSetMetaData rsmd = null;
                rsmd = rs.getMetaData();
                int cols = rsmd.getColumnCount();

                switch(type){
	                case 1:	Rows = new ArrayList();
	                		while(rs.next()){
	                    		HashMap datos = new HashMap();
	                    		for(int i = 1; i <= cols; i++)
	                    			datos.put(rsmd.getColumnName(i), rs.getString(i).trim());
	                    		Rows.add(datos);
	                		}
	                		numRows = Rows.size();
	                		break;
	                case 2:	
	                		Rows = new ArrayList();
			                while(rs.next()){
			                    ArrayList tmp = new ArrayList(cols);
			                    for(int i = 1; i <= cols; i++)
			                        tmp.add(rs.getString(i));
			                    Rows.add(tmp);
			                }
			                numRows = Rows.size();
	                		break;
	                case 3: 
	                		if (strRespuesta == null){strRespuesta = "";}
		                		
		                    while(rs.next())
		                    {
		                    	for(int i = 1; i <= cols; i++){
		                            String strResp = rs.getString(i);
		                            if(strResp == null){strResp = "";}
		                            	
		                        	strRespuesta += strResp.trim() + token;
		                        }
		                    }
	                		break;
                }

            }
        }
        catch(SQLException sqlException)
        {
            System.out.println("Error DBQueryUM::SELECT [" + sqlException + "]");
            System.out.println("Error DBQueryUM::SQL[" + getSQL(Ref,sqltmp.toString()) + "]");
        }
        finally {
            try {
                if(rs != null) {
                    rs.close();
                    rs = null;
                }
                if(stmt != null) {
                    stmt.close();
                    stmt = null;
                }
                if(conn != null) {
                    conn.close();
                    conn = null;
                }
            }
            catch(java.sql.SQLException sqlException)
            {System.out.println("Error FINALLY DBQueryUM::executeQuery [" + sqlException + "]");}
        }
    }
    
    public int getRows(){
        return numRows;
    }
    
    public ArrayList getDatos(){//Arraylist
    	return Rows;
    }
    
    public String getStrDatos(){
    	return strRespuesta;
    }
    
    public HashMap getHDatos(int row){
    	return (HashMap)Rows.get(row);
    }
    
    private void setReferencias(ArrayList valores){
		try{
			SQLRefer dato = null;
			for(int i = 0; i<valores.size(); i++){
				dato = (SQLRefer) valores.get(i);
				if(dato.getType() == SQLRefer.STRING){stmt.setString(i+1,dato.getString());}
					
				if(dato.getType() == SQLRefer.LONG){stmt.setLong(i+1, dato.getLong().longValue());}
					
				if(dato.getType() == SQLRefer.INT){stmt.setInt(i+1, dato.getInt().intValue());}
					
			}
    	}catch(SQLException sqlException){
			System.out.println("Error DBQueryUM::setReferencias [" + sqlException + "]");
		}
    }
    
    private String getSQL(ArrayList valores, String SQL){
    	SQLRefer dato = null;
    	StringBuffer fSQL = new StringBuffer (SQL);
    	int indice = 0;
    	if(valores != null){
	    	for(int i = 0; i < valores.size(); i++){
	    		dato = (SQLRefer) valores.get(i);
	    		indice = fSQL.indexOf("?");
	    		fSQL.delete(indice, indice+1);
				if(dato.getType() == SQLRefer.STRING){
					String tmp = "'" + dato.getString() + "'";
					fSQL.insert(indice, tmp);
				}if(dato.getType() == SQLRefer.LONG){
					fSQL.insert(indice, dato.getLong());
				}if(dato.getType() == SQLRefer.INT){
					fSQL.insert(indice, dato.getInt());
				}
	    	}
    	}
    	return fSQL.toString();
    }
}
