package ventanilla.com.bital.admin;

import java.io.StringReader;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;

/**
 * @author YGX
 * Lee una cadena XML y la imprime, si fuera de un archivo lo genera..
 */
public class PerfilCuenta {
	    	
	private String cuenta= "";
	private String sucursal = "";
	private String nombre1="";
	private String nombre2="";
	private String nombre3="";
	private String nombre4="";
	private String codigo = "";
	private boolean cta_found = false;
	private String descrip = "";
	private Object TxnBean = null;
	
	
	public String getCuenta() {
		return cuenta;	}
	 
	public String getSucursal() {
		return sucursal;	}
	 		
	public String getNombre1() {
		return nombre1;	}
    	
	public String getNombre2() {
		return nombre2;	}
    	
	public String getNombre3() {
		return nombre3;	}
    	
	public String getNombre4() {
		return nombre4;	}
	
	/* Se agregan los sets*/
	public void setCuenta(String newCuenta)   {
	cuenta = newCuenta;   }
   
    public void setNombre1(String newNombre1)   {
	nombre1 = newNombre1;   }
   
    public void setNombre2(String newNombre2)   {
	nombre2 = newNombre2;   }
   
    public void setNombre3(String newNombre3)   {
	nombre3 = newNombre3;   }
   
    public void setNombre4(String newNombre4)   {
	nombre4 = newNombre4;   }
   
    public void setSucursal(String newSucursal)   {
	sucursal = newSucursal;   }
   			
    public String getCodigoerror() {
    return codigo;	}
		
	public String getDescriperror() {
	return descrip;	}
	
	
	/**
	 * @param string Cadena que contiene los datos de la cuenta en formato xml
	 */
	public void leePerfil(String string) {	
			 
		
		try {
				//System.out.println("PerfilCuenta:");			
				//System.out.println(string);
				SAXBuilder builder = new SAXBuilder(false);
			
				//usar el parser Xerces y no queremos que valide el documento
				Document doc = builder.build(new StringReader(string));
			
				//construyo el arbol en memoria desde el fichero que se lo pasar� por parametro.			
				Element raiz = doc.getRootElement(); //tomo el elemento raiz

			   if (raiz.getName().equals("cuenta")) 
				{				
				cuenta=	raiz.getChildText("cuenta");			
				nombre1=raiz.getChildText("nombre1");
				nombre2=raiz.getChildText("nombre2");
				nombre3=raiz.getChildText("nombre3");
				nombre4=raiz.getChildText("nombre4");
				sucursal=raiz.getChildText("sucursal");						
				cta_found = true;
				
				} else if (raiz.getName().equals("error")) {
					codigo = raiz.getChildText("codigo");
					descrip = raiz.getChildText("descrip");
				}
			}catch (JDOMException e) {
			System.out.println(e.getMessage());
	  		}

	}	
	 		
}

