//*************************************************************************************************
//             Funcion: Clase que realiza la consulta de Campos lista a desplegar y los conserva en memoria
//            Elemento: CamposL.java
//          Creado por: Rutilo Zarco Reyes
//      Modificado por: Juvenal R. Fernandez V.
//*************************************************************************************************
// CCN - 4360253 - 07/01/2005 - Establecer como la primera opcion del Acdo, Credencial de IFE
// CCN - 4360334 - 17/06/2005 - Se elimina variable dbQualifier evitando enviar "." antes del nombre de la tabla
// CCN - 4360368 - 02/09/2005 - Se genera la nueva version de SUAS
// CCN - 4360373 - 07/09/2005 - Se genera nuevo CCN a peticion de QA 
// CCN - 4360406 - 15/12/2005 - Nueva version de suas
//*************************************************************************************************

package ventanilla.com.bital.beans;

import ventanilla.com.bital.util.DBQuery;

import java.util.Vector;
import java.util.Hashtable;

public class CamposL
{
    private Hashtable campomoneda = null;
    private Hashtable campos = null;
    private static CamposL instance = null;
    
    private CamposL() 
    {   
        campomoneda = new Hashtable();
        campos = new Hashtable();
        execute();
    }
    
    public static synchronized CamposL getInstance()
    {
        if(instance == null)
            instance = new CamposL();
        return instance;
    }
    
    public Vector getcamposl(String campo, String moneda)
    {
        Vector tablacl = new Vector();
        if(campomoneda.containsKey(campo + moneda))
            tablacl = (Vector)this.campomoneda.get(campo+moneda);
        else
            if(campos.containsKey(campo))
              tablacl = (Vector)this.campos.get(campo);
        return tablacl;
    }
    
    public String getDValor(String campo, String descrip)
    {
        String DValor = "";
        String Desc = "";
     
        if(campos.containsKey(campo))
        {
            Vector camposl = (Vector)campos.get(campo);
            int size = camposl.size();
            Vector row = new Vector();
            
            for(int i=0; i<size; i++)
            {                
                row = (Vector)camposl.get(i);
                Desc = (String)row.get(3);
                if(Desc.equals(descrip))
                {
                    DValor += ((String)row.get(2)).trim() + "~";
                    break;
                }
                
            }
        }
        return DValor;
    }
    
    public void execute() 
    {
        String sql = null;
        sql = "SELECT C_CAMPO, C_MONEDA, D_VALOR, D_DESCRIPCION FROM ";
        String clause = null;
        clause = " ORDER BY C_CAMPO, C_MONEDA, D_DESCRIPCION";
        
        DBQuery dbq = new DBQuery(sql,"TC_CNT_CAMPOS_L", clause);
        Vector Regs = dbq.getRows();
        
        int size = Regs.size();
        Vector row = new Vector();
        String campo = null;
        String newcampo = null;
        String moneda = null;
        String newkeycm = null;
        String keycm = null;
        Vector rowscm = new Vector();
        Vector rowsc = new Vector();
        for(int i=0; i<size; i++)
        {
            row = (Vector)Regs.get(i);
            campo = (String)row.get(0);
            moneda = (String)row.get(1);
            keycm = campo + moneda;
            if (newkeycm == null)
                newkeycm = keycm;
            if (newcampo == null)
                newcampo = campo;
            if ((!newkeycm.equals(keycm)) || (i == size-1))
            {
                if(i == size-1)
                    rowscm.add(row);
              campomoneda.put(newkeycm,rowscm);
              rowscm = new Vector();
              newkeycm = keycm;
            }
            if ((!newcampo.equals(campo)) || (i == size-1))
            {
                if(i == size-1)
                    rowsc.add(row);
              campos.put(newcampo,rowsc);
              rowsc = new Vector();
              newcampo = campo;
            }
            rowscm.add(row);
            rowsc.add(row);
        }
        OrdenaCampo();
    }
    
    public void OrdenaCampo()
    {
        Vector tablacl = null;
        Vector valor = null;
        tablacl = (Vector)this.campomoneda.get("lstTipoDocto01");
        valor =  (Vector)tablacl.elementAt(0);
        tablacl.removeElementAt(0);
        tablacl.insertElementAt(valor,1);
        
        tablacl = (Vector)this.campos.get("lstTipoDocto");
        valor =  (Vector)tablacl.elementAt(0);
        tablacl.removeElementAt(0);
        tablacl.insertElementAt(valor,1);

        tablacl = (Vector)this.campomoneda.get("lstTPSua01");
        valor = (Vector)tablacl.elementAt(1);
        tablacl.removeElementAt(1);
        tablacl.insertElementAt(valor,0);
        valor = (Vector)tablacl.elementAt(3);
        tablacl.removeElementAt(3);
        tablacl.insertElementAt(valor,0);

        tablacl = (Vector)this.campos.get("lstTPSua");
        valor = (Vector)tablacl.elementAt(1);
        tablacl.removeElementAt(1);
        tablacl.insertElementAt(valor,0);
        valor = (Vector)tablacl.elementAt(3);
        tablacl.removeElementAt(3);
        tablacl.insertElementAt(valor,0);
    }
}
