//*************************************************************************************************
//             Funcion: Clase que realiza la consulta de Campos a desplegar de la txn y los conserva en memoria
//          Creado por: Rutilo Zarco Reyes
//      Modificado por: Juvenal R. Fernandez V.
//*************************************************************************************************
// CCN -4360152- 21/06/2004 - Se crea la clase para realizar select y mantener los datos en memoria
// CCN - 4360334 - 17/06/2005 - Se elimina variable dbQualifier evitando enviar "." antes del nombre de la tabla
//*************************************************************************************************

package ventanilla.com.bital.beans;

import ventanilla.com.bital.util.QueryName;

import java.util.Vector;
import java.util.Hashtable;

public class CamposXTxn
{
    private Hashtable camposxtxn = null;
    private static CamposXTxn instance = null;
    
    private CamposXTxn() 
    {   
        camposxtxn = new Hashtable();
        execute();
    }
    
    public static synchronized CamposXTxn getInstance()
    {
        if(instance == null)
            instance = new CamposXTxn();
        return instance;
    }
    
    public Hashtable getcamposxtxn(String txn)
    {
        java.util.Hashtable cxt = null;
        if(camposxtxn.containsKey(txn))
        {
            cxt = new java.util.Hashtable();            
            cxt = (Hashtable)this.camposxtxn.get(txn);
        }
        return cxt;
    }
    
    public void execute()
    {
        String sql = null;
        sql = "SELECT C_TRANSACCION,D_CAMPOS_DE_TXN,D_DESCRIPCION,D_NVL_AUTORIZ,D_TITULO_IMAGEN,D_RQ_AUTO_FIRMA,D_VISTA_ENTRADA,D_VISTA_SALIDA,D_CMPO_RQ_FIRMA,D_REQ_CERTIFICA FROM ";
        String clause = null;
        clause = " ";
        
        QueryName dbq = new QueryName(sql,"TC_CAMPOS_X_TXN", clause);
        Vector Regs = dbq.getRows();
        
        int size = Regs.size();
        Vector row = new Vector();
        Vector rowname = new Vector();
        String txn = null;

        for(int i=0; i<size; i+=2)
        {
            row = (Vector)Regs.get(i);
            rowname = (Vector)Regs.get(i+1);
            txn = row.get(0).toString();
            Hashtable info = new Hashtable();
            for(int j=0; j<row.size(); j++)
                info.put((String)rowname.get(j),(String)row.get(j));
            camposxtxn.put(txn, info);
        }
    }
}
