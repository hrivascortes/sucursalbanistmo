//*************************************************************************************************
//             Funcion: Bean para captura de datos txn 0081
//            Elemento: Login.java
//          Creado por: Alejandro Gonzalez Casttro
//      Modificado por: Juvenal R. Fernandez V.
//*************************************************************************************************
// CCN - 4360178 - 20/08/2004 - Se agregan metodos get y setDescrip para journailzar en el Diario
//*************************************************************************************************


package ventanilla.com.bital.sfb;

public class Login extends Header {
    private java.lang.String txtCashIn = "";
    private java.lang.String txtDescrip = "";
    
    public Login() {
    }
    
    public java.lang.String getCashIn() {
        return txtCashIn + "*";
    }
    public void setCashIn(java.lang.String newCashIn) {
        txtCashIn = newCashIn;
    }
    
    public java.lang.String getDescrip() {
        return txtDescrip + "*";
    }
    public void setDescrip(java.lang.String newDescrip) {
        txtDescrip = newDescrip;
    }
    
    public String toString() {
        return super.toString() + "**" + getCashIn() + "********************";
    }
}
