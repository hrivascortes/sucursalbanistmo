package ventanilla.com.bital.sfb;

public class VouchersA extends Header
{
  private String txtEffDate  = "";
  private String txtAcctNo   = "";
  private String txtTranAmt  = "";
  private String txtTranCur  = "";
  private String txtMoamoun = "";
  private String txtTranDesc = "";
  private String txtHoldays  = "";
  private String txtCheckNo3 = "";
  private String txtAmount4  = "";
  
  private String txtDescRev  = "";  

  public String getEffDate()
  {
	return txtEffDate + "*";
  }
  public void setEffDate(String newEffDate)
  {
	txtEffDate = newEffDate;
  }
  
  public String getAcctNo()
  {
	return txtAcctNo + "*";
  }
  public void setAcctNo(String newAcctNo)
  {
	txtAcctNo = newAcctNo;
  }

  public String getTranAmt()
  {
	return txtTranAmt + "*";
  }
  public void setTranAmt(String newTranAmt)
  {
	txtTranAmt = newTranAmt;
  }
  
  public String getTranCur()
  {
	return txtTranCur + "*";
  }
  public void setTranCur(String newTranCur)
  {
	txtTranCur = newTranCur;
  }
  
  public String getMoamoun()
  {
	return txtMoamoun + "*";
  }
  public void setMoamoun(String newMoamoun)
  {
	txtMoamoun = newMoamoun;
  }
  
  public String getTranDesc()
  {
	return txtTranDesc + "*";
  }
  public void setTranDesc(String newTranDesc)
  {
	txtTranDesc = newTranDesc;
  }
  
  public String getHoldays()
  {
	return txtHoldays + "*";
  }
  public void setHoldays(String newHoldays)
  {
	txtHoldays = newHoldays;
  }
  
  public String getCheckNo3()
  {
	return txtCheckNo3 + "*";
  }
  public void setCheckNo3(String newCheckNo3)
  {
	txtCheckNo3 = newCheckNo3;
  }
  
  public String getAmount4()
  {
	return txtAmount4 + "*";
  }
  public void setAmount4(String newAmount4)
  {
	txtAmount4 = newAmount4;
  }
  
  public String getDescRev()
  {
	return txtDescRev;
  }    
  
  public void setDescRev(String newDescRev)
  {
	txtDescRev = newDescRev;
  }   

 
  public String toString()
  {
	return super.toString()
	+ getEffDate()	+ getAcctNo()	+ "*"	+ getTranAmt()
	+ getTranCur()	+ "*"	+ "*"		+ "*"
	+ "*"	+ "*"	+ getMoamoun()	+ "*"
	+ getTranDesc()	+ getHoldays()	+ "*"
	+ "*"	+ "*"	+ "*"	+ "*"	+ "*"	+ "*"	+ "*"
	+ "*"	+ "*"	+ "*"	+ "*"	+ "*"	+ "*"	+ "*"
	+ "*"	+ "*"	+ "*"	+ "*"	+ getCheckNo3()	+ "*"	+ "*"
	+ "*"	+ getAmount4()	+ "*"	+ "*"	+ "*"	+ "*"	+ "*"
	+ "*"	+ "*"	+ "*"	+ "*"	+ "*"	+ "*"	+ "*";
  }    
}
