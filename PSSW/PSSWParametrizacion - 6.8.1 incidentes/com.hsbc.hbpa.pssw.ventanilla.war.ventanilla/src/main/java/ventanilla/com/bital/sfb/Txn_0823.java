//*************************************************************************************************
//             Funcion: Bean Txn 0823
//      Modificado por: Jesus Emmanuel Lopez Rosales
//*************************************************************************************************
// CCN - 4360424 - 03/02/2006 - Se agrega para txn combinada
// CCN - 4360576 - 09/03/2007 - Modificaciones para proyecto OPMN
//*************************************************************************************************
package ventanilla.com.bital.sfb;

import ventanilla.GenericClasses;
import com.bital.util.ParameterNotFoundException;
import com.bital.util.ParameterParser;


public class Txn_0823 extends Transaction
{		 
	public Header doAction(ParameterParser param)
		throws ParameterNotFoundException
	{				
        GenericClasses gc = new GenericClasses();                				
		FormatoG formatog = new FormatoG();
		String TI = param.getString("iTxn");
		String TO = param.getString("oTxn");
		String Monto = "";
		 
/*[ODCT 0823G 00835083506  083506  000*              *       V      *     US$    *    00000     *     6        *              *  7141462        *    000      *     0         *  0574376     *   0574376     *              *                 *            *]	
	
 super.toString()                    + getEffDate()  + getService() + getToCurr()+ getCheckNo() + getGStatus() + getBcoTrnsf()+ getTranAmt()    + getComAmt() + getIVAAmt()   + getAcctNo()  + getCtaBenef() + getBenefic() + getOrdenan()    + getInstruc();
	*/
	   		formatog.setTxnCode("0823");
    		formatog.setFormat("G");	
	
	        if(param.getString("moneda").equals("02"))
        {
            formatog.setService( "C" );
            formatog.setGStatus("1");
            if (TI.equals("4057") || TI.equals("4059") || TI.equals("4063"))
                formatog.setGStatus("6");
            if (TI.equals("4043"))
                formatog.setGStatus("5");
        }
        else
        {
            formatog.setService( "V" );
            if (TO.equals("0106"))
                formatog.setGStatus("1");
            else if (TO.equals("4111"))
                formatog.setGStatus("5");
            else if (TO.equals("4115"))
                formatog.setGStatus("6");
            else if (TO.equals("0778"))
                formatog.setGStatus("4");
            else if (TO.equals("0786"))
                formatog.setGStatus("3");
            else if (TO.equals("0116"))
                formatog.setGStatus("2");
        }

        if(TI.equals("0112") || TI.equals("0114"))
            formatog.setGStatus("5");

        if(TI.equals("0108") || TI.equals("0110") || TI.equals("0554") || TI.equals("0600"))
            formatog.setGStatus("6");

        formatog.setToCurr("US$");

        String txtAutoriCV = param.getString("txtAutoriCV");
        if ( txtAutoriCV == null )
            txtAutoriCV = "00000";
        formatog.setCheckNo( "00000" + txtAutoriCV );
        Monto = param.getString("txtMonto1");
        Monto = gc.delCommaPointFromString(Monto);
        
        formatog.setTranAmt( "000" );
        
       
        
        if(Long.parseLong(Monto) > 0)
            formatog.setTranAmt( gc.delCommaPointFromString(param.getString("txtMonto1")) );
        String txtTipoCambioCV = param.getString("txtTipoCambioCV");
        if ( txtTipoCambioCV.equals("0.0000") )
            txtTipoCambioCV = "0";
        else
            txtTipoCambioCV = gc.delCommaPointFromString(param.getString("txtTipoCambioCV"));
        formatog.setComAmt(gc.delCommaPointFromString(param.getString("txtTipoCambioCV") + "00" ));
        formatog.setIVAAmt( "0" );
        formatog.setAcctNo( param.getString("registro") );
        formatog.setCtaBenef( param.getString("txtRegistro") );

		return formatog;
	}
	

}
