//*************************************************************************************************
//			 Funcion: Clase que realiza txn 1073
//			Elemento: Group16Serv.java
//		  Creado por: Alejandro Gonzalez Castro
//	  Modificado por: Juvenal Fernandez
//*************************************************************************************************
//CCN - 4360268 - 04/02/2005 - Se realizan adecuaciones para eliminar Agencia NY
//*************************************************************************************************
package ventanilla.com.bital.sfb;

import com.bital.util.ParameterParser;
import com.bital.util.ParameterNotFoundException;

public class Group05 extends Transaction
{
  public Header doAction(ParameterParser param)
    throws ParameterNotFoundException
  {
    // Instanciar el bean a utilizar
    Deposito deposito = new Deposito();
    
    // Ingresar datos requeridos
    deposito.setAcctNo (param.getString("txtDDACuenta"));
    deposito.setCashIn (param.getCString("txtEfectivo"));
    if (param.getString("override") != null)
       if (param.getString("override").equals("SI"))
           deposito.setOverride("3");
    
    long monto = Long.parseLong( param.getCString("txtEfectivo"));
    monto += Long.parseLong( param.getCString("txtCheque"));
    deposito.setTranAmt( Long.toString(monto) );

    int moneda = param.getInt("moneda");
    if( moneda == 1 )
      deposito.setTranCur("N$");
    else if( moneda == 2 )
      deposito.setTranCur("US$");
    else
      deposito.setTranCur("UDI");
    
    return deposito;  
  }               
}
