//*************************************************************************************************
//             Funcion: Bean Txn 0350 para comisi�n en txn 4155
//          Creado por: Fausto Rodrigo Flores Moreno
//*************************************************************************************************
// CCN - 4360593 - 20/04/2007 - Se crea bean para corregir ivas duplicados
//*************************************************************************************************
package ventanilla.com.bital.sfb;

import com.bital.util.ParameterParser;
import com.bital.util.ParameterNotFoundException;
import ventanilla.GenericClasses;

public class Txn_0350 extends Transaction
{
  public Header doAction(ParameterParser param)
    throws ParameterNotFoundException
  {
    FormatoB formatob = new FormatoB();
    GenericClasses gc = new GenericClasses();

    formatob.setFormat("B");
    formatob.setTxnCode("0350");
    formatob.setAcctNo("0350");
	formatob.setTranAmt(param.getCString("txtComision"));
	formatob.setReferenc(param.getString("txtSerialCheq"));
	formatob.setCashIn(param.getCString("txtComision"));
    String moneda = gc.getDivisa(param.getString("moneda"));
    formatob.setFromCurr(moneda);

    return formatob;
  }               
}
