//*************************************************************************************************
//             Funcion: Bean Txn 1653
//           Elemento : Txn_1653.java
//          Creado por: Fausto Rodrigo Flores Moreno
//*************************************************************************************************
// CCN - 4360493 - 23/06/2006 - Se crea Bean para txn Depto 9005
//*************************************************************************************************
package ventanilla.com.bital.sfb;

import com.bital.util.ParameterParser;
import com.bital.util.ParameterNotFoundException;
import ventanilla.GenericClasses;

public class Txn_1653 extends Transaction
{
	public Header doAction(ParameterParser param)
		throws ParameterNotFoundException
	{
		FormatoA formatoa = new FormatoA();
		GenericClasses gc = new GenericClasses();
		formatoa.setTxnCode("1653");
		formatoa.setAcctNo(param.getString("txtDDACuenta"));
		formatoa.setTranAmt(param.getCString("txtMonto"));
		formatoa.setCheckNo(param.getCString("txtSerial"));
		String moneda = gc.getDivisa(param.getString("moneda"));
		formatoa.setTranCur(moneda);
		String FEfect = param.getString("txtFechaEfect");
		FEfect = FEfect.substring(0,2)+FEfect.substring(3,5)+FEfect.substring(6,FEfect.length());
		formatoa.setTranDesc("01DEV COM SERV COB DOCUMENTARIA"+FEfect);
		formatoa.setDescRev("01REV.DEV COM SERV COB DOCUMENTARI"+FEfect.substring(0,6));
		formatoa.setMoAmoun("000");
        
        return formatoa;
	}
}
