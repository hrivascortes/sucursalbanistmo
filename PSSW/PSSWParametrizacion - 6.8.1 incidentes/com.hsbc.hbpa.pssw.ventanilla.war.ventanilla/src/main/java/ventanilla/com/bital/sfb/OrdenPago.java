package ventanilla.com.bital.sfb;

public class OrdenPago extends Header2
{
  private String txtTxn         = "";
  private String txtField32Date = "";
  private String txtField32Curr = "";
  private String txtField32Amnt = "";
  private String txtField14a    = "";
  private String txtField14n    = "";
  private String txtField12a    = "";
  private String txtField12n    = "";
  private String txtField12e    = "";
  private String txtField12t    = "";
  private String txtField25n    = "";
  private String txtField27     = "";
  private String txtField17f    = "";
  private String txtField17t    = "";
  private String txtField17u    = "";
  private String txtField28     = "";
  private String txtData        = "";
  private String newBranch      = "";
  private String currency       = "";
  private String txtTranAmt     = "";
  private String txtOrden       = "";
  private String txtSupervisor  = "";
  private String txtReversable     = "";
  private String txtNomOrd      = "";
  private String txtApeOrd      = "";
  private String txtNomBen      = "";
  private String txtApeBen      = "";
  private String txtBanco       = "";

  public OrdenPago()
  {
  }

  public void setTxnCode(String newTxn)
  {
    txtTxn = newTxn;
  }

  public void setProcessCode(String newTxn)
  {
    txtTxn = newTxn;
  }

  public void setField32(String newField32Date, String newField32Curr, String newField32Amnt)
  {
    txtField32Date = newField32Date;
    txtField32Curr = newField32Curr;
    txtField32Amnt = newField32Amnt;
  }

  public void setField14a(String newField14a)
  {
    txtField14a = newField14a;
  }

  public void setField14n(String newField14n)
  {
    txtField14n = newField14n;
  }

  public void setField12a(String newField12a)
  {
    txtField12a = newField12a;
  }

  public void setField12n(String newField12n)
  {
    txtField12n = newField12n;
  }

  public void setField12e(String newField12e)
  {
    txtField12e = newField12e;
  }

  public void setField12t(String newField12t)
  {
    txtField12t = newField12t;
  }

  public void setField25n(String newField25n)
  {
    txtField25n = newField25n;
  }

  public void setField27(String newField27)
  {
    txtField27 = newField27;
  }

  public void setField17f(String newField17f)
  {
    txtField17f = newField17f;
  }

  public void setField17t(String newField17t)
  {
    txtField17t = newField17t;
  }

  public void setTeller(String newField17u)
  {
    txtField17u = newField17u;
  }

  public String getSupervisor()
  {
  	return txtField17u;
  }

  public void setBranch(String newBranch){
  	this.newBranch = newBranch;
  }

  public String getBranch(){
  	return newBranch;
  }

  public void setField28(String newField28)
  {
    txtField28 = newField28;
  }

  public void setData(String newData)
  {
    txtData = newData;
  }

  public String getTxnCode()
  {
    return "DSLZ";
  }

  public String getProcessCode()
  {
    return txtTxn;
  }

  public String getField32()
  {
    return "32@" + txtField32Date + txtField32Curr + txtField32Amnt + "@" ;
  }

  public String getField14a()
  {
    return "14A@" + txtField14a + "@";
  }

  public String getField14n()
  {
    return "14N@" + txtField14n + "@";
  }

  public String getField12a()
  {
    return "12A@" + txtField12a + "@";
  }

  public String getField12n()
  {
    return "12N@" + txtField12n + "@";
  }

  public String getField12e()
  {
    return "12E@" + txtField12e + "@";
  }

  public String getField12t()
  {
    return "12T@" + txtField12t + "@";
  }

  public String getField25n()
  {
    return "25N@" + txtField25n + "@";
  }

  public String getField27()
  {
    return "27@" + txtField27 + "@";
  }

  public String getField17f()
  {
    return "17F@" + txtField17f + "@";
  }

  public String getField17t()
  {
    return "17T@" + txtField17t + "@";
  }

  public String getTeller()
  {
    return txtField17u;
  }

  public String getField28()
  {
    return "28@" + txtField28;
  }

  public String getData()
  {
    return txtData;
  }

  public void setFromCurr(String currency)
  {
  	this.currency = currency;
  }

  public String getFromCurr()
  {
  	return currency;
  }

  public void setOrden(String Orden)
  {
  	 txtOrden = Orden;
  }

  public String getOrden()
  {
  	return txtOrden;
  }

  public void setTranAmt(String TranAmt)
  {
  	txtTranAmt = TranAmt;
  }

  public String getTranAmt()
  {
  	return txtTranAmt;
  }

  public String getReversable()
  {
	return txtReversable;
  }    
  
  public void setReversable(String newReverso)
  {
	txtReversable = newReverso;
  }

  public String getNomOrd()
  {
	return txtNomOrd;
  }    
  
  public void setNomOrd(String newNomOrd)
  {
	txtNomOrd = newNomOrd;
  }

  public String getApeOrd()
  {
	return txtApeOrd;
  }    
  
  public void setApeOrd(String newApeOrd)
  {
	txtApeOrd = newApeOrd;
  }

  public String getNomBen()
  {
	return txtNomBen;
  }    
  
  public void setNomBen(String newNomBen)
  {
	txtNomBen = newNomBen;
  }
  
  public String getApeBen()
  {
	return txtApeBen;
  }    
  
  public void setApeBen(String newApeBen)
  {
	txtApeBen = newApeBen;
  }
  
  public String getBanco()
  {
	return txtBanco;
  }    
  
  public void setBanco(String newBanco)
  {
	txtBanco = newBanco;
  }

  public String toString()
  {

    String Cadena;
    if(getProcessCode().equals("EXP "))
     Cadena = new String(super.toString() + getProcessCode() + "@"
	               + getField32()     + getField14a() + getField14n()
	               + getField12a()    + getField12n() + getField12e()
	               + getField12t()    + getField25n() + getField27()
	               + getField17f()    + getField17t() + "17U@" + getTeller() + "@"
	               + getField28()     + "-");
    else
     Cadena = new String(super.toString() + getProcessCode() + getData());
    for(int i = Cadena.length(); i < 512; i++)
     Cadena = Cadena + " ";
    return Cadena;
  }
}
