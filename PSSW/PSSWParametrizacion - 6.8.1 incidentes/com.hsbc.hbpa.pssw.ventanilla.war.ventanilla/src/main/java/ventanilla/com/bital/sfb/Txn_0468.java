//*************************************************************************************************
//             Funcion: Bean Txn 0468
//      Modificado por: Alejandro Gonzalez Castro
//*************************************************************************************************
// CCN - 4360268 - 04/02/2005 - Se realizan adecuaciones para eliminar Agencia NY
// CCN - 4360424 - 03/02/2006 - Se agrega para txn combinada
//*************************************************************************************************
package ventanilla.com.bital.sfb;

import com.bital.util.ParameterParser;
import com.bital.util.ParameterNotFoundException;
import ventanilla.GenericClasses;

public class Txn_0468 extends Transaction
{
	public Header doAction(ParameterParser param)
		throws ParameterNotFoundException
	{
		FormatoB formatob = new FormatoB();
		GenericClasses gc = new GenericClasses();

		formatob.setTxnCode("0468");
		formatob.setFormat("B");
		formatob.setAcctNo(param.getString("cTxn"));
		formatob.setTranAmt(param.getCString("txtMonto"));
		formatob.setReferenc(param.getString("txtRefer0468"));
		formatob.setCashIn(param.getCString("txtMonto"));
		String moneda = gc.getDivisa(param.getString("moneda"));
		formatob.setFromCurr(moneda);

		return formatob;
	}
}
