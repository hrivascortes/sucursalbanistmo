//*************************************************************************************************
//          Funcion: Clase especial para transacciones de OPEE
//          Elemento: InterfazSF.java
//          Creado por: Yair Jesus Ch�vez Antonel.
//      Modificado por: Fausto Rodrigo Flores Moreno
//*************************************************************************************************
// CCN - 4360533 - 20/10/2006 - Se crea elemento para funcion especial de OPEE
// CR -  - 25/11/2010 - FFM - Se aplica esquema de M�xico de SpFunc a Panama
// CO - CR7564 - 26/01/2011 - Cambios Proyecto camp
//*************************************************************************************************
package ventanilla.com.bital.admin;

import java.util.Vector;
import java.util.Hashtable;
import javax.servlet.http.HttpSession;

public interface InterfazSF {
	public int SpFuncionBD(Object o, Vector r, HttpSession s, String Connureg);//DIARIO Before
	public int SpFuncionAD(Object o, Vector r, HttpSession s, String Connureg);//DIARIO After
	public int SpFuncionBR(Hashtable txnRev, Hashtable txnOrg, HttpSession session, String Connureg, String Supervisor);
	public int SpFuncionAR(Hashtable txnRev, Hashtable txnOrg, HttpSession session, String Connureg, String Cosecutivo, String Supervisor, Vector VResp);
}
