//*************************************************************************************************
//             Funcion: Bean que contiene los atributos para cada servicio RAPL
//            Elemento: ServicioRAPL.java
//*************************************************************************************************
package ventanilla.com.bital.sfb;


public class ServicioRAPL
{
  private String noServicio   = null;
  private String descServicio = null;
  private boolean notificacion   = false; //true=servicio de notificacion
  private boolean cancelacion    = false; //true=servicio de Reverso
  private boolean consulta     = false;	 //true=servicio de consulta mto x ref.
  private boolean folioNotificacion    = false;	//true si regresa folio
  private String tipoPago = null;		//T=total y P=parcial
  
  public ServicioRAPL()
  {
	  
  }

public boolean isCancelacion() {
	return cancelacion;
}

public void setCancelacion(boolean cancelacion) {
	this.cancelacion = cancelacion;
}

public boolean isConsulta() {
	return consulta;
}

public void setConsulta(boolean consulta) {
	this.consulta = consulta;
}

public String getDescServicio() {
	return descServicio;
}

public void setDescServicio(String descServicio) {
	this.descServicio = descServicio;
}

public String getNoServicio() {
	return noServicio;
}

public void setNoServicio(String noServicio) {
	this.noServicio = noServicio;
}

public boolean isNotificacion() {
	return notificacion;
}

public void setNotificacion(boolean notificacion) {
	this.notificacion = notificacion;
}

public boolean isFolio() {
	return folioNotificacion;
}

public void setFolio(boolean folioNotificacion) {
	this.folioNotificacion = folioNotificacion;
}

public String getTipoPago() {
	return tipoPago;
}

public void setTipoPago(String tipoPago) {
	this.tipoPago = tipoPago;
}


   
  
}

