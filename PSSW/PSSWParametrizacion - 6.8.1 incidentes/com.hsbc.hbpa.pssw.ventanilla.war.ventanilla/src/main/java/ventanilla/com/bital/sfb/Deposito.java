//************************************************************************************************* 
//             Funcion: Bean para depositos
//            Elemento: Deposito.java
//          Creado por: Alejandro Gonzalez Castro
//*************************************************************************************************
// CCN - 4360221 - 22/10/2004 - Inclusion del tipo de pago en la certificacion de la 
//				txn "PAGO EN EFECTIVO / CON DOCUMENTOS / MIXTO
//*************************************************************************************************
package ventanilla.com.bital.sfb;

public class Deposito extends Header
{
  private String txtEffDate  = null;
  private String txtAcctNo   = null;
  private String txtTranAmt  = null;
  private String txtTranCur  = null;
  private String txtCheckNo  = null;
  private String txtTrNo     = null;
  private String txtCashIn   = null;
  private String txtMoamoun = null;
  private String txtFees     = null;
  private String txtTranDesc = null;
  private String txtHoldays  = null;
  private String txtTrNo1    = null;
  private String txtAcctNo2  = null;
  private String txtAmount2  = null;
  private String txtCheckNo2 = null;
  private String txtTrNo2    = null;
  private String txtHolDays2 = null;
  private String txtAmount3  = null;
  private String txtCheckNo3 = null;
  private String txtTrNo3    = null;
  private String txtAmount4  = null;
  private String txtCheckNo4 = null;
  private String txtAmount5  = null;
  private String txtCheckNo5 = null;
  private String txtDescRev = null;
  private String txtDesc2   = null;

  public Deposito()
  {
    super();
  }

  private String getValue(String value)
  {
    if( value == null )
      return "*";
    else
      return value + "*";  
  }

  public String getAcctNo()
  {
    return getValue(txtAcctNo);
  }

  public String getAmount2()
  {
    return getValue(txtAmount2);
  }
  
  public String getAmount3()
  {
    return getValue(txtAmount3);
  }

  public String getAmount4()
  {
    return getValue(txtAmount4);
  }

  public String getAmount5()
  {
    return getValue(txtAmount5);
  }

  public String getCashIn()
  {
    return getValue(txtCashIn);
  }

  public String getCheckNo()
  {
    return getValue(txtCheckNo);
  }

  public String getCheckNo3()
  {
    return getValue(txtCheckNo3);
  }

  public String getCheckNo4()
  {
    return getValue(txtCheckNo4);
  }

  public String getCheckNo5()
  {
    return getValue(txtCheckNo5);
  }

  public String getEffDate()
  {
    return getValue(txtEffDate);
  }

  public String getFees()
  {
    return getValue(txtFees);
  }

  public String getMoamoun()
  {
    return getValue(txtMoamoun);
  }

  public String getHoldays()
  {
    return getValue(txtHoldays);
  }

  public String getTrNo1()
  {
    return getValue(txtTrNo1);
  }

  public String getAcctNo2()
  {
    return getValue(txtAcctNo2);
  }

  public String getCheckNo2()
  {
    return getValue(txtCheckNo2);
  }

  public String getTrNo2()
  {
    return getValue(txtTrNo2);
  }

  public String getHolDays2()
  {
    return getValue(txtHolDays2);
  }

  public String getTranAmt()
  {
    return getValue(txtTranAmt);
  }

  public String getTranCur()
  {
    return getValue(txtTranCur);
  }

  public String getTranDesc()
  {
    return getValue(txtTranDesc);
  }

  public String getTrNo()
  {
    return getValue(txtTrNo);
  }

  public String getTrNo3()
  {
    return getValue(txtTrNo3);
  }

  public void setAcctNo(String newAcctNo)
  {
    txtAcctNo = newAcctNo;
  }

  public void setAmount2(String newAmount2)
  {
    txtAmount2 = newAmount2;
  }  
  
  public void setAmount3(String newAmount3)
  {
    txtAmount3 = newAmount3;
  }

  public void setAmount4(String newAmount4)
  {
    txtAmount4 = newAmount4;
  }

  public void setAmount5(String newAmount5)
  {
    txtAmount5 = newAmount5;
  }

  public void setCashIn(String newCashIn)
  {
    txtCashIn = newCashIn;
  }

  public void setCheckNo(String newCheckNo)
  {
    txtCheckNo = newCheckNo;
  }

  public void setCheckNo3(String newCheckNo3)
  {
    txtCheckNo3 = newCheckNo3;
  }

  public void setCheckNo4(String newCheckNo4)
  {
    txtCheckNo4 = newCheckNo4;
  }

  public void setCheckNo5(String newCheckNo5)
  {
    txtCheckNo5 = newCheckNo5;
  }

  public void setEffDate(String newEffDate)
  {
    txtEffDate = newEffDate;
  }

  public void setMoamoun(String newMoamoun)
  {
    txtMoamoun = newMoamoun;
  }

  public void setFees(String newFees)
  {
    txtFees = newFees;
  }

  public void setHoldays(String newHoldays)
  {
    txtHoldays = newHoldays;
  }

  public void setTrNo1(String newTrNo1)
  {
    txtTrNo1 = newTrNo1;
  }

  public void setAcctNo2(String newAcctNo2)
  {
    txtAcctNo2 = newAcctNo2;
  }

  public void setCheckNo2(String newCheckNo2)
  {
    txtCheckNo2 = newCheckNo2;
  }

  public void setTrNo2(String newTrNo2)
  {
    txtTrNo2 = newTrNo2;
  }

  public void setHolDays2(String newHolDays2)
  {
    txtHolDays2 = newHolDays2;
  }

  public void setTranAmt(String newTranAmt)
  {
    txtTranAmt = newTranAmt;
  }

  public void setTranCur(String newTranCur)
  {
    txtTranCur = newTranCur;
  }

  public void setTranDesc(String newTranDesc)
  {
    txtTranDesc = newTranDesc;
  }

  public void setTrNo(String newTrNo)
  {
    txtTrNo = newTrNo;
  }

  public void setTrNo3(String newTrNo3)
  {
    txtTrNo3 = newTrNo3;
  }

  public String getDescRev()
  {
	return txtDescRev;
  }    
  
  public void setDescRev(String newDescRev)
  {
	txtDescRev = newDescRev;
  }

  public String getDesc2()
  {
	return txtDesc2;
  }    
  
  public void setDesc2(String newDesc2)
  {
	txtDesc2 = newDesc2;
  }

  
  public String toString()
  {
    return super.toString() + getEffDate() + getAcctNo() + "*"
	  + getTranAmt() + getTranCur() + getCheckNo() + getTrNo()
          + getCashIn() + "**" + getMoamoun() + getFees() + getTranDesc()
          + getHoldays() + "******" + getTrNo1() + "**"+ getAmount2() + "*"
          + getAcctNo2() + getCheckNo2() + getTrNo2() + getHolDays2() + "*" 
          + getAmount3() + "**" + getCheckNo3() + getTrNo3() + "**" + getAmount4()
          + "**" + getCheckNo4() + "***" + getAmount5() + "**" + getCheckNo5() + "**";
  }
}
