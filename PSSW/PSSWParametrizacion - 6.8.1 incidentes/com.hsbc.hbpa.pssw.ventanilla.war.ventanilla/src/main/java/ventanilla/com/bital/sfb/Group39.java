//*************************************************************************************************
//		   Funcion: Transaccion 4157 - Cheques de Caja
//		  Elemento: Group35.java
//	Modificado por: Humberto Enrique Balleza Mej�a
//*************************************************************************************************
////CCN - 4360424 - 03/02/2006 - Se realizan adecuaciones para combinadas
////CCN -         - 01/03/2008 - Se realizan adecuaciones para cuenta de cheque de Gerencia Panam�
//*************************************************************************************************

package ventanilla.com.bital.sfb;

import com.bital.util.ParameterParser;
import com.bital.util.ParameterNotFoundException;

public class Group39 extends Transaction
{
  public Header doAction(ParameterParser param)
    throws ParameterNotFoundException
  {
    Cheques cheque = new Cheques();
	cheque.setTxnCode("4157");    
    cheque.setTranAmt( param.getCString("txtMonto") );
    int moneda = param.getInt("moneda");
    if( moneda == 1 )
    {
      cheque.setTranCur("N$");
      cheque.setAcctNo ("0103000023");
    }
    else if( moneda == 2 )
    {
      cheque.setTranCur("US$");
      cheque.setAcctNo ("0103000023");
    }
    else
      cheque.setTranCur("UDI");

    cheque.setCheckNo (param.getString("txtSerialExp"));
    cheque.setTranDesc(param.getString("txtBeneficiario"));
    return cheque;
  }               
}
