//*************************************************************************************************
//			   Funcion: Bean Txn 0278
//			Creado por: Guillermo Rodrigo Escand�n Soto
//*************************************************************************************************
//
//*************************************************************************************************
package ventanilla.com.bital.sfb;

import com.bital.util.ParameterParser;
import com.bital.util.ParameterNotFoundException;
import ventanilla.GenericClasses;

public class Txn_0278 extends Transaction
{
	public Header doAction(ParameterParser param)
		throws ParameterNotFoundException
	{

		FormatoB formatob = new FormatoB();
		GenericClasses gc = new GenericClasses();
		
		formatob.setTxnCode("0278");
		formatob.setFormat("B");
		formatob.setAcctNo(param.getCString("txtDDACuenta"));
		formatob.setTranAmt(param.getCString("txtMonto1"));
		formatob.setReferenc("0000000000000000");
		formatob.setCashOut(param.getCString("txtMonto1"));
		String moneda = gc.getDivisa((String)param.getString("moneda"));
		formatob.setFromCurr(moneda);

		return formatob;
	}
}