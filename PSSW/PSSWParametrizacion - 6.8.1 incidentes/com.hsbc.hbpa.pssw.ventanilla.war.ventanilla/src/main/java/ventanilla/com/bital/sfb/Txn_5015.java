//*************************************************************************************************
//             Funcion: Bean Txn 4209 Impresion de Cheque de Gerencia.
//             Elaborado por: YGX
//*************************************************************************************************

package ventanilla.com.bital.sfb;

import java.util.StringTokenizer;

import ventanilla.GenericClasses;
import com.bital.util.ParameterNotFoundException;
import com.bital.util.ParameterParser;


public class Txn_5015 extends Transaction
{		 
	public Header doAction(ParameterParser param)
		throws ParameterNotFoundException
	{				
        GenericClasses gc = new GenericClasses();   
        FormatoA formatoa = new FormatoA();         				
		ventanilla.com.bital.sfb.Cheques Cheques = new ventanilla.com.bital.sfb.Cheques();
		String cad_llave="";
		String data="";
	    String datatmp="";
	    String datatmp1="";
	    String datatmp2="";
	    String datatmp3="";
	    String datatmp4="";
	    String datatmp5="";
	    String datatmp6="";
	    String datatmp7="";  
	    String datatmp8="";
	    String datatmp9="";
	   
		cad_llave=param.getString("val_datos");
		
		StringTokenizer tok = new StringTokenizer(cad_llave,"~");
		while(tok.hasMoreTokens()) 
	   { 
	    datatmp = tok.nextToken();
	    datatmp1 = tok.nextToken();
	    //System.out.println(datatmp1);	   
	    datatmp2 = tok.nextToken(); 
	    datatmp3 = tok.nextToken();			   
		datatmp4 = tok.nextToken();		 
		datatmp5 = tok.nextToken();		 
		datatmp6 = tok.nextToken();		  
		datatmp7 = tok.nextToken();
		datatmp8 = tok.nextToken(); 
		datatmp9 = tok.nextToken(); 
	   }
		
		/*System.out.println(datatmp);
		System.out.println(datatmp1);
		System.out.println(datatmp2);
		System.out.println(datatmp3);
		System.out.println(datatmp4);
		System.out.println(datatmp5);
		System.out.println(datatmp6);
		System.out.println(datatmp7);
		System.out.println(datatmp8);
		System.out.println(datatmp9);*/
			   
		
		formatoa.setTxnId("ODPA");
		formatoa.setTxnCode("5015");
	    formatoa.setFormat("A");   
	    
	      //Cuenta de Cheques de Gerencia
	    formatoa.setAcctNo(param.getString("txtDDACuenta").trim());
	    formatoa.setTranAmt(datatmp1);
	    int moneda = param.getInt("moneda");
	    formatoa.setTranCur(gc.getDivisa(param.getString("moneda")));
	    formatoa.setCashIn(datatmp1);
	    formatoa.setMoAmoun("003");
	    formatoa.setTranDesc("CANCELACION SOL. CHEQUE GER.");	    
	    formatoa.setHolDays("0");
	    formatoa.setHolDays2("0");	    

		//Descripcion para Reverso
		String DescRev = "REV. CANCELACION SOL. CHEQUE GER.";
		formatoa.setDescRev(DescRev);

	    	   	
	   	//System.out.println(formatoa);
	   	
		return formatoa;
	}
	

}
