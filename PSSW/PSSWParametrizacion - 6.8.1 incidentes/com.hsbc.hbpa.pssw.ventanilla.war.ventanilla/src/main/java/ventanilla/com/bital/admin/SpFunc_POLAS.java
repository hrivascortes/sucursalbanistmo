 
//*************************************************************************************************
//CR - CRXXXX - 23/07/2012 - Proyecto POLAS
//*************************************************************************************************


package ventanilla.com.bital.admin;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Hashtable;
import java.util.StringTokenizer;
import java.util.Vector;

import javax.servlet.http.HttpSession;

import com.hbmx.cppcore.model.Payment;
import com.hbmx.cppcore.model.PaymentType;

import ventanilla.GenericClasses;
import ventanilla.com.bital.beans.DatosVarios;
import ventanilla.com.bital.beans.MsgPolas;
import ventanilla.com.bital.sfb.ServicioRAPL;
import ventanilla.com.bital.util.ServiciosPolas;

public class SpFunc_POLAS implements InterfazSF{
	public SpFunc_POLAS(){
	}

	public int SpFuncionAD(Object o, Vector r, HttpSession session, String connureg) {
		int iResultado = 0;
		ServicioRAPL srapl=null;
		GenericClasses gc = new GenericClasses();	
		Hashtable datasession = (Hashtable)session.getAttribute("page.datasession");
		
		
		
//		INICIO DE POLAS
        DatosVarios rapl = DatosVarios.getInstance();                   
        Hashtable htRAPL = null;        
        htRAPL =(Hashtable)rapl.getDatosH("SERVICRAPL");
        // Si entra al if la operación viene directamente de la 5503 .
        if( null == datasession.get("txtServicio") ){
        	return 0;
        }else{
        	srapl = (ServicioRAPL)htRAPL.get((String)datasession.get("txtServicio"));
            if(null == srapl){
            	return 0;
            }	
        }
        
        
        
        
        if(srapl.isNotificacion()){
		    	//Recuperar datos para enviar a Admon Pagos
		    	
		    	int serv  = Integer.parseInt((String)datasession.get("txtServicio"));
		    	String ref = (String)datasession.get("txtReferencia");
		    	
		    	String mto = (String)datasession.get("total");
		    	mto=gc.delCommaFromString(mto);
		    	int suc  = Integer.parseInt((String)datasession.get("sucursal"));
		    	long cuenta=0;
		    	int consecHogan= Integer.parseInt((String)r.get(2));
		    	String tipoPago  = (String)datasession.get("TipoPago");
		    	
		       
		       	//Se crea bean Payment para enviarse a Admon Pagos
		       	Payment pago = new Payment();
		    	pago.setAccount(cuenta);
		    	pago.setService(serv);
		    	pago.setReference(ref);
		    	pago.setAmount(new BigDecimal(mto));
		    	pago.setTransactionNumber(consecHogan);
		    	pago.setBranchNumber(suc);
		    	pago.setChannel("PSSWPA");
		    	Date date = new java.util.Date();
		        java.sql.Timestamp timeStampDate = new Timestamp(date.getTime());
		        pago.setChannelDate(timeStampDate);
		        
		        if(tipoPago.compareTo("efe")==0){
		        	pago.setType(PaymentType.CASH);
		        }else if(tipoPago.compareTo("rem")==0){
		        	pago.setType(PaymentType.REMITTANCE);
		        }else if(tipoPago.compareTo("chq")==0){
		        	pago.setType(PaymentType.HSBC_CHECK);
		        }else if(tipoPago.compareTo("coi")==0){
		        	pago.setType(PaymentType.OTHER_BANKS_CHECK);
		        }else if(tipoPago.compareTo("cert")==0){
		        	pago.setType(PaymentType.CERTIF_HSBC_CHECK);
		        }else if(tipoPago.compareTo("tdc")==0){
		        	pago.setType(PaymentType.CREDIT);
		        }
		        
		        MsgPolas mp = new MsgPolas(pago); //se crea objeto para mensaje entrada y salida
		        
		   	            	
		    	//Para el log
		    	 
		      	 String  txn = "5503";
		       	
		      
		    	       		
		 		Hashtable datosCom = new Hashtable();
		     	datosCom.put("D_SUCURSAL",(String)datasession.get("sucursal"));
		 		datosCom.put("D_OPERADOR",(String)datasession.get("teller"));
		 		datosCom.put("D_DIVISA",gc.getDivisa((String)datasession.get("moneda")));
		  		datosCom.put("D_TXN", txn);        		
		  		datosCom.put("D_CONNUREG", connureg);
		  		datosCom.put("D_CONSECLIGA", gc.getDate(3));
		  		datosCom.put("D_INTERFASE", "POLAS");
		  		datosCom.put("D_SPFUNC_TXN", "POLAS");
		  		datosCom.put("D_ETAPA_TXN", "AD");
		  		datosCom.put("D_ESTATUS", "E");
		  		datosCom.put("D_MOTIVO", "N");
		  		
		  		InterfasesLog log=new InterfasesLog(datosCom);
		  		
		  		String metodos="canal=getChannel&cuenta=getAccount&servicio=getService&referencia=getReference&monto=getAmount&fecha=getChannelDate&notransaccion=getTransactionNumber&sucursal=getBranchNumber&tipopago=getType&";
	   	    	log.insertLog(pago, metodos);
		  	
		         ServiciosPolas sp=new ServiciosPolas();
		         sp.enviarNotificacion(pago,mp,session);
		         
		   	    String codResp=mp.getCodigo();
		   	    String msgResp=mp.getMensaje();
		   	    String folioResp=mp.getFolio();	   	    
		   	    	   	    
		   	    StringBuffer msgOut=new StringBuffer();
	  	    	msgOut.append(createResponse(mp,"codigo=getCodigo&"));
	  	    	Object objeto=mp.getMsgOutNot();
	  	    	if(objeto==null){
	  	    		metodos="mensaje=getMensaje&";
	      	    	objeto=mp;
	      	    	msgOut.append(createResponse(objeto,metodos));
	  	    	}
	  	    	else{
	  	    		metodos="mensaje=getMessage&folio=getExternalPaymentNumber&";
	  	    		msgOut.append(createResponse(objeto,metodos));
	  	    	}
	  	    	

		            	    
		   	    if (codResp.compareTo("2")==0 ||codResp.compareTo("3")==0){
		   	    	if(srapl.isFolio()){		   	    	  
		   	    		session.setAttribute("folioNotificacion","ND");
		   	    	}
		   	    	
		   	  
		   	    }else{		   	     
		   	     		if (codResp.compareTo("0")==0){
		   	     			if(srapl.isFolio()){
		   	     				if(folioResp.length()>30){
		   	     					folioResp=folioResp.substring(0,30);
		   	     				}
		   	     			   session.setAttribute("folioNotificacion",folioResp);
		   	     			}
		   	     		}      	     
		   	     
		   	    }
		   	    

		   	log.updateLog(connureg,"R", msgOut.toString(),"N",txn);	
   
		   	    
		   	    
        }else{iResultado=0;}
		return iResultado;
	}

	public int SpFuncionAR(Hashtable txnRev, Hashtable txnOrg, HttpSession session, String connureg, String cosecutivo, String supervisor, Vector vResp) {
		 
		int res=0;
		 DatosVarios rapl = DatosVarios.getInstance();                   
         Hashtable htRAPL = null;  
         Hashtable txn=txnRev;
         
         htRAPL =(Hashtable)rapl.getDatosH("SERVICRAPL");
         if(htRAPL!=null){
        	 if(htRAPL.get(txn.get("D_CUENTA").toString())!=null){
        		 ServicioRAPL srapl = (ServicioRAPL)htRAPL.get(txn.get("D_CUENTA").toString());
        		 
                 if(srapl.isCancelacion()){
	                	
	                	GenericClasses gc = new GenericClasses();
	                	int serv = Integer.parseInt(txn.get("D_CUENTA").toString());
	                	String ref = txn.get("D_REFER").toString();
	                	String mto = gc.formatMonto(txn.get("D_MONTO").toString());
	                	int consecHogan=Integer.parseInt(cosecutivo);
	                	int consec  = Integer.parseInt(txn.get("D_CONSECUTIVO").toString());
	                	int suc  = Integer.parseInt(txn.get("D_SUCURSAL").toString());
	                	long cuenta=0;
	                	   
	                   	// Se crea bean Payment para enviarse a Admon Pagos
	                   	Payment pago = new Payment();
	                	pago.setAccount(cuenta);
	                	pago.setService(serv);
	                	pago.setReference(ref);
	                	pago.setAmount(new BigDecimal(gc.delCommaFromString(mto)));
	                	pago.setTransactionNumber(consec);
	                	pago.setBranchNumber(suc);
	                	pago.setChannel("PSSWPA");
	                	Date date = new java.util.Date();
	                    java.sql.Timestamp timeStampDate = new Timestamp(date.getTime());
	                    pago.setChannelDate(timeStampDate);
	                    
	                    MsgPolas mp = new MsgPolas(pago); //se crea objeto para mensaje entrada y salida
	                   	
	           	    	           	    	
	           	    	Hashtable datosCom = new Hashtable();
	           	    	datosCom.put("D_SUCURSAL",txn.get("D_SUCURSAL").toString());
	           	    	datosCom.put("D_OPERADOR",txn.get("D_OPERADOR").toString());
	           	    	datosCom.put("D_DIVISA",txn.get("D_DIVISA").toString());
	           	    	datosCom.put("D_TXN", txn.get("D_TXN").toString());        		
	           	    	datosCom.put("D_CONNUREG", connureg);
	           	    	datosCom.put("D_CONSECLIGA", gc.getDate(3));
	           	    	datosCom.put("D_INTERFASE", "POLAS");
	           	    	datosCom.put("D_SPFUNC_TXN", "POLAS");
	           	    	datosCom.put("D_ETAPA_TXN", "AR");
	           	    	datosCom.put("D_ESTATUS", "E");
	           	    	datosCom.put("D_MOTIVO", "R");
	           	    	
	           	    	InterfasesLog log=new InterfasesLog(datosCom);
	           	    	
	           	    	String metodos="canal=getChannel&cuenta=getAccount&servicio=getService&referencia=getReference&monto=getAmount&fecha=getChannelDate&notransaccion=getTransactionNumber&sucursal=getBranchNumber&tipopago=getType&";
	           	    	log.insertLog(pago, metodos);
		   	    	   		  		
	           	    	
	                   	ServiciosPolas sp=new ServiciosPolas();//JGA
	                   	sp.enviarReverso(pago,mp);
	                   	String codResp=mp.getCodigo();
	               	    String msgResp=mp.getMensaje();
	               	    
	               	   
		               	StringBuffer MsgOut=new StringBuffer();
		 	  	    	MsgOut.append(createResponse(mp,"codigo=getCodigo&"));
		 	  	    	Object objeto=mp.getMsgOutCan();
		 	  	    	if(objeto==null){
		 	  	    		metodos="mensaje=getMensaje&";
		 	      	    	objeto=mp;
		 	      	    	MsgOut.append(createResponse(objeto,metodos));
		 	  	    	}
		 	  	    	else{
		 	  	    		metodos="mensaje=getMessage&";
		 	  	    		MsgOut.append(createResponse(objeto,metodos));
		 	  	    	}
	 	  	    	
	               	    log.updateLog(connureg,"R",MsgOut.toString(),"R",txn.get("D_TXN").toString());
	               	
	                }  
        	 }
         }
         else{ 
        	 return 0;
         }
		return res;
	}

	public int SpFuncionBD(Object o, Vector r, HttpSession session, String connureg) {
		// TODO Apéndice de método generado automáticamente
			return 0;
	}

	public int SpFuncionBR(Hashtable txnRev, Hashtable txnOrg, HttpSession session, String connureg, String supervisor) {
		// TODO Apéndice de método generado automáticamente		
		return 0;
	}
	
	private String createResponse(Object obj, String method){
		GenericClasses gc = new GenericClasses();
		StringBuffer result = new StringBuffer();
		StringTokenizer datos = new StringTokenizer(method,"&");
		while(datos.hasMoreTokens()){
			StringTokenizer values = new StringTokenizer(datos.nextToken().trim(),"=");
			while(values.hasMoreTokens()){
				result.append(values.nextToken())
					  .append("=")
					  .append(gc.getBeanData(obj, values.nextToken()));
			}
			result.append("|");
		}
		
		return result.toString();
	}

}

