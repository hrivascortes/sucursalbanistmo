//*************************************************************************************************
//             Funcion: Bean para txns de transferencia (0041 - 0043 - 0044 - 0045 - 0046 - 0051)
//            Elemento: Transferencia.java
//          Creado por: Alejandro Gonzalez Castro
//      Modificado por: Juvenal Fernandez
//*************************************************************************************************
// CCN - 4360204 - 24/09/2004 - Se agrega metodo de get y serVer0051 para que la txn 0051 funcione correctamente
//*************************************************************************************************

package ventanilla.com.bital.sfb;

public class Transferencia extends Header
{
  private String txtEffDate = "";
  private String txtAcctNo = "";
  private String txtAcctNo1 = "";
  private String txtAcctNo2 = "";
  private String txtTranAmt = "";
  private String txtTranCur = "";
  private String txtTranDesc = "";
  private String txtMoamoun = "";
  private String txtDescRev = "";
  private String txtVer0051 = "";

  public String getEffDate()
  {
	return txtEffDate + "*";
  }    

  public String getAcctNo()
  {
	return txtAcctNo + "*";
  }
  
  public String getAcctNo1()
  {
	return txtAcctNo1 + "*";
  }

  public String getAcctNo2()
  {
	return txtAcctNo2 + "*";
  }

  public String getTranAmt()
  {
	return txtTranAmt + "*";
  }    
  
  public String getTranCur()
  {
	return txtTranCur + "*";
  }    
  
  public String getTranDesc()
  {
	return txtTranDesc + "*";
  }    
  
  public String getDescRev()
  {
	return txtDescRev;
  }    
  
  public void setDescRev(String newDescRev)
  {
	txtDescRev = newDescRev;
  }
  
  public void setEffDate(String newEffDate)
  {
	txtEffDate = newEffDate;
  }
  
  public void setAcctNo(String newAcctNo)
  {
	txtAcctNo = newAcctNo;
  }  
  
  public void setAcctNo1(String newAcctNo1)
  {
	txtAcctNo1 = newAcctNo1;
  }  

  public void setAcctNo2(String newAcctNo2)
  {
	txtAcctNo2 = newAcctNo2;
  }  

  public void setTranAmt(String newTranAmt)
  {
	txtTranAmt = newTranAmt;
  }
  
  public void setTranCur(String newTranCur)
  {
	txtTranCur = newTranCur;
  }
  
  public void setTranDesc(String newTranDesc)
  {
	txtTranDesc = newTranDesc;
  }
  
  public String getMoamoun()
  {
	return txtMoamoun + "*";
  }
  public void setMoamoun(String newMoamoun)
  {
	txtMoamoun = newMoamoun;
  }
  
  public String getVer0051()
  {
	return txtVer0051 + "*";
  }
  public void setVer0051(String newVer0051)
  {
	txtVer0051 = newVer0051;
  }

  public String toString()
  {
	return super.toString() + getEffDate() + getAcctNo() + "*"
	  + getTranAmt() + getTranCur() + "*****" + getMoamoun() + "*" + getTranDesc()
	  + "*****" + getAcctNo1() + "******" + getAcctNo2()
	  + "*************"+getVer0051()+"**********";
  }    
}
