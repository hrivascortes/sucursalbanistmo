//************************************************************************************************* 
//             Funcion: Bean para WU Pago
//            Elemento: WUPago.java
//          Creado por: Alejandro Gonzalez Castro
//      Modificado por: Rutilo Zarco Reyes
//*************************************************************************************************
// CCN - 4360189 - 03/09/2004 - Se habilita WU
//*************************************************************************************************

package ventanilla.com.bital.sfb;

public class WUPago extends Header3 
{
    private String txtPaginacion    = "";
    private String txtDBkey         = "";
    private String txtMonto         = "";
    private String txtAcct          = "";

    public String getPaginacion() 
    {
        return txtPaginacion;
    }
    public void setPaginacion(String newPaginacion) 
    {
        txtPaginacion = newPaginacion;
    }

    public String getDBkey() 
    {
        return Filler(txtDBkey,10, "0", "L");
    }
    public void setDBkey(String newDBkey) 
    {
        txtDBkey = newDBkey;
    }

    public String getMonto() 
    {
        return Filler(txtMonto,10, "0", "L");
    }
    public void setMonto(String newMonto) 
    {
        txtMonto = newMonto;
    }
    
    public String getAcct() 
    {
        return Filler(txtAcct,10, "0", "L");
    }
    public void setAcct(String newAcct) 
    {
        txtAcct = newAcct;
    }

    public String Filler(String cadena, int largo, String filler, String side)
    {
        int cadlong = cadena.length();
        for(int i=cadlong; i<largo; i++)
        {
            if(side.equals("R"))
                cadena = cadena + filler;
            else
                cadena = filler + cadena;
        }
        return cadena;
    }
    
    public String toString() 
    {
        return super.toString() + getPaginacion() + getDBkey()  +   getMonto()  +   getAcct();
    }
}
