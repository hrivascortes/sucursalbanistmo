//*************************************************************************************************
//             Funcion: Bean Txn 4193 SOLICTUD DE CHEQUES DE GERENCIA 
//             Elaborado por: YGX
//*************************************************************************************************

package ventanilla.com.bital.sfb;


import ventanilla.GenericClasses;
import com.bital.util.ParameterNotFoundException;
import com.bital.util.ParameterParser;


public class Txn_0136 extends Transaction
{		 
	public Header doAction(ParameterParser param)
		throws ParameterNotFoundException
	{				
        
        GenericClasses gc = new GenericClasses();   
        FormatoH formatoh = new FormatoH();         				
		ventanilla.com.bital.sfb.Cheques Cheques = new ventanilla.com.bital.sfb.Cheques();
		
		formatoh.setTxnId("ODPA");
		formatoh.setTxnCode("0136");
	    formatoh.setFormat("H");  	    
	    	    
	    //1*Cuenta de Cheques de Gerencia
	    formatoh.setAcctNo("0103000023");
	    
	    //formatoh.setNatCurr("");
	    formatoh.setTranDesc(param.getString("txtBeneficiario").trim());
	    
	    //1*Cuenta de Cuenta informativa se env�a la cta del cargo
	    formatoh.setAcctDeb("0103000023");	    
	    
	    formatoh.setTranAmt(gc.delCommaPointFromString(param.getString("txtMonto")).trim());
	    
	    int moneda = param.getInt("moneda"); 
	    formatoh.setTranCur(gc.getDivisa(param.getString("moneda")));
	    formatoh.setCashIn("000");
	    formatoh.setCashOut("");
	    formatoh.setDraftAm("");
	    formatoh.setFees("");
	    formatoh.setDescRev("REV SOLICITUD CG");    
    
    	return formatoh;
	}
}
