package ventanilla.com.bital.util;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class MD5digest
{
  private MessageDigest __md5;
  private StringBuffer __digestBuffer;

  public MD5digest() throws NoSuchAlgorithmException {
    __md5 = MessageDigest.getInstance("MD5");
    __digestBuffer = new StringBuffer();
  }

  public String md5crypt(String password) throws Exception {
    int index;
    byte[] digest;
    __digestBuffer.setLength(0);
    digest = __md5.digest(password.getBytes("ISO-8859-1"));

    for(index = 0; index < digest.length; ++index)
    {
    	String tmp = Integer.toHexString(digest[index] & 0xff);
    	if (tmp.length() < 2)
    	{tmp = "0" + tmp;}
      __digestBuffer.append(tmp);
      }
//System.out.println("MD5 :[" + __digestBuffer.toString()+"]");
    return __digestBuffer.toString();
  }
}
