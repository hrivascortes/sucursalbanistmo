//*************************************************************************************************
//             Funcion: Bean Txn 0772
//          Creado por: Liliana Neyra Salazar
//*************************************************************************************************
// CCN - 4360602 - 18/05/2007 - Se realizan modificaciones para proyecto Codigo de Barras SUAS
//*************************************************************************************************
package ventanilla.com.bital.sfb;

import com.bital.util.ParameterParser;
import com.bital.util.ParameterNotFoundException;
import ventanilla.GenericClasses;

public class Txn_0772 extends Transaction
{
	public Header doAction(ParameterParser param)
		throws ParameterNotFoundException
	{
		FormatoB formatob = new FormatoB();
		GenericClasses gc = new GenericClasses();
		String Monto = "";
		
		formatob.setTxnCode("0772");
		formatob.setFormat("B");
		
		formatob.setAcctNo(param.getString("txtServicio"));
		String opcion = param.getString("opcionSuas");
		if(opcion.equals("1")){
			Monto = gc.quitap(param.getString("txtimporte"));
			formatob.setTranAmt(Monto);
			formatob.setReferenc(param.getString("txtnumctrl"));
		}
		else
		{
			String ref = param.getString("txtRFCSUA").substring(0,11)+param.getString("txtPagoPeriodo") + param.getString("idpagosSuas");
			formatob.setReferenc(ref);
			formatob.setFeeAmoun(param.getString("txtnumctrl"));
			Monto = gc.quitap(param.getString("txtTotalSUAS"));
			formatob.setTranAmt(Monto);
		}
			
		String tipo = param.getString("txttipo");
		if (tipo.equals("E"))
		  {formatob.setCashIn(Monto);}
		else
		  {formatob.setCashIn("000");}
		if (tipo.equals("C"))
		  formatob.setIntWh(param.getString("txtDDACuenta"));
		else if(opcion.equals("1"))
		  formatob.setIntWh("0000000000");
			
		String moneda = gc.getDivisa(param.getString("moneda"));
		formatob.setFromCurr(moneda);

		return formatob;
	}
}
