package ventanilla.com.bital.sfb;

public class Vouchers extends Header
{
  private String txtEffDate  = "";
  private String txtAcctNo   = ""; 
  private String txtTranAmt  = ""; 
  private String txtReferenc = ""; 
  private String txtFeeAmoun = ""; 
  private String txtIntern   = "";
  private String txtIntWh    = "";
  private String txtCashIn   = "";
  private String txtCashOut  = "";
  private String txtFromCurr = "";
  private String txtToCurr   = "";

  public String getEffDate()
  {
	return txtEffDate + "*";
  }
  public void setEffDate(String newEffDate)
  {
	txtEffDate = newEffDate;
  }
  
  public String getAcctNo()
  {
	return txtAcctNo + "*";
  }
  public void setAcctNo(String newAcctNo)
  {
	txtAcctNo = newAcctNo;
  }

  public String getTranAmt()
  {
	return txtTranAmt + "*";
  }
  public void setTranAmt(String newTranAmt)
  {
	txtTranAmt = newTranAmt;
  }
  
  public String getReferenc()
  {
	return txtReferenc + "*";
  }
  public void setReferenc(String newReferenc)
  {
	txtReferenc = newReferenc;
  }
  
  public String getFeeAmoun()
  {
	return txtFeeAmoun + "*";
  }
  public void setFeeAmoun(String newFeeAmoun)
  {
	txtFeeAmoun = newFeeAmoun;
  }
  
  public String getIntern()
  {
	return txtIntern + "*";
  }
  public void setIntern(String newIntern)
  {
	txtIntern = newIntern;
  }
  
  public String getIntWh()
  {
	return txtIntWh + "*";
  }
  public void setIntWh(String newIntWh)
  {
	txtIntWh = newIntWh;
  }

  public String getCashIn()
  {
	return txtCashIn + "*";
  }
  public void setCashIn(String newCashIn)
  {
	txtCashIn = newCashIn;
  }
  
  public String getCashOut()
  {
	return txtCashOut + "*";
  }
  public void setCashOut(String newCashOut)
  {
	txtCashOut = newCashOut;
  }
  
  public String getFromCurr()
  {
	return txtFromCurr + "*";
  }
  public void setFromCurr(String newFromCurr)
  {
	txtFromCurr = newFromCurr;
  }
  
  public String getToCurr()
  {
	return txtToCurr + "*";
  }
  public void setToCurr(String newToCurr)
  {
	txtToCurr = newToCurr;
  }
  
  public String toString()
  {
	return super.toString()
	+ getEffDate()	+ getAcctNo()	+ getTranAmt()
	+ getReferenc()	+ getFeeAmoun()	+ getIntern()	+ getIntWh()
	+ getCashIn()	+ getCashOut()	+ getFromCurr()	+ getToCurr();
  }    
}
