//*************************************************************************************************
//             Funcion: Bean Txn 5687
//          Creado por: Fausto Rodrigo Flores Moreno
//  Utilizada para el abono de un recargo para RAP calculadora
//*************************************************************************************************
// CCN - 4360574 - 09/03/2007 - Se crea bean abono de recargo en RAP Calculadora
//*************************************************************************************************
package ventanilla.com.bital.sfb;

import com.bital.util.ParameterParser;
import com.bital.util.ParameterNotFoundException;
import ventanilla.GenericClasses;

public class Txn_5687 extends Transaction
{
	public Header doAction(ParameterParser param)
		throws ParameterNotFoundException
	{
		FormatoA formatoa = new FormatoA();
		GenericClasses gc = new GenericClasses();
		String mtoDescRec = "";
		long mtoOrg = 0;
		long mtoefe = 0;
		if(!param.getString("montos1p1").equals(""))
			mtoOrg = Long.parseLong(gc.quitap(param.getString("montos1p1")));
		
		if(!param.getString("txtEfectivo").equals(""))
			mtoefe = Long.parseLong(gc.quitap(param.getString("txtEfectivo")));
			
		long mtotem = 0;
		formatoa.setTxnCode("5687");
		formatoa.setAcctNo(param.getString("servicio1"));
		
		formatoa.setTranAmt(param.getString("MtoDes_Rec"));
		String moneda = gc.getDivisa(param.getString("moneda"));
		formatoa.setTranCur(moneda);
		formatoa.setTrNo("9");
		formatoa.setTranDesc(param.getString("ref1s1p1"));
		
		if (mtoefe > mtoOrg){
        	mtotem = mtoefe - mtoOrg;
        	mtoOrg = 0;
        }else{
        	mtoOrg = mtoOrg - mtoefe;
        	mtotem = 0;
		}
            
        if(mtotem > 0){
			formatoa.setCashIn(new Long(mtotem).toString());
        }
        if(mtoOrg > 0){
        	formatoa.setCashIn("000");
        }

 		if(param.getString("RapCCheque").equals("S")){
 			formatoa.setCheckNo("000"+param.getString("txtSerial2"));//serial del cheque 			
 		}
		return formatoa;
	}
}