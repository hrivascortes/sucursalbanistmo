// Transaccion 4011 para 0061 - Cheques de Caja
package ventanilla.com.bital.sfb;

import com.bital.util.ParameterParser;
import com.bital.util.ParameterNotFoundException;
import ventanilla.GenericClasses;

public class Txn_2199 extends Transaction
{
  public Header doAction(ParameterParser param)
    throws ParameterNotFoundException
  {
    
  FormatoA formatoa = new FormatoA();
	GenericClasses gc = new GenericClasses();
	formatoa.setTxnCode("2199");
	
	formatoa.setAcctNo(param.getString("txtDDACuenta"));
	formatoa.setTranAmt(param.getCString("txtMonto"));
	
	String moneda = gc.getDivisa(param.getString("moneda"));
	formatoa.setTranCur(moneda);
	
  String ceros="";
	if(moneda.equals("N$"))
		//formatoa.setTranDesc(param.getString("lstBanCorresp1")+gc.StringFiller( ceros,12-param.getString("lstBanCorresp1").length(),false," ")+"USD000000000       00");
		formatoa.setTranDesc(param.getString("lstBanCorresp1") + gc.StringFiller(ceros, 12-param.getString("lstBanCorresp1").length(),false," ") + "USD" + param.getString("txtRefTIRE"));
	else
		//formatoa.setTranDesc(param.getString("lstBanCorresp1")+gc.StringFiller( ceros,12-param.getString("lstBanCorresp1").length(),false," ")+moneda+"000000000       00");
		formatoa.setTranDesc(param.getString("lstBanCorresp1") + gc.StringFiller(ceros, 12-param.getString("lstBanCorresp1").length(),false," ") + moneda + param.getString("txtRefTIRE"));
	    
    //System.out.println(formatoa);
    return formatoa;
  }
}