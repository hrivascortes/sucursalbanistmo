//*************************************************************************************************
//             Funcion: Clase que establece los datos necesarios para la afectacion del Diario
//            Elemento: Diario.java
//          Creado por: Alejandro Gonzalez Castro.
//          Modificado por: Jesus Emmanuel Lopez Rosales
//*************************************************************************************************
// CCN - 4360291 - 18/03/2005 - Se unifican las tabas de TW_DIARIO Y TW_DIARIO_RESPUEST
// CCN - 4360296 - 23/03/2005 - Se cambia CCN por peticion de QA, CCN anterior 4360291
// CCN - 4360310 - 25/04/2005 - Se cambia la insercion de Int a Long para evitar problemas con Montos grandes
// CCN - 4360315 - 06/05/2005 - Se agrega armado de sql para rastrear problemas
// CCN - 4360334 - 17/06/2005 - Se elimina variable dbQualifier
// CCN - 4360336 - 17/06/2005 - Se cambia la obtenci�n de variables de ambiente
// CCN - 4360364 - 19/08/2005 - Se activa Control de Efectivo en D�lares
//				Se inserta en la tabla TW_CTRL_LOG para reportes de GTE
//				Se realiza modificacion a la txn 0730 servicio 304379 para no postearla cuando sea  monto 0
// CCN - 4360368 - 02/09/2005 - Se activa Mensaje para Efectivo.
// CCN - 4360374 - 08/09/2005 - Se genera nuevo paquete a peticion de QA.
// CCN - 4360395 - 21/10/2005 - Se genera funcion que para el mensaje de alerta de efectivo.
// CCN - 4360425 - 03/02/2006 - Proyecto Tarjeta de Identificacion TDI
// CCN - 4360456 - 07/04/2006 - Se elimna la validaci�n de CRM para sucursales.
// CCN - 4360474 - 05/05/2006 - Se cambia el num. de cta del servicio RAP en la txn 5361 por el num de servicio para mostrar en el diario.
// CCN - 4360493 - 23/06/2006 - Se realizan modificaciones para mostrar mensaje de alerta para txns que afectan efectivo >= 5000.00
// CCN - 4360533 - 20/10/2006 - Se realizan modificaciones para OPEE
// CCN - 4360542 - 10/11/2006 - Se realizan modificaciones para OPEE y txn 0821
// CCN - 4360551 - 22/12/2006 - Se realiza correccion para la impresion de la txn 0112-4111 en pesos
// CCN - 4360576 - 09/03/2007 - Modificaciones para proyecto OPMN
// CCN - 4360584 - 23/03/2007 - Modificaciones para proyecto OPMN
// CCN - 4360587 - 03/04/2007 - Modificaciones para Monitoreo de Efectivo en sucursales
// CCN - 4360607 - 08/05/2007 - Modificaciones para control de firmas 
// CCN - 4620021 - 17/10/2007 - Nuevo esquema para funciones especiales Humberto Balleza
// CO - CR7564 - 26/01/2011 - Cambios Proyecto camp
//*************************************************************************************************

package ventanilla.com.bital.admin;

import java.lang.reflect.Method;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.Vector;
import java.util.Hashtable;
import java.util.Enumeration;
import java.util.StringTokenizer;

import com.bital.util.ConnectionPoolingManager;
import javax.servlet.http.HttpSession;

import ventanilla.GenericClasses;
import ventanilla.com.bital.beans.DataDiario;
import ventanilla.com.bital.beans.DatosFuncionesSP;
import ventanilla.com.bital.beans.DatosVarios;
import ventanilla.com.bital.beans.SpecialFunc;
import ventanilla.com.bital.sfb.Txn_0821;
import ventanilla.com.bital.util.PostingCAPR;

public class Diario {
	private String txn = "";
	private String iTxn = "";
	private Object txnBean = null;
	private String ambiente = null;
	private String sql = "";
	private String consecutivoDiario = "";
	private Hashtable datosDiario = null;
	private Vector metodosGet = new Vector();
	private long monto = 0;
	private String totA = null;
	private String totC = null;
	private GenericClasses gc = new GenericClasses();
	String statementPS = com.bital.util.ConnectionPoolingManager
			.getStatementPostFix();
	private HttpSession session;
	private String dirIP = null;
	private String cajero = null;
	private String supervisor = null;
	String transaccion = null;

	private String specialKey = "";

	public void setSesion(HttpSession sesion) {

		session = sesion;
	}

	public HttpSession getSesion() {

		return session;
	}

	public void setDirIP(String iP) {

		dirIP = iP;
	}

	public String getDirIp() {

		return dirIP;
	}

	public void setCajero(String caj) {

		cajero = caj;
	}

	public String getCajero() {

		return cajero;
	}

	public void setSupervisor(String sup) {

		supervisor = sup;
	}

	public String getSupervisor() {

		return supervisor;
	}

	public void setFuncEsp(String key) {
		specialKey = key;
	}

	public String getFuncEsp() {
		return specialKey;
	}

	public String invokeDiario(Object Bean, String ConLiga, String DirIP,
			String Moneda, HttpSession session) {

		DatosFuncionesSP dSP = DatosFuncionesSP.getInstance();
		Vector vSP = new Vector();
		int messagecode = 0;
		int codigo = 0;
		String Afecta_Diario = "";
		Vector VResp = new Vector();
		javax.naming.Context initialContext = null;
		setSesion(session);
		String funcEspAP = "NA";
		String funcEspBP = "NA";

		try {

			initialContext = new javax.naming.InitialContext();
			ambiente = (String) initialContext.lookup("java:comp/env/AMB");
		}

		catch (javax.naming.NamingException e) {

			System.out.println("Diario::Lookup::java:comp/env/AMB");
			System.out.println(e);
		}

		Moneda = gc.getDivisa(Moneda);

		txnBean = Bean;
		metodosGet = getMethods(Bean);
		txn = getBeanData("getTxnCode");

		datosDiario = getDatosDiario(txn, Moneda);

		if (datosDiario.isEmpty()) {
			messagecode = 5;
		}

		if (messagecode == 0) {
			funcEspBP = ((String) datosDiario.get("DEBP")).trim();
			funcEspAP = ((String) datosDiario.get("DEAP")).trim();
		}

		String connureg = setDiarioConsecutivo();

		if (!funcEspBP.equals("NA")) {
			session.setAttribute("EstadoTxn", "DEBP");
			messagecode = SpecialFunc("DEBP", funcEspBP, VResp, connureg);
		}

		if (messagecode == 0) {

			Afecta_Diario = (String) datosDiario.get("AfectaD");
			int result = 0;
			if (Afecta_Diario.equals("S")) {
				datosDiario.remove("AfectaD");
				result = InsertDiario(datosDiario, ConLiga, DirIP);

				if (txn.equals("4193") || txn.equals("0270")
						|| txn.equals("4019") || txn.equals("1003")
						|| txn.equals("CAPR")) {
					connureg = (String) this.getDiarioConsecutivo();
					// System.out.println("Imprime el connureg"+connureg);
					session.setAttribute("page.diario_consecutivo", connureg);
				}
			}

			if (result != 1) {
				messagecode = 6;
			}
		}

		if (messagecode == 0) {

			VResp = TxnPosting();

			String code = (String) VResp.get(0);
			try {
				codigo = Integer.parseInt(code);
			} catch (NumberFormatException nfe) {
				System.out.println("Error Diario::TxnPosting [" + nfe + "]");
				messagecode = 7;
			}

		}

		if (messagecode == 0 && codigo == 0) {
			if (!funcEspAP.equals("NA")) {
				session.setAttribute("EstadoTxn", "DEAP");
				messagecode = SpecialFunc("DEAP", funcEspAP, VResp, connureg);
			}
		}

		if (messagecode == 0) {

			if (codigo == 0) {
				// Afectacion de Totales para Control de Efectivo Pesos
				long efe = Long.parseLong(session.getAttribute("efectivo")
						.toString());
				long efe2 = Long.parseLong(session.getAttribute("efectivo2")
						.toString());
				if (Moneda.equals("N$") && efe > 0) {
					DoTotales(datosDiario, session, Moneda);
				} else if (Moneda.equals("US$") && efe2 > 0) {
					DoTotales(datosDiario, session, Moneda);
				}
				AfectaEfectivo(session);
			}

			if (codigo >= 0 || codigo <= 3) {
				if (Afecta_Diario.equals("S")) {
					int res = UpdateDiario(VResp, txn);
					if (res != 1)
						messagecode = 9;
					else if (VerificaAuthoriz(codigo)) {
						res = InsertLog();
						if (res != 1) {
							messagecode = 10;
						}
					}
				}
			}
		}

		if (messagecode == 0 && codigo == 0) { // Verifica si la txn tiene CRM
			String CRM = (String) session.getAttribute("CRMActive");
			String iTxn = (String) session.getAttribute("page.iTxn");
			if (iTxn != null) {
				DatosVarios crmrt = DatosVarios.getInstance();
				Hashtable txnscrm = crmrt.getDatosH("CRMRTV");
				Vector DatosTxn = (Vector) txnscrm.get(iTxn);
				if (DatosTxn != null) {
					String txnCRM = (String) DatosTxn.get(0);
					if (txnCRM.equals(txn)) {
						session.setAttribute("CRM", "S");
						session.setAttribute("CRMTxn", iTxn);
						session.setAttribute("CRMQualifier", ambiente);
						String metodo = (String) DatosTxn.get(1);
						String cta = getBeanData(metodo);
						session.setAttribute("CRMcta", cta);
					}
				}
			}
		}

		String message = getMessage(messagecode);
		return message;
	}

	public String getMessage(int code) {
		String respuesta = "";
		switch (code) {
		case 5:
			respuesta = "1~01~0101010~TRANSACCION NO EXISTE EN TABLA TC_DATOS_DIARIO";
			break;
		case 6:
			respuesta = "1~01~0101010~ERROR AL INTENTAR INSERTAR EN EL DIARIO ELECTRONICO";
			break;
		case 7:
			respuesta = "1~01~0101010~ERROR DE ENLACE A LA APLICACION CICS~";
			break;
		case 9:
			respuesta = "1~01~0101010~ERROR AL INTENTAR ACTUALIZAR EN EL DIARIO ELECTRONICO";
			break;
		case 10:
			respuesta = "1~01~0101010~ERROR AL INTENTAR ACTUALIZAR REGISTRO EN EL CONTROL DE REVERSOS";
			break;
		case 0:
			respuesta = getBeanData("getMessage");
			break;
		}
		return respuesta;
	}

	public int UpdateDiario(Vector VRespuesta, String Txn)

	{
		int coderesp = Integer.parseInt((String) VRespuesta.get(0));
		String consec = (String) VRespuesta.get(2);
		String respuest = (String) VRespuesta.get(3);
		String status = "";
		String serv5361 = (String) session.getAttribute("numServ5361");
		if (serv5361 == null) {
			serv5361 = "";
		}
		if (coderesp == 0) {
			status = "A";
		} else {
			status = "R";
		}

		Connection pooledConnection = ConnectionPoolingManager
				.getPooledConnection();
		PreparedStatement update = null;
		int ResultUpdate = 0;

		try {
			if (pooledConnection != null) // TRES SQL's necesarios para evitar
											// borrar datos de otras txns
											// verificar txn 1543 y 0520

			{
				if (Txn.equals("DSLZ")
						&& getBeanData("getProcessCode").equals("EXP ")) {
					update = pooledConnection
							.prepareStatement("UPDATE TW_DIARIO_ELECTRON SET D_ESTATUS = ?, D_CONSECUTIVO = ?, D_RESPUESTA = ?, D_DESCRIP = ? WHERE D_CONNUREG = ?"
									+ statementPS);
					update.setString(1, status);
					update.setString(2, consec);
					update.setString(3, respuest.trim());

					update.setString(4, getBeanData("getOrden"));
					update.setString(5, getDiarioConsecutivo());
					sql = "UPDATE TW_DIARIO_ELECTRON SET D_ESTATUS ='" + status
							+ "', D_CONSECUTIVO = '" + consec
							+ "', D_RESPUESTA = '" + respuest.trim()
							+ "', D_DESCRIP ='" + getBeanData("getOrden")
							+ " WHERE D_CONNUREG = '" + getDiarioConsecutivo()
							+ "'" + statementPS;
				} else if (Txn.equals("0820") && coderesp == 0) {
					String msg = (String) VRespuesta.get(3);
					int size = msg.trim().length();
					String rfc = msg.substring(size - 22, size - 9);
					String fecha = msg.substring(size - 8, size);

					update = pooledConnection
							.prepareStatement("UPDATE TW_DIARIO_ELECTRON SET D_ESTATUS = ?, D_CONSECUTIVO = ?, D_RESPUESTA = ?,  D_REFER2 = ?, D_REFER3 = ? WHERE D_CONNUREG = ?"
									+ statementPS);
					update.setString(1, status);
					update.setString(2, consec);
					update.setString(3, respuest.trim());
					update.setString(4, rfc.trim());
					update.setString(5, fecha.trim());
					update.setString(6, getDiarioConsecutivo());
					sql = "UPDATE TW_DIARIO_ELECTRON SET D_ESTATUS ='" + status
							+ "', D_CONSECUTIVO = '" + consec
							+ "', D_RESPUESTA = '" + respuest.trim()
							+ "', D_REFER2 ='" + rfc.trim() + " , D_REFER3 ='"
							+ fecha.trim() + "' WHERE D_CONNUREG = '"
							+ getDiarioConsecutivo() + "'" + statementPS;
				} else if (Txn.equals("1543") || Txn.equals("0520")) {
					update = pooledConnection
							.prepareStatement("UPDATE TW_DIARIO_ELECTRON SET D_ESTATUS = ?, D_CONSECUTIVO = ?, D_RESPUESTA = ?, D_MONTO = 0 WHERE D_CONNUREG = ?"
									+ statementPS);
					update.setString(1, status);
					update.setString(2, consec);
					update.setString(3, respuest.trim());
					update.setString(4, getDiarioConsecutivo());
					sql = "UPDATE TW_DIARIO_ELECTRON SET D_ESTATUS ='" + status
							+ "', D_CONSECUTIVO = '" + consec
							+ "', D_RESPUESTA = '" + respuest.trim()
							+ "D_MONTO = 0 WHERE D_CONNUREG = '"
							+ getDiarioConsecutivo() + "'" + statementPS;
				} else if (Txn.equals("5361")) {
					update = pooledConnection
							.prepareStatement("UPDATE TW_DIARIO_ELECTRON SET D_CUENTA = ?, D_ESTATUS = ?, D_CONSECUTIVO = ?, D_RESPUESTA = ? WHERE D_CONNUREG = ?"
									+ statementPS);
					update.setString(1, serv5361);
					update.setString(2, status);
					update.setString(3, consec);
					update.setString(4, respuest.trim());
					update.setString(5, getDiarioConsecutivo());
					sql = "UPDATE TW_DIARIO_ELECTRON SET D_CUENTA ='"
							+ serv5361 + "', D_ESTATUS ='" + status
							+ "', D_CONSECUTIVO = '" + consec
							+ "', D_RESPUESTA = '" + respuest.trim()
							+ " WHERE D_CONNUREG = '" + getDiarioConsecutivo()
							+ "'" + statementPS;
				} else {
					update = pooledConnection
							.prepareStatement("UPDATE TW_DIARIO_ELECTRON SET D_ESTATUS = ?, D_CONSECUTIVO = ?, D_RESPUESTA = ? WHERE D_CONNUREG = ?"
									+ statementPS);
					update.setString(1, status);
					update.setString(2, consec);
					update.setString(3, respuest.trim());
					update.setString(4, getDiarioConsecutivo());
					sql = "UPDATE TW_DIARIO_ELECTRON SET D_ESTATUS ='" + status
							+ "', D_CONSECUTIVO = '" + consec
							+ "', D_RESPUESTA = '" + respuest.trim()
							+ " WHERE D_CONNUREG = '" + getDiarioConsecutivo()
							+ "'" + statementPS;
				}

				ResultUpdate = update.executeUpdate();
				if (ResultUpdate > 1) {
					System.out.println("Diario::UpdateDiario::ResUpdate>1 ["
							+ ResultUpdate + "]");
					System.out.println("Diario::UpdateDiario::SQL [" + sql
							+ "]");
				}
			}
		} catch (SQLException sqlException) {
			System.out.println("Error Diario::UpdateDiario [" + sqlException
					+ "]");
			System.out.println("Error Diario::UpdateDiario::SQL [" + sql + "]");
		}

		finally {
			try {
				if (update != null) {
					update.close();
					update = null;
				}
				if (pooledConnection != null) {
					pooledConnection.close();
					pooledConnection = null;
				}
			} catch (java.sql.SQLException sqlException)

			{
				System.out.println("Error FINALLY Diario::UpdateDiario ["
						+ sqlException + "]");
				System.out.println("Error FINALLY Diario::UpdateDiario::SQL ["
						+ sql + "]");
			}
		}
		return ResultUpdate;
	}

	public boolean VerificaAuthoriz(int codigo) {
		boolean band = false;
		if (codigo == 0) {
			String Cajero = getBeanData("getTeller");
			String Supervisor = getBeanData("getSupervisor");
			if (!Cajero.equals(Supervisor)) {
				this.setCajero(Cajero);
				this.setSupervisor(Supervisor);
				band = true;
			}

		}
		return band;
	}

	public int InsertLog() {
		Connection pooledConnection = ConnectionPoolingManager
				.getPooledConnection();
		PreparedStatement select = null;
		ResultSet Rselect = null;
		Hashtable UserInfo = (Hashtable) getSesion().getAttribute("userinfo");
		Hashtable supervisores = (Hashtable) getSesion().getAttribute(
				"user.par");
		String indice = (String) getSesion().getAttribute("autho.indice");
		String Nom_Sup = (String) supervisores.get("D_NOMBRE" + indice);
		String RACF_Sup = (String) supervisores.get("D_EJCT_CTA" + indice);
		String IP_Sup = (String) supervisores.get("D_DIRECCION_IP" + indice);
		String Sucursal = (String) getSesion().getAttribute("branch");

		PreparedStatement update = null;

		int ResultUpdate = 0;
		pooledConnection = ConnectionPoolingManager.getPooledConnection();

		sql = "INSERT INTO TW_CTRL_LOG VALUES('" + Sucursal + "','"
				+ this.getCajero() + "','" + this.getDiarioConsecutivo()
				+ "','" + (String) UserInfo.get("D_NOMBRE") + "','"
				+ (String) UserInfo.get("D_EJCT_CTA") + "','"
				+ (String) UserInfo.get("D_DIRECCION_IP") + "','" + Nom_Sup
				+ "','" + RACF_Sup + "','" + IP_Sup + ",'A','"
				+ this.getDiarioConsecutivo().substring(8, 14) + "')";

		try {
			if (pooledConnection != null) {
				update = pooledConnection
						.prepareStatement("INSERT INTO TW_CTRL_LOG VALUES(?,?,?,?,?,?,?,?,?,?,?)");
				update.setString(1, Sucursal);
				update.setString(2, this.getCajero());
				update.setString(3, this.getDiarioConsecutivo());
				update.setString(4, (String) UserInfo.get("D_NOMBRE"));
				update.setString(5, (String) UserInfo.get("D_EJCT_CTA"));
				update.setString(6, (String) UserInfo.get("D_DIRECCION_IP"));
				update.setString(7, Nom_Sup);
				update.setString(8, RACF_Sup);
				update.setString(9, IP_Sup);
				update.setString(10, "A");
				update.setString(11,
						this.getDiarioConsecutivo().substring(8, 14));
				ResultUpdate = update.executeUpdate();

				if (ResultUpdate > 1) {
					System.out.println("Diario::InsertLog::ResUpdate>1 ["
							+ ResultUpdate + "]");
					System.out.println("Diario::InsertLog::SQL [" + sql + "]");
				}
			}
		}

		catch (SQLException sqlException) {

			System.out
					.println("Error Diario::InsertLog [" + sqlException + "]");
			System.out.println("Error Diario::InsertLog::SQL [" + sql + "]");
		}

		finally {
			try {
				if (update != null) {
					update.close();
					update = null;
				}
				if (pooledConnection != null) {
					pooledConnection.close();
					pooledConnection = null;
				}
			} catch (java.sql.SQLException sqlException)

			{
				System.out.println("Error FINALLY Diario::InsertLog ["
						+ sqlException + "]");
				System.out.println("Error FINALLY Diario::InsertLog::SQL ["
						+ sql + "]");

			}
		}
		return ResultUpdate;
	}

	public String setDiarioConsecutivo() {

		String fecha = gc.getDate(6);
		String hora = gc.getDate(7);
		String teller = getBeanData("getTeller");
		consecutivoDiario = teller + fecha + hora;

		// System.out.println("DiarioElectronico::Consecutivo Generado [" +
		// ConsecutivoDiario + "]");
		return consecutivoDiario;
	}

	public String getDiarioConsecutivo() {
		return consecutivoDiario;
	}

	public Vector getLinesResp(String lineas, int nolineas) {
		Vector Lineas = new Vector();
		for (int i = 0; i < nolineas; i++) {
			Lineas.add(lineas.substring(i * 78,
					Math.min(i * 78 + 77, lineas.length())));
			if (Math.min(i * 78 + 77, lineas.length()) == lineas.length()) {
				break;
			}
		}
		return Lineas;
	}

	public Vector TxnPosting() {

		String execute = null;
		String respuesta = null;
		transaccion = getBeanData("getTxnCode");

		if (transaccion.equals("0728")) {

			setBeanData("setIntern", ""); // Loch Ness

		}
		if (transaccion.equals("0510") && false)

		{

			setBeanData("setIntern", "");
			setBeanData("setIntWh", "");
		}

		if (transaccion.equals("0587")) {
			setBeanData("setInter", "");
		}

		if (transaccion.equals("5103"))

		{
			setBeanData("setReferenc1", "");
		}

		if (transaccion.equals("5503")) {
			setBeanData("setFiller", ""); // Loch Ness
			String servicio = getBeanData("getAcctNo");
			String mto = getBeanData("getTranAmt");
			if ((servicio.equals("448") || servicio.equals("66"))
					&& mto.equals("000")) {
				respuesta = "0~01~0" + gc.getDate(3)
						+ "~*+ TRANSACCION ACEPTADA +*";
				setBeanData("setMessage", respuesta);
			} else {
				execute = getBeanData("execute");
				respuesta = getBeanData("getMessage");
			}

		} else if (transaccion.equals("CON "))

		{
			respuesta = "0~01~0" + gc.getDate(3)
					+ "~*+ TRANSACCION ACEPTADA +*";
			setBeanData("setMessage", respuesta);
		} else if (transaccion.equals("RECG"))

		{
			respuesta = "0~01~0" + gc.getDate(3)
					+ "~*+ TRANSACCION ACEPTADA +*";
			setBeanData("setMessage", respuesta);
		} else if (transaccion.equals("FIRM"))

		{
			respuesta = "0~01~0" + gc.getDate(3)
					+ "~*+ TRANSACCION ACEPTADA +*";
			setBeanData("setMessage", respuesta);
		}

		else if (transaccion.equals("0730"))

		{
			setBeanData("setIntern", ""); // Loch Ness
			String servicio = getBeanData("getAcctNo");
			String mto = getBeanData("getTranAmt");
			if (servicio.equals("304379") && mto.equals("000")) {
				respuesta = "0~03~0" + gc.getDate(3) + "~CONSECUTIVO:  0"
						+ gc.getDate(3)
						+ "    0         *+ TRANSACCION  ACEPTADA  +*~";
				setBeanData("setMessage", respuesta);
			} else {
				execute = getBeanData("execute");
				respuesta = getBeanData("getMessage");

			}
		} else if (0 == transaccion.trim().compareTo("CAPR")) {

            // MOD_POLAS.- Se agrego este IF para la txn CAPR que hace la
            // consulta de la REF al AdmonWEB
            PostingCAPR postingCapr = new PostingCAPR();
            int codePolas = postingCapr.excutePosting(txnBean, session,
                    getDiarioConsecutivo());
            StringBuffer sbRespuesta = new StringBuffer(200);
            
            if (codePolas == 0) {
                sbRespuesta.append("0~03~0")
                .append(gc.getDate(3))
                .append("~CONSECUTIVO:  0")
                .append(gc.getDate(3))
                .append("    0         *+ TRANSACCION  ACEPTADA  +*")
                .append("   MONTO:          ")
                .append(session.getAttribute("mtoSP"))
                .append("~");
            } else {
            	sbRespuesta.append("1~01~0101010~")
            	.append(session.getAttribute("mensajeSP"));
            }

            respuesta = sbRespuesta.toString();
            setBeanData("setMessage", respuesta);
        } else {
			execute = getBeanData("execute");
			respuesta = getBeanData("getMessage");
		}

		// Limpia Ceros Binarios de la Respuesta de Hogan
		int y = 0;
		int count = 0;
		StringBuffer RespSB = new StringBuffer(respuesta);
		for (y = 0; y < RespSB.length(); y++)
			if (RespSB.charAt(y) == 0x00) {
				count++;
				RespSB.replace(y, y + 1, " ");
			}
		respuesta = RespSB.toString();

		if (count > 0) {
			setBeanData("setMessage", respuesta);
		}

		if (transaccion.equals("0080")) {

			respuesta = getBeanData("getMessage");
			respuesta = gc.ClearString(respuesta);
			setBeanData("setMessage", respuesta);
		}

		if (transaccion.equals("0825")) {

			if (respuesta.endsWith("ESTATUS INVALIDO                    ODS~")) {
				int inicio = respuesta
						.lastIndexOf("ESTATUS INVALIDO                    ODS~");
				respuesta = respuesta.substring(0, inicio);
				respuesta = respuesta
						+ "DOCUMENTO POSIBLEMENTE FALSO, NOTIFICAR AL GERENTE~";
			}
			setBeanData("setMessage", respuesta);
		}

		Vector VRespuesta = new Vector();

		if (transaccion.equals("DSLZ")) {
			String resp = null;
			// Armado de respuesta, codigo de terminacion, no lineas y
			// consecutivo
			resp = respuesta.substring(0, 1) + "~10" + "~0" + gc.getDate(3);
			// Armado de Vector
			VRespuesta.add(respuesta.substring(0, 1));
			VRespuesta.add("10");
			VRespuesta.add("0" + gc.getDate(3));

			String tipoDSLZ = getBeanData("getProcessCode");

			if (respuesta.startsWith("0")) {

				// TXN ACEPTADA
				String tmp = "CONSECUTIVO:  " + gc.getDate(3) + " "
						+ respuesta.substring(0);
				tmp = Filler(tmp, 78);

				String temp = null;
				String orden = respuesta.substring(3);

				if (tipoDSLZ.equals("EXP ")) {
					temp = getBeanData("getNomOrd") + " "
							+ getBeanData("getApeOrd");
					temp = Filler(temp, 78);
					tmp = tmp + temp;
					temp = getBeanData("getNomBen") + " "
							+ getBeanData("getApeBen");
					temp = Filler(temp, 78);
					tmp = tmp + temp;
					temp = getBeanData("getBanco");
					temp = Filler(temp, 78);
					tmp = tmp + temp;
					temp = "ORDEN : " + orden;
					temp = Filler(temp, 78);
					tmp = tmp + temp;
					setBeanData("setOrden", orden);
				}
				tmp = tmp + Filler("*+ TRANSACCION ACEPTADA POR MERVA +*", 78);

				if (tipoDSLZ.equals("INQ1") || tipoDSLZ.equals("LIB ")) {
					tmp = respuesta;
				}

				if (tipoDSLZ.equals("LIQ ")) {
					tmp = tmp + "ORDEN LIQUIDADA";
				}
				if (tipoDSLZ.equals("LSU ")) {
					tmp = tmp + "ORDEN SUSPENDIDA / LIQUIDADA";
				}
				if (tipoDSLZ.equals("CAN ")) {
					tmp = tmp + "ORDEN CANCELADA";
				}
				if (tipoDSLZ.equals("SUS ")) {
					tmp = tmp + "ORDEN SUSPENDIDA";
				}
				if (tipoDSLZ.equals("REA ")) {
					tmp = tmp + "ORDEN REACTIVADA";
				}

				VRespuesta.add(tmp);
				resp = resp + "~" + tmp + "~";
			}

			else {

				// TXN RECHAZADA
				VRespuesta.add(respuesta);
				resp = resp + "~" + respuesta + "~";
			}

			if (tipoDSLZ.equals("EXP ") || tipoDSLZ.equals("INQ1")
					|| tipoDSLZ.equals("LIB ") || tipoDSLZ.equals("LIQ ")
					|| tipoDSLZ.equals("CAN ") || tipoDSLZ.equals("SUS ")
					|| tipoDSLZ.equals("LSU ") || tipoDSLZ.equals("REA ")) {
				setBeanData("setMessage", resp);
			}

		} else {
			StringTokenizer STRespuesta = new StringTokenizer(respuesta, "~");
			while (STRespuesta.hasMoreTokens()) {
				String token = STRespuesta.nextToken();
				VRespuesta.add(token);
			}

		}

		if (VRespuesta.elementAt(3).toString().indexOf("CONSECUTIVO:") != -1) {
			enviaConsecutivo_Txns(transaccion,
					gc.getString(VRespuesta.elementAt(3).toString(), 1));
		}
		return VRespuesta;
	}

	public int InsertDiario(Hashtable Datos, String ConsecLiga, String IP) {

		String TotC = (String) Datos.get("TotC");
		String TotA = (String) Datos.get("TotA");

		setTotales(TotC, TotA);

		String Reversable = (String) Datos.get("Reversable");
		String Visible = (String) Datos.get("Visible");
		String txn = getBeanData("getTxnCode");
		String cents = "";

		if (txn.equals("0820")) {

			cents = getBeanData("getCashOut");
			setBeanData("setCashOut", "");
		}

		Datos.remove("TotC");
		Datos.remove("TotA");
		Datos.remove("Reversable");
		Datos.remove("Visible");
		Datos.remove("DEBP");
		Datos.remove("DEAP");

		DatosVarios TDiario = DatosVarios.getInstance();
		String txnRev = null;
		if (metodosGet.contains("getReversable")) {
			txnRev = getBeanData("getReversable");
		}

		if (txnRev != null && txnRev.length() > 0) {
			Reversable = txnRev;
		}

		Connection pooledConnection = ConnectionPoolingManager
				.getPooledConnection();
		PreparedStatement insert = null;
		int ResultUpdate = 0;
		sql = "INSERT INTO TW_DIARIO_ELECTRON (D_SUCURSAL,D_OPERADOR,D_CONNUREG,D_TXN,C_TXN_REVERSABLE,C_TXN_VISIBLE,D_SUPERVISOR,D_FECHOPER,D_CUENTA,D_SERIAL,D_REFER,D_REFER1,D_REFER2,D_REFER3,D_EFECTIVO,D_MONTO,D_FECHEFEC,D_DIVISA,D_DESCRIP,D_DESCRIP_REVERSO,D_ESTATUS,D_CONSECUTIVO,D_CONSECLIGA,D_CARGO,D_ABONO,D_MSGENVIO) VALUES(";
		Vector Valores = new Vector();
		try {
			if (pooledConnection != null) {
				Hashtable TablaDiario = TDiario.getTablaDiario();
				insert = pooledConnection
						.prepareStatement("INSERT INTO TW_DIARIO_ELECTRON (D_SUCURSAL,D_OPERADOR,D_CONNUREG,D_TXN,C_TXN_REVERSABLE,C_TXN_VISIBLE,D_SUPERVISOR,D_FECHOPER,D_CUENTA,D_SERIAL,D_REFER,D_REFER1,D_REFER2,D_REFER3,D_EFECTIVO,D_MONTO,D_FECHEFEC,D_DIVISA,D_DESCRIP,D_DESCRIP_REVERSO,D_ESTATUS,D_CONSECUTIVO,D_CONSECLIGA,D_CARGO,D_ABONO,D_MSGENVIO) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");

				for (int i = 1; i < TablaDiario.size(); i++) // inserta los
																// datos en
																// blanco (de 1
																// a 25)
				{
					if (i == 15 || i == 16) {
						insert.setInt(i, 0);
					} else {
						insert.setString(i, "");
					}
					Valores.addElement("''");
				}
				Enumeration Keys = Datos.keys();
				int pos = 0;
				String tarjetaTDI = null; // Loch Ness
				if (txn.equals("5503")) {
					tarjetaTDI = getBeanData("getFiller"); // Loch Ness
				}
				if (txn.equals("0728") || txn.equals("0730")) {
					tarjetaTDI = getBeanData("getIntern"); // Loch Ness
				}
				// System.err.println("tarjetaTDI :" + tarjetaTDI);
				while (Keys.hasMoreElements()) // inserta los datos con valores
				{
					String Key = Keys.nextElement().toString();
					String ColumnTable = (String) Datos.get(Key);
					if (tarjetaTDI != null) // LochNess
					{
						if (ColumnTable.equals("D_MSGENVIO")) {
							if (txn.equals("5503")) {
								setBeanData("setFiller", ""); // Loch Ness
							}
							if (txn.equals("0728") || txn.equals("0730")) {
								setBeanData("setIntern", ""); // Loch Ness
							}
						}
						if (ColumnTable.equals("D_DESCRIP")) {
							if (txn.equals("5503")) {
								setBeanData("setFiller", tarjetaTDI); // Loch
																		// Ness
							}
							if (txn.equals("0728") || txn.equals("0730")) {
								setBeanData("setIntern", tarjetaTDI); // Loch
																		// Ness
							}
						}
					}
					String Valor = getBeanData(Key);
					if (Valor == null) { // Verifica si el valor es nulo e
											// inserta el dato como string
						Valor = "nulo";
					}
					pos = Integer.parseInt((String) TablaDiario
							.get(ColumnTable));
					if (ColumnTable.equals("D_MONTO")
							|| ColumnTable.equals("D_EFECTIVO")) {
						// Control de Efectivo
						if (ColumnTable.equals("D_EFECTIVO")) {
							setMonto(Valor);
						}

						if (txn.equals("0820")) {
							Valor = Valor + cents;
						}
						insert.setLong(pos, Long.parseLong(Valor));
					} else {
						insert.setString(pos, Valor);
					}
					Valores.setElementAt("'" + Valor + "'", pos - 1);
				}
				// inserta datos fijos
				pos = Integer.parseInt((String) TablaDiario
						.get("C_TXN_REVERSABLE"));
				Valores.setElementAt("'" + Reversable + "'", pos - 1);
				insert.setString(pos, Reversable);
				pos = Integer.parseInt((String) TablaDiario
						.get("C_TXN_VISIBLE"));
				Valores.setElementAt("'" + Visible + "'", pos - 1);
				insert.setString(pos, Visible);
				pos = Integer.parseInt((String) TablaDiario.get("D_CARGO"));
				Valores.setElementAt("'" + TotC + "'", pos - 1);
				insert.setString(pos, TotC);
				pos = Integer.parseInt((String) TablaDiario.get("D_ABONO"));
				Valores.setElementAt("'" + TotA + "'", pos - 1);
				insert.setString(pos, TotA);
				pos = Integer
						.parseInt((String) TablaDiario.get("D_CONSECLIGA"));
				Valores.setElementAt("'" + ConsecLiga + "'", pos - 1);
				insert.setString(pos, ConsecLiga);
				pos = Integer.parseInt((String) TablaDiario.get("D_FECHOPER"));
				Valores.setElementAt("'" + gc.getDate(1) + "'", pos - 1);
				insert.setString(pos, gc.getDate(1));
				pos = Integer.parseInt((String) TablaDiario.get("D_ESTATUS"));
				Valores.setElementAt("'?'", pos - 1);
				insert.setString(pos, "?");
				pos = Integer.parseInt((String) TablaDiario.get("D_CONNUREG"));
				Valores.setElementAt("'" + setDiarioConsecutivo() + "'",
						pos - 1);
				insert.setString(pos, setDiarioConsecutivo());

				sql = sql + Valores + ")";
				ResultUpdate = insert.executeUpdate();
			}
		} catch (SQLException sqlException) {

			System.out.println("Error Diario::InsertDiario [" + sqlException
					+ "]");
			System.out.println("Error Diario::InsertDiario::SQL INSERT [" + sql
					+ "]");
		}

		finally {
			try {
				if (insert != null) {
					insert.close();
					insert = null;
				}
				if (pooledConnection != null) {
					pooledConnection.close();
					pooledConnection = null;
				}
			} catch (java.sql.SQLException sqlException)

			{
				System.out.println("Error FINALLY Diario::InsertDiario ["
						+ sqlException + "]");
				System.out.println("Error FINALLY Diario::InsertDiario::SQL ["
						+ sql + "]");

			}
		}
		return ResultUpdate;
	}

	public Hashtable getDatosDiario(String Txn, String Moneda) {
		String AfectaDiario = "";
		String CamposDiario = "getTxnCode=D_TXN&getBranch=D_SUCURSAL&getTeller=D_OPERADOR&getSupervisor=D_SUPERVISOR&toString=D_MSGENVIO&";
		String Reversable = "";
		String TotC = "";
		String TotA = "";
		String Visible = "";
		String DEBP = "";
		String DEAP = "";
		Hashtable Datos = new Hashtable();

		DataDiario datosd = DataDiario.getInstance();
		Vector Rselect = datosd.getDatosd(Txn, Moneda);
		if (Rselect != null) {
			AfectaDiario = Rselect.get(0).toString();
			Visible = Rselect.get(1).toString();
			Reversable = Rselect.get(2).toString();
			TotC = Rselect.get(3).toString();
			TotA = Rselect.get(4).toString();
			CamposDiario = CamposDiario + Rselect.get(5).toString();
			if (txn.equals("0806") && getBeanData("getAcctNo").equals("4")) {
				Visible = "S";
				Reversable = "S";
			}
			DEBP = Rselect.get(6).toString();
			DEAP = Rselect.get(7).toString();
			Datos.put("AfectaD", AfectaDiario);
			Datos.put("Visible", Visible);
			Datos.put("Reversable", Reversable);
			Datos.put("TotC", TotC);
			Datos.put("TotA", TotA);
			Datos.put("DEBP", DEBP);
			Datos.put("DEAP", DEAP);
		}

		if (!Datos.isEmpty()) {
			StringTokenizer STCampos = new StringTokenizer(CamposDiario, "&");
			while (STCampos.hasMoreTokens()) {
				String token = STCampos.nextToken();
				int posicion = token.indexOf("=");
				Datos.put(token.substring(0, posicion),
						token.substring(posicion + 1, token.length()));
			}
		}
		return Datos;
	}

	public String getBeanData(String metodo) {
		Object Valor = null;
		try {
			// obtiene la clase
			Class Bean = txnBean.getClass();
			// Tipo de los argumentos que recibe el metodo en un arreglo de
			// clases
			Class[] TipoArgs = new Class[] {};
			// obtiene el metodo del Bean
			Method Metodo = null;
			Metodo = Bean.getMethod(metodo, TipoArgs);
			// argumentos de la clase en un arreglo de Objects
			Object[] Args = new Object[] {};
			// Invocacion dinamica
			Valor = Metodo.invoke(txnBean, Args);
			if (Valor == null) {
				Valor = "";
			}
			if (!metodo.equals("toString")) {
				if (Valor.toString().endsWith("*")) {
					Valor = Valor.toString().substring(0,
							Valor.toString().length() - 1);
				}
			} else if (Valor.toString().startsWith("DSLZ")) {
				Valor = Valor.toString().trim();
			}

		} catch (Exception e) {

			System.out.println("Error Diario::getBeanData [" + e.getMessage()
					+ "]");
			e.printStackTrace();
			return null;
		}
		/*
		 * System.out.println("Metodo ["+metodo+"] = Valor ["+Valor.toString()+"]"
		 * );
		 */
		return Valor.toString();
	}

	public String setBeanData(String metodo, String valor) {
		Object Valor = null;
		try {
			// obtiene la clase
			Class Bean = txnBean.getClass();
			// Tipo de los argumentos que recibe el metodo en un arreglo de
			// clases
			// En este caso el arreglo se inicializa con un elemento
			Class[] TipoArgs = new Class[1];

			// El primer elemento del arreglo lleva la claso tipo String de
			// valor
			TipoArgs[0] = valor.getClass();

			// obtiene el metodo del Bean
			Method Metodo = null;
			Metodo = Bean.getMethod(metodo, TipoArgs);

			// argumentos de la clase en un arreglo de Objects
			Object[] Args = new Object[] { valor };

			// Invocacion dinamica
			Valor = Metodo.invoke(txnBean, Args);

			if (Valor == null) {
				Valor = "";

			}
			if (Valor.toString().endsWith("*")) {
				Valor = Valor.toString().substring(0,
						Valor.toString().length() - 1);

			}

		} catch (Exception e) {
			System.out.println("Error Diario::setBeanData [" + e.getMessage()
					+ "]");
			e.printStackTrace();
			return null;
		}
		return Valor.toString();
	}

	public Vector getMethods(Object DataBean) {
		// obtiene la clase
		Class BeanClass = DataBean.getClass();
		Method List[] = BeanClass.getDeclaredMethods();
		int nmethods = List.length;
		Vector MethodNames = new Vector();
		String metodo = "";
		for (int i = 0; i < nmethods; i++) {
			metodo = List[i].getName();
			if (metodo.startsWith("get")) {
				MethodNames.addElement(metodo);
			}
		}
		return MethodNames;
	}

	public String Filler(String cadena, int largo) {

		int cadlong = cadena.length();
		for (int i = cadlong; i < largo; i++)
			cadena = cadena + " ";

		return cadena;

	}

	public void AfectaEfectivo(HttpSession sesion) {
		String totalC = getTotal(1);
		String totalA = getTotal(2);
		String alertefec;

		if (session.getAttribute("AlertEfec") == null) {
			alertefec = "0";
		} else {
			alertefec = (String) session.getAttribute("AlertEfec");
		}

		session.setAttribute("AlertEfec", alertefec);

		boolean totc = false;
		boolean tota = false;

		StringTokenizer ST = new StringTokenizer(totalC, ",");
		String token = null;

		while (ST.hasMoreTokens()) {
			token = ST.nextToken();
			if (token.equals("1")) {
				totc = true;
				break;
			}
		}

		ST = new StringTokenizer(totalA, ",");
		token = null;

		while (ST.hasMoreTokens()) {
			token = ST.nextToken();
			if (token.equals("7")) {
				tota = true;

				break;
			}
		}

		if (totc || tota) {
			if (getMonto() >= 500000)

			{
				alertefec = "1";
				session.setAttribute("AlertEfec", alertefec);

			}
		}

	}

	public void DoTotales(Hashtable datos, HttpSession sesion, String Moneda) {

		String totalC = getTotal(1);
		String totalA = getTotal(2);

		boolean totc = false;
		boolean tota = false;

		StringTokenizer ST = new StringTokenizer(totalC, ",");

		String token = null;

		while (ST.hasMoreTokens()) {

			token = ST.nextToken();
			if (token.equals("1")) {
				totc = true;
				break;
			}
		}

		ST = new StringTokenizer(totalA, ",");
		token = null;

		while (ST.hasMoreTokens()) {

			token = ST.nextToken();
			if (token.equals("7")) {
				tota = true;
				break;
			}
		}

		if (totc || tota) {

			long EfeR = 0;
			long EfeP = 0;
			long efectivo = 0;
			long EfeLim = 0;
			long MontoFactor = 0;
			String mtoPif = "MontoFactor";
			String Rec = "EfeRec";
			String Pag = "EfePag";
			String efec = "efectivo";
			String alert = "AlertCash";
			String pifalert = "PIFAlertCash";

			if (Moneda.equals("US$")) {
				Rec = Rec + "2";
				Pag = Pag + "2";
				efec = efec + "2";
				alert = alert + "Dlls";
				pifalert = pifalert + "Dlls";
				mtoPif = mtoPif + "2";
			}

			EfeR = Long.parseLong(sesion.getAttribute(Rec).toString());
			EfeP = Long.parseLong(sesion.getAttribute(Pag).toString());

			if (totc) {
				EfeR += getMonto();
			}

			if (tota) {
				EfeP += getMonto();
			}

			sesion.setAttribute("EfeRec", String.valueOf(EfeR));
			sesion.setAttribute("EfePag", String.valueOf(EfeP));

			efectivo = EfeR - EfeP;

			sesion.setAttribute(Rec, String.valueOf(EfeR));
			sesion.setAttribute(Pag, String.valueOf(EfeP));
			EfeLim = Long.parseLong(sesion.getAttribute(efec).toString());
			// CAMBIOPIF
			MontoFactor = Long
					.parseLong(sesion.getAttribute(mtoPif).toString());
			if (efectivo > MontoFactor) {
				sesion.setAttribute(pifalert, "1");
				sesion.setAttribute("efectivoPIF", String.valueOf(efectivo));
			} else
				sesion.setAttribute(pifalert, "0");

			if (efectivo > EfeLim) {
				sesion.setAttribute(alert, "1");
			} else {
				sesion.setAttribute(alert, "0");
			}
		}

	}

	public void setMonto(String mto) {

		monto = Long.parseLong(mto);
	}

	public long getMonto() {

		return monto;
	}

	public void setTotales(String C, String A) {
		totC = C;
		totA = A;
	}

	public String getTotal(int tot) {
		if (tot == 1)
			return totC;
		else
			return totA;
	}

	private boolean isTxnEspecial(String llaveOPP) {
		DatosVarios mot = DatosVarios.getInstance();
		Vector vTxnEsp = new Vector();
		vTxnEsp = (Vector) mot.getDatosV("PROCESP");
		StringTokenizer st = null;
		StringTokenizer st2 = null;
		String llave = "";
		String llave2 = "";
		boolean txnSpecial = false;

		for (int x = 0; x < vTxnEsp.size(); x++) {
			String cad = (String) vTxnEsp.elementAt(x);
			st = new StringTokenizer(cad, "*");
			llave = st.nextToken();

			while (st.hasMoreElements()) {
				String token = st.nextToken();
				st2 = new StringTokenizer(token, "-");
				while (st2.hasMoreElements()) {
					String token2 = st2.nextToken();
					if (token2.equals(llaveOPP)) {
						llave2 = token2;
						break;
					}
				}
				if (token.equals(txn) && llave2.equals(llaveOPP)) {
					txnSpecial = true;
					setFuncEsp(llave);
					break;
				}
				if (token.equals(transaccion) && llave2.equals(llaveOPP)) {
					txnSpecial = true;
					setFuncEsp(llave);
					break;
				}
			}
		}
		return txnSpecial;
	}

	public int SpecialFunc(String invoke, String func, Vector Resp,
			String Connureg) {
		int msgcode = 0;
		try {
			StringTokenizer sFunc = new StringTokenizer(func, "*");
			SpecialFunc Funciones = SpecialFunc.getInstance();
			Hashtable functions = Funciones.getFunciones();
			while (sFunc.hasMoreTokens()) {
				String codFunc = null;
				String newFunc = null;
				codFunc = sFunc.nextToken().toString().trim();
				newFunc = (String) functions.get(codFunc);
				Class Funcion = null;
				InterfazSF inter = null;
				Funcion = Class.forName("ventanilla.com.bital.admin.SpFunc_"
						+ newFunc.toString());
				inter = (InterfazSF) Funcion.newInstance();
				if (invoke.equals("DEBP")) {
					msgcode = inter.SpFuncionBD(txnBean, Resp, session,
							Connureg);
				} else {
					msgcode = inter.SpFuncionAD(txnBean, Resp, session,
							Connureg);
				}
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
		return msgcode;
	}

	public void enviaConsecutivo_Txns(String txnAct, String consecutivo) {
		iTxn = (String) session.getAttribute("page.iTxn") == null ? ""
				: (String) session.getAttribute("page.iTxn");
		String oTxn = (String) session.getAttribute("page.oTxn") == null ? ""
				: (String) session.getAttribute("page.oTxn");
		Txn_0821.cajero = getBeanData("getTeller");

		if (iTxn.equals("5159") && txnAct.equals("5159") || iTxn.equals("5183")
				&& txnAct.equals("5183") || iTxn.equals("4033")
				&& txnAct.equals("4033") || iTxn.equals("0128")
				&& txnAct.equals("0128") || iTxn.equals("0130")
				&& txnAct.equals("0130")) {
			Txn_0821.consecin = consecutivo;
		}

		if (iTxn.equals("5159") && oTxn.equals("0594") && txnAct.equals("0594")
				|| iTxn.equals("5183") && oTxn.equals("0598")
				&& txnAct.equals("0598") || iTxn.equals("4033")
				&& oTxn.equals("0126") && txnAct.equals("0126")
				|| iTxn.equals("0128") && oTxn.equals("4101")
				&& txnAct.equals("4101") || iTxn.equals("0130")
				&& oTxn.equals("0130") && txnAct.equals("0130")) {
			Txn_0821.consecout = consecutivo;
		}

	}
}
