//*************************************************************************************************
//             Funcion: Bean Txn 0360 para comisi�n en txn 0500
//          Creado por: Hugo Gabriel Rivas Cortes
//*************************************************************************************************
// CNN-
//*************************************************************************************************
package ventanilla.com.bital.sfb;

import ventanilla.GenericClasses;

import com.bital.util.ParameterParser;
import com.bital.util.ParameterNotFoundException;
//import ventanilla.GenericClasses;

public class Txn_0360 extends Transaction
{
  public Header doAction(ParameterParser param)
    throws ParameterNotFoundException
  {
    FormatoB formatob = new FormatoB();
    GenericClasses gc = new GenericClasses();
    formatob.setFormat("B");
    formatob.setAcctNo(param.getString("cTxn"));
    formatob.setTranAmt(param.getCString("txtMonto"));
    String strListTipoComision = param.getString("lstTipoComision").substring(0,2);
    formatob.setReferenc(strListTipoComision);
    String descRev = param.getString("lstTipoComision");
    descRev = descRev.substring(0,2) + "REV " + descRev.substring(2);
    formatob.setCashIn(param.getCString("txtMonto"));
    String moneda = gc.getDivisa(param.getString("moneda"));
    formatob.setDescRev(descRev);
    formatob.setFromCurr(moneda);
    
    return formatob;
  }               
}
