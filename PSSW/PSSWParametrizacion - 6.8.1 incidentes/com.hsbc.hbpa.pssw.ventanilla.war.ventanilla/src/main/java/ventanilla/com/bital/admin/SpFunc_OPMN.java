//*************************************************************************************************
//          Funcion: Clase especial para transacciones de OPMN
//          Elemento: SpFunc_OPMN.java
//          Creado por: Jesus Emmanuel Lopez Rosales.
//          Modificado por: Jesus Emmanuel Lopez Rosales
//*************************************************************************************************
// CCN - 4360576 - 09/03/2007 - Modificaciones para proyecto OPMN
// CCN - 4360584 - 23/03/2007 - Modificaciones para proyecto OPMN
//*************************************************************************************************
package ventanilla.com.bital.admin;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Hashtable;
import java.util.Vector;
import javax.servlet.http.HttpSession;
import ventanilla.GenericClasses;
import com.bital.util.ConnectionPoolingManager;



public class SpFunc_OPMN implements InterfazSF{
	
	public SpFunc_OPMN(){			
	}		
	
	public int SpFuncionBD(Object bean, Vector Respuesta, HttpSession session, String Connureg){
		int messagecode = 0;
		return messagecode;
	}
	
	public int SpFuncionAD(Object bean, Vector Respuesta, HttpSession session, String Connureg){
		int messagecode = 0;							

      if(session.getAttribute("EstadoTxn").toString().equals("DEAP")){
		GenericClasses GC = new GenericClasses();				
		
		String NoOrden =GC.getBeanData(bean,"getCtaBenef").trim(); 
		String Status =GC.getBeanData(bean,"getGStatus").trim();
		if(Status.equals("1"))
		   Status = "L";
		else				
		   Status = "C";
		   
		Connection pooledConnection = ConnectionPoolingManager.getPooledConnection();
		PreparedStatement insert = null;	
		
		try{			       
			if(pooledConnection != null){	
			  try{	
				insert = pooledConnection.prepareStatement("UPDATE TC_OPMN SET D_STATUS =? WHERE C_NUMORD_NVO=?");
				insert.setString(1, Status); 
				insert.setString(2, NoOrden);
				insert.executeUpdate();							
			 }catch(SQLException sqlException){
		            System.err.println("SpFunc_OPMN::InsertLog [" + sqlException + "]");		 			 			
			 }
			}        
	   	}catch( Exception e ){
	   		System.err.println("SpFunc_OPMN:: " + e.getMessage());
			e.printStackTrace();		 
	   	}
      }      
      return messagecode;
	}
	public int SpFuncionBR(Hashtable txnRev, Hashtable txnOrg, HttpSession session, String Connureg, String Supervisor){
		int messagecode = 0;
		return messagecode;
	}

	public int SpFuncionAR(Hashtable txnRev, Hashtable txnOrg, HttpSession session, String Connureg, String Cosecutivo, String Supervisor, Vector VResp){
		int messagecode = 0;
		return messagecode;
	}
}							
