//*************************************************************************************************
//			Funcion: Clase que inserta los datos de los depositos
//			Elemento: SpFunc_PAGV.java
//			Creado por: carolina Vela
//          Modificado por: Jesus Emmanuel Lopez Rosales
//*************************************************************************************************


package ventanilla.com.bital.admin;

/**
 * @author Carolina Vela 
 * Fecha:13/01/08
 * Clase que inserta los datos del Deposito Global Cobro Inmediato
 */

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Hashtable;
import java.util.Vector;

import javax.servlet.http.HttpSession;

import ventanilla.GenericClasses;
import ventanilla.com.bital.util.DBQuery;

import com.bital.util.ConnectionPoolingManager;



public class SpFunc_PAGV implements InterfazSF{
	
		
	public SpFunc_PAGV(){	}
	
	public int SpFuncionBD(Object bean, Vector Respuesta, HttpSession session, String Connureg){
		int messagecode = 0;
		return messagecode;
	}

	public int SpFuncionAD(Object bean, Vector Respuesta, HttpSession session, String Connureg){
		int messagecode = 0;
		
		String resp= (String) Respuesta.firstElement();	
		String val4019 = (String)session.getAttribute("bandera4019");
		String connureg = (String)session.getAttribute("page.diario_consecutivo");
		
		
		
		if(resp.equals("0") && val4019 !=null && val4019.equals("SI")){
			GenericClasses GC = new GenericClasses();	
			session.removeAttribute("bandera4019");					
			Hashtable datasession = (Hashtable)session.getAttribute("page.datasession");
			Hashtable usrinfo = (Hashtable)session.getAttribute("userinfo");
			
			//Campos llave para actualizar la tabla de tw_kron
			
			String fecha_sol = GC.getDate(1).trim();	
			String cajero = (String)session.getAttribute("teller");
												
			//Campos a insertar				
			String numCredito = (String)session.getAttribute("creditoKronner");
			String numCuenta=(String)session.getAttribute("ctaRepago");
			String montoPago=(String)session.getAttribute("montoKronner");
			montoPago=GC.delCommaPointFromString(montoPago);
			
			//String llave  = numCuenta+montoPago;	
			String llave  = numCuenta+connureg.substring(9);
							
			String 	valorLetraPago= (String)datasession.get("rdoTipoPago");	
			//	if(){
			//		cveOpcion=3;
			//	}
			//Inserta el dato del Pago
			Connection pooledConnection = ConnectionPoolingManager.getPooledConnection();
			PreparedStatement insert = null;
			String statementPS = com.bital.util.ConnectionPoolingManager.getStatementPostFix();
	        long i = 0;
	        
				int ResultUpdate = 0;
				try{
					
					String sql = null;
					sql = "SELECT D_NUM_TRANS FROM ";
					String clause = " ORDER BY D_NUM_TRANS DESC";
					DBQuery dbq = new DBQuery(sql,"TW_KRON", clause);
					Vector Regs = dbq.getRows(); //All records
					Vector record = new Vector();
					String consecutivo = null;
					if (Regs.size() > 0) 
					{
							record = (Vector)Regs.get(0);
							consecutivo = (String)record.get(0);
							i = Long.parseLong(consecutivo);
							i=i+1;
							consecutivo=Long.toString(i);
							consecutivo = GC.StringFiller(consecutivo,5,false,"0");
					}else
					{	
							consecutivo = "00999";
					}

					//System.out.println("SpFunc_SpFunc_PAGV:Insert Dato::");			
					pooledConnection = ConnectionPoolingManager.getPooledConnection();
					insert = pooledConnection.prepareStatement("INSERT INTO TW_KRON (D_NUM_CREDITO,D_NUM_SUCURSAL ,D_NUM_TRANS ,D_FECHA ,D_FECHA_ULT ,D_FECHA_ANU ,D_CONCEPTO ,D_APLICACION ,D_IMP_MULT ,D_IMP_PAGO ,D_PORCENTAJE ,D_USER_ANU ,D_USER_PAGO ,D_USER_AUT ,D_ESTATUS_REG ,D_CUENTA ,D_FILLER) VALUES(?,?,?,?,?,'0000000000','01','01',?,?,'000000000','        ',?,?,'0','0000000000',?)");
					insert.setString(1, GC.StringFiller(numCredito,10,false,"0"));
					insert.setString(2, "0"+cajero.substring(0,4));  
					insert.setString(3, consecutivo);
					insert.setString(4, "00"+fecha_sol);
					insert.setString(5, "00"+fecha_sol);
					insert.setString(6, GC.StringFiller(valorLetraPago,12,false,"0"));
					insert.setString(7, GC.StringFiller(montoPago,12,false,"0"));  
					insert.setString(8, "PGOVOLBT");
					insert.setString(9, "PGOVOLBT");
					insert.setString(10, connureg);
					ResultUpdate= insert.executeUpdate();
					if(ResultUpdate > 0){
						//System.out.println("SpFunc_SpFunc_PAGV::InsertDato::Se realizo ["+ResultUpdate+"] registro en la tabla TW_KRON");
						//System.out.println("SpFunc_SpFunc_PAGV::InsertDato::Se inserto el nuevo dato a \"\"");
						setConsec4019(connureg,(String)session.getAttribute("teller"),consecutivo);
					}
				}catch(Exception e){
					System.out.println(e.getMessage());
			 			 		
				}finally {		 			
					try{
						if(insert != null) {
							insert.close();
							insert = null;
						}
						if(pooledConnection != null) {
							pooledConnection.close();
							pooledConnection = null;
						}
					}catch(java.sql.SQLException sqlException){
						System.out.println("Error FINALLY SpFunc_SpFunc_PAGV::InsertLog [" + sqlException + "]");	            	
	              		
					}
				}	
									
			}
		return messagecode;
	}
	
	public int SpFuncionBR(Hashtable txnRev, Hashtable txnOrg, HttpSession session, String Connureg, String Supervisor){
		int messagecode = 0;
		return messagecode;
	}
	
	public int SpFuncionAR(Hashtable txnRev, Hashtable txnOrg, HttpSession session, String Connureg, String Cosecutivo, String Supervisor, Vector VResp){
		int messagecode = 0;
		return messagecode;
	}
	 
	public boolean SpFunc_PAGV_1003(String consecutivo, String cajero){
		//Funci�n que hace no reversable la operaci�n inicial con cheques
		String sql = null;
		String CONSECLIGA = null;
		String PostFixUR = com.bital.util.ConnectionPoolingManager.getPostFixUR();
		Connection pooledConnection = ConnectionPoolingManager.getPooledConnection();
		ResultSet Rselect = null;
		Statement select = null;
		Vector vTxn = new Vector();
		int i = 0;

		try
				{
					if(pooledConnection != null)
					{
						select = pooledConnection.createStatement();
						sql = "SELECT D_CONSECLIGA ";
						sql = sql + "FROM TW_DIARIO_ELECTRON WHERE D_OPERADOR = '"+ cajero +"' AND D_CONNUREG = '"+consecutivo+"' ";
						sql += PostFixUR;
						Rselect = select.executeQuery(sql);

						//System.out.println("SQL "+ sql);
						while(Rselect.next())
						{
							CONSECLIGA=Rselect.getString("D_CONSECLIGA").trim();
						}
						
						sql = "SELECT D_CONNUREG ";
						sql = sql + "FROM TW_DIARIO_ELECTRON WHERE D_OPERADOR = '"+ cajero +"' AND D_CONSECLIGA = '"+CONSECLIGA+"' ";
						sql += PostFixUR;
						Rselect = select.executeQuery(sql);

						//System.out.println("SQL "+ sql);
						while(Rselect.next())
						{
							vTxn.add(Rselect.getString("D_CONNUREG").trim());
						}
						for(i=0;i<vTxn.size();i++)
						{
							noReversable((String)vTxn.get(i), cajero);
						}
					}
				}
				catch(SQLException sqlException)
				{
					System.out.println("Error SpFun_PAGV 1003::executeDQuery [" + sqlException + "]");
					System.out.println("Error SpFun_PAGV 1003::executeDQuery::SQL ["+sql+"]");
				}
				finally
				{
					try 
					{
						if(Rselect != null) 
						{
							Rselect.close();
							Rselect = null;
						}
						if(select != null) 
						{
							select.close();
							select = null;
						}
						if(pooledConnection != null) 
						{
							pooledConnection.close();
							pooledConnection = null;
						}
            
					}
					catch(java.sql.SQLException sqlException)
					{
						System.out.println("Error FINALLY SpFun_PAGV 1003::executeDQuery [" + sqlException + "]");
						System.out.println("Error FINALLY SpFun_PAGV 1003::executeDQuery::SQL ["+sql+"]");
					}
				}

	 	return false;
	 }
	 
	 private void noReversable(String consecutivo, String cajero)
	 {
	 	//Funci�n que hace no reversable la operaci�n inicial
					Connection pooledConnection = ConnectionPoolingManager.getPooledConnection();
					PreparedStatement insert = null;
					String statementPS = com.bital.util.ConnectionPoolingManager.getStatementPostFix();
	        
						int ResultUpdate = 0;
						try{
							//System.out.println("SpFunc_PAGV :Update de datos::");			
							pooledConnection = ConnectionPoolingManager.getPooledConnection();
							insert = pooledConnection.prepareStatement("UPDATE TW_DIARIO_ELECTRON SET C_TXN_REVERSABLE = 'N' WHERE D_CONNUREG =? AND D_OPERADOR = ?");
							insert.setString(1, consecutivo);
							insert.setString(2, cajero);  
							ResultUpdate= insert.executeUpdate();
							/*if(ResultUpdate > 0){
								System.out.println("SpFunc_PAGV ::Update ::Se realizo ["+ResultUpdate+"] actualizaci�n de Pago Voluntario");
							}*/
						}catch(Exception e){
							System.out.println(e.getMessage());
			 			 		
						}finally {		 			
							try{
								if(insert != null) {
									insert.close();
									insert = null;
								}
								if(pooledConnection != null) {
									pooledConnection.close();
									pooledConnection = null;
								}
							}catch(java.sql.SQLException sqlException){
								System.out.println("Error FINALLY SpFunc_PAGV::UpdateLog [" + sqlException + "]");	            	
	              		
							}
						}	
									
					}
					
	private void setConsec4019(String consecutivo, String cajero, String consec)
	{
	   //Funci�n que hace no reversable la operaci�n inicial
				   Connection pooledConnection = ConnectionPoolingManager.getPooledConnection();
				   PreparedStatement insert = null;
				   String statementPS = com.bital.util.ConnectionPoolingManager.getStatementPostFix();
	        
					   int ResultUpdate = 0;
					   try{
						   //System.out.println("SpFunc_PAGV :Update de datos::");			
						   pooledConnection = ConnectionPoolingManager.getPooledConnection();
						   insert = pooledConnection.prepareStatement("UPDATE TW_DIARIO_ELECTRON SET D_REFER3 = '"+consec+"' WHERE D_CONNUREG =? AND D_OPERADOR = ?");
						   insert.setString(1, consecutivo);
						   insert.setString(2, cajero);  
						   ResultUpdate= insert.executeUpdate();
						   /*if(ResultUpdate > 0){
							   System.out.println("SpFunc_PAGV ::Update ::Se realizo ["+ResultUpdate+"] actualizaci�n de Pago Voluntario");
						   }*/
					   }catch(Exception e){
						   System.out.println(e.getMessage());
			 			 		
					   }finally {		 			
						   try{
							   if(insert != null) {
								   insert.close();
								   insert = null;
							   }
							   if(pooledConnection != null) {
								   pooledConnection.close();
								   pooledConnection = null;
							   }
						   }catch(java.sql.SQLException sqlException){
							   System.out.println("Error FINALLY SpFunc_PAGV::UpdateLog [" + sqlException + "]");	            	
	              		
						   }
					   }	
									
				   }
		
}
