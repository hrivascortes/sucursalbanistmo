//*************************************************************************************************
//             Funcion: BEAN para transacciones de RAP formato E
//            Elemento: RAPE.java
//          Creado por: Alejandro Gonzalez  
// Ultima Modificacion: 31/05/2004
//      Modificado por: Juvenal R. Fernandez Varela
//*************************************************************************************************
// CCN -4360171 - 23/07/2004 - Se agrega metodo getFormatoSAT para pagos 13 y 13A
//*************************************************************************************************

package ventanilla.com.bital.sfb; 

public class RAPE extends Header
{
  private String txtEffDate   = "";
  private String txtAcctNo    = "";
  private String txtTranAmt   = "";
  private String txtReferenc1 = "";
  private String txtReferenc2 = "";
  private String txtReferenc3 = "";
  private String txtCashIn    = "";
  private String txtCashOut   = "";
  private String txtFromCurr  = "";
  private String txtToCurr    = "";
  private String txtCardType  = "";
  private String txtFiller    = "";
  private String txtFiller1   = "";
  private String txtHolDays   = "";
  private String txtTrNo      = "";
  private String txtCheckNo   = "";
  private String txtMoAmoun   = "";
  private String txtDraftAm   = "";
  private String txtFormatoSAT= "";

  
  public String getFormatoSAT()
  {
      return txtFormatoSAT + "*";
  }
  public void setFormatoSAT(String newFormatoSAT)
  {
      txtFormatoSAT = newFormatoSAT;
  }
  
  public String getEffDate()
  {
	return txtEffDate + "*";
  }
  public void setEffDate(String newEffDate)
  {
	txtEffDate = newEffDate;
  }

  public String getAcctNo()
  {
	return txtAcctNo + "*";
  }
  public void setAcctNo(String newAcctNo)
  {
	txtAcctNo = newAcctNo;
  }

  public String getTranAmt()
  {
	return txtTranAmt + "*";
  }
  public void setTranAmt(String newTranAmt)
  {
	txtTranAmt = newTranAmt;
  }

  public String getReferenc1()
  {
	return txtReferenc1 + "*";
  }
  public void setReferenc1(String newReferenc1)
  {
	txtReferenc1 = newReferenc1;
  }

  public String getReferenc2()
  {
	return txtReferenc2 + "*";
  }
  public void setReferenc2(String newReferenc2)
  {
	txtReferenc2 = newReferenc2;
  }

  public String getReferenc3()
  {
	return txtReferenc3 + "*";
  }
  public void setReferenc3(String newReferenc3)
  {
	txtReferenc3 = newReferenc3;
  }

  public String getCashIn()
  {
	return txtCashIn + "*";
  }
  public void setCashIn(String newCashIn)
  {
	txtCashIn = newCashIn;
  }

  public String getCashOut()
  {
	return txtCashOut + "*";
  }
  public void setCashOut(String newCashOut)
  {
	txtCashOut = newCashOut;
  }

  public String getFromCurr()
  {
	return txtFromCurr + "*";
  }
  public void setFromCurr(String newFromCurr)
  {
	txtFromCurr = newFromCurr;
  }

  public String getToCurr()
  {
	return txtToCurr + "*";
  }
  public void setToCurr(String newToCurr)
  {
	txtToCurr = newToCurr;
  }

  public String getCardType()
  {
	return txtCardType + "*";
  }
  public void setCardType(String newCardType)
  {
	txtCardType = newCardType;
  }

  public String getFiller()
  {
	return txtFiller + "*";
  }
  public void setFiller(String newFiller)
  {
	txtFiller = newFiller;
  }

  public String getFiller1()
  {
	return txtFiller1 + "*";
  }
  public void setFiller1(String newFiller1)
  {
	txtFiller1 = newFiller1;
  }

  public String getHolDays()
  {
	return txtHolDays + "*";
  }
  public void setHolDays(String newHolDays)
  {
	txtHolDays = newHolDays;
  }

  public String getTrNo()
  {
	return txtTrNo + "*";
  }
  public void setTrNo(String newTrNo)
  {
	txtTrNo = newTrNo;
  }

  public String getCheckNo()
  {
	return txtCheckNo + "*";
  }
  public void setCheckNo(String newCheckNo)
  {
	txtCheckNo = newCheckNo;
  }

  public String getMoAmoun()
  {
	return txtMoAmoun + "*";
  }
  public void setMoAmoun(String newMoAmoun)
  {
	txtMoAmoun = newMoAmoun;
  }

  public String getDraftAm()
  {
	return txtDraftAm + "*";
  }
  public void setDraftAm(String newDraftAm)
  {
	txtDraftAm = newDraftAm;
  }

  public String toString()
  {
	return super.toString()
	+ getEffDate()	+ getAcctNo()		+ getTranAmt()
	+ getReferenc1()+ getReferenc2()	+ getReferenc3()
	+ getCashIn()	+ getCashOut()		+ getFromCurr()	+ getToCurr()
	+ getCardType()	+ getFiller()		+ getFiller1()	+ getHolDays()
	+ getTrNo()		+ getCheckNo()		+ getMoAmoun()	+ getDraftAm();
  }
}
