//*************************************************************************************************
//          Funcion: Clase que actualiza el status de Cheque de gerencia (Status 2 impreso)
//          Elemento: SpFunc_CGER.java
//          Creado por: YGX
//          Modificado por: Jesus Emmanuel Lopez Rosales
//*************************************************************************************************


package ventanilla.com.bital.admin;

/**
 * @author YGX 
 * Fecha:05/10/07
 * Clase qye inserta los datos de las solicitudes de Cheque de Gerencia
 */

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.Hashtable;
import java.util.Vector;

import javax.servlet.http.HttpSession;

import ventanilla.GenericClasses;

import com.bital.util.ConnectionPoolingManager;


public class SpFunc_CGER implements InterfazSF{
	
		
	public SpFunc_CGER(){	}
	
	public int SpFuncionBD(Object bean, Vector Respuesta, HttpSession session, String Connureg){
		int messagecode = 0;
		return messagecode;
	}

	public int SpFuncionAD(Object bean, Vector Respuesta, HttpSession session, String Connureg){
		int messagecode = 0;
		GenericClasses GC = new GenericClasses();						
		Hashtable datasession = (Hashtable)session.getAttribute("page.datasession");		
		Hashtable usrinfo = (Hashtable)session.getAttribute("userinfo"); 
		
		//Campos llave para actualizar la tabla de tw_chqger
		
		String sucDestino = (String)datasession.get("txtSucDestino");
		String fecha_sol = (String)datasession.get("txtFchSol");
		String connureg  = (String)datasession.get("txtConureg");
		String serial  = (String)datasession.get("txtSerial");	
		
		//Campos a actualizar				
		String fecha_exp =  GC.getDate(1).trim();	
		
		//Cambio de Status del Cheque
		Connection pooledConnection = ConnectionPoolingManager.getPooledConnection();
		PreparedStatement update = null;
		String statementPS = com.bital.util.ConnectionPoolingManager.getStatementPostFix();
        
    	int ResultUpdate = 0;
        try{
        	
	 		pooledConnection = ConnectionPoolingManager.getPooledConnection();
	 		update = pooledConnection.prepareStatement("UPDATE TW_CHQGER SET D_STATUS=?, D_FECHA_EXP=?, D_SERIAL=? WHERE C_FECHA_SOL=? AND D_SUC_EXP=? AND D_CONNUREG=?" + statementPS);
		    update.setString(1, "3");
		    update.setString(2, fecha_exp);
		    update.setString(3, serial.trim());
		    update.setString(4, fecha_sol);
		    update.setString(5, sucDestino);
		    update.setString(6, connureg);
		    ResultUpdate= update.executeUpdate();
		   // if(ResultUpdate > 0){
		//		System.out.println("SPFunc_CGER::UpdateImpCheque::Se actualiz� ["+ResultUpdate+"] registro en la tabla TW_CHQGER");
		//		System.out.println("SPFunc_CGER::UpdateImpCheque::Se actualiz� el status a \"2\"");
		//	}
	 	}catch(Exception e){
	 		//System.out.println(e.getMessage());
	 		e.printStackTrace();
	 			 		
	 	}finally {		 			
    		try{
    			if(update != null) {
            		update.close();
                	update = null;
            	}
            	if(pooledConnection != null) {
                	pooledConnection.close();
                	pooledConnection = null;
            	}
        	}catch(java.sql.SQLException sqlException){
        		
        		sqlException.printStackTrace();
            	//System.out.println("Error FINALLY SPFunc_CGER::UpdateLog [" + sqlException + "]");	            	
          		
        	}
	 	}	
	 	insertDiario(fecha_sol,connureg);
	 	return messagecode;
	}

	public int SpFuncionBR(Hashtable txnRev, Hashtable txnOrg, HttpSession session, String Connureg, String Supervisor){
		int messagecode = 0;
		return messagecode;
	}
	
	public int SpFuncionAR(Hashtable txnRev, Hashtable txnOrg, HttpSession session, String Connureg, String Cosecutivo, String Supervisor, Vector VResp){
		int messagecode = 0;
		return messagecode;
	}
	
	private void insertDiario(String c_fch_sol, String d_connureg)	{
		//Busca Registros
		String strTxn = new String("");
        String strConsecutivo = new String("");
        String strLiga = new String("");
        
        String statementPS = com.bital.util.ConnectionPoolingManager.getStatementPostFix();
        java.sql.Connection pooledConnection = com.bital.util.ConnectionPoolingManager.getPooledConnection( );
        java.sql.Statement select = null;
        java.sql.ResultSet Rselect = null;
        
        Hashtable hs = new Hashtable();
    	String sql="";    	
    	int key = 0; 
   		try
		{
       
         if(pooledConnection != null)
           {
             select=pooledConnection.createStatement();
           	 sql = "SELECT D_TXN,D_CONSECLIGA,D_CONNUREG ";
           	 sql = sql + "FROM TW_DIARIO_ELECTRON WHERE D_CONNUREG='";
           	 sql = sql + d_connureg +"' AND D_FECHOPER='"+c_fch_sol+"'";           
           	 
           	 //System.out.println("SQL SELECT ["+sql+"]");
             
             Rselect = select.executeQuery(sql); 	   
             while(Rselect.next())
                {  
                  StringBuffer st=new StringBuffer();                  
                  strTxn=(String)Rselect.getString("D_TXN");
                  if (strTxn.equals("")){
                      strTxn=""; }                  
                  strLiga=(String)Rselect.getString("D_CONSECLIGA");
                  if (strLiga.equals("")) {
                      strLiga=""; }                  
                  strConsecutivo=(String)Rselect.getString("D_CONNUREG");
                  if (strConsecutivo.equals("")){
                      strConsecutivo="";}
                                    
                  key=1;                                           
    			}    
    			    			
    			//System.out.println("key " + key);    		 
           }
        }
        catch(java.sql.SQLException sqlException){	        

			sqlException.printStackTrace();
	        //System.out.println("TW_DIARIO_ELECTRON::SelectRegistros [" + sqlException + "]");                    
		}        
        finally {
	    	try {
	    		if(Rselect != null) {
		           Rselect.close();
		           Rselect = null;
		        }
        		if(select != null) {
	            	select.close();
    	            select = null;
                }
	            if(pooledConnection != null) {
    	            pooledConnection.close();
        	        pooledConnection = null;
            	}
            }
	        catch(java.sql.SQLException sqlException){
	        	sqlException.printStackTrace();
    	        //System.out.println("Error FINALLY TW_DIARIO_ELECTRON::SelectLog [" + sqlException + "]");                
           	}
	    } 

        if(key ==1){
		
			//Deshabiltar el revrso en el diario electron
		
			//Connection pooledConnection = ConnectionPoolingManager.getPooledConnection();
			PreparedStatement update = null;
			String statementPS1 = com.bital.util.ConnectionPoolingManager.getStatementPostFix();
	        
	           	int ResultUpdate = 0;
		        try{
		        	
			 		pooledConnection = ConnectionPoolingManager.getPooledConnection();
			 		update = pooledConnection.prepareStatement("UPDATE TW_DIARIO_ELECTRON SET C_TXN_REVERSABLE=? WHERE D_FECHOPER=? AND D_CONSECLIGA=?" + statementPS1);
				    update.setString(1,"N");
				    update.setString(2, c_fch_sol);
				    update.setString(3,strLiga.trim());
				    ResultUpdate= update.executeUpdate();
				  //  if(ResultUpdate > 0){
					//	System.out.println("SPFunc_CGER::CancelacionReverso_Sol::Se actualiz� ["+ResultUpdate+"] registro en la tabla TW_CHQGER");
					//	System.out.println("SPFunc_CGER::CancelacionReverso_Sol::Se actualiz� el status a \"2\"");
					//}
			 	}catch(Exception e){
			 		
			 		e.printStackTrace();
			 		//System.out.println(e.getMessage());
			 			 		
			 	}finally {		 			
		    		try{
		    			if(update != null) {
		            		update.close();
		                	update = null;
		            	}
		            	if(pooledConnection != null) {
		                	pooledConnection.close();
		                	pooledConnection = null;
		            	}
		        	}catch(java.sql.SQLException sqlException){
		        		sqlException.printStackTrace();
		            	//System.out.println("Error FINALLY SPFunc_CGER::UpdateLog [" + sqlException + "]");	            	
	              		
		        	}
			 	}
  		}else {
  			   System.out.println("SPFunc_CGER::CancelacionReverso_Sol::No se encontraron registros en la tabla TW_DIARIO_ELECTRON para cancelar reverso");
  		       System.out.println("SPFunc_CGER::CancelacionReverso_Sol::No se actualizaron registros en la tabla TW_DIARIO_ELECTRON");
  		}	
	 }
}

