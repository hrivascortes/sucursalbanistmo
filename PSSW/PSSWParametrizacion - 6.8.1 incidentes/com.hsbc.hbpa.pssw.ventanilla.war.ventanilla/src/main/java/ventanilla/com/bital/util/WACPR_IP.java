package ventanilla.com.bital.util;

public class WACPR_IP {

  private String   companyNumber = "";
  private String   productID = "";
  private String   accountNumber = "";
  private int      returnStatus = 1;
  private String   returnDescription = "";
  private String   returnCode = "";
  private String[] returnItems = null;
  private String   CISnumber = "";

  public WACPR_IP() {
  }

  public void setCompanyNumber(String companyNumber){
    this.companyNumber = companyNumber;
  }

  private String getCompanyNumber(){
    return this.companyNumber;
  }

  public void setProductID(String productID){
    this.productID = productID;
  }

  private String getProductID(){
    return this.productID;
  }

  public void setAccountNumber(String accountNumber){
    this.accountNumber = accountNumber;
  }

  private String getAccountNumber(){
    return this.accountNumber;
  }

  public int getStatus() {
    return this.returnStatus;
  }

  public String getDescription() {
    return this.returnDescription;
  }

  public String getCode() {
    return this.returnCode;
  }

  public String getCISNumber(){
    return this.CISnumber;
  }

  public void parse(ventanilla.com.bital.util.PASSResponse response) {
    returnItems = response.parseItems(45);
    this.returnStatus = 1;
    if(returnItems[1].equals("00") && returnItems.length > 43){
      this.returnStatus = 0;
      this.CISnumber = returnItems[43];
    }
  }

  public String getMessage() {
    StringBuffer buffer = new StringBuffer(64);
    buffer.append("WACPR_IP~00~");
    buffer.append(this.getCompanyNumber()).append("~");
    buffer.append(this.getProductID()).append("~");
    buffer.append(this.getAccountNumber()).append("~");
    buffer.append("INQ").append("~");
    return new String(buffer);
  }
}
