//*************************************************************************************************
//             Funcion: Bean Txn 0236
//          Creado por: Oscar L�pez Gutierrez
//*************************************************************************************************
// CCN - 4360500 - 04/08/2006 - Se agrega para sobrantes de efectivo CDM
//*************************************************************************************************
package ventanilla.com.bital.sfb;

import com.bital.util.ParameterParser;
import com.bital.util.ParameterNotFoundException;
import ventanilla.GenericClasses;

public class Txn_0236 extends Transaction
{
	public Header doAction(ParameterParser param)
		throws ParameterNotFoundException
	{
		FormatoB formatob = new FormatoB();
		GenericClasses gc = new GenericClasses();
		formatob.setTxnCode("0236");
		formatob.setAcctNo("0236");		
		formatob.setFormat("B");
		formatob.setTranAmt(param.getCString("txtMontoNoCero"));
	        String moneda = gc.getDivisa((String)param.getString("moneda"));
        	formatob.setFromCurr(moneda);
	        String numSuc = (String)param.getCString("sucursal");
        	if(numSuc.length()>4)
	            numSuc = numSuc.substring(numSuc.length()-4);		
        	formatob.setBranch((String)param.getCString("sucursal"));
		String empno = (String)param.getCString("teller");
		String txtRegResp = (String)param.getCString("txtRegResp");
		formatob.setReferenc(txtRegResp + txtRegResp + "D"+numSuc+(String)param.getCString("lstNemo"));

		return formatob;
	}
}
