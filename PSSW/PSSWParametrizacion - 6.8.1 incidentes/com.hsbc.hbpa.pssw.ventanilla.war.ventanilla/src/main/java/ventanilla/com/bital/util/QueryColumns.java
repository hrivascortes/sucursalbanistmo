package ventanilla.com.bital.util;

//public class QueryColumns implements java.io.Serializable
public class QueryColumns
{
  private java.util.Vector   resultArray  = null;
  java.sql.ResultSet newResultSet = null;

  public QueryColumns(java.sql.ResultSet newResultSet)
  {
    this.newResultSet = newResultSet;
    execute();
  }

  public void execute()
  {
   java.sql.ResultSetMetaData rsmd = null;
   try{
    rsmd = this.newResultSet.getMetaData();
    int numberOfColumns = rsmd.getColumnCount();
    resultArray  = new java.util.Vector();
    while(this.newResultSet.next()){
     java.util.Vector newColumns = new java.util.Vector();
     for(int i = 1; i <= numberOfColumns; i++)
       newColumns.add(this.newResultSet.getString(i));
     resultArray.add(newColumns);
    }
   }
   catch(java.sql.SQLException sqlException){
    System.out.println("Error QueryColumns::execute [" + sqlException.toString() + "]");
   }
   finally{
     try{
       if(rsmd != null)
         rsmd = null;
       if(newResultSet != null){
         newResultSet.close();
         newResultSet = null;
       }
     }
     catch(java.sql.SQLException sqlException){
       System.out.println("Error FINALLY QueryColumns::execute [" + sqlException.toString() + "]");
     }
   }
  }

  public java.util.Vector getData()
  {
   return resultArray;
  }
}
