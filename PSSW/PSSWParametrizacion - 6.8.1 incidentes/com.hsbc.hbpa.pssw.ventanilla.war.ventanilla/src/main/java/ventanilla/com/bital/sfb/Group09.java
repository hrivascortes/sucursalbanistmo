//*************************************************************************************************
//		   Funcion: Clase que realiza txn 5903
//		  Elemento: Group09.java
//	Modificado por: Alejandro Gonzalez Castro
//*************************************************************************************************
//CCN - 4360268 - 04/02/2005 - Se realizan adecuaciones para eliminar Agencia NY
//CCN - 4360424 - 03/02/2006 - Se realizan adecuaciones para combinada
//*************************************************************************************************
package ventanilla.com.bital.sfb;

import com.bital.util.ParameterParser;
import com.bital.util.ParameterNotFoundException;
import ventanilla.GenericClasses;

public class Group09 extends Transaction
{
  public Header doAction(ParameterParser param)
    throws ParameterNotFoundException
  {
    Deposito deposito = new Deposito();
    deposito.setTxnCode("5903");
    deposito.setAcctNo  (param.getString("txtDDADeposito"));
    deposito.setTranAmt (param.getCString("txtMonto"));
	GenericClasses gc = new GenericClasses();
    
    String descrip = param.getString("txtDepMisc");
    deposito.setTranDesc(descrip);  
    String DescRev = descrip.substring(0,2) + "REV." + descrip.substring(2,descrip.length());
    if(DescRev.length() > 40)
       DescRev = DescRev.substring(0,40);
    deposito.setDescRev(DescRev);

    int moneda = param.getInt("moneda");
	deposito.setTranCur( gc.getDivisa(param.getString("moneda")));

    if( param.getString("txtFechaEfect", null) != null )
    {
      String fecha1 = param.getString("txtFechaEfect");
      String fecha2 = param.getString("txtFechaSys");
      fecha1 = fecha1.substring(6) + fecha1.substring(3, 5) + fecha1.substring(0, 2);

      if( !fecha1.equals(fecha2) )
      {
        fecha2 = fecha1.substring(4, 6) + fecha1.substring(6) + fecha1.substring(2, 4);
        deposito.setEffDate(fecha2);
      }
    }
    
    return deposito;
  }               
}
