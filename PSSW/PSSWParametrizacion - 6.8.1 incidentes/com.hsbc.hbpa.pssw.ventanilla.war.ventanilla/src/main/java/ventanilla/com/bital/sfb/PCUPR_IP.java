package ventanilla.com.bital.sfb;

import ventanilla.com.bital.util.Segment;

public class PCUPR_IP extends Segment{

  private String   companyNumber = "";
  private String   numeroCIS = "";
  private String   consultaFlag = "1";
  private int      returnStatus = 1;
  
  private String   nacionalidadCliente = "";
  private String   nombreFisica = "";
  private String   nombreMoral = "";
  private String[] returnItems = null;
  private String   CISnumber = "";

  public PCUPR_IP() {
  }
//Request
  public void setCompanyNumber(String companyNumber){
    this.companyNumber = companyNumber;
  }

  private String getCompanyNumber(){
    return this.companyNumber;
  }

  public void setNumeroCIS(String numeroCIS){
    this.numeroCIS = numeroCIS;
  }

  private String getNumeroCIS(){
    return this.numeroCIS;
  }

  public void setConsultaFlag(String consultaFlag){
	    this.consultaFlag = consultaFlag;
	  }

	  private String getConsultaFlag(){
	    return this.consultaFlag;
	  }
	  // Response
  public void setNacionalidadCliente(String nacionalidadCliente){
		    this.nacionalidadCliente = nacionalidadCliente;
		  }

  public String getNacionalidadCliente(){
		    return this.nacionalidadCliente;
		  }
  public void setNombreFisica(String nombreFisica){
	    this.nombreFisica = nombreFisica;
	  }

  public String getNombreFisica(){
	    return this.nombreFisica;
	  }

public void setNombreMoral(String nombreMoral){
    this.nombreMoral = nombreMoral;
  }

public String getNombreMoral(){
    return this.nombreMoral;
  }


  public void parse(ventanilla.com.bital.util.PASSResponse response) {
    returnItems = response.parseItems(45);
    this.returnStatus = 1;
    if(returnItems[1].equals("00") && returnItems.length > 43){
      this.returnStatus = 0;
      this.CISnumber = returnItems[43];
    }
    nombreFisica = returnItems[6];
    nombreMoral  = returnItems[34];
    nacionalidadCliente   = returnItems[24];
	
  }

  public String getMessage() {
    StringBuffer buffer = new StringBuffer(64);
    buffer.append("PCUPR_IP~00~");
    buffer.append(this.getCompanyNumber()).append("~");
    buffer.append(this.getNumeroCIS()).append("~~1~~");
    return new String(buffer);
  }
}
