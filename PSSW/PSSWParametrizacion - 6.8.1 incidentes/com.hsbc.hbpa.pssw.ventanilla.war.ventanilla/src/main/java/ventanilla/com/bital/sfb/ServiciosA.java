package ventanilla.com.bital.sfb;

public class ServiciosA extends Header
{
  private String txtEffDate  = "";
  private String txtAcctNo   = "";
  private String txtNatCurr  = "";
  private String txtTranAmt  = "";
  private String txtTranCur  = "";
  private String txtCheckNo  = "";
  private String txtTrNo     = "";
  private String txtCashIn   = "";
  private String txtCashOut  = "";
  private String txtDraftAm  = "";
  private String txtMoamoun = "";
  private String txtFees     = "";
  private String txtTranDesc = "";
  private String txtHoldays  = "";
  private String txtHoldAmt  = "";
  private String txtGrpTyp1  = "";
  private String txtAmount1  = "";
  private String txtApplid1  = "";
  private String txtAcctNo1  = "";
  private String txtCheckNo1 = "";
  private String txtTrNo1    = "";
  private String txtHolDays1 = "";
  private String txtGrpTyp2  = "";
  private String txtAmount2  = "";
  private String txtApplid2  = "";
  private String txtAcctNo2  = "";
  private String txtCheckNo2 = "";
  private String txtTrNo2    = "";
  private String txtHolDays2 = "";
  private String txtGrpTyp3  = "";
  private String txtAmount3  = "";
  private String txtApplid3  = "";
  private String txtAcctNo3  = "";
  private String txtCheckNo3 = "";
  private String txtTrNo3    = "";
  private String txtHolDays3 = "";
  private String txtGrpTyp4  = "";
  private String txtAmount4  = "";
  private String txtApplid4  = "";
  private String txtAcctNo4  = "";
  private String txtCheckNo4 = "";
  private String txtTrNo4    = "";
  private String txtHolDays4 = "";
  private String txtGrpTyp5  = "";
  private String txtAmount5  = "";
  private String txtApplid5  = "";
  private String txtAcctNo5  = "";
  private String txtCheckNo5 = "";
  private String txtTrNo5    = "";
  private String txtHolDays5 = "";

  private String txtDescRev  = "";

  public String getEffDate()
  {
	return txtEffDate + "*";
  }
  public void setEffDate(String newEffDate)
  {
	txtEffDate = newEffDate;
  }

  public String getAcctNo()
  {
	return txtAcctNo + "*";
  }
  public void setAcctNo(String newAcctNo)
  {
	txtAcctNo = newAcctNo;
  }

  public String getNatCurr()
  {
	return txtNatCurr + "*";
  }
  public void setNatCurr(String newNatCurr)
  {
	txtNatCurr = newNatCurr;
  }

  public String getTranAmt()
  {
	return txtTranAmt + "*";
  }
  public void setTranAmt(String newTranAmt)
  {
	txtTranAmt = newTranAmt;
  }

  public String getTranCur()
  {
	return txtTranCur + "*";
  }
  public void setTranCur(String newTranCur)
  {
	txtTranCur = newTranCur;
  }

  public String getCheckNo()
  {
	return txtCheckNo + "*";
  }
  public void setCheckNo(String newCheckNo)
  {
	txtCheckNo = newCheckNo;
  }

  public String getTrNo()
  {
	return txtTrNo + "*";
  }
  public void setTrNo(String newTrNo)
  {
	txtTrNo = newTrNo;
  }

  public String getCashIn()
  {
	return txtCashIn + "*";
  }
  public void setCashIn(String newCashIn)
  {
	txtCashIn = newCashIn;
  }

  public String getCashOut()
  {
	return txtCashOut + "*";
  }
  public void setCashOut(String newCashOut)
  {
	txtCashOut = newCashOut;
  }

  public String getDraftAm()
  {
	return txtDraftAm + "*";
  }
  public void setDraftAm(String newDraftAm)
  {
	txtDraftAm = newDraftAm;
  }

  public String getMoamoun()
  {
	return txtMoamoun + "*";
  }
  public void setMoamoun(String newMoamoun)
  {
	txtMoamoun = newMoamoun;
  }

  public String getFees()
  {
	return txtFees + "*";
  }
  public void setFees(String newFees)
  {
	txtFees = newFees;
  }

  public String getTranDesc()
  {
	return txtTranDesc + "*";
  }
  public void setTranDesc(String newTranDesc)
  {
	txtTranDesc = newTranDesc;
  }

  public String getHoldays()
  {
	return txtHoldays + "*";
  }
  public void setHoldays(String newHoldays)
  {
	txtHoldays = newHoldays;
  }

  public String getHoldAmt()
  {
	return txtHoldAmt + "*";
  }
  public void setHoldAmt(String newHoldAmt)
  {
	txtHoldAmt = newHoldAmt;
  }

  public String getGrpTyp1()
  {
	return txtGrpTyp1 + "*";
  }
  public void setGrpTyp1(String newGrpTyp1)
  {
	txtGrpTyp1 = newGrpTyp1;
  }

  public String getAmount1()
  {
	return txtAmount1 + "*";
  }
  public void setAmount1(String newAmount1)
  {
	txtAmount1 = newAmount1;
  }

  public String getApplid1()
  {
	return txtApplid1 + "*";
  }
  public void setApplid1(String newApplid1)
  {
	txtApplid1 = newApplid1;
  }

  public String getAcctNo1()
  {
	return txtAcctNo1 + "*";
  }
  public void setAcctNo1(String newAcctNo1)
  {
	txtAcctNo1 = newAcctNo1;
  }

  public String getCheckNo1()
  {
	return txtCheckNo1 + "*";
  }
  public void setCheckNo1(String newCheckNo1)
  {
	txtCheckNo1 = newCheckNo1;
  }

  public String getTrNo1()
  {
	return txtTrNo1 + "*";
  }
  public void setTrNo1(String newTrNo1)
  {
	txtTrNo1 = newTrNo1;
  }

  public String getHolDays1()
  {
	return txtHolDays1 + "*";
  }
  public void setHolDays1(String newHolDays1)
  {
	txtHolDays1 = newHolDays1;
  }

  public String getGrpTyp2()
  {
	return txtGrpTyp2 + "*";
  }
  public void setGrpTyp2(String newGrpTyp2)
  {
	txtGrpTyp2 = newGrpTyp2;
  }

  public String getAmount2()
  {
	return txtAmount2 + "*";
  }
  public void setAmount2(String newAmount2)
  {
	txtAmount2 = newAmount2;
  }

  public String getApplid2()
  {
	return txtApplid2 + "*";
  }
  public void setApplid2(String newApplid2)
  {
	txtApplid2 = newApplid2;
  }

  public String getAcctNo2()
  {
	return txtAcctNo2 + "*";
  }
  public void setAcctNo2(String newAcctNo2)
  {
	txtAcctNo2 = newAcctNo2;
  }

  public String getCheckNo2()
  {
	return txtCheckNo2 + "*";
  }
  public void setCheckNo2(String newCheckNo2)
  {
	txtCheckNo2 = newCheckNo2;
  }

  public String getTrNo2()
  {
	return txtTrNo2 + "*";
  }
  public void setTrNo2(String newTrNo2)
  {
	txtTrNo2 = newTrNo2;
  }

  public String getHolDays2()
  {
	return txtHolDays2 + "*";
  }
  public void setHolDays2(String newHolDays2)
  {
	txtHolDays2 = newHolDays2;
  }

    public String getGrpTyp3()
  {
	return txtGrpTyp3 + "*";
  }
  public void setGrpTyp3(String newGrpTyp3)
  {
	txtGrpTyp3 = newGrpTyp3;
  }

  public String getAmount3()
  {
	return txtAmount3 + "*";
  }
  public void setAmount3(String newAmount3)
  {
	txtAmount3 = newAmount3;
  }

  public String getApplid3()
  {
	return txtApplid3 + "*";
  }
  public void setApplid3(String newApplid3)
  {
	txtApplid3 = newApplid3;
  }

  public String getAcctNo3()
  {
	return txtAcctNo3 + "*";
  }
  public void setAcctNo3(String newAcctNo3)
  {
	txtAcctNo3 = newAcctNo3;
  }

  public String getCheckNo3()
  {
	return txtCheckNo3 + "*";
  }
  public void setCheckNo3(String newCheckNo3)
  {
	txtCheckNo3 = newCheckNo3;
  }

  public String getTrNo3()
  {
	return txtTrNo3 + "*";
  }
  public void setTrNo3(String newTrNo3)
  {
	txtTrNo3 = newTrNo3;
  }

  public String getHolDays3()
  {
	return txtHolDays3 + "*";
  }
  public void setHolDays3(String newHolDays3)
  {
	txtHolDays3 = newHolDays3;
  }

  public String getGrpTyp4()
  {
	return txtGrpTyp4 + "*";
  }
  public void setGrpTyp4(String newGrpTyp4)
  {
	txtGrpTyp4 = newGrpTyp4;
  }

  public String getAmount4()
  {
	return txtAmount4 + "*";
  }
  public void setAmount4(String newAmount4)
  {
	txtAmount4 = newAmount4;
  }

  public String getApplid4()
  {
	return txtApplid4 + "*";
  }
  public void setApplid4(String newApplid4)
  {
	txtApplid4 = newApplid4;
  }

  public String getAcctNo4()
  {
	return txtAcctNo4 + "*";
  }
  public void setAcctNo4(String newAcctNo4)
  {
	txtAcctNo4 = newAcctNo4;
  }

  public String getCheckNo4()
  {
	return txtCheckNo4 + "*";
  }
  public void setCheckNo4(String newCheckNo4)
  {
	txtCheckNo4 = newCheckNo4;
  }

  public String getTrNo4()
  {
	return txtTrNo4 + "*";
  }
  public void setTrNo4(String newTrNo4)
  {
	txtTrNo4 = newTrNo4;
  }

  public String getHolDays4()
  {
	return txtHolDays4 + "*";
  }
  public void setHolDays4(String newHolDays4)
  {
	txtHolDays4 = newHolDays4;
  }

  public String getGrpTyp5()
  {
	return txtGrpTyp5 + "*";
  }
  public void setGrpTyp5(String newGrpTyp5)
  {
	txtGrpTyp5 = newGrpTyp5;
  }

  public String getAmount5()
  {
	return txtAmount5 + "*";
  }
  public void setAmount5(String newAmount5)
  {
	txtAmount5 = newAmount5;
  }

  public String getApplid5()
  {
	return txtApplid5 + "*";
  }
  public void setApplid5(String newApplid5)
  {
	txtApplid5 = newApplid5;
  }

  public String getAcctNo5()
  {
	return txtAcctNo5 + "*";
  }
  public void setAcctNo5(String newAcctNo5)
  {
	txtAcctNo5 = newAcctNo5;
  }

  public String getCheckNo5()
  {
	return txtCheckNo5 + "*";
  }
  public void setCheckNo5(String newCheckNo5)
  {
	txtCheckNo5 = newCheckNo5;
  }

  public String getTrNo5()
  {
	return txtTrNo5 + "*";
  }
  public void setTrNo5(String newTrNo5)
  {
	txtTrNo5 = newTrNo5;
  }

  public String getHolDays5()
  {
	return txtHolDays5 + "*";
  }
  public void setHolDays5(String newHolDays5)
  {
	txtHolDays5 = newHolDays5;
  }
  
  public String getDescRev()
  {
	return txtDescRev;
  }    
  
  public void setDescRev(String newDescRev)
  {
	txtDescRev = newDescRev;
  }    

  public String toString()
  {
	return super.toString()
	+ getEffDate()	+ getAcctNo()	+ getNatCurr()	+ getTranAmt()
	+ getTranCur()	+ getCheckNo()	+ getTrNo()		+ getCashIn()
	+ getCashOut()	+ getDraftAm()	+ getMoamoun()	+ getFees()
	+ getTranDesc()	+ getHoldays()	+ getHoldAmt()
	+ getGrpTyp1()	+ getAmount1()	+ getApplid1()	+ getAcctNo1()	+ getCheckNo1()	+ getTrNo1()	+ getHolDays1()
	+ getGrpTyp2()	+ getAmount2()	+ getApplid2()	+ getAcctNo2()	+ getCheckNo2()	+ getTrNo2()	+ getHolDays2()
	+ getGrpTyp3()	+ getAmount3()	+ getApplid3()	+ getAcctNo3()	+ getCheckNo3()	+ getTrNo3()	+ getHolDays3()
	+ getGrpTyp4()	+ getAmount4()	+ getApplid4()	+ getAcctNo4()	+ getCheckNo4()	+ getTrNo4()	+ getHolDays4()
	+ getGrpTyp5()	+ getAmount5()	+ getApplid5()	+ getAcctNo5()	+ getCheckNo5()	+ getTrNo5()	+ getHolDays5();
  }
}
