//************************************************************************************************************************
//            Elemento: FilterDataAR.java
//************************************************************************************************************************
// CCN - 4360456 - 07/04/2006 - Se agraga codigo para mostrar información de cta en autorización remota de combinadas.
//************************************************************************************************************************

package ventanilla.com.bital.util;

import java.util.Hashtable;
import java.util.Vector;

public class FilterDataAR 
{
  StringBuffer buffer = new StringBuffer();
  String cheque = null;
  String cuentaorigen = null;
  String cuentadestino = null;
  String monto = null;
  String cuenta = null;
  Hashtable table = new Hashtable();

  public FilterDataAR(Hashtable data)
  {
      table = data;
  }
    
  public String generate()
  {
      if (getCheque() != null)
          buffer.append("cheque=").append(cheque).append("#");
      
      if (getcuentaorigen() != null)
          buffer.append("cuentaorigen=").append(cuentaorigen).append("#");
      
      if (getcuentadestino() != null)
          buffer.append("cuentadestino=").append(cuentadestino).append("#");
      
      if (getmonto() != null) 
          buffer.append("monto=").append(monto).append("#");
      
      if (getcuenta() != null)
          buffer.append("cuenta=").append(cuenta).append("#");
      
      if (table.get("txtEfectivo") != null)
          buffer.append("efectivo=").append((String)table.get("txtEfectivo")).append("#");
      return buffer.toString();
  }
  public String getCheque()
  {
    if (table.get("txtCodSeg") == null || table.get("txtCveTran") == null || table.get("txtDDACuenta2") == null ||
        table.get("txtSerial2") == null)
      return null;
   else
      cheque = (String)table.get("txtCodSeg") + "-" + (String)table.get("txtCveTran") + "-" + (String)table.get("txtDDACuenta2") + "-" + (String)table.get("txtSerial2");
    return cheque;
  }

  public String getcuentaorigen()
  {
    if (table.get("txtDDAO") != null)
      cuentaorigen = (String)table.get("txtDDAO");
    if (table.get("txtCDAO") != null)
      cuentaorigen = (String)table.get("txtCDAO");
    if (table.get("txtCuentaCBA_C") != null)
      cuentaorigen = (String)table.get("txtCuentaCBA_C");
    if (table.get("txtDDAC") != null)
      cuentaorigen = (String)table.get("txtDDAC");
    if (table.get("txtDDARetiro") != null)
      cuentaorigen = (String)table.get("txtDDARetiro");  
    return cuentaorigen;
  }

  public String getcuentadestino()
  {
if (table.get("txtDDAA") != null)
   cuentadestino = (String)table.get("txtDDAA");
if (table.get("txtCuentaCBA_A") != null)       
    cuentadestino = (String)table.get("txtCuentaCBA_A");
if (table.get("txtDDAD") != null)       
   cuentadestino = (String)table.get("txtDDAD");
if (table.get("txtCDAD") != null)       
   cuentadestino = (String)table.get("txtCDAD");
   if (table.get("txtDDADeposito") != null)       
      cuentadestino = (String)table.get("txtDDADeposito");
   return cuentadestino;
  }
  
  public String getmonto()
  {
  	Vector montos = new Vector();
montos.add("txtMontoC");
montos.add("txtCveIMSS");
montos.add("txtCveINFONAV");
montos.add("txtCveISSSTE");  
montos.add("txtCveFOVI");    
montos.add("txtMto");        
montos.add("txtMontoT");     
montos.add("txtMontoBital");
montos.add("txtMontoCobro"); 
montos.add("txtMtoSAAC"); 
montos.add("txtMontoRemesa");
montos.add("txtMontoRC");    
montos.add("txtMontoCheque");
montos.add("txtChqsBital");  
montos.add("txtChqsOtros");  
montos.add("txtMtoCOPM");    
montos.add("txtMtoOPM");     
montos.add("txtMontoA");     
montos.add("txtMonto");      
montos.add("txtCheque");     
montos.add("txtMontod");     
montos.add("txtMontoRem");   
montos.add("txtMonto1");     
montos.add("txtMontoNoCero");
     for (int i=0; i<montos.size(); i++)
     {
       if (table.get((String)montos.get(i)) != null)
       {
          monto = (String)table.get((String)montos.get(i));
          break;
        }
     }
     return monto;
  }
 
  public String getcuenta()
  {
    Vector cuentas = new Vector();
cuentas.add("txtDDACuenta2");
cuentas.add("txtCtaCia");    
cuentas.add("txtDDACuenta3");
cuentas.add("txtNumcta");    
cuentas.add("txtCtaAfore");  
cuentas.add("txtCtaAfore");  
cuentas.add("txtAcct8000");  
cuentas.add("txtDDACuenta"); 
cuentas.add("txtDDACuenta2");
cuentas.add("txtDDA");       
cuentas.add("txtCDA");       
cuentas.add("txtDDACuenta1");
cuentas.add("txtCuenta");
cuentas.add("txtDDA");       
cuentas.add("txtCuentaCom"); 
     for (int i=0; i<cuentas.size(); i++)
     {
       if (table.get((String)cuentas.get(i)) != null)
       {
          cuenta = (String)table.get((String)cuentas.get(i));
//System.out.println("cuenta [" + cuenta + "]");
          break;
        }
     }
     return cuenta;
  }  

}
