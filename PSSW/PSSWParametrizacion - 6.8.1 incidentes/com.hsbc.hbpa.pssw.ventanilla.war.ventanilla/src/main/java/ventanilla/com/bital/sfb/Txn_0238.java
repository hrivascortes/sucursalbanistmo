//*************************************************************************************************
//             Funcion: Bean Txn 0238
//          Creado por: Oscar Lopez Gutierrez
//*************************************************************************************************
// CCN - 4360512 - 08/09/2006 - Se agrega para identificar Depůsitos por ETV's
//*************************************************************************************************
package ventanilla.com.bital.sfb;

import com.bital.util.ParameterParser;
import com.bital.util.ParameterNotFoundException;
import ventanilla.GenericClasses;

public class Txn_0238 extends Transaction
{
	public Header doAction(ParameterParser param)
		throws ParameterNotFoundException
	{

		FormatoB formatob = new FormatoB();
		GenericClasses gc = new GenericClasses();
		formatob.setTxnCode("0238");
		formatob.setAcctNo("0238");		
		formatob.setFormat("B");
		formatob.setTranAmt(param.getCString("txtMonto"));
        String moneda = gc.getDivisa((String)param.getString("moneda"));
        formatob.setFromCurr(moneda);
        formatob.setCashOut(param.getCString("txtMonto")); // test replica
        String numFolio=(String)param.getCString("txtNumFolio");        
      	while(numFolio.length() < 10)
          numFolio = "0" + numFolio;        
		formatob.setReferenc((String)param.getCString("sucursal")+ numFolio);

		return formatob;
	}
}
