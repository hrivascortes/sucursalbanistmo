//*************************************************************************************************
//             Funcion: Bean Txn 0817
//          Creado por: Liliana Neyra Salazar
//*************************************************************************************************
// CCN - 4360602 - 18/05/2007 - Se realizan modificaciones para proyecto Codigo de Barras SUAS
//*************************************************************************************************
package ventanilla.com.bital.sfb;

import com.bital.util.ParameterParser;
import com.bital.util.ParameterNotFoundException;
import ventanilla.GenericClasses;

public class Txn_0817 extends Transaction
{
	public Header doAction(ParameterParser param)
		throws ParameterNotFoundException
	{
		FormatoB formatob = new FormatoB();
		GenericClasses gc = new GenericClasses();
			
		formatob.setTxnCode("0817");
		formatob.setFormat("B");
		formatob.setAcctNo(param.getString("txtnumctrl"));
		formatob.setTranAmt(gc.quitap(param.getString("txtMonto")));
		formatob.setReferenc("0772");
		String moneda = gc.getDivisa(param.getString("moneda"));
		formatob.setFromCurr(moneda);
		
		return formatob;
	}
}
