//*************************************************************************************************
//		 Funcion: Clase que realiza txn 5203 & 5703
//		Elemento: Group16Serv.java
//Modificado por: Alejandro Gonzalez Castro
//*************************************************************************************************
//CCN - 4360268 - 04/02/2005 - Se realizan adecuaciones para eliminar Agencia NY
//CCN - 4360368 - 02/09/2005 - Adecuacion para Depto 9024 txn 5203
// CCN - 4360374 - 08/09/2005 - Se genera nuevo paquete a peticion de QA.
//*************************************************************************************************
 
package ventanilla.com.bital.sfb;

import com.bital.util.ParameterParser;
import com.bital.util.ParameterNotFoundException;

public class Group11 extends Transaction
{
  public Header doAction(ParameterParser param)
    throws ParameterNotFoundException
  {
    // Instanciar el bean a utilizar
    Deposito deposito = new Deposito();
    
    // Ingresar datos requeridos
    deposito.setAcctNo  (param.getString("txtDDACuenta"));
    deposito.setTranAmt (param.getCString("txtMonto"));
    deposito.setMoamoun("000");

    int moneda = param.getInt("moneda");
    if( moneda == 1 )
      deposito.setTranCur("N$");
    else if( moneda == 2 )
      deposito.setTranCur("US$");
    else
      deposito.setTranCur("UDI");
    
    if(param.getString("cTxn").equals("5203"))
    {
           deposito.setTranDesc("ABONO POR CARTERA "+param.getString("txtReferencia3"));
           deposito.setDescRev("REV ABONO CARTERA "+param.getString("txtReferencia3"));
    }
    else
    {
      String descrip = "ABONO MANUAL CARTERA HIP.";
      deposito.setTranDesc(descrip);
      // Descripcion para Reverso
      String DescRev = "REV." + descrip;
      if(DescRev.length() > 40)
          DescRev = DescRev.substring(0,40);
      deposito.setDescRev(DescRev);
    }
    
    return deposito;
  }               
}
