//*************************************************************************************************
//             Funcion: Clase que realiza la consulta del flujo Interno de la txn y lo conserva en memoria
//          Creado por: Humberto Enrique Balleza Mej�a
//      Modificado por: Jesus Emmanuel Lopez Rosales
//*************************************************************************************************
// CCN - 4360576 - 09/03/2007 - Modificaciones para proyecto OPMN
//*************************************************************************************************

package ventanilla.com.bital.beans;

import ventanilla.com.bital.util.DBQuery;

import java.util.Vector;
import java.util.Hashtable;


public class Flujotxn_Int {
	   private Hashtable flujotxn = null;
	   private static Flujotxn_Int instance = null;
    
	   private Flujotxn_Int() 
	   {   
		   flujotxn = new Hashtable();
		   execute();
	   }
    
	   public static synchronized Flujotxn_Int getInstance()
	   {
		   if(instance == null)
			   instance = new Flujotxn_Int();
		   return instance;
	   }
    
	   public String[] getFlujotxn(String txne, String txns, String moneda, String cTxn, String idFlujo)
	   {    
	   	  String id = "";
	   	  int j = 0;
	   	  if(idFlujo == null)
	   	  	id="00";
	   	  else
	   	    id=idFlujo; 	
	   	  
		  for(int i = txns.length(); i < 4; i++)
			txns += " ";
		   String dflujotxn = null;
		   java.util.Vector vflujo = new java.util.Vector();
		   String[] iflujo = null;
		   if(flujotxn.containsKey(txne + txns + moneda + id) && !cTxn.equals("0825"))
		   {
			   dflujotxn = (String)this.flujotxn.get(txne + txns + moneda + id);
			   ventanilla.com.bital.util.NSTokenizer parser = new ventanilla.com.bital.util.NSTokenizer(dflujotxn, "*");
			   while( parser.hasMoreTokens() ){
				 String token = parser.nextToken();
				 if( token == null )
				  continue;
				 vflujo.add(token);
			   }
		   }
		   if(!vflujo.isEmpty())
		   {
		   	 iflujo	= new String[vflujo.size()];			
			   for(int i=0;i<vflujo.size();i++)
			   {
			   		iflujo[j]=(String)vflujo.get(i);
			   		j++;
			   }
		   }
		   else
		   {
			 iflujo= new String[]{cTxn};
		   }
		return iflujo;
	   }
    
	   public void execute() 
	   {
		   String sql = null;
		   sql = "SELECT C_TXN_I,C_TXN_O,C_MONEDA,D_FLUJO_INT,C_IDFLUJO FROM ";
//		   sql = "SELECT C_TXN_ENTRADA,C_TXN_SALIDA,C_MONEDA,D_FLUJO_TXN FROM ";
		   String clause = null;
		   clause = " ";
        
		   DBQuery dbq = new DBQuery(sql,"TC_FLUJO_INT", clause);
//		   DBQuery dbq = new DBQuery(sql,"TC_FLUJO_TXNS", clause);
		   Vector Regs = dbq.getRows();
        
		   int size = Regs.size();
		   Vector row = new Vector();
		   String txne = null;
		   String txns = null;
		   String moneda = null;
		   String flujo = null;
		   String idFlujo = null;

		   for(int i=0; i<size; i++)
		   {
			   row = (Vector)Regs.get(i);
			   txne = row.get(0).toString();
			   txns = row.get(1).toString();
			   moneda = row.get(2).toString();
			   flujo = row.get(3).toString();
			   idFlujo = row.get(4).toString();
			   flujotxn.put(txne+txns+moneda+idFlujo, flujo);
			   //flujotxn.put(txne+txns+moneda+"00", flujo);
		   }
	   }
}
