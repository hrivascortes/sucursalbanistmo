//*************************************************************************************************
//          Funcion: Clase que inserta los datos de trabajo del formato SIB
//          Elemento: SpFunc_SIB.java
//          Creado por: Humberto Enrique Balleza Mej�a
//          Modificado por: Jesus Emmanuel Lopez Rosales
//*************************************************************************************************

package ventanilla.com.bital.admin;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Hashtable;
import java.util.Stack;
import java.util.StringTokenizer;
import java.util.Vector;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpSession;

import ventanilla.GenericClasses;
import ventanilla.com.bital.beans.DatosVarios;
import ventanilla.com.bital.sfb.FormatoG;
import ventanilla.com.bital.sfb.PACPR_IP;
import ventanilla.com.bital.sfb.PCUPR_IP;
import ventanilla.com.bital.util.Base;
import ventanilla.com.bital.util.PASSRequest;
import ventanilla.com.bital.util.Segment;
import ventanilla.com.bital.util.Usuario;

import com.bital.util.ConnectionPoolingManager;


public class SpFunc_SIB extends HttpServlet implements InterfazSF{
	
	  GenericClasses gc = new GenericClasses();

	public int SpFuncionBD(Object bean, Vector respuesta, HttpSession session, String connureg)
	  {
	    return 0;
	  }

	  
		public int SpFuncionAD(Object bean, Vector respuesta, HttpSession session,
				String connureg) {
			int messagecode = 0;

		Hashtable datasession = (Hashtable) session.getAttribute("page.datasession");

		//IDDEVA Se agrega una variable para mostrar el formulario UAF
		datasession.put("formulariaUAF","NO");
		
        
		// Proyecto Daily Cash - Transacciones Automaticas y Manuales		
		datasession.put("HAB", "XHAB");
		String resp_Hab ="1";
		String ctxn = (String) datasession.get("cTxn");
		String iTxn = (String) datasession.get("iTxn");
		String oTxn = (String) datasession.get("oTxn");	
		String moneda = (String) datasession.get("moneda");
		String monto = (String) datasession.get("txtMonto");
		Stack flujotxn = (Stack) session.getAttribute("page.flujotxn");
		
		
		if((ctxn.equals("0726")||ctxn.equals("0106")) 
				&& (iTxn.equals("0726")||oTxn.equals("0106"))){
			monto = (String) datasession.get("txtTotalMN");
			moneda = "01";
		}

		if (datasession.get("lstForLiq") == null) {
			datasession.put("lstForLiq", "01");

		}
		//ini_if_0001
		if (moneda.equals("01") && searchEfectivo(session)) {
			
			
			if (ctxn.equals("0566")
					|| ctxn.equals("0568") 
					&& !datasession.get("lstForLiq").toString().equals("02") 
					|| ctxn.equals("0476") 
					|| ctxn.equals("4153")
					|| (datasession.get("iTxn").toString().equals("0726")&& datasession.get("oTxn").toString().equals("0106"))) 
			{
				resp_Hab="0";
				datasession.put("HAB", "MHAB");
				if(datasession.get("servicio1")!=null){
					
					if(datasession.get("servicio1").toString().equals("60")||datasession.get("servicio1").toString().equals("670")){
						
					    datasession.put("HAB", "MHAB");
					}else{
							
					   datasession.put("HAB", "XHAB");
					   if (!flujotxn.empty()){
						  flujotxn.pop();
					   }
					}

			    }
//				IDDEVA Se agrego el codigo || ctxn.equals("5503") para que se consulte la txn 0840
			}else if (ctxn.equals("1001") 
						|| ctxn.equals("1003") 
						|| ctxn.equals("1053") 
						|| ctxn.equals("5357") 
						|| ctxn.equals("1057") 
						|| ctxn.equals("0468") 
						|| ctxn.equals("4537") 
						|| ctxn.equals("1193") 
						|| ctxn.equals("1195")
						|| (datasession.get("iTxn").toString().equals("4043")&& datasession.get("oTxn").toString().equals("0106"))
						|| (datasession.get("iTxn").toString().equals("0726")&& datasession.get("oTxn").toString().equals("4111"))
						|| ctxn.equals("5503") 
						|| ctxn.equals("ARPL")
					) 
			{
				
				FormatoG txn = new FormatoG();
				txn.setTxnCode("0840");
				txn.setFormat("G");
				txn.setToCurr("N$");
				txn.setTranAmt("1");

				if ( ctxn.equals("1001") 
				  || ctxn.equals("1003") 
				  || ctxn.equals("1193") 
				  || ctxn.equals("5357") 
				  || ctxn.equals("1195")
				  || (datasession.get("iTxn").toString().equals("4043")&& datasession.get("oTxn").toString().equals("0106"))
				  )
				{
						txn.setAcctNo((String) datasession.get("txtDDACuenta"));
						datasession.put("txtDDAHAB", datasession.get("txtDDACuenta"));
				}else if (ctxn.equals("1053") || ctxn.equals("4537")) {
					String cta = (String) datasession.get("txtDDACuenta2");
					if (cta.length() == 11){
						txn.setAcctNo(cta.substring(1, cta.length()));
						datasession.put("txtDDAHAB", cta.substring(1, cta.length()));
					}
					else{
						txn.setAcctNo((String) datasession.get("txtDDACuenta2"));
						datasession.put("txtDDAHAB", datasession.get("txtDDACuenta2"));
						}
				} else if (ctxn.equals("3001")){
					txn.setAcctNo((String) datasession.get("txtCDACuenta"));
					datasession.put("txtDDAHAB", datasession.get("txtCDACuenta"));
			}
				else if (ctxn.equals("1057")){
					txn.setAcctNo((String) datasession.get("txtDDARetiro"));
					datasession.put("txtDDAHAB", datasession.get("txtDDARetiro"));
				}
				else if (ctxn.equals("0468")){
					txn.setAcctNo((String) datasession.get("txtDDADeposito"));
					datasession.put("txtDDAHAB", datasession.get("txtDDADeposito"));
				}
				else if ((datasession.get("iTxn").toString().equals("0726")&& datasession.get("oTxn").toString().equals("4111"))){
					txn.setAcctNo((String) datasession.get("txtDDACuenta1"));
					datasession.put("txtDDAHAB", datasession.get("txtDDACuenta1"));
					//20120125
				}else if (ctxn.equals("5503") || ctxn.equals("ARPL")){
					//IDDEVA.- Se obtiene la cta rap para enviarla en la txn 0840
					String sCtaServicioRAP = (String) datasession.get("CuentaRAP");
					txn.setAcctNo(sCtaServicioRAP);
					//Creo que nunca recupera esta variable
					datasession.put("txtDDAHAB",sCtaServicioRAP);
					
				}
				
				session.setAttribute("page.datasession", datasession);
				txn.setBenefic("25");
				txn.setBranch((String) datasession.get("sucursal"));
				txn.setTeller((String) datasession.get("teller"));
				txn.execute();
				resp_Hab = txn.getMessage();
				//condicionar a la respuesta de hogan
				
				if (resp_Hab.substring(0, 1).equals("0")) {
					String respHAB = getHabitualidad(resp_Hab);
					
					
					//Verificacion de Habitualidad para Credito o Debito
					//IDDEVA Se agrega la txn 5503 para consultar la habitualidad  
					if(
							( (     ctxn.equals("1001") 
								 || ctxn.equals("1003") 
								 || ctxn.equals("4111")
								 || ctxn.equals("0468")
								 || ctxn.equals("P001")
								 || ctxn.equals("5503")
								 || ctxn.equals("ARPL")
							   ) 
							  && respHAB.equals("DHAB")
							 )
	   				      || (   
	   				    		  (   ctxn.equals("1053")
	   				    	       || ctxn.equals("1193")
	   				    	       || ctxn.equals("5357")
	   				    	       || ctxn.equals("1195")
	   				    	       || ctxn.equals("4043")
	   				    	       || ctxn.equals("0490")
	   				    	       || ctxn.equals("4537")
	   				    	       ) 
	   				    	    && respHAB.equals("CHAB")
	   				    	 )	
						  || ( respHAB.equals("NHAB"))
					  ){
						    
						     
						    //IDDEVA.- Se setea la bnd formularisUAT con SI para mostrar del Formulario						    
						    datasession.put("formulariaUAF","SI");
							datasession.put("HAB", respHAB);	
							//Codigo en de consulta para PLD de los datos del cliente y cuenta
							invokePLD(datasession,1,gc.getBeanData(bean, "getBranch"),gc.getBeanData(bean, "getTeller"),	session,"");
							
						    DatosVarios datosSIB = DatosVarios.getInstance();                   
					        Vector paises = new Vector();  
					    	paises =(Vector)datosSIB.getDatosV("PAISES");
					    	String rubro = (String)datosSIB.getDatosv("RUBRO");
					    	
					    	String paisCliente = (String)datasession.get("paisCliente");;
					    	String subProducto =(String)datasession.get("subProducto");
					    	String tipoRubro="";
					    	String nacCliente="";
					    	String temp="";
					    	
					    	
					    	for (int i=0; i< paises.size(); i++){
					    		StringTokenizer lineaTemp = new StringTokenizer(paises.elementAt(i).toString(),"*");
					    		while(lineaTemp.hasMoreTokens()){
					    			temp = (String)lineaTemp.nextToken();
					    			if(temp.substring(0, 2).equals(paisCliente)){
					    				datasession.put("lstNacCte",temp);
					    				temp="ok";
					    				break;
					    			}
					    		}
				    			if(temp.equals("ok")){
				    				break;
				    			}else
				    				datasession.put("lstNacCte","No definido en PSSW");
					    	}
					    	
					    	StringTokenizer lineaTemp = new StringTokenizer(rubro,"|");					    	
					    	while(lineaTemp.hasMoreTokens()){
				    			temp = (String)lineaTemp.nextToken();
				    			tipoRubro = temp.substring(0,4);
				    			StringTokenizer lineaTemp2 = new StringTokenizer(temp,"*");
				    			while(lineaTemp2.hasMoreTokens()){
				    				temp = (String)lineaTemp2.nextToken();
				    				if(temp.substring(0, 2).equals(subProducto)){
				    					datasession.put("lstRubro", tipoRubro);
					    				temp="ok";
					    				break;
					    			}
					    		}
				    			if(temp.equals("ok")){
				    				break;
				    			}else
				    				datasession.put("lstRubro", "9999");
					    	}
					    	//IDDEVA SE AGREGO LA TXN 5503
					    	if(ctxn.equals("1003")||ctxn.equals("1001")||ctxn.equals("0468")||ctxn.equals("P001")||ctxn.equals("5503")){
								datasession.put("lstOperacion", "001 E");
								if(ctxn.equals("P001")){
									datasession.put("lstRubro", "7101");
								}
							}else if(ctxn.equals("1053")||ctxn.equals("1193")||ctxn.equals("5357")||ctxn.equals("1195")||ctxn.equals("1057")||ctxn.equals("4537"))
							{
								datasession.put("lstOperacion", "002 S");				
							}else if(  (datasession.get("iTxn").toString().equals("4043")&& datasession.get("oTxn").toString().equals("0106"))
									|| (datasession.get("iTxn").toString().equals("0726")&& datasession.get("oTxn").toString().equals("4111")))
							{
								datasession.put("lstOperacion", "003 C");
							}else if( datasession.get("iTxn").toString().equals("5503")
									|| datasession.get("iTxn").toString().equals("ARPL")
									  )
							{
								//IDDEVA.- Se asigna el valor de la operaci�n para el RAP
								datasession.put("lstOperacion", "001 E");
							}
					    	
							session.setAttribute("page.datasession", datasession);
					}else
					{
						
						datasession.put("SHAB", "SHAB");
					    datasession.put("HAB", "XHAB");
					    if (!flujotxn.empty() && !resp_Hab.equals("0")){
							flujotxn.pop();
						}

					}
				}
			} 
			}
		//fin_if_0001
		else {			
			if (!flujotxn.empty() && !resp_Hab.equals("0")){
				flujotxn.pop();
			}
			
			
		}


				return messagecode;
	}

  public int SpFuncionBR(Hashtable txnRev, Hashtable txnOrg, HttpSession session, String connureg, String supervisor) {
    return 0;
  }

  public int SpFuncionAR(Hashtable txnRev, Hashtable txnOrg,
			HttpSession session, String connureg, String cosecutivo,
			String supervisor, Vector vResp) {

		String consecutivoLiga = (String)session.getAttribute("consecutivoLiga");

		int resultDelete=0;
		
        String statementPS = ConnectionPoolingManager.getStatementPostFix();
        Connection pooledConnection = ConnectionPoolingManager.getPooledConnection();
        PreparedStatement delete = null;
        try
        {
            if(pooledConnection != null)
            {
                delete = pooledConnection.prepareStatement("DELETE FROM TW_FSIB WHERE D_CONSECLIGA=?" + statementPS);
                delete.setString(1,consecutivoLiga);
                resultDelete = delete.executeUpdate();


            }
        }
        catch(SQLException sqlException)
            {System.out.println("Error SpFunc_SIB::S [" + sqlException + "]");}
        finally
        {
            try
            {
                if(delete != null)
                {
                    delete.close();
                    delete = null;
                }
                if(pooledConnection != null)
                {
                    pooledConnection.close();
                    pooledConnection = null;
                }
            }
            catch(java.sql.SQLException sqlException){System.out.println("Error FINALLY GroupRAP::clearcertif [" + sqlException + "]");}
        }
        if(resultDelete == 1){
        	session.setAttribute("DELETESIB", "DELETESIB");
        	resultDelete = 0;
        }
		return resultDelete;
	}
  private boolean searchEfectivo(HttpSession session) {
		Hashtable hs = new Hashtable();
		hs = (Hashtable) session.getAttribute("page.datasession");
		String sServicioRAP = (String)hs.get("servicio1");
		
		long efectivo = 0;
		String campoEfectivo = "";
		String ctxn = (String) hs.get("cTxn");
		
		if (ctxn.equals("1001") || ctxn.equals("1003") || ctxn.equals("3001") )
			efectivo = Long.parseLong(gc.delCommaPointFromString((String) hs.get("txtEfectivo")));
		else if (ctxn.equals("1053") || ctxn.equals("1193")
				|| ctxn.equals("5357") || ctxn.equals("1079")
				|| ctxn.equals("1195") || ctxn.equals("0566")
				|| ctxn.equals("0568") || ctxn.equals("0476")  
				|| ctxn.equals("4153") || ctxn.equals("1057")
				|| ctxn.equals("0468") || ctxn.equals("4537"))
			efectivo = Long.parseLong(gc.delCommaPointFromString((String) hs
					.get("txtMonto")));
		else if  ((hs.get("iTxn").toString().equals("4043")&& hs.get("oTxn").toString().equals("0106"))
				|| (hs.get("iTxn").toString().equals("0726")&& hs.get("oTxn").toString().equals("4111"))
				|| (hs.get("iTxn").toString().equals("0726")&& hs.get("oTxn").toString().equals("0106"))){
			efectivo = Long.parseLong(gc.delCommaPointFromString(hs.get("txtTotalMN").toString()));
		//20120125	
		}else if( (ctxn.equals("5503") || ctxn.equals("ARPL") ) && !sServicioRAP.equals("60")  && !sServicioRAP.equals("670") ){
			//IDDEVA, esta validaci�n es para que solo entre con los servicios diferente de 60 y 670
			//efectivo = Long.parseLong(GC.delCommaPointFromString(hs.get("txtEfectivo").toString()));
			String sEfectivo = (String)hs.get("efectivoCSIB");
			if(null == sEfectivo){
			   
			   sEfectivo = (String)hs.get("txtEfectivo");
			   if(null == sEfectivo){
				   
				   efectivo = Long.parseLong(gc.delCommaPointFromString(sEfectivo));
			   }
			}			
			efectivo = Long.parseLong(gc.delCommaPointFromString(sEfectivo));
			
		}
		
		
		if (efectivo >= 1000000)
			return true;
		else
			return false;
	}

  private String getHabitualidad(String newCadNum)
  {
    int inicio = newCadNum.indexOf("HABITUALIDAD:");
    String bandera = newCadNum.substring(inicio + 13, newCadNum.length());
    int fin = bandera.indexOf("~");
    String nCadNum = new String("");
    nCadNum = bandera.substring(0, fin);

    return nCadNum.trim();
  }
  
  private boolean invokePLD (Hashtable info, 
								int op,
								String sucursal,
								String teller,
								HttpSession session, 
								String consecutivo){
	    
		boolean status = false;

		GenericClasses gc = new GenericClasses();
		String moneda = gc.getDivisa((String)info.get("moneda"));
		
		Usuario usuario = new Usuario();
		usuario.setSucursal(sucursal);
		usuario.setPromotor("");
		
		PACPR_IP pacpr_ip = new PACPR_IP();
		PCUPR_IP pcupr_ip = new PCUPR_IP();
		
		pacpr_ip.setCompanyNumber("60");
		pacpr_ip.setProductID("DDA");
		pacpr_ip.setAccountNumber((String)info.get("txtDDAHAB"));
		
		Segment[] cmd = { new Base(usuario), pacpr_ip };
		status = PASSRequest.execute(cmd);


		String txtCYS = pacpr_ip.getNumeroCYS();
		String txtRUC = pacpr_ip.getRUC();
		String txtSubproducto = pacpr_ip.getSubproducto();
		
		pcupr_ip.setNumeroCIS(pacpr_ip.getNumeroCYS());
		pcupr_ip.setCompanyNumber("60");
		
		Segment[] cmd1 = { new Base(usuario), pcupr_ip };
		status = PASSRequest.execute(cmd1);
				
		String txtNombreFisica = pcupr_ip.getNombreFisica();
		String txtNombreMoral = pcupr_ip.getNombreMoral();
		String txtNacionalidadCliente = pcupr_ip.getNacionalidadCliente();
		
		//IDDEVA-Se valida null
		if(null == pacpr_ip.getRUC()){
			info.put("txtCedCte", "Dato Inv�lido");
		}else{
			info.put("txtCedCte", pacpr_ip.getRUC());
		}
		if(null == pcupr_ip.getNombreFisica()){
			info.put("txtNomCte", "Dato Inv�lido");
		}else{
			info.put("txtNomCte", pcupr_ip.getNombreFisica());
		}
		if(null == pcupr_ip.getNacionalidadCliente()){
			info.put("paisCliente","Dato Inv�lido");
		}else{
			info.put("paisCliente", pcupr_ip.getNacionalidadCliente());
		}
    	if(null == pacpr_ip.getSubproducto()){
    		info.put("subProducto","Dato Inv�lido");
    	}else{
    		info.put("subProducto", pacpr_ip.getSubproducto());	
    	}
    	
    	
    	session.setAttribute("page.datasession", info);
		return status;
	}
}