// Transaccion 4151 - Cheques de Caja
package ventanilla.com.bital.sfb;

import com.bital.util.ParameterParser;
import com.bital.util.ParameterNotFoundException;

public class Group37 extends Transaction
{
  private long CheckDigit(long Cuenta, long Serial)
  {
    Cuenta = Cuenta / 10;
    long Div = Cuenta * 100 / Serial;
    Div = Div / 100;
    long V = Cuenta-Serial-Div;
    if( V < 0 )
      V = V * -1;
    long D = Serial % 10;
    long W = V % 1000;

    long C = W % 10;
    long B = (W / 10) % 10;
    long A = W / 100;

    long X = D + A;
    long Y = B + D;
    long Z = D + C;

    if( X >= 10 )
      X = X % 10;
    if( Y >= 10 )
      Y = Y % 10;
    if( Z >= 10 )
      Z = Z % 10;

    long Suma = X*100 + Y*10 + Z;
    return Suma;
  }

  public Header doAction(ParameterParser param)
    throws ParameterNotFoundException
  {
    // Instanciar el bean a utilizar
    Cheques cheque = new Cheques();
    
    // Ingresar datos requeridos    
    cheque.setTranAmt( param.getCString("txtMonto") );

    long suma = 0;
    int moneda = param.getInt("moneda");
    if( moneda == 1 )
    {
      cheque.setTranCur("N$");
      cheque.setAcctNo ("0103000023");
      suma = CheckDigit(0103000023L, param.getLong("txtSerial"));
    }
    else if( moneda == 2 )
    {
      cheque.setTranCur("US$");
      cheque.setAcctNo ("3902222222");
      suma = CheckDigit(3902222222L, param.getLong("txtSerial"));
    }

    cheque.setCheckNo( param.getString("txtSerial") );
    cheque.setTrNo   ( param.getString("lstBanco") );
    if( suma == param.getLong("txtCCodSeg") )
      cheque.setFees("0");
    else
      cheque.setFees("2");

    return cheque;
  }               
}
