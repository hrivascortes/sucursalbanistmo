package ventanilla.com.bital.sfb;

public class ChqCertif extends Header {
    private String txtDescRev = "";
    private String txtAcctNo  = "";
    private String txtTranAmt = "";
    private String txtTranCur = "";
    private String txtCheckNo = "";
    private String txtCashOut = "";
    private String txtMoamoun = "";
    private String txtFees    = "";
    private String txtTranDesc = "";
    private String txtTrNo1   = "";
    private String txtTrNo2   = "";
    
    public void setAcctNo(String newAcctNo) {
        txtAcctNo = newAcctNo;
    }
    public String getAcctNo() {
        return txtAcctNo + "*";
    }
    
    public void setTranAmt(String newTranAmt) {
        txtTranAmt = newTranAmt;
    }
    public String getTranAmt() {
        return txtTranAmt + "*";
    }
    
    public void setTranCur(String newTranCur) {
        txtTranCur = newTranCur;
    }
    public String getTranCur() {
        return txtTranCur + "*";
    }
    
    public void setCheckNo(String newCheckNo) {
        txtCheckNo = newCheckNo;
    }
    public String getCheckNo() {
        return txtCheckNo + "*";
    }
    
    public void setCashOut(String newCashOut) {
        txtCashOut = newCashOut;
    }
    public String getCashOut() {
        return txtCashOut + "*";
    }
    
    public void setMoamoun(String newMoamoun) {
        txtMoamoun = newMoamoun;
    }
    public String getMoamoun() {
        return txtMoamoun + "*";
    }
    
    public void setFees(String newFees) {
        txtFees = newFees;
    }
    public String getFees() {
        return txtFees + "*";
    }
    
    public void setTranDesc(String newTranDesc) {
        txtTranDesc = newTranDesc;
    }
    public String getTranDesc() {
        return txtTranDesc + "*";
    }
    
    public void setTrNo1(String newTrNo1) {
        txtTrNo1 = newTrNo1;
    }
    public String getTrNo1() {
        return txtTrNo1 + "*";
    }
    
    public void setTrNo2(String newTrNo2) {
        txtTrNo2 = newTrNo2;
    }
    public String getTrNo2() {
        return txtTrNo2 + "*";
    }
    
    public String getDescRev() {
        return txtDescRev;
    }
    
    public void setDescRev(String newDescRev) {
        txtDescRev = newDescRev;
    }
    public String toString() {
        return super.toString() + "*" + getAcctNo() + "*" + getTranAmt() + getTranCur() + getCheckNo() + "**" + getCashOut() + "*" + getMoamoun() + getFees() + getTranDesc() + "*******" + getTrNo1() + "******" + getTrNo2() + "**********************";
    }
}
