//*************************************************************************************************
//             Funcion: Bean Txn 0216
//      Modificado por: Jesus Emmanuel Lopez Rosales
//*************************************************************************************************
// CCN - 4360268 - 04/02/2005 - Se realizan adecuaciones para eliminar Agencia NY
// CCN - 4360424 - 03/02/2006 - Se agrega txn para divisas.
// CCN - 4360576 - 09/03/2007 - Modificaciones para proyecto OPMN
//*************************************************************************************************
package ventanilla.com.bital.sfb;

import com.bital.util.ParameterParser;
import com.bital.util.ParameterNotFoundException;

public class Txn_0372 extends Transaction
{
	public Header doAction(ParameterParser param)
		throws ParameterNotFoundException
	{
        String TE = param.getString("iTxn");
		FormatoB formatob = new FormatoB();
        formatob.setTxnCode("0372");
        formatob.setFromCurr("N$");
        formatob.setFormat("B");
        formatob.setAcctNo("0372");
        if(param.getStringNULL("montoIVA") != null){
          formatob.setTranAmt(param.getCString("montoIVA"));
          formatob.setCashIn(param.getCString("montoIVA"));
          
        }else if(param.getStringNULL("txtIvaMN") != null){
          formatob.setTranAmt(param.getCString("txtIvaMN"));
          formatob.setCashIn(param.getCString("txtIvaMN"));        	        
                
        }else if(param.getStringNULL("txtMtoIva") != null){
          formatob.setTranAmt(param.getCString("txtMtoIva"));
          formatob.setCashIn(param.getCString("txtMtoIva"));        	                
        
        }else if(param.getStringNULL("txtIVA0124") != null){
          formatob.setTranAmt(param.getCString("txtIVA0124"));
          formatob.setCashIn(param.getCString("txtIVA0124"));        	
        
        }else if(param.getStringNULL("txtIVA") != null){
          formatob.setTranAmt(param.getCString("txtIVA"));
          formatob.setCashIn(param.getCString("txtIVA"));        	
        
        }else{
         //System.out.println("Buscar el nombre del campo de IVA para esta Txn");      
		 System.out.println("");
        }
        
        if(TE.equals("0578")){
           formatob.setReferenc("IVA GIRO DOLARES");        
        }else if(TE.equals("0732")){
           formatob.setReferenc("IVA");
        }else if(TE.equals("0500") || TE.equals("4155")){
            formatob.setReferenc("ITBMS");
        }else{		
           formatob.setReferenc("IVA COM.CHQ.CAJA");
        }
        
		return formatob;
	}
}
