//************************************************************************************************* 
//             Funcion: Clase para txn 0200
//            Elemento: Balance.java
//          Creado por: Alejandro Gonzalez Castro
//      Modificado por: Juvenal R. Fernandez V.
//*************************************************************************************************
// CCN - 4360175 - 03/08/2004 - Se controla txn 0200 por suc solo para horario "C"
// CCN - 4360178 - 20/08/2004 - Se habilita para journalizar txn 'CON' al hacer txn 0200
// CCN - 4360189 - 03/09/2004 - Se cambia invoke al Diario para Control de Efectivo
// CCN - 4360334 - 17/06/2005 - Se elimina variable dbQualifier
//*************************************************************************************************

package ventanilla.com.bital.admin;

import java.util.Hashtable;

import javax.servlet.http.HttpSession;

import ventanilla.GenericClasses;
import ventanilla.com.bital.sfb.GerenteD;

public class Balance {
  private Hashtable balance;
  private Hashtable names;
  private int status = 0;
  private int column = 0;
  private int fila = 1;
  private String name = "";
  private String campo = "";

  private HttpSession session;

  public Balance(HttpSession sesion)
  {
      session = sesion;
  }

    public void ExecuteSql() 
    {
        String horario = (String)session.getAttribute("horario");
        String moneda = (String)session.getAttribute("page.moneda");
        String suc = (String)session.getAttribute("branch");
        String cajero = (String)session.getAttribute("teller");
        String ip = (String)session.getAttribute("dirip");

        GenericClasses gc = new GenericClasses();

        if(horario.equals("C")) {
            GerenteD GD = new GerenteD();
            GD.setTxnCode("CON ");
            GD.setBranch(suc);
            GD.setTeller(cajero);
            GD.setSupervisor(cajero);
            GD.setTotalsId("DOT-CON");
            GD.setCurrCod1("VISUALIZO DOTACIONES Y CONCENTRACIONES");
            String consecliga =  gc.stringFormat(java.util.Calendar.HOUR_OF_DAY, 2) + gc.stringFormat(java.util.Calendar.MINUTE, 2) + gc.stringFormat(java.util.Calendar.SECOND, 2);
            Diario diario = new Diario();
            String sMessage = diario.invokeDiario(GD, consecliga, ip, "01", session);
        }
        
        String statementPS = com.bital.util.ConnectionPoolingManager.getPostFixUR();    
        java.sql.Connection pooledConnection = com.bital.util.ConnectionPoolingManager.getPooledConnection( );
        java.sql.Statement stmt = null;
        java.sql.ResultSet rs = null;
        java.sql.ResultSetMetaData rsmd = null;
        try {
            if(pooledConnection != null) {
                stmt = pooledConnection.createStatement();
                StringBuffer cadsql = new StringBuffer();
                if(horario.equals("C")) {
                    cadsql.append("select d_txn, d_operador, d_efectivo, d_Consecutivo from TW_DIARIO_ELECTRON where d_txn='0201' and d_divisa='"+ gc.getDivisa(moneda) + "' and d_estatus = 'A' and d_fechoper = '" + gc.getDate(1) + "' and D_SUCURSAL = '" + suc + "' ");
                    cadsql.append("UNION ALL ");
                    cadsql.append("select d_txn, d_operador, d_efectivo, d_Consecutivo from TW_DIARIO_ELECTRON where d_txn='0202' and d_divisa='"+ gc.getDivisa(moneda) + "' and d_estatus = 'A' and d_fechoper = '" + gc.getDate(1) + "' and D_SUCURSAL = '" + suc + "' ");
                }
                else {
                    cadsql.append("select d_txn, d_operador, d_efectivo, d_Consecutivo from TW_DIARIO_ELECTRON where d_txn='0201' and d_divisa='"+ gc.getDivisa(moneda) + "' and d_estatus = 'A' and d_fechoper = '" + gc.getDate(1) + "' and D_OPERADOR = '" + cajero + "' ");
                    cadsql.append("UNION ALL ");
                    cadsql.append("select d_txn, d_operador, d_efectivo, d_Consecutivo from TW_DIARIO_ELECTRON where d_txn='0202' and d_divisa='"+ gc.getDivisa(moneda) + "' and d_estatus = 'A' and d_fechoper = '" + gc.getDate(1) + "' and D_OPERADOR = '" + cajero + "' ");
                }
                cadsql.append(" order by d_Consecutivo asc" + statementPS);
                
                rs = stmt.executeQuery(cadsql.toString());
                rsmd = rs.getMetaData();
                balance = new Hashtable();
                names = new Hashtable();
                column = rsmd.getColumnCount();
                while(rs.next()) {
                    for(int i = 1; i <= column; i++) {
                        campo = rs.getString(i);
                        name = rsmd.getColumnName(i);
                        balance.put("f" + fila + "c" + i,campo);
                        names.put("f" + fila + "c" + i, name);
                    }
                    fila ++;
                }
            }
        }
        catch(java.sql.SQLException sqlException) {
            System.out.println("Error Balance::ExecuteSql [" + sqlException + "]");
            status = 1;
            
        }
        finally {
            try {
                if(rsmd != null){
                    rsmd = null;
                }
                if(rs != null) {
                    rs.close();
                    rs = null;
                }
                if(stmt != null) {
                    stmt.close();
                    stmt = null;
                }
                if(pooledConnection != null) {
                    pooledConnection.close();
                    pooledConnection = null;
                }
            }
            catch(java.sql.SQLException sqlException) {
                System.out.println("Error FINALLY [" + sqlException + "]");
                status = 1;
            }
        }
    }
    
    public int getStatus() {
        return status;
    }
    public Hashtable getcampos() {
        return balance;
    }
    public Hashtable getnames() {
        return names;
    }
    public int getcolumn() {
        return column;
    }
    public int getfila() {
        return fila;
    }
}
