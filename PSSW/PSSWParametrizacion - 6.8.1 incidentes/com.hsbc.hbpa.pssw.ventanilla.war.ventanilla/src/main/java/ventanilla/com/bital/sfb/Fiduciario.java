package ventanilla.com.bital.sfb;

import ventanilla.GenericClasses;

public class Fiduciario extends Header {
    private String txtDescRev = null;
    private String txtEffDate = null;
    private String txtAcctNo = null;
    private String txtNatcurr = null;
    private String txtTranAmt = null;
    private String txtTranCur = null;
    private String txtCheckNo = null;
    private String txtTrNo = null;
    private String txtCashIn = null;
    private String txtCashOut = null;
    private String txtDraftAm = null;
    private String txtMoamoun = null;
    private String txtFees = null;
    private String txtTranDesc = null;
    private String txtHoldays = null;
    private String txtHoldAmt = null;
    private String txtGrptyp1 = null;
    private String txtAmount1 = null;
    private String txtApplid1 = null;
    private String txtAcctNo1 = null;
    private String txtCheckNo1 = null;
    private String txtTrNo1 = null;
    private String txtHolDays1 = null;
    private String txtGrptyp2 = null;
    private String txtAmount2 = null;
    private String txtApplid2 = null;
    private String txtAcctNo2 = null;
    private String txtCheckNo2 = null;
    private String txtTrNo2 = null;
    private String txtHolDays2 = null;
    private String txtGrptyp3 = null;
    private String txtAmount3 = null;
    private String txtApplid3 = null;
    private String txtAcctNo3 = null;
    private String txtCheckNo3 = null;
    private String txtTrNo3 = null;
    private String txtHolDays3 = null;
    private String txtGrptyp4 = null;
    private String txtAmount4 = null;
    private String txtApplid4 = null;
    private String txtAcctNo4 = null;
    private String txtCheckNo4 = null;
    private String txtTrNo4 = null;
    private String txtHolDays4 = null;
    private String txtGrptyp5 = null;
    private String txtAmount5 = null;
    private String txtApplid5 = null;
    private String txtAcctNo5 = null;
    private String txtCheckNo5 = null;
    private String txtTrNo5 = null;
    private String txtHolDays5 = null;
    
    public Fiduciario() {
        txtEffDate = "";
        txtAcctNo = "";
        txtNatcurr = "";
        txtTranAmt = "";
        txtTranCur = "";
        txtCheckNo = "";
        txtTrNo = "";
        txtCashIn = "";
        txtCashOut = "";
        txtDraftAm = "";
        txtMoamoun = "";
        txtFees = "";
        txtTranDesc = "";
        txtHoldays = "";
        txtHoldAmt = "";
        txtGrptyp1 = "";
        txtAmount1 = "";
        txtApplid1 = "";
        txtAcctNo1 = "";
        txtCheckNo1 = "";
        txtTrNo1 = "";
        txtHolDays1 = "";
        txtGrptyp2 = "";
        txtAmount2 = "";
        txtApplid2 = "";
        txtAcctNo2 = "";
        txtCheckNo2 = "";
        txtTrNo2 = "";
        txtHolDays2 = "";
        txtGrptyp3 = "";
        txtAmount3 = "";
        txtApplid3 = "";
        txtAcctNo3 = "";
        txtCheckNo3 = "";
        txtTrNo3 = "";
        txtHolDays3 = "";
        txtGrptyp4 = "";
        txtAmount4 = "";
        txtApplid4 = "";
        txtAcctNo4 = "";
        txtCheckNo4 = "";
        txtTrNo4 = "";
        txtHolDays4 = "";
        txtGrptyp5 = "";
        txtAmount5 = "";
        txtApplid5 = "";
        txtAcctNo5 = "";
        txtCheckNo5 = "";
        txtTrNo5 = "";
        txtHolDays5 = "";
    }
    
    public String getEffDate() {
        return txtEffDate + "*";
    }
    
    public String getAcctNo() {
        return txtAcctNo + "*";
    }
    
    public String getNatCurr() {
        return txtNatcurr + "*";
    }
    
    public String getTranAmt() {
        return txtTranAmt + "*";
    }
    
    public String getTranCur() {
        return txtTranCur + "*";
    }
    
    public String getCheckNo() {
        return txtCheckNo + "*";
    }
    
    public String getTrNo() {
        return txtTrNo + "*";
    }
    
    public String getCashIn() {
        return txtCashIn + "*";
    }
    
    public String getCashOut() {
        return txtCashOut + "*";
    }
    
    public String getDraftAm() {
        return txtDraftAm + "*";
    }
    
    public String getMoamoun() {
        return txtMoamoun + "*";
    }
    
    public String getFees() {
        return txtFees + "*";
    }
    
    public String getTranDesc() {
        return txtTranDesc + "*";
    }
    
    public String getHoldays() {
        return txtHoldays + "*";
    }
    
    public String getHoldAmt() {
        return txtHoldAmt + "*";
    }
    
    public String getGrpTyp1() {
        return txtGrptyp1 + "*";
    }
    
    public String getAmount1() {
        return txtAmount1 + "*";
    }
    
    public String getApplid1() {
        return txtApplid1 + "*";
    }
    
    public String getAcctNo1() {
        return txtAcctNo1 + "*";
    }
    
    public String getCheckNo1() {
        return txtCheckNo1 + "*";
    }
    
    public String getTrNo1() {
        return txtTrNo1 + "*";
    }
    
    public String getHolDays1() {
        return txtHolDays1 + "*";
    }
    
    public String getGrpTyp2() {
        return txtGrptyp2 + "*";
    }
    
    public String getAmount2() {
        return txtAmount2 + "*";
    }
    
    public String getApplid2() {
        return txtApplid2 + "*";
    }
    
    public String getAcctNo2() {
        return txtAcctNo2 + "*";
    }
    
    public String getCheckNo2() {
        return txtCheckNo2 + "*";
    }
    
    public String getTrNo2() {
        return txtTrNo2 + "*";
    }
    
    public String getHolDays2() {
        return txtHolDays2 + "*";
    }
    
    public String getGrpTyp3() {
        return txtGrptyp3 + "*";
    }
    
    public String getAmount3() {
        return txtAmount3 + "*";
    }
    
    public String getApplid3() {
        return txtApplid3 + "*";
    }
    
    public String getAcctNo3() {
        return txtAcctNo3 + "*";
    }
    
    public String getCheckNo3() {
        return txtCheckNo3 + "*";
    }
    
    public String getTrNo3() {
        return txtTrNo3 + "*";
    }
    
    public String getHolDays3() {
        return txtHolDays3 + "*";
    }
    
    public String getGrpTyp4() {
        return txtGrptyp4 + "*";
    }
    
    public String getAmount4() {
        return txtAmount4 + "*";
    }
    
    public String getApplid4() {
        return txtApplid4 + "*";
    }
    
    public String getAcctNo4() {
        return txtAcctNo4 + "*";
    }
    
    public String getCheckNo4() {
        return txtCheckNo4 + "*";
    }
    
    public String getTrNo4() {
        return txtTrNo4 + "*";
    }
    
    public String getHolDays4() {
        return txtHolDays4 + "*";
    }
    
    public String getGrpTyp5() {
        return txtGrptyp5 + "*";
    }
    
    public String getAmount5() {
        return txtAmount5 + "*";
    }
    
    public String getApplid5() {
        return txtApplid5 + "*";
    }
    
    public String getAcctNo5() {
        return txtAcctNo5 + "*";
    }
    
    public String getCheckNo5() {
        return txtCheckNo5 + "*";
    }
    
    public String getTrNo5() {
        return txtTrNo5 + "*";
    }
    
    public String getHolDays5() {
        return txtHolDays5 + "*";
    }
    
    public void setEffDate(String s) {
        txtEffDate = s;
    }
    
    public void setAcctNo(String s) {
        txtAcctNo = s;
    }
    
    public void setNatCurr(String s) {
        txtNatcurr = s;
    }
    
    public void setTranAmt(String s) {
        txtTranAmt = s;
    }
    
    public void setTranCur(String s) {
		GenericClasses gc = new GenericClasses();
        txtTranCur = gc.getDivisa(s);
    }
    
    public void setCheckNo(String s) {
        txtCheckNo = s;
    }
    
    public void setTrNo(String s) {
        txtTrNo = s;
    }
    
    public void setCashIn(String s) {
        txtCashIn = s;
    }
    
    public String setCashOut(String s) {
        return txtCashOut = s;
    }
    
    public String setDraftAm(String s) {
        return txtDraftAm = s;
    }
    
    public void setMoamoun(String s) {
        txtMoamoun = s;
    }
    
    public void setFees(String s) {
        txtFees = s;
    }
    
    public void setTranDesc(String s) {
        txtTranDesc = s;
    }
    
    public void setHoldays(String s) {
        txtHoldays = s;
    }
    
    public void setHoldAmt(String s) {
        txtHoldAmt = s;
    }
    
    public void setGrptyp1(String s) {
        txtGrptyp1 = s;
    }
    
    public void setAmount1(String s) {
        txtAmount1 = s;
    }
    
    public void setApplid1(String s) {
        txtApplid1 = s;
    }
    
    public void setAcctNo1(String s) {
        txtAcctNo1 = s;
    }
    
    public void setCheckNo1(String s) {
        txtCheckNo1 = s;
    }
    
    public void setTrNo1(String s) {
        txtTrNo1 = s;
    }
    
    public void setHolDays1(String s) {
        txtHolDays1 = s;
    }
    
    public void setGrptyp2(String s) {
        txtGrptyp2 = s;
    }
    
    public void setAmount2(String s) {
        txtAmount2 = s;
    }
    
    public void setApplid2(String s) {
        txtApplid2 = s;
    }
    
    public void setAcctNo2(String s) {
        txtAcctNo2 = s;
    }
    
    public void setCheckNo2(String s) {
        txtCheckNo2 = s;
    }
    
    public void setTrNo2(String s) {
        txtTrNo2 = s;
    }
    
    public void setHolDays2(String s) {
        txtHolDays2 = s;
    }
    
    public void setGrptyp3(String s) {
        txtGrptyp3 = s;
    }
    
    public void setAmount3(String s) {
        txtAmount3 = s;
    }
    
    public void setApplid3(String s) {
        txtApplid3 = s;
    }
    
    public void setAcctNo3(String s) {
        txtAcctNo3 = s;
    }
    
    public void setCheckNo3(String s) {
        txtCheckNo3 = s;
    }
    
    public void setTrNo3(String s) {
        txtTrNo3 = s;
    }
    
    public void setHolDays3(String s) {
        txtHolDays3 = s;
    }
    
    public void setGrptyp4(String s) {
        txtGrptyp4 = s;
    }
    
    public void setAmount4(String s) {
        txtAmount4 = s;
    }
    
    public void setApplid4(String s) {
        txtApplid4 = s;
    }
    
    public void setAcctNo4(String s) {
        txtAcctNo4 = s;
    }
    
    public void setCheckNo4(String s) {
        txtCheckNo4 = s;
    }
    
    public void setTrNo4(String s) {
        txtTrNo4 = s;
    }
    
    public void setHolDays4(String s) {
        txtHolDays4 = s;
    }
    
    public void setGrptyp5(String s) {
        txtGrptyp5 = s;
    }
    
    public void setAmount5(String s) {
        txtAmount5 = s;
    }
    
    public void setApplid5(String s) {
        txtApplid5 = s;
    }
    
    public void setAcctNo5(String s) {
        txtAcctNo5 = s;
    }
    
    public void setCheckNo5(String s) {
        txtCheckNo5 = s;
    }
    
    public void setTrNo5(String s) {
        txtTrNo5 = s;
    }
    
    public void setHolDays5(String s) {
        txtHolDays5 = s;
    }
    
    public String getDescRev() {
        return txtDescRev;
    }
    
    public void setDescRev(String newDescRev) {
        txtDescRev = newDescRev;
    }
    
    public String toString() {
        return super.toString() +
        getEffDate() + getAcctNo() + getNatCurr() + getTranAmt() + getTranCur() +
        getCheckNo() + getTrNo() + getCashIn() + getCashOut() + getDraftAm() +
        getMoamoun() + getFees() + getTranDesc() + getHoldays() + getHoldAmt() +
        getGrpTyp1() + getAmount1() + getApplid1() + getAcctNo1() + getCheckNo1() +
        getTrNo1() + getHolDays1() + getGrpTyp2() + getAmount2() + getApplid2() +
        getAcctNo2() + getCheckNo2() + getTrNo2() + getHolDays2() + getGrpTyp3() +
        getAmount3() + getApplid3() + getAcctNo3() + getCheckNo3() + getTrNo3() +
        getHolDays3() + getGrpTyp4() + getAmount4() + getApplid4() + getAcctNo4() +
        getCheckNo4() + getTrNo4() + getHolDays4() + getGrpTyp5() + getAmount5() +
        getApplid5() + getAcctNo5() + getCheckNo5() + getTrNo5() + getHolDays5();
    }
}