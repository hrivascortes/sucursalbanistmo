//*************************************************************************************************
//             Funcion: Clase que realiza la consulta de Campos a desplegar y los conserva en memoria
//          Creado por: Rutilo Zarco Reyes
//      Modificado por: Juvenal R. Fernandez V.
//*************************************************************************************************
// CCN -4360152- 21/06/2004 - Se crea la clase para realizar select y mantener los datos en memoria
// CCN - 4360334 - 17/06/2005 - Se elimina variable dbQualifier evitando enviar "." antes del nombre de la tabla
//*************************************************************************************************

package ventanilla.com.bital.beans;

import ventanilla.com.bital.util.DBQuery;

import java.util.Vector;
import java.util.Hashtable;

public class CamposDisp
{
    private Hashtable campomoneda = null;
    private Hashtable campos = null;
    private static CamposDisp instance = null;
    
    private CamposDisp() 
    {   
        campomoneda = new Hashtable();
        campos = new Hashtable();
        execute();
    }
    
    public static synchronized CamposDisp getInstance()
    {
        if(instance == null)
            instance = new CamposDisp();
        return instance;
    }
    
    public Vector getcamposdisp(String campo, String moneda)
    {
        Vector regcampos = new Vector();
        Vector tablacampos = new Vector();
        if(campomoneda.containsKey(campo + moneda))
            regcampos = (Vector)this.campomoneda.get(campo+moneda);
        else
          if(campos.containsKey(campo))
            regcampos = (Vector)this.campos.get(campo);
        tablacampos.add(regcampos);
        return tablacampos;
    }
    
    public void execute() 
    {
        String sql = null;
        sql = "SELECT C_NOM_CAMPO, C_MONEDA, D_DESCRIPCION, N_LONGITUD, D_VALIDAC_INIC, D_VALOR_INICIAL FROM ";        
        String clause = null;
        clause = " ORDER BY C_NOM_CAMPO";
        
        DBQuery dbq = new DBQuery(sql,"TC_CAMPOS_DISP", clause);
        Vector Regs = dbq.getRows();
        
        int size = Regs.size();
        Vector row = new Vector();
        String campo = null;
        String moneda = null;

        for(int i=0; i<size; i++)
        {
            row = (Vector)Regs.get(i);
            campo = (String)row.get(0);
            moneda = (String)row.get(1);
            
            campomoneda.put(campo+moneda, row);
            if (!campos.containsKey(campo))
              campos.put(campo,row);
        }
    }    
}
