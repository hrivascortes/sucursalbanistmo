package ventanilla.com.bital.util;

import ventanilla.com.bital.util.NSTokenizer;
import java.util.List;
import java.util.ArrayList;

public class PASSResponse
{
  private List items = null;
  private String token = null;

  public PASSResponse(String str)
  {
    this(str, "~");
  }

  public PASSResponse(String str, String token)
  {
    NSTokenizer tokens = new NSTokenizer(str, token);

    items = new ArrayList(256);
    while( tokens.hasMoreElements() )
    {
      String item = (String)tokens.nextToken();
      if( item == null )
        continue;

      items.add(item);
    }
  }

  public String[] parseItems(int count)
  {
    // Modificacion temporal para validar longitudes de respuesta
    if( count > items.size() )
      count = items.size();
    // Generar un String[] del tamaqo requerido
    String[] result = new String[count];
    for(int i=0; i<count; i++)
      result[i] = (String)items.remove(0);

    return result;
  }

  public String peek(int index)
  {
    if( index >= items.size() )
      return null;

    return (String)items.get(index);
  }
}
