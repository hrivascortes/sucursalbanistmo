//*************************************************************************************************
//          Funcion: Bean para el LOG de la TXN FIRM
//          Elemento: Txn_Firm.java
//          Creado por: Fredy Pe�a Moreno
//	Modificado por: Fredy Pe�a Moreno		
//*************************************************************************************************
// CCN - 4360617 - 08/06/2007 - Se cambia varaible de cta para guardar en el log
//*************************************************************************************************


package ventanilla.com.bital.sfb;

import com.bital.util.ParameterParser;
import com.bital.util.ParameterNotFoundException;
import ventanilla.GenericClasses;

public class Txn_Firm extends Transaction
{
	public Header doAction(ParameterParser param)
		throws ParameterNotFoundException
	{
		FormatoE formatoe = new FormatoE();
		GenericClasses gc = new GenericClasses();
		
		//getEffDate()|getAcctNo()|getTranAmt()|getReferenc()|getFeeAmoun()|
		//getIntern()|getIntWh()|getCashIn()|getCashOut()|getFromCurr()|getToCurr();
		
		formatoe.setTxnCode("FIRM");
		formatoe.setAcctNo(param.getString("txtDDACuentaFirm"));
		formatoe.setReferenc1(param.getString("racf"));
		formatoe.setReferenc2(param.getString("dirip"));
		String paramFirma = param.getString("tipoConsulta");
		if(paramFirma.equals("0"))
			formatoe.setReferenc3("CONSULTA DE FIRMA LOCAL");
		else if(paramFirma.equals("1"))
			formatoe.setReferenc3("CONSULTA DE FIRMA CENTRAL");
		else
			formatoe.setReferenc3("LA CUENTA NO TIENE IMAGENES");
		String moneda = gc.getDivisa(param.getString("moneda"));
		formatoe.setFromCurr(moneda);
		return formatoe;
	}
}