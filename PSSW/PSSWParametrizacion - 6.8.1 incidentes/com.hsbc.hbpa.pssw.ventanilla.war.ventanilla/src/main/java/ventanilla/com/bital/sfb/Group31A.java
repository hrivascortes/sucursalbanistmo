// Transaccion 0825 - ACDO
package ventanilla.com.bital.sfb;

import com.bital.util.ParameterParser;
import com.bital.util.ParameterNotFoundException;

public class Group31A extends Transaction
{
  public Header doAction(ParameterParser param)
    throws ParameterNotFoundException
  {
    // Instanciar el bean a utilizar
    Cheques cheque = new Cheques();
    
    // Ingresar datos requeridos
    cheque.setFormat("B");
    cheque.setAcctNo(param.getString("lstTipoDocto") + param.getString("txtFolio"));
    cheque.setTranAmt("000");
    cheque.setReferenc("0");
    cheque.setFeeAmoun("0");
    cheque.setIntern("0");
    cheque.setIntWh("0");
    cheque.setCashIn("000");
    
    int moneda = param.getInt("moneda");
    if( moneda == 1 )
      cheque.setFromCurr("N$");
    else if( moneda == 2 )
      cheque.setFromCurr("US$");
    else
      cheque.setFromCurr("UDI");

    return cheque;
  }
}
