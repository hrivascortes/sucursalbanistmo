//*************************************************************************************************
//             Funcion: Bean Txn 4209 Impresion de Cheque de Gerencia.
//             Elaborado por: YGX
//*************************************************************************************************

package ventanilla.com.bital.sfb;

import java.util.StringTokenizer;

import ventanilla.GenericClasses;
import com.bital.util.ParameterNotFoundException;
import com.bital.util.ParameterParser;


public class Txn_0274 extends Transaction
{		 
	public Header doAction(ParameterParser param)
		throws ParameterNotFoundException
	{				
        GenericClasses gc = new GenericClasses();   
        FormatoH formatoh = new FormatoH();         				
		ventanilla.com.bital.sfb.Cheques Cheques = new ventanilla.com.bital.sfb.Cheques();
		
		String cad_llave="";
		String data="";
	    String datatmp="";
	    String datatmp1="";
	    String datatmp2="";
	    String datatmp3="";
	    String datatmp4="";
	    String datatmp5="";
	    String datatmp6="";
	    String datatmp7="";  
	    String datatmp8="";
	    String datatmp9="";
	   
		cad_llave=param.getString("val_datos");
		//System.out.println("cad_llave"+cad_llave);
		
		StringTokenizer tok = new StringTokenizer(cad_llave,"~");
		while(tok.hasMoreTokens()) 
	   { 
	    datatmp = tok.nextToken();
	    datatmp1 = tok.nextToken();
	    //System.out.println(datatmp1);	   
	    datatmp2 = tok.nextToken(); 
	    datatmp3 = tok.nextToken();			   
		datatmp4 = tok.nextToken();		 
		datatmp5 = tok.nextToken();		 
		datatmp6 = tok.nextToken();		  
		datatmp7 = tok.nextToken();
		datatmp8 = tok.nextToken(); 
		datatmp9 = tok.nextToken(); 
	   }
		
		/*System.out.println(datatmp);
		System.out.println(datatmp1);
		System.out.println(datatmp2);
		System.out.println(datatmp3);
		System.out.println(datatmp4);
		System.out.println(datatmp5);
		System.out.println(datatmp6);
		System.out.println(datatmp7);
		System.out.println(datatmp8);
		System.out.println(datatmp9);*/
		
		
		formatoh.setTxnId("ODPA");
		formatoh.setTxnCode("0274");
	    formatoh.setFormat("H");   
	    
	    
	   
	    //Cuenta de Cheques de Gerencia
	    formatoh.setAcctNo("0");
	    //Cuenta de Cuenta informativa se env�a la cta del cargo
//	    formatoh.setAcctDeb("0103000023");	    
		formatoh.setAcctDeb("0100000018");	    
//	    formatoh.setTranDesc(param.getString("txtBeneficiario").trim());
	    formatoh.setTranDesc(datatmp.trim());	    
//	    formatoh.setTranAmt(gc.delCommaPointFromString(param.getString("txtMonto")).trim());
	    formatoh.setTranAmt(datatmp1);
	    
	    int moneda = param.getInt("moneda"); 
	    formatoh.setTranCur(gc.getDivisa(param.getString("moneda")));
	    
//   formatoh.setCheckNo(param.getCString("txtSerial"));	    	    
        formatoh.setCheckNo("0000000000");	    	    
	    formatoh.setCashIn("000");	    
	    formatoh.setDescRev("REV 0274");	    	   	

	   	//System.out.println(formatoh);
	   	
		return formatoh;
	}
	

}
