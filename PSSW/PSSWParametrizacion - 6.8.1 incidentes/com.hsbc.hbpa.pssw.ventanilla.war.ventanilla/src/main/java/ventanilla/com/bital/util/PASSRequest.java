package ventanilla.com.bital.util;

import com.bital.util.Gateway;
import com.bital.util.GatewayFactory;

public final class PASSRequest
{
  public static boolean execute(Segment[] cmd)
  {
    StringBuffer msg = new StringBuffer(512);
    for(int i=0; i<cmd.length; i++)
      msg.append(cmd[i].getMessage());

    Gateway cg = GatewayFactory.getGateway("PASS");
    String result = null;
    try
    {
      cg.write(new String(msg));
      result = cg.readLine();
    }
    catch(Exception e)
    {
      result = new String(msg);
    } 
    
    /*System.out.println(msg);
    System.out.println(result);*/

    ventanilla.com.bital.util.PASSResponse response = new ventanilla.com.bital.util.PASSResponse(result);
    for(int i=0; i<cmd.length; i++)
    {
      cmd[i].parse(response);
      if( cmd[i].getStatus() != 0 )
        return false;
    } 
    return true;
  }
}
