//*************************************************************************************************
//			   Funcion: Bean Txn 1161
//		Creado por: Guillermo Rodrigo Escand�n Soto
//*************************************************************************************************

//*************************************************************************************************
package ventanilla.com.bital.sfb;

import com.bital.util.ParameterParser;
import com.bital.util.ParameterNotFoundException;
import ventanilla.GenericClasses;

public class Txn_1161 extends Transaction
{
	public Header doAction(ParameterParser param)
		throws ParameterNotFoundException
	{
		FormatoA formatoa = new FormatoA();
		GenericClasses gc = new GenericClasses();
		
		formatoa.setTxnCode("1161");
		formatoa.setFormat("A");
		formatoa.setAcctNo(param.getString("txtDDACuenta"));
		formatoa.setTranAmt(param.getCString("txtMonto"));
		String moneda = gc.getDivisa(param.getString("moneda"));
		formatoa.setTranCur(moneda);
		String fecha1 = param.getString("txtFechaEfect");
		String fecha2 = param.getString("txtFechaSys");
		String txtFechaEfect =  gc.ValEffDate(fecha1, fecha2);
		formatoa.setEffDate(txtFechaEfect);
		if (param.getString("override") != null)
			if (param.getString("override").equals("SI"))
				formatoa.setOverride("3");
				
		return formatoa;
	}
}
