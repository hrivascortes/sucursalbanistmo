package ventanilla.com.bital.sfb;

public class Nomina extends Header
{
  private String txtEffDate  = "";
  private String txtAcctNo   = "";
  private String txtTranAmt  = "";
  private String txtTranCur  = "";
  private String txtCheckNo  = "";
  private String txtCashIn   = "";
  private String txtMoamoun = "";
  private String txtFees     = "";
  private String txtTranDesc = "";
  private String txtTrNo1    = "";
  private String txtTrNo2    = "";
  
  private String txtDescRev  = "";  

  public String getEffDate()
  {
	return txtEffDate + "*";
  }
  public void setEffDate(String newEffDate)
  {
	txtEffDate = newEffDate;
  }

  public String getAcctNo()
  {
	return txtAcctNo + "*";
  }
  public void setAcctNo(String newAcctNo)
  {
	txtAcctNo = newAcctNo;
  }

  public String getTranAmt()
  {
	return txtTranAmt + "*";
  }
  public void setTranAmt(String newTranAmt)
  {
	txtTranAmt = newTranAmt;
  }
  
  public String getTranCur()
  {
	return txtTranCur + "*";
  }
  public void setTranCur(String newTranCur)
  {
	txtTranCur = newTranCur;
  }
  
  public String getCheckNo()
  {
	return txtCheckNo + "*";
  }
  public void setCheckNo(String newCheckNo)
  {
	txtCheckNo = newCheckNo;
  }
  
  public String getCashIn()
  {
	return txtCashIn + "*";
  }
  public void setCashIn(String newCashIn)
  {
	txtCashIn = newCashIn;
  }
   
  public String getMoamoun()
  {
	return txtMoamoun + "*";
  }
  public void setMoamoun(String newMoamoun)
  {
	txtMoamoun = newMoamoun;
  }
  
  public String getFees()
  {
	return txtFees + "*";
  }
  public void setFees(String newFees)
  {
	txtFees = newFees;
  }
  
  public String getTranDesc()
  {
	return txtTranDesc + "*";
  }
  public void setTranDesc(String newTranDesc)
  {
	txtTranDesc = newTranDesc;
  }
  
  public String getTrNo1()
  {
	return txtTrNo1 + "*";
  }
  public void setTrNo1(String newTrNo1)
  {
	txtTrNo1 = newTrNo1;
  }
  
  public String getTrNo2()
  {
	return txtTrNo2 + "*";
  }
  public void setTrNo2(String newTrNo2)
  {
	txtTrNo2 = newTrNo2;
  }

  public String getDescRev()
  {
	return txtDescRev;
  }    
  
  public void setDescRev(String newDescRev)
  {
	txtDescRev = newDescRev;
  }   
    
  public String toString()
  {
	return super.toString()
	+ getEffDate()	+ getAcctNo()	+ "*"	+ getTranAmt()
	+ getTranCur()	+ getCheckNo()	+ "*"		+ getCashIn()
	+ "*"	+ "*"	+ getMoamoun()	+ getFees()
	+ getTranDesc()	+ "*"	+ "*"
	+ "*"	+ "*"	+ "*"	+ "*"	+ "*"	+ getTrNo1()	+ "*"
	+ "*"	+ "*"	+ "*"	+ "*"	+ "*"	+ getTrNo2()	+ "*"
	+ "*"	+ "*"	+ "*"	+ "*"	+ "*"	+ "*"	+ "*"
	+ "*"	+ "*"	+ "*"	+ "*"	+ "*"	+ "*"	+ "*"
	+ "*"	+ "*"	+ "*"	+ "*"	+ "*"	+ "*"	+ "*";
  }    
}
