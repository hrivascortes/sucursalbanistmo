//*************************************************************************************************
//Funcion: Bean para Posteo de SIB
//Creado por: Hugo Gabriel Rivas Cortes
//
//*************************************************************************************************
//CR262793-       - 01/07/2012 - Creacion de elemento para el proyecto Daily Cash
//*************************************************************************************************

package ventanilla.com.bital.admin;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.Hashtable;

import javax.servlet.http.HttpSession;

import ventanilla.GenericClasses;

import com.bital.util.ConnectionPoolingManager;

public class PostingSIB {

	public PostingSIB()
	{}
	GenericClasses GC = new GenericClasses();
	public String codigo = null;
	public String tipo = "";
	public void execute(HttpSession session)
	{
		Hashtable datasession = (Hashtable) session.getAttribute("page.datasession");
		Hashtable usrinfo = (Hashtable) session.getAttribute("userinfo");
		String temporal = "";
		temporal = (String) datasession.get("lstOperacion");

		String operacion = temporal.substring(temporal.indexOf(" ") + 1,
				temporal.length());
		temporal = (String) datasession.get("lstRubro");

		String rubro = temporal.substring(temporal.indexOf(" ") + 1, temporal
				.length());
		String ubicacion = "";
		
		String tipoUbicacion = (String) datasession.get("lstCuentaSIB");

		if (!tipoUbicacion.equals("05"))
			ubicacion = (String) datasession.get("txtCtaSIB");
		else if(tipoUbicacion.equals("05"))
			ubicacion = (String) datasession.get("lstCtaCon");
		else
			ubicacion = (String) datasession.get("txtCtaSIB");

//		String monto = GC.delCommaPointFromString(request.getParameter("txtMonto"));

		String monto = GC.delCommaPointFromString((String)datasession.get("txtMonto"));
		String nombreDepositante = (String) datasession.get("txtNomDep");

		nombreDepositante = nombreDepositante.replace('|', ' ');
		temporal = (String) datasession.get("lstNacDep");

		String nacionalidadDepositante = temporal.substring(temporal
				.indexOf(" ") + 1, 7);
		String nombreCliente = "";
		if (tipoUbicacion.equals("04") || tipoUbicacion.equals("05"))
			nombreCliente = (String) datasession.get("txtNomCte");
		else
			nombreCliente = (String) datasession.get("txtNomCte");

		temporal = (String) datasession.get("lstNacCte");

		String nacionalidadCliente = temporal.substring(temporal.indexOf(" ") + 1, 7);
		String cedulaDepositante = (String) datasession.get("txtCedDep");

		cedulaDepositante = cedulaDepositante.replace('|', ' ');
		String cedulaCliente = (String) datasession.get("txtCedCte");

		cedulaCliente = cedulaCliente.replace('|', ' ');
		String nombreFuncionario = (String) datasession.get("txtFunc");

		nombreFuncionario = nombreFuncionario.replace('|', ' ');
		String cargoFuncionario = (String) datasession.get("lstCargoFunc");

		String fechaSys = GC.getDate(8);
		temporal = (String) datasession.get("lstNacCte");

		String NacCliente = temporal.substring(temporal.indexOf(" ") + 1,
				temporal.length());
		temporal = (String) datasession.get("lstNacDep");

		String NacDepositante = temporal.substring(temporal.indexOf(" ") + 1,
				temporal.length());
		String d_ConsecLiga="";
		if(session.getAttribute("d_ConsecLiga")!=null){
			d_ConsecLiga = (String) session.getAttribute("d_ConsecLiga");
		}
		
			

		int i = insertSIB(d_ConsecLiga, operacion, rubro, ubicacion, monto,
				nombreDepositante, nacionalidadDepositante, cedulaDepositante,
				nombreCliente, cedulaCliente, nacionalidadCliente,
				nombreFuncionario, cargoFuncionario, (String) datasession
				.get("sucursal"), fechaSys, (String) datasession
				.get("teller"));
		if (i == 1) {
			session.setAttribute("txtCadImpresion", "SIB~" + operacion + "~"
					+ rubro + "~" + ubicacion + "~"
					+ (String) (String) datasession.get("txtMonto") + "~"
					+ nombreDepositante + "~" + NacDepositante + "~"
					+ cedulaDepositante + "~" + nombreCliente + "~"
					+ cedulaCliente + "~" + NacCliente + "~"
					+ nombreFuncionario + "~" + cargoFuncionario + "~"
					+ (String) datasession.get("sucursal") + "~" + fechaSys
					+ "~" + (String) datasession.get("teller") + "~");
		} 
	}

	private int insertSIB(String consecLiga, String operacion, String rubro, String ubicacion,
			String monto, String nom_depositante, String nac_depositante,
			String ced_depositante, String nom_titular, String id_titular,
			String nac_titular, String nom_funcionario, String car_funcionario,
			String sucursal, String fecha, String cajero) {
		Connection pooledConnection = ConnectionPoolingManager
				.getPooledConnection();
		PreparedStatement insert = null;
		int ResultInsert = 0;
		try {
			if (pooledConnection != null) {
				insert = pooledConnection
						.prepareStatement("INSERT INTO TW_FSIB (D_OPERACION,D_RUBRO,D_UBICACION,D_MONTO_OPER,D_NOM_DEPOSITANTE,D_NAC_DEPOSITANTE,D_CED_DEPOSITANTE,D_NOM_TITULAR,D_ID_TITULAR,D_NAC_TITULAR,D_NOM_FUNCIONARIO,D_CAR_FUNCIONARIO,D_SUCURSAL,D_FECHA,D_CAJERO,D_STATUS, D_CONSECLIGA) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
				insert.setString(17, consecLiga);
				insert.setString(1, operacion);
				insert.setString(2, rubro);
				insert.setString(3, ubicacion);
				insert.setString(4, monto);
				insert.setString(5, nom_depositante);
				insert.setString(6, nac_depositante);
				insert.setString(7, ced_depositante);
				insert.setString(8, nom_titular);
				insert.setString(9, id_titular);
				insert.setString(10, nac_titular);
				insert.setString(11, nom_funcionario);
				insert.setString(12, car_funcionario);
				insert.setString(13, sucursal);
				insert.setString(14, fecha);
				insert.setString(15, cajero);
				insert.setString(16, "1");
				ResultInsert = insert.executeUpdate();
			}
		} catch (java.sql.SQLException sqlException) {

			return 0;
		} finally {
			try {
				if (insert != null) {
					insert.close();
					insert = null;
				}
				if (pooledConnection != null) {
					pooledConnection.close();
					pooledConnection = null;
				}
			} catch (java.sql.SQLException sqlException) {
				System.out.println("Error Sp_Func_SIB::insertSIB FINALLY: ["
						+ sqlException.toString() + "] ");
			}
		}
		return ResultInsert;
	}

	private int insertSIB(String consecLiga, String operacion, String ubicacion, String monto,
			String nom_funcionario, String car_funcionario,
			String nom_depositante, String sucursal, String fecha, String cajero) {
		Connection pooledConnection = ConnectionPoolingManager
				.getPooledConnection();
		PreparedStatement insert = null;
		int ResultInsert = 0;
		try {
			if (pooledConnection != null) {
				insert = pooledConnection
						.prepareStatement("INSERT INTO TW_FSIB (D_OPERACION,D_RUBRO,D_UBICACION,D_MONTO_OPER,D_NOM_DEPOSITANTE,D_NAC_DEPOSITANTE,D_CED_DEPOSITANTE,D_NOM_TITULAR,D_ID_TITULAR,D_NAC_TITULAR,D_NOM_FUNCIONARIO,D_CAR_FUNCIONARIO,D_SUCURSAL,D_FECHA,D_CAJERO,D_STATUS, D_CONSECLIGA) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
				insert.setString(1, operacion);
				insert.setString(2, "");
				insert.setString(3, ubicacion);
				insert.setString(4, monto);
				insert.setString(5, nom_depositante);
				insert.setString(6, "");
				insert.setString(7, "");
				insert.setString(8, "");
				insert.setString(9, "");
				insert.setString(10, "");
				insert.setString(11, nom_funcionario);
				insert.setString(12, car_funcionario);
				insert.setString(13, sucursal);
				insert.setString(14, fecha);
				insert.setString(15, cajero);
				insert.setString(16, "1");
				insert.setString(17, consecLiga);
				ResultInsert = insert.executeUpdate();
			}
		} catch (java.sql.SQLException sqlException) {
			System.out.println("Sp_Func_SIB::insertSIB::" + sqlException);
			return 0;
		} finally {
			try {
				if (insert != null) {
					insert.close();
					insert = null;
				}
				if (pooledConnection != null) {
					pooledConnection.close();
					pooledConnection = null;
				}
			} catch (java.sql.SQLException sqlException) {
				System.out.println("Error Sp_Func_SIB::insertSIB FINALLY: ["
						+ sqlException.toString() + "] ");
			}
		}
		return ResultInsert;
	}


}


