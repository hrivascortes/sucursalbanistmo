package ventanilla;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import ventanilla.com.bital.admin.ConsultaTDI;

public class Group01TDI extends HttpServlet {

	public void doPost(HttpServletRequest request, HttpServletResponse response)
		throws ServletException, IOException {
        HttpSession session = request.getSession(false);
        ConsultaTDI consulta = new ConsultaTDI();
        consulta.execute(session,request);
        String respuesta = consulta.getrespuesta();
        session.setAttribute("page.txnresponse", respuesta);
        session.setAttribute("page.getoTxnView", "PageBuilderTDID");
        getServletContext().getRequestDispatcher("/servlet/ventanilla.Redirector").forward(request,response);
	}

	public void doGet(HttpServletRequest request, HttpServletResponse response)
		throws ServletException, IOException {
       doPost(request, response);
	}	
}
