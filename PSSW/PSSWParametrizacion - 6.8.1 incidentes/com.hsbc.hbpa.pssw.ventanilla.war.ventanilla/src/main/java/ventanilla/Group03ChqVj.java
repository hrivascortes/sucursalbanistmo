//*************************************************************************************************
//             Funcion: Clase que realiza txn Cheques de Viajero
//            Elemento: Group03ChqVj.java
//          Creado por: Alejandro Gonzalez Castro
//		Modificado por: Juvenal Fernandez
//*************************************************************************************************
// CCN - 4360189 - 03/09/2004 - Se cambia invoke al Diario para Control de Efectivo
// CCN - 4360268 - 04/02/2005 - Se realizan adecuaciones para eliminar Agencia NY
//*************************************************************************************************
package ventanilla;

import java.io.IOException;
import java.util.Hashtable;
import java.util.Vector;
import java.util.Stack;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import ventanilla.com.bital.sfb.ChqVjA;
import ventanilla.com.bital.admin.Diario;
import ventanilla.com.bital.util.NSTokenizer;

public class Group03ChqVj extends HttpServlet
{
  private String getItem(String newCadNum, int newOption)
  {
    int iPIndex = newCadNum.indexOf("CONSECUTIVO:") + 12, NumSaltos, Num = 0, Ban = 0;
    String nCadNum = new String("");
    NumSaltos = newOption;
    while (Num < NumSaltos) {
          if (newCadNum.charAt(iPIndex) == 0x20)
          {
             while(newCadNum.charAt(iPIndex) == 0x20)
                  iPIndex++;
             Num++;
          }
          else
              while (newCadNum.charAt(iPIndex) != 0x20)
                    iPIndex++;
    }
    while (newCadNum.charAt(iPIndex) != 0x20)
          nCadNum = nCadNum + newCadNum.charAt(iPIndex++);

   return nCadNum;
  }

  public void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
  {
    // Instanciar a los bean

    ChqVjA chqvja = new ChqVjA();
	GenericClasses cGenericas = new GenericClasses();
    resp.setContentType("text/plain");

    HttpSession session = req.getSession(false);
    Hashtable datasession = (Hashtable)session.getAttribute("page.datasession");

    String dirip = (String)session.getAttribute("dirip");

    String getMsg = "";

     // Se obtienen los datos del encabezado(estandar para todas las transacciones)
       String txtTxnCode    = (String)datasession.get("cTxn");
       String txtFormat     = "A";
       String txtBranch     = (String)datasession.get("sucursal");
       String txtTeller     = (String)datasession.get("teller");//"000108";
       String txtSupervisor = (String)datasession.get("teller");//"000101";
       String txtBackOut    = "0";
       String txtOverride   = "0";
       String txtCurrentDay = "0";

       if ((String)datasession.get("supervisor")!= null)
           txtSupervisor = (String)datasession.get("supervisor");

      // Se establesen por medio del BEAN los datos del encabezado
       chqvja.setFormat(txtFormat);
       chqvja.setBranch(txtBranch);
       chqvja.setTeller(txtTeller);
       chqvja.setSupervisor(txtSupervisor);
       chqvja.setBackOut(txtBackOut);
       chqvja.setOverride(txtOverride);
       chqvja.setCurrentDay(txtCurrentDay);

       String TXN = (String)datasession.get("iTxn");

       // Se Obtienen los datos para la transacci"n

       String txtMonto      = cGenericas.delCommaPointFromString((String)datasession.get("txtMonto"));
       String txtMoneda     = (String)datasession.get("moneda");
       String txtDescrip    = "CARGO POR EXP. DE A.E.TRAVELER CHQS.";
       String txtCasoEsp    = "5353";

       // variables 5359 RAP
       String txtMontoTot = "";
       long MontoCuenta = 0;
       long MontoTotal = 0;


       if(TXN.equals("5503"))
       {
           txtMontoTot = (String)datasession.get("MontoTotal");
           if( txtMontoTot != null )
           MontoTotal = Long.parseLong(txtMontoTot);

           MontoCuenta= Long.parseLong(cGenericas.delCommaPointFromString((String)datasession.get("txtCuenta")));

           // RFC A 19 POSICIONES JUSTIFICADO A LA IZQ
            // IVA 14 POSICIONES INCLUYENDO COMAS Y PUNTO DECIMAL JUSTIFICADO A LA DER
            String CF = (String)datasession.get("lstCF");
            if (CF.equals("1"))
            {
			String RFC = (String)datasession.get("txtRFC");
			int longitud = RFC.length();
			for (int i=longitud; i<19; i++)
			{
				RFC = RFC + " ";
			}
			String IVA = (String)datasession.get("txtIVA");
			longitud = IVA.length();
			for (int i=longitud; i<14; i++)
			{
				IVA = " " + IVA;
			}
			txtDescrip = "-CGO " + RFC + " " + IVA;
            }
            else
			txtDescrip = "CARGO POR COBRANZA";
       }

       // Se establesen por medio del BEAN los datos para la transacci"n.
       String DescRev = "";
       if ( txtTxnCode.equals("V353"))
       {
          String txtFees="8";   //Constante
          String txtDDACuenta  = (String)datasession.get("txtDDACuenta2");
          //String txtTrNo1      = (String)datasession.get("txtCodSeg");
		  String txtTrNo1      = "0000";
          String txtTrNo2      = (String)datasession.get("txtCveTran");
          String txtCheckNo    = (String)datasession.get("txtSerial2");
          txtTxnCode = "5353";
          DescRev = "REV. " + txtDescrip;
          if(DescRev.length() > 40)
              DescRev = DescRev.substring(0,40);
          chqvja.setDescRev(DescRev);

          chqvja.setTrNo2(txtTrNo1.substring(0,3));      // Codigo de seguridad
          chqvja.setTrNo1(txtTrNo2.substring(2,5));      // Numero de plaza
          chqvja.setCheckNo(txtCheckNo);                 //No de cheque
          chqvja.setAcctNo(txtDDACuenta.substring(1,11));//No de Cuenta
          chqvja.setFees(txtFees);

          if ( (String)datasession.get("override") != null )
             if (datasession.get("override").toString().equals("SI"))
                 chqvja.setOverride("3");
       }
       else
       {
          // Descripcion para Reverso
          DescRev = "REV. " + txtDescrip;
          if(DescRev.length() > 40)
              DescRev = DescRev.substring(0,40);
          chqvja.setDescRev(DescRev);

          String txtDDACuenta  = (String)datasession.get("txtDDACuenta");
          chqvja.setAcctNo(txtDDACuenta);//No de Cuenta
       }

       chqvja.setTxnCode(txtTxnCode);
       chqvja.setTranAmt(txtMonto);                   //Monto Total de la Txn.
       chqvja.setTranCur(txtMoneda);                  //Tipo de Moneda con que se opera la Txn.
       chqvja.setTranDesc(txtDescrip);                //Descripcion

        Diario diario = new Diario();
        String sMessage = diario.invokeDiario(chqvja, (String)session.getAttribute("d_ConsecLiga"), dirip, txtMoneda, session);

        session.setAttribute("page.txnresponse", sMessage);

       if ( sMessage.charAt(0) == '0' )
       {
          MontoTotal = MontoTotal + Long.parseLong(txtMonto);
          datasession.put("MontoTotal",new Long(MontoTotal).toString());

       }

       if (txtTxnCode.equals("5353")) {
          if ( sMessage.charAt(0) == 'T' )                 // Error en Linea
              txtCasoEsp = txtCasoEsp + "~ERR~";
          else {
               NSTokenizer parser = new NSTokenizer((String)sMessage, "~");
               Vector cics_resp = new Vector();

               while ( parser.hasMoreTokens() ) {
      	             String token = parser.nextToken();
                     if ( token == null )
                        continue;
                     cics_resp.addElement(token);
               }
               txtCasoEsp = txtCasoEsp + "~" + getItem((String)cics_resp.elementAt(3), 1) + "~" ;
          }
          datasession.put("txtCasoEsp",txtCasoEsp);
       }

       if(TXN.equals("5503"))
       {
            Stack flujotxn = (Stack)session.getAttribute("page.flujotxn");

            if (MontoTotal != MontoCuenta)
            {

                flujotxn.push("5359");

                session.setAttribute("page.flujotxn", flujotxn);
                datasession.remove("txtDDACuenta");
                datasession.remove("txtMonto");
                datasession.remove("txtRFC");
                datasession.remove("txtIVA");
            }
      }

       getServletContext().getRequestDispatcher("/servlet/ventanilla.Redirector").forward(req,resp);

  }// doPost
  public void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
  {
    doPost(req, resp);
  }// doGet

}// de la clase
