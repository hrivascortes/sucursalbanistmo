//*************************************************************************************************
//			 Funcion: Clase que realiza la consulta de tipos de cambio para la firma
//			Elemento: ObtainForeignExchange.java
//		  Creado por: Alejandro Gonzalez Castro
//	  Modificado por: Fredy Pe�a Moreno
//*************************************************************************************************
//CCN - 4360291 - 18/03/2005 - Se eliminan consultas en EUROS y YENS en el Login  
//CCN - 4360297 - 23/03/2005 - Se cambia CCN por peticion de QA, CCN anterior 4360291 
// CCN - 4360368 - 02/09/2005 - Se elimina sleep    
// CCN - 4360374 - 08/09/2005 - Se genera nuevo paquete a peticion de QA.  
// CCN - 4620048 - 14/12/2007 - Se realizan modificaciones para compraventa
//*************************************************************************************************

package ventanilla;

public class ObtainForeignExchange{
    private java.util.Vector foreignExchanges = new java.util.Vector();
    private java.util.Hashtable obtainedExchanges = new java.util.Hashtable();
    private String currentBranch = null;
    private String currentTeller = null;
    private String currentTellerRegister = null;
    private ventanilla.com.bital.sfb.CompraVenta currentExchange = new ventanilla.com.bital.sfb.CompraVenta();

    public ObtainForeignExchange(){
          foreignExchanges.add("EUR");
		  foreignExchanges.add("GBP");
    }

    public String extractForeignExchange(String returnStream){
        int consecutiveIndex = returnStream.indexOf("CONSECUTIVO");

        String computedValue = returnStream.substring(consecutiveIndex + 234, consecutiveIndex + 234 + 16);
        java.text.DecimalFormat reformattedValue = new java.text.DecimalFormat("#,###.0000");
        returnStream = reformattedValue.format((double)(java.lang.Double.parseDouble(computedValue) / 1000000000.00));
        return returnStream;
    }

    public void doForeignExchanges(){

		String currencyOptions[] = {"V", "C"};
        short currentCurrency = -1;
        currentExchange.setTxnCode("0823");
        currentExchange.setBranch(this.currentBranch);
        currentExchange.setTeller(this.currentTeller);
        currentExchange.setSupervisor(this.currentTeller);
        currentExchange.setFormat( "G" );
        currentExchange.setCheckNo("00000");
        currentExchange.setComAmt("000");
        currentExchange.setIVAAmt("0");
        int i = 0;
        while(++currentCurrency < foreignExchanges.size()){
            for(int currencyOptionsIndex = 0; currencyOptionsIndex < 2; currencyOptionsIndex++){
                String currentValue = (String)foreignExchanges.get(currentCurrency);
                currentExchange.setService(currencyOptions[currencyOptionsIndex]);
                currentExchange.setGStatus( "1" );
                currentExchange.setToCurr(currentValue);
                currentExchange.setTranAmt("1");
                currentExchange.setAcctNo(this.currentTellerRegister);
                currentExchange.setCtaBenef(this.currentTellerRegister);
                currentExchange.execute();
                String returnStream = currentExchange.getMessage();
                if(returnStream.startsWith("0")){
                    String exchangeKey = currentValue + currencyOptions[currencyOptionsIndex];
                    obtainedExchanges.put(exchangeKey, this.extractForeignExchange(returnStream));                   
                }else
                {	
					obtainedExchanges.put(currentValue + currencyOptions[currencyOptionsIndex], "N.D.");
                }
            }           
            
        }
    }

    public void setBranch(String currentBranch){
        this.currentBranch = currentBranch;
    }
    public void setTeller(String currentTeller){
        this.currentTeller = currentTeller;
    }
    public void setTellerRegister(String currentTellerRegister){
        this.currentTellerRegister = currentTellerRegister;
    }
    public java.util.Hashtable obtainedExchanges(){
        return this.obtainedExchanges;
    }
    public static void main(String inputArguments[]){
        ventanilla.ObtainForeignExchange ofe = new ventanilla.ObtainForeignExchange();
        ofe.setBranch("00002");
        ofe.setTeller("000249");
        ofe.setTellerRegister("0431866");
        ofe.doForeignExchanges();
        //System.out.println("mon "+ofe.obtainedExchanges());
    }
}
