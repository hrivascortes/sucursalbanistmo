//*************************************************************************************************
//             Funcion: Clase para consulta de firmas locales
//            Elemento: BajasServlet.java
//          Creado por: Juan Gaona
//      Modificado por: Juvenal R. Fernandez V.
//*************************************************************************************************
// CCN - 4360180 - 20/08/2004 - Se crea clase para Consulta de Firmas Locales
// CCN - 4360334 - 17/06/2005 - Se elimina variable dbQualifier
//*************************************************************************************************

package ventanilla;
import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ventanilla.com.bital.admin.OperacionesDBBean;

public class BajasServlet extends HttpServlet
{
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, NullPointerException
    {		
    	String statementPS = com.bital.util.ConnectionPoolingManager.getStatementPostFix();		
        String strAccion = "SELECT C_NUMERO_CUENTA FROM TC_CUENTAS_ESPLS WHERE c_tipo='03'" + statementPS;
     	OperacionesDBBean rbQuery = new OperacionesDBBean();		
        String strRespuesta = rbQuery.EjecutaQuery(strAccion);
        response.setContentType("text/html");        
        //OutputStream os = response.getOutputStream();
        PrintWriter pwRespm = response.getWriter();   
        pwRespm.print(strRespuesta);
        //os.write(strRespuesta.getBytes(), 0, strRespuesta.length());
        //os.close();				
        pwRespm.close();
    }
  
    public void doPost(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException
    {
        doGet(request, response);
    }
}
