//*************************************************************************************************
//             Funcion: Clase que realiza la consulta de Cuentas especiales
//          Creado por: Juan Carlos Gaona
// Ultima Modificacion: 24/06/2004
//      Modificado por: Juvenal R. Fernandez V.
//*************************************************************************************************
// CCN -4360160- 30/06/2004 - Se agrega persistencia de datos en firmas
//*************************************************************************************************

package ventanilla;
import java.io.IOException;
import java.io.OutputStream;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ventanilla.com.bital.beans.CuentasEsp;


public class GetCtaEsp extends HttpServlet
{
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, NullPointerException
	{		
		String strParametro = request.getQueryString();		
		CuentasEsp ctaesp = CuentasEsp.getInstance();
                String strRespuesta = ctaesp.getCuentaEsp( strParametro.substring(6,16));
                
		response.setContentType("text/html");
		OutputStream os = response.getOutputStream();
		os.write(strRespuesta.getBytes(), 0, strRespuesta.length());
		os.close();				
	}
  
	public void doPost(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException
	{
		doGet(request, response);
	}
}
