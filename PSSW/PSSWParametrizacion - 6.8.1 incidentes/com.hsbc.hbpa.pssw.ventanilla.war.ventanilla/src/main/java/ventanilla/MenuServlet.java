//*************************************************************************************************
//             Funcion: Clase que establece los datos necesarios para la construccion del menu
//          Creado por: Alejandro Gonzalez Castro.
//      Modificado por: Jes�s Emmanuel L�pez Rosales
//*************************************************************************************************
// CCN - 4360152 - 21/06/2004 - Se modifica el acceso a los datos para obtenerlos de memoria
// CCN - 4360169 - 12/07/2004 - Se corrige problema de impresion del Diario en sesion
// CCN - 4360342 - 08/07/2005 - Se realizan modificaciones para departamentos.
// CCN - 4360364 - 19/08/2005 - Se controlan departamentos por Centro de Costos.
// CCN - 4360437 - 24/02/2006 - Se realizan modifiaciones para mostrar txns
// CCN - 4360510 - 08/09/2006 - Valida que tipo de menu se va a pintar.
// CCN - 4360593 - 20/04/2007 - Se elimina varibale de session idFlujo
//*************************************************************************************************

package ventanilla;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import ventanilla.com.bital.beans.Menues;
import ventanilla.com.bital.beans.MenuesS;
import ventanilla.com.bital.beans.Monedas;

public class MenuServlet extends HttpServlet {
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession(false);
        if( session == null )
            return;
        
        String[] page = request.getParameterValues("list");
        
        if( page == null ) {
            session.setAttribute("menu.Builder.Error","Imposible determinar el requerimiento, intente despu�s");
            response.sendRedirect("/ventanilla/paginas/error.jsp");
            return;
        }
              
        java.util.Hashtable userinfo = null;

        if(session.getAttribute("userinfo") != null)
       {
     userinfo = (java.util.Hashtable)session.getAttribute("userinfo");
}      
              
        Menues menues = Menues.getInstance();
        MenuesS menuess = MenuesS.getInstance();
        Monedas monedas = Monedas.getInstance();
       
		if(((String)session.getAttribute("isForeign")).equals("S") && (userinfo.get("N_NIVEL").equals("2")||userinfo.get("N_NIVEL").equals("1")))
		{				
			session.setAttribute("menu.info", menues.getMenuesDep(((String)session.getAttribute("teller")).substring(0,4)));
			session.setAttribute("menu2.info", menuess.getMenuesSDept());

		}
		else if(userinfo.get("N_NIVEL").equals("1") || userinfo.get("N_NIVEL").equals("2")){			
			if(session.getAttribute("tipomenu").equals("P")){
			   session.setAttribute("menu.info", menues.getMenuesPerfil((String)session.getAttribute("tellerperfil"),false, page[0].toString()));   
		       session.setAttribute("menu2.info", menuess.getMenuesS(page[0]));
			}else{
               session.setAttribute("menu.info", menues.getMenuesPerfil((String)session.getAttribute("tellerperfil"),true, ""));
               session.setAttribute("menu2.info", menuess.getMenuesSDept()); 			   
			}
		}else{
        session.setAttribute("menu.info", menues.getMenues(page[0]));
        session.setAttribute("menu2.info", menuess.getMenuesS(page[0]));
		}
		
        session.setAttribute("monedas.info", monedas.getMonedas());                
        
        String iTxn = (String)session.getAttribute("page.iTxn");
        if(session.getAttribute("page.flujotxn") != null)
            session.removeAttribute("page.flujotxn");
        if(session.getAttribute("page.datasession") != null)
            session.removeAttribute("page.datasession");
        if(session.getAttribute("page.iTxn") != null)
            session.removeAttribute("page.iTxn");
        if(session.getAttribute("txtCadImpresion") != null)
            session.removeAttribute("txtCadImpresion");
        if(session.getAttribute("idFlujo") != null)
        	session.removeAttribute("idFlujo");

        if(page[0].equals("99"))
            getServletContext().getRequestDispatcher("/ventanilla/paginas/MenuBuilderGTE.jsp").forward(request, response);
        else
            getServletContext().getRequestDispatcher("/ventanilla/paginas/MenuBuilder.jsp").forward(request, response);
    }
}
