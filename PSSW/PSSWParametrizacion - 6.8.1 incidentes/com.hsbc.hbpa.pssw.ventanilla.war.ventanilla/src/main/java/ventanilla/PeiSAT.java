//*************************************************************************************************
//             Funcion: Clase que realiza txn PEISAT
//            Elemento: PeiSAT.java
//          Creado por: Alejandro Gonzalez Castro
//      Modificado por: Alejandro Gonzalez Castro
//*************************************************************************************************
// CCN - 4360189 - 03/09/2004 - Se cambia invoke al Diario para Control de Efectivo
// CCN - 4360237 - 05/11/2004 - SAT v3.0
// CCN - 4360407 - 15/12/2005 - Nueva version SAT 4.0 
// CCN - 4360416 - 20/01/2006 - Correcion en RFC para personas morales eliminando espacio
// CCN - 4360419 - 25/01/2006 - Correccion para aceptar nombre largos
//*************************************************************************************************
package ventanilla; 

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.util.Hashtable;
import java.util.Vector;
import java.util.StringTokenizer;

import ventanilla.com.bital.admin.Diario;
import ventanilla.com.bital.sfb.RAPG;

public class PeiSAT extends HttpServlet
{
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        HttpSession session = request.getSession(false);
        Hashtable datasession = (Hashtable)session.getAttribute("page.datasession");
        String dirip = (String)session.getAttribute("dirip");
		GenericClasses gc = new GenericClasses();
        String txn = (String)datasession.get("cTxn");

        RAPG rapg = new RAPG();

        if (txn.equals("0806"))
        {
            int segments = 1;
            String allsegments = new String();
            String codigo = "";
            while(true)
            {
                rapg.setTxnCode(txn);
                rapg.setBranch((String)datasession.get("sucursal"));
                rapg.setTeller((String)datasession.get("teller"));
                rapg.setSupervisor((String)datasession.get("teller"));
                rapg.setFormat("G");
                String moneda = gc.getDivisa((String)datasession.get("moneda"));
                rapg.setToCurr(moneda);
                rapg.setTranAmt("000");
                rapg.setAcctNo("1");
                String txtRfc = (String)datasession.get("txtRfc");
                if (txtRfc.length() == 0)
                {
                    String numcredito = (String)datasession.get("numcredito");
                    if (numcredito.length() > 0)
                    {
                      if (numcredito.length() == 14)
                         numcredito = numcredito.substring(0,13);
                      txtRfc = numcredito;
                    }
                    String clavedpa = (String)datasession.get("clavedpa");
                    if (clavedpa.length() > 0)
                        txtRfc = clavedpa + "0000";
                    datasession.put("txtRfc",txtRfc);
                }
                rapg.setBenefic(txtRfc + (String)datasession.get("txtRefer16") + "0" + Integer.toString(segments));

                Diario diario = new Diario();
                String sMessage = diario.invokeDiario(rapg, (String)session.getAttribute("d_ConsecLiga"), dirip, moneda, session);

                String respons = sMessage;
                session.setAttribute("page.txnresponse",respons);
                codigo = respons.substring(0,1);

                if (!codigo.equals("0"))
                    break;

                int posicion = 4;
                String cadena = "";
                int indice = respons.indexOf("10001= "); //Persona Moral
                if (indice > 0)
                	respons = respons.substring(0,indice) + "10001=" + respons.substring(indice+7);
                while(true)
                {
                    String tmp = getItem(respons,posicion);
                    if (tmp.substring(0,1).equals("|"))
                    {
                        cadena = tmp;
                        break;
                    }
                    else
                        posicion++;
                }
                String txtnombre = respons.substring(246, (246+50));
                datasession.put("txtnombre", txtnombre);
                segments++;
                allsegments = allsegments + cadena.substring(0,cadena.length() - 1);
                if (getItem(respons,3).equals("NE"))
                    break;
            }
            if (codigo.equals("0"))
              session.setAttribute("cadena.original",getdata(allsegments));
        }

        session.setAttribute("page.datasession",datasession);
        getServletContext().getRequestDispatcher("/servlet/ventanilla.Redirector").forward(request,response);
    }
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        doPost(request,response);
    }


    public String getItem(String newCadNum, int newOption)
    {
        int iPIndex = newCadNum.indexOf("CONSECUTIVO:") + 12, NumSaltos, Num = 0, Ban = 0;
        String nCadNum = new String("");
        NumSaltos = newOption;
        while(Num < NumSaltos) {
            if(newCadNum.charAt(iPIndex) == 0x20) {
                while(newCadNum.charAt(iPIndex) == 0x20)
                    iPIndex++;
                Num++;
            }
            else
                while(newCadNum.charAt(iPIndex) != 0x20)
                {
                    iPIndex++;
                    if (iPIndex == 297)
                    {
                    	Num++;
                    	break;
                    }
                }
        }
        while(newCadNum.charAt(iPIndex) != 0x20 && iPIndex < newCadNum.length()) {
            nCadNum = nCadNum + newCadNum.charAt(iPIndex);
            if(iPIndex < newCadNum.length())
                iPIndex++;
            if(iPIndex == newCadNum.length() || (iPIndex == 297 && nCadNum.length()> 0))
                break;
        }
        return nCadNum;
    }

    public Hashtable getdata(String cadenaoriginal)
    {
        Hashtable datatocadorig = new Hashtable();    
        int indice = 0;    
/*        indice = cadenaoriginal.indexOf("10001=?");
        if (indice > 0)
           cadenaoriginal = cadenaoriginal.substring(0,indice) + "10001= " + cadenaoriginal.substring(indice+7);*/
        indice = cadenaoriginal.indexOf("40001=");
        String materia = cadenaoriginal.substring(indice + 6, indice + 9);
        String ultimocampo = "02"; //Provisionales - Ejercicio - Coordinados
        int mat = Integer.parseInt(materia);
        int numconce = 0;
        if (materia.equals("004")) //Creditos Fiscales
           ultimocampo = "31";
        if (materia.equals("001") || materia.equals("005") || materia.equals("012")) //Provisionales - Ejercicio - Coordinados
          numconce = -1;
        //Calcula encabezados
        datatocadorig = setencabezados(datatocadorig,cadenaoriginal, "20009=","|");
        datatocadorig = setencabezados(datatocadorig,cadenaoriginal, "40006=",null);
        if (materia.equals("010")) //Arma cadena original de manera inversa.
           datatocadorig.put("Cadena.DPA",getdpa(cadenaoriginal));
        //Calcula pagos
        String conceptos = cadenaoriginal.substring(setposicion(cadenaoriginal, '|', cadenaoriginal.indexOf("20009=") + 6), cadenaoriginal.indexOf("40006="));
        StringTokenizer st = new StringTokenizer(conceptos, "|");
//        int isiva = 0;
//        int isdpa = 0;
        while(st.hasMoreElements()) {
            String cadenatmp = st.nextToken();
            indice = cadenatmp.indexOf("=");
            String campo = cadenatmp.substring(0,indice);
            String valor = cadenatmp.substring(indice + 1, cadenatmp.length());
            String clvimp = campo.substring(0,3);
            if (materia.equals("010")) //DPA
            {
              if (clvimp.equals("120"))
              {
                ultimocampo = "20";
//                isdpa = 0;
//                if (campo.substring(3).equals("02"))
//                {
//                  isiva++; //Trae iva
//                  numconce++; //Invierte iva
//                }
              }
              else
              {
                ultimocampo = "34";
/*                if (isiva > 0 && isdpa == 0)
                {
                  numconce = numconce - 2; //Invierte dpa
                  isiva = 2; 
                  isdpa++;
                }*/
              }
            }
            if (campo.substring(3).equals(ultimocampo) && !materia.equals("004") && !materia.equals("010"))
               numconce++;
            datatocadorig.put("clvimpuesto" + Integer.toString(numconce), clvimp); //solo se graba una vez clave de impuesto
            datatocadorig.put(campo + Integer.toString(numconce),valor);
            if (campo.substring(3).equals(ultimocampo) && (materia.equals("004") || materia.equals("010")))
               numconce++;
//            if (materia.equals("010") && isiva == 2 && campo.substring(3).equals(ultimocampo))
/*            if (materia.2equals("010") && campo.substring(3).equals(ultimocampo))
            {
               numconce++;
//               isiva = 0; 
            }*/
        }
        if (materia.equals("001") || materia.equals("005") || materia.equals("012")) //Provisionales - Ejercicio - Coordinados
          numconce ++;
        if (materia.equals("010"))
        {
           Vector dpa = new Vector();
           Vector iva = new Vector();
           Vector dpadatos = new Vector();
           Vector ivadatos = new Vector();
           Vector codigos = getcodImp();
           for (int i=0; i<numconce; i++)
           {
           	String impuesto = (String)datatocadorig.get("clvimpuesto" + i);
           	if (impuesto.equals("147"))
           	{
           	  dpa.add(impuesto);
           	  Hashtable newimp = new Hashtable();
              for (int j=0; j<codigos.size(); j++)
              {
              	 String codigo = (String)codigos.get(j);
              	 String key = impuesto+ codigo+Integer.toString(i);
              	 String valor = (String)datatocadorig.get(key);
              	 if (valor != null)
                   newimp.put(key.substring(0,key.length()-1), valor);
              }
              dpadatos.add(newimp);
           	}
           	else
           	{
           	  iva.add(impuesto);
           	  Hashtable newimp = new Hashtable();
              for (int j=0; j<codigos.size(); j++)
              {
              	 String codigo = (String)codigos.get(j);
              	 String key = impuesto+ codigo+Integer.toString(i);
              	 String valor = (String)datatocadorig.get(key);
              	 if (valor != null)
                   newimp.put(key.substring(0,key.length()-1), valor);
              }
              ivadatos.add(newimp);
           	}
           }
           int isdpa = dpa.size();
           int isiva = iva.size();
           int posicion =0;
           int isend = 0;
           while(true)
           {
           	 if (isend < isdpa)
           	 {
           	  String impuesto = (String)dpa.get(isend); //147
           	  Hashtable datos = (Hashtable)dpadatos.get(isend); //14702
            	 datatocadorig.put("clvimpuesto" + Integer.toString(posicion), impuesto);
//System.out.println("dpa clvimpuesto" + Integer.toString(posicion) + " " + impuesto);
               for (int j=0; j<codigos.size(); j++)
               {
              	 String codigo = (String)codigos.get(j);
              	 if (datos.get(impuesto+codigo) != null)
              	 {
                	 datatocadorig.put(impuesto+codigo+posicion, (String)datos.get(impuesto+codigo));
//                     System.out.println(impuesto+codigo+posicion + " " + (String)datos.get(impuesto+codigo));
              	 }
               }
           	 }
           	 if (isend < isiva)
           	 {
         	   String impuesto = (String)iva.get(isend); //120
           	   Hashtable datos1 = (Hashtable)ivadatos.get(isend); //12002
           	   datatocadorig.put("clvimpuesto" + Integer.toString(posicion+1), impuesto);
//System.out.println("iva clvimpuesto" + Integer.toString(posicion+1) + " " + impuesto);
                for (int j=0; j<codigos.size(); j++)
                {
              	 String codigo = (String)codigos.get(j);
              	 if (datos1.get(impuesto+codigo) != null)
              	 {
                	 datatocadorig.put(impuesto+codigo+(posicion+1), (String)datos1.get(impuesto+codigo));
//                     System.out.println(impuesto+codigo+(posicion+1) + " " + (String)datos1.get(impuesto+codigo));
              	 }
               }
           	 }
           	 posicion = posicion + 2;
           	 isend++;
           	 if (isend >= isdpa && isend >= isiva)
           	   break;
           }
//           numconce = posicion;
        }
        datatocadorig.put("numconce",Integer.toString(numconce));
//        System.out.println("datacadorig :" + datatocadorig.toString());
        return datatocadorig;
    }
    public Hashtable setencabezados(Hashtable table, String cadena, String dato, String delimita)
    {
      int pos = cadena.indexOf(dato);
      String encabezado = "";
      if (delimita == null)
         encabezado = cadena.substring(pos);
      else
        encabezado = cadena.substring(0,setposicion(cadena,'|',pos + 6));
      StringTokenizer st = new StringTokenizer(encabezado, "|");
      while(st.hasMoreElements()) {
            String cadenatmp = st.nextToken();
            int indice = cadenatmp.indexOf("=");
            String campo = cadenatmp.substring(0,indice);
            String valor = cadenatmp.substring(indice + 1, cadenatmp.length());
            table.put(campo,valor);
      }
      return table;
    }
    public int setposicion(String cadena, char c, int pos)
    {
      	while(true)
      	{
      	  if (cadena.charAt(pos) == c)
      	    break;
      	  else
      	    pos++;
      	}
      	return pos;
    }
    public Hashtable getdpa(String cadenaoriginal)
    {
        Hashtable datatocadorig = new Hashtable();    
        int indice = 0;    
        indice = cadenaoriginal.indexOf("10001=?");
        if (indice > 0)
           cadenaoriginal = cadenaoriginal.substring(0,indice) + "10001= " + cadenaoriginal.substring(indice+7);
        indice = cadenaoriginal.indexOf("40001=");
        String materia = cadenaoriginal.substring(indice + 6, indice + 9);
        String ultimocampo = "";
        int mat = Integer.parseInt(materia);
        int numconce = 0;
        String conceptos = cadenaoriginal.substring(setposicion(cadenaoriginal, '|', cadenaoriginal.indexOf("20009=") + 6), cadenaoriginal.indexOf("40006="));
        StringTokenizer st = new StringTokenizer(conceptos, "|");
        while(st.hasMoreElements()) {
            String cadenatmp = st.nextToken();
            indice = cadenatmp.indexOf("=");
            String campo = cadenatmp.substring(0,indice);
            String valor = cadenatmp.substring(indice + 1, cadenatmp.length());
            String clvimp = campo.substring(0,3);
            if (clvimp.equals("120"))
              ultimocampo = "20";
            else
              ultimocampo = "34";
            datatocadorig.put("clvimpuesto" + Integer.toString(numconce), clvimp); //solo se graba una vez clave de impuesto
            datatocadorig.put(campo + Integer.toString(numconce),valor);
            if (campo.substring(3).equals(ultimocampo))
               numconce++;
        }
        datatocadorig.put("numconce",Integer.toString(numconce));
//        System.out.println("datacadorigDPA :" + datatocadorig.toString());
        return datatocadorig;
    }

public Vector getcodImp()
{ //Columnas en formato para impresion
    Vector codigo = new Vector();
    codigo.add("31");
    codigo.add("02");	
    codigo.add("27");	
    codigo.add("22");	
    codigo.add("32");		
    codigo.add("03");	
    codigo.add("04");	
    codigo.add("05");	
    codigo.add("06");		
    codigo.add("07");		
//    codigo.add("08");	//Total de contribuciones
    codigo.add("09");	
    codigo.add("14");	
    codigo.add("10");	
    codigo.add("24");	
    codigo.add("25");	
    codigo.add("26");	
    codigo.add("12");	
    codigo.add("35");	
    codigo.add("36");		
    codigo.add("37");	
//    codigo.add("16");	 //Total de aplicaciones
    codigo.add("17");
    codigo.add("23");		
    codigo.add("15");	
    codigo.add("18");	
    codigo.add("19");
    codigo.add("21");
    codigo.add("20");
    codigo.add("29");
    codigo.add("30");
    codigo.add("33");
    codigo.add("34");
    return codigo;
}

}
