//*************************************************************************************************
//             Funcion: Clase para TXNS : 0825 & 1053 & 1075 & 1077 & 1079 & 1081 & 5573
//            Elemento: Group03R.java
//          Creado por: Alejandro Gonzalez Castro.
//		Modificado por: Juvenal Fernandez
//*************************************************************************************************
// CCN - 4360189 - 03/09/2004 - Se cambia invoke al Diario para Control de Efectivo
// CCN - 4360204 - 24/09/2004 - Correccion para Agencia NY
// CCN - 4360268 - 04/02/2005 - Se realizan adecuaciones para eliminar Agencia NY
// CCN - 4360368 - 02/09/2005 - Se elimina sleep
// CCN - 4360374 - 08/09/2005 - Se genera nuevo paquete a peticion de QA.
//*************************************************************************************************

package ventanilla;

import java.io.IOException;
import java.util.Hashtable;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import ventanilla.com.bital.sfb.Retiros;
import ventanilla.com.bital.sfb.Acdo;

import ventanilla.com.bital.admin.Diario;

public class Group03R extends HttpServlet
{
    public void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {
        Retiros oRetiros = new Retiros();
        Acdo    oAcdo    = new Acdo();
		GenericClasses cGenericas = new GenericClasses();
        resp.setContentType("text/plain");

        HttpSession session = req.getSession(false);
        Hashtable datasession = (Hashtable)session.getAttribute("page.datasession");

        String dirip        = (String)session.getAttribute("dirip");
        String txtMoneda    = (String)datasession.get("moneda");
        char status = '1';

        String getMsg = "";
        String txtTxnCode    = "0825";
        String txtFormat     = "B";
        String txtBranch     = (String)datasession.get("sucursal");
        String txtTeller     = (String)datasession.get("teller");
        String txtSupervisor = (String)datasession.get("teller");
        String txtBackOut    = "0";
        String txtOverride   = "0";
        String txtCurrentDay = "0";
        oAcdo.setTxnCode(txtTxnCode);
        oAcdo.setFormat(txtFormat);
        oAcdo.setBranch(txtBranch);
        oAcdo.setTeller(txtTeller);
        oAcdo.setSupervisor(txtSupervisor);
        oAcdo.setBackOut(txtBackOut);
        oAcdo.setOverride(txtOverride);
        oAcdo.setCurrentDay(txtCurrentDay);

        String lstTipoDocto  = (String)datasession.get("lstTipoDocto");
        String txtReferencia = (String)datasession.get("txtFolio");
        oAcdo.setAcctNo((lstTipoDocto + txtReferencia));
        String Override = "NO";
        if ( (String)datasession.get("override")!= null )
            Override = datasession.get("override").toString();

        if ( Override.charAt(0) == 'N') {
            Diario diario = new Diario();
            String sMessage = diario.invokeDiario(oAcdo, (String)session.getAttribute("d_ConsecLiga"), dirip, txtMoneda, session);


            if(sMessage.charAt(0) !='0') {
                session.setAttribute("txtCadImpresion", "NOIMPRIMIRNOIMPRIMIR");
                session.setAttribute("page.txnresponse", sMessage);
                getServletContext().getRequestDispatcher("/servlet/ventanilla.Redirector").forward(req,resp);
                return;
            }
            status = sMessage.charAt(0);
        }
        else
            status = '0';

        if ( status == '0' )
        {
            txtTxnCode    = (String)datasession.get("cTxn");
            txtFormat     = "A";

            if ( (String)datasession.get("supervisor") != null )
                txtSupervisor = (String)datasession.get("supervisor");

            oRetiros.setTxnCode(txtTxnCode);
            oRetiros.setFormat(txtFormat);
            oRetiros.setBranch(txtBranch);
            oRetiros.setTeller(txtTeller);
            oRetiros.setSupervisor(txtSupervisor);
            oRetiros.setBackOut(txtBackOut);
            oRetiros.setOverride(txtOverride);
            oRetiros.setCurrentDay(txtCurrentDay);
            if ( (String)datasession.get("override") != null )
                if (datasession.get("override").toString().equals("SI"))
                    oRetiros.setOverride("3");

            String txtFees="8";
            String txtEfectivo = "000";
            String txtDDACuenta  = (String)datasession.get("txtDDACuenta2");
            //String txtTrNo1      = (String)datasession.get("txtCodSeg");
			String txtTrNo1      = "0000";
            String txtTrNo2      = (String)datasession.get("txtCveTran");
            String txtMonto      = cGenericas.delCommaPointFromString((String)datasession.get("txtMonto"));
            String txtCheckNo    = (String)datasession.get("txtSerial2");
            String txtCashOut    = "";
            oRetiros.setFees(txtFees);
            if(txtDDACuenta.length()==10)
				txtDDACuenta="0"+txtDDACuenta;
            oRetiros.setAcctNo(txtDDACuenta.substring(1,11));
            oRetiros.setTrNo2(txtTrNo1.substring(0,3));
            if(!txtTrNo2.equals("1481"))
                oRetiros.setTrNo1(txtTrNo2.substring(2,5));      // Numero de plaza
                else
                	oRetiros.setTrNo1(txtTrNo2);
            oRetiros.setCheckNo(txtCheckNo);
            oRetiros.setTranAmt(txtMonto);
            oRetiros.setTranCur(txtMoneda);

            if ( txtTxnCode.equals("1053") || txtTxnCode.equals("1075") || txtTxnCode.equals("1077") ||
            txtTxnCode.equals("1079") || txtTxnCode.equals("1081") || txtTxnCode.equals("1093"))
            {
                txtCashOut  = txtMonto;
                txtEfectivo = txtCashOut;
                oRetiros.setCashOut(txtCashOut);
            }

            if (txtTxnCode.equals("1053"))
            {
                String cuenta = (String)datasession.get("txtDDACuenta2");
                cuenta = cuenta.substring(1,2);
                //oRetiros.setTranDesc("CHEQUE PAGADO EN EFECTIVO " + txtCheckNo);
                //oRetiros.setDescRev("REV. CHEQUE PAGADO EN EFECTIVO " + txtCheckNo);
            }
            

            Diario diario = new Diario();
            String sMessage = diario.invokeDiario(oRetiros, (String)session.getAttribute("d_ConsecLiga"), dirip, txtMoneda, session);

            /*ventanilla.com.bital.util.searchForCRM cfCRM = new ventanilla.com.bital.util.searchForCRM();
            cfCRM.setAccountNumber(txtDDACuenta.substring(1,11));
            cfCRM.setBranchNumber((String)datasession.get("sucursal"));
            cfCRM.setTellerName("SOMEONE");
            cfCRM.setTellerNumber((String)datasession.get("teller"));
            cfCRM.executeCISSearch();
            if(cfCRM.getCISNumber().length() != 0){
                cfCRM.executeDBSearch();
                if(!cfCRM.getCustomerInfo().isEmpty()){
                    session.setAttribute("crmInfo", cfCRM.getCustomerInfo());
                }
            }*/

            session.setAttribute("page.txnresponse", sMessage);
            getServletContext().getRequestDispatcher("/servlet/ventanilla.Redirector").forward(req,resp);
        }
    }

    public void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doPost(req, resp);
    }
}
