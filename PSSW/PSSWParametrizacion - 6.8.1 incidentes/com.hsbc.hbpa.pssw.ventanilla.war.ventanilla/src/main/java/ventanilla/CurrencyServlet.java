//*************************************************************************************************
//             Funcion: Clase para obtener el tipo de cambio
//            Elemento: CurrencyServlet.java
//          Creado por: Alejandro Gonzalez Castro
//      Modificado por: Jesus Emmanuel Lopez Rosales
//*************************************************************************************************
// CCN - 4360189 - 03/09/2004 - Se crea clase para realizar actualizacion de tipo de cambio
// CCN - 4360518 - 21/09/2005 - Se agrega en sesion varaible para controlar ruta para el intercambio de perfiles.
//*************************************************************************************************
package ventanilla;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class CurrencyServlet extends HttpServlet 
{
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
    {
        response.setContentType("text/plain");
        HttpSession session = request.getSession(false);

        ventanilla.ObtainForeignExchange ofe = new ventanilla.ObtainForeignExchange();
        ofe.setBranch(request.getParameter("branch"));
        ofe.setTeller(request.getParameter("teller"));
        ofe.setTellerRegister(request.getParameter("empno"));
        ofe.doForeignExchanges();
        java.util.Hashtable htOFE = ofe.obtainedExchanges();
        session.setAttribute("foreignExchange", htOFE);
        session.setAttribute("updatecurr", "1"); 
        session.setAttribute("PRESSTC","S");
        getServletContext().getRequestDispatcher("/ventanilla/paginas/seccioninformativa.jsp").forward(request,response);
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}
