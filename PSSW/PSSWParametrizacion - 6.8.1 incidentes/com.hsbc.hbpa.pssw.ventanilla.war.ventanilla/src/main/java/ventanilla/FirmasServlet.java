//*************************************************************************************************
//             Funcion: Clase que realiza la consulta de firmas 
//          Creado por: Rutilo Zarco Reyes
// Ultima Modificacion: 24/06/2004
//      Modificado por: Juvenal R. Fernandez V.
//*************************************************************************************************
// CCN -4360160- 30/06/2004 - Se agregan presistencia de datos en firmas
// CCN - 4360455 - 07/04/2006 - Se realizan modifiaciones para esquema de cheques devueltos.
// CCN - 4360462 - 10/04/2005 - Se genera nuevo paquete por errores en paquete anterior
//*************************************************************************************************

package ventanilla;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import ventanilla.com.bital.beans.CuentasEsp;
import ventanilla.com.bital.sfb.FormatoB;
import ventanilla.com.bital.admin.Diario;

import java.util.Hashtable;

public class FirmasServlet extends HttpServlet {
    
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, NullPointerException {
        HttpSession sessions = request.getSession(false);
        GenericClasses GC = new GenericClasses();
		///////////cheques Devueltos////////
		Hashtable datasession = null;
		String cuenta = null;
		boolean valor = false;
		if (sessions.getAttribute("page.datasession") != null)		
		{
			datasession = (Hashtable) sessions.getAttribute("page.datasession");
			if (datasession.get("chqDev") != null)		
			{
				if (datasession.get("chqDev").equals("SI")) {
					datasession.put("lstChqDev", "04");
					datasession.put(
						"txtCodSeg",
						(String) request.getParameter("txtCodSeg"));
					datasession.put(
						"txtCveTran",
						(String) request.getParameter("txtCveTran"));
					datasession.put(
						"txtDDACuenta2",
						(String) request.getParameter("txtDDACuenta2"));
					datasession.put(
						"txtSerial2",
						(String) request.getParameter("txtSerial2"));
					datasession.put(
						"txtMontoCheque",
						(String) request.getParameter("txtMontoCheque"));
					datasession.put(
						"txtFirmaFun",
						(String) request.getParameter("txtFirmaFun"));
					sessions.setAttribute("page.datasession", datasession);
				}
			}
		}
		////////////////////
        String[] strParametro = request.getParameterValues("txtDDACuenta");
        String strCuenta = new String("");
        String strCeros = new String("0000000000000000");
        String temporal = new String("");
        if( strParametro != null )
        {
            strCuenta = strParametro[0];
            temporal = strParametro[0];
        }
        if(strCuenta.length() <= 5)
        {
            strCuenta = strCeros.substring(0,5-strCuenta.length()) + strCuenta + "01800000018";
            valor = true;
        }
        else
            strCuenta = strCeros.substring(0,16-strCuenta.length()) + strCuenta;
        
        if(temporal.length() == 11 && temporal.substring(0, 1).equals("0"))
        	valor = GC.validateModulo10(temporal);

        if(!valor)
        {
	        //Validaciones de cuenta vieja de Banistmo y cmtH Panam�
			if (sessions.getAttribute("page.cTxn") != null && (sessions.getAttribute("page.cTxn").equals("5353")
			|| sessions.getAttribute("page.cTxn").equals("1051") || sessions.getAttribute("page.cTxn").equals("1053")
			|| sessions.getAttribute("page.cTxn").equals("4543") || sessions.getAttribute("page.cTxn").equals("4547")
			|| sessions.getAttribute("page.cTxn").equals("4537") || sessions.getAttribute("page.cTxn").equals("4539")
			|| sessions.getAttribute("page.cTxn").equals("4545") || sessions.getAttribute("page.cTxn").equals("1079")
			|| sessions.getAttribute("page.cTxn").equals("4229") || sessions.getAttribute("page.cTxn").equals("M007") 
			))
			{
				String[] strTran = request.getParameterValues("txtCveTran");
				String strCve = new String("");
				if( strTran != null )
					strCve = strTran[0];
				else if(sessions.getAttribute("specialCveTran") != null)
					{
						String Cvetran = (String)sessions.getAttribute("specialCveTran");
						strCve = Cvetran;
					}
				if (strCve.equals("1481"))
					strCve = "000" + strCve;
				if(strCve != null && (strCve.substring(4,6).equals("88") || (!strCve.substring(1,3).equals("01") && strCve.substring(3,6).equals("148"))))
				{	
					String banco = null;
					if((strCve.substring(4,6).equals("88")))
						banco="BCC";
					else
						banco="HCC";
					ICRServlet icr = new ICRServlet();
					String cta_nueva = null;
					FormatoB formatob = new FormatoB();
					formatob.setBranch((String)sessions.getAttribute("branch"));
					formatob.setTeller((String)sessions.getAttribute("teller"));
					formatob.setTxnCode("0830");
					formatob.setFormat("B");
					if(strCuenta.length()>=16)
						formatob.setAcctNo(strCuenta.substring(1,16));
					else
						formatob.setAcctNo(GC.rellenaZeros(strCuenta,15));
					formatob.setTranAmt("1");
					formatob.setReferenc(banco);
					formatob.setFeeAmoun("0");
					formatob.setIntern("0");
					formatob.setIntWh("0");
					formatob.setCashIn("000");
					String divisa = GC.getDivisa("N$");
					formatob.setFromCurr(divisa);
					String dirip =  (String)sessions.getAttribute("dirip");
					Diario diario = new Diario();
					String sMessage = diario.invokeDiario(formatob,(String)sessions.getAttribute("d_ConsecLiga"), dirip, divisa, sessions);
					cuenta = icr.ICRforFirma(sMessage,banco);
					if(cuenta!=null && !cuenta.equals("No existe la cuenta") && sessions.getAttribute("page.cTxn").equals("5353"))
					{
						datasession.put("txtDDACuenta2",cuenta);
						sessions.setAttribute("page.datasession", datasession);
						sessions.setAttribute("CuentaNuevaFirma", "0"+cuenta);
					}else 
					if(cuenta!=null && !cuenta.equals("No existe la cuenta") && (sessions.getAttribute("page.cTxn").equals("1051") 
					|| sessions.getAttribute("page.cTxn").equals("1053") || sessions.getAttribute("page.cTxn").equals("4543")
					|| sessions.getAttribute("page.cTxn").equals("4547") || sessions.getAttribute("page.cTxn").equals("4537")
					|| sessions.getAttribute("page.cTxn").equals("4539") || sessions.getAttribute("page.cTxn").equals("4545")
					|| sessions.getAttribute("page.cTxn").equals("1079") || sessions.getAttribute("page.cTxn").equals("4229")
					|| sessions.getAttribute("page.cTxn").equals("M007")
					))
					{
						//cuenta="4013703889";
						if (sessions.getAttribute("page.cTxn").equals("4545"))
							sessions.setAttribute("CuentaNuevaFirma", "0"+cuenta);
						else
							sessions.setAttribute("CuentaNuevaFirma", cuenta);
					}
					if(!cuenta.equals("No existe la cuenta") && !cuenta.substring(0,1).equals("A"))
						cuenta = strCeros.substring(0,16-cuenta.length()) + cuenta;
				}
			}	
	        
	        if(cuenta != null && !cuenta.equals("No existe la cuenta") && !cuenta.substring(0,1).equals("A"))
				sessions.setAttribute("Cuenta", cuenta);
	        else
				sessions.setAttribute("Cuenta", strCuenta);
        }else
        {
        	sessions.setAttribute("Cuenta", strCuenta);
        }
        	
        
        CuentasEsp ctaesp = CuentasEsp.getInstance();
        String strRespuesta = ctaesp.getCuentaEsp(strCuenta.substring(6,16));
        
        if (strRespuesta.length() > 10)
            if (strRespuesta.substring(0,10).equals(strCuenta.substring(6,16)))
                sessions.setAttribute("CuentaEspecial", strCuenta);
        
        response.sendRedirect("../ventanilla/paginas/DespFirma.jsp");
    }
    
    
    
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}
