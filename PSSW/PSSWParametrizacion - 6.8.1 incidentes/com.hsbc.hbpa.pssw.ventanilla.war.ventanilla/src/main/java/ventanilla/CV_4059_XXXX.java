//*************************************************************************************************
//             Funcion: Clase que realiza txn 4059
//            Elemento: CV_4059_XXXX.java
//          Creado por: Alejandro Gonzalez Castro
//		Modificado por: Jes�s Emmanuel L�pez Rosales
//*************************************************************************************************
// CCN - 4360189 - 03/09/2004 - Se cambia invoke al Diario para Control de Efectivo
// CCN - 4360211 - 08/10/2004 - Se habilita override para txn 4059
// CCN - 4360268 - 04/02/2005 - Se realizan adecuaciones para eliminar Agencia NY
// CCN - 4360368 - 02/09/2005 - Se elimina sleep
// CCN - 4360374 - 08/09/2005 - Se genera nuevo paquete a peticion de QA.
// CCN - 4360532 - 20/10/2006 - Se realizan modificaciones para enviar el numero de cajero, consecutivo txn entrada y salida en txn 0821
//*************************************************************************************************
package ventanilla; 

import java.io.IOException;
import java.util.Hashtable;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import ventanilla.com.bital.admin.Diario;
import ventanilla.com.bital.sfb.CompraVenta;

public class CV_4059_XXXX extends HttpServlet {
   
      public void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String TE, TO, Currency;
        int Resultado = -1;

        CompraVenta oTxn = new CompraVenta();
        Diario diario;
        String sMessage = null;
        GenericClasses cGenericas = new GenericClasses();

        HttpSession session = req.getSession(false);
        Hashtable datasession = (Hashtable)session.getAttribute("page.datasession");
        TE = (String)datasession.get("cTxn");
        TO = (String)datasession.get("oTxn");
        Currency = (String)datasession.get("moneda");
        String txtPlaza = (String)datasession.get("txtPlaza");
        String dirip =  (String)session.getAttribute("dirip");
        String msg = null;
        String txtCasoEsp = new String("");
        String txtCadImpresion = new String("");
        String txtNomCliente = (String)datasession.get("txtCliente");
        String txtSupervisor = (String)datasession.get("teller");
        String txtRegistro = (String)datasession.get("txtRegistro");
        String Currency2 = "";
        String txtComisionGiro = "";
        String txtIvaGiro = "";
        String txtNumOrden = "0";
        String Monto = "";
        String ConsecutivoIN = "";
        String ConsecutivoOUT = "";        
        if ( Currency.equals("01"))
            Currency2 = "02";
        else
            Currency2 = "01";

        if ( (String)datasession.get("supervisor") != null )       //pidio autorizacisn de supervisor
            txtSupervisor =  (String)datasession.get("supervisor");
        
        String txtOverride = "0";
        if ( (String)datasession.get("override") != null )
        {
            if (datasession.get("override").toString().equals("SI"))
                txtOverride = "3";
        }

        if ( datasession.get("posteo") == null ) 
        {
            datasession.put("posteo","1");
            session.setAttribute("page.datasession",(Hashtable)datasession);
        }

        oTxn.setTxnCode(TE);
        oTxn.setBranch((String)datasession.get("sucursal"));
        oTxn.setTeller((String)datasession.get("teller"));
        oTxn.setSupervisor(txtSupervisor);

        if( TE.equals( "4059" ) )
        {
            //ODCT 4059A 00001000131  000131  000**4016322240**600000*N$*0000000175****00100100000*60000*8*PAGO CHQ.M.N. P/ABO.CTA.CHQS.  US$********999*******553*********************4111**
            //ODCT 4059A 00001000131  000131  000**0444444446**60000*US$*0000003305****00100100000*600000*8*PAGO CHQ.US$ P/VENTA DE DOLARES********999*******147*********************0106**
            //ODCT 4059A 00001000131  000131  000**7010530152**600000*US$*0000001881****00100100000*6000000*8*PAGO CHQ.US$ P/ABO.CTA.INVERS.  M.N.********197*******543*********************4113**

            if ( datasession.get("posteo").toString().equals("1"))
            {
                oTxn.setFormat( "A" );
                String txtTipoCambio = cGenericas.StringFiller((String)datasession.get( "txtTipoCambio" ), ((String)datasession.get( "txtTipoCambio" )).length()+3, false, "0");
                if(Currency.equals("01")) {
                    oTxn.setTranAmt( cGenericas.delCommaPointFromString((String)datasession.get("txtMonto")) );
                    oTxn.setTranCur( "N$" );
                    oTxn.setMoAmoun( cGenericas.delCommaPointFromString((String)datasession.get( "txtMontoC" )) );
                }
                else {
                    oTxn.setTranAmt( cGenericas.delCommaPointFromString((String)datasession.get( "txtMonto1" )) );
                    oTxn.setTranCur( "US$" );
                    oTxn.setMoAmoun( cGenericas.delCommaPointFromString((String)datasession.get( "txtMontoC" )) );
                }
                if(Currency.equals("01"))
                    oTxn.setTranDesc( "PAGO CHQ.M.N. P/ABO.CTA.CHQS. US$" );
                else {
                    if (TO.equals("0106"))
                        oTxn.setTranDesc("PAGO CHQ.US$ P/VENTA DE DOLARES");
                    else if (TO.equals("4111"))
                        oTxn.setTranDesc( "PAGO CHQ.US$ P/ABO.CTA.CHQS. M.N.");
                    else
                        oTxn.setTranDesc( "PAGO CHQ.US$ P/ABO.CTA.INVERS.  M.N." );
                }
                oTxn.setDescRev("EMPTY");
                txtTipoCambio = "0000" + txtTipoCambio;
                txtTipoCambio = txtTipoCambio.substring(txtTipoCambio.length()-8);
                oTxn.setDraftAm(txtPlaza + txtTipoCambio);
                oTxn.setCheckNo((String)datasession.get("txtSerial2"));
                String txtDDACuenta  = (String)datasession.get("txtDDACuenta2");
                oTxn.setAcctNo(txtDDACuenta.substring(1,11));
                String txtTrNo1      = (String)datasession.get("txtCodSeg");
                String txtTrNo2      = (String)datasession.get("txtCveTran");
                oTxn.setTrNo2(txtTrNo1.substring(0,3));      // Codigo de seguridad
                oTxn.setTrNo1(txtTrNo2.substring(2,5));      // Numero de plaza
                
                oTxn.setTrNo5("4059");
                oTxn.setFees( "8" );
                
                oTxn.setOverride(txtOverride);
                
                diario = new Diario();
                sMessage = diario.invokeDiario(oTxn, (String)session.getAttribute("d_ConsecLiga"), dirip, Currency, session);
                
                Resultado = oTxn.getStatus();
                
                if ( Currency.equals("01"))
                    Monto = cGenericas.delCommaPointFromString((String)datasession.get("txtMonto"));
                else
                    Monto = cGenericas.delCommaPointFromString((String)datasession.get("txtMonto1"));
                if(sMessage.startsWith("0"))
                {
                    txtCasoEsp = txtCasoEsp + TE + "~" + cGenericas.getString(oTxn.getMessage(),1) + "~";
                    ConsecutivoIN = cGenericas.getString(oTxn.getMessage(),1);
                    if (ConsecutivoIN.length()<6){
                   	    ConsecutivoIN = "0" + ConsecutivoIN; 
                    }                    
                }
            }
            else
                Resultado = 0;
        }

        if ( TE.equals("4059") && Resultado == 0 )
        {   // Cargo fue aceptado

            if ( datasession.get("posteo").toString().equals("1")) 
            {
                datasession.put("override","NO");
                datasession.put("posteo","2");
                txtOverride = "0";
            }
            

            oTxn = new CompraVenta();
            oTxn.setBranch((String)datasession.get("sucursal"));
            oTxn.setTeller((String)datasession.get("teller"));
            oTxn.setSupervisor(txtSupervisor);
            oTxn.setOverride(txtOverride);

            if( TO.equals("0106"))
            {
                //ODCT 0106B 00001000131  000131  000**0106*1000*1*0*0*0**1000*US$**
                //ODCT 0106B 00001000131  000131  000**0106*10000*1*0*0*0**10000*N$**
                oTxn.setTxnCode("0106");
                oTxn.setFormat("B");
                oTxn.setAcctNo( "0106" );
                oTxn.setTranAmt( cGenericas.delCommaPointFromString((String)datasession.get( "txtMontoC" )) );
                oTxn.setCashOut( cGenericas.delCommaPointFromString((String)datasession.get( "txtMontoC" )) );
                if(Currency.equals("01"))
                    oTxn.setFromCurr("US$");
                else
                    oTxn.setFromCurr("N$");
                oTxn.setReferenc("1");
            }
            else if ( TO.equals("4111") )
            {
                //ODCT 4111A 00001000131  000131  000**4000000000**6000000*N$*0000001881**000**00100100000*600000**ABO.CTA.CHQS.  M.N.************************************4059**
                oTxn.setTxnCode("4111");
                oTxn.setFormat( "A" );
                oTxn.setAcctNo( (String)datasession.get( "txtDDACuenta" ) );
                oTxn.setCheckNo((String)datasession.get("txtSerial2"));
                oTxn.setTranAmt( cGenericas.delCommaPointFromString((String)datasession.get( "txtMontoC" )) );
                if(Currency.equals("01"))
                {
                    oTxn.setTranCur( "US$" );
                    oTxn.setMoAmoun( cGenericas.delCommaPointFromString((String)datasession.get( "txtMonto" )) );
                    oTxn.setTranDesc( "ABO.CTA.CHQS.  US$" );
                }
                else
                {
                    oTxn.setTranCur( "N$" );
                    oTxn.setMoAmoun( cGenericas.delCommaPointFromString((String)datasession.get( "txtMonto1" )) );
                    oTxn.setTranDesc( "ABO.CTA.CHQS.  M.N." );
                }
                oTxn.setDescRev("EMPTY");
                oTxn.setCashIn( "000" );
                oTxn.setTrNo5( "4059" );
                String txtTipoCambio =	cGenericas.StringFiller((String)datasession.get( "txtTipoCambio" ), ((String)datasession.get( "txtTipoCambio" )).length()+3, false, "0"); 
                txtTipoCambio = "0000" + txtTipoCambio;
                txtTipoCambio = txtTipoCambio.substring(txtTipoCambio.length()-8);
                oTxn.setDraftAm(txtPlaza + txtTipoCambio);
                oTxn.setFees( "0" );
            }
            else if ( TO.equals("4113") )
            {
                //ODCT 4113A 00001000131  000131  000**160004566520493**6000000*N$*****00100100000*600000**ABO.CTA.INVERS.  M.N.************************************4059**
                oTxn.setFormat( "A" );
                oTxn.setTxnCode(TO);
                oTxn.setAcctNo( (String)datasession.get( "txtCDACuenta" ) );
                oTxn.setTranAmt( cGenericas.delCommaPointFromString((String)datasession.get( "txtMonto" )) );
                oTxn.setTranCur( "N$" );
                oTxn.setMoAmoun( cGenericas.delCommaPointFromString((String)datasession.get( "txtMonto1" )) );
                String txtTipoCambio = cGenericas.StringFiller((String)datasession.get( "txtTipoCambio" ), ((String)datasession.get( "txtTipoCambio" )).length()+3, false, "0");
                txtTipoCambio = "0000" + txtTipoCambio;
                txtTipoCambio = txtTipoCambio.substring(txtTipoCambio.length()-8);
                oTxn.setDraftAm( txtPlaza + txtTipoCambio );
                oTxn.setTranDesc( "ABO.CTA.INVERS.  M.N." );
                oTxn.setTrNo5( "4059" );
                oTxn.setDescRev("EMPTY");
            }

            diario = new Diario();
            sMessage = diario.invokeDiario(oTxn, (String)session.getAttribute("d_ConsecLiga"), dirip, Currency2, session);

            Resultado = oTxn.getStatus();

            if ( Currency2.equals("01"))
                Monto = cGenericas.delCommaPointFromString((String)datasession.get("txtMonto"));
            else
                Monto = cGenericas.delCommaPointFromString((String)datasession.get("txtMonto1"));

            if(sMessage.startsWith("0") || sMessage.startsWith("1"))
            {
                txtCasoEsp = txtCasoEsp + TO + "~" + cGenericas.getString(oTxn.getMessage(),1) + "~";
                ConsecutivoOUT = cGenericas.getString(oTxn.getMessage(),1);
                if (ConsecutivoOUT.length()<6){
                   	ConsecutivoOUT = "0" + ConsecutivoOUT; 
                }                
            }
        }

        //ODCT 0821G 00001000131  000131  000**V*US$*0000000000*1**1000*009470000*0*0431866*0431866*4000000000***
        //ODCT 0821G 00001000131  000131  000**C*US$*0000000000*5**1085*009220000*0*431866*0431866*7000000000***
        //ODCT 0821G 00001000131  000131  000**V*US$*0000000000*5**1056*009470000*0*0431866*0431866*4000000000***
        //ODCT 0821G 00001000131  000131  000**C*US$*0000000000*5**1085*009220000*0*0431866*0431866*7000000000***
        if (  TE.equals("4059") && Resultado == 0)
        {   // Cargo y Abono Aceptado

            oTxn = new CompraVenta();
            oTxn.setBranch((String)datasession.get("sucursal"));
            oTxn.setTeller((String)datasession.get("teller"));
            oTxn.setSupervisor(txtSupervisor);
            oTxn.setTxnCode("0821");
            oTxn.setFormat( "G" );
            oTxn.setToCurr( "US$" );
            if(Currency.equals("02")){
                oTxn.setService( "C" );
                oTxn.setGStatus( "6" );
            }
            else {
                oTxn.setService( "V" );
                if (TO.equals("4111"))
                    oTxn.setGStatus( "5" );
            }

            String txtAutoriCV = (String)datasession.get("txtAutoriCV");
            if ( txtAutoriCV == null )
                txtAutoriCV = "00000";
            oTxn.setCheckNo( "00000" + txtAutoriCV );
            oTxn.setTranAmt( cGenericas.delCommaPointFromString((String)datasession.get( "txtMonto1" )) );
            String txtTipoCambio = cGenericas.StringFiller((String)datasession.get( "txtTipoCambio" ), ((String)datasession.get( "txtTipoCambio" )).length()+2, false, "0") + "00";
            txtTipoCambio = "0000" + txtTipoCambio;
            txtTipoCambio = txtTipoCambio.substring(txtTipoCambio.length()-9);
            oTxn.setComAmt(txtTipoCambio);
            oTxn.setIVAAmt( "0" );
            oTxn.setAcctNo( (String)datasession.get( "registro" ) );
            oTxn.setCtaBenef( (String)datasession.get( "txtRegistro" ) );
            String txtDDACuenta  = (String)datasession.get("txtDDACuenta2");            
            String cadtemp = txtDDACuenta.substring(1);            
            int lon = cadtemp.length();
            for(int j=lon;j<10;j++){
               cadtemp = cadtemp + " ";
            }	
            oTxn.setBenefic(cadtemp + ConsecutivoIN + ConsecutivoOUT + (String)datasession.get("teller"));            
            
            diario = new Diario();
            sMessage = diario.invokeDiario(oTxn, (String)session.getAttribute("d_ConsecLiga"), dirip, "US$", session);

            Monto = cGenericas.delCommaPointFromString((String)datasession.get("txtMonto"));
            if(sMessage.startsWith("0"))
            {
                txtCasoEsp = txtCasoEsp + "0821" + "~" + cGenericas.getString(oTxn.getMessage(),1) + "~";
                String txtMonto = cGenericas.delCommaPointFromString((String)datasession.get( "txtMonto" ));
                String txtMonto1 = cGenericas.delCommaPointFromString((String)datasession.get( "txtMonto1" ));
                txtTipoCambio = cGenericas.StringFiller((String)datasession.get( "txtTipoCambio" ), ((String)datasession.get( "txtTipoCambio" )).length()+2, false, "0");
                txtTipoCambio = "0000" + txtTipoCambio;
                txtTipoCambio = txtTipoCambio.substring(txtTipoCambio.length()-7);
                txtTipoCambio = txtTipoCambio.substring(0,3) + "." + txtTipoCambio.substring(3,6);
                String txtDDACuenta1 = (String)datasession.get( "txtDDACuenta2" );
                txtDDACuenta1 = txtDDACuenta1.substring(1);
                String txtDDACuenta2 = (String)datasession.get( "txtDDACuenta" );
                String txtSerial = (String)datasession.get( "txtSerial2" );
                String txtBeneficiario = (String)datasession.get( "txtBeneficiario" );
                if ( txtDDACuenta2 == null )
                    txtDDACuenta2 = (String)datasession.get( "txtCDACuenta" );
                txtCadImpresion = txtCadImpresion + "~COMPRAVENTA~" + TE +"-" + TO +"~" + txtNomCliente.trim() + "~" +
                txtMonto + "~" + txtTipoCambio + "~" + txtMonto1 + "~" + txtSerial + "~" +
                txtDDACuenta1 + "~" + txtDDACuenta2 + "~" + "" + "~" + txtBeneficiario + "~"  +
                " " + "~" + " " + "~" + " " + "~" + Currency + "~";
            }
        }
        if(txtCasoEsp.length() > 0){
            session.setAttribute("txtCasoEsp", txtCasoEsp);
        }
        session.setAttribute("txtCadImpresion", sMessage);
        if(txtCadImpresion.length() > 0){
            session.setAttribute("txtCadImpresion", txtCadImpresion);
        }
        session.setAttribute("page.txnresponse", sMessage);
        getServletContext().getRequestDispatcher("/servlet/ventanilla.Redirector").forward(req,resp);
    }

    public void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doPost(req, resp);
    }
}
