//*************************************************************************************************
//             Funcion: Servlet para posteo de txns 0082 y 0083
//            Elemento: Logoff.java
//          Creado por: Alejandro Gonzalez Castro
//      Modificado por: Juvenal R. Fernandez V.
//      Modificado por: Fredy Pe�a Moreno.
//*************************************************************************************************
// CCN - 4360178 - 20/08/2004 - Se agrega txn 0083 y se journalizan txns 0082 y 0083
// CCN - 4360189 - 03/09/2004 - Se cambia invoke al Diario para Control de Efectivo
// CCN - 4360211 - 08/10/2004 - Se elimina mensaje de documentos pendientes por digitalizar
// CCN - 4360291 - 18/03/2005 - Se agrega codigo para redireccionar al ResponseLogoff.jsp
// CCN - 4360297 - 23/03/2005 - Se cambia CCN por peticion de QA, CCN anterior 4360291
// CCN - 4360334 - 17/06/2005 - Se elimina variable dbQualifier
// CCN - 4360387 - 07/10/2005 - Se realizan adecuaciones para pedir autorizaci�n cuando se desfirme el cajero.
// CCN - 4620008 - 14/09/2007 - se elimina mensaje de documentos pendientes por digitalizar
//*************************************************************************************************

package ventanilla;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import ventanilla.com.bital.sfb.GerenteD;
import ventanilla.com.bital.admin.Diario;
import ventanilla.com.bital.util.VolanteoDDA;

public class Logoff extends HttpServlet {
	
	public void doPost(HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException
	 {
		response.setContentType("text/plain");
		HttpSession session = request.getSession(false);
		
	    String tellerlevel = "";
        String PageAutoriz="";
        String[] page;
        tellerlevel = (String)session.getAttribute("tellerlevel");
        String sup = (String)request.getParameter("supervisor");
        
        if( session.getAttribute("FlagAutoriz") == null)
        {
          PageAutoriz="0";  
          page = request.getParameterValues("salida");
        }  
        else
        {
          if (sup == null)	
          {
          PageAutoriz="0";  
          page = request.getParameterValues("salida");	
          }
          else
          {	
          PageAutoriz="1";  
          page =(String[])session.getAttribute("Flagpage");
          }
        }  
        session.setAttribute("OutTxn",page[0]);       
               
		if (session == null)
			return;

		if (page == null && PageAutoriz.equals("0"))
		 {
		 	session.setAttribute("procesosdisponibles.Error","Imposible determinar el requerimiento, intente despuis");
			response.sendRedirect("../ventanilla/paginas/error.jsp");
			return;
		}

		String statementPS = com.bital.util.ConnectionPoolingManager.getStatementPostFix();

		VolanteoDDA oVolanteoDDA =  new VolanteoDDA();

		oVolanteoDDA.setTeller(session.getAttribute("teller").toString().substring(4, 6));
		oVolanteoDDA.setBranch(session.getAttribute("branch").toString().substring(1));
		String sMessage = "";

		oVolanteoDDA.searchDoctosDig("1");
		int docsdig = oVolanteoDDA.getNumDoctosNotGiven();

		oVolanteoDDA.searchDoctosDig("2");
		int docsproc = oVolanteoDDA.getNumDoctosNotGiven();


		if (docsdig != docsproc && PageAutoriz.equals("0") && tellerlevel.equals("0")) {
			sMessage = "Tiene documentos pendientes por digitalizar";
			HttpSession sessions = request.getSession(false);
			sessions.setAttribute("Flagpage", page);
			sessions.setAttribute("mensaje", sMessage);
			response.sendRedirect("../ventanilla/paginas/ResponseLogoff.jsp");
		}else
		{

		int code = 0;
		String coma = "'";
		String sql_req = "";
		String status = "";

		GerenteD gerented = new GerenteD();
		Diario diario = new Diario();

		if (page[0].equals("1")) //0083
			{
			gerented.setBranch((String) session.getAttribute("branch"));
			gerented.setTxnCode("0083");
			gerented.setTeller((String) session.getAttribute("teller"));
			gerented.setSupervisor((String) session.getAttribute("teller"));
			gerented.setDescrip("SALIDA PERMANENTE");
		} else {
			gerented.setBranch((String) session.getAttribute("branch"));
			gerented.setTxnCode("0082");
			gerented.setTeller((String) session.getAttribute("teller"));
			gerented.setSupervisor((String) session.getAttribute("teller"));
			gerented.setDescrip("SALIDA TEMPORAL");
		}
		GenericClasses gc = new GenericClasses();

		String resp = diario.invokeDiario(gerented,gc.getDate(3),(String) session.getAttribute("dirip"),"N$",session);

		if (resp.substring(0, 1).equals("0")) {
			String teller = ((String) session.getAttribute("teller"));
			status = "N";
			java.sql.Connection pooledConnection = com.bital.util.ConnectionPoolingManager.getPooledConnection();
			java.sql.PreparedStatement newPreparedStatement = null;
			try {
				if (pooledConnection != null) {
					newPreparedStatement = pooledConnection.prepareStatement("UPDATE tc_Usuarios_Suc SET d_Sign_Status = 'N' WHERE c_Cajero = ? "+ statementPS);
					newPreparedStatement.setString(1, teller);
					code = newPreparedStatement.executeUpdate();
					if (code > 1)
						System.out.println("Error Logoff::doPost::Update>1 -" + code + "-");

				}
			} 
			catch (java.sql.SQLException sqlexception) {
				System.out.println("Error en Logoff::doPost ["+ sqlexception.toString()+ "]");
			} finally {
				try {
					if (newPreparedStatement != null) {
						newPreparedStatement.close();
						newPreparedStatement = null;
					}
					if (pooledConnection != null) {
						pooledConnection.close();
						pooledConnection = null;
					}
				} catch (java.sql.SQLException sqlException) {
					System.out.println("Error Logoff::doPost FINALLY: ["+ sqlException.toString()+ "] ");
				}
			}

		} else
			code = 1;

		if (code < 1) {
			String message = "ERROR AL ACTUALIZAR BASE DE DATOS, INTENTE DE NUEVO POR FAVOR";
			HttpSession sessions = request.getSession(false);
			sessions.setAttribute("mensaje", message);
			response.sendRedirect("../../ventanilla/paginas/ResponseGTE.jsp");
		} else {
			HttpSession sessions = request.getSession(false);
			sessions.setAttribute("mensaje", sMessage);
			response.sendRedirect("../ventanilla/paginas/ResponseLogoff.jsp");
		}
		
	  }		
		
	}

	public void doGet(HttpServletRequest request, HttpServletResponse response)
		throws ServletException, IOException {
		doPost(request, response);
	}
}
