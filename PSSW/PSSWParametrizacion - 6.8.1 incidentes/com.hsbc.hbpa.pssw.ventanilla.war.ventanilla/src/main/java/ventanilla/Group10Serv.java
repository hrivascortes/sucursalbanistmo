//*************************************************************************************************
//             Funcion: Clase que realiza txn Servicios
//            Elemento: Group10Serv.java
//      Modificado por: Alejandro Gonzalez Castro
//*************************************************************************************************
// CCN - 4360189 - 03/09/2004 - Se cambia invoke al Diario para Control de Efectivo
// CCN - 4360253 - 07/01/2005 - Se envia efectivo solo cuando la txn es con efectivo
//*************************************************************************************************
package ventanilla;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.Hashtable;

import java.util.Stack;

import ventanilla.com.bital.sfb.ServiciosE;
import ventanilla.com.bital.admin.Diario;

public class Group10Serv extends HttpServlet {
    
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession(false);
        Hashtable datasession = (Hashtable)session.getAttribute("page.datasession");
        GenericClasses GC = new GenericClasses();
        
        String txn = (String)datasession.get("cTxn");
        String moneda = GC.getDivisa((String)datasession.get("moneda"));
        
        long efectivo = Long.parseLong(GC.delCommaPointFromString((String)datasession.get("txtEfectivo")));
        long monto = Long.parseLong(GC.delCommaPointFromString((String)datasession.get("txttotal")));
        String ftime = (String)session.getAttribute("ftime");
        if ((efectivo < monto) && ftime.equals("1")) {
            long docs = monto - efectivo;
            datasession.put("txtCheque", GC.addpunto(Long.toString(docs)));
            session.setAttribute("ftime","2");
            Stack flujotxn = (Stack)session.getAttribute("page.flujotxn");
            String[] newflujo = {"S" + txn.substring(1),"5353"};
            for (int i=0; i<newflujo.length; i++)
                flujotxn.push(newflujo[i]);
            session.setAttribute("page.flujotxn",flujotxn);
            getServletContext().getRequestDispatcher("/servlet/ventanilla.PageServlet?transaction=" + datasession.get("iTxn") + "&transaction1=" + datasession.get("oTxn")).forward(request,response);
        }
        else {
            String dirip =  (String)session.getAttribute("dirip");
            String sMessage = "";
            txn = "5503";
            ServiciosE servicioE = new ServiciosE();
            servicioE.setBranch((String)datasession.get("sucursal"));
            servicioE.setTeller((String)datasession.get("teller"));
            servicioE.setSupervisor((String)datasession.get("teller"));
            if ( (String)datasession.get("override") != null )
                if (datasession.get("override").toString().equals("SI"))
                    servicioE.setOverride("3");
            if ( (String)datasession.get("supervisor") != null )
                servicioE.setSupervisor((String)datasession.get("supervisor"));
            servicioE.setFormat("A");
            String Monto = "";
            servicioE.setTxnCode(txn);
            servicioE.setAcctNo("917");
            Monto = GC.delCommaPointFromString((String)datasession.get("txttotal"));
            servicioE.setTranAmt(Monto);
            String strReferencia = (String)datasession.get("txtcredito");
            servicioE.setReferenc1(strReferencia);
            strReferencia = (String)session.getAttribute("txtfactor") + (String)session.getAttribute("txttipoinf") + (String)session.getAttribute("txtmesinf") + (String) session.getAttribute("txtverif");
            servicioE.setReferenc2(strReferencia);
            Monto = "";
            if (efectivo != 0)
                Monto = GC.delCommaPointFromString((String)datasession.get("txtEfectivo"));
            else
                Monto = GC.delCommaPointFromString((String)datasession.get("txttotal"));
            session.removeAttribute("txtfactor");
            session.removeAttribute("txttipoinf");
            session.removeAttribute("txtmesinf");
            session.removeAttribute("txtverif");
            
            if (efectivo != 0)
                servicioE.setCashIn(Monto);
            else
                servicioE.setCashIn("000");                
            servicioE.setFromCurr(moneda);
            servicioE.setDraftAm("000");
            
            Diario diario = new Diario();
            sMessage = diario.invokeDiario(servicioE, (String)session.getAttribute("d_ConsecLiga"), dirip, moneda, session);
            
            session.setAttribute("page.txnresponse", sMessage);
            getServletContext().getRequestDispatcher("/servlet/ventanilla.Redirector").forward(request,response);
        }
    }
    
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }  
}
