//*************************************************************************************************
//             Funcion: Clase que realiza txn G061,G063
//            Elemento: Group01Suas.java
//          Creado por: Fausto Rodrigo Flores Moreno
//*************************************************************************************************
// CCN - 4360532 - 20/10/2006 - Se crea servlet para control de giros
//*************************************************************************************************
package ventanilla; 

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Hashtable;
import java.util.Stack;
import java.util.Vector;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.bital.util.ConnectionPoolingManager;

public class Group01Giros extends HttpServlet 
{

	private String Fecha(String cad){
        StringBuffer cadena = new StringBuffer(cad);
        for(int i=0; i<cadena.length();) {
   			if( cadena.charAt(i) == ' ')
          		cadena.deleteCharAt(i);
            else
   		        ++i;
        }
        return cadena.toString();
  	}
  	
  	private int UpdateLog(String n_giro, String n_sucursal){
  		int ResultUpdate = 0;
        Connection pooledConnection = ConnectionPoolingManager.getPooledConnection();
		PreparedStatement update = null;
		String statementPS = com.bital.util.ConnectionPoolingManager.getStatementPostFix();
		
		try
		{
			if(pooledConnection != null)
			{
				update = pooledConnection.prepareStatement("UPDATE TW_GIROS SET D_STATUS = 'C', D_MONTO = '000' WHERE C_GIRO = ? AND C_NUM_SUCURSAL  = ?");
				update.setString(1, n_giro);
				update.setString(2, n_sucursal);
				ResultUpdate = update.executeUpdate();
			}    
		}catch(SQLException sqlException){
	        System.out.println("TW_GIROS::UpdateLog [" + sqlException + "]");            
        }catch(Exception e){
            System.out.println(e.getMessage());
	        e.printStackTrace();
		}        
        finally {
	    	try {
        		if(update != null) {
	            	update.close();
    	            update = null;
                }
	            if(pooledConnection != null) {
    	            pooledConnection.close();
        	        pooledConnection = null;
            	}
            }
	        catch(java.sql.SQLException sqlException){
    	        System.out.println("Error FINALLY TW_GIROS::UpdateLog [" + sqlException + "]");                
           	}
	    } 		
  		return  ResultUpdate;
  	}

  	private int InsertLog(Vector datos){
  		int ResultUpdate = 0;
        Connection pooledConnection = ConnectionPoolingManager.getPooledConnection();
		PreparedStatement insert = null;
		String statementPS = com.bital.util.ConnectionPoolingManager.getStatementPostFix();
		int i=0;
		try
		{
			if(pooledConnection != null)
			{
				insert = pooledConnection.prepareStatement("INSERT INTO TW_GIROS VALUES(?,?,?,?,?,?,?,?,?)");
				for(i=0; i<datos.size();i++){
					insert.setString(i+1, datos.get(i).toString());
				}
				ResultUpdate = insert.executeUpdate();
			}    
		}catch(SQLException sqlException){
	        System.out.println("TW_GIROS::InsertLog [" + sqlException + "]");            
        }catch(Exception e){
            System.out.println(e.getMessage());
	        e.printStackTrace();
		}        
        finally {
	    	try {
        		if(insert != null) {
	            	insert.close();
    	            insert = null;
                }
	            if(pooledConnection != null) {
    	            pooledConnection.close();
        	        pooledConnection = null;
            	}
            }
	        catch(java.sql.SQLException sqlException){
    	        System.out.println("Error FINALLY TW_GIROS::UpdateLog [" + sqlException + "]");                
           	}
	    } 		
  		return  ResultUpdate;
  	}

  	private Vector SelectLog(String n_giro, String c_moneda,String n_sucursal, String ctxn){
  		Vector ResultSelect = new Vector();
  		ResultSelect.addElement("0");
  		int l = ResultSelect.size();
  		String Status = "";
  		Connection pooledConnection = ConnectionPoolingManager.getPooledConnection();
		PreparedStatement select = null;
		ResultSet Rselect = null;
		String statementPS = com.bital.util.ConnectionPoolingManager.getStatementPostFix();
		try
		{
			if(pooledConnection != null)
			{	
				select = pooledConnection.prepareStatement("SELECT * FROM TW_GIROS WHERE C_GIRO=? AND C_MONEDA=? AND C_NUM_SUCURSAL=? ORDER BY C_GIRO ASC");
				select.setString(1, n_giro);
				select.setString(2,c_moneda);
				select.setString(3, n_sucursal);
				Rselect = select.executeQuery();
				while(Rselect.next()){
					ResultSelect.setElementAt("1",l-1);
					Status = Rselect.getString("D_STATUS");
					if(Status.equals("C"))
						ResultSelect.setElementAt("2",l-1);
					if(ctxn.equals("G063")){
					    ResultSelect.addElement(Rselect.getString("C_GIRO"));
					    ResultSelect.addElement(Rselect.getString("D_MONTO"));
					    ResultSelect.addElement(Rselect.getString("D_BENEFICIARIO"));
						ResultSelect.addElement(Rselect.getString("D_BANCO"));
						ResultSelect.addElement(Rselect.getString("D_PLAZA"));
						ResultSelect.addElement(Rselect.getString("C_MONEDA"));
					}
				}
			}    
		}catch(SQLException sqlException){
	        System.out.println("TW_GIROS::SelectLog [" + sqlException + "]");            
        }catch(Exception e){
            System.out.println(e.getMessage());
	        e.printStackTrace();
		}        
        finally {
	    	try {
        		if(select != null) {
	            	select.close();
    	            select = null;
                }
	            if(pooledConnection != null) {
    	            pooledConnection.close();
        	        pooledConnection = null;
            	}
            }
	        catch(java.sql.SQLException sqlException){
    	        System.out.println("Error FINALLY TW_GIROS::SelectLog [" + sqlException + "]");                
           	}
	    } 
  		return  ResultSelect;
  	}  	
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
    {
		response.setContentType("text/plain");
        HttpSession session = request.getSession(false);
        Hashtable datasession = (Hashtable)session.getAttribute("page.datasession");
      
        String txn = (String)datasession.get("cTxn");
        String itxn = (String)datasession.get("iTxn");
        String otxn = (String)datasession.get("oTxn");

        String giro = "";
        String monto = "";
        String beneficiario ="";
        String plaza = "";
        String fecha = "";
        String bancocorres = "";
        String moneda = "";
        String sucursal= "";
        String status = "";
        String sql = "";
      	String mensaje = "1~1~0000000~TRANSACCION RECHAZADA~";
        String txtPrint = "";
        int tipo = 0;
        int consec = 0;
		int code = 0;
		Vector code2 = new Vector();	
        
        boolean existe = false;
        
        sucursal = (String)datasession.get("sucursal");
        sucursal = sucursal.substring(1);
      
        if (txn.equals("G061") && itxn.equals("G063")) {
            giro = (String)datasession.get("txtGiro");
            code = UpdateLog(giro,sucursal);
            if(code == 1)
			{
                mensaje = "0~1~0000000~TRANSACCION ACEPTADA, GIRO CANCELADO~";
                session.setAttribute("txtCadImpresion", "NOIMPRIMIRNOIMPRIMIRNOIMPRIMIR");
            }
            else
                System.out.println("Error Group01Giro::doPost::Update>1 [" + code + "]");
        }
        else{
			moneda = (String)datasession.get("moneda");
        	if(txn.equals("G061")) {
                giro = (String)datasession.get("txtGiro");
                txtPrint = "GIROCITI3~EXP~"+giro+"~";
				GenericClasses gc = new GenericClasses();
                monto = gc.delCommaPointFromString((String)datasession.get("txtMonto"));
                beneficiario = (String)datasession.get("txtBenefiGiro");
                plaza = (String)datasession.get("txtPlaza");
                fecha = Fecha((String)datasession.get("fecha"));
                bancocorres = (String)datasession.get("txtBancoC");
            }
            
            if(txn.equals("G063")) {
                giro = (String)datasession.get("txtGiro");

            }
            code2 = SelectLog(giro, moneda,sucursal,txn);
            if(code2.get(0).toString().equals("0")){
	        	mensaje = "1~1~0000000~EL NO. DE GIRO NO EXISTE~";
	        	existe = false;
            }
            else if(code2.get(0).toString().equals("1")){
	        	mensaje = "1~1~0000000~EL NO. DE GIRO YA EXISTE EN LA BASE DE DATOS, VERIFIQUE~";
	        	existe = true;
            }
            else{
	            mensaje = "1~1~0000000~EL GIRO YA FUE CANCELADO~";
	            existe = false;
            }
            if(code2.size()>1) {
	            giro = code2.get(1).toString();
                monto = code2.get(2).toString();
                beneficiario = code2.get(3).toString();
                bancocorres = code2.get(4).toString();
                plaza = code2.get(5).toString();
                moneda = code2.get(6).toString();
            }
        
        	if(existe == false && txn.equals("G061"))
        	{
        		Vector vGiro = new Vector();
        		vGiro.addElement(giro);
        		vGiro.addElement(moneda);
        		vGiro.addElement(fecha);
        		vGiro.addElement(sucursal);
        		vGiro.addElement(beneficiario);
        		vGiro.addElement(monto);
        		vGiro.addElement(bancocorres);
        		vGiro.addElement(plaza.trim());
        		vGiro.addElement("E");
        		code = InsertLog(vGiro);
                if( code == 1)
                {
	                mensaje = "0~1~0000000~TRANSACCION ACEPTADA~";
                    session.setAttribute("txtCadImpresion", txtPrint);                            
                }
	     	}
             if(existe == true && txn.equals("G063")) {
                Stack flujotxn = (java.util.Stack)session.getAttribute("page.flujotxn");
				if(flujotxn.isEmpty()){
					flujotxn.push("G061");
				}
                datasession.put("txtGiro", giro);
                datasession.put("txtBenefiGiro", beneficiario);
                datasession.put("txtBancoC", bancocorres);
                datasession.put("txtPlaza", plaza);
                datasession.put("txtMonto", monto);
                datasession.put("moneda",moneda);
                datasession.put("flagGiro","1");
                
                session.setAttribute("page.datasession", datasession);
                
                mensaje = "0~1~0000000~TRANSACCION ACEPTADA~";
            }
        }
        session.setAttribute("page.txnresponse", mensaje);
        getServletContext().getRequestDispatcher("/servlet/ventanilla.Redirector").forward(request,response);   
    }
    
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}
