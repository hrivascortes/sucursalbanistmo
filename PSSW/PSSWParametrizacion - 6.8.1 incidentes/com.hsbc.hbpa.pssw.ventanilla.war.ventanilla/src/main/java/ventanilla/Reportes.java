//*************************************************************************************************
//             Funcion: Clase que realiza ela consulta de la tabla TW_CTRL_LOG
//            Elemento: Reportes.java
//          Creado por: Juvenal R. Fernandez Varela
//      Modificado por: Jes�s Emmanuel L�pez Rosales
//*************************************************************************************************
// CCN - 4360364 - 19/08/2005 - Se crea clase para la obtencion de reportes del GTE
// CCN - 4360456 - 07/04/2006 - Se agraga codigo para reportes por sucursal y cajero en Reverso y Autorizaciones.
// CCN - 4360510 - 08/09/2006 - Obtiene la informacion de la tabla de venta_log del reporte de adm de usuarios
//*************************************************************************************************

package ventanilla; 

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.sql.*;
import java.util.*;


import com.bital.util.ConnectionPoolingManager;

public class Reportes extends HttpServlet {
	
	private String cajero = null;
	private String Tipo = null;
	
		/**
	 * Returns the cajero.
	 * @return String
	 */
	public String getCajero() {
		return cajero;
	}

	/**
	 * Returns the tipo.
	 * @return String
	 */
	public String getTipo() {
		return Tipo;
	}

	/**
	 * Sets the cajero.
	 * @param cajero The cajero to set
	 */
	public void setCajero(String cajero) {
		this.cajero = cajero;
	}

	/**
	 * Sets the tipo.
	 * @param tipo The tipo to set
	 */
	public void setTipo(String tipo) {
		Tipo = tipo;
	}
		
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession(false);
		GenericClasses GC = new GenericClasses();
		String T = (String)request.getParameter("TipoRep");
		String TipoB = (String)request.getParameter("TipoBusqueda");
		int totalpages = 0;
		int res = 0;
		
		if(T.equals("2"))
			this.setTipo("R");
		else if(T.equals("3"))
			this.setTipo("A");
		else if(T.equals("4"))
			this.setTipo("U");
			
		this.setCajero((String)request.getParameter("cajero"));
		
		Vector Select = SelectLog(TipoB, T);
        session.setAttribute("Log.Registros",Select);
        session.setAttribute("Log.Tipo",this.getTipo());
        session.setAttribute("Log.TipoB",TipoB);
        session.setAttribute("Log.pagina","0");
        res = Select.size()%10;
        totalpages = Select.size()/10;
        if (res>0)
          totalpages++;
        session.setAttribute("Log.totalpages",String.valueOf(totalpages));
        if(T.equals("2") || T.equals("3"))
           response.sendRedirect("../ventanilla/paginas/ResponseReportes.jsp");
        else if(T.equals("4"))
           response.sendRedirect("../ventanilla/paginas/ResponseReportes2.jsp");
    }
    
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
    
	public Vector SelectLog(String Tipo, String Opcion) {
		GenericClasses GC = new GenericClasses();
		String PostFixUR = ConnectionPoolingManager.getPostFixUR();    
		StringBuffer StrAccion = new StringBuffer();
		if (Opcion.equals("2") || Opcion.equals("3")){
  			StrAccion.append("select tw_ctrl_log.D_SUCURSAL, tw_ctrl_log.D_OPERADOR,"); 
  			StrAccion.append("tw_ctrl_log.D_CONNUREG, tw_ctrl_log.D_NOM_CAJERO,"); 
  			StrAccion.append("tw_ctrl_log.D_RACF_CAJERO, tw_ctrl_log.D_DIRECCION_IP,"); 
  			StrAccion.append("tw_diario_electron.D_SUPERVISOR, tw_ctrl_log.D_NOM_SUP,");
  			StrAccion.append("tw_ctrl_log.D_RACF_SUP, tw_ctrl_log.D_DIRIP_SUP, tw_diario_electron.D_TXN,");  			  			
  			StrAccion.append("tw_diario_electron.D_FECHOPER, tw_ctrl_log.D_HORA,");
  			StrAccion.append("tw_diario_electron.D_MONTO, tw_diario_electron.D_DIVISA from ");
            StrAccion.append("tw_ctrl_log INNER JOIN tw_diario_electron ON ");
            StrAccion.append("tw_ctrl_log.D_SUCURSAL = tw_diario_electron.D_SUCURSAL and ");
            StrAccion.append("tw_ctrl_log.D_OPERADOR = tw_diario_electron.D_OPERADOR and ");
            StrAccion.append("tw_ctrl_log.D_CONNUREG = tw_diario_electron.D_CONNUREG and ");
            StrAccion.append("tw_ctrl_log.D_SUCURSAL='" + "0"+this.getCajero().substring(0,4) + "' and ");
            if(Tipo.equals("S"))
              StrAccion.append("tw_ctrl_log.D_OPERADOR LIKE '"+ this.getCajero().substring(0,4) + "%' and ");
            else
              StrAccion.append("tw_ctrl_log.D_OPERADOR='"+ this.getCajero() + "' and ");
            StrAccion.append("tw_diario_electron.D_FECHOPER= '"+GC.getDate(1)+"' and ");
            StrAccion.append("tw_ctrl_log.D_DESCRIP='"+ this.getTipo() + "' ORDER BY tw_ctrl_log.D_OPERADOR " + PostFixUR);
		}else {
  			StrAccion.append("SELECT C_CAJERO, D_ACCION, C_CAJERO_AFECT, ");
  			StrAccion.append("D_DATO_ANTERIOR, D_DATO_POSTERIOR, D_HORA "); 
  			StrAccion.append("FROM TW_VENTA_LOG WHERE "); 

            if(Tipo.equals("S"))
              StrAccion.append("C_NUM_SUCURSAL= '"+ this.getCajero().substring(0,4) + "' ");
            else
              StrAccion.append("C_CAJERO_AFECT= '"+ this.getCajero() + "' ");
            StrAccion.append("ORDER BY C_CAJERO " + PostFixUR);		   
		}	    
		
		String sql = StrAccion.toString();		
		
		Connection pooledConnection = ConnectionPoolingManager.getPooledConnection( );
		PreparedStatement newPreparedStatement = null;
		ResultSet query = null;
		Vector Registros = new Vector();
		try{
		    if(pooledConnection != null) {
		        newPreparedStatement = pooledConnection.prepareStatement(sql);        		        
		        query = newPreparedStatement.executeQuery();
		        while(query.next())
		        { 
					Vector tmp = new Vector();
					for(int columnNumber = 1; columnNumber <= query.getMetaData().getColumnCount(); columnNumber++)
					{
						String columnData = query.getString(columnNumber);
                		
                		if (Opcion.equals("2") || Opcion.equals("3")){						
							if(columnData == null)
								columnData = "";
							else if(columnNumber==3)
							{
								columnData = columnData.substring(8,14);
								columnData = columnData.substring(0,2)+":"+columnData.substring(2,4)+":"+columnData.substring(4,6);
							}
							else if(columnNumber==12) 
								columnData = columnData.substring(6,8)+"-"+columnData.substring(4,6)+"-"+columnData.substring(0,4);
							else if(columnNumber==13) 
								columnData = columnData.substring(0,2)+":"+columnData.substring(2,4)+":"+columnData.substring(4,6);
							else if(columnNumber==14)  
								columnData = GC.formatMonto(columnData);
                		}else{
							   if(columnData == null)
								  columnData = "";
							   else if(columnNumber==2 || columnNumber==4 || columnNumber==5){
                                   
                                    columnData = columnData.trim();
                                    if (columnData.indexOf("-") == (columnData.length()-1) && columnData.length() > 2)
                                       columnData = columnData.substring(0,(columnData.length()-1));                                            
							   }else if(columnNumber==6)
                                   columnData = columnData.substring(0,2)+":"+columnData.substring(2,4)+":"+columnData.substring(4,6);							                   	      		
                			
                		}	
						tmp.addElement(columnData);						
					}
					Registros.addElement(tmp);
		        }
		    }
		}
		catch(java.sql.SQLException sqlException) {
		    System.out.println("Error firma::getIP: [" + sql + "] ");
		    System.out.println("Error firma::getIP: [" + sqlException.toString() + "] ");
		}
		finally {
		    try{
		        if(query != null){
		            query.close();
		            query = null;
		        }
		        if(newPreparedStatement != null){
		            newPreparedStatement.close();
		            newPreparedStatement = null;
		        }
		        if(pooledConnection != null){
		            pooledConnection.close();
		            pooledConnection = null;
		        }
		    }
		    catch(java.sql.SQLException sqlException){
		        System.out.println("Error firma::getIP FINALLY: [" + sqlException.toString() + "] ");
		    }
		}
				
        return Registros;
    }

}
