//*************************************************************************************************
//             Funcion: Clase que realiza txn 0554
//            Elemento: CV_0554_XXXX.java
//          Creado por: Alejandro Gonzalez Castro
//		Modificado por: Jes�s Emmanuel L�pez Rosales
//*************************************************************************************************
// CCN - 4360189 - 03/09/2004 - Se cambia invoke al Diario para Control de Efectivo
// CCN - 4360268 - 04/02/2005 - Se realizan adecuaciones para eliminar Agencia NY
// CCN - 4360368 - 02/09/2005 - Se elimina sleep
// CCN - 4360374 - 08/09/2005 - Se genera nuevo paquete a peticion de QA.
// CCN - 4360532 - 20/10/2006 - Se realizan modificaciones para enviar el numero de cajero, consecutivo txn entrada y salida en txn 0821
//*************************************************************************************************
package ventanilla;

import java.io.IOException;
import java.util.Hashtable;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import ventanilla.com.bital.admin.Diario;
import ventanilla.com.bital.sfb.CompraVenta;

public class CV_0554_XXXX extends HttpServlet {

    public void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {
        String TE, TO, Currency;
        int Resultado = -1;

        CompraVenta oTxn = new CompraVenta();
        String sMessage = null;
        Diario diario;
        GenericClasses cGenericas = new GenericClasses();

        HttpSession session = req.getSession(false);
        Hashtable datasession = (Hashtable)session.getAttribute("page.datasession");
        TE = (String)datasession.get("cTxn");
        TO = (String)datasession.get("oTxn");
        Currency = (String)datasession.get("moneda");
        String txtPlaza = (String)datasession.get("txtPlaza");
        String dirip =  (String)session.getAttribute("dirip");
        String msg = null;
        String txtCasoEsp = new String("");
        String txtCadImpresion = new String("");
        String txtSupervisor = (String)datasession.get("teller");
        String txtRegistro = (String)datasession.get("txtRegistro");
        String txtOverride = "0";
        String Currency2 = "";
        String Monto = "";
        String ConsecutivoIN = "";
        String ConsecutivoOUT = "";        
        if ( Currency.equals("01"))
            Currency2 = "02";
        else
            Currency2 = "01";

        if ( (String)datasession.get("supervisor") != null )       //pidio autorizacisn de supervisor
            txtSupervisor =  (String)datasession.get("supervisor");
        
        if ( (String)datasession.get("override") != null )
            if (datasession.get("override").toString().equals("SI"))
                  txtOverride = "3";
        
        if ( datasession.get("posteo") == null ) {
            datasession.put("posteo","1");
            session.setAttribute("page.datasession",(Hashtable)datasession);
        }
        
        if ( session.getAttribute("txtCasoEsp") != null )
            txtCasoEsp = session.getAttribute("txtCasoEsp").toString();   

        oTxn.setTxnCode(TE);
        oTxn.setBranch((String)datasession.get("sucursal"));
        oTxn.setTeller((String)datasession.get("teller"));
        oTxn.setSupervisor(txtSupervisor);
        oTxn.setOverride(txtOverride);

        if( TE.equals( "0554" ) )
        {
            //ODCT 0554B 00001000131  000131  000**123*600000*1234567890*002*****N$**
            oTxn.setFormat( "B" );
            oTxn.setTxnCode(TE);
            oTxn.setAcctNo( (String)datasession.get("txtDias") );
            if(Currency.equals("02")) {
                oTxn.setFromCurr("US$");
                oTxn.setTranAmt( cGenericas.delCommaPointFromString((String)datasession.get( "txtMonto1" )) );
            }
            else
            {
                oTxn.setFromCurr("N$");
                oTxn.setTranAmt( cGenericas.delCommaPointFromString((String)datasession.get( "txtMonto" )) );
            }
            oTxn.setReferenc((String)datasession.get("txtSerial"));
            oTxn.setFees((String)datasession.get("lstBancoRemCV"));

            if ( datasession.get("posteo").toString().equals("1")){
                diario = new Diario();
                sMessage = diario.invokeDiario(oTxn, (String)session.getAttribute("d_ConsecLiga"), dirip, Currency, session);
                
                Resultado = oTxn.getStatus();
                
                if ( Currency.equals("01"))
                    Monto = cGenericas.delCommaPointFromString((String)datasession.get("txtMonto"));
                else
                    Monto = cGenericas.delCommaPointFromString((String)datasession.get("txtMonto1"));
                
                if(sMessage.startsWith("0") || sMessage.startsWith("1")) {
                    txtCasoEsp = txtCasoEsp + TE + "~" + cGenericas.getString(oTxn.getMessage(),1) + "~";
                    ConsecutivoIN = cGenericas.getString(oTxn.getMessage(),1);
                    if (ConsecutivoIN.length()<6){
                   	    ConsecutivoIN = "0" + ConsecutivoIN; 
                    }                    
                }
            }
            else
                Resultado = 0;
        }
        if ( TE.equals("0554") && Resultado == 0 )
        {   // Cargo fue aceptado
            if ( datasession.get("posteo").toString().equals("1")) {
                 datasession.put("override","NO");
                 datasession.put("posteo","2");
                 txtOverride = "0";
            }

            oTxn = new CompraVenta();
            oTxn.setBranch((String)datasession.get("sucursal"));
            oTxn.setTeller((String)datasession.get("teller"));
            oTxn.setSupervisor(txtSupervisor);
            oTxn.setOverride(txtOverride);
            //ODCT 4385A 00001000131  000131  000**7000001321**60000*US$*1234567890*00201***00100100000*600000**CARGO POR DEV REMESA TC0100000 C01 D123***********************************123*0554**
            oTxn.setTxnCode(TO);
            oTxn.setFormat( "A" );
            oTxn.setAcctNo( (String)datasession.get( "txtDDACuenta" ) );
            oTxn.setCheckNo((String)datasession.get("txtSerial"));
            oTxn.setTranAmt( cGenericas.delCommaPointFromString((String)datasession.get( "txtMontoC" )) );
            String Curr = null;
            if(Currency.equals("01"))
            {
                oTxn.setTranCur( "US$" );
                oTxn.setMoAmoun( cGenericas.delCommaPointFromString((String)datasession.get( "txtMonto" )) );
            }
            else
            {
                oTxn.setTranCur( "N$" );
                oTxn.setMoAmoun( cGenericas.delCommaPointFromString((String)datasession.get( "txtMonto1" )) );
            }
            oTxn.setTrNo((String)datasession.get("lstBancoRemCV") + (String)datasession.get("lstCausa"));
            oTxn.setCheckNo5((String)datasession.get("txtDias"));
            String txtTipoCambio = cGenericas.StringFiller((String)datasession.get( "txtTipoCambio" ), ((String)datasession.get( "txtTipoCambio" )).length()+3, false, "0");
            txtTipoCambio = "0000" + txtTipoCambio;
            txtTipoCambio = txtTipoCambio.substring(txtTipoCambio.length()-8);
            oTxn.setDraftAm(txtPlaza + txtTipoCambio);
            oTxn.setTranDesc("CARGO POR DEV REMESA TC" + txtTipoCambio.substring(1) + " C" + datasession.get("lstCausa").toString() + " D" + datasession.get("txtDias").toString());
            oTxn.setTrNo5( "0554" );
            oTxn.setDescRev("EMPTY");

            diario = new Diario();
            sMessage = diario.invokeDiario(oTxn, (String)session.getAttribute("d_ConsecLiga"), dirip, Currency2, session);

            Resultado = oTxn.getStatus();

            if ( Currency2.equals("01"))
                Monto = cGenericas.delCommaPointFromString((String)datasession.get("txtMonto"));
            else
                Monto = cGenericas.delCommaPointFromString((String)datasession.get("txtMonto1"));

            if(sMessage.startsWith("0") || sMessage.startsWith("1"))
            {
                txtCasoEsp = txtCasoEsp + TO + "~" + cGenericas.getString(oTxn.getMessage(),1) + "~";
                ConsecutivoOUT = cGenericas.getString(oTxn.getMessage(),1);
                if (ConsecutivoOUT.length()<6){
                   	ConsecutivoOUT = "0" + ConsecutivoOUT; 
                }                
            }
        }

        if (  TE.equals("0554") && Resultado == 0)
        {   // Cargo y Abono Aceptado

            oTxn = new CompraVenta();
            oTxn.setBranch((String)datasession.get("sucursal"));
            oTxn.setTeller((String)datasession.get("teller"));
            oTxn.setSupervisor(txtSupervisor);
            oTxn.setTxnCode("0821");
            oTxn.setFormat( "G" );
            oTxn.setToCurr( "US$" );
            oTxn.setGStatus( "6" );
            if(Currency.equals("02"))
                oTxn.setService( "C" );
            else
                oTxn.setService( "V" );
            String txtAutoriCV = (String)datasession.get("txtAutoriCV");
            if ( txtAutoriCV == null )
                txtAutoriCV = "00000";
            oTxn.setCheckNo( "00000" + txtAutoriCV );
            oTxn.setTranAmt( cGenericas.delCommaPointFromString((String)datasession.get( "txtMonto1" )) );
            String txtTipoCambio = cGenericas.StringFiller((String)datasession.get( "txtTipoCambio" ), ((String)datasession.get( "txtTipoCambio" )).length()+2, false, "0") + "00";
            txtTipoCambio = "0000" + txtTipoCambio;
            txtTipoCambio = txtTipoCambio.substring(txtTipoCambio.length()-9);
            oTxn.setComAmt(txtTipoCambio);
            oTxn.setIVAAmt( "0" );
            oTxn.setAcctNo( (String)datasession.get( "registro" ) );
            oTxn.setCtaBenef( (String)datasession.get( "txtRegistro" ) );
            int lon = TE.length();
            for(int j=lon;j<10;j++){
               TE = TE + " ";
            }	
            oTxn.setBenefic(TE + ConsecutivoIN + ConsecutivoOUT + (String)datasession.get("teller"));
            oTxn.setInstruc(TO);

            diario = new Diario();
            sMessage = diario.invokeDiario(oTxn, (String)session.getAttribute("d_ConsecLiga"), dirip, "US$", session);

            Monto = cGenericas.delCommaPointFromString((String)datasession.get("txtMonto"));
            if(sMessage.startsWith("0"))
            {
                txtCasoEsp = txtCasoEsp + "0821" + "~" + cGenericas.getString(oTxn.getMessage(),1) + "~";
            }
        }

        if(txtCasoEsp.length() > 0){
            session.setAttribute("txtCasoEsp", txtCasoEsp);
        }
        session.setAttribute("txtCadImpresion", sMessage);
        if(txtCadImpresion.length() > 0){
            session.setAttribute("txtCadImpresion", txtCadImpresion);
        }
        session.setAttribute("page.txnresponse", sMessage);
        getServletContext().getRequestDispatcher("/servlet/ventanilla.Redirector").forward(req,resp);
    }

    public void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doPost(req, resp);
    }
}
