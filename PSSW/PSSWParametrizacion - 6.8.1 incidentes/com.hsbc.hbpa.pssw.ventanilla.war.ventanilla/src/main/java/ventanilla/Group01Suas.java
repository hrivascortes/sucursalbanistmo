//*************************************************************************************************
//             Funcion: Clase que realiza txn Suas
//            Elemento: Group01Suas.java
//          Creado por: Alejandro Gonzalez Castro
//      Modificado por: Alejandro Gonzalez Castro
//*************************************************************************************************
// CCN - 4360302 - 01/04/2005 - Se evita problema de pago SUA con Cobro Inmediato
// CCN - 4360368 - 02/09/2005 - Se genera la nueva version de SUAS
// CCN - 4360373 - 07/09/2005 - Se genera nuevo CCN a peticion de QA 
// CCN - 4360406 - 15/12/2005 - Nueva version de SUAS 
//*************************************************************************************************
package ventanilla;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.util.Hashtable;

import ventanilla.com.bital.sfb.ServiciosB;
import ventanilla.com.bital.sfb.ServiciosA;
import ventanilla.com.bital.util.VolanteoDDA;
import ventanilla.com.bital.admin.Diario;

public class Group01Suas extends HttpServlet
{
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        HttpSession session = request.getSession(false);
        Hashtable datasession = (Hashtable)session.getAttribute("page.datasession");
        String txn = (String)datasession.get("cTxn");
		GenericClasses gc = new GenericClasses();
        String moneda = gc.getDivisa((String)datasession.get("moneda"));

        String dirip =  (String)session.getAttribute("dirip");
        Hashtable Truncamiento = (Hashtable)session.getAttribute("truncamiento");
        long TruncamientoMN = 500000L;
        if (!Truncamiento.isEmpty() ){
            TruncamientoMN = Long.parseLong(Truncamiento.get("TruncamientoMN").toString());
        }
        VolanteoDDA oVolanteoDDA = null;
        String txtVolante = "NO";
        String txtCadImpresion = "";
        String sMessage = "";
        
        String Monto = "";
        ServiciosB servicio = new ServiciosB();
        servicio.setBranch((String)datasession.get("sucursal"));
        servicio.setTeller((String)datasession.get("teller"));
        servicio.setSupervisor((String)datasession.get("supervisor"));
        servicio.setFormat("B");
        servicio.setTxnCode(txn);
        servicio.setFromCurr(moneda);

        ServiciosA servicioA = new ServiciosA();
        servicioA.setBranch((String)datasession.get("sucursal"));
        servicioA.setTeller((String)datasession.get("teller"));
        servicioA.setSupervisor((String)datasession.get("supervisor"));
        servicioA.setTxnCode(txn);
        servicioA.setFormat("A");
        servicioA.setTranCur(moneda);

        if (txn.equals("0817"))
        {
            servicio.setAcctNo((String)datasession.get("txtnumctrl"));
            servicio.setTranAmt(quitap((String)datasession.get("txtMonto")));
            servicio.setReferenc("0772");
            Diario diario = new Diario();
            sMessage = diario.invokeDiario(servicio, (String)session.getAttribute("d_ConsecLiga"), dirip, moneda, session);
            if(sMessage.charAt(0) =='0')
               txtCadImpresion = "NOIMPRIMIRNOIMPRIMIR";
        }

        if (txn.equals("0772"))
        {
            servicio.setAcctNo((String)datasession.get("txtServicio"));
            Monto = quitap((String)datasession.get("txtimporte"));
            servicio.setTranAmt(Monto);
            servicio.setReferenc((String)datasession.get("txtnumctrl"));
            String tipo = (String)datasession.get("txttipo");
            if (tipo.equals("E"))
            {servicio.setCashIn(Monto);}
            else
            {servicio.setCashIn("000");}
            if (tipo.equals("C"))
               servicio.setIntWh((String)datasession.get("txtDDACuenta"));
            else
               servicio.setIntWh("0000000000");
            Diario diario = new Diario();
            sMessage = diario.invokeDiario(servicio, (String)session.getAttribute("d_ConsecLiga"), dirip, moneda, session);
        }

        if (txn.equals("5359")) {
            servicioA.setAcctNo((String)datasession.get("txtDDACuenta"));
            Monto = "";
            Monto = quitap((String)datasession.get("txtMonto"));
            servicioA.setTranAmt(Monto);
            servicioA.setMoamoun("000");
            String txtDescripcion = "CARGO PARA PAGO SERV. X COBRANZA";
            servicioA.setTranDesc(txtDescripcion);
            String DescRev = "REV." + txtDescripcion;
            if(DescRev.length() > 40)
                    DescRev = DescRev.substring(0,40);
            servicioA.setDescRev(DescRev);
            if ( (String)datasession.get("override") != null )
                 if (datasession.get("override").toString().equals("SI"))
                     servicioA.setOverride("3");
            if ( (String)datasession.get("supervisor") != null )
                 servicioA.setSupervisor((String)datasession.get("supervisor"));
            Diario diario = new Diario();
            sMessage = diario.invokeDiario(servicioA, (String)session.getAttribute("d_ConsecLiga"), dirip, moneda, session);
        }

        if (txn.equals("0580")) {
            servicio.setAcctNo(txn + (String)datasession.get("txtCodSeg") + (String)datasession.get("txtCveTran") + datasession.get("txtDDACuenta2").toString().substring(0,4));
            Monto = quitap((String)datasession.get("txtimporte"));
            servicio.setTranAmt(Monto);
            String rdoACorte = (String)datasession.get("rdoACorte");
            if (rdoACorte.equals("01"))
            {rdoACorte = "T1";}
            else
            {rdoACorte = "T2";}
            servicio.setReferenc(datasession.get("txtDDACuenta2").toString().substring(4) + (String)datasession.get("txtSerial2") + rdoACorte);
            servicio.setIntern(datasession.get("txtnumctrl").toString().substring(0,10));
            servicio.setIntWh(datasession.get("txtnumctrl").toString().substring(10,12));
            servicio.setCashIn("000");
            oVolanteoDDA = new VolanteoDDA();
            String txtBanco = (String)datasession.get("txtCveTran");
            txtBanco = txtBanco.substring(5,8);
            oVolanteoDDA.setTeller(datasession.get("teller").toString().substring(4,6));
            oVolanteoDDA.setBranch(datasession.get("sucursal").toString().substring(1));
            oVolanteoDDA.setBank(txtBanco);
            oVolanteoDDA.setCurrency((String)datasession.get("moneda"));
            oVolanteoDDA.setCorte((String)datasession.get("rdoACorte"));
            oVolanteoDDA.setChequeData((String)datasession.get("txtSerial2") +(String)datasession.get("txtDDACuenta2") + (String)datasession.get("txtCveTran") +
            (String)datasession.get("txtCodSeg"));
            oVolanteoDDA.setChequeAmount(Monto);
            if ( Long.parseLong(Monto) >= TruncamientoMN ) {              // Monto Mayor al de Truncamiento
                txtVolante = "SI";
                servicio.setFeeAmoun(oVolanteoDDA.getConsecutive());
            }
            Diario diario = new Diario();
            sMessage = diario.invokeDiario(servicio, (String)session.getAttribute("d_ConsecLiga"), dirip, moneda, session);

            if(sMessage.charAt(0) =='0') 
               oVolanteoDDA.updateConsecutive(getString(servicio.getMessage(),1), txtVolante);
                   
        }
        session.setAttribute("page.txnresponse", sMessage);
        getServletContext().getRequestDispatcher("/servlet/ventanilla.Redirector").forward(request,response);
        return;
   }

            

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }

    private String quitap(String Valor) {
        StringBuffer Cantidad = new StringBuffer(Valor);
        for(int i=0; i<Cantidad.length();) {
            if( Cantidad.charAt(i) == ',' || Cantidad.charAt(i) == '.' )
                Cantidad.deleteCharAt(i);
            else
                ++i;
        }
        return Cantidad.toString();
    }

    public String getmoneda(String cmoneda) {
        String newmoneda = "";
        if (cmoneda.equals("N$"))
        { newmoneda = "01";}
        else if (cmoneda.equals("US$"))
        { newmoneda = "02";}
        else
        { newmoneda = "03";}
        return newmoneda;
    }

    public String removeAsterisk(String campo) {
        StringBuffer result = new StringBuffer(campo);
        String caracter = result.substring(result.length()-1,result.length());
        if (caracter.equals("*"))
            result.deleteCharAt(result.length()-1);
        return result.toString();
    }


    private String getString(String newCadNum, int newOption) {
        int iPIndex = newCadNum.indexOf("CONSECUTIVO:") + 12, NumSaltos, Num = 0, Ban = 0;
        String nCadNum = new String("");
        NumSaltos = newOption;
        while(Num < NumSaltos) {
            if(newCadNum.charAt(iPIndex) == 0x20) {
                while(newCadNum.charAt(iPIndex) == 0x20)
                    iPIndex++;
                Num++;
            }
            else
                while(newCadNum.charAt(iPIndex) != 0x20)
                    iPIndex++;
        }
        while(newCadNum.charAt(iPIndex) != 0x20 && iPIndex < newCadNum.length()){
            nCadNum = nCadNum + newCadNum.charAt(iPIndex);
            if(iPIndex < newCadNum.length())
                iPIndex++;
            if(iPIndex == newCadNum.length())
                break;
        }

        return nCadNum;
    }
}
