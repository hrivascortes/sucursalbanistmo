//*************************************************************************************************
//             Funcion: Clase que realiza txn Vouchers
//            Elemento: Vouchers03.java
//          Creado por: Alejandro Gonzalez Castro
//      Modificado por: Juvenal R. Fernandez V.
//*************************************************************************************************
// CCN - 4360189 - 03/09/2004 - Se cambia invoke al Diario para Control de Efectivo
// CCN - 4360334 - 17/06/2005 - Se elimina variable dbQualifier
// CCN - 4360368 - 02/09/2005 - Se elimina sleep
// CCN - 4360374 - 08/09/2005 - Se genera nuevo paquete a peticion de QA.
//*************************************************************************************************
package ventanilla;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Hashtable;
import java.util.StringTokenizer;

import javax.servlet.http.HttpSession;

import ventanilla.com.bital.admin.Diario;
import ventanilla.com.bital.sfb.VouchersA;
import ventanilla.com.bital.sfb.VouchersB;

import com.bital.util.ConnectionPoolingManager;

public class Vouchers03 {
    Hashtable datasession = null;
    String txn = "";
    String cajero = "";
    String cuenta = "";
    String mto = "";
    String tipo = "";
    String auto = "";
    String fecha = "";
    String nd = "";
    String nv = "";
    String no = "";
    String moneda = "01";
    String Tmoneda = "";
    String codigo = "";
    String respuesta = "";
    String dirip = "";
    String consec = "";
    String lresp = "";
    int intTermino = 0;

    int i = 0;
    int j = 0;
    int k = 0;
    int nod = 0;
    int nov = 0 ;
    int num = 0;
    int tot = 0;
    int rows = 0;
    int lineas = 0;
    int consecutivo = 0;

    int intCiclo = 0;
    int intConsecutivo = 0;
    
    HttpSession session;

    public int getCiclo() {
        return intCiclo;
    }

    public int getConsecutivo() {
        return intConsecutivo;
    }

    public int getTermino() {
        return intTermino;
    }

    public Vouchers03(Hashtable data, HttpSession sesion) 
    {
        this.datasession = data;
        session = sesion;
        datasession.put("conteo", new Integer(0));
        datasession.put("codetxn", new String("X"));
        datasession.put("numero", new Integer(0));
        datasession.put("end", new String("X"));
    }

    private void descarga() {
        VouchersB vouchersb = new VouchersB();
        vouchersb.setFormat("B");
        vouchersb.setBranch((String)datasession.get("sucursal"));
        vouchersb.setTeller((String)datasession.get("teller"));
        vouchersb.setSupervisor((String)datasession.get("supervisor"));
        vouchersb.setTxnCode(txn);
        vouchersb.setTxn(txn);
		GenericClasses gc = new GenericClasses();
        moneda = (String)datasession.get("moneda");
		vouchersb.setFromCurr(gc.getDivisa(moneda));
		Tmoneda=gc.getDivisa(moneda);
        datasession.put("conteo", new Integer(intCiclo));
        cuenta = (String)datasession.get("tarjeta"+tipo+Integer.toString(intCiclo));
        vouchersb.setAcctNo(cuenta);
        StringBuffer monto = new StringBuffer((String)datasession.get("monto"+tipo+Integer.toString(intCiclo)));
        for(j=0; j<monto.length();) {
            if( monto.charAt(j) == ',' || monto.charAt(j) == '.' )
                monto.deleteCharAt(j);
            else
                ++j;
        }
        mto = monto.toString();
        vouchersb.setTranAmt(mto);
        auto = (String)datasession.get("auto"+tipo+Integer.toString(intCiclo));
        fecha = (String)datasession.get("fecha"+tipo+Integer.toString(intCiclo));
        vouchersb.setReferenc(auto+fecha);
        vouchersb.setCashIn("000");

        Diario diario = new Diario();
        String sMessage = diario.invokeDiario(vouchersb,(String)datasession.get("d_ConsecLiga"),dirip, moneda, session);
        respuesta = sMessage;

        codigo = respuesta.substring(0,1);
        datasession.put("codetxn", codigo);
        // llamado a grabar certificacion
        try {
            int respuesta = Integer.parseInt(codigo);
        }
        catch(NumberFormatException nfException) {
            System.out.println("Error Vouchers03::descarga [" +  nfException.toString() + "]");
            respuesta = "1~01~9999999~CONSECUTIVO:  999999 RECHAZO TXN PROCESADOR CENTRAL~";
        }
        consecutivo=intConsecutivo;
        certif(respuesta);

        intConsecutivo++;
        intCiclo++;
    }

    private void cargo() {
        VouchersA vouchersa = new VouchersA();
        vouchersa.setFormat("F");
        vouchersa.setBranch((String)datasession.get("sucursal"));
        vouchersa.setTeller((String)datasession.get("teller"));
        vouchersa.setSupervisor((String)datasession.get("supervisor"));
        vouchersa.setTxnCode(txn);
        cuenta = (String)datasession.get("cuenta");
        vouchersa.setAcctNo(cuenta);
        StringBuffer monto = new StringBuffer((String)datasession.get("mto"+tipo));
        for(j=0; j<monto.length();) {
            if( monto.charAt(j) == ',' || monto.charAt(j) == '.' )
                monto.deleteCharAt(j);
            else
                ++j;
        }
        mto = monto.toString();
        vouchersa.setTranAmt(mto);
        moneda = (String)datasession.get("moneda");
        if(moneda.equals("01")) {
            vouchersa.setTranCur("N$");
            Tmoneda = "N$";
        }
        else if(moneda.equals("02")) {
            vouchersa.setTranCur("US$");
            Tmoneda = "US$";
        }
        else {
            vouchersa.setTranCur("UDI");
            Tmoneda = "UDI";
        }
        vouchersa.setMoamoun("000");
        String cajero = (String)datasession.get("teller");
        String supervisor = (String)datasession.get("supervisor");

        if (!supervisor.equals(cajero))
           vouchersa.setOverride("3");


        if (txn.equals("4577")) {
            vouchersa.setTranDesc("CARGO DEVOLUCION VOUCHERS TDC "+(String)datasession.get("txtNoNegocio"));
            vouchersa.setDescRev("REV.CARGO DEVOLUCION VOUC     "+(String)datasession.get("txtNoNegocio"));
        }
        if (txn.equals("4581")) {
            vouchersa.setTranDesc("CARGO ACLAR VOUCHERS TDC      "+(String)datasession.get("txtNoNegocio"));
            vouchersa.setDescRev("REV.CARGO ACLAR VOUCHERS      "+(String)datasession.get("txtNoNegocio"));
        }
        if (txn.equals("4585")) {
            vouchersa.setTranDesc("CARGO EXTEMP VOUCHERS TDC     "+(String)datasession.get("txtNoNegocio"));
            vouchersa.setDescRev("REV.CARGO EXTEMP VOUCHERS     "+(String)datasession.get("txtNoNegocio"));
        }
        if (txn.equals("4575")) {
            vouchersa.setTranDesc("DEPOSITO VOUCHERS TDC         "+(String)datasession.get("txtNoNegocio"));
            vouchersa.setDescRev("REV.DEPOSITO VOUCHERS TDC     "+(String)datasession.get("txtNoNegocio"));
        }
        if (txn.equals("4579")) {
            vouchersa.setTranDesc("DEPOSITO ACLAR VOUCHERS TDC   "+(String)datasession.get("txtNoNegocio"));
            vouchersa.setDescRev("REV.DEPOSITO ACLAR VOUCHE     "+(String)datasession.get("txtNoNegocio"));
        }
        if (txn.equals("4583")) {
            vouchersa.setTranDesc("DEPOSITO EXTEMP VOUCHERS TDC  "+(String)datasession.get("txtNoNegocio"));
            vouchersa.setDescRev("REV.DEPOSITO EXTEMP VOUCH     "+(String)datasession.get("txtNoNegocio"));
        }
        if (tipo.equals("V"))
            vouchersa.setHoldays((String)datasession.get("dsbc"));

        vouchersa.setCheckNo3(no);
        StringBuffer comision = new StringBuffer((String)datasession.get("c"+tipo));
        for(j=0; j<comision.length();) {
            if( comision.charAt(j) == ',' || comision.charAt(j) == '.' )
                comision.deleteCharAt(j);
            else
                ++j;
        }
        vouchersa.setAmount4(comision.toString());

        Diario diario = new Diario();
        String sMessage = diario.invokeDiario(vouchersa, (String)datasession.get("d_ConsecLiga"), dirip, moneda, session);
        respuesta = sMessage;

        codigo = respuesta.substring(0,1);
        datasession.put("codetxn", codigo);
        // llamado a grabar certificacion
        try {
            int respuesta = Integer.parseInt(codigo);
        }
        catch(NumberFormatException nfException) {
            System.out.println("Error Vouchers03::abono [" +  nfException.toString() + "]");
            respuesta = "1~01~9999999~CONSECUTIVO:  999999 RECHAZO TXN PROCESADOR CENTRAL~";
        }
        consecutivo=intConsecutivo;
        certif(respuesta);

        intConsecutivo++;
        intCiclo++;
    }

    private void certif(String nrespuesta) {
        StringTokenizer resp = new StringTokenizer(nrespuesta, "~");
        respuesta = "";
        while (resp.hasMoreElements()) {
            respuesta = respuesta + resp.nextElement().toString();
        }
        consec = respuesta.substring(3,10);
        respuesta = respuesta.substring(10,respuesta.length());
        lineas = respuesta.length()/78;
        if(respuesta.length()%78 > 0)
            lineas++;
        Connection pooledConnection = ConnectionPoolingManager.getPooledConnection();
        Statement insert = null;
        String sql = "";
        try {
            if(pooledConnection != null) {
                j = 0;
                for(k=0; k<lineas; k++) {
                    insert = pooledConnection.createStatement();
                    lresp = respuesta.substring(j,Math.min(j+78, respuesta.length()));
					sql = "INSERT INTO TW_CERTIFICACIONES VALUES('"+cajero+"','"+codigo+cuenta+"',"+mto+","+0+",'"+txn+"','"+Tmoneda+"',"+consecutivo+","+(k+1)+",'"+lresp.trim()+"','"+consec+"','"+""+"')";
                    int code = 0;
                    code = insert.executeUpdate(sql);
                    j = j+78;
                    if(insert != null) {
                        insert.close();
                        insert = null;
                    }
                }
            }
        }
        catch(SQLException sqlException) 
        {
            System.out.println("Error Vouchers03::certif [" + sqlException + "]");
            System.out.println("Error Vouchers03::certif::SQL [" + sql + "]");
        }
        finally {
            try {
                if(insert != null) {
                    insert.close();
                    insert = null;
                }
                if(pooledConnection != null) {
                    pooledConnection.close();
                    pooledConnection = null;
                }
            }
            catch(java.sql.SQLException sqlException) 
            {
                System.out.println("Error FINALLY Vouchers03::certif [" + sqlException + "]");
                System.out.println("Error FINALLY Vouchers03::certif::SQL [" + sql + "]");
            }
        }
    }

    private void clearcertif() {
        cajero = (String)datasession.get("teller");
        String statementPS = ConnectionPoolingManager.getStatementPostFix();

        Connection pooledConnection = ConnectionPoolingManager.getPooledConnection();
        PreparedStatement delete = null;
        try {
            if(pooledConnection != null) {
                delete = pooledConnection.prepareStatement("DELETE FROM TW_CERTIFICACIONES WHERE D_CAJERO=?" + statementPS);                
                delete.setString(1,cajero);
                int i = delete.executeUpdate();
            }
        }
        catch(SQLException sqlException) 
        {
            System.out.println("Error Vouchers03::clearcertif [" + sqlException + "]");
        }
        finally {
            try {
                if(delete != null) {
                    delete.close();
                    delete = null;
                }
                if(pooledConnection != null) {
                    pooledConnection.close();
                    pooledConnection = null;
                }
            }
            catch(java.sql.SQLException sqlException) {
                System.out.println("Error FINALLY Vouchers03::clearcertif [" + sqlException + "]");
            }
        }
    }

    public void initProceso() {
        dirip =  (String)datasession.get("dirip");
        codigo="0";
        String txnorig = (String)datasession.get("cTxn");
        String strCiclo =  (String)datasession.get("ctrlCiclo");
        intCiclo = Integer.parseInt(strCiclo);
        String strConsecutivo =  (String)datasession.get("ctrlConsecutivo");
        intConsecutivo = Integer.parseInt(strConsecutivo);
        String strPrimeraVez =  (String)datasession.get("ctrl");
        int intPrimeraVez = Integer.parseInt(strPrimeraVez);
        nd = (String)datasession.get("noD");
        nod = Integer.parseInt(nd);
        nv = (String)datasession.get("noV");
        nov = Integer.parseInt(nv);
        if (nod != 0)
            tot = nod + 1;
        if (nov != 0)
            tot = tot + nov + 1;
        datasession.put("numero", new Integer(tot));
        cajero = (String)datasession.get("teller");
        // CLEAR A TABLA CERTIF
        if (intPrimeraVez == 0)
            clearcertif();
        if((nod > 0) && (intCiclo < (nod+1)) && (intConsecutivo < (nod + 1)) ) {
            txn = "0556";
            tipo = "D";
            num = nod;
            intTermino = 1;
            // de 1 - 10 abonos - txns 0556 & 0558
            descarga();
            if (intCiclo == (nod+1) && codigo.equals("0"))
            {
                // txns 4577 & 4575 - 4581 & 4579 - 4585 & 4583
                if (txnorig.equals("0066"))
                    txn = "4577";
                if (txnorig.equals("0072"))
                    txn = "4581";
                if (txnorig.equals("0073"))
                    txn = "4585";
                no = nd;

                cargo();
                intCiclo	= 1;
                if ( !codigo.equals("3") && !codigo.equals("2"))
                    intTermino = 0;
            }
        }

        if((nov > 0) && (intCiclo < (nov+1)) && (intTermino == 0)   &&(codigo.equals("0"))  )
        {
            intTermino = 1;
            txn = "0558";
            tipo = "V";
            num = nov;
            // de 1 - 10 abonos - txns 0556 & 0558

            descarga();
            if (intCiclo == (nov+1)&& codigo.equals("0"))
            {
                // txns 4577 & 4575 - 4581 & 4579 - 4585 & 4583
                if (txnorig.equals("0066"))
                    txn = "4575";
                if (txnorig.equals("0072"))
                    txn = "4579";
                if (txnorig.equals("0073"))
                    txn = "4583";
                no = nv;

                cargo();
                if ( !codigo.equals("3") && !codigo.equals("2"))
                    intTermino = 0;
            }
        }
        if (intTermino == 0)
            intTermino = 2;
    }
}
