//*************************************************************************************************
//             Funcion: Clase para digitalizacion
//            Elemento: ActualizarServlet.java
//          Creado por: Alejandro Gonzalez Castro.
//      Modificado por: Juvenal Fernandez Varela
//*************************************************************************************************
// CCN - 4360334 - 17/06/2005 - Se elimina variable dbQualifier
// CCN - 4360347 - 17/06/2005 - Se envia salida para evitar Warning en el log
//*************************************************************************************************

package ventanilla;

import java.io.IOException;
import java.io.OutputStream;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ventanilla.com.bital.admin.OperacionesDBBean;

public class ActualizarServlet extends HttpServlet
{
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		OperacionesDBBean rbQuery = new OperacionesDBBean();
		String strParametro = request.getQueryString();
		String strCajero = new String("");
		String strSucursal = new String("");
		String strAccion = new String("");
		String strTipo = new String("");
		strTipo = strParametro.substring(0,1);
		strSucursal = strParametro.substring(1,5);
		strCajero = strParametro.substring(5,7);
		if (strTipo.equals("1"))
		{
			strAccion = "UPDATE TW_VOLANTEO_DDA set D_VOLANTE_CERRADO= 'S' WHERE C_NUMERO_SUCURSAL ='" + strSucursal+ "' AND C_NUMERO_CAJERO = '" + strCajero + "'";
			rbQuery.InsertUpdateRegistro(strAccion);
		}
		else
		{
			int intSize = strParametro.length() - 6;
			intSize = intSize / 7;
			String strConsecutivo = new String("");
			for (int i= 0 ; i < intSize; i++)
			{
				strConsecutivo = strParametro.substring(i*7+7,i*7+14);
				strAccion = "UPDATE TW_CONSEC_VOL_DDA set D_VOLANTE_CERRADO= 'S' WHERE C_NUMERO_SUCURSAL ='" + strSucursal+ "' AND C_NUMERO_CAJERO = '" + strCajero + "' AND D_CONSEC_HOGAN ='" + strConsecutivo +"'";
				rbQuery.InsertUpdateRegistro(strAccion);
			}
		}
		byte[] buffer1   = new byte[2];
		buffer1[0]=0;
		buffer1[1]=1;			
		response.setContentType("application/octet-stream");			
		OutputStream os1 = response.getOutputStream();
		os1.write(buffer1, 0, 2);
		os1.close();		
	}

 	public void doPost(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException
	{
		doGet(request, response);
	}

}
