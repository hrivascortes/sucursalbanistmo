//*************************************************************************************************
//             Funcion: Clase para digitalizacion
//            Elemento: ActDigServlet.java
//          Creado por: Alejandro Gonzalez Castro.
//      Modificado por: Juvenal Fernandez Varela
//*************************************************************************************************
// CCN - 4360334 - 17/06/2005 - Se elimina variable dbQualifier
//*************************************************************************************************

package ventanilla;

import java.io.IOException;
import java.io.OutputStream;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ventanilla.com.bital.admin.OperacionesDBBean;

public class ActDigServlet extends HttpServlet {
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        OperacionesDBBean rbQuery = new OperacionesDBBean();
        String statementPS = com.bital.util.ConnectionPoolingManager.getStatementPostFix();
        String strParametro = request.getQueryString();
        String strAccion = new String("");
        String strTipo = new String("");
        byte [] bRespuesta = new byte[5];
        strTipo = strParametro.substring(0,1);
        strParametro = strParametro.substring(1,strParametro.length());
        String strCajero = strParametro.substring(0,6);
        strParametro = strParametro.substring(6,strParametro.length());
        int intOffset = Integer.parseInt(strParametro.substring(0,1));
        intOffset--;
        strParametro = strParametro.substring(1,strParametro.length());
        if (strTipo.equals("D")) {
            String strTipoDoc = strParametro.substring(0,1);
            strParametro = strParametro.substring(1,strParametro.length());
            String strDato = new String("");
            if (!strTipoDoc.equals("1")) {
                strDato = strParametro.substring(0,intOffset);
                strParametro = strParametro.substring(intOffset,strParametro.length());
                String strCheque = strParametro.substring(0,31);
                String strMonto =  strParametro.substring(31,strParametro.length());
                strAccion = "INSERT INTO TW_CONSEC_VOL_DDA VALUES ('";
                strAccion = strAccion + strCajero.substring(0,4) + "','" + strCajero.substring(4,6) + "','0000000000','";
                strAccion = strAccion + strCheque + "','0000000', '000','T2', 'S', '" + strMonto + "', '?', 'E', 'D','";
                strAccion = strAccion + strDato + "','" + strTipoDoc + "')";
                rbQuery.InsertUpdateRegistro(strAccion);
                if( rbQuery.getStatus() < 0) {
                    bRespuesta [0] = 1; bRespuesta [1] = 1; bRespuesta [2] = 1;	bRespuesta [3] = 1; bRespuesta [4] = 1;
                }
                else {
                    bRespuesta [0] = 0; bRespuesta [1] = 0; bRespuesta [2] = 0;	bRespuesta [3] = 0; bRespuesta [4] = 0;
                }
            }
            else {
                String strCheque = strParametro.substring(0,31);
                String strMonto =  strParametro.substring(31,strParametro.length());
                strAccion = "SELECT  D_DOCUM_DIGITALIZ, D_DOCUM_ENTREGADO, D_IMPORTE_CHEQUE FROM TW_CONSEC_VOL_DDA WHERE D_DATOS_CHEQUE ='" + strCheque + "'" + statementPS;
                String strRespuesta = rbQuery.EjecutaQuery(strAccion);
                if (strRespuesta.length() > 2) {
                    if (strRespuesta.substring(0,1).equals("E")) {
                        bRespuesta [0] = 4; bRespuesta [1] = 4; bRespuesta [2] = 4;	bRespuesta [3] = 4; bRespuesta [4] = 4;
                    }
                    else {
                        if (strRespuesta.substring(2,3).equals("E") || strRespuesta.substring(2,3).equals("I")) {
                        	int iRespuesta = 	Integer.parseInt(strRespuesta.substring(4,strRespuesta.length()-1));
                        	int iMonto = Integer.parseInt(strMonto);
                            if (iRespuesta == iMonto) {
                                strAccion = "UPDATE TW_CONSEC_VOL_DDA set D_DOCUM_DIGITALIZ = 'D', D_DOCUM_ENTREGADO = 'E' , D_TIPO_DOCUMENTO = '" + strTipoDoc + "' WHERE D_DATOS_CHEQUE = '" + strCheque + "'";
                                //System.out.println("1 ActDigServlet strAccion " + strAccion);
                                rbQuery.InsertUpdateRegistro(strAccion);
                                if( rbQuery.getStatus() < 0) {
                                    bRespuesta [0] = 1; bRespuesta [1] = 1; bRespuesta [2] = 1;	bRespuesta [3] = 1; bRespuesta [4] = 1;
                                }
                                else {
                                    bRespuesta [0] = 0; bRespuesta [1] = 0; bRespuesta [2] = 0;	bRespuesta [3] = 0; bRespuesta [4] = 0;
                                }
                            }
                            else {
                                strAccion = "UPDATE TW_CONSEC_VOL_DDA set D_DOCUM_ENTREGADO = 'I' WHERE D_DATOS_CHEQUE = '" + strCheque + "'";
                                //System.out.println("2 ActDigServlet strAccion " + strAccion);
                                rbQuery.InsertUpdateRegistro(strAccion);
                                if( rbQuery.getStatus() < 0) {
                                    bRespuesta [0] = 1; bRespuesta [1] = 1; bRespuesta [2] = 1;	bRespuesta [3] = 1; bRespuesta [4] = 1;
                                }
                                else {
                                    bRespuesta [0] = 2; bRespuesta [1] = 2; bRespuesta [2] = 2;	bRespuesta [3] = 2; bRespuesta [4] = 2;
                                }
                            }
                        }
                        else {
                            bRespuesta [0] = 3; bRespuesta [1] = 3; bRespuesta [2] = 3;	bRespuesta [3] = 3; bRespuesta [4] = 3;
                        }
                    }
                }
                else {
                    bRespuesta [0] = 5; bRespuesta [1] = 5; bRespuesta [2] = 5;	bRespuesta [3] = 5; bRespuesta [4] = 5;
                }
            }
        }
        response.setContentType("application/octet-stream");
        OutputStream os = response.getOutputStream();
        os.write(bRespuesta, 0, 5);
        os.close();
    }
    
    public void doPost(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException {
        doGet(request, response);
    }
}
