//*************************************************************************************************
//             Funcion: Clase que realiza txn 9815 - 9803 - 9809 - 9711
//            Elemento: Group07I.java
//          Creado por: Alejandro Gonzalez Castro
//      Modificado por: Alejandro Gonzalez Castro
//*************************************************************************************************
// CCN - 4360189 - 03/09/2004 - Se cambia invoke al Diario para Control de Efectivo
//*************************************************************************************************
package ventanilla;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import ventanilla.com.bital.admin.Diario;
import java.util.Hashtable;
import ventanilla.com.bital.sfb.Transferencia;

public class Group07I extends HttpServlet {
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/plain");
        HttpSession session = request.getSession(false);
        Hashtable datasession = (Hashtable)session.getAttribute("page.datasession");
        
        String dirip =  (String)session.getAttribute("dirip");
        String msg = "";
        Transferencia transferencia = new Transferencia();
        
        transferencia.setBranch((String)datasession.get("sucursal"));
        transferencia.setTeller((String)datasession.get("teller"));
        transferencia.setSupervisor((String)datasession.get("supervisor"));
        
        String txn = (String)datasession.get("cTxn");
        StringBuffer monto = new StringBuffer("");
        transferencia.setTxnCode(txn);
        String txtMoneda = null;
        
        if ( (String)datasession.get("override") != null )
            if (datasession.get("override").toString().equals("SI"))
                transferencia.setOverride("3");
        
        if (txn.equals("9815") || txn.equals("9809")) {
            transferencia.setAcctNo((String)datasession.get("txtCDACuenta"));
            monto = new StringBuffer((String)datasession.get("txtMonto") );
            transferencia.setTranCur("UDI");
            txtMoneda = "UDI";
            transferencia.setTranDesc("CARGO P/VENTA UDIS " + (String)datasession.get("txtDDACuenta"));
            transferencia.setDescRev("EMPTY");
        }
        if (txn.equals("9803") || txn.equals("9711")) {
            transferencia.setAcctNo((String)datasession.get("txtDDACuenta"));
            monto = new StringBuffer((String)datasession.get("txtMonto1") );
            transferencia.setTranCur("N$");
            txtMoneda = "N$";
            transferencia.setTranDesc("ABONO P/VENTA UDIS " + (String)datasession.get("txtCDACuenta"));
            transferencia.setDescRev("EMPTY");
        }
        
        for(int i=0; i<monto.length();) {
            if( monto.charAt(i) == ',' || monto.charAt(i) == '.' )
                monto.deleteCharAt(i);
            else
                ++i;
        }
        
        
        transferencia.setTranAmt(monto.toString());

        Diario diario = new Diario();
        String sMessage = diario.invokeDiario(transferencia, (String)session.getAttribute("d_ConsecLiga"), dirip, txtMoneda, session);
        
        session.setAttribute("page.txnresponse", transferencia.getMessage());
        getServletContext().getRequestDispatcher("/servlet/ventanilla.Redirector").forward(request,response);
    }
    
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}
