//*************************************************************************************************
//             Funcion: Retiros Txn 5353
//            Elemento: Group04R.java
//          Creado por: Alejandro Gonzalez Castro
//		Modificado por: Humberto Enrique Balleza Mej�a
//*************************************************************************************************
// CCN - 4360204 - 24/09/2004 - Correccion para Agencia NY
// CCN - 4360211 - 08/10/2004 - Se corrige validacion del numero de cuenta de cheque de caja en dolares
// CCN - 4360221 - 22/10/2004 - Inclusion en la impresion de la certificacion del nombre corto del 
//					   		    cliente a quien se le deposita
// CCN - 4360268 - 04/02/2005 - Se realizan adecuaciones para eliminar Agencia NY
// CCN - 4360368 - 02/09/2005 - Se elimina sleep
// CCN - 4360374 - 08/09/2005 - Se genera nuevo paquete a peticion de QA.
// CCN - 4360455 - 07/04/2005 - Se realizan modificaciones para el esquema de cheques devueltos.
// CCN - 4360462 - 10/04/2005 - Se genera nuevo paquete por errores en paquete anterior
// CCN - 4360508 - 08/09/2006 - Se obtiene monto del cheque para la ficha de deposito
// CCN - 4360574 - 09/03/2007 - Se realiza modificaci�n para saber si el recargo de RAP Calculadora se hizo con cheque
// CCN - 4360585 - 23/03/2007 - Se realizan modificaciones para RAP Calculadora
// CCN -         - 03/01/2008 - Se realizan modificaciones para la cuenta de cheque de Gerencia de Panam�
//*************************************************************************************************
 
package ventanilla;

import java.io.IOException;
import java.util.Hashtable;
import java.util.Stack;
import java.util.Vector;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import ventanilla.com.bital.sfb.Retiros;
import ventanilla.com.bital.admin.Diario;
import ventanilla.com.bital.util.NSTokenizer;
import ventanilla.GenericClasses;

public class Group04R extends HttpServlet {

    private String getItem(String newCadNum, int newOption) {
        int iPIndex = newCadNum.indexOf("CONSECUTIVO:") + 12, NumSaltos, Num = 0, Ban = 0;
        String nCadNum = new String("");
        NumSaltos = newOption;
        while (Num < NumSaltos) {
            if (newCadNum.charAt(iPIndex) == 0x20) {
                while(newCadNum.charAt(iPIndex) == 0x20)
                    iPIndex++;
                Num++;
            }
            else
                while (newCadNum.charAt(iPIndex) != 0x20)
                    iPIndex++;
        }
        while (newCadNum.charAt(iPIndex) != 0x20)
            nCadNum = nCadNum + newCadNum.charAt(iPIndex++);
        
        return nCadNum;
    }
    
    public void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        
        Retiros oRetiros = new Retiros();
        Diario diario = new Diario();
        String sMessage = null;
        String beneficiario = null;
        resp.setContentType("text/plain");
		GenericClasses cGenericas = new GenericClasses();
        HttpSession session = req.getSession(false);
        Hashtable datasession = (Hashtable)session.getAttribute("page.datasession");
        
        String dirip = (String)session.getAttribute("dirip");
        
        String getMsg = "";
        String txtFormat     = "A";
        String txtTxnCode    = (String)datasession.get("cTxn");
        String txtBranch     = (String)datasession.get("sucursal");
        String txtTeller     = (String)datasession.get("teller");
        String txtSupervisor = (String)datasession.get("teller");
        String txtMoneda     = (String)datasession.get("moneda");
        
        session.setAttribute("isChequeFormatoNuevo", req.getParameter("isChequeFormatoNuevo"));
        
        session.setAttribute("dvCuenta", req.getParameter("dvCuenta"));
        
        session.setAttribute("dvCheque", req.getParameter("dvCheque"));
        
        session.setAttribute("dvRuta", req.getParameter("dvRuta"));        
        if ( txtTxnCode.equals("5353") && session.getAttribute("benef5353") == null) 
        {
            // Txn 0080 para consulta de Nombre Corto
            oRetiros.setFormat("A");
            oRetiros.setBranch(txtBranch);
            oRetiros.setTeller(txtTeller);
            oRetiros.setSupervisor(txtSupervisor);
            oRetiros.setTxnCode("0080");
            oRetiros.setAcctNo((String)datasession.get("txtDDACuenta"));
            oRetiros.setTranCur(txtMoneda);
            oRetiros.setCashIn("000");
            sMessage = diario.invokeDiario(oRetiros, (String)session.getAttribute("d_ConsecLiga"), dirip, txtMoneda, session);

            if(sMessage.startsWith("0"))
            {    
                beneficiario = sMessage.substring(91,sMessage.length()-1);
                beneficiario = "A " + beneficiario;
            }
            else
                beneficiario = "";
        }
       
        oRetiros = new Retiros();
        String txtBackOut    = "0";
        String txtOverride   = "0";
        String txtCurrentDay = "0";
        
        if ((String)datasession.get("supervisor")!= null)
            txtSupervisor = (String)datasession.get("supervisor");
        
        oRetiros.setFormat(txtFormat);
        oRetiros.setBranch(txtBranch);
        oRetiros.setTeller(txtTeller);
        oRetiros.setSupervisor(txtSupervisor);
        oRetiros.setBackOut(txtBackOut);
        oRetiros.setOverride(txtOverride);
        oRetiros.setCurrentDay(txtCurrentDay);
        
        if ( (String)datasession.get("override") != null )
            if (datasession.get("override").toString().equals("SI"))
                oRetiros.setOverride("3");
        
        String txtFees="8";   //Constante
        String txtDDACuenta  = (String)datasession.get("txtDDACuenta2");
        //String txtTrNo1      = (String)datasession.get("txtCodSeg");
		String txtTrNo1      = "0000";
        String txtTrNo2      = (String)datasession.get("txtCveTran");
        String txtMonto      = cGenericas.delCommaPointFromString((String)datasession.get("txtMontoCheque"));
        String txtCheckNo    = (String)datasession.get("txtSerial2");
        String txtDescrip    = "";
        String txtEfectivo   = "000";
        String txtMontoTot   = (String)datasession.get("MontoTotal");
        long MontoCheque       = Long.parseLong(cGenericas.delCommaPointFromString((String)datasession.get("txtCheque")));
        long MontoTotal      = 0;
        String txtCasoEsp    = "5353";
        
		if (txtTxnCode.equals("5353"))
		{
			if (session.getAttribute("FichaDeposito") == null)
				session.setAttribute("FichaDeposito",txtMonto + "~" + txtCheckNo + "~");
			else
				session.setAttribute("FichaDeposito",session.getAttribute("FichaDeposito").toString() + txtMonto + "~" + txtCheckNo + "~");
		}
		
		String RapCal = "";
		if(datasession.get("RAPCalculadora")!=null){
			RapCal = (String)datasession.get("RAPCalculadora");
			if(RapCal.equals("S"))
				datasession.put("RapCCheque","S");//Se pago con cheque
			else
				datasession.put("RapCCheque","N");//No se pago con cheque
		}else
			datasession.put("RapCCheque","N");//No se pago con cheque
		
        if( txtMontoTot != null )
            MontoTotal = Long.parseLong(txtMontoTot);
        
        if (txtDDACuenta.equals("00103000023")) {
            txtFees = (String)datasession.get("txtFees");
            txtTxnCode = "4103";
            txtCasoEsp = "4103";
        }
        if (txtTrNo1.equals("0200") && txtDDACuenta.startsWith("080")) //Condicion para cheques NY anteriores
           txtFees = "0";
        
        oRetiros.setTxnCode(txtTxnCode);
        oRetiros.setFees(txtFees);
        oRetiros.setAcctNo(txtDDACuenta.substring(1,11));//No de Cuenta
        if ( txtTxnCode.equals("5353")) 
        {
            String cuenta = (String)datasession.get("txtDDACuenta2");
            cuenta = cuenta.substring(1,2);
            txtDescrip = "CHEQUE DEPOSITADO";
            
            if(beneficiario == null)
            {
                beneficiario = session.getAttribute("benef5353").toString();
            }    
            oRetiros.setBeneficiario(beneficiario);
           
            oRetiros.setTrNo2(txtTrNo1.substring(0,3));      // Codigo de seguridad
            if(!txtTrNo2.equals("1481"))
            oRetiros.setTrNo1(txtTrNo2.substring(2,5));      // Numero de plaza
            else
            	oRetiros.setTrNo1(txtTrNo2);
            //oRetiros.setTranDesc(txtDescrip);                 //Descripcion
            oRetiros.setMoAmoun("000");
            
            // Descripcion para Reverso
            String DescRev = "REV. " + txtDescrip;
            if(DescRev.length() > 40)
                DescRev = DescRev.substring(0,40);
            //oRetiros.setDescRev(DescRev);
            
        }
        
        oRetiros.setCheckNo(txtCheckNo);                 //No de cheque
        oRetiros.setTranAmt(txtMonto);                   //Monto Total de la Txn.
        oRetiros.setTranCur(txtMoneda);                  //Tipo de Moneda con que se opera la Txn.

        
        // Txn No Reversable cuando entra por 1073
        Stack flujotxn = (Stack)session.getAttribute("page.flujotxn");
        int rev = flujotxn.search("1073");
        if(rev > -1)
            oRetiros.setReversable("N");
        
        
        diario = new Diario();
        sMessage = diario.invokeDiario(oRetiros, (String)session.getAttribute("d_ConsecLiga"), dirip, txtMoneda, session);

        oRetiros.setCheckNo2(txtCheckNo);
        if ( sMessage.charAt(0) == '0' )
        {
            MontoTotal = MontoTotal + Long.parseLong(txtMonto);
            datasession.put("MontoTotal",new Long(MontoTotal).toString());
			if (txtTxnCode.equals("5353"))
			{
				if (session.getAttribute("FichaDeposito") != null)
					session.setAttribute("FichaDeposito",session.getAttribute("FichaDeposito").toString()  + "A~");
			}            
        }

        String iTxn    = (String)datasession.get("iTxn");
        String cTxn    = (String)datasession.get("cTxn");
        if( (iTxn.equals("1003") || iTxn.equals("1001")) && cTxn.equals("5353") && sMessage.charAt(0) == '1' )
        {
        	if(!iTxn.equals("1003")){
            MontoCheque = MontoCheque - Long.parseLong(txtMonto);
            }
            GenericClasses GC = new GenericClasses();
            String tmp = GC.formatMonto(new Long(MontoCheque).toString());
            datasession.put("txtCheque",tmp);
			if (txtTxnCode.equals("5353"))
			{
				if (session.getAttribute("FichaDeposito") != null)
					session.setAttribute("FichaDeposito",session.getAttribute("FichaDeposito").toString()  + "R~");
			}                        
        }

        if ( sMessage.charAt(0) == 'T' )                 // Error en Linea
            txtCasoEsp = txtCasoEsp + "~ERR~";
        else 
        {
            NSTokenizer parser = new NSTokenizer((String)sMessage, "~");
            Vector cics_resp = new Vector();
            
            while ( parser.hasMoreTokens() ) {
                String token = parser.nextToken();
                if ( token == null )
                    continue;
                cics_resp.addElement(token);
            }
            txtCasoEsp = txtCasoEsp + "~" + getItem((String)cics_resp.elementAt(3), 1) + "~" ;
        }
        datasession.put("txtCasoEsp",txtCasoEsp);
        
        if ( MontoTotal != MontoCheque ) 
        {
                session.setAttribute("benef5353", beneficiario);
            if ( sMessage.charAt(0) != '3' )
            {
                flujotxn.push("5353");
                session.setAttribute("page.flujotxn", flujotxn);
                
           
               String chqdev = (String)datasession.get("chqDev");
               if(chqdev == null)
                  chqdev = "NO";
                
                if(chqdev.equals("SI") && sMessage.charAt(0) == '1')
                   datasession.put("lstChqDev","01");
                  else
                  {
                datasession.remove("txtMontoCheque");
                datasession.remove("txtCodSeg");
                datasession.remove("txtCveTran");
                datasession.remove("txtDDACuenta2");
                datasession.remove("txtSerial2");
                datasession.remove("txtFirmaFun");
            }
        }
        }
        else 
        {
                session.removeAttribute("benef5353");
                
            String txtMontoCobro = (String)datasession.get("txtMontoCobro");
            String txtMontoRemesa = (String)datasession.get("txtMontoRemesa");
            if ( txtMontoCobro != null ) 
            {
                if ( txtMontoCobro.equals("0.00")) 
                {
                    flujotxn.pop();
                    session.setAttribute("page.flujotxn", flujotxn);
                    if ( txtMontoRemesa != null) 
                    {
                        if ( txtMontoRemesa.equals("0.00")) 
                        {
                            flujotxn.pop();
                            session.setAttribute("page.flujotxn", flujotxn);
                        }
                    }
                }
            }
            else 
            {
                if( (iTxn.equals("1003") || iTxn.equals("1001")) && cTxn.equals("5353") && MontoTotal==0 && MontoCheque==0)
                {
                    flujotxn.pop();
                    session.setAttribute("page.flujotxn", flujotxn);
                }
            }
        }
        session.setAttribute("page.txnresponse", sMessage);
        getServletContext().getRequestDispatcher("/servlet/ventanilla.Redirector").forward(req,resp);
    }
    
    public void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doPost(req, resp);
    }
}
