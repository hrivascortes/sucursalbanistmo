//*************************************************************************************************
//             Funcion: Clase que realiza Pago de RAP txn 5503, 0832
//            Elemento: Group01RAP.java
//          Creado por: Rutilo Zarco Reyes
//      Modificado por: Alejandro Gonzalez Castro
//*************************************************************************************************
// CCN - 4360160 - 30/06/2004 - Se agrega persistencia de datos en consulta de Bines
// CCN - 4360189 - 03/09/2004 - Se cambia invoke al Diario para Control de Efectivo
// CCN - 4360259 - 21/01/2005 - Se modifica la consulta de los Bines como mejora al proceso
// CCN - 4360347 - 22/07/2005 - Se eliminna los espacion en blanco de la variable de servicios
// CCN - 4360368 - 02/09/2005 - Se elimina sleep
// CCN - 4360374 - 08/09/2005 - Se genera nuevo paquete a peticion de QA.
//*************************************************************************************************
package ventanilla;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.Vector;
import java.util.Hashtable;
import ventanilla.com.bital.admin.Diario;

import ventanilla.com.bital.sfb.RAPG;
import ventanilla.com.bital.sfb.ServiciosA;
import ventanilla.com.bital.beans.BinesTDC;
import ventanilla.com.bital.beans.BinesAMEX;

public class Group01RAP extends HttpServlet {
    
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/plain");
        HttpSession session = request.getSession(false);
        Hashtable datasession = (Hashtable)session.getAttribute("page.datasession");
        String dirip = (String)session.getAttribute("dirip");
        
        Vector bines = new Vector();
		Vector binesAMEX = new Vector();
        
        RAPG rapg = new RAPG();
		GenericClasses gc = new GenericClasses();
        
        rapg.setBranch((String)datasession.get("sucursal"));
        rapg.setTeller((String)datasession.get("teller"));
        rapg.setSupervisor((String)datasession.get("supervisor"));
        
        rapg.setTxnCode("0832");
        rapg.setFormat("G");
        String moneda = (String)datasession.get("moneda");
		rapg.setToCurr(gc.getDivisa(moneda));
        
        rapg.setTranAmt("1");
        rapg.setAcctNo("1");

        if (datasession.get("snSrvBusqEnLinea1") != null && Boolean.parseBoolean((String)datasession.get("snSrvBusqEnLinea1"))) {
        	ventanilla.com.bital.beans.Flujotxn ft = ventanilla.com.bital.beans.Flujotxn.getInstance();
        	String inputTxn = (String)datasession.get("iTxn");
            String outputTxn  = (String)datasession.get("oTxn");
            if(outputTxn == null) outputTxn = inputTxn;

            session.setAttribute("page.iTxn", inputTxn);
            session.setAttribute("page.oTxn", outputTxn);

            java.util.Stack flujotxn = ft.getFlujotxn(inputTxn, outputTxn, (String)session.getAttribute("page.moneda"));
	        if(!flujotxn.isEmpty()) {
	        	String currentTxn = (String)flujotxn.pop();

	        	ventanilla.com.bital.util.PageBean pag = new ventanilla.com.bital.util.PageBean();
	            pag.setTransaction( currentTxn );
	            session.setAttribute("page.flujotxn", flujotxn);
	            session.setAttribute("page.cTxn", currentTxn);

				pag.setData((String)session.getAttribute("page.iTxn"),
				(String)session.getAttribute("page.oTxn"),
				(String)session.getAttribute("page.moneda"));
	            pag.execute();
	            if(pag.gettxnInfo() != null && pag.getmissingFields() == null){
	                    session.setAttribute("page.listcampos", pag.getfieldTxnContent());
	                    session.setAttribute("page.listcont", pag.getContentListing());
	                    session.setAttribute("page.txnlabel", pag.gettxnInfo().get("D_DESCRIPCION").toString().trim());
	                    session.setAttribute("page.txnAuthLevel", pag.gettxnInfo().get("D_NVL_AUTORIZ").toString().trim());
	                    session.setAttribute("page.txnImage", pag.gettxnInfo().get("D_TITULO_IMAGEN").toString().trim());
	                    session.setAttribute("page.txnIMGAutoriz", pag.gettxnInfo().get("D_RQ_AUTO_FIRMA").toString().trim());
	                    session.setAttribute("page.getiTxnView", pag.gettxnInfo().get("D_VISTA_ENTRADA").toString().trim());
	                    session.setAttribute("page.getoTxnView", pag.gettxnInfo().get("D_VISTA_SALIDA").toString().trim());
	                    session.setAttribute("page.SignField", pag.gettxnInfo().get("D_CMPO_RQ_FIRMA").toString().trim());
	                    session.setAttribute("page.certifField", pag.gettxnInfo().get("D_REQ_CERTIFICA").toString().trim());
	                    session.setAttribute("page.specialacct", pag.getSpecialAccounts());
	            } else {
	            	session.setAttribute("page.Builder.Error", "No esta definida la transacci&oacute;n <b>" +
	                currentTxn + "</b> en la aplicaci&oacute;n.");
					response.sendRedirect("../ventanilla/paginas/error.jsp");
	                return;
	            }
	        }
	        datasession.remove("snSrvBusqEnLinea1");
        }

        Vector servs   = new Vector();
        
        String ns = (String)datasession.get("nservicios");
        int nos = Integer.parseInt(ns);
        
        String servicios = "";
        String ser60 = "";
        String ser670 = "";
        for(int i=1; i <nos+1;i++) {
            //servs.addElement((String)datasession.get("servicio"+Integer.toString(i)));
			servs.addElement(gc.rellenaZeros((String)datasession.get("servicio"+Integer.toString(i)),5));
            //System.out.println(gc.rellenaZeros((String)datasession.get("servicio"+Integer.toString(i)),3));
            String servicio = (String)servs.elementAt(i-1);
            if(servicio.equals("00060")) {
                ser60 = "1";
                datasession.put("ser60", "1");
                session.setAttribute("page.datasession", datasession);
            }else if(servicio.equals("00670")) {
				ser670 = "1";
				datasession.put("ser670", "1");
				session.setAttribute("page.datasession", datasession);
			}
            int  spaces = 7 - servicio.length();
            for (int j=1; j < spaces+1; j++)
                servicio = servicio + " ";
            servicios = servicios + servicio;
        }
        
       
		String iTxn = (String)datasession.get("iTxn");
		
		if(iTxn.equals("RAPM"))
		{
			servicios=servicios.trim();	
		}
		
        
        rapg.setBenefic(servicios);
        
        Diario diario = new Diario();
        String sMessage = diario.invokeDiario(rapg, (String)session.getAttribute("d_ConsecLiga"), dirip, moneda, session);
		if(sMessage.charAt(0) != 'O'){
	    String cuentaSerARP = sMessage.substring(sMessage.length() - 15);
	    cuentaSerARP = cuentaSerARP.replace('~', ' ').trim();
	    if (cuentaSerARP.startsWith("01")) {
	      datasession.put("DDAARP", cuentaSerARP);
			} 
		}else {
	      datasession.put("DDAARP", "");
		}
		  
        String cuenta = (String)datasession.get("txtDDACuenta");
        String res = "";
        if (cuenta == null)
            cuenta = "";
        if(cuenta.length() > 0) 
        {

            ServiciosA serviciosA = new ServiciosA();
            serviciosA.setBranch((String)datasession.get("sucursal"));
            serviciosA.setTeller((String)datasession.get("teller"));
            serviciosA.setSupervisor((String)datasession.get("teller"));
            serviciosA.setTxnCode("0080");
            serviciosA.setAcctNo(cuenta);
            serviciosA.setTranCur("N$");
            serviciosA.setCashIn("000");
            res = diario.invokeDiario(serviciosA, (String)session.getAttribute("d_ConsecLiga"), dirip, moneda, session);
        }
        if(res.startsWith("0"))
            datasession.put("Cliente", "S");
        else
            datasession.put("Cliente", "N");
        
        if(ser60.equals("1")) {
            BinesTDC tdcbines = BinesTDC.getInstance();
            bines = tdcbines.getBines();
            session.setAttribute("bines",bines);
        }
		if(ser670.equals("1")) {
			BinesAMEX tdcbinesAMEX = BinesAMEX.getInstance();
			binesAMEX = tdcbinesAMEX.getBines();
			session.setAttribute("binesAMEX",binesAMEX);
		}
        session.setAttribute("page.txnresponse", sMessage);
        getServletContext().getRequestDispatcher("/servlet/ventanilla.Redirector").forward(request,response);
    }
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}