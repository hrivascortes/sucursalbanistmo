//*************************************************************************************************
//             Funcion: Clase que realiza txn Cheques de Viajero
//            Elemento: Group04ChqVj.java
//          Creado por: Alejandro Gonzalez Castro
//      Modificado por: Alejandro Gonzalez Castro
//*************************************************************************************************
// CCN - 4360189 - 03/09/2004 - Se cambia invoke al Diario para Control de Efectivo
//*************************************************************************************************
package ventanilla;

import java.io.IOException;
import java.util.Hashtable;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import ventanilla.com.bital.admin.Diario;
import ventanilla.com.bital.sfb.ChqVj;

public class Group04ChqVj extends HttpServlet
{
  private String delCommaPointFromString(String newCadNum)
  {
    String nCad = new String(newCadNum);
    if ( nCad.indexOf(".") > -1) {
       nCad = nCad.substring(nCad.indexOf(".") + 1, nCad.length());
       if (nCad.length() != 2) {
           String szTemp = new String("");
           for (int j = nCad.length(); j < 2; j++)
               szTemp = szTemp + "0";
           newCadNum = newCadNum + szTemp;
       }
    }

    StringBuffer nCadNum = new StringBuffer(newCadNum);
    for (int i = 0; i < nCadNum.length(); i++)
        if (nCadNum.charAt(i) == ',' || nCadNum.charAt(i) == '.')
            nCadNum.deleteCharAt(i);

    return nCadNum.toString();
  }


  public void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
  {
    // Instanciar a los bean

    ChqVj chqvj = new ChqVj();
	GenericClasses gc = new GenericClasses();

    resp.setContentType("text/plain");

    HttpSession session = req.getSession(false);
    Hashtable datasession = (Hashtable)session.getAttribute("page.datasession");

    String dirip = (String)session.getAttribute("dirip");

    String getMsg = "";

     // Se obtienen los datos del encabezado(estandar para todas las transacciones)
       String txtTxnCode    = (String)datasession.get("cTxn");
       String txtFormat     = "B";
       String txtBranch     = (String)datasession.get("sucursal");
       String txtTeller     = (String)datasession.get("teller");//"000108";
       String txtSupervisor = (String)datasession.get("teller");//"000101";
       String txtBackOut    = "0";
       String txtOverride   = "0";
       String txtCurrentDay = "0";

       if ((String)datasession.get("supervisor")!= null)
           txtSupervisor = (String)datasession.get("supervisor");

      // Se establesen por medio del BEAN los datos del encabezado
       chqvj.setFormat(txtFormat);
       chqvj.setBranch(txtBranch);
       chqvj.setTeller(txtTeller);
       chqvj.setSupervisor(txtSupervisor);
       chqvj.setBackOut(txtBackOut);
       chqvj.setOverride(txtOverride);
       chqvj.setCurrentDay(txtCurrentDay);
       // Se Obtienen los datos para la transacci"n

       String txtMonto      = delCommaPointFromString((String)datasession.get("txtMonto"));
       String txtMoneda = gc.getDivisa((String)datasession.get("moneda"));

       // Se establesen por medio del BEAN los datos para la transacci"n.
//       chqvj.settxn(txtTxnCode);
       chqvj.setTxnCode(txtTxnCode);
       chqvj.setAcctNo(txtTxnCode);
       chqvj.setTranAmt(txtMonto);
       String tipcamb = "0010000";
       //chqvj.setReferenc((String)datasession.get("txtFolSerbital") + "000" + tipcamb);
	   chqvj.setReferenc("000" + tipcamb);
       chqvj.setCashIn("000");
       //chqvj.setFeeAmoun((String)datasession.get("txtFolSerbital"));
	   chqvj.setFeeAmoun("0");
       chqvj.setFromCurr(txtMoneda);

        Diario diario = new Diario();
        String sMessage = diario.invokeDiario(chqvj, (String)session.getAttribute("d_ConsecLiga"), dirip, txtMoneda, session);

        session.setAttribute("page.txnresponse", sMessage);


        getServletContext().getRequestDispatcher("/servlet/ventanilla.Redirector").forward(req,resp);

   }// doPost

  public void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
  {
    doPost(req, resp);
  }// doGet

}// de la clase
