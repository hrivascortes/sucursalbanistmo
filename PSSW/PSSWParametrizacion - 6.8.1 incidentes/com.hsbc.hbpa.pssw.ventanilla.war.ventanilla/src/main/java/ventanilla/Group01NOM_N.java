//******************************************************************************************************************
//             Funcion: Clase que realiza txn 4227 - 4229 - 4231 - 4233
//            Elemento: Group01NOM_N.java
//          Creado por: Alejandro Gonzalez Castro
//      Modificado por: Fausto R. Flores Moreno
//******************************************************************************************************************
// CCN - 4360189 - 03/09/2004 - Se cambia invoke al Diario para Control de Efectivo
// CCN - 4360334 - 17/06/2005 - Se elimina variable dbQualifier
// CCN - 4360368 - 02/09/2005 - Se elimina sleep
// CCN - 4360374 - 08/09/2005 - Se genera nuevo paquete a peticion de QA.
// CCN - 4360456 - 07/04/2006 - Se realizan correcciones en devoluciones y se agraga override para ctas con status 3 y 4.
//******************************************************************************************************************
package ventanilla;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.Hashtable;
import java.util.Vector;
import java.util.StringTokenizer;
import java.sql.*;
import com.bital.util.ConnectionPoolingManager;
import ventanilla.com.bital.util.NSTokenizer;
import ventanilla.com.bital.admin.Diario;
import ventanilla.com.bital.sfb.Nomina;
//import ventanilla.GenericClasses;

public class Group01NOM_N extends HttpServlet 
{

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
    {
        String txn = "";
        String cajero = "";
        String dirip = "";
        int numtotnom = 0 ;
        int size = 0;
        int mtodev = 0;
        Vector Cuentas = new Vector();
        Vector Montos = new Vector();
		
        
        HttpSession session = request.getSession(false);
        Hashtable datasession = (Hashtable)session.getAttribute("page.datasession");
        datasession.put("d_ConsecLiga",(String)session.getAttribute("d_ConsecLiga"));
        
        txn = (String)datasession.get("cTxn");
        cajero = (String)datasession.get("teller");
        String firsttime = (String)datasession.get("firsttime");
        session.setAttribute("page.txnresponse", "0~02~0151515~TRANSACCION ACEPTADA~");
        numtotnom = Integer.parseInt((String)datasession.get("numtotnom"));
        String needOvrr = "NO";        
        dirip = (String)session.getAttribute("dirip");
        
        if (datasession.get("size") != null)
            size = Integer.parseInt((String)datasession.get("size"));
        if (datasession.get("mtodev") != null)
            mtodev = Integer.parseInt((String)datasession.get("mtodev"));
        if (datasession.get("Cuentas") != null)
            Cuentas = (Vector)datasession.get("Cuentas");
        if (datasession.get("Montos") != null)
            Montos = (Vector)datasession.get("Montos");
        if (datasession.get("override") != null)
            needOvrr = datasession.get("override").toString();
        
        if (firsttime.equals("si")) 
        {
            mtodev=0;
            datasession.put("firsttime","no");
            datasession.put("conteo", new Integer(0));
            datasession.put("numero", new Integer(0));
            datasession.put("codecargo", new String("X"));
            datasession.put("codeabono", new String("X"));
            datasession.put("end", new String("X"));
            if(datasession.get("mtodev") != null) {
                datasession.remove("mtodev");
            }
            
            clearcertif(cajero);
            
            getdata(datasession, Cuentas, Montos, txn);
            
            insertdata(Cuentas, Montos, cajero, datasession);
            
            String codeC = cargo(datasession, txn, Cuentas, Montos, numtotnom, dirip, session);

            
            if (!codeC.equals("0")) 
            {
                // Actualizacion tabla por RECHAZO TOTAL
                String statementPS = ConnectionPoolingManager.getStatementPostFix();
                Connection pooledConnection = ConnectionPoolingManager.getPooledConnection();
                PreparedStatement update = null;
                try {
                    if(pooledConnection != null) {
                        update = pooledConnection.prepareStatement("UPDATE TW_NOMINA SET D_STATUS ='R' WHERE D_CAJERO =?" + statementPS);
                        update.setString(1,cajero);
                        int i = update.executeUpdate();
                        if(i > 1)
                            System.out.println("Error Group01NOM_N::doPost::Update>1 [" + i + "]");
                    }
                }
                catch(SQLException sqlException)
                {
                    System.out.println("Error Group01NOM_N::doPost [" + sqlException + "]");
                }
                finally {
                    try {
                        if(update != null) {
                            update.close();
                            update = null;
                        }
                        if(pooledConnection != null) {
                            pooledConnection.close();
                            pooledConnection = null;
                        }
                    }
                    catch(java.sql.SQLException sqlException)
                    {System.out.println("Error FINALLY Group01NOM_N::doPost [" + sqlException + "]");}
                }
                datasession.put("end", "1");
                session.setAttribute("page.txnresponse", "0~02~0000000~TRANSACCION RECHAZADA~");
            }
        }
        
        String end = (String)datasession.get("end");
        if(!needOvrr.equals("SI")){
        numtotnom++;
        }
        datasession.put("numtotnom",Integer.toString(numtotnom));
        datasession.put("Cuentas", Cuentas);
        datasession.put("Montos", Montos);
        
        size = Integer.parseInt((String)datasession.get("size"));
        
        if (end.equals("X") && numtotnom <=size) 
        {
            abonos(datasession, size, numtotnom, Cuentas, Montos, dirip, session);
        }
        else
            datasession.put("end", "1");
        //********   Modificación para Subproducto de Nomina 
        if(datasession.get("mtodev")!=null){    
          mtodev = Integer.parseInt((String)datasession.get("mtodev"));
        }
        //********
        if (mtodev > 0 && numtotnom >= size) 
        {
            devolucion(datasession, Cuentas, mtodev, dirip, session);
        }
        
        session.setAttribute("page.datasession",datasession);
        session.setAttribute("page.getoTxnView","ResponseNOM_N");
        
        getServletContext().getRequestDispatcher("/servlet/ventanilla.Redirector").forward(request,response);
    }
    
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
    {
        doPost(request, response);
    }
    
    private void clearcertif(String Cajero) 
    {
        String statementPS = ConnectionPoolingManager.getStatementPostFix();
        Connection pooledConnection = ConnectionPoolingManager.getPooledConnection();
        PreparedStatement delete = null;
        try {
            if(pooledConnection != null) {
                delete = pooledConnection.prepareStatement("DELETE FROM TW_CERTIFICACIONES WHERE D_CAJERO=?" + statementPS);
                delete.setString(1,Cajero);
                int i = delete.executeUpdate();
            }
            if(pooledConnection != null) {
                delete = pooledConnection.prepareStatement("DELETE FROM TW_NOMINA WHERE D_CAJERO=?" + statementPS);
                delete.setString(1,Cajero);
                int i = delete.executeUpdate();
            }
        }
        catch(SQLException sqlException)
        {System.out.println("Error Group01NOM_N::clearcertif [" + sqlException + "]");}
        finally {
            try {
                if(delete != null) {
                    delete.close();
                    delete = null;
                }
                if(pooledConnection != null) {
                    pooledConnection.close();
                    pooledConnection = null;
                }
            }
            catch(java.sql.SQLException sqlException)
            { System.out.println("Error FINALLY Group01NOM_N::clearcertif [" + sqlException + "]");}
        }
    }
    
    private void getdata(Hashtable datos, Vector ctas, Vector mtos, String Txn) 
    {
        String cuentas = "";
        String montos = "";
        int size = 0;
        if (Txn.equals("M006") || Txn.equals("M007")) 
        {
            int m = 0;
            for(m = 1; m <6; m++) 
            {
                cuentas = cuentas + (String)datos.get("Scuentas"+m);
                montos = montos + (String)datos.get("Smontos"+m);
            }
        }
        else 
        {
            cuentas = (String)datos.get("Scuentas");
            montos = (String)datos.get("Smontos");
        }
        try 
        {
            NSTokenizer ctastmp = new NSTokenizer(cuentas, "*");
            while(ctastmp.hasMoreTokens()) 
            {
                String token = ctastmp.nextToken();
                if(token == null)
                    continue;
                ctas.addElement(token);
            }
            datos.put("ctas",ctas);
            size = ctas.size() - 1;
            datos.put("size",Integer.toString(size));
            NSTokenizer mtostmp = new NSTokenizer(montos, "*");
            while(mtostmp.hasMoreTokens()) 
            {
                String token = mtostmp.nextToken();
                if(token == null)
                    continue;
                mtos.addElement(token);
            }
            datos.put("mtos",mtos);
        }
        catch(Exception e){}
    }
    
    private void insertdata(Vector ctas, Vector mtos, String teller, Hashtable data) 
    {
        Connection pooledConnection = ConnectionPoolingManager.getPooledConnection();
        Statement insert = null;
        String coma = "'";
        int rows = 0;
        int size = ctas.size();
        try 
        {
            if(pooledConnection != null) 
            {
                for(int z=0; z<size; z++) 
                {
                    insert = pooledConnection.createStatement();
                    String sql = "INSERT INTO TW_NOMINA VALUES(" +
                    coma + teller + coma + ',' + coma + ctas.elementAt(z) + coma +
                    ',' + coma + mtos.elementAt(z) + coma + ',' + "'P'," + coma +
                    z + coma + ")";
                    int code = 0;
                    code = insert.executeUpdate(sql);
                    if(insert != null) {
                        insert.close();
                        insert = null;
                    }
                    rows = rows +1;
                }
                data.put("rows", Integer.toString(rows));
            }
        }
        catch(SQLException sqlException)
        { System.out.println("Error Group01NOM_N::insertdata [" + sqlException + "]");}
        finally {
            try {
                if(insert != null) {
                    insert.close();
                    insert = null;
                }
                if(pooledConnection != null) {
                    pooledConnection.close();
                    pooledConnection = null;
                }
            }
            catch(java.sql.SQLException sqlException)
            {System.out.println("Error FINALLY Group01NOM_N::insertdata [" + sqlException + "]");}
        }
    }
    
    private String cargo(Hashtable info, String Txn, Vector ctas, Vector mtos, int ntotnom, String ip, HttpSession sesion) 
    {
        Nomina nomina = new Nomina();
        nomina.setBranch((String)info.get("sucursal"));
        nomina.setTeller((String)info.get("teller"));
        nomina.setSupervisor((String)info.get("supervisor"));
        String noabonos = "";
        int size = ctas.size()-1;
        
        String Teller = (String)info.get("teller");
        
        if (Txn.equals("4227") || Txn.equals("M006")) 
        {
            if(Txn.equals("M006"))
                Txn = "4227";
            nomina.setTxnCode("4227");
            nomina.setAcctNo((String)ctas.elementAt(0));
            nomina.setTranAmt((String)mtos.elementAt(0));
            nomina.setMoamoun("000");
            noabonos = Integer.toString(size);
            if(noabonos.length() == 1)
                noabonos = "00" + noabonos;
            else if(noabonos.length() == 2)
                noabonos = "0" + noabonos;
            nomina.setTranDesc("CARGO POR PAGO DE NOMINA        EMP." + noabonos);
            nomina.setDescRev("REV.CARGO POR PAGO DE NOMINA    EMP." + noabonos);
            
        }
        if (Txn.equals("4229") || Txn.equals("M007")) 
        {
            if(Txn.equals("M007"))
                Txn = "4229";
            nomina.setTxnCode("4229");
            nomina.setAcctNo((String)ctas.elementAt(0));
            nomina.setTranAmt((String)mtos.elementAt(0));
            String cheque = (String)info.get("serial");
            int ceros = 10 - cheque.length();
            int j = 0;
            for(j=0; j<ceros; j++) 
            {
                cheque = '0'+ cheque;
            }
            nomina.setCheckNo(cheque);
            nomina.setMoamoun("000");
            nomina.setFees("8");
            noabonos = Integer.toString(size);
            if(noabonos.length() == 1)
                noabonos = "00" + noabonos;
            else if(noabonos.length() == 2)
                noabonos = "0" + noabonos;
            nomina.setTranDesc("CARGO POR PAGO DE NOMINA CHEQUE EMP." + noabonos);
            nomina.setDescRev("REV.CARGO POR PAGO DE NOMINA CHQ EMP" + noabonos);
            String ct = (String)info.get("cvetran");
            String plaza = ct.substring(2,5);
            nomina.setTrNo1(plaza);
            //String cseg = (String)info.get("codseg");
            String cseg ="";
            //cseg = cseg.substring(0,cseg.length()-1);
            nomina.setTrNo2(cseg);
        }
        String cuenta = (String)ctas.elementAt(0);
        String monto = (String)mtos.elementAt(0);
        String moneda = (String)info.get("moneda");
		
		GenericClasses gc = new GenericClasses();
		moneda=gc.getDivisa(moneda);
		nomina.setTranCur(moneda);
        
       
        Diario diario = new Diario();
        String sMessage = diario.invokeDiario(nomina, (String)info.get("d_ConsecLiga"), ip, moneda, sesion);

        String respuesta = sMessage;
                
        String codigoC = respuesta.substring(0,1);
        info.put("codecargo", codigoC);
        
        String status = "";
        if (codigoC.equals("0")) 
        {
            status = "E";
            updatenomina(status, ctas, ntotnom, Teller);
            respuesta = nomina.getMessage();
            String contrato = respuesta.substring(169,177);
            info.put("contrato",contrato);
            String nombre = respuesta.substring(91,131);
            info.put("nombre",nombre);
        }
        else 
        {
            status = "R";
            updatenomina(status, ctas, ntotnom, Teller);
        }
        try 
        {
            int codigo = Integer.parseInt(codigoC);
        }
        catch(NumberFormatException nfException) 
        {
            respuesta = new String("1~01~0010101~TXN RECHAZADA POR HOST~");
        }
        // llamado a grabar certificacion
        certif(respuesta, Teller, cuenta, monto, Txn, moneda);
        return codigoC;
    }
    
    private void updatenomina(String Status, Vector cuentas, int nototnom, String Cajero) 
    {
        String statementPS = ConnectionPoolingManager.getStatementPostFix();
        Connection pooledConnection = ConnectionPoolingManager.getPooledConnection();
        PreparedStatement update = null;
        try {
            if(pooledConnection != null) 
            {
                update = pooledConnection.prepareStatement("UPDATE TW_NOMINA SET D_STATUS ='" + Status +
                "' WHERE D_CUENTA='"+ (String)cuentas.elementAt(nototnom) + "' AND D_CAJERO =? AND D_CONSEC ='" + nototnom +"'"+ statementPS);
                update.setString(1,Cajero);
                int i = update.executeUpdate();
                if(i > 1)
                    System.out.println("Error Group01NOM_N::updatenomina::Update>1 [" + i + "]");

            }
        }
        catch(SQLException sqlException)
        {System.out.println("Error Group01NOM_N::updatenomina [" + sqlException + "]");}
        finally {
            try {
                if(update != null) {
                    update.close();
                    update = null;
                }
                if(pooledConnection != null) {
                    pooledConnection.close();
                    pooledConnection = null;
                }
            }
            catch(java.sql.SQLException sqlException)
            {System.out.println("Error FINALLY Group01NOM_N::updatenomina [" + sqlException + "]");}
        }
    }
    
    private void certif(String nrespuesta, String tell, String cta, String mto, String Txn, String moneda) 
    {
        StringTokenizer resp = new StringTokenizer(nrespuesta, "~");
        String respuesta = "";
        while (resp.hasMoreElements())
            respuesta = respuesta + resp.nextElement().toString();
        int lineas = respuesta.length()/78;
        if(respuesta.length()%78 > 0)
            lineas++;
        
        String consec = respuesta.substring(3,10);
        respuesta = respuesta.substring(10,respuesta.length());
        Connection pooledConnection = ConnectionPoolingManager.getPooledConnection();
        Statement insert = null;
        try 
        {
            if(pooledConnection != null) 
            {
                int j = 0;
                for(int k=0; k<lineas; k++) 
                {
                    insert = pooledConnection.createStatement();
                    String lresp = respuesta.substring(j,Math.min(j+78, respuesta.length()));
                    String sql = "INSERT INTO TW_CERTIFICACIONES VALUES('"+tell+"','"+cta+
                    "',"+mto+","+0+",'"+Txn+"','"+moneda+"',"+0+","+(k+1)+",'"+lresp.trim()+"','"+consec+"','"+""+"')";
                    int code = 0;
                    code = insert.executeUpdate(sql);
                    j = j+78;
                    if(insert != null) {
                        insert.close();
                        insert = null;
                    }
                }
            }
        }
        catch(SQLException sqlException)
        {System.out.println("Error Group01NOM_N::certif [" + sqlException + "]");}
        finally {
            try {
                if(insert != null) {
                    insert.close();
                    insert = null;
                }
                if(pooledConnection != null) {
                    pooledConnection.close();
                    pooledConnection = null;
                }
            }
            catch(java.sql.SQLException sqlException)
            {System.out.println("Error FINALLY Group01NOM_N::certif [" + sqlException + "]");}
        }
    }
    
    private void abonos(Hashtable data, int Size, int Numtotnom, Vector cts, Vector mts, String ip, HttpSession sesion) 
    {
        Nomina nomina = new Nomina();
        nomina.setBranch((String)data.get("sucursal"));
        nomina.setTeller((String)data.get("teller"));
        nomina.setSupervisor((String)data.get("supervisor"));
        
        String teller = (String)data.get("teller");
//*******        
        if ( (String)data.get("override") != null )
          if (data.get("override").toString().equals("SI"))
                nomina.setOverride("3");
        
        if ( (String)data.get("supervisor") != null ){
             nomina.setSupervisor((String)data.get("supervisor"));
        }
//*******        
        // TXNS de abono
        data.put("numero", new Integer(Size));
        data.put("conteo", new Integer(Numtotnom));
        
        
        nomina.setTxnCode("4231");
        nomina.setAcctNo((String)cts.elementAt(Numtotnom));
        nomina.setTranAmt((String)mts.elementAt(Numtotnom));
        
        String moneda = (String)data.get("moneda");
		GenericClasses gc = new GenericClasses();
		moneda=gc.getDivisa(moneda);
		nomina.setTranCur(moneda);

        nomina.setCashIn("000");
        nomina.setMoamoun("000");
        nomina.setTranDesc("ABONO POR PAGO DE NOMINA  CONT." + (String)data.get("contrato"));
        nomina.setDescRev("REV.ABONO PAGO DE NOMINA  CONT." + (String)data.get("contrato"));
        String txn = "4231";
        String cuenta = (String)cts.elementAt(Numtotnom);
        String monto = (String)mts.elementAt(Numtotnom);
        
		try {
		Thread.sleep(1000);
		} catch (InterruptedException e) {
		}
		/*GenericClasses GC = new GenericClasses();
		String hora  = GC.getDate(7);
		System.out.println("hora: " + hora);*/
		
		Diario diario = new Diario();
        
        String sMessage = diario.invokeDiario(nomina, (String)data.get("d_ConsecLiga"), ip, moneda, sesion);
        
        String respuesta=sMessage;
        
        String codigoA = respuesta.substring(0,1);
        data.put("codeabono", codigoA);
        
        String status = "";
        int mtodev = 0;
        if (data.get("mtodev") != null)
            mtodev = Integer.parseInt((String)data.get("mtodev"));

        if (codigoA.equals("0")) 
        {
            
            status = "E";
            // Actualizacion tabla para ABONO EXITOSO
            updatenomina(status, cts, Numtotnom, teller);
        }
        else 
        {
       	//poner condición para solo actualizar cuando el codigo sea diferente de 4 y 3 aun cunado sea rechaza la txn

            if(!codigoA.equals("3")){	            
            status = "R";
            // Actualizacion tabla para ABONO RECHAZADO
            updatenomina(status, cts, Numtotnom, teller);
            int mto = Integer.parseInt((String)mts.elementAt(Numtotnom));
            mtodev = mtodev + mto;
            data.put("mtodev",Integer.toString(mtodev));
            try 
            {
                int codigo = Integer.parseInt(codigoA);
            }
            catch(NumberFormatException nfException) 
            {
                respuesta = new String("1~01~0010101~TXN RECHAZADA POR HOST~");
            }
            // llamado a grabar certificacion
            certif(respuesta, teller, cuenta, monto, txn, moneda);
        }
            else
            {
				String cad = monto;
            	String cadFormat="", infoTemp="";
            	int i=0,g=0;
				infoTemp = cad.substring(cad.length()-2,cad.length());
				cad = cad.substring(0,cad.length()-2);
		        for(i=cad.length()-1;i>-1;i--){
		        	if((g%3)==0 && g>0)
			        	cadFormat = cad.charAt(i)+","+cadFormat;
			        else
			            cadFormat = cad.charAt(i) + cadFormat;
			        g++;
			    }
  		        cadFormat = cadFormat + "." +infoTemp;

			    data.put("txtMontoC",cadFormat);
   				data.put("txtDDAA",cuenta);
            }
         }
    }
    
    private void devolucion(Hashtable data, Vector ctas, int mtodevol, String dir, HttpSession sesion) 
    {
        data.put("end", "1");
        Nomina nomina = new Nomina();
        nomina.setBranch((String)data.get("sucursal"));
        nomina.setTeller((String)data.get("teller"));
        nomina.setSupervisor((String)data.get("supervisor"));
        // TXN de devolucion
        nomina.setTxnCode("4233");
        nomina.setAcctNo((String)ctas.elementAt(0));
        nomina.setTranAmt(Integer.toString(mtodevol));
        String Cajero = (String)data.get("teller");
        String moneda = (String)data.get("moneda");
		GenericClasses gc = new GenericClasses();
		moneda=gc.getDivisa(moneda);
		nomina.setTranCur(moneda);
        nomina.setCashIn("000");
        nomina.setMoamoun("000");
        nomina.setTranDesc("ABONO POR DEVOLUCION DE NOMINA");
        nomina.setDescRev("REV.ABONO POR DEVOLUCION DE NOMINA");
        String txn = "4233";
        String cuenta = (String)ctas.elementAt(0);
        String monto = Integer.toString(mtodevol);
        
        
        Diario diario = new Diario();
        String sMessage = diario.invokeDiario(nomina, (String)data.get("d_ConsecLiga"), dir, moneda, sesion);
        
        String respuesta=sMessage;
        
        // llamado a grabar certificacion
         certif(respuesta, Cajero, cuenta, monto, txn, moneda);
    }
}
