//*************************************************************************************************
//             Funcion: Servlet que redirecciona al control de efectivoS
//            Elemento: Desbloqueo.java
//          Creado por: Fausto Rodrigo Flores Morneo
//*************************************************************************************************
// CCN - 4360589 - 16/04/2007 - Se realizan modificaciones para el monitoreo de efectivo en las sucursales
//*************************************************************************************************

package ventanilla;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import ventanilla.com.bital.util.MonitorEfectivo;
import java.util.*;

public class Desbloqueo extends HttpServlet {


    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
    	MonitorEfectivo monitor = new MonitorEfectivo();
    	String vecInfo = request.getParameter("usersBloq");
    	GenericClasses gc = new GenericClasses();
    	String userPIF = "";
	    HttpSession session = request.getSession(false);

	    if(session.getAttribute("userPIF")!=null)
   		   userPIF = (String)session.getAttribute("userPIF");
	    else
		   userPIF = (String)request.getParameter("userC");
   		
    	StringTokenizer strInfo = new StringTokenizer(vecInfo,"|");
    	String cajero = strInfo.nextToken();
    	String fecha = strInfo.nextToken().toString().replace('-','\'');
    	String hora = gc.quitap(strInfo.nextToken().toString().replace(':','\''));
    	String moneda = strInfo.nextToken();
    	String status = strInfo.nextToken();
    	while(strInfo.hasMoreTokens())
	    	strInfo.nextToken();
    	String racfpif = userPIF.trim();
   		String fecha_desb = gc.getDate(1);
   		String hora_desb = gc.getDate(7);
    	
   		monitor.setDesbloquear(cajero,fecha,hora,racfpif,fecha_desb,hora_desb);
    	response.sendRedirect("../ventanilla/paginas/ControlEfectivo.jsp?userC="+racfpif.trim());
    	
    }

    public void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doPost(req, resp);
    }
}
