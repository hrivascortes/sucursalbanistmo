//*************************************************************************************************
//             Funcion: Clase que realiza txn Suas
//            Elemento: Group01Suas.java
//          Creado por: Alejandro Gonzalez Castro
//      Modificado por: Juvenal R Fernandez Varela
//*************************************************************************************************
// CCN - 4360211 - 08/10/2004 - Se elimina txn N181, y se cambia N281 por N081
// CCN - 4360325 - 03/06/2005 - Se crean y cierran correctamente las conexiones a DB2
// CCN - 4360334 - 17/06/2005 - Se elimina variable dbQualifier
//*************************************************************************************************
package ventanilla; 

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.util.Hashtable;
import java.util.Stack;
import java.sql.*;
import com.bital.util.ConnectionPoolingManager;

public class Group01Nafin extends HttpServlet 
{
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
    {
        response.setContentType("text/plain");
        HttpSession session = request.getSession(false);
        Hashtable datasession = (Hashtable)session.getAttribute("page.datasession");
        
        String txn = (String)datasession.get("cTxn");
        String itxn = (String)datasession.get("iTxn");
        String otxn = (String)datasession.get("oTxn");
        
        String billete = "";
        String monto = "";
        String clave = "";
        String depositante = "";
        String adisposicion = "";
        String concepto = "";
        String autoridad = "";
        String nomplaza = "";
        String nomsuc = "";
        String nafin = "";
        String fecha = "";
        String sql = "";
        String mensaje = "1~1~0000000~TRANSACCION RECHAZADA~";
        String moneda = "";
        String sucursal= "";
        String status = "";
        String txtPrint = "";
        int tipo = 0;
        int consec = 0;
        
        boolean existe = false;
        
        
        sucursal = (String)datasession.get("sucursal");
        sucursal = sucursal.substring(1);
        
        if (txn.equals("N081") && itxn.equals("N083")) {
            billete = (String)datasession.get("txtBillete");
            String tmp = (String)datasession.get("txtConsec");
            consec = Integer.parseInt(tmp);
			Connection pooledConnection = ConnectionPoolingManager.getPooledConnection();
            Statement update = null;
            try {
                if(pooledConnection != null) {
                    update = pooledConnection.createStatement();
                    sql = "UPDATE " +
                    "TW_NAFINSA  SET D_STATUS = 'C', D_MONTO = '000' WHERE C_BILLETE = '"+
                    billete+"' AND C_NUM_SUCURSAL ='"+sucursal+"' AND N_CONSEC="+consec;
                    
                    int code = update.executeUpdate(sql);
                    if( code == 1) 
                    {
                        mensaje = "0~1~0000000~TRANSACCION ACEPTADA, BILLETE CANCELADO~";
                        session.setAttribute("txtCadImpresion", "NOIMPRIMIRNOIMPRIMIRNOIMPRIMIR");
                    }
                    else
                        System.out.println("Error Group01Nafin::doPost::Update>1 [" + code + "]");

                }
            }
            catch(SQLException sqlException)
            {
                System.out.println("Error Group01Nafin::doPost::Update [" + sqlException + "]");
                System.out.println("Error Group01Nafin::doPost::Update::SQL [" + sql + "]");
            }
            finally {
                try {
                    if(update != null) {
                        update.close();
                        update = null;
                    }
                    if(pooledConnection != null) {
                        pooledConnection.close();
                        pooledConnection = null;
                    }
                }
                catch(java.sql.SQLException sqlException)
                {
                    System.out.println("Error FINALLY Group01Nafin::doPost::Update [" + sqlException + "]");
                    System.out.println("Error FINALLY Group01Nafin::doPost::Update::SQL [" + sql + "]");
                }
            }
            
        }
        else {
            if(txn.equals("N081")) {
                billete = (String)datasession.get("txtBillete");
                txtPrint = "NAFINSA~"+billete+"~";
                StringBuffer montotmp = new StringBuffer((String)datasession.get("txtMonto") );
                for(int i=0; i<montotmp.length();) {
                    if( montotmp.charAt(i) == ',' || montotmp.charAt(i) == '.' )
                        montotmp.deleteCharAt(i);
                    else
                        ++i;
                }
                monto = montotmp.toString();
                clave = (String)datasession.get("txtClave");
                nomplaza = (String)datasession.get("nomplaza");
                fecha = (String)datasession.get("fecha");
                moneda = (String)datasession.get("moneda");
                depositante = (String)datasession.get("txtDepositante2");
                concepto = (String)datasession.get("txtConcepto");
                autoridad = (String)datasession.get("txtAutoridad");
                tipo = 2;
            }
            
            if(txn.equals("N083")) {
                billete = (String)datasession.get("txtBillete");
            }
            
			Connection pooledConnection = ConnectionPoolingManager.getPooledConnection();
			ResultSet Rselect = null;
            Statement select = null;
            try {
                if(pooledConnection != null) {
                    select = pooledConnection.createStatement();
                    sql = "SELECT * FROM TW_NAFINSA WHERE C_BILLETE ='" + billete + "' AND C_NUM_SUCURSAL ='"+sucursal+"' ORDER BY N_CONSEC ASC";
                    Rselect = select.executeQuery(sql);
                    
                    mensaje = "1~1~0000000~EL NO. DE BILLETE NO EXISTE~";
                    while(Rselect.next()) {
                        existe = true;
                        mensaje = "1~1~0000000~EL NO. DE BILLETE YA EXISTE EN LA BASE DE DATOS, VERIFIQUE~";
                        status = Rselect.getString("D_STATUS");
                        if(status.equals("C")) {
                            existe = false;
                            mensaje = "1~1~0000000~EL BILLETE YA FUE CANCELADO~";
                            consec = Rselect.getInt("N_CONSEC");
                        }
                        if(txn.equals("N083")) {
                            tipo = Rselect.getInt("N_TIPO");
                            consec = Rselect.getInt("N_CONSEC");
                            billete = Rselect.getString("C_BILLETE");
                            monto = Rselect.getString("D_MONTO");
                            depositante = Rselect.getString("D_DEPOSITANTE");
                            adisposicion = Rselect.getString("D_DISPONIBLE");
                            autoridad = Rselect.getString("D_AUTORIDAD");
                            concepto = Rselect.getString("D_CONCEPTO");
                            clave = Rselect.getString("C_CLAVE");
                        }
                    }
                }
            }
            catch(SQLException sqlException)
            {
                System.out.println("Error Group01Nafin::doPost::Select [" + sqlException + "]");
                System.out.println("Error Group01Nafin::doPost::Select::SQL [" + sql + "]");
            }
            finally {
                try {
                    if(Rselect != null) {
                        Rselect.close();
                        Rselect = null;
                    }
                    if(select != null) {
                        select.close();
                        select = null;
                    }
                    if(pooledConnection != null) {
                        pooledConnection.close();
                        pooledConnection = null;
                    }
                }
                catch(java.sql.SQLException sqlException)
                {
                    System.out.println("Error FINALLY Group01Nafin::doPost::Select [" + sqlException + "]");
                    System.out.println("Error FINALLY Group01Nafin::doPost::Select::SQL [" + sql + "]");
                }
            }
            
            if(existe == false && txn.equals("N081")) {
                pooledConnection = ConnectionPoolingManager.getPooledConnection();
                Statement insert = null;
                try {
                    if(pooledConnection != null) {
                        insert = pooledConnection.createStatement();
                        consec ++;
                        sql = "INSERT INTO " +
                        "TW_NAFINSA (C_BILLETE,C_NUM_SUCURSAL,D_STATUS,N_CONSEC,N_TIPO,C_MONEDA,C_CLAVE,D_MONTO,D_NOM_PLAZA,D_NOM_SUCURSAL,D_NAFIN,D_DEPOSITANTE,D_DISPONIBLE,D_CONCEPTO,D_AUTORIDAD) VALUES ('"+billete+"','"+sucursal+"', 'E', "+consec+","
                        + tipo + ", '"+moneda+"', '"+clave+"', '"+monto+"', '"+nomplaza.trim()
                        +"', '"+nomsuc.trim()+"', '"+nafin+
                        "', '"+depositante+"', '"+adisposicion+"', '"+concepto+
                        "', '"+autoridad+"' )";
                        
                        int code = insert.executeUpdate(sql);
                        if( code == 1) {
                            mensaje = "0~1~0000000~TRANSACCION ACEPTADA~";
                            session.setAttribute("txtCadImpresion", txtPrint);
                            
                        }
                    }
                }
                catch(SQLException sqlException)
                {
                    System.out.println("Error Group01Nafin::doPost::Insert [" + sqlException + "]");
                    System.out.println("Error Group01Nafin::doPost::Insert::SQL [" + sql + "]");
                }
                finally {  
                    try {
                        if(insert != null) {
                            insert.close();
                            insert = null;
                        }
                        if(pooledConnection != null) {
                            pooledConnection.close();
                            pooledConnection = null;
                        }
                    }
                    catch(java.sql.SQLException sqlException)
                    {
                        System.out.println("Error FINALLY Group01Nafin::doPost::Insert [" + sqlException + "]");
                        System.out.println("Error FINALLY Group01Nafin::doPost::Insert::SQL [" + sql + "]");
                    }
                }
            }
            
            if(existe == true && txn.equals("N083")) {
                Stack flujotxn = (java.util.Stack)session.getAttribute("page.flujotxn");
                if(tipo == 2)
                    flujotxn.push("N081");
                session.setAttribute("page.flujotxn",flujotxn);
                datasession.put("txtBillete", billete);
                datasession.put("txtClave", clave);
                datasession.put("txtDepositante", depositante);
                datasession.put("txtDepositante2", depositante);
                datasession.put("txtADisposicion", adisposicion);
                datasession.put("txtConcepto", concepto);
                datasession.put("txtAutoridad", autoridad);
                datasession.put("txtMonto", monto);
                datasession.put("txtConsec", String.valueOf(consec));
                
                session.setAttribute("page.datasession", datasession);
                
                mensaje = "0~1~0000000~TRANSACCION ACEPTADA~";
            }
        }
        session.setAttribute("page.txnresponse", mensaje);
        getServletContext().getRequestDispatcher("/servlet/ventanilla.Redirector").forward(request,response);
    }
    
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}
