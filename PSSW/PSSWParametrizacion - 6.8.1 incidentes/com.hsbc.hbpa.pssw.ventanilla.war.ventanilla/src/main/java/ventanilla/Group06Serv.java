//*************************************************************************************************
//             Funcion: Clase que realiza txn Servicios
//            Elemento: Group06Serv.java
//          Creado por: Alejandro Gonzalez Castro
//      Modificado por: Alejandro Gonzalez Castro
//      Modificado por: Fredy Pe�a Moreno
//*************************************************************************************************
// CCN - 4360189 - 03/09/2004 - Se cambia invoke al Diario para Control de Efectivo
// CCN - 4360312 - 26/04/2005 - Se elimina referencia a opcion 2 de ATM
// CCN - 4360347 - 22/07/2005 - Modificar flujo de cobro de Comision e IVA para las txn 
//                              0732-0352 / 0732-0360 / 0732-0370
// CCN - 4620008 - 14/09/2007 - Se modifica flujo para el evitar el cobro de iva en txn 0732-0370
//*************************************************************************************************
package ventanilla;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.util.Hashtable;
import java.util.Stack;

import ventanilla.com.bital.sfb.ServiciosB;
import ventanilla.com.bital.admin.Diario;

public class Group06Serv extends HttpServlet 
{
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
    {
        HttpSession session = request.getSession(false);
        Hashtable datasession = (Hashtable)session.getAttribute("page.datasession");
        String txn = (String)datasession.get("cTxn");
		GenericClasses gc = new GenericClasses();
        String moneda = gc.getDivisa((String)datasession.get("moneda"));

        
        long efectivo = Long.parseLong(quitap((String)datasession.get("txtEfectivo")));
        long monto;
        if (datasession.get("txtMonto") != null) 
        {
            monto = Long.parseLong(quitap((String)datasession.get("txtMonto")));
        }
        else 
        {
            monto = Long.parseLong(quitap((String)datasession.get("txtMontoNoCero")));
        }
        if ((txn.equals("0456") || txn.equals("0458") || txn.equals("0468") || txn.equals("0540")) && ((efectivo == 0 || efectivo < monto))) 
        {
            if (txn.equals("0540")) 
            {
                long docs = monto - efectivo;
                datasession.put("txtCheque", addpunto(Long.toString(docs)));
                session.setAttribute("ftime","2");
                Stack flujotxn = (Stack)session.getAttribute("page.flujotxn");
                String[] newflujo = {"S" + txn.substring(1),"5353"};
                for (int i=0; i<newflujo.length; i++)
                { flujotxn.push(newflujo[i]);}
                session.setAttribute("page.flujotxn",flujotxn);
                getServletContext().getRequestDispatcher("/servlet/ventanilla.PageServlet?transaction=" + datasession.get("iTxn") + "&transaction1=" + datasession.get("oTxn")).forward(request,response);
            }
            else {
                session.setAttribute("ftime", "2");
                session.setAttribute("page.txnresponse","0~01~2~Desplegando datos para Descarga... ");
                getServletContext().getRequestDispatcher("/servlet/ventanilla.Redirector").forward(request,response);
            }
        }
        else 
        {
            String dirip =  (String)session.getAttribute("dirip");
            String msg = "";
            ServiciosB serviciob = new ServiciosB();
            serviciob.setBranch((String)datasession.get("sucursal"));
            serviciob.setTeller((String)datasession.get("teller"));
            serviciob.setSupervisor((String)datasession.get("teller"));
            serviciob.setFormat("B");
            String sucursal = (String)datasession.get("sucursal");
            String Monto = "";
            String ftime = (String)session.getAttribute("ftime");
            if (ftime.equals("1") && (txn.equals("0352") || txn.equals("0360"))) 
            {
                session.setAttribute("ftime", "2");
                String plaza = (String)session.getAttribute("plaza");
                txn = "0810";
                serviciob.setTxnCode(txn);
                serviciob.setAcctNo(plaza);
                serviciob.setTranAmt("000");
                serviciob.setCashIn("000");
                serviciob.setReferenc("1");
                serviciob.setFromCurr(moneda);
            }
            if( txn.equals("0352") || txn.equals("0360") || txn.equals("0370") || txn.equals("0372") ||
                txn.equals("0456") || txn.equals("0458") || txn.equals("0468") || txn.equals("S352") ||
                txn.equals("S360") || txn.equals("S370") || txn.equals("S372") || txn.equals("S456") ||
                txn.equals("S458") || txn.equals("S468")) 
            {
                if (txn.substring(0,1).equals("S"))
                {txn = "0" + txn.substring(1);}
                
                serviciob.setTxnCode(txn);
                serviciob.setAcctNo(txn);
                if (txn.equals("0372")) 
                {
                    Monto = quitap((String)datasession.get("txtMtoIva"));
                    serviciob.setTranAmt(Monto);
                    serviciob.setCashIn(Monto);
                }
                else 
                {
                    Monto = quitap((String)datasession.get("txtMonto"));
                    serviciob.setTranAmt(Monto);
                    Monto = "";
                    Monto = quitap((String)datasession.get("txtEfectivo"));
                    serviciob.setCashIn(Monto);
                }
                serviciob.setFromCurr(moneda);
                
                String refer1 = "";
                String refer2 = "";
                if (txn.equals("0378")) {
                    refer1 = (String)datasession.get("txtReferenc");
                    refer2 = (String)datasession.get("txtReferenc1");
                    serviciob.setReferenc(refer1 + refer2);
                }
                else if (txn.equals("0458")) {
                    refer1 = (String)datasession.get("txtFolio0732");
                    serviciob.setReferenc(sucursal + setzeros(refer1));
                }
                else if (txn.equals("0468") || txn.equals("0456")) { //serviciob.setReferenc((String)datasession.get("txtReferenc"));
                    serviciob.setReferenc((String)datasession.get("txtRefer0732"));}
                else if(txn.equals("0372"))
                    serviciob.setReferenc("IVA");
                else
                { serviciob.setReferenc("1"); }
                
            }
            
            if( txn.equals("0368") ) {
                serviciob.setTxnCode(txn);
                serviciob.setAcctNo(txn);
                Monto = quitap((String)datasession.get("txtMontoNoCero"));
                serviciob.setTranAmt(Monto);
                String regist = (String)session.getAttribute("empno");
                String respon = (String)datasession.get("txtRegResp");
                String causas = (String)datasession.get("lstReferenc1");
                serviciob.setReferenc(regist + respon + causas);
                serviciob.setCashIn(Monto);
                serviciob.setFromCurr(moneda);
            }
            
            if( txn.equals("0540") || txn.equals("S540"))
            {     txn = "0540";
                  serviciob.setTxnCode(txn);
                  serviciob.setAcctNo((String)datasession.get("txtNumExpIn"));
                  Monto = quitap((String)datasession.get("txtMonto"));
                  serviciob.setTranAmt(Monto);
                  serviciob.setReferenc("2" + (String)datasession.get("lstPPagmes") + (String)datasession.get("txtPPagani"));
                  Monto = "";
                  Monto = quitap((String)datasession.get("txtEfectivo"));
                  serviciob.setCashIn(Monto);
                  serviciob.setFromCurr(moneda);
            }
            if ( (String)datasession.get("supervisor") != null )
                serviciob.setSupervisor((String)datasession.get("supervisor"));
            
            Diario diario = new Diario();
            String sMessage = diario.invokeDiario(serviciob, (String)session.getAttribute("d_ConsecLiga"), dirip, moneda, session);
            String txtCasoEsp = txn + "~"+ getString(sMessage,1) + "~";
            /*
            if (txn.equals("0352") || txn.equals("0360")) //Requieren cobro de comision || txn.equals("0370")
           {
            	ServiciosB comservicio = new ServiciosB();
	            comservicio.setBranch((String)datasession.get("sucursal"));
    		    comservicio.setTeller((String)datasession.get("teller"));
	            comservicio.setSupervisor((String)datasession.get("teller"));
    		    comservicio.setFormat("B");	
    		    comservicio.setTxnCode("0372");
                comservicio.setAcctNo("0372");
                Monto = quitap((String)datasession.get("txtMtoIva"));
		        comservicio.setFromCurr(moneda);
                comservicio.setTranAmt(Monto);
                comservicio.setCashIn(Monto);
                comservicio.setReferenc("IVA");
                Stack flujotxn = (Stack)session.getAttribute("page.flujotxn");
		        try{Thread.sleep( 1500 );}
                catch (InterruptedException e){}
		        sMessage = diario.invokeDiario(comservicio, (String)session.getAttribute("d_ConsecLiga"), dirip, moneda, session);                
		        txtCasoEsp = txtCasoEsp + "0372" + "~"+ getString(sMessage,1) + "~";				
                session.setAttribute("ftime", "3");
            }*/
            if(txn.equals("0370")){
            	Stack flujotxn = (Stack)session.getAttribute("page.flujotxn"); 
            	session.setAttribute("ftime", "3");           
            }
            session.setAttribute("txtCasoEsp", txtCasoEsp);
            
            session.setAttribute("page.txnresponse", sMessage);
            getServletContext().getRequestDispatcher("/servlet/ventanilla.Redirector").forward(request,response);
        }
    }
    
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
    
    private String quitap(String Valor) {
        if (Valor == null)
        {return "000";}
        StringBuffer Cantidad = new StringBuffer(Valor);
        try {
            for(int i=0; i<Cantidad.length();) {
                if( Cantidad.charAt(i) == ',' || Cantidad.charAt(i) == '.' )
                    Cantidad.deleteCharAt(i);
                else
                    ++i;
            }
        }
        catch(NullPointerException e)
        {System.out.println("Group06Serv::quitap handle NULL");}
        return Cantidad.toString();
    }
    public String getmoneda(String cmoneda) {
        String newmoneda = "";
        if (cmoneda.equals("N$"))
        { newmoneda = "01";}
        else if (cmoneda.equals("US$"))
        { newmoneda = "02";}
        else
        { newmoneda = "03";}
        return newmoneda;
    }
    
   
    public String removeAsterisk(String campo) {
        StringBuffer result = new StringBuffer(campo);
        String caracter = result.substring(result.length()-1,result.length());
        if (caracter.equals("*"))
            result.deleteCharAt(result.length()-1);
        return result.toString();
    }
    
    private String addpunto(String valor) {
        int longini = valor.length();
        String cents  = valor.substring(longini-2,longini);
        String entero = valor.substring(0,longini-2);
        int longente = entero.length();
        for (int i = 0; i < (longente-(1+i))/3; i++) {
            longente = entero.length();
            entero = entero.substring(0,longente-(4*i+3))+','+entero.substring(longente-(4*i+3));
        }
        entero = entero + '.' + cents;
        return entero;
    }
    
    private String getString(String newCadNum, int newOption) {
        int iPIndex = newCadNum.indexOf("CONSECUTIVO:") + 12, NumSaltos, Num = 0, Ban = 0;
        String nCadNum = new String("");
        NumSaltos = newOption;
        while(Num < NumSaltos) {
            if(newCadNum.charAt(iPIndex) == 0x20) {
                while(newCadNum.charAt(iPIndex) == 0x20)
                    iPIndex++;
                Num++;
            }
            else
                while(newCadNum.charAt(iPIndex) != 0x20)
                    iPIndex++;
        }
        while(newCadNum.charAt(iPIndex) != 0x20 && iPIndex < newCadNum.length()){
            nCadNum = nCadNum + newCadNum.charAt(iPIndex);
            if(iPIndex < newCadNum.length())
                iPIndex++;
            if(iPIndex == newCadNum.length())
                break;
        }
        
        if ( nCadNum.endsWith("~"))
            nCadNum = nCadNum.substring(0,nCadNum.length()-1);
        return nCadNum;
    }

    private String setzeros(String campo)
  {
      while(campo.length() < 10)
          campo = "0" + campo;
      return campo;
  }  
}
