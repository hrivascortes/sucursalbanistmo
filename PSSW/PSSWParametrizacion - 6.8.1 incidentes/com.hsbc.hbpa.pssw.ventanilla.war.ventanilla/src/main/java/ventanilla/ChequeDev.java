//*************************************************************************************************
//             Funcion: Clase Cheques devuelos
//            Elemento: ChequeDev.java
//          Creado por: Israel de Paz Mercado
//      Modificado por: Israel de Paz Mercado
//*************************************************************************************************
// CCN - 4360455 - 07/04/2005 - Se realizan modificaciones para el esquema de cheques devueltos.
// CCN - 4360462 - 10/04/2005 - Se genera nuevo paquete por errores en paquete anterior
//*************************************************************************************************



package ventanilla;

import java.io.IOException;
import java.util.Hashtable;
import java.util.Stack;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


public class ChequeDev extends HttpServlet
{
    private String delCommaPointFromString(String newCadNum) {
        String nCad = new String(newCadNum);
        if( nCad.indexOf(".") > -1) {
            nCad = nCad.substring(nCad.indexOf(".") + 1, nCad.length());
            if(nCad.length() != 2) {
                String szTemp = new String("");
                for(int j = nCad.length(); j < 2; j++)
                    szTemp = szTemp + "0";
                newCadNum = newCadNum + szTemp;
            }
        }
        
        StringBuffer nCadNum = new StringBuffer(newCadNum);
        for(int i = 0; i < nCadNum.length(); i++)
            if(nCadNum.charAt(i) == ',' || nCadNum.charAt(i) == '.')
                nCadNum.deleteCharAt(i);
        
        return nCadNum.toString();
    }	
	
    public void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {
    HttpSession session = req.getSession(false);
    Hashtable Chequesdevueltos = (Hashtable)session.getAttribute("ChequesDevueltos");
    Hashtable datasession = (Hashtable)session.getAttribute("page.datasession");
    if(Chequesdevueltos == null)
       Chequesdevueltos = new Hashtable();
        
        String lstChqDev = req.getParameter("lstChqDev");
        if (lstChqDev == null)
           lstChqDev = (String)datasession.get("lstChqDev");
        
        String txtCodSeg = req.getParameter("txtCodSeg");
        String txtCveTran = req.getParameter("txtCveTran");
        String txtDDACuenta2 = req.getParameter("txtDDACuenta2");
        session.setAttribute("CuentaChq", txtDDACuenta2);
        String txtSerial2 = req.getParameter("txtSerial2");
        session.setAttribute("SerialChq", txtSerial2);
        String txtMontoCheque = req.getParameter("txtMontoCheque");
        String txtFirmaFun = req.getParameter("txtFirmaFun");
        String Tot =(String)datasession.get("txtCheque");
        long Total = 0;
        Total =   Long.parseLong(delCommaPointFromString(Tot));
        
        String valor = txtMontoCheque+"*"+lstChqDev;
        
        long MontoTot = 0;
        long MontoCheque = 0;
        
       String Monto = (String)datasession.get("MontoTotal");
       if( Monto != null )
               MontoTot = Long.parseLong(Monto);
               
       MontoCheque =   Long.parseLong(delCommaPointFromString(txtMontoCheque));
       MontoTot = MontoTot +  MontoCheque;
       
       datasession.put("MontoTotal",new Long(MontoTot).toString());
       session.setAttribute("page.datasession",datasession);
       Chequesdevueltos.put(txtDDACuenta2+txtSerial2,valor);    
       session.setAttribute("ChequesDevueltos",Chequesdevueltos);
       
       if (((String)session.getAttribute("page.iTxn")).equals("1003") && (MontoTot < Total))
       {
    	   Stack flujotxn = (Stack)session.getAttribute("page.flujotxn");
           flujotxn.push("5353");
           session.setAttribute("page.flujotxn", flujotxn);
       }      
       
       resp.sendRedirect("../ventanilla/paginas/ResponseChqDev.jsp");
    }
    
    
    public void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doPost(req, resp);
    }
}
