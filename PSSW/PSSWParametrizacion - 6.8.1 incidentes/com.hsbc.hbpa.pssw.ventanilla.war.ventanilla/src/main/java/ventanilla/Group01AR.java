//*************************************************************************************************
//             Funcion: Clase que realiza txn 4223 - 4225
//            Elemento: Group01AR.java
//          Creado por: Alejandro Gonzalez Castro
//      Modificado por: Alejandro Gonzalez Castro
//*************************************************************************************************
// CCN - 4360189 - 03/09/2004 - Se cambia invoke al Diario para Control de Efectivo
//*************************************************************************************************
package ventanilla;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import ventanilla.com.bital.admin.Diario;

import java.util.Hashtable;

import ventanilla.com.bital.sfb.AclaracionesRAP;

public class Group01AR extends HttpServlet {
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/plain");
        HttpSession session = request.getSession(false);
        Hashtable datasession = (Hashtable)session.getAttribute("page.datasession");
        
        String dirip =  (String)session.getAttribute("dirip");
        
        AclaracionesRAP aclaracionesrap = new AclaracionesRAP();
        
        aclaracionesrap.setBranch((String)datasession.get("sucursal"));
        aclaracionesrap.setTeller((String)datasession.get("teller"));
        aclaracionesrap.setSupervisor((String)datasession.get("supervisor"));
        
        String txn = (String)datasession.get("cTxn");
        aclaracionesrap.setTxnCode(txn);
        
        aclaracionesrap.setAcctNo((String)datasession.get("txtDDA"));
        
        StringBuffer monto = new StringBuffer((String)datasession.get("txtMonto") );
        for(int i=0; i<monto.length();) {
            if( monto.charAt(i) == ',' || monto.charAt(i) == '.' )
                monto.deleteCharAt(i);
            else
                ++i;
        }
        
        aclaracionesrap.setTranAmt(monto.toString());
		GenericClasses gc = new GenericClasses();
        String moneda = (String)datasession.get("moneda");
		aclaracionesrap.setTranCur(gc.getDivisa(moneda));
       
        String sucursal = (String)datasession.get("sucursal");
        String fecha = (String)datasession.get("txtFechaError");
        
        String daymonth = fecha.substring(0,2) + fecha.substring(3,5);
        aclaracionesrap.setAcctNo5(sucursal + daymonth);
        
        String referencia = (String)datasession.get("txtRefer");
        
        if (txn.equals("4223")) 
        {
            aclaracionesrap.setTranDesc("CGO.X ACLA. " + referencia);
            aclaracionesrap.setDescRev("REV. CARGO POR ACLARACION DE COBRANZA");
        }
        else if (txn.equals("4225"))
        {
            aclaracionesrap.setTranDesc("ABO.X ACLA. " + referencia);
            aclaracionesrap.setDescRev("REV. ABONO POR ACLARACION DE COBRANZA");
        }
        
        
        Diario diario = new Diario();
        String sMessage = diario.invokeDiario(aclaracionesrap, (String)session.getAttribute("d_ConsecLiga"), dirip, moneda, session);

        session.setAttribute("page.txnresponse", sMessage);
        getServletContext().getRequestDispatcher("/servlet/ventanilla.Redirector").forward(request,response);
        
    }
    
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}