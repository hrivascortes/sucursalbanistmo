//*************************************************************************************************
//             Funcion: Clase que realiza txn 0856 - 4529 - 4531 - 4533 - 4011 - 4015
//            Elemento: Group01DAP.java
//          Creado por: Alejandro Gonzalez Castro
//      Modificado por: Alejandro Gonzalez Castro
//*************************************************************************************************
// CCN - 4360189 - 03/09/2004 - Se cambia invoke al Diario para Control de Efectivo
// CCN - 4360242 - 05/11/2004 - Se corrige problema de reverso de la txn 4011
// CCN - 4360368 - 02/09/2005 - Se elimina sleep
// CCN - 4360374 - 08/09/2005 - Se genera nuevo paquete a peticion de QA.
//*************************************************************************************************
package ventanilla;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import ventanilla.com.bital.admin.Diario;

import java.util.Hashtable;
import java.util.Stack;

import ventanilla.com.bital.sfb.DAPG;
import ventanilla.com.bital.sfb.DAPA;

public class Group01DAP extends HttpServlet
{
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        response.setContentType("text/plain");
        HttpSession session = request.getSession(false);
        Hashtable datasession = (Hashtable)session.getAttribute("page.datasession");
		GenericClasses gc = new GenericClasses();

        //Variables para diario electronico
        String dirip =  (String)session.getAttribute("dirip");

        String Itxn1 = "";
        String Iconsec1 = "";
        String Itxn2 = "";
        String txn = (String)datasession.get("cTxn");     //TIP
        String txn1 = (String)datasession.get("iTxn");    // TIP
        StringBuffer	monto = new StringBuffer("000");        //TIP

        if ((String)datasession.get("txtMonto")!= null )
            monto = new StringBuffer((String)datasession.get("txtMonto"));


        for(int i=0; i<monto.length();) {
            if( monto.charAt(i) == ',' || monto.charAt(i) == '.' )
                monto.deleteCharAt(i);
            else
                ++i;
        }

        String mto = monto.toString();
        String code = "";
        String mensaje = "";
        String Iconsec2 = "";
        String casoesp = "";
        String cadimpresion = "~DAP~";
        String txtBenefic = "";
        String txtValMonto = "NO";
        String sMessage = "";
        char status = '1';

        
        if ( datasession.get("posteo") == null ) {
            datasession.put("posteo","1");
            session.setAttribute("page.datasession",(Hashtable)datasession);
        }
        
        if ( session.getAttribute("txtCadImpresion") != null )
            cadimpresion = session.getAttribute("txtCadImpresion").toString();

        DAPG dapg = new DAPG();
        DAPA dapa = new DAPA();

        if (txn.equals("0856"))
        {
            if(!mto.equals("000") && txn.equals("0856"))
                txtValMonto = "SI";
        }
        datasession.put("txtValMonto", txtValMonto);

        if((mto.equals("000") && txn.equals("0856")) || txn1.equals("4011"))
        {
            // TXN DE CONSULTA 0856
            dapg.setBranch((String)datasession.get("sucursal"));
            dapg.setTeller((String)datasession.get("teller"));
            dapg.setSupervisor((String)datasession.get("supervisor"));
            dapg.setFormat("G");
            dapg.setTxnCode("0856");
            String moneda = (String)datasession.get("moneda");
			dapg.setToCurr(gc.getDivisa(moneda));
            dapg.setTranAmt("000");
            if ( txn1.equals("T529") || txn1.equals("T531"))
            {    //TIP
                dapg.setAcctNo(datasession.get("txtReferTIP").toString().substring(0,4));
                dapg.setBenefic(datasession.get("txtReferTIP").toString().substring(4));
                datasession.put("txtServicio",datasession.get("txtReferTIP").toString().substring(0,4));
                datasession.put("txtRefer",datasession.get("txtReferTIP").toString().substring(4));
                session.setAttribute("page.datasession", (Hashtable)datasession);
            }
            else
            {
                dapg.setAcctNo((String)datasession.get("txtServicio"));
                dapg.setBenefic((String)datasession.get("txtRefer"));
            }

            if(txn1.equals("4011"))
            {
                dapg.setCheckNo((String)datasession.get("txtSerial"));
                if(!mto.equals("000"))
                {
                    dapg.setInstruc("INQ");
                    dapg.setTranAmt(mto);
                }
            }

            Diario diario = new Diario();
            sMessage = diario.invokeDiario(dapg, (String)session.getAttribute("d_ConsecLiga"), dirip, moneda, session);

            code = sMessage.substring(0,1);
            mensaje = sMessage;

            if(code.equals("0"))
            {
                if(txn1.equals("4011") && !mto.equals("000"))
                {
                    txtBenefic = mensaje.substring(325,mensaje.length()-1);
                    datasession.put("txtBeneficiario", txtBenefic);
                    datasession.put("txtMonto",(String)datasession.get("txtMonto"));
                }
                else
                {
                    StringBuffer tmp = new StringBuffer(mensaje.substring(247,263));
                    for(int i=0; i<tmp.length();)
                    {
                        if(tmp.charAt(i) == '0')
                            tmp.deleteCharAt(i);
                        else
                            break;
                    }
                    String tem = tmp.toString();
                    tem = tem.trim();

                    String nCadNum = new String(tem.substring(0, tem.length()-2));
                    String cents = "." + tem.substring(tem.length()-2, tem.length());

                    for (int i = 0; i < (int)((nCadNum.length()-(1+i))/3); i++)
                        nCadNum = nCadNum.substring(0, nCadNum.length()-(4*i+3))+','+nCadNum.substring(nCadNum.length()-(4*i+3));

                    tem = nCadNum + cents;

                    txtBenefic = mensaje.substring(325,mensaje.length()-1);
                    datasession.put("txtMonto",tem);
                    datasession.put("txtBeneficiario", txtBenefic);
                }
                datasession.put("posteo","1");
                session.setAttribute("page.datasession", datasession);
            }
            else
            {
                String consec = mensaje.substring(25,34);
                casoesp =  "0856~" + consec.trim() + "~";
                session.setAttribute("txtCasoEsp",casoesp);
                session.setAttribute("page.certifField", "S");
            }
        }

        if(!mto.equals("000") && (txn1.equals("4529") || txn1.equals("4531") ||
                                  txn1.equals("T529") || txn1.equals("T531") )
            || txn.equals("4011"))
        {
            // TXN 4529 - EFECTIVO
            // TXN 4531 - CARGO CUENTA
            if ( (String)datasession.get("override") != null )
                if (datasession.get("override").toString().equals("SI"))
                    dapa.setOverride("3");

            dapa.setBranch((String)datasession.get("sucursal"));
            dapa.setTeller((String)datasession.get("teller"));
            dapa.setSupervisor((String)datasession.get("teller"));

            if ( (String)datasession.get("supervisor") != null )
                dapa.setSupervisor((String)datasession.get("supervisor"));

            if (txn1.equals("T529"))
                txn1 = "4529";
            if (txn1.equals("T531"))
                txn1 = "4531";

            dapa.setTxnCode(txn1);
            dapa.setAcctNo((String)datasession.get("txtServicio"));
            dapa.setTranAmt(mto);
            String moneda = (String)datasession.get("moneda");
			dapa.setTranCur(gc.getDivisa(moneda));

            if (txn1.equals("4529") || txn1.equals("T529"))
                dapa.setCashOut(mto);

            txn = txn1;

            dapa.setMoamoun("000");

            String txtDescrip= "";
            String DescRev = "";

            if(txn.equals("4011"))
            {
                dapa.setCheckNo((String)datasession.get("txtSerial"));
                dapa.setCashIn("000");

                txtDescrip= "DISP.PAGO"+(String)datasession.get("txtRefer");

                DescRev =   "REV.DISP."+(String)datasession.get("txtRefer");
                /*if(DescRev.length() > 25)
                    DescRev = DescRev.substring(0,25);*/
            }
            else
            {
                txtDescrip= "DISP.PAGO"+(String)datasession.get("txtRefer");
                DescRev =   "REV.DISP."+(String)datasession.get("txtRefer");

                while( DescRev.length() < 40 )
                    DescRev = DescRev + " ";

                if ( datasession.get("txtValMonto").toString().equals("SI"))
                {
                    while( txtDescrip.length() < 39 )
                        txtDescrip = txtDescrip + " ";
                    if(txtDescrip.length() > 39)
                        txtDescrip = txtDescrip.substring(0,39);

                    txtDescrip = txtDescrip + "Y";

                    DescRev = DescRev.substring(0,39);
                    DescRev = DescRev + "Y";
                }
            }

            dapa.setTranDesc(txtDescrip);
            dapa.setDescRev(DescRev);

            if ( datasession.get("posteo").toString().equals("1")) 
            {

                Diario diario = new Diario();
                sMessage = diario.invokeDiario(dapa, (String)session.getAttribute("d_ConsecLiga"), dirip, moneda, session);
                status = sMessage.charAt(0);
                mensaje = sMessage;

                Itxn1 = txn;
                Iconsec1 = mensaje.substring(25,34);

                casoesp = Itxn1 + "~" + Iconsec1.trim() + "~";

            }
            else
                status = '0';

            if(status == '0')
            {
                // Llamado a Totales

                if ( datasession.get("txtValMonto").toString().equals("SI")) { // Se capturo Monto
                    txtBenefic = mensaje.substring(169,mensaje.length()-1);
                    datasession.put("txtBeneficiario", txtBenefic);
                }

                if(!txn1.equals("4011"))
                {
                    cadimpresion = cadimpresion + mensaje.substring(91,133).trim() + "~" +
                    (String)datasession.get("txtServicio") + "~" +
                    (String)datasession.get("txtMonto") + "~" +
                    (String)datasession.get("txtBeneficiario")+ "~" +
                    (String)datasession.get("txtRefer") + "~~";
                }
            }
            else
                cadimpresion = "";


            if(!txn1.equals("4011"))
            {
                Stack flujotxn = (Stack)session.getAttribute("page.flujotxn");
                if(!flujotxn.empty())
                {
                    flujotxn.pop();
                    session.setAttribute("page.flujotxn", flujotxn);
                }
            }

            if(status == '0' && (txn1.equals("4531") || txn1.equals("T531") || txn1.equals("4011")))
            {


                if ( datasession.get("posteo").toString().equals("1")) {
                  datasession.put("override","NO");
                  datasession.put("posteo","2");
                  session.setAttribute("page.datasession", datasession);
               }

                dapa = new DAPA();
                dapa.setBranch((String)datasession.get("sucursal"));
                dapa.setTeller((String)datasession.get("teller"));
                dapa.setSupervisor((String)datasession.get("supervisor"));

                if ( (String)datasession.get("supervisor") != null )
                     dapa.setSupervisor((String)datasession.get("supervisor"));

                if ( (String)datasession.get("override") != null )
                   if (datasession.get("override").toString().equals("SI"))
                      dapa.setOverride("3");

                moneda = (String)datasession.get("moneda");
				dapa.setTranCur(gc.getDivisa(moneda));

                if(txn1.equals("4011"))
                {
                    dapa.setTxnCode("4015");
                    dapa.setCashIn("000");
                    if(moneda.equals("01"))
                        dapa.setAcctNo("0103000023");
                    if(moneda.equals("02"))
                        dapa.setAcctNo("3902222222");
                    txtDescrip = (String)datasession.get("txtBeneficiario");
                    dapa.setCheckNo((String)datasession.get("txtSerial"));
                    dapa.setAcctNo1((String)datasession.get("txtServicio"));
                    datasession.put("txtDDACuenta", (String)datasession.get("txtServicio"));
                    Itxn2 = "4015";
                }
                else
                {
                    dapa.setTxnCode("4533");
                    dapa.setAcctNo((String)datasession.get("txtDDACuenta"));
                    txtDescrip = "ABONO REF"+(String)datasession.get("txtRefer");

                    DescRev = "REV."+txtDescrip;
                    if(DescRev.length() > 40)
                        DescRev = DescRev.substring(0,40);
                    dapa.setDescRev(DescRev);

                    Itxn2 = "4533";
                }

                dapa.setTranAmt(mto);

                dapa.setMoamoun("000");
                dapa.setTranDesc(txtDescrip);

                Diario diario = new Diario();
                sMessage = diario.invokeDiario(dapa, (String)session.getAttribute("d_ConsecLiga"), dirip, moneda, session);

                mensaje = sMessage;

                Iconsec2 = mensaje.substring(25,34);

                casoesp = casoesp + Itxn2 + "~" + Iconsec2.trim() + "~";
                if(sMessage.substring(0,1).equals("0"))
                {

                    cadimpresion = "~DAP~";
                    cadimpresion = cadimpresion +  mensaje.substring(91,133).trim() + "~" +
                    (String)datasession.get("txtServicio") + "~" +
                    (String)datasession.get("txtMonto") + "~" +
                    (String)datasession.get("txtBeneficiario") + "~" +
                    (String)datasession.get("txtRefer") + "~" +
                    (String)datasession.get("txtDDACuenta")+ "~";
                }
                else
                    cadimpresion = "";
            }
            session.setAttribute("txtCasoEsp",casoesp);
            session.setAttribute("txtCadImpresion",cadimpresion);
            session.setAttribute("page.certifField", "S");
        }
        session.setAttribute("page.txnresponse", mensaje);
        getServletContext().getRequestDispatcher("/servlet/ventanilla.Redirector").forward(request,response);
    }
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}
