//*************************************************************************************************
//             Funcion: Clase que realiza txn ATM
//            Elemento: GroupATM1.java
//          Creado por: Alejandro Gonzalez Castro
//      Modificado por: Alejandro Gonzalez Castro
//*************************************************************************************************
// CCN - 4360189 - 03/09/2004 - Se cambia invoke al Diario para Control de Efectivo
//*************************************************************************************************
package ventanilla;

import java.io.IOException;
import java.util.Hashtable;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import ventanilla.com.bital.admin.Diario;
import ventanilla.com.bital.sfb.FormatoA;

public class GroupATM1 extends HttpServlet
{

  private String delCommaPointFromString(String newCadNum)
  {
    String nCad = new String(newCadNum);
    if ( nCad.indexOf(".") > -1) {
       nCad = nCad.substring(nCad.indexOf(".") + 1, nCad.length());
       if (nCad.length() != 2) {
           String szTemp = new String("");
           for (int j = nCad.length(); j < 2; j++)
               szTemp = szTemp + "0";
           newCadNum = newCadNum + szTemp;
       }
    }

    StringBuffer nCadNum = new StringBuffer(newCadNum);
    for (int i = 0; i < nCadNum.length(); i++)
        if (nCadNum.charAt(i) == ',' || nCadNum.charAt(i) == '.')
            nCadNum.deleteCharAt(i);

    return nCadNum.toString();
  }



  public void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
  {
    // Instanciar a los bean

    FormatoA oFormatoA = new FormatoA();
	GenericClasses gc = new GenericClasses();

    resp.setContentType("text/plain");

    HttpSession session = req.getSession(false);
    Hashtable datasession = (Hashtable)session.getAttribute("page.datasession");
    
    String dirip = (String)session.getAttribute("dirip");

    String getMsg = "";

       String txtTxnCode    = (String)datasession.get("cTxn");
       String txtFormat     = "A";
       String txtBranch     = (String)datasession.get("sucursal");
       String txtTeller     = (String)datasession.get("teller");//"000108";
       String txtSupervisor = (String)datasession.get("teller");//"000101";
       String txtBackOut    = "0";
       String txtOverride   = "0";
       String txtCurrentDay = "0";

       if ((String)datasession.get("supervisor")!= null)
           txtSupervisor = (String)datasession.get("supervisor");
       
       if ( (String)datasession.get("override") != null )
                if (datasession.get("override").toString().equals("SI"))
                    txtOverride = "3";
            

      // Se establesen por medio del BEAN los datos del encabezado
       oFormatoA.setFormat(txtFormat);
       oFormatoA.setBranch(txtBranch);
       oFormatoA.setTeller(txtTeller);
       oFormatoA.setSupervisor(txtSupervisor);
       oFormatoA.setBackOut(txtBackOut);
       oFormatoA.setOverride(txtOverride);
       oFormatoA.setCurrentDay(txtCurrentDay);
       // Se Obtienen los datos para la transacci"n
       String txtEfectivo   = "000";
       String txtDescrip    = "";
       String txtNumFolio   = "";
       String txtDDACuenta  = (String)datasession.get("txtDDACuenta");
       String txtComision   = "";
       String txtMonto      = delCommaPointFromString((String)datasession.get("txtMonto"));
       String txtMoneda     = gc.getDivisa((String)datasession.get("moneda"));
       String txtCheckNo    = "";
       String txtNumTarj    = "";
       String txtFechaEfect = "";
       String txtHora    = "";
       String txtNemonico    = "";
       String DescRev = "";                       // Descripcisn para el reverso
       
       if ( !txtTxnCode.equals("5539")) {
          txtCheckNo    = (String)datasession.get("txtSecuencia");
          txtNumTarj    = (String)datasession.get("txtNumTarj");
          txtFechaEfect = (String)datasession.get("txtFechaEfect");
          txtFechaEfect = txtFechaEfect.substring(6,10) + txtFechaEfect.substring(3,5) +  txtFechaEfect.substring(0,2);
          txtHora       = (String)datasession.get("txtHora");
          txtNemonico   = (String)datasession.get("txtNemonico");
       }

       if (!txtTxnCode.equals("5089") && !txtTxnCode.equals("5539"))
           txtNumFolio = (String)datasession.get("txtNumFolioATM");

       if ( txtTxnCode.equals("5539"))
           txtComision = (String)datasession.get("txtComision");

       if ( txtTxnCode.equals("5089")) {
           txtDescrip = "RETIRO CAJERO " + txtNumTarj + " " + txtFechaEfect;
           DescRev    = "REVERSOCAJERO" + " " +  txtNumTarj + " " + txtFechaEfect;
       }

       if ( txtTxnCode.equals("5523")) {
           txtDescrip = "CAR.ATM" + txtNumFolio + " " + txtNumTarj + " " + txtFechaEfect;
           DescRev    = "REVERSO" + txtNumFolio + " " + txtNumTarj + " " + txtFechaEfect;
       }

       if ( txtTxnCode.equals("5525")) {
           txtDescrip = "ABO.ATM" + txtNumFolio + " " + txtNumTarj + " " + txtFechaEfect;
           DescRev    = "REVERSO" + txtNumFolio + " " + txtNumTarj + " " + txtFechaEfect;
       }
       
       if ( txtTxnCode.equals("5537")) {
           txtDescrip = "ABO.ATM" + txtNumFolio + " " + txtNumTarj + " " + txtFechaEfect;
           DescRev    = "REVERSOCAJERO" + " " + txtNumTarj + " " + txtFechaEfect;
       }

       oFormatoA.setTxnCode(txtTxnCode);
       oFormatoA.setTranAmt(txtMonto);                   //Monto Total de la Txn.
       oFormatoA.setTranCur(txtMoneda);                  //Tipo de Moneda con que se opera la Txn.
       oFormatoA.setAcctNo(txtDDACuenta);                //No de Cuenta

       if (!txtTxnCode.equals("5539")) {
           oFormatoA.setTranDesc(txtDescrip);                //Descripcion
           oFormatoA.setCheckNo(txtCheckNo);                 //Secuencia
	   oFormatoA.setAcctNo1(txtHora);                    // Hora
           oFormatoA.setAcctNo2(txtNemonico);                // Nemonico
           if(DescRev.length() > 40)
              DescRev = DescRev.substring(0,40);
           oFormatoA.setDescRev(DescRev);
       }
       else
           oFormatoA.setAmount5(txtComision);                // Nemonico

       Diario diario = new Diario();
       String sMessage = diario.invokeDiario(oFormatoA, (String)session.getAttribute("d_ConsecLiga"), dirip, txtMoneda, session);

       session.setAttribute("page.txnresponse", sMessage);
       getServletContext().getRequestDispatcher("/servlet/ventanilla.Redirector").forward(req,resp);
   }

  public void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
  {
    doPost(req, resp);
  }

}
