//*************************************************************************************************
//             Funcion: Clase que realiza txn 1059 - 1067 - 1125 - 1161 - 1065
//            Elemento: Group01R.java
//      Modificado por: Alejandro Gonzalez Castro
//*************************************************************************************************
// CCN - 4360189 - 03/09/2004 - Se cambia invoke al Diario para Control de Efectivo
// CCN - 4360237 - 05/11/2004 - Se incluye txn 1515
// CCN - 4360268 - 04/02/2005 - Se realizan adecuaciones para eliminar Agencia NY
// CCN - 4360368 - 02/09/2005 - Adecuacion para Depto 9024 txn 1065
// CCN - 4360374 - 08/09/2005 - Se genera nuevo paquete a peticion de QA.
// CCN -         - 10/01/2008 - Se hacen modificaciones para transacción global de cheques locales
//*************************************************************************************************

package ventanilla;

import java.io.IOException;
import java.util.Hashtable;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import ventanilla.com.bital.sfb.Retiros;
import ventanilla.com.bital.admin.Diario;

public class Group01R extends HttpServlet
{
    public void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {
        Retiros oRetiros = new Retiros();
		GenericClasses cGenericas = new GenericClasses();

        resp.setContentType("text/plain");

        HttpSession session = req.getSession(false);
        Hashtable datasession = (Hashtable)session.getAttribute("page.datasession");

        String dirip = (String)session.getAttribute("dirip");
        String txtMoneda = (String)datasession.get("moneda");

        String getMsg = "";
        String txtTxnCode    = (String)datasession.get("cTxn");
        String txtFormat     = "A";
        String txtBranch     = (String)datasession.get("sucursal");
        String txtTeller     = (String)datasession.get("teller");
        String txtSupervisor = (String)datasession.get("teller");
        String txtBackOut    = "0";
        String txtOverride   = "0";
        String txtCurrentDay = "0";

        if ( (String)datasession.get("supervisor") != null )
            txtSupervisor =  (String)datasession.get("supervisor");

        oRetiros.setTxnCode(txtTxnCode);
        oRetiros.setFormat(txtFormat);
        oRetiros.setBranch(txtBranch);
        oRetiros.setTeller(txtTeller);
        oRetiros.setSupervisor(txtSupervisor);
        oRetiros.setBackOut(txtBackOut);
        oRetiros.setOverride(txtOverride);
        oRetiros.setCurrentDay(txtCurrentDay);
        if ((String)datasession.get("override") != null )
            if (datasession.get("override").toString().equals("SI"))
                oRetiros.setOverride("3");

        String txtRefer      = "";
        String txtFechaEfect  = "";
        String txtFechaSys  = "";
        String lstBanco      = "";
        String lstCausa      = "";
        String txtSerial     = "";
        String txtEfectivo   = "000";
        String txtDescripcion= "";
        String txtMonto      = "";

        String txtDDACuenta  = (String)datasession.get("txtDDACuenta");

        if ( txtTxnCode.equals("4067"))
            txtMonto = cGenericas.delCommaPointFromString((String)datasession.get("txtMonto1"));
        else
            txtMonto = cGenericas.delCommaPointFromString((String)datasession.get("txtMonto"));

        if (txtTxnCode.equals("1065"))
        {
            txtRefer      = (String)datasession.get("txtReferencia3");
            oRetiros.setTranDesc("CARGO POR CARTERA "+txtRefer);
            oRetiros.setDescRev("REV CARGO CARTERA "+txtRefer);

        }
        if ( txtTxnCode.equals("1504") || txtTxnCode.equals("1237")
            || txtTxnCode.equals("1506") || txtTxnCode.equals("1508") || txtTxnCode.equals("1510")
            || txtTxnCode.equals("1532") || txtTxnCode.equals("5197") || txtTxnCode.equals("1067")
            || txtTxnCode.equals("1512") || txtTxnCode.equals("1059") || txtTxnCode.equals("1235")
            || txtTxnCode.equals("1509"))
        {
            txtFechaEfect  = (String)datasession.get("txtFechaEfect");
            txtFechaSys  = (String)datasession.get("txtFechaSys");
            txtFechaEfect =  cGenericas.ValEffDate(txtFechaEfect, txtFechaSys);
            oRetiros.setEffDate(txtFechaEfect);
        }

        if (txtTxnCode.equals("1125") /*|| txtTxnCode.equals("1161")*/) {
            txtFechaEfect  = (String)datasession.get("txtFechaEfect");
            txtFechaSys  = (String)datasession.get("txtFechaSys");
            txtFechaEfect =  cGenericas.ValEffDate(txtFechaEfect, txtFechaSys);
            oRetiros.setEffDate(txtFechaEfect);
        }

        if ( txtTxnCode.equals("5487"))
        {
            txtFechaEfect  = (String)datasession.get("txtFechaEfect");
            //lstBanco      = (String)datasession.get("lstBanco");
            lstBanco = "";
            lstCausa      = (String)datasession.get("lstCausa2");
            //txtSerial     = (String)datasession.get("txtSerial3");
			txtSerial = "";
            txtFechaSys  = (String)datasession.get("txtFechaSys");
            txtFechaEfect =  cGenericas.ValEffDate(txtFechaEfect, txtFechaSys);
            oRetiros.setEffDate(txtFechaEfect);
            oRetiros.setCheckNo(lstBanco+lstCausa+txtSerial);
            oRetiros.setTrNo(lstBanco+lstCausa);
        }
        if(txtTxnCode.equals("1525"))
        {
            String cuenta = (String)datasession.get("txtAcct8000");
            if ( cuenta == null)
                cuenta = (String)datasession.get("txtDDACuenta");
            if ( cuenta != null ) {
                txtDDACuenta  = cuenta;
                cuenta = cuenta.substring(0,1);
            }
        }

        if ( txtTxnCode.equals("5553")) {
            txtSerial     = (String)datasession.get("txtSerial");
            oRetiros.setCheckNo(txtSerial);
        }
        if ( txtTxnCode.equals("4067")) {
            txtSerial     = (String)datasession.get("txtSerial4");
            oRetiros.setCheckNo(txtSerial);
        }

        String DescRev = "";

        if ( txtTxnCode.equals("1523")) {
            txtDescripcion= (String)datasession.get("lstDesc1523");
            DescRev = txtDescripcion.substring(0,2) + "REV." + txtDescripcion.substring(2,txtDescripcion.length());
            if(DescRev.length() > 40)
                DescRev = DescRev.substring(0,40);
            oRetiros.setDescRev(DescRev);
            txtSerial     = (String)datasession.get("txtSerial");
            String cuenta = (String)datasession.get("txtDDACuenta");
            cuenta = cuenta.substring(0,1);
            oRetiros.setTranDesc(txtDescripcion);
            oRetiros.setCheckNo(txtSerial);
        }
        if ( txtTxnCode.equals("5389")) {
            txtFechaEfect  = (String)datasession.get("txtFechaEfect");
            txtSerial     = (String)datasession.get("txtSerial");
            txtFechaSys  = (String)datasession.get("txtFechaSys");
            txtFechaEfect =  cGenericas.ValEffDate(txtFechaEfect, txtFechaSys);
            oRetiros.setEffDate(txtFechaEfect);
            oRetiros.setCheckNo(txtSerial);
        }
        if(txtTxnCode.equals("1515"))
        {
            oRetiros.setDescRev("REV.");
            oRetiros.setMoAmoun("000");
        }
        oRetiros.setAcctNo(txtDDACuenta);
        oRetiros.setTranAmt(txtMonto);
        oRetiros.setTranCur(txtMoneda);

        Diario diario = new Diario();
        String sMessage = diario.invokeDiario(oRetiros, (String)session.getAttribute("d_ConsecLiga"), dirip, txtMoneda, session);

        session.setAttribute("page.txnresponse", sMessage);
        getServletContext().getRequestDispatcher("/servlet/ventanilla.Redirector").forward(req,resp);

    }

    public void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {
        doPost(req, resp);
    }
}
