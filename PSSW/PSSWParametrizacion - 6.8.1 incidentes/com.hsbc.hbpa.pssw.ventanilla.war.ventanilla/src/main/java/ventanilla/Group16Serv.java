//*************************************************************************************************
//             Funcion: Clase que realiza txn Servicios 0087
//            Elemento: Group16Serv.java
//          Creado por: Alejandro Gonzalez Castro
//		Modificado por: Juvenal Fernandez
//*************************************************************************************************
// CCN - 4360189 - 03/09/2004 - Se cambia invoke al Diario para Control de Efectivo
// CCN - 4360268 - 04/02/2005 - Se realizan adecuaciones para eliminar Agencia NY
//*************************************************************************************************
package ventanilla;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.Hashtable;

import ventanilla.com.bital.sfb.ServiciosA;
import ventanilla.com.bital.admin.Diario;

public class Group16Serv extends HttpServlet
{
  public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
  {
    response.setContentType("text/plain");
	GenericClasses gc = new GenericClasses();
    HttpSession session = request.getSession(false);
    Hashtable datasession = (Hashtable)session.getAttribute("page.datasession");

    String sMessage = "";
    String dirip =  (String)session.getAttribute("dirip");
    ServiciosA serviciosa = new ServiciosA();

    serviciosa.setBranch((String)datasession.get("sucursal"));
    serviciosa.setTeller((String)datasession.get("teller"));
    serviciosa.setSupervisor((String)datasession.get("teller"));

    if ( (String)datasession.get("override") != null )
        if (datasession.get("override").toString().equals("SI"))
            serviciosa.setOverride("3");
    if ( (String)datasession.get("supervisor") != null )
        serviciosa.setSupervisor((String)datasession.get("supervisor"));
    
    String txn = (String)datasession.get("cTxn");
    serviciosa.setTxnCode(txn);

     String txtDDACuenta =  (String)datasession.get("txtDDACuenta");    
     serviciosa.setAcctNo(txtDDACuenta);
  
     String chqs = (String)datasession.get("lstNoChqs");
     if(chqs.startsWith("0"))
         chqs = chqs.substring(1);
     
     serviciosa.setTranAmt(chqs);
     
     serviciosa.setMoamoun("000");

     String moneda = (String)datasession.get("moneda");
	 serviciosa.setTranCur(gc.getDivisa(moneda));
     
     Diario diario = new Diario();
     sMessage = diario.invokeDiario(serviciosa, (String)session.getAttribute("d_ConsecLiga"), dirip, moneda, session);

     session.setAttribute("page.txnresponse", sMessage);
     getServletContext().getRequestDispatcher("/servlet/ventanilla.Redirector").forward(request,response);
  }

  public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
  {
    doPost(request, response);
  }
}
