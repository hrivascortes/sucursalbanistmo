//*************************************************************************************************
//             Funcion: Clase que realiza txn 4453
//            Elemento: Group05R.java
//          Creado por: Alejandro Gonzalez Castro
//      Modificado por: Alejandro Gonzalez Castro
//*************************************************************************************************
// CCN - 4360189 - 03/09/2004 - Se cambia invoke al Diario para Control de Efectivo
//*************************************************************************************************

package ventanilla;

import java.io.IOException;
import java.util.Hashtable;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import ventanilla.com.bital.sfb.FormatoE;
import ventanilla.com.bital.sfb.Acdo;
import ventanilla.com.bital.admin.Diario;

public class Group05R extends HttpServlet {

    private String delCommaPointFromString(String newCadNum) {
        String nCad = new String(newCadNum);
        if( nCad.indexOf(".") > -1) {
            nCad = nCad.substring(nCad.indexOf(".") + 1, nCad.length());
            if(nCad.length() != 2) {
                String szTemp = new String("");
                for(int j = nCad.length(); j < 2; j++)
                    szTemp = szTemp + "0";
                newCadNum = newCadNum + szTemp;
            }
        }

        StringBuffer nCadNum = new StringBuffer(newCadNum);
        for(int i = 0; i < nCadNum.length(); i++)
            if(nCadNum.charAt(i) == ',' || nCadNum.charAt(i) == '.')
                nCadNum.deleteCharAt(i);

        return nCadNum.toString();
    }


    public void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {
        FormatoE oFormatoE = new FormatoE();
        Acdo    oAcdo    = new Acdo();
		GenericClasses gc = new GenericClasses();

        resp.setContentType("text/plain");

        HttpSession session = req.getSession(false);
        Hashtable datasession = (Hashtable)session.getAttribute("page.datasession");

        String txtTxnCode    = "0825";// la Txn 0825 es la del ACDO.
        String txtFormat     = "B";
        String txtBranch     = (String)datasession.get("sucursal");
        String txtTeller     = (String)datasession.get("teller");//"000108";
        String txtSupervisor = (String)datasession.get("teller");//"000101";
        String txtBackOut    = "0";
        String txtOverride   = "0";
        String txtCurrentDay = "0";

        if ( (String)datasession.get("supervisor") != null )             //se pidio autorizacisn
            txtSupervisor = (String)datasession.get("supervisor");

        oAcdo.setTxnCode(txtTxnCode);
        oAcdo.setFormat(txtFormat);
        oAcdo.setBranch(txtBranch);
        oAcdo.setTeller(txtTeller);
        oAcdo.setSupervisor(txtSupervisor);
        oAcdo.setBackOut(txtBackOut);
        oAcdo.setOverride(txtOverride);
        oAcdo.setCurrentDay(txtCurrentDay);

        String lstTipoDocto  = (String)datasession.get("lstTipoDocto"); //Tipo de Documento de ACDO
        String txtReferencia = (String)datasession.get("txtFolio");//Referencia del Documento de ACDO
        oAcdo.setAcctNo((lstTipoDocto + txtReferencia));
        oAcdo.execute();
        String sMessage = oAcdo.getMessage();
        if(sMessage.charAt(0) == '0')        
        {
            txtTxnCode    = (String)datasession.get("cTxn");
            txtFormat     = "A";

            String dirip = (String)session.getAttribute("dirip");
            String getMsg = "";

            if ( (String)datasession.get("supervisor") != null )
                txtSupervisor = (String)datasession.get("supervisor");

            oFormatoE.setTxnCode(txtTxnCode);
            oFormatoE.setFormat(txtFormat);
            oFormatoE.setBranch(txtBranch);
            oFormatoE.setTeller(txtTeller);
            oFormatoE.setSupervisor(txtSupervisor);
            oFormatoE.setBackOut(txtBackOut);
            oFormatoE.setOverride(txtOverride);
            oFormatoE.setCurrentDay(txtCurrentDay);
            String txtFechEfect  = "";
            String txtSerial  = "";
            String txtDDACuenta  = "400";
            String txtMonto      = delCommaPointFromString((String)datasession.get("txtMonto"));
            String txtMoneda     = (String)datasession.get("moneda");
            txtReferencia        = (String)datasession.get("txtReferencia");

            oFormatoE.setAcctNo(txtDDACuenta); //No de Cuenta
            oFormatoE.setTranAmt(txtMonto);    //Monto Total de la Txn.
            oFormatoE.setCashOut(txtMonto);    // Salida de efectivo
			txtMoneda=gc.getDivisa(txtMoneda);
            oFormatoE.setFromCurr(txtMoneda);   //Tipo de Moneda con que se opera la Txn.
            oFormatoE.setReferenc1(txtReferencia);

            Diario diario = new Diario();
            sMessage = diario.invokeDiario(oFormatoE, (String)session.getAttribute("d_ConsecLiga"), dirip, txtMoneda, session);

            session.setAttribute("page.txnresponse", sMessage);
            getServletContext().getRequestDispatcher("/servlet/ventanilla.Redirector").forward(req,resp);

        }
    }

    public void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {
        doPost(req, resp);
    }
}