//*************************************************************************************************
//             Funcion: Clase que realiza txn 0823
//            Elemento: CV_0823_XXXX.java
//          Creado por: Alejandro Gonzalez Castro
//		Modificado por: Fredy Pe�a Moreno
//*************************************************************************************************
// CCN - 4360189 - 03/09/2004 - Se cambia invoke al Diario para Control de Efectivo
// CCN - 4360268 - 04/02/2005 - Se realizan adecuaciones para eliminar Agencia NY
// CCN - 4360368 - 02/09/2005 - Se elimina sleep
// CCN - 4360374 - 08/09/2005 - Se genera nuevo paquete a peticion de QA.
// CCN - 4360533 - 20/10/2006 - Se realizan modificaciones para OPEE
// CCN - 4620048 - 14/12/2007 - Se realizan modificaciones para compraventa
//*************************************************************************************************
package ventanilla;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import ventanilla.GenericClasses;

import java.util.Hashtable;
import java.util.Vector;

import ventanilla.com.bital.sfb.CompraVenta;
import ventanilla.com.bital.admin.Diario;

public class CV_0823 extends HttpServlet {

	    private GenericClasses gc = new GenericClasses();
        private String delLeadingfZeroes(String newString){
        StringBuffer newsb = new StringBuffer(newString);
        int i = 0;
        while(newsb.charAt(i) == '0')
            newsb.deleteCharAt(0);
        return newsb.toString();
    }

    private String setFormat(String newCadNum) {
        int iPIndex = newCadNum.indexOf(".");
        String nCadNum = new String(newCadNum.substring(0, iPIndex));
        for (int i = 0; i < (int)((nCadNum.length()-(1+i))/3); i++)
            nCadNum = nCadNum.substring(0, nCadNum.length()-(4*i+3))+','+nCadNum.substring(nCadNum.length()-(4*i+3));
        return nCadNum + newCadNum.substring(iPIndex, newCadNum.length());
    }

    private String setPointToString(String newCadNum) {
        int iPIndex = newCadNum.indexOf("."), iLong;
        String szTemp;

        if(iPIndex > 0) {
            newCadNum = new String(newCadNum + "00");
            newCadNum = newCadNum.substring(0, iPIndex + 1) + newCadNum.substring(iPIndex + 1, iPIndex + 3);
        }
        else {
            for(int i = newCadNum.length(); i < 3; i++)
                newCadNum = "0" + newCadNum;
            iLong = newCadNum.length();
            if(iLong == 3)
                szTemp = newCadNum.substring(0, 1);
            else
                szTemp = newCadNum.substring(0, iLong - 2);
            newCadNum = szTemp + "." + newCadNum.substring(iLong - 2);
        }
        return newCadNum;
    }

    public void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {
        CompraVenta oTxn = new CompraVenta();

        HttpSession session = req.getSession(false);
        Hashtable datasession = (Hashtable)session.getAttribute("page.datasession");
        GenericClasses cGenericas = new GenericClasses();

        String Currency = (String)datasession.get("moneda");   
        String Currency2 = (String)session.getAttribute("page.moneda_s");
        String dirip =  (String)session.getAttribute("dirip");
        String iTxn = (String)datasession.get("iTxn");
        String TO = (String)datasession.get("oTxn");
        String cTxn = (String)datasession.get("cTxn");

        String txtCasoEsp = new String("");
        String txtCadImpresion = new String("");
        String txtSupervisor = (String)datasession.get("teller");
        String txtRegistro = (String)datasession.get("txtRegistro");
        String Monto = "";
        String txtComision = "";
        String txtComisionMN = "";
        String txtComisionUSD = "";
        String txtPorcIVA = "";
        String txtIvaMN = "";
        String txtIvaUSD = "";
        String txtDirSuc = "";

        int Resultado = 1;

        if ( (String)datasession.get("supervisor") != null )       //pidio autorizacisn de supervisor
            txtSupervisor =  (String)datasession.get("supervisor");



        oTxn.setTxnCode("0823");
        oTxn.setBranch((String)datasession.get("sucursal"));
        oTxn.setTeller((String)datasession.get("teller"));
        oTxn.setSupervisor(txtSupervisor);

        oTxn.setFormat( "G" );
        
        if(Currency.equals("01"))
        {
			oTxn.setService( "V" );
			if (TO.equals("0106"))
				oTxn.setGStatus( "1" );
			else if (TO.equals("4111"))
				oTxn.setGStatus( "5" );
			else if (TO.equals("4115"))
				oTxn.setGStatus( "6" );
			else if (TO.equals("0778"))
				oTxn.setGStatus( "4" );
			else if (TO.equals("0786"))
				oTxn.setGStatus( "3" );
			else if (TO.equals("0594"))
				oTxn.setGStatus( "3" );    
			else if (TO.equals("0116"))
				oTxn.setGStatus( "2" );
        }else
		{
			oTxn.setService( "C" );
			oTxn.setGStatus( "1" );
			if (iTxn.equals("4057") || iTxn.equals("4059") || iTxn.equals("4063"))
				oTxn.setGStatus( "6" );
			if (iTxn.equals("4043") && !TO.equals("4111"))
			{
			//if (iTxn.equals("4043"))
			   if(!Currency.equals("02") && !Currency.equals("06") && !Currency.equals("04") && !Currency.equals("09"))
				oTxn.setGStatus( "5" );
			}
			if (iTxn.equals("4043") && TO.equals("4111"))
			{
				oTxn.setGStatus( "5" );
			}
			if (iTxn.equals("5159"))
				oTxn.setGStatus( "5" );
		}

		
		
/*
        if(Currency.equals("02"))
        {
            oTxn.setService( "C" );
            oTxn.setGStatus( "1" );
            if (iTxn.equals("4057") || iTxn.equals("4059") || iTxn.equals("4063"))
                oTxn.setGStatus( "6" );
            if (iTxn.equals("4043"))
                oTxn.setGStatus( "5" );
            if (iTxn.equals("5159"))
                oTxn.setGStatus( "5" );
        }
        else
        {
            oTxn.setService( "V" );
            if (TO.equals("0106"))
                oTxn.setGStatus( "1" );
            else if (TO.equals("4111"))
                oTxn.setGStatus( "5" );
            else if (TO.equals("4115"))
                oTxn.setGStatus( "6" );
            else if (TO.equals("0778"))
                oTxn.setGStatus( "4" );
            else if (TO.equals("0786"))
                oTxn.setGStatus( "3" );
            else if (TO.equals("0594"))
                oTxn.setGStatus( "3" );    
            else if (TO.equals("0116"))
                oTxn.setGStatus( "2" );
        }
*/
        if(iTxn.equals("0112") || iTxn.equals("0114") || iTxn.equals("2201") || iTxn.equals("4525") || iTxn.equals("5183"))
            oTxn.setGStatus("5");

        if(iTxn.equals("0108") || iTxn.equals("0110") || iTxn.equals("0554") || iTxn.equals("0600"))
            oTxn.setGStatus("6");

		if(!Currency.equals("01"))
			oTxn.setToCurr(gc.getDivisa(Currency));	
		else
            oTxn.setToCurr(gc.getDivisa((String)session.getAttribute( "page.moneda_s" )));
		

        String txtAutoriCV = (String)datasession.get("txtAutoriCV");
        if ( txtAutoriCV == null )
            txtAutoriCV = "00000";
		
		if(!txtAutoriCV.equals("00000") && !txtAutoriCV.equals("") && iTxn.equals("2201"))
			oTxn.setGStatus( "5" );
			
		if(!txtAutoriCV.equals("00000") && !txtAutoriCV.equals("") && iTxn.equals("4525"))
				oTxn.setGStatus( "5" );

		if(!txtAutoriCV.equals("00000") && !txtAutoriCV.equals("") && iTxn.equals("5183"))
					oTxn.setGStatus( "3" );

        oTxn.setCheckNo( "00000" + txtAutoriCV );
        Monto = (String)datasession.get("txtMonto1");
        Monto = cGenericas.delCommaPointFromString(Monto);
        oTxn.setTranAmt( "000" );
        if(Long.parseLong(Monto) > 0)
            oTxn.setTranAmt( cGenericas.delCommaPointFromString((String)datasession.get( "txtMonto1" )) );
        String txtTipoCambioCV = (String)datasession.get( "txtTipoCambioCV" );
        if ( txtTipoCambioCV.equals("0.0000") )
            txtTipoCambioCV = "0";
        else
            txtTipoCambioCV = cGenericas.delCommaPointFromString(txtTipoCambioCV);
        oTxn.setComAmt( txtTipoCambioCV + "00" );
        oTxn.setIVAAmt( "0" );
        oTxn.setAcctNo( (String)datasession.get( "registro" ) );
        oTxn.setCtaBenef( (String)datasession.get( "txtRegistro" ) );

        Diario diario = new Diario();
        String sMessage = diario.invokeDiario(oTxn, (String)session.getAttribute("d_ConsecLiga"), dirip, gc.getDivisa((String)session.getAttribute( "page.moneda_s" )), session);

        if (sMessage.startsWith("0")  && ((TO.equals("4115") && !iTxn.equals("4043")) || (TO.equals("0786") && !iTxn.equals("4043")) || (TO.equals("0594") && !iTxn.equals("5159"))))
        {
            Monto = cGenericas.delCommaPointFromString((String)datasession.get("txtMonto1") + "00");
            long lMonto = Long.parseLong(Monto);
            String MontoUSD = cGenericas.delCommaPointFromString((String)datasession.get("txtMonto1"));
            long USDMonto = Long.parseLong(MontoUSD);
            Long NumeroC = new Long(0);
            int iTemp = 0;
            String Numero = new String("0.00");
            String txtMsg = new String("");

            ventanilla.com.bital.util.NSTokenizer parser = new ventanilla.com.bital.util.NSTokenizer(sMessage, "~");
            Vector cics_resp = new Vector();
            while( parser.hasMoreTokens() )
            {
                String token = parser.nextToken();
                if( token == null )
                    continue;
                cics_resp.addElement(token);
            }

            txtMsg = (String)cics_resp.elementAt(3);
            String tipoCambio = cGenericas.getString(txtMsg,7);
            String PlazaTP = cGenericas.getString(txtMsg,8);
            String MontoLimite = delLeadingfZeroes(cGenericas.getString(txtMsg,9));
            tipoCambio = delLeadingfZeroes(tipoCambio.substring(0, tipoCambio.length() - 5));

            if(lMonto > 0)
            {
                NumeroC = new Long(lMonto * Long.parseLong(tipoCambio) / 1000000);
                Monto = setFormat(setPointToString(cGenericas.delCommaPointFromString(NumeroC.toString())));
                datasession.put("txtMonto",Monto);
            }
            else
            {
                iTemp = 1;
                Monto = cGenericas.delCommaPointFromString((String)datasession.get("txtMonto"));
                NumeroC = new Long((Long.parseLong(Monto + "0000") / Long.parseLong(tipoCambio)));
                Monto = setFormat(setPointToString(cGenericas.delCommaPointFromString(NumeroC.toString())));
                datasession.put("txtMonto1",Monto);
            }

            Numero = setFormat(setPointToString(cGenericas.delCommaPointFromString(NumeroC.toString())));
            datasession.put("txtTipoCambio", tipoCambio);
            datasession.put("txtPlaza", PlazaTP);
            datasession.put("txtMontoLimite", MontoLimite);

            if ( Currency.equals("01"))
                datasession.put("txtMontoC", (String)datasession.get("txtMonto1"));
            else
                datasession.put("txtMontoC", (String)datasession.get("txtMonto"));
            session.setAttribute("page.datasession", (Hashtable)datasession);

            oTxn = new CompraVenta();
            oTxn.setBranch((String)datasession.get("sucursal"));
            oTxn.setTeller((String)datasession.get("teller"));
            oTxn.setSupervisor(txtSupervisor);
            String CurrCon = null;

            if ( TO.equals("4115"))
            {
                //ODCT 0810B 00001000130  000130  000**01*600000*1******N$**
                oTxn.setTxnCode("0810");//consulya comision e iva
                oTxn.setFromCurr("N$");
                CurrCon = "N$";
                Monto =  cGenericas.delCommaPointFromString((String)datasession.get("txtMonto"));
            }
            //else if( TO.equals("0786"))
            else if( TO.equals("0594"))
            {
                //ODCT 0814B 00001000130  000130  000**01*6000*1******US$**
                oTxn.setTxnCode("0814"); //consulta comision e iva
                oTxn.setFromCurr("US$");
                CurrCon = "US$";
                Monto =  cGenericas.delCommaPointFromString((String)datasession.get("txtMonto1"));
            }

            oTxn.setFormat("B");
            oTxn.setAcctNo(PlazaTP);
            oTxn.setTranAmt(Monto);
            oTxn.setReferenc("1");


            diario = new Diario();
            sMessage = diario.invokeDiario(oTxn, (String)session.getAttribute("d_ConsecLiga"), dirip, CurrCon, session);


            Resultado = oTxn.getStatus();

            if ( Resultado == 0 )
            {
                txtComision =  cGenericas.getString(oTxn.getMessage(),3);
                txtPorcIVA  =  cGenericas.getString(oTxn.getMessage(),4);
                txtDirSuc   =  cGenericas.getPositionalString(oTxn.getMessage(),91,32, "NO");
                txtComision = delLeadingfZeroes(txtComision);
                Long montoIVA = new Long((Long.parseLong(txtComision) * Long.parseLong(txtPorcIVA)) / 100);
                Long montoCOMeIVA = new Long(Long.parseLong(txtComision)  + montoIVA.longValue());
                if ( TO.equals("4115"))
                {
                    txtComisionMN = cGenericas.delCommaPointFromString(txtComision);
                    txtIvaMN      = cGenericas.delCommaPointFromString(montoIVA.toString());
                    NumeroC = new Long((Long.parseLong(txtComision + "0000") / Long.parseLong(tipoCambio)));
                    txtComisionUSD = setFormat(setPointToString(cGenericas.delCommaPointFromString(NumeroC.toString())));
                    datasession.put("txtComisionUSD",txtComisionUSD);
                    NumeroC = new Long((Long.parseLong(txtIvaMN + "0000") / Long.parseLong(tipoCambio)));
                    txtIvaUSD = setFormat(setPointToString(cGenericas.delCommaPointFromString(NumeroC.toString())));
                    datasession.put("txtIvaUSD",txtIvaUSD);
                    Monto = setFormat(setPointToString(txtComisionMN));
                    datasession.put("txtComisionMN",Monto);
                    datasession.put("txtComision",Monto);   // Para comprobante de cobro comision
                    Monto = setFormat(setPointToString(txtIvaMN));
                    datasession.put("txtIvaMN",Monto);
                    datasession.put("txtIVA",Monto);         // Para comprobante de cobro comision
                }
                else
                {
                    txtComisionUSD = cGenericas.delCommaPointFromString(txtComision);
                    txtIvaUSD      = cGenericas.delCommaPointFromString(montoIVA.toString());
                    Monto = cGenericas.delCommaPointFromString(txtComisionUSD + "00");
                    lMonto = Long.parseLong(Monto);
                    NumeroC = new Long(lMonto * Long.parseLong(tipoCambio) / 1000000);
                    txtComisionMN = setFormat(setPointToString(cGenericas.delCommaPointFromString(NumeroC.toString())));
                    datasession.put("txtComisionMN",txtComisionMN);
                    datasession.put("txtComision",txtComisionMN);
                    datasession.put("txtConcepto","EXP. ORDEN DE PAGO");
                    Monto = cGenericas.delCommaPointFromString(txtIvaUSD + "00");
                    lMonto = Long.parseLong(Monto);
                    NumeroC = new Long(lMonto * Long.parseLong(tipoCambio) / 1000000);
                    txtIvaMN = setFormat(setPointToString(cGenericas.delCommaPointFromString(NumeroC.toString())));
                    datasession.put("txtIvaMN",txtIvaMN);
                    datasession.put("txtIVA",txtIvaMN);
                    Monto = setFormat(setPointToString(txtComisionUSD));
                    datasession.put("txtComisionUSD",Monto);
                    Monto = setFormat(setPointToString(txtIvaUSD));
                    datasession.put("txtIvaUSD",Monto);
                }
                txtComisionUSD = cGenericas.delCommaPointFromString(txtComisionUSD);
                txtIvaUSD      = cGenericas.delCommaPointFromString(txtIvaUSD);
                txtComisionMN = cGenericas.delCommaPointFromString(txtComisionMN);
                txtIvaMN      = cGenericas.delCommaPointFromString(txtIvaMN);
                Monto = cGenericas.delCommaPointFromString((String)datasession.get("txtMonto"));
                NumeroC = new Long(Long.parseLong(Monto)+ Long.parseLong(txtComisionMN)  + Long.parseLong(txtIvaMN));
                Monto = setFormat(setPointToString(cGenericas.delCommaPointFromString(NumeroC.toString())));
                datasession.put("txtTotalMN",Monto);
                Monto = Monto =  cGenericas.delCommaPointFromString((String)datasession.get("txtMonto1"));
                NumeroC = new Long(Long.parseLong(Monto)+ Long.parseLong(txtComisionUSD)  + Long.parseLong(txtIvaUSD));
                Monto = setFormat(setPointToString(cGenericas.delCommaPointFromString(NumeroC.toString())));
                datasession.put("txtTotalUSD",Monto);
                datasession.put("txtDomEsta",txtDirSuc);
                session.setAttribute("page.datasession", (Hashtable)datasession);
            }
        }

        if(txtCasoEsp.length() > 0){
            session.setAttribute("txtCasoEsp", txtCasoEsp);
        }
        session.setAttribute("txtCadImpresion", sMessage);

        if(txtCadImpresion.length() > 0) {
            session.setAttribute("txtCadImpresion", txtCadImpresion);
        }

        session.setAttribute("page.txnresponse", sMessage);
        getServletContext().getRequestDispatcher("/servlet/ventanilla.Redirector").forward(req,resp);
    }

    public void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doPost(req, resp);
    }
}
