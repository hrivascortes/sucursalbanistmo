//*************************************************************************************************
//             Funcion: Clase que realiza txn 1019
//            Elemento: Group02NOM.java
//          Creado por: Alejandro Gonzalez Castro
//      Modificado por: Alejandro Gonzalez Castro
//*************************************************************************************************
// CCN - 4360189 - 03/09/2004 - Se cambia invoke al Diario para Control de Efectivo
//*************************************************************************************************
package ventanilla;

import java.io.IOException;
import java.util.Hashtable;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import ventanilla.com.bital.admin.Diario;
import ventanilla.com.bital.sfb.Nomina;

public class Group02NOM extends HttpServlet
{
  public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
  {
    response.setContentType("text/plain");
    HttpSession session = request.getSession(false);
    Hashtable datasession = (Hashtable)session.getAttribute("page.datasession");
	GenericClasses gc = new GenericClasses();

	//Variables para diario electronico
    String dirip =  (String)session.getAttribute("dirip");
	String msg = "";

    Nomina nomina = new Nomina();

    nomina.setBranch((String)datasession.get("sucursal"));
    nomina.setTeller((String)datasession.get("teller"));
    nomina.setSupervisor((String)datasession.get("supervisor"));

	String txn = (String)datasession.get("cTxn");
	nomina.setTxnCode(txn);

    nomina.setAcctNo((String)datasession.get("txtDDA"));

    StringBuffer monto = new StringBuffer((String)datasession.get("txtMontoA") );
    for(int i=0; i<monto.length();)
    {
      if( monto.charAt(i) == ',' || monto.charAt(i) == '.' )
        monto.deleteCharAt(i);
      else
        ++i;
    }
	nomina.setTranAmt(monto.toString());

    String moneda = (String)datasession.get("moneda");
	nomina.setTranCur(gc.getDivisa(moneda));
	nomina.setMoamoun("000");

	if((String)datasession.get("txtFechaEfect") != null )
	{
		StringBuffer fechatmp = new StringBuffer((String)datasession.get("txtFechaEfect"));
		for(int i=0; i<fechatmp.length();)
		{
			if( fechatmp.charAt(i) == '/' || fechatmp.charAt(i) == '-' )
				fechatmp.deleteCharAt(i);
		    else
				++i;
		}

		String fechacap = fechatmp.substring(4,fechatmp.length()) + fechatmp.substring(2,4) + fechatmp.substring(0,2);

		String fechasys = (String)datasession.get("txtFechaSys");

		if (!fechasys.equals(fechacap))
		{
			String fecha = fechacap.substring(4,6) + fechacap.substring(6,fechacap.length()) + fechacap.substring(2,4);
			nomina.setEffDate(fecha);
		}
	}

     Diario diario = new Diario();
     String sMessage = diario.invokeDiario(nomina,(String)session.getAttribute("d_ConsecLiga"), dirip, moneda, session);

     session.setAttribute("page.txnresponse", sMessage);
     getServletContext().getRequestDispatcher("/servlet/ventanilla.Redirector").forward(request,response);
  }

  public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
  {
    doPost(request, response);
  }
}
