//*************************************************************************************************
//             Funcion: Clase que realiza txn 0726
//            Elemento: CV_C726_XXXX.java
//          Creado por: Alejandro Gonzalez Castro
//		Modificado por: Fredy Pe�a Moreno
//*************************************************************************************************
// CCN - 4360189 - 03/09/2004 - Se cambia invoke al Diario para Control de Efectivo
// CCN - 4360268 - 04/02/2005 - Se realizan adecuaciones para eliminar Agencia NY
// CCN - 4360368 - 02/09/2005 - Se elimina sleep
// CCN - 4360374 - 08/09/2005 - Se genera nuevo paquete a peticion de QA.
// CCN - 4360532 - 20/10/2006 - Se realizan modificaciones para enviar el numero de cajero, consecutivo txn entrada y salida en txn 0821
// CCN - 4620048 - 14/12/2007 - Se realizan modificaciones para compraventa
//*************************************************************************************************
package ventanilla; 

import java.io.IOException;
import java.util.Hashtable;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import ventanilla.com.bital.admin.Diario;
import ventanilla.com.bital.sfb.CompraVenta;

public class CV_C726_XXXX extends HttpServlet {

    public void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException 
    {
        String TE, TO, Currency, Currency2,Divisa,Divisa2;
        int Resultado = -1;
        
        CompraVenta oTxn = new CompraVenta();
        Diario diario;
        String sMessage = null;
        GenericClasses cGenericas = new GenericClasses();
        
        HttpSession session = req.getSession(false);
        Hashtable datasession = (Hashtable)session.getAttribute("page.datasession");
        TE = (String)datasession.get("cTxn");
        TO = (String)datasession.get("oTxn");
        Currency = (String)datasession.get("moneda");
        Currency2 = (String)session.getAttribute("page.moneda_s");
        String txtPlaza = (String)datasession.get("txtPlaza");
        String msg = null;
        String dirip =  (String)session.getAttribute("dirip");
        String txtCasoEsp = new String("");
        String txtCadImpresion = new String("");
        String txtNomCliente = new String("");
        String txtSupervisor = (String)datasession.get("teller");
        String txtRegistro = (String)datasession.get("txtRegistro");
        String Monto = "";
        String ConsecutivoIN = "";
        String ConsecutivoOUT = "";
		Divisa = cGenericas.getDivisa(Currency);
		Divisa2 = cGenericas.getDivisa(Currency2);        
       /* if ( Currency.equals("01"))
            Currency2 = "02";
        else
            Currency2 = "01";*/
        
        if ( (String)datasession.get("supervisor") != null )       //pidio autorizacisn de supervisor
            txtSupervisor =  (String)datasession.get("supervisor");
        
        oTxn.setTxnCode(TE);
        oTxn.setBranch((String)datasession.get("sucursal"));
        oTxn.setTeller((String)datasession.get("teller"));
        oTxn.setSupervisor(txtSupervisor);
       
        if( TE.equals( "C726" ) ) {
            //ODCT 0726B 00001000130  000130  000**0726*300000*1****300000**US$**
            oTxn.setFormat( "B" );
            oTxn.setAcctNo( "0726" );
            oTxn.setTxnCode("0726");
            if(Currency.equals("01"))
            {
                oTxn.setTranAmt(cGenericas.delCommaPointFromString((String)datasession.get( "txtMonto" )));
                oTxn.setCashIn(cGenericas.delCommaPointFromString((String)datasession.get( "txtMonto" )));
            }
            else
            {
                oTxn.setTranAmt(cGenericas.delCommaPointFromString((String)datasession.get( "txtMonto1" )));
                oTxn.setCashIn(cGenericas.delCommaPointFromString((String)datasession.get( "txtMonto1" )));
            }    
            oTxn.setFromCurr(cGenericas.getDivisa(Currency));
            oTxn.setReferenc("1");
            
            diario = new Diario();
            sMessage = diario.invokeDiario(oTxn, (String)session.getAttribute("d_ConsecLiga"), dirip, Currency, session);
            
            Resultado = oTxn.getStatus();
            Monto = cGenericas.delCommaPointFromString((String)datasession.get("txtMonto1"));
            
            if(sMessage.startsWith("0")) {
                txtCasoEsp = txtCasoEsp + "0726" + "~" + cGenericas.getString(oTxn.getMessage(),1) + "~";
                ConsecutivoIN = cGenericas.getString(oTxn.getMessage(),1);
                if (ConsecutivoIN.length()<6){
                   	ConsecutivoIN = "0" + ConsecutivoIN; 
                }                
            }
        }
        if ( TE.equals("C726") && Resultado == 0 ) {

            oTxn = new CompraVenta();
            oTxn.setBranch((String)datasession.get("sucursal"));
            oTxn.setTeller((String)datasession.get("teller"));
            oTxn.setSupervisor(txtSupervisor);
            if( TO.equals("0106")) {
                //ODCT 0106B 00001000130  000130  000**0106*64500*1*****64500*N$**
                oTxn.setTxnCode("0106");
                oTxn.setFormat("B");
                oTxn.setAcctNo( "0106" );
                oTxn.setTranAmt( cGenericas.delCommaPointFromString((String)datasession.get( "txtMontoC" )) );
                oTxn.setCashOut( cGenericas.delCommaPointFromString((String)datasession.get( "txtMontoC" )) );
               /* if(Currency.equals("01"))
                {
                    oTxn.setFromCurr("US$");
                }
                else
                {
                    oTxn.setFromCurr("N$");
                }    */
                oTxn.setFromCurr(cGenericas.getDivisa(Currency2)); 
                oTxn.setReferenc("1");
            }
            
            diario = new Diario();
            sMessage = diario.invokeDiario(oTxn, (String)session.getAttribute("d_ConsecLiga"), dirip, Currency2, session);
            
            Resultado = oTxn.getStatus();
            Monto = cGenericas.delCommaPointFromString((String)datasession.get("txtMonto"));
            
            if(sMessage.startsWith("0")) {
                txtCasoEsp = txtCasoEsp + TO + "~" + cGenericas.getString(oTxn.getMessage(),1) + "~";
                ConsecutivoOUT = cGenericas.getString(oTxn.getMessage(),1);
                if (ConsecutivoOUT.length()<6){
                   	ConsecutivoOUT = "0" + ConsecutivoOUT; 
                }                
            }
        }
        if (  TE.equals("C726") && Resultado == 0) {   // Cargo y Abono Aceptado

            oTxn = new CompraVenta();
            oTxn.setBranch((String)datasession.get("sucursal"));
            oTxn.setTeller((String)datasession.get("teller"));
            oTxn.setSupervisor(txtSupervisor);
            oTxn.setTxnCode("0821");
            oTxn.setFormat( "G" );
            String monedacv="";
            if(Currency.equals("01")){
            	monedacv= Currency2;            
            }else{
            	monedacv=Currency;
            }
//            oTxn.setToCurr( "US$" );
            oTxn.setToCurr(cGenericas.getDivisa(monedacv));
            
            if(Currency.equals("01"))
            {
                oTxn.setService( "V" );
            }
            else
            {
                oTxn.setService( "C" );
            }    
            oTxn.setGStatus( "1" );
            String txtAutoriCV = (String)datasession.get("txtAutoriCV");
            if ( txtAutoriCV == null )
                txtAutoriCV = "00000";
            oTxn.setCheckNo( "00000" + txtAutoriCV );
            oTxn.setTranAmt( cGenericas.delCommaPointFromString((String)datasession.get( "txtMonto1" )) );
            String txtTipoCambio = cGenericas.StringFiller((String)datasession.get( "txtTipoCambio" ), ((String)datasession.get( "txtTipoCambio" )).length()+2, false, "0")+ "00";
            txtTipoCambio = "0000" + txtTipoCambio;
            txtTipoCambio = txtTipoCambio.substring(txtTipoCambio.length()-9);
            oTxn.setComAmt(txtTipoCambio);
            //oTxn.setIVAAmt( "0" );
            if(Currency.equals("01"))
            {
            	oTxn.setIVAAmt(cGenericas.delCommaPointFromString((String)datasession.get("txtMonto")));
            }
            else
            {
            	oTxn.setIVAAmt(cGenericas.delCommaPointFromString((String)datasession.get("txtMonto")));
            }
            oTxn.setAcctNo( (String)datasession.get( "registro" ) );
            oTxn.setCtaBenef( (String)datasession.get( "txtRegistro" ) );
            oTxn.setBenefic("07260106  " + ConsecutivoIN + ConsecutivoOUT + (String)datasession.get("teller"));
            
            diario = new Diario();
            sMessage = diario.invokeDiario(oTxn, (String)session.getAttribute("d_ConsecLiga"), dirip, monedacv, session);
            
            Monto = cGenericas.delCommaPointFromString((String)datasession.get("txtMonto"));
            
            if(sMessage.startsWith("0")) {
                txtCasoEsp = txtCasoEsp + "0821" + "~" + cGenericas.getString(oTxn.getMessage(),1) + "~";
                String txtMonto = cGenericas.delCommaPointFromString((String)datasession.get( "txtMonto" ));
                String txtMonto1 = cGenericas.delCommaPointFromString((String)datasession.get( "txtMonto1" ));
                txtTipoCambio = cGenericas.StringFiller((String)datasession.get( "txtTipoCambio" ), ((String)datasession.get( "txtTipoCambio" )).length()+2, false, "0");
                txtTipoCambio = "0000" + txtTipoCambio;
                txtTipoCambio = txtTipoCambio.substring(txtTipoCambio.length()-7);
                txtTipoCambio = txtTipoCambio.substring(0,3) + "." + txtTipoCambio.substring(3,6);
                String txtDDACuenta1 = "";
                String txtDDACuenta2 = "";
                String txtSerial = (String)datasession.get( "txtSerial" );
                String txtBeneficiario = (String)datasession.get( "txtBeneficiario" );
                txtNomCliente = (String)datasession.get( "txtCliente" );
                txtCadImpresion = txtCadImpresion + "~COMPRAVENTA~" + "0726" +"-" + TO +"~" + txtNomCliente + "~" +
                txtMonto + "~" + txtTipoCambio + "~" + txtMonto1 + "~" + txtSerial + "~" +
                txtDDACuenta1 + "~" + txtDDACuenta2 + "~" + "" + "~" + txtBeneficiario + "~" +
                "" + "~" + "" + "~" + "" + "~" + Currency + "~" + Divisa + "~" + Divisa2 + "~";
            }
        }
        if(txtCasoEsp.length() > 0){
            session.setAttribute("txtCasoEsp", txtCasoEsp);
        }
        session.setAttribute("txtCadImpresion", sMessage);
        if(txtCadImpresion.length() > 0){
            session.setAttribute("txtCadImpresion", txtCadImpresion);
        }
        session.setAttribute("page.txnresponse", sMessage);
        getServletContext().getRequestDispatcher("/servlet/ventanilla.Redirector").forward(req,resp);
    }
    
    public void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doPost(req, resp);
    }
}
