//*************************************************************************************************
//			   Funcion: Redirector de Txns
//			  Elemento: Redirector.java
//			Creado por: Alejandro Gonzalez Castro
//		Modificado por: Juvenal R. Fernandez V.
//*************************************************************************************************
// CCN - 4360175 - 03/08/2004 - Se habilita deposito parcial descontando monto de chqs rechazados
// CCN - 4360334 - 17/06/2005 - Se modifica redireccion a relativa para WAS5
//*************************************************************************************************
package ventanilla;

import java.io.IOException;
import java.util.Hashtable;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class Redirector extends HttpServlet {
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			HttpSession session = request.getSession(false);
			Hashtable datasession = (Hashtable)session.getAttribute("page.datasession");
            
           
			java.util.Vector cics_resp = new java.util.Vector();
			StringBuffer respuestaSB = new StringBuffer((String)session.getAttribute("page.txnresponse"));
            
			if(session.getAttribute("txtCasoEsp") != null){
				session.setAttribute("txtCasoEsp",session.getAttribute("txtCasoEsp"));
				getServletContext().removeAttribute("txtCasoEsp");
			}
			if(session.getAttribute("txtCadImpresion") != null){
				session.setAttribute("txtCadImpresion",(String)session.getAttribute("txtCadImpresion"));
				getServletContext().removeAttribute("txtCadImpresion");
			}
			String respuesta = (String)session.getAttribute("page.txnresponse");
            
			if( respuestaSB.charAt(0) == 'O' )
				respuesta = "1~01~010101~ERROR GENERAL DE COMUNICACIONES~";
			ventanilla.com.bital.util.NSTokenizer parser = new ventanilla.com.bital.util.NSTokenizer(respuesta, "~");
			while( parser.hasMoreTokens() ) {
				String token = parser.nextToken();
				if( token == null )
					continue;
                
				cics_resp.addElement(token);
			}
            
			session.setAttribute("response", cics_resp);
			String codigo = (String)cics_resp.elementAt(0);
            
			String needOvrr = "NO";
			if (datasession.get("override") != null)
				needOvrr = datasession.get("override").toString();
            
			String iTxn = (String)datasession.get("iTxn");
			String oTxn = (String)datasession.get("oTxn");
			String cTxn = (String)datasession.get("cTxn"); 
			String pcTxn =  (String)session.getAttribute("page.cTxn");
           
			if( !codigo.equals("0") || needOvrr.startsWith("SI")) 
			{
				java.util.Stack flujotxn = (java.util.Stack)session.getAttribute("page.flujotxn");
				if ( needOvrr.equals("SI"))
				{
					needOvrr = "NO";
				}
				else 
				{
					if((!iTxn.equals("1003") && !cTxn.equals("5353")) &&
					   (!iTxn.equals("1001") && !cTxn.equals("5353")) &&
					   (!iTxn.equals("1009") && !cTxn.equals("1009")))
					{    
						if ( !flujotxn.empty() && !codigo.equals("3")) 
						{
							flujotxn.removeAllElements();
						}
					}
				}
				session.setAttribute("page.flujotxn",flujotxn);
			}
            
			datasession.put("override",needOvrr);
			session.setAttribute("page.datasession",datasession);
            
			String Response = (String)session.getAttribute("page.getoTxnView");
           
			if( Response == null )
				Response = new String("Response");
			else if(Response.equals(""))
				Response = new String("Response");
            
                
			if(pcTxn != null && pcTxn.equals("CSIB"))
				Response = Response+"SIB";
            	
			if(pcTxn != null && pcTxn.equals("1003")){
            
            	String paso=(String)session.getAttribute("pasoP001");
            	int resp= Integer.parseInt(respuesta.substring(0,respuesta.indexOf("~")));
            	if(paso!=null && paso.equals("SI") && resp==0){
            	
					String Cad="ventanilla.GroupPVConsulta";
					response.sendRedirect(Cad);
				}
				else 
				response.sendRedirect( "../ventanilla/paginas/" + Response + ".jsp");
			}	           
			else
            
			response.sendRedirect( "../ventanilla/paginas/" + Response + ".jsp");
            
		}
		catch(Exception e) {
			e.printStackTrace(System.out);
		}
	}
    
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}
}
