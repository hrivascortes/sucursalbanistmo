//*************************************************************************************************
//             Funcion: Clase que realiza txn Servicios
//            Elemento: Group12Serv.java
//          Creado por: Alejandro Gonzalez Castro
//      Modificado por: Alejandro Gonzalez Castro
//*************************************************************************************************
// CCN - 4360189 - 03/09/2004 - Se cambia invoke al Diario para Control de Efectivo
//*************************************************************************************************
package ventanilla;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.util.Hashtable;

import ventanilla.com.bital.sfb.ServiciosA;
import ventanilla.com.bital.sfb.ServiciosB;
import ventanilla.com.bital.admin.Diario;

public class Group12Serv extends HttpServlet
{
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        HttpSession session = request.getSession(false);
        Hashtable datasession = (Hashtable)session.getAttribute("page.datasession");
        String txn = (String)datasession.get("cTxn");
		GenericClasses gc = new GenericClasses();
        String moneda = gc.getDivisa((String)datasession.get("moneda"));
        String[] invoqued = request.getParameterValues("txtInvoqued");
        String invTxn = "";
        if(invoqued != null)
            invTxn = (String)invoqued[0];
        String dirip =  (String)session.getAttribute("dirip");
        String msg = "";
        
        String tipdrepo = "";
        String signo = "";
        String txtPrint = "";
        
        // CORRECION ALEJGOCA - 17/09/2002
        if (txn.equals("90AF"))
        {
            txn = "0858";
            ServiciosB serviciob = new ServiciosB();
            serviciob.setBranch((String)datasession.get("sucursal"));
            serviciob.setTeller((String)datasession.get("teller"));
            serviciob.setSupervisor((String)datasession.get("teller"));
            serviciob.setFormat("B");
            serviciob.setTxnCode(txn);
            serviciob.setAcctNo((String)datasession.get("txtCtaAfore"));            
            serviciob.setTranAmt("000");
            serviciob.setReferenc((String)datasession.get("txttarjact")+ (String)datasession.get("pssnip"));
            serviciob.setCashIn("000");
            serviciob.setFromCurr(moneda);

            Diario diario = new Diario();
            String sMessage = diario.invokeDiario(serviciob, (String)session.getAttribute("d_ConsecLiga"), dirip, moneda, session);

            session.setAttribute("page.txnresponse", sMessage);
            getServletContext().getRequestDispatcher("/servlet/ventanilla.Redirector").forward(request,response);
        }
        else
        {
            ServiciosA servicioA = new ServiciosA();
            servicioA.setBranch((String)datasession.get("sucursal"));
            servicioA.setTeller((String)datasession.get("teller"));
            servicioA.setSupervisor((String)datasession.get("teller"));
            servicioA.setFormat("A");
            servicioA.setAcctNo((String)datasession.get("txtDDACuenta"));
            signo = moneda;
            
            String tarjant = "0000000000000000";
            String tarjact = (String)datasession.get("txttarjact");
            String nip = (String)datasession.get("pssnip");
            servicioA.setCheckNo((String)session.getAttribute("empno"));
            
            if (txn.equals("90RO")) {
                txn = "1543";
                servicioA.setTranAmt("100");
                servicioA.setTranCur(signo);
                tarjant = (String)datasession.get("txttarjant");
                tipdrepo = "BR";
            }
            else if (txn.equals("90CO")) {
                txn = "1543";
                servicioA.setTranAmt("100");
                servicioA.setTranCur(signo);
                tipdrepo = "BN";
            }
            else if (txn.equals("90AD")) {
                txn = "1543";
                servicioA.setTranAmt("100");
                servicioA.setTranCur(signo);
                tipdrepo = "BA";
            }
            else if (txn.equals("90NI")) {
                txn = "0090";
                tipdrepo = "RN";
            }
            else if (txn.equals("90CA")) {
                txn = "0090";
                tipdrepo = "C";
                nip	= "0000";
                servicioA.setTrNo("1");
                servicioA.setTranCur(signo);
            }
            
            servicioA.setTxnCode(txn);
            servicioA.setMoamoun("000");
            servicioA.setTranDesc(tarjant + tarjact + nip + tipdrepo);
            
            String tem = (String)datasession.get("cTxn");
            
            if(tem.equals("90CA"))
                servicioA.setTranCur("");

            Diario diario = new Diario();
            String sMessage = diario.invokeDiario(servicioA, (String)session.getAttribute("d_ConsecLiga"), dirip, moneda, session);
       
            session.setAttribute("page.txnresponse", sMessage);
            //if(tem.equals("90CA"))
            //{
                txtPrint = txn + "~" + getString(servicioA.getMessage(),1) + "~";
                session.setAttribute("txtCasoEsp", txtPrint);
            //}
            getServletContext().getRequestDispatcher("/servlet/ventanilla.Redirector").forward(request,response);
        }
        
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
    public String getmoneda(String cmoneda) {
        String newmoneda = "";
        if (cmoneda.equals("N$"))
        { newmoneda = "01";}
        else if (cmoneda.equals("US$"))
        { newmoneda = "02";}
        else
        { newmoneda = "03";}
        return newmoneda;
    }
    
   
    public String removeAsterisk(String campo) {
        StringBuffer result = new StringBuffer(campo);
        String caracter = result.substring(result.length()-1,result.length());
        if (caracter.equals("*"))
            result.deleteCharAt(result.length()-1);
        return result.toString();
    }
  private String getString(String newCadNum, int NumSaltos)
  {
   int iPIndex = newCadNum.indexOf("CONSECUTIVO:") + 12, Num = 0, Ban = 0;
   String nCadNum = new String("");
   while(Num < NumSaltos)
   {
    if(newCadNum.charAt(iPIndex) == 0x20)
    {
     while(newCadNum.charAt(iPIndex) == 0x20)
      iPIndex++;
     Num++;
    }
    else
     while(newCadNum.charAt(iPIndex) != 0x20)
      iPIndex++;
   }
   while(newCadNum.charAt(iPIndex) != 0x20)
   {
    nCadNum = nCadNum + newCadNum.charAt(iPIndex++);
    if(newCadNum.charAt(iPIndex) == 0x20 && newCadNum.charAt(iPIndex + 1) == 0x2A)
    {
     nCadNum = nCadNum + newCadNum.charAt(iPIndex) + newCadNum.charAt(iPIndex + 1);
     iPIndex += 2;
    }
   }

   return nCadNum;
  }
    
    
}