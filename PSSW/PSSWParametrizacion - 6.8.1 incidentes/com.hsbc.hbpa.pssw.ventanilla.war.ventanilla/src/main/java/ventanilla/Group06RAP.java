//*************************************************************************************************
//             Funcion: Clase que realiza txn 5361
//            Elemento: Group06RAP.java
//          Creado por: Israel De Paz Mercado
//*************************************************************************************************
// CCN - 4360455 - 07/04/2006 - Se realizan modificaciones para el esquema de cheques devueltos.
// CCN - 4360462 - 10/04/2006 - Se genera nuevo paquete por errores en paquete anterior
// CCN - 4360474 - 05/05/2006 - Se realizan modificaciones para mostrar mensaje de espera cuando se procensan devoluciones
//*************************************************************************************************
package ventanilla;

import java.io.IOException;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Stack;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import ventanilla.com.bital.admin.Diario;
import ventanilla.com.bital.sfb.FormatoA;

public class Group06RAP extends HttpServlet
{
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        response.setContentType("text/plain");
        HttpSession session = request.getSession(false);
        Hashtable datasession = (Hashtable)session.getAttribute("page.datasession");
      
        Hashtable chequesdevueltos = (Hashtable)session.getAttribute("ChequesDevueltos");
        
        if(chequesdevueltos.isEmpty())
        {
           Stack flujotxn = (Stack)session.getAttribute("page.flujotxn");
	       if (flujotxn != null){
	    	  if(!flujotxn.empty()){
	    		  flujotxn.removeAllElements();
		          session.setAttribute("page.flujotxn", flujotxn);  
	    	  }	          
	        } 	        	
        	session.removeAttribute("txn5361");
        	session.removeAttribute("numServ5361");
        	session.removeAttribute("");
        	response.sendRedirect( "../ventanilla/paginas/Final.jsp");
        }else
        {	

		   ventanilla.GenericClasses gc = new ventanilla.GenericClasses();
           String conteo = (String)session.getAttribute("conteoChqDev");
           conteo = String.valueOf(Integer.parseInt(conteo) + 1 );
           Enumeration Keys = (Enumeration)chequesdevueltos.keys();
           String Key = Keys.nextElement().toString();
           String Cuenta = (String)datasession.get("CuentaRAP");
		   String Serial = Key.substring(11,Key.length());
		   Serial = gc.rellenaZeros(Serial,7);
           String valor = (String)chequesdevueltos.get(Key);
           int pos = valor.indexOf("*");
           String monto = valor.substring(0,pos); 
           monto = gc.delCommaPointFromString(monto);
           String rechazo = valor.substring(pos+1,valor.length());              
           Stack flujotxn = (Stack)session.getAttribute("page.flujotxn");
           String moneda = (String)datasession.get("moneda");
           String Serv = (String)datasession.get("servicio1");
           
           
      
           FormatoA fa = new FormatoA();
        
           fa.setBranch((String)datasession.get("sucursal"));
           fa.setTeller((String)datasession.get("teller"));
           fa.setSupervisor((String)datasession.get("supervisor"));
           fa.setTxnCode("5361");
           fa.setFormat("A");
           
           fa.setAcctNo((String)Cuenta);
           fa.setTranAmt(monto);
		   fa.setTranCur(gc.getDivisa(moneda));
           fa.setCheckNo((String)Serial);	
           fa.setMoAmoun("000");
           fa.setTrNo("021"+rechazo); 
           fa.setCashOut(monto);
           
           String ref1 = "";           
           String ref2 = "";
           String ref3 = "";
           
           if((String)datasession.get("ref1s1p1")!=null)
              ref1=(String)datasession.get("ref1s1p1");
           if((String)datasession.get("ref2s1p1")!=null)
              ref2=(String)datasession.get("ref2s1p1");
           if((String)datasession.get("ref3s1p1")!=null)
             ref3=(String)datasession.get("ref3s1p1");

           String ref  = "";
                ref =ref1+ref2+ref3;    
  
           fa.setTrNo1(Serv);
           fa.setTranDesc(ref);
           String dirip = (String)session.getAttribute("dirip");
           try{//delay para evitar txns duplicada
		   	 Thread.sleep(1500);
		   }catch (InterruptedException e){
		   }
           Diario diario = new Diario();
           String sMessage = diario.invokeDiario(fa, (String)session.getAttribute("d_ConsecLiga"), dirip, moneda, session);
           chequesdevueltos.remove(Key);
           
           session.setAttribute("conteoChqDev",conteo);
           session.setAttribute("page.txnresponse", sMessage);
  
        getServletContext().getRequestDispatcher("/servlet/ventanilla.Redirector").forward(request,response);
        }   
    }  
    
    
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        doPost(request, response);
    }
}
