//*************************************************************************************************
//             Funcion: Clase que realiza txn 0854 - 5565- 5561 - 5563
//            Elemento: Group01TIP.java
//          Creado por: Alejandro Gonzalez Castro
//		Modificado por: Juvenal Fernandez
//*************************************************************************************************
// CCN - 4360189 - 03/09/2004 - Se cambia invoke al Diario para Control de Efectivo
// CCN - 4360253 - 07/01/2005 - Enviar numero de identificacion en lugar del registro de empleado, 
//				dato que se graba en DHQR opcion 23 - 1
//CCN - 4360268 - 04/02/2005 - Se realizan adecuaciones para eliminar Agencia NY
//*************************************************************************************************
package ventanilla;

import java.io.IOException;
import java.util.Hashtable;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import ventanilla.com.bital.admin.Diario;
import ventanilla.com.bital.sfb.DAPG;
import ventanilla.com.bital.sfb.FormatoA;

public class Group01TIP extends HttpServlet
{
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        response.setContentType("text/plain");
        HttpSession session = request.getSession(false);
        GenericClasses GC = new GenericClasses();
        Hashtable datasession = (Hashtable)session.getAttribute("page.datasession");
        
        String dirip = (String)session.getAttribute("dirip");
        String txtMoneda = (String)datasession.get("moneda");
        String txtCadImpresion = "";

        String mensaje = "";
        String sMessage = "";
        String txtTxnCode    = (String)datasession.get("cTxn");
        String txtFormat     = "A";
        String txtBranch     = (String)datasession.get("sucursal");
        String txtTeller     = (String)datasession.get("teller");
        String txtSupervisor = (String)datasession.get("teller");
        String txtBackOut    = "0";
        String txtOverride   = "0";
        String txtCurrentDay = "0";

        if ( (String)datasession.get("supervisor") != null )
            txtSupervisor =  (String)datasession.get("supervisor");

        String txn = (String)datasession.get("cTxn");     //TIP
        String itxn = (String)datasession.get("iTxn");    // TIP
        StringBuffer	monto = new StringBuffer("000");        //TIP
        String txtDescrip = "";
        String DescRev = "";

        if ((String)datasession.get("txtMonto")!= null )
            monto = new StringBuffer((String)datasession.get("txtMonto"));

        for(int i=0; i<monto.length();) {
            if( monto.charAt(i) == ',' || monto.charAt(i) == '.' )
                monto.deleteCharAt(i);
            else
                ++i;
        }

        String mto = monto.toString();
        DAPG dapg = new DAPG();
        FormatoA dapa = new FormatoA();

        if (txn.equals("0854"))   // Txn de Consulta 0854
        {
            dapg.setBranch(txtBranch);
            dapg.setTeller(txtTeller);
            dapg.setSupervisor(txtSupervisor);
            dapg.setFormat("G");
            dapg.setTxnCode("0854");
            
			dapg.setToCurr(GC.getDivisa(txtMoneda));
            
            dapg.setTranAmt("000");
            dapg.setAcctNo(datasession.get("txtReferTIP").toString().substring(0,4));
            dapg.setBenefic(datasession.get("txtReferTIP").toString().substring(4));
            /*datasession.put("txtServicio",datasession.get("txtReferTIP").toString().substring(0,4));
            datasession.put("txtRefer",datasession.get("txtReferTIP").toString().substring(4));
            session.setAttribute("page.datasession", (Hashtable)datasession);*/
        
            Diario diario = new Diario();
            sMessage = diario.invokeDiario(dapg, (String)session.getAttribute("d_ConsecLiga"), dirip, txtMoneda, session);

            String code = sMessage.substring(0,1);
            mensaje = sMessage;

            if(code.equals("0")) {
                StringBuffer tmp = new StringBuffer(mensaje.substring(249,265));
                for(int i=0; i<tmp.length();) {
                    if(tmp.charAt(i) == '0')
                        tmp.deleteCharAt(i);
                    else
                        break;
                }
                String tem = tmp.toString();
                tem = tem.trim();
                
                String nCadNum = new String(tem.substring(0, tem.length()-2));
                String cents = "." + tem.substring(tem.length()-2, tem.length());
                
                for (int i = 0; i < (int)((nCadNum.length()-(1+i))/3); i++)
                    nCadNum = nCadNum.substring(0, nCadNum.length()-(4*i+3))+','+nCadNum.substring(nCadNum.length()-(4*i+3));
                
                tem = nCadNum + cents;
                
                String txtOrdenante = mensaje.substring(325,365);
                String txtBenefic = mensaje.substring(403,443);
                String txtNombre = mensaje.substring(481,511);
                char status = mensaje.charAt(512);
                char movimiento = mensaje.charAt(513);
                if ((( itxn.equals("5561") || itxn.equals("5565")) && ( status != '1' || movimiento != '1')) ||
                     ( itxn.equals("5567") && ( status != '2' || movimiento != '2'))) 
                     sMessage = "1~01~0101010~REFERENCIA CON ESTATUS INVALIDO";
                else  {                                      // Coloca los datos en la sesion
                   datasession.put("txtServicio",datasession.get("txtReferTIP").toString().substring(0,4));
                   datasession.put("txtRefer",datasession.get("txtReferTIP").toString().substring(4)); 
                   datasession.put("txtMonto",tem);
                   datasession.put("txtBeneficiario", txtBenefic);
                   datasession.put("txtOrdenante", txtOrdenante);
                   datasession.put("txtNombre", txtNombre);
                   session.setAttribute("page.datasession", (Hashtable)datasession);
                }
            }
        }

        if( txn.equals("5565") || txn.equals("5561")) {                     // Liquidacisn en efectivo o abono a cuenta
            if ( (String)datasession.get("override") != null )
                if (datasession.get("override").toString().equals("SI"))
                    dapa.setOverride("3");
            
            dapa.setBranch(txtBranch);
            dapa.setTeller(txtTeller);
            dapa.setSupervisor(txtSupervisor);
            dapa.setTxnCode(txn);
            dapa.setFormat("A");
            
            dapa.setAcctNo((String)datasession.get("txtServicio"));
            dapa.setTranAmt(mto);
			dapa.setTranCur(GC.getDivisa(txtMoneda));
            dapa.setAcctNo3(GC.StringFiller((String)datasession.get("txtFolio"), 17, true, " ") + (String)datasession.get("lstTipoDocto"));
            if ( txn.equals("5565") ) {   // en efectivo
               txtDescrip= "CARGO LIQ TIP EFVO " + (String)datasession.get("txtRefer");
               DescRev = "REV P/LIQ TIP EFVO " + (String)datasession.get("txtRefer");
               dapa.setCashOut(mto);
            }
            else{
               txtDescrip= "CARGO P/LIQ TIP    " + (String)datasession.get("txtRefer");
               DescRev = "REV CARGO P/LIQ TIP" + (String)datasession.get("txtRefer");
               dapa.setAcctNo4((String)datasession.get("txtDDACuenta"));
            }
                
            if(DescRev.length() > 40)
                DescRev = DescRev.substring(0,40);
            dapa.setTranDesc(txtDescrip);
            dapa.setDescRev(DescRev);
            Diario diario = new Diario();
            sMessage = diario.invokeDiario(dapa, (String)session.getAttribute("d_ConsecLiga"), dirip, txtMoneda, session);
        }

        if( txn.equals("5563") ) {                     // Abono a cuenta por Liquidacisn 
            if ( (String)datasession.get("override") != null )
                if (datasession.get("override").toString().equals("SI"))
                    dapa.setOverride("3");
            
            dapa.setBranch(txtBranch);
            dapa.setTeller(txtTeller);
            dapa.setSupervisor(txtSupervisor);
            dapa.setTxnCode(txn);
            dapa.setFormat("A");
            
            dapa.setAcctNo((String)datasession.get("txtDDACuenta"));
            dapa.setTranAmt(mto);
			dapa.setTranCur(GC.getDivisa(txtMoneda));
            dapa.setAcctNo3(GC.StringFiller((String)datasession.get("txtFolio"), 17, true, " ") + (String)datasession.get("lstTipoDocto"));
            
            txtDescrip= "ABONO CT P/LIQ TIP " + (String)datasession.get("txtRefer");
            DescRev = "REV AB CT P/LIQ TIP" + (String)datasession.get("txtRefer");
            dapa.setAcctNo4((String)datasession.get("txtDDACuenta"));
                
            if(DescRev.length() > 40)
                DescRev = DescRev.substring(0,40);
            dapa.setTranDesc(txtDescrip);
            dapa.setDescRev(DescRev);
            Diario diario = new Diario();
            sMessage = diario.invokeDiario(dapa, (String)session.getAttribute("d_ConsecLiga"), dirip, txtMoneda, session);
        }
        
        if( txn.equals("5567") || txn.equals("5569")) {                     // cancelacion con cargo a cuenta
            if ( (String)datasession.get("override") != null )
                if (datasession.get("override").toString().equals("SI"))
                    dapa.setOverride("3");
            
            dapa.setBranch(txtBranch);
            dapa.setTeller(txtTeller);
            dapa.setSupervisor(txtSupervisor);
            dapa.setTxnCode(txn);
            dapa.setFormat("A");
            
            if ( txn.equals("5569"))
               dapa.setAcctNo((String)datasession.get("txtServicio"));
            else
               dapa.setAcctNo((String)datasession.get("txtDDACuenta"));
            
            dapa.setTranAmt(mto);
			dapa.setTranCur(GC.getDivisa(txtMoneda));
            
            if ( txn.equals("5567") ) {   // Cargo a cuenta
               txtDescrip= "CARGO/ABONO IND TIP" + (String)datasession.get("txtRefer");
               DescRev = "REV CAR/ABO IND TIP" + (String)datasession.get("txtRefer");
            }
            else{                         // Abono a TIP
               txtDescrip= "ABONO/CARGO IND TIP" + (String)datasession.get("txtRefer");
               DescRev = "REV ABO/CAR IND TIP" + (String)datasession.get("txtRefer");
            }
                
            if(DescRev.length() > 40)
                DescRev = DescRev.substring(0,40);
            dapa.setTranDesc(txtDescrip);
            dapa.setDescRev(DescRev);
            Diario diario = new Diario();
            sMessage = diario.invokeDiario(dapa, (String)session.getAttribute("d_ConsecLiga"), dirip, txtMoneda, session);
        }
        
        if (( txn.equals("5565") || txn.equals("5563")) &&  sMessage.startsWith("0")){   // transaccisn aceptada
            String txtCuenta = "";
            if (datasession.get("txtDDACuenta")!= null)
                txtCuenta = (String)datasession.get("txtDDACuenta");
            txtCadImpresion = "~TIP~" + (String)datasession.get("txtServicio") + "   " + (String)datasession.get("txtNombre") + "~"+ 
                              (String)datasession.get("txtRefer") + "~" + (String)datasession.get("txtBeneficiario") + "~" + 
                              (String)datasession.get("txtMonto") + "~" + txtCuenta + "~" + txtMoneda.substring(1) + "~";
        }
        

        session.setAttribute("txtCadImpresion",txtCadImpresion);
        session.setAttribute("page.txnresponse", sMessage);
        getServletContext().getRequestDispatcher("/servlet/ventanilla.Redirector").forward(request,response);
    }
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}
