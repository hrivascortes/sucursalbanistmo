//*************************************************************************************************
//             Funcion: Clase que realiza txn 0834
//            Elemento: Group01VOU.java
//          Creado por: Alejandro Gonzalez Castro
//      Modificado por: Alejandro Gonzalez Castro
//*************************************************************************************************
// CCN - 4360189 - 03/09/2004 - Se cambia invoke al Diario para Control de Efectivo
//*************************************************************************************************
package ventanilla;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import ventanilla.com.bital.admin.Diario;
import ventanilla.com.bital.sfb.Vouchers;
import java.util.Hashtable;

public class Group01VOU extends HttpServlet
{
  public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
  {
    response.setContentType("text/plain");
    HttpSession session = request.getSession(false);
    Hashtable datasession = (Hashtable)session.getAttribute("page.datasession");
	GenericClasses gc = new GenericClasses();

    Vouchers vouchers = new Vouchers();

    vouchers.setBranch((String)datasession.get("sucursal"));
    vouchers.setTeller((String)datasession.get("teller"));
    vouchers.setSupervisor((String)datasession.get("supervisor"));

	vouchers.setTxnCode("0834");


	vouchers.setFormat("B");

	String devs = (String)datasession.get("txtNoDevs");
	int ndevs = Integer.parseInt(devs);
	Integer nd = new Integer(ndevs);
	devs = nd.toString();
	vouchers.setAcctNo(devs);
     datasession.put("txtNoDevs",devs);

	String vtas = (String)datasession.get("txtNoVtas");
	int nvtas = Integer.parseInt(vtas);
	Integer nv = new Integer(nvtas);
	vtas = nv.toString();
	vouchers.setTranAmt(vtas);
     datasession.put("txtNoVtas",vtas);

    vouchers.setReferenc((String)datasession.get("txtNoNegocio"));

    vouchers.setFromCurr("N$");

    String dirip =  (String)session.getAttribute("dirip");
	String moneda= (String)datasession.get("moneda");
	vouchers.setFromCurr(gc.getDivisa(moneda));

    Diario diario = new Diario();
    String sMessage = diario.invokeDiario(vouchers, (String)session.getAttribute("d_ConsecLiga"), dirip, moneda, session);

    session.setAttribute("page.txnresponse", sMessage);

    getServletContext().getRequestDispatcher("/servlet/ventanilla.Redirector").forward(request,response);

  }

  public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
  {
    doPost(request, response);
  }
}
