//*************************************************************************************************
//             Funcion: Clase para Inversiones
//            Elemento: GroupMI.java
//          Creado por: Alejandro Gonzalez Castro
//      Modificado por: Israel De Paz
//*************************************************************************************************
// CCN - 4360387 - 07/10/2005 - Se realizan modificaciones para Inversiones con remesas
//*************************************************************************************************

package ventanilla;

import java.io.IOException;
import java.util.Hashtable;
import java.util.Stack;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class GroupMI extends HttpServlet
{
    public void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {
        HttpSession session = req.getSession(false);
        Stack flujotxn = (Stack)session.getAttribute("page.flujotxn");
        Hashtable ht = (Hashtable)session.getAttribute("page.datasession");
        session.setAttribute("page.flujotxn", flujotxn);
        String iTxn=(String) session.getAttribute("page.iTxn");
        String txtMontoRemesa="0.00" ;

        String txtCheque = (String)ht.get("txtCheque");
        String txtMontoCobro = (String)ht.get("txtMontoCobro");
        
        if (!iTxn.equals("3001"))
        txtMontoRemesa = (String)ht.get("txtMontoRemesa");

        if ( !txtCheque.equals("0.00"))                      // Hay descargas de cheques
            getServletContext().getRequestDispatcher("/servlet/ventanilla.PageServlet?transaction=" + ht.get("iTxn") +
            "&transaction1=" + ht.get("oTxn")).forward(req,resp);
        else  {
            flujotxn.pop();
            if (!txtMontoCobro.equals("0.00")) {
                getServletContext().getRequestDispatcher("/servlet/ventanilla.PageServlet?transaction=" + ht.get("iTxn") +
                "&transaction1=" + ht.get("oTxn")).forward(req,resp);
            }
            else {
                flujotxn.pop();
                if (!txtMontoRemesa.equals("0.00")) {
                    getServletContext().getRequestDispatcher("/servlet/ventanilla.PageServlet?transaction=" + ht.get("iTxn") +
                    "&transaction1=" + ht.get("oTxn")).forward(req,resp);
                }
                else {
                    flujotxn.pop();
                    session.setAttribute("page.flujotxn", flujotxn);
                    getServletContext().getRequestDispatcher("/servlet/ventanilla.PageServlet?transaction=" + ht.get("iTxn") +
                    "&transaction1=" + ht.get("oTxn")).forward(req,resp);
                }
            }
        }

    }
    public void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doPost(req, resp);
    }
}
