//*************************************************************************************************
//             Funcion: Clase para TXNS 0825 & 1027 & 1057 & 1091 & 1179 & 1193 & 1195 & 5571
//            Elemento: Group02R.java
//          Creado por: Alejandro Gonzalez Castro.
//		Modificado por: Juvenal Fernandez
//*************************************************************************************************
// CCN - 4360178 - 20/08/2004 - Se agrega sleep en 0825
// CCN - 4360189 - 03/09/2004 - Se cambia invoke al Diario para Control de Efectivo
// CCN - 4360268 - 04/02/2005 - Se realizan adecuaciones para eliminar Agencia NY
// CCN - 4360368 - 02/09/2005 - Se elimina sleep
// CCN - 4360374 - 08/09/2005 - Se genera nuevo paquete a peticion de QA.
//*************************************************************************************************

package ventanilla;

import java.io.IOException;
import java.util.Hashtable;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import ventanilla.com.bital.sfb.Retiros;
import ventanilla.com.bital.sfb.Acdo;

import ventanilla.com.bital.admin.Diario;

public class Group02R extends HttpServlet 
{
    public void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {
        Retiros oRetiros = new Retiros();
        Acdo    oAcdo    = new Acdo();
		GenericClasses cGenericas = new GenericClasses();
        resp.setContentType("text/plain");

        HttpSession session = req.getSession(false);
        Hashtable datasession = (Hashtable)session.getAttribute("page.datasession");

        String dirip = (String)session.getAttribute("dirip");
        String txtMoneda     = (String)datasession.get("moneda");
        String txtTxnCode    = "0825";
        String txtFormat     = "B";
        String txtBranch     = (String)datasession.get("sucursal");
        String txtTeller     = (String)datasession.get("teller");
        String txtSupervisor = (String)datasession.get("teller");
        String txtBackOut    = "0";
        String txtOverride   = "0";
        String txtCurrentDay = "0";
        char status = '1';



        if ( (String)datasession.get("supervisor") != null )
            txtSupervisor = (String)datasession.get("supervisor");

        String lstTipoDocto  = (String)datasession.get("lstTipoDocto");
        String txtReferencia = (String)datasession.get("txtFolio");

        oAcdo.setTxnCode(txtTxnCode);
        oAcdo.setFormat(txtFormat);
        oAcdo.setBranch(txtBranch);
        oAcdo.setTeller(txtTeller);
        oAcdo.setSupervisor(txtSupervisor);
        oAcdo.setBackOut(txtBackOut);
        oAcdo.setOverride(txtOverride);
        oAcdo.setCurrentDay(txtCurrentDay);
        oAcdo.setAcctNo((lstTipoDocto + txtReferencia));
        String Override = "NO";
        if ( (String)datasession.get("override")!= null )
            Override = datasession.get("override").toString();


        if ( Override.charAt(0) == 'N') {

            Diario diario = new Diario();
            String sMessage = diario.invokeDiario(oAcdo, (String)session.getAttribute("d_ConsecLiga"), dirip, txtMoneda, session);

            
              if(sMessage.charAt(0) !='0') {
                session.setAttribute("txtCadImpresion", "NOIMPRIMIRNOIMPRIMIR");
                session.setAttribute("page.txnresponse", sMessage);
                getServletContext().getRequestDispatcher("/servlet/ventanilla.Redirector").forward(req,resp);
                return;
            }
            status = sMessage.charAt(0);
        }
        else
            status = '0';


        if ( status == '0' )
        {
            txtTxnCode    = (String)datasession.get("cTxn");
            txtFormat     = "A";

            String getMsg = "";

            if ( (String)datasession.get("supervisor") != null )
                txtSupervisor = (String)datasession.get("supervisor");

            oRetiros.setTxnCode(txtTxnCode);
            oRetiros.setFormat(txtFormat);
            oRetiros.setBranch(txtBranch);
            oRetiros.setTeller(txtTeller);
            oRetiros.setSupervisor(txtSupervisor);
            oRetiros.setBackOut(txtBackOut);
            oRetiros.setOverride(txtOverride);
            oRetiros.setCurrentDay(txtCurrentDay);

            if ( (String)datasession.get("override") != null )
                if (datasession.get("override").toString().equals("SI"))
                    oRetiros.setOverride("3");
            String txtFechaEfect  = "";
            String txtFechaSys  = "";
            String txtSerial  = "";
            String txtDescripcion = "";
            String txtEfectivo = "000";
            String txtDDACuenta  = (String)datasession.get("txtDDACuenta");
            String txtMonto      = cGenericas.delCommaPointFromString((String)datasession.get("txtMonto"));

            oRetiros.setAcctNo(txtDDACuenta); //No de Cuenta
            oRetiros.setTranAmt(txtMonto);    //Monto Total de la Txn.
            oRetiros.setTranCur(txtMoneda);   //Tipo de Moneda con que se opera la Txn.

            if (txtTxnCode.equals("1238")) {
                txtFechaEfect  = (String)datasession.get("txtFechaEfect"); //Fecha en que se Opera la Txn.
                txtSerial     = (String)datasession.get("txtSerial");
                txtFechaSys  = (String)datasession.get("txtFechaSys"); //Fecha en que se Opera la Txn.
                txtFechaEfect =  cGenericas.ValEffDate(txtFechaEfect, txtFechaSys);
                oRetiros.setEffDate(txtFechaEfect);  //Fecha en que se opera la Txn.
                oRetiros.setCheckNo(txtSerial);   // Serial
            }
            if (txtTxnCode.equals("5253") || txtTxnCode.equals("4061")) {
                txtSerial     = (String)datasession.get("txtSerial4");
                oRetiros.setCheckNo(txtSerial);   // Serial
            }
            if (txtTxnCode.equals("5253")) {
                txtSerial     = (String)datasession.get("txtSerial");
                oRetiros.setCheckNo(txtSerial);   // Serial
            }

            String DescRev = "";

            if (txtTxnCode.equals("1197"))
            {
                txtFechaEfect  = (String)datasession.get("txtFechaEfect");  //Fecha en que se Opera la Txn.
                txtDescripcion= (String)datasession.get("txtDesc1197");//Descripci"n
                txtFechaSys  = (String)datasession.get("txtFechaSys"); //Fecha en que se Opera la Txn.
                txtFechaEfect =  cGenericas.ValEffDate(txtFechaEfect, txtFechaSys);
                String cuenta = (String)datasession.get("txtDDACuenta");
                cuenta = cuenta.substring(0,1);
                DescRev = txtDescripcion.substring(0,2) + "REV." + txtDescripcion.substring(2,txtDescripcion.length());
                if(DescRev.length() > 40)
                    DescRev = DescRev.substring(0,40);
                oRetiros.setDescRev(DescRev);
                oRetiros.setEffDate(txtFechaEfect);  //Fecha en que se opera la Txn.
                oRetiros.setTranDesc(txtDescripcion);//Descripcion
            }

            if (txtTxnCode.equals("1091") || txtTxnCode.equals("1179") || txtTxnCode.equals("5357") ||
            txtTxnCode.equals("1193") || txtTxnCode.equals("1195") || txtTxnCode.equals("1027")) {
                txtEfectivo = txtMonto;           // Salida de efectivo
                oRetiros.setCashOut(txtMonto);    //Monto Total de la Txn.
            }
            if(txtTxnCode.equals("1091")) {
                String cuenta = (String)datasession.get("txtDDACuenta");
                cuenta = cuenta.substring(0,1);
                txtEfectivo = txtMonto;
                oRetiros.setCashOut(txtMonto);

            }
            if (txtTxnCode.equals("5357")) {
                txtDescripcion = "RETIRO EFECTIVO CON FICHA MULTIPLE" + lstTipoDocto;
                oRetiros.setTranDesc(txtDescripcion);//Descripcion
                // Descripcion para Reverso
                DescRev = "REV. " + txtDescripcion;
                if(DescRev.length() > 40)
                    DescRev = DescRev.substring(0,40);
                oRetiros.setDescRev(DescRev);

            }
            

            Diario diario = new Diario();
            String sMessage = diario.invokeDiario(oRetiros, (String)session.getAttribute("d_ConsecLiga"), dirip, txtMoneda, session);

            session.setAttribute("page.txnresponse", sMessage);
            getServletContext().getRequestDispatcher("/servlet/ventanilla.Redirector").forward(req,resp);

        }
    }

    public void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {
        doPost(req, resp);
    }
}