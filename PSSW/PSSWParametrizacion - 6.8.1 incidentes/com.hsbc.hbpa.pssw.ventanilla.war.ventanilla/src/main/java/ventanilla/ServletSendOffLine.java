package ventanilla;

import java.io.IOException;
import java.io.OutputStream;
import java.net.URLDecoder;
import java.util.Vector;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ventanilla.com.bital.util.Base64;
import ventanilla.com.bital.util.NSTokenizer;

import com.bital.util.Gateway;
import com.bital.util.GatewayException;
import com.bital.util.GatewayFactory;

public class ServletSendOffLine extends HttpServlet
{
	private int    len      = 0;
	private String filename = null;
	private byte[] buffer   = new byte[4096];
	int status=0;
	String strMsg=new String("");

	public Vector Campos (String cadena, String delimitador) {
      NSTokenizer parser = new NSTokenizer(cadena, delimitador);
      Vector campos = new Vector();
      while(parser.hasMoreTokens()) {
         String token = parser.nextToken();
         if( token == null )
            continue;
         campos.addElement(token);
      }
      return campos;
   }
	
  
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{		
		Gateway cg = GatewayFactory.getGateway("");
		String strParametro = request.getQueryString();
		//System.out.println("1 strParametro " + strParametro);		
		strParametro = URLDecoder.decode(strParametro);
		//System.out.println("2 strParametro " + strParametro);				
		strParametro = strParametro.substring(0,strParametro.length()-7);
		Base64 decoder = new Base64();
		byte[] byteParametro = decoder.decode(strParametro);
		String strMsgEnvio = new String(byteParametro, 0, byteParametro.length,"ISO-8859-1");		
		//System.out.println(" strMsgEnvio " + strMsgEnvio);		
		String strMsg = new String("");
		OutputStream os = null;
                response.setContentType("application/text");
		byte[] bMsg = null;
		try
		{
			//System.out.println("Mensaje Original Des:" + strMsgEnvio );						
			String dirip = request.getRemoteAddr();
			String getMsg = "";
			Vector vCampos = Campos(strMsgEnvio,"*");
			//System.out.println(" strMsgEnvio " + strMsgEnvio);
			//System.out.println(" vCampos " + vCampos );
			//System.out.println(" dirip " + dirip );
                        try{Thread.sleep( 1500 );}
                        catch (InterruptedException e){}
			cg.write(strMsgEnvio);
			String result = cg.readLine();
                        //System.out.println("Respuesta Host:" + result);
			strMsg = result.substring(0, 1) + "~" + result.substring(2, 4) + "~"
				+ result.substring(4, 11) + "~" + result.substring(12, result.length()) + "~";
			os = response.getOutputStream();
			bMsg = strMsg.getBytes("ISO-8859-1");
			os.write(bMsg, 0, bMsg.length);
			os.close();

		}
		catch( GatewayException e )
		{
			os = response.getOutputStream();
			strMsg = "1~01~0000000~ERROR DE ENLACE A LA APLICACION CICS~";
			bMsg = strMsg.getBytes();
			os.write(bMsg, 0, bMsg.length);		
			os.close();                    
		}
	}
  
	public void doPost(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException
	{
		doGet(request, response);
	}
}
