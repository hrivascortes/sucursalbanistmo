//*************************************************************************************************
//             Funcion: Clase para Login
//            Elemento: DBUsers.java
//          Creado por: Alejandro Gonzalez Castro
//      Modificado por: Fausto Rodrigo Flores Moreno
//*************************************************************************************************
// CCN - 4360189 - 03/09/2004 - Se corrigen SQL's para no actualizar usuario de RACF y opcion de digitalizacion
//                              Se actualizan SQL's para mostrar y guardar el tipo de Suc para
//                              Control de Efectivo
// CCN - 4360334 - 17/06/2005 - Se elimina variable dbQualifier
// CCN - 4360347 - 22/07/2005 - Validar usuario capturado de RACF(Auditoria)
// CCN - 4360361 - 05/08/2005 - Corregir error de Insert a TW_VENTA_LOG
// CCN - 4360510 - 08/09/2006 - Se almacena en el log los cambios que realiza un gerente o supervisor a un cajero
// CCN - 4360589 - 16/04/2007 - Se realizan modificaciones para el monitoreo de efectivo en las sucursales
//*************************************************************************************************
package ventanilla;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.ServletException;
import java.io.IOException;

import java.sql.*;
import java.util.Hashtable;
import com.bital.util.ConnectionPoolingManager;

public class DBUsers extends HttpServlet 
{
    private String PSUR = ConnectionPoolingManager.getPostFixUR();
    private GenericClasses GC = new GenericClasses();
    private String dato_anterior = "";
    private String dato_posterior = "";    
    
    public void setDatoAnt(String data)
    {
    	dato_anterior = dato_anterior + data +" - ";		
    }
    
    public String getDatoAnt()
    {
    	return dato_anterior;
    }
    
    public void setDatoPost(String data)
    {
    	dato_posterior = dato_posterior + data +" - ";		
    }
    
    public String getDatoPost()
    {
    	return dato_posterior;
    }
    
    public String getRACF(HttpServletRequest request)
    {	
    	String RACF ="";
	    if(request.getRemoteUser() == null)
			RACF ="sg060440";
		else
		RACF = request.getRemoteUser().toString();
		return RACF;
    }
    
    public String getDescripcion(String Cnom, String Cracf, String Cperfil, boolean password, String AntNom, String AntRACF, String AntPerfil, String name, String userracf, String userperfil)
    {
    	String Descrip = "";
    	if(Cnom.equals("S"))
    	{
    		Descrip = Descrip + "CAMBIO DE NOMBRE -";
    		setDatoAnt(AntNom);
    		setDatoPost(name);
    	}
    	if(Cracf.equals("S"))
    	{
    		Descrip = Descrip + "CAMBIO DE USUARIO RACF -";
	   		setDatoAnt(AntRACF);
	   		setDatoPost(userracf);
    	}
    	if(Cperfil.equals("S"))
    	{
    		Descrip = Descrip + "CAMBIO DE PERFIL -";
	   		setDatoAnt(AntPerfil);
	   		setDatoPost(userperfil);
    	}
    	if(password)
    	{
    		Descrip = Descrip + "REACTIVACION DE PASSWORD -";
    		setDatoAnt("********");  		
      		setDatoPost("12345678");
    	}
    	
    	return Descrip;
    }
    
    public String getPassword(boolean password)
    {
        if(password)
            return new String(", D_CLAVE_ACC = '12345678' , D_SIGN_STATUS='N', N_SIGN_INTENTOS = 0 ");
        else
            return new String("");
    }
    
    public String getName(String name)
    {
            return new String("D_NOMBRE = '"+name.toUpperCase().trim()+"' ");
    }
    
    public String getUserRACF(String userracf)
    {
        if(userracf.length() > 0)
            return new String(", D_EJCT_CTA = '"+ userracf.toUpperCase().trim() +"' ");
        else
            return new String("");
    }    
    
    public String getPerfil(String perfil1)
    {
        if(perfil1.length() > 0)
            return new String(", C_PERFIL = '"+ perfil1.toUpperCase().trim() +"' ");
        else
            return new String("");
    } 
        
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
    {
        HttpSession session = request.getSession(false);
        
        boolean password = false;
        String name     = "";
        String userracf = "";
        String usuperfil= "";
        
        if( session == null )
            return;
        ResultSet Rselect = null;
        int i = 0;
        int j = 0;
        int rows = 0;
        int cols = 0;
        
        String sql ="";
        String txn = request.getParameter("transaction");
        String txn1 = request.getParameter("transaction1");
        String suc = (String)session.getAttribute("branch");
        suc = suc.substring(1,suc.length());
        String message = "";
        String signon = "";
        
        Hashtable data = new Hashtable();
        
        if (txn1.equals("M012"))
            // M012 - Listar Usuarios
        {
            Connection pooledConnection = ConnectionPoolingManager.getPooledConnection();
            Statement select = null;
            try {
                if(pooledConnection != null) {
                    select = pooledConnection.createStatement();
                    sql = "SELECT C_CAJERO, D_NOMBRE, D_EJCT_CTA, D_REGISTRO, N_NIVEL, D_SIGN_STATUS FROM TC_USUARIOS_SUC WHERE C_CAJERO LIKE '" + suc + "%' ORDER BY C_CAJERO" + PSUR;                    
                    Rselect = select.executeQuery(sql);
                    
                    rows=1;
                    while(Rselect.next()) {
                        data.put("cajero"+rows,Rselect.getString(1));
                        data.put("nombre"+rows,Rselect.getString(2));
                        data.put("idracf"+rows,Rselect.getString(3));
                        data.put("regnom"+rows,Rselect.getString(4));
                        data.put("nivel"+rows,Rselect.getString(5));
                        data.put("status"+rows,Rselect.getString(6));
                        rows++;
                    }
                }
                data.put("control",txn1);
                String regs = "";
                rows--;
                data.put("regs",regs.valueOf(rows));
                
                
                session.setAttribute("datos", data);
            }
            catch(SQLException sqlException)
            {System.out.println("Error DBUsers::M012 [" + sqlException + "]");}
            finally {
                try {
                    if(Rselect != null) {
                        Rselect.close();
                        Rselect = null;
                    }
                    if(select != null) {
                        select.close();
                        select = null;
                    }
                    if(pooledConnection != null) {
                        pooledConnection.close();
                        pooledConnection = null;
                    }
                }
                catch(java.sql.SQLException sqlException){System.out.println("Error FINALLY DBUsers::M012 [" + sqlException + "]");}
            }
  		    getServletContext().getRequestDispatcher("/ventanilla/paginas/DisplayGTE.jsp").forward(request, response);            
        }
        if (txn1.equals("M011"))
            // M011 - Modificar Usuario - Display de Datos - Consulta
        {
        	boolean ok = false;
            String teller = request.getParameter("txtIDCajero");
            Connection pooledConnection = ConnectionPoolingManager.getPooledConnection();
            Statement select = null;
            try {
                if(pooledConnection != null) {
                    select = pooledConnection.createStatement();
                    sql = "SELECT C_CAJERO, D_EJCT_CTA, D_REGISTRO, D_NOMBRE, N_NIVEL, D_HORARIO, D_DIRECCION_IP, C_PERFIL FROM TC_USUARIOS_SUC WHERE C_CAJERO = '" + teller + "'"+PSUR;
                    Rselect = select.executeQuery(sql);
                    
                    Rselect.next();
                    String reg = "";
                    String dato = "";
                    for(i=1; i<9; i++) {
                        reg = reg.valueOf(i);
                        dato = (String)Rselect.getString(i).trim();
                        data.put(reg,dato);
                    }
                }
                data.put("control",txn1);
                ok = true;
                session.setAttribute("datos", data);
                
            }
            catch(SQLException sqlException) {
                System.out.println("Error DBUsers::M011 [" + sqlException + "]");
                
                message = "EL CAJERO NO EXISTE, POR FAVOR VERIFIQUE...";
                session.setAttribute("mensaje", message);
                
            }
            finally {
                try {
                    if(Rselect != null) {
                        Rselect.close();
                        Rselect = null;
                    }
                    if(select != null) {
                        select.close();
                        select = null;
                    }
                    if(pooledConnection != null) {
                        pooledConnection.close();
                        pooledConnection = null;
                    }
                }
                catch(java.sql.SQLException sqlException){System.out.println("Error FINALLY DBUsers::M011 [" + sqlException + "]");}
            }
            if(ok)
	            getServletContext().getRequestDispatcher("/ventanilla/paginas/DisplayGTE.jsp").forward(request, response);
	        else
		        getServletContext().getRequestDispatcher("/ventanilla/paginas/ResponseGTE.jsp").forward(request, response);
        }
        if(txn.equals("M111"))
            // OPCION M011 - MODIFICACION DE DATOS - UPDATE
        {
        	String cambio_nombre = "";
        	String cambio_racf = "";
        	String cambio_perfil = "";
       	   	String nombreant = "";
		   	String racfant = "";
		   	String perfant = "";
		   	String Descrip = "";
		   	String perfil = "00";
            String teller = request.getParameter("txtIDCajero");
            dato_anterior="";
            dato_posterior="";
            name = (String)request.getParameter("txtNombre");
            usuperfil = (String)request.getParameter("actperfil");
			
            if(request.getParameter("modifnombre")!=null)
	            cambio_nombre = (String)(request.getParameter("modifnombre"));            
    	    if(request.getParameter("antnombre")!=null)
    	        nombreant = (String)(request.getParameter("antnombre"));	            
            if(request.getParameter("modifracf")!=null)
    	        cambio_racf = (String)(request.getParameter("modifracf"));             
    	    if(request.getParameter("antracf")!=null)
    	        racfant = (String)(request.getParameter("antracf"));         
            if(request.getParameter("modifperfil")!=null)
	           cambio_perfil = (String)(request.getParameter("modifperfil"));
    	    if(request.getParameter("antperfil")!=null)
    	        perfant = (String)(request.getParameter("antperfil"));	           
            if(request.getParameter("txtUserRACF")!=null)
	           userracf = (String)(request.getParameter("txtUserRACF"));            
            if(request.getParameter("lstPerfil")!=null)
               perfil = (String)(request.getParameter("lstPerfil"));
            if(request.getParameter("txtPssw") != null)
                password = true;
            

            sql = "UPDATE TC_USUARIOS_SUC SET ";
            
            String statementPS = ConnectionPoolingManager.getStatementPostFix();
            Connection pooledConnection = ConnectionPoolingManager.getPooledConnection();
            PreparedStatement update = null;
            int code = 0;
            try {
                if(pooledConnection != null) 
                {
                    sql = sql + getName(name) + getPassword(password) + getUserRACF(userracf) + getPerfil(perfil);
                    update = pooledConnection.prepareStatement(sql + "WHERE C_CAJERO =? AND D_SIGN_STATUS <> 'B'"+ statementPS);
                    update.setString(1,teller);
                    code = update.executeUpdate();
                    
                    if( code == 1) 
                    {
                        message = "DATOS ACTUALIZADOS EXITOSAMENTE...";
                        session.setAttribute("mensaje", message);
                    }else{
	                    message = "CAJERO BLOQUEO...";
    	                session.setAttribute("mensaje", message);
    	                System.err.println("code: "+code);
        	        }
                }
            }
            catch(SQLException sqlException)
            {System.out.println("Error DBUsers::M111 [" + sqlException + "]");}
            finally {
                try {
                    if(update != null) {
                        update.close();
                        update = null;
                    }
                    if(pooledConnection != null) {
                        pooledConnection.close();
                        pooledConnection = null;
                    }
                }
                catch(java.sql.SQLException sqlException)
                {System.out.println("Error FINALLY DBUsers::M111 [" + sqlException + "]");}
            }
          
       Descrip = getDescripcion(cambio_nombre, cambio_racf, cambio_perfil, password, nombreant, racfant, perfant, name, userracf, usuperfil);     
       if(Descrip.length()>0 && code != 0)
       {
            sql = "INSERT INTO TW_VENTA_LOG VALUES(?,?,?,?,?,?,?,?,?) ";
            pooledConnection = ConnectionPoolingManager.getPooledConnection();
            PreparedStatement insert = null;
            try {
                if(pooledConnection != null) 
                {
                    insert = pooledConnection.prepareStatement(sql);
                    insert.setString(1,suc);
                    insert.setString(2,(String)session.getAttribute("teller"));
                    insert.setString(3,getRACF(request));                    
                    insert.setString(4,GC.getDate(1));
                    insert.setString(5,GC.getDate(3));
                    insert.setString(6,Descrip);
                    insert.setString(7,teller);
					insert.setString(8,getDatoAnt());                    					       
					insert.setString(9,getDatoPost());                    					       
                    code = insert.executeUpdate();
                    if( code == 1) 
                    {
                        message = "DATOS ACTUALIZADOS EXITOSAMENTE...";
                        session.setAttribute("mensaje", message);
                       
                    }
                }
            }
            catch(SQLException sqlException)
            {
   	            sql = "INSERT INTO TW_VENTA_LOG VALUES('"+suc+"','"+(String)session.getAttribute("teller")+"','"+getRACF(request)+"','"+GC.getDate(1)+"','"+GC.getDate(3)+"','"+ Descrip+"','"+teller+"','"+getDatoAnt()+"','"+getDatoPost()+"' )";
            	System.out.println("Error DBUsers::M111C [" + sql+ "]");
            	System.out.println("Error DBUsers::M111C [" + sqlException + "]");}
            finally {
                try {
                    if(update != null) {
                        update.close();
                        update = null;
                    }
                    if(pooledConnection != null) {
                        pooledConnection.close();
                        pooledConnection = null;
                    }
                }
                catch(java.sql.SQLException sqlException)
                {System.out.println("Error FINALLY DBUsers::M111C [" + sqlException + "]");}
            }
     	  }        
		  getServletContext().getRequestDispatcher("/ventanilla/paginas/ResponseGTE.jsp").forward(request, response);
        }
        
        if(txn.equals("M003"))
            // LIMITE DE FIRMA Y OTRAS OPCIONES - CONSULTA
        {
            boolean ok = false;
            Connection pooledConnection = ConnectionPoolingManager.getPooledConnection();
            Statement select = null;
            try {
                if(pooledConnection != null) {
                    select = pooledConnection.createStatement();
                    sql = "SELECT N_LIMITE_FIRMA, N_RETIRO_INVER, D_REMESAS_MN, D_REMESAS_US, D_NAFIN, C_TIPO_SUCURSAL FROM TC_INFO_SUCURSAL WHERE C_NUM_SUCURSAL= '" + suc + "'"+PSUR;
                    Rselect = select.executeQuery(sql);
                    
                    Rselect.next();
                    String reg = "";
                    String dato = "";
                    for(i=1; i<7; i++) {
                        reg = reg.valueOf(i);
                        dato = (String)Rselect.getString(i).trim();
                        data.put(reg,dato);
                    }
                }
                data.put("control",txn1);
                ok= true;
                session.setAttribute("datos", data);
            }
            catch(SQLException sqlException) {
                System.out.println("Error DBUsers::M011 [" + sqlException + "]");
                
                message = "EL CAJERO NO EXISTE, POR FAVOR VERIFIQUE...";
                session.setAttribute("mensaje", message);
                
            }
            finally {
                try {
                    if(Rselect != null) {
                        Rselect.close();
                        Rselect = null;
                    }
                    if(select != null) {
                        select.close();
                        select = null;
                    }
                    if(pooledConnection != null) {
                        pooledConnection.close();
                        pooledConnection = null;
                    }
                }
                catch(java.sql.SQLException sqlException){System.out.println("Error FINALLY DBUsers::M003 [" + sqlException + "]");}
            }
            if(ok)
	            getServletContext().getRequestDispatcher("/ventanilla/paginas/DisplayGTE.jsp").forward(request, response);            
	        else
				getServletContext().getRequestDispatcher("/ventanilla/paginas/ResponseGTE.jsp").forward(request, response);	        	
        }        
        if(txn.equals("M031"))
            // M031 - Lmmite de firma y otras opciones - Guardar
        {
            String remph = request.getParameter("txtRemPesosH");
            String rempm = request.getParameter("txtRemPesosM");
            String remps = request.getParameter("txtRemPesosS");
            String remdh = request.getParameter("txtRemDolaresH");
            String remdm = request.getParameter("txtRemDolaresM");
            String remds = request.getParameter("txtRemDolaresS");
            String nafin = request.getParameter("txtNafin");
            String tipo  = request.getParameter("txtTipoSuc");
            nafin = nafin.trim();
            
            StringBuffer montotmp = new StringBuffer(request.getParameter("txtMonto"));
            for(i=0; i<montotmp.length();) {
                if( montotmp.charAt(i) == ',')
                    montotmp.deleteCharAt(i);
                else
                    ++i;
            }
            String monto = montotmp.toString();
            
            StringBuffer firmatmp = new StringBuffer(request.getParameter("txtFirma"));
            for(i=0; i<firmatmp.length();) {
                if( firmatmp.charAt(i) == ',')
                    firmatmp.deleteCharAt(i);
                else
                    ++i;
            }
            String firma = firmatmp.toString();
            
            String statementPS = ConnectionPoolingManager.getStatementPostFix();
            Connection pooledConnection = ConnectionPoolingManager.getPooledConnection();
            PreparedStatement update = null;
            try {
                if(pooledConnection != null) {
                    update = pooledConnection.prepareStatement("UPDATE TC_INFO_SUCURSAL SET N_LIMITE_FIRMA ="+firma+", N_RETIRO_INVER="+monto+", D_REMESAS_MN ='"+remph+rempm+remps+"', D_REMESAS_US='"+remdh+remdm+remds+"', D_NAFIN='"+nafin+ "', C_TIPO_SUCURSAL='"+tipo+"' WHERE C_NUM_SUCURSAL =?"+ statementPS);
                    update.setString(1,suc);
                    int code = update.executeUpdate();
                    
                    if( code == 1) {
                        message = "DATOS ACTUALIZADOS EXITOSAMENTE...";
                        session.setAttribute("mensaje", message);

                    }
                }
            }
            catch(SQLException sqlException)
            {System.out.println("Error DBUsers::M031 [" + sqlException + "]");}
            finally {
                try {
                    if(update != null) {
                        update.close();
                        update = null;
                    }
                    if(pooledConnection != null) {
                        pooledConnection.close();
                        pooledConnection = null;
                    }
                }
                catch(java.sql.SQLException sqlException)
                {System.out.println("Error FINALLY DBUsers::M031 [" + sqlException + "]");}
            }
            getServletContext().getRequestDispatcher("/ventanilla/paginas/ResponseGTE.jsp").forward(request, response);
            
        }
        
        if(txn1.equals("M013"))
            // OPCION M011 - MODIFICACION DE STAUS DE FIRMA DE USUARIO - UPDATE
        {
            String teller = request.getParameter("txtIDCajero");
            
            String statementPS = ConnectionPoolingManager.getStatementPostFix();
            Connection pooledConnection = ConnectionPoolingManager.getPooledConnection();
            PreparedStatement update = null;
            int code = 0;
            try {
                if(pooledConnection != null) {
                    update = pooledConnection.prepareStatement("UPDATE TC_USUARIOS_SUC SET D_SIGN_STATUS='N' WHERE C_CAJERO =? AND D_SIGN_STATUS <> 'B'"+ statementPS);
                    update.setString(1,teller);
                    code = update.executeUpdate();
                    if( code == 1) {
                        message = "CAJERO DESFIRMADO EXITOSAMENTE...";
                        session.setAttribute("mensaje", message);

                    }else{
                        message = "CAJERO BLOQUEADO...";
                        session.setAttribute("mensaje", message);
                    	
                    }
                }
            }
            catch(SQLException sqlException)
            {System.out.println("Error DBUsers::M013 [" + sqlException + "]");}
            finally {
                try {
                    if(update != null) {
                        update.close();
                        update = null;
                    }
                    if(pooledConnection != null) {
                        pooledConnection.close();
                        pooledConnection = null;
                    }
                }
                catch(java.sql.SQLException sqlException)
                {System.out.println("Error FINALLY DBUsers::M013 [" + sqlException + "]");}
            }
            if(code !=0){
            sql = "INSERT INTO TW_VENTA_LOG VALUES(?,?,?,?,?,?,?,?,?) ";
            pooledConnection = ConnectionPoolingManager.getPooledConnection();
            PreparedStatement insert = null;
            try {
                if(pooledConnection != null) 
                {
                    insert = pooledConnection.prepareStatement(sql);
                    insert.setString(1,suc);
                    insert.setString(2,(String)session.getAttribute("teller"));
                    insert.setString(3,getRACF(request));                    
                    insert.setString(4,GC.getDate(1));
                    insert.setString(5,GC.getDate(3));
                    insert.setString(6,"FORZAR SALIDA USUARIO-");
                    insert.setString(7,teller);
					insert.setString(8,"ESTATUS F");                    					       
					insert.setString(9,"ESTATUS N");                    					       
                    code = insert.executeUpdate();
                    if( code == 1) 
                    {
                        message = "CAJERO DESFIRMADO EXITOSAMENTE...";
                        session.setAttribute("mensaje", message);
                    }
                }
            }
            catch(SQLException sqlException)
            {
   	            sql = "INSERT INTO TW_VENTA_LOG VALUES('"+suc+"','"+(String)session.getAttribute("teller")+"','"+getRACF(request)+"','"+GC.getDate(1)+"','"+GC.getDate(3)+"','FORZAR SALIDA USUARIO-','"+teller+"','ESTATUS F','ESTATUS N' )";
            	System.out.println("Error DBUsers::M013C [" + sql + "]");
            	System.out.println("Error DBUsers::M013C [" + sqlException + "]");}
            finally {
                try {
                    if(update != null) {
                        update.close();
                        update = null;
                    }
                    if(pooledConnection != null) {
                        pooledConnection.close();
                        pooledConnection = null;
                    }
                }
                catch(java.sql.SQLException sqlException)
                {System.out.println("Error FINALLY DBUsers::M013C [" + sqlException + "]");}
	            }
            }
     	          
            getServletContext().getRequestDispatcher("/ventanilla/paginas/ResponseGTE.jsp").forward(request, response);
        }
        
    }
    
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}
