//*************************************************************************************************
//             Funcion: Clase que realiza txn 0854 - 5565 - 5561 - 5563
//            Elemento: Group01AVA.java
//          Creado por: Alejandro Gonzalez Castro
//      Modificado por: Alejandro Gonzalez Castro
//*************************************************************************************************
// CCN - 4360189 - 03/09/2004 - Se cambia invoke al Diario para Control de Efectivo
//*************************************************************************************************
package ventanilla;

import java.io.IOException;
import java.util.Hashtable;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import ventanilla.com.bital.admin.Diario;
import ventanilla.com.bital.sfb.FormatoA;
import ventanilla.com.bital.sfb.FormatoB;

public class Group01AVA extends HttpServlet
{
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        response.setContentType("text/plain");
        HttpSession session = request.getSession(false);
        Hashtable datasession = (Hashtable)session.getAttribute("page.datasession");
        
        String dirip = (String)session.getAttribute("dirip");
        String txtMoneda = (String)datasession.get("moneda");
        String sMessage = "";
        String txtTxnCode    = (String)datasession.get("cTxn");
        String txtFormat     = "A";
        String txtBranch     = (String)datasession.get("sucursal");
        String txtTeller     = (String)datasession.get("teller");
        String txtSupervisor = (String)datasession.get("teller");
        String txtBackOut    = "0";
        String txtOverride   = "0";
        String txtCurrentDay = "0";

        if ( (String)datasession.get("supervisor") != null )
            txtSupervisor =  (String)datasession.get("supervisor");

        String txn = (String)datasession.get("cTxn");     //TIP
        StringBuffer	monto = new StringBuffer("000");        //TIP
        String txtDescrip = "";
        String DescRev = "";

        if ((String)datasession.get("txtMonto")!= null )
            monto = new StringBuffer((String)datasession.get("txtMonto"));

        for(int i=0; i<monto.length();) {
            if( monto.charAt(i) == ',' || monto.charAt(i) == '.' )
                monto.deleteCharAt(i);
            else
                ++i;
        }

        String mto = monto.toString();
		GenericClasses gc = new GenericClasses();

        if (txn.equals("0606"))      // Pago en efectivo
        {
            FormatoB avaluoB = new FormatoB();
            avaluoB.setBranch(txtBranch);
            avaluoB.setTeller(txtTeller);
            avaluoB.setSupervisor(txtSupervisor);
            avaluoB.setFormat("B");
            avaluoB.setTxnCode(txn);
            
			avaluoB.setFromCurr(gc.getDivisa(txtMoneda));
            
            avaluoB.setAcctNo(datasession.get("txtFolioAvaluo").toString());
            avaluoB.setTranAmt(mto);
            avaluoB.setReferenc("1");
            avaluoB.setCashIn(mto);
                        
            Diario diario = new Diario();
            sMessage = diario.invokeDiario(avaluoB, (String)session.getAttribute("d_ConsecLiga"), dirip, txtMoneda, session);
        }

        if( txn.equals("1641")) {                     // Cargo a cuenta
            FormatoA avaluoA = new FormatoA();
            
            if ( (String)datasession.get("override") != null )
                if (datasession.get("override").toString().equals("SI"))
                    avaluoA.setOverride("3");
            
            avaluoA.setBranch(txtBranch);
            avaluoA.setTeller(txtTeller);
            avaluoA.setSupervisor(txtSupervisor);
            avaluoA.setTxnCode(txn);
            avaluoA.setFormat("A");
            
            avaluoA.setAcctNo((String)datasession.get("txtDDACuenta"));
            avaluoA.setTranAmt(mto);
            
			avaluoA.setTranCur(gc.getDivisa(txtMoneda));
            txtDescrip= (String)datasession.get("lstTipoAvaluo");
            txtDescrip = txtDescrip + "      .";
            txtDescrip = txtDescrip.substring(0,25) + datasession.get("txtFolioAvaluo");
            DescRev = "REV. " + txtDescrip.substring(5);

            if(DescRev.length() > 40)
                DescRev = DescRev.substring(0,40);
            
            avaluoA.setTranDesc(txtDescrip);
            avaluoA.setDescRev(DescRev);
            Diario diario = new Diario();
            sMessage = diario.invokeDiario(avaluoA, (String)session.getAttribute("d_ConsecLiga"), dirip, txtMoneda, session);
        }

        session.setAttribute("page.txnresponse", sMessage);
        getServletContext().getRequestDispatcher("/servlet/ventanilla.Redirector").forward(request,response);
    }
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}