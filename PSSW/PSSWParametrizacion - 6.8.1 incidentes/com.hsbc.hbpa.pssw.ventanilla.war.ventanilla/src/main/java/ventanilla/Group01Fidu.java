//*************************************************************************************************
//             Funcion: Clase que realiza txn Fiduciario
//            Elemento: Group01Fidu.java
//          Creado por: Alejandro Gonzalez Castro
//      Modificado por: Alejandro Gonzalez Castro
//*************************************************************************************************
// CCN - 4360189 - 03/09/2004 - Se cambia invoke al Diario para Control de Efectivo
// CCN - 4360211 - 08/10/2004 - Se elimina mensaje de documentos pendientes por digitalizar
//*************************************************************************************************
package ventanilla;

import java.io.IOException;
import java.util.Hashtable;
import java.util.Stack;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import ventanilla.com.bital.sfb.Fiduciario;
import ventanilla.com.bital.admin.Diario;
import ventanilla.com.bital.util.VolanteoDDA;

public class Group01Fidu extends HttpServlet {
    private String delCommaPointFromString(String newCadNum) {
        String nCad = new String(newCadNum);
        if( nCad.indexOf(".") > -1) {
            nCad = nCad.substring(nCad.indexOf(".") + 1, nCad.length());
            if(nCad.length() != 2) {
                String szTemp = new String("");
                for(int j = nCad.length(); j < 2; j++)
                    szTemp = szTemp + "0";
                newCadNum = newCadNum + szTemp;
            }
        }
        
        StringBuffer nCadNum = new StringBuffer(newCadNum);
        for(int i = 0; i < nCadNum.length(); i++)
            if(nCadNum.charAt(i) == ',' || nCadNum.charAt(i) == '.')
                nCadNum.deleteCharAt(i);
        
        return nCadNum.toString();
    }
    
    private String getString(String newCadNum, int newOption) {
        int iPIndex = newCadNum.indexOf("CONSECUTIVO:") + 12, NumSaltos, Num = 0, Ban = 0;
        String nCadNum = new String("");
        NumSaltos = newOption;
        while(Num < NumSaltos) {
            if(newCadNum.charAt(iPIndex) == 0x20) {
                while(newCadNum.charAt(iPIndex) == 0x20)
                    iPIndex++;
                Num++;
            }
            else
                while(newCadNum.charAt(iPIndex) != 0x20)
                    iPIndex++;
        }
        while(newCadNum.charAt(iPIndex) != 0x20 && iPIndex < newCadNum.length()){
            nCadNum = nCadNum + newCadNum.charAt(iPIndex);
            if(iPIndex < newCadNum.length())
                iPIndex++;
            if(iPIndex == newCadNum.length())
                break;
        }
        return nCadNum;
    }
    
    public void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Fiduciario oFiduciario = new Fiduciario();
        VolanteoDDA oVolanteoDDA = null;
        
        resp.setContentType("text/plain");
        
        HttpSession session = req.getSession(false);
        Hashtable datasession = (Hashtable)session.getAttribute("page.datasession");
        
        String dirip = (String)session.getAttribute("dirip");
        
        String txtTxnCode    = (String)datasession.get("cTxn");
        String txtFormat     = "A";
        String txtBranch     = (String)datasession.get("sucursal");
        String txtTeller     = (String)datasession.get("teller");//"000108";
        String txtSupervisor = (String)datasession.get("teller");//"000101";
        String txtBackOut    = "0";
        String txtOverride   = "0";
        String txtCurrentDay = "0";
        Hashtable Truncamiento = (Hashtable)session.getAttribute("truncamiento");
        long TruncamientoMN = 500000L;
        long TruncamientoUSD = 50000L;
        if (!Truncamiento.isEmpty() ){
            TruncamientoMN = Long.parseLong(Truncamiento.get("TruncamientoMN").toString());
            TruncamientoUSD = Long.parseLong(Truncamiento.get("TruncamientoUSD").toString());
        }
        String txtVolante = "NO";
        
        if ( (String)datasession.get("supervisor") != null )       //pidio autorizacisn de supervisor
            txtSupervisor =  (String)datasession.get("supervisor");
        
        if ( (String)datasession.get("override") != null )
            if (datasession.get("override").toString().equals("SI"))
                txtOverride = "3";
        
        oFiduciario.setTxnCode(txtTxnCode);
        oFiduciario.setFormat(txtFormat);
        oFiduciario.setBranch(txtBranch);
        oFiduciario.setTeller(txtTeller);
        oFiduciario.setSupervisor(txtSupervisor);
        oFiduciario.setBackOut(txtBackOut);
        oFiduciario.setOverride(txtOverride);
        oFiduciario.setCurrentDay(txtCurrentDay);
        String txtRefer      = "";
        String txtFechEfect  = "";
        String lstBanco      = "";
        String lstCausa      = "";
        String txtSerial     = "";
        String txtEfectivo   = "000";
        String txtDescripcion= "";
        String txtDDACuenta  = "";
        String txtMonto      = "";
        String txtCuenta1    = "";
        String txtCodSeg    = "";
        String txtCveTran    = "";
        String txtDepFirm    = "";
        String txtDepFirm2    = "";
        long MontoTotal  = 0;
        long MontoCheque = 0;
        String txtMoneda     = (String)datasession.get("moneda");    //Moneda
        oFiduciario.setTranCur(txtMoneda);     //Tipo de Moneda con que se opera la Txn.
        String DescRev = "";
        if (txtTxnCode.equals("1181")) {
            txtDDACuenta     = (String)datasession.get("txtDDACuenta"); //No. de cuenta
            MontoCheque = Long.parseLong(delCommaPointFromString((String)datasession.get("txtCheque")));
            long MontoEfec   = Long.parseLong(delCommaPointFromString((String)datasession.get("txtEfectivo")));
            MontoTotal       = MontoCheque + MontoEfec;
            txtMonto         = new Long(MontoTotal).toString();
            txtEfectivo      = delCommaPointFromString((String)datasession.get("txtEfectivo"));
            txtDescripcion   = (String)datasession.get("lstFidu0001");  //Descripcion
            oFiduciario.setCashIn(txtEfectivo);
            oFiduciario.setTranDesc(txtDescripcion);                    // Descripcion
            // Descripcion para Reverso
            DescRev = txtDescripcion.substring(0,2) + "REV." + txtDescripcion.substring(2,txtDescripcion.length());
            if(DescRev.length() > 40)
                DescRev = DescRev.substring(0,40);
            oFiduciario.setDescRev(DescRev);
        }
        if ( txtTxnCode.equals("1183")) {
            txtDDACuenta   = (String)datasession.get("txtDDACuenta"); //No. de cuenta
            txtMonto       = delCommaPointFromString((String)datasession.get("txtMontoCheque"));     //Monto de la Txn.
            txtDescripcion = (String)datasession.get("lstFidu0001");
            txtCodSeg      = (String)datasession.get("txtCodSeg");
            txtCveTran     = (String)datasession.get("txtCveTran");
            txtCuenta1     = (String)datasession.get("txtDDACuenta2");
            txtSerial      = (String)datasession.get("txtSerial2");
            txtDepFirm     = (String)datasession.get("rdoACorte");   // Antes o despues del corte
            String txtMontoTot   = (String)datasession.get("MontoTotal");
            MontoCheque = Long.parseLong(delCommaPointFromString((String)datasession.get("txtCheque")));
            
            if( txtMontoTot != null )
                MontoTotal = Long.parseLong(txtMontoTot);
            
            
            if ( txtDepFirm.equals("01")) {                          // Antes del Corte
                txtDepFirm  = "1";
                txtDepFirm2 = "2";
            }
            else   {
                txtDepFirm  = "2";                                     // Despues del Corte
                txtDepFirm2 = "4";
            }
            txtDescripcion = txtDescripcion.substring(0,2);
            oFiduciario.setTranDesc(txtDescripcion + " CHQ" +txtCodSeg + txtCveTran + txtCuenta1 + txtSerial + "T" + txtDepFirm);
            // Descripcion para Reverso
            DescRev = txtDescripcion + " REV." + txtCodSeg + txtCveTran + txtCuenta1 + txtSerial + "T" + txtDepFirm;
            if(DescRev.length() > 40)
                DescRev = DescRev.substring(0,40);
            oFiduciario.setDescRev(DescRev);
            
            oFiduciario.setCheckNo("000" + txtSerial);
            oFiduciario.setTrNo(txtDepFirm);
            oFiduciario.setHoldays(txtDepFirm2);
            oFiduciario.setHolDays2(txtDepFirm2);
        }
        if (txtTxnCode.equals("1185")) {
            txtDDACuenta  = (String)datasession.get("txtDDAO");      //No. de cuenta origen
            txtMonto      = delCommaPointFromString((String)datasession.get("txtMonto"));     //Monto de la Txn.
        }
        if (txtTxnCode.equals("1387")) {
            txtDDACuenta  = (String)datasession.get("txtDDAO");      //No. de cuenta origen
            txtMonto      = delCommaPointFromString((String)datasession.get("txtMonto"));     //Monto de la Txn.
            txtDescripcion  = (String)datasession.get("lstFidu1387");  //Descripcion
            oFiduciario.setTranDesc(txtDescripcion);                    // Descripcion
            // Descripcion para Reverso
            DescRev = txtDescripcion.substring(0,2) + "REV." + txtDescripcion.substring(2,txtDescripcion.length());
            if(DescRev.length() > 40)
                DescRev = DescRev.substring(0,40);
            oFiduciario.setDescRev(DescRev);
        }
        if (txtTxnCode.equals("1189")) {
            txtDDACuenta  = (String)datasession.get("txtDDACuenta"); //No. de cuenta origen
            txtMonto      = delCommaPointFromString((String)datasession.get("txtMonto"));     //Monto de la Txn.
            txtSerial     = (String)datasession.get("txtSerial");     // Numero cheque de caja
            txtDescripcion  = (String)datasession.get("lstFidu0002");  //Descripcion
            oFiduciario.setCheckNo(txtSerial);
            oFiduciario.setTranDesc(txtDescripcion);                    // Descripcion
            // Descripcion para Reverso
            DescRev = txtDescripcion.substring(0,2) + "REV." + txtDescripcion.substring(2,txtDescripcion.length());
            if(DescRev.length() > 40)
                DescRev = DescRev.substring(0,40);
            oFiduciario.setDescRev(DescRev);
        }
        if (txtTxnCode.equals("1385")) {
            txtDDACuenta  = (String)datasession.get("txtDDAD"); //No. de cuenta destino
            txtDescripcion  = (String)datasession.get("lstFidu0001");  //Descripcion
            txtMonto      = delCommaPointFromString((String)datasession.get("txtMonto"));     //Monto de la Txn.
            oFiduciario.setTranDesc(txtDescripcion);                    // Descripcion
            // Descripcion para Reverso
            DescRev = txtDescripcion.substring(0,2) + "REV." + txtDescripcion.substring(2,txtDescripcion.length());
            if(DescRev.length() > 40)
                DescRev = DescRev.substring(0,40);
            oFiduciario.setDescRev(DescRev);
        }
        if (txtTxnCode.equals("1389")){
            txtDDACuenta  = (String)datasession.get("txtDDAD"); //No. de cuenta  destino
            txtMonto      = delCommaPointFromString((String)datasession.get("txtMonto"));     //Monto de la Txn.
        }
        if (txtTxnCode.equals("4015")){
            txtDescripcion = (String)datasession.get("txtBeneficiario"); //Nombre beneficiario
            txtCuenta1 = (String)datasession.get("txtDDACuenta");        //Cuenta Fiduciario
            txtSerial     = (String)datasession.get("txtSerial");     // Numero cheque de caja
            txtMonto      = delCommaPointFromString((String)datasession.get("txtMonto"));     //Monto de la Txn.
            if (txtMoneda.equals("01"))
                txtDDACuenta = "0103000023";                            // Cuenta cheques de caja pesos
            else
                txtDDACuenta = "3902222222";                            // Cuenta cheques de caja dolares
            oFiduciario.setCheckNo(txtSerial);
            oFiduciario.setTranDesc(txtDescripcion);                    // Descripcion
            oFiduciario.setAcctNo1(txtCuenta1);
        }
        
        oFiduciario.setAcctNo(txtDDACuenta);                        // No de Cuenta
        oFiduciario.setTranAmt(txtMonto);                           // Monto Total de la Txn.
        
        if (txtTxnCode.equals("1183")) {
            oVolanteoDDA = new VolanteoDDA();
            String txtBanco = (String)datasession.get("txtCveTran");
            txtBanco = txtBanco.substring(5,8);
            oVolanteoDDA.setTeller(datasession.get("teller").toString().substring(4,6));
            oVolanteoDDA.setBranch(datasession.get("sucursal").toString().substring(1));
            oVolanteoDDA.setBank(txtBanco);
            oVolanteoDDA.setCurrency((String)datasession.get("moneda"));
            oVolanteoDDA.setCorte((String)datasession.get("rdoACorte"));
            oVolanteoDDA.setChequeData((String)datasession.get("txtSerial2") +(String)datasession.get("txtDDACuenta2") + (String)datasession.get("txtCveTran") +
            (String)datasession.get("txtCodSeg"));
            oVolanteoDDA.setChequeAmount(txtMonto);
            if ( ( Long.parseLong(txtMonto.toString()) >= TruncamientoMN  && txtMoneda.equals("01")) ||
            ( Long.parseLong(txtMonto.toString()) >= TruncamientoUSD  && txtMoneda.equals("02")) ) {
                txtVolante = "SI";
                oFiduciario.setAmount2(oVolanteoDDA.getConsecutive());
            }
        }
        
        Diario diario = new Diario();
        String sMessage = diario.invokeDiario(oFiduciario, (String)session.getAttribute("d_ConsecLiga"), dirip, txtMoneda, session);
        
        if ( sMessage.charAt(0) == '0') {
            if ( txtTxnCode.equals("1183")) {
                MontoTotal = MontoTotal + Long.parseLong(txtMonto);
                datasession.put("MontoTotal",new Long(MontoTotal).toString());
                oVolanteoDDA.updateConsecutive(getString(oFiduciario.getMessage(),1), txtVolante);
                if ( MontoTotal != MontoCheque ) {
                    Stack flujotxn = (Stack)session.getAttribute("page.flujotxn");
                    if ( !flujotxn.empty())
                        flujotxn.pop();
                    flujotxn.push("1183");
                    session.setAttribute("page.flujotxn", flujotxn);
                }
            }
            
        }
        else {
            Stack flujotxn = (Stack)session.getAttribute("page.flujotxn");
            if ( !flujotxn.empty())
                flujotxn.removeAllElements();
            session.setAttribute("page.flujotxn",flujotxn);
        }
        
        session.setAttribute("page.txnresponse", sMessage);
        getServletContext().getRequestDispatcher("/servlet/ventanilla.Redirector").forward(req,resp);
    }
    public void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doPost(req, resp);
    }
}
