//*************************************************************************************************
//             Funcion: Clase que realiza txn Inversiones
//            Elemento: Group01I.java
//          Creado por: Alejandro Gonzalez Castro
//		Modificado por: Juvenal Fernandez
//*************************************************************************************************
// CCN - 4360189 - 03/09/2004 - Se cambia invoke al Diario para Control de Efectivo
// CCN - 4360268 - 04/02/2005 - Se realizan adecuaciones para eliminar Agencia NY
// CCN - 4360315 - 06/05/2005 - Se corrige problema con la descripcion de las txns txns 2526,2529,2525,2527
// CCN - 4360364 - 19/08/2005 - Se envia la descripcion correcta.
//*************************************************************************************************
package ventanilla;

import java.io.IOException;
import java.util.Hashtable;
import java.util.Stack;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import ventanilla.com.bital.sfb.Inversiones;
import ventanilla.com.bital.admin.Diario;

public class Group01I extends HttpServlet {
    public void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {
        Inversiones oInversiones = new Inversiones();
		GenericClasses cGenericas = new GenericClasses();

        resp.setContentType("text/plain");
        HttpSession session = req.getSession(false);
        Hashtable datasession = (Hashtable)session.getAttribute("page.datasession");

        String dirip = (String)session.getAttribute("dirip");

        String txtTxnCode    = (String)datasession.get("cTxn");
        String txtFormat     = "A";
        String txtBranch     = (String)datasession.get("sucursal");
        String txtTeller     = (String)datasession.get("teller");//"000108";
        String txtSupervisor = (String)datasession.get("teller");//"000101";
        String txtBackOut    = "0";
        String txtOverride   = "0";
        String txtCurrentDay = "0";
        String txtFechaEfect  = "";
        String txtFechaSys  = "";
        String txtDescripcion= "";
        String lstBanco      = "";    //Banco
        String lstCausa      = "";
        String txtSerial     = "";
        String txtEfectivo   = "000";
        String txtMonto      = "";

        if ((String)datasession.get("supervisor")!= null)
            txtSupervisor = (String)datasession.get("supervisor");

        if ( (String)datasession.get("override") != null )
            if (datasession.get("override").toString().equals("SI"))
                txtOverride = "3";

        oInversiones.setTxnCode(txtTxnCode);
        oInversiones.setFormat(txtFormat);
        oInversiones.setBranch(txtBranch);
        oInversiones.setTeller(txtTeller);
        oInversiones.setSupervisor(txtSupervisor);
        oInversiones.setBackOut(txtBackOut);
        oInversiones.setOverride(txtOverride);
        oInversiones.setCurrentDay(txtCurrentDay);
        String txtMoneda     = (String)datasession.get("moneda");    //Moneda
        String txtCDACuenta  = (String)datasession.get("txtCDACuenta"); //No. de cuenta

        if (!txtTxnCode.equals("3001"))                     //Si no es 3001
            txtMonto      = cGenericas.delCommaPointFromString((String)datasession.get("txtMonto"));     //Monto de la Txn.

        if (txtTxnCode.equals("2503")) {
            txtFechaEfect  = (String)datasession.get("txtFechaEfect"); //Fecha en que se Opera la Txn.
            txtFechaSys  = (String)datasession.get("txtFechaSys"); //Fecha en que se Opera la Txn.
            txtDescripcion= (String)datasession.get("lstDesc2503");//Descripci"n
            txtFechaEfect =  cGenericas.ValEffDate(txtFechaEfect, txtFechaSys);
            oInversiones.setEffDate(txtFechaEfect);  //Fecha en que se opera la Txn.
            oInversiones.setTranDesc(txtDescripcion);//Descripcion
            // Descripcion para Reverso
            String DescRev = "REV." + txtDescripcion;
            if(DescRev.length() > 40)
                DescRev = DescRev.substring(0,40);
            oInversiones.setDescRev(DescRev);
        }
        if (txtTxnCode.equals("2523") ) {
            txtFechaEfect  = (String)datasession.get("txtFechaEfect"); //Fecha en que se Opera la Txn.
            txtFechaSys  = (String)datasession.get("txtFechaSys"); //Fecha en que se Opera la Txn.
            txtFechaEfect =  cGenericas.ValEffDate(txtFechaEfect, txtFechaSys);
            txtDescripcion = "TRANSFERENCIA DE ";
            oInversiones.setTranDesc(txtDescripcion);//Descripcion
            oInversiones.setEffDate(txtFechaEfect);  //Fecha en que se opera la Txn.
        }
        if (txtTxnCode.equals("2529") ) {
            txtFechaEfect  = (String)datasession.get("txtFechaEfect"); //Fecha en que se Opera la Txn.
            txtFechaSys  = (String)datasession.get("txtFechaSys"); //Fecha en que se Opera la Txn.
            txtFechaEfect =  cGenericas.ValEffDate(txtFechaEfect, txtFechaSys);
            txtDescripcion = "CREDITO PARA ";
            oInversiones.setTranDesc(txtDescripcion);//Descripcion
            oInversiones.setEffDate(txtFechaEfect);  //Fecha en que se opera la Txn.
        }
        if ( txtTxnCode.equals("3001")) {
            txtFechaEfect  = (String)datasession.get("txtFechaEfect"); //Fecha en que se Opera la Txn.
            txtFechaSys  = (String)datasession.get("txtFechaSys"); //Fecha en que se Opera la Txn.
            txtEfectivo   = cGenericas.delCommaPointFromString((String)datasession.get("txtEfectivo"));
            txtFechaEfect =  cGenericas.ValEffDate(txtFechaEfect, txtFechaSys);
            long Efectivo = Long.parseLong(cGenericas.delCommaPointFromString(txtEfectivo));
            long MontoCheque  = Long.parseLong(cGenericas.delCommaPointFromString((String)datasession.get("txtCheque")));
            long MontoCobro  = Long.parseLong(cGenericas.delCommaPointFromString((String)datasession.get("txtMontoCobro")));
            long MontoRemesa  = Long.parseLong(cGenericas.delCommaPointFromString((String)datasession.get("txtMontoRemesa")));
            long MontoTotal = MontoCheque+MontoCobro+MontoRemesa+Efectivo;
            txtMonto = new Long(MontoTotal).toString();
            if ( txtEfectivo.equals("000") )
                txtDescripcion = "DEPOSITO CON DOCUMENTOS";
            else {
                if ( Efectivo != MontoTotal )
                    txtDescripcion = "DEPOSITO EN EFECTIVO Y DOCUMENTOS";
                else
                    txtDescripcion = "DEPOSITO EN EFECTIVO";
            }
            oInversiones.setCashIn(txtEfectivo);
            oInversiones.setEffDate(txtFechaEfect);  //Fecha en que se opera la Txn.
            oInversiones.setTranDesc(txtDescripcion);//Descripcion
            // Descripcion para Reverso
            String DescRev = "REV." + txtDescripcion;
            if(DescRev.length() > 40)
                DescRev = DescRev.substring(0,40);
            oInversiones.setDescRev(DescRev);
        }

        if( txtTxnCode.equals("3009")  ) {
            txtEfectivo = txtMonto;
            oInversiones.setCashIn(txtMonto);
        }
        if( txtTxnCode.equals("3077") ) {
            txtEfectivo = txtMonto;
            oInversiones.setCashOut(txtMonto);
        }
        if (txtTxnCode.equals("3587") || txtTxnCode.equals("3487")) {
            txtSerial     = (String)datasession.get("txtSerial");
            txtFechaEfect  = (String)datasession.get("txtFechaEfect"); //Fecha en que se Opera la Txn.
            txtFechaSys  = (String)datasession.get("txtFechaSys"); //Fecha en que se Opera la Txn.
            txtFechaEfect =  cGenericas.ValEffDate(txtFechaEfect, txtFechaSys);
            oInversiones.setEffDate(txtFechaEfect);  //Fecha en que se opera la Txn.
            oInversiones.setCheckNo(txtSerial);
        }
        if (txtTxnCode.equals("3314")) {           // Retiro Total
            txtFechaEfect = (String)datasession.get("txtFechaEfect"); //Fecha en que se Opera la Txn.
            txtFechaSys   = (String)datasession.get("txtFechaSys");   //Fecha en que se Opera la Txn.
            txtFechaEfect =  cGenericas.ValEffDate(txtFechaEfect, txtFechaSys);
            txtEfectivo = txtMonto;
            oInversiones.setEffDate(txtFechaEfect);
            oInversiones.setCashOut(txtMonto);                        // Salida de efectivo
            Stack flujotxn = (Stack)session.getAttribute("page.flujotxn");
            int Existe3009 = flujotxn.search("3009");
            if(Existe3009 != -1)
                oInversiones.setReversable("N");
        }

        // Se establesen por medio del BEAN los datos para la transacci"n.
        oInversiones.setTranCur(txtMoneda);     //Tipo de Moneda con que se opera la Txn.
        oInversiones.setAcctNo(txtCDACuenta);   //No de Cuenta
        oInversiones.setTranAmt(txtMonto);      //Monto Total de la Txn.

        Diario diario = new Diario();
        String sMessage = diario.invokeDiario(oInversiones, (String)session.getAttribute("d_ConsecLiga"), dirip, txtMoneda, session);

        if (!sMessage.equals(""))
        {
            session.setAttribute("page.txnresponse",sMessage);
            getServletContext().getRequestDispatcher("/servlet/ventanilla.Redirector").forward(req,resp);
            return;
        }


        session.setAttribute("page.txnresponse", oInversiones.getMessage());

        getServletContext().getRequestDispatcher("/servlet/ventanilla.Redirector").forward(req,resp);

    }

    public void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doPost(req, resp);
    }
}
