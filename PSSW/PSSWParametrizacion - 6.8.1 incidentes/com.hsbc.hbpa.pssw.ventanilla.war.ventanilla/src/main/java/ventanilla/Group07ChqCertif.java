//*************************************************************************************************
//             Funcion: Clase que realiza txn Cheques Certificados
//            Elemento: Group07ChqCertif.java
//          Creado por: Alejandro Gonzalez Castro
//      Modificado por: Alejandro Gonzalez Castro
//*************************************************************************************************
// CCN - 4360189 - 03/09/2004 - Se cambia invoke al Diario para Control de Efectivo
//*************************************************************************************************
package ventanilla;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.util.Hashtable;

import ventanilla.com.bital.sfb.ChqCertif;
import ventanilla.com.bital.admin.Diario;

public class Group07ChqCertif extends HttpServlet {
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String txtTxn = "";
        HttpSession session = request.getSession(false);
        Hashtable datasession = (Hashtable)session.getAttribute("page.datasession");
        String cuentaAbono = (String)datasession.get("txtCuenta2");
        String cuentaCheque = (String)datasession.get("txtDDACuenta2");

        String compara = (String)datasession.get("cTxn");
        String dirip =  (String)session.getAttribute("dirip");

        ChqCertif chqCert = new ChqCertif();
        txtTxn = (String)datasession.get("cTxn");
        String txtTeller = (String)datasession.get("teller");
        chqCert.setTxnCode(txtTxn);
        chqCert.setBranch((String)datasession.get("sucursal"));
        chqCert.setTeller(txtTeller);
        chqCert.setSupervisor(txtTeller);

        if ( (String)datasession.get("override") != null )
            if (datasession.get("override").toString().equals("SI"))
                chqCert.setOverride("3");
        if ( (String)datasession.get("supervisor") != null )
            chqCert.setSupervisor((String)datasession.get("supervisor"));

        StringBuffer efectivo = new StringBuffer((String)datasession.get("txtMonto"));
        for(int j=0; j<efectivo.length();) {
            if(efectivo.charAt(j) == ',' || efectivo.charAt(j) == '.')
                efectivo.deleteCharAt(j);
            else
                ++j;
        }

        String moneda = (String)datasession.get("moneda");
        String cuenta = "";
		GenericClasses gc = new GenericClasses();
		moneda=gc.getDivisa(moneda);
        if( moneda.equals("01")  || moneda.equals("1")  || moneda.equals("N$")) {
            cuenta = "0103000015";
        }
        else if( moneda.equals("02") || moneda.equals("2") ) {
            cuenta = "4902222222";
        }

        String tmpTrNo1 = (String)datasession.get("txtCveTran");
        //String tmpTrNo2 = (String)datasession.get("txtCodSeg");
		String tmpTrNo2 = "0000";
		
        tmpTrNo1 = tmpTrNo1.substring(2,2+3);
        tmpTrNo2 = tmpTrNo2.substring(0,3);

        if(cuentaCheque.length() > 10)
            cuentaCheque = cuentaCheque.substring(1);


        int longSerial = datasession.get("txtSerial2").toString().length();
        String Serial = (String)datasession.get("txtSerial2");
        for(int i=longSerial; i<10; i++) {
            Serial = "0" + Serial;
        }

        chqCert.setAcctNo(cuenta);
        chqCert.setTranAmt(efectivo.toString());
        chqCert.setTranCur(moneda);
        chqCert.setCheckNo(Serial);
        chqCert.setMoamoun("000");
        chqCert.setFees("8");
        String txtDescrip = "ABONO DEV. OTRAS CAUSAS      " + cuentaCheque;
        chqCert.setTranDesc(txtDescrip);
        chqCert.setTrNo1(tmpTrNo1);
        chqCert.setTrNo2(tmpTrNo2);

       // Descripcion para Reverso
        String DescRev = "REV.ABONO DEV. OTRAS CAUSAS  " + cuentaCheque;
        if(DescRev.length() > 40)
            DescRev = DescRev.substring(0,40);
        chqCert.setDescRev(DescRev);

        Diario diario = new Diario();
        String sMessage = diario.invokeDiario(chqCert, (String)session.getAttribute("d_ConsecLiga"), dirip, moneda, session);

        session.setAttribute("page.txnresponse", sMessage);
        getServletContext().getRequestDispatcher("/servlet/ventanilla.Redirector").forward(request,response);
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}