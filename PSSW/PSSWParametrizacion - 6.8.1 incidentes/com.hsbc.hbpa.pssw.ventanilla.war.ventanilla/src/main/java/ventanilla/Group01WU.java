//*************************************************************************************************
//             Funcion: Clase que realiza txn WU
//            Elemento: Group01WU.java
//          Creado por: Alejandro Gonzalez Castro
//      Modificado por: Rutilo Zarco Reyes
//*************************************************************************************************
// CCN - 4360189 - 03/09/2004 - Se habilita WU
// CCN - 4360368 - 03/09/2004 - Se agrega numero de intentos y tiempo de espera para localizar MTCN
// CCN - 4360374 - 08/09/2005 - Se genera nuevo paquete a peticion de QA.
// CCN - 4360378 - 19/09/2005 - Se agrega log para WesterUnion
// CCN - 4360383 - 26/09/2005 - Se incrementa el numero de intentos con WU
// CCN - 4360384 - 28/09/2005 - Se eliminan displays
// CCN - 4360392 - 07/10/2005 - Se modifica para visualizar mensajes de WU
//*************************************************************************************************

package ventanilla;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.Hashtable;
import java.util.Vector;
import java.text.DecimalFormat;

import ventanilla.com.bital.sfb.WUC;
import ventanilla.com.bital.sfb.WUCD;
import ventanilla.com.bital.sfb.WUVSAM;

public class Group01WU extends HttpServlet 
{
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
    {
        response.setContentType("text/plain");
        HttpSession session = request.getSession(false);
        Hashtable datasession = (Hashtable)session.getAttribute("page.datasession");
        boolean detorden = false;
        String sMessage = "";
        WUC wuc = new WUC();
        WUCD wucd = new WUCD();
        
        if(request.getParameter("WUaction") != null)
            detorden = true;
        
        
        if(detorden)
        {    
            Hashtable data = new Hashtable();
            data = (Hashtable)session.getAttribute("WUdata");

            wucd.setBranch((String)datasession.get("sucursal"));
            wucd.setRegistro((String)datasession.get("registro"));
            wucd.setTeller((String)datasession.get("teller"));
            wucd.setNoSecuencia("05");
            wucd.setNoTerminal((String)data.get("noterm"));
            wucd.setNoSecAnt((String)data.get("nosec"));
            wucd.setCveDespierta((String)data.get("cvedesp"));
            wucd.setCuenta((String)data.get("cta"));
            wucd.setLU((String)data.get("lu"));
            wucd.setTipoApp((String)data.get("app"));
            wucd.setConsecutivo((String)data.get("consec"));
            wucd.setResultado("00");
            wucd.setPaginacion("2");
            wucd.setMTCN(request.getParameter("orden"));
            wucd.execute();
            sMessage = wucd.getMessage();
        }
        else
        {
            wuc.setBranch((String)datasession.get("sucursal"));
            wuc.setRegistro((String)datasession.get("registro"));
            wuc.setTeller((String)datasession.get("teller"));
            wuc.setNoSecuencia("01");
            wuc.setNoTerminal("0000");
            wuc.setNoSecAnt("00");
            wuc.setCveDespierta("00000000");
            wuc.setCuenta("         ");
            wuc.setLU("    ");
            wuc.setTipoApp("1");
            wuc.setConsecutivo("00000000");
            wuc.setResultado("00");
            wuc.setMTCN((String)datasession.get("txtMTCNS"));
            wuc.setNomBenS((String)datasession.get("txtNomBenS"));
            wuc.setApePBenS((String)datasession.get("txtApePatBenS"));
            wuc.setNomOrdS((String)datasession.get("txtNomOrdS"));
            wuc.setApePOrdS((String)datasession.get("txtApePatOrdS"));
            wuc.setNomBen((String)datasession.get("txtNomBen"));
            wuc.setApePBen((String)datasession.get("txtApePatBen"));
            wuc.setApeMBen((String)datasession.get("txtApeMatBen"));
            String calle = ((String)datasession.get("txtCalleBen")).trim();
            String col  = ((String)datasession.get("txtColBen")).trim();
            wuc.setCalle(calle + " " + col);
            wuc.setCiudad((String)datasession.get("txtCiudadBen"));
            wuc.setEstado((String)datasession.get("txtEdoBen"));
            wuc.setCP((String)datasession.get("txtCPBen"));
            wuc.setTelefono((String)datasession.get("txtTelBen"));
            wuc.execute();
            sMessage = wuc.getMessage();
        }
        
        String code = sMessage.substring(8,10);
        if(code.equals("00"))
        {
            WUVSAM vsam = new WUVSAM();
            vsam.setOPSucursal((String)datasession.get("sucursal"));
            vsam.setTxnId("VSAM");
            vsam.setFuncion("R");
            vsam.setFile("PASWESTE");
            vsam.setLenKey("7");
            String key = sMessage.substring(70,sMessage.length()-1);
            key = key + "2";
            vsam.setKey(key);
            vsam.setLenReg("2300");
            for(int i=0; i < 19; i++)
            {
                vsam.execute();
                sMessage = vsam.getMessage();
                code = sMessage.substring(0,2);
                // CODIGO 13 - NO SE ENCONTRO LA LLAVE => REINTENTAR TXN
                if(!code.equals("13"))
                    break;
                try{Thread.sleep(15000);}
                catch (InterruptedException e){}
            }

            StringBuffer tmpsb = new StringBuffer(sMessage);
            for(int i=0; i<tmpsb.length();) 
            {
                if( tmpsb.charAt(i) == '\\')
                {
                    tmpsb.deleteCharAt(i);
                    tmpsb.insert(i, "*");
                }
                else
                    ++i;
            }
            sMessage = tmpsb.toString();
            
            if(code.equals("00"))
            {
                Hashtable datos = new Hashtable();
                datos.put("nosec", sMessage.substring(161,163));
                datos.put("noterm", sMessage.substring(163,167));
                datos.put("cvedesp", sMessage.substring(169,177));
                datos.put("cta", sMessage.substring(177,186).trim());
                datos.put("lu", sMessage.substring(186,190));
                datos.put("app", sMessage.substring(190,191).trim());
                datos.put("consec", sMessage.substring(191,199).trim());
                
                String res = sMessage.substring(199,201);
                String tmp = "";
                String dato = "";
                
                if(res.equals("01") || res.equals("03") || res.equals("06"))
                {
                    sMessage = "1~01~0000000~"+sMessage.substring(206,sMessage.length()-1).trim()+"~";
                    session.setAttribute("txtCadImpresion", "");
                }
                
                // RESPUESTA DETALLE DE ORDEN
                else if(res.equals("04"))
                {
                    tmp  = sMessage.substring(286,sMessage.length()).trim();
                    dato = tmp.substring(3,13).trim();
                    dato = dato + "00";
                    dato = FormatAmount(dato);
                    datos.put("mto",dato);
                    datos.put("dbkey",tmp.substring(13,23).trim());
                    datos.put("mtcn",tmp.substring(23,34).trim());
                    datos.put("ord",tmp.substring(34,114).trim());
                    datos.put("ciudad",tmp.substring(114,167).trim());
                    datos.put("tel", tmp.substring(167,177).trim());
                    datos.put("ben", tmp.substring(177,257).trim());
                    
                    dato = tmp.substring(261,269);
                    dato = dato.substring(6,8)+"-"+dato.substring(4,6)+"-"+dato.substring(0,4);
                    datos.put("fecha",dato);
                    
                    dato = tmp.substring(269,275);
                    dato = dato.substring(0,2)+":"+dato.substring(2,4)+":"+dato.substring(4,6);
                    datos.put("hora",dato);
                    
                    datos.put("contra",tmp.substring(285,325).trim());

                    datos.put("dir",tmp.substring(1291,1355).trim());
                    datos.put("estado",tmp.substring(1379,1391).trim());

                    dato = tmp.substring(1391,1401);
                    int mtous = Integer.parseInt(dato);
                    dato = String.valueOf(mtous);
                    dato = FormatAmount(dato);
                    datos.put("mtoUS",dato);
                    
                    datos.put("moneda",tmp.substring(1401,1404).trim());
                    dato = tmp.substring(1404,1419);
                    long mto = Long.parseLong(dato);
                    dato = String.valueOf(mto); 
                    dato = dato.substring(0,1) + "." + dato.substring(1,dato.length());
                    datos.put("tipocambio",dato);
                    session.setAttribute("WUdata", datos);
                    session.setAttribute("page.getoTxnView", "ResponseWUDO");
                }
                // RESPUESTA MULTIPLE
                else if(res.equals("05"))
                {
                    Vector beneficiario = new Vector();
                    Vector ordenante    = new Vector();
                    Vector monto        = new Vector();
                    Vector status       = new Vector();
                    Vector fecha        = new Vector();
                    Vector dbkey        = new Vector();
                    
                    tmp  = sMessage.substring(301,sMessage.length()).trim();
                    int size = 0;
                    size = tmp.length();
                    for(int j=0; j<27; j++)
                    {
                        if(size >= 87*(j+1))
                        {
                            dato = tmp.substring(10+(87*j),30+(87*j)).trim();
                            if(dato.length() <= 0)
                                break;
                            beneficiario.add(j,dato);
                            dato = tmp.substring(30+(87*j),50+(87*j)).trim();
                            ordenante.add(j,dato);
                            dato = tmp.substring(50+(87*j),65+(87*j)).trim();
                            int mto = Integer.parseInt(dato);
                            dato = String.valueOf(mto);
                            dato = FormatAmount(dato);
                            monto.add(j,dato);
                            dato = tmp.substring(65+(87*j),69+(87*j)).trim();
                            status.add(j,dato);
                            dato = tmp.substring(69+(87*j),77+(87*j)).trim();
                            dato = dato.substring(6,8)+"-"+dato.substring(4,6)+"-"+dato.substring(0,4);
                            fecha.add(j,dato);
                            dato = tmp.substring(77+(87*j),87+(87*j)).trim();
                            dbkey.add(j,dato);
                        }
                        else
                            break;
                    }
                    datos.put("beneficiario", beneficiario);
                    datos.put("ordenante", ordenante);
                    datos.put("monto", monto);
                    datos.put("status", status);
                    datos.put("fecha", fecha);
                    datos.put("dbkey", dbkey);
                    session.setAttribute("WUdata", datos);
                    session.setAttribute("page.getoTxnView", "ResponseWUCM");
                }
                else
                {
                    session.setAttribute("txtCadImpresion", "");
                    sMessage = "0~01~0000000~"+sMessage.substring(206,sMessage.length()).trim();
                    session.setAttribute("page.getoTxnView", "Response");
                }
            }
            else
            {
                session.setAttribute("txtCadImpresion", "");
                sMessage = "0~01~0000000~"+sMessage.substring(2,42).trim();
            }
            
        }
        else
        {
            session.setAttribute("txtCadImpresion", "");
            sMessage = "0~01~0000000~"+sMessage.substring(10,50).trim();
       }
        
        session.setAttribute("page.txnresponse", sMessage);
        getServletContext().getRequestDispatcher("/servlet/ventanilla.Redirector").forward(request,response);
       
    }
    
    public String FormatAmount(String monto)
    {
        double number = Double.parseDouble(monto)/100.00;
        DecimalFormat DecFormat = new DecimalFormat("###,##0.00");
        String FormatMto = DecFormat.format(number);
        return FormatMto;
    }
    
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
    {
        doPost(request, response);
    }
}
