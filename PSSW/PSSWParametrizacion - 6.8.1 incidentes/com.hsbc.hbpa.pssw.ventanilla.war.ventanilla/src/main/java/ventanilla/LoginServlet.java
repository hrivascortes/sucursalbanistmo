//*************************************************************************************************
//             Funcion: Clase para Login
//            Elemento: LoginServlet.java
//          Creado por: Alejandro Gonzalez Castro
//      Modificado por: Fausto Rodrigo Flores Moreno
//*************************************************************************************************
// CCN - 4360175 - 03/08/2004 - Se habilita guardado correcto de horario en la sesion
// CCN - 4360178 - 20/08/2004 - Se journaliza txn 0081
// CCN - 4360189 - 03/09/2004 - Se Almacena usuario de RACF en la tabla de TC_USUARIOS_SUC
//                              Se agrega txn de consulta de Totales para Control de Efectivo
// CCN - 4360260 - 21/01/2005 - Se agrega variable en sesion CRMactive.
// CCN - 4360291 - 18/03/2005 - Se agrega codigo para redireccionar al main.jsp
// CCN - 4360297 - 23/03/2005 - Se cambia CCN por peticion de QA, CCN anterior 4360291
// CCN - 4360334 - 17/06/2005 - Se modifica llamado a main
// CCN - 4360364 - 19/08/2005 - Se modifca acceso al sistema enviando id de GTE la primera vez.
//								Se activa Control de Efectivo en D�lares.	
// CCN - 4360390 - 07/10/2005 - Se realizan correciones para acdo
// CCN - 4360456 - 07/04/2006 - Se eleimina la validaci�n de CRM para sucursales.
// CCN - 4360510 - 08/09/2006 - Se almacena en sesion el perfil con el que firma un cajero 
// CCN - 4360518 - 21/09/2006 - Se agrega en sesion varaible para controlar ruta para el intercambio de perfiles.
// CCN - 4360525 - 10/10/2006 - Se quita instrucci�n session.setMaxInactiveInterval(-1) para habiliatar inactividad de sesi�n.
// CCN - 4360576 - 09/03/2007 - Modificaciones para proyecto OPMN
// CCN - 4360587 - 02/04/2007 - Modificaciones para monitoreo de efectivo
// CCN - 4360589 - 16/04/2007 - Se realizan modificaciones para el monitoreo de efectivo en las sucursales
//*************************************************************************************************

package ventanilla;

import java.io.IOException;
import java.util.Hashtable;
import java.util.StringTokenizer;
import java.util.Vector;
import java.util.*;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import ventanilla.com.bital.beans.DatosVarios;
import ventanilla.com.bital.beans.MtosMax;
import ventanilla.com.bital.sfb.BTotales;

public class LoginServlet extends HttpServlet {
	private GenericClasses gc = new GenericClasses();

	private void setMontoPIF(HttpSession sesion, long efec, String fact,
			String moneda) {
		String PIFFfactor = "";
		String PIFSfactor = "";
		long MontoFactor = 0;
		long MontoFactor2 = 0;

		long mtoTmp1 = efec
				* Integer.parseInt(fact.substring(0, fact.indexOf(".")));
		long mtoTmp2 = efec
				* Integer.parseInt(fact.substring(fact.indexOf(".") + 1));
		String mtotmp = String.valueOf(mtoTmp2);
		mtotmp = mtotmp.substring(0, mtotmp.length() - 1);
		if (!mtotmp.equals(""))
			MontoFactor = mtoTmp1 + Long.parseLong(mtotmp);
		else
			MontoFactor = mtoTmp1;
		sesion.setAttribute("MontoFactor" + moneda, String.valueOf(MontoFactor));
	}

	private void setTxnWithoutMontoValidate(HttpServletRequest request) {
		HttpSession sesion = request.getSession(false);

		if (sesion.getServletContext().getAttribute("txnsWithOutValidateMonto") == null) {
			DatosVarios perfiles = DatosVarios.getInstance();
			String txn = perfiles.getDatosv("TXN_XMONTO");
			sesion.getServletContext().setAttribute("txnsWithOutValidateMonto",
					txn);

			/* LOCAL-916793 */
			
		}

	}

	private void CheckPerfil(HttpSession sesion, String perfil, String cajero) {
		DatosVarios perfiles = DatosVarios.getInstance();
		Vector vperfiles = new Vector();
		vperfiles = (Vector) perfiles.getDatosV("PERFIL");

		DatosVarios perfilt = DatosVarios.getInstance();
		Hashtable hPerfil = perfilt.getDatosH("PERFILT");

		int banperexst = 0;
		String tipoEnt = "";

		int i = 0;
		while (i < vperfiles.size() && banperexst == 0) {
			String cad = vperfiles.elementAt(i).toString();
			String indice = cad.substring(0, 2);
			if (perfil.equals(indice) && cad.substring(3, 7).equals("0000")) {
				if (hPerfil.containsKey(indice)) {
					tipoEnt = cad.substring(10, 11);
					sesion.setAttribute("tipomenu", tipoEnt);
					banperexst = 1;
					sesion.setAttribute("perfdel", "N");
				}
			}
			i++;
		}

		if (banperexst == 0) {
			sesion.setAttribute("perfdel", "S");
			tipoEnt = "P";
			sesion.setAttribute("tipomenu", tipoEnt);
			sesion.setAttribute("tellerperfil", "00");
			sesion.setAttribute("perfilbase", "00");
			java.sql.Connection pooledConnection = com.bital.util.ConnectionPoolingManager
					.getPooledConnection();
			java.sql.PreparedStatement newPreparedStatement = null;
			String statementPS = com.bital.util.ConnectionPoolingManager
					.getStatementPostFix();
			int code = 0;
			try {
				if (pooledConnection != null) {
					newPreparedStatement = pooledConnection
							.prepareStatement("UPDATE tc_Usuarios_Suc SET C_PERFIL='00' WHERE c_Cajero = ? "
									+ statementPS);
					newPreparedStatement.setString(1, cajero);
					code = newPreparedStatement.executeUpdate();
					if (code > 1)
						System.out.println("Error Login::doPost::Update>1 -"
								+ code + "-");
				}
			} catch (java.sql.SQLException sqlexception) {
				System.out.println("Error en Login::doPost ["
						+ sqlexception.toString() + "]");
			} finally {
				try {
					if (newPreparedStatement != null) {
						newPreparedStatement.close();
						newPreparedStatement = null;
					}
					if (pooledConnection != null) {
						pooledConnection.close();
						pooledConnection = null;
					}
				} catch (java.sql.SQLException sqlException) {
					System.out.println("Error Login::doPost FINALLY: ["
							+ sqlException.toString() + "] ");
				}
			}
		}
	}

	public Vector GetTotales(String Moneda, String sucursal, String cajero,
			String supervisor) {
		String efe1 = "";
		String efe2 = "";
		BTotales total = new BTotales();
		Vector totales = new Vector();

		total.setTxnCode("0011");
		total.setBranch(sucursal);
		total.setTeller(cajero);
		total.setSupervisor(supervisor);
		total.setTrNo(Moneda);
		total.execute();
		String mtotales = total.getMessage();

		StringTokenizer st = new StringTokenizer(mtotales, "(");
		String tk = "";
		while (st.hasMoreTokens()) {
			tk = st.nextToken();
			if ((tk.indexOf("EFECTIVO RECIBIDO")) > 0) {
				efe1 = tk.substring(tk.indexOf(".") - 18, tk.length());
				efe1 = efe1.trim();
				efe1 = gc.delCommaPointFromString(efe1);
				totales.addElement(efe1);
			} else if ((tk.indexOf("EFECTIVO PAGADOS")) > 0) {
				efe2 = tk.substring(tk.indexOf(".") - 18, tk.length());
				efe2 = efe2.trim();
				efe2 = gc.delCommaPointFromString(efe2);
				totales.addElement(efe2);
			}
		}

		return totales;
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// Obtener informacion de la sesion
		HttpSession session = request.getSession(true);
		java.util.Hashtable branchInfo = new java.util.Hashtable();
		java.util.Hashtable truncamiento = new java.util.Hashtable();
		java.util.Hashtable userInfo = new java.util.Hashtable();
		String nivelcajero = "";
		String sucursal = "";
		String limefe = "";
		String limefe2 = "";
		String perfil = "";
		String perfilbase = "";
		String cajero = "";
		long EfeLim = 0;
		long EfeLim2 = 0;

		if (getServletContext().getAttribute("ctgID$") != null)
			System.setProperty("ctgID$",
					getServletContext().getAttribute("ctgID$").toString());
		if (getServletContext().getAttribute("ctgID") != null)
			System.setProperty("ctgID",
					getServletContext().getAttribute("ctgID").toString());

		// Asegurar una nueva sesion sin timeout explicito
		if (!session.isNew()) {
			session.invalidate();
			session = request.getSession(true);
		}

		// Verificar datos del usuario
		ventanilla.com.bital.util.AdminUsers users = new ventanilla.com.bital.util.AdminUsers();
		branchInfo = (java.util.Hashtable) users.getBranchInfo(request
				.getParameter("teller"));
		truncamiento = (java.util.Hashtable) users.getMontTruncamiento();
		String txtHorario = null;
		if (request.getParameter("horario") != null)
			txtHorario = (String) request.getParameter("horario");

		String pass1 = new String("X");
		if (request.getParameter("password1") != null)
			pass1 = request.getParameter("password1");

		String cambio = "N";
		if (request.getParameter("cambio") != null)
			cambio = "S";

		if (branchInfo.get("ERROR_DATOS") == null) {
			userInfo = (java.util.Hashtable) 
					users.getUser(
					request.getParameter("teller"),
					request.getParameter("password"), request.getRemoteAddr(),
					txtHorario, pass1, request.getParameter("idNum"), cambio,
					request.getRemoteUser(),
					// "I0242818",
					request.getParameter("band"));

			// Control de Efectivo
			limefe = branchInfo.get("N_LIMITE_EFECTIVO").toString();
			limefe2 = (String) branchInfo.get("N_LIM_EFE_DLLS");
			EfeLim = Long.parseLong(limefe);
			EfeLim2 = Long.parseLong(limefe2);
			session.setAttribute("efectivo", limefe);
			session.setAttribute("efectivo2", limefe2);
			// ----------CONTROL PIF
			String PIFFfactor = "";
			String PIFSfactor = "";
			Vector PIFVector = new Vector();
			DatosVarios ddvv = DatosVarios.getInstance();
			PIFVector = (Vector) ddvv.getDatosV("PIFfactor");
			PIFSfactor = (String) PIFVector.elementAt(0);
			StringTokenizer valores = new StringTokenizer(PIFSfactor, "*");
			String fact = valores.nextToken().toString();
			String time = valores.nextToken().toString();
			String activo = valores.nextToken().toString();
			setMontoPIF(session, EfeLim, fact.toString(), "");
			setMontoPIF(session, EfeLim2, fact.toString(), "2");
			session.setAttribute("ActiveLock", activo);
			// ---------------
		} else {
			session.setAttribute("error", branchInfo.get("ERROR_DATOS"));
			session.setAttribute("teller", request.getParameter("teller"));
			session.setAttribute("empno", request.getParameter("idNum"));
		}

		// Validaciones locales de usuario
		if (!userInfo.isEmpty())
			if (userInfo.get("ERROR_DATOS") != null)
				session.setAttribute("error", userInfo.get("ERROR_DATOS"));
			else {
				session.setAttribute("error", "ok");
				try {
					sucursal = "0" + branchInfo.get("C_NUM_SUCURSAL");
					nivelcajero = (String) userInfo.get("N_NIVEL");
					perfil = (String) userInfo.get("C_PERFIL");
					perfilbase = (String) userInfo.get("C_PERFIL");
					cajero = request.getParameter("teller");
					ventanilla.com.bital.sfb.Login lsb = new ventanilla.com.bital.sfb.Login();
					ventanilla.com.bital.admin.Diario diario = new ventanilla.com.bital.admin.Diario();

					lsb.setTxnCode("0081");
					lsb.setBranch(sucursal);
					lsb.setTeller(request.getParameter("teller"));
					lsb.setSupervisor(request.getParameter("teller"));
					lsb.setCashIn(request.getParameter("horario")
							+ request.getParameter("idNum"));
					lsb.setDescrip("LOGIN");

					String consecliga = gc.getDate(3);

					String msg = diario.invokeDiario(lsb, consecliga,
							request.getRemoteAddr(), "N$", session);
					// String msg = "";

					java.util.Vector items = new java.util.Vector();
					ventanilla.com.bital.util.NSTokenizer parser = new ventanilla.com.bital.util.NSTokenizer(
							msg, "~");
					while (parser.hasMoreElements()) {
						String temp = (String) parser.nextElement();
						if (temp == null)
							continue;
						items.addElement(temp);
					}

					if (!items.elementAt(0).equals("0"))
						session.setAttribute("error", items.elementAt(3));

					// Obtencion de Tipos de Cambio
					ventanilla.ObtainForeignExchange ofe = new ventanilla.ObtainForeignExchange();
					ofe.setBranch(sucursal);
					ofe.setTeller(request.getParameter("teller"));
					ofe.setTellerRegister(request.getParameter("idNum"));
					ofe.doForeignExchanges();
					java.util.Hashtable htOFE = ofe.obtainedExchanges();
					session.setAttribute("foreignExchange", htOFE);
					Vector totales = null;
					if (EfeLim > 0) {
						// Obtencion de totales para Control de Efectivo
						totales = GetTotales("N$", sucursal,
								request.getParameter("teller"),
								request.getParameter("teller"));
						if (!totales.isEmpty()) {
							session.setAttribute("EfeRec", totales.elementAt(0)
									.toString());
							session.setAttribute("EfePag", totales.elementAt(1)
									.toString());
						} else
							System.out
									.println("LoginServlet::Error::No existe informacion de TOTALES!!!! $");
					}

					if (EfeLim2 > 0) {
						totales = GetTotales("US$", sucursal,
								request.getParameter("teller"),
								request.getParameter("teller"));
						if (!totales.isEmpty()) {
							session.setAttribute("EfeRec2", totales
									.elementAt(0).toString());
							session.setAttribute("EfePag2", totales
									.elementAt(1).toString());
						} else
							System.out
									.println("LoginServlet::Error::No existe informacion de TOTALES!!!! US$");
					}
				} catch (Exception e) {
					e.printStackTrace(System.out);
				}
			}
		// Verifica monto limite para Txn 1053
		MtosMax mtos = MtosMax.getInstance();
		Vector LimiteRetiro = mtos.getMtosM("1053*");
		String retiropesos = "000";
		String retirodolar = "000";
		if (!LimiteRetiro.isEmpty()) {
			retiropesos = (String) LimiteRetiro.get(1);
			retirodolar = (String) LimiteRetiro.get(2);
		}
		session.setAttribute("Retiro.Pesos", retiropesos);
		session.setAttribute("Retiro.Dolar", retirodolar);
		ventanilla.com.bital.beans.DatosVarios insIdentidad = ventanilla.com.bital.beans.DatosVarios
				.getInstance();
		String identidad = (String) insIdentidad.getDatosv("IDENTIDAD");
		String identidad1 = (String) insIdentidad.getDatosv("IDENTIDAD1");
		String identidad2 = (String) insIdentidad.getDatosv("IDENTIDAD2");
		String identidad3 = (String) insIdentidad.getDatosv("IDENTIDAD3");

		session.setAttribute("identidadApp", identidad);
		session.setAttribute("identidadApp1", identidad1);
		session.setAttribute("identidadApp2", identidad2);
		session.setAttribute("identidadApp3", identidad3);

		// Almacenar atributos de sesion
		if (!userInfo.isEmpty())
			if (userInfo.get("ERROR_DATOS") == null)
				session.setAttribute("userinfo", userInfo);

		if (!branchInfo.isEmpty())
			if (branchInfo.get("ERROR_DATOS") == null) {
				session.setAttribute("teller", request.getParameter("teller"));
				session.setAttribute("empno", request.getParameter("idNum"));
				session.setAttribute("password",
						request.getParameter("password"));
				session.setAttribute("tellerlevel", nivelcajero);
				session.setAttribute("tellerperfil", perfil);
				session.setAttribute("perfilbase", perfilbase);
				session.setAttribute("branch", sucursal);
				session.setAttribute("dirip", request.getRemoteAddr());
				session.setAttribute("branchinfo", branchInfo);
				session.setAttribute("truncamiento", truncamiento);
				session.setAttribute("plaza", branchInfo.get("D_NUM_PLAZA"));
				session.setAttribute("ciudad", branchInfo.get("D_CIUDAD"));
				session.setAttribute("isForeign",
						branchInfo.get("C_SUC_EXTERIOR"));
				session.setAttribute("numdoctodig",
						branchInfo.get("N_DOCS_DIG"));
				session.setAttribute("horario", request.getParameter("horario"));
				session.setAttribute("AlertCash", "0");
				session.setAttribute("AlertCashDlls", "0");
				session.setAttribute("PIFAlertCash", "0");
				session.setAttribute("PIFAlertCashDlls", "0");
				session.setAttribute("FichaActiva", branchInfo.get("C_FICHA"));

			}

		CheckPerfil(session, perfil, cajero);
		setParametrosCheques(request);
		setTxnWithoutMontoValidate(request);
		session.setAttribute("PRESSTC", "N");
		response.sendRedirect("../main.jsp");

	}

	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

	private void setParametrosCheques(HttpServletRequest request) {
		HttpSession sesion = request.getSession(false);

		try {

			DatosVarios perfiles = DatosVarios.getInstance();

			if (sesion.getAttribute("TxnGrup1") == null
					|| sesion.getAttribute("TxnGrup2") == null
					|| sesion.getAttribute("TxnGrup3") == null) {

				String txnGrup1 = perfiles.getDatosv("TxnGrup1");
				sesion.setAttribute("TxnGrup1", txnGrup1);

				String txnGrup2 = perfiles.getDatosv("TxnGrup2");
				sesion.setAttribute("TxnGrup2", txnGrup2);

				String txnGrup3 = perfiles.getDatosv("TxnGrup3");
				sesion.setAttribute("TxnGrup3", txnGrup3);

				
			}

			String feMaxVigCh = perfiles.getDatosv("feMaxVigCh");
			sesion.setAttribute("feMaxVigCh", feMaxVigCh);

		} catch (Exception exception) {
			System.out
					.println("Error DatosVarios.getInstance()::setParametrosCheques: ["
							+ exception.toString() + "] ");
		}

	}

}
