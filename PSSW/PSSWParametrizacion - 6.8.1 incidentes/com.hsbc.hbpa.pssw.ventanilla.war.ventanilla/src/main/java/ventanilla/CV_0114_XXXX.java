//*************************************************************************************************
//             Funcion: Clase que realiza txn 0114
//            Elemento: CV_0114_XXXX.java
//          Creado por: Alejandro Gonzalez Castro
//		Modificado por: Jes�s Emmanuel L�pez Rosales
//*************************************************************************************************
// CCN - 4360189 - 03/09/2004 - Se cambia invoke al Diario para Control de Efectivo
// CCN - 4360268 - 04/02/2005 - Se realizan adecuaciones para eliminar Agencia NY
// CCN - 4360368 - 02/09/2005 - Se elimina sleep
// CCN - 4360374 - 08/09/2005 - Se genera nuevo paquete a peticion de QA.
// CCN - 4360532 - 20/10/2006 - Se realizan modificaciones para enviar el numero de cajero, consecutivo txn entrada y salida en txn 0821
//*************************************************************************************************
package ventanilla;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import ventanilla.GenericClasses;

import java.util.Calendar;
import java.util.Hashtable;

import ventanilla.com.bital.sfb.CompraVenta;
import ventanilla.com.bital.sfb.OrdenPago;
import ventanilla.com.bital.admin.Diario;

public class CV_0114_XXXX extends HttpServlet {
    private String LiberarOrden( Hashtable data, String cliga, String dir, HttpSession sesion)
    {
        Diario diarioLib = new Diario();
        OrdenPago oTxnOP = new OrdenPago();
        oTxnOP.setOPSucursal((String)data.get("sucursal"));
        oTxnOP.setProcessCode("LIB ");
        String newTemp = (String)data.get( "txtNumOrden" );
        int NumPos = 33;
        int len = newTemp.length();
        for(int i = len; i < NumPos; i++)
            newTemp = newTemp + " ";
        newTemp = newTemp + "0001";
        newTemp = newTemp + (String)data.get( "txtOSN" );
        NumPos = 14;
        for(int i = 0; i < NumPos; i++)
            newTemp = newTemp + " ";

        newTemp = newTemp + (String)data.get("teller") + "  ";
        String newBranch = (String)data.get("sucursal");
        newTemp = newTemp + newBranch.substring(1);
        oTxnOP.setData(newTemp);

        oTxnOP.setOrden((String)data.get("txtNumOrden"));
        oTxnOP.setTeller((String)data.get("teller"));
        oTxnOP.setFromCurr("N$");
        oTxnOP.setBranch((String)data.get("sucursal"));
        oTxnOP.setTranAmt("0");

        String Message = diarioLib.invokeDiario(oTxnOP, cliga, dir, "N$", sesion);
        return Message;
    }

    private String stringFormat2(int option, int sum) {
        Calendar now = Calendar.getInstance();
        String temp = new Integer(now.get(option) + sum).toString();
        if( temp.length() != 2 )
            temp = "0" + temp;

        return temp;
    }

    public void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {
        String TE, TO, Currency;
        int Resultado = -1;

        HttpSession session = req.getSession(false);
        Hashtable datasession = (Hashtable)session.getAttribute("page.datasession");
        GenericClasses cGenericas = new GenericClasses();

        CompraVenta oTxn = new CompraVenta();
        OrdenPago oTxnOP = new OrdenPago();
        oTxnOP.setOPSucursal((String)datasession.get("sucursal"));
        Diario diario = new Diario();
        String sMessage = null;
        String conliga = (String)session.getAttribute("d_ConsecLiga");

        TE = (String)datasession.get("cTxn");
        TO = (String)datasession.get("oTxn");
        Currency = (String)datasession.get("moneda");
        String txtPlaza = (String)datasession.get("txtPlaza");
        String dirip =  (String)session.getAttribute("dirip");
        String msg = null;
        String txtCasoEsp = new String("");
        String txtCadImpresion = new String("");
        String txtSupervisor = (String)datasession.get("teller");
        String txtRegistro = (String)datasession.get("txtRegistro");
        String txtOverride = "0";
        String txtDepFirm = "";
        String Currency2 = "";
        String Monto = "";
        String ConsecutivoIN = "";
        String ConsecutivoOUT = "";        
        if ( Currency.equals("01"))
            Currency2 = "02";
        else
            Currency2 = "01";
        String liberarOrden = new String("");
        if(datasession.get("liberarOrden") != null)
            liberarOrden = datasession.get("liberarOrden").toString();

        if ( (String)datasession.get("supervisor") != null )       //pidio autorizacisn de supervisor
            txtSupervisor =  (String)datasession.get("supervisor");
        
                if ( (String)datasession.get("override") != null )
            if (datasession.get("override").toString().equals("SI"))
                  txtOverride = "3";
        
        if ( datasession.get("posteo") == null ) {
            datasession.put("posteo","1");
            session.setAttribute("page.datasession",(Hashtable)datasession);
        }
        
        if ( session.getAttribute("txtCasoEsp") != null )
            txtCasoEsp = session.getAttribute("txtCasoEsp").toString();    


        oTxn.setTxnCode(TE);
        oTxn.setBranch((String)datasession.get("sucursal"));
        oTxn.setTeller((String)datasession.get("teller"));
        oTxn.setSupervisor(txtSupervisor);
        oTxn.setOverride(txtOverride);

        if(TE.equals("SLOP") && !liberarOrden.equals("SI"))
        {
            oTxnOP.setProcessCode("INQ1");
            String newTemp = (String)datasession.get( "txtNumOrden" );
            int len = newTemp.length();
            for(int i = len; i < 37; i++)
                newTemp = newTemp + " ";
            newTemp = newTemp + "00000000";
            for(int i = 0; i < 14; i++)
                newTemp = newTemp + " ";
            newTemp = newTemp + (String)datasession.get("registro") + " ";
            String newBranch = (String)datasession.get("sucursal");
            newTemp = newTemp + newBranch.substring(1);
            oTxnOP.setData(newTemp);

            oTxnOP.setOrden((String)datasession.get("txtNumOrden"));
            oTxnOP.setTeller((String)datasession.get("teller"));
            oTxnOP.setFromCurr("N$");
            oTxnOP.setBranch((String)datasession.get("sucursal"));
            oTxnOP.setTranAmt("0");

            diario = new Diario();
            sMessage = diario.invokeDiario(oTxnOP, (String)session.getAttribute("d_ConsecLiga"), dirip, "N$", session);
            Resultado = oTxnOP.getStatus();
        }
        else if(TE.equals("0114"))
        {

            //ODCT 0114B 00001000130  000130  000**0114*500000*4456*021*****N$**
            oTxn.setFormat( "B" );
            oTxn.setFromCurr("N$");
            oTxn.setTranAmt( cGenericas.delCommaPointFromString((String)datasession.get( "txtMonto" )) );
            oTxn.setAcctNo( TE );
            oTxn.setReferenc((String)datasession.get("txtNumOrden") );
            oTxn.setFees("021");
            if ( datasession.get("posteo").toString().equals("1") ){
                diario = new Diario();
                sMessage = diario.invokeDiario(oTxn, (String)session.getAttribute("d_ConsecLiga"), dirip, "N$", session);
                
                Resultado = oTxn.getStatus();
                
                Monto = cGenericas.delCommaPointFromString((String)datasession.get("txtMonto"));
                
                if( sMessage.startsWith("0") || sMessage.startsWith("1") ) {
                    txtCasoEsp = txtCasoEsp + TE + "~" + cGenericas.getString(oTxn.getMessage(),1) + "~";
                    ConsecutivoIN = cGenericas.getString(oTxn.getMessage(),1);
                    if (ConsecutivoIN.length()<6){
                   	    ConsecutivoIN = "0" + ConsecutivoIN; 
                    }                    
                }
                if(sMessage.startsWith("1"))
                    liberarOrden = "SI";
            }
            else
                Resultado = 0;
        }

        if ( TE.equals("0114") && Resultado == 0 )
        {   // Cargo fue aceptado
            if ( datasession.get("posteo").toString().equals("1")) {
                 datasession.put("override","NO");
                 datasession.put("posteo","2");
                 txtOverride = "0";
            }

            oTxn = new CompraVenta();
            oTxn.setBranch((String)datasession.get("sucursal"));
            oTxn.setTeller((String)datasession.get("teller"));
            oTxn.setSupervisor(txtSupervisor);
            oTxn.setOverride(txtOverride);
            //ODCT 4111A 00001000130  000130  000**7000001321**50000*US$*0000004456**000**00100100000*500000**ABO.CTA.CHQS.  US$************************************0114**
            oTxn.setTxnCode(TO);
            oTxn.setFormat( "A" );
            oTxn.setAcctNo( (String)datasession.get( "txtDDACuenta" ) );
            oTxn.setCheckNo((String)datasession.get("txtNumOrden"));
            oTxn.setTranAmt( cGenericas.delCommaPointFromString((String)datasession.get( "txtMontoC" )) );
            oTxn.setTranCur( "US$" );
            oTxn.setMoAmoun( cGenericas.delCommaPointFromString((String)datasession.get( "txtMonto" )) );
            oTxn.setTranDesc( "ABO.CTA.CHQS.  US$" );
            String txtTipoCambio = cGenericas.StringFiller((String)datasession.get( "txtTipoCambio" ), ((String)datasession.get( "txtTipoCambio" )).length()+3, false, "0");
            txtTipoCambio = "0000" + txtTipoCambio;
            txtTipoCambio = txtTipoCambio.substring(txtTipoCambio.length()-8);
            oTxn.setDraftAm(txtPlaza + txtTipoCambio);
            oTxn.setDescRev("EMPTY");
            oTxn.setTrNo5( "0114" );

            diario = new Diario();
            sMessage = diario.invokeDiario(oTxn, (String)session.getAttribute("d_ConsecLiga"), dirip, "US$", session);

            if(sMessage.startsWith("1"))
            {
                liberarOrden = "NO";
                String MessageLib = LiberarOrden(datasession, conliga, dirip, session);
                if(MessageLib.startsWith("0"))
                {
                    datasession.put("statusOrden", "LIBERADA");
                    session.setAttribute("page.datasession", datasession);
                    session.setAttribute("page.txnresponse","0~01~000000~" + "OK" + "~");
                }
            }

            Resultado = oTxn.getStatus();

            Monto = cGenericas.delCommaPointFromString((String)datasession.get("txtMonto1"));
            
           if ( sMessage.startsWith("1") ){
               txtCasoEsp = txtCasoEsp + TO + "~" + cGenericas.getString(oTxn.getMessage(),1) + "~";
               ConsecutivoOUT = cGenericas.getString(oTxn.getMessage(),1);
                if (ConsecutivoOUT.length()<6){
                   	ConsecutivoOUT = "0" + ConsecutivoOUT; 
                }               
           }
            if(sMessage.startsWith("0"))
            {

                txtCasoEsp = txtCasoEsp + TO + "~" + cGenericas.getString(oTxn.getMessage(),1) + "~";
                ConsecutivoOUT = cGenericas.getString(oTxn.getMessage(),1);
                if (ConsecutivoOUT.length()<6){
                   	ConsecutivoOUT = "0" + ConsecutivoOUT; 
                }                
                oTxnOP.setTxnCode("CAN ");
                String newTemp = (String)datasession.get( "txtNumOrden" );
                int len = newTemp.length();
                for(int i = len; i < 37; i++)
                    newTemp = newTemp + " ";
                newTemp = newTemp + (String)datasession.get( "txtOSN" );
                newTemp = newTemp + "CAN ";
                newTemp = newTemp + new String(stringFormat2(Calendar.YEAR, 0).substring(3) + stringFormat2(Calendar.MONTH, 1) + stringFormat2(Calendar.DAY_OF_MONTH, 0));
                newTemp = newTemp + new String(stringFormat2(Calendar.HOUR_OF_DAY, 0) + stringFormat2(Calendar.MINUTE, 0));
                newTemp = newTemp + (String)datasession.get("teller") + "  ";
                String sucursal = (String)datasession.get("sucursal");
                if(sucursal.length() > 4)
                    sucursal = sucursal.substring(sucursal.length() - 4);
                newTemp = newTemp + sucursal;
                oTxnOP.setData(newTemp);
                oTxnOP.setOrden((String)datasession.get("txtNumOrden"));
                oTxnOP.setTeller((String)datasession.get("teller"));
                oTxnOP.setBranch((String)datasession.get("sucursal"));
                oTxnOP.setFromCurr("N$");
                oTxnOP.setTranAmt(cGenericas.delCommaPointFromString((String)datasession.get("txtMonto")));
                oTxnOP.setReversable("S");
                oTxnOP.setBanco((String)session.getAttribute("identidadApp"));
                diario = new Diario();
                sMessage = diario.invokeDiario(oTxnOP, (String)session.getAttribute("d_ConsecLiga"), dirip, "N$", session);

                Resultado = 1;
                if(sMessage.startsWith("0"))
                    Resultado = 0;
            }
            else
                liberarOrden = "SI";

        }
        if(liberarOrden.equals("SI"))
        {

            Resultado = 1;
            sMessage = LiberarOrden(datasession, conliga, dirip, session);
            if(sMessage.startsWith("0"))
            {
                datasession.put("statusOrden", "LIBERADA");
                session.setAttribute("page.datasession", datasession);
                session.setAttribute("page.txnresponse","0~01~000000~" + "OK" + "~");
            }
        }
        if (  TE.equals("0114") && Resultado == 0)
        {   // Cargo y Abono Aceptado
            txtCasoEsp = txtCasoEsp + "DSLZ~" + cGenericas.getString(oTxnOP.getMessage(),1) + "~";
            oTxn = new CompraVenta();
            oTxn.setBranch((String)datasession.get("sucursal"));
            oTxn.setTeller((String)datasession.get("teller"));
            oTxn.setSupervisor(txtSupervisor);
            oTxn.setTxnCode("0821");
            oTxn.setFormat( "G" );
            oTxn.setToCurr( "US$" );
            oTxn.setGStatus( "5" );
            if(Currency.equals("02"))
                oTxn.setService( "C" );
            else
                oTxn.setService( "V" );
            String txtAutoriCV = (String)datasession.get("txtAutoriCV");
            if ( txtAutoriCV == null )
                txtAutoriCV = "00000";
            oTxn.setCheckNo( "00000" + txtAutoriCV );
            oTxn.setTranAmt( cGenericas.delCommaPointFromString((String)datasession.get( "txtMonto1" )) );
            String txtTipoCambio = cGenericas.StringFiller((String)datasession.get( "txtTipoCambio" ), ((String)datasession.get( "txtTipoCambio" )).length()+2, false, "0")+ "00";
            txtTipoCambio = "0000" + txtTipoCambio;
            txtTipoCambio = txtTipoCambio.substring(txtTipoCambio.length()-9);
            oTxn.setComAmt(txtTipoCambio);
            oTxn.setIVAAmt( "0" );
            oTxn.setAcctNo( (String)datasession.get( "registro" ) );
            oTxn.setCtaBenef( (String)datasession.get( "txtRegistro" ) );
            int lon = TE.length();
            for(int j=lon;j<10;j++){
               TE = TE + " ";
            }	
            oTxn.setBenefic(TE + ConsecutivoIN + ConsecutivoOUT + (String)datasession.get("teller"));

            diario = new Diario();
            sMessage = diario.invokeDiario(oTxn, (String)session.getAttribute("d_ConsecLiga"), dirip, "US$", session);

            Monto = cGenericas.delCommaPointFromString((String)datasession.get("txtMonto"));
            
            if (sMessage.startsWith("0") || sMessage.startsWith("1") )
                txtCasoEsp = txtCasoEsp + "0821" + "~" + cGenericas.getString(oTxn.getMessage(),1) + "~";
                
            if(sMessage.startsWith("0")){
                //txtCasoEsp = txtCasoEsp + "0821" + "~" + cGenericas.getString(oTxn.getMessage(),1) + "~";
                String txtMonto = cGenericas.delCommaPointFromString((String)datasession.get( "txtMonto" ));
                String txtMonto1 = cGenericas.delCommaPointFromString((String)datasession.get( "txtMonto1" ));

                txtTipoCambio = cGenericas.StringFiller((String)datasession.get( "txtTipoCambio" ), ((String)datasession.get( "txtTipoCambio" )).length()+2, false, "0");
                txtTipoCambio = "0000" + txtTipoCambio;
                txtTipoCambio = txtTipoCambio.substring(txtTipoCambio.length()-7);
                txtTipoCambio = txtTipoCambio.substring(0,3) + "." + txtTipoCambio.substring(3,6);
                String txtDDACuenta2 = (String)datasession.get( "txtDDACuenta" );
                String txtSerial = (String)datasession.get( "txtNumOrden" );
                String txtNomCliente = (String)datasession.get( "txtNomBen" ) + " " + (String)datasession.get( "txtApeBen" );
                txtCadImpresion = txtCadImpresion + "~COMPRAVENTA~" + TE +"-" + TO +"~" + txtNomCliente + "~" +
                txtMonto + "~" + txtTipoCambio + "~" + txtMonto1 + "~" + txtSerial + "~" +
                " " + "~" + txtDDACuenta2 + "~" + "" + "~" + "" + "~"  +
                " " + "~" + " " + "~" + " " + "~" + Currency + "~";

            }
        }

        if(txtCasoEsp.length() > 0)
        {
            session.setAttribute("txtCasoEsp", txtCasoEsp);
        }
        session.setAttribute("txtCadImpresion", sMessage);
        if(txtCadImpresion.length() > 0)
        {
            session.setAttribute("txtCadImpresion", txtCadImpresion);
        }
        session.setAttribute("page.txnresponse", sMessage);

        getServletContext().getRequestDispatcher("/servlet/ventanilla.Redirector").forward(req,resp);
    }

    public void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doPost(req, resp);
    }
}
