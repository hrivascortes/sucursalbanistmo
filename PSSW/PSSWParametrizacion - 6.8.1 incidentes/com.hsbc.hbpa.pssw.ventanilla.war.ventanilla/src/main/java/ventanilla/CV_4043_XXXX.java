//*************************************************************************************************
//             Funcion: Clase que realiza txn 4043
//            Elemento: CV_4043_XXXX.java
//          Creado por: Alejandro Gonzalez Castro
//		Modificado por: Fredy Pe�a Moreno
//*************************************************************************************************
// CCN - 4360189 - 03/09/2004 - Se cambia invoke al Diario para Control de Efectivo
// CCN - 4360268 - 04/02/2005 - Se realizan adecuaciones para eliminar Agencia NY
// CCN - 4360368 - 02/09/2005 - Se elimina sleep
// CCN - 4360374 - 08/09/2005 - Se genera nuevo paquete a peticion de QA.
// CCN - 4360437 - 24/02/2006 - Se realizan correcciones para descripci�n de reverso
// CCN - 4360456 - 07/04/2006 - Se realiza correcci�n a impresion de Giro Citibank con cta libre de comision
// CCN - 4360532 - 20/10/2006 - Se realizan modificaciones para enviar el numero de cajero, consecutivo txn entrada y salida en txn 0821
// CCN - 4620048 - 14/12/2007 - Se realizan modificaciones para compraventa
//*************************************************************************************************

package ventanilla;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import ventanilla.GenericClasses;

import java.util.Calendar;
import java.util.Hashtable;

import ventanilla.com.bital.sfb.CompraVenta;
import ventanilla.com.bital.admin.Diario;
import ventanilla.com.bital.sfb.OrdenPago;

public class CV_4043_XXXX extends HttpServlet {

    private String stringFormat2(int option, int sum) {
        Calendar now = Calendar.getInstance();
        String temp = new Integer(now.get(option) + sum).toString();
        if( temp.length() != 2 )
            temp = "0" + temp;

        return temp;
    }

    private String setCommaToString(String newCadNum) {
        int iPIndex = newCadNum.indexOf(","), iLong;
        String szTemp;

        if(iPIndex > 0) {
            newCadNum = new String(newCadNum + "00");
            newCadNum = newCadNum.substring(0, iPIndex + 1) + newCadNum.substring(iPIndex + 1, iPIndex + 3);
        }
        else {
            for(int i = newCadNum.length(); i < 3; i++)
                newCadNum = "0" + newCadNum;
            iLong = newCadNum.length();
            if(iLong == 3)
                szTemp = newCadNum.substring(0, 1);
            else
                szTemp = newCadNum.substring(0, iLong - 2);
            newCadNum = szTemp + "," + newCadNum.substring(iLong - 2);
        }

        return newCadNum;
    }

    private String setFormat(String newCadNum) {
        int iPIndex = newCadNum.indexOf(".");
        String nCadNum = new String(newCadNum.substring(0, iPIndex));
        for (int i = 0; i < (int)((nCadNum.length()-(1+i))/3); i++)
            nCadNum = nCadNum.substring(0, nCadNum.length()-(4*i+3))+','+nCadNum.substring(nCadNum.length()-(4*i+3));

        return nCadNum + newCadNum.substring(iPIndex, newCadNum.length());
    }

    private String delLeadingfZeroes(String newString){
        StringBuffer newsb = new StringBuffer(newString);
        int i = 0;
        boolean flag=true;
          while(flag && newsb.length()>0){
            if(newsb.charAt(i)=='0')
            newsb.deleteCharAt(0);
            else
              flag=false;
          }
         //System.out.println("newsb: "+newsb);
        return newsb.toString();
    }

    private String setPointToString(String newCadNum) {
        int iPIndex = newCadNum.indexOf("."), iLong;
        String szTemp;

        if(iPIndex > 0) {
            newCadNum = new String(newCadNum + "00");
            newCadNum = newCadNum.substring(0, iPIndex + 1) + newCadNum.substring(iPIndex + 1, iPIndex + 3);
        }
        else {
            for(int i = newCadNum.length(); i < 3; i++)
                newCadNum = "0" + newCadNum;
            iLong = newCadNum.length();
            if(iLong == 3)
                szTemp = newCadNum.substring(0, 1);
            else
                szTemp = newCadNum.substring(0, iLong - 2);
            newCadNum = szTemp + "." + newCadNum.substring(iLong - 2);
        }

        return newCadNum;
    }

    public void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {
    	GenericClasses cGenericas = new GenericClasses();
        String time = cGenericas.stringFormat(Calendar.HOUR_OF_DAY,2) + cGenericas.stringFormat(Calendar.MINUTE,2);
        String TE, TO, Currency,Currency2,Divisa,Divisa2;
        int Resultado = -1;
        

        CompraVenta oTxn = new CompraVenta();
        OrdenPago oTxnOP = new OrdenPago();
        Diario diario;
        String sMessage = null;

        HttpSession session = req.getSession(false);
        Hashtable datasession = (Hashtable)session.getAttribute("page.datasession");
        oTxnOP.setOPSucursal((String)datasession.get("sucursal"));
        TE = (String)datasession.get("cTxn");
        TO = (String)datasession.get("oTxn");
        Currency = (String)datasession.get("moneda");
		Currency2 = (String)session.getAttribute("page.moneda_s");
        String txtPlaza = (String)datasession.get("txtPlaza");
        String dirip =  (String)session.getAttribute("dirip");
        String msg = null;
        String txtCasoEsp = new String("");
        String txtCadImpresion = new String("");
        String txtNomCliente = new String("");
        String txtSupervisor = (String)datasession.get("teller");
        String txtRegistro = (String)datasession.get("txtRegistro");
        String txtOverride = "0";
        //String Currency2 = "";
        String txtComisionGiro = "";
        String txtIvaGiro = "";
        String txtNumOrden = "0";
        String Monto = "";
        String ConsecutivoIN = "";
        String ConsecutivoOUT = "";
		Divisa = cGenericas.getDivisa(Currency);
		Divisa2 = cGenericas.getDivisa(Currency2);        
        /*if ( Currency.equals("01"))
            Currency2 = "02";
        else
             Currency2 = "01";*/
        
        if ( (String)datasession.get("override") != null )
            if (datasession.get("override").toString().equals("SI"))
                  txtOverride = "3";
        
        if ( (String)datasession.get("supervisor")  != null )                      //pidio autorizacisn de supervisor
            txtSupervisor =  (String)datasession.get("supervisor");

        
        if ( datasession.get("posteo") == null ) {
            datasession.put("posteo","1");
            session.setAttribute("page.datasession",(Hashtable)datasession);
        }
        
        if ( session.getAttribute("txtCasoEsp") != null )
            txtCasoEsp = session.getAttribute("txtCasoEsp").toString();
        
        if ( datasession.get("txtNomCliente") != null ) {
            txtNomCliente = datasession.get("txtNomCliente").toString();
        }
        
        oTxn.setTxnCode(TE);
        oTxn.setBranch((String)datasession.get("sucursal"));
        oTxn.setTeller((String)datasession.get("teller"));
        oTxn.setSupervisor(txtSupervisor);
        oTxn.setOverride("3");

        if( TE.equals( "4043" ) )
        {
            //ODCT 4043A 00001000131  000131  000**4000000000**9470*N$*****00100094700*1000*0*CARGO P/COMPRA DE DOLARES************************************0106**
            //ODCT 4043A 00001000131  000131  000**7000000000**1085*US$*****00100092200*10000*0000000*CARGO P/VENTA DE DOLARES************************************0106**
            //ODCT 4043A 00001000131  000131  000**4000000000**10000*N$*****00100094700*1056*0000000*CARGO P/ABO CTA US$************************************4111**
            //ODCT 4043A 00001000131  000131  000**7000000000**1085*US$*****00100092200*10000*0000000*CARGO P/ABO CTA MN************************************4111**
            //ODCT 4043A 00001000130  000130  000**4000000000**600000*N$*0710406001****00100100000*60000*0000000*CARGO P/EXP.CHQ.CAJA  US$************************************4115**
            //ODCT 4043A 00001000131  000131  000**7000001321**600000*US$*****00100100000*6000000*0000000*CARGO P/EXP.ORD.PAGO BITAL  M.N.************************************0120**
            //ODCT 4043A 00001000131  000131  000**7000001321**200000*US$*****00100100000*2000000*0000000*CARGO P/EXP.ORD.PAGO OT-BCO  M.N.************************************0122**
            if ( datasession.get("posteo").toString().equals("1")) {
                oTxn.setFormat( "A" );
                oTxn.setAcctNo( (String)datasession.get( "txtDDACuenta" ) );
                String txtTipoCambio =	cGenericas.StringFiller((String)datasession.get( "txtTipoCambio" ), ((String)datasession.get( "txtTipoCambio" )).length()+3, false, "0");
                if(Currency.equals("01")) {
                    oTxn.setTranAmt( cGenericas.delCommaPointFromString((String)datasession.get("txtMonto")) );
                   // oTxn.setTranCur( "N$" );
                    oTxn.setMoAmoun( cGenericas.delCommaPointFromString((String)datasession.get( "txtMontoC" )) );
                }
                else {
                    oTxn.setTranAmt( cGenericas.delCommaPointFromString((String)datasession.get( "txtMonto1" )) );
                   // oTxn.setTranCur( "US$" );
                    oTxn.setMoAmoun( cGenericas.delCommaPointFromString((String)datasession.get( "txtMontoC" )) );
                }
                oTxn.setTranCur(cGenericas.getDivisa(Currency));
                
                String desc = "";
                String descrev = "";
                if(Currency.equals("01")) {
                    if(TO.equals("0106")) {
                        desc = "CARGO P/COMPRA DE DOLARES";
                    }
                    else if(TO.equals("4111")) {
                        desc = "CARGO P/ABO CTA US$";
                    }
                    else if(TO.equals("4115")) {
                        desc = "CARGO P/EXP.CHQ.CAJA  US$";
                    }
                    else if(TO.equals("0116")) {
                        desc = "CARGO P/EXP.GIRO CITIBANK  US$";
                    }
                    else if(TO.equals("0778")) {
                        desc = "CARGO P/EXP.CHQ.VIAJERO  US$";
                    }
                    else if(TO.equals("0786")) {
                        desc = "CARGO P/EXP.ORD.PAGO US$";
                        oTxn.setReversable("N");
                    }
                }
                else {
                    if(TO.equals("0106")) {
                        desc = "CARGO P/VENTA DE DOLARES";
                    }
                    else if(TO.equals("4111")) {
                        desc = "CARGO P/ABO CTA M.N.";
                    }
                    else if(TO.equals("4115")) {
                        desc = "CARGO P/EXP.CHQ.CAJA  M.N.";
                    }
                    else if(TO.equals("4113")) {
                        desc = "CARGO P/ABO.CTA.INVERS.  M.N.";
                    }
                    else if(TO.equals("0120")) {
                        desc = "CARGO P/EXP.ORD.PAGO "+(String)session.getAttribute("identidadApp")+"  M.N.";
                    }
                    else if(TO.equals("0122")) {
                        desc = "CARGO P/EXP.ORD.PAGO OT-BCO  M.N.";
                    }
            }

                oTxn.setTranDesc(desc);
                oTxn.setDescRev("EMPTY");
                
              /*  if(TO.equals("4111")) {
                    String CF = (String)datasession.get("lstCF");
                    if (CF.equals("1")) {
                        String RFC = (String)datasession.get("txtRFC");
                        int longitud = RFC.length();
                        for (int i=longitud; i<19; i++) {
                            RFC = RFC + " ";
                        }
                        String IVA = (String)datasession.get("txtIVA");
                        longitud = IVA.length();
                        for (int i=longitud; i<14; i++) {
                            IVA = " " + IVA;
                        }
                        oTxn.setTranDesc("-CGO "+ RFC + " " + IVA);
                        descrev = "REV. -CGO "+ RFC + " " + IVA;
                        if(descrev.length() > 20)
                            descrev = descrev.substring(0,20);
                    }
                    /*if(descrev.length() > 25)  
                        descrev = descrev.substring(0,25);*/
                    // SE AGREGA EMPTY PARA CORRECCI�N DE MENSAJE DE REVERSO CORRECCI�N 4043-4111
               //     descrev ="EMPTY";    
               //     oTxn.setDescRev(descrev);
               // }
                if ( TO.equals("4115"))
                    oTxn.setCheckNo((String)datasession.get("txtSerial"));
                else if ( TO.equals("0116"))
                    oTxn.setCheckNo("0" + (String)datasession.get("txtSerial5"));
                
                txtTipoCambio = "0000" + txtTipoCambio;
                txtTipoCambio = txtTipoCambio.substring(txtTipoCambio.length()-8);
                oTxn.setDraftAm(txtPlaza + txtTipoCambio);
                oTxn.setFees( "0" );
                oTxn.setTrNo5(TO);
                oTxn.setOverride(txtOverride);
                
                diario = new Diario();
                sMessage = diario.invokeDiario(oTxn, (String)session.getAttribute("d_ConsecLiga"), dirip, Currency, session);
                Resultado = oTxn.getStatus();
                if ( Resultado == 0 || Resultado == 1){
                    txtCasoEsp = txtCasoEsp + TE + "~" + cGenericas.getString(oTxn.getMessage(),1) + "~";
                    ConsecutivoIN = cGenericas.getString(oTxn.getMessage(),1);
                    if (ConsecutivoIN.length()<6){
                   	    ConsecutivoIN = "0" + ConsecutivoIN; 
                    }                    
                }
                if ( Currency.equals("01"))
                    Monto = cGenericas.delCommaPointFromString((String)datasession.get("txtMonto"));
                else
                    Monto = cGenericas.delCommaPointFromString((String)datasession.get("txtMonto1"));
                
                if(sMessage.startsWith("0")) {
                    txtNomCliente = cGenericas.getPositionalString(oTxn.getMessage(),54,32, "NO");
                    txtNomCliente = txtNomCliente.replace('*',' ');
                    if ( TO.equals("0786") )
                        txtNumOrden = cGenericas.getPositionalString(oTxn.getMessage(),91,10, "NO");
                    if ( TO.equals("0116") ) {
                        String txtComisionIva = cGenericas.getPositionalString(oTxn.getMessage(),247,30, "NO");
                        txtComisionGiro = setFormat(setPointToString(delLeadingfZeroes(txtComisionIva.substring(0,15))));
                        txtIvaGiro = setFormat(setPointToString(delLeadingfZeroes(txtComisionIva.substring(15,txtComisionIva.length()))));
                    }
                    datasession.put("txtNomCliente","1");
                    session.setAttribute("page.datasession",(Hashtable)datasession);
                }
            }
            else 
                Resultado = 0;
        }

        if ( TE.equals("4043") && Resultado == 0 )
        {   // Cargo fue aceptado
            if ( datasession.get("posteo").toString().equals("1")) {
                 datasession.put("override","NO");
                 datasession.put("posteo","2");
                 txtOverride = "0";
            }


            oTxn = new CompraVenta();
            oTxn.setBranch((String)datasession.get("sucursal"));
            oTxn.setTeller((String)datasession.get("teller"));
            oTxn.setSupervisor(txtSupervisor);
            if( TO.equals("0106"))
            {
                //ODCT 0106B 00001000131  000131  000**0106*1000*1*0*0*0**1000*US$**
                //ODCT 0106B 00001000131  000131  000**0106*10000*1*0*0*0**10000*N$**
                oTxn.setTxnCode("0106");
                oTxn.setFormat("B");
                oTxn.setAcctNo( "0106" );
                oTxn.setTranAmt( cGenericas.delCommaPointFromString((String)datasession.get( "txtMontoC" )) );
                oTxn.setCashOut( cGenericas.delCommaPointFromString((String)datasession.get( "txtMontoC" )) );
               /* if(Currency.equals("01"))
                    oTxn.setFromCurr("US$");
                else
                    oTxn.setFromCurr("N$");*/
                oTxn.setFromCurr(cGenericas.getDivisa(Currency2));  
                oTxn.setReferenc("1");
            }
            else if ( TO.equals("4111") )
            {
                //ODCT 4111A 00001000131  000131  000**7000000000**1056*US$***000**01400094700*10000**ABO.CTA.CHQS.  US$************************************4043**
                //ODCT 4111A 00001000131  000131  000**4000000000**10000*N$***000**0140009220*1085**ABO.CTA.CHQS.  M.N.************************************4043**
                oTxn.setTxnCode("4111");
                oTxn.setFormat( "A" );
                oTxn.setAcctNo( (String)datasession.get( "txtDDACuenta1" ) );
                oTxn.setOverride(txtOverride);
                oTxn.setTranAmt( cGenericas.delCommaPointFromString((String)datasession.get( "txtMontoC" )) );
                if(Currency.equals("01"))
                {
                 //   oTxn.setTranCur( "US$" );
                    oTxn.setMoAmoun( cGenericas.delCommaPointFromString((String)datasession.get( "txtMonto" )) );
                  //  oTxn.setTranDesc( "ABO.CTA.CHQS.  US$" );
                }
                else {
                    //oTxn.setTranCur( "N$" );
                    oTxn.setMoAmoun( cGenericas.delCommaPointFromString((String)datasession.get( "txtMonto1" )) );
                 //   oTxn.setTranDesc( "ABO.CTA.CHQS.  M.N." );
                }
                oTxn.setTranCur(cGenericas.getDivisa(Currency2));
                oTxn.setTranDesc( "ABO.CTA.CHQS.  "+cGenericas.getDivisa(Currency2));  
                oTxn.setCashIn( "000" );
                String txtTipoCambio = cGenericas.StringFiller((String)datasession.get( "txtTipoCambio" ), ((String)datasession.get( "txtTipoCambio" )).length()+3, false, "0");
                txtTipoCambio = "0000" + txtTipoCambio;
                txtTipoCambio = txtTipoCambio.substring(txtTipoCambio.length()-8);
                oTxn.setDraftAm(txtPlaza + txtTipoCambio);
                oTxn.setFees( "0" );
                oTxn.setDescRev("EMPTY");
            }
            else if ( TO.equals("4115") )
            {//NO SE TOCAN
                //ODCT 4115A 00001000130  000130  000**3902222222**600.00*US$*0710406001****00100100000*6000.00**EDWIN SANCHEZ******4000000000******************************4043**
                oTxn.setTxnCode("4115");
                oTxn.setFormat( "A" );
                oTxn.setOverride(txtOverride);
                if(Currency.equals("01")) {
                    oTxn.setAcctNo( "3902222222" );
                    oTxn.setTranCur( "US$" );
                    oTxn.setMoAmoun( cGenericas.delCommaPointFromString((String)datasession.get( "txtMonto" )) );
                }
                else {
                    oTxn.setAcctNo( "0103000023" );
                    oTxn.setTranCur( "N$" );
                    oTxn.setMoAmoun( cGenericas.delCommaPointFromString((String)datasession.get( "txtMonto1" )) );
                }
                oTxn.setTranAmt( cGenericas.delCommaPointFromString((String)datasession.get( "txtMontoC" )) );
                oTxn.setCheckNo( (String)datasession.get( "txtSerial" ) );
                oTxn.setTranDesc( (String)datasession.get( "txtBeneficiario" ) );
                oTxn.setAcctNo1( (String)datasession.get( "txtDDACuenta" ) );
                String txtTipoCambio = 	cGenericas.StringFiller((String)datasession.get( "txtTipoCambio" ), ((String)datasession.get( "txtTipoCambio" )).length()+3, false, "0");
                txtTipoCambio = "0000" + txtTipoCambio;
                txtTipoCambio = txtTipoCambio.substring(txtTipoCambio.length()-8);
                oTxn.setDraftAm(txtPlaza + txtTipoCambio);
                oTxn.setDescRev("EMPTY");
            }
            else if ( TO.equals("0116") )
            {
                oTxn.setTxnCode(TO);
                oTxn.setFormat( "G" );
                oTxn.setService("WCH");
                oTxn.setToCurr( "US$" );
                oTxn.setCheckNo( "0" + (String)datasession.get( "txtSerial5" ));
                oTxn.setGStatus("1");
                oTxn.setBcoTrnsf( "021000089" );
                oTxn.setTranAmt( cGenericas.delCommaPointFromString((String)datasession.get( "txtMonto1" )) );
                oTxn.setComAmt( "0" );
                oTxn.setIVAAmt( "0" );
                oTxn.setAcctNo( (String)datasession.get( "txtDDACuenta" ) );
                oTxn.setCtaBenef( txtRegistro );
                oTxn.setBenefic( (String)datasession.get("txtBeneficiario") );
                oTxn.setOrdenan( txtNomCliente );
            }
            else if ( TO.equals("0778") )
            {
                oTxn.setTxnCode(TO);
                oTxn.setFormat( "B" );
                oTxn.setAcctNo( "0778" );
                oTxn.setTranAmt( cGenericas.delCommaPointFromString((String)datasession.get( "txtMonto1" )) );
                String txtTipoCambio = cGenericas.StringFiller((String)datasession.get( "txtTipoCambio" ), ((String)datasession.get( "txtTipoCambio" )).length()+3, false, "0");
                txtTipoCambio = "0000" + txtTipoCambio;
                txtTipoCambio = txtTipoCambio.substring(txtTipoCambio.length()-10);
                oTxn.setReferenc( "00400" + (String)datasession.get( "txtFolioSerb" ) + txtTipoCambio);
                oTxn.setFees("400" + (String)datasession.get( "txtFolioSerb" ));
                oTxn.setCashOut(cGenericas.delCommaPointFromString((String)datasession.get("txtMonto1")));
                oTxn.setFromCurr("US$");

            }
            else if ( TO.equals("0786") )
            {
                oTxn.setTxnCode(TO);
                oTxn.setFormat( "B" );
                oTxn.setAcctNo((String)datasession.get( "txtDDACuenta" ));
                oTxn.setTranAmt(cGenericas.delCommaPointFromString((String)datasession.get( "txtMonto1" )));
                oTxn.setReferenc((String)datasession.get( "txtBeneficiario" ));
                oTxn.setFees(txtNumOrden);
                oTxn.setFromCurr("US$");
                oTxn.setReversable("N");
            }
            else if ( TO.equals("4113") )
            {
                oTxn.setFormat( "A" );
                oTxn.setTxnCode(TO);
                oTxn.setOverride(txtOverride);
                oTxn.setAcctNo( (String)datasession.get( "txtCDACuenta" ) );
                oTxn.setTranAmt( cGenericas.delCommaPointFromString((String)datasession.get( "txtMonto" )) );
                oTxn.setTranCur( "N$" );
                oTxn.setMoAmoun( cGenericas.delCommaPointFromString((String)datasession.get( "txtMonto1" )) );
                String txtTipoCambio = cGenericas.StringFiller((String)datasession.get( "txtTipoCambio" ), ((String)datasession.get( "txtTipoCambio" )).length()+3, false, "0");
                txtTipoCambio = "0000" + txtTipoCambio;
                txtTipoCambio = txtTipoCambio.substring(txtTipoCambio.length()-8);
                oTxn.setDraftAm( txtPlaza + txtTipoCambio );
                oTxn.setTranDesc( "ABO.CTA.INVERS.  M.N." );
                oTxn.setDescRev("EMPTY");
            }
            else if ( TO.equals("0120") || TO.equals("0122") )
            {
                //ODCT 0120B 00001000131  000131  000**0120*6000000*1*021*0*0***N$**
                //ODCT 0122B 00001000131  000131  000**0122*2000000*1*007*0*0***N$**
                oTxn.setTxnCode(TO);
                oTxn.setFormat( "B" );
                oTxn.setAcctNo(TO);
                oTxn.setTranAmt(cGenericas.delCommaPointFromString((String)datasession.get( "txtMonto" )));
                oTxn.setReferenc("1");
                if ( TO.equals("0122") )
                    oTxn.setFees((String)datasession.get( "lstBanco" ));
                else
                    oTxn.setFees("021");
                oTxn.setFromCurr("N$");
            }//<-- HASTA AQUI FPM

            if ( TO.equals("4111") || TO.equals("4115") || TO.equals("4113"))
                oTxn.setTrNo5( "4043" );

            diario = new Diario();
            sMessage = diario.invokeDiario(oTxn, (String)session.getAttribute("d_ConsecLiga"), dirip, Currency2, session);
            session.setAttribute("page.datasession",(Hashtable)datasession);
            Resultado = oTxn.getStatus();

            if ( Currency2.equals("01"))
                Monto = cGenericas.delCommaPointFromString((String)datasession.get("txtMonto"));
            else
                Monto = cGenericas.delCommaPointFromString((String)datasession.get("txtMonto1"));
            
            if ( Resultado == 0 || Resultado == 1){ 
                txtCasoEsp = txtCasoEsp + TO + "~" + cGenericas.getString(oTxn.getMessage(),1) + "~";
                ConsecutivoOUT = cGenericas.getString(oTxn.getMessage(),1);
                if (ConsecutivoOUT.length()<6){
                   	ConsecutivoOUT = "0" + ConsecutivoOUT; 
                }                
            }
            if(sMessage.startsWith("0"))
            {
                if ( TO.equals("0120") || TO.equals("0122") )
                {
                    // OrdenPago oTxnOP = new OrdenPago();
                    oTxnOP.setProcessCode("EXP ");
                    String date = new String(stringFormat2(Calendar.YEAR, 0).substring(3) + stringFormat2(Calendar.MONTH, 1) + stringFormat2(Calendar.DAY_OF_MONTH, 0));
                    oTxnOP.setField32( date, "MXN", setCommaToString( cGenericas.delCommaPointFromString((String)datasession.get( "txtMonto" ) ) ) );
                    oTxnOP.setField14a((String)datasession.get( "txtApeOrd" ));
                    oTxnOP.setField14n((String)datasession.get( "txtNomOrd" ));
                    oTxnOP.setField12a((String)datasession.get( "txtApeBen" ));
                    oTxnOP.setField12n((String)datasession.get( "txtNomBen" ));
                    oTxnOP.setField27("EXP");
                    oTxnOP.setField17f(date);
                    oTxnOP.setField17t(time);
                    oTxnOP.setTeller((String)datasession.get("teller"));
                    oTxnOP.setBranch((String)datasession.get("sucursal"));
                    oTxnOP.setFromCurr("N$");

                    // datos para la respuesta del diario
                    oTxnOP.setNomOrd((String)datasession.get( "txtNomOrd" ));
                    oTxnOP.setApeOrd((String)datasession.get( "txtApeOrd" ));
                    oTxnOP.setNomBen((String)datasession.get( "txtNomBen" ));
                    oTxnOP.setApeBen((String)datasession.get( "txtApeBen" ));
                    oTxnOP.setTranAmt(cGenericas.delCommaPointFromString((String)datasession.get("txtMonto")));
                    oTxnOP.setBanco((String)session.getAttribute("identidadApp"));
                    oTxnOP.setReversable("S");

                    if( TO.equals("0122") )
                    {
                        oTxnOP.setField12e((String)datasession.get( "txtCiudad" ));
                        String szBanco = (String)datasession.get( "lstBanco" );
                        if(szBanco.length() != 4)
                            szBanco = "0" + szBanco;
                        oTxnOP.setField25n(szBanco);
                        oTxnOP.setField28((String)datasession.get( "txtInstrucc" ));
                        oTxnOP.setBanco("OTROS BANCOS");
                    }
                    oTxnOP.setTranAmt(cGenericas.delCommaPointFromString((String)datasession.get("txtMonto")));
                    oTxnOP.setReversable("S");

                    diario = new Diario();
                    sMessage = diario.invokeDiario(oTxnOP, (String)session.getAttribute("d_ConsecLiga"), dirip, "N$", session);
                    Resultado = 1;
                    if(sMessage.startsWith("0"))
                    {
                        Resultado = 0;
                        String monto = cGenericas.delCommaPointFromString((String)datasession.get( "txtMonto" ));
                        String comision = "";
                        String iva = "";
                        String total = "" ;
                        String ordenante = (String)datasession.get( "txtNomOrd" ) + " " + (String)datasession.get( "txtApeOrd" );
                        String beneficiario = (String)datasession.get( "txtNomBen" ) + " " + (String)datasession.get( "txtApeBen" );
                        txtCasoEsp = txtCasoEsp + "DSLZ~" + cGenericas.getString(oTxnOP.getMessage(),1) + "~";
                        txtCadImpresion = txtCadImpresion + "~CARGO~N$~" + oTxnOP.getOrden() + "~";
                        txtCadImpresion = txtCadImpresion + monto + "~" + comision + "~" + iva + "~" + monto + "~";
                        txtCadImpresion = txtCadImpresion + beneficiario + "~" + ordenante + "~" + datasession.get( "txtDDACuenta" ).toString() + "~" + ordenante;
                        String thisBank = "";
                        if ( TO.equals("0122") )
                            thisBank = "OTROS BANCOS";
                        txtCadImpresion = txtCadImpresion + "~" + thisBank + "~" + "" + "~" + "" + "~" + "" + "~" + Divisa + "~" + Divisa2 + "~";
                    }
                }
            }
        }

        //ODCT 0821G 00001000131  000131  000**V*US$*0000000000*1**1000*009470000*0*0431866*0431866*4000000000***
        //ODCT 0821G 00001000131  000131  000**C*US$*0000000000*5**1085*009220000*0*431866*0431866*7000000000***
        //ODCT 0821G 00001000131  000131  000**V*US$*0000000000*5**1056*009470000*0*0431866*0431866*4000000000***
        //ODCT 0821G 00001000131  000131  000**C*US$*0000000000*5**1085*009220000*0*0431866*0431866*7000000000***
        if (  TE.equals("4043") && Resultado == 0)
        {   // Cargo y Abono Aceptado

            oTxn = new CompraVenta();
            oTxn.setBranch((String)datasession.get("sucursal"));
            oTxn.setTeller((String)datasession.get("teller"));
            oTxn.setSupervisor(txtSupervisor);
            oTxn.setTxnCode("0821");
            oTxn.setFormat( "G" );
            String monedacv="";
            if(Currency.equals("01")){
            	monedacv= Currency2;            
            }else{
            	monedacv=Currency;
            }
            //oTxn.setToCurr(monedacv);
            oTxn.setToCurr(cGenericas.getDivisa(monedacv));
//            if(Currency.equals("02")){
            if(Integer.parseInt(Currency)>1){
                oTxn.setService( "C" );
                oTxn.setGStatus( "5" );
            }
            else {
                oTxn.setService( "V" );
                if ( TO.equals("0106"))
                    oTxn.setGStatus( "1" );
                else if (TO.equals("4111"))
                    oTxn.setGStatus( "5" );
                else if (TO.equals("4115") || TO.equals("4113"))
                    oTxn.setGStatus( "6" );
                else if (TO.equals("0778"))
                    oTxn.setGStatus( "4" );
                else if (TO.equals("0786"))
                {
                    oTxn.setGStatus( "3" );
                    oTxn.setReversable("N");
                }
                else if (TO.equals("0116"))
                    oTxn.setGStatus( "2" );
            }

            String txtAutoriCV = (String)datasession.get("txtAutoriCV");
            if ( txtAutoriCV == null )
                txtAutoriCV = "00000";
            oTxn.setCheckNo( "00000" + txtAutoriCV );
            oTxn.setTranAmt( cGenericas.delCommaPointFromString((String)datasession.get( "txtMonto1" )) );
            String txtTipoCambio = cGenericas.StringFiller((String)datasession.get( "txtTipoCambio" ), ((String)datasession.get( "txtTipoCambio" )).length()+2, false, "0") + "00";
            txtTipoCambio = "0000" + txtTipoCambio;
            txtTipoCambio = txtTipoCambio.substring(txtTipoCambio.length()-9);
            oTxn.setComAmt(txtTipoCambio);
            oTxn.setIVAAmt( cGenericas.delCommaPointFromString((String)datasession.get("txtMonto")));
            oTxn.setAcctNo( (String)datasession.get( "registro" ) );
            oTxn.setCtaBenef( (String)datasession.get( "txtRegistro" ) );   
            if(Integer.parseInt(Currency)>1) {
            	oTxn.setBenefic("4043"+TO+"  " + ConsecutivoIN + ConsecutivoOUT + (String)datasession.get("teller"));
            }else{
            	oTxn.setBenefic(TO+"4043  " + ConsecutivoIN + ConsecutivoOUT + (String)datasession.get("teller"));
            }
            //oTxn.setAcctNo( (String)datasession.get( "registro" ) );
            //oTxn.setCtaBenef( (String)datasession.get( "txtRegistro" ) );
            //String cadtemp = (String)datasession.get("txtDDACuenta");
            //int lon = cadtemp.length();
            //for(int j=lon;j<10;j++){
            //   cadtemp = cadtemp + " ";
            //}	
            //oTxn.setBenefic(cadtemp + ConsecutivoIN + ConsecutivoOUT + (String)datasession.get("teller"));

            diario = new Diario();
            sMessage = diario.invokeDiario(oTxn, (String)session.getAttribute("d_ConsecLiga"), dirip, monedacv, session);
            Monto = cGenericas.delCommaPointFromString((String)datasession.get("txtMonto"));
            
            if (sMessage.startsWith("0") || sMessage.startsWith("1"))
               txtCasoEsp = txtCasoEsp + "0821" + "~" + cGenericas.getString(oTxn.getMessage(),1) + "~";
                            
            if(sMessage.startsWith("0"))
            {
                if ( !TO.equals("0778") && !TO.equals("0116") && !TO.equals("0120") && !TO.equals("0122") && !TO.equals("0786"))
                {
                    String txtMonto = cGenericas.delCommaPointFromString((String)datasession.get( "txtMonto" ));
                    String txtMonto1 = cGenericas.delCommaPointFromString((String)datasession.get( "txtMonto1" ));
                    txtTipoCambio = cGenericas.StringFiller((String)datasession.get( "txtTipoCambio" ), ((String)datasession.get( "txtTipoCambio" )).length()+2, false, "0");
                    txtTipoCambio = "0000" + txtTipoCambio;
                    txtTipoCambio = txtTipoCambio.substring(txtTipoCambio.length()-7);
                    txtTipoCambio = txtTipoCambio.substring(0,3) + "." + txtTipoCambio.substring(3,6);
                    String txtDDACuenta1 = (String)datasession.get( "txtDDACuenta" );
                    String txtDDACuenta2 = (String)datasession.get( "txtDDACuenta1" );
                    String txtSerial = (String)datasession.get( "txtSerial" );
                    String txtBeneficiario = (String)datasession.get( "txtBeneficiario" );
                    if ( txtDDACuenta2 == null )
                        txtDDACuenta2 = (String)datasession.get( "txtCDACuenta" );
                    txtCadImpresion = txtCadImpresion + "~COMPRAVENTA~" + TE +"-" + TO +"~" + txtNomCliente + "~" +
                    txtMonto + "~" + txtTipoCambio + "~" + txtMonto1 + "~" + txtSerial + "~" +
                    txtDDACuenta1 + "~" + txtDDACuenta2 + "~" + "" + "~" + txtBeneficiario + "~"  +
                    " " + "~" + " " + "~" + " " + "~" + Currency + "~" + Divisa + "~" + Divisa2 + "~";
                }
                else if (TO.equals("0116")){
                    String txtDDACuenta1 = (String)datasession.get( "txtDDACuenta" );
                    String txtSerial = (String)datasession.get( "txtSerial5" );
                    String txtBeneficiario = (String)datasession.get( "txtBeneficiario" );
                    Long TotalG = new Long(0);
                    long Montol = Long.parseLong(cGenericas.delCommaPointFromString(txtComisionGiro)) +
                    Long.parseLong(cGenericas.delCommaPointFromString(txtIvaGiro)) +
                    Long.parseLong(cGenericas.delCommaPointFromString((String)datasession.get("txtMonto1")));
                    TotalG = new Long(Montol);
                    Monto = cGenericas.delCommaPointFromString(delLeadingfZeroes((String)datasession.get("txtMonto1")));
                    String txtTotalGiro = TotalG.toString();
                    txtTotalGiro =  cGenericas.delCommaPointFromString(delLeadingfZeroes(txtTotalGiro));
                    txtCadImpresion = txtCadImpresion + "~GIROCITI1~EXP~" +  txtSerial  + "~" +
                    Monto + "~" + cGenericas.delCommaPointFromString(txtComisionGiro) +  "~" + cGenericas.delCommaPointFromString(txtIvaGiro) +
                    "~" + txtTotalGiro + "~" + txtNomCliente +  "~" + txtBeneficiario + "~" + txtDDACuenta1 +"~" ;
/*                    txtCadImpresion = txtCadImpresion + "~GIROCITI1~"+ txtDDACuenta1 + "~" +  txtSerial  + "~" +
                    Monto + "~" + cGenericas.delCommaPointFromString(txtComisionGiro) +  "~" + cGenericas.delCommaPointFromString(txtIvaGiro) +
                    "~" + txtTotalGiro + "~" + txtNomCliente +  "~" + txtBeneficiario + "~" ;
  */              }
                else if (TO.equals("0786")){
                    String txtDDACuenta1 = (String)datasession.get( "txtDDACuenta" );
                    String txtSerial = txtNumOrden;
                    String txtBeneficiario = (String)datasession.get( "txtBeneficiario" );
                    Monto = (String)datasession.get("txtMonto1");
                    txtCadImpresion = txtCadImpresion + "~EXPORDUSD~" + txtSerial  + "~" + txtNomCliente + "~" +
                    Monto + "~" + txtDDACuenta1 + "~" + txtBeneficiario + "~";
                }
            }
        }

        if(txtCasoEsp.length() > 0){
            session.setAttribute("txtCasoEsp", txtCasoEsp);
        }
        session.setAttribute("txtCadImpresion", sMessage);
        if(txtCadImpresion.length() > 0){
            session.setAttribute("txtCadImpresion", txtCadImpresion);
        }
        session.setAttribute("page.txnresponse", sMessage);
        getServletContext().getRequestDispatcher("/servlet/ventanilla.Redirector").forward(req,resp);
    }

    public void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doPost(req, resp);
    }
}
