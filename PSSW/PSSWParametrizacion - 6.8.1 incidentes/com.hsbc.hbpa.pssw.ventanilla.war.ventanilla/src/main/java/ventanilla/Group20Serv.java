//*************************************************************************************************
//             Funcion: Servlet que postea txn 0093 para consulta de Motivator
//            Elemento: Group20Serv.java
//          Creado por: Israel De Paz MErcado
//*************************************************************************************************
// CCN - 4360314 - Se crea Group20Serv para la txn 0093 de Motivator. 
//*************************************************************************************************

package ventanilla;

import java.io.IOException;
import java.util.Hashtable;
import java.util.Vector;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import ventanilla.com.bital.sfb.FormatoB;


public class Group20Serv extends HttpServlet {
    
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession(false);
        Hashtable datasession = (Hashtable)session.getAttribute("page.datasession");
        
        String txn = (String)datasession.get("cTxn");
        String branch = (String)datasession.get("sucursal");
        String teller = (String)datasession.get("teller");
        String registro = (String)session.getAttribute("empno");
        String moneda = "N$";
        String respuesta = "";
        int res=0;
        int lines;
        String szResp;
        String line;
        //java.util.Vector cics_resp = new java.util.Vector(); 
        Vector VecRespuesta = new Vector();
        Vector VecRes = new Vector();
        
               
        do
          { 
          	FormatoB motiv = new FormatoB();
            	motiv.setFormat("B");
            	motiv.setTxnCode(txn);
            	motiv.setBranch(branch);
            	motiv.setTeller(teller);
            	motiv.setSupervisor(teller);
            	motiv.setTranAmt("000");
	        motiv.setReferenc("0");
        	motiv.setFeeAmoun("0");
            	motiv.setFromCurr(moneda);
            	
          	
            if (res == 2)
            {
            	String line4 = (String)VecRes.get(4);
            	line4 = line4.trim();
                motiv.setAcctNo("R"+line4);
                VecRes = new Vector();
               
            }	
            else
            	{	         
            	motiv.setAcctNo("R"+registro);
                }
            motiv.execute();
            respuesta = motiv.getMessage();
            
            
            ventanilla.com.bital.util.NSTokenizer parser = new ventanilla.com.bital.util.NSTokenizer(respuesta, "~");
            Vector cics_resp = new Vector();
            while( parser.hasMoreTokens() )
                 {
                 String token = parser.nextToken();
                 if( token == null )
                    continue;
                
                 cics_resp.addElement(token);
                 }
            int ErrorRespuesta = Integer.parseInt((String)cics_resp.elementAt(0));     
            lines = Integer.parseInt((String)cics_resp.elementAt(1));
    	    szResp = (String)cics_resp.elementAt(3);
    	   
    	  
    	  
    	   if (res == 2)
    	   {
    	     for(int i=5; i<lines; i++)
    	       	{
    		line = szResp.substring( i*78, Math.min(i*78+77, szResp.length()) );
    		VecRespuesta.addElement(line);
     		if(Math.min(i*78+77, szResp.length()) == szResp.length())
      		break;
    	       	}	
    	   }	
    	   else{
    	   	for(int i=0; i<lines; i++)
    	       	{
    		line = szResp.substring( i*78, Math.min(i*78+77, szResp.length()) );
    		VecRespuesta.addElement(line);
     		if(Math.min(i*78+77, szResp.length()) == szResp.length())
      		break;
    	       	}
    	      }	
    	      
    	      
    	      
    	    for(int i=0; i<lines; i++)
    	       	{
    		line = szResp.substring( i*78, Math.min(i*78+77, szResp.length()) );
    		VecRes.addElement(line);
     		if(Math.min(i*78+77, szResp.length()) == szResp.length())
      		break;
    	       	}  
            
            
            
            if (ErrorRespuesta == 0)
               {
               //funcion que verifica si  tiene error	
               res = getMotiv(VecRes);
               }
               else
               {
                res = 1;   
                //break;
               } 
               
           }while(res == 2);
            
            session.setAttribute("FlagMotivator",new Integer(res));
            session.setAttribute("FlagSelectMot","1");
            
            if (res == 1)
               session.setAttribute("VecMotivator",VecRes);
               else
               session.setAttribute("VecMotivator",VecRespuesta);
            
            String Response = (String)session.getAttribute("page.getoTxnView");	
            getServletContext().getRequestDispatcher("/ventanilla/paginas/" + Response + ".jsp").forward(request,response);       
               
            
            
   }       
            
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
    
    private int getMotiv(Vector r)
    {
     	int sizer = r.size();
     	int flag;
     	java.util.Vector VecLine5 = new java.util.Vector();
     	String line4 = (String)r.get(4);
     	String line5 = (String)r.get(5);
       
       
     	if ((line5.trim().substring(1,2).equals("0")) || (line5.trim().substring(1,2).equals("1")) || (line5.trim().substring(1,2).equals("2")) || (line5.trim().substring(1,2).equals("3")) || (line5.trim().substring(1,2).equals("4")) || (line5.trim().substring(1,2).equals("5")) || (line5.trim().substring(1,2).equals("6")) || (line5.trim().substring(1,2).equals("7")) || (line5.trim().substring(1,2).equals("8")) || (line5.trim().substring(1,2).equals("9")) )
     	{
     		line4 = line4.trim();
     		if ((line4 == null) || (line4.equals("")))
     	   	{
     	   	flag = 0;
     	   	}
     	   	else
     	       	   {
     	       	   flag = 2;
     	           }
     	
     	}	
     	else
	        {
	        flag = 1;
        	} 	
        
    	return flag;	 
    }
    
   
    
}
