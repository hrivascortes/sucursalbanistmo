//*************************************************************************************************
//             Funcion: Clase que realiza txn Servicios
//            Elemento: Group15Serv.java
//          Creado por: Alejandro Gonzalez Castro
//      Modificado por: Alejandro Gonzalez Castro
//*************************************************************************************************
// CCN - 4360189 - 03/09/2004 - Se cambia invoke al Diario para Control de Efectivo
// CCN - 4360271 - 11/02/2005 - Se reversa envio de Sleep
//*************************************************************************************************
package ventanilla;

import java.io.IOException;
import java.util.Hashtable;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import ventanilla.com.bital.admin.Diario;
import ventanilla.com.bital.sfb.DevolImpuestos;


public class Group15Serv extends HttpServlet
{
  
  private String delCommaPointFromString(String newCadNum)
  {
   String nCad = new String(newCadNum);
   if( nCad.indexOf(".") > -1)
   {
    nCad = nCad.substring(nCad.indexOf(".") + 1, nCad.length());
    if(nCad.length() != 2)
    {
     String szTemp = new String("");
     for(int j = nCad.length(); j < 2; j++)
      szTemp = szTemp + "0";
     newCadNum = newCadNum + szTemp;
    }
   }

   StringBuffer nCadNum = new StringBuffer(newCadNum);
   for(int i = 0; i < nCadNum.length(); i++)
    if(nCadNum.charAt(i) == ',' || nCadNum.charAt(i) == '.')
     nCadNum.deleteCharAt(i);

   return nCadNum.toString();
  }
  
  private String getString(String newCadNum, int newOption)
  {
   int iPIndex = newCadNum.indexOf("CONSECUTIVO:") + 12, NumSaltos, Num = 0, Ban = 0;
   String nCadNum = new String("");
   NumSaltos = newOption;
   while(Num < NumSaltos)
   {
    if(newCadNum.charAt(iPIndex) == 0x20)
    {
     while(newCadNum.charAt(iPIndex) == 0x20)
      iPIndex++;
     Num++;
    }
    else
     while(newCadNum.charAt(iPIndex) != 0x20)
      iPIndex++;
   }
   while(newCadNum.charAt(iPIndex) != 0x20 && iPIndex < newCadNum.length()){
    nCadNum = nCadNum + newCadNum.charAt(iPIndex);
    if(iPIndex < newCadNum.length())
      iPIndex++;
    if(iPIndex == newCadNum.length())
      break;
   }

   return nCadNum;
  }

  public void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
   {

       String TE, TO, Currency;
       
       DevolImpuestos oTxn = new DevolImpuestos();
       HttpSession session = req.getSession(false);
       Hashtable datasession = (Hashtable)session.getAttribute("page.datasession");
       TE = (String)datasession.get("cTxn");
       TO = (String)datasession.get("oTxn");
       
       Currency = (String)datasession.get("moneda");
       String msg = null;
       String dirip =  (String)session.getAttribute("dirip");
       String txtCasoEsp = new String("");
       String txtSupervisor = (String)datasession.get("teller");
       String Monto = "";
       
       if ( (String)datasession.get("supervisor") != null )       //pidio autorizacisn de supervisor
           txtSupervisor =  (String)datasession.get("supervisor");
       
       oTxn.setTxnCode(TE);
       oTxn.setBranch((String)datasession.get("sucursal"));
       oTxn.setTeller((String)datasession.get("teller"));
       oTxn.setSupervisor(txtSupervisor);
       
       
       Diario diario = new Diario();
       String sMessage="";
                   
       if( TE.equals( "0812" ) ) {
           //ODCT 0812B 00001000130  000130  000**580*1552800*920000677300000******N$**
           oTxn = new DevolImpuestos();
           oTxn.setTxnCode("0812");
           oTxn.setBranch((String)datasession.get("sucursal"));
           oTxn.setTeller((String)datasession.get("teller"));
           oTxn.setSupervisor(txtSupervisor);
           oTxn.setFormat( "B" );
           oTxn.setAcctNo("580");
           oTxn.setTranAmt(delCommaPointFromString((String)datasession.get("txtMonto")));
           oTxn.setReferenc((String)datasession.get("txtReferTesof"));
           oTxn.setFromCurr("N$");
           oTxn.setReversable("N");
           sMessage = diario.invokeDiario(oTxn, (String)session.getAttribute("d_ConsecLiga"), dirip, Currency, session);
       }
       if ( TE.equals( "0067" ) ) {
           if( TO.equals( "5415" ) ) {
               //ODCT 5415A 00001000130  000130  000**580*1552800*920000677300000****1552800*N$**********
               oTxn = new DevolImpuestos();
               oTxn.setBranch((String)datasession.get("sucursal"));
               oTxn.setTeller((String)datasession.get("teller"));
               oTxn.setSupervisor(txtSupervisor);
               oTxn.setFormat( "A" );
               oTxn.setTxnCode("5415");
               
               if ( (String)datasession.get("override") != null )
                   if (datasession.get("override").toString().equals("SI"))
                       oTxn.setOverride("3");
               
               if ( (String)datasession.get("supervisor") != null )
                   oTxn.setSupervisor((String)datasession.get("supervisor"));
               
               oTxn.setAcctNo("580");
               oTxn.setTranAmt(delCommaPointFromString((String)datasession.get("txtMonto")));
               oTxn.setReferenc1((String)datasession.get("txtReferTesof"));
               oTxn.setFromCurr("N$");
               oTxn.setCashOut(delCommaPointFromString((String)datasession.get("txtMonto")));
               Monto = delCommaPointFromString((String)datasession.get("txtMonto"));
                              
               sMessage = diario.invokeDiario(oTxn, (String)session.getAttribute("d_ConsecLiga"), dirip, Currency, session);

               if(sMessage.substring(0,1).equals("0")){
                   txtCasoEsp = txtCasoEsp + "5415" + "~" + getString(sMessage,1) + "~";
               }
               
               
           }
           else {
               // ODCT 0041A 00001000130  000130  000****1552800*N$**333****000**920000677300000******4000001628*******4000000000*************************
               oTxn = new DevolImpuestos();
               oTxn.setBranch((String)datasession.get("sucursal"));
               oTxn.setTeller((String)datasession.get("teller"));
               oTxn.setSupervisor(txtSupervisor);
               oTxn.setFormat( "A" );
               oTxn.setTxnCode("0041");               
               
               if ( (String)datasession.get("override") != null )
                   if (datasession.get("override").toString().equals("SI"))
                       oTxn.setOverride("3");
               
               if ( (String)datasession.get("supervisor") != null )
                   oTxn.setSupervisor((String)datasession.get("supervisor"));
               
               oTxn.setTranAmt( delCommaPointFromString((String)datasession.get( "txtMonto" )) );
               oTxn.setTranCur( "N$" );
               oTxn.setTrNo("333");
               oTxn.setTranDesc((String)datasession.get("txtReferTesof"));
               oTxn.setAcctNo1((String)datasession.get("txtDDACuenta"));
               oTxn.setAcctNo2((String)datasession.get("txtDDA"));
               Monto = delCommaPointFromString((String)datasession.get("txtMonto"));  
               oTxn.setDescRev("EMPTY"); 
               sMessage = diario.invokeDiario(oTxn, (String)session.getAttribute("d_ConsecLiga"), dirip, Currency, session);
              
               if(sMessage.substring(0,1).equals("0")){
                   txtCasoEsp = txtCasoEsp + "0041" + "~" + getString(sMessage,1) + "~";
                   //ODCT 0812B 00001000130  000130  000**99999*1552800*920000677300000******N$**                   
                   oTxn = new DevolImpuestos();
                   oTxn.setTxnCode("0812");
                   oTxn.setBranch((String)datasession.get("sucursal"));
                   oTxn.setTeller((String)datasession.get("teller"));
                   oTxn.setSupervisor(txtSupervisor);
                   oTxn.setFormat( "B" );
                   oTxn.setAcctNo("99999");
                   oTxn.setTranAmt(delCommaPointFromString((String)datasession.get("txtMonto")));
                   oTxn.setReferenc((String)datasession.get("txtReferTesof"));
                   oTxn.setFromCurr("N$");
                   sMessage = diario.invokeDiario(oTxn, (String)session.getAttribute("d_ConsecLiga"), dirip, Currency, session);
               }
           }
       }
       if(txtCasoEsp.length() > 0){
           session.setAttribute("txtCasoEsp", txtCasoEsp);
       }
       session.setAttribute("page.txnresponse", sMessage);
       getServletContext().getRequestDispatcher("/servlet/ventanilla.Redirector").forward(req,resp);
  }
  
  public void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
  {
    doPost(req, resp);
  }
}
