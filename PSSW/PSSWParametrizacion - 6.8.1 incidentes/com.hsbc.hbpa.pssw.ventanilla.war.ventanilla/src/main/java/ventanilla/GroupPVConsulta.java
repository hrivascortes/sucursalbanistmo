//************************************************************************************************
//			   Funcion: Clase que realiza la interconexion
//						con Pagos Voluntarios
//			  Elemento: GroupPVConsulta.java
//			Creado por: Carolina Vela Seraf�n
//			Modificado por: Humberto Enrique Balleza Mej�a
//*************************************************************************************************
// CCN 
// CR -  CR11593 - 14/02/2011 - Correcci�n en impresi�n de pago voluntario
//*************************************************************************************************
package ventanilla;

import java.io.IOException;
import java.util.Hashtable;
import java.util.Vector;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import ventanilla.com.bital.admin.SpFunc_PAGV;

import com.bital.cics.CicsCtgBean;
import com.bital.cics.CicsCtgImpl;
import com.bital.util.Comunica;


public class GroupPVConsulta extends HttpServlet {
	 
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		HttpSession session = request.getSession(false);
		session.setAttribute("pasoP001","NO");
		Hashtable datasession = (Hashtable)session.getAttribute("page.datasession");
		Hashtable userinfo = (Hashtable)session.getAttribute("userinfo");
		GenericClasses gc = new GenericClasses();
		String txtCredito  	= (String)datasession.get("txtCredito");

			ventanilla.com.bital.beans.DatosVarios datosv = ventanilla.com.bital.beans.DatosVarios.getInstance();
			Vector selectu = (Vector)datosv.getDatosV("USERKRONER");
			String UserId = selectu.get(0).toString(); 
			
			Vector selectr = (Vector)datosv.getDatosV("ROLEKRONER");
			String RolKroner = selectr.get(0).toString(); 
			Vector selectc = (Vector)datosv.getDatosV("CICSSERVER");
			String CicsServer = selectc.get(0).toString(); 
			Vector selectg = (Vector)datosv.getDatosV("GATEWAYURL");
			String GatewayURL = selectg.get(0).toString(); 	
			Comunica c = new Comunica();
			CicsCtgBean cicsCtgObj = new CicsCtgBean();		
			cicsCtgObj.setCicsProgramName("KPNO003"); //"rpc"	
			cicsCtgObj.setCommAreaLength(4096); //"long"
			cicsCtgObj.setUserId(UserId);//("IDDAGJ"); //"user"	
			//cicsCtgObj.setUserId("DPSPUSR2");//("IDDAGJ"); //"user"
			//System.out.println("Usuario para Kroner: "+UserId);	
			//cicsCtgObj.setPassword("PUMAS17");//("ANAKIN69"); //"password"
			cicsCtgObj.setRolKroner(RolKroner);//("IDDAGJ"); //"rol Kroner"	
			//System.out.println("Rol para Kroner: "+RolKroner);	
			cicsCtgObj.setCicsServer(CicsServer); //"cics"
			//System.out.println("CICS de conexi�n: "+CicsServer);
			cicsCtgObj.setGatewayURL("local:"); //"url"
			//cicsCtgObj.setGatewayURL("web53.mx.cmtH"); //"url" LOCAL
			//System.out.println("Gateway de conecci�n "+GatewayURL);
			cicsCtgObj.setGatewayPort(0); //"puerto" --LOCAL
			//cicsCtgObj.setGatewayPort(2006); //"puerto" --LOCAL
			cicsCtgObj.setGatewayProtocol("local"); //"protocolo"			
			//cicsCtgObj.setGatewayProtocol("tcp"); //"protocolo"
			cicsCtgObj.setEncoding("IBM037"); //"encoding"
			cicsCtgObj.setTransId("KPNO"); //"trans"
			
			
			//SE ARMA CADENA PARA KRONER
			String user = (String)userinfo.get("D_EJCT_CTA");
			String asignado = "EDOCC";
			String fchConsulta= gc.getDate(1);
			int cveMov=1;
			String ctaRepago=(String)session.getAttribute("ctaRepago");
			
			
			//Cadena de pago
			asignado="RECUP";
			String branch = (String)session.getAttribute("branch");
			
			
			String inputStr = (String)datasession.get("txtEfectivo");
			String inputStr2 = (String)datasession.get("txtCheque");
			/*
		    inputStr = inputStr.toString().replace(",","");
		    inputStr2 = inputStr2.toString().replace(",","");
			String intVal1 = inputStr.substring(0, inputStr.indexOf("."));
			String intVal2 = inputStr2.substring(0, inputStr2.indexOf("."));
			String decVal1 = inputStr.substring(inputStr.indexOf(".") + 1, inputStr.length());
		    String decVal2 = inputStr2.substring(inputStr2.indexOf(".") + 1, inputStr2.length());
			String monto1 = intVal1 + decVal1;
		    String monto2 = intVal2 + decVal2;
			*/
			long monto = Long.parseLong(gc.delCommaPointFromString(inputStr));
			monto += Long.parseLong(gc.delCommaPointFromString(inputStr2));
			
			
			//String importe = (String)session.getAttribute("montoKronner");
			
			String importe = "";
			importe=String.valueOf(monto);
			
			datasession.put("MontoFinal",gc.FormatAmount(importe));
			session.setAttribute("page.datasession",datasession);
					
			int i =0;
			
			String caracter = null;
			String importef = "";
			for(i=0;i<importe.length();i++)
			{
				caracter=importe.substring(i,i+1);
				if(!caracter.equals(",") && !caracter.equals("."))
				{	
					importef=importef+caracter;
				} 
			}
			
			
			String tipoPago = (String)datasession.get("rdoTipoPago");
			
			String cadFInalCronner = "";
			
			
			if(tipoPago.equals("4"))
			    cadFInalCronner = asignado+UserId+"01A"+branch+txtCredito.substring(2,10)+fchConsulta+gc.rellenaZeros(importef,12)+"_01001"+ctaRepago+"00000000000000000"+UserId+"000000000000000000000000000000____________000100200018"+RolKroner+"0211010000000000000000";
			else if(tipoPago.equals("3"))
				cadFInalCronner = asignado+UserId+"01A"+branch+txtCredito.substring(2,10)+fchConsulta+gc.rellenaZeros(importef,12)+"S"+"00001"+ctaRepago+"00000000000000000"+UserId+"000000000000000000000000000000____________000100200018"+RolKroner+"0211010000000000000000";
		    else
		        cadFInalCronner = asignado+UserId+"01A"+branch+txtCredito.substring(2,10)+fchConsulta+gc.rellenaZeros(importef,12)+"_00001"+ctaRepago+"00000000000000000"+UserId+"000000000000000000000000000000____________000100200018"+RolKroner+"0211010000000000000000";
		    //String cadFInalCronner= asignado+UserId+"01C"+gc.rellenaZeros(txtCredito,8)+fchConsulta+cveMov;
		    //String cadFInalCronner="RECUPIDDRDE__01A000010233023020080107000000000100_00000100901020100000000000000000IDDRDE__000000000000000000000000000000____________0001002000182133014001110211010000000000000000";
		    //RECUPDPSPUSR201A000027795903420080410000000142241_00000101000001660000000000000000DPSPUSR2000000000000000000000000000000____________0001002000182133014001110211010000000000000000
		    //System.out.println("MSG IN KRONER: "+cadFInalCronner);
			
			
			
			cicsCtgObj.setCommAreaInput(cadFInalCronner);
			//System.out.println("MSG Consulta POST KRONER: "+cadFInalCronner);
			//cicsCtgObj.setCommAreaInput("EDOCCIDDRDE__00101530496200711301");
			CicsCtgImpl cicsCtg = new CicsCtgImpl();
				
			String res=c.executale(cicsCtgObj);	
		    //System.out.println("MSG Consulta RESPONSE KRONER: "+res);
		    
		
			
		    //String res = "0058622OPERACION_REALIZADA__________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________";
		    
		if(res != null)
		{
		if(res.length()>=2 && !res.substring(0,2).equals("00"))
		{
			session.setAttribute("page.txnresponse", res);
			if(res.substring(0,6).equals("020174")){
					
				session.setAttribute("respKronner",res);			
			}
			Vector resKroner = new Vector();
			resKroner.add("1");
			resKroner.add("01");
			resKroner.add(res.substring(0,5));
			resKroner.add(res);
			session.setAttribute("response",resKroner);
			response.sendRedirect("../ventanilla/paginas/Response.jsp");	
			//getServletContext().getRequestDispatcher("/servlet/ventanilla.Redirector").forward(request,response);
		}else if(res.length()>=2 && res.substring(0,2).equals("00")){
		    session.setAttribute("respuestaKronner", res);  
			//session.setAttribute("page.txnresponse", sMessage);
			//session.setAttribute("ctaRepago",ctaRepago);
			//datasession.put("txtDDACuenta",ctaKronner);
			//System.out.println("Pago exitoso");
			asignado="EDOCC";
			cicsCtgObj.setCicsProgramName("KPHO075"); //"rpc"
			cicsCtgObj.setTransId("KPHO");
			cadFInalCronner= asignado+UserId+"00C"+txtCredito.substring(2,10)+fchConsulta+cveMov;
			cicsCtgObj.setCommAreaInput(cadFInalCronner);
			cicsCtg = new CicsCtgImpl();
			res=c.executale(cicsCtgObj);	
			//res = "000233023020080409000011000000000000095JOE_*MCENROE____________________________0000025000002005121520051123024cmtH_BANK_(PANAMA)_S20110131102008041502DOLAR_AMERICANO_____1009010201200804150057400000046320060613_________________1000601000000000000000000000001442666000000000000000001442666000000000000000000000000000000000000000000000000009794990000000206450000001212760000000010820000000000000000000000000000000603070000000000000000000233880000012652070000000003000000000000000000000000000000027106160000012652070000000000000700PAGOS_NIVELADOS_____00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000010000000000000____________________00000000000000000000000000000000VIGENTE___PUNTA_PAITILLA__________________________PRESTAMOS_PERSONALES_(PCON)_____________________________________________________________________________________________*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_01____________________________________________________________________________________________________00000000000410000000000000000000000000000000000________________________________________________________________________________________________________________________0000501___________72011013100000000000000000000000000000000017160000000000000006000000000000000000000000000000000000000000217270000011007750000000013000000000637000000000000000000000000000000000000000000000000000000000000000000000224480000000003610000000404250000000000000000000000000000000000000000000000000000000000002045010100000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000PRESTAMOS_PERSONALES__________000000000000_____________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________";
			//System.out.println("MSG OUT KRONER: "+res);
			//getServletContext().getRequestDispatcher("/servlet/ventanilla.PageServlet").forward(request,response);
			if(res.length()>=2 && !res.substring(0,2).equals("00"))
			{
				session.setAttribute("page.txnresponse", res);
				if(res.substring(0,6).equals("020174")){
					
					session.setAttribute("respKronner",res);			
				}
				Vector resKroner = new Vector();
				resKroner.add("1");
				resKroner.add("03");
				resKroner.add(res.substring(0,5));
				resKroner.add("VERIFICA EL ESTADO DEL CR�DITO EN KRONER                                      REVERSA ABONO A CUENTA EN CASO DE SER NECESARIO                               "+res);
				session.setAttribute("response",resKroner);  
				response.sendRedirect("../ventanilla/paginas/Response.jsp");	
				//getServletContext().getRequestDispatcher("/servlet/ventanilla.Redirector").forward(request,response);
			}else
			{
				
				//Despues del pago
				String connureg = (String)session.getAttribute("page.diario_consecutivo");
				SpFunc_PAGV sp = new SpFunc_PAGV();
				session.removeAttribute("page.diario_consecutivo");
				
				sp.SpFunc_PAGV_1003(connureg,(String)session.getAttribute("teller"));
				
				String RS_SDO_CAPVIG = res.substring(277,288);
				int i_RS_SDO_CAPVIG=Integer.parseInt(RS_SDO_CAPVIG.trim());
				String RS_SDO_VENC = res.substring(333,344);
				int i_RS_SDO_VENC=Integer.parseInt(RS_SDO_VENC.trim());
				String RS_SDO_INTS = res.substring(369,380);
				int i_RS_SDO_INTS=Integer.parseInt(RS_SDO_INTS.trim());
				String RS_INT_VENC = res.substring(357,368);
				int i_RS_INT_VENC=Integer.parseInt(RS_INT_VENC.trim());
				String RS_IMP_FECI_VIG = res.substring(1583,1594);
				int i_RS_IMP_FECI_VIG=Integer.parseInt(RS_IMP_FECI_VIG.trim());
				String RS_IMP_FECI_VEN = res.substring(1595,1606);
				int i_RS_IMP_FECI_VEN=Integer.parseInt(RS_IMP_FECI_VEN.trim());
				String RS_SDO_MORA = res.substring(405,416);
				int i_RS_SDO_MORA=Integer.parseInt(RS_SDO_MORA.trim());
				String RS_GASTOS = res.substring(574,585);
				int i_RS_GASTOS=Integer.parseInt(RS_GASTOS.trim());
				String RS_COMISIONES = res.substring(453,464);
				int i_RS_COMISIONES=Integer.parseInt(RS_COMISIONES.trim());
				//String RS_SEGURO = res.substring(610,621);
				//int i_RS_SEGURO=Integer.parseInt(RS_SEGURO.trim());
				String RS_IVA_CAP_VEN = res.substring(1387,1398);
				int i_RS_IVA_CAP_VEN=Integer.parseInt(RS_IVA_CAP_VEN.trim());
				String RS_IMP_SEG_VIG = res.substring(1487,1498);
				int i_RS_IMP_SEG_VIG=Integer.parseInt(RS_IMP_SEG_VIG.trim());
				String RS_IMP_SEG_VEN = res.substring(1499,1510);
				int i_RS_IMP_SEG_VEN=Integer.parseInt(RS_IMP_SEG_VEN.trim());
				String RS_IMP_SEGOVIG = res.substring(1733,1744);
				int i_RS_IMP_SEGOVIG=Integer.parseInt(RS_IMP_SEGOVIG.trim());
				String RS_IMP_SEGOVEN = res.substring(1745,1756);
				int i_RS_IMP_SEGOVEN=Integer.parseInt(RS_IMP_SEGOVEN.trim());
				String RS_TOT_VIGENTE = res.substring(429,440);
				int i_RS_TOT_VIGENTE=Integer.parseInt(RS_TOT_VIGENTE.trim());
				String RS_TOT_ADEUDO = res.substring(489,500);
				int i_RS_TOT_ADEUDO=Integer.parseInt(RS_TOT_ADEUDO.trim());
				String RS_GASTOSVIG = res.substring(1721,1732);
				int i_RS_GASTOSVIG=Integer.parseInt(RS_GASTOSVIG.trim());
				String RS_SDO_SEGVEN = res.substring(586,597);
				int i_RS_SDO_SEGVEN=Integer.parseInt(RS_SDO_SEGVEN.trim());
				String RS_COMISIONESVIG = res.substring(1709,1720);
				int i_RS_COMISIONESVIG=Integer.parseInt(RS_COMISIONESVIG.trim());
				String RS_TOT_VENCIDO = res.substring(501,512);
				int i_RS_TOT_VENCIDO=Integer.parseInt(RS_TOT_VENCIDO.trim());
				
				//itbms DEL PAGO

		        String RS_ITBM_VIG = res.substring(1510,1522);
		        int i_RS_ITBM_VIG=Integer.parseInt(RS_ITBM_VIG.trim());
				String RS_ITBM_VEN = res.substring(1522,1534);
				int i_RS_ITBM_VEN=Integer.parseInt(RS_ITBM_VEN.trim());
				
				String RS_ITBM_VIG_GAS = res.substring(1920,1932);
				int i_RS_ITBM_VIG_GAS=Integer.parseInt(RS_ITBM_VIG_GAS.trim());
				String RS_ITBM_VEN_GAS = res.substring(1932,1944);
				int i_RS_ITBM_VEN_GAS=Integer.parseInt(RS_ITBM_VEN_GAS.trim());

				
				
				//Antes del pago
				String a_RS_SDO_CAPVIG = (String)session.getAttribute("RS_SDO_CAPVIG");
				int RS_SDO_CAPVIG_a=Integer.parseInt(a_RS_SDO_CAPVIG.trim());
				String a_RS_SDO_VENC = (String)session.getAttribute("RS_SDO_VENC");
				int RS_SDO_VENC_a=Integer.parseInt(a_RS_SDO_VENC.trim());
				String a_RS_SDO_INTS = (String)session.getAttribute("RS_SDO_INTS");
				int RS_SDO_INTS_a=Integer.parseInt(a_RS_SDO_INTS.trim());
				String a_RS_INT_VENC = (String)session.getAttribute("RS_INT_VENC");
				int RS_INT_VENC_a=Integer.parseInt(a_RS_INT_VENC.trim());
				String a_RS_IMP_FECI_VIG = (String)session.getAttribute("RS_IMP_FECI_VIG");
				int RS_IMP_FECI_VIG_a=Integer.parseInt(a_RS_IMP_FECI_VIG.trim());
				String a_RS_IMP_FECI_VEN = (String)session.getAttribute("RS_IMP_FECI_VEN");
				int RS_IMP_FECI_VEN_a=Integer.parseInt(a_RS_IMP_FECI_VEN.trim());
				String a_RS_SDO_MORA = (String)session.getAttribute("RS_SDO_MORA");
				int RS_SDO_MORA_a=Integer.parseInt(a_RS_SDO_MORA.trim());
				String a_RS_GASTOS = (String)session.getAttribute("RS_GASTOS");
				int RS_GASTOS_a=Integer.parseInt(a_RS_GASTOS.trim());
				String a_RS_COMISIONES = (String)session.getAttribute("RS_COMISIONES");
				int RS_COMISIONES_a=Integer.parseInt(a_RS_COMISIONES.trim());
				//String a_RS_SEGURO = (String)session.getAttribute("RS_SEGURO");
				//int RS_SEGURO_a=Integer.parseInt(a_RS_SEGURO.trim());
                String a_RS_IVA_CAP_VEN = (String)session.getAttribute("RS_IVA_CAP_VEN");
				int RS_IVA_CAP_VEN_a=Integer.parseInt(a_RS_IVA_CAP_VEN.trim());
				String a_RS_IMP_SEG_VIG = (String)session.getAttribute("RS_IMP_SEG_VIG");
				int RS_IMP_SEG_VIG_a=Integer.parseInt(a_RS_IMP_SEG_VIG.trim());
				String a_RS_IMP_SEG_VEN = (String)session.getAttribute("RS_IMP_SEG_VEN");
				int RS_IMP_SEG_VEN_a=Integer.parseInt(a_RS_IMP_SEG_VEN.trim());
				String a_RS_IMP_SEGOVIG = (String)session.getAttribute("RS_IMP_SEGOVIG");
				int RS_IMP_SEGOVIG_a=Integer.parseInt(a_RS_IMP_SEGOVIG.trim());
				String a_RS_IMP_SEGOVEN = (String)session.getAttribute("RS_IMP_SEGOVEN");
				int RS_IMP_SEGOVEN_a=Integer.parseInt(a_RS_IMP_SEGOVEN.trim());
				String a_RS_TOT_VIGENTE = (String)session.getAttribute("RS_TOT_VIGENTE");
				int RS_TOT_VIGENTE_a=Integer.parseInt(a_RS_TOT_VIGENTE.trim());
				String a_RS_TOT_ADEUDO = (String)session.getAttribute("RS_TOT_ADEUDO");
				int RS_TOT_ADEUDO_a=Integer.parseInt(a_RS_TOT_ADEUDO.trim());
				String a_RS_GASTOSVIG = (String)session.getAttribute("RS_GASTOSVIG");
				int RS_GASTOSVIG_a=Integer.parseInt(a_RS_GASTOSVIG.trim());
				String a_RS_SDO_SEGVEN = (String)session.getAttribute("RS_SDO_SEGVEN");
				int RS_SDO_SEGVEN_a=Integer.parseInt(a_RS_SDO_SEGVEN.trim());
				String a_RS_COMISIONESVIG = (String)session.getAttribute("RS_COMISIONESVIG");
				int RS_COMISIONESVIG_a=Integer.parseInt(a_RS_COMISIONESVIG.trim());
				String a_RS_TOT_VENCIDO = (String)session.getAttribute("RS_TOT_VENCIDO");
				int RS_TOT_VENCIDO_a=Integer.parseInt(a_RS_TOT_VENCIDO.trim());
				
				//ANTES DEL PAGO ITBMS

				String a_RS_ITBM_VIG = (String)session.getAttribute("RS_ITBM_VIG");
				int RS_ITBM_VIG_a=Integer.parseInt(a_RS_ITBM_VIG.trim());
				String a_RS_ITBM_VEN = (String)session.getAttribute("RS_ITBM_VEN");
				int RS_ITBM_VEN_a=Integer.parseInt(a_RS_ITBM_VEN.trim());

				String a_RS_ITBM_VIG_GAS = (String)session.getAttribute("RS_ITBM_VIG_GAS");
				int RS_ITBM_VIG_GAS_a=Integer.parseInt(a_RS_ITBM_VIG_GAS.trim());
				String a_RS_ITBM_VEN_GAS = (String)session.getAttribute("RS_ITBM_VEN_GAS");
				int RS_ITBM_VEN_GAS_a=Integer.parseInt(a_RS_ITBM_VEN_GAS.trim());

				
				//Calculo del pago
				RS_SDO_CAPVIG="" + (RS_SDO_CAPVIG_a - i_RS_SDO_CAPVIG);
				RS_SDO_VENC="" + (RS_SDO_VENC_a - i_RS_SDO_VENC);
				RS_SDO_INTS="" + (RS_SDO_INTS_a - i_RS_SDO_INTS);
				RS_INT_VENC="" + (RS_INT_VENC_a - i_RS_INT_VENC);
				RS_IMP_FECI_VIG="" + (RS_IMP_FECI_VIG_a - i_RS_IMP_FECI_VIG);
				RS_IMP_FECI_VEN="" + (RS_IMP_FECI_VEN_a - i_RS_IMP_FECI_VEN);
				RS_SDO_MORA="" + (RS_SDO_MORA_a - i_RS_SDO_MORA);
				RS_GASTOS="" + (RS_GASTOS_a - i_RS_GASTOS);
				RS_COMISIONES="" + (RS_COMISIONES_a - i_RS_COMISIONES);
				//RS_SEGURO="" + (RS_SEGURO_a - i_RS_SEGURO);
				//RS_IVA_CAP_VEN="" + i_RS_IVA_CAP_VEN;
                                RS_IVA_CAP_VEN="" + (i_RS_IVA_CAP_VEN - RS_IVA_CAP_VEN_a);
				RS_IMP_SEG_VIG="" + (RS_IMP_SEG_VIG_a - i_RS_IMP_SEG_VIG + RS_IMP_SEGOVIG_a - i_RS_IMP_SEGOVIG);
				RS_IMP_SEG_VEN="" + (RS_IMP_SEG_VEN_a - i_RS_IMP_SEG_VEN + RS_IMP_SEGOVEN_a - i_RS_IMP_SEGOVEN);
				//RS_IMP_SEGOVIG="" + (RS_IMP_SEGOVIG_a - i_RS_IMP_SEGOVIG);
				//RS_IMP_SEGOVEN="" + (RS_IMP_SEGOVEN_a - i_RS_IMP_SEGOVEN);
				RS_TOT_VIGENTE="" + (RS_TOT_VIGENTE_a - i_RS_TOT_VIGENTE);
				RS_TOT_ADEUDO="" + (RS_TOT_ADEUDO_a - i_RS_TOT_ADEUDO);
				RS_GASTOSVIG="" + (RS_GASTOSVIG_a - i_RS_GASTOSVIG);
				RS_SDO_SEGVEN="" + (RS_SDO_SEGVEN_a - i_RS_SDO_SEGVEN);
				RS_COMISIONESVIG="" + (RS_COMISIONESVIG_a - i_RS_COMISIONESVIG);
				RS_TOT_VENCIDO="" + (RS_TOT_VENCIDO_a - i_RS_TOT_VENCIDO);
				
				//itbms 
				//RS_ITBM_VIG="" + (RS_ITBM_VIG_a - i_RS_ITBM_VIG);
				//RS_ITBM_VEN="" + (RS_ITBM_VEN_a - i_RS_ITBM_VEN);
				RS_ITBM_VIG="" + (RS_ITBM_VIG_a - i_RS_ITBM_VIG + RS_ITBM_VIG_GAS_a - i_RS_ITBM_VIG_GAS);
				RS_ITBM_VEN="" + (RS_ITBM_VEN_a - i_RS_ITBM_VEN + RS_ITBM_VEN_GAS_a - i_RS_ITBM_VEN_GAS);

				
				
				session.setAttribute("RS_SDO_CAPVIG",RS_SDO_CAPVIG);
				session.setAttribute("RS_SDO_VENC",RS_SDO_VENC);
				session.setAttribute("RS_SDO_INTS",RS_SDO_INTS);
				session.setAttribute("RS_INT_VENC",RS_INT_VENC);
				session.setAttribute("RS_IMP_FECI_VIG",RS_IMP_FECI_VIG);
				session.setAttribute("RS_IMP_FECI_VEN",RS_IMP_FECI_VEN);
				session.setAttribute("RS_SDO_MORA",RS_SDO_MORA);
				session.setAttribute("RS_GASTOS",RS_GASTOS);
				session.setAttribute("RS_COMISIONES",RS_COMISIONES);
				//session.setAttribute("RS_SEGURO",RS_SEGURO);
				session.setAttribute("RS_IVA_CAP_VEN",RS_IVA_CAP_VEN);
				session.setAttribute("RS_IMP_SEG_VIG",RS_IMP_SEG_VIG);
				session.setAttribute("RS_IMP_SEG_VEN",RS_IMP_SEG_VEN);
				//session.setAttribute("RS_IMP_SEGOVIG",RS_IMP_SEGOVIG);
				//session.setAttribute("RS_IMP_SEGOVEN",RS_IMP_SEGOVEN);
				session.setAttribute("RS_TOT_VIGENTE",RS_TOT_VIGENTE);
				session.setAttribute("RS_TOT_ADEUDO",RS_TOT_ADEUDO);
				session.setAttribute("RS_GASTOSVIG",RS_GASTOSVIG);
				session.setAttribute("RS_SDO_SEGVEN",RS_SDO_SEGVEN);
				session.setAttribute("RS_COMISIONESVIG",RS_COMISIONESVIG);
				session.setAttribute("RS_TOT_VENCIDO",RS_TOT_VENCIDO);
				session.setAttribute("TOTAL_ADEUDO",res.substring(489,500));
				
				//ITBMS
				session.setAttribute("RS_ITBM_VIG",RS_ITBM_VIG);
				session.setAttribute("RS_ITBM_VEN",RS_ITBM_VEN);
				
				
							
				session.setAttribute("page.datasession", datasession);	
				response.sendRedirect("../ventanilla/paginas/ResponseSaldosPV.jsp");
			}       
		}
		if(res == null)
		{
			Vector resKroner = new Vector();
			resKroner.add("1");
			resKroner.add("01");
			resKroner.add("000000");
			resKroner.add("ERROR DE COMUNICACI�N CON EL HOST DE KRONER");
			session.setAttribute("response",resKroner);
			response.sendRedirect("../ventanilla/paginas/Response.jsp");
		}		
	}		
}
    
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}
}
