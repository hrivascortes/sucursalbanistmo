//*************************************************************************************************
//             Funcion: Clase que realiza txn 0604 - CI RAP
//            Elemento: Group03RAP.java
//          Creado por: Alejandro Gonzalez Castro
//      Modificado por: Alejandro Gonzalez Castro
//*************************************************************************************************
// CCN - 4360189 - 03/09/2004 - Se cambia invoke al Diario para Control de Efectivo
// CCN - 4360211 - 08/10/2004 - Se elimina mensaje de documentos pendientes por digitalizar
// CCN -         - 10/01/2008 - Se hacen modificaciones para transacción global de cheques locales
//*************************************************************************************************
package ventanilla;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import ventanilla.com.bital.admin.Diario;
import ventanilla.com.bital.util.VolanteoDDA;
import ventanilla.com.bital.sfb.RAPG;

import java.util.Hashtable;
import java.util.Stack;


public class Group03RAP extends HttpServlet
{

    private String getString(String newCadNum, int newOption) {
        int iPIndex = newCadNum.indexOf("CONSECUTIVO:") + 12, NumSaltos, Num = 0, Ban = 0;
        String nCadNum = new String("");
        NumSaltos = newOption;
        while(Num < NumSaltos) {
            if(newCadNum.charAt(iPIndex) == 0x20) {
                while(newCadNum.charAt(iPIndex) == 0x20)
                    iPIndex++;
                Num++;
            }
            else
                while(newCadNum.charAt(iPIndex) != 0x20)
                    iPIndex++;
        }
        while(newCadNum.charAt(iPIndex) != 0x20 && iPIndex < newCadNum.length()){
            nCadNum = nCadNum + newCadNum.charAt(iPIndex);
            if(iPIndex < newCadNum.length())
                iPIndex++;
            if(iPIndex == newCadNum.length())
                break;
        }

        return nCadNum;
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
         response.setContentType("text/plain");
        HttpSession session = request.getSession(false);
        Hashtable datasession = (Hashtable)session.getAttribute("page.datasession");
		GenericClasses gc = new GenericClasses();

        String dirip =  (String)session.getAttribute("dirip");

        Hashtable Truncamiento = (Hashtable)session.getAttribute("truncamiento");
        long TruncamientoMN = 500000L;
        long TruncamientoUSD = 50000L;

        if (!Truncamiento.isEmpty() )
        {
            TruncamientoMN = Long.parseLong(Truncamiento.get("TruncamientoMN").toString());
            TruncamientoUSD = Long.parseLong(Truncamiento.get("TruncamientoUSD").toString());
        }
        VolanteoDDA oVolanteoDDA = null;
        String txtVolante = "NO";

        RAPG rapg = new RAPG();

        rapg.setBranch((String)datasession.get("sucursal"));
        rapg.setTeller((String)datasession.get("teller"));
        rapg.setSupervisor((String)datasession.get("supervisor"));

        rapg.setTxnCode("0604");
        rapg.setFormat("G");
		rapg.setTxtDocs((String)datasession.get("txtDocsLoc"));
		rapg.setTxtRAPservice((String)datasession.get("servicio1"));

        String moneda = (String)datasession.get("moneda");
		rapg.setToCurr(gc.getDivisa(moneda));

        StringBuffer monto = new StringBuffer((String)datasession.get("txtMonto"));
        for(int i=0; i<monto.length();)
        {
            if( monto.charAt(i) == ',' || monto.charAt(i) == '.' )
                monto.deleteCharAt(i);
            else
                ++i;
        }
        rapg.setTranAmt(monto.toString());
        rapg.setAcctNo("1");

        String corte = (String)datasession.get("rdoACorte");
		corte="3";
/*
        if (corte.equals("01"))
            corte  = "1";
        else
            corte  = "2";
*/
        //rapg.setBenefic("0604"+(String)datasession.get("txtCodSeg")+(String)datasession.get("txtCveTran")+(String)datasession.get("txtDDACuenta2")+(String)datasession.get("txtSerial2")+"T"+corte);
		rapg.setBenefic("0604"+"0000000000000000000000000000000"+"T"+corte);

        String CI = (String)session.getAttribute("ConsecI");
        CI = CI.substring(1,CI.length());
        String CF = (String)session.getAttribute("ConsecF");
        if(CF.length() > 1)
            CF = CF.substring(1,CF.length());
        if(CF.equals("0"))
            CF = CI;

        rapg.setOrdenan("VPR " + CI + " " + CF + (String)datasession.get("s1"));

        String txtMontoTot = "";
        long MontoCI = 0;
        long MontoTotal = 0;

        txtMontoTot = (String)datasession.get("MontoTotal");
        if( txtMontoTot != null )
            MontoTotal = Long.parseLong(txtMontoTot);

        StringBuffer montoci = new StringBuffer((String)datasession.get("txtDocsCI"));
        for(int i=0; i<montoci.length();) {
            if( montoci.charAt(i) == ',' || montoci.charAt(i) == '.' )
                montoci.deleteCharAt(i);
            else
                ++i;
        }


        MontoCI= Long.parseLong(montoci.toString());

        oVolanteoDDA = new VolanteoDDA();
        String txtBanco = "";
		//String txtBanco = (String)datasession.get("txtCveTran");
        //txtBanco = txtBanco.substring(5,8);
        oVolanteoDDA.setTeller(datasession.get("teller").toString().substring(4,6));
        oVolanteoDDA.setBranch(datasession.get("sucursal").toString().substring(1));
        oVolanteoDDA.setBank(txtBanco);
        oVolanteoDDA.setCurrency((String)datasession.get("moneda"));
        oVolanteoDDA.setCorte((String)datasession.get("rdoACorte"));
        oVolanteoDDA.setChequeData((String)datasession.get("txtSerial2") +(String)datasession.get("txtDDACuenta2") + (String)datasession.get("txtCveTran") +
        (String)datasession.get("txtCodSeg"));
        oVolanteoDDA.setChequeAmount(monto.toString());

        if ( ( Long.parseLong(monto.toString()) >= TruncamientoMN  && moneda.equals("01")) ||
        ( Long.parseLong(monto.toString()) >= TruncamientoUSD  && moneda.equals("02")) ) {
            txtVolante = "SI";
            rapg.setComAmt(oVolanteoDDA.getConsecutive());
        }

        Diario diario = new Diario();
        String sMessage = diario.invokeDiario(rapg, (String)session.getAttribute("d_ConsecLiga"), dirip, moneda, session);

        if(sMessage.substring(0,1).equals("0"))
        {
            oVolanteoDDA.updateConsecutive(getString(rapg.getMessage(),1), txtVolante);
        }

        if ( sMessage.charAt(0) == '0' )
        {
            MontoTotal = MontoTotal + Long.parseLong(monto.toString());
            datasession.put("MontoTotal",new Long(MontoTotal).toString());
        }
        
        Stack flujotxn = (Stack)session.getAttribute("page.flujotxn");

        if (MontoTotal != MontoCI) {
            flujotxn.push("0604");

            session.setAttribute("page.flujotxn", flujotxn);
            datasession.remove("txtCodSeg");
            datasession.remove("txtCveTran");
            datasession.remove("txtDDACuenta2");
            datasession.remove("txtSerial2");
            datasession.remove("txtMonto");
        }

        session.setAttribute("page.txnresponse", rapg.getMessage());
        getServletContext().getRequestDispatcher("/servlet/ventanilla.Redirector").forward(request,response);
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}
