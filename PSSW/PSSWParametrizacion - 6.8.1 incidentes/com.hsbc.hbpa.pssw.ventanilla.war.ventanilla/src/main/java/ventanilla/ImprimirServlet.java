//***************************************************************************************************************
// CCN - 4360437 - 24/02/2006 - Se realiza simplifaci�n para la impresi�n del comprobante de compra-venta
// CCN - 4360446 - 10/03/2006 - Proyecto TDI fase 2 cobro de comision
// CCN - 4360455 - 07/04/2006 - Se realizan modificaciones para esquema de cheques devueltos
// CCN - 4360456 - 07/04/2006 - Se realizan modificaciones al formatos de ordenes de pago y reportes de gerente.
// CCN - 4360462 - 10/04/2006 - Se genera nuevo paquete por errores en paquete anterior
// CCN - 4360477 - 18/05/2006 - Se utiliza el nuevo formato de impresi�n en reinversiones (txn 3009,3313 y 3314)
// CCN - 4360481 - 22/05/2006 - se crea nuevo paquete a petici�n de QA
// CCN - 4360495 - 04/08/2006 - Se realizan modificaciones para formato nuevo de impresi�n en txns 0021 US$
// CCN - 4360508 - 08/09/2006 - Se realizan modificaciones para ficha de deposito
// CCN - 4360533 - 20/10/2006 - Se realizan modificaciones para OPEE
// CCN - 4360540 - 09/11/2006 - Se realizan modificaciones pra ficha de deposito
// CCN - 4360541 - 10/11/2006 - Se realizan modificaciones borrar variable de sesion con txns antriores que operan cheques.
// CCN - 4360549 - 15/12/2006 - Se realiza correcci�n en la impresi�n del Diario Electron y Totales
// CCN - 4360576 - 09/03/2007 - Modificaciones para proyecto OPMN
//***************************************************************************************************************

package ventanilla;

import java.io.IOException;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Stack;
import java.util.Vector;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class ImprimirServlet extends HttpServlet {
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException, NullPointerException {
		javax.servlet.http.HttpSession sessions = request.getSession(false);

		String txtCadImpresion = new String("");
		String txtTmpCan = new String("");
		String txtTmp = new String("");
		String txtCadTemporal = new String("");
		String txtOrdenMasiva = new String("");
		String txtExcepciones = new String("");

		java.util.Hashtable requestParameters = new java.util.Hashtable();
		Stack flujotxn = (Stack) sessions.getAttribute("page.flujotxn");

		if (sessions.getAttribute("txtAfore") != null) {
			txtTmp = sessions.getAttribute("txtAfore").toString();
		}
		if (flujotxn != null) {
			if (flujotxn.empty()) {
				if (sessions.getAttribute("txtAfore") != null) {
					sessions.setAttribute("txtCadImpresion", sessions
							.getAttribute("txtAfore").toString());
					sessions.removeAttribute("txtAfore");
				}
			}
		}
		if (sessions.getAttribute("strFlujoRap") != null)
			sessions.removeAttribute("strFlujoRap");
		if (sessions.getAttribute("strFlujoRapCAPR") != null) {
			sessions.removeAttribute("strFlujoRapCAPR");
		}
		if (sessions.getAttribute("teller") != null) {
			requestParameters.put("tellerID", sessions.getAttribute("teller")
					.toString());
			sessions.setAttribute("tellerID", requestParameters.get("tellerID")
					.toString());
		}
		if (request.getParameter("teller") != null) {
			requestParameters.put("tellerID", request.getParameter("teller")
					.toString());
			sessions.setAttribute("tellerID", requestParameters.get("tellerID")
					.toString());
		}
		if (sessions.getAttribute("Consecutivo") != null) {
			requestParameters.put("consecutiveID",
					sessions.getAttribute("Consecutivo").toString());
		}
		if (request.getParameter("Consecutivo") != null) {
			requestParameters.put("consecutiveID",
					request.getParameter("Consecutivo").toString());
			sessions.setAttribute("Consecutivo",
					request.getParameter("Consecutivo").toString());
		}
		String aditionalData = new String("");
		if (sessions.getAttribute("page.datasession") != null) {
			java.util.Hashtable ht = (java.util.Hashtable) sessions
					.getAttribute("page.datasession");
			String strcTxn = ht.get("cTxn").toString();
			if (strcTxn.equals("0024") || strcTxn.equals("0025")) {
				if (ht.get("ctacargo") != null) {
					txtOrdenMasiva = (String) ht.get("ctacargo") + "~";
					ht.remove("ctacargo");
					txtOrdenMasiva = txtOrdenMasiva
							+ (String) ht.get("mtocargo") + "~";
					ht.remove("mtocargo");
					if (strcTxn.equals("0025"))
						txtOrdenMasiva = txtOrdenMasiva
								+ (String) ht.get("nomcargo") + "~";
					else {
						txtOrdenMasiva = txtOrdenMasiva
								+ (String) ht.get("txtNomEmpresa") + "~";
						ht.remove("txtNomEmpresa");
					}
					ht.remove("nomcargo");
					ht.remove("codecargo");
					for (int i = 1; i < 11; i++)
						if (ht.get("consecs" + i) != null) {
							txtOrdenMasiva += ht.get("consecs" + i);
							ht.remove("consecs" + i);
						}
					txtOrdenMasiva += "~";
					for (int i = 1; i < 11; i++) {
						if (ht.get("nombres" + i) != null) {
							txtOrdenMasiva += ht.get("nombres" + i);
							ht.remove("nombres" + i);
						}
						if (ht.get("apellidos" + i) != null) {
							txtOrdenMasiva += " " + ht.get("apellidos" + i);
							ht.remove("apellidos" + i);
						}
					}
					txtOrdenMasiva += "~";
					for (int i = 1; i < 11; i++)
						if (ht.get("montos" + i) != null) {
							txtOrdenMasiva += ht.get("montos" + i);
							ht.remove("montos" + i);
						}
					txtOrdenMasiva += "~";
					for (int i = 1; i < 11; i++)
						if (ht.get("status" + i) != null) {
							txtOrdenMasiva += ht.get("status" + i);
							ht.remove("status" + i);
						}
					txtOrdenMasiva += "~";
				}
			}
			requestParameters.put("transactionID", ht.get("cTxn").toString());
			if (ht.get("lstCF") != null) {
				aditionalData += "COMPROBANTE_FISCAL@"
						+ ht.get("lstCF").toString() + "^";
				ht.remove("lstCF");
			} else
				aditionalData += "COMPROBANTE_FISCAL@" + "N" + "^";

			// JGA
			if (ht.get("flujorapcapr") != null) {
				sessions.setAttribute("strFlujoRapCAPR", ht.get("flujorapcapr")
						.toString());
				if (ht.get("finrapcapr") != null) {
					// if (ht.get("finrap") != null) {
					if (ht.get("finrapcapr").toString().equals("1")) {
						sessions.setAttribute("strFlujoRapCAPR", "0");
					}
					ht.remove("flujorapcapr");
					ht.remove("finrapcapr");
				}
			}
			// JGA

			if (ht.get("flujorap") != null) {
				sessions.setAttribute("strFlujoRap", ht.get("flujorap")
						.toString());
				if (ht.get("finrap") != null) {
					if (ht.get("finrap").toString().equals("1")) {
						sessions.setAttribute("strFlujoRap", "0");
					}
					ht.remove("flujorap");
					ht.remove("finrap");
				}
			}
			sessions.setAttribute("page.datasession", ht);
		}
		if (sessions.getAttribute("page.cTxn") != null) {

			/*
			 * if(sessions.getAttribute("page.cTxn").equals("SLCG")||sessions.
			 * getAttribute("page.cTxn").equals("CANC")) {
			 * 
			 * if (sessions.getAttribute("page.cTxn").equals("SLCG"))
			 * sessions.setAttribute("transactionID","4193"); if
			 * (sessions.getAttribute("page.cTxn").equals("CANC")) { if(
			 * sessions.getAttribute("Txnint") != null)
			 * 
			 * txtTmpCan = sessions.getAttribute("Txnint").toString();
			 * System.out.println(txtTmpCan); if( txtTmpCan.equals("0275")) {
			 * sessions.setAttribute("transactionID","0275");
			 * 
			 * }
			 * 
			 * }
			 * 
			 * }
			 */
			/*
			 * if(sessions.getAttribute("page.cTxn").equals("SLCG"))
			 * requestParameters.put( "transactionID","4193");
			 */
			if (sessions.getAttribute("page.cTxn").equals("SLCG")
					|| sessions.getAttribute("page.cTxn").equals("CANC")) {
				if (sessions.getAttribute("page.cTxn").equals("SLCG")) {
					requestParameters.put("transactionID", "4193");
				}
				if (sessions.getAttribute("page.cTxn").equals("CANC")) {

					if (sessions.getAttribute("Txnint") != null) {
						txtTmpCan = sessions.getAttribute("Txnint").toString();
						if (txtTmpCan.equals("0275")) {
							requestParameters.put("transactionID", "0275");
						}
					}
				}

			} else
				requestParameters.put("transactionID",
						sessions.getAttribute("page.cTxn").toString());

			sessions.setAttribute("transactionID",
					sessions.getAttribute("page.cTxn").toString());

		} else if (request.getParameter("cTxn") != null) {
			requestParameters.put("transactionID", request.getParameter("cTxn")
					.toString());
			sessions.setAttribute("transactionID",
					requestParameters.get("transactionID").toString());

		}
		if (sessions.getAttribute("txtCadImpresion") != null) {
			requestParameters.put("printParameters",
					sessions.getAttribute("txtCadImpresion"));

			txtCadTemporal = (String) sessions.getAttribute("txtCadImpresion");
		} else {
			if (request.getParameter("txtCadImpresion") != null) {
				requestParameters.put("printParameters",
						request.getParameter("txtCadImpresion"));
				txtCadTemporal = request.getParameter("txtCadImpresion")
						.toString();
			}
		}
		if (!(txtCadTemporal.startsWith("~COMPRAVENTA~0726-0106")
				|| txtCadTemporal.startsWith("~COMPRAVENTA~0726-4111")
				|| txtCadTemporal.startsWith("~COMPRAVENTA~4043-0106")
				|| txtCadTemporal.startsWith("~COMPRAVENTA~4043-4111")
				|| txtCadTemporal.startsWith("~COMPRAVENTA~0112-4111")
				|| txtCadTemporal.startsWith("~COMPRAVENTA~0128-4101")
				|| txtCadTemporal.startsWith("~COMPRAVENTA~0114-4111")
				|| txtCadTemporal.startsWith("~COMPRAVENTA~0130-4101")
				|| txtCadTemporal.startsWith("~CARGO~N$")
				|| txtCadTemporal.startsWith("~CARGO~US$")
				|| txtCadTemporal.startsWith("~EFECTIVO~N$")
				|| txtCadTemporal.startsWith("~ABONO~N$")
				|| txtCadTemporal.startsWith("~LIQUIDACION~N$")
				|| txtCadTemporal.startsWith("~CANCELACION~N$") 
				|| txtCadTemporal.startsWith("~ITBMS")
				|| txtCadTemporal.startsWith("~CHQCERT"))) {
			if (sessions.getAttribute("transactionID") != null && sessions.getAttribute("transactionID").toString().equals("1003")) {
				if (sessions.getAttribute("txtCasoEsp") != null
						&& sessions.getAttribute("transactionID") != null
						&& sessions.getAttribute("Consecutivo").toString() != null) {
					sessions.setAttribute("txtCasoEsp",
							sessions.getAttribute("transactionID").toString()
									+ "~"
									+ sessions.getAttribute("Consecutivo")
											.toString()
									+ "~"
									+ sessions.getAttribute("txtCasoEsp")
											.toString());
				}
			}
			if (sessions.getAttribute("txtCasoEsp") != null) {
				requestParameters.put("casoEspecial",
						sessions.getAttribute("txtCasoEsp").toString());
			}
			if (request.getParameter("txtCasoEsp") != null) {
				requestParameters.put("casoEspecial",
						request.getParameter("txtCasoEsp").toString());
			}
			ventanilla.com.bital.util.PrinterQuery printerQuery = new ventanilla.com.bital.util.PrinterQuery(
					requestParameters);
			txtCadImpresion = new String("");
			for (int i = 0; i < printerQuery.getData().size(); i++) {
				txtCadImpresion += printerQuery.getData().get(i) + "^";
			}
			// JGA-impresion POLAS//

			String folio = (String) sessions.getAttribute("folioNotificacion") == null ? ""
					: (String) sessions.getAttribute("folioNotificacion");
			if (!folio.equals("")) {
				txtCadImpresion += "FOLIO~" + folio + "^";
			}

			if (sessions.getAttribute("folioNotificacion") != null) {
				sessions.removeAttribute("folioNotificacion");
			}

			// /

			// hgrc CHEQUE DEVUELTO EN ARP
			Hashtable chequesAceptados = (Hashtable) sessions
					.getAttribute("chequesAceptados");
			if (chequesAceptados == null) {
				chequesAceptados = new Hashtable();
			}
			if (sessions.getAttribute("page.cTxn") != null) {
				if (sessions.getAttribute("page.cTxn").equals("5353")
						&& sessions.getAttribute("page.iTxn").equals("5503")) {
					String chqAcep[] = txtCadImpresion.split("~");
					if (chqAcep[10].equals("A")) {
						chequesAceptados.put(chqAcep[1].trim() + chqAcep[3],
								chqAcep[11] + "!" + chqAcep[3]);
						sessions.setAttribute("chequesAceptados",
								chequesAceptados);
					}
				}
			}
			Hashtable chequesdevueltos = new Hashtable();
			if ((sessions.getAttribute("ChequesDevueltos") != null && flujotxn
					.contains("5361"))
					|| sessions.getAttribute("chequesAceptados") != null) {
				if (sessions.getAttribute("ChequesDevueltos") != null) {
					chequesdevueltos = (Hashtable) sessions
							.getAttribute("ChequesDevueltos");
					Enumeration elementsCHDV = (Enumeration) chequesdevueltos
							.elements();
					while (elementsCHDV.hasMoreElements()) {
						String param = (String) elementsCHDV.nextElement();
						String strValorCD = param.substring(0,
								param.indexOf("*"));
						txtCadImpresion += "=" + strValorCD;
					}
					txtCadImpresion += "=";
				}
				if (sessions.getAttribute("chequesAceptados") != null) {
					chequesAceptados = (Hashtable) sessions
							.getAttribute("chequesAceptados");
					Enumeration elementsCHAC = (Enumeration) chequesAceptados
							.elements();
					while (elementsCHAC.hasMoreElements()) {
						String param = (String) elementsCHAC.nextElement();
						txtCadImpresion += "/#/" + param;
					}
					txtCadImpresion += "/#/";
				}
				if (txtCadImpresion.startsWith("MOVIMIENTOS_RAP", 0)) {
					sessions.removeAttribute("chequesAceptados");
				}
			}
			// fin hgrc CHEQUE DEVUELTO EN ARP
			aditionalData += "MAS_TXN@";
		}
		if (txtCadTemporal.startsWith("~LOCH")
				|| txtCadTemporal.startsWith("~LCFISCAL")) {
			txtCadTemporal = txtCadTemporal.substring(1,
					txtCadTemporal.length() - 1);
		}
		if (sessions.getAttribute("page.flujotxn") != null) {
			java.util.Stack st = (java.util.Stack) sessions
					.getAttribute("page.flujotxn");
			if (!st.isEmpty() && !st.peek().toString().equals("1111"))
				aditionalData += "S^";
			else
				aditionalData += "N^";
		} else
			aditionalData += "N^";
		txtCadImpresion += aditionalData;

		sessions.setAttribute("txtResponse", txtCadImpresion);
		if (txtCadTemporal.startsWith("~COMPRAVENTA~0726-0106")
				|| txtCadTemporal.startsWith("~COMPRAVENTA~0726-4111")
				|| txtCadTemporal.startsWith("~COMPRAVENTA~4043-0106")
				|| txtCadTemporal.startsWith("~COMPRAVENTA~4043-4111")
				|| txtCadTemporal.startsWith("~COMPRAVENTA~0112-4111")
				|| txtCadTemporal.startsWith("~COMPRAVENTA~0128-4101")
				|| txtCadTemporal.startsWith("~COMPRAVENTA~0114-4111")
				|| txtCadTemporal.startsWith("~COMPRAVENTA~0130-4101")
				|| txtCadTemporal.startsWith("~CARGO~N$")
				|| txtCadTemporal.startsWith("~CARGO~US$")
				|| txtCadTemporal.startsWith("~EFECTIVO~N$")
				|| txtCadTemporal.startsWith("~ABONO~N$")
				|| txtCadTemporal.startsWith("~LIQUIDACION~N$")
				|| txtCadTemporal.startsWith("~CANCELACION~N$")
				|| txtCadTemporal.startsWith("~ITBMS")
				|| txtCadTemporal.startsWith("~CHQCERT"))
			if (sessions.getAttribute("txtCasoEsp") != null)
				sessions.setAttribute("txtResponse",sessions.getAttribute("txtCasoEsp").toString());
		if (sessions.getAttribute("transactionID") != null)
			if (sessions.getAttribute("transactionID").toString().equals("5161"))
			{
				String txtCadImpresionTmp = sessions.getAttribute(
						"transactionID").toString()
						+ "~"
						+ sessions.getAttribute("Consecutivo").toString()
						+ "~";
				sessions.setAttribute("txtResponse", txtCadImpresionTmp);
			}
		if (txtCadTemporal.startsWith("~REINVERSION")) {
			txtCadImpresion = sessions.getAttribute("transactionID").toString()
					+ "~" + sessions.getAttribute("Consecutivo").toString()
					+ "~";
			sessions.setAttribute("txtResponse", txtCadImpresion);
		}
		if (txtCadTemporal.startsWith("~RETIRO")) {
			if (aditionalData.endsWith("N^"))
				aditionalData = "N^";
			else
				aditionalData = "S^";
			txtCadImpresion = sessions.getAttribute("transactionID").toString()
					+ "~" + sessions.getAttribute("Consecutivo").toString()
					+ "~" + "^" + "MAS_TXN@" + aditionalData;
			sessions.setAttribute("txtResponse", txtCadImpresion);
		}
		if (txtCadTemporal.startsWith("~ITBMS") 
				&& (sessions.getAttribute("transactionID").toString().equals("4155") 
				|| sessions.getAttribute("transactionID").toString().equals("4545"))) 
		{
			if (aditionalData.endsWith("N^"))
				aditionalData = "N^";
			else
				aditionalData = "S^";
			txtCadImpresion = sessions.getAttribute("txtCasoEsp").toString()
					+ "^" + "MAS_TXN@" + aditionalData;
			sessions.setAttribute("txtResponse", txtCadImpresion);
		}
		// Proyecto Daily Cash - Transaccion CSIB Adicional

		if (sessions.getAttribute("page.flujotxn") != null
				&& !(sessions.getAttribute("page.cTxn").equals("1053")
						|| sessions.getAttribute("page.cTxn").equals("1003")
						|| sessions.getAttribute("page.cTxn").equals("1001")
						|| sessions.getAttribute("page.cTxn").equals("4153")
						|| sessions.getAttribute("page.cTxn").equals("0476")
						|| sessions.getAttribute("page.cTxn").equals("1057")
						|| sessions.getAttribute("page.cTxn").equals("0468")
						|| sessions.getAttribute("page.cTxn").equals("4537")
						|| sessions.getAttribute("page.cTxn").equals("1193")
						|| sessions.getAttribute("page.cTxn").equals("1195")
						|| sessions.getAttribute("page.cTxn").equals("5357") || sessions
						.getAttribute("page.cTxn").equals("0604"))) {
			java.util.Stack st = (java.util.Stack) sessions
					.getAttribute("page.flujotxn");
			if ((!st.empty() && st.peek().toString().equals("CSIB"))) {
				if (aditionalData.endsWith("N^"))
					aditionalData = "N^";
				else
					aditionalData = "S^";
				txtCadImpresion = sessions.getAttribute("txtCasoEsp")
						.toString() + "^" + "MAS_TXN@" + aditionalData;
				sessions.setAttribute("txtResponse", txtCadImpresion);

			}
		}
		//
		if (sessions.getAttribute("page.flujotxn") != null && (sessions.getAttribute("page.cTxn").equals("0061"))) {
			String addDataNoImp2 ="";
			Vector vCamposImp = Campos((String)sessions.getAttribute("txtResponse"), "|");
			for(int zi=0; zi < vCamposImp.size(); zi++){
				if(zi != 1){
				addDataNoImp2 += (String)vCamposImp.elementAt(zi);
				}
			}		
			sessions.setAttribute("txtResponse", addDataNoImp2);
		}
		
		
		if (!txtOrdenMasiva.equals("")) {
			txtCadTemporal += txtOrdenMasiva;
		}
		if (sessions.getAttribute("txn5361") != null) {
			sessions.setAttribute("transactionID", "txn5361");
		}
		if (sessions.getAttribute("transactionID") == null) {
			sessions.setAttribute("transactionID", "0000");
		}
		if ((sessions.getAttribute("transactionID").toString().equals("1003"))
				|| (sessions.getAttribute("transactionID").toString()
						.equals("1001"))
				|| (sessions.getAttribute("transactionID").toString()
						.equals("4541"))) {
			sessions.setAttribute("transactionID", "txn1003");
			sessions.setAttribute("strRespuesta",
					sessions.getAttribute("txtResponse").toString());
		}
		sessions.setAttribute("txtCadImpresion", txtCadTemporal);

		response.sendRedirect("../ventanilla/paginas/DespImp.jsp");
	}

	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}
	
	public Vector Campos(String cadena, String delimitador) {
		ventanilla.com.bital.util.NSTokenizer parser = new ventanilla.com.bital.util.NSTokenizer(cadena, delimitador);
		Vector campos = new Vector();
		while (parser.hasMoreTokens()) {
			String token = parser.nextToken();
			//System.out.println("Imprimir Servlet--CAMPOS--"+delimitador+"cadena="+token);
			if (token == null)
				continue;
			campos.addElement(token);
		}
		return campos;
	}
}
