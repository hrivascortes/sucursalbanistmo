//*************************************************************************************************
//             Funcion: Clase para autorizaciones Remotas
//            Elemento: autorizRemote.java
//          Creado por: Rutilo Zarco
//      Modificado por: Juvenal Fernandez
//*************************************************************************************************
// CCN - 4360259 - 21/01/2005 - Se agrega el nombre en la autorizacion remota
// CCN - 4360274 - 16/02/2005 - Se corrige problema con horario C
// CCN - 4360325 - 03/06/2005 - Se apuntan los cursores de selección a NULL
// CCN - 4360334 - 17/06/2005 - Se elimina variable dbQualifier
// CCN - 4360364 - 19/08/2005 - Se consulta RACF del gerente para reportes
// CCN - 4360387 - 07/10/2005 - Se realizan adecuaciones para pedir autorización cuando se desfirme el cajero.
//*************************************************************************************************

package ventanilla;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.Hashtable;
import java.util.StringTokenizer;
import java.util.Vector;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ventanilla.com.bital.util.FilterDataAR;
import ventanilla.com.bital.util.MD5digest;

public class authorizRemote extends HttpServlet
{
  public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
  {
    javax.servlet.http.HttpSession session = request.getSession();
	GenericClasses gc = new GenericClasses();
    //Eliminar datos de session para Autorizacion Remota.
    session.removeAttribute("data.to.authoriz");
    session.removeAttribute("user.par");
    Hashtable cajeros = new Hashtable();
    String statementPS = com.bital.util.ConnectionPoolingManager.getStatementPostFix();
    String PostFixUR   = com.bital.util.ConnectionPoolingManager.getPostFixUR();
    Hashtable datasession = (Hashtable)session.getAttribute("page.datasession");
    int horarioc = 0;
    String teller = new String();
    String moneda = new String("00");
    String nivel = "3";
    //Obtiene el Cajero 
    if(session.getAttribute("teller") != null)
      teller = (String)session.getAttribute("teller");
    else
    {
      teller = request.getParameter("teller");
      horarioc = 1;
    }
    //Obtiene la moneda 
    if (datasession == null)
        datasession = new Hashtable();
    if (datasession.get("moneda") != null)
    {    moneda = (String)datasession.get("moneda");}
    else if(session.getAttribute("page.moneda") != null)
    {    moneda = (String)session.getAttribute("page.moneda");}
    //Cambia el Formato de la moneda
    moneda=gc.getDivisa(moneda);
    
    String NameTxn="";
    
        int indice = Integer.parseInt(request.getParameter("indice"));
        String ValuestoAuto = request.getParameter("ValuesToAuto");
        String newcajero = request.getParameter("newcajero");
        int override = Integer.parseInt(request.getParameter("override"));
        String firmaCajeroC = request.getParameter("firmaCajeroC");
        StringBuffer values = new StringBuffer();
        String sucursal = new String();
		String iTxn = (String)session.getAttribute("page.iTxn");
		String oTxn = (String)session.getAttribute("page.oTxn");
        if(session.getAttribute("branch") != null)
          sucursal = (String)session.getAttribute("branch");
        else
          sucursal = teller.substring(0,5);
        values.append("sucursal=" + sucursal + "#")
              .append("userapplicant=" + teller + "#");
        if ((iTxn !=null && oTxn != null && iTxn.equals("0726") && oTxn.equals("0106")) || ValuestoAuto.length() > 0) //Autorizacion flujo normal
        {
           String cTxn = (String)session.getAttribute("page.cTxn");
           
           if(cTxn.equals("0083"))
            NameTxn="Salida Permanente";
            
            if(cTxn.equals("0082"))
            NameTxn="Salida Temporal";
            
           if (cTxn.equals("5405") || cTxn.equals("5407") || cTxn.equals("0332") || cTxn.equals("0368") || cTxn.equals("4503"))
               nivel = "4";
           if(cTxn.equals("0083") || cTxn.equals("0082"))
              {
              	
              	values.append("transaccion=" + cTxn + "#")
                 //.append("nombretxn=Salida Permanente#");
                  .append("nombretxn=" + cTxn + "#");
              }
            else
            {  	 
           values.append("transaccion=" + cTxn + "#")
                 .append("nombretxn=" + (String)session.getAttribute("page.txnlabel") + "#");
            }     
           if (!moneda.equals("00"))
             values.append("moneda=" + moneda + "#");
           StringTokenizer tk = new StringTokenizer(ValuestoAuto, "#");
           Hashtable list = new Hashtable();
           while (tk.hasMoreElements())
           {
             String tmp = (String)tk.nextElement();
             int index = tmp.indexOf("=");
             list.put(tmp.substring(0,index), tmp.substring(index+1));
           }
           FilterDataAR filter = new FilterDataAR(list);
           values.append(filter.generate());
        }
        if (indice > -1) //Autorizacion reverso de Diario Electronico
        {
            ventanilla.com.bital.admin.DiarioQuery DiarioSQL = (ventanilla.com.bital.admin.DiarioQuery)session.getAttribute("session.DiarioSQL");
            Hashtable Data = new Hashtable();
            int nopagina   = Integer.parseInt((String)request.getParameter("nopagina"));
            Data = DiarioSQL.getData(nopagina-1);
            Vector Ctxn     = (Vector)Data.get("Txn");
            Vector Caccount = (Vector)Data.get("Account");
            Vector Camount  = (Vector)Data.get("Amount");
            Vector Ccash    = (Vector)Data.get("Cash");
            Vector Cmoneda  = (Vector)Data.get("Moneda");
            values.append("transaccion=" + Ctxn.get(indice) + "#")
                  .append("monto=" + Camount.get(indice) + "#")
                  .append("cuenta=" + Caccount.get(indice) + "#")
                  .append("efectivo=" + Ccash.get(indice) + "#")
                  .append("moneda=" + Cmoneda.get(indice) + "#")
                  .append("nombretxn=Reverso de Transaccion#");
        }

        if (!newcajero.equals("0")) //Cambio de Cajero en Diario Electronico
        {
            values.append("transaccion=Cambio de Usuario en DiarioElectronico#")
                  .append("nombretxn=Cajero Actual: " + teller + "  ---  Cajero Nuevo: " + newcajero +"#");
        }
        if (override == 1) //Autorizacion por Override
        {
           String cTxn = (String)session.getAttribute("page.cTxn");
           
            
           
            if(cTxn.equals("0083"))
            NameTxn="Salida Permanente";
            
            if(cTxn.equals("0082"))
            NameTxn="Salida Temporal";
           
           if (cTxn.equals("1051") || cTxn.equals("1053") || cTxn.equals("1075") || cTxn.equals("1077") || cTxn.equals("1079") ||
               cTxn.equals("1081") || cTxn.equals("1093") || cTxn.equals("4251") || cTxn.equals("4271") || cTxn.equals("5051") ||
               cTxn.equals("5353"))
               nivel = "4";
            
            if(cTxn.equals("0083") || cTxn.equals("0082"))
              {
              	
              	values.append("transaccion=" + cTxn + "#")
                 //.append("nombretxn=Salida Permanente#");
                  .append("nombretxn=" + NameTxn + "#");
              }
           else
           {
           values.append("transaccion=" + (String)session.getAttribute("page.cTxn") + "#")
                 .append("nombretxn=" + (String)session.getAttribute("page.txnlabel") + "#");
           }
            if(!cTxn.equals("0082") && !cTxn.equals("0083")) 
           {
        	   if(!moneda.equals("00")){
	               values.append("moneda=" + moneda + "#");
	           }
	           FilterDataAR filter = new FilterDataAR(datasession);
	           values.append(filter.generate());	   
           }
	                 
        }
        if(firmaCajeroC.equals("1")){ //firma de usuario en horario C
            values.append("transaccion=Firma de Usuario en HORARIO C#")
                  .append("nombretxn=#");
        }
        //Se agrega el nombre del cajero
        if (horarioc == 0)
        {
        	String nombre = new String(""); 
			if (session.getAttribute("userinfo") == null)
				nombre = (String)session.getAttribute("NombreCajero");
			else
			{
				Hashtable userinfo = (Hashtable)session.getAttribute("userinfo");
				nombre  =(String)userinfo.get("D_NOMBRE");
			}
             values.append("nombrecajero=").append(nombre).append("#");
        }
        else
        {
          java.sql.Connection pooledConnection = com.bital.util.ConnectionPoolingManager.getPooledConnection( );
		  java.sql.PreparedStatement newPreparedStatement = null;
		  java.sql.ResultSet rs = null;
          try
          {
             if(pooledConnection != null)
             {
               String select = "SELECT d_nombre from TC_USUARIOS_SUC where c_cajero = '" + teller+ "' " + PostFixUR;
               newPreparedStatement = pooledConnection.prepareStatement(select);
               rs = newPreparedStatement.executeQuery();
               java.sql.ResultSetMetaData rsmd = rs.getMetaData();               
               String nombre = new String("");
               while(rs.next())
                  nombre = (String)rs.getString(1);
               values.append("nombrecajero=").append(nombre).append("#");
               session.setAttribute("NombreCajero",nombre);
             }
          }
          catch(java.sql.SQLException sqlexception)
          {
            System.out.println("Error AuthorizServlet <doPost>: [" + sqlexception.toString() + "] ");
          }
		  finally 
		  {
			try 
			{
			  if(rs != null) 
			  {
				  rs.close();
				  rs = null;
			  }
			  if(newPreparedStatement != null) 
			  {
				  newPreparedStatement.close();
				  newPreparedStatement = null;
			  }
			  if(pooledConnection != null) 
			  {
				  pooledConnection.close();
				  pooledConnection = null;
			  }
			 }
			 catch(java.sql.SQLException sqlException)
			 {
			  System.out.println("Error FINALLY autorizRemote::doPost::Select [" + sqlException + "]");
			 }
		  }
        }    
     
        session.setAttribute("data.to.authoriz", values.toString());
        java.sql.Connection pooledConnection = com.bital.util.ConnectionPoolingManager.getPooledConnection( );
		java.sql.PreparedStatement newPreparedStatement = null;
		java.sql.ResultSet rs = null;
        try
        {
           if(pooledConnection != null)
           {
             String select = "SELECT d_direccion_ip, d_clave_acc, c_cajero, d_nombre, n_nivel, d_ejct_cta from TC_USUARIOS_SUC where c_cajero like '" + teller.substring(0,4) + "%' and n_nivel>=" + nivel + " and d_direccion_ip <> '' order by n_nivel asc " + PostFixUR;             
             newPreparedStatement = pooledConnection.prepareStatement(select);
             rs = newPreparedStatement.executeQuery();
             java.sql.ResultSetMetaData rsmd = rs.getMetaData();
             MD5digest md5 = new MD5digest();
             int numrow = 1;
             while(rs.next())
             {
               for (int i=1; i<=rsmd.getColumnCount(); i++)
               {
                 String name = rsmd.getColumnName(i);
                 String value = "";
                 if (name.equals("D_CLAVE_ACC") || name.equals("C_CAJERO"))
                    value = md5.md5crypt(rs.getString(i));
                 else
                    value = rs.getString(i);
                 cajeros.put(name + numrow ,value);
               }
               numrow++;
             }
             cajeros.put("numrow",Integer.toString(numrow));
           }
        }
        catch(java.sql.SQLException sqlexception)
        {
           System.out.println("Error AuthorizServlet <doPost>: [" + sqlexception.toString() + "] ");
        }
        catch(Exception e)
        {
          e.printStackTrace();
          if (e instanceof NoSuchAlgorithmException)
          {
            System.out.println("Se genero error de Algoritmo[RSA,MD5...]");
          }
        }
	    finally {
		  try {
		  	if(rs != null) 
		  	{
				rs.close();
				rs = null;
			}
			if(newPreparedStatement != null) 
			{
				newPreparedStatement.close();
				newPreparedStatement = null;
			}
			if(pooledConnection != null) 
			{
				pooledConnection.close();
				pooledConnection = null;
			}
		}
		catch(java.sql.SQLException sqlException)
		{
			System.out.println("Error FINALLY autorizRemote::doPost::Select [" + sqlException + "]");
		}
	}
        session.setAttribute("user.par", cajeros);
        response.sendRedirect("../ventanilla/paginas/UsersAutoRemote.jsp");
  }

  public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
  {
    doPost(request, response);
  }
}
