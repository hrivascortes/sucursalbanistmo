//*************************************************************************************************
//             Funcion: Clase que realiza txn Servicios 0204 / 0206 / 0208 / 0212 / 0214
//            Elemento: Group09Serv.java
//          Creado por: Alejandro Gonzalez Castro
//      Modificado por: Alejandro Gonzalez Castro
//*************************************************************************************************
// CCN - 4360189 - 03/09/2004 - Se cambia invoke al Diario para Control de Efectivo
// CCN - 4360312 - 26/04/2005 - Se agregan transacciones 0208, 0212 y 0214 para la separacion de ATM's
//*************************************************************************************************
package ventanilla;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.util.Hashtable;

import ventanilla.com.bital.sfb.ServiciosB;
import ventanilla.com.bital.admin.Diario;

public class Group09Serv extends HttpServlet
{
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession(false);
        Hashtable datasession = (Hashtable)session.getAttribute("page.datasession");
        String txn = (String)datasession.get("cTxn");
		GenericClasses gc = new GenericClasses();
        String moneda = gc.getDivisa((String)datasession.get("moneda"));
        String[] invoqued = request.getParameterValues("txtInvoqued");
        String invTxn = "";
        if(invoqued != null)
            invTxn = (String)invoqued[0];
        String dirip =  (String)session.getAttribute("dirip");
        String msg = "";
        
        ServiciosB serviciob = new ServiciosB();
        serviciob.setBranch((String)datasession.get("sucursal"));
        serviciob.setTeller((String)datasession.get("teller"));
        serviciob.setSupervisor((String)datasession.get("teller"));
		String empno = (String)session.getAttribute("empno");
		String txtRegResp = (String)datasession.get("txtRegResp");
		
        serviciob.setFormat("B");
        String Monto = "";
        serviciob.setTxnCode(txn);
        
        serviciob.setFromCurr(moneda);
        
        serviciob.setAcctNo(txn);
        Monto = quitap((String)datasession.get("txtMontoNoCero"));
        serviciob.setTranAmt(Monto);
        String numSuc = (String)datasession.get("sucursal");
        if(numSuc.length()>4)
            numSuc = numSuc.substring(numSuc.length()-4);
            
     
        
        if( txn.equals("0204") || txn.equals("0206")) {
            serviciob.setReferenc("              I" + numSuc + (String)datasession.get("lstNemo"));
        }
        else if (txn.equals("0214")) {        	
            serviciob.setReferenc(empno+txtRegResp+"I" + numSuc + (String)datasession.get("lstNemo")+(String)datasession.get("lst0214"));
        }
        else
        {
            serviciob.setReferenc(empno+txtRegResp+"I" + numSuc + (String)datasession.get("lstNemo"));
        }
        
     
        
        if( txn.equals("0204"))
        { serviciob.setCashOut(Monto);}
        
        if (txn.equals("0214"))
        { serviciob.setCashIn(Monto);}    
        

        Diario diario = new Diario();
        String sMessage = diario.invokeDiario(serviciob, (String)session.getAttribute("d_ConsecLiga"), dirip, moneda, session);
       
        session.setAttribute("page.txnresponse",sMessage);
        getServletContext().getRequestDispatcher("/servlet/ventanilla.Redirector").forward(request,response);
        
    }
    
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
    
    private String quitap(String Valor) {
        StringBuffer Cantidad = new StringBuffer(Valor);
        for(int i=0; i<Cantidad.length();) {
            if( Cantidad.charAt(i) == ',' || Cantidad.charAt(i) == '.' )
                Cantidad.deleteCharAt(i);
            else
                ++i;
        }
        return Cantidad.toString();
    }
    public String getmoneda(String cmoneda) {
        String newmoneda = "";
        if (cmoneda.equals("N$"))
        { newmoneda = "01";}
        else if (cmoneda.equals("US$"))
        { newmoneda = "02";}
        else
        { newmoneda = "03";}
        return newmoneda;
    }
    

    
    public String removeAsterisk(String campo) {
        StringBuffer result = new StringBuffer(campo);
        String caracter = result.substring(result.length()-1,result.length());
        if (caracter.equals("*"))
            result.deleteCharAt(result.length()-1);
        return result.toString();
    }
   
}
