//*************************************************************************************************
//             Funcion: Clase que realiza la administracion de usuarios del gerente
//            Elemento: Group01Users.java
//          Creado por: Alejandro Gonzalez Castro
//      Modificado por: Juvenal R. Fernandez V.
//*************************************************************************************************
// CCN - 4360334 - 17/06/2005 - Se elimina variable dbQualifier
//*************************************************************************************************
package ventanilla;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.util.Hashtable;
import java.sql.*;
import com.bital.util.ConnectionPoolingManager;

public class Group01Users extends HttpServlet
{
  public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
  {
    response.setContentType("text/plain");
    HttpSession session = request.getSession(false);
    Hashtable datasession = (Hashtable)session.getAttribute("page.datasession");

    String statementPS = ConnectionPoolingManager.getStatementPostFix();
    Connection pooledConnection = ConnectionPoolingManager.getPooledConnection();
    PreparedStatement update = null;

    String pssw = (String)datasession.get("pssword");
    String teller = (String)datasession.get("teller");
    String mensaje = "";
    try
    {
        if(pooledConnection != null)
        {
            update = pooledConnection.prepareStatement("UPDATE TC_USUARIOS_SUC SET D_CLAVE_ACC='"+pssw+"' WHERE C_CAJERO =?"+ statementPS);
            update.setString(1,teller);
            int code = update.executeUpdate();

            if( code == 1)
            {
                mensaje = "0~01~0010101~CONSECUTIVO:  010101 TRANSACCION ACEPTADA~";
            }
            else
            {
                mensaje = "1~01~0010101~CONSECUTIVO:  010101 TRANSACCION RECHAZADA~";
            }

        }
    }
    catch(SQLException sqlException)
    {System.out.println("Error Group01Users::[" + sqlException + "]");}
    finally
    {
        try
        {
            if(update != null)
            {
                update.close();
                update = null;
            }
            if(pooledConnection != null)
            {
                pooledConnection.close();
                pooledConnection = null;
            }
        }
        catch(java.sql.SQLException sqlException)
        {System.out.println("Error FINALLY Group01Users [" + sqlException + "]");}
    }
	    session.setAttribute("page.txnresponse", mensaje);
    	getServletContext().getRequestDispatcher("/servlet/ventanilla.Redirector").forward(request,response);
  }

  public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
  {
    doPost(request, response);
  }
}
