//*************************************************************************************************
//             Funcion: Clase que realiza txn 0047 - 9853 - 9813 - 0049
//            Elemento: Group02TR.java
//          Creado por: Alejandro Gonzalez Castro
//      Modificado por: Alejandro Gonzalez Castro
//*************************************************************************************************
// CCN - 4360189 - 03/09/2004 - Se cambia invoke al Diario para Control de Efectivo
//*************************************************************************************************
package ventanilla;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import ventanilla.com.bital.admin.Diario;
import java.util.Hashtable;
import ventanilla.com.bital.sfb.Transferencia;

public class Group02TR extends HttpServlet {
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/plain");
        HttpSession session = request.getSession(false);
        Hashtable datasession = (Hashtable)session.getAttribute("page.datasession");
		GenericClasses gc = new GenericClasses();

        //Variables para diario electronico
        String dirip =  (String)session.getAttribute("dirip");

        Transferencia transferencia = new Transferencia();

        transferencia.setBranch((String)datasession.get("sucursal"));
        transferencia.setTeller((String)datasession.get("teller"));
        transferencia.setSupervisor((String)datasession.get("supervisor"));

        if ( (String)datasession.get("override") != null )
             if (datasession.get("override").toString().equals("SI"))
                transferencia.setOverride("3");

        if ( (String)datasession.get("supervisor") != null )
             transferencia.setSupervisor((String)datasession.get("supervisor"));

        String txn = (String)datasession.get("cTxn");

        String moneda = "";

        if (txn.equals("9853") || txn.equals("9859"))
        {
            transferencia.setTxnCode(txn);
            transferencia.setAcctNo((String)datasession.get("txtDDAO"));

            String inversion = (String)datasession.get("txtCDAD");

            transferencia.setTranDesc("CARGO P/COMPRA UDIS " + inversion );

            StringBuffer monto = new StringBuffer((String)datasession.get("txtMonto") );
            for(int i=0; i<monto.length();) {
                if( monto.charAt(i) == ',' || monto.charAt(i) == '.' )
                    monto.deleteCharAt(i);
                else
                    ++i;
            }
            transferencia.setTranAmt(monto.toString());
			
            moneda = (String)datasession.get("moneda");
			transferencia.setTranCur(gc.getDivisa(moneda));
        }

        if (txn.equals("9813") || txn.equals("9811")) {
            transferencia.setTxnCode(txn);
            transferencia.setAcctNo((String)datasession.get("txtCDAD"));

            String cheques = (String)datasession.get("txtDDAO");

            transferencia.setTranDesc("ABONO P/COMPRA UDIS " + cheques );
            transferencia.setTranAmt((String)datasession.get("montoUDI"));
            transferencia.setTranCur("UDI");
            moneda = "UDI";
        }
        transferencia.setDescRev("EMPTY");

        Diario diario = new Diario();
        String sMessage = diario.invokeDiario(transferencia, (String)session.getAttribute("d_ConsecLiga"), dirip, moneda, session);

        session.setAttribute("page.txnresponse", sMessage);
        getServletContext().getRequestDispatcher("/servlet/ventanilla.Redirector").forward(request,response);
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}
