//*************************************************************************************************
//             Funcion: Clase que realiza txn 0021
//            Elemento: OP_0021.java
//          Creado por: Alejandro Gonzalez Castro
//      Modificado por: Fausto Rodrigo Flores Moreno
//      Modificado por: Carolina Velas
//*************************************************************************************************
// CCN - 4360189 - 03/09/2004 - Se cambia invoke al Diario para Control de Efectivo
// CCN - 4360368 - 02/09/2005 - Se elimina sleep
// CCN - 4360374 - 08/09/2005 - Se genera nuevo paquete a peticion de QA.
// CCN - 4360522 - 06/10/2006 - Se realizan modificaciones para eliminar la opci�n de Otros Bancos
// CCN - 4620010 - 19/09/2007 - Se realizan modificaciones
//*************************************************************************************************
package ventanilla;

import java.io.IOException;
import java.util.Calendar;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import ventanilla.com.bital.admin.Diario;
import ventanilla.com.bital.sfb.CompraVenta;
import ventanilla.com.bital.sfb.OrdenPago;

public class OP_0021 extends HttpServlet {
    private String stringFormat(int option, int sum) {
        Calendar now = Calendar.getInstance();
        String temp = new Integer(now.get(option) + sum).toString();
        if( temp.length() != 2 )
            temp = "0" + temp;

        return temp;
    }

    private String setCommaToString(String newCadNum) {
        int iPIndex = newCadNum.indexOf(","), iLong;
        String szTemp;

        if(iPIndex > 0) {
            newCadNum = new String(newCadNum + "00");
            newCadNum = newCadNum.substring(0, iPIndex + 1) + newCadNum.substring(iPIndex + 1, iPIndex + 3);
        }
        else {
            for(int i = newCadNum.length(); i < 3; i++)
                newCadNum = "0" + newCadNum;
            iLong = newCadNum.length();
            if(iLong == 3)
                szTemp = newCadNum.substring(0, 1);
            else
                szTemp = newCadNum.substring(0, iLong - 2);
            newCadNum = szTemp + "," + newCadNum.substring(iLong - 2);
        }

        return newCadNum;
    }
    private String delCommaPointFromString(String newCadNum) {
        String nCad = new String(newCadNum);
        if( nCad.indexOf(".") > -1) {
            nCad = nCad.substring(nCad.indexOf(".") + 1, nCad.length());
            if(nCad.length() != 2) {
                String szTemp = new String("");
                for(int j = nCad.length(); j < 2; j++)
                    szTemp = szTemp + "0";
                newCadNum = newCadNum + szTemp;
            }
        }

        StringBuffer nCadNum = new StringBuffer(newCadNum);
        for(int i = 0; i < nCadNum.length(); i++)
            if(nCadNum.charAt(i) == ',' || nCadNum.charAt(i) == '.')
                nCadNum.deleteCharAt(i);

        return nCadNum.toString();
    }

    private String getString(String newCadNum, int NumSaltos) {
        int iPIndex = newCadNum.indexOf("CONSECUTIVO:") + 12, Num = 0, Ban = 0;
        String nCadNum = new String("");
/*   NumSaltos = 4;
   if(newOption == 0)
    NumSaltos = 3;*/
        while(Num < NumSaltos) {
            if(newCadNum.charAt(iPIndex) == 0x20) {
                while(newCadNum.charAt(iPIndex) == 0x20)
                    iPIndex++;
                Num++;
            }
            else
                while(newCadNum.charAt(iPIndex) != 0x20)
                    iPIndex++;
        }
        while(newCadNum.charAt(iPIndex) != 0x20) {
            nCadNum = nCadNum + newCadNum.charAt(iPIndex++);
            if(newCadNum.charAt(iPIndex) == 0x20 && newCadNum.charAt(iPIndex + 1) == 0x2A) {
                nCadNum = nCadNum + newCadNum.charAt(iPIndex) + newCadNum.charAt(iPIndex + 1);
                iPIndex += 2;
            }
        }


        return nCadNum;
    }

    private String getPositionalString(String inputString, int initialPosition, int maxNumberOfBytes, String withSpaces){
        String outputString = new String("");
        int i = 0;
        if(maxNumberOfBytes == 0)
            maxNumberOfBytes = inputString.length();
        if(withSpaces.equals("YES"))
            while(initialPosition < inputString.length() && inputString.charAt(initialPosition) != 0x20 && i++ < maxNumberOfBytes)
                outputString += inputString.charAt(initialPosition++);
        else
            while(initialPosition < inputString.length() && i++ < maxNumberOfBytes)
                outputString += inputString.charAt(initialPosition++);
        if(outputString.length() == 0)
            outputString = new String("000");
        return outputString;
    }

    private String dataFiller(String inputString, int numberOfBytes, String shiftSide){
        int inputLength = inputString.length();
        for(int i = inputLength; i < numberOfBytes; i++)
            if(shiftSide.equals("LEFT"))
                inputString += " ";
            else
                inputString = " " + inputString;
        return inputString;
    }

    public void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {
        String time = stringFormat(Calendar.HOUR_OF_DAY, 0) + stringFormat(Calendar.MINUTE, 0);
        String TI, TE;
        StringBuffer E = new StringBuffer("");

        CompraVenta oTxn = new CompraVenta();
        OrdenPago oTxnOP = new OrdenPago();

        Diario diario;
        String sMessage = new String("");

        HttpSession session = req.getSession(false);
        java.util.Hashtable datasession = (java.util.Hashtable)session.getAttribute("page.datasession");

        oTxnOP.setOPSucursal((String)datasession.get("sucursal"));        
        String dirip =  (String)session.getAttribute("dirip");
        String msg = null;
        int noTax = 0;
        String noTaxS = new String("");
        TE = (String)datasession.get("cTxn");
        TI = (String)datasession.get("iTxn");
        String Currency2 = (String)datasession.get("moneda");
        String txtCasoEsp = new String("");
        String txtCadImpresion = new String("");

        oTxn.setTxnCode(TE);
        oTxn.setBranch((String)datasession.get("sucursal"));
        oTxn.setTeller((String)datasession.get("teller"));
        oTxn.setSupervisor((String)datasession.get("teller"));

        if(TE.equals("0814"))
        {
            String accchrg = (String)datasession.get("lstForPago");
            if(accchrg.trim().equals("01"))
            {
                oTxn.setFormat("B");
                oTxn.setAcctNo( (String)session.getAttribute("plaza") );
                oTxn.setTranAmt( delCommaPointFromString((String)datasession.get("txtMonto")) );
                oTxn.setReferenc("1");   //Requerido por ODS
                oTxn.setFees("0");
                oTxn.setInter("0");
                oTxn.setWH("0");
                oTxn.setCashIn("000");
                oTxn.setFromCurr("N$");

                diario = new Diario();
                sMessage = diario.invokeDiario(oTxn, (String)session.getAttribute("d_ConsecLiga"), dirip, "N$", session);

                if(sMessage.startsWith("0"))
                {
                    String domiSuc = getPositionalString(oTxn.getMessage(),91,30, "NO");
                    datasession.put("domiSuc", domiSuc);
					String ordenante = (String)datasession.get( "txtNomOrd" ) + " " + (String)datasession.get( "txtApeOrd" );
                    session.setAttribute("page.datasession", datasession);
                }

                /*String seleccion = (String)datasession.get("txtRFC01");
				
	                if(seleccion.length() > 0)
	                    session.setAttribute("page.getoTxnView", "ComIVAFmt");
	                noTax = 0;
	             */
				}
            
            else
            {
                noTax = 1;
                noTaxS = "0~02~0171902~LA COMISION SE CARGARA A LA CUENTA                                                                   * +TRANSACCION ACEPTADA +~";
            }
        }
        else if(TE.equals("0560"))
        {

            oTxn.setTxnCode(TE);
            oTxn.setFormat("B");
            oTxn.setAcctNo( TE );
            oTxn.setTranAmt( delCommaPointFromString((String)datasession.get( "txtMonto" )) );
            oTxn.setReferenc("EXPEDICION O.P.");
            oTxn.setFees("0");
            oTxn.setInter("0");
            oTxn.setWH("0");
            oTxn.setCashIn( delCommaPointFromString((String)datasession.get( "txtMonto" )) );
            oTxn.setFromCurr("N$");

            diario = new Diario();
            sMessage = diario.invokeDiario(oTxn, (String)session.getAttribute("d_ConsecLiga"), dirip, "N$", session);


            String Monto = delCommaPointFromString((String)datasession.get("txtMonto"));
            if(sMessage.startsWith("0"))
            {

                txtCasoEsp = txtCasoEsp + TE + "~" + getString(oTxn.getMessage(),1) + "~";

                oTxn.setTxnCode("0354");
                oTxn.setFormat("B");
                oTxn.setAcctNo( "0354" );
                oTxn.setTranAmt( delCommaPointFromString((String)datasession.get( "montoComision" )) );
                oTxn.setReferenc("COMISION O.P.");
                oTxn.setFees("0");
                oTxn.setInter("0");
                oTxn.setWH("0");
                oTxn.setCashIn( delCommaPointFromString((String)datasession.get( "montoComision" )) );
                oTxn.setFromCurr("N$");

                diario = new Diario();
                sMessage = diario.invokeDiario(oTxn, (String)session.getAttribute("d_ConsecLiga"), dirip, "N$", session);

                Monto = delCommaPointFromString((String)datasession.get("montoComision"));
                if(sMessage.startsWith("0"))
                {

                    txtCasoEsp = txtCasoEsp + "0354" + "~" + getString(oTxn.getMessage(),1) + "~";

                    oTxn.setTxnCode("0372");
                    oTxn.setFormat("B");
                    oTxn.setAcctNo( "0372" );
                    oTxn.setTranAmt( delCommaPointFromString((String)datasession.get( "montoIVA" )) );
                    oTxn.setReferenc("IVA O.P.");
                    oTxn.setFees("0");
                    oTxn.setInter("0");
                    oTxn.setWH("0");
                    oTxn.setCashIn( delCommaPointFromString((String)datasession.get( "montoIVA" )) );
                    oTxn.setFromCurr("N$");

                    diario = new Diario();
                    sMessage = diario.invokeDiario(oTxn, (String)session.getAttribute("d_ConsecLiga"), dirip, "N$", session);

                    Monto = delCommaPointFromString((String)datasession.get("montoIVA"));

                    if(sMessage.startsWith("0"))
                    {

                        txtCasoEsp = txtCasoEsp + "0372" + "~" + getString(oTxn.getMessage(),1) + "~";

                        oTxnOP.setProcessCode("EXP ");
                        String date = new String(stringFormat(Calendar.YEAR, 0).substring(3) + stringFormat(Calendar.MONTH, 1) + stringFormat(Calendar.DAY_OF_MONTH, 0));
                        oTxnOP.setField32( date, "MXN", setCommaToString( delCommaPointFromString((String)datasession.get( "txtMonto" ) ) ) );
                        oTxnOP.setField14a((String)datasession.get( "txtApeOrd" ));
                        oTxnOP.setField14n((String)datasession.get( "txtNomOrd" ));
                        oTxnOP.setField12a((String)datasession.get( "txtApeBen" ));
                        oTxnOP.setField12n((String)datasession.get( "txtNomBen" ));
                        oTxnOP.setField27("EXP");
                        oTxnOP.setField17f(date);
                        oTxnOP.setField17t(time);
                        oTxnOP.setTeller((String)datasession.get("teller"));
                        oTxnOP.setBranch((String)datasession.get("sucursal"));
                        oTxnOP.setFromCurr("N$");

                        // datos para la respuesta del diario
                        oTxnOP.setNomOrd((String)datasession.get( "txtNomOrd" ));
                        oTxnOP.setApeOrd((String)datasession.get( "txtApeOrd" ));
                        oTxnOP.setNomBen((String)datasession.get( "txtNomBen" ));
                        oTxnOP.setApeBen((String)datasession.get( "txtApeBen" ));
                        oTxnOP.setTranAmt(delCommaPointFromString((String)datasession.get("txtMonto")));
                        oTxnOP.setBanco((String)session.getAttribute("identidadApp"));
                        oTxnOP.setReversable("S");

                        diario = new Diario();
                        sMessage = diario.invokeDiario(oTxnOP, (String)session.getAttribute("d_ConsecLiga"), dirip, "N$", session);

                        long dTotal = 0;

                        if(sMessage.startsWith("0"))
                        {
                            txtCasoEsp = txtCasoEsp + "DSLZ" + "~" + getString(oTxnOP.getMessage(),1) + "~";
                            String monto = delCommaPointFromString((String)datasession.get( "txtMonto" ));
                            String comision = delCommaPointFromString((String)datasession.get( "montoComision" ));
                            String iva = delCommaPointFromString((String)datasession.get( "montoIVA" ));
                            String total = "" + ( Long.parseLong(monto) + Long.parseLong(comision) + Long.parseLong(iva) );
                            dTotal = Long.parseLong(comision) + Long.parseLong(iva);
                            String ordenante = (String)datasession.get( "txtNomOrd" ) + " " + (String)datasession.get( "txtApeOrd" );
                            String beneficiario = (String)datasession.get( "txtNomBen" ) + " " + (String)datasession.get( "txtApeBen" );
                            txtCadImpresion = txtCadImpresion + "~EFECTIVO~N$~" + oTxnOP.getOrden() + "~";
                            txtCadImpresion = txtCadImpresion + monto + "~" + comision + "~" + iva + "~" + total + "~";
                            txtCadImpresion = txtCadImpresion + beneficiario + "~" + ordenante + "~0000000000~NOMCORTO";
                           String thisBank = "";

                            txtCadImpresion = txtCadImpresion + "~" + thisBank + "~";
                       }

                        if (datasession.get("lstdeIVACOM").toString().equals("1"))
                        {
                            txtCadImpresion += "COMPCOM~" + datasession.get("txtNombre") + "~" + datasession.get("txtRFC01").toString() + "~" + "CHEQUES" + "~" + "EXP. ORDEN PAGO" + "~" + datasession.get("txtDomici") + "~";
                            txtCadImpresion += addpunto(datasession.get( "montoComision" ).toString()) + "~" + addpunto(datasession.get( "montoIVA" ).toString()) + "~";
                            txtCadImpresion += addpunto(Long.toString(dTotal)) + "~" + datasession.get("domiSuc") + "~";
                        }
                    }
                }
            }
        }

        else if(TE.equals("4071") || TE.equals("4073") || TE.equals("407A"))
        {
            if(TE.equals("407A"))
            {
                TE = "4071";
                oTxn.setTxnCode(TE);
            }
            //USD ODCT 4071A 00001000130  000130  000**7000001321**100000*US$******000**EX.ORD.PAGO S/BITAL**************************************
            //    ODCT 4071A 00001000143  000143  000**7000001321**100000*US$******000**-CGO MEPA740621EJ1               100.00**************************************
            oTxn.setFormat( "A" );
            oTxn.setAcctNo( (String)datasession.get( "txtDDACuenta" ) );
            oTxn.setTranAmt( delCommaPointFromString((String)datasession.get( "txtMonto" )) );
            oTxn.setTranCur( "N$" );
            String moneda = (String)datasession.get( "moneda" );
            oTxn.setReversable("S");

            if(moneda.equals("02"))
            {    
                oTxn.setTranCur( "US$" );
                oTxn.setReversable("N");
            }

            oTxn.setMoAmoun("000");
            oTxn.setTranDesc("EX.ORD.PAGO S/"+(String)session.getAttribute("identidadApp")+"       ");
            oTxn.setDescRev("REV. EX.ORD.PAGO S/"+(String)session.getAttribute("identidadApp")+"      ");

            if ((String)datasession.get("override") != null)
                if (datasession.get("override").toString().equals("SI"))
                    oTxn.setOverride("3");

            String lstForPago = new String("");
            if(datasession.get( "lstForPago" ) != null)
                lstForPago = (String)datasession.get( "lstForPago" );

            String txtRFC = new String("");
            if(datasession.get( "txtRFC01" ) != null)
                txtRFC = (String)datasession.get( "txtRFC01" );
            String txtIVA = new String("");
            if(datasession.get( "txtImporteIVA" ) != null)
                txtIVA = (String)datasession.get( "txtImporteIVA" );

            if(lstForPago.equals("01"))
                txtRFC = new String("");

            if(txtRFC.length() > 0 && txtIVA.length() > 0)
            {
                txtRFC = dataFiller(txtRFC,15,"LEFT");
                txtIVA = dataFiller(txtIVA,19,"RIGHT");
                String desc = "-CGO " + txtRFC.toUpperCase() + txtIVA;
                oTxn.setTranDesc(desc);
                if(desc.length() > 25)
                    desc = desc.substring(0,25);
                oTxn.setDescRev("REV. " +desc);

                datasession.put("lstCF", "1");
                session.setAttribute("page.datasession", datasession);
            }

            diario = new Diario();
            sMessage = diario.invokeDiario(oTxn, (String)session.getAttribute("d_ConsecLiga"), dirip, moneda, session);

            String Monto = delCommaPointFromString((String)datasession.get("txtMonto"));

            if(sMessage.startsWith("0"))
            {
                txtCasoEsp = txtCasoEsp + TE + "~" + getString(oTxn.getMessage(),1) + "~";
                oTxnOP = new OrdenPago();
                oTxnOP.setOPSucursal((String)datasession.get("sucursal"));
                if(moneda.equals("01"))
                {
                    String sznomapl = new String(getString(oTxn.getMessage(),4));
                    String sznombre, szapellido;
                    int indice = sznomapl.indexOf("*");
                    if(indice > 0)
                    {
                        szapellido = new String(sznomapl.substring(0, indice - 1));
                        sznombre = new String(sznomapl.substring(indice + 1, sznomapl.length()));
                    }
                    else{
                        sznombre = sznomapl.trim();
                        szapellido = "";
                    }
                    oTxnOP.setProcessCode("EXP ");
                    String date = new String(stringFormat(Calendar.YEAR, 0).substring(3) + stringFormat(Calendar.MONTH, 1) + stringFormat(Calendar.DAY_OF_MONTH, 0));
                    oTxnOP.setField32( date, "MXN", setCommaToString( delCommaPointFromString((String)datasession.get( "txtMonto" ) ) ) );
                    oTxnOP.setField14a(szapellido);
                    oTxnOP.setField14n(sznombre);
                    oTxnOP.setField12a((String)datasession.get( "txtApeBen" ));
                    oTxnOP.setField12n((String)datasession.get( "txtNomBen" ));
                    oTxnOP.setField27("EXP");
                    oTxnOP.setField17f(date);
                    oTxnOP.setField17t(time);
                    oTxnOP.setTeller((String)datasession.get("teller"));
                    oTxnOP.setBranch((String)datasession.get("sucursal"));
                    oTxnOP.setFromCurr("N$");

                    // datos para la respuesta del diario
                    oTxnOP.setNomOrd(sznombre);
                    oTxnOP.setApeOrd(szapellido);
                    oTxnOP.setNomBen((String)datasession.get( "txtNomBen" ));
                    oTxnOP.setApeBen((String)datasession.get( "txtApeBen" ));
                    oTxnOP.setTranAmt(delCommaPointFromString((String)datasession.get("txtMonto")));
                    oTxnOP.setBanco((String)session.getAttribute("identidadApp"));
                    oTxnOP.setReversable("S");

                    diario = new Diario();
                    sMessage = diario.invokeDiario(oTxnOP, (String)session.getAttribute("d_ConsecLiga"), dirip, "N$", session);

                    double dTotal = 0.0;
                    String monto = new String("");
                    String comision = new String("");
                    String iva = new String("");
                    String total = new String("");

                    if(sMessage.startsWith("0"))
                    {
                        txtCasoEsp = txtCasoEsp + "DSLZ" + "~" + getString(oTxnOP.getMessage(),1) + "~";
                        monto = delCommaPointFromString((String)datasession.get( "txtMonto" ));
                        comision = new Long(Long.parseLong(getPositionalString(oTxn.getMessage(),40,0, "YES"))).toString();
                        iva = new Long(Long.parseLong(getPositionalString(oTxn.getMessage(),118,0, "YES"))).toString();
                        total = "" + ( Long.parseLong(monto) + Long.parseLong(comision) + Long.parseLong(iva) );
                        dTotal = ( Long.parseLong(monto) + Long.parseLong(comision) + Long.parseLong(iva) ) / 100;
                        String ordenante = getPositionalString(oTxn.getMessage(),91,27, "NO").trim();
                        String beneficiario = (String)datasession.get( "txtNomBen" ) + " " + (String)datasession.get( "txtApeBen" );
                        txtCadImpresion = txtCadImpresion + "~CARGO~N$~" + oTxnOP.getOrden() + "~";
                        txtCadImpresion = txtCadImpresion + monto + "~" + comision + "~" + iva + "~" + total + "~";
                        txtCadImpresion = txtCadImpresion + beneficiario + "~" + ordenante + "~" + datasession.get( "txtDDACuenta" ).toString() + "~" + ordenante;
                        String thisBank = "";

                        txtCadImpresion = txtCadImpresion + "~" + thisBank + "~" ;
                    }

                    if(datasession.get("lstCF").toString().equals("1"))
                    {
                       session.removeAttribute("CFESP");
                       session.setAttribute("CFESP",(String)datasession.get("txtCveRFC") + "~" + (String)datasession.get("txtImporteIVA"));
                    }
                }
                else
                {
                    String numorden = getPositionalString(oTxn.getMessage(),169,10, "NO").trim();
                    String monto = delCommaPointFromString((String)datasession.get( "txtMonto" ));
                    String comision = new Long(Long.parseLong(getPositionalString(oTxn.getMessage(),40,0, "YES"))).toString();
                    String iva = new Long(Long.parseLong(getPositionalString(oTxn.getMessage(),118,0, "YES"))).toString();
                    String total = "" + ( Long.parseLong(monto) + Long.parseLong(comision) + Long.parseLong(iva) );
                    double dTotal = ( Long.parseLong(monto) + Long.parseLong(comision) + Long.parseLong(iva) ) / 100;
                    String ordenante = getPositionalString(oTxn.getMessage(),91,27, "NO").trim();
                    String beneficiario = (String)datasession.get( "txtNomBen" ) + " " + (String)datasession.get( "txtApeBen" );
                    txtCadImpresion = txtCadImpresion + "~CARGO~US$~" + numorden + "~";
                    txtCadImpresion = txtCadImpresion + monto + "~" + comision + "~" + iva + "~" + total + "~";
                    txtCadImpresion = txtCadImpresion + beneficiario + "~" + ordenante + "~" + datasession.get( "txtDDACuenta" ).toString() + "~" + ordenante;
                    String thisBank = "";
                    txtCadImpresion = txtCadImpresion + "~" + thisBank + "~" ;
                    if(datasession.get("ComproFiscal").toString().equals("1"))
                    {
                       session.removeAttribute("CFESP");
                       session.setAttribute("CFESP",(String)datasession.get("txtRFC01") + "~" + (String)datasession.get("txtImporteIVA"));
                    }
                }
               session.setAttribute("page.txnresponse",sMessage.substring(0,1) + "~01~010101~Transaccion Orden de Pago~");                
            }else
            if(sMessage.startsWith("1"))
            {
                txtCasoEsp = txtCasoEsp + TE + "~" + getString(oTxn.getMessage(),1) + "~";
            }else
            if (sMessage.startsWith("3") || sMessage.startsWith("4"))
               session.setAttribute("page.txnresponse",sMessage);                
            else
               session.setAttribute("page.txnresponse",sMessage.substring(0,1) + "~01~010101~Error Orden de Pago~");
        }

        if(txtCasoEsp.length() > 0)
        {
            session.setAttribute("txtCasoEsp", txtCasoEsp);
        }
        
        //session.setAttribute("txtCadImpresion", sMessage);
        
        if(txtCadImpresion.length() > 0)
        {
            session.setAttribute("txtCadImpresion", txtCadImpresion);
        }

        if(TE.equals("0814"))
        {
            session.setAttribute("page.txnresponse",sMessage);
        }

        if(noTax == 1)
            session.setAttribute("page.txnresponse", noTaxS);
        getServletContext().getRequestDispatcher("/servlet/ventanilla.Redirector").forward(req,resp);
    }

    public void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doPost(req, resp);
    }

   
    	private String addpunto(String valor)
	{
	int longini = valor.length();
        String cents  = valor.substring(longini-2,longini);
	String entero = valor.substring(0,longini-2);
	int longente = entero.length();
	for (int i = 0; i < (longente-(1+i))/3; i++)
	{
		longente = entero.length();
	    entero = entero.substring(0,longente-(4*i+3))+','+entero.substring(longente-(4*i+3));
	}
  	entero = entero + '.' + cents;
	return entero;
	}

}
