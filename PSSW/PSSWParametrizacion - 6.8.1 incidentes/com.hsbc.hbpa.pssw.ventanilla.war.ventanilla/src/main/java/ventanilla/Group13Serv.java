//*************************************************************************************************
//             Funcion: Clase que realiza txn Servicios
//            Elemento: Group13Serv.java
//          Creado por: Alejandro Gonzalez Castro
//      Modificado por: Alejandro Gonzalez Castro
//*************************************************************************************************
// CCN - 4360189 - 03/09/2004 - Se cambia invoke al Diario para Control de Efectivo
//*************************************************************************************************
package ventanilla;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.util.Hashtable;

import ventanilla.com.bital.sfb.ServiciosB;
import ventanilla.com.bital.admin.Diario;

public class Group13Serv extends HttpServlet
{
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
         HttpSession session = request.getSession(false);
         Hashtable datasession = (Hashtable)session.getAttribute("page.datasession");
         String txn = (String)datasession.get("cTxn");
		GenericClasses gc = new GenericClasses();
         String moneda = gc.getDivisa((String)datasession.get("moneda"));
         String[] invoqued = request.getParameterValues("txtInvoqued");
         String invTxn = "";
         if(invoqued != null)
             invTxn = (String)invoqued[0];

         String dirip =  (String)session.getAttribute("dirip");
         String msg = "";
         
         ServiciosB serviciob = new ServiciosB();
         serviciob.setBranch((String)datasession.get("sucursal"));
         serviciob.setTeller((String)datasession.get("teller"));
         serviciob.setSupervisor((String)datasession.get("teller"));
         serviciob.setFormat("B");
         String sucursal = (String)datasession.get("sucursal");
         String Monto = "";
         serviciob.setTxnCode(txn);
         serviciob.setAcctNo(txn);
         Monto = quitap((String)datasession.get("txtMontoNoCero"));
         
       
         int camlon = Monto.length();
         String entero = Monto.substring(0, camlon - 2);
         String centavo = Monto.substring(camlon - 2, camlon);

         serviciob.setTranAmt(entero);
         serviciob.setCashOut(centavo);
         serviciob.setReferenc((String)datasession.get("txtNumFol"));
         serviciob.setCashIn("000");
         serviciob.setFromCurr(moneda);
         
         Diario diario = new Diario();
         String sMessage = diario.invokeDiario(serviciob, (String)session.getAttribute("d_ConsecLiga"), dirip, moneda, session);
       
         session.setAttribute("page.txnresponse", sMessage);
         getServletContext().getRequestDispatcher("/servlet/ventanilla.Redirector").forward(request,response);
    }
    
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
    
    private String quitap(String Valor) {
        StringBuffer Cantidad = new StringBuffer(Valor);
        for(int i=0; i<Cantidad.length();) {
            if( Cantidad.charAt(i) == ',' || Cantidad.charAt(i) == '.' )
                Cantidad.deleteCharAt(i);
            else
                ++i;
        }
        return Cantidad.toString();
    }
    
    public String getmoneda(String cmoneda) {
        String newmoneda = "";
        if (cmoneda.equals("N$"))
        { newmoneda = "01";}
        else if (cmoneda.equals("US$"))
        { newmoneda = "02";}
        else
        { newmoneda = "03";}
        return newmoneda;
    }

    
    public String removeAsterisk(String campo) {
        StringBuffer result = new StringBuffer(campo);
        String caracter = result.substring(result.length()-1,result.length());
        if (caracter.equals("*"))
            result.deleteCharAt(result.length()-1);
        return result.toString();
    }
    
}
