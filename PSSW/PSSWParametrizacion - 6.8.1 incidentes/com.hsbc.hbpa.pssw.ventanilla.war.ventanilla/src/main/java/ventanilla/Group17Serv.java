//*************************************************************************************************
//             Funcion: Clase que realiza txn Servicios 0818
//            Elemento: Group17Serv.java
//          Creado por: Alejandro Gonzalez Castro
//		Modificado por: Juvenal Fernandez
//*************************************************************************************************
// CCN - 4360189 - 03/09/2004 - Se cambia invoke al Diario para Control de Efectivo
// CCN - 4360268 - 04/02/2005 - Se realizan adecuaciones para eliminar Agencia NY
//*************************************************************************************************
package ventanilla;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.Hashtable;
import ventanilla.GenericClasses;
import java.util.Stack;

import ventanilla.com.bital.sfb.ServiciosA;
import ventanilla.com.bital.admin.Diario;

public class Group17Serv extends HttpServlet
{
  public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
  {
    response.setContentType("text/plain");
    HttpSession session = request.getSession(false);
    Hashtable datasession = (Hashtable)session.getAttribute("page.datasession");
    GenericClasses cGenericas = new GenericClasses();

    String sMessage = "";
    String txtMonto = "000";
    String txtDescrip = "";
    String txtEstado = "5";                                             // Estado de Reactivacion
    String dirip =  (String)session.getAttribute("dirip");
    ServiciosA serviciosa = new ServiciosA();

    serviciosa.setBranch((String)datasession.get("sucursal"));
    serviciosa.setTeller((String)datasession.get("teller"));
    serviciosa.setSupervisor((String)datasession.get("teller"));

    if ( (String)datasession.get("override") != null )
        if (datasession.get("override").toString().equals("SI"))
            serviciosa.setOverride("3");
    if ( (String)datasession.get("supervisor") != null )
        serviciosa.setSupervisor((String)datasession.get("supervisor"));
    
    String txn = (String)datasession.get("oTxn");
    serviciosa.setTxnCode(txn);

    String txtDDACuenta =  (String)datasession.get("txtDDACuenta");    
    String txtSerial = (String)datasession.get("txtSerial");
    String moneda = (String)datasession.get("moneda");

    if ( txn.equals("818A") ) {                                     // Alta Cheque
        txtEstado = "1";
        txtMonto = (String)datasession.get("txtMonto");
        txtDescrip = (String)datasession.get("txtBeneficiario");
        txtMonto = cGenericas.delCommaPointFromString((String)datasession.get("txtMonto"));
    }   
    
    if ( txn.equals("818C") )                                      // Cancelación Cheque
        txtEstado = "3";
    
    if ( txn.equals("818S") )                                      // Suspension Cheque
        txtEstado = "4";
   
    serviciosa.setTxnCode("0818");
    serviciosa.setAcctNo(txtDDACuenta);
    serviciosa.setTranAmt(txtMonto);
    serviciosa.setCheckNo(txtSerial);
    serviciosa.setTranDesc(txtDescrip);
    serviciosa.setTrNo(txtEstado);
    serviciosa.setDescRev("EMPTY");
	serviciosa.setTranCur(cGenericas.getDivisa(moneda));
   
    Diario diario = new Diario();
    sMessage = diario.invokeDiario(serviciosa, (String)session.getAttribute("d_ConsecLiga"), dirip, moneda, session);
    
    if ( sMessage.startsWith("0")) {
        datasession.put("txtNoTxn",new Integer(1).toString());
        session.setAttribute("page.datasession", (Hashtable)datasession);
        Stack flujotxn = (Stack)session.getAttribute("page.flujotxn");
        flujotxn.push(txn);
        session.setAttribute("page.flujotxn", (Stack)flujotxn); 
    }
    
    String txtCasoEsp = "0818~"+ cGenericas.getString(sMessage,1) + "~";
    session.setAttribute("txtCasoEsp", txtCasoEsp); 
    
    session.setAttribute("page.txnresponse", sMessage);
    getServletContext().getRequestDispatcher("/servlet/ventanilla.Redirector").forward(request,response);
  }

  public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
  {
    doPost(request, response);
  }
}
