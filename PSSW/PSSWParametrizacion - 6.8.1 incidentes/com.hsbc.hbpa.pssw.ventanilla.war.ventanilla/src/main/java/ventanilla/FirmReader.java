package ventanilla;

import java.io.IOException;
import java.io.OutputStream;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.bital.util.CicsGateway;

public class FirmReader extends HttpServlet
{
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{		
		byte[] buffer   = new byte[12284];	
		String strParametro = request.getQueryString();		
		try
		{
			String szCadEnvio = "FIRM " + strParametro;
			CicsGateway cg = new CicsGateway();
			cg.write(szCadEnvio);
			buffer = cg.read();
		}
		catch(Exception e)
		{
			System.out.println("Error FirmReader::doGet::Exception-"+e+"-");
		}
		response.setContentType("application/octet-stream");
		OutputStream os = response.getOutputStream();
		os.write(buffer, 0, buffer.length);
		os.close();
	}
  
	public void doPost(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException
	{
		doGet(request, response);
	}
}
