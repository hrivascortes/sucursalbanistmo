//*************************************************************************************************
//             Funcion: Clase que realiza txns 0815 - 0572 - 0578
//            Elemento: GCBGroup01.java
//          Creado por: Alejandro Gonzalez Castro
//      Modificado por: Fausto R. Flores Moreno
//*************************************************************************************************
// CCN - 4360189 - 03/09/2004 - Se cambia invoke al Diario para Control de Efectivo
// CCN - 4360253 - 07/01/2005 - En el comprobante el nombre del ordenante se muestra completo
// CCN - 4360368 - 02/09/2005 - Se elimina sleep
// CCN - 4360374 - 08/09/2005 - Se genera nuevo paquete a peticion de QA.
// CCN - 4360456 - 07/04/2006 - Se corrige ciclo en autorización remota para ctas libres de comision.
//*************************************************************************************************
package ventanilla;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.util.Calendar;
import java.util.Hashtable;

import ventanilla.com.bital.sfb.CompraVenta;
import ventanilla.com.bital.admin.Diario;

public class GCBGroup01 extends HttpServlet {
    private String stringFormat(int option) {
        Calendar now = Calendar.getInstance();
        String temp = new Integer(now.get(option)).toString();
        if( temp.length() != 2 )
            temp = "0" + temp;

        return temp;
    }

    private String delCommaPointFromString(String newCadNum) {
        String nCad = new String(newCadNum);
        if( nCad.indexOf(".") > -1) {
            nCad = nCad.substring(nCad.indexOf(".") + 1, nCad.length());
            if(nCad.length() != 2) {
                String szTemp = new String("");
                for(int j = nCad.length(); j < 2; j++)
                    szTemp = szTemp + "0";
                newCadNum = newCadNum + szTemp;
            }
        }

        StringBuffer nCadNum = new StringBuffer(newCadNum);
        for(int i = 0; i < nCadNum.length(); i++)
            if(nCadNum.charAt(i) == ',' || nCadNum.charAt(i) == '.')
                nCadNum.deleteCharAt(i);

        return nCadNum.toString();
    }

    private String getTipoCambio(String newTipo, int newZeroes) {
        int newCadLen = newTipo.length();
        for(int i = newCadLen; i < newCadLen + newZeroes; i++)
            newTipo = "0" + newTipo;

        return newTipo;
    }

    private String getString(String newCadNum, int NumSaltos) {
        int iPIndex = newCadNum.indexOf("CONSECUTIVO:") + 12, Num = 0, Ban = 0;
        String nCadNum = new String("");
/*   NumSaltos = 4;
   if(newOption == 0)
    NumSaltos = 3;*/
        while(Num < NumSaltos) {
            if(newCadNum.charAt(iPIndex) == 0x20) {
                while(newCadNum.charAt(iPIndex) == 0x20)
                    iPIndex++;
                Num++;
            }
            else
                while(newCadNum.charAt(iPIndex) != 0x20)
                    iPIndex++;
        }
        while(newCadNum.charAt(iPIndex) != 0x20) {
            nCadNum = nCadNum + newCadNum.charAt(iPIndex++);
            if(newCadNum.charAt(iPIndex) == 0x20 && newCadNum.charAt(iPIndex + 1) == 0x2A) {
                nCadNum = nCadNum + newCadNum.charAt(iPIndex) + newCadNum.charAt(iPIndex + 1);
                iPIndex += 2;
            }
        }

        return nCadNum;
    }

    private String getPositionalString(String inputString, int initialPosition, int maxNumberOfBytes, String withSpaces){
        String outputString = new String("");
        int i = 0;
        if(maxNumberOfBytes == 0)
            maxNumberOfBytes = inputString.length();
        if(withSpaces.equals("YES"))
            while(initialPosition < inputString.length() && inputString.charAt(initialPosition) != 0x20 && i++ < maxNumberOfBytes)
                outputString += inputString.charAt(initialPosition++);
        else
            while(initialPosition < inputString.length() && i++ < maxNumberOfBytes)
                outputString += inputString.charAt(initialPosition++);
        if(outputString.length() == 0)
            outputString = new String("000");
        return outputString;
    }

    private String setPointToString(String newCadNum) {
        int iPIndex = newCadNum.indexOf("."), iLong;
        String szTemp;

        if(iPIndex > 0) {
            newCadNum = new String(newCadNum + "00");
            newCadNum = newCadNum.substring(0, iPIndex + 1) + newCadNum.substring(iPIndex + 1, iPIndex + 3);
        }
        else {
            for(int i = newCadNum.length(); i < 3; i++)
                newCadNum = "0" + newCadNum;
            iLong = newCadNum.length();
            if(iLong == 3)
                szTemp = newCadNum.substring(0, 1);
            else
                szTemp = newCadNum.substring(0, iLong - 2);
            newCadNum = szTemp + "." + newCadNum.substring(iLong - 2);
        }

        return newCadNum;
    }


    private String setFormat(String newCadNum) {
        int iPIndex = newCadNum.indexOf(".");
        String nCadNum = new String(newCadNum.substring(0, iPIndex));
        for (int i = 0; i < (int)((nCadNum.length()-(1+i))/3); i++)
            nCadNum = nCadNum.substring(0, nCadNum.length()-(4*i+3))+','+nCadNum.substring(nCadNum.length()-(4*i+3));

        return nCadNum + newCadNum.substring(iPIndex, newCadNum.length());
    }

    public void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {
        String time = stringFormat(Calendar.HOUR_OF_DAY) + stringFormat(Calendar.MINUTE) + stringFormat(Calendar.SECOND);
        String TE, TO, Currency, Currency2, rspDiario;
        String lstForPago = new String("");

        CompraVenta oTxn = new CompraVenta();

        String txtCasoEsp = new String("");
        String txtCadImpresion = new String("");
        HttpSession session = req.getSession(false);

        Hashtable datasession = (Hashtable)session.getAttribute("page.datasession");
        TE = (String)datasession.get("cTxn");
        TO = (String)datasession.get("oTxn");
        Currency = (String)datasession.get("lstMoneda");
        Currency2 = (String)datasession.get("moneda");
        String dirip =  (String)session.getAttribute("dirip");

        oTxn.setTxnCode(TE);
        oTxn.setBranch((String)datasession.get("sucursal"));
        oTxn.setTeller((String)datasession.get("teller"));
        oTxn.setSupervisor((String)datasession.get("teller"));

        
 //************** Elimina el ciclo en Autorización Remota       
        if ( (String)datasession.get("override") != null )
          if (datasession.get("override").toString().equals("SI"))
                oTxn.setOverride("3");

        if ( (String)datasession.get("supervisor") != null )
             oTxn.setSupervisor((String)datasession.get("supervisor"));
//*************** Fin
        oTxn.setTeller((String)datasession.get("teller"));
        Diario diario = new Diario();
        String sMessage="";
		GenericClasses gc = new GenericClasses();
        
        if( TE.equals( "0815" ) )
        {
            lstForPago = (String)datasession.get("lstForPago");
            if(lstForPago.equals("01"))
            {
                oTxn.setFormat( "A" );
                oTxn.setAcctNo( (String)datasession.get( "txtDDACuenta" ) );
                oTxn.setTranAmt( (String)session.getAttribute("plaza") + "00" );
                oTxn.setCashIn( "000" );
                oTxn.setMoAmoun( "000" );
                oTxn.setTranDesc( "CONSULTA COMISION,IVA Y CUENTA P/GIRO" );
				oTxn.setTranCur(gc.getDivisa(Currency));

                sMessage = diario.invokeDiario(oTxn, (String)session.getAttribute("d_ConsecLiga"), dirip, Currency2, session);
                session.setAttribute("page.txnresponse", sMessage);
            }
            else
                sMessage = "0~03~0010101~CONSECUTIVO:  010101    0    00000000000 00~";
        }
        else if( TE.equals( "0572" ) )
        {
            lstForPago = (String)datasession.get("lstForPago");
            if(lstForPago.equals("01"))
            {
                // Pago Efectivo
                oTxn.setFormat( "G" );
                oTxn.setService("WCH");
                oTxn.setToCurr("US$");
                oTxn.setCheckNo((String)datasession.get("txtSerial5"));
                oTxn.setGStatus("1");
                oTxn.setBcoTrnsf("021000089");
                oTxn.setTranAmt(delCommaPointFromString((String)datasession.get("txtMonto")));
                oTxn.setComAmt(delCommaPointFromString((String)datasession.get("montoComision")));
                oTxn.setIVAAmt(delCommaPointFromString((String)datasession.get("montoIVA")));
                oTxn.setAcctNo((String)datasession.get("txtDDACuenta"));
                oTxn.setCtaBenef("000000000000000");
                oTxn.setBenefic((String)datasession.get("txtBeneficiario"));
                oTxn.setOrdenan((String)datasession.get("txtNomOrd"));
            }
            else
            {
                // Cargo a Cuenta
                TE = "4089";
                oTxn.setTxnCode("4089");
                oTxn.setFormat( "G" );
                oTxn.setService("WCH");
                oTxn.setToCurr("US$");
                oTxn.setCheckNo((String)datasession.get("txtSerial5"));
                oTxn.setGStatus("1");
                oTxn.setBcoTrnsf("021000089");
                oTxn.setTranAmt(delCommaPointFromString((String)datasession.get("txtMonto")));
                oTxn.setAcctNo((String)datasession.get("txtDDACuenta"));
                oTxn.setCtaBenef("000000000000000");
                oTxn.setBenefic((String)datasession.get("txtBeneficiario"));
                oTxn.setInstruc("EXPED. GIRO DOLARES");

                // RFC A 19 POSICIONES JUSTIFICADO A LA IZQ
                // IVA 14 POSICIONES INCLUYENDO COMAS Y PUNTO DECIMAL JUSTIFICADO A LA DER
                String CF = (String)datasession.get("lstCF");
                if (CF.equals("1")) {
                    session.removeAttribute("CFESP");
                    session.setAttribute("CFESP",(String)datasession.get("txtRFC") + "~" + (String)datasession.get("txtIVA"));
                    String RFC = (String)datasession.get("txtRFC");
                    int longitud = RFC.length();
                    for (int i=longitud; i<19; i++) {
                        RFC = RFC + " ";
                    }
                    String IVA = (String)datasession.get("txtIVA");
                    longitud = IVA.length();
                    for (int i=longitud; i<14; i++) {
                        IVA = " " + IVA;
                    }
                    String Desc = "-CGO "+ RFC + " " + IVA;
                    oTxn.setInstruc(Desc);
                    String DescRev = "REV. " + Desc;
                    if(DescRev.length() > 25)
                        DescRev = DescRev.substring(0,25);
                    oTxn.setDescRev(DescRev);
                }
                else
                {
                    oTxn.setDescRev("REV. EXPED. GIRO DOLARES");
                }
            }

            sMessage = diario.invokeDiario(oTxn, (String)session.getAttribute("d_ConsecLiga"), dirip, Currency2, session);
            session.setAttribute("page.txnresponse", sMessage);

            txtCasoEsp = txtCasoEsp + TE + "~" + getString(sMessage,1) + "~";

            if(sMessage.substring(0,1).equals("0") && TE.equals("4089"))
            {

                String Monto = delCommaPointFromString((String)datasession.get("txtMonto"));
                String txtSerial = (String)datasession.get("txtSerial5");
                String txtCuenta = (String)datasession.get("txtDDACuenta");
                String txtMonto = delCommaPointFromString((String)datasession.get("txtMonto"));
                String txtComision = delCommaPointFromString(getPositionalString(oTxn.getMessage(),54,13,"NO"));
                String txtIVA = delCommaPointFromString(getPositionalString(oTxn.getMessage(),132,13,"NO"));
                String txtTotal = new Long(Long.parseLong(txtMonto) + Long.parseLong(txtComision) + Long.parseLong(txtIVA)).toString();
                
                String txtOrdenante = getPositionalString(oTxn.getMessage(),91,27,"NO");
                if (txtOrdenante.length() > 0)
                	txtOrdenante = txtOrdenante.replace('*',' ');
                
                String txtBeneficiario = (String)datasession.get("txtBeneficiario");
                Monto = delCommaPointFromString((String)datasession.get("txtMonto"));

                txtCadImpresion = "~" + "GIROCITI1~EXP" + "~" + txtSerial + "~" + txtMonto +
                "~" + Long.parseLong(txtComision) + "~" + Long.parseLong(txtIVA) + "~" + txtTotal +
                "~" + txtOrdenante + "~" + txtBeneficiario + "~" + txtCuenta;
            }
        }
        else if( TE.equals( "0578" ) )
        {
            oTxn.setFormat( "B" );
            oTxn.setAcctNo( TE );
            oTxn.setTranAmt(delCommaPointFromString((String)datasession.get( "montoComision" )));
            oTxn.setReferenc("COMIS. GIRO DLLS");
            oTxn.setCashIn(delCommaPointFromString((String)datasession.get( "montoComision" )));
            oTxn.setFromCurr("US$");

            sMessage = diario.invokeDiario(oTxn, (String)session.getAttribute("d_ConsecLiga"), dirip, Currency2, session);
            session.setAttribute("page.txnresponse", sMessage);

            if(sMessage.substring(0,1).equals("0"))
            {

                txtCasoEsp = txtCasoEsp + TE + "~" + getString(oTxn.getMessage(),1) + "~";
                String Monto = delCommaPointFromString((String)datasession.get("montoComision"));
                oTxn.setTxnCode("0372");
                oTxn.setFormat( "B" );
                oTxn.setAcctNo("0372");
                oTxn.setTranAmt(delCommaPointFromString((String)datasession.get( "montoIVA" )));
                oTxn.setReferenc("IVA GIRO DOLARES");
                oTxn.setCashIn(delCommaPointFromString((String)datasession.get( "montoIVA" )));
                oTxn.setFromCurr("US$");

                sMessage = diario.invokeDiario(oTxn, (String)session.getAttribute("d_ConsecLiga"), dirip, Currency2, session);
                session.setAttribute("page.txnresponse", sMessage);

                if(sMessage.substring(0,1).equals("0"))
                {
                    txtCasoEsp = txtCasoEsp + "0372" + "~" + getString(oTxn.getMessage(),1) + "~";
                    String txtSerial = (String)datasession.get("txtSerial5");
                    String txtMonto = delCommaPointFromString((String)datasession.get("txtMonto"));
                    String txtComision = delCommaPointFromString((String)datasession.get( "montoComision" ));
                    String txtIVA = delCommaPointFromString((String)datasession.get( "montoIVA" ));
                    String txtTotal = new Long(Long.parseLong(txtMonto) + Long.parseLong(txtComision) + Long.parseLong(txtIVA)).toString();
                    String txtOrdenante = (String)datasession.get("txtNomOrd");
                    String txtBeneficiario = (String)datasession.get("txtBeneficiario");
                    Monto = delCommaPointFromString((String)datasession.get("montoIVA"));

                    datasession.put("txtComision",setFormat(setPointToString(txtComision)));
                    datasession.put("txtIVA",setFormat(setPointToString(txtIVA)));

                    txtCadImpresion = "~" + "GIROCITI1~EXP" + "~" + txtSerial + "~" + txtMonto +
                    "~" + txtComision + "~" + txtIVA + "~" + txtTotal +
                    "~" + txtOrdenante + "~" + txtBeneficiario + "~" + "0000000000~";
                }
            }
        }

        if(txtCasoEsp.length() > 0)
        {
            session.setAttribute("txtCasoEsp", txtCasoEsp);
        }

        //session.setAttribute("txtCadImpresion", oTxn.getMessage());

        if(txtCadImpresion.length() > 0)
        {
            session.setAttribute("txtCadImpresion", txtCadImpresion);
        }

        /*if(lstForPago != null)
        {
            if(lstForPago.equals("02") && TE.equals("0815"))
                session.setAttribute("page.txnresponse", "0~03~0010101~CONSECUTIVO:  010101    0    00000000000 00~");
            else
            {
                session.setAttribute("page.txnresponse", sMessage);
            }
        }
        else
        {
            session.setAttribute("page.txnresponse", sMessage);
        }*/
        session.setAttribute("page.txnresponse", sMessage);
        getServletContext().getRequestDispatcher("/servlet/ventanilla.Redirector").forward(req,resp);
    }

    public void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doPost(req, resp);
    }
}
