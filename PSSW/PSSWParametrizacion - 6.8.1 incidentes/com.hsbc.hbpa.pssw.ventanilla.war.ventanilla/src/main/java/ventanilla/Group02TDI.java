package ventanilla;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import ventanilla.com.bital.admin.PostingTDI;

public class Group02TDI extends HttpServlet {
	public void doPost(HttpServletRequest request, HttpServletResponse response)
		throws ServletException, IOException {
    HttpSession session = request.getSession(false);
    PostingTDI posting = new PostingTDI();
    posting.execute(session);
    session.setAttribute("page.getoTxnView","ResponseTDI");
    session.setAttribute("page.txnresponse", "0~02~0151515~Proceso TDI Terminado, verifique certificacion.~");
    getServletContext().getRequestDispatcher("/servlet/ventanilla.Redirector").forward(request,response);
    }
	public void doGet(HttpServletRequest request, HttpServletResponse response)
		throws ServletException, IOException {
       doPost(request, response);
	}
}
