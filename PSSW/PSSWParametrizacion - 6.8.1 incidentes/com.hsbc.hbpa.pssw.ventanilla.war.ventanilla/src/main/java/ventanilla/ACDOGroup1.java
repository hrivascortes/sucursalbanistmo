//*************************************************************************************************
//             Funcion: Clase que realiza txn 0825 ACDO
//            Elemento: ACDOGroup1.java
//          Creado por: Alejandro Gonzalez Castro
//      Modificado por: Alejandro Gonzalez Castro
//*************************************************************************************************
// CCN - 4360189 - 03/09/2004 - Se cambia invoke al Diario para Control de Efectivo
// CCN - 4360237 - 05/11/2004 - Se establece credencial de IFE como primera opcion de ACDO
// CCN - 4360248 - 19/11/2004 - Se reversa cambio de credencial de IFE
//*************************************************************************************************

package ventanilla;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.util.Hashtable;

import ventanilla.com.bital.sfb.Acdo;
import ventanilla.com.bital.admin.Diario;

public class ACDOGroup1 extends HttpServlet
{
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        HttpSession session = request.getSession(false);
        String dirip =  (String)session.getAttribute("dirip");

        Acdo    oAcdo    = new Acdo();
        Hashtable datasession = (Hashtable)session.getAttribute("page.datasession");
        String txtTxnCode    = (String)datasession.get("cTxn");
        String txtFormat     = "B";
        String txtBranch     = (String)datasession.get("sucursal");
        String txtTeller     = (String)datasession.get("teller");
        String txtSupervisor = (String)datasession.get("teller");
        String txtBackOut    = "0";
        String txtOverride   = "0";
        String txtCurrentDay = "0";
        String txtMoneda = (String)datasession.get("moneda");

        oAcdo.setTxnCode(txtTxnCode);
        oAcdo.setFormat(txtFormat);
        oAcdo.setBranch(txtBranch);
        oAcdo.setTeller(txtTeller);
        oAcdo.setSupervisor(txtSupervisor);
        oAcdo.setBackOut(txtBackOut);
        oAcdo.setOverride(txtOverride);
        oAcdo.setCurrentDay(txtCurrentDay);

        String lstTipoDocto  = ((String)datasession.get("lstTipoDocto"));
        String txtReferencia = (String)datasession.get("txtFolio");
        
        oAcdo.setAcctNo((lstTipoDocto + txtReferencia));

        Diario diario = new Diario();
        String sMessage = diario.invokeDiario(oAcdo, (String)session.getAttribute("d_ConsecLiga"), dirip, txtMoneda, session);

        session.setAttribute("page.txnresponse", sMessage);
        getServletContext().getRequestDispatcher("/servlet/ventanilla.Redirector").forward(request,response);
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}
