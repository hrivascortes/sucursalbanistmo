//*************************************************************************************************
//             Funcion: Clase que realiza txn Cheques Certificados
//            Elemento: Group01ChqCertif.java
//          Creado por: Alejandro Gonzalez Castro
//      Modificado por: Alejandro Gonzalez Castro
//*************************************************************************************************
// CCN - 4360189 - 03/09/2004 - Se cambia invoke al Diario para Control de Efectivo
//*************************************************************************************************
package ventanilla;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.util.Hashtable;

import ventanilla.com.bital.sfb.ChqCertif;
import ventanilla.com.bital.admin.Diario;

public class Group01ChqCertif extends HttpServlet {
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        HttpSession session = request.getSession(false);
        Hashtable datasession = (Hashtable)session.getAttribute("page.datasession");
		GenericClasses gc = new GenericClasses();

        String compara = (String)datasession.get("cTxn");
        String dirip =  (String)session.getAttribute("dirip");
        //Valida comprobante Fiscal para impresion.
        String ComproFiscal = (String)datasession.get("ComproFiscal");
        if (ComproFiscal != null)
        {
          session.removeAttribute("CFESP");
          if (ComproFiscal.equals("1"))
              session.setAttribute("CFESP",(String)datasession.get("txtCveRFC") + "~" + (String)datasession.get("txtImporteIVA"));
        }
        ChqCertif chqCert = new ChqCertif();
        String txnTxn = (String)datasession.get("cTxn");
        String txtTeller = (String)datasession.get("teller");
        chqCert.setTxnCode(txnTxn);
        chqCert.setBranch((String)datasession.get("sucursal"));
        chqCert.setTeller(txtTeller);
        chqCert.setSupervisor(txtTeller);

        if ( (String)datasession.get("override") != null )
            if (datasession.get("override").toString().equals("SI"))
                chqCert.setOverride("3");
        if ( (String)datasession.get("supervisor") != null )
            chqCert.setSupervisor((String)datasession.get("supervisor"));

        StringBuffer efectivo = new StringBuffer((String)datasession.get("txtMonto"));
        for(int j=0; j<efectivo.length();) {
            if(efectivo.charAt(j) == ',' || efectivo.charAt(j) == '.')
                efectivo.deleteCharAt(j);
            else
                ++j;
        }

        String moneda = (String)datasession.get("moneda");
		moneda=gc.getDivisa(moneda);
        String tmpTrNo1 = (String)datasession.get("txtCveTran");
        //String tmpTrNo2 = (String)datasession.get("txtCodSeg");
		String tmpTrNo2 = "0000";

		
        if(!tmpTrNo1.equals("1481")){
        	tmpTrNo1 = tmpTrNo1.substring(2,2+3);
            tmpTrNo2 = tmpTrNo2.substring(0,3);
        }
            

        

        int longSerial = datasession.get("txtSerial2").toString().length();
        String Serial = (String)datasession.get("txtSerial2");
        for(int i=longSerial; i<10; i++) {
            Serial = "0" + Serial;
        }


        chqCert.setAcctNo(datasession.get("txtDDACuenta2").toString().substring(1));
        chqCert.setTranAmt(efectivo.toString());
        chqCert.setTranCur(moneda);
        chqCert.setCheckNo(Serial);
        chqCert.setMoamoun("000");
        chqCert.setFees("8");//pendiente *************************************************
        chqCert.setTranDesc((String)datasession.get("txtBeneficiario"));
        chqCert.setTrNo1(tmpTrNo1);
        chqCert.setTrNo2(tmpTrNo2);

        Diario diario = new Diario();
        String sMessage = diario.invokeDiario(chqCert, (String)session.getAttribute("d_ConsecLiga"), dirip, moneda, session);
        //System.out.println("sMessage:"+sMessage);
        
        String result = sMessage;
        
        if (result.charAt(0) == '0'){
        
	        String montoCom = gc.delLeadingfZeroes(sMessage.substring(54,65));
	        //System.out.println("montoCom: "+montoCom);
			String montoITBMS = gc.delLeadingfZeroes(sMessage.substring(210,233));
			//System.out.println("montoITBMS: "+montoITBMS);
			long totalMonto = Long.parseLong(efectivo.toString()) + Long.parseLong(montoCom) + Long.parseLong(montoITBMS);
			//String strTotalMonto=delCommaPointFromString(String.valueOf(totalMonto));
			String serial = Serial;
			String txtCadImpresion = "~ITBMS~4545~"+efectivo.toString()+"~"+montoCom+"~"+montoITBMS+"~"+totalMonto+"~"+serial+"~"+datasession.get("txtDDACuenta2").toString()+"~"+(String)datasession.get("txtBeneficiario")+"~";
			//System.out.println("txtCadImpresion: "+txtCadImpresion);
			
			if(txtCadImpresion.length() > 0){
				session.setAttribute("txtCadImpresion", txtCadImpresion);
			}
			datasession.put("txtComision",gc.formatMonto(montoCom));
			datasession.put("txtITBMS",gc.formatMonto(montoITBMS));
			datasession.put("txtMontoT",gc.formatMonto(String.valueOf(totalMonto)));
			session.setAttribute("txtCasoEsp", "");
        
        }
        
        	
        session.setAttribute("page.txnresponse", sMessage);
        getServletContext().getRequestDispatcher("/servlet/ventanilla.Redirector").forward(request,response);
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}

