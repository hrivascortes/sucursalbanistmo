//*************************************************************************************************
//             Funcion: Clase que realiza txn Servicios
//            Elemento: Group08Serv.java
//          Creado por: Alejandro Gonzalez Castro
//      Modificado por: Alejandro Gonzalez Castro
//*************************************************************************************************
// CCN - 4360189 - 03/09/2004 - Se cambia invoke al Diario para Control de Efectivo
//*************************************************************************************************
package ventanilla;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.util.Hashtable;

import ventanilla.com.bital.sfb.ServiciosD;
import ventanilla.com.bital.admin.Diario;

public class Group08Serv extends HttpServlet
{
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
        HttpSession session = request.getSession(false);
        Hashtable datasession = (Hashtable)session.getAttribute("page.datasession");
        String txn = (String)datasession.get("cTxn");
		GenericClasses gc = new GenericClasses();
        String moneda = gc.getDivisa((String)datasession.get("moneda"));
        String[] invoqued = request.getParameterValues("txtInvoqued");
        String invTxn = "";
        if(invoqued != null)
            invTxn = (String)invoqued[0];
        
        String dirip =  (String)session.getAttribute("dirip");
        String msg = "";
        String password = (String)session.getAttribute("password");
        String pssContrase = (String)datasession.get("pssContrase");
        if (!pssContrase.equals(password)) {
            msg ="1~01~0000000~CONSECUTIVO: 0000000             El password es Invalido...  ~";
            session.setAttribute("page.txnresponse", msg);
            getServletContext().getRequestDispatcher("/servlet/ventanilla.Redirector").forward(request,response);
            return;
        }
        
        ServiciosD serviciod = new ServiciosD();
        serviciod.setBranch((String)datasession.get("sucursal"));
        serviciod.setTeller((String)datasession.get("teller"));
        serviciod.setSupervisor((String)datasession.get("teller"));
        serviciod.setFormat("D");
        String Monto = "";
        serviciod.setTxnCode(txn);
        serviciod.setCurrCod1(moneda);
        
        if( txn.equals("0201")) {
            Monto = quitap((String)datasession.get("txtMontoNoCero"));                     
            serviciod.setCashIn(Monto);
            serviciod.setTotalsld((String)datasession.get("registro"));            
        }
        
        if( txn.equals("0202")) {
            Monto = quitap((String)datasession.get("txtMontoNoCero"));
            serviciod.setCashOut(Monto);
            serviciod.setTotalsld((String)datasession.get("registro"));
        }
        Diario diario = new Diario();
        String sMessage = diario.invokeDiario(serviciod, (String)session.getAttribute("d_ConsecLiga"), dirip, moneda, session);
        
        session.setAttribute("page.txnresponse", sMessage);
        getServletContext().getRequestDispatcher("/servlet/ventanilla.Redirector").forward(request,response);
    }
    
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
    
    private String quitap(String Valor) {
        StringBuffer Cantidad = new StringBuffer(Valor);
        for(int i=0; i<Cantidad.length();) {
            if( Cantidad.charAt(i) == ',' || Cantidad.charAt(i) == '.' )
                Cantidad.deleteCharAt(i);
            else
                ++i;
        }
        return Cantidad.toString();
    }
    
    public String getmoneda(String cmoneda) {
        String newmoneda = "";
        if (cmoneda.equals("N$"))
        { newmoneda = "01";}
        else if (cmoneda.equals("US$"))
        { newmoneda = "02";}
        else
        { newmoneda = "03";}
        return newmoneda;
    }
    
   
    public String removeAsterisk(String campo) {
        StringBuffer result = new StringBuffer(campo);
        String caracter = result.substring(result.length()-1,result.length());
        if (caracter.equals("*"))
            result.deleteCharAt(result.length()-1);
        return result.toString();
    }
    
}
