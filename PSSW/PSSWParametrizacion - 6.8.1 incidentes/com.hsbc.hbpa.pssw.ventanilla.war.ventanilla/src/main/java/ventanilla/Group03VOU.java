//*************************************************************************************************
//             Funcion: Clase que realiza txn 0066 - 0072 - 0073
//            Elemento: Group03VOU.java
//          Creado por: Alejandro Gonzalez Castro
//      Modificado por: Alejandro Gonzalez Castro
//*************************************************************************************************
// CCN - 4360189 - 03/09/2004 - Se cambia invoke al Diario para Control de Efectivo
//*************************************************************************************************

package ventanilla; 

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.util.Hashtable;

public class Group03VOU extends HttpServlet {
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession(false);
        Hashtable data = (Hashtable)session.getAttribute("page.datasession");
        data.put("d_ConsecLiga",(String)session.getAttribute("d_ConsecLiga"));
        data.put("dirip",(String)session.getAttribute("dirip"));

        if (session.getAttribute("ctrlCiclo") == null)
            data.put("ctrlCiclo","1");
        else
            data.put("ctrlCiclo",(String)session.getAttribute("ctrlCiclo"));
        if (session.getAttribute("ctrlConsecutivo") == null)
            data.put("ctrlConsecutivo","1");
        else
            data.put("ctrlConsecutivo",(String)session.getAttribute("ctrlConsecutivo"));
        if (session.getAttribute("ctrl") == null)
            data.put("ctrl","0");
        else
            data.put("ctrl","1");
        ventanilla.Vouchers03 vVouchers = new ventanilla.Vouchers03(data, session);
        vVouchers.initProceso();
        int intCiclo = vVouchers.getCiclo();
        int intConsecutivo = vVouchers.getConsecutivo();
        int intTermino = vVouchers.getTermino();
        session.setAttribute("ctrlConsecutivo",String.valueOf(intConsecutivo));
        session.setAttribute("ctrlCiclo",String.valueOf(intCiclo));
        String codetxn = (String)data.get("codetxn");
        if (intTermino == 2 || !codetxn.equals("0") )
            session.setAttribute("page.getoTxnView","ResponseVouchers4");
        else
            session.setAttribute("page.getoTxnView","ResponseVouchers3");
        session.setAttribute("ctrl","1");
        session.setAttribute("page.txnresponse", "0~02~0151515~TRANSACCION ACEPTADA~");
        getServletContext().getRequestDispatcher("/servlet/ventanilla.Redirector").forward(request,response);
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}
