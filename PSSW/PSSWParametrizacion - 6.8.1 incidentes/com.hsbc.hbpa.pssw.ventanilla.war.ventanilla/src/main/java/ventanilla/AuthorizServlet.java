//*************************************************************************************************
//			 Funcion: Clase para autorizaciones Remotas
//			Elemento: autorizRemote.java
//		  Creado por: Rutilo Zarco
//	  Modificado por: Juvenal Fernandez
//*************************************************************************************************
//CCN - 4360259 - 21/01/2005 - Se agrega el nombre en la autorizacion remota
//CCN - 4360274 - 16/02/2005 - Se corrige problema con horario C
//CCN - 4360325 - 03/06/2005 - Se apuntan los cursores de selecci�n a NULL
// CCN - 4360334 - 17/06/2005 - Se elimina variable dbQualifier
//*************************************************************************************************

package ventanilla;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class AuthorizServlet extends HttpServlet
{
  public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
  {
        String statementPS = com.bital.util.ConnectionPoolingManager.getStatementPostFix();
        String SupervisorID = request.getParameter("txtSupervisorID");
        String SupervisorPsswrd = request.getParameter("txtPassword");
        String txtAutoriza = "0";
        String txtMensaje = "";

        java.sql.Connection pooledConnection = com.bital.util.ConnectionPoolingManager.getPooledConnection( );
		java.sql.PreparedStatement newPreparedStatement = null;
		java.sql.ResultSet SupervisorRS = null;
        try{
         if(pooledConnection != null){
          newPreparedStatement =
              pooledConnection.prepareStatement("SELECT * FROM tc_Usuarios_Suc WHERE c_Cajero = ?" + statementPS);
          newPreparedStatement.setString(1,SupervisorID);
          SupervisorRS = newPreparedStatement.executeQuery();
          txtMensaje = "El usuario es invalido";
          if(SupervisorRS.next()){
           String newPassword = (String)SupervisorRS.getString("d_clave_acc");
           if(newPassword.equals(SupervisorPsswrd)){
            short newnivelCajero = (short)SupervisorRS.getShort("n_Nivel");
            if(newnivelCajero < 2)
             txtMensaje = "El nivel de cajero es incorrecto";
            else{
             txtMensaje = "";
             txtAutoriza = "1";
            }
           }
           else
            txtMensaje = "La contraseqa es invalida";
          }
         }
        }
        catch(java.sql.SQLException sqlexception){
         System.out.println("Error AuthorizServlet::doPost: [" + sqlexception.toString() + "] ");
        }
		finally 
		{
			try 
			{
			  if(SupervisorRS != null) 
			  {
				SupervisorRS.close();
				SupervisorRS = null;
			  }
			  if(newPreparedStatement != null) 
			  {
				  newPreparedStatement.close();
				  newPreparedStatement = null;
			  }
			  if(pooledConnection != null) 
			  {
				  pooledConnection.close();
				  pooledConnection = null;
			  }
			 }
			 catch(java.sql.SQLException sqlException)
			 {
			  System.out.println("Error FINALLY AuthorizServlet::doPost::Select [" + sqlException + "]");
			 }
		}
        javax.servlet.http.HttpSession httpSession = request.getSession();
        httpSession.setAttribute("autho.txtAutoriza",txtAutoriza);
        httpSession.setAttribute("autho.Supervisor",SupervisorID);
        httpSession.setAttribute("autho.Mensaje",txtMensaje);
        response.sendRedirect("../ventanilla/paginas/authorizlocal.jsp");
  }

  public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
  {
    doPost(request, response);
  }
}
