package ventanilla.services;


import java.rmi.RemoteException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.StringTokenizer;
import com.bital.util.ConnectionPoolingManager;



public class InsCGER {

	public int num=0;
	
	public String putCGER(String sCGER, String sKey) throws RemoteException{

		String sCGER2 = "";		
		//Datos para insertar la tabla	
		String fch_sol   = "";				
		String suc_sol   = "";			
		String consec    = "";
		String suc_exp   = "";		
		String monto	 = "";
		String canal     = "";
		String cta	     = "";									
		String benefi	 = "";		
		//String detalle	 = "";				
		String cedula    = "";
		String autoriz   = "";			
		String consec1   = "";	
		//nt num=0;
		
		if(sCGER != null)
		{
		   if(!sCGER.equals("")){	  
		       StringTokenizer tok = new StringTokenizer(sCGER,"*");  
		       try{		   	 
			   		num = tok.countTokens();
			   		//System.out.println("numero de tokens"+num); 		
	
					if(tok.countTokens()==10){
					  while(tok.hasMoreTokens()) 
			  			 { 	  			    
	 		  			 	 fch_sol = tok.nextToken();
	 		  			 	 suc_sol = tok.nextToken();
	 		  			 	 consec  = tok.nextToken();
			  			     suc_exp = tok.nextToken();	
							 monto	 = tok.nextToken();
							 canal	 = tok.nextToken();						
							 cta	 = tok.nextToken();			
							 benefi	 = tok.nextToken();							 						
							 cedula  = tok.nextToken();	
							 autoriz = tok.nextToken();																 						 	 					 
						 }
							
				      if((fch_sol==null) ||(suc_sol==null) || (consec==null) || (suc_exp==null) || (monto==null) || (canal==null) || (cta==null)|| (benefi==null) || (cedula==null) || autoriz==null) 
				      	{			         
				         	sCGER2="InsCGER::Error::[Uno de los elementos de la cadena viene null]";
				        }
				      else
				      {			      
				      		String statementPS = ConnectionPoolingManager.getStatementPostFix();			
							Connection pooledConnection = ConnectionPoolingManager.getPooledConnection();
							PreparedStatement insert = null;
			
							int ResultInsert = 0;									
								try{							 
									if(pooledConnection != null){	
									  try{						 
									//	insert = pooledConnection.prepareStatement("INSERT INTO TW_CHQGER (C_FECHA_SOL,C_SUC_SOL,C_CONCARGA,D_SUC_EXP,D_MONTO,D_CANAL,D_SERIAL,D_CUENTA ,D_FECHA_EXP ,D_STATUS ,D_BENEFICIARIO ,D_DETALLE ,D_DIRECCION ,D_CEDULA,D_AUTORIZADO ,D_CONUREG) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
									    insert = pooledConnection.prepareStatement("INSERT INTO TW_CHQGER VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
										//System.out.println(insert);
										insert.setString(1,fch_sol);
										insert.setString(2,suc_sol);
										insert.setString(3,consec); 
										insert.setString(4,suc_exp);
										insert.setInt(5,Integer.parseInt(monto)); 
										insert.setString(6,canal);
										insert.setString(7," ");
										insert.setString(8,cta); 
										insert.setString(9," ");
										insert.setString(10,"1"); 
										insert.setString(11,benefi); 
										insert.setString(12," ");
										insert.setString(13," ");
										insert.setString(14,cedula);
										insert.setString(15,autoriz);						
										insert.setString(16,consec);
										ResultInsert= insert.executeUpdate();
							    		if(ResultInsert > 0){
											sCGER2="InsCGER::InsertSolCheque::Se insert� ["+ResultInsert+"] registro en la tabla TW_CHQGER";
											System.out.println("InsCGER::InsertSolCheque::Se insert� ["+ResultInsert+"] registro en la tabla TW_CHQGER");
											System.out.println("InsCGER::InsertSolCheque::Se actualiz� el status a \"1\"");
										}						
									 //insert.executeUpdate();						
									 }catch(SQLException sqlException){
									 		sCGER2="InsCGER::Error::InsertLog [" + sqlException + "]";
								            System.err.println("InsCGER::Error::InsertLog [" + sqlException + "]");		 			 			
									 }
									}        
							   	}catch( Exception e ){
							   		System.err.println("InsCGER::Error::[" + e.getMessage()+"]");
									e.printStackTrace();		 	}	
					    }
	
			
					} else if(tok.countTokens()>10) { 				   
	 				    sCGER2="InsCGER::Error::[La cadena tiene mas elementos]";
					} else if(tok.countTokens()<10) {
						sCGER2="InsCGER::Error::[La cadena esta incompleta]";
					}
		       }catch(Exception e){
			     	sCGER2="InsCGER::Error::ParserLog["+e.getMessage()+"]";		     	
			     	System.out.println(e.getMessage());
		       }
		   }
		   //***
		}else if(sCGER == null || sCGER.equals(""))
		{
			  sCGER2="InsCGER::Error::[La cadena esta vacia]";
		}			      	
    	return sCGER2;		
	}		
}
