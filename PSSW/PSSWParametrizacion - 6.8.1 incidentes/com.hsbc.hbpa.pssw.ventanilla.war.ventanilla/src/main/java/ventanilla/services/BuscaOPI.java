package ventanilla.services;


import java.rmi.RemoteException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.bital.util.ConnectionPoolingManager;


public class BuscaOPI {

	public String getOPI(String sOPI, String sKey) throws RemoteException{
		String sOPI2 = "";				
		Connection pooledConnection = ConnectionPoolingManager.getPooledConnection();
		PreparedStatement select = null;
		ResultSet res = null;		
		try {
			if(pooledConnection != null){				
				select = pooledConnection.prepareStatement("SELECT * FROM TW_OPEE WHERE N_ORDEN_PAGO = ?");
				select.setString(1, sOPI); 
				res = select.executeQuery();
				if(res != null){
					while(res.next()){						
						sOPI2 = "|"+ res.getString(1).trim() + "|"+ res.getString(2).trim() + "|"+ res.getString(3).trim() +
							"|"+ res.getString(4).trim() + "|"+ res.getString(5).trim() + "|"+ res.getString(6).trim() +
							"|"+ res.getString(7).trim() + "|"+ res.getString(8).trim() + "|"+ res.getString(9).trim() +
							"|"+ res.getString(10).trim() + "|"+ res.getString(11).trim() + "|"+ res.getString(12).trim() +
							"|"+ res.getString(13).trim() + "|"+ res.getString(14).trim() + "|"+ res.getString(15).trim() + 
							"|"+ res.getString(16).trim() + "|"+ res.getString(17).trim() + "|"+ res.getString(18).trim() + 
							"|"+ res.getString(19).trim() + "|"+ res.getString(20).trim() + "|";						
					}					
				}
				else{
					sOPI2 = "";
				}			
			}    
		}catch(SQLException sqlException){
            System.err.println("BuscaOPI::InsertLog [" + sqlException.toString() + "]");
            throw new RemoteException(sqlException.getMessage());            
        }      
        finally {
        	try {
        		if(select != null) {
                	select.close();
                    select = null;
                }
                if(pooledConnection != null) {
                    pooledConnection.close();
                    pooledConnection = null;
                }
            }
            catch(java.sql.SQLException sqlException){
                System.err.println("BuscaOPI::InsertLog [" + sqlException.toString() + "]");
            	throw new RemoteException(sqlException.getMessage());                 
            }
        }                        
        
        if(!sOPI.equals("")){
        	PreparedStatement update = null;        
			String statementPS = com.bital.util.ConnectionPoolingManager.getStatementPostFix();
        	int ResultUpdate = 0;
	        try{
		 		pooledConnection = ConnectionPoolingManager.getPooledConnection();
		 		update = pooledConnection.prepareStatement("UPDATE TW_OPEE SET D_STATUS = ? WHERE N_ORDEN_PAGO = ? " + statementPS);
			    update.setString(1, "R");
			    update.setString(2, sOPI);
			    ResultUpdate= update.executeUpdate();
			    if(ResultUpdate > 0){
					System.out.println("BuscaOPI::UpdateBuscaOPI::Se actualiz� ["+ResultUpdate+"] registro");
					System.out.println("BuscaOPI::UpdateBuscaOPI::Se actualiz� el status a \"R\"");
				}
		 	}catch(Exception e){
		 		System.out.println(e.getMessage());
		 		throw new RemoteException(e.getMessage());		 		
		 	}finally {		 			
	    		try{
	    			if(update != null) {
	            		update.close();
	                	update = null;
	            	}
	            	if(pooledConnection != null) {
	                	pooledConnection.close();
	                	pooledConnection = null;
	            	}
	        	}catch(java.sql.SQLException sqlException){
	            	System.out.println("Error FINALLY BuscaOPI::UpdateLog [" + sqlException + "]");
	            	throw new RemoteException(sqlException.getMessage());                		
	        	}
		 	}
        } 	 	      		
    	return sOPI2;		
	}		
}
