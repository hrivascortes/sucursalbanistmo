//*************************************************************************************************
//             Funcion: Clase que realiza txn 5261
//            Elemento: Group02Remesas.java
//          Creado por: Alejandro Gonzalez Castro
//		Modificado por: Juvenal Fernandez
//*************************************************************************************************
// CCN - 4360189 - 03/09/2004 - Se cambia invoke al Diario para Control de Efectivo
// CCN - 4360268 - 04/02/2005 - Se realizan adecuaciones para eliminar Agencia NY
//*************************************************************************************************
package ventanilla;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.util.Hashtable;
import ventanilla.com.bital.sfb.Remesas;
import ventanilla.com.bital.admin.Diario;

public class Group02Remesas extends HttpServlet {
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession(false);
        Hashtable datasession = (Hashtable)session.getAttribute("page.datasession");
        String compara = (String)datasession.get("cTxn");

        String dirip =  (String)session.getAttribute("dirip");
		GenericClasses cGenericas = new GenericClasses();
        Remesas remesas = new Remesas();
        String txnTxn = (String)datasession.get("cTxn");
        String txtTeller = (String)datasession.get("teller");
        String txtFechaSys = "";
        String txtFechaEfect = "";

        remesas.setTxnCode(txnTxn);
        remesas.setBranch((String)datasession.get("sucursal"));	//sucursal
        remesas.setTeller(txtTeller);		//cajero
        remesas.setSupervisor(txtTeller);	//supervisor

         if ( (String)datasession.get("override") != null )
             if (datasession.get("override").toString().equals("SI"))
                remesas.setOverride("3");

        if ( (String)datasession.get("supervisor") != null )
             remesas.setSupervisor((String)datasession.get("supervisor"));

        StringBuffer efectivo = new StringBuffer((String)datasession.get("txtMonto"));
        for(int j=0; j<efectivo.length();) {
            if(efectivo.charAt(j) == ',' || efectivo.charAt(j) == '.')
                efectivo.deleteCharAt(j);
            else
                ++j;
        }

        String moneda = cGenericas.getDivisa((String)datasession.get("moneda"));

        if ( txnTxn.equals("5261")) {                            // Sucursal Mexico
            String cuenta = (String)datasession.get("txtDDACuenta");
            cuenta = cuenta.substring(0,1);
            remesas.setTranDesc("CARGO POR DEV REMESA C" + (String)datasession.get("lstCausa") + " D" + (String)datasession.get("txtDias"));
            remesas.setCheckNo5((String)datasession.get("txtDias"));
            remesas.setAcctNo((String)datasession.get("txtDDACuenta")); //cuenta
        }
        else {
            remesas.setHoldays("5");                              // Float 5 (dias)
            remesas.setAcctNo((String)datasession.get("txtAcct8000")); //cuenta
            txtFechaEfect  = (String)datasession.get("txtFechaEfect"); //Fecha en que se Opera la Txn.
            txtFechaSys  = (String)datasession.get("txtFechaSys"); //Fecha en que se Opera la Txn.
            txtFechaEfect =  cGenericas.ValEffDate(txtFechaEfect, txtFechaSys);
            remesas.setEffDate(txtFechaEfect);  //Fecha en que se opera la Txn.
        }

        remesas.setTranAmt(efectivo.toString());	//monto
        remesas.setTranCur(moneda);
        remesas.setCheckNo((String)datasession.get("txtSerial"));
        remesas.setTrNo((String)datasession.get("lstBancoRem") + (String)datasession.get("lstCausa"));
        remesas.setMoamoun("000");
        // Descripcion para reverso
        remesas.setDescRev("EMPTY");

        Diario diario = new Diario();
        String sMessage = diario.invokeDiario(remesas, (String)session.getAttribute("d_ConsecLiga"), dirip, moneda, session);

        session.setAttribute("page.txnresponse", sMessage);
        getServletContext().getRequestDispatcher("/servlet/ventanilla.Redirector").forward(request,response);
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}
