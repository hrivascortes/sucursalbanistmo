//*************************************************************************************************
//             Funcion: Clase para control de Usuarios de Apoyo
//          Creado por: Marco A. Lara Palacios
//      Modificado por: Juvenal R. Fernandez V.
//*************************************************************************************************
// CCN -4360176 - 06/08/2004 - Se habilita control de usuarios de apoyo
// CCN - 4360334 - 17/06/2005 - Se elimina variable dbQualifier
//*************************************************************************************************

package ventanilla;

import java.io.IOException;
import javax.servlet.ServletException;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.util.Calendar;
import java.util.Hashtable;

public class Group03GTE extends HttpServlet
{

  public void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException
  {
    HttpSession session = request.getSession(false);
    if( session == null )
      return;

    if( request.getParameter("[Cancelar].x") != null )
    {
      getServletContext().getRequestDispatcher("/ventanilla/paginas/final.html").forward(request, response);
      return;
    }

    Hashtable info = null;
    session.removeAttribute("error.msg");
    if( request.getParameter("inicial") != null )
    {
      info = new Hashtable();
      String temp = (String)session.getAttribute("branch");

      info.put("registro", request.getParameter("registro"));
      info.put("periodo", request.getParameter("periodo"));
      info.put("sucursal", temp.substring(1));
      info.put("gerente", session.getAttribute("empno"));
      session.setAttribute("info.user", info);

      getServletContext().getRequestDispatcher("/ventanilla/paginas/PageBuilderM0211.jsp").forward(request, response);
    }
    else if( request.getParameter("confirmar") != null )
    {
      info = (Hashtable)session.getAttribute("info.user");
      info.put("promotor", request.getParameter("promotor"));
      info.put("usuario", request.getParameter("usuario"));
      info.put("nombre", request.getParameter("nombre"));
      info.put("origen", request.getParameter("origen"));

      if( ExecuteSql(info) == -803 )
        session.setAttribute("error.msg", "Solo se permite un ejecutivo de apoyo por sucursal. Por favor verifique.");
      getServletContext().getRequestDispatcher("/ventanilla/paginas/ResponseM021.jsp").forward(request, response);
    }
    else if( request.getParameter("eliminar") != null )
    {
      String sucursal = request.getParameter("sucursal");
      String registro = request.getParameter("registro");

      if( ! ExecuteDelete(sucursal, registro) )
        session.setAttribute("error.msg", "Ocurrio un error al procesar su requerimiento. Intente mas tarde.");
      getServletContext().getRequestDispatcher("/ventanilla/paginas/ResponseM022.jsp").forward(request, response);
    }
  }

  public int ExecuteSql(Hashtable info)
  {
    String statementPS = com.bital.util.ConnectionPoolingManager.getStatementPostFix();
    java.sql.Connection pooledConnection = com.bital.util.ConnectionPoolingManager.getPooledConnection( );
    java.sql.PreparedStatement stmt = null;

    int registros = 0;
    int result = 0;
    try
    {
      if(pooledConnection != null)
      {
        StringBuffer cadsql = new StringBuffer(512);
        cadsql.append("INSERT INTO ")
          .append("TC_USUARIOS_APOYO VALUES(?, ?, ?, ?, ?, ?, ?, ?)");

        int dias = 1;
        Calendar today = Calendar.getInstance();
        try
        {
          dias = Integer.parseInt( info.get("periodo").toString() );
        }
        catch( NumberFormatException e )
        {
          e.printStackTrace(System.out);
          dias = 1;
        }
        today.add(Calendar.DATE, dias-1);

        stmt = pooledConnection.prepareStatement(cadsql.toString());
        stmt.setString(1, info.get("sucursal").toString());
        stmt.setString(2, info.get("gerente").toString());
        stmt.setString(3, info.get("registro").toString());
        stmt.setString(4, info.get("nombre").toString());
        stmt.setString(5, info.get("origen").toString());
        stmt.setString(6, info.get("usuario").toString());
        stmt.setString(7, info.get("promotor").toString());
        stmt.setDate(8, new java.sql.Date(today.getTime().getTime()));

        registros = stmt.executeUpdate();
      }
    }
    catch(java.sql.SQLException sqlException)
    {
      System.out.println("Error Group03GTE [" + sqlException + "]");
      System.out.println(sqlException.getErrorCode() + "/" + sqlException.getSQLState());
      result = sqlException.getErrorCode();
    }
    finally
    {
      try
      {
        if(stmt != null)
        {
          stmt.close();
          stmt = null;
        }

        if(pooledConnection != null)
        {
          pooledConnection.close();
          pooledConnection = null;
        }
      }
      catch(java.sql.SQLException sqlException)
      {
        System.out.println("Error FINALLY [" + sqlException + "]");
      }
    }
    return result;
  }

  public boolean ExecuteDelete(String sucursal, String registro)
  {
    String statementPS = com.bital.util.ConnectionPoolingManager.getStatementPostFix();
    java.sql.Connection pooledConnection = com.bital.util.ConnectionPoolingManager.getPooledConnection( );
    java.sql.PreparedStatement stmt = null;

    int registros = 0;
    try
    {
      if(pooledConnection != null)
      {
        StringBuffer cadsql = new StringBuffer(512);
        cadsql.append("DELETE FROM ")
          .append("TC_USUARIOS_APOYO WHERE C_SUCURSAL=? AND C_REGISTRO=?");

        stmt = pooledConnection.prepareStatement(cadsql.toString());
        stmt.setString(1, sucursal);
        stmt.setString(2, registro);

        registros = stmt.executeUpdate();
      }
    }
    catch(java.sql.SQLException sqlException)
    {
      System.out.println("Error Group03GTE [" + sqlException + "]");
      System.out.println(sqlException.getErrorCode() + "/" + sqlException.getSQLState());
    }
    finally
    {
      try
      {
        if(stmt != null)
        {
          stmt.close();
          stmt = null;
        }

        if(pooledConnection != null)
        {
          pooledConnection.close();
          pooledConnection = null;
        }
      }
      catch(java.sql.SQLException sqlException)
      {
        System.out.println("Error FINALLY [" + sqlException + "]");
      }
    }
    return (registros != 0);
  }
}
