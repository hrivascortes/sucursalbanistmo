//*************************************************************************************************
//             Funcion: Clase que realiza txn 0836
//            Elemento: Group0aRAP.java
//          Creado por: Alejandro Gonzalez Castro
//      Modificado por: Alejandro Gonzalez Castro
//*************************************************************************************************
// CCN - 4360189 - 03/09/2004 - Se cambia invoke al Diario para Control de Efectivo
// CCN - 4360259 - 21/01/2005 - Se evita crear una tabla de bines que no se ocupa
// CCN - 4360347 - 22/07/2005 - Se agregan "0" a la variable del servicio.
//*************************************************************************************************
package ventanilla;

import java.io.IOException;
import java.util.Hashtable;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import ventanilla.com.bital.admin.Diario;
import ventanilla.com.bital.sfb.RAPG;

public class Group04RAP extends HttpServlet
{
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        response.setContentType("text/plain");
        HttpSession session = request.getSession(false);
        Hashtable datasession = (Hashtable)session.getAttribute("page.datasession");
        String dirip = (String)session.getAttribute("dirip");

        RAPG rapg = new RAPG();

        rapg.setBranch((String)datasession.get("sucursal"));
        rapg.setTeller((String)datasession.get("teller"));
        rapg.setSupervisor((String)datasession.get("supervisor"));

        rapg.setTxnCode("0836");
        rapg.setFormat("G");
		GenericClasses gc = new GenericClasses();

        String moneda = (String)datasession.get("moneda");
		rapg.setToCurr(gc.getDivisa(moneda));
        rapg.setTranAmt("1");
        rapg.setAcctNo("1");

               
        String txtSer=(String)datasession.get("txtServicio");
        int size = txtSer.length();
  	int i=0;
  	for(i = size; i < 7; i++)
	   {
	   txtSer = "0" + txtSer;
	   }
	rapg.setBenefic(txtSer);
        rapg.setOrdenan((String)datasession.get("txtReferencia"));

        Diario diario = new Diario();
        String sMessage = diario.invokeDiario(rapg, (String)session.getAttribute("d_ConsecLiga"), dirip, moneda, session);

        if(sMessage.startsWith("0"))
        {
            int x = sMessage.indexOf("MONTO:");
            String monto = sMessage.substring(x+6, sMessage.length()-1);
            monto = monto.trim();
            sMessage = sMessage.substring(0,x);
            datasession.put("txtMonto", monto);
            session.setAttribute("page.datasession", datasession);
        }
        else
        {
            datasession.remove("flujorap");
        }

        session.setAttribute("page.txnresponse", sMessage);
        getServletContext().getRequestDispatcher("/servlet/ventanilla.Redirector").forward(request,response);
    }
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        doPost(request, response);
    }
}