//*************************************************************************************************
//             Funcion: Clase que realiza txn 5103
//            Elemento: Group01Remesas.java
//          Creado por: Alejandro Gonzalez Castro
//		Modificado por: Juvenal Fernandez
//*************************************************************************************************
// CCN - 4360189 - 03/09/2004 - Se cambia invoke al Diario para Control de Efectivo
// CCN - 4360268 - 04/02/2005 - Se realizan adecuaciones para eliminar Agencia NY
//*************************************************************************************************
package ventanilla;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.util.Hashtable;
import java.util.Stack;

import ventanilla.com.bital.sfb.Remesas;
import ventanilla.com.bital.admin.Diario;

public class Group01Remesas extends HttpServlet 
{   
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
    {
        HttpSession session = request.getSession(false);
        Hashtable datasession = (Hashtable)session.getAttribute("page.datasession");
        String compara = (String)datasession.get("cTxn");
        
        String dirip =  (String)session.getAttribute("dirip");
		GenericClasses cGenericas = new GenericClasses();
        Remesas remesas = new Remesas();
        String txnTxn = (String)datasession.get("cTxn");
        String txtTeller = (String)datasession.get("teller");
        
        remesas.setTxnCode(txnTxn);
        remesas.setBranch((String)datasession.get("sucursal"));	//sucursal
        remesas.setTeller(txtTeller);		//cajero
        remesas.setSupervisor(txtTeller);	//supervisor
        remesas.setAcctNo((String)datasession.get("txtDDACuenta")); //cuenta
        
        if ( (String)datasession.get("override") != null )
             if (datasession.get("override").toString().equals("SI"))
                remesas.setOverride("3");
        
        if ( (String)datasession.get("supervisor") != null )
             remesas.setSupervisor((String)datasession.get("supervisor"));
        
        
        String efectivo      = cGenericas.delCommaPointFromString((String)datasession.get("txtMontoCheque"));
        
        remesas.setTranAmt(efectivo); //monto
        
        String txtMontoTot   = (String)datasession.get("MontoTotal");
        long MontoCheque       = Long.parseLong(cGenericas.delCommaPointFromString((String)datasession.get("txtCheque")));
        long MontoTotal      = 0;
        
        if( txtMontoTot != null )
            MontoTotal = Long.parseLong(txtMontoTot);
        
        String moneda = "";
        String tmpMoneda = (String)datasession.get("moneda");
        moneda=cGenericas.getDivisa(tmpMoneda);
       
        remesas.setTranCur(moneda);
        
        //remesas.setCheckNo((String)datasession.get("txtSerial"));
        remesas.setTrNo((String)datasession.get("lstBancoRem"));
        remesas.setMoamoun("000");
        remesas.setHoldays("9");
        remesas.setCheckNo((String)datasession.get("txtNoCheqExt"));
        remesas.setReferenc1((String)datasession.get("lstBancoRem"));

        Diario diario = new Diario();
        String sMessage = diario.invokeDiario(remesas, (String)session.getAttribute("d_ConsecLiga"), dirip, moneda, session);
        
        if(sMessage.substring(0,1).equals("0"))
        {
            MontoTotal = MontoTotal + Long.parseLong(efectivo);
            datasession.put("MontoTotal",new Long(MontoTotal).toString());
            if (  MontoTotal != MontoCheque ) {
                Stack flujotxn = (Stack)session.getAttribute("page.flujotxn");
                if ( !flujotxn.empty())
                    flujotxn.pop();
                
                flujotxn.push("5103");
                session.setAttribute("page.flujotxn", flujotxn);
                datasession.remove("txtMontoCheque");
                datasession.remove("txtSerial");
                datasession.remove("lstBancoRem");
            }
        }
        else 
        {
            Stack flujotxn = (Stack)session.getAttribute("page.flujotxn");
            flujotxn.removeAllElements();
            session.setAttribute("page.flujotxn",flujotxn);
        }
        
        session.setAttribute("page.txnresponse", sMessage);
        getServletContext().getRequestDispatcher("/servlet/ventanilla.Redirector").forward(request,response);
    }
    
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}