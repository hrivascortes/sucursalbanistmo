//*************************************************************************************************
//             Funcion: Clase que realiza txn 0726
//            Elemento: CV_0726_XXXX.java
//          Creado por: Alejandro Gonzalez Castro
//		Modificado por: Fredy Pe�a moreno
//*************************************************************************************************
// CCN - 4360189 - 03/09/2004 - Se cambia invoke al Diario para Control de Efectivo
// CCN - 4360221 - 22/10/2004 - Se envia codigo de instrumento para txn 0821
// CCN - 4360268 - 04/02/2005 - Se realizan adecuaciones para eliminar Agencia NY
// CCN - 4360368 - 02/09/2005 - Se elimina sleep
// CCN - 4360374 - 08/09/2005 - Se genera nuevo paquete a peticion de QA.
// CCN - 4360532 - 20/10/2006 - Se realizan modificaciones para enviar el numero de cajero, consecutivo txn entrada y salida en txn 0821
// CCN - 4620048 - 14/12/2007 - Se realizan modificaciones para compraventa
// CCN - 4620068 - 13/02/2008 - Se realizan modificaciones en cadena de impresi�n para que mande las divisas
//*************************************************************************************************
package ventanilla;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import ventanilla.GenericClasses;

import java.util.Hashtable;

import ventanilla.com.bital.sfb.CompraVenta;
import ventanilla.com.bital.admin.Diario;

public class CV_0726_XXXX extends HttpServlet 
{   
    public void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException 
    {
        String TE, TO, Currency, Currency2,Divisa,Divisa2;
        int Resultado = -1;
        
        CompraVenta oTxn = new CompraVenta();
        Diario diario;
        String sMessage = null;
        GenericClasses cGenericas = new GenericClasses();
        
        HttpSession session = req.getSession(false);
        Hashtable datasession = (Hashtable)session.getAttribute("page.datasession");
        TE = (String)datasession.get("cTxn");
        TO = (String)datasession.get("oTxn");
        Currency = (String)datasession.get("moneda");
        Currency2 = (String)session.getAttribute("page.moneda_s");
        String txtPlaza = (String)datasession.get("txtPlaza");
        String msg = null;
        String dirip =  (String)session.getAttribute("dirip");
        String txtCasoEsp = new String("");
        String txtCadImpresion = new String("");
        String txtNomCliente = new String("");
        String txtSupervisor = (String)datasession.get("teller");
        String txtRegistro = (String)datasession.get("txtRegistro");
        String txtOverride = "0";
        String txtComision =  "";
        String txtPorcIVA  =  "";
        String txtDirSuc   =  "";
        //String Currency2 = "";
        String txtNumOrden = "0";
        String Monto = "";
        String txtComisionMN = "";
        String txtComisionUSD = "";
        String txtIvaMN = "";
        String txtIvaUSD = "";
        String ConsecutivoIN = "";
        String ConsecutivoOUT = "";
		Divisa = cGenericas.getDivisa(Currency);
		Divisa2 = cGenericas.getDivisa(Currency2);
        if (datasession.get("txtComision") == null) {
            datasession.put("txtComision","0.00");
            datasession.put("txtIVA","0.00");
        }
       /* if ( Currency.equals("01"))//fpm multimoneda
        //	Currency2=cGenericas.getDivisa(Currency2);
            Currency2 = "02";
        else
            Currency2 = "01";*/
        
        if ( (String)datasession.get("override") != null )
            if (datasession.get("override").toString().equals("SI"))
                txtOverride = "3";
        
        if ( (String)datasession.get("supervisor") != null )                      //pidio autorizacisn de supervisor
            txtSupervisor =  (String)datasession.get("supervisor");
        
        if ( datasession.get("posteo") == null ) {
            datasession.put("posteo","1");
            session.setAttribute("page.datasession",(Hashtable)datasession);
        }
        
        if ( session.getAttribute("txtCasoEsp") != null ){
            txtCasoEsp = session.getAttribute("txtCasoEsp").toString();
        }
        oTxn.setTxnCode(TE);
        oTxn.setBranch((String)datasession.get("sucursal"));
        oTxn.setTeller((String)datasession.get("teller"));
        oTxn.setSupervisor(txtSupervisor);
        
        if( TE.equals( "0726" ) ) {
            //ODCT 0726B 00001000130  000130  000**0726*300000*1****300000**US$**
            if ( datasession.get("posteo").toString().equals("1"))
            {
                oTxn.setFormat( "B" );
                oTxn.setAcctNo( "0726" );
                if(Currency.equals("01")) {//fpm                	
                    oTxn.setTranAmt(cGenericas.delCommaPointFromString((String)datasession.get( "txtMonto" )));
                    oTxn.setCashIn(cGenericas.delCommaPointFromString((String)datasession.get( "txtMonto" )));
                    if(TO.equals("0786"))
                        oTxn.setReversable("N");
                }
                else {
                    oTxn.setTranAmt(cGenericas.delCommaPointFromString((String)datasession.get( "txtMonto1" )));
                    oTxn.setCashIn(cGenericas.delCommaPointFromString((String)datasession.get( "txtMonto1" )));
                }
                oTxn.setFromCurr(cGenericas.getDivisa(Currency));
                oTxn.setReferenc("1");
                oTxn.setOverride(txtOverride);
                
                diario = new Diario();
                sMessage = diario.invokeDiario(oTxn, (String)session.getAttribute("d_ConsecLiga"), dirip, Currency, session);//fpm
                
                Resultado = oTxn.getStatus();
                
                if ( Currency.equals("01"))//fpm
                    Monto = cGenericas.delCommaPointFromString((String)datasession.get("txtMonto"));
                else
                    Monto = cGenericas.delCommaPointFromString((String)datasession.get("txtMonto1"));
                if ( Resultado == 0 || Resultado == 1){
                    txtCasoEsp = txtCasoEsp + TE + "~" + cGenericas.getString(oTxn.getMessage(),1) + "~";
                    ConsecutivoIN = cGenericas.getString(oTxn.getMessage(),1);
                    if (ConsecutivoIN.length()<6){
                    	ConsecutivoIN = "0" + ConsecutivoIN; 
                    }	
                }    
            }
            else
                Resultado = 0;
        }
        
        if ( TE.equals("0726") && Resultado == 0 ) 
        {
            if ( datasession.get("posteo").toString().equals("1")) {
                datasession.put("override","NO");
                datasession.put("posteo","2");
                txtOverride = "0";
            }
            
            oTxn = new CompraVenta();
            oTxn.setBranch((String)datasession.get("sucursal"));
            oTxn.setTeller((String)datasession.get("teller"));
            oTxn.setSupervisor(txtSupervisor);
            oTxn.setOverride(txtOverride);
            
            if(TO.equals("0106")) {
                //ODCT 0106B 00001000130  000130  000**0106*64500*1*****64500*US$**
                oTxn.setTxnCode("0106");
                oTxn.setFormat("B");
                oTxn.setAcctNo( "0106" );
                oTxn.setTranAmt( cGenericas.delCommaPointFromString((String)datasession.get( "txtMontoC" )) );
                oTxn.setCashOut( cGenericas.delCommaPointFromString((String)datasession.get( "txtMontoC" )) );
/*                if(Currency.equals("01"))//moneda
                    oTxn.setFromCurr("US$");
                else
                    oTxn.setFromCurr("N$");*/                    
                oTxn.setFromCurr(cGenericas.getDivisa(Currency2));                       
                oTxn.setReferenc("1");
            }
            
            else if ( TO.equals("4111") ) {
                //ODCT 4111A 00001000131  000131  000**7000000000**1056*US$***000**01400094700*10000**ABO.CTA.CHQS.  US$************************************4043**
                //ODCT 4111A 00001000131  000131  000**4000000000**10000*N$***000**0140009220*1085**ABO.CTA.CHQS.  M.N.************************************4043**
                //ODCT 4111A 00001000130  000130  000**4000000000**60000*US$***000**00100100000*600000**ABO.CTA.CHQS.  US$************************************0726**
                oTxn.setTxnCode("4111");
                oTxn.setFormat( "A" );
                oTxn.setAcctNo( (String)datasession.get( "txtDDACuenta1" ) );
                oTxn.setTranAmt( cGenericas.delCommaPointFromString((String)datasession.get( "txtMontoC" )) );
                oTxn.setTranCur(cGenericas.getDivisa(Currency2));
                if(Currency.equals("01")) {//fpm
                 //   oTxn.setTranCur( "US$" );
                    oTxn.setMoAmoun( cGenericas.delCommaPointFromString((String)datasession.get( "txtMonto" )) );
                }
                else {
                   // oTxn.setTranCur( "N$" );
                    oTxn.setMoAmoun( cGenericas.delCommaPointFromString((String)datasession.get( "txtMonto1" )) );
                }
                oTxn.setTranDesc( "ABO.CTA.CHQS. "+cGenericas.getDivisa(Currency2));
                oTxn.setCashIn( "000" );
                oTxn.setTrNo5( "0726" );
                String txtTipoCambio =  cGenericas.StringFiller((String)datasession.get( "txtTipoCambio" ), ((String)datasession.get( "txtTipoCambio" )).length()+3, false, "0");
                txtTipoCambio = "0000" + txtTipoCambio;
                txtTipoCambio = txtTipoCambio.substring(txtTipoCambio.length()-8);
                oTxn.setDraftAm(txtPlaza + txtTipoCambio);
                oTxn.setFees( "0" );
                oTxn.setDescRev("EMPTY");
            }
            //txn's no habilitas -->
            else if ( TO.equals("0778") ) {
                //ODCT 0778B 00001000130  000130  000**0778*10000*004001234560000092500*400123456****10000*US$**
                oTxn.setTxnCode(TO);
                oTxn.setFormat( "B" );
                oTxn.setAcctNo( "0778" );
                oTxn.setTranAmt( cGenericas.delCommaPointFromString((String)datasession.get( "txtMonto1" )) );
                String txtTipoCambio =  cGenericas.StringFiller((String)datasession.get( "txtTipoCambio" ), ((String)datasession.get( "txtTipoCambio" )).length()+3, false, "0");
                txtTipoCambio = "0000" + txtTipoCambio;
                txtTipoCambio = txtTipoCambio.substring(txtTipoCambio.length()-10);
                oTxn.setReferenc( "00400" + (String)datasession.get( "txtFolioSerb" ) + txtTipoCambio);
                oTxn.setFees("400" + (String)datasession.get( "txtFolioSerb" ));
                oTxn.setCashOut(cGenericas.delCommaPointFromString((String)datasession.get("txtMonto1")));
                oTxn.setFromCurr("US$");
            }
            
            else if ( TO.equals("4113") ) {
                //ODCT 4113A 00001000130  000130  000**102963154815581**2700000*N$*****00100090000*300000**ABO.CTA.INVERS.  M.N.************************************0726**
                oTxn.setFormat( "A" );
                oTxn.setTxnCode(TO);
                oTxn.setAcctNo( (String)datasession.get( "txtCDACuenta" ) );
                oTxn.setTranAmt( cGenericas.delCommaPointFromString((String)datasession.get( "txtMonto" )) );
                oTxn.setTranCur("N$");
                oTxn.setMoAmoun( cGenericas.delCommaPointFromString((String)datasession.get( "txtMonto1" )) );
                String txtTipoCambio =  cGenericas.StringFiller((String)datasession.get( "txtTipoCambio" ), ((String)datasession.get( "txtTipoCambio" )).length()+3, false, "0");
                txtTipoCambio = "0000" + txtTipoCambio;
                txtTipoCambio = txtTipoCambio.substring(txtTipoCambio.length()-8);
                oTxn.setDraftAm( txtPlaza + txtTipoCambio );
                oTxn.setTranDesc( "ABO.CTA.INVERS.  M.N." );
                oTxn.setTrNo5( "0726" );
                oTxn.setDescRev("EMPTY");
            }
            
            else if ( TO.equals("4115") ) {
                //ODCT 0350B 00001000130  000130  000**0350*8500*COM.CHQ.CAJA****8500**N$**
                //if ( datasession.get("posteo").toString().equals("1")) {

                oTxn.setTxnCode("0350");
                oTxn.setFromCurr("N$");
                oTxn.setFormat("B");
                oTxn.setAcctNo("0350");
                oTxn.setTranAmt(cGenericas.delCommaPointFromString((String)datasession.get( "txtComisionMN" )));
                oTxn.setReferenc("COM.CHQ.CAJA");
                oTxn.setCashIn(cGenericas.delCommaPointFromString((String)datasession.get( "txtComisionMN" )));
                
                diario = new Diario();
                sMessage = diario.invokeDiario(oTxn, (String)session.getAttribute("d_ConsecLiga"), dirip, "N$", session);
                
                Resultado = oTxn.getStatus();
                
                Monto = cGenericas.delCommaPointFromString((String)datasession.get("txtMonto"));
                
                if ( Resultado == 0 || Resultado == 1)
                    txtCasoEsp = txtCasoEsp + "0350" + "~" + cGenericas.getString(oTxn.getMessage(),1) + "~";
                
                if(sMessage.startsWith("0")) 
                {

                    //ODCT 0372B 00001000130  000130  000**0372*850*IVA COM.CHQ.CAJA****850**N$**
                    oTxn.setTxnCode("0372");
                    oTxn.setFromCurr("N$");
                    oTxn.setFormat("B");
                    oTxn.setAcctNo("0372");
                    oTxn.setTranAmt(cGenericas.delCommaPointFromString((String)datasession.get( "txtIvaMN" )));
                    oTxn.setReferenc("IVA COM.CHQ.CAJA");
                    oTxn.setCashIn(cGenericas.delCommaPointFromString((String)datasession.get( "txtIvaMN" )));
                    
                    diario = new Diario();
                    sMessage = diario.invokeDiario(oTxn, (String)session.getAttribute("d_ConsecLiga"), dirip, "N$", session);
                    
                    Resultado = oTxn.getStatus();
                    
                    if ( Resultado == 0 || Resultado == 1)
                        txtCasoEsp = txtCasoEsp + "0372" + "~" + cGenericas.getString(oTxn.getMessage(),1) + "~";
                    Monto = cGenericas.delCommaPointFromString((String)datasession.get("txtMonto"));
                    
                }
                 /*}
                 else
                     Resultado = 0;*/
                
                if(Resultado == 0) 
                {

                    //ODCT 4115A 00001000130  000130  000**3902222222**60000*US$*0830001457****00100100000*600000**BENEFICIARIO UNO******010.0000******************************0726**
                    oTxn.setTxnCode(TO);
                    oTxn.setFormat( "A" );
                    if(Currency.equals("01")) {//fpm
                        oTxn.setAcctNo( "3902222222" );
                        oTxn.setTranCur( "US$" );
                        oTxn.setMoAmoun( cGenericas.delCommaPointFromString((String)datasession.get( "txtMonto" )) );
                    }
                    else {
                        oTxn.setAcctNo( "0103000023" );
                        oTxn.setTranCur( "N$" );
                        oTxn.setMoAmoun( cGenericas.delCommaPointFromString((String)datasession.get( "txtMonto1" )) );
                    }
                    oTxn.setTranAmt( cGenericas.delCommaPointFromString((String)datasession.get( "txtMontoC" )) );
                    oTxn.setCheckNo( (String)datasession.get( "txtSerial" ) );
                    oTxn.setTranDesc( (String)datasession.get( "txtBeneficiario" ) );
                    oTxn.setDescRev( (String)datasession.get( "txtBeneficiario" ) );
                    oTxn.setTrNo5( "0726" );
                    String txtTipoCambio =  cGenericas.StringFiller((String)datasession.get( "txtTipoCambio" ), ((String)datasession.get( "txtTipoCambio" )).length()+3, false, "0");
                    txtTipoCambio = "0000" + txtTipoCambio;
                    txtTipoCambio = txtTipoCambio.substring(txtTipoCambio.length()-8);
                    oTxn.setDraftAm(txtPlaza + txtTipoCambio);
                    oTxn.setAcctNo1( txtTipoCambio.substring(1,4) + "." +  txtTipoCambio.substring(5));
                }
            }
            
            else if ( TO.equals("0786") ) 
            {
                //ODCT 0354B 00001000130  000130  000**0354*35000*COM.EXP.O.P.****35000**N$**

                oTxn.setTxnCode("0354");
                oTxn.setFromCurr("N$");
                oTxn.setFormat("B");
                oTxn.setAcctNo("0354");
                oTxn.setTranAmt(cGenericas.delCommaPointFromString((String)datasession.get( "txtComisionMN" )) );
                oTxn.setReferenc("COM.EXP.O.P.");
                oTxn.setCashIn(cGenericas.delCommaPointFromString((String)datasession.get( "txtComisionMN" )));
                oTxn.setReversable("N");
                
                diario = new Diario();
                sMessage = diario.invokeDiario(oTxn, (String)session.getAttribute("d_ConsecLiga"), dirip, "N$", session);
                
                Resultado = oTxn.getStatus();
                
                if ( Resultado == 0 || Resultado == 1)
                    txtCasoEsp = txtCasoEsp + "0354" + "~" + cGenericas.getString(oTxn.getMessage(),1) + "~";
                
                Monto = cGenericas.delCommaPointFromString((String)datasession.get("txtMonto"));
                
                if(sMessage.startsWith("0")) {

                    //ODCT 0372B 00001000130  000130  000**0372*3500*IVA COM.EXP.O.P.****3500**N$**
                    oTxn.setTxnCode("0372");
                    oTxn.setFromCurr("N$");
                    oTxn.setFormat("B");
                    oTxn.setAcctNo("0372");
                    oTxn.setTranAmt(cGenericas.delCommaPointFromString((String)datasession.get( "txtIvaMN" )));
                    oTxn.setReferenc("IVA COM.EXP.O.P.");
                    oTxn.setCashIn(cGenericas.delCommaPointFromString((String)datasession.get( "txtIvaMN" )));
                    oTxn.setReversable("N");
                    
                    diario = new Diario();
                    sMessage = diario.invokeDiario(oTxn, (String)session.getAttribute("d_ConsecLiga"), dirip, "N$", session);
                    
                    Resultado = oTxn.getStatus();
                    
                    Monto = cGenericas.delCommaPointFromString((String)datasession.get("txtMonto"));
                    
                    if ( Resultado == 0 || Resultado == 1)
                        txtCasoEsp = txtCasoEsp + "0372" + "~" + cGenericas.getString(oTxn.getMessage(),1) + "~";
                    
                    if(sMessage.startsWith("0")) {

                        //ODCT 0786B 00001000130  000130  000**0726*6000*BENEFICIARIO TRES******US$**
                        oTxn.setTxnCode(TO);
                        oTxn.setFormat( "B" );
                        oTxn.setAcctNo(TE);
                        oTxn.setTranAmt(cGenericas.delCommaPointFromString((String)datasession.get( "txtMonto1" )));
                        oTxn.setReferenc((String)datasession.get( "txtBeneficiario" ));
                        oTxn.setFromCurr("US$");
                        oTxn.setReversable("N");
                    }
                }
            }
            // <-- fin txn's no habilitadas  
            diario = new Diario();
            sMessage = diario.invokeDiario(oTxn, (String)session.getAttribute("d_ConsecLiga"), dirip, Currency2, session);
            
            Resultado = oTxn.getStatus();
            
            if ( Currency2.equals("01"))//fpm
                Monto = cGenericas.delCommaPointFromString((String)datasession.get("txtMonto"));
            else
                Monto = cGenericas.delCommaPointFromString((String)datasession.get("txtMonto1"));
            
            if ( Resultado == 0 || Resultado == 1){
                txtCasoEsp = txtCasoEsp + TO + "~" + cGenericas.getString(oTxn.getMessage(),1) + "~";
                ConsecutivoOUT = cGenericas.getString(oTxn.getMessage(),1);
                    if (ConsecutivoOUT.length()<6){
                    	ConsecutivoOUT = "0" + ConsecutivoOUT; 
                    }                
            }
            if(sMessage.startsWith("0")) {
                if (TO.equals("0786")) {
                    txtNumOrden = cGenericas.getPositionalString(oTxn.getMessage(),247,10, "NO");
                }
            }
        }
        
        if (  TE.equals("0726") && Resultado == 0) {   // Cargo y Abono Aceptado
            //ODCT 0821G 00001000130  000130  000**V*US$*0000000000*1**64500*009288000*0*0446096*0446096*0726***
            //ODCT 0821G 00001000130  000130  000**V*US$*0000000000*5**600000*010000000*0*0446096*0446096*0726***
            //ODCT 0821G 00001000130  000130  000**V*US$*0000000000*6**60000*010000000*850*0446096*0446096*0726***

            oTxn = new CompraVenta();
            oTxn.setBranch((String)datasession.get("sucursal"));
            oTxn.setTeller((String)datasession.get("teller"));
            oTxn.setSupervisor(txtSupervisor);
            oTxn.setTxnCode("0821");
            oTxn.setFormat( "G" );
            String monedacv="";
            if(Currency.equals("01")){
            	monedacv= Currency2;            
            }else{
            	monedacv=Currency;
            }
//            oTxn.setToCurr( "US$" );
            oTxn.setToCurr(cGenericas.getDivisa(monedacv));
           // if(Currency.equals("02")) {//fpm
           if(Integer.parseInt(Currency)>1) {
                oTxn.setService( "C" );
                oTxn.setGStatus( "1" );
            }
            else {
                oTxn.setService( "V" );
                if ( TO.equals("0106"))
                    oTxn.setGStatus( "1" );
                else if (TO.equals("4111"))
                    oTxn.setGStatus( "5" );
                else if (TO.equals("4115"))
                    oTxn.setGStatus( "6" );
                else if (TO.equals("0778"))
                    oTxn.setGStatus( "4" );
                else if (TO.equals("0786")) {
                    oTxn.setGStatus( "3" );
                    oTxn.setReversable("N");
                }
                else if (TO.equals("0116"))
                    oTxn.setGStatus( "2" );
            }
            String txtAutoriCV = (String)datasession.get("txtAutoriCV");
            if ( txtAutoriCV == null )
                txtAutoriCV = "00000";
            oTxn.setCheckNo( "00000" + txtAutoriCV );
            oTxn.setTranAmt( cGenericas.delCommaPointFromString((String)datasession.get( "txtMonto1" )) );
            String txtTipoCambio =  cGenericas.StringFiller((String)datasession.get( "txtTipoCambio" ), ((String)datasession.get( "txtTipoCambio" )).length()+2, false, "0") + "00"; 
            txtTipoCambio = "0000" + txtTipoCambio;
            txtTipoCambio = txtTipoCambio.substring(txtTipoCambio.length()-9);
            oTxn.setComAmt(txtTipoCambio);
            oTxn.setIVAAmt( cGenericas.delCommaPointFromString((String)datasession.get("txtMonto")));
            oTxn.setAcctNo( (String)datasession.get( "registro" ) );
            oTxn.setCtaBenef( (String)datasession.get( "txtRegistro" ) );   
            if(Integer.parseInt(Currency)>1) {
            	oTxn.setBenefic("0726"+TO+"  " + ConsecutivoIN + ConsecutivoOUT + (String)datasession.get("teller"));
            }else{
            	oTxn.setBenefic(TO+"0726  " + ConsecutivoIN + ConsecutivoOUT + (String)datasession.get("teller"));
            }
            diario = new Diario();
            //sMessage = diario.invokeDiario(oTxn, (String)session.getAttribute("d_ConsecLiga"), dirip, "US$", session);//FPM
            sMessage = diario.invokeDiario(oTxn, (String)session.getAttribute("d_ConsecLiga"), dirip, monedacv, session);//FPM

            Monto = cGenericas.delCommaPointFromString((String)datasession.get("txtMonto"));
            
            if(sMessage.startsWith("0") || sMessage.startsWith("1")){
                txtCasoEsp = txtCasoEsp + "0821" + "~" + cGenericas.getString(oTxn.getMessage(),1) + "~";
                 
            }    
            
            if(sMessage.startsWith("0")) {
                String txtMonto = cGenericas.delCommaPointFromString((String)datasession.get( "txtMonto" ));
                String txtMonto1 = cGenericas.delCommaPointFromString((String)datasession.get( "txtMonto1" ));
                txtTipoCambio =  cGenericas.StringFiller((String)datasession.get( "txtTipoCambio" ), ((String)datasession.get( "txtTipoCambio" )).length()+2, false, "0");
                txtTipoCambio = "0000" + txtTipoCambio;
                txtTipoCambio = txtTipoCambio.substring(txtTipoCambio.length()-7);
                txtTipoCambio = txtTipoCambio.substring(0,3) + "." + txtTipoCambio.substring(3,6);
                String txtDDACuenta1 = (String)datasession.get( "txtDDACuenta" );
                String txtDDACuenta2 = (String)datasession.get( "txtDDACuenta1" );
                String txtSerial = (String)datasession.get( "txtSerial" );
                txtComision = cGenericas.delCommaPointFromString((String)datasession.get( "txtComision" ));
                String txtIva = cGenericas.delCommaPointFromString((String)datasession.get( "txtIVA" ));
                String txtTotalCI = cGenericas.delCommaPointFromString((String)datasession.get("txtMonto"));
                String txtComision$ = null;
                String txtIva$ = null;
                String txtTot$ = null;
                if ( TO.equals("0786")) {
                    txtSerial = txtNumOrden;
                    txtComision = cGenericas.delCommaPointFromString((String)datasession.get( "txtComisionUSD" ));
                    txtIva = cGenericas.delCommaPointFromString((String)datasession.get( "txtIvaUSD" ));
                    txtTotalCI =  cGenericas.delCommaPointFromString((String)datasession.get("txtMonto1"));
                    txtComision$ = cGenericas.delCommaPointFromString((String)datasession.get( "txtComision" ));
                    txtIva$ = cGenericas.delCommaPointFromString((String)datasession.get( "txtIVA" ));
                    long tot =  Long.parseLong(txtComision$) + Long.parseLong(txtIva$);
                    txtTot$ = String.valueOf(tot);
                }
                String txtBeneficiario = (String)datasession.get( "txtBeneficiario" );
                txtNomCliente = (String)datasession.get( "txtCliente" );
                if ( txtDDACuenta2 == null )
                    txtDDACuenta2 = (String)datasession.get( "txtCDACuenta" );
                
                long Monto1 = Long.parseLong(txtComision) + Long.parseLong(txtIva) +
                Long.parseLong(txtTotalCI);
                
                Long TotalCI = new Long(Monto1);
                String txtTotal = TotalCI.toString();
                txtCadImpresion = txtCadImpresion + "~COMPRAVENTA~" + TE +"-" + TO +"~" + txtNomCliente + "~" +
                txtMonto + "~" + txtTipoCambio + "~" + txtMonto1 + "~" + txtSerial + "~" +
                txtDDACuenta1 + "~" + txtDDACuenta2 + "~" + "" + "~" + txtBeneficiario + "~" +
                txtComision + "~" + txtIva  + "~" + txtTotal + "~" + Currency + "~" + Divisa + "~" + Divisa2 + "~";
                if ( TO.equals("0786"))
                    txtCadImpresion = txtCadImpresion + txtComision$ + "~" + txtIva$ + "~" + txtTot$ + "~";
            }
        }
        
        if(txtCasoEsp.length() > 0) {
            session.setAttribute("txtCasoEsp", txtCasoEsp);
        }
        session.setAttribute("txtCadImpresion", sMessage);
        if(txtCadImpresion.length() > 0){
            session.setAttribute("txtCadImpresion", txtCadImpresion);
        }
        session.setAttribute("page.txnresponse", sMessage);
        getServletContext().getRequestDispatcher("/servlet/ventanilla.Redirector").forward(req,resp);
    }
    
    public void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doPost(req, resp);
    }
}
