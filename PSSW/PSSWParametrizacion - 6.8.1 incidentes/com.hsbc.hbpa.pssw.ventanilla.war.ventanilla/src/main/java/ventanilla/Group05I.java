//*************************************************************************************************
//             Funcion: Clase que realiza txn Inversiones
//            Elemento: Group05I.java
//          Creado por: Alejandro Gonzalez Castro
//      Modificado por: Alejandro Gonzalez Castro
//*************************************************************************************************
// CCN - 4360189 - 03/09/2004 - Se cambia invoke al Diario para Control de Efectivo
// CCN - 4360211 - 08/10/2004 - Se elimina mensaje de documentos pendientes por digitalizar
//*************************************************************************************************
package ventanilla;

import java.io.IOException;
import java.util.Hashtable;
import java.util.Stack;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import ventanilla.com.bital.sfb.InversionesB;
import ventanilla.com.bital.admin.Diario;
import ventanilla.com.bital.util.VolanteoDDA;

public class Group05I extends HttpServlet {

    private String delCommaPointFromString(String newCadNum) {
        String nCad = new String(newCadNum);
        if( nCad.indexOf(".") > -1) {
            nCad = nCad.substring(nCad.indexOf(".") + 1, nCad.length());
            if(nCad.length() != 2) {
                String szTemp = new String("");
                for(int j = nCad.length(); j < 2; j++)
                    szTemp = szTemp + "0";
                newCadNum = newCadNum + szTemp;
            }
        }

        StringBuffer nCadNum = new StringBuffer(newCadNum);
        for(int i = 0; i < nCadNum.length(); i++)
            if(nCadNum.charAt(i) == ',' || nCadNum.charAt(i) == '.')
                nCadNum.deleteCharAt(i);

        return nCadNum.toString();
    }

    private String getString(String newCadNum, int newOption) {
        int iPIndex = newCadNum.indexOf("CONSECUTIVO:") + 12, NumSaltos, Num = 0, Ban = 0;
        String nCadNum = new String("");
        NumSaltos = newOption;
        while(Num < NumSaltos) {
            if(newCadNum.charAt(iPIndex) == 0x20) {
                while(newCadNum.charAt(iPIndex) == 0x20)
                    iPIndex++;
                Num++;
            }
            else
                while(newCadNum.charAt(iPIndex) != 0x20)
                    iPIndex++;
        }
        while(newCadNum.charAt(iPIndex) != 0x20 && iPIndex < newCadNum.length()){
            nCadNum = nCadNum + newCadNum.charAt(iPIndex);
            if(iPIndex < newCadNum.length())
                iPIndex++;
            if(iPIndex == newCadNum.length())
                break;
        }

        return nCadNum;
    }

    public void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {
        InversionesB oFormatoB = new InversionesB();
        VolanteoDDA oVolanteoDDA = null;
        String txtVolante = "NO";
		GenericClasses gc = new GenericClasses();

        resp.setContentType("text/plain");

        HttpSession session = req.getSession(false);
        Hashtable datasession = (Hashtable)session.getAttribute("page.datasession");

        Hashtable Truncamiento = (Hashtable)session.getAttribute("truncamiento");
        long TruncamientoMN = 500000L;
        long TruncamientoUSD = 50000L;
        if (!Truncamiento.isEmpty() ){
            TruncamientoMN = Long.parseLong(Truncamiento.get("TruncamientoMN").toString());
            TruncamientoUSD = Long.parseLong(Truncamiento.get("TruncamientoUSD").toString());
        }

        String dirip = (String)session.getAttribute("dirip");

        String getMsg = "";

        String txtTxnCode    = (String)datasession.get("cTxn");
        String txtFormat     = "B";
        String txtBranch     = (String)datasession.get("sucursal");
        String txtTeller     = (String)datasession.get("teller");//"000108";
        String txtSupervisor = (String)datasession.get("teller");//"000101";
        String txtBackOut    = "0";
        String txtOverride   = "0";
        String txtCurrentDay = "0";

        if ((String)datasession.get("supervisor")!= null)
            txtSupervisor = (String)datasession.get("supervisor");

        oFormatoB.setTxnCode(txtTxnCode);
        oFormatoB.setFormat(txtFormat);
        oFormatoB.setBranch(txtBranch);
        oFormatoB.setTeller(txtTeller);
        oFormatoB.setSupervisor(txtSupervisor);
        oFormatoB.setBackOut(txtBackOut);
        oFormatoB.setOverride(txtOverride);
        oFormatoB.setCurrentDay(txtCurrentDay);
        String txtMonto      = delCommaPointFromString((String)datasession.get("txtMontoCheque"));
        String txtMoneda     = (String)datasession.get("moneda");
        String txtEfectivo   = "000";
        String txtCodSeg  = (String)datasession.get("txtCodSeg");
        String txtCveTran = (String)datasession.get("txtCveTran");
        String txtSerial  = (String)datasession.get("txtSerial2");
        String txtDepFirm = (String)datasession.get("rdoACorte");
        String txtDDACuenta = (String)datasession.get("txtDDACuenta2");
        String txtCDACuenta = (String)datasession.get("txtCDACuenta");
        oFormatoB.setIntern(txtCDACuenta.substring(0,10));
        oFormatoB.setIntWh(txtCDACuenta.substring(10,15));
        if ( txtDepFirm.equals("01"))                           // Antes del Corte
            txtDepFirm  = "1";
        else
            txtDepFirm  = "2";                                     // Despues del Corte

		txtMoneda=gc.getDivisa(txtMoneda);

        String txtMontoTot   = (String)datasession.get("MontoTotalC");
        long MontoCobro     = Long.parseLong(delCommaPointFromString((String)datasession.get("txtMontoCobro")));
        long MontoTotalC      = 0;

        if( txtMontoTot != null )
            MontoTotalC = Long.parseLong(txtMontoTot);

        oFormatoB.setAcctNo( txtTxnCode + txtCodSeg + txtCveTran + txtDDACuenta.substring(0,4));
        oFormatoB.setReferenc(txtDDACuenta.substring(4,11) + txtSerial + "T" + txtDepFirm);
        oFormatoB.setTranAmt(txtMonto);                   //Monto Total de la Txn.
        oFormatoB.setFromCurr(txtMoneda);                  //Tipo de Moneda con que se opera la Txn.

        oVolanteoDDA = new VolanteoDDA();
        String txtBanco = (String)datasession.get("txtCveTran");
        String Currency = (String)datasession.get("moneda");
        txtBanco = txtBanco.substring(5,8);
        oVolanteoDDA.setTeller(datasession.get("teller").toString().substring(4,6));
        oVolanteoDDA.setBranch(datasession.get("sucursal").toString().substring(1));
        oVolanteoDDA.setBank(txtBanco);
        oVolanteoDDA.setCurrency((String)datasession.get("moneda"));
        oVolanteoDDA.setCorte((String)datasession.get("rdoACorte"));
        oVolanteoDDA.setChequeData((String)datasession.get("txtSerial2") +(String)datasession.get("txtDDACuenta2") + (String)datasession.get("txtCveTran") +
        (String)datasession.get("txtCodSeg"));
        oVolanteoDDA.setChequeAmount(txtMonto);

        if ( ( Long.parseLong(txtMonto) >= TruncamientoMN  && Currency.equals("01")) ||
        ( Long.parseLong(txtMonto) >= TruncamientoUSD  && Currency.equals("02")) ) {
            txtVolante = "SI";
            oFormatoB.setFeeAmoun(oVolanteoDDA.getConsecutive());
        }

        Diario diario = new Diario();
        String sMessage = diario.invokeDiario(oFormatoB, (String)session.getAttribute("d_ConsecLiga"), dirip, txtMoneda, session);

        if ( sMessage.charAt(0) == '0' )
        {
            MontoTotalC = MontoTotalC + Long.parseLong(txtMonto);
            datasession.put("MontoTotalC",new Long(MontoTotalC).toString());
            oVolanteoDDA.updateConsecutive(getString(oFormatoB.getMessage(),1), txtVolante);
        }
        
        Stack flujotxn = (Stack)session.getAttribute("page.flujotxn");
        if ( MontoTotalC != MontoCobro ) {
            flujotxn.push("0542");
            session.setAttribute("page.flujotxn", flujotxn);
            session.setAttribute("page.txnresponse", sMessage);
            getServletContext().getRequestDispatcher("/servlet/ventanilla.Redirector").forward(req,resp);
        }
        else {

            String txtMontoRemesa   = (String)datasession.get("txtMontoRemesa");
            if ( txtMontoRemesa.equals("0.00")) {                        // No hay descarga de remesas
                flujotxn.pop();
                session.setAttribute("page.flujotxn", flujotxn);
            }
            session.setAttribute("page.txnresponse", sMessage);
            getServletContext().getRequestDispatcher("/servlet/ventanilla.Redirector").forward(req,resp);
        }
    }
    public void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doPost(req, resp);
    }
}
