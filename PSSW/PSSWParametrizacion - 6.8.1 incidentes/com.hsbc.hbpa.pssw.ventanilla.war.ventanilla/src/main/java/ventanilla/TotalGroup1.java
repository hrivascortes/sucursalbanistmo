//*************************************************************************************************
//             Funcion: Servlet que postea txn 0011 y 0013 para consulta de totales
//            Elemento: TotalGroup1.java
//          Creado por: Alejandro Gonzalez Castro
//      Modificado por: Juvenal R Fernandez V
//*************************************************************************************************
// CCN - 4360211 - 01/10/2004 - Se modifica para enviar txn 0011 y 0013
//*************************************************************************************************

package ventanilla;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.util.Hashtable;
import ventanilla.com.bital.sfb.BTotales;
import ventanilla.com.bital.sfb.GerenteD;

public class TotalGroup1 extends HttpServlet {
    
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession(false);
        Hashtable datasession = (Hashtable)session.getAttribute("page.datasession");
        
        String txn = (String)datasession.get("cTxn");
        String branch = (String)datasession.get("sucursal");
        String teller = (String)datasession.get("teller");
        String moneda = (String)datasession.get("moneda");
		GenericClasses gc = new GenericClasses();
        
        String respuesta = "";
        moneda=gc.getDivisa(moneda);
        
        /*
        if(moneda.equals("N$")){
        	moneda="$";
        }
       */
        if (txn.equals("0013"))
	{
            GerenteD gerented = new GerenteD();

            gerented.setBranch(branch);
            gerented.setTxnCode(txn);
            gerented.setTeller(teller);
            gerented.setSupervisor(teller);
            gerented.setCurrCod1(moneda);
            gerented.execute();
            respuesta = gerented.getMessage();
        }
        else if(txn.equals("0011"))
        {
            BTotales total = new BTotales();
            total.setTxnCode(txn);
            total.setBranch(branch);
            total.setTeller(teller);
            total.setSupervisor(teller);
            total.setTrNo(moneda);
            total.execute();
            respuesta = total.getMessage();
        }
        
        session.setAttribute("page.txnresponse", respuesta );
        getServletContext().getRequestDispatcher("/servlet/ventanilla.Redirector").forward(request,response);      
    }
    
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}