//*************************************************************************************************
//             Funcion: Clase que realiza txn 0108
//            Elemento: CV_0108_XXXX.java
//          Creado por: Alejandro Gonzalez Castro
//		Modificado por: Jes�s Emmanuel L�pez Rosales
//*************************************************************************************************
// CCN - 4360189 - 03/09/2004 - Se cambia invoke al Diario para Control de Efectivo
// CCN - 4360211 - 08/10/2004 - Se elimina mensaje de Documentos pendientes por Digitalizar
// CCN - 4360268 - 04/02/2005 - Se realizan adecuaciones para eliminar Agencia NY
// CCN - 4360368 - 02/09/2005 - Se elimina sleep
// CCN - 4360374 - 08/09/2005 - Se genera nuevo paquete a peticion de QA.
// CCN - 4360532 - 20/10/2006 - Se realizan modificaciones para enviar el numero de cajero, consecutivo txn entrada y salida en txn 0821
//*************************************************************************************************
package ventanilla;

import java.io.IOException;
import java.util.Hashtable;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import ventanilla.com.bital.admin.Diario;
import ventanilla.com.bital.sfb.CompraVenta;
import ventanilla.com.bital.util.VolanteoDDA;

public class CV_0108_XXXX extends HttpServlet
{
    public void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {
        String TE, TO, Currency;
        int Resultado = -1;
        CompraVenta oTxn = new CompraVenta();
        Diario diario;

        HttpSession session = req.getSession(false);
        Hashtable datasession = (Hashtable)session.getAttribute("page.datasession");
        TE = (String)datasession.get("cTxn");
        TO = (String)datasession.get("oTxn");
        Currency = (String)datasession.get("moneda");
        String txtPlaza = (String)datasession.get("txtPlaza");
        String dirip =  (String)session.getAttribute("dirip");
        String msg = null;
        String sMessage = null;
        GenericClasses GC = new GenericClasses();

        String txtCasoEsp = new String("");
        String txtCadImpresion = new String("");
        String txtSupervisor = (String)datasession.get("teller");
        String txtRegistro = (String)datasession.get("txtRegistro");
        String txtOverride = "0";
        String txtNomCliente = new String("");
        String txtDepFirm = "";
        String Currency2 = "";
        String Monto = "";
        String ConsecutivoIN = "";
        String ConsecutivoOUT = "";        
        Hashtable Truncamiento = (Hashtable)session.getAttribute("truncamiento");
        long TruncamientoMN = 500000L;
        long TruncamientoUSD = 50000L;
        if (!Truncamiento.isEmpty() ){
            TruncamientoMN = Long.parseLong(Truncamiento.get("TruncamientoMN").toString());
            TruncamientoUSD = Long.parseLong(Truncamiento.get("TruncamientoUSD").toString());
        }
        VolanteoDDA oVolanteoDDA = null;
        String txtVolante = "NO";

        if ( Currency.equals("01"))
            Currency2 = "02";
        else
            Currency2 = "01";

        if ( (String)datasession.get("supervisor") != null )       //pidio autorizacisn de supervisor
            txtSupervisor =  (String)datasession.get("supervisor");

        if ( datasession.get("posteo") == null ) {
            datasession.put("posteo","1");
            session.setAttribute("page.datasession",(Hashtable)datasession);
        }
        
        if ( session.getAttribute("txtCasoEsp") != null )
            txtCasoEsp = session.getAttribute("txtCasoEsp").toString();
        
        if ( (String)datasession.get("override") != null )
            if (datasession.get("override").toString().equals("SI"))
                  txtOverride = "3";
        
        oTxn.setTxnCode(TE);
        oTxn.setBranch((String)datasession.get("sucursal"));
        oTxn.setTeller((String)datasession.get("teller"));
        oTxn.setSupervisor(txtSupervisor);
        oTxn.setOverride(txtOverride);

        if(TE.equals("0108"))
        {
            //0108B 00001000131  000131  000**010800085110102620239*60000*09042502161731T1******N$**
            if ( datasession.get("posteo").toString().equals("1")){
                oTxn.setFormat( "B" );
                oTxn.setTxnCode(TE);
                if(Currency.equals("02")) {
                    oTxn.setFromCurr("US$");
                    oTxn.setTranAmt( GC.delCommaPointFromString((String)datasession.get( "txtMonto1" )) );
                }
                else {
                    oTxn.setFromCurr("N$");
                    oTxn.setTranAmt( GC.delCommaPointFromString((String)datasession.get( "txtMonto" )) );
                }
                String txtCodSeg  = (String)datasession.get("txtCodSeg");
                String txtCveTran = (String)datasession.get("txtCveTran");
                String txtSerial  = (String)datasession.get("txtSerial2");
                txtDepFirm = (String)datasession.get("rdoACorte");
                String txtDDACuenta = (String)datasession.get("txtDDACuenta2");
                
                if ( txtDepFirm.equals("01"))                           // Antes del Corte
                    txtDepFirm  = "1";
                else
                    txtDepFirm  = "2";                                     // Despues del Corte
                
                oTxn.setAcctNo( TE + txtCodSeg + txtCveTran + txtDDACuenta.substring(0,4));
                oTxn.setReferenc(txtDDACuenta.substring(4,11) + txtSerial + "T" + txtDepFirm);
                oTxn.setInter(datasession.get("txtDDACuenta").toString());
                String txtTipoC = GC.StringFiller((String)datasession.get( "txtTipoCambio" ), ((String)datasession.get( "txtTipoCambio" )).length()+3, false, "0");
                txtTipoC = "0000" + txtTipoC;
                txtTipoC = txtTipoC.substring(txtTipoC.length()-8);
                oTxn.setWH(txtTipoC);
                
                oVolanteoDDA = new VolanteoDDA();
                String txtBanco = (String)datasession.get("txtCveTran");
                txtBanco = txtBanco.substring(5,8);
                oVolanteoDDA.setTeller(datasession.get("teller").toString().substring(4,6));
                oVolanteoDDA.setBranch(datasession.get("sucursal").toString().substring(1));
                oVolanteoDDA.setBank(txtBanco);
                oVolanteoDDA.setCurrency((String)datasession.get("moneda"));
                oVolanteoDDA.setCorte((String)datasession.get("rdoACorte"));
                oVolanteoDDA.setChequeData((String)datasession.get("txtSerial2") +(String)datasession.get("txtDDACuenta2") + (String)datasession.get("txtCveTran") +
                (String)datasession.get("txtCodSeg"));
                if ( Currency.equals("01"))
                    Monto = GC.delCommaPointFromString((String)datasession.get("txtMonto"));
                else
                    Monto = GC.delCommaPointFromString((String)datasession.get("txtMonto1"));
                
                oVolanteoDDA.setChequeAmount(Monto);
                
                if ( ( Long.parseLong(Monto.toString()) >= TruncamientoMN  && Currency.equals("01")) ||
                ( Long.parseLong(Monto.toString()) >= TruncamientoUSD  && Currency.equals("02")) ) {
                    txtVolante = "SI";
                    oTxn.setFees(oVolanteoDDA.getConsecutive());
                }
                
                diario = new Diario();
                sMessage = diario.invokeDiario(oTxn, (String)session.getAttribute("d_ConsecLiga"), dirip, Currency, session);
                
                Resultado = oTxn.getStatus();
                
                if(sMessage.substring(0,1).equals("0")) {
                    txtCasoEsp = txtCasoEsp + TE + "~" + GC.getString(oTxn.getMessage(),1) + "~";
                    ConsecutivoIN = GC.getString(oTxn.getMessage(),1);
                    if (ConsecutivoIN.length()<6){
                    	ConsecutivoIN = "0" + ConsecutivoIN; 
                    }                    
                    oVolanteoDDA.updateConsecutive(GC.getString(sMessage,1), txtVolante);
                }
            }
            else
                Resultado = 0;
        }

        if ( TE.equals("0108") && Resultado == 0 )
        {   // Cargo fue aceptado
            
            if ( datasession.get("posteo").toString().equals("1")) {
                 datasession.put("override","NO");
                 datasession.put("posteo","2");
                 txtOverride = "0";
            }


            oTxn = new CompraVenta();
            oTxn.setBranch((String)datasession.get("sucursal"));
            oTxn.setTeller((String)datasession.get("teller"));
            oTxn.setSupervisor(txtSupervisor);
            oTxn.setOverride(txtOverride);
            //ODCT 4117A 00001000131  000131  000**7000001321**6000*US$*0002161731*026*000**00100100000*60000**ABO.P/CHQ.C.I.RECIB.C/V  US$*2***************2********************0108**
            oTxn.setTxnCode(TO);
            oTxn.setFormat( "A" );
            oTxn.setAcctNo( (String)datasession.get( "txtDDACuenta" ) );
            oTxn.setCheckNo((String)datasession.get("txtSerial2"));
            String txtCveTran = (String)datasession.get("txtCveTran");
            oTxn.setTrNo(txtCveTran.substring(5,8));                  // Banco
            oTxn.setTranAmt( GC.delCommaPointFromString((String)datasession.get( "txtMontoC" )) );
            if(Currency.equals("01"))
            {
                oTxn.setTranCur( "US$" );
                oTxn.setMoAmoun( GC.delCommaPointFromString((String)datasession.get( "txtMonto" )) );
                oTxn.setTranDesc( "ABO.P/CHQ.C.I.RECIB.C/V  US$" );
            }
            else
            {
                oTxn.setTranCur( "N$" );
                oTxn.setMoAmoun( GC.delCommaPointFromString((String)datasession.get( "txtMonto1" )) );
                oTxn.setTranDesc( "ABO.P/CHQ.C.I.RECIB.C/V  M.N." );
            }
            oTxn.setDescRev("REV DEPOSITO COBRO INMEDIATO POR C/V");
            oTxn.setCashIn( "000" );
            if ( txtDepFirm.equals("1"))
            {
                oTxn.setHolDays("2");
                oTxn.setHolDays2("2");
            }
            else
            {
                oTxn.setHolDays("4");
                oTxn.setHolDays2("4");
            }
            oTxn.setTrNo5( "0109" );
            String txtTipoCambio = GC.StringFiller((String)datasession.get( "txtTipoCambio" ), ((String)datasession.get( "txtTipoCambio" )).length()+3, false, "0"); 
            txtTipoCambio = "0000" + txtTipoCambio;
            txtTipoCambio = txtTipoCambio.substring(txtTipoCambio.length()-8);
            oTxn.setDraftAm(txtPlaza + txtTipoCambio);
            oTxn.setTrNo5( "0108" );

            diario = new Diario();
            sMessage = diario.invokeDiario(oTxn, (String)session.getAttribute("d_ConsecLiga"), dirip, Currency2, session);

            Resultado = oTxn.getStatus();

            if ( Currency2.equals("01"))
                Monto = GC.delCommaPointFromString((String)datasession.get("txtMonto"));
            else
                Monto = GC.delCommaPointFromString((String)datasession.get("txtMonto1"));
            
            if (sMessage.startsWith("0") || sMessage.startsWith("1")){
                txtCasoEsp = txtCasoEsp + TO + "~" + GC.getString(oTxn.getMessage(),1) + "~";
                ConsecutivoOUT = GC.getString(oTxn.getMessage(),1);
                if (ConsecutivoOUT.length()<6){
                    ConsecutivoOUT = "0" + ConsecutivoOUT; 
                }                
            }
            if(sMessage.substring(0,1).equals("0"))
            {
                txtNomCliente = GC.getPositionalString(oTxn.getMessage(),91,32, "NO");
                txtNomCliente = txtNomCliente.replace('*',' ');
            }
        }

        if (  TE.equals("0108") && Resultado == 0)
        {   // Cargo y Abono Aceptado

            oTxn = new CompraVenta();
            oTxn.setBranch((String)datasession.get("sucursal"));
            oTxn.setTeller((String)datasession.get("teller"));
            oTxn.setSupervisor(txtSupervisor);
            oTxn.setTxnCode("0821");
            oTxn.setFormat( "G" );
            oTxn.setToCurr( "US$" );
            oTxn.setGStatus( "6" );
            if(Currency.equals("02"))
                oTxn.setService( "C" );
            else
                oTxn.setService( "V" );
            String txtAutoriCV = (String)datasession.get("txtAutoriCV");
            if ( txtAutoriCV == null )
                txtAutoriCV = "00000";
            oTxn.setCheckNo( "00000" + txtAutoriCV );
            oTxn.setTranAmt( GC.delCommaPointFromString((String)datasession.get( "txtMonto1" )) );
            String txtTipoCambio = GC.StringFiller((String)datasession.get( "txtTipoCambio" ), ((String)datasession.get( "txtTipoCambio" )).length()+2, false, "0") + "00"; 
            txtTipoCambio = "0000" + txtTipoCambio;
            txtTipoCambio = txtTipoCambio.substring(txtTipoCambio.length()-9);
            oTxn.setComAmt(txtTipoCambio);
            oTxn.setIVAAmt( "0" );
            oTxn.setAcctNo( (String)datasession.get( "registro" ) );
            oTxn.setCtaBenef( (String)datasession.get( "txtRegistro" ) );
            int lon = TE.length();
            for(int j=lon;j<10;j++){
               TE = TE + " ";
            }	
            oTxn.setBenefic(TE + ConsecutivoIN + ConsecutivoOUT + (String)datasession.get("teller"));

            diario = new Diario();
            sMessage = diario.invokeDiario(oTxn, (String)session.getAttribute("d_ConsecLiga"), dirip, "US$", session);

            Monto = GC.delCommaPointFromString((String)datasession.get("txtMonto"));
            
            if (sMessage.startsWith("0") || sMessage.startsWith("1"))
                txtCasoEsp = txtCasoEsp + "0821" + "~" + GC.getString(oTxn.getMessage(),1) + "~";


            if(sMessage.substring(0,1).equals("0"))
            {
//                txtCasoEsp = txtCasoEsp + "0821" + "~" + GC.getString(oTxn.getMessage(),1) + "~";
                String txtMonto = GC.delCommaPointFromString((String)datasession.get( "txtMonto" ));
                String txtMonto1 = GC.delCommaPointFromString((String)datasession.get( "txtMonto1" ));
                txtTipoCambio = GC.StringFiller((String)datasession.get( "txtTipoCambio" ), ((String)datasession.get( "txtTipoCambio" )).length()+2, false, "0"); 
                txtTipoCambio = "0000" + txtTipoCambio;
                txtTipoCambio = txtTipoCambio.substring(txtTipoCambio.length()-7);
                txtTipoCambio = txtTipoCambio.substring(0,3) + "." + txtTipoCambio.substring(3,6);
                String txtDDACuenta2 = (String)datasession.get( "txtDDACuenta" );
                String txtSerial = (String)datasession.get( "txtSerial2" );
                String txtCveTran = (String)datasession.get("txtCveTran");
                txtCadImpresion = txtCadImpresion + "~COMPRAVENTA~" + TE +"-" + TO +"~" + txtNomCliente + "~" +
                txtMonto + "~" + txtTipoCambio + "~" + txtMonto1 + "~" + txtSerial + "~" +
                "" + "~" + txtDDACuenta2 + "~" + txtCveTran.substring(5,8) + "~" + "" + "~"  +
                " " + "~" + " " + "~" + " " + "~" + Currency + "~";
            }
        }

        if(txtCasoEsp.length() > 0){
            session.setAttribute("txtCasoEsp", txtCasoEsp);
        }
        session.setAttribute("txtCadImpresion", sMessage);

        if(txtCadImpresion.length() > 0)
        {
            session.setAttribute("txtCadImpresion", txtCadImpresion);
        }

        session.setAttribute("page.txnresponse", sMessage);
        getServletContext().getRequestDispatcher("/servlet/ventanilla.Redirector").forward(req,resp);
    }

    public void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doPost(req, resp);
    }
}
