//*************************************************************************************************
//             Funcion: Clase que realiza txn 4045
//            Elemento: OP_4045.java
//          Creado por: Alejandro Gonzalez Castro
//      Modificado por: Alejandro Gonzalez Castro
//*************************************************************************************************
// CCN - 4360189 - 03/09/2004 - Se cambia invoke al Diario para Control de Efectivo
//*************************************************************************************************
package ventanilla;

import java.io.IOException;
import java.util.Calendar;
import java.util.Hashtable;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import ventanilla.com.bital.admin.Diario;
import ventanilla.com.bital.sfb.OrdenPago;

public class OP_4045 extends HttpServlet {
    private String stringFormat(int option, int sum) {
        Calendar now = Calendar.getInstance();
        String temp = new Integer(now.get(option) + sum).toString();
        if( temp.length() != 2 )
            temp = "0" + temp;

        return temp;
    }

    private String setCommaToString(String newCadNum) {
        int iPIndex = newCadNum.indexOf(","), iLong;
        String szTemp;

        if(iPIndex > 0) {
            newCadNum = new String(newCadNum + "00");
            newCadNum = newCadNum.substring(0, iPIndex + 1) + newCadNum.substring(iPIndex + 1, iPIndex + 3);
        }
        else {
            for(int i = newCadNum.length(); i < 3; i++)
                newCadNum = "0" + newCadNum;
            iLong = newCadNum.length();
            if(iLong == 3)
                szTemp = newCadNum.substring(0, 1);
            else
                szTemp = newCadNum.substring(0, iLong - 2);
            newCadNum = szTemp + "," + newCadNum.substring(iLong - 2);
        }

        return newCadNum;
    }
    private String delCommaPointFromString(String newCadNum) {
        String nCad = new String(newCadNum);
        if( nCad.indexOf(".") > -1) {
            nCad = nCad.substring(nCad.indexOf(".") + 1, nCad.length());
            if(nCad.length() != 2) {
                String szTemp = new String("");
                for(int j = nCad.length(); j < 2; j++)
                    szTemp = szTemp + "0";
                newCadNum = newCadNum + szTemp;
            }
        }

        StringBuffer nCadNum = new StringBuffer(newCadNum);
        for(int i = 0; i < nCadNum.length(); i++)
            if(nCadNum.charAt(i) == ',' || nCadNum.charAt(i) == '.')
                nCadNum.deleteCharAt(i);

        return nCadNum.toString();
    }

    private String getString(String newCadNum, int NumSaltos) {
        int iPIndex = newCadNum.indexOf("CONSECUTIVO:") + 12, Num = 0, Ban = 0;
        String nCadNum = new String("");
/*   NumSaltos = 4;
   if(newOption == 0)
    NumSaltos = 3;*/
        while(Num < NumSaltos) {
            if(newCadNum.charAt(iPIndex) == 0x20) {
                while(newCadNum.charAt(iPIndex) == 0x20)
                    iPIndex++;
                Num++;
            }
            else
                while(newCadNum.charAt(iPIndex) != 0x20)
                    iPIndex++;
        }
        while(newCadNum.charAt(iPIndex) != 0x20) {
            nCadNum = nCadNum + newCadNum.charAt(iPIndex++);
            if(newCadNum.charAt(iPIndex) == 0x20 && newCadNum.charAt(iPIndex + 1) == 0x2A) {
                nCadNum = nCadNum + newCadNum.charAt(iPIndex) + newCadNum.charAt(iPIndex + 1);
                iPIndex += 2;
            }
        }

        return nCadNum;
    }

    public void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {
        String time = stringFormat(Calendar.HOUR_OF_DAY, 0) + stringFormat(Calendar.MINUTE, 0);
        String TI, TE;
        StringBuffer E = new StringBuffer("");

        String txtCasoEsp = new String("");
        String txtCadImpresion = new String("");
        OrdenPago oTxnOP = new OrdenPago();
        Diario diario;
        String sMessage = null;
        HttpSession session = req.getSession(false);
        Hashtable datasession = (Hashtable)session.getAttribute("page.datasession");
        oTxnOP.setOPSucursal((String)datasession.get("sucursal"));
        int noTax = 0;
        String dirip =  (String)session.getAttribute("dirip");
        String noTaxS = new String("");
        TE = (String)datasession.get("cTxn");
        TI = (String)datasession.get("iTxn");
        String liberarOrden = new String("");
        if(datasession.get("liberarOrden") != null)
            liberarOrden = datasession.get("liberarOrden").toString();


        if(TE.equals("SLOP") && !liberarOrden.equals("SI"))
        {
            oTxnOP.setTxnCode("INQ1");
            String newTemp = (String)datasession.get( "txtNumOrden" );
            String lstBanLiq = (String)datasession.get( "lstBanLiq" );
            int NumPos = 37;
            if(lstBanLiq.equals("02"))
                NumPos -= 4;
            int len = newTemp.length();
            for(int i = len; i < NumPos; i++)
                newTemp = newTemp + " ";
            if(lstBanLiq.equals("02"))
                newTemp = newTemp + "0001";
            newTemp = newTemp + "00000000";
            for(int i = 0; i < 14; i++)
                newTemp = newTemp + " ";
            newTemp = newTemp + (String)datasession.get("registro") + " ";
            String newBranch = (String)datasession.get("sucursal");
            newTemp = newTemp + newBranch.substring(1);
            oTxnOP.setData(newTemp);

            // datos requeridos por el Diario
            oTxnOP.setOrden((String)datasession.get("txtNumOrden"));
            oTxnOP.setTeller((String)datasession.get("teller"));
            oTxnOP.setFromCurr("N$");
            oTxnOP.setBranch((String)datasession.get("sucursal"));
            oTxnOP.setTranAmt("0");

            diario = new Diario();
            sMessage = diario.invokeDiario(oTxnOP, (String)session.getAttribute("d_ConsecLiga"), dirip, "N$", session);
        }
        else if(liberarOrden.equals("SI"))
        {
            oTxnOP.setProcessCode("LIB ");
            String newTemp = (String)datasession.get( "txtNumOrden" );
            String lstBanLiq = (String)datasession.get( "lstBanLiq" );
            int NumPos = 37;
            if(lstBanLiq.equals("02"))
                NumPos -= 4;
            int len = newTemp.length();
            for(int i = len; i < NumPos; i++)
                newTemp = newTemp + " ";
            if(lstBanLiq.equals("02"))
                newTemp = newTemp + "0001";
            newTemp = newTemp + (String)datasession.get( "txtOSN" );
            NumPos = 14;
            for(int i = 0; i < NumPos; i++)
                newTemp = newTemp + " ";

            newTemp = newTemp + (String)datasession.get("teller") + "  ";
            String newBranch = (String)datasession.get("sucursal");
            newTemp = newTemp + newBranch.substring(1);
            oTxnOP.setData(newTemp);

            // datos requeridos por el Diario
            oTxnOP.setOrden((String)datasession.get("txtNumOrden"));
            oTxnOP.setTeller((String)datasession.get("teller"));
            oTxnOP.setFromCurr("N$");
            oTxnOP.setBranch((String)datasession.get("sucursal"));
            oTxnOP.setTranAmt("0");

            diario = new Diario();
            sMessage = diario.invokeDiario(oTxnOP, (String)session.getAttribute("d_ConsecLiga"), dirip, "N$", session);

            datasession.put("statusOrden", "LIBERADA");
            session.setAttribute("page.datasession", datasession);
            session.setAttribute("page.txnresponse","0~01~000000~" + "OK" + "~");
        }
        else if(TE.equals("4045"))
        {
            String bBoton = (String)datasession.get( "Cancelar.y" );
            if(bBoton != null)
            {
                oTxnOP.setTxnCode("LIB ");
                String newTemp = (String)datasession.get( "txtNumOrden" );
                String lstBanLiq = (String)datasession.get( "lstBanLiq" );
                int NumPos = 37;
                if(lstBanLiq.equals("02"))
                    NumPos -= 4;
                int len = newTemp.length();
                for(int i = len; i < NumPos; i++)
                    newTemp = newTemp + " ";
                if(lstBanLiq.equals("02"))
                    newTemp = newTemp + "0001";
                newTemp = newTemp + (String)datasession.get( "txtOSN" );
                NumPos = 14;
                for(int i = 0; i < NumPos; i++)
                    newTemp = newTemp + " ";

                newTemp = newTemp + (String)datasession.get("teller") + "  ";
                String newBranch = (String)datasession.get("sucursal");
                newTemp = newTemp + newBranch.substring(1);
                oTxnOP.setData(newTemp);

                // datos requeridos por el Diario
                oTxnOP.setOrden((String)datasession.get("txtNumOrden"));
                oTxnOP.setTeller((String)datasession.get("teller"));
                oTxnOP.setFromCurr("N$");
                oTxnOP.setBranch((String)datasession.get("sucursal"));
                oTxnOP.setTranAmt("0");

                diario = new Diario();
                sMessage = diario.invokeDiario(oTxnOP, (String)session.getAttribute("d_ConsecLiga"), dirip, "N$", session);
            }
            else
            {
                oTxnOP.setTxnCode("SUS ");
                String newTemp = (String)datasession.get( "txtNumOrden" );
                String lstBanLiq = (String)datasession.get( "lstBanLiq" );
                int NumPos = 37;
                if(lstBanLiq.equals("02"))
                    NumPos -= 4;
                int len = newTemp.length();
                for(int i = len; i < NumPos; i++)
                    newTemp = newTemp + " ";
                if(lstBanLiq.equals("02"))
                    newTemp = newTemp + "0001";
                newTemp = newTemp + (String)datasession.get( "txtOSN" );
                newTemp = newTemp + "SUS ";
                newTemp = newTemp + new String(stringFormat(Calendar.YEAR, 0).substring(3) + stringFormat(Calendar.MONTH, 1) + stringFormat(Calendar.DAY_OF_MONTH, 0));
                newTemp = newTemp + new String(stringFormat(Calendar.HOUR_OF_DAY, 0) + stringFormat(Calendar.MINUTE, 0));
                newTemp = newTemp + (String)datasession.get("teller") + "  ";
                String newBranch = (String)datasession.get("sucursal");
                newTemp = newTemp + newBranch.substring(1);
                oTxnOP.setData(newTemp);

                // datos requeridos por el Diario
                oTxnOP.setOrden((String)datasession.get("txtNumOrden"));
                oTxnOP.setTeller((String)datasession.get("teller"));
                oTxnOP.setFromCurr("N$");
                oTxnOP.setBranch((String)datasession.get("sucursal"));
                oTxnOP.setTranAmt(delCommaPointFromString((String)datasession.get( "txtMonto" )));
                oTxnOP.setReversable("S");
                String bank = (String)datasession.get("txtBanco");
                bank = bank.trim();
                if(bank.length() > 0)
                    oTxnOP.setBanco(bank);
                else
                    oTxnOP.setBanco((String)session.getAttribute("identidadApp"));

                diario = new Diario();
                sMessage = diario.invokeDiario(oTxnOP, (String)session.getAttribute("d_ConsecLiga"), dirip, "N$", session);

                if(sMessage.startsWith("0"))
                {
                    txtCasoEsp = "DSLZ" + "~" + getString(oTxnOP.getMessage(),1) + "~";                    String monto = delCommaPointFromString((String)datasession.get( "txtMonto" ));
                }
            }
        }

        if(txtCasoEsp.length() > 0){
            session.setAttribute("txtCasoEsp", txtCasoEsp);
        }
        session.setAttribute("txtCadImpresion", sMessage);
        if(txtCadImpresion.length() > 0){
            session.setAttribute("txtCadImpresion", txtCadImpresion);
       }
        session.setAttribute("page.txnresponse", sMessage);

        getServletContext().getRequestDispatcher("/servlet/ventanilla.Redirector").forward(req,resp);
    }

    public void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doPost(req, resp);
    }
}
