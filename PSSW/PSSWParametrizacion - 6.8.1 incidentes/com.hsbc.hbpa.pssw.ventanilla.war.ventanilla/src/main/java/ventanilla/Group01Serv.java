//*************************************************************************************************
//             Funcion: Clase que realiza txn Servicios
//            Elemento: Group01Serv.java
//          Creado por: Alejandro Gonzalez Castro
//      Modificado por: Alejandro Gonzalez Castro
//*************************************************************************************************
// CCN - 4360189 - 03/09/2004 - Se cambia invoke al Diario para Control de Efectivo
//*************************************************************************************************

package ventanilla;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.util.Hashtable;

import ventanilla.com.bital.sfb.Deposito;
import ventanilla.com.bital.sfb.ServiciosB;
import ventanilla.com.bital.admin.Diario;
 
public class Group01Serv extends HttpServlet
{
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
    {
        HttpSession session = request.getSession(false);
        Hashtable datasession = (Hashtable)session.getAttribute("page.datasession");
		GenericClasses gc = new GenericClasses();
        String txn = (String)datasession.get("cTxn");
        String moneda = gc.getDivisa((String)datasession.get("moneda"));
        String sMessage = "";
        String txtCadImpresion = "";
        String dirip =  (String)session.getAttribute("dirip");
        
        
        ServiciosB servicio = new ServiciosB();
        servicio.setBranch((String)datasession.get("sucursal"));
        servicio.setTeller((String)datasession.get("teller"));
        servicio.setSupervisor((String)datasession.get("teller"));
        
        Deposito deposito = new Deposito();
        deposito.setBranch((String)datasession.get("sucursal"));
        deposito.setTeller((String)datasession.get("teller"));
        deposito.setSupervisor((String)datasession.get("teller"));
        
        if( txn.equals("0857") ) {
            //	  txn = "0857";
            servicio.setFormat("B");
            servicio.setTxnCode(txn);
            servicio.setAcctNo((String)datasession.get("txtAfiliacion")+ "0000");
            servicio.setTranAmt(quitap((String)datasession.get("txtMonto")));
            servicio.setReferenc("1");
            servicio.setFromCurr(moneda);
            servicio.setCashIn(quitap((String)datasession.get("txtEfectivo")));
            Diario diario = new Diario();
            sMessage = diario.invokeDiario(servicio, (String)session.getAttribute("d_ConsecLiga"), dirip, moneda, session);
            txtCadImpresion = "NOIMPRIMIRNOIMPRIMIR";
        }
        if( txn.equals("SS03") || txn.equals("1003")) 
        {
            String txtCasoEsp = "";
            if ( txn.equals("SS03"))
                txtCasoEsp = txtCasoEsp + "1003" + "~";
            txn = "1003";
            String Cuenta = "4004019386";
            deposito.setTxnCode(txn);
            deposito.setAcctNo(Cuenta);
            String MtoChqs = "";
            String Efe = "";
            String Amount = "";

            MtoChqs = quitap((String)datasession.get("txtChqsBital"));
            Efe = quitap((String)datasession.get("txtEfectivo"));
            
            long mtochqs = Long.parseLong(MtoChqs);
            long efe = Long.parseLong(Efe);
            long amount = mtochqs + efe;
            Amount = String.valueOf(amount);
            
            deposito.setCashIn(Efe);
            deposito.setTranAmt(Amount);

            deposito.setTranCur(moneda);
            deposito.setTranDesc((String)datasession.get("txtAfiliacion")+"0000");
            String DescRev = "REV." + datasession.get("txtAfiliacion").toString()+"0000";
            if(DescRev.length() > 40)
                    DescRev = DescRev.substring(0,40);
            deposito.setDescRev(DescRev);
            if ( (String)datasession.get("override") != null )
                if (datasession.get("override").toString().equals("SI"))
                   deposito.setOverride("3");
            if ( (String)datasession.get("supervisor") != null )
                 deposito.setSupervisor((String)datasession.get("supervisor"));
            Diario diario = new Diario();
            sMessage = diario.invokeDiario(deposito, (String)session.getAttribute("d_ConsecLiga"), dirip, moneda, session);
            if(sMessage.charAt(0) !='0') {
              txtCadImpresion  = ""; // Para Certificacisn
              session.setAttribute("txtAfore", txtCadImpresion);
            }
            else {
                if (txtCasoEsp.length() > 0){
                    txtCasoEsp = txtCasoEsp + getString(sMessage,1) + "~";
                    session.setAttribute("txtCasoEsp", txtCasoEsp);
                }
            }
        }
        session.setAttribute("page.txnresponse", sMessage);
        getServletContext().getRequestDispatcher("/servlet/ventanilla.Redirector").forward(request,response);
        
    }
    
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
    
    private String quitap(String Valor) {
        StringBuffer Cantidad = new StringBuffer(Valor);
        for(int i=0; i<Cantidad.length();) {
            if( Cantidad.charAt(i) == ',' || Cantidad.charAt(i) == '.' )
                Cantidad.deleteCharAt(i);
            else
                ++i;
        }
        return Cantidad.toString();
    }
    
    public String getmoneda(String cmoneda) {
        String newmoneda = "";
        if (cmoneda.equals("N$"))
        { newmoneda = "01";}
        else if (cmoneda.equals("US$"))
        { newmoneda = "02";}
        else
        { newmoneda = "03";}
        return newmoneda;
    }
    
    public String removeAsterisk(String campo) {
        StringBuffer result = new StringBuffer(campo);
        String caracter = result.substring(result.length()-1,result.length());
        if (caracter.equals("*"))
            result.deleteCharAt(result.length()-1);
        return result.toString();
    }
    
   
    private String getString(String newCadNum, int newOption) {
        int iPIndex = newCadNum.indexOf("CONSECUTIVO:") + 12, NumSaltos, Num = 0, Ban = 0;
        String nCadNum = new String("");
        NumSaltos = newOption;
        while(Num < NumSaltos) {
            if(newCadNum.charAt(iPIndex) == 0x20) {
                while(newCadNum.charAt(iPIndex) == 0x20)
                    iPIndex++;
                Num++;
            }
            else
                while(newCadNum.charAt(iPIndex) != 0x20)
                    iPIndex++;
        }
        while(newCadNum.charAt(iPIndex) != 0x20 && iPIndex < newCadNum.length()){
            nCadNum = nCadNum + newCadNum.charAt(iPIndex);
            if(iPIndex < newCadNum.length())
                iPIndex++;
            if(iPIndex == newCadNum.length())
                break;
        }
        
        if ( nCadNum.endsWith("~"))
            nCadNum = nCadNum.substring(0,nCadNum.length()-1);
        return nCadNum;
    }   
}