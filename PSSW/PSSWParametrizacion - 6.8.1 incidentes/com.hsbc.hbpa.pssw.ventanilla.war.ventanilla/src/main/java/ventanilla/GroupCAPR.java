//*************************************************************************************************
//             Funcion: Clase que arma la transaccion para la Txn CAPR Pago en linea y simular ir a Hogan
//            Elemento: GroupCAPR.java
//*************************************************************************************************
// 
//*************************************************************************************************
package ventanilla;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.Hashtable;
import ventanilla.com.bital.sfb.RAPG;
import ventanilla.com.bital.admin.Diario;

public class GroupCAPR extends HttpServlet
{
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        response.setContentType("text/plain");
        HttpSession session = request.getSession(false);
        Hashtable datasession = (Hashtable)session.getAttribute("page.datasession");
        String dirip = (String)session.getAttribute("dirip");
    	String sMessage=null;             	
        //Arma el msg para el posting a WedAdmon
        RAPG rapg = new RAPG();
        rapg.setBranch((String)datasession.get("sucursal"));
        rapg.setTeller((String)datasession.get("teller"));
        rapg.setSupervisor((String)datasession.get("supervisor"));
        rapg.setTxnCode("CAPR");
        rapg.setTxnId("ODDT");
        rapg.setFormat("G");
        String moneda = (String)datasession.get("moneda");
      
        if(moneda.equals("01")){
            rapg.setToCurr("N$");
        }else if( moneda.equals("02") ){
            rapg.setToCurr("US$");
        }else{
            rapg.setToCurr("UDI");
        }
        rapg.setTranAmt("1");
        rapg.setAcctNo("1");
        
        
        String txtSer=(String)datasession.get("servicio1");
        
        ///Ref multiples
        	String txtReferencia="";
        	String temp;
	  	for(int refe=1; refe < 4; refe ++){
	  		temp="ref"+refe+"s1p1";
	  		if((String)datasession.get(temp) != null){
	  		txtReferencia = txtReferencia + (String)datasession.get(temp) + "*";
	  		}
	  	}
	  	
	  	datasession.put("txtReferencia", txtReferencia);
	  	datasession.put("txtServicio", txtSer);
        
        ///
              
        
        
        int size = txtSer.length();
        int i=0;
	  	for(i = size; i < 7; i++)
		   {
		   txtSer = "0" + txtSer;
		   }
	  	
	    rapg.setBenefic(txtSer);
	    if(txtReferencia.length()>39){
		    String referTmp = txtReferencia.replace("*", "");
	        rapg.setOrdenan(referTmp);
		}
		else{
		    rapg.setOrdenan(txtReferencia);
		 }
 
        Diario diario = new Diario();
        sMessage = diario.invokeDiario(rapg, (String)session.getAttribute("d_ConsecLiga"), dirip, moneda, session);
	        
        if(sMessage.startsWith("0"))
        {
            int x = sMessage.indexOf("MONTO:");
            String monto = sMessage.substring(x+6, sMessage.length()-1);
            monto = monto.trim();
            sMessage = sMessage.substring(0,x);
            datasession.put("txtMonto", monto);
            session.setAttribute("page.datasession", datasession);        
        }else{        
            datasession.remove("flujorap");
            datasession.remove("flujorapcapr");
        }
	    session.setAttribute("page.txnresponse", sMessage);
	    getServletContext().getRequestDispatcher("/servlet/ventanilla.Redirector").forward(request,response);
    }
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        doPost(request, response);
    }
}