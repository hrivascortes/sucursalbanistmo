//*************************************************************************************************
//             Funcion: Clase que realiza txn Servicios
//            Elemento: Group19Serv.java
//          Creado por: Alejandro Gonzalez Castro
//      Modificado por: Alejandro Gonzalez Castro
//*************************************************************************************************
// CCN - 4360189 - 03/09/2004 - Se cambia invoke al Diario para Control de Efectivo
// CCN - 4360368 - 02/09/2005 - Se elimina sleep
// CCN - 4360374 - 08/09/2005 - Se genera nuevo paquete a peticion de QA.
//*************************************************************************************************
package ventanilla;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.util.Hashtable;
import ventanilla.com.bital.sfb.ServiciosA;
import ventanilla.com.bital.sfb.ServiciosB;
import ventanilla.GenericClasses;

import ventanilla.com.bital.admin.Diario;

public class Group19Serv extends HttpServlet 
{
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
    {
        HttpSession session = request.getSession(false);
        Hashtable datasession = (Hashtable)session.getAttribute("page.datasession");
        String dirip =  (String)session.getAttribute("dirip");
        String conliga = (String)session.getAttribute("d_ConsecLiga");
        String suc = datasession.get("sucursal").toString();
        String cajero = datasession.get("teller").toString();
        String sup = datasession.get("teller").toString();

        ServiciosA sa;
        ServiciosB sb;
        GenericClasses gc = new GenericClasses();

        StringBuffer sbcasoesp = new StringBuffer(20);
        
        String txn = (String)datasession.get("cTxn");
        String txtMoneda     = (String)datasession.get("moneda");
        txtMoneda = gc.getDivisa(txtMoneda);
        Diario diario;
        String sMessage = null;
        String tmp = null;

        String mtotot = datasession.get("txtTotal").toString();
        long tot = Long.parseLong(mtotot);
        mtotot = String.valueOf(tot);
        
        String centropago = datasession.get("txtCentroPago").toString(); 
        String consec = datasession.get("txtConsec0530").toString();
        String bimestre = datasession.get("txtBimPago").toString();
        String tipo = datasession.get("txtTipoPago").toString();            
        String dv = datasession.get("txtDigVerif").toString();            
        String retiro = datasession.get("txtRetiro").toString();
        String vivienda = datasession.get("txtVivienda").toString();

        if (txn.equals("0608")) 
        {
            sb = new ServiciosB();
            sb.setBranch(suc);
            sb.setTeller(cajero);
            sb.setSupervisor(sup);
            sb.setTxnCode(txn);
            sb.setFormat("B");
            sb.setAcctNo(txn);
            sb.setTranAmt(mtotot);
            sb.setReferenc(centropago);
            sb.setCashIn(mtotot);
            sb.setFromCurr(txtMoneda);
            
            diario = new Diario();
            sMessage = diario.invokeDiario(sb, conliga, dirip, txtMoneda, session);
        }
        
        if (txn.equals("5581")) 
        {
            sa = new ServiciosA();
            sa.setBranch(suc);
            sa.setTeller(cajero);
            sa.setSupervisor(sup);
            sa.setTxnCode(txn);
            sa.setAcctNo(datasession.get("txtDDACuenta").toString());
            sa.setTranAmt(mtotot);
            sa.setTranCur(txtMoneda);
            sa.setMoamoun("000");
            sa.setTranDesc("CARGO CTA PAGO DE SAR " + centropago);
            sa.setDescRev("REV CARGO CTA PAGO DE SAR " + centropago);
            
            diario = new Diario();
            sMessage = diario.invokeDiario(sa, conliga, dirip, txtMoneda, session);
        }
            
        if (txn.equals("5583")) 
        {
            sa = new ServiciosA();
            sa.setBranch(suc);
            sa.setTeller(cajero);
            sa.setSupervisor(sup);
            sa.setTxnCode(txn);
            String cta = datasession.get("txtDDACuenta2").toString();
            cta = cta.substring(1);
            sa.setAcctNo(cta);
            sa.setTranAmt(mtotot);
            sa.setTranCur(txtMoneda);
            sa.setCheckNo("000" + datasession.get("txtSerial2").toString());
            sa.setMoamoun("000");
            sa.setFees("8");
            sa.setTranDesc("CARGO CHQ "+(String)session.getAttribute("identidadApp")+" PAGO DE SAR " + centropago);
            sa.setDescRev("REV CARGO CHQ "+(String)session.getAttribute("identidadApp")+" PAGO DE SAR " + centropago);
            String codseg = (String)datasession.get("txtCodSeg");
            String cvetran = (String)datasession.get("txtCveTran");
            sa.setTrNo2(codseg.substring(0,3));
            sa.setTrNo1(cvetran.substring(2,5));
            
            diario = new Diario();
            sMessage = diario.invokeDiario(sa, conliga, dirip, txtMoneda, session);
        }
        
        if(sMessage.startsWith("0"))
        {
            sbcasoesp.append(txn).append("~").append(gc.getString(sMessage,1)).append("~");
            sb = new ServiciosB();
            sb.setBranch(suc);
            sb.setTeller(cajero);
            sb.setSupervisor(sup);
            sb.setTxnCode("0530");
            sb.setFormat("B");
            sb.setAcctNo("0530" + gc.getDate(1));
            sb.setTranAmt(mtotot);
            bimestre = bimestre.substring(2) + bimestre.substring(0,2);
            sb.setReferenc( consec + centropago + tipo + bimestre + dv);
            sb.setCashIn(retiro);
            sb.setCashOut(vivienda);
            sb.setFromCurr(txtMoneda);

            diario = new Diario();
            sMessage = diario.invokeDiario(sb, conliga, dirip, txtMoneda, session);
            
            if(txn.equals("5583"))
                sbcasoesp.append("0530").append("~").append(gc.getString(sMessage,1)).append("~");
            
            if(sMessage.startsWith("0"))
            {
                sa = new ServiciosA();
                sa.setBranch(suc);
                sa.setTeller(cajero);
                sa.setSupervisor(sup);
                sa.setTxnCode("0534");
                sa.setFormat("F");
                sa.setAcctNo("0534");
                long ret = Long.parseLong(retiro);
                tmp = String.valueOf(ret);
                sa.setTranAmt(tmp);
                sa.setTranCur(txtMoneda);
                sa.setMoamoun("000");
                sa.setTranDesc(centropago);
                

                diario = new Diario();
                sMessage = diario.invokeDiario(sa, conliga, dirip, txtMoneda, session);
            
                if(sMessage.startsWith("0")) 
                {
                    sa = new ServiciosA();
                    sa.setBranch(suc);
                    sa.setTeller(cajero);
                    sa.setSupervisor(sup);
                    sa.setTxnCode("0536");
                    sa.setFormat("F");
                    sa.setAcctNo("0536");
                    long viv = Long.parseLong(vivienda);
                    tmp = String.valueOf(viv);
                    sa.setTranAmt(tmp);
                    sa.setTranCur(txtMoneda);
                    sa.setMoamoun("000");
                    sa.setTranDesc(centropago);
                    
                    diario = new Diario();
                    sMessage = diario.invokeDiario(sa, conliga, dirip, txtMoneda, session);
                    
                }

            }
            
        }

        session.setAttribute("txtCasoEsp", sbcasoesp.toString());
        session.setAttribute("page.txnresponse", sMessage);
        getServletContext().getRequestDispatcher("/servlet/ventanilla.Redirector").forward(request,response);
    }
    
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
    {
        doPost(request, response);
    }
}
