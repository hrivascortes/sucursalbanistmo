//************************************************************************************************* 
//             Funcion: Clase que realiza la consulta para relaciones varias
//            Elemento: RelacionesServlet.java
//          Creado por: Juan Carlos Gaona
//      Modificado por: Fausto Rodrigo Flores Moreno
//*************************************************************************************************
// CCN -4360274- 16/02/2005 - Eliminacion de Columnas poco utilizadas en el Diario Electronico
// CCN -4360306- 22/04/2005 - Se modifica el ORDER BY de SQLS para la relacion de remesas
// CCN -4360315- 06/05/2005 - Se corrige problema con la relacion de remesas en dolares
// CCN - 4360334 - 17/06/2005 - Se elimina variable dbQualifier
// CCN - 4360337 - 25/06/2005 - Se modifica SQLs para relaciones de != a <>
// CCN - 4360390 - 07/10/2005 - Se agregan sqls para las relaciones
// CCN - 4360456 - 07/04/2006 - Se realizan correcciones para txn 0800 impresion de ultima txn y se elimina txn 9540
// CCN - 4360532 - 20/10/2006 - Se realizan modificaciones impresión de giros
//*************************************************************************************************

package ventanilla;

import java.io.IOException;
import java.util.Hashtable;
import java.util.Vector;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import ventanilla.com.bital.admin.OperacionesDBBean;
import ventanilla.com.bital.beans.CamposL;
import ventanilla.com.bital.sfb.FormatoD;
import ventanilla.com.bital.util.NSTokenizer;

import com.bital.util.ConnectionPoolingManager;

public class RelacionesServlet extends HttpServlet {
    private String stringFormat(int option, int sum) {
        java.util.Calendar now = java.util.Calendar.getInstance();
        String temp = new Integer(now.get(option) + sum).toString();
        if( temp.length() != 2 )
            temp = "0" + temp;
        return temp;
    }
    
    public Vector Campos(String cadena, String delimitador) {
        NSTokenizer parser = new NSTokenizer(cadena, delimitador);
        Vector campos = new Vector();
        while(parser.hasMoreTokens()) {
            String token = parser.nextToken();
            if( token == null )
                continue;
            campos.addElement(token);
        }
        return campos;
    }
    
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, NullPointerException {
        String strTxn       = " ";
        String strSucursal  = " ";
        String strSucursal2  = " ";
        String strCajero  = " ";
        String strAccion    = " ";
        String strRespuesta = " ";
        String NoError		  = " ";
        String strMoneda  = "01";
		GenericClasses gc = new GenericClasses();
        
        String PostFixUR = ConnectionPoolingManager.getPostFixUR();
        HttpSession sessions = request.getSession(true);
        Hashtable requestParameters = new Hashtable();
        OperacionesDBBean rbQuery = new OperacionesDBBean();
        if (sessions.getAttribute("branch")!= null)
            strSucursal = (String) sessions.getAttribute("branch");
     
        strSucursal2 = strSucursal.substring(1);
        if (sessions.getAttribute("teller")!= null)
            strCajero = (String) sessions.getAttribute("teller");
        if(request.getParameter("cTxn") != null)
            strTxn = new String((String)request.getParameter("cTxn").toString());
        if(request.getParameter("moneda") != null)
            strMoneda = new String((String)request.getParameter("moneda").toString());
        String strDate = new String(stringFormat(java.util.Calendar.YEAR, 0) + stringFormat(java.util.Calendar.MONTH, 1) + stringFormat(java.util.Calendar.DAY_OF_MONTH, 0));
        if(strTxn.trim().equals("0250")) {
            String strDivisa = "";
			strDivisa = gc.getDivisa(strMoneda);
            FormatoD fdTxn = new FormatoD();
            fdTxn.setTxnCode(strTxn);
            fdTxn.setFormat("D");
            fdTxn.setBranch(strSucursal);
            fdTxn.setTeller(strCajero);
            fdTxn.setSupervisor(strCajero);
            fdTxn.setCurrCod1(strDivisa);
            fdTxn.execute();
            String MsgResp = fdTxn.getMessage();
            int y = 0;
            int count = 0;
            StringBuffer RespSB = new StringBuffer(MsgResp);
            for(y = 0; y < RespSB.length(); y++)
                if(RespSB.charAt(y) == 0x00) {
                    count ++;
                    RespSB.replace(y,y+1," ");
                }
            MsgResp = RespSB.toString();
            if ( !MsgResp.startsWith("0") && !MsgResp.startsWith("1"))
                MsgResp = "1~01~0101010~ERROR DE ENLACE A LA APLICACION CICS~";
            
            strRespuesta = "0250" + "~" + MsgResp;
        }
        else if(strTxn.trim().equals("0800")) {
            String strTmp = new String("");
            if (strDate.length() > 8)
                strTmp = strDate.substring(1,9);
            strAccion = "SELECT D_CONSECUTIVO, D_TXN FROM TW_DIARIO_ELECTRON WHERE C_TXN_VISIBLE = 'S' AND D_ESTATUS <> '?' AND D_OPERADOR ='" + strCajero + "' AND D_FECHOPER='" + strTmp + "' AND D_TXN <> 'CON' AND D_TXN <> '0081' AND D_TXN <> '0082' AND D_TXN <> '0083' ORDER BY D_CONSECUTIVO DESC " + PostFixUR;
            strRespuesta = rbQuery.EjecutaQuery(strAccion);
            if (strRespuesta.length() < 6 )
                NoError = "No se han realizado transacciones ";
            else {
                sessions.setAttribute("Consecutivo", strRespuesta.substring(0,7));
                sessions.setAttribute("page.cTxn", strRespuesta.substring(8,12));
            }
        }
        else if(strTxn.trim().equals("N082")) {
            strAccion = "SELECT C_BILLETE, D_DEPOSITANTE, D_MONTO, D_STATUS FROM TW_NAFINSA WHERE C_MONEDA = '" + strMoneda + "' AND C_NUM_SUCURSAL = '" + strSucursal2  + "'"  + PostFixUR;            
            strRespuesta = strTxn.trim() + "~" + strMoneda + "~" + rbQuery.EjecutaQuery(strAccion);
            if (strRespuesta.length() < 10 )
                NoError = "No se han realizado transacciones ";
        }
		else if(strTxn.trim().equals("G062")) { //CONSULTA PARA GIROS 26/06/2006
            String respSQL = "";
            strAccion = "SELECT SUM(INTEGER(D_MONTO)) FROM TW_GIROS WHERE C_MONEDA = '" + strMoneda + "' AND C_NUM_SUCURSAL = '" + strSucursal2  + "'"  + PostFixUR;                        
            respSQL = rbQuery.EjecutaQuery(strAccion);
            strAccion = "SELECT C_GIRO, D_BENEFICIARIO, D_MONTO, D_BANCO, D_STATUS, D_PLAZA FROM TW_GIROS WHERE C_MONEDA = '" + strMoneda + "' AND C_NUM_SUCURSAL = '" + strSucursal2  + "'"  + PostFixUR;
            strRespuesta = strTxn.trim() + "~" + strMoneda + "~" + respSQL + rbQuery.EjecutaQuery(strAccion);
            if (strRespuesta.length() < 10 )
                NoError = "No se han realizado transacciones ";
		}
        else if(strTxn.trim().equals("9520")) {
            /*strAccion = "SELECT C_BILLETE, D_DEPOSITANTE, D_MONTO FROM TW_NAFINSA WHERE D_STATUS = 'E' AND C_MONEDA = '" + strMoneda + "'" + statementPS;
            strRespuesta = strTxn.trim() + "~" + strMoneda + "~" + rbQuery.EjecutaQuery(strAccion);
            if (strRespuesta.length() < 10 )*/
            NoError = "Relacion No disponible";
        }
        else if(strTxn.trim().equals("9540")) {
            /*strAccion = "SELECT D_CONSECUTIVO, D_CUENTA, D_DESCRIP, D_EFECTIVO, D_MONTO, D_OPERADOR  FROM TW_DIARIO_ELECTRON WHERE D_ESTATUS = 'A' AND D_TXN = '0540' AND D_SUCURSAL = '" + strSucursal + "' AND D_FECHOPER = '" + strDate.substring(1,strDate.length()) + "'" + PostFixUR;
            strRespuesta = strTxn.trim() + "~" + rbQuery.EjecutaQuery(strAccion);
            if (strRespuesta.length() < 10 )
                NoError = "No se han realizado transacciones ";*/
            NoError = "Relacion No disponible";                
        }
        else if(strTxn.trim().equals("9251")) {
            String strDivisa = "";
            String strBanco = "000";
            String strCeros = "000";
            String strOpcion1 = "1";
			strDivisa = gc.getDivisa(strMoneda);
            if(request.getParameter("opcion1") != null)
                strOpcion1 = new String((String)request.getParameter("opcion1").toString());
            if(request.getParameter("Banco") != null) {
                strBanco = new String((String)request.getParameter("Banco").toString());
                strBanco = strCeros.substring(0,3-strBanco.length()) + strBanco;
            }
            if (strOpcion1.equals("1") && strBanco.equals("000"))
                strAccion = "SELECT D_REFER, D_CONSECUTIVO, D_CUENTA, D_SERIAL, D_MONTO, D_OPERADOR  FROM TW_DIARIO_ELECTRON WHERE D_TXN='4251' AND D_ESTATUS ='A' AND D_DIVISA = '" + strDivisa + "' AND D_SUCURSAL = '" + strSucursal + "' AND D_FECHOPER = '" + strDate.substring(1,strDate.length()) + "'"+ PostFixUR;                
            else if(strOpcion1.equals("1") && !strBanco.equals("000"))
                strAccion = "SELECT D_CONSECUTIVO, D_CUENTA, D_SERIAL, D_MONTO, D_OPERADOR  FROM TW_DIARIO_ELECTRON WHERE D_TXN='4251' AND D_ESTATUS ='A' AND D_DIVISA = '" + strDivisa + "' AND D_SUCURSAL = '" + strSucursal + "' AND D_REFER = '" + strBanco + "' AND D_FECHOPER = '" + strDate.substring(1,strDate.length()) + "'"+ PostFixUR;                
            else if(strOpcion1.equals("2") && !strBanco.equals("000"))
                strAccion = "SELECT D_CONSECUTIVO, D_CUENTA, D_SERIAL, D_MONTO, D_OPERADOR  FROM TW_DIARIO_ELECTRON WHERE D_TXN='4271' AND D_ESTATUS ='A' AND D_DIVISA = '" + strDivisa + "' AND D_SUCURSAL = '" + strSucursal + "' AND D_REFER = '" + strBanco + "' AND D_FECHOPER = '" + strDate.substring(1,strDate.length()) + "'"+ PostFixUR;                
            else if (strOpcion1.equals("2") && strBanco.equals("000"))
                strAccion = "SELECT D_REFER, D_CONSECUTIVO, D_CUENTA, D_SERIAL, D_MONTO, D_OPERADOR  FROM TW_DIARIO_ELECTRON WHERE D_TXN='4251' AND D_ESTATUS ='A' AND D_DIVISA = '" + strDivisa + "' AND D_SUCURSAL = '" + strSucursal + "' AND D_FECHOPER = '" + strDate.substring(1,strDate.length()) + "'"+ PostFixUR;
            strRespuesta = strTxn.trim() + "~" + strMoneda + "~"+ strBanco + "~" + strOpcion1 + "~";
            strRespuesta = strRespuesta + rbQuery.EjecutaQuery(strAccion);
            if (strRespuesta.length() < 15 )
                NoError = "No se han realizado transacciones ";
        }
        else if(strTxn.trim().equals("9503")) {
            String strTmp = new String("");
            String strOpcion1 = "1";
            String strOpcion2 = "1";
            if (strDate.length() > 8)
                strTmp = strDate.substring(1,9);
            if(request.getParameter("opcion1") != null)
                strOpcion1 = new String((String)request.getParameter("opcion1").toString());
            if(request.getParameter("opcion2") != null)
                strOpcion2 = new String((String)request.getParameter("opcion2").toString());
            if ((strOpcion1.equals("1")) && (strOpcion2.equals("1")))
                strAccion = "SELECT D_CONSECUTIVO, D_CUENTA, D_REFER, D_EFECTIVO, D_MONTO, D_OPERADOR, D_MSGENVIO FROM TW_DIARIO_ELECTRON WHERE D_SUCURSAL = '" + strSucursal + "' AND D_ESTATUS ='A' AND D_TXN = '5503' AND D_CUENTA = '448' AND D_MONTO=0 AND D_EFECTIVO = 0 AND D_FECHOPER = '" + strTmp + "' ORDER BY D_REFER, D_CONSECUTIVO ASC " + PostFixUR;
            if ((strOpcion1.equals("1")) && (strOpcion2.equals("2")))
                strAccion = "SELECT D_CONSECUTIVO, D_CUENTA, D_REFER, D_EFECTIVO, D_MONTO, D_OPERADOR, D_MSGENVIO FROM TW_DIARIO_ELECTRON WHERE D_SUCURSAL = '" + strSucursal + "' AND D_ESTATUS ='A' AND D_TXN = '5503' AND D_CUENTA = '448' AND D_MONTO<>0 AND D_FECHOPER = '" + strTmp + "' ORDER BY D_REFER, D_CONSECUTIVO ASC " + PostFixUR;
            if ((strOpcion1.equals("1")) && (strOpcion2.equals("3")))
                strAccion = "SELECT D_CONSECUTIVO, D_CUENTA, D_REFER, D_EFECTIVO, D_MONTO, D_OPERADOR, D_MSGENVIO FROM TW_DIARIO_ELECTRON WHERE D_SUCURSAL = '" + strSucursal + "' AND D_ESTATUS ='A' AND D_TXN = '5503' AND D_CUENTA = '480' AND D_FECHOPER = '" + strTmp + "' ORDER BY D_REFER, D_CONSECUTIVO ASC " + PostFixUR;                
            if ((strOpcion1.equals("2")) && (strOpcion2.equals("1")))
                strAccion = "SELECT D_CONSECUTIVO, D_CUENTA, D_REFER, D_EFECTIVO, D_MONTO, D_OPERADOR, D_MSGENVIO FROM TW_DIARIO_ELECTRON WHERE D_SUCURSAL = '" + strSucursal + "' AND D_ESTATUS ='A' AND D_TXN = '5503' AND D_CUENTA = '66' AND D_MONTO=0 AND D_EFECTIVO = 0 AND D_FECHOPER = '" + strTmp + "' ORDER BY D_REFER, D_CONSECUTIVO ASC " + PostFixUR;
            if ((strOpcion1.equals("2")) && (strOpcion2.equals("2")))
                strAccion = "SELECT D_CONSECUTIVO, D_CUENTA, D_REFER, D_EFECTIVO, D_MONTO, D_OPERADOR, D_MSGENVIO FROM TW_DIARIO_ELECTRON WHERE D_SUCURSAL = '" + strSucursal + "' AND D_ESTATUS ='A' AND D_TXN = '5503' AND D_CUENTA = '66' AND D_MONTO<>0 AND D_FECHOPER = '" + strTmp + "' ORDER BY D_REFER, D_CONSECUTIVO ASC " + PostFixUR;
            if ((strOpcion1.equals("2")) && (strOpcion2.equals("3")))
                strAccion = "SELECT D_CONSECUTIVO, D_CUENTA, D_REFER, D_EFECTIVO, D_MONTO, D_OPERADOR, D_MSGENVIO FROM TW_DIARIO_ELECTRON WHERE D_SUCURSAL = '" + strSucursal + "' AND D_ESTATUS ='A' AND D_TXN = '5503' AND D_CUENTA = '297' AND D_FECHOPER = '" + strTmp + "' ORDER BY D_REFER, D_CONSECUTIVO ASC " + PostFixUR;
            if ((strOpcion1.equals("3")) && (strOpcion2.equals("2")))
	            strAccion = "SELECT D_CONSECUTIVO, D_CUENTA, D_REFER, D_EFECTIVO, D_MONTO, D_OPERADOR, D_MSGENVIO FROM TW_DIARIO_ELECTRON WHERE D_SUCURSAL = '" + strSucursal + "' AND D_ESTATUS ='A' AND D_TXN = '5503' AND D_CUENTA = '3151' AND D_MONTO<>0 AND D_FECHOPER = '" + strTmp + "' ORDER BY D_REFER, D_CONSECUTIVO ASC " + PostFixUR;
     	    if ((strOpcion1.equals("3")) && (strOpcion2.equals("3")))
	            strAccion = "SELECT D_CONSECUTIVO, D_CUENTA, D_REFER, D_EFECTIVO, D_MONTO, D_OPERADOR, D_MSGENVIO FROM TW_DIARIO_ELECTRON WHERE D_SUCURSAL = '" + strSucursal + "' AND D_ESTATUS ='A' AND D_TXN = '5503' AND D_CUENTA = '3151' AND D_MONTO<>0 AND D_EFECTIVO = 0 AND D_FECHOPER = '" + strTmp + "' ORDER BY D_REFER, D_CONSECUTIVO ASC " + PostFixUR;
	    if ((strOpcion1.equals("4")) && (strOpcion2.equals("2")))
		    strAccion = "SELECT D_CONSECUTIVO, D_CUENTA, D_REFER, D_EFECTIVO, D_MONTO, D_OPERADOR, D_MSGENVIO FROM TW_DIARIO_ELECTRON WHERE D_SUCURSAL = '" + strSucursal + "' AND D_ESTATUS ='A' AND D_TXN = '5503' AND D_CUENTA = '3155' AND D_MONTO<>0 AND D_FECHOPER = '" + strTmp + "' ORDER BY D_REFER, D_CONSECUTIVO ASC " + PostFixUR;
            if ((strOpcion1.equals("4")) && (strOpcion2.equals("3")))
	        strAccion = "SELECT D_CONSECUTIVO, D_CUENTA, D_REFER, D_EFECTIVO, D_MONTO, D_OPERADOR, D_MSGENVIO FROM TW_DIARIO_ELECTRON WHERE D_SUCURSAL = '" + strSucursal + "' AND D_ESTATUS ='A' AND D_TXN = '5503' AND D_CUENTA = '3155' AND D_MONTO<>0 AND D_EFECTIVO = 0 AND D_FECHOPER = '" + strTmp + "' ORDER BY D_REFER, D_CONSECUTIVO ASC " + PostFixUR;
	    if ((strOpcion1.equals("5")) && (strOpcion2.equals("2")))
	        strAccion = "SELECT D_CONSECUTIVO, D_CUENTA, D_REFER, D_EFECTIVO, D_MONTO, D_OPERADOR, D_MSGENVIO FROM TW_DIARIO_ELECTRON WHERE D_SUCURSAL = '" + strSucursal + "' AND D_ESTATUS ='A' AND D_TXN = '5503' AND D_CUENTA = '3181' AND D_MONTO<>0 AND D_FECHOPER = '" + strTmp + "' ORDER BY D_REFER, D_CONSECUTIVO ASC " + PostFixUR;
	    if ((strOpcion1.equals("5")) && (strOpcion2.equals("3")))
	        strAccion = "SELECT D_CONSECUTIVO, D_CUENTA, D_REFER, D_EFECTIVO, D_MONTO, D_OPERADOR, D_MSGENVIO FROM TW_DIARIO_ELECTRON WHERE D_SUCURSAL = '" + strSucursal + "' AND D_ESTATUS ='A' AND D_TXN = '5503' AND D_CUENTA = '3181' AND D_MONTO<>0 AND D_EFECTIVO = 0 AND D_FECHOPER = '" + strTmp + "' ORDER BY D_REFER, D_CONSECUTIVO ASC " + PostFixUR;
	    if ((strOpcion1.equals("6")) && (strOpcion2.equals("2")))
	        strAccion = "SELECT D_CONSECUTIVO, D_CUENTA, D_REFER, D_EFECTIVO, D_MONTO, D_OPERADOR, D_MSGENVIO FROM TW_DIARIO_ELECTRON WHERE D_SUCURSAL = '" + strSucursal + "' AND D_ESTATUS ='A' AND D_TXN = '5503' AND D_CUENTA = '4047' AND D_MONTO<>0 AND D_FECHOPER = '" + strTmp + "' ORDER BY D_REFER, D_CONSECUTIVO ASC " + PostFixUR;
	    if ((strOpcion1.equals("6")) && (strOpcion2.equals("3")))
	        strAccion = "SELECT D_CONSECUTIVO, D_CUENTA, D_REFER, D_EFECTIVO, D_MONTO, D_OPERADOR, D_MSGENVIO FROM TW_DIARIO_ELECTRON WHERE D_SUCURSAL = '" + strSucursal + "' AND D_ESTATUS ='A' AND D_TXN = '5503' AND D_CUENTA = '4047' AND D_MONTO<>0 AND D_EFECTIVO = 0 AND D_FECHOPER = '" + strTmp + "' ORDER BY D_REFER, D_CONSECUTIVO ASC " + PostFixUR;
	    if ((strOpcion1.equals("7")) && (strOpcion2.equals("2")))
	        strAccion = "SELECT D_CONSECUTIVO, D_CUENTA, D_REFER, D_EFECTIVO, D_MONTO, D_OPERADOR, D_MSGENVIO FROM TW_DIARIO_ELECTRON WHERE D_SUCURSAL = '" + strSucursal + "' AND D_ESTATUS ='A' AND D_TXN = '5503' AND D_CUENTA = '4853' AND D_MONTO<>0 AND D_FECHOPER = '" + strTmp + "' ORDER BY D_REFER, D_CONSECUTIVO ASC " + PostFixUR;
	    if ((strOpcion1.equals("7")) && (strOpcion2.equals("3")))
	        strAccion = "SELECT D_CONSECUTIVO, D_CUENTA, D_REFER, D_EFECTIVO, D_MONTO, D_OPERADOR, D_MSGENVIO FROM TW_DIARIO_ELECTRON WHERE D_SUCURSAL = '" + strSucursal + "' AND D_ESTATUS ='A' AND D_TXN = '5503' AND D_CUENTA = '4853' AND D_MONTO<>0 AND D_EFECTIVO = 0 AND D_FECHOPER = '" + strTmp + "' ORDER BY D_REFER, D_CONSECUTIVO ASC " + PostFixUR;																					    
                
         
                
            strRespuesta = "9503" + "~" + strOpcion1 + "~"+ strOpcion2 + "~" + rbQuery.EjecutaQuery(strAccion);
            if (strRespuesta.length() < 11 )
                NoError = "No se han realizado transacciones ";
        }
        else if(strTxn.trim().equals("9029")) {
            String strOpcion1 = "1";
            String strTmp = new String("");
            if(request.getParameter("opcion1") != null)
                strOpcion1 = new String((String)request.getParameter("opcion1").toString());
            if (strDate.length() > 8)
                strTmp = strDate.substring(1,9);
            if (strOpcion1.equals("1"))
                strAccion = "SELECT D_REFER, D_REFER3, D_MONTO FROM TW_DIARIO_ELECTRON WHERE  D_SUCURSAL = '" + strSucursal + "' AND D_ESTATUS ='A' AND D_TXN = '0806' AND D_CUENTA = '4' AND D_FECHOPER = '" + strTmp + "' AND D_MONTO = 0 AND (D_REFER3 = '13' OR D_REFER3 = '13A') ORDER BY D_REFER3, D_REFER ASC " + PostFixUR;
            else
                strAccion = "SELECT D_REFER, D_REFER3, D_MONTO FROM TW_DIARIO_ELECTRON WHERE  D_SUCURSAL = '" + strSucursal + "' AND D_ESTATUS ='A' AND D_TXN = '5503' AND D_CUENTA = '4376' AND D_FECHOPER = '" + strTmp + "' AND D_MONTO <> 0 AND (D_REFER3 = '13' OR D_REFER3 = '13A') ORDER BY D_REFER3, D_REFER ASC " + PostFixUR;
            strRespuesta = "9029" + "~" + rbQuery.EjecutaQuery(strAccion);
            if (strRespuesta.length() < 11 )
                NoError = "No se han realizado transacciones ";
        }
        else if(strTxn.trim().equals("9630")) {
            String strDivisa = "";
            String strServicio = "0";
            String strOpcion1 = "1";
			strDivisa = gc.getDivisa(strMoneda);
            if(request.getParameter("opcion1") != null)
                strOpcion1 = new String((String)request.getParameter("opcion1").toString());
            if (strOpcion1.equals("1"))
                strAccion = "SELECT D_CONSECUTIVO, D_DESCRIP, D_MONTO, D_OPERADOR FROM TW_DIARIO_ELECTRON WHERE D_ESTATUS ='A' AND ( D_TXN='0460' OR D_TXN='0472' OR D_TXN='0474' OR D_TXN='0476' OR D_TXN='0478' OR D_TXN ='0482' OR D_TXN='0490' OR D_TXN='0492' OR D_TXN ='0496') AND D_DIVISA = '" + strDivisa + "' AND D_SUCURSAL = '" + strSucursal + "' AND D_FECHOPER = '" + strDate.substring(1,strDate.length()) + "'"+ PostFixUR;
            else if (strOpcion1.equals("5"))
                strServicio = "0476";
            else if (strOpcion1.equals("7"))
                strServicio = "0482";
            else if (strOpcion1.equals("8"))
                strServicio = "0490";
            else if (strOpcion1.equals("9"))
                strServicio = "0492";
            if (!strOpcion1.equals("1"))
                strAccion = "SELECT D_CONSECUTIVO, D_DESCRIP, D_MONTO, D_OPERADOR FROM TW_DIARIO_ELECTRON WHERE D_TXN= '" + strServicio + "' AND D_ESTATUS ='A' AND D_DIVISA = '" + strDivisa + "' AND D_SUCURSAL = '" + strSucursal + "' AND D_FECHOPER = '" + strDate.substring(1,strDate.length()) + "'"+ PostFixUR;
            strRespuesta = strTxn.trim() + "~" + strMoneda + "~"+ strServicio + "~";
            strRespuesta = strRespuesta + rbQuery.EjecutaQuery(strAccion);
            if (strRespuesta.length() < 14 )
                NoError = "No se han realizado transacciones ";
        }
        else if(strTxn.trim().equals("9640")) {
            String strOpcion1 = "1";
            String strOpcion2 = "1";
            if(request.getParameter("opcion1") != null)
                strOpcion1 = new String((String)request.getParameter("opcion1").toString());
            if(request.getParameter("opcion2") != null)
                strOpcion2 = new String((String)request.getParameter("opcion2").toString());            
            String remesas = null;
            if (strMoneda.equals("01"))
            {
            	if (sessions.getAttribute("rempesos") != null )
                	remesas = sessions.getAttribute("rempesos").toString();
            }
            else
            {
            	if (sessions.getAttribute("remus") != null )            	
                remesas = sessions.getAttribute("remus").toString();
            }
            strRespuesta = "9640" + "~" + strMoneda + "~"+ strOpcion1 + "~"+ strOpcion2 + "~" + remesas + "~";            
            if (strMoneda.equals("02")) {
                if(strOpcion1.equals("1")) {
                    strAccion = "SELECT D_REFER3, D_CONSECUTIVO, D_CUENTA, D_SERIAL, D_MONTO, D_OPERADOR, D_REFER1, D_REFER2, D_DESCRIP , D_TXN FROM TW_DIARIO_ELECTRON WHERE D_TXN = '0602' AND D_ESTATUS ='A' AND D_DIVISA = 'US$' AND D_SUCURSAL = '" + strSucursal + "' AND D_FECHOPER = '" + strDate.substring(1,strDate.length()) + "' Order by D_REFER3 ASC " + PostFixUR;
                    strRespuesta = strRespuesta + rbQuery.EjecutaQuery(strAccion);
                }
                else if (strOpcion1.equals("2")) 
                {
                                strAccion = "SELECT D_REFER3, D_CONSECUTIVO, D_CUENTA, D_SERIAL, D_MONTO, D_OPERADOR, D_REFER1, D_REFER2, D_DESCRIP , D_TXN FROM TW_DIARIO_ELECTRON WHERE D_TXN = '4303' AND D_DIVISA = 'N$'  AND D_REFER3 LIKE '6%' AND D_ESTATUS ='A' AND D_SUCURSAL = '" + strSucursal + "' AND D_FECHOPER = '" + strDate.substring(1,strDate.length()) + "' UNION ALL ";
                    strAccion = strAccion + "SELECT D_REFER3, D_CONSECUTIVO, D_CUENTA, D_SERIAL, D_MONTO, D_OPERADOR, D_REFER1, D_REFER2, D_DESCRIP , D_TXN FROM TW_DIARIO_ELECTRON WHERE D_TXN IN ('0510', '0600', '5103') AND D_DIVISA = 'US$' AND D_REFER3 LIKE '6%' AND D_ESTATUS ='A' AND D_SUCURSAL = '" + strSucursal + "' AND D_FECHOPER = '" + strDate.substring(1,strDate.length()) + "' Order by D_REFER3 ASC,  D_CONSECUTIVO DESC, D_TXN DESC "+ PostFixUR;                	
                    strRespuesta = strRespuesta + rbQuery.EjecutaQuery(strAccion);
                }
                else if (strOpcion1.equals("3")) {
                                strAccion = "SELECT D_REFER3, D_CONSECUTIVO, D_CUENTA, D_SERIAL, D_MONTO, D_OPERADOR, D_REFER1, D_REFER2, D_DESCRIP , D_TXN FROM TW_DIARIO_ELECTRON WHERE D_TXN = '4303' AND D_DIVISA = 'N$' AND D_REFER3 NOT LIKE '6%' AND D_ESTATUS ='A' AND D_SUCURSAL = '" + strSucursal + "' AND D_FECHOPER = '" + strDate.substring(1,strDate.length()) + "' UNION ALL ";
                    strAccion = strAccion + "SELECT D_REFER3, D_CONSECUTIVO, D_CUENTA, D_SERIAL, D_MONTO, D_OPERADOR, D_REFER1, D_REFER2, D_DESCRIP , D_TXN FROM TW_DIARIO_ELECTRON WHERE D_TXN IN ('0600', '0544', '0510', '5103')  AND D_DIVISA = 'US$' AND D_REFER3 NOT LIKE '6%' AND D_ESTATUS ='A' AND D_SUCURSAL = '" + strSucursal + "' AND D_FECHOPER = '" + strDate.substring(1,strDate.length()) + "' Order by D_REFER3 ASC,  D_CONSECUTIVO DESC, D_TXN DESC "+ PostFixUR;                    
                    strRespuesta = strRespuesta + rbQuery.EjecutaQuery(strAccion);
                }
                else if (strOpcion1.equals("4")) {
                                strAccion = "SELECT D_REFER3, D_CONSECUTIVO, D_CUENTA, D_SERIAL, D_MONTO, D_OPERADOR, D_REFER1, D_REFER2, D_DESCRIP , D_TXN FROM TW_DIARIO_ELECTRON WHERE D_TXN = '4303' AND D_ESTATUS ='A' AND D_DIVISA = 'US$' AND D_SUCURSAL = '" + strSucursal + "' AND D_FECHOPER = '" + strDate.substring(1,strDate.length()) + "' UNION ALL ";
                    strAccion = strAccion + "SELECT D_REFER3, D_CONSECUTIVO, D_CUENTA, D_SERIAL, D_MONTO, D_OPERADOR, D_REFER1, D_REFER2, D_DESCRIP , D_TXN FROM TW_DIARIO_ELECTRON WHERE D_TXN IN ('0544', '5103', '0510', '0600', '0602')  AND D_ESTATUS ='A' AND D_DIVISA = 'US$' AND D_SUCURSAL = '" + strSucursal + "' AND D_FECHOPER = '" + strDate.substring(1,strDate.length()) + "' Order by D_REFER3 ASC "+ PostFixUR;
                    strRespuesta = strRespuesta + rbQuery.EjecutaQuery(strAccion);
                }
                if (strRespuesta.length() < 20 )
                    NoError = "No se han realizado transacciones ";
            }
            else {
                            strAccion = "SELECT D_REFER3, D_CONSECUTIVO, D_CUENTA, D_SERIAL, D_MONTO, D_OPERADOR, D_REFER1, D_REFER2, D_DESCRIP , D_TXN FROM TW_DIARIO_ELECTRON WHERE D_TXN = '4303' AND D_DIVISA = 'US$' AND D_ESTATUS='A' AND D_SUCURSAL = '" + strSucursal + "' AND D_FECHOPER = '" + strDate.substring(1,strDate.length()) + "' UNION ALL ";
                strAccion = strAccion + "SELECT D_REFER3, D_CONSECUTIVO, D_CUENTA, D_SERIAL, D_MONTO, D_OPERADOR, D_REFER1, D_REFER2, D_DESCRIP , D_TXN FROM TW_DIARIO_ELECTRON WHERE D_TXN IN ('0600', '0544', '5103', '0510')  AND D_DIVISA = 'N$' AND D_ESTATUS='A' AND D_SUCURSAL = '" + strSucursal + "' AND D_FECHOPER = '" + strDate.substring(1,strDate.length()) + "' Order by D_REFER3 ASC, D_TXN DESC , D_CONSECUTIVO ASC "+ PostFixUR;
                
                strRespuesta = strRespuesta + rbQuery.EjecutaQuery(strAccion);
                if (strRespuesta.length() < 20 )
                    NoError = "No se han realizado transacciones ";
            }
        }
        else if(strTxn.trim().equals("9642")) {
            String strDivisa = "N$";
			strDivisa = gc.getDivisa(strMoneda);
            strAccion = "SELECT D_CONSECUTIVO, D_CUENTA, D_SERIAL, D_MONTO, D_OPERADOR FROM TW_DIARIO_ELECTRON WHERE D_TXN = '4545' AND D_ESTATUS = 'A' AND D_SUCURSAL = '" + strSucursal + "' AND D_DIVISA = '" + strDivisa + "' AND ( D_FECHOPER = '" + strDate + "' Or D_FECHOPER = '" + strDate.substring(1,strDate.length()) + "')"+ PostFixUR;
            strRespuesta = "9642~" + strMoneda + "~" + rbQuery.EjecutaQuery(strAccion);
            sessions.setAttribute("txtCadImpresion", strRespuesta);
            if (strRespuesta.length() < 10 )
                NoError = "No se han realizado transacciones de Cheques Certificados";
        }
        else if(strTxn.trim().equals("9730")) {
            String strDivisa = "";
            String strServicio = "000000";
            String strCeros = "000000";
			strDivisa = gc.getDivisa(strMoneda);
            if(request.getParameter("Servicio") != null) {
                strServicio = new String((String)request.getParameter("Servicio").toString());
                strServicio = strCeros.substring(0,6-strServicio.length()) + strServicio;
            }
            if (strServicio.equals("000000"))
                strAccion = "SELECT D_CONSECUTIVO, D_CUENTA, D_REFER, D_EFECTIVO, D_MONTO, D_OPERADOR  FROM TW_DIARIO_ELECTRON WHERE D_TXN='0730' AND D_ESTATUS ='A' AND D_DIVISA = '" + strDivisa + "' AND D_SUCURSAL = '"+ strSucursal + "' AND D_FECHOPER = '" + strDate.substring(1,strDate.length()) + "' Order by D_CUENTA, D_CONSECUTIVO ASC "+ PostFixUR;                
            else
				strAccion = "SELECT D_CONSECUTIVO, D_CUENTA, D_REFER, D_EFECTIVO, D_MONTO, D_OPERADOR  FROM TW_DIARIO_ELECTRON WHERE D_TXN='0730' AND D_ESTATUS ='A' AND D_DIVISA = '" + strDivisa + "' AND D_SUCURSAL = '"+ strSucursal + "' AND D_CUENTA= '" + strServicio + "' AND D_FECHOPER = '"+ strDate.substring(1,strDate.length()) +"' Order by D_CONSECUTIVO ASC "+ PostFixUR;                

            strRespuesta = strTxn.trim() + "~" + strMoneda + "~"+ strServicio + "~";
            strRespuesta = strRespuesta + rbQuery.EjecutaQuery(strAccion);
            if (strRespuesta.length() < 17 )
                NoError = "No se han realizado transacciones ";
        }
        else if(strTxn.trim().equals("9732")) {
            String strDivisa = "";
            String strServicio = "0";
            String strOpcion1 = "1";
			strDivisa = gc.getDivisa(strMoneda);
            if(request.getParameter("opcion1") != null)
                strOpcion1 = new String((String)request.getParameter("opcion1").toString());
            if (strOpcion1.equals("1"))
                strAccion = "SELECT D_CONSECUTIVO, D_DESCRIP, D_EFECTIVO, D_MONTO, D_OPERADOR FROM TW_DIARIO_ELECTRON WHERE (D_TXN='0352' OR D_TXN='0360' OR D_TXN='0370' OR D_TXN='0456' OR D_TXN='0458' OR D_TXN ='0468' OR D_TXN ='0124') AND D_ESTATUS ='A' AND D_DIVISA = '" + strDivisa + "' AND D_SUCURSAL = '" + strSucursal + "' AND D_FECHOPER = '" + strDate.substring(1,strDate.length()) + "'"+ PostFixUR;
            else if (strOpcion1.equals("2"))
                strServicio = "0124";
            else if (strOpcion1.equals("3"))
                strServicio = "0352";
            else if (strOpcion1.equals("4"))
                strServicio = "0360";
            else if (strOpcion1.equals("5"))
                strServicio = "0370";
            else if (strOpcion1.equals("6"))
                strServicio = "0456";
            else if (strOpcion1.equals("7"))
                strServicio = "0458";
            else if (strOpcion1.equals("8"))
                strServicio = "0468";
            if (!strOpcion1.equals("1"))
                strAccion = "SELECT D_CONSECUTIVO, D_DESCRIP, D_EFECTIVO, D_MONTO, D_OPERADOR FROM TW_DIARIO_ELECTRON WHERE D_TXN= '" + strServicio + "' AND D_ESTATUS ='A' AND D_DIVISA = '" + strDivisa + "' AND D_SUCURSAL = '" + strSucursal + "' AND D_FECHOPER = '" + strDate.substring(1,strDate.length()) + "'"+ PostFixUR;
            strRespuesta = strTxn.trim() + "~" + strMoneda + "~"+ strServicio + "~";
            strRespuesta = strRespuesta + rbQuery.EjecutaQuery(strAccion);
            if (strRespuesta.length() < 14 )
                NoError = "No se han realizado transacciones ";
        }
        else if(strTxn.trim().equals("9821")) {
            String strDivisa = "";
            String strServicio = "0";
            String strOpcion1 = "1";
			strDivisa = gc.getDivisa(strMoneda);
            if(request.getParameter("opcion1") != null)
                strOpcion1 = new String((String)request.getParameter("opcion1").toString());
            if (strOpcion1.equals("1"))
                strServicio = "5405";
            else
                strServicio = "5407";
            strAccion = "SELECT D_CONSECUTIVO, D_CUENTA, D_REFER, D_DESCRIP, D_MONTO, D_OPERADOR FROM TW_DIARIO_ELECTRON WHERE D_TXN= '" + strServicio + "' AND D_ESTATUS ='A' AND D_DIVISA = '" + strDivisa + "' AND D_SUCURSAL = '" + strSucursal + "' AND D_FECHOPER = '" + strDate.substring(1,strDate.length()) + "'"+ PostFixUR;
            strRespuesta = strTxn.trim() + "~" + strMoneda + "~"+ strServicio + "~";
            strRespuesta = strRespuesta + rbQuery.EjecutaQuery(strAccion);
            if (strRespuesta.length() < 15 )
                NoError = "No se han realizado transacciones ";
        }
        else if(strTxn.trim().equals("9820")) {
            String strOpcion = "1";
            String strServicio = "1";
            if(request.getParameter("opcion1") != null)
                strOpcion = new String((String)request.getParameter("opcion1").toString());
            if (strOpcion.equals("1"))
                strAccion = "SELECT D_REFER1, D_REFER2, D_REFER3, D_MONTO FROM TW_DIARIO_ELECTRON WHERE D_TXN= '0820' AND D_ESTATUS ='A' AND D_SUCURSAL = '" + strSucursal + "' AND D_REFER3 = '" + strDate.substring(1,strDate.length()) + "'"+ PostFixUR;                
            else
                strAccion = "SELECT D_REFER1, D_REFER2, D_REFER3, D_MONTO FROM TW_DIARIO_ELECTRON WHERE D_TXN= '0820' AND D_ESTATUS ='A' AND D_SUCURSAL = '" + strSucursal + "' AND D_FECHOPER = '" + strDate.substring(1,strDate.length()) + "'"+ PostFixUR;
            strRespuesta = strTxn.trim() + "~" + strOpcion + "~";
            strRespuesta = strRespuesta + rbQuery.EjecutaQuery(strAccion);
            if (strRespuesta.length() < 10 )
                NoError = "No se han realizado transacciones ";
        }
        else if(strTxn.trim().equals("9009")) {
            String strOpcion1 = "1";
            if(request.getParameter("opcion1") != null)
                strOpcion1 = new String((String)request.getParameter("opcion1").toString());
            
            Hashtable Truncamiento = (Hashtable)sessions.getAttribute("truncamiento");
            String  strTruncamiento = "50000";
            if (strMoneda.equals("01"))
                strTruncamiento = "500000";
            if(Truncamiento !=null) {
                if (!Truncamiento.isEmpty() ) {
                    if (strMoneda.equals("01"))
                        strTruncamiento = Truncamiento.get("TruncamientoMN").toString();
                    else
                        strTruncamiento = Truncamiento.get("TruncamientoUSD").toString();
                }
            }
            if (strOpcion1.equals("1")) {
                String strQuery1 = new String("");
                String strQuery2 = new String("");
                String strQuery3 = new String("");
				strAccion = "SELECT D_SUCURSAL, D_OPERADOR, D_TXN, D_FECHOPER, D_MONTO, D_CONSECLIGA, D_CUENTA, D_SERIAL FROM TW_DIARIO_ELECTRON WHERE D_SUCURSAL ='0" + strSucursal2 + "' AND D_OPERADOR = '" + strCajero + "' AND D_DIVISA ='" + gc.getDivisa(strMoneda) + "' AND D_ESTATUS = 'A' AND D_TXN IN ('4019','0604') Order by D_CONSECLIGA ASC "+ PostFixUR;
                //strAccion = "SELECT C_NUMERO_BANCO, C_NUMERO_SUCURSAL, C_NUMERO_CAJERO, C_CONSEC_VOLANTEO, C_CONSEC_CHEQUE, C_MONEDA, D_CORTE FROM TW_VOLANTEO_DDA WHERE ( C_CONSEC_CHEQUE > 0 ) AND D_VOLANTE_CERRADO= 'N' AND C_NUMERO_SUCURSAL ='" + strSucursal2 + "' AND C_NUMERO_CAJERO = '" + strCajero.substring(4,6) + "' AND C_MONEDA ='" + strMoneda + "' Order by C_NUMERO_BANCO, C_CONSEC_VOLANTEO, D_CORTE ASC " + PostFixUR;
                strQuery1 = rbQuery.EjecutaQuery(strAccion);
                Vector vDatos = Campos(strQuery1 , "~");
                int intCiclo = vDatos.size() / 8;
                /*CamposL campos = CamposL.getInstance();
                for (int intContador = 0 ; intContador < intCiclo ; intContador++) {
                    String strBanco = (String)vDatos.elementAt(intContador * 7 );
                    String strConsecutivo = (String)vDatos.elementAt(intContador * 7) + (String)vDatos.elementAt( intContador * 7 + 1) + (String)vDatos.elementAt(intContador * 7 + 2) + (String)vDatos.elementAt(intContador * 7 + 3);
                    strQuery2 += campos.getDValor("lstBanco",strBanco);
                    strAccion = "SELECT  D_IMPORTE_CHEQUE, D_DATOS_CHEQUE FROM TW_CONSEC_VOL_DDA WHERE D_STATUS='A' AND D_CONSECUTIVO = '" + strConsecutivo + "' Order by D_DATOS_CHEQUE ASC "+ PostFixUR;                    
                    strQuery3 += rbQuery.EjecutaQuery(strAccion);
                    strQuery3 += "^";
                }*/
                strQuery1 += "&";
                strQuery2 += "&";
                strQuery3 += "&";
                
                strRespuesta = strTxn.trim() + "~1~&" + strQuery1 + strQuery2 + strQuery3;
                if (strRespuesta.length() < 20 )
                    NoError = "No se han realizado transacciones para la generacion de volantes";
            }
            else if (strOpcion1.equals("2")) {
                String strQuery1 = new String("");
                strAccion = "SELECT D_CONSEC_HOGAN, D_CONSECUTIVO, D_DATOS_CHEQUE, D_IMPORTE_CHEQUE, D_CORTE FROM TW_CONSEC_VOL_DDA WHERE D_STATUS='A' AND D_VOLANTE_CERRADO= 'N' AND C_NUMERO_SUCURSAL ='" + strSucursal2 +"' AND C_NUMERO_CAJERO = '" + strCajero.substring(4,6) + "' AND D_MONEDA ='" + strMoneda + "' Order by D_CONSECUTIVO, D_CONSEC_HOGAN ASC "+ PostFixUR;
                strQuery1 = rbQuery.EjecutaQuery(strAccion);
                strRespuesta = strTxn.trim() + "~2~" + strMoneda + "~" + strTruncamiento + "~" + strQuery1;
                if (strRespuesta.length() < 20 )
                    NoError = "No se han realizado transacciones para la generacion de volantes";
            }
            else {
                String strQuery1 = new String("");
                strAccion = "SELECT D_CONSEC_HOGAN, D_CONSECUTIVO, D_DATOS_CHEQUE, D_IMPORTE_CHEQUE, D_CORTE FROM TW_CONSEC_VOL_DDA WHERE D_STATUS='A' AND D_VOLANTE_CERRADO= 'N' AND C_NUMERO_SUCURSAL ='" + strSucursal2 +"' AND C_NUMERO_CAJERO = '" + strCajero.substring(4,6) + "' AND D_MONEDA ='" + strMoneda + "' Order by D_CONSECUTIVO, D_CONSEC_HOGAN ASC "+ PostFixUR;
                strQuery1 = rbQuery.EjecutaQuery(strAccion);
                strRespuesta = strTxn.trim() + "~3~" + strMoneda + "~" +  strTruncamiento + "~" + strQuery1;
                if (strRespuesta.length() < 20 )
                    NoError = "No se han realizado transacciones para la generacion de volantes";
            }
        }
        else if(strTxn.trim().equals("RDIG")) {
            if(request.getParameter("lstMoneda") != null)
               strMoneda = new String((String)request.getParameter("lstMoneda").toString());
            StringBuffer StrAccion = new StringBuffer();
  			            StrAccion.append("select tw_consec_vol_dda.D_CONSEC_HOGAN, tw_consec_vol_dda.D_DATOS_CHEQUE,"); 
            StrAccion.append("tw_consec_vol_dda.D_IMPORTE_CHEQUE, tw_consec_vol_dda.D_CORTE from ");
            StrAccion.append("tw_consec_vol_dda INNER JOIN tw_cheques_dig ON ");
            StrAccion.append("tw_consec_vol_dda.C_NUMERO_SUCURSAL = tw_cheques_dig.C_NUMERO_SUCURSAL and ");
            StrAccion.append("tw_consec_vol_dda.C_NUMERO_CAJERO = tw_cheques_dig.C_NUMERO_CAJERO and ");
            StrAccion.append("tw_consec_vol_dda.D_DATOS_CHEQUE = tw_cheques_dig.D_DATOS_CHEQUE and ");
            StrAccion.append("tw_consec_vol_dda.C_NUMERO_SUCURSAL='" + strSucursal2 + "' and ");
            StrAccion.append("tw_consec_vol_dda.C_NUMERO_CAJERO='"+ strCajero.substring(4) + "' and ");
            StrAccion.append("tw_consec_vol_dda.D_MONEDA='"+ strMoneda + "' " + PostFixUR);
            Vector vDatos = Campos(rbQuery.EjecutaQuery(StrAccion.toString()) , "~");
            int numcampos = 4; //Numero de campos
            int registros = vDatos.size() / numcampos;
            CamposL campos = CamposL.getInstance();
            StringBuffer respuesta = new StringBuffer();
            respuesta.append("DIGITA~"+strMoneda+"~");
            respuesta.append(formatdata(registros, campos, vDatos, "SI", numcampos));
            StrAccion = new StringBuffer();
 			            StrAccion.append("select D_CONSEC_HOGAN, D_DATOS_CHEQUE, D_IMPORTE_CHEQUE, D_CORTE from tw_consec_vol_dda ");
            StrAccion.append("where C_NUMERO_SUCURSAL='"+ strSucursal2 +"' AND C_NUMERO_CAJERO='" + strCajero.substring(4) + "' AND D_MONEDA='" + strMoneda + "' and NOT EXISTS (SELECT C_NUMERO_SUCURSAL,C_NUMERO_CAJERO,D_DATOS_CHEQUE from tw_cheques_dig where ");
            StrAccion.append("tw_consec_vol_dda.C_NUMERO_SUCURSAL = tw_cheques_dig.C_NUMERO_SUCURSAL and ");
            StrAccion.append("tw_consec_vol_dda.C_NUMERO_CAJERO = tw_cheques_dig.C_NUMERO_CAJERO and ");
            StrAccion.append("tw_consec_vol_dda.D_DATOS_CHEQUE = tw_cheques_dig.D_DATOS_CHEQUE)");
            vDatos = new Vector();
            vDatos = Campos(rbQuery.EjecutaQuery(StrAccion.toString()) , "~");
            registros = vDatos.size() / numcampos;
            respuesta.append(formatdata(registros, campos, vDatos, "NO", numcampos));
            strRespuesta = respuesta.toString();
        }
        if (NoError.equals(" ")) {
            
            if (!strTxn.trim().equals("0800"))
                sessions.setAttribute("txtCadImpresion", strRespuesta);
            if (strTxn.trim().equals("0250"))
                response.sendRedirect("../ventanilla/paginas/Response0250.jsp");
            else
                response.sendRedirect("../ventanilla/paginas/ResponseCHQ.jsp");
        }
        else {
            sessions.setAttribute("NoError",NoError);
            response.sendRedirect("../ventanilla/paginas/NoOperacion.jsp");
        }
    }
    
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
    
    public String formatdata(int registros, CamposL campos,Vector vDatos, String digit, int numcampos)
    {
            StringBuffer respuesta = new StringBuffer();
            for (int i=0; i<registros; i++)
            {
                  String cheque = (String)vDatos.elementAt(i * numcampos + 1); //Banda del cheque
                  respuesta.append(campos.getDValor("lstBanco",cheque.substring(23, 26))); //Nombre del Banco
                  respuesta.append(vDatos.elementAt(i * numcampos ) + "~"); //Consecutivo
                  respuesta.append(cheque + "~"); //Banda del cheque
                  respuesta.append(vDatos.elementAt(i * numcampos + 2) + "~"); //Importe
                  String corte = (String)vDatos.elementAt(i * numcampos + 3);
                  if (corte.equals("01"))
                      corte = "T1";
                  else
                      corte = "T2";
                  respuesta.append(corte + "~"); //Corte
                  respuesta.append(digit + "~"); // Status
            }
       return respuesta.toString();
    }
}
