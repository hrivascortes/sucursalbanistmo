//*************************************************************************************************
//             Funcion: Clase que realiza txn Servicios
//            Elemento: Group03Serv.java
//          Creado por: Alejandro Gonzalez Castro
//      Modificado por: Alejandro Gonzalez Castro
//*************************************************************************************************
// CCN - 4360189 - 03/09/2004 - Se cambia invoke al Diario para Control de Efectivo
//*************************************************************************************************
package ventanilla;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.util.Hashtable;
import ventanilla.com.bital.sfb.ServiciosA;

import ventanilla.com.bital.admin.Diario;

public class Group03Serv extends HttpServlet
{
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
    {
        HttpSession session = request.getSession(false);
        Hashtable datasession = (Hashtable)session.getAttribute("page.datasession");
        String txn = (String)datasession.get("cTxn");
        String txtMoneda     = (String)datasession.get("moneda");
        

        if (txn.equals("0522") || txn.equals("0532") || txn.equals("0000")) 
        {
            if (txn.equals("0000")) {//getServletContext().setAttribute("page.txnresponse","0~01~2~Transaccion Terminada Exitosamente... ");
                session.setAttribute("page.txnresponse","0~01~2~Transaccion Terminada Exitosamente... ");
            }
            else {//getServletContext().setAttribute("page.txnresponse","0~01~2~Desplegando datos para Descarga... ");
                session.setAttribute("page.txnresponse","0~01~2~Desplegando datos para Descarga... ");
            }
            getServletContext().getRequestDispatcher("/servlet/ventanilla.Redirector").forward(request,response);
            //getServletContext().getRequestDispatcher("/servlet/PageServlet?transaction=" + datasession.get("iTxn")+ "&transaction1=" + datasession.get("oTxn")).forward(request,response);
        }
        else 
        {
            //Variables para diario electronico
            String dirip =  (String)session.getAttribute("dirip");
            String sMessage = "";
            
            ServiciosA servicioA = new ServiciosA();
            servicioA.setFormat("F");
            servicioA.setBranch(datasession.get("sucursal").toString());
            servicioA.setTeller(datasession.get("teller").toString());
            servicioA.setSupervisor(datasession.get("teller").toString());
            
            if( txn.equals("0520")) 
            {
                servicioA.setTxnCode(txn);
                servicioA.setAcctNo(txn);
                servicioA.setTranAmt("1");
                servicioA.setTranCur("N$");
                servicioA.setTranDesc(datasession.get("txtRfc").toString());
                datasession.put("OutOff","0");
            }
            
            String Monto1 = "";
            if( txn.equals("S522") ) {
                txn = "0522";
                servicioA.setTxnCode(txn);
                servicioA.setAcctNo(txn);
                Monto1 = quitap(datasession.get("txtMto").toString());
                servicioA.setTranAmt(Monto1);
                servicioA.setTranCur("N$");
                Monto1 = "";
                String NumTrab = "00000";
                NumTrab = NumTrab + datasession.get("txtNumTrab").toString();
                Monto1 = quitap(datasession.get("txtEfect").toString());
                servicioA.setCashIn(Monto1);
                servicioA.setFees(NumTrab.substring(NumTrab.length()-5));
                servicioA.setTranDesc(datasession.get("txtRfc").toString() + datasession.get("txtpatron").toString() + datasession.get("txtsector").toString());
                servicioA.setCheckNo3(datasession.get("txtBimestre").toString());
                servicioA.setTrNo3(datasession.get("lstNumDisp").toString());
            }
            if( txn.equals("0524")) {
                //servicioA.setCashIn("000");
                Monto1 = "";
                servicioA.setFees("");
                servicioA.setCheckNo3("");
                servicioA.setTrNo3("");
                servicioA.setTxnCode(txn);
                servicioA.setAcctNo(txn);
                Monto1 = quitap(datasession.get("txtCveIMSS").toString());
                servicioA.setTranAmt(Monto1);
                servicioA.setTranCur("N$");
                servicioA.setTranDesc(datasession.get("txtRfc").toString());
                Monto1 = "";
                Monto1 = quitap(datasession.get("txtPagIMSS").toString());
                servicioA.setAmount3(Monto1);
                Monto1 = "";
                Monto1 = quitap(datasession.get("txtRcgIMSS").toString());
                servicioA.setAmount4(Monto1);
            }
            if( txn.equals("0526")) {
                servicioA.setTxnCode(txn);
                servicioA.setAcctNo(txn);
                Monto1 = "";
                Monto1 = quitap(datasession.get("txtCveINFONAV").toString());
                servicioA.setTranAmt(Monto1);
                servicioA.setTranCur("N$");
                servicioA.setTranDesc(datasession.get("txtRfc").toString());
                Monto1 = "";
                Monto1 = quitap(datasession.get("txtPagINFONAV").toString());
                servicioA.setAmount3(Monto1);
                
                Monto1 = "";
                Monto1 = quitap(datasession.get("txtRcgINFONAV").toString());
                servicioA.setAmount4(Monto1);
                datasession.put("OutOff","1");
            }
            
            if( txn.equals("S532") ) {
                txn = "0532";
                servicioA.setTxnCode(txn);
                servicioA.setAcctNo(txn);
                Monto1 = quitap(datasession.get("txtMto").toString());
                servicioA.setTranAmt(Monto1);
                servicioA.setTranCur("N$");
                String NumTrab = "00000";
                NumTrab = NumTrab + datasession.get("txtNumTrab").toString();
                Monto1 = "";
                Monto1 = quitap(datasession.get("txtEfect").toString());
                servicioA.setCashIn(Monto1);
                servicioA.setFees(NumTrab.substring(NumTrab.length()-5));
                servicioA.setTranDesc(datasession.get("txtRfc").toString() + datasession.get("txtpatron").toString() + datasession.get("txtsector").toString());
                servicioA.setCheckNo3(datasession.get("txtBimestre").toString());
                servicioA.setTrNo3(datasession.get("lstNumDisp").toString());
                
            }
            
            if( txn.equals("0534")) {
                //servicioA.setCashIn("000");
                Monto1 = "";
                servicioA.setFees("");
                servicioA.setCheckNo3("");
                servicioA.setTrNo3("");
                servicioA.setTxnCode(txn);
                servicioA.setAcctNo(txn);
                Monto1 = quitap(datasession.get("txtCveISSSTE").toString());
                servicioA.setTranAmt(Monto1);
                servicioA.setTranCur("N$");
                servicioA.setTranDesc(datasession.get("txtRfc").toString());
                Monto1 = "";
                Monto1 = quitap(datasession.get("txtPagISSSTE").toString());
                servicioA.setAmount3(Monto1);
                Monto1 = "";
                Monto1 = quitap(datasession.get("txtRcgISSSTE").toString());
                servicioA.setAmount4(Monto1);
            }
            
            if( txn.equals("0536") ) {
                servicioA.setTxnCode(txn);
                servicioA.setAcctNo(txn);
                Monto1 = "";
                Monto1 = quitap(datasession.get("txtCveFOVI").toString());
                servicioA.setTranAmt(Monto1);
                servicioA.setTranCur("N$");
                servicioA.setTranDesc(datasession.get("txtRfc").toString());
                Monto1 = "";
                Monto1 = quitap(datasession.get("txtPagFOVI").toString());
                servicioA.setAmount3(Monto1);
                
                Monto1 = "";
                Monto1 = quitap(datasession.get("txtRcgFOVI").toString());
                servicioA.setAmount4(Monto1);
                datasession.put("OutOff","1");
            }
            Diario diario = new Diario();
            sMessage = diario.invokeDiario(servicioA, (String)session.getAttribute("d_ConsecLiga"), dirip, txtMoneda, session);
            if ( (txn.equals("0524") || txn.equals("0526") || txn.equals("0534")  || txn.equals("0536"))
                 && sMessage.charAt(0) == '0')
               session.setAttribute("txtCadImpresion", "NOIMPRIMIRNOIMPRIMIR");

            session.setAttribute("page.txnresponse", sMessage);
            getServletContext().getRequestDispatcher("/servlet/ventanilla.Redirector").forward(request,response);
        }
    }
    
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
    
    private String quitap(String Valor) {
        StringBuffer Cantidad = new StringBuffer(Valor);
        for(int i=0; i<Cantidad.length();) {
            if( Cantidad.charAt(i) == ',' || Cantidad.charAt(i) == '.' )
                Cantidad.deleteCharAt(i);
            else
                ++i;
        }
        return Cantidad.toString();
    }
    
    private int GetConv(String parame) {
        int entero   = Integer.parseInt(parame);
        return entero;
    }
    
    private String Vali(String v1, String v2, String v3, String v4, String v5, String v6, String v7, String v8) {
        String valor = "";
        int immiss    = GetConv(v1);
        int inffov    = GetConv(v2);
        int immissact = GetConv(v3);
        int inffovact = GetConv(v4);
        int immissrec = GetConv(v5);
        int inffovrec = GetConv(v6);
        int efectivo  = GetConv(v7);
        int monto     = GetConv(v8);
        
        if (efectivo == 0 && monto == 0) {
            //		alert("efectivo no puede ser cero");
            valor = "1";
            return valor;
        }
        int Total = immiss + inffov;
        //	alert("total: " + Total );
        if (efectivo != 0  && Total != efectivo && monto == 0) {
            //		alert("efectivo no coincide");
            valor = "1";
            return valor;
        }
        if (monto != 0 && Total != monto) {
            //		alert("monto no coincide");
            valor = "1";
            return valor;
        }
        if (efectivo != 0 && monto == 0) {
            monto = efectivo;
        }
        if (monto < immissact || monto < inffovact ||
        monto < immissrec || monto < inffovrec) {
            //		alert("Cargos y Actualizaciones no pueden ser mayor a los importes...");
            valor = "1";
            return valor;
        }
        
        return valor;
    }
    public String getmoneda(String cmoneda) {
        String newmoneda = "";
        if (cmoneda.equals("N$"))
        { newmoneda = "01";}
        else if (cmoneda.equals("US$"))
        { newmoneda = "02";}
        else
        { newmoneda = "03";}
        return newmoneda;
    }
    
    public String removeAsterisk(String campo) {
        StringBuffer result = new StringBuffer(campo);
        String caracter = result.substring(result.length()-1,result.length());
        if (caracter.equals("*"))
            result.deleteCharAt(result.length()-1);
        return result.toString();
    }
    
}
