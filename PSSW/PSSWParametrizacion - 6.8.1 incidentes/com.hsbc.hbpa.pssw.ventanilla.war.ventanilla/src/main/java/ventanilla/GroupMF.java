//*************************************************************************************************
//             Funcion: Redirector de Txns Miscelaneas
//            Elemento: GroupMF.java
//          Creado por: Alejandro Gonzalez Castro
//      Modificado por: Juvenal R. Fernandez V.
//*************************************************************************************************
// CCN - 4360334 - 17/06/2005 - Se modifica redireccion a relativa para WAS5
//*************************************************************************************************
package ventanilla;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class GroupMF extends HttpServlet
{
  public void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
  {
       HttpSession session = req.getSession(false);
       java.util.Stack flujotxn = (java.util.Stack)session.getAttribute("page.flujotxn");
       java.util.Hashtable ht = (java.util.Hashtable)session.getAttribute("page.datasession");

       String txtMontoCheque = (String)ht.get("txtCheque");

       if ( txtMontoCheque.equals("0.00")){
       	  if(flujotxn != null){
       	    flujotxn.pop();
            session.setAttribute("page.flujotxn", flujotxn);
          }
       }
       String iTxn = ht.get("iTxn").toString();
       String oTxn = ht.get("oTxn").toString();

	   String Cad = "ventanilla.PageServlet?transaction=" + iTxn +
					   "&transaction1=" + oTxn;
       resp.sendRedirect(Cad);

  }
  public void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
  {
    doPost(req, resp);
  }
}
