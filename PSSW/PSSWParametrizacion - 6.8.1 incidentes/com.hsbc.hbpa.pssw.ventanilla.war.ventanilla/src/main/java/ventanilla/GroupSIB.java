//*************************************************************************************************
//             Funcion: Bean para Operacion CSIB
//          Creado por: Hugo Gabriel Rivas
//      
//*************************************************************************************************
// CR - 262793 - 01/07/2012 - Creacion de elemento para el proyecto Daily Cash
//*************************************************************************************************

package ventanilla;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import ventanilla.com.bital.admin.PostingSIB;

public class GroupSIB extends HttpServlet {
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/plain");
        HttpSession session = request.getSession(false);
        PostingSIB posting = new PostingSIB();
        posting.execute(session);
        getServletContext().getRequestDispatcher("/servlet/ventanilla.Redirector").forward(request,response);
  }
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}
