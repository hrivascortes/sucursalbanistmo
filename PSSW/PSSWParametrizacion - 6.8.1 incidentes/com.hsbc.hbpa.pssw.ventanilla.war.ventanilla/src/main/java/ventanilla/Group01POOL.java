package ventanilla;

import java.io.IOException;
import java.util.Hashtable;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class Group01POOL extends HttpServlet
{

  public void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
  {
       resp.setContentType("text/plain");

       HttpSession session = req.getSession(false);
       Hashtable datasession = (Hashtable)session.getAttribute("page.datasession");

       String txtBranch     = (String)datasession.get("txtSucursal");
       String txtTeller     = (String)datasession.get("txtGerente");//"000108";
       String txtRegistro   = (String)datasession.get("txtRegistro");//"000101";
       String Contrasenia  = "";

       String dayNumber = String.valueOf(java.util.Calendar.getInstance().get(java.util.Calendar.DAY_OF_MONTH));
       String monthNumber = String.valueOf( 1 + java.util.Calendar.getInstance().get(java.util.Calendar.MONTH));
       String yearNumber = String.valueOf(java.util.Calendar.getInstance().get(java.util.Calendar.YEAR));
       dayNumber = dayNumber.substring(1);
       if (monthNumber.length() == 1 )
           monthNumber = "0" + monthNumber;
       monthNumber = monthNumber.substring(1);
       yearNumber = yearNumber.substring(3);
       String Cajero = txtTeller.substring(5);
       String Registro = txtRegistro.substring(6);
       String Sucursal = txtBranch.substring(3);
       String Tira = monthNumber + Cajero + yearNumber + Registro + dayNumber + Sucursal;
       int car_A = (( Integer.parseInt(Sucursal) * 2) % 10) + 1;
       int car_B = ((Integer.parseInt(dayNumber) * 3) % 10) + 1;
       int car_C = (Integer.parseInt(Registro) * 2) % 10;
       int car_D = (Integer.parseInt(yearNumber) * 3) % 10;
       int car_E = (Integer.parseInt(Cajero) * 2) % 10;
       int car_F = (Integer.parseInt(monthNumber) * 3) % 10;

       String car_ZZ = String.valueOf(car_A);
       String cadena = new String("EMOQSUWYAC");
       String car_AA = cadena.substring(car_A - 1,car_A);
       cadena = "VDFHJLNPRT";
       String car_YY = String.valueOf(car_B);
       String car_CC = String.valueOf(car_C);
       String car_DD = String.valueOf(car_D);
       String car_EE = String.valueOf(car_E);
       String car_FF = String.valueOf(car_F);
       String car_BB = cadena.substring(car_B - 1, car_B);

       Contrasenia =  car_AA + car_BB + car_ZZ + car_YY + car_CC + car_DD + car_EE + car_FF;
       String sMessage =  "0~01~2~Password:  " + Contrasenia + "~";

       //getServletContext().setAttribute("page.txnresponse", sMessage);
       session.setAttribute("page.txnresponse", sMessage);
       getServletContext().getRequestDispatcher("/servlet/ventanilla.Redirector").forward(req,resp);
  }// doPost


  public void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
  {
    doPost(req, resp);
  }// doGet

}// de la clase
