package ventanilla;

import java.io.IOException;
import java.util.Hashtable;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import ventanilla.com.bital.sfb.GerenteD;

public class Group02GTE extends HttpServlet
{
  public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
  {
    response.setContentType("text/plain");
    HttpSession session = request.getSession(false);
    Hashtable datasession = (Hashtable)session.getAttribute("page.datasession");
	GenericClasses gc = new GenericClasses();

	int code = 0;
	String coma = "'";
	String sql_req ="";
	String status ="";

    GerenteD gerented = new GerenteD();

    gerented.setBranch((String)datasession.get("sucursal"));

	String txn = (String)datasession.get("cTxn");

	if (txn.equals("0013"))
	{
		gerented.setTxnCode(txn);
		gerented.setTeller((String)datasession.get("teller"));
		gerented.setSupervisor((String)datasession.get("teller"));

		String moneda = (String)datasession.get("lstMoneda");
		gerented.setCurrCod1(gc.getDivisa(moneda));
	    gerented.execute();

//	    getServletContext().setAttribute("page.txnresponse", gerented.getMessage());
	    session.setAttribute("page.txnresponse", gerented.getMessage());
    	getServletContext().getRequestDispatcher("/servlet/ventanilla.Redirector").forward(request,response);
	}
  }

  public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
  {
    doPost(request, response);
  }
}
