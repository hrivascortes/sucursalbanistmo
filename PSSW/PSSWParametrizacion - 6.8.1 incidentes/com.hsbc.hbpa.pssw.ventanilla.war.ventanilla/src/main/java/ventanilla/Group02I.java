//*************************************************************************************************
//             Funcion: Clase para TXNS de inversiones
//            Elemento: Group02I.java
//          Creado por: Alejandro Gonzalez Castro.
//		Modificado por: Juvenal Fernandez
//*************************************************************************************************
// CCN - 4360189 - 03/09/2004 - Se cambia invoke al Diario para Control de Efectivo
// CCN - 4360268 - 04/02/2005 - Se realizan adecuaciones para eliminar Agencia NY
// CCN - 4360315 - 06/05/2005 - Se corrige problema con la descripcion de las txns txns 2526,2529,2525,2527
// CCN - 4360368 - 02/09/2005 - Se elimina sleep
// CCN - 4360374 - 08/09/2005 - Se genera nuevo paquete a peticion de QA.
//*************************************************************************************************


package ventanilla;

import java.io.IOException;
import java.util.Hashtable;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import ventanilla.com.bital.sfb.Inversiones;
import ventanilla.com.bital.sfb.Acdo;
import ventanilla.com.bital.admin.Diario;

public class Group02I extends HttpServlet 
{
    public void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {
        Inversiones oInversiones = new Inversiones();
        Acdo    oAcdo    = new Acdo();
		GenericClasses cGenericas = new GenericClasses();
        resp.setContentType("text/plain");

        HttpSession session = req.getSession(false);
        Hashtable datasession = (Hashtable)session.getAttribute("page.datasession");

        String dirip = (String)session.getAttribute("dirip");
        String txtMoneda     = (String)datasession.get("moneda");
        String txtTxnCode    = "0825";// la Txn 0825 es la del ACDO.
        String txtFormat     = "B";
        String txtBranch     = (String)datasession.get("sucursal");
        String txtTeller     = (String)datasession.get("teller");//"000108";
        String txtSupervisor = (String)datasession.get("teller");//"000101";
        String txtBackOut    = "0";
        String txtOverride   = "0";
        String txtCurrentDay = "0";

        oAcdo.setTxnCode(txtTxnCode);
        oAcdo.setFormat(txtFormat);
        oAcdo.setBranch(txtBranch);
        oAcdo.setTeller(txtTeller);
        oAcdo.setSupervisor(txtSupervisor);
        oAcdo.setBackOut(txtBackOut);
        oAcdo.setOverride(txtOverride);
        oAcdo.setCurrentDay(txtCurrentDay);

        String lstTipoDocto  = (String)datasession.get("lstTipoDocto"); //Tipo de Documento de ACDO
        String txtReferencia = (String)datasession.get("txtFolio");//Referencia del Documento de ACDO
        oAcdo.setAcctNo((lstTipoDocto + txtReferencia));

        Diario diario = new Diario();
        String sMessage = diario.invokeDiario(oAcdo, (String)session.getAttribute("d_ConsecLiga"), dirip, txtMoneda, session);

        if(sMessage.charAt(0) !='0')
        {
            session.setAttribute("txtCadImpresion", "NOIMPRIMIRNOIMPRIMIR");
            session.setAttribute("page.txnresponse", sMessage);
            getServletContext().getRequestDispatcher("/servlet/ventanilla.Redirector").forward(req,resp);
            return;
        }
        else
        {
            txtTxnCode    = (String)datasession.get("cTxn");
            txtFormat     = "A";
            String txtDescripcion= "";
            if ((String)datasession.get("supervisor")!= null)
                txtSupervisor = (String)datasession.get("supervisor");

            oInversiones.setTxnCode(txtTxnCode);
            oInversiones.setFormat(txtFormat);
            oInversiones.setBranch(txtBranch);
            oInversiones.setTeller(txtTeller);
            oInversiones.setSupervisor(txtSupervisor);
            oInversiones.setBackOut(txtBackOut);
            oInversiones.setOverride(txtOverride);
            oInversiones.setCurrentDay(txtCurrentDay);
			
            if ( (String)datasession.get("override") != null )
                if (datasession.get("override").toString().equals("SI"))
                    oInversiones.setOverride("3");
            String txtCDACuenta  = (String)datasession.get("txtCDACuenta");
            String txtMonto      = cGenericas.delCommaPointFromString((String)datasession.get("txtMonto"));
            String txtFechaEfect = (String)datasession.get("txtFechaEfect"); //Fecha en que se Opera la Txn.
            String txtFechaSys   = (String)datasession.get("txtFechaSys"); //Fecha en que se Opera la Txn.
            String txtEfectivo   = "000";

            txtFechaEfect =  cGenericas.ValEffDate(txtFechaEfect, txtFechaSys);
            oInversiones.setAcctNo(txtCDACuenta); //No de Cuenta
            oInversiones.setTranAmt(txtMonto);    //Monto Total de la Txn.
            oInversiones.setTranCur(txtMoneda);   //Tipo de Moneda con que se opera la Txn.
            oInversiones.setEffDate(txtFechaEfect);  //Fecha en que se opera la Txn.
			
            if( txtTxnCode.equals("3075")){
                txtEfectivo = txtMonto;
                oInversiones.setCashOut(txtMonto);   // Salida de efectivo
            }
            if( txtTxnCode.equals("3105"))
            {
                txtDescripcion= (String)datasession.get("lstDesc3105");//Descripci"n
                oInversiones.setTranDesc(txtDescripcion);//Descripcion
                // Descripcion para Reverso
                String DescRev = "REV." + txtDescripcion;
                if(DescRev.length() > 40)
                    DescRev = DescRev.substring(0,40);
                oInversiones.setDescRev(DescRev);
            }
            if( txtTxnCode.equals("2525")) {
                 txtDescripcion = "TRANSFERENCIA A ";
                 oInversiones.setTranDesc(txtDescripcion);//Descripcion
            }
            if( txtTxnCode.equals("2527")) {
                txtDescripcion = "CREDITO PARA ";
                oInversiones.setTranDesc(txtDescripcion);//Descripcion
            }

            diario = new Diario();
            sMessage = diario.invokeDiario(oInversiones, (String)session.getAttribute("d_ConsecLiga"), dirip, txtMoneda, session);
			
            if (!sMessage.equals(""))
            {
                session.setAttribute("page.txnresponse",sMessage);
                getServletContext().getRequestDispatcher("/servlet/ventanilla.Redirector").forward(req,resp);
                return;
            }
            session.setAttribute("page.txnresponse", oInversiones.getMessage());
            getServletContext().getRequestDispatcher("/servlet/ventanilla.Redirector").forward(req,resp);
        }
    }
    public void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doPost(req, resp);
    }
}
