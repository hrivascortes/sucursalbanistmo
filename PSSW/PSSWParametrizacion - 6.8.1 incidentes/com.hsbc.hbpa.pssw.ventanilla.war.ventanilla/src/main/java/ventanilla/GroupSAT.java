//*************************************************************************************************
//             Funcion: Clase para el pago de impuestos de SAT
//            Elemento: GroupSAT.java
//          Creado por: Alejandro Gonzalez Castro
//      Modificado por: Juvenal Fernandez
//*************************************************************************************************
// CCN -4360137 - 27/05/2004 - Eliminacion de Autocommit (Reversado)
// CCN -4360171 - 23/07/2004 - Se modifica para pago de SAT 13 - 13A
// CCN -4360189 - 03/09/2004 - Se envia txn 0806 para montos en cero
//                             y Se cambia invoke al Diario para Control de Efectivo
// CCN -4360237 - 05/10/2004 - Se envia formato para txn 0806 pago Anual SAT en ceros
// CCN -4360245 - 15/11/2004 - Se evita imprimir sello digital con error de comunicacion
// CCN -4360259 - 21/01/2005 - Se evita crear una tabla de bines que no se ocupa
// CCN - 4360334 - 17/06/2005 - Se elimina variable dbQualifier
// CCN - 4360364 - 19/08/2005 - Se realizan correciones para txns duplicadas del SAT
// CCN - 4360410 - 06/01/2006 - Se realizan modificaciones al SAT
//*************************************************************************************************

package ventanilla;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Hashtable;
import java.util.StringTokenizer;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import ventanilla.com.bital.admin.Diario;
import ventanilla.com.bital.sfb.RAPE;
import ventanilla.com.bital.sfb.RAPG;

import com.bital.util.ConnectionPoolingManager;

public class GroupSAT extends HttpServlet
{

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        response.setContentType("text/plain");
        HttpSession session = null;
        Hashtable datasession = null;
        String CurrServ = "";        
        String CurrPago = "";
        String cobrocom = "";
        String compcom = "";
        String txn = "";
        String cuenta = "";
        String moneda = "";
        String msg = "";
        String dirip = "";
        String codigo = "";
        String tipo = "";
        String conRAP = "";
        int consecrap = 0;        
        session = request.getSession(false);
        datasession = (Hashtable)session.getAttribute("page.datasession");
        dirip =  (String)datasession.get("dirip");
        String cajero = (String)datasession.get("teller");        
        // CLEAR A TABLA CERTIF
        clearcertif(cajero);
        // OBTENCION DE IMPORTES DE PAGOS POR RUBRO
        long mtoefe = getpago(datasession, "efe");
        long mtotmp = mtoefe;
        CurrServ = "1";
        CurrPago = "1";
        datasession.put("ConRAP", "0");

        // Tipo de Servicio 1 o 2
        tipo = (String)datasession.get("tiposerv");

        // CONTROL DE PAGOS - ACEPTADO O RECHAZADO
        String acepta = (String)datasession.get("aceptas"+CurrServ+"p"+CurrPago);

        // AJUSTE PARA SERVICIOS EXCEPTO LOS QUE COBRAN COMISION
        if(acepta == null)
            acepta = "1";

        cobrocom = (String)datasession.get("cobrocoms"+CurrServ+"p"+CurrPago);
        if(cobrocom == null)
            cobrocom = "0";

        compcom = (String)datasession.get("compcoms"+CurrServ+"p"+CurrPago);
        if(compcom == null)
            compcom = "0";

        if(acepta.equals("1"))
        {
            // TXN 5503  
            txn = "5503";
            codigo = RAP5503(datasession, session, CurrServ, CurrPago, txn, dirip, mtotmp, consecrap);
        }

        String nservicios = (String)datasession.get("nos");
        String npagos = (String)datasession.get("nps"+CurrServ);

        if(CurrServ.equals(nservicios) && CurrPago.equals(npagos))
        {
            datasession.put("end", "1");
        }

        datasession.put("codigo", codigo);


        conRAP = Integer.toString(consecrap);
        String tmp = Long.toString(mtotmp);
        datasession.put("mtoRAP", tmp);
        datasession.put("ConRAP", conRAP);
        datasession.put("CurrServ", CurrServ);
        datasession.put("CurrPago", CurrPago);

        session.setAttribute("page.datasession",datasession);
        session.setAttribute("page.getoTxnView","ResponseS503");

        if (codigo.equals("0"))
            session.setAttribute("page.txnresponse", "0~02~0151515~TRANSACCION ACEPTADA~");
        else
            session.setAttribute("page.txnresponse", "1~02~0151515~TRANSACCION RECHAZADA~");
        session.setAttribute("Code.Pago.SAT", codigo);
        getServletContext().getRequestDispatcher("/servlet/ventanilla.Redirector").forward(request,response);
    }

    private void certif(String nrespuesta, String teller, String cuenta, String txn, String refs, String moneda, String codigo, String amount, String efectivo, int consecrap) 
    {

        StringTokenizer resp = new StringTokenizer(nrespuesta, "~");
        String respuesta = "";
        while (resp.hasMoreElements()) {
            respuesta = respuesta + resp.nextElement().toString();
        }

        int lineas = Integer.parseInt(respuesta.substring(1,3));
        String consec = respuesta.substring(3,10);
        respuesta = respuesta.substring(10,respuesta.length());

        Connection pooledConnection = ConnectionPoolingManager.getPooledConnection();
        Statement insert = null;
        String sql = "";
        try {
            if(pooledConnection != null) {
                int j = 0;
                for(int k=0; k<lineas; k++) {
                    insert = pooledConnection.createStatement();
                    String lresp = respuesta.substring(j,Math.min(j+78, respuesta.length()));
                    sql = "INSERT INTO TW_CERTIFICACIONES VALUES('"+teller+"','"+codigo+cuenta+"',"+amount+","+efectivo+",'"+txn+"','"+moneda+"',"+consecrap+","+(k+1)+",'"+lresp.trim()+"','"+consec+"','"+refs+"')";
                    int code = 0;
                    code = insert.executeUpdate(sql);
                    
                    j = j+78;
                    if(insert != null){
                        insert.close();
                        insert = null;
                    }
                }
            }
        }
        catch(SQLException sqlException)
        {
            System.out.println("Error GroupSAT::certif [" + sqlException + "]");
            System.out.println("Error GroupSAT::certif::SQL [" + sql + "]");
        }
        finally{
            try{
                if(insert != null){
                    insert.close();
                    insert = null;
                }
                if(pooledConnection != null){
                    pooledConnection.close();
                    pooledConnection = null;
                }
            }
            catch(java.sql.SQLException sqlException)
            {
                System.out.println("Error FINALLY GroupSAT::certif [" + sqlException + "]");
                System.out.println("Error FINALLY GroupSAT::certif::SQL [" + sql + "]");
            }
        }
    }

    private String RAP5503(Hashtable datasession, HttpSession session, String CurrServ, String CurrPago, String txn, String dirip, long mtotmp, int consecrap)
//    private String RAP5503(Hashtable info, String Txn, String Serv, String Pago, long mtotem, String dirIP, int ConsecRAP, String conliga, HttpSession sesion)    
    {
        String ref1 = "";
        String ref2 = "";
        String ref3 = "";
        String codigo = "";
        String monto = "";
        String refs = "";
        String beneficiario = "";
        long mtoefe = 0;
        long mtochq = 0;
       
        String servicio = (String)datasession.get("s"+CurrServ);
        monto = (String)datasession.get("montos"+CurrServ+"p"+CurrPago);
        String moneda = (String)datasession.get("moneda");

        ventanilla.GenericClasses gc = new ventanilla.GenericClasses();
        monto = gc.delCommaPointFromString(monto);
        moneda = gc.getDivisa(moneda);

        if( (servicio.equals("4376")) && (Long.parseLong(monto) == 0))
        {
                RAPG rapg = new RAPG();
                rapg.setTxnCode("0806");
                rapg.setBranch((String)datasession.get("sucursal"));
                rapg.setTeller((String)datasession.get("teller"));
                rapg.setSupervisor((String)datasession.get("teller"));
                rapg.setFormat("G");
                rapg.setToCurr(moneda);
                rapg.setTranAmt("000");
                rapg.setAcctNo("4");
                String txtRfc = (String)datasession.get("txtRfc");
                if (txtRfc.length() == 0)
                {
                    String numcredito = (String)datasession.get("numcredito");
                    if (numcredito.length() == 14)
                        numcredito = numcredito.substring(0,13);
                    txtRfc = numcredito;
                    datasession.put("txtRfc",txtRfc);
                }
                rapg.setBenefic(txtRfc + (String)datasession.get("txtRefer16"));
                if((String)datasession.get("pagoA")!=null)
            		rapg.setFormatoSAT((String)datasession.get("pagoA"));
                
                Diario diario = new Diario();
                String sMessage = diario.invokeDiario(rapg, (String)session.getAttribute("d_ConsecLiga"), dirip, moneda, session);

                String respons = sMessage;
                session.setAttribute("page.txnresponse",respons);
                codigo = respons.substring(0,1);
        }
        else
        {
        RAPE rape = new RAPE();
        rape.setFormat("A");
        rape.setBranch((String)datasession.get("sucursal"));
        rape.setTeller((String)datasession.get("teller"));
        rape.setSupervisor((String)datasession.get("supervisor"));
        rape.setTxnCode(txn);
        rape.setAcctNo(servicio);
        String cuenta = servicio;

        mtoefe = getpago(datasession, "efe");
        mtochq = getpago(datasession, "chq");

        // CASO SOLO EFECTIVO
        if(mtoefe > 0 && mtochq == 0) {
        	monto = Long.toString(mtoefe);
            rape.setTranAmt(monto);
            rape.setCashIn(monto);
        }
        // CASO EFECTIVO Y CHQ
        else if(mtoefe > 0 && mtochq >= 0) {
           rape.setCashIn(Long.toString(mtoefe));
           rape.setTranAmt(monto);
        }
        // CASO CHQ Y / O CTA
        else if(mtochq >= 0 && mtoefe == 0) {
            rape.setTranAmt(monto);
            rape.setCashIn("000");
        }
        else {
            System.out.println("No entra a ningun caso de combinación de pagos");
        }

        if(servicio.equals("480") || servicio.equals("448") || servicio.equals("297") || servicio.equals("66")) {

            String RFC = (String)datasession.get("ref1s"+CurrServ+"p"+CurrPago);

            if(RFC != null) {
                RFC = RFC.trim();
                int large = RFC.length();
                if (large == 10) {
                    ref1 = RFC.substring(0,4);
                    ref2 = RFC.substring(4,RFC.length());
                    ref3 = "000";
                }
                else if (large == 13) {
                    ref1 = RFC.substring(0,4);
                    ref2 = RFC.substring(4,10);
                    ref3 = RFC.substring(10,RFC.length());
                }
                else {
                    ref1 = RFC.substring(0,3);
                    ref2 = RFC.substring(3,9);
                    ref3 = RFC.substring(9,RFC.length());
                }
                rape.setReferenc1(ref1);
                rape.setReferenc2(ref2);
                rape.setReferenc3(ref3);
            }
        }
        else {
            ref1 = (String)datasession.get("ref1s"+CurrServ+"p"+CurrPago);
            if(ref1 != null)
                rape.setReferenc1(ref1);
            else
                ref1 = "";
            ref2 = (String)datasession.get("ref2s"+CurrServ+"p"+CurrPago);
            if(ref2 != null)
                rape.setReferenc2(ref2);
            else
                ref2 = "";
            ref3 = (String)datasession.get("ref3s"+CurrServ+"p"+CurrPago);
            if(ref3 != null)
                rape.setReferenc3(ref3);
            else
                ref3 = "";
        }

        refs = ref1 + "&" + ref2 + "&" + ref3 + "&";

        
            rape.setFromCurr(moneda);
        if((String)datasession.get("pagoA")!=null)
            rape.setFormatoSAT((String)datasession.get("pagoA"));
        
        Diario diario = new Diario();
        String sMessage = diario.invokeDiario(rape, (String)session.getAttribute("d_ConsecLiga"), dirip, moneda, session);

        String respuesta = sMessage;

        codigo = respuesta.substring(0,1);

        if (CurrServ.equals("1") && codigo.equals("0") && respuesta.length() > 235 )
            beneficiario = respuesta.substring(235,respuesta.length()-1).trim();

        String amount = rape.getTranAmt().toString().substring(0,rape.getTranAmt().length() - 1);
        String efectivo = rape.getCashIn().toString().substring(0,rape.getCashIn().length() - 1);

        if(efectivo.length() == 0)
            efectivo = "000";
        String teller = (String)datasession.get("teller");
        certif(respuesta,teller, cuenta, txn, refs,moneda, codigo, amount, efectivo, consecrap);
        }    
        return codigo;
    }

    private void clearcertif(String cajero) {
        String statementPS = ConnectionPoolingManager.getStatementPostFix();
        Connection pooledConnection = ConnectionPoolingManager.getPooledConnection();
        PreparedStatement delete = null;
        try {
            if(pooledConnection != null) {
                delete = pooledConnection.prepareStatement("DELETE FROM TW_CERTIFICACIONES WHERE D_CAJERO=?" + statementPS);
                delete.setString(1,cajero);
                int i = delete.executeUpdate();
                
            }
        }
        catch(SQLException sqlException)
        {System.out.println("Error GroupSAT::clearcertif [" + sqlException + "]");}
        finally {
            try {
                if(delete != null) {
                    delete.close();
                    delete = null;
                }
                if(pooledConnection != null) {
                    pooledConnection.close();
                    pooledConnection = null;
                }
            }
            catch(java.sql.SQLException sqlException){System.out.println("Error FINALLY GroupSAT::clearcertif [" + sqlException + "]");}
        }
    }

    private long getpago(Hashtable data, String pago)
    {
        String monto = new String();
        long mto = 0;
        
        if(pago.equals("efe"))
        {
            monto = (String)data.get("txtEfectivo");
            if(monto != null)
            {
                monto = clearmto(monto);
                mto = Long.parseLong(monto);
            }
        }

        if(pago.equals("chq"))
        {
            monto = (String)data.get("txtCheque");
            if(monto != null)
            {
                monto = clearmto(monto);
                mto = Long.parseLong(monto);
            }
        }
        
        if(pago.equals("cta"))
        {
            monto = (String)data.get("txtCuenta");
            if(monto != null)
            {
                monto = clearmto(monto);
                mto = Long.parseLong(monto);
            }   
        }
        
        if(pago.equals("coi"))
        {
            monto = (String)data.get("txtDocsCI");
            if(monto != null)
            {
                monto = clearmto(monto);
                mto = Long.parseLong(monto);
                if(mto > 0)
                    data.put("CIrap", "1");
            }
        }

        if(pago.equals("rem"))
        {
            monto = (String)data.get("txtRemesas");
            if(monto != null)
            {
                monto = clearmto(monto);
                mto = Long.parseLong(monto);
                if(mto > 0)
                    data.put("REMrap", "1");
            }
        }
        
        return mto;
    }

    private String clearmto(String mto)
    {
        StringBuffer montotmp = new StringBuffer(mto);
        for(int k=0; k<montotmp.length();) {
            if(montotmp.charAt(k) == ',' || montotmp.charAt(k) == '.')
                montotmp.deleteCharAt(k);
            else
                ++k;
        }
        mto = montotmp.toString();
        return mto;
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}
