//*************************************************************************************************
//             Funcion: Clase que realiza txn 0097
//            Elemento: Group08I.java
//          Creado por: Israel De Paz Mercado
//      Modificado por: Israel De Paz Mercado
//*************************************************************************************************
// CCN - 4360364 - 19/08/2005 - Se crea elemento Group08I para la nueva txn 0097 de consulta de subproducto
//*************************************************************************************************
package ventanilla;

import java.io.IOException;
import java.util.Hashtable;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import ventanilla.com.bital.admin.Diario;
import ventanilla.com.bital.sfb.FormatoA;


public class Group08I extends HttpServlet
{
    public void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {
        HttpSession session = req.getSession(false);
        Hashtable datasession = (Hashtable)session.getAttribute("page.datasession");
        GenericClasses GC = new GenericClasses();
        
        String dirip = (String)session.getAttribute("dirip");
        String txtMoneda = GC.getDivisa((String)datasession.get("moneda"));
        String sMessage = "";
        String txtTxnCode    = "0097";
        String txtCDACuenta = (String)datasession.get("txtCDACuenta");
        String txtFormat     = "A";
        String txtBranch     = (String)datasession.get("sucursal");
        String txtTeller     = (String)datasession.get("teller");
        String txtSupervisor = (String)datasession.get("teller");
        
        FormatoA Inv = new FormatoA();
        Inv.setTxnCode(txtTxnCode);
        Inv.setFormat(txtFormat);
        Inv.setBranch(txtBranch);
        Inv.setTeller(txtTeller);
        Inv.setSupervisor(txtSupervisor);
        Inv.setAcctNo(txtCDACuenta);
        Inv.setTranCur(txtMoneda);
        Inv.setMoAmoun("000");
       
        Diario diario = new Diario();
        sMessage = diario.invokeDiario(Inv, (String)session.getAttribute("d_ConsecLiga"), dirip, txtMoneda, session);
        
        session.setAttribute("page.txnresponse", sMessage);
        getServletContext().getRequestDispatcher("/servlet/ventanilla.Redirector").forward(req,resp);
   }


    public void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doPost(req, resp);
    }
}
