//*************************************************************************************************
//          Funcion: Servlet para el LOG de la TXN FIRM
//          Elemento: FirmLog.java
//          Creado por: Fredy Pe�a Moreno
//	Modificado por: Fredy Pe�a Moreno		
//*************************************************************************************************
// CCN - 4360617 - 08/06/2007 - Se cambia varaible de cta para guardar en el log
//*************************************************************************************************

package ventanilla;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.Hashtable;
import java.io.IOException;
import java.io.OutputStream;
import ventanilla.com.bital.admin.Diario;
import ventanilla.com.bital.sfb.Transaction;
import ventanilla.com.bital.util.TransactionFactory;
import com.bital.util.ParameterNotFoundException;

public class FirmLog extends HttpServlet
{
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{		
		byte[] buffer   = new byte[10];	
		String strParametro = request.getQueryString();		
   		//System.out.println("parametro "+strParametro);
		HttpSession session = request.getSession(false);
		String teller="";
		String moneda="";
		String sucursal="";   
			
        Hashtable datasession = (Hashtable)session.getAttribute("page.datasession");
        if(datasession==null){
        	datasession = new Hashtable();
        	teller = (String)session.getAttribute("teller");
        	moneda = "NA";
        	sucursal = (String)session.getAttribute("branch");
        }else{
        	moneda = "NA";
		    teller = (String)datasession.get("teller");
        	sucursal = (String)datasession.get("sucursal");
        }
        
        Hashtable usrinfo = (Hashtable)session.getAttribute("userinfo");
        String dirip = (String)session.getAttribute("dirip");
        String ConLiga = (String)session.getAttribute("d_ConsecLiga");       
		String emplo = usrinfo.get("D_EJCT_CTA").toString().trim();		

	    String txtDDACuenta =(String)session.getAttribute("Cuenta");	    
	    datasession.put("tipoConsulta",strParametro);	    
	    datasession.put("dirip",dirip);
	    datasession.put("racf",emplo);
		datasession.put("txtDDACuentaFirm",txtDDACuenta.substring(1,16));
	    datasession.put("moneda", moneda);
	    datasession.put("sucursal",sucursal);
	    datasession.put("teller",teller);
	    datasession.put("cTxn","Firm");
	    datasession.put("supervisor",teller);	   

   		Transaction generic = null;         
        generic = TransactionFactory.getTransaction("Firm");
        
        if( generic == null )
            System.out.println("NO EXISTE LA TXN");
        
        try {// Obtener referencia a la transaccion
            generic.perform(datasession);
        }
        catch( ParameterNotFoundException e ) {           
            System.out.println("1~1~0000000~ERROR EN LA TRANSACCION~"+e);
        }
		
        Diario diario = new Diario();
        String sMessage = diario.invokeDiario(generic.getProcess(), ConLiga, dirip, moneda, session);  
       
		buffer [0] = 0;
		buffer [1] = 0;
		buffer [2] = 0;
		buffer [0] = 1;
		buffer [1] = 1;
		buffer [2] = 1;
		
		response.setContentType("application/octet-stream");
		OutputStream os = response.getOutputStream();
		os.write(buffer, 0, buffer.length);
		os.close();
	}
  
	public void doPost(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException
	{
		doGet(request, response);
	}
}
