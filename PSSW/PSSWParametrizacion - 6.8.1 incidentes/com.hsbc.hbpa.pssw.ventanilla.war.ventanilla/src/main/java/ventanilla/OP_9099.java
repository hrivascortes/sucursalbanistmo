package ventanilla;

import java.io.IOException;
import java.util.Calendar;
import java.util.Hashtable;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import ventanilla.com.bital.sfb.CompraVenta;
import ventanilla.com.bital.sfb.OrdenPago;

public class OP_9099 extends HttpServlet {
    private String stringFormat(int option, int sum) {
        Calendar now = Calendar.getInstance();
        String temp = new Integer(now.get(option) + sum).toString();
        if( temp.length() != 2 )
            temp = "0" + temp;
        
        return temp;
    }
    
    private String setCommaToString(String newCadNum) {
        int iPIndex = newCadNum.indexOf(","), iLong;
        String szTemp;
        
        if(iPIndex > 0) {
            newCadNum = new String(newCadNum + "00");
            newCadNum = newCadNum.substring(0, iPIndex + 1) + newCadNum.substring(iPIndex + 1, iPIndex + 3);
        }
        else {
            for(int i = newCadNum.length(); i < 3; i++)
                newCadNum = "0" + newCadNum;
            iLong = newCadNum.length();
            if(iLong == 3)
                szTemp = newCadNum.substring(0, 1);
            else
                szTemp = newCadNum.substring(0, iLong - 2);
            newCadNum = szTemp + "," + newCadNum.substring(iLong - 2);
        }
        
        return newCadNum;
    }
    private String delCommaPointFromString(String newCadNum) {
        String nCad = new String(newCadNum);
        if( nCad.indexOf(".") > -1) {
            nCad = nCad.substring(nCad.indexOf(".") + 1, nCad.length());
            if(nCad.length() != 2) {
                String szTemp = new String("");
                for(int j = nCad.length(); j < 2; j++)
                    szTemp = szTemp + "0";
                newCadNum = newCadNum + szTemp;
            }
        }
        
        StringBuffer nCadNum = new StringBuffer(newCadNum);
        for(int i = 0; i < nCadNum.length(); i++)
            if(nCadNum.charAt(i) == ',' || nCadNum.charAt(i) == '.')
                nCadNum.deleteCharAt(i);
        
        return nCadNum.toString();
    }
    
    private String getString(String newCadNum, int newOption) {
        int iPIndex = newCadNum.indexOf("CONSECUTIVO:") + 12, NumSaltos, Num = 0, Ban = 0;
        String nCadNum = new String("");
        NumSaltos = 4;
        if(newOption == 0)
            NumSaltos = 3;
        while(Num < NumSaltos) {
            if(newCadNum.charAt(iPIndex) == 0x20) {
                while(newCadNum.charAt(iPIndex) == 0x20)
                    iPIndex++;
                Num++;
            }
            else
                while(newCadNum.charAt(iPIndex) != 0x20)
                    iPIndex++;
        }
        while(newCadNum.charAt(iPIndex) != 0x20) {
            nCadNum = nCadNum + newCadNum.charAt(iPIndex++);
            if(newCadNum.charAt(iPIndex) == 0x20 && newCadNum.charAt(iPIndex + 1) == 0x2A) {
                nCadNum = nCadNum + newCadNum.charAt(iPIndex) + newCadNum.charAt(iPIndex + 1);
                iPIndex += 2;
            }
        }
        
        return nCadNum;
    }
    
    public void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String time = stringFormat(Calendar.HOUR_OF_DAY, 0) + stringFormat(Calendar.MINUTE, 0);
        String TI, TE;
        StringBuffer E = new StringBuffer("");
        
        CompraVenta oTxn = new CompraVenta();
        OrdenPago oTxnOP = new OrdenPago();
        HttpSession session = req.getSession(false);
        Hashtable datasession = (Hashtable)session.getAttribute("page.datasession");
        oTxnOP.setOPSucursal((String)datasession.get("sucursal"));
        int noTax = 0;
        String noTaxS = new String("");
        TE = (String)datasession.get("cTxn");
        TI = (String)datasession.get("iTxn");
        
        oTxn.setTxnCode(TE);
        oTxn.setBranch((String)datasession.get("sucursal"));
        oTxn.setTeller((String)datasession.get("teller"));
        oTxn.setSupervisor((String)datasession.get("teller"));
        
        if(TE.equals("SLOP")) {
            //     oTxnOP.setTransaction("INQ1");
            String newTemp = (String)datasession.get( "txtNumOrden" );
            String lstBanLiq = "02";
            int NumPos = 37;
            if(lstBanLiq.equals("02"))
                NumPos -= 4;
            int len = newTemp.length();
            for(int i = len; i < NumPos; i++)
                newTemp = newTemp + " ";
            if(lstBanLiq.equals("02"))
                newTemp = newTemp + "0001";
            newTemp = newTemp + "00000000";
            oTxnOP.setData(newTemp);
            
            oTxnOP.execute();
            oTxnOP.getStatus();
        }
        else if(TE.equals("0587")) {
            oTxn.setTxnCode("0587");
            oTxn.setFormat("B");
            oTxn.setAcctNo( "0587" );
            oTxn.setTranAmt( delCommaPointFromString((String)datasession.get( "txtMonto" )) );
            oTxn.setReferenc((String)datasession.get( "txtOSN" ));
            oTxn.setFees((String)datasession.get( "txtBanco" ));
            oTxn.setFromCurr("N$");
            
            oTxn.execute();
            oTxn.getStatus();
            
            oTxnOP.setProcessCode("LIQ ");
            String newTemp = (String)datasession.get( "txtNumOrden" );
            String lstBanLiq = "02";
            int NumPos = 37;
            if(lstBanLiq.equals("02"))
                NumPos -= 4;
            int len = newTemp.length();
            for(int i = len; i < NumPos; i++)
                newTemp = newTemp + " ";
            if(lstBanLiq.equals("02"))
                newTemp = newTemp + "0001";
            newTemp = newTemp + (String)datasession.get( "txtOSN" );
            newTemp = newTemp + "LIQ ";
            newTemp = newTemp + stringFormat(Calendar.YEAR, 0).substring(2, 4) + stringFormat(Calendar.MONTH, 0) + stringFormat(Calendar.DAY_OF_MONTH, 0);
            newTemp = newTemp + stringFormat(Calendar.HOUR_OF_DAY, 0) + stringFormat(Calendar.MINUTE, 0);
            newTemp = newTemp + (String)datasession.get("teller") + "  ";
            String newBranch = (String)datasession.get("sucursal");
            newTemp = newTemp + newBranch.substring(1);
            oTxnOP.setData(newTemp);
            
            oTxnOP.execute();
            oTxnOP.getStatus();
            
            //DBApp myDBApp = DBApp.getInstance();
            long systemDate = Long.parseLong(stringFormat(Calendar.YEAR, 0) +
            stringFormat(Calendar.MONTH, 1) +
            stringFormat(Calendar.DAY_OF_MONTH,0));
            String txtBanco = (String)datasession.get("txtBanco");
            if(txtBanco == null)
                txtBanco = new String("");
            else
                txtBanco = txtBanco.substring(1);
            java.sql.Connection pooledConnection = com.bital.util.ConnectionPoolingManager.getPooledConnection( );
            java.sql.Statement newStatement = null;
            try{
                if(pooledConnection != null){
                    String statement = "DELETE from idpsw.TW_ORDPAGTRANS where U_IDCAJERO like '" + (String)datasession.get("teller") + "' and F_FECHAEMI < " + systemDate;
                    newStatement = pooledConnection.createStatement();
                    int response = newStatement.executeUpdate(statement);
                    statement = "INSERT INTO idpsw.TW_ORDPAGTRANS values(" +
                    Long.parseLong((String)datasession.get( "txtNumOrden" )) + "," +
                    "'" + (String)datasession.get("teller") + "'," +
                    "'" + txtBanco + "'," +
                    Long.parseLong(delCommaPointFromString((String)datasession.get( "txtMonto" ))) + "," +
                    "'" + (String)datasession.get("txtApeOrd") + (String)datasession.get("txtNomOrd")+ "'," +
                    "'" + (String)datasession.get("txtApeBen") + (String)datasession.get("txtNomBen")+ "'," +
                    "'" + "'," +
                    "'" + "'," +
                    "20" + (String)datasession.get("txtFecha") + ")";
                    newStatement = pooledConnection.createStatement();
                    response = newStatement.executeUpdate(statement);
                }
            }
            catch(java.sql.SQLException sqlException){
                System.out.println("Error OP_9099::executeQuery: [" + sqlException.toString() + "] ");
            }
            finally{
                try{
                    if(newStatement != null){
                        newStatement.close();
                        newStatement = null;
                    }
                    if(pooledConnection != null){
                        pooledConnection.close();
                        pooledConnection = null;
                    }
                }
                catch(java.sql.SQLException sqlException){
                    System.out.println("Error FINALLY OP_9099::executeQuery: [" + sqlException.toString() + "] ");
                }
            }
        }
        
        session.setAttribute("page.txnresponse", oTxn.getMessage());
        getServletContext().getRequestDispatcher("/servlet/ventanilla.Redirector").forward(req,resp);
    }
    
    public void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doPost(req, resp);
    }
}
