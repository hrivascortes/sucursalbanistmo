//*************************************************************************************************
//			   Funcion: Clase que realiza el redireccionamiento al servlet correspondiente a la txn
//			  Elemento: DataServlet.java
//			Creado por: Alejandro Gonzalez Castro
//		Modificado por: Fausto Rodrigo Flores Moreno
//		Modificado por: Yessica G Garcia
//*************************************************************************************************
// CCN - 4360171 - 23/07/2004 - Se agrega condicion para ejecutar txn 0732-0124
// CCN - 4360175 - 03/08/2004 - Se habilita deposito parcial txn 1009 descontado chqs rechazados
// CCN - 4360178 - 20/08/2004 - Se agrega Group19Serv.java para txn 0530 ISSTE
// CCN - 4360189 - 03/09/2004 - Se cambia invoke al Diario para Control de Efectivo
// CCN - 4360211 - 08/10/2004 - Se cambia txn N281 por N081, Se cambia el servlet de consulta 
//				de totales txn 0013, Se elimina mensaje de Documentos pendientes 
//				por Digitalizar
// CCN - 4360237 - 05/11/2004 - Se modifica agrega txn 1515
// CCN - 4360312 - 26/04/2005 - Se agrega txn 0208-0212-0214 para ATM'S
// CCN - 4360314 - 06/05/2005 - Se agrega transaccion 0093 para Motivator
// CCN - 4360347 - 21/07/2005 - Se realizan modificaciones para la txn 0061 cuando la cuenta tiene status 4-5
// CCN - 4360356 - 19/08/2005 - Se agrega txn 0097 para consulta del subrpoducto de Inversiones.
// CCN - 4360409 - 06/01/2006 - Se agrega txn 0027,S027 para la Linea de Captura del SAT
// CCN - 4360424 - 03/02/2006 - Se realizan modificaciones para txn combinadas y divisas
// CCN - 4360425 - 03/02/2006 - Proyecto Tarjeta de Identificacion TDI
// CCN - 4360437 - 24/02/2006 - Se realizan modificaciones para txns de SPEI
// CCN - 4360455 - 07/04/2006 - Se realizan modificaciones para el esquema de cheques devueltos.
// CCN - 4360462 - 10/04/2006 - Se genera nuevo paquete por errores en paquete anterior
// CCN - 4360493 - 23/06/2006 - Se realizan modificaciones para txns 5627, 5621, 5619, 1653 y 1651 para depto 9005
// CCN - 4360500 - 04/08/2006 - Se agregan txn 0230, 0232, 0234, 0236, 5005, 5007 para CDM's
// CCN - 4360512 - 08/09/2006 - Se agrega txn 0238 Identificaci�n de dep�sitos (ETV) OLG
// CCN - 4360532 - 20/10/2006 - Se realizan modificaciones para giros
// CCN - 4360533 - 20/10/2006 - Se realizan modificaciones para OPEE
// CCN - 4360542 - 10/11/2006 - Se realizan modificaciones para OPEE y txn 0821
// CCN - 4360574 - 09/03/2007 - Se realizan modificaciones para RAP Calculadora
// CCN - 4360576 - 09/03/2007 - Modificaciones para proyecto OPMN
// CCN - 4360584 - 23/03/2007 - Modificaciones para proyecto OPMN
// CCN - 4360592 - 20/04/2007 - Modificaciones para proyecto OPMN
// CCN - 4360602 - 18/05/2007 - Se realizan modifiaciones para el pago de SUAS con cedula y genericas
// CCN - 4620008 - 14/09/2007 - Se agrega TXN 0476 para nuevo esquema
// CCN - 4620021 - 17/10/2007 - Se agrega modificaci�n para formato SIB, Humberto Balleza
// CCN - 4620055 - 09/01/2008 - Se hacen modificaciones para la txn 0021 con cargo a cuenta
// CCN - 4620057 - 11/01/2008 - Se ambienta la txn 0101, Rodrigo Escand�n
//*************************************************************************************************

package ventanilla;


import java.io.IOException;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Stack;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import com.bital.util.ParameterParser;
import com.bital.util.ParameterNotFoundException;
import ventanilla.com.bital.util.TransactionFactory;
import ventanilla.com.bital.sfb.Transaction;
import ventanilla.com.bital.admin.Diario;
import ventanilla.com.bital.admin.LimiteTransacciones;

public class DataServlet extends HttpServlet {

	private String iTxn = "";
	private String oTxn = "";
	private String cTxn = "";
	public boolean cger_imp=false;
    
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        
		HttpSession session = request.getSession(false);
		if( session == null ){return;}
			

		String[] values;
		values = null;
		try 
		{
			StringBuffer svrRequest = new StringBuffer();
			Enumeration params = request.getParameterNames();
            
			Hashtable datasession = (Hashtable)session.getAttribute("page.datasession");
			if(datasession == null){datasession = new Hashtable();}
				
			while( params.hasMoreElements() ) {
				String param = (String)params.nextElement();
				values = request.getParameterValues( param );
				for(int i=0; i<values.length; i++)
					datasession.put(param, values[i]);
			}
		datasession.put("dirip",session.getAttribute("dirip"));
		datasession.put("ciudad",(String)session.getAttribute("ciudad"));            
			session.setAttribute("page.datasession", datasession);
			Enumeration campos = datasession.keys();
			while( campos.hasMoreElements() ) {
				String sigcampo = (String)campos.nextElement();
				svrRequest.append(sigcampo + "=" + datasession.get(sigcampo) + "&");
			}            
			iTxn = (String)datasession.get("iTxn");
			oTxn = (String)datasession.get("oTxn");
			cTxn = (String)datasession.get("cTxn");  
			String txnProcess = (String)session.getAttribute("txnProcess");
			String montotran = "";
			String txtTxn = cTxn;
            
			if(cTxn.equals("0568") && datasession.get("lstForLiq").equals("02")){
				cTxn="4099";
				Stack flujotxn = (Stack)session.getAttribute("page.flujotxn");
				if (!flujotxn.empty()){
					flujotxn.pop();
				}
			}	 
			
				if(cTxn.equals("1051") || cTxn.equals("1053") 
			|| cTxn.equals("4543") || cTxn.equals("4547")
			|| cTxn.equals("4537") || cTxn.equals("4539")
			|| cTxn.equals("5353") || cTxn.equals("1079")
			|| cTxn.equals("4545") || cTxn.equals("4229")
			|| cTxn.equals("M007")
			){
				String cuentaFirma = null;
				cuentaFirma = (String)session.getAttribute("CuentaNuevaFirma");
				if(cuentaFirma != null)
				{
					datasession.put("txtDDACuenta2",cuentaFirma);
				}if(cuentaFirma != null && cTxn.equals("4229"))
				{
					String Scuentas = "";
					Scuentas = (String)datasession.get("Scuentas");
					Scuentas = cuentaFirma+Scuentas.substring(10,Scuentas.length());
					datasession.put("Scuentas",Scuentas);
				}else if(cuentaFirma != null && cTxn.equals("M007"))
				{
					String Scuentas5 = "";
					Scuentas5 = (String)datasession.get("Scuentas5");
					Scuentas5 = cuentaFirma+Scuentas5.substring(10,Scuentas5.length());
					datasession.put("Scuentas5",Scuentas5);
				}
				session.removeAttribute("CuentaNuevaFirma");
			}
            
			String divisatran = (String)datasession.get("moneda");
			if (txnProcess.equals("03"))  {                                       // Compra / Venta
				montotran = (String)datasession.get("txtMonto1");          // Siempre en dolares
				divisatran = "02";
			}
			else
				montotran = (String)datasession.get("txtMonto");
            
            
			if (txnProcess.equals("06") && cTxn.equals("0572")) {
				if ( datasession.get("lstForPago").toString().equals("02")){ txtTxn = "4089"; }
					
			}
			if(cTxn.equals("0825")){
			   session.removeAttribute("idFlujo");
			}
			LimiteTransacciones lm = new LimiteTransacciones();                   // valida montos maximos por transaccion
			String msglm = lm.getMontosLimite(txnProcess,iTxn,oTxn,txtTxn,divisatran,montotran);
			if (!msglm.equals("")) 
			{
				Stack flujotxn = (Stack)session.getAttribute("page.flujotxn");
				if (!flujotxn.empty()){
					flujotxn.removeAllElements();
					session.setAttribute("page.flujotxn", flujotxn);
				}
				session.setAttribute("txtCadImpresion", "ERRORMONTOLIM~" + msglm.substring(7) + "~");
				session.setAttribute("page.txnresponse", msglm);
				getServletContext().getRequestDispatcher("/servlet/ventanilla.Redirector").forward(request,response);
				return;
			}
            
			if ( cTxn.equals("5353")) 
			{
				getServletContext().getRequestDispatcher("/servlet/ventanilla.Group04R").forward(request,response);
			}else if (iTxn.equals("PCGA"))
			{
				getServletContext().getRequestDispatcher("/servlet/ventanilla.Group03R").forward(request,response);
			}
			else if(txnProcess.equals("29") || txnProcess.equals("00") || (txnProcess.equals("01") && (cTxn.equals("4251") || cTxn.equals("4271") || cTxn.equals("1051") || cTxn.equals("5051")
			|| cTxn.equals("1357") || cTxn.equals("5209") || cTxn.equals("5215") || cTxn.equals("5627") || cTxn.equals("5619") || cTxn.equals("1651")))
			|| txnProcess.equals("05") || (txnProcess.equals("13") && (cTxn.equals("1009") || cTxn.equals("0238") || cTxn.equals("0817")
			|| cTxn.equals("0837") || cTxn.equals("0580") || cTxn.equals("0772")  || cTxn.equals("5359"))) 
			|| (txnProcess.equals("26") && cTxn.equals("5159") || cTxn.equals("0278") || cTxn.equals("5183")) || cTxn.equals("0825") || txnProcess.equals("24") 
			|| (txnProcess.equals("25") && !cTxn.equals("0823")) || (txnProcess.equals("26") && !cTxn.equals("0823")) || cTxn.equals("0230") || cTxn.equals("0232") || cTxn.equals("0234") 
			|| cTxn.equals("0236") || cTxn.equals("5005") || cTxn.equals("1295") || cTxn.equals("1797") || cTxn.equals("5007") || (txnProcess.equals("26") && (cTxn.equals("5161") 
			|| (cTxn.equals("5177"))) && !iTxn.equals("0021")) || (cTxn.equals("0838")) ||(txnProcess.equals("04") && !cTxn.equals("0814"))          
			|| (txnProcess.equals("13") && (cTxn.equals("0476"))) || (txnProcess.equals("05") && (cTxn.equals("CGER")))
			|| (txnProcess.equals("05") && (cTxn.equals("SLCG"))) || (txnProcess.equals("05") && (cTxn.equals("0136")))          
			|| cTxn.equals("ISIB") ||  (txnProcess.equals("20") && (cTxn.equals("DMOV"))
			|| cTxn.equals("0101"))||(txnProcess.equals("05") && (cTxn.equals("SDCG")))		
			|| (cTxn.equals("9914"))|| (cTxn.equals("2215")) || (cTxn.equals("2217")) || (cTxn.equals("2219")) || (cTxn.equals("0272"))
			|| (txnProcess.equals("05") && cTxn.equals("RECG")) || (txnProcess.equals("05") && cTxn.equals("ECGD"))
			|| (txnProcess.equals("05") && cTxn.equals("CACG")) || cTxn.equals("CANC") || cTxn.equals("5217") || cTxn.equals("5219")			
			|| (txnProcess.equals("26") && cTxn.equals("2199")) || (txnProcess.equals("26") && cTxn.equals("2203"))
			|| (txnProcess.equals("26") && cTxn.equals("4525")) || (txnProcess.equals("26") && cTxn.equals("2201")) || (cTxn.equals("2101")) || (cTxn.equals("4523"))
			|| cTxn.equals("4019") || cTxn.equals("3025")  || cTxn.equals("0332") || cTxn.equals("1161") || cTxn.equals("1119") || cTxn.equals("0492")
			|| (txnProcess.equals("29") && cTxn.equals("0360")) ||(txnProcess.equals("02") && (cTxn.equals("2507") || cTxn.equals("4017")))
			|| cTxn.equals("CSIB") || cTxn.equals("0080"))
			{
//CAMBIOSNOMBRECORTO: Se agrega TXN 0080 para que ingrese a esta condici�n.
				if(cTxn.equals("S001") || cTxn.equals("S003") || cTxn.equals("S009") || cTxn.equals("S073"))
					getServletContext().getRequestDispatcher("/servlet/ventanilla.GroupMF").forward(request,response);
             
				else if (cTxn.equals("DMOV"))              
					getServletContext().getRequestDispatcher("/servlet/ventanilla.com.bital.sfb.DetalleMovimientos").forward(request, response);										                           
				else if (cTxn.equals("CGER"))              
					getServletContext().getRequestDispatcher("/servlet/ventanilla.Redirector").forward(request, response);										                           
				else if (cTxn.equals("CACG"))              
					getServletContext().getRequestDispatcher("/servlet/ventanilla.Redirector").forward(request, response);										                           						
				else if (cTxn.equals("5525") || cTxn.equals("5537") || cTxn.equals("5539"))
					getServletContext().getRequestDispatcher("/servlet/ventanilla.GroupATM1").forward(request,response);
				else if (cTxn.equals("1509"))
					getServletContext().getRequestDispatcher("/servlet/ventanilla.Group01R").forward(request,response);
				else if (cTxn.equals("CSIB"))
					getServletContext().getRequestDispatcher("/servlet/ventanilla.GroupSIB").forward(request,response);
				else if (cTxn.equals("ISIB") && ((datasession.get("lstCuentaSIB").equals("01")) || (datasession.get("lstCuentaSIB").equals("05")) || (datasession.get("lstCuentaSIB").equals("04"))))
				{
					session.setAttribute("page.txnresponse","0~03~ 000001~ CONSECUTIVO:  000001                                                          CUENTA:  00000000                                                   COMPA�IA:0060~");
					getServletContext().getRequestDispatcher("/servlet/ventanilla.Redirector").forward(request, response);
					return;
				}else 
				{
					if(cTxn.equals("ISIB")){
						session.setAttribute("idFlujo",(String)datasession.get("lstCuentaSIB"));
					}
//INI: CAMBIOSNOMBRECORTO
                                        if (this.cTxn.equals("0101")) {
                                            session.setAttribute("idFlujo", "01");
                                        }
//FIN: CAMBIOSNOMBRECORTO
					Transaction generic = null;
                    
					String idFlujo = (String)session.getAttribute("idFlujo");
                         
					ventanilla.com.bital.beans.Flujotxn_Int ft = ventanilla.com.bital.beans.Flujotxn_Int.getInstance();
					if(oTxn == null){oTxn = iTxn;}
					   
					String[] flow = ft.getFlujotxn(iTxn, oTxn, (String)session.getAttribute("page.moneda"), cTxn, idFlujo);                        
						String especial = "";
						String result = "";
						
						int x = 0;
					
					if(txnProcess.equals("02") && session.getAttribute("data.to.authoriz") != null){                    	
                    	if(session.getAttribute("CombReqAuto")!=null){
                    		x=Integer.parseInt(session.getAttribute("CombReqAuto").toString());
                    	}
                                session.removeAttribute("CombReqAuto");                 
                    }else{	                       
                          session.setAttribute("CombReqAuto","0");
                    }
                    
					for(int i=x; i<flow.length; i++) 
					{	
							if(flow[i].equals("4101")){
								session.setAttribute("page.getoTxnView","");
								datasession.put("cTxn","4101"); 
								session.setAttribute("page.datasession", datasession);
								session.setAttribute("page.cTxn","4101");             
							}	
							/*
							if(flow[i].equals("0821") || flow[i].equals("0594"))
							{
									datasession.put("moneda", "02");
									session.setAttribute("page.datasession", datasession);
							}
							if(flow[i].equals("0598"))
							{
									datasession.put("moneda", "01");
									session.setAttribute("page.datasession", datasession);
							}*/
							if(flow[i].equals("0580")){
								Hashtable Truncamiento = (Hashtable)session.getAttribute("truncamiento");
							   long TruncamientoMN = 500000L;
							   if (!Truncamiento.isEmpty() ){
								   datasession.put("trunc0580",Truncamiento.get("TruncamientoMN").toString());
								   
							   }						   
							}
							session.setAttribute("Txnint",flow[i]);
							generic = TransactionFactory.getTransaction(flow[i]);
							
							if (session.getAttribute("Txnint").toString().equals("0360")||
									session.getAttribute("Txnint").toString().equals("2505") ||
									session.getAttribute("Txnint").toString().equals("4029")){
								datasession.put("txnCurr", session.getAttribute("Txnint"));
							}
							else 
								datasession.put("txnCurr", "0372");
								
							
							if( generic == null ){
								return;
							}
							
							
							result = execTransaction(generic, session);
							session.setAttribute("page.txnresponse", result);
							
							if(generic.getProcess().getTxnCode().equals("4131"))
							{ 
								datasession.put("cTxn", "4131");
								session.setAttribute("page.cTxn", "4131");
							}
							if( result.charAt(0) == '3' && txnProcess.equals("02")){
	                               session.setAttribute("CombReqAuto",String.valueOf(i));
	                               session.setAttribute("especialOver", especial);
	                        }else if(result.charAt(0) != '3' && txnProcess.equals("02") && (String)datasession.get("override")!= null && datasession.get("override").toString().startsWith("S")){
	                        	datasession.put("override","N");
								datasession.put("supervisor",(String)datasession.get("teller"));
								especial = session.getAttribute("especialOver").toString();
	                        }
                            
							if( flow.length > 1 )
							{
								if( result.indexOf("CONSECUTIVO") > 0 ){
									especial = especial + flow[i] + "~" + result.substring(5, 12) + "~";
								}
							}
							else if( flow[i].equals("S165") || flow[i].equals("S365") )
								especial = "4153~" + result.substring(5, 12) + "~";
							else if( flow[i].equals("S265") || flow[i].equals("S465") )
								especial = "0065~" + result.substring(5, 12) + "~";
							else if( flow[i].equals("4097"))
								especial = "4097~" + result.substring(5, 12) + "~";
							else if( flow[i].equals("0568"))
								especial = "0568~" + result.substring(5, 12) + "~";   
							else if( flow[i].equals("4099"))
								especial = "4099~" + result.substring(5, 12) + "~";                                
							else if( flow[i].equals("4101"))
								especial = "4101~" + result.substring(5, 12) + "~";                                                  
							
							if( result.charAt(0) != '0' ){
								break;
							}    
                                
							Hashtable newdatasession = (Hashtable)session.getAttribute("page.datasession");
							String valoverride = (String)newdatasession.get("override"); 
							String valteller = (String)newdatasession.get("teller");    
							if(flow[i].equals("4011") && result.charAt(0) == '0'&& valoverride.startsWith("S"))
							  {
							  newdatasession.put("override","N");
							  newdatasession.put("supervisor",valteller);
							  session.setAttribute("page.datasession", newdatasession);
							  }	                                 
						}						
						session.removeAttribute("data.to.authoriz");
						if( especial.length() > 0 ){
							session.setAttribute("txtCasoEsp", especial);
						}
						ParameterParser data = new ParameterParser(datasession);
						try 
						{
							if( data.getString("cTxn").equals("1009") ) 
							{
								long monto = 0L;
                                
								if( generic.toString().startsWith("0") ) 
								{
									monto = data.getLong("MontoTotal", 0);
									monto += Long.parseLong(data.getCString("txtMontoCheque"));
									datasession.put("MontoTotal", new Long(monto).toString());
								}

								if( data.getString("iTxn").equals("1009") && generic.toString().startsWith("1") ) 
								{
									monto = Long.parseLong(data.getCString("txtCheque"));
									monto -= Long.parseLong(data.getCString("txtMontoCheque"));
									GenericClasses GC = new GenericClasses();
									String mto = GC.formatMonto(new Long(monto).toString());
									datasession.put("txtCheque", mto);
									monto = data.getLong("MontoTotal", 0);
								}
                                
								long mto = 0;
                                
								if (data.getString("iTxn").equals("0069"))
									mto = Long.parseLong(data.getCString("txtChequeCI"));
								else
									mto = Long.parseLong(data.getCString("txtCheque"));
                                
								if( monto < mto && generic.toString().startsWith("0")) 
								{
									Stack flujotxn = (Stack)session.getAttribute("page.flujotxn");
									flujotxn.push("1009");
								}

								if( data.getString("iTxn").equals("1009") && monto < mto && generic.toString().startsWith("1")) 
								{
									Stack flujotxn = (Stack)session.getAttribute("page.flujotxn");
									flujotxn.push("1009");
								}
							}
							if( data.getString("cTxn").equals("0528") ) 
							{
								long monto = 0L;
                                
								if( generic.toString().startsWith("0") ) {
									monto = data.getLong("MontoTotal", 0);
									monto += Long.parseLong(data.getCString("txtMonto"));
									datasession.put("MontoTotal", new Long(monto).toString());
									int nocheques = data.getInt("txtNoCheques");
									nocheques++;
									datasession.put("txtNoCheques",new Integer(nocheques).toString());
									if ( datasession.get("txtPrimerConsec") == null )
										datasession.put("txtPrimerConsec",result.substring(5, 12));
									if (data.getString("TxnSig").equals("0528")) {
										Stack flujotxn = (Stack)session.getAttribute("page.flujotxn");
										flujotxn.push("0528");
									}
                                    
								}
							}
						}
						catch( ParameterNotFoundException e ) {
							e.printStackTrace(System.out);
						}
						getServletContext().getRequestDispatcher("/servlet/ventanilla.Redirector").forward(request, response);
				}
			}
            
			else if(txnProcess.equals("01")) {
				if(iTxn.equals("1125") || /*iTxn.equals("1161") || */iTxn.equals("1504") || iTxn.equals("1506")
				|| iTxn.equals("1508") || iTxn.equals("1510") || iTxn.equals("1532") || iTxn.equals("5197")
				|| iTxn.equals("1067") || iTxn.equals("1512") || iTxn.equals("1059") || iTxn.equals("1235")
				|| iTxn.equals("1237") || iTxn.equals("5065") || iTxn.equals("1207") || iTxn.equals("1525")
				|| iTxn.equals("1550") || iTxn.equals("1275") || iTxn.equals("5487") || iTxn.equals("5553")
				|| iTxn.equals("4067") || iTxn.equals("1523") || iTxn.equals("5389") || iTxn.equals("1065")
				|| iTxn.equals("1515"))
					getServletContext().getRequestDispatcher("/servlet/ventanilla.Group01R").forward(request,response);
				else if(iTxn.equals("1238") || iTxn.equals("1240") || iTxn.equals("1027") || iTxn.equals("1057")
				|| iTxn.equals("1179") || iTxn.equals("5357") || iTxn.equals("5253") || iTxn.equals("4061")
				|| iTxn.equals("1197") || iTxn.equals("1091") || iTxn.equals("1193") || iTxn.equals("1195") || iTxn.equals("5571"))
					getServletContext().getRequestDispatcher("/servlet/ventanilla.Group02R").forward(request,response);
				else if(iTxn.equals("5053") || iTxn.equals("1075") || iTxn.equals("1077") || iTxn.equals("1053")
				|| iTxn.equals("1079") || iTxn.equals("1081") || iTxn.equals("1093")  || iTxn.equals("5573"))
					getServletContext().getRequestDispatcher("/servlet/ventanilla.Group03R").forward(request,response);
				else if(iTxn.equals("5353"))
					getServletContext().getRequestDispatcher("/servlet/Group04R").forward(request,response);
				else if(iTxn.equals("4453"))
					getServletContext().getRequestDispatcher("/servlet/ventanilla.Group05R").forward(request,response);
				else if (cTxn.equals("5453"))
					getServletContext().getRequestDispatcher("/servlet/ventanilla.Group03Remesas").forward(request,response);
				else if (cTxn.equals("5089") || cTxn.equals("5523"))
					getServletContext().getRequestDispatcher("/servlet/ventanilla.GroupATM1").forward(request,response);
                
			}
			else if(txnProcess.equals("02")) {
				if(cTxn.equals("0041") || cTxn.equals("0043") || cTxn.equals("0044") || cTxn.equals("0045") || cTxn.equals("0046") || cTxn.equals("0051"))
					getServletContext().getRequestDispatcher("/servlet/ventanilla.Group01TR").forward(request,response);
				if(iTxn.equals("0047") || iTxn.equals("0049"))
					getServletContext().getRequestDispatcher("/servlet/ventanilla.Group02TR").forward(request,response);
			}
			else if(txnProcess.equals("03")  || txnProcess.equals("25")) {
				if(cTxn.equals("0823"))
					getServletContext().getRequestDispatcher("/servlet/ventanilla.CV_0823").forward(request,response);
				else
					getServletContext().getRequestDispatcher("/servlet/ventanilla.CV" + "_" + iTxn + "_" + "XXXX").forward(request,response);
			}
			else if(txnProcess.equals("04")) {
				if(cTxn.equals("0024") || cTxn.equals("0025"))
					getServletContext().getRequestDispatcher("/servlet/ventanilla.Group02OPM").forward(request,response);
				else
					getServletContext().getRequestDispatcher("/servlet/ventanilla." + "OP" + "_" + iTxn).forward(request,response);
			}
			else if(txnProcess.equals("06")) {
				if(cTxn.equals("0815") || cTxn.equals("0572") || cTxn.equals("0578"))
					getServletContext().getRequestDispatcher("/servlet/ventanilla.GCBGroup01").forward(request,response);
				else if(cTxn.equals("0816") || cTxn.equals("0576"))
					getServletContext().getRequestDispatcher("/servlet/ventanilla.GCBGroup02").forward(request,response);
			}
			else if(txnProcess.equals("07")) 
			{
				if(cTxn.equals("0020"))
					getServletContext().getRequestDispatcher("/servlet/ventanilla.Group01WU").forward(request,response);
			}
			else if(txnProcess.equals("08")) {
				if (cTxn.equals("0806"))
					getServletContext().getRequestDispatcher("/servlet/ventanilla.PeiSAT").forward(request,response);
				if(cTxn.equals("0832") || cTxn.equals("S832")||cTxn.equals("C832")||cTxn.equals("A832"))
					getServletContext().getRequestDispatcher("/servlet/ventanilla.Group01RAP").forward(request,response);
				if(cTxn.equals("0836"))
					getServletContext().getRequestDispatcher("/servlet/ventanilla.Group04RAP").forward(request,response);
				if(cTxn.equals("5503") || cTxn.equals("RAPM"))
					getServletContext().getRequestDispatcher("/servlet/ventanilla.GroupRAP").forward(request,response);
				if(cTxn.equals("0027"))
					getServletContext().getRequestDispatcher("/servlet/ventanilla.Group01SAT").forward(request,response);    
				if(cTxn.equals("S027"))
					getServletContext().getRequestDispatcher("/servlet/ventanilla.Group02SAT").forward(request,response);        
				if(cTxn.equals("S503"))
					getServletContext().getRequestDispatcher("/servlet/ventanilla.GroupSAT").forward(request,response);
				if(cTxn.equals("5359"))
					getServletContext().getRequestDispatcher("/servlet/ventanilla.Group03ChqVj").forward(request,response);
				if(cTxn.equals("0604"))
					getServletContext().getRequestDispatcher("/servlet/ventanilla.Group03ARP").forward(request,response);
				if(cTxn.equals("0510"))
					getServletContext().getRequestDispatcher("/servlet/ventanilla.Group05RAP").forward(request,response);
				if(cTxn.equals("4223") || cTxn.equals("4225"))
					getServletContext().getRequestDispatcher("/servlet/ventanilla.Group01AR").forward(request,response);
				if(cTxn.equals("0030"))
					getServletContext().getRequestDispatcher("/servlet/ventanilla.Group01TDI").forward(request,response);
				if(cTxn.equals("P030"))
					getServletContext().getRequestDispatcher("/servlet/ventanilla.Group02TDI").forward(request,response);
				if(cTxn.equals("5361"))
					getServletContext().getRequestDispatcher("/servlet/ventanilla.Group06RAP").forward(request,response);
                if(cTxn.equals("ARPL")){
                    getServletContext().getRequestDispatcher("/servlet/ventanilla.GroupRAPCAPR").forward(request,response);
                }
                if(cTxn.equals("CAPR")){
                    getServletContext().getRequestDispatcher("/servlet/ventanilla.GroupCAPR").forward(request,response);
                }    
			}
			else if(txnProcess.equals("09")) {
				if (cTxn.equals("4529") || cTxn.equals("4531") || cTxn.equals("0856") || cTxn.equals("4011"))
					getServletContext().getRequestDispatcher("/servlet/ventanilla.Group01DAP").forward(request,response);
				if(iTxn.equals("0067"))
					getServletContext().getRequestDispatcher("/servlet/ventanilla.Group15Serv").forward(request,response);
				if(cTxn.equals("0854") || cTxn.equals("5561") || cTxn.equals("5565") || cTxn.equals("5563") || cTxn.equals("5567") || cTxn.equals("5569"))
					getServletContext().getRequestDispatcher("/servlet/ventanilla.Group01TIP").forward(request,response);
			}
			else if(txnProcess.equals("10")) {
				if(cTxn.equals("5103"))
					getServletContext().getRequestDispatcher("/servlet/ventanilla.Group01Remesas").forward(request,response);
				else if(cTxn.equals("5261") || cTxn.equals("5287"))
					getServletContext().getRequestDispatcher("/servlet/ventanilla.Group02Remesas").forward(request,response);
				else if(cTxn.equals("4487"))
					getServletContext().getRequestDispatcher("/servlet/ventanilla.Group03Remesas").forward(request,response);
				else if (cTxn.equals("M017"))
					getServletContext().getRequestDispatcher("/servlet/ventanilla.RedirectorMisc").forward(request,response);
			}
			else if(txnProcess.equals("11")) {
				if (cTxn.equals("1181") || cTxn.equals("1183") || cTxn.equals("1185") ||
				cTxn.equals("1387") || cTxn.equals("1189") || cTxn.equals("1385") ||
				cTxn.equals("1389") || cTxn.equals("4015"))
					getServletContext().getRequestDispatcher("/servlet/ventanilla.Group01Fidu").forward(request,response);
				if (cTxn.equals("M014") || cTxn.equals("M015"))
					getServletContext().getRequestDispatcher("/servlet/ventanilla.RedirectorMisc").forward(request,response);
                
			}
			else if(txnProcess.equals("12")) {
				if(cTxn.equals("4227") || cTxn.equals("4229") || cTxn.equals("M006") || cTxn.equals("M007"))
					getServletContext().getRequestDispatcher("/servlet/ventanilla.Group01NOM_N").forward(request,response);
				else if(cTxn.equals("1019"))
					getServletContext().getRequestDispatcher("/servlet/ventanilla.Group02NOM").forward(request,response);
				else if(cTxn.equals("5153"))
					getServletContext().getRequestDispatcher("/servlet/ventanilla.Group04NOM").forward(request,response);
			}
			else if(txnProcess.equals("13")) {
				if(iTxn.equals("0087"))
					getServletContext().getRequestDispatcher("/servlet/ventanilla.Group16Serv").forward(request,response);
				if(iTxn.equals("0728"))
					getServletContext().getRequestDispatcher("/servlet/ventanilla.Group04Serv").forward(request,response);
				if(iTxn.equals("0069") && !cTxn.equals("5353"))
					getServletContext().getRequestDispatcher("/servlet/ventanilla.Group01Serv").forward(request,response);
				if(iTxn.equals("0070") || iTxn.equals("0826"))
					getServletContext().getRequestDispatcher("/servlet/ventanilla.Group02Serv").forward(request,response);
				if(iTxn.equals("0520") && !cTxn.equals("5353"))
					getServletContext().getRequestDispatcher("/servlet/ventanilla.Group03Serv").forward(request,response);
				if(iTxn.equals("0630"))
               
					getServletContext().getRequestDispatcher("/servlet/ventanilla.Group05Serv").forward(request,response);
				if((iTxn.equals("0732") || iTxn.equals("0368") || iTxn.equals("0540")) && !cTxn.equals("5353"))
					if( (iTxn.equals("0732") && cTxn.equals("0732")) || (iTxn.equals("0732") && cTxn.equals("0124")) )
					{    
						getServletContext().getRequestDispatcher("/servlet/ventanilla.Group18Serv").forward(request,response);
					}
					else
						getServletContext().getRequestDispatcher("/servlet/ventanilla.Group06Serv").forward(request,response);
				if(iTxn.equals("0488") && !cTxn.equals("5353"))
					getServletContext().getRequestDispatcher("/servlet/ventanilla.Group07Serv").forward(request,response);
				if(iTxn.equals("0201") || iTxn.equals("0202"))
					getServletContext().getRequestDispatcher("/servlet/ventanilla.Group08Serv").forward(request,response);
				if(iTxn.equals("0204") || iTxn.equals("0206") || iTxn.equals("0208") || iTxn.equals("0212") || iTxn.equals("0214"))
					getServletContext().getRequestDispatcher("/servlet/ventanilla.Group09Serv").forward(request,response);
				if(iTxn.equals("4455") && !cTxn.equals("5353")) {
					if (session.getAttribute("txtfactor")==null)
						if (datasession.get("txtfactor")!=null)
							session.setAttribute("txtfactor", (String)datasession.get("txtfactor"));
					if (session.getAttribute("txttipoinf")==null)
						if (datasession.get("txttipoinf")!=null)
							session.setAttribute("txttipoinf", (String) datasession.get("txttipoinf"));
					if (session.getAttribute("txtmesinf")==null)
						if (datasession.get("txtmesinf")!=null)
							session.setAttribute("txtmesinf", (String) datasession.get("txtmesinf"));
					if (session.getAttribute("txtverif")==null)
						if (datasession.get("txtverif")!=null)
							session.setAttribute("txtverif", (String) datasession.get("txtverif"));
					getServletContext().getRequestDispatcher("/servlet/ventanilla.Group10Serv").forward(request,response);
				}
				if(iTxn.equals("5405") || iTxn.equals("5407"))
					getServletContext().getRequestDispatcher("/servlet/ventanilla.Group11Serv").forward(request,response);
				if(iTxn.equals("0090"))
					getServletContext().getRequestDispatcher("/servlet/ventanilla.Group12Serv").forward(request,response);
				if(iTxn.equals("0820"))
					getServletContext().getRequestDispatcher("/servlet/ventanilla.Group13Serv").forward(request,response);
				if(iTxn.equals("0730"))
					getServletContext().getRequestDispatcher("/servlet/ventanilla.Group14Serv").forward(request,response);
				if(iTxn.equals("0818"))
					getServletContext().getRequestDispatcher("/servlet/ventanilla.Group17Serv").forward(request,response);
				if(iTxn.equals("0530"))
					getServletContext().getRequestDispatcher("/servlet/ventanilla.Group19Serv").forward(request,response);
			}
			else if(txnProcess.equals("15")) {
				if(cTxn.equals("2503") || cTxn.equals("2523") || cTxn.equals("3009") || cTxn.equals("3077") ||
				cTxn.equals("3219") || cTxn.equals("3261") || cTxn.equals("3511") || cTxn.equals("3532") ||
				cTxn.equals("3533") || cTxn.equals("3559") || cTxn.equals("3001") || cTxn.equals("3487") ||
				cTxn.equals("3587") || cTxn.equals("3314") || cTxn.equals("2529"))
					getServletContext().getRequestDispatcher("/servlet/ventanilla.Group01I").forward(request,response);
				if (cTxn.equals("2525") || cTxn.equals("3075") || cTxn.equals("3105") || cTxn.equals("2527"))
					getServletContext().getRequestDispatcher("/servlet/ventanilla.Group02I").forward(request,response);
				if ( cTxn.equals("3313") || cTxn.equals("3021"))
					getServletContext().getRequestDispatcher("/servlet/ventanilla.Group03I").forward(request,response);
				if ( cTxn.equals("0544"))
					getServletContext().getRequestDispatcher("/servlet/ventanilla.Group04I").forward(request,response);
				if ( cTxn.equals("0542"))
					getServletContext().getRequestDispatcher("/servlet/ventanilla.Group05I").forward(request,response);    
				if ( cTxn.equals("0097"))
					getServletContext().getRequestDispatcher("/servlet/ventanilla.Group08I").forward(request,response);
				if ( cTxn.equals("M016"))
					getServletContext().getRequestDispatcher("/servlet/ventanilla.GroupMI").forward(request,response);    
				if ( cTxn.equals("INV1"))
					getServletContext().getRequestDispatcher("/servlet/ventanilla.GroupIRedirector").forward(request,response);
				if ( cTxn.equals("0063"))
					getServletContext().getRequestDispatcher("/servlet/ventanilla.Group06I").forward(request,response);
				if ( cTxn.equals("0044"))
					getServletContext().getRequestDispatcher("/servlet/ventanilla.Group01TR").forward(request,response);
				if(cTxn.equals("9815") || cTxn.equals("9803") || cTxn.equals("9809") || cTxn.equals("9711"))
					getServletContext().getRequestDispatcher("/servlet/ventanilla.Group07I").forward(request,response);
			}
			else if(txnProcess.equals("16")) {
				if(cTxn.equals("0011")||cTxn.equals("0013"))
					getServletContext().getRequestDispatcher("/servlet/ventanilla.TotalGroup1").forward(request,response);
				else if(cTxn.equals("0093"))
					 getServletContext().getRequestDispatcher("/servlet/ventanilla.Group20Serv").forward(request,response);    
			}
			else if(txnProcess.equals("17")) {
				if(cTxn.equals("0790") || cTxn.equals("0788"))
					getServletContext().getRequestDispatcher("/servlet/ventanilla.Group01ChqVj").forward(request,response);
				else if(cTxn.equals("0602") || cTxn.equals("0552"))
					getServletContext().getRequestDispatcher("/servlet/ventanilla.Group02ChqVj").forward(request,response);
				else if (cTxn.equals("V353") || cTxn.equals("5359"))
					getServletContext().getRequestDispatcher("/servlet/ventanilla.Group03ChqVj").forward(request,response);
				else if (cTxn.equals("0564"))
					getServletContext().getRequestDispatcher("/servlet/ventanilla.Group04ChqVj").forward(request,response);
			}
			else if(txnProcess.equals("18")) {
				if(cTxn.equals("5505") || cTxn.equals("5507") || cTxn.equals("5509") || cTxn.equals("5511"))
					getServletContext().getRequestDispatcher("/servlet/ventanilla.Group01CBA").forward(request,response);
			}
			else if(txnProcess.equals("19")) {
				if(cTxn.equals("4545"))
					getServletContext().getRequestDispatcher("/servlet/ventanilla.Group01ChqCertif").forward(request,response);
				if(cTxn.equals("4535"))
					getServletContext().getRequestDispatcher("/servlet/ventanilla.Group02ChqCertif").forward(request,response);
				if(cTxn.equals("4537"))
					getServletContext().getRequestDispatcher("/servlet/ventanilla.Group03ChqCertif").forward(request,response);
				if(cTxn.equals("4539"))
					getServletContext().getRequestDispatcher("/servlet/ventanilla.Group04ChqCertif").forward(request,response);
				if(cTxn.equals("4541"))
					getServletContext().getRequestDispatcher("/servlet/ventanilla.Group05ChqCertif").forward(request,response);
				if(cTxn.equals("4543"))
					getServletContext().getRequestDispatcher("/servlet/ventanilla.Group06ChqCertif").forward(request,response);
				if(cTxn.equals("4547"))
					getServletContext().getRequestDispatcher("/servlet/ventanilla.Group07ChqCertif").forward(request,response);
			}
			else if(txnProcess.equals("20")) {
				if(cTxn.equals("M008"))
					getServletContext().getRequestDispatcher("/servlet/ventanilla.Group02CFED").forward(request,response);
				if(cTxn.equals("PASS"))
					getServletContext().getRequestDispatcher("/servlet/Group01POOL").forward(request,response);
				if(cTxn.equals("9816"))
					getServletContext().getRequestDispatcher("/servlet/ventanilla.Group01Users").forward(request,response);
				if(cTxn.equals("N081") || cTxn.equals("N083"))
					getServletContext().getRequestDispatcher("/servlet/ventanilla.Group01Nafin").forward(request,response);
				if(cTxn.equals("G061") || cTxn.equals("G063"))
					getServletContext().getRequestDispatcher("/servlet/ventanilla.Group01Giros").forward(request,response);
    
			}
			else if(txnProcess.equals("21")) {
				if(cTxn.equals("0834"))
					getServletContext().getRequestDispatcher("/servlet/ventanilla.Group01VOU").forward(request,response);
				if(cTxn.equals("0066") || cTxn.equals("0072") || cTxn.equals("0073"))
					getServletContext().getRequestDispatcher("/servlet/ventanilla.Group03VOU").forward(request,response);
			}
			else if(txnProcess.equals("99")) {
				if(cTxn.equals("0093"))
					getServletContext().getRequestDispatcher("/servlet/ventanilla.Group01GTE").forward(request,response);
			}
			else if(txnProcess.equals("22")){
				getServletContext().getRequestDispatcher("/servlet/ventanilla.Group01AVA").forward(request,response);
			}
			else if(txnProcess.equals("26")){
					if(cTxn.equals("0823"))
					   getServletContext().getRequestDispatcher("/servlet/ventanilla.CV_0823").forward(request,response);
			}
			else if(txnProcess.equals("27")){
					if(cTxn.equals("P001") || cTxn.equals("P002"))
					getServletContext().getRequestDispatcher("/servlet/ventanilla.GroupPV").forward(request,response);
					session.setAttribute("PasoP001","sp");
			}
		}
		catch(Exception e) {
			e.printStackTrace(System.out);
		}
	}
    
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}
    
	private String execTransaction(Transaction generic, HttpSession session) throws ServletException, IOException {
		String liga = (String)session.getAttribute("d_ConsecLiga");
		String ip   = (String)session.getAttribute("dirip");
        
		Hashtable datasession = (Hashtable)session.getAttribute("page.datasession");
		String txtMoneda = (String)datasession.get("moneda");
		String Txn = (String)datasession.get("cTxn");
		String TxnE = (String)datasession.get("iTxn");
		String Txnint = "";
		if(session.getAttribute("Txnint") != null)                 
		   Txnint = session.getAttribute("Txnint").toString();
   
		String txtVolante = "NO";
		ventanilla.com.bital.util.VolanteoDDA oVolanteoDDA  = null;
        
		if ( Txn.equals("1009") || Txn.equals("0528") ) {
			Hashtable Truncamiento = (Hashtable)session.getAttribute("truncamiento");
			long TruncamientoMN = 500000L;
			long TruncamientoUSD = 50000L;
			if (!Truncamiento.isEmpty() ){
				TruncamientoMN = Long.parseLong(Truncamiento.get("TruncamientoMN").toString());
				TruncamientoUSD = Long.parseLong(Truncamiento.get("TruncamientoUSD").toString());
			}
			oVolanteoDDA = new ventanilla.com.bital.util.VolanteoDDA();
			String txtMonto = "";
			if ( Txn.equals("1009"))
				txtMonto = delCommaPointFromString((String)datasession.get("txtMontoCheque"));
			else
				txtMonto = delCommaPointFromString((String)datasession.get("txtMonto"));
			String txtBanco = (String)datasession.get("txtCveTran");
			txtBanco = txtBanco.substring(5,8);
			oVolanteoDDA.setTeller(datasession.get("teller").toString().substring(4,6));
			oVolanteoDDA.setBranch(datasession.get("sucursal").toString().substring(1));
			oVolanteoDDA.setBank(txtBanco);
			oVolanteoDDA.setCurrency((String)datasession.get("moneda"));
			oVolanteoDDA.setCorte((String)datasession.get("rdoACorte"));
			oVolanteoDDA.setChequeData((String)datasession.get("txtSerial2") +(String)datasession.get("txtDDACuenta2") + (String)datasession.get("txtCveTran") +
			(String)datasession.get("txtCodSeg"));
			oVolanteoDDA.setChequeAmount(txtMonto);
			if ( ( Long.parseLong(txtMonto.toString()) >= TruncamientoMN  && txtMoneda.equals("01")) ||
			( Long.parseLong(txtMonto.toString()) >= TruncamientoUSD  && txtMoneda.equals("02")) ) {
				txtVolante = "SI";
				datasession.put("Volante",oVolanteoDDA.getConsecutive());
			}
			datasession.put("txtVolante",txtVolante);
		}
        
		try {
			// Obtener referencia a la transaccion
			generic.perform(datasession);
		}
		catch( ParameterNotFoundException e ) {
			return "1~1~0000000~ERROR EN LA TRANSACCION~";
		}
        

		Diario diario = new Diario();
		String sMessage = diario.invokeDiario(generic.getProcess(), (String)session.getAttribute("d_ConsecLiga"), ip, txtMoneda, session);        
                
		/*if ( Txn.equals("1003")) {
			ventanilla.com.bital.util.searchForCRM cfCRM = new ventanilla.com.bital.util.searchForCRM();
			cfCRM.setAccountNumber((String)datasession.get("txtDDACuenta"));
			cfCRM.setBranchNumber((String)datasession.get("sucursal"));
			cfCRM.setTellerName("SOMEONE");
			cfCRM.setTellerNumber((String)datasession.get("teller"));
			cfCRM.executeCISSearch();
			if(cfCRM.getCISNumber().length() != 0){
				cfCRM.executeDBSearch();
				if(!cfCRM.getCustomerInfo().isEmpty()){
					session.setAttribute("crmInfo", cfCRM.getCustomerInfo());
				}
			}
		}*/
        
		String result = sMessage;
        
		if (Txn.equals("0096") && result.charAt(0) == '0')
			session.setAttribute("txtCadImpresion", "NOIMPRIMIRNOIMPRIMIR");
		if (Txn.equals("0566") && Txnint.equals("0566") && result.charAt(0) == '0'){   
			datasession.put("txtNumOrden", result.substring(247,259));        
		}        	        
		if (Txn.equals("4097") && result.charAt(0) == '0'){
			datasession.put("txtNumOrden", result.substring(327,339));
			datasession.put("txtComision", result.substring(40,53));
			datasession.put("txtIva", result.substring(118,131));
			//datasession.put("txtIva", "0000000000025");
			datasession.put("txtOrdenante", result.substring(91,118));
		}
		if (Txn.equals("4033") && Txnint.equals("4033") && result.charAt(0) == '0'){        
			datasession.put("txtComisionMN", result.substring(40,53));
			datasession.put("txtIvaMN", result.substring(118,131));
			datasession.put("txtNumOrden", result.substring(327,339));
			datasession.put("txtNomOrd", result.substring(91,118));          
		}
		if (Txn.equals("0860") && Txnint.equals("0860") && result.charAt(0) == '0'){      
			datasession.put("txtNumOrden", result.substring(247,259));        
		}
                        	                
			if( result.charAt(0) == '0' ) {
				if (Txn.equals("1009") || Txn.equals("0528")) {
					oVolanteoDDA.updateConsecutive(result.substring(5, 12), txtVolante);
				}
				if (Txn.equals("4009")){
					session.setAttribute("txtCadImpresion", "~FICDECOB4009~" + datasession.get("moneda").toString() + "~" +
					result.substring(91, 131)  + "~" + datasession.get("txtDDACuenta").toString()
					+ "~" + datasession.get("txtPrimerConsec").toString() + "~" + datasession.get("txtNoCheques").toString()
					+ "~" + delCommaPointFromString(datasession.get("txtMontoT").toString()) +"~");
				}	            
				if(((String)datasession.get("oTxn")).equals("0594")){
					String txtDDACuenta1 = (String)datasession.get( "txtDDACuenta" )==null?"":(String)datasession.get( "txtDDACuenta" );   		
					String OPI = (String)session.getAttribute("numOPEE");
					String txtNomCliente = (String)session.getAttribute("ordenante");
					String Monto = (String)datasession.get("txtMonto1")==null?"":(String)datasession.get("txtMonto1");        	
					String txtCadImpresion = "~EXPORDUSD~" + OPI + "~" + txtNomCliente + "~" +
					Monto + "~" + txtDDACuenta1 + "~" + "" + "~";        
	        	
					if(txtCadImpresion.length() > 0){
						session.setAttribute("txtCadImpresion", txtCadImpresion);
					}
				}
	        
				if(Txn.equals("5161")){
					String txtDDACuenta1 = (String)datasession.get( "txtDDACuenta" )==null?"":(String)datasession.get( "txtDDACuenta" );
					String OPI = (String)session.getAttribute("numOPEE")==null?"":(String)session.getAttribute("numOPEE");
					String Monto = (String)datasession.get("txtMonto")==null?"":(String)datasession.get("txtMonto");
					String comision = (String)session.getAttribute("comision")==null?"":(String)session.getAttribute("comision");
					String iva = (String)session.getAttribute("Iva")==null?"":(String)session.getAttribute("Iva");        	
					String txtNomCliente = (String)session.getAttribute("ordenante")==null?"":(String)session.getAttribute("ordenante");        	
					String txtCadImpresion = "~CARGO~US$~" + OPI + "~" + delCommaPointFromString(Monto) + "~" + 
					Integer.parseInt(comision) + "~" + Integer.parseInt(iva) + "~" + delCommaPointFromString(Monto) + "~" +
					"" + "~" + txtNomCliente + "~" + txtDDACuenta1 + "~" + txtNomCliente + "~" + "" + "~";        	
					
					if(txtCadImpresion.length() > 0){ 
						session.setAttribute("txtCadImpresion", txtCadImpresion);
					}
				}       
		        
		      
		        /*if(Txn.equals("4193") || Txn.equals("0270")){
		           
				   GenericClasses gc = new GenericClasses();
		           
				   String plaza = datasession.get( "txtPlazaSuc" ).toString().trim();		           
				  // String moneda= datasession.get("moneda").toString().trim();	
				   String moneda="02";
				   moneda.trim();              
				   String cuenta = datasession.get( "txtCuenta" ).toString().trim();		           
				   String sucursal = datasession.get( "sucursal" ).toString().trim();		                      
		           	          
				   //String monto = gc.formatMonto((String)datasession.get( "txtMonto" ));
				   String monto = datasession.get( "txtMonto" ).toString();
				   monto = monto.trim();
				   System.out.println("monto:"+monto);
		           
				   String beneficiario = (String)datasession.get( "txtBeneficiario" ).toString().trim();
				   String cheque = (String)datasession.get( "txtSerial" ).toString().trim();            
                                      
				   String txtCadImpresion = "~CHQGER"+ "~"+ beneficiario;                 
				   txtCadImpresion = txtCadImpresion + "~"+ moneda;
				   txtCadImpresion = txtCadImpresion + "~"+ monto;
				   txtCadImpresion = txtCadImpresion + "~"+ cheque;
				   txtCadImpresion = txtCadImpresion + "~"+ sucursal;
				   txtCadImpresion = txtCadImpresion + "~"+ cuenta;
				   txtCadImpresion = txtCadImpresion + "~";
                   
                                
					if(txtCadImpresion.length() > 0){ 
						session.setAttribute("txtCadImpresion", txtCadImpresion);
					}
				}      */ 
				
				
				//if(Txn.equals("0136")){
					
					
						
					if(Txn.equals("0136") && Txnint.equals("0136") && result.charAt(0) == '0')
					{						
						 cger_imp=true;
				        
					}
						
					if(Txn.equals("0136") && Txnint.equals("4209") && result.charAt(0) == '0' && cger_imp ==true)
					{
				       	   GenericClasses gc = new GenericClasses();
				           
				           String plaza = datasession.get( "txtPlazaSuc" ).toString().trim();
				           
			               String moneda= datasession.get("moneda").toString().trim();		             
		 	               
			               
				           String cuenta = datasession.get( "txtCuenta" ).toString().trim();		           
				           String sucursal = datasession.get( "sucursal" ).toString().trim();		                      
				           	          
						   String monto = delCommaPointFromString((String)datasession.get( "txtMonto" ));
						   
						   
				           
						   String beneficiario = (String)datasession.get( "txtBeneficiario" ).toString().trim();
						   String cheque = (String)datasession.get( "txtSerial" ).toString().trim(); 
						   
						   
						   String detalle = (String)datasession.get( "txtDetalle" ).toString().trim();
						   String adress = (String)datasession.get( "adress" ).toString().trim();
						   String pautorizada = (String)datasession.get( "txtPAutorizado" ).toString().trim();
						   String cedula = (String)datasession.get( "txtCedula" ).toString().trim();
						   				   
						   //System.out.println("cadena" + detalle +" "+ adress +" "+ pautorizada +" "+ cedula);
		                                      
						   String txtCadImpresion = "CHQGER"+ "~"+ beneficiario;                 
						   txtCadImpresion = txtCadImpresion + "~"+ moneda;
						   txtCadImpresion = txtCadImpresion + "~"+ monto;
						   txtCadImpresion = txtCadImpresion + "~"+ cheque;
						   txtCadImpresion = txtCadImpresion + "~"+ sucursal;
						   txtCadImpresion = txtCadImpresion + "~"+ cuenta;
						   //mod				   
						   txtCadImpresion = txtCadImpresion + "~"+ detalle;
						   txtCadImpresion = txtCadImpresion + "~"+ adress;
						   txtCadImpresion = txtCadImpresion + "~"+ pautorizada;
						   txtCadImpresion = txtCadImpresion + "~"+ cedula; 
						   //mod			   
						   txtCadImpresion = txtCadImpresion + "~";
		                   
		                                
							if(txtCadImpresion.length() > 0){ 
								session.setAttribute("txtCadImpresion", txtCadImpresion);
							}
				        	
						}   
		        							   
					
				//}      
				if(Txn.equals("ECGD")){
					
					
		           
				   GenericClasses gc = new GenericClasses();
		           
		           String plaza = datasession.get( "txtPlazaSuc" ).toString().trim();		           
	               String moneda= datasession.get("moneda").toString().trim();		             
 	                             
		           String cuenta = "85490000000000027777";//datasession.get( "txtCuenta" ).toString().trim();		           
		           String sucursal = datasession.get( "sucursal" ).toString().trim();		                      
		           	          
				   String monto = delCommaPointFromString((String)datasession.get( "txtMonto" ));
				   //String monto = datasession.get( "txtMonto" ).toString();
				   monto = monto.trim();
		           
				   String beneficiario = (String)datasession.get( "txtBenefiCheq" ).toString().trim();
				   String cheque = (String)datasession.get( "txtSerialCheq" ).toString().trim();            
				   
				   //Cambios
				   String detalle = (String)datasession.get( "txtDetallePago" ).toString().trim();
				   String adress = (String)datasession.get( "txtDomiciB" ).toString().trim();
				   
				   if(detalle.equals("") || detalle==  null)
				   		detalle="";
				   if(adress.equals("")|| adress == null)
				   		adress="";
				   //
                                      
				   String txtCadImpresion = "CHQGERE"+ "~"+ beneficiario;                 
				   txtCadImpresion = txtCadImpresion + "~"+ moneda;
				   txtCadImpresion = txtCadImpresion + "~"+ monto;
				   txtCadImpresion = txtCadImpresion + "~"+ cheque;
				   txtCadImpresion = txtCadImpresion + "~"+ sucursal;
				   txtCadImpresion = txtCadImpresion + "~"+ cuenta;
				   
				   //Cambios
				   txtCadImpresion = txtCadImpresion + "~"+ detalle;
				   txtCadImpresion = txtCadImpresion + "~"+ adress;
				   //
				   txtCadImpresion = txtCadImpresion + "~";
                   
                   //System.out.println("txtcadimpresion: "+ txtCadImpresion);
                                
					if(txtCadImpresion.length() > 0){ 
						session.setAttribute("txtCadImpresion", txtCadImpresion);
					}
				}       	

				if(Txn.equals("0566") && Txnint.equals("0354")){
				   String monto = delCommaPointFromString((String)datasession.get( "txtMonto" ));
				   String comision = delCommaPointFromString((String)datasession.get( "montoComision" ));
				   String iva="000";
				   if(datasession.get( "montoIVA" ) != null)
						iva = delCommaPointFromString((String)datasession.get( "montoIVA" ));
				   String total = "" + ( Long.parseLong(monto) + Long.parseLong(comision) + Long.parseLong(iva) );
				   long dTotal = Long.parseLong(comision) + Long.parseLong(iva);
				   String ordenante = (String)datasession.get( "txtNomOrd" ) + " " + (String)datasession.get( "txtApeOrd" );
				   String beneficiario = (String)datasession.get( "txtNomBen" ) + " " + (String)datasession.get( "txtApeBen" );
				   String txtCadImpresion = "~EFECTIVO~N$~" + datasession.get("txtNumOrden") + "~";
				   txtCadImpresion = txtCadImpresion + monto + "~" + comision + "~" + iva + "~" + total + "~";
				   txtCadImpresion = txtCadImpresion + beneficiario + "~" + ordenante + "~0000000000~NOMCORTO";
				   String thisBank = "";
				   txtCadImpresion = txtCadImpresion + "~" + thisBank + "~";
				   String ref = (String)datasession.get( "txtReferOP" );
				   txtCadImpresion = txtCadImpresion + ref + "~";
				   /*
				   if (datasession.get("lstdeIVACOM").toString().equals("1"))
						{
							GenericClasses gc = new GenericClasses();  
							txtCadImpresion += "COMPCOM~" + datasession.get("txtNombre") + "~" + datasession.get("txtRFC01").toString() + "~" + "CHEQUES" + "~" + "EXP. ORDEN PAGO" + "~" + datasession.get("txtDomici") + "~";
							txtCadImpresion += gc.addpunto(datasession.get( "montoComision" ).toString()) + "~" + gc.addpunto(datasession.get( "montoIVA" ).toString()) + "~";
							txtCadImpresion += gc.addpunto(Long.toString(dTotal)) + "~" + datasession.get("domiSuc") + "~";
						}
						*/
					 if(txtCadImpresion.length() > 0){
						session.setAttribute("txtCadImpresion", txtCadImpresion);
					 }            
		        	 
		                    		        	
		        			        
				}

				if(Txn.equals("4097")){
					   GenericClasses gc = new GenericClasses();  		        
						String monto = gc.delCommaPointFromString((String)datasession.get( "txtMonto" ));
						String comision = new Long(Long.parseLong(datasession.get("txtComision").toString())).toString();//*
						//String iva="000";
						String iva=new Long(Long.parseLong(datasession.get("txtIva").toString())).toString();
						
						if(datasession.get( "montoIVA" ) != null)
						 	iva = delCommaPointFromString((String)datasession.get( "montoIVA" ));
						String total = "" + ( Long.parseLong(monto) + Long.parseLong(comision) + Long.parseLong(iva) );
						//long dTotal = ( Long.parseLong(monto) + Long.parseLong(comision) + Long.parseLong(iva) ) / 100;
						String ordenante = datasession.get("txtOrdenante").toString().trim();
						String beneficiario = (String)datasession.get( "txtNomBen" ) + " " + (String)datasession.get( "txtApeBen" );
						String txtCadImpresion = "~CARGO~N$~" + datasession.get("txtNumOrden") + "~";
						txtCadImpresion = txtCadImpresion + monto + "~" + comision + "~" + iva + "~" + total + "~";
						txtCadImpresion = txtCadImpresion + beneficiario + "~" + ordenante + "~" + datasession.get( "txtDDACuenta" ).toString() + "~" + ordenante;
						String thisBank = "";
						txtCadImpresion = txtCadImpresion + "~" + thisBank + "~" ;
						String ref = (String)datasession.get( "txtReferOP" );
						txtCadImpresion = txtCadImpresion + ref + "~";
						/*
					//System.out.println("lstCF" + datasession.get("lstCF"));
					if(datasession.get("lstCF").toString().equals("1"))
					{
					   session.removeAttribute("CFESP");
					   session.setAttribute("CFESP",(String)datasession.get("txtCveRFC") + "~" + (String)datasession.get("txtImporteIVA"));
					}	
                    */
					if(txtCadImpresion.length() > 0){
						session.setAttribute("txtCadImpresion", txtCadImpresion);
					}                        		        	                    	        
		        
				}




				if(Txn.equals("5217")){
		           
				   GenericClasses gc = new GenericClasses();	           
				   String moneda= datasession.get( "moneda" ).toString().trim();	        
				   String cuenta = datasession.get( "txtDDACuenta" ).toString().trim();		           		                              
				   String monto = gc.delCommaPointFromString((String)datasession.get( "txtMonto" ));
				   monto = monto.trim();
				   String tipoCargo = datasession.get( "lstTipoCargo" ).toString();
				   String consecutivo = sMessage.substring(6,12);
                                      
				   String txtCadImpresion = "DEBITO" + "~" + moneda;                 
				   txtCadImpresion = txtCadImpresion + "~" + cuenta;
				   txtCadImpresion = txtCadImpresion + "~" + monto;
				   txtCadImpresion = txtCadImpresion + "~" + tipoCargo;
				   txtCadImpresion = txtCadImpresion + "~" + consecutivo;
				   txtCadImpresion = txtCadImpresion + "~";
				   
				   if(txtCadImpresion.length() > 0)
					   session.setAttribute("txtCadImpresion", txtCadImpresion);
				}	   


				if(Txn.equals("5219")){
		           
				   GenericClasses gc = new GenericClasses();	           
				   String moneda= datasession.get( "moneda" ).toString();	        
				   String cuenta = datasession.get( "txtDDACuenta" ).toString();		           		                              
				   String monto = gc.delCommaPointFromString((String)datasession.get( "txtMonto" ));
				   String tipoCargo = datasession.get( "lstTipoAbono" ).toString(); 
				   String consecutivo = sMessage.substring(6, 12);
                                      
				   String txtCadImpresion = "CREDITO"+ "~" + moneda;                 
				   txtCadImpresion = txtCadImpresion + "~" + cuenta;
				   txtCadImpresion = txtCadImpresion + "~" + monto;
				   txtCadImpresion = txtCadImpresion + "~" + tipoCargo;
				   txtCadImpresion = txtCadImpresion + "~" + consecutivo;
				   txtCadImpresion = txtCadImpresion + "~";
				   
				   if(txtCadImpresion.length() > 0)
					   session.setAttribute("txtCadImpresion", txtCadImpresion);
				}
								
				if(Txn.equals("0101")){
					GenericClasses gc = new GenericClasses();
					String TipoChqr = datasession.get( "lstTipoCheq" ).toString();
					String SucDestino = datasession.get( "lstSucursal" ).toString();
					//System.out.println("lstTipoCheq: " + TipoChqr);
					//System.out.println("lstSucursal: " + SucDestino);
					
					String CFESP = "101~" + TipoChqr + "~" + SucDestino + "~";
					if(CFESP.length() > 0)
						session.setAttribute("CFESP", CFESP);
				}

				if(Txn.equals("0568") || Txn.equals("4099")){
					String monto = delCommaPointFromString((String)datasession.get( "txtMonto" ));
					String comision = new String("000");
					String iva = new String("000");
					String total = new String("000");
					String ordenante = (String)datasession.get("txtNomOrd"); //+ " " + (String)datasession.get("txtApeOrd");
					String beneficiario = (String)datasession.get("txtNomBen"); //+ " " + (String)datasession.get("txtApeBen");
					String cuenta = new String("");
					if(datasession.get( "txtDDACuenta3" ) != null)
					   cuenta = datasession.get( "txtDDACuenta3" ).toString();
                    
					String TipoTxn = "~ABONO~N$~";   
					if(datasession.get("txtStatusOrden").toString().equals("1")){
					   TipoTxn = "~LIQUIDACION~N$~"; 
					}else{
					   TipoTxn = "~CANCELACION~N$~";
					}	  
					String txtCadImpresion = TipoTxn + (String)datasession.get( "txtNumOrden" ) + "~";
					txtCadImpresion = txtCadImpresion + monto + "~" + comision + "~" + iva + "~" + total + "~";
					txtCadImpresion = txtCadImpresion + beneficiario + "~" + ordenante + "~" + cuenta + "~" + ordenante;
					txtCadImpresion = txtCadImpresion + "~~~";		        

					if(txtCadImpresion.length() > 0){
						session.setAttribute("txtCadImpresion", txtCadImpresion);
					}                        		        	                    	      		        	
				} 
		        
				if(Txn.equals("0566") ){
					if(datasession.get("lstForPago").equals("02"))
					   {
				//CargoCta
				
						String monto = delCommaPointFromString((String)datasession.get( "txtMonto" ));
						String comision = new String("000");
						String iva = new String("000");
						String total = new String("000");
						String ordenante = (String)datasession.get("txtNomOrd") + " " + (String)datasession.get("txtApeOrd");
						String beneficiario = (String)datasession.get("txtNomBen")+ " " + (String)datasession.get("txtApeBen");
						String cuenta = new String("");
						String TipoTxn = "~CARGO~N$~"; 
						if(datasession.get( "txtDDACuenta3" ) != null)
											   cuenta = datasession.get( "txtDDACuenta3" ).toString();
                    
						String txtCadImpresion = TipoTxn + (String)datasession.get( "txtNumOrden" ) + "~";
												txtCadImpresion = txtCadImpresion + monto + "~" + comision + "~" + iva + "~" + total + "~";
												txtCadImpresion = txtCadImpresion + beneficiario + "~" + ordenante + "~" + cuenta + "~" + ordenante;
												txtCadImpresion = txtCadImpresion + "~~";		        

						if(txtCadImpresion.length() > 0){
							session.setAttribute("txtCadImpresion", txtCadImpresion);
							}                        		        	                    
						} 
							
					if(datasession.get("lstForPago").equals("02"))//Efectivo
					{
						String monto = delCommaPointFromString((String)datasession.get( "txtMonto" ));
						String comision = new String("000");
						String iva = new String("000");
						String total = new String("000");
						String ordenante = (String)datasession.get("txtNomOrd")+ " " + (String)datasession.get("txtApeOrd");
						String beneficiario = (String)datasession.get("txtNomBen")+ " " + (String)datasession.get("txtApeBen");
						String cuenta = new String("");
						String TipoTxn = "~EFECTIVO~N$~"; 
						
						String txtCadImpresion = TipoTxn + (String)datasession.get( "txtNumOrden" ) + "~";
											txtCadImpresion = txtCadImpresion + monto + "~" + comision + "~" + iva + "~" + total + "~";
											txtCadImpresion = txtCadImpresion + beneficiario + "~" + ordenante + "~" + cuenta + "~" + ordenante;
											txtCadImpresion = txtCadImpresion + "~~";	        

						if(txtCadImpresion.length() > 0){
								session.setAttribute("txtCadImpresion", txtCadImpresion);
							}                        		        	                    
					  } 
								                		        	                    	      		        	
				} 
		        
				if(Txn.equals("4033") && Txnint.equals("0821")){
						GenericClasses gc = new GenericClasses();		        	
						String monto = delCommaPointFromString((String)datasession.get( "txtMonto" ));
						String comision = String.valueOf(Integer.parseInt(datasession.get("txtComisionMN").toString()));
						String iva = String.valueOf(Integer.parseInt(datasession.get("txtIvaMN").toString()));
						String ordenante = datasession.get("txtNomOrd").toString().trim(); // + " " + datasession.get( "txtApeOrd" ).toString().trim();
						String beneficiario = datasession.get( "txtNomBen" ).toString().trim() + " " + datasession.get( "txtApeBen" ).toString().trim();
						String montoUSD = delCommaPointFromString((String)datasession.get("txtMonto1"));
						String TipCam = gc.StringFiller((String)datasession.get("txtTipoCambio"), ((String)datasession.get( "txtTipoCambio" )).length()+2, false, "0");
						TipCam = "0000" + TipCam;
						TipCam = TipCam.substring(TipCam.length()-7);
						TipCam = TipCam.substring(0,3) + "." + TipCam.substring(3,6);
						String txtCadImpresion = "~CARGO~N$~" + datasession.get("txtNumOrden") + "~";
						txtCadImpresion = txtCadImpresion + monto + "~" + comision + "~" + iva + "~" + monto + "~";
						txtCadImpresion = txtCadImpresion + beneficiario + "~" + ordenante + "~" + datasession.get( "txtDDACuenta" ).toString() + "~" + ordenante + "~"+ "~"+ TipCam + "~"+ montoUSD + "~";		        

					if(txtCadImpresion.length() > 0){
						session.setAttribute("txtCadImpresion", txtCadImpresion);
					}                        		        	                    	      		    		        			        
				}
				if(TxnE.equals("4155") && Txnint.equals("0372") ) {
				
				String resp = sMessage.substring(sMessage.indexOf("CONSECUTIVO:"),sMessage.length()-1) ;

				String montoCheq = delCommaPointFromString((String)datasession.get("txtMonto"));
				String montoCom = delCommaPointFromString((String)datasession.get("txtComision"));
				String montoImp = delCommaPointFromString((String)datasession.get("txtIVA"));
				int totalMonto = Integer.valueOf(montoCom).intValue()+Integer.valueOf(montoImp).intValue()+Integer.valueOf(montoCheq).intValue();
				String strTotalMonto=delCommaPointFromString(String.valueOf(totalMonto));
				String serial = delCommaPointFromString((String)datasession.get("txtSerialCheq"));
				String txtCadImpresion = "~ITBMS~4155~"+montoCheq+"~"+montoCom+"~"+montoImp+"~"+strTotalMonto+"~"+serial+"~";
				
				if(txtCadImpresion.length() > 0){
					session.setAttribute("txtCadImpresion", txtCadImpresion);
				}	
				}
				
				if(TxnE.equals("1695")){// Cargo a Cuenta
					String txtMonto = delCommaPointFromString((String)datasession.get("txtMonto"));
					String txtTitular = sMessage.substring(47,70);
					String ITBMS = sMessage.substring(sMessage.length()-12,sMessage.length()-1);
					StringBuffer tmp = new StringBuffer(ITBMS);
		        	
		        	for(int y=0; y<tmp.length();)
		            {
		                if(tmp.charAt(y) == '0')
		                    tmp.deleteCharAt(y);
		                else
		                    break;
		            }
		            String tem = tmp.toString();
		            tem = tem.trim();
		            if(tem.length() == 0)
		            	tem = "0";
		            long mtoComision = Long.parseLong(txtMonto);
		            long mtoITBMS = Long.parseLong(tem.toString().substring(0, tem.toString().length()-2));
		            mtoITBMS = (mtoComision * mtoITBMS) / 100;
					String txtDescrip = delCommaPointFromString((String)datasession.get("lstTipoComision"));
					//System.out.println("********AQUI--->titular="+txtTitular+"********AQUI--->Comision"+txtDescrip);
					String txtCadImpresion = "~ITBMS~" + iTxn + "~" + txtMonto + "~" + mtoITBMS + "~" + (mtoComision + mtoITBMS) + "~" + txtTitular + "~"+txtDescrip+"~"+(String)datasession.get("txtReferITBMS")+"~";
					if(txtCadImpresion.length() > 0){
						session.setAttribute("txtCadImpresion", txtCadImpresion);
					}
				}
				
				if((TxnE.equals("0128") || TxnE.equals("0130")) && Txnint.equals("0821")){
					GenericClasses gc = new GenericClasses();
					String txtMonto = delCommaPointFromString((String)datasession.get("txtMonto"));
					String txtMonto1 = delCommaPointFromString((String)datasession.get("txtMonto1"));
					String txtTipoCambio = gc.StringFiller((String)datasession.get("txtTipoCambio"), ((String)datasession.get( "txtTipoCambio" )).length()+2, false, "0");
					txtTipoCambio = "0000" + txtTipoCambio;
					txtTipoCambio = txtTipoCambio.substring(txtTipoCambio.length()-7);
					txtTipoCambio = txtTipoCambio.substring(0,3) + "." + txtTipoCambio.substring(3,6);
					String txtDDACuenta2 = (String)datasession.get( "txtDDACuenta" );
					String txtSerial = (String)datasession.get( "txtNumOrden" );
					String txtNomCliente = (String)datasession.get( "txtNomBen" ); //+ " " + (String)datasession.get( "txtApeBen" );
					String txtCadImpresion = "~COMPRAVENTA~" + iTxn +"-" + oTxn +"~" + txtNomCliente + "~" +
					txtMonto + "~" + txtTipoCambio + "~" + txtMonto1 + "~" + txtSerial + "~" +
					" " + "~" + txtDDACuenta2 + "~" + "" + "~" + "" + "~"  +
					" " + "~" + " " + "~" + " " + "~" + (String)session.getAttribute("page.moneda") + "~";		        
		        
					if(txtCadImpresion.length() > 0){
						session.setAttribute("txtCadImpresion", txtCadImpresion);
					}	
					Stack flujotxn = (Stack)session.getAttribute("page.flujotxn");
					flujotxn.clear();
					session.setAttribute("page.flujotxn",flujotxn);		        		        
				}	
			else{
				if(Txn.equals("5161")){
					session.setAttribute("txtCadImpresion", result);
				}
			}
			}
	        
		return result;
	}
    
	private String trimValue(String value) {
		int len = value.length();
		return value.substring(0, len-1);
	}
    
	private String delCommaPointFromString(String newCadNum) {
		String nCad = new String(newCadNum);
		if( nCad.indexOf(".") > -1) {
			nCad = nCad.substring(nCad.indexOf(".") + 1, nCad.length());
			if(nCad.length() != 2) {
				String szTemp = new String("");
				for(int j = nCad.length(); j < 2; j++)
					szTemp = szTemp + "0";
				newCadNum = newCadNum + szTemp;
			}
		}
        
		StringBuffer nCadNum = new StringBuffer(newCadNum);
		for(int i = 0; i < nCadNum.length(); i++)
			if(nCadNum.charAt(i) == ',' || nCadNum.charAt(i) == '.')
				nCadNum.deleteCharAt(i);
        
		return nCadNum.toString();
	}
}


