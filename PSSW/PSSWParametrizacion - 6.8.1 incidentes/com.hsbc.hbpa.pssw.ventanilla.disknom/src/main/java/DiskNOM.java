import java.applet.Applet;
import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.Color;
import java.awt.Label;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Vector;

import netscape.javascript.JSObject;

public class DiskNOM extends Applet
  implements ActionListener
{
  Label Mensaje;
  Label TMP1;
  Label TMP2;
  Button ReadDisk;
  Vector lineas;
  Color color;

  public void init()
  {
    this.color = new Color(255, 255, 255);

    super.setLayout(new BorderLayout());

    this.Mensaje = new Label("Por favor, inserte el Disquete en la unidad A:                         ");
    this.Mensaje.setBackground(this.color);
    super.add("South", this.Mensaje);
    this.ReadDisk = new Button("Lectura Disquete");
    this.ReadDisk.setSize(10, 10);
    this.ReadDisk.addActionListener(this);
    super.add("Center", this.ReadDisk);
    this.TMP1 = new Label("               ");
    this.TMP1.setBackground(this.color);
    super.add("East", this.TMP1);
    this.TMP2 = new Label("               ");
    this.TMP2.setBackground(this.color);
    super.add("West", this.TMP2);
  }

  public void actionPerformed(ActionEvent accion)
  {
    if (accion.getActionCommand() != "Lectura Disquete")
      return;
    this.Mensaje.setText("Leyendo Disquete, por favor espere...");
    this.ReadDisk.setEnabled(false);
    try
    {
      lectura();
    }
    catch (FileNotFoundException e)
    {
      this.Mensaje.setText("El archivo de nomina no existe...");
    }
    catch (IOException e)
    {
      this.Mensaje.setText("Error de lectura, en el Disquete...");
    }
  }

  public void lectura()
    throws FileNotFoundException, IOException
  {
    File archivo = new File("a:\\nomina.txt");

    if (!(archivo.exists()))
    {
      this.Mensaje.setText("El archivo nomina.txt no existe...");
      this.ReadDisk.setEnabled(true);
      return;
    }

    FileReader fr = new FileReader(archivo);
    BufferedReader input = new BufferedReader(fr);

    int i = 0;
    int flag = 0;

    this.lineas = new Vector();
    while (true)
    {
      i += 1;
      String linea = input.readLine();
      if (linea == null)
        break;
      if (linea.length() > 22)
      {
        this.Mensaje.setText("Disquete rechazado, error en linea " + i + " mayor a 22 posiciones...");
        flag = 1;
        break;
      }
      if ((linea.length() < 22) && (linea.length() != 0))
      {
        this.Mensaje.setText("Disquete rechazado, error en linea " + i + " menor a 22 posiciones...");
        flag = 1;
        break;
      }
      if (linea.length() != 0) {
        this.lineas.addElement(linea);
      }
      int size1 = this.lineas.size();
      JSObject win1 = JSObject.getWindow(this);
      String depnom = "";
      depnom = (String)win1.eval("document.entry.depnom.value");

      int depmax = Integer.parseInt(depnom);

      if (size1 > depmax)
      {
        this.Mensaje.setText("Disquete rechazado, rebasa número máximo de Depósitos...");
        flag = 1;
      }
    }

    if (flag != 0)
      return;
    int size = this.lineas.size();

    Long mto = new Long(0L);
    Long cargo = new Long(0L);
    long abonos = 0L;
    String cuentas = "";
    String montos = "";
    String ccargo = "";
    String cta = "";

    for (i = 0; i < size; ++i)
    {
      cta = this.lineas.elementAt(i).toString().substring(0, 10);

      if (!(ValidDDA(cta)))
      {
        this.Mensaje.setText("Disquete rechazado, cuenta invalida en linea " + i + " ...");
        return;
      }

      cuentas = cuentas + cta + "*";

      mto = Long.valueOf(this.lineas.elementAt(i).toString().substring(10, 21));

      if (i == 0)
      {
        cargo = mto;
        ccargo = cta;
      }
      else {
        abonos += mto.longValue();
      }
      montos = montos + mto.toString() + "*";
    }

    if (abonos == cargo.longValue())
    {
      this.Mensaje.setText("Disquete aceptado. Presione Continuar ...");

      JSObject win = JSObject.getWindow(this);

      win.eval("document.entry.ctacargo.value ='" + ccargo + "'");

      int sizec = cuentas.length();

      int sizem = montos.length();

      int cortec = sizec / 11 / 5;

      int cortem = sizem / 5;

      String cadenacta = "";
      String cadenamto = "";
      int endc = 0;
      int endm = 0;
      for (int j = 0; j < 5; ++j)
      {
        endc += cortec * 11;
        endm += cortem;
        if (j == 4)
        {
          cadenacta = cuentas.substring(j * cortec * 11, sizec);

          cadenamto = montos.substring(j * cortem, sizem);
        }
        else
        {
          cadenacta = cuentas.substring(j * cortec * 11, endc);

          cadenamto = montos.substring(j * cortem, endm);
        }

        win.eval("document.entry.Scuentas" + (j + 1) + ".value = '" + cadenacta + "'");

        win.eval("document.entry.Smontos" + (j + 1) + ".value = '" + cadenamto + "'");
      }
    }
    else
    {
      this.Mensaje.setText("Disquete rechazado, suma de abonos diferente al cargo...");
    }
  }

  public boolean ValidDDA(String cuenta)
  {
    double sum = 0.0D;
    double temp = 0.0D;
    double x = 0.0D;

    if (cuenta.length() < 10) {
      return false;
    }
    for (int i = 2; i < cuenta.length() - 1; ++i)
    {
      if (i % 2 != 0) {
        sum += Integer.parseInt(cuenta.substring(i, i + 1));
      }
      else {
        temp = Integer.parseInt(cuenta.substring(i, i + 1)) * 2;
        if (temp < 10.0D)
          sum += temp;
        else
          sum = sum + Math.floor(temp / 10.0D) + temp % 10.0D;
      }
    }
    if ((sum % 10.0D == 0.0D) && (Integer.parseInt(cuenta.substring(cuenta.length() - 1, cuenta.length())) == 0))
      return true;
    x = 10.0D - (sum % 10.0D);

    return (10.0D - (sum % 10.0D) == Integer.parseInt(cuenta.substring(cuenta.length() - 1, cuenta.length())));
  }
}