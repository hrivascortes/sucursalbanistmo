import java.awt.Color;
import java.io.IOException;
import java.io.InterruptedIOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.Hashtable;
import java.util.StringTokenizer;

import javax.swing.JApplet;
import javax.swing.JLabel;
import javax.swing.JPanel;

import netscape.javascript.JSObject;

public class authorizRemote extends JApplet
{
	private static final long serialVersionUID = -6251216150691025070L;
	public Hashtable ListUsers = null;;
    public String supervisor = null;;
    public JSObject win;
    public Color color;
    public DatagramSocket socket;
    public DatagramPacket message;
    public DatagramPacket reply;
    public int retries;
    public InetAddress serverAddr;
    public int serverPort;
    public byte messageBytes[]  = null;
    
    public authorizRemote()
    {
        supervisor = null;
        color = new Color(255, 255, 255);
    }

    public void init()
    {
        win = JSObject.getWindow(this);
        ListUsers = new Hashtable();
        String data = new String(getParameter("chain"));
        /*
        String inx = getParameter("posSup");
        int posSup = Integer.parseInt(inx);*/ //Cambio para cuando la sucursal tiene m�s de 9 autorizadores
        String tmp;
        int index;
        StringTokenizer tk = new StringTokenizer(data, "#");
        while(tk.hasMoreElements()){
        	tmp = (String)tk.nextElement();
        	index = tmp.indexOf("=");
        	ListUsers.put(tmp.substring(0, index), tmp.substring(index + 1));
        }
        
        System.err.println("ListUsers: " + ListUsers.toString());
        
        JPanel panel = new JPanel();
        JLabel label = new JLabel("Esperando Respuesta de Autorizaci\363n, Por favor espere...");
        label.setBackground(color);
        label.setForeground(Color.blue);
        panel.add(label);
        int numrow = 0;
        try
        {
            numrow = Integer.parseInt((String)ListUsers.get("numrow"));
        }
        catch(Exception e)
        {
            System.out.println("No Existen lista de Supervisores. \n" + e);
        }
        System.err.println("numrow: " + numrow);
        for(int i = 1; i < numrow; i++)
        {
        	System.err.println("i: " + i);
        	connect(i, getParameter("dataauthoriz"));
            if(supervisor == null)
                continue;
            win.eval("document.prefs.txtSupervisor.value = '" + supervisor + "'");
            win.eval("document.prefs.txtAutoriza.value = '1'");
            break;
        }
        
        //connect(posSup,getParameter("dataauthoriz")); //Cambio para cuando la sucursal tiene m�s de 9 autorizadores
        if(supervisor != null){
        	win.eval("document.prefs.txtSupervisor.value = '" + supervisor + "'");
            win.eval("document.prefs.txtAutoriza.value = '1'");
        }
        	
        if(supervisor == null)
            win.eval("cancelartxn()");
        else
            win.eval("handleOK()");
    }

    public void connect(int numrow, String datauthoriz)
    {
        String addres = (String)ListUsers.get("D_DIRECCION_IP" + numrow);
        String prueba = "1prueba de auto";
        String autoremote = "2" + datauthoriz + "supervisor=" + (String)ListUsers.get("D_NOMBRE" + numrow) + "#usersupervisor=" + (String)ListUsers.get("C_CAJERO" + numrow) + "#access=" + (String)ListUsers.get("D_CLAVE_ACC" + numrow) + "#";
        int port = 1120;
        int timeout = 0;
        System.out.println("Datos para autorizacion : [" + autoremote + "]");
        do
        {
            try
            {
                serverAddr = InetAddress.getByName(addres);
                serverPort = port;
            }
            catch(UnknownHostException e)
            {
                throw new RuntimeException("host not known");
            }
            try
            {
                System.out.println("Prueba de conexion, Por Favor Espere...");
                int st = buildsocket(prueba);
                socket.setSoTimeout(3000);
                socket.send(message);
                socket.receive(reply);
                System.out.println(" Server said Authorization ok. : ");
                st = buildsocket(autoremote);
                System.out.println("Realizando Autorizacion, Por Favor Espere...");
                socket.setSoTimeout(60000);
                timeout = 1;
                socket.send(message);
                socket.receive(reply);
                System.out.println(" Server said Authorization ok. : ");
                supervisor = new String(reply.getData(), 0, reply.getLength());
                if(supervisor.equals("nouser"))
                    supervisor = null;
                break;
            }
            catch(SocketException e)
            {
                System.out.println("TimeOut not set.");
                break;
            }
            catch(InterruptedIOException e)
            {
                if(port >= 1122 || timeout == 1)
                    break;
                port++;
                System.out.println("Receive timed out. ");
            }
            catch(IOException e)
            {
                System.out.println("Packet send/receive failure.");
            }
        } while(true);
        socket.close();
    }

    public int buildsocket(String mensaje)
    {
        int st = 0;
        messageBytes = mensaje.getBytes();
        try
        {
            socket = new DatagramSocket();
            reply = new DatagramPacket(new byte[2048], 2048);
            message = new DatagramPacket(messageBytes, messageBytes.length, serverAddr, serverPort);
        }
        catch(SocketException e)
        {
            st = 1;
            throw new RuntimeException("Socket creation failure");
        }
        return st;
    }
}