public class GeneraArboles
{
   GeneraArboles()
   {
   }

   Nodo insTree1(String ptr, Nodo tree, short  val)
   {
      Nodo rama = new Nodo();
      Nodo nodoaux = new Nodo();
      Nodo nodotmp = new Nodo();
      char tmp;
      String  temp;

      nodoaux = tree ;
      if (ptr != "null")
      {
         tmp = ptr.charAt(0);
         if (tmp == '0')
            nodotmp = nodoaux.izq;
         else
            nodotmp = nodoaux.der;
         if (nodotmp == null)
         {
            rama.izq = null;
            rama.der = null;
            rama.val = ((ptr.length()) == 1) ? val: -1;
         }
         else
            rama = nodotmp;
         if (ptr.length() > 1 )
            temp = ptr.substring(1,ptr.length());
         else
            temp = "null";
         if (tmp == '0')
         {
            nodoaux.izq = rama;
            insTree1(temp, nodoaux.izq, val);
         }
         else
         {
            nodoaux.der = rama;
            insTree1(temp, nodoaux.der, val);
         }
      }
      return nodoaux;
   }

   Nodo Genera1(Nodo tree, String arr[])
   {
      short    i = 0;

      Nodo treeraiz = new Nodo();
      treeraiz =  tree;
      treeraiz.izq = null;
      treeraiz.der = null;
      treeraiz.val = -1;
      for (i = 0 ; arr[i] != "null" ; i++)
         treeraiz = insTree1(arr[i], treeraiz, i);
      return treeraiz;
   }

   Nodo GeneraArboles1(Nodo Nodoaux, String arrcoloraux[])
   {
      Nodo Nodotmp = new Nodo();
      Nodotmp = Nodoaux;
      Nodotmp = Genera1(Nodotmp, arrcoloraux);
      return Nodotmp;
   }
}
