public class GetOffs
{
   GetOffs()
   {
   }

	IMAGEN PrintState1(IMAGEN imgaux)
	{
      byte    c;
      short   ctmp;
      IMAGEN  imgtmp = new IMAGEN();

      imgtmp = imgaux;
      c = imgtmp.ptrOri[imgtmp.indOri];
      if (c < 0)
         ctmp = 256;
      else
         ctmp = 0;
      imgtmp.indOri += (short)(c+1+ctmp) ;
      imgtmp.tot    += (short)(c+1+ctmp) ;
      return imgtmp;
	}

   IMAGEN  NextState1( IMAGEN imgaux, short insideSeg)
   {
      long    num_err = 0;
      long    num_bco = 0;
      byte    c, ctmp;
      IMAGEN  imgtmp = new IMAGEN();

      imgtmp = imgaux;
      while ( imgtmp.tot < imgtmp.sizeori )
      {
         c = imgtmp.ptrOri[imgtmp.indOri];
         imgtmp.indOri++;
         imgtmp.tot++;
         if ( c == 112)
         {
            ctmp = imgtmp.ptrOri[imgtmp.indOri];
            if ( ctmp == 4 )
            {
               imgtmp.cr = 1;
               return imgtmp;
            }
         }
         else if ( c == -111)
         {
            if ( insideSeg != 0 )
            {
               imgtmp.cr = 2;
               return imgtmp;
            }
         }
         else if ( c == -110)
         {
            if ( insideSeg != 0 )
            {
               imgtmp.cr = 12;
               return imgtmp;
            }
         }
         else if ( c == -108)
         {
            if ( insideSeg != 0 )
            {
               imgtmp.cr = 7;
               return imgtmp;
            }
         }
         else if ( c == -107)
         {
            if ( insideSeg != 0 )
            {
               imgtmp.cr = 8;
               return imgtmp;
            }
         }
         else if ( c == -106)
         {
            if ( insideSeg != 0 )
            {
               imgtmp.cr = 9;
               return imgtmp;
            }
         }
         else if ( c == -105)
         {
            if ( insideSeg != 0 )
            {
               imgtmp.cr = 10;
               return imgtmp;
            }
         }
         else if ( c == -2)
         {
            if ( insideSeg != 0 )
            {
               imgtmp.cr = 11;
               return imgtmp;
            }
         }
         else if ( c == -109)
         {
            if ( insideSeg != 0 )
            {
               imgtmp.cr = 5;
               return imgtmp;
            }
         }
         else if ( c == 113)
         {
            if ( insideSeg != 0 )
            {
               imgtmp.cr = 4;
               return imgtmp;
            }
         }
         else
         {
            if ((c == 32) || (c == 0))
            {
               num_bco++;
            }
            else
               if (++num_err > 128)
               {
                  //System.out.println("Estado erroneo " + c + " " + imgtmp.tot);
                  imgtmp.cr = 13;
                  return imgtmp;
               }
         }
      }
      return imgtmp;
   }

   short Param1(short indice, IMAGEN imgtmp)
   {
      byte c;
      short val1, val2;
      short numero;

      c = imgtmp.ptrOri[indice];
      if (c < 0)
         val1 = (short) (256 + c);
      else
         val1 = (short) c;
      c = imgtmp.ptrOri[indice+1];
      if (c < 0)
         val2 = (short) (256 + c);
      else
         val2 = (short) c;
      numero = val1;
      numero = (short)(((short)val1 << 8) | (short) val2);
      return numero;
   }

   IMAGEN PrintParametros1( IMAGEN imgaux)
   {
      byte   c;
      short  ctmp;
      IMAGEN  imgtmp = new IMAGEN();

      imgtmp = imgaux;
      c = imgtmp.ptrOri[imgtmp.indOri];
      if (c < 0)
         ctmp = 256;
      else
         ctmp = 0;
      imgtmp.tot += (short) (c + 1 + ctmp);
      if (c != 0)
      {
         imgtmp.width  = Param1((short)(imgtmp.indOri + 6), imgtmp);
         imgtmp.height = Param1((short)(imgtmp.indOri + 8), imgtmp);
      }
      imgtmp.indOri += (short)(c + 1 + ctmp) ;
      return imgtmp;
   }

   IMAGEN LeeImagen1(IMAGEN imgaux , char stClase)
   {
      short   len, val1, val2;
      byte    c;
      IMAGEN  imgtmp = new IMAGEN();

      imgtmp = imgaux;
      c = imgtmp.ptrOri[imgtmp.indOri];
      imgtmp.indOri++ ;
      imgtmp.tot++ ;
      c = imgtmp.ptrOri[imgtmp.indOri+1] ;
      if (c < 0)
         val1 = (short) (c + 256);
      else
         val1 = (short) c;
      c = imgtmp.ptrOri[imgtmp.indOri] ;
      if (c < 0)
         val2 = (short) (c + 256);
      else
         val2 = (short) c;
      len = (short)(((short)val2 << 8) | (short) val1);
      imgtmp.indOri += 2 ;
      imgtmp.tot += 2 ;
      imgtmp.ptrCmp = new byte[len];
      for (int i=0;i<len;i++)
         imgtmp.ptrCmp[i] = imgtmp.ptrOri[imgtmp.indOri+i];
      imgtmp.indOri += len;
      imgtmp.tot    += (long)len ;
      imgtmp.size    = len;
      imgtmp.clase   = (byte) stClase;
      imgtmp.flag_inv= 1;
      return imgtmp;
   }

   IMAGEN GetOffs1(IMAGEN imgaux)
   {      
      byte    state = 0;
      char    stClase;
      short   insideSeg = 0 ;
      IMAGEN imgtmp = new IMAGEN();
      
      stClase = '\0';
      imgtmp = imgaux;
      imgtmp.tot = 0;
      imgtmp.noimg = 0;
      while (imgtmp.tot < imgtmp.sizeori)
      {
         if (state == 0)
         {
            imgtmp = NextState1(imgtmp, insideSeg);
            state = imgtmp.cr;
            if (state == 13)
               state = 0;
         }
         else if (state == 1)
         {
            insideSeg = 1 ;
            if ( imgtmp.noimg == 20)
            {
               imgtmp.cr = 0 ;
               return imgtmp;
            }
            imgtmp = PrintState1(imgtmp);
            imgtmp = NextState1(imgtmp, insideSeg);
            state = imgtmp.cr;
         }
         else if ( state == 2)
         {
            imgtmp = PrintState1(imgtmp);
            imgtmp = NextState1(imgtmp, insideSeg);
            state = imgtmp.cr;
         }
         else if (state == 7)
         {
            imgtmp = PrintParametros1(imgtmp);
            imgtmp = NextState1(imgtmp, insideSeg);
            state = imgtmp.cr;
         }
         else if (state == 8)
         {
            imgtmp = PrintState1(imgtmp);
            imgtmp = NextState1(imgtmp, insideSeg);
            state = imgtmp.cr;
         }
         else if (state == 9)
         {
            imgtmp = PrintState1(imgtmp);
            imgtmp = NextState1(imgtmp, insideSeg);
            state = imgtmp.cr;
         }
         else if (state == 10)
         {
            imgtmp = PrintState1(imgtmp);
            imgtmp = NextState1(imgtmp, insideSeg);
            state = imgtmp.cr;
         }
         else if (state == 11)
         {
            imgtmp = LeeImagen1(imgtmp, stClase);
            imgtmp = NextState1(imgtmp, insideSeg);
            state = imgtmp.cr;
         }
         else if (state == 12)
         {
            imgtmp.cr = -1 ;
            return imgtmp;
         }
         else if (state == 5)
         {
            imgtmp = PrintState1(imgtmp);
            imgtmp = NextState1(imgtmp, insideSeg);
            state = imgtmp.cr;
         }
         else if (state == 4)
         {
            imgtmp = PrintState1(imgtmp);
            insideSeg = 0 ;
            imgtmp = NextState1(imgtmp, insideSeg);
            state = imgtmp.cr;
            imgtmp.noimg++;
         }
         else if (state == 13)
         {
            imgtmp.cr = -1 ;
            return imgtmp;
         }
      }
      imgtmp.cr = 0 ;
      return imgtmp;
   }
}
