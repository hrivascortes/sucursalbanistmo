public class GetBMP
{

   GetBMP()
   {
   }

   MMR InitMMR1(IMAGEN img, Nodo TreeBco, Nodo TreeBid, MMR mmr)
   {
      MMR mmrtmp  = new MMR();
      long   size;

      mmrtmp = mmr;
      mmrtmp.img = img;
      mmrtmp.sp1.Codigo = new byte[80];
      mmrtmp.sp2.Codigo = new byte[80];
      mmrtmp.LR.linea   = new byte[2000];
      mmrtmp.LC.linea   = new byte[2000];
      mmrtmp.LR.Pos     = new POS[1000];
      mmrtmp.LC.Pos     = new POS[1000];
      for(int i=0;i<1000;i++)
      {
         mmrtmp.LC.Pos[i] = new POS();
         mmrtmp.LR.Pos[i] = new POS();
      }
      mmrtmp.sizelin = mmrtmp.img.width;
      size = (mmrtmp.img.height * mmrtmp.sizelin) + 1078;
      mmrtmp.img.ptrBmp = new byte[(int)size];
      mmrtmp.sp1.Ptr    = mmr.sp1.Codigo;
      mmrtmp.tipo_cmp  = '1';
      mmrtmp.nlin = 0;
      mmrtmp.Bit  = 0;
      mmrtmp.LC.indlinea = 0;
      mmrtmp.sp2.ptrCod = mmr.sp2.Codigo;
      mmrtmp.sp2.indCod = 0;
      mmrtmp.sp2.nodo   = TreeBid;
      mmrtmp.sp2.state  = (short) 0;
      mmrtmp.sp2.modo   = 0;
      mmrtmp.sp2.flag   = 0;
      mmrtmp.sp1.nodo  = TreeBco;
      mmrtmp.sp1.color = 0;
      mmrtmp.sp1.tot   = 0;
      mmrtmp.sp1.Ptr   = mmr.sp1.Codigo;
      mmrtmp.sp1.indPtr= 0;
      mmrtmp.sp1.flag  = 0;
      mmrtmp.LR.pos_b1       = mmr.img.width;
      mmrtmp.LR.pos_b2       = mmr.img.width;
      mmrtmp.LR.ind          = 1;
      mmrtmp.LR.Pos[0].color = 0;
      mmrtmp.LR.Pos[0].pos   = (short) mmrtmp.img.width;
      mmrtmp.LC.ptr    = mmr.LC.linea;
      mmrtmp.LC.indptr = 0;
      mmrtmp.LC.a0     = null;
      mmrtmp.LC.pos_a0 = -1;
      mmrtmp.LC.pos_a1 = 0;
      mmrtmp.LC.tot    = 0;
      mmrtmp.LC.paso   = 0;
      mmrtmp.LC.ind    = 0;
      return mmrtmp;
   }

   IMAGEN GetBMP1(IMAGEN Imgaux , Nodo TreeBco, Nodo TreeNgo, Nodo TreeBid)
   {
      MMR mmr = new MMR();
      IMAGEN imgtmp = new IMAGEN();
      Descomprime  descom = new Descomprime();

      imgtmp = Imgaux;
      mmr = InitMMR1(imgtmp, TreeBco, TreeBid, mmr);
      for(int i= 0; i<1078; i++)
         mmr.img.ptrBmp[i]=(byte)255;
      mmr = descom.Descomprime1(imgtmp, TreeBco, TreeNgo, TreeBid, mmr);
      imgtmp.ptrBmp = mmr.img.ptrBmp;
      return imgtmp;
   }
}
