public class DoLinUno
{
   DoLinUno()
   {
   }

   MMR MetodoUno1(long  state, long  val, MMR mmraux)
   {
      LinCod LC = new LinCod();
      MMR mmrtmp = new MMR();
      byte color;
      byte tmp, tmp2;
      int  i;

      mmrtmp = mmraux;
      LC = mmrtmp.LC;
      if ((state == (long)1)||(state == (long)5))
         color = 0;
      else
         color = 1;
      if (val > 63)
         val = (val - 63) * 64 ;
      for (i = 0; i < val ; i++)
         LC.ptr[(int)LC.indptr++] = color;
      LC.tot += val;
      if (val < 64)
      {
         if (state == 2 || state == 5) {
            LC.a0 = LC.ptr;
            LC.inda0 = LC.indptr;
            LC.pos_a0 = LC.inda0 - LC.indlinea;
         }
         tmp = (byte)color;
         if (LC.ind > 0)
            tmp2 = (byte)LC.Pos[LC.ind-1].color;
         else
            tmp2 = 64;
         if (tmp == tmp2)
            tmp = 1;
         else
            tmp = 0;
         if ((LC.ind!=0) && (tmp!=0))
            LC.ind--;
         else
            LC.Pos[LC.ind].color = color;
         LC.Pos[LC.ind].pos = (short)LC.tot;
         LC.ind++;
         LC.paso   = 0;
      }
      mmrtmp.LC = LC;
      return mmrtmp;
   }

   MMR MetodoDos1(long  val, MMR mmraux)
   {
      LinRef LR = new LinRef();
      LinCod LC = new LinCod();
      MMR mmrtmp = new MMR();
      long dif;
      byte color;
      byte tmp,tmp2;

      mmrtmp = mmraux;
      LC = mmrtmp.LC;
      LR = mmrtmp.LR;
      if (LC.paso !=0 )
      {
         tmp = (byte) LC.a0[(int)(LC.inda0 - 1)];    // ???
         color = tmp;
      }
      else
      {
         if (LC.a0 != null)
         {
            if (LC.inda0 > 0)
            {
               tmp = (byte) LC.a0[(int)(LC.inda0 - 1)];
               if (tmp == 0)
                  color = 1;
               else
                  color = 0;
            }
            else
               color = 1;        // ???
         }
         else
            color = 0;
      }
      dif = (val > 3) ? -(val - 3) : val;
      LC.a1 = LC.linea;
      LC.inda1 = LC.indlinea + LR.pos_b1 + dif;
      if (LC.a0 == null)
         val = LC.inda1 - LC.indlinea;
      else
         val = LC.inda1 - LC.inda0;
      LC.a0 = LC.a1;
      LC.inda0 = LC.inda1;
      for (int i = 0 ; i < val ; i++)
         mmrtmp.LC.ptr[(int)LC.indptr++] = color;
      LC.tot += val;
      LC.pos_a0 = LC.inda0 - LC.indlinea;
      LC.paso   = 0;
      tmp = (byte)color;
      if (LC.ind > 0)
         tmp2 = (byte)LC.Pos[LC.ind-1].color;
      else
         tmp2 = 64;
      if ((LC.ind!=0) && (tmp!=0))
         tmp = 1;
      else
         tmp = 0;
      if (tmp == tmp2)
         LC.ind--;
      else
         LC.Pos[LC.ind].color = color;
      LC.Pos[LC.ind].pos = (short) LC.tot;
      LC.ind++;
      mmrtmp.LC = LC;
      mmrtmp.LR = LR;
      return mmrtmp;
   }

   MMR MetodoTres1(MMR mmraux)
   {
      LinCod LC = new LinCod();
      MMR mmrtmp = new MMR();
      LinRef LR = new LinRef();
      long val;
      byte color;
      byte tmp,tmp2;

      mmrtmp = mmraux;
      LC = mmrtmp.LC;
      LR = mmrtmp.LR;

      if (LC.paso !=0 )
      {
         color = LC.a0[(int)(LC.inda0 - 1)];  // ???
      }
      else
      {
         if (LC.a0 == null)
            color = 0;
         else
         	if (LC.inda0 > 0)  //modificacion 2/12/2003
         	{
	            if (LC.a0[(int)(LC.inda0 - 1)] != 0)
   	            color = 0;
      	      else
         	      color = 1;
         	}
         	else
   	        color = 1;         	 
      }
      if (LC.a0 == null)
         val = LR.pos_b2;
      else
         val = LR.pos_b2 - LC.pos_a0;
      for (int i = 0; i < val ; i++)
         mmrtmp.LC.ptr[(int)LC.indptr++] = color;
      LC.a0 = LC.ptr;
      LC.inda0 = LC.indptr;
      LC.tot += val;
      LC.pos_a0 = LC.inda0 - LC.indlinea;
      LR.pos_b2 = LR.b2 - LR.indlinea;
      if (LC.ind!=0) // ???
      {
         tmp = (byte)color;
         tmp2 = (byte)LC.Pos[LC.ind-1].color;
         if (tmp2 == tmp)
            LC.ind--;
         else
            LC.Pos[LC.ind].color = color;
      }
      else
         LC.Pos[LC.ind].color = color;
      LC.Pos[LC.ind].pos = (short)LC.tot;
      LC.ind++;
      LC.paso = 1;
      mmrtmp.LC = LC;
      mmrtmp.LR = LR;
      return mmrtmp;
   }

   MMR Busca_b1_b21(MMR mmraux)
   {
      LinCod LC = new LinCod();
      LinRef LR = new LinRef();
      MMR mmrtmp = new MMR();
      byte color = 0;
      int  LastInd = 0;

      mmrtmp = mmraux;
      LC = mmrtmp.LC;
      LR = mmrtmp.LR;
      if (LC.a0 == null)
         LastInd = 0;
      else
      {
         while ((LastInd < LR.ind) && ((LR.Pos[LastInd].pos) <= (LC.pos_a0)))
            LastInd++;
         while ((LastInd > 0) && ((LR.Pos[LastInd - 1].pos) > (LC.pos_a0)))
            LastInd--;
      }
      if (LastInd < (LR.ind - 1))
      {
         if (LC.paso !=0 )
            color = LC.a0[(int)(LC.inda0 - 1)];
         else
         {
            if (LC.a0 == null)
               color = 0;
            else
            {
               if (LC.inda0 > 0)
               {
                  if (LC.a0[(int)(LC.inda0 - 1)] != 0)
                     color = 0;
                  else
                     color = 1;
               }
               else
               {
                  color = 1;
               }
            }
         }
         if (color != LR.Pos[LastInd].color)
            LastInd++;
      }
      LR.pos_b1 = LR.Pos[LastInd].pos;
      if (LastInd < (LR.ind-1))
         LR.pos_b2 = LR.Pos[LastInd+1].pos;
      else
         LR.pos_b2 = LR.Pos[LastInd].pos;
      LR.b2 = LR.indlinea + LR.pos_b2;
      mmrtmp.LC = LC;
      mmrtmp.LR = LR;
      return mmrtmp;
   }

   MMR DoLinUno1(long  val, long  state, MMR mmraux)
   {

      LinCod LC = new LinCod();
      LinRef LR = new LinRef();
      DoLinDos DoLinD = new DoLinDos();
      MMR mmrtmp = new MMR ();

      mmrtmp = mmraux;
      LC = mmrtmp.LC;
      LR = mmrtmp.LR;
      if (LC.tot == mmrtmp.img.width && state == 0)
         DoLinD.DoLinDos1(mmrtmp);
      if (state == 0)
      {
         if (val == 9)
         {
            //System.out.println("****  Se encontro fin de linea ***");
            System.exit(0);
         }
         mmrtmp = Busca_b1_b21(mmrtmp);
         if (val == 8)
         {
            if (LC.paso > 0)
               mmrtmp.sp2.modo = (short) LC.a0[(int)(LC.inda0-1)];
            else
               if (LC.a0 != null)
               {
               		byte con = 0;
               		if (LC.inda0 > 0)
                     	con = (byte) LC.a0[(int)(LC.inda0-1)];
                     if (con != 0)
                        mmrtmp.sp2.modo = 0;
                     else
                        mmrtmp.sp2.modo = 1;
               }
               else
                  mmrtmp.sp2.modo = 0;
         }
         else
            if (val < 7)
               mmrtmp = MetodoDos1(val, mmrtmp);
            else
               if (val == 7)
                  mmrtmp = MetodoTres1(mmrtmp);
      }
      else
         mmrtmp = MetodoUno1(state, val, mmrtmp);
      mmrtmp.LC = LC;
      mmrtmp.LR = LR;
      return mmrtmp;
   }
}
