import java.awt.*;
import java.awt.event.*;

public class CtasEsp extends Dialog implements ActionListener
{
   private TextField Imagen1;
   private TextField Imagen2;
   private TextField Imagen3;
   private Component a, b, c;   
	private Button bImp_;
	public  String strImagenes = new String("");		
   public CtasEsp(Frame f)
   {
		super(f);
		setSize(650,120);
		setLocation(80,250);      				
		setTitle(" Captura de Imagenes de la Cuenta Especial ");			
		setLayout(new FlowLayout());
		c = new Label("No. Imagen1    : ");
		c.setBackground(Color.white);         
		add( c);
		Imagen1 = new TextField("          ");         
		Imagen1.setEditable( true );
		Imagen1.setVisible( true );         
		Imagen1.setBackground(Color.white);         
		add( Imagen1 );
		a = new Label("No. Imagen2 : ");
		a.setBackground(Color.white);         
		add( a);
		Imagen2 = new TextField("          ");         
		Imagen2.setEditable( true );
		Imagen2.setVisible( true );         
		Imagen2.setBackground(Color.white);         
		add( Imagen2 );
		b = new Label("No. Imagen3 : ");
		b.setBackground(Color.white);         
		add( b);
		Imagen3 = new TextField("          ");         
		Imagen3.setEditable( true );
		Imagen3.setVisible( true );         
		Imagen3.setBackground(Color.white);         
		add( Imagen3 );
		bImp_ = new Button( "Aceptar" );
		bImp_.addActionListener(this);
		add( bImp_);		
		setModal(true);
		show();				
   }   
   
	public void actionPerformed(ActionEvent ae)
	{
      Button de = (Button)ae.getSource();

      if (de.getLabel().equals("Aceptar") == true)
		{	
			String strTmp = Imagen1.getText().trim();
			String strCeros = "000";
			strTmp = strCeros.substring(0,3-strTmp.length()) + strTmp;
			strImagenes = strTmp;
			strTmp = Imagen2.getText().trim();
			strTmp = strCeros.substring(0,3-strTmp.length()) + strTmp;
			strImagenes += strTmp;
			strTmp = Imagen3.getText().trim();
			strTmp = strCeros.substring(0,3-strTmp.length()) + strTmp;
			strImagenes += strTmp;
			dispose();
      }
	}   

	public String getImagenes()
	{
		return strImagenes;
	}   
	
}