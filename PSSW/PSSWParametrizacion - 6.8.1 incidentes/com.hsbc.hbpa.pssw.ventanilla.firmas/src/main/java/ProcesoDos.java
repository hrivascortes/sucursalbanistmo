public class ProcesoDos
{
   ProcesoDos(){}

   MMR  CambioMetodo1(byte ptr, short mask, MMR mmraux)
   {
      MMR mmrtmp = new MMR();
      mmrtmp = mmraux;
      mmrtmp.tipo_cmp = ((short)ptr & mask) > 0 ?  '1' : '0';
      mmrtmp.sp2.flag = 0;
      if (++mmrtmp.Bit == 8)
         mmrtmp.Bit = 0;
      if (mmrtmp.tipo_cmp == '1' || mmrtmp.Bit == 0)
         mmrtmp.cr = 1;
      else
         mmrtmp.cr = 0;
      return mmrtmp;
   }

   MMR  PrintCodigo1(Nodo nodo, MMR mmraux)
   {
      MMR mmrtmp = new MMR();

      mmrtmp = mmraux;
      mmrtmp.sp2.ptrCod[mmrtmp.sp2.indCod] = (byte) '\0';
      if (nodo == null)
      {
         //System.out.println("CodDesc: " + mmrtmp.sp2.Codigo[0] + " " + mmrtmp.sp2.indCod + " Tot " + mmrtmp.LC.tot + "Ptr: " + ptr + " Bit: " + mmrtmp.Bit);
         if (++mmrtmp.Bit == 8)
            mmrtmp.Bit = 0;
         mmrtmp.cr = 1;
         return mmrtmp;
      }
      mmrtmp.sp2.ptrCod = mmrtmp.sp2.Codigo;
      mmrtmp.sp2.indCod = 0;
      mmrtmp.cr = 0;
      return mmrtmp;
   }

   MMR  EsFinLinea1(long  state, long  val, MMR mmraux)
   {
      MMR mmrtmp = new MMR();
      long    falta;
      int i;
      byte color;

      mmrtmp = mmraux;
      if ((state == 0 && val == 9) ||
         (state == 1 && val == 104)) {
         falta = mmrtmp.img.width - mmrtmp.LC.tot;
         color = 0;
         for (i = 0; i < falta ; i++)
            mmrtmp.LC.ptr[(int)mmrtmp.LC.indptr++] = color;
         mmrtmp.LC.tot += val;
         mmrtmp.cr = 1;
         return mmrtmp;
      }
      mmrtmp.cr = 0;
      return mmrtmp;
   }

   boolean  CasoInesperado1(long  state, Nodo nodo)
   {
      if (state == 0)
      {
         //if (nodo.val == 10)
         //   System.out.println("!! AMPLIAC !!!");
         //else
         //   if (nodo.val == 9)
         //      System.out.println("!! EOL !!!");
         if (nodo.val == 10 || nodo.val == 9)
            return true;
      }
      return false;
   }

   MMR ProcesoDos1(byte ptr, MMR mmraux, Nodo TreeBco, Nodo TreeNgo, Nodo TreeBid)
   {
      short temporal;
      SP2 sp2 = new SP2();
      DoLinDos DoLinD = new DoLinDos();
      DoLinUno DoLinU = new DoLinUno();
      MMR mmrtmp = new MMR();

      mmrtmp = mmraux;
      sp2 = mmrtmp.sp2;
      short   mask = (short) (mmrtmp.img.flag_inv > 0 ? 1 : 128);
      while (mmrtmp.nlin < mmrtmp.img.height)
      {
         if (mmrtmp.img.flag_inv > 0)
            mask =  (short) (0x80 >> mmrtmp.Bit);
         else
            mask =  (short) (0x01 << mmrtmp.Bit);
         if (sp2.flag != 0)
         {
            mmrtmp = CambioMetodo1(ptr, (short) mask, mmrtmp );
            sp2 = mmrtmp.sp2;
            if (mmrtmp.cr > 0)
               return mmrtmp;
            else
               continue;
         }
         temporal = (short) ptr;
         if (temporal < 0)
            temporal = (short)(temporal + 256);
         sp2.nodo = (temporal & mask) > 0 ? sp2.nodo.der : sp2.nodo.izq;
         temporal = (short) (temporal & mask);
         if (temporal > 0)
            mmrtmp.sp2.ptrCod[mmrtmp.sp2.indCod] =  (byte) '1';
         else
            mmrtmp.sp2.ptrCod[mmrtmp.sp2.indCod] =  (byte) '0';
         mmrtmp.sp2.indCod++;
         if (sp2.nodo == null)
         {
            mmrtmp = PrintCodigo1(sp2.nodo, mmrtmp);
            sp2 = mmrtmp.sp2;
            if (mmrtmp.cr > 0 )
            {
               sp2.nodo   = TreeBid;
               sp2.state  = (short) 0;
               sp2.modo   = 0;
               return mmrtmp;
            }
            else
               continue;
         }
         mmrtmp = EsFinLinea1(sp2.state, sp2.nodo.val, mmrtmp);
         if (mmrtmp.cr > 0)
         {
            mmrtmp = DoLinD.DoLinDos1(mmrtmp);
            continue;
         }
         if (CasoInesperado1(sp2.state, sp2.nodo))
            sp2.nodo = TreeBid;
         else
         {
            if (sp2.nodo.val != -1)
            {
               mmrtmp = PrintCodigo1(sp2.nodo, mmrtmp);
               sp2    = mmrtmp.sp2;
               mmrtmp = DoLinU.DoLinUno1(sp2.nodo.val, sp2.state, mmrtmp);
               sp2    = mmrtmp.sp2;
               if (sp2.state == 0)
               {
                  if (sp2.nodo.val == (short) 8)
                  {
                     if (sp2.modo != 0)
                        sp2.state = (short) 10;
                     else
                        sp2.state = (short) 1;
                     if (sp2.modo != 0)
                        sp2.nodo = TreeNgo;
                     else
                        sp2.nodo = TreeBco;
                  }
                  else
                  {
                     sp2.state = (short) 0;
                     sp2.nodo = TreeBid;
                  }
               }
               else if (((int)sp2.state) == ((int)1))
               {
                  if (sp2.nodo.val > 63)
                     sp2.nodo = TreeBco;
                  else
                  {
                     sp2.nodo = TreeNgo;
                     sp2.state = 2;
                  }
               }
               else if (((int)sp2.state) == ((int)2))
               {
                  if (sp2.nodo.val > 63)
                     sp2.nodo = TreeNgo;
                  else
                  {
                     sp2.nodo = TreeBid;
                     sp2.state = (short)0;
                  }
               }
               else if (((int)sp2.state) == ((int)10))
               {
                  if (sp2.nodo.val > 63)
                     sp2.nodo = TreeNgo;
                  else
                  {
                     sp2.nodo = TreeBco;
                     sp2.state = 5;
                  }
               }
               else if (((int)sp2.state) == ((int)5))
               {
                  if (sp2.nodo.val > 63)
                     sp2.nodo = TreeBco;
                  else
                  {
                     sp2.nodo = TreeBid;
                     sp2.state = (short)0;
                  }
               }
            }
         }
         if (++mmrtmp.Bit == 8)
         {
            mmrtmp.Bit = 0;
            return mmrtmp;
         }
      }
      return mmrtmp;
   }
}

