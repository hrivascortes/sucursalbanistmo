public class ProcesoUno
{
   ProcesoUno()
   {
   }

   MMR LineaActual1(long  val, MMR mmraux)
   {
      LinCod LC = new LinCod();
      MMR mmrtmp = new MMR();
      int i;
      int tmp, tmp2, tmp3;

      mmrtmp = mmraux;
      LC = mmrtmp.LC;
      for (i = (int) LC.indptr; i < (val + LC.indptr); i++)
         LC.ptr[i] = mmrtmp.sp1.color;
      LC.indptr += val;
      LC.tot += val;
      if (val < 64)
      {
         tmp = (int) mmrtmp.sp1.color;
         if (tmp == 1)
         {
            LC.a0 = LC.ptr;
            LC.inda0 = LC.indptr;
            LC.pos_a0 = LC.inda0 - LC.indlinea;
         }
         tmp2 = (int) LC.ind;
         if (tmp2 > 0)
            tmp3 = LC.Pos[tmp2-1].color;
         else
            tmp3 = -1;
         if ((tmp2 !=0) && (tmp != 0))
            tmp = 1;
         else
            tmp = 0;
         if (tmp == tmp3)
            LC.ind--;
         else
            LC.Pos[LC.ind].color = mmrtmp.sp1.color;
         LC.Pos[LC.ind].pos = (short) LC.tot;
         LC.ind++;
      }
      mmrtmp.LC = LC;
      return mmrtmp;
   }

   MMR ProcesoUno1(byte ptr, MMR mmraux, Nodo TreeBco, Nodo TreeNgo)
   {
      long   valor = 0;
      short  temporal;
      DoLinDos DoL = new DoLinDos();
      MMR mmrtmp  = new MMR();
      short   mask;

      mmrtmp = mmraux;
      while (mmrtmp.nlin < mmrtmp.img.height)
      {
         if (ptr < 0)
            temporal = (short)(ptr + 256);
         else
            temporal = ptr;
         if (mmrtmp.img.flag_inv > 0)
            mask = (short) ((short)128 >> (short) mmrtmp.Bit);
         else
            mask = (short) ((short)1 << (short) mmrtmp.Bit);
         mask = (short) (mask & (short)255);
         if (mmrtmp.sp1.flag == 0)
            mmrtmp.sp1.nodo = (temporal & mask) > 0 ? mmrtmp.sp1.nodo.der : mmrtmp.sp1.nodo.izq;
         if ((temporal & mask) > 0)
            mmrtmp.sp1.Ptr[mmrtmp.sp1.indPtr++] = (byte) '1';
         else
            mmrtmp.sp1.Ptr[mmrtmp.sp1.indPtr++] = (byte) '0';
         if (mmrtmp.sp1.flag != 0)
         {
            mmrtmp.tipo_cmp = (temporal & mask) > 0 ?  '1' : '0';
            mmrtmp.sp1.flag = 0;
            if (mmrtmp.tipo_cmp == '0')
            {
               if (++mmrtmp.Bit == 8)
                  mmrtmp.Bit = 0;
               return mmrtmp;
            }
         }
         else
         {
            if (mmrtmp.sp1.nodo == null)
            {
               mmrtmp.sp1.nodo = TreeBco;
               mmrtmp.sp1.color = 0;
               mmrtmp.sp1.tot = 0;
               mmrtmp.sp1.Ptr[mmrtmp.sp1.indPtr] = 0;
               mmrtmp.sp1.indPtr = 0;
               mmrtmp.sp1.Ptr = mmrtmp.sp1.Codigo;
               //System.out.println("Error de codigo " + mmrtmp.sp1.Codigo);
               return mmrtmp;
            }
            else
            {
               if (mmrtmp.sp1.nodo.val != -1)
               {
                  if (mmrtmp.sp1.nodo.val == 104)
                  {
                     if (mmrtmp.sp1.tot > 0)
                        mmrtmp = DoL.DoLinDos1(mmrtmp);
                     mmrtmp.sp1.flag = 1;
                     mmrtmp.sp1.tot = 0;
                  }
                  valor = (short) (mmrtmp.sp1.nodo.val > 63 ? ((mmrtmp.sp1.nodo.val-63)*64) : mmrtmp.sp1.nodo.val);
                  if (mmrtmp.sp1.nodo.val != 104)
                  {
                     mmrtmp = LineaActual1(valor, mmrtmp);
                     mmrtmp.sp1.tot += valor;
                  }
                  else
                     valor = 104;
                  mmrtmp.sp1.Ptr[mmrtmp.sp1.indPtr] = 0;
                  mmrtmp.sp1.Ptr = mmrtmp.sp1.Codigo;
                  mmrtmp.sp1.indPtr = 0;
                  if (mmrtmp.sp1.nodo.val < 64)
                  {
                     if (mmrtmp.sp1.color != 0)
                        mmrtmp.sp1.color = 0;
                     else
                        mmrtmp.sp1.color = 1;
                  }
                  if (mmrtmp.sp1.nodo.val != 104)
                  {
                     if (mmrtmp.sp1.color !=0)
                        mmrtmp.sp1.nodo = TreeNgo;
                     else
                        mmrtmp.sp1.nodo = TreeBco;
                  }
                  if (mmrtmp.sp1.nodo.val == 104)
                  {
                     mmrtmp.sp1.nodo = TreeBco;
                     mmrtmp.sp1.color = 0;
                  }
               }
            }
         }
         if ( ++mmrtmp.Bit== 8)
         {
            mmrtmp.Bit = 0;
            return(mmrtmp);
         }
      }
      return(mmrtmp);
   }
}
