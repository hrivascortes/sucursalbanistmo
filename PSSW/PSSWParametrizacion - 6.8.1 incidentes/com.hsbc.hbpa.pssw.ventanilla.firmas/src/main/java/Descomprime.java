class Descomprime
{
   Descomprime()
   {
   }

   MMR  Descomprime1(IMAGEN img, Nodo TreeBco, Nodo TreeNgo, Nodo TreeBid, MMR mmr)
   {
      byte    c = 0;
      byte    buf[] = new byte[512];
      int     offs = 0;
      MMR mmrtmp  = new MMR();

      mmrtmp = mmr;
      ProcesoUno procU = new ProcesoUno();
      ProcesoDos procD = new ProcesoDos();

      while (offs < img.size)
      {
         c   = img.ptrCmp[offs];
         offs++;
         if ((c == 0)&&((offs + 2) < img.size))
         {
            buf[0] = img.ptrCmp[offs];
            buf[1] = img.ptrCmp[offs+1];
            buf[2] = img.ptrCmp[offs+2];
            if ((buf[0]==0) && (buf[1]==0) && (buf[2]==0))
            {
               offs += 3;
               continue;
            }
         }
         if (mmrtmp.tipo_cmp == '1')
         {
            mmrtmp = procU.ProcesoUno1(c, mmrtmp, TreeBco, TreeNgo);
            if ((mmrtmp.tipo_cmp != '1') && (mmrtmp.Bit != 0))
               mmrtmp = procD.ProcesoDos1(c, mmrtmp, TreeBco, TreeNgo, TreeBid);
         }
         else
         {
            mmrtmp = procD.ProcesoDos1(c, mmrtmp, TreeBco, TreeNgo, TreeBid);
            if ((mmrtmp.tipo_cmp != '0') && (mmrtmp.Bit != 0))
               mmrtmp = procU.ProcesoUno1(c, mmrtmp, TreeBco, TreeNgo);
         }
      }
      return(mmrtmp);
   }
}
