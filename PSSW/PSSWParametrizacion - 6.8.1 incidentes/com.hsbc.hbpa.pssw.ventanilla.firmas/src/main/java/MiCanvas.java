import java.awt.Canvas;
import java.awt.Image;
import java.awt.Color;
import java.awt.Graphics;

public class MiCanvas extends Canvas
{
   Image logo1;
   int indice1;
   int ancho1;
   int largo1;

   public MiCanvas(Image logo, int indice, int ancho, int largo)
   {
      logo1 = logo;
      indice1 = indice;
      ancho1 = ancho * 2;
      largo1 = largo * 2;
   }

   public void paint( Graphics g )
   {
      g.drawImage(logo1, 0, 0,ancho1 , largo1 , this);
      g.setColor(Color.red);
      g.drawString("Firma " + (indice1 + 1), ((ancho1 * 2) / 2) , ((largo1 * 2) + 20));
   }
}
