import java.applet.Applet;
import java.awt.*;
import java.awt.datatransfer.*;
import java.awt.event.*;
import java.awt.image.MemoryImageSource;
import java.awt.image.DirectColorModel;
import java.io.*;
import java.net.URL;
import java.net.URLConnection;
import java.util.zip.*;
import netscape.javascript.JSObject;
import java.sql.*;
import java.text.*;

public class CFirmas extends Applet implements ActionListener, MouseListener, MouseMotionListener,KeyListener
{
	public boolean Pintar;
	public Image  logo[];
	public int    ancho[];
	public String local = new String("1");
	public int    largo[];
	public int    s1 = 30, s2 = 160, c1 = 0, c2 = 50, img, ant = 0, sig = 2, uipp = 0;
	public int imgini, imgfin, total=0;
	byte pix[][];
	char   clase[]= null;
	ComponentesAWT window;
	public int StatusError = 0;
	public String temporal= new String("");
	public int intBase = 0;
	public String strImagen1= new String("");
	public String strImagen2= new String("");
	public String strImagen3= new String("");
	public String strCuentaR= new String("");
	public String strCuentaEsp = new String("000000000");
	public String strCuenta = new String("");
	public String strFechaActual = new String(""); 
	public String strOffLine = new String("");
	public int open, close, win_h;
	public TextField cuenta2;
	public String strCondicion = new String("");
	public TextField nImagenes;
	public TextField condiciones1;
	public TextField condiciones2;
	public TextField Blq;
	public Button sig_;
	public Button ant_;
	public Button blq_;
	public int intComunicaciones = 0;
	public int intIndice = 0;   
	public String Label3 = "C. Bloque";
	public String Label0 = "Actualizar Cta.";
	public String Label1 = "Pag. Ant.";
	public String Label2 = "Pag. Sig.";
	public boolean EjecucionEnApplet = true;
	public Component b, c ,d, e ;
	public JSObject win;
	public Clipboard clipbd = getToolkit().getSystemClipboard();
	/* Realizar cambios Dependiendo de la aplicacion (Plataforma/Ventanilla) */	
	//public String sAplicacion = new String("Plataforma");
	public String sAplicacion = new String("Ventanilla");
	public String sHttp = null;
		
	public void destroy()
	{
		eliminaCache();
	}

	public void stop()
	{
		eliminaCache();
	}
	
	public void init()
	{
		try
		{					
			b = new Label("No. Cuenta :");
			b.setBackground(Color.white);         
			add( b );
			cuenta2 = new TextField("");
			cuenta2.setColumns(11);         
			cuenta2.setEditable( false );
			cuenta2.setVisible( true );
			cuenta2.setBackground(Color.white);
			add( cuenta2 );
			c = new Label("No. Imagenes :");
			c.setBackground(Color.white);
			add( c );
			nImagenes = new TextField("");
			nImagenes.setBackground(Color.white);
			nImagenes.setEditable( false );
			nImagenes.setColumns(2);
			add( nImagenes );
			Button ant_ = new Button( Label1 );
			ant_.setBackground(java.awt.Color.white);
			ant_.addActionListener(this);
			add( ant_ );
			Button sig_ = new Button( Label2 );
			sig_.setBackground(java.awt.Color.white);
			sig_.addActionListener(this);
			add( sig_ );
    		if (this.sAplicacion.equals("Ventanilla"))
        		d = new Label("                         ");
      		else
        		d = new Label("                       ");
			d.setBackground(Color.white);         
			add( d );
			if (sAplicacion.equals("Ventanilla"))
			{
				Button act_ = new Button( Label0 );
				act_.setBackground(java.awt.Color.white);
				act_.addActionListener(this);
				add( act_ );
				act_.setVisible( true );
			}
			else
			{
				c = new Label("               ");
				c.setBackground(Color.white);
				add( c );				
			}			
			Button blq_ = new Button(Label3);
			blq_.setBackground(Color.white);
			blq_.addActionListener(this);
			blq_.setEnabled(true);
			add(blq_);
			Blq = new TextField("0");
			Blq.setEditable(false);
			Blq.setEnabled(false);
			Blq.setBackground(Color.white);
			add(Blq);

			c = new Label("Condiciones :");
			c.setBackground(Color.white);
			add( c );			
			condiciones1 = new TextField("");
			condiciones1.setEditable( false );
			condiciones1.setVisible( true );
      		if (sAplicacion.equals("Ventanilla"))
        		condiciones1.setColumns(62);
      		else
        		condiciones1.setColumns(52);
 			condiciones1.setBackground(Color.white);         
			add( condiciones1 ); 			
			condiciones2 = new TextField("");
			condiciones2.setEditable( false );
			condiciones2.setVisible( true );
			condiciones2.setColumns(92);
 			condiciones2.setBackground(Color.white);                  
			add( condiciones2 );
			win = JSObject.getWindow(this);
			CFirmas ve = new CFirmas();
			ve.EjecucionEnApplet = false;
			ve.setVisible(true);
			URL uHttp = getCodeBase();
			sHttp= uHttp.getProtocol().trim();						
			//System.out.println(" sHttp " + sHttp);									
			addMouseListener(this);
			addMouseMotionListener(this);
			addKeyListener(this);
			inicia();
			setupAdapters();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	public void inicia()
	{
		repaint();
		Pintar = false;
		open = 0;
		close = 1;
		win_h = 0;
		total = 0;
		pix = new byte[19][];
		ancho = new int[19];
		largo = new int[19];
		clase	= new char[20];
		logo = new Image[19];

		strCuenta = getParameter( "Cuenta" );
		strCuentaR = strCuenta;
		strFechaActual = getParameter( "Fecha" );
		strOffLine = getParameter( "OffLine" );

		if (strCuenta.substring(0,6).equals("000000"))
			cuenta2.setText(strCuenta.substring(6,16));
		else if (strCuenta.substring(5,16).equals("01800000018"))
			cuenta2.setText(strCuenta.substring(0,5));
		else
			cuenta2.setText(strCuenta);         						
	}

	private void setupAdapters()
	{		
		DirectColorModel cm = new DirectColorModel(32,255,255,255);
		boolean bBuscaCuenta=false;
		boolean bBajarCuenta=false;

		if (sAplicacion.equals("Ventanilla"))
		{
			bBajarCuenta = bajarCuentas(strFechaActual);
			if (bBajarCuenta)
			{
				String strCuentas = leeBajas();
				if (strCuentas.length() > 0)
					borraCuenta(strCuentas, strFechaActual);
			}
			intBase = 0;
			if (strCuenta.substring(0,6).equals("000000"))
				bBuscaCuenta = buscaCuenta();
			condiciones1.setText("");
			condiciones2.setText("");  
			if (bBuscaCuenta)
			{
				intBase = 1;
				if (strCondicion.length() < 71)
					condiciones1.setText(strCondicion.substring(0,strCondicion.length()));			
				else
				{
					condiciones1.setText(strCondicion.substring(0,70));
					condiciones2.setText(strCondicion.substring(70,strCondicion.length()));				
				}
			}
			else
			{
				local = "0";
				if (strOffLine.equals("0"))
					valida();
			}
		}
		else
			valida();		
		if (sAplicacion.equals("Plataforma") && total != 0 && StatusError == 0)		   	     
			if ( strCuentaR.endsWith("L"))
				win.eval("actualizaStatus()");
		for (img = 0; img < total ; img++)
			logo[img] = createImage(new MemoryImageSource(ancho[img], largo[img], cm, pix[img], 0, ancho[img]));
		if (sAplicacion.equals("Ventanilla"))		   	     
			win.eval("activar()");         
	}

	public boolean bajarCuentas( String strFechaActual)
	{
		boolean bBajarCtas = false;
		String url = "jdbc:odbc:FirmOffline";
		String strFechaBase = "";
		try 
		{
			Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");
		} 
		catch(java.lang.ClassNotFoundException e) 
		{
			System.err.println(e.getMessage());
		}								
		try
		{												
			Connection conn = DriverManager.getConnection(url);					
			if(conn != null)
			{
				Statement stmt = conn.createStatement();
				ResultSet rs = stmt.executeQuery( "SELECT FEC_CONTROL FROM TC_CONTROL " );
				if( rs.next() )
					strFechaBase = ( String ) rs.getString ( "FEC_CONTROL" );
				if (strFechaBase.equals("99999999"))
				{
					bBajarCtas = false;
					intBase = 1;
				}
				else
				{
					if (strFechaBase.equals(strFechaActual))
					{
						intBase = 0;
						bBajarCtas = false;
					}
					else
					{
						intBase = 2;
						bBajarCtas = true;
						try
						{			
							Statement stmt1 = conn.createStatement();
							stmt1.executeUpdate("UPDATE TC_CONTROL SET FEC_CONTROL ='99999999'");
   	      					stmt1.close();
         					conn.close();													   	      
						}
						catch(SQLException sqlExc)
						{
							System.out.println( "Error: " + sqlExc);
						}
					}
				}
				if(stmt != null)
				{
					stmt.close();
					stmt = null;					
				}
			}
			if(conn != null)
				conn.close();			  
		}
		catch(SQLException sqlExc)
		{
			System.out.println( "Error: " + sqlExc);
		}				
		return bBajarCtas;		
	}

	public String leeBajas() 
	{
		URL                 url1;
		URLConnection       conn;
		BufferedReader        in = null;
		String              RemoteIP = null;
		String strBajas = new String("");
		try
		{
			RemoteIP = getParameter( "RemoteIP" );
			int Port = Integer.parseInt(getParameter( "RemotePort" ));			
			String strRuta = getParameter( "Ruta" ) + "/servlet/ventanilla.BajasServlet";			
			if (sHttp.toUpperCase().trim().equals("HTTPS"))
				Port = Port - 1 ;				
			
			url1 = new URL(sHttp, RemoteIP, Port, strRuta);
			//url1 = new URL("http", RemoteIP, Port, strRuta);
			conn = url1.openConnection();			
			in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
			strBajas = in.readLine();
			in.close();			
			in = null;
		}
		catch (IOException x)
		{
			System.out.println(" IOException x " + x);
			return "Error";
		}
		finally
		{
			try
			{
				if (in != null)
				{
					in.close();
					in = null;
				}
			} catch (IOException e)
			{
				e.printStackTrace();
			}
		}		
		if (strBajas.length() > 0)
			strBajas = strBajas.trim();
		return strBajas;
	}	

	public void borraCuenta( String strCuentas, String strFechaActual)
	{	
		String url = "jdbc:odbc:FirmOffline";		
		int intCuentas = 0;		
		String strCuentaBajas = "";
      try 
      {
      	Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");
      } 
      catch(java.lang.ClassNotFoundException e) 
      {
          System.err.println(e.getMessage());
      }	      
      intCuentas = strCuentas.length() / 11;
		try
		{			   	
   		Connection conn = DriverManager.getConnection(url);			
			for (int j = 0 ; j < intCuentas ; j++)
			{			
				strCuentaBajas = strCuentas.substring(j*11,(j+1)*11-1);
				Statement stmt1 = conn.createStatement();
				int valDV = Integer.parseInt(strCuentaBajas.substring(9,10));
				strCuentaBajas = "000000" + strCuentaBajas;
				switch(valDV)
				{
					case  0 : stmt1.executeUpdate("DELETE FROM TW_FIRMAS_SUC0 WHERE D_CUENTA='" + strCuentaBajas + "'");
								break;
					case  1 : stmt1.executeUpdate("DELETE FROM TW_FIRMAS_SUC1 WHERE D_CUENTA='" + strCuentaBajas + "'");
								break;					
					case  2 : stmt1.executeUpdate("DELETE FROM TW_FIRMAS_SUC2 WHERE D_CUENTA='" + strCuentaBajas + "'");
								break;					
					case  3 : stmt1.executeUpdate("DELETE FROM TW_FIRMAS_SUC3 WHERE D_CUENTA='" + strCuentaBajas + "'");
								break;					
					case  4 : stmt1.executeUpdate("DELETE FROM TW_FIRMAS_SUC4 WHERE D_CUENTA='" + strCuentaBajas + "'");
								break;					
					case  5 : stmt1.executeUpdate("DELETE FROM TW_FIRMAS_SUC5 WHERE D_CUENTA='" + strCuentaBajas + "'");
								break;				
					case  6 : stmt1.executeUpdate("DELETE FROM TW_FIRMAS_SUC6 WHERE D_CUENTA='" + strCuentaBajas + "'");
								break;					
					case  7 : stmt1.executeUpdate("DELETE FROM TW_FIRMAS_SUC7 WHERE D_CUENTA='" + strCuentaBajas + "'");
								break;					
					case  8 : stmt1.executeUpdate("DELETE FROM TW_FIRMAS_SUC8 WHERE D_CUENTA='" + strCuentaBajas + "'");
								break;					
					case  9 : stmt1.executeUpdate("DELETE FROM TW_FIRMAS_SUC9 WHERE D_CUENTA='" + strCuentaBajas + "'");
				}   	      
   	      stmt1.close();
			}
			Statement stmt = conn.createStatement();
   	   stmt.executeUpdate("UPDATE TC_CONTROL SET FEC_CONTROL ='" + strFechaActual + "'");
   	   stmt.close();
       	conn.close();													   	      
		}
		catch(SQLException sqlExc)
		{
			System.out.println( "Error: " + sqlExc);
		}
	}

	public void CuentaMala( )
	{	
		String url = "jdbc:odbc:FirmOffline";		
      try 
      {
      	Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");
      } 
      catch(java.lang.ClassNotFoundException e) 
      {
          System.err.println(e.getMessage());
      }	      
 		try
		{			   				
   		Connection conn = DriverManager.getConnection(url);			
			Statement stmt1 = conn.createStatement();   		
			int valDV = Integer.parseInt(strCuenta.substring(15,16));
			switch(valDV)
			{
				case  0 : stmt1.executeUpdate("DELETE FROM TW_FIRMAS_SUC0 WHERE D_CUENTA='" + strCuenta + "'");
							break;
				case  1 : stmt1.executeUpdate("DELETE FROM TW_FIRMAS_SUC1 WHERE D_CUENTA='" + strCuenta + "'");
							break;					
				case  2 : stmt1.executeUpdate("DELETE FROM TW_FIRMAS_SUC2 WHERE D_CUENTA='" + strCuenta + "'");
							break;					
				case  3 : stmt1.executeUpdate("DELETE FROM TW_FIRMAS_SUC3 WHERE D_CUENTA='" + strCuenta + "'");
							break;					
				case  4 : stmt1.executeUpdate("DELETE FROM TW_FIRMAS_SUC4 WHERE D_CUENTA='" + strCuenta + "'");
							break;					
				case  5 : stmt1.executeUpdate("DELETE FROM TW_FIRMAS_SUC5 WHERE D_CUENTA='" + strCuenta + "'");
							break;				
				case  6 : stmt1.executeUpdate("DELETE FROM TW_FIRMAS_SUC6 WHERE D_CUENTA='" + strCuenta + "'");							
							break;					
				case  7 : stmt1.executeUpdate("DELETE FROM TW_FIRMAS_SUC7 WHERE D_CUENTA='" + strCuenta + "'");
							break;					
				case  8 : stmt1.executeUpdate("DELETE FROM TW_FIRMAS_SUC8 WHERE D_CUENTA='" + strCuenta + "'");
							break;					
				case  9 : stmt1.executeUpdate("DELETE FROM TW_FIRMAS_SUC9 WHERE D_CUENTA='" + strCuenta + "'");
			}   	      
   	   stmt1.close();
   	   conn.close();													   	      
		}
		catch(SQLException sqlExc)
		{
			System.out.println( "Error: " + sqlExc);
		}
	}

	public boolean buscaCuenta( )
	{
		boolean bExisteCuenta = false;
		String strImagenes = new String("");      
		String strPropiedades = new String("");      
		String strClase = new String("");         		
		String strTmp = new String("");
		int	 intLongitud = 0;
		int	 intSuma = 0;		
		String url = "jdbc:odbc:FirmOffline";
		
		try 
		{
			Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");
		} 
		catch(java.lang.ClassNotFoundException e) 
		{
			System.err.println(e.getMessage());
			bExisteCuenta = false;
		}								
		try
		{
			Connection conn = DriverManager.getConnection(url);					
			if(conn != null)
			{
				Statement stmt = conn.createStatement();				
				ResultSet rs = null;
				int valDV = Integer.parseInt(strCuenta.substring(15,16));
				switch(valDV)
				{
					case  0 : rs = stmt.executeQuery( "SELECT * FROM TW_FIRMAS_SUC0 WHERE D_CUENTA='" + strCuenta + "'" );
								break;
					case  1 : rs = stmt.executeQuery( "SELECT * FROM TW_FIRMAS_SUC1 WHERE D_CUENTA='" + strCuenta + "'" );
								break;					
					case  2 : rs = stmt.executeQuery( "SELECT * FROM TW_FIRMAS_SUC2 WHERE D_CUENTA='" + strCuenta + "'" );
								break;					
					case  3 : rs = stmt.executeQuery( "SELECT * FROM TW_FIRMAS_SUC3 WHERE D_CUENTA='" + strCuenta + "'" );
								break;					
					case  4 : rs = stmt.executeQuery( "SELECT * FROM TW_FIRMAS_SUC4 WHERE D_CUENTA='" + strCuenta + "'" );
								break;					
					case  5 : rs = stmt.executeQuery( "SELECT * FROM TW_FIRMAS_SUC5 WHERE D_CUENTA='" + strCuenta + "'" );
								break;				
					case  6 : rs = stmt.executeQuery( "SELECT * FROM TW_FIRMAS_SUC6 WHERE D_CUENTA='" + strCuenta + "'" );
								break;					
					case  7 : rs = stmt.executeQuery( "SELECT * FROM TW_FIRMAS_SUC7 WHERE D_CUENTA='" + strCuenta + "'" );
								break;					
					case  8 : rs = stmt.executeQuery( "SELECT * FROM TW_FIRMAS_SUC8 WHERE D_CUENTA='" + strCuenta + "'" );
								break;					
					case  9 : rs = stmt.executeQuery( "SELECT * FROM TW_FIRMAS_SUC9 WHERE D_CUENTA='" + strCuenta + "'" );
				}   	      
				if( rs.next() )
				{
					strImagenes = ( String ) rs.getString ( "D_IMAGENES" );
					strClase = ( String ) rs.getString ( "D_CLASES" );
					strPropiedades = ( String ) rs.getString ( "D_PROPIEDADES" );
					strPropiedades = strPropiedades.trim();
					total = (int)((strPropiedades.length() / 10 ) - 1 );					
					strCondicion = ( String ) rs.getString ( "D_CONDICIONES" );
					if ( total > 0 )
						bExisteCuenta = true;
				}				
				if(stmt != null)
				{
					stmt.close();
					stmt = null;					
				}
			}
			if(conn != null)
				conn.close();			  
		}
		catch(SQLException sqlExc)
		{
			System.out.println( "Error: " + sqlExc);
			CuentaMala();
			bExisteCuenta = false;
		}
		if (bExisteCuenta)
		{
			for (int j = 0 ; j < total; j++)
			{
				try
				{							
					ancho[j] = Integer.parseInt(strPropiedades.substring(j*10,(j*10)+3));
					largo[j] = Integer.parseInt(strPropiedades.substring((j*10)+3,(j*10)+6));
					intLongitud = Integer.parseInt(strPropiedades.substring((j*10)+6,(j*10)+10));
					clase[j] = strClase.charAt(j);
					strTmp = strImagenes.substring(intSuma, intSuma + intLongitud);
					intSuma = intSuma + intLongitud;			
					Base64 decoder = new Base64();
					byte[] byteImg = decoder.decode(strTmp);
					ByteArrayInputStream strCadenaIn = new ByteArrayInputStream (byteImg);
					GZIPInputStream gzZip = new GZIPInputStream(strCadenaIn);
					byte[] buffer = new byte[1024];
					int len = 0;
					int intSumalen = 0;
					pix[j] = new byte[ancho[j] * largo[j] + 1024 + 54];				
					while( (len = gzZip.read(buffer, 0, 1024)) != -1 )
						for (int i =0; i< len ; i++)
							pix[j][intSumalen++] = buffer[i];      	            
					gzZip.close();
				}
				catch(IOException ioe)
				{
					ioe.printStackTrace();
					bExisteCuenta = false;
					CuentaMala();
				}
				catch(Exception e)
				{
					e.printStackTrace();
					bExisteCuenta = false;
					CuentaMala();
				}
			}
		}		
		return bExisteCuenta;		
	}	
	
	private byte[] leeArchivo()
	{
		URL                 url;
		URLConnection       conn;
		BufferedInputStream in = null;
		byte[]              inBytes = new byte[12284];
		String              RemoteIP = null;
		int len=0;

		try
		{
			RemoteIP = getParameter( "RemoteIP" );
			int Port = Integer.parseInt(getParameter( "RemotePort" ));
			String strRuta = "";
			if (sAplicacion.equals("Plataforma") )
				strRuta = getParameter( "Ruta" ) + "/servlet/firmas.FirmReader?" + strCuenta.substring(0,16);			
			else
				strRuta = getParameter( "Ruta" ) + "/servlet/ventanilla.FirmReader?" + strCuenta;			
			if (sHttp.toUpperCase().trim().equals("HTTPS"))
				Port = Port - 1 ;				
			//url = new URL("http", RemoteIP, Port, strRuta);			
			url = new URL(sHttp, RemoteIP, Port, strRuta);
			conn = url.openConnection();		
			in = new BufferedInputStream(conn.getInputStream());
			for(int offset=0; offset<inBytes.length; offset+=len)
			{
				len = in.read(inBytes, offset, Math.min(inBytes.length-offset, 1024));
				if( len < 0 )
				  break;
			}
			in.close();
			in = null;			
		}
		catch (IOException x)
		{
			x.printStackTrace();			
		}
		finally
		{
			try
			{
				if (in != null)
				{
					in.close();
					in = null;
				}
			} catch (IOException e)
			{
				e.printStackTrace();
			}
		}
		return inBytes;
	}

  private byte[] GeneraLog(int iAcceso)
  {
    BufferedInputStream in = null;
    byte[] inBytes = new byte[12284];
    String RemoteIP = null;
    int len = 0;
    try
    {
      RemoteIP = super.getParameter("RemoteIP");
      int Port = Integer.parseInt(super.getParameter("RemotePort"));
      String strRuta = "";
      strRuta = super.getParameter("Ruta") + "/servlet/ventanilla.FirmLog?" + iAcceso;
      if (this.sHttp.toUpperCase().trim().equals("HTTPS"))
        --Port;

      URL url = new URL("http", RemoteIP, Port, strRuta);
      URLConnection conn = url.openConnection();
      in = new BufferedInputStream(conn.getInputStream());
      for (int offset = 0; offset < inBytes.length; offset += len)
      {
        len = in.read(inBytes, offset, Math.min(inBytes.length - offset, 1024));
        if (len < 0)
          break;
      }
      in.close();
      in = null;
    }
    catch (IOException x)
    {
    }
    finally
    {
      try
      {
        if (in != null){
        	in.close();
        	in = null;
        }
      }
      catch (IOException e)
      {
        e.printStackTrace();
      }
    }
    label244: return inBytes;
  }
	
	public void insertaCuenta( )
	{		
		String strImagenes = new String("");      
		String strPropiedades = new String("");      
		String strClase = new String("");      		
		String url = "jdbc:odbc:FirmOffline";		
      try 
      {
      	Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");
      } 
      catch(java.lang.ClassNotFoundException e) 
      {
          System.err.println(e.getMessage());
      }				
		for (int j = 0 ; j < total; j++)
		{
			try
			{						
				ByteArrayOutputStream os1 = new ByteArrayOutputStream();
				GZIPOutputStream z = new GZIPOutputStream(os1);
				z.write(pix[j],0, pix[j].length);
				z.finish();
				z.close();
				os1.close();
				
				Base64 encoder = new Base64();
				String strCadenaZip = encoder.encode(os1.toByteArray());
				strImagenes = strImagenes + strCadenaZip;
				if (strCadenaZip.length() > 999)
					strPropiedades = strPropiedades + ancho[j] + "0" + largo[j] + strCadenaZip.length();
				else
					strPropiedades = strPropiedades + ancho[j] + "0" + largo[j] + "0" + strCadenaZip.length();
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}						      
			strClase =	strClase + clase [j];		
		}		
      java.util.Date fecha = new java.util.Date();
      SimpleDateFormat formatof = new SimpleDateFormat("ddMMyyyy");
      String fech = formatof.format(fecha);
		strPropiedades = strPropiedades + "20" + fech;		
		try
		{			
   	   	Connection conn = DriverManager.getConnection(url);					
				PreparedStatement ps = null;
				int valDV = Integer.parseInt(strCuenta.substring(15,16));
				switch(valDV)
				{
					case  0 : ps = conn.prepareStatement( "INSERT INTO TW_FIRMAS_SUC0 (D_CUENTA, D_CLASES, D_CONDICIONES,  D_IMAGENES, D_PROPIEDADES) VALUES (?,?,?,?,?)");
								break;
					case  1 : ps = conn.prepareStatement( "INSERT INTO TW_FIRMAS_SUC1 (D_CUENTA, D_CLASES, D_CONDICIONES,  D_IMAGENES, D_PROPIEDADES) VALUES (?,?,?,?,?)");
								break;					
					case  2 : ps = conn.prepareStatement( "INSERT INTO TW_FIRMAS_SUC2 (D_CUENTA, D_CLASES, D_CONDICIONES,  D_IMAGENES, D_PROPIEDADES) VALUES (?,?,?,?,?)");
								break;					
					case  3 : ps = conn.prepareStatement( "INSERT INTO TW_FIRMAS_SUC3 (D_CUENTA, D_CLASES, D_CONDICIONES,  D_IMAGENES, D_PROPIEDADES) VALUES (?,?,?,?,?)");
								break;					
					case  4 : ps = conn.prepareStatement( "INSERT INTO TW_FIRMAS_SUC4 (D_CUENTA, D_CLASES, D_CONDICIONES,  D_IMAGENES, D_PROPIEDADES) VALUES (?,?,?,?,?)");
								break;					
					case  5 : ps = conn.prepareStatement( "INSERT INTO TW_FIRMAS_SUC5 (D_CUENTA, D_CLASES, D_CONDICIONES,  D_IMAGENES, D_PROPIEDADES) VALUES (?,?,?,?,?)");
								break;				
					case  6 : ps = conn.prepareStatement( "INSERT INTO TW_FIRMAS_SUC6 (D_CUENTA, D_CLASES, D_CONDICIONES,  D_IMAGENES, D_PROPIEDADES) VALUES (?,?,?,?,?)");
								break;					
					case  7 : ps = conn.prepareStatement( "INSERT INTO TW_FIRMAS_SUC7 (D_CUENTA, D_CLASES, D_CONDICIONES,  D_IMAGENES, D_PROPIEDADES) VALUES (?,?,?,?,?)");
								break;					
					case  8 : ps = conn.prepareStatement( "INSERT INTO TW_FIRMAS_SUC8 (D_CUENTA, D_CLASES, D_CONDICIONES,  D_IMAGENES, D_PROPIEDADES) VALUES (?,?,?,?,?)");
								break;					
					case  9 : ps = conn.prepareStatement( "INSERT INTO TW_FIRMAS_SUC9 (D_CUENTA, D_CLASES, D_CONDICIONES,  D_IMAGENES, D_PROPIEDADES) VALUES (?,?,?,?,?)");
				}   	      				
   	 		String longString = strImagenes;
    			int size = longString.length();
    			BufferedInputStream bais = new BufferedInputStream( new ByteArrayInputStream( longString.getBytes() ) );    		
	    		ps.setString(1,strCuenta);
   	 		ps.setString(2,strClase);
    			ps.setString(3,strCondicion);
    			ps.setAsciiStream( 4, bais, size );
	    		ps.setString(5,strPropiedades);
   	      ps.executeUpdate();
   	      ps.close();
         	conn.close();													   	      
		}
		catch(SQLException sqlExc)
		{
			System.out.println( "Error: " + sqlExc);
		}
	}
	public void valida()
	{
		Nodo    TreeBco = new Nodo();
		Nodo    TreeNgo = new Nodo();
		Nodo    TreeBid = new Nodo();
		Colorr arrcolor = new Colorr();
		GeneraArboles Arbol = new GeneraArboles ();
		IMAGEN img  = new IMAGEN();
		int		stmp=0;
		int		offset;

		TreeBco = Arbol.GeneraArboles1(TreeBco, arrcolor.WhiteCode);
		TreeNgo = Arbol.GeneraArboles1(TreeNgo, arrcolor.BlackCode);
		TreeBid = Arbol.GeneraArboles1(TreeBid, arrcolor.VerCode);	      	
	
		intComunicaciones = 1;				

	    String strCeros = "000000";
	    String strTmp = Blq.getText();
	    if ((!(strCuenta.substring(12, 16).equals("0000"))) && (!(strCuenta.substring(5, 16).equals("01800000018"))))
	    {
	      strTmp = strTmp.trim();
	      strTmp = strCeros.substring(0, 6 - strTmp.length()) + strTmp;
	      strTmp = strTmp + strCuenta.substring(6, 16);
	      strCuenta = strTmp;
	    }
	    condiciones1.setText("");
	    condiciones2.setText("");

		for (int intCiclo = 0; intCiclo < intComunicaciones; intCiclo++)
		{
	      byte[] bRespuesta;
	      byte[] databyte = leeArchivo();
	      total = databyte[34];
	
	      strTmp = Blq.getText();
	      strTmp = strTmp.trim();
	      if ((total == 0) && (strTmp.equals("0")) && (!(strCuenta.substring(12, 16).equals("0000"))) && (!(this.strCuenta.substring(5, 16).equals("01800000018"))))
	      {
	        strTmp = "000001" + strCuenta.substring(6, 16);
	        strCuenta = strTmp;
	        databyte = leeArchivo();
	        Blq.setEnabled(true);
	        Blq.setEditable(true);
	        Blq.setText("1");
	      }	

			nImagenes.setText(Integer.toString(total));			
			stmp  = databyte[40];  
			if (total > 0 ) 
			{
		        if (this.sAplicacion.equals("Ventanilla"))
		        {
		          bRespuesta = GeneraLog(1);
		          if (bRespuesta[0] == 1)
		            System.out.println("Error en Inserción");
		        }
				if ((stmp == 0) || (stmp == 99))
				{  
	      			if (stmp == 0)
	  	      		{
      					StringBuffer condicion = new StringBuffer();	  	      		
						offset = 239;
      	      			stmp = 238;  
						while (databyte[(int)stmp] == ' ') stmp--;
						stmp-=43; 
						String temp = new String(databyte, 44, (int)stmp); 
						condicion.append(temp);
						strCondicion = condicion.toString();
						if (stmp > 70)
						{
							temporal = condicion.substring(0,70);
							condiciones1.setText(temporal);
							if (stmp > 150)
								temporal = condicion.substring(70,150);
							else
								temporal = condicion.substring(70,stmp);
							condiciones2.setText(temporal);
						}
						else
						{
							temporal = condicion.substring(0,stmp);
							condiciones1.setText(temporal);
						}
					}
					else
						offset = 41; 
					int intImgreal = 0;   	   	    
					for (int j = 0 ; j < total; j++)
					{
						short tmp1, tmp2;
						if ((offset + 4 ) < databyte.length)
						{
							clase[j+intIndice] = (char)databyte[offset];
							offset++;
							tmp1 = (short) databyte[offset];
							if (tmp1 < 0)
								tmp1 = (short)(tmp1 + 256);
							tmp2 = (short) databyte[offset+1];
							if (tmp2 < 0)
               					tmp2 = (short)(tmp2 + 256);
							img.sizeori = (short)(((short)tmp1 << 8) | (short) tmp2);
							if (img.sizeori < 10 ) 
							{
								condiciones1.setText("ERROR IMAGENES NO RECUPERABLES");      	   				
								condiciones2.setText("ERROR IMAGENES NO RECUPERABLES");      		         	
								j = total + 1 ;
							}			            	
							else
							{
								offset+=2;			            
								if ((offset + img.sizeori ) < databyte.length)
								{
									img.ptrOri = new byte[img.sizeori];
									for(int i = 0; i < img.sizeori; i++)
										img.ptrOri [i] = databyte[offset+i];
									img.indOri = 0;
									GetOffs Offset = new GetOffs();
									img = Offset.GetOffs1(img);
									ancho[j+intIndice]=img.width;
									largo[j+intIndice]=img.height;
									GetBMP Bmp = new GetBMP();
									img = Bmp.GetBMP1(img, TreeBco, TreeNgo, TreeBid);
									if (j < total)
									{     
										byte imgtmp[];         	   	       	   
										pix[j+intIndice] = new byte[img.ptrBmp.length-1078];
										imgtmp = img.ptrBmp;
										for (int i =1078; i<img.ptrBmp.length;i++)
											pix[j+intIndice][i-1078] = imgtmp[i];      	            
									}
									offset += img.sizeori;
									intImgreal++;   				         	
								}
								else
								{
									condiciones1.setText("ERROR IMAGENES NO RECUPERABLES");      	   				
									condiciones2.setText("ERROR IMAGENES NO RECUPERABLES");      		         	
									j = total + 1 ;      		         	
									StatusError = 1;
								}
							}
						}
						else
						{
							condiciones1.setText("ERROR IMAGENES NO RECUPERABLES");      	   				
							condiciones2.setText("ERROR IMAGENES NO RECUPERABLES");      		         	      	   			
							j = total + 1 ;
							StatusError = 1;							
						}
					}
					intIndice+=total;   	         	
					total = intImgreal;
				}
				else
				{
					condiciones1.setText("ERROR CODIGO DE MAINFRAME     ");      	   				
					condiciones2.setText("INVALIDO                      ");
				}
			}
   	   		else
			{
		        logo = new Image[19];
		        Pintar = false;
		        open = 0;
		        close = 1;
		        win_h = 0;
		        pix = new byte[19][];
		        ancho = new int[19];
		        largo = new int[19];
		        clase = new char[20];
		        intIndice = 0;
		        repaint();
				condiciones1.setText("Firma(s) no disponible(s) favor de checar con la Unidad de Negocio o promotor ");
				condiciones2.setText("correspondiente  a la cuenta.");
				if (this.sAplicacion.equals("Ventanilla"))
		        {
		          bRespuesta = GeneraLog(2);
		          if (bRespuesta[0] == 1)
		            System.out.println("Error en Inserción");
		        }
			}

			if (sAplicacion.equals("Ventanilla"))			      
				if (local.equals("0") && (total > 0) && (intComunicaciones ==1) && (intBase == 0))
					if (strCuenta.substring(0,6).equals("000000"))
						insertaCuenta(); 
		}
      total = intIndice;
//      strCuenta = strTmp;
   }
	public void actionPerformed(ActionEvent ae)
	{		
		Button de = (Button)ae.getSource();
		if (de.getLabel().equals("Pag. Sig.") == true)
		{
			if (uipp == 3)
			{
				Pintar = false;
				uipp = 0;
				repaint();
			}
			else
			{
				if (((uipp == 0) && (total > 5)) || ((uipp == 1) && (total > 11))||
				((uipp == 2) && (total > 17)))
				{
					uipp = uipp + 1;
					Pintar = true;
					repaint();
				}
				else
				{
					uipp = 0;
					Pintar = true;
					repaint();
				}
			}
		}
		if (de.getLabel().equals("Pag. Ant.") == true)
		{
			if (uipp == 0)
			{
				Pintar = false;
				if (total > 18)
					uipp = 3;
				else
					if ((total > 12) && (total < 19))
						uipp = 2;
					else
						if ((total > 6) && (total < 13))
							uipp = 1;
				repaint();
			}
			else
			{
				if (total > 6)
				{
					uipp = uipp - 1;
					Pintar = true;
					repaint();
				}
			}
		}
		if (sAplicacion.equals("Ventanilla"))
		{			                  
			if (de.getLabel().equals("Actualizar Cta.") == true)
			{
				CuentaMala();
				s1 = 30;
				s2 = 160;
				c1 = 0; 
				c2 = 50; 
				img = 0;
				ant = 0; 
				sig = 2;
				uipp = 0;
				intIndice = 0;			
				inicia();	
				setupAdapters();				
			}
		}
	    if (de.getLabel().equals("C. Bloque"))
	    {
	      s1 = 30;
	      s2 = 160;
	      c1 = 0;
	      c2 = 50;
	      img = 0;
	      ant = 0;
	      sig = 2;
	      uipp = 0;
	      intIndice = 0;
	      setupAdapters();
	    }            
		eliminaCache();
	}

	public void eliminaCache()
	{
		String selection = "";
		StringSelection clipString = new StringSelection(selection);
		clipbd.setContents(clipString, clipString);
	}


	public void mouseReleased(MouseEvent e) {
		if (e.getX() != 0)
			eliminaCache();
	}

	public void mousePressed(MouseEvent e) {
		if (e.getX() != 0)
			eliminaCache();
	}

	public void mouseMoved(MouseEvent e) {
		if (e.getX() != 0)
			eliminaCache();
	}

	public void mouseDragged(MouseEvent e) {
		if (e.getX() != 0)
			eliminaCache();
	}

	public void mouseEntered(MouseEvent e) {
		if (e.getX() != 0)
			repaint();
		Pintar = true;
	}

	public void mouseExited(MouseEvent e) {
		if (e.getX() != 0)
			repaint();
		Pintar = false;
	}

	public void keyPressed(KeyEvent e) {
		if (e.KEY_PRESSED != 0)
			eliminaCache();
	}

	public void keyReleased(KeyEvent e) {
		if (e.KEY_PRESSED != 0)
			eliminaCache();
	}

	public void keyTyped(KeyEvent e) {
		if (e.KEY_PRESSED != 0)
			eliminaCache();
	}

	public void mouseClicked(MouseEvent e)   
	{
		int	x = e.getX(); 
		int y = e.getY();  
		int indice = 0;
		
		if ((x >= 0) && (x <= 215))
		{
			if ((y >= 100) && (y <= 171))
			{
				if (uipp == 0)
					indice = 0;
				else
					if (uipp == 1)
						indice = 6;
					else
						if (uipp == 2)
							indice = 12;
						else
							if (uipp == 3)
								indice = 18;
			}
			else
				if ((y >= 210) && (y <= 281))
				{
					if (uipp == 0)
						indice = 3;
					else
						if (uipp == 1)
							indice = 9;
						else
							if (uipp == 2)
								indice = 15;
							else
								if (uipp == 3)
									indice = 21;
				}
				else
					indice = -1;
		}
		else
			if ((x >= 220) && (x <= 435))
			{
				if ((y >= 100) && (y <= 171))
				{
					if (uipp == 0)
						indice = 1;
					else
						if (uipp == 1)
							indice = 7;
						else
							if (uipp == 2)
								indice = 13;
							else
								if (uipp == 3)
									indice = 19;
				}
				else
					if ((y >= 210) && (y <= 281))
					{
						if (uipp == 0)
							indice = 4;
						else
							if (uipp == 1)
								indice = 10;
							else
								if (uipp == 2)
									indice = 16;
								else
									if (uipp == 3)
										indice = 20;
					}
					else
						indice = -1;
			}
			else
				if ((x >= 490) && (x <= 705))
				{
					if ((y >= 100) && (y <= 171))
					{
						if (uipp == 0)
							indice = 2;
						else
							if (uipp == 1)
								indice = 8;
							else
								if (uipp == 2)
									indice = 14;
								else
									if (uipp == 3)
										indice = 20;
					}
					else
						if ((y >= 210) && (y <= 281))
						{
							if (uipp == 0)
								indice = 5;
							else
								if (uipp == 1)
									indice = 11;
								else
									if (uipp == 2)
										indice = 17;
									else
										if (uipp == 3)
											indice = 23;
						}
						else
							indice = -1;
				}
				else
					indice = -1;
		if ((indice >= 0) && (indice < img))      
		{
			if ((close == 1) && (open == 0))
			{
				window = new ComponentesAWT(logo[indice], indice, ancho[indice], largo[indice]);
				window.setTitle("ZOOM de la Firma Digital");
				window.setSize(460,200);
				window.show();
				close = 0;
				open = 1;
				win_h = 1;
			}
			else
			{
				window.hide();
				close = 1;
				open = 0;
				win_h = 2;
			}
		}
	}

	public void update(Graphics g)
	{
		if ( !Pintar )
		{
			g.setColor(Color.white);
			g.fillRect(0, 0, getSize().width, getSize().height);
			g.setColor(Color.white);
		}
		paint(g);
		eliminaCache();
	}

	public void paint( Graphics g )
	{
		imgini = 0; imgfin = 5;
		s1 = 60; s2 = 185; c1 = 0; c2 = 100;

		if ( Pintar )
		{
			imgini = imgini + (uipp * 6);
			imgfin = imgfin + (uipp * 6);
			if (imgfin > 18)
				imgfin = 18;
			for(img = imgini; img <= imgfin; img++)
			{         	
				if(logo[img] == null)
					break;
				else
				{
					g.drawImage(logo[img], c1, c2, CFirmas.this);
					g.setColor(Color.white);
					g.fillRect(s1 - 1, s2 - 10, 100, 10); /* 40 50 10 */
					g.setColor(Color.blue);
					g.drawString("Firma " + (img + 1) + " Clase : " +clase[img], s1, s2);
					c1 = c1 + 215;
					s1 = s1 + 215;
					if ((img == 2) || (img == 5) || (img == 8) || (img == 11) ||
					(img == 14) || (img == 17) || (img == 20) || (img == 23))
					{
						c1 = 0;
						s1 = 60;
						c2 = c2 + 90;
						s2 = s2 + 90;
					}
				}
			}
			s1 = 20;s2 = 185;c1 = 0;c2 = 100;
		}
		else
		{
			repaint();
			if ((close == 0) && (open == 1))
			{
				window.hide();
				close = 1;
				open = 0;
				win_h = 2;
			}
		}
		eliminaCache();		
	}
}