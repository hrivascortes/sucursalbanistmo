public class DoLinDos
{
   DoLinDos(){}

   MMR DoLinDos1(MMR mmraux)
   {
      int   i;
      LinCod LC = new LinCod();
      LinRef LR = new LinRef();
      MMR mmrtmp  = new MMR();

      mmrtmp = mmraux;
      LC = mmraux.LC;
      LR = mmraux.LR;
      LC.ptr  = LC.linea;
      LC.indptr = 0;
      for (i = 0; i < mmraux.img.width; i++ )
         LR.linea[i] = LC.linea[i];
      LC.a0    = null;
      LC.inda0 = 0;
      LC.tot   = 0;
      LC.paso  = 0;
      LC.pos_a0 = -1;
      LR.pos_b1 = 0;
      LR.pos_b2 = 0;
      LR.ind = LC.ind;
      for (i = 0; i < LC.ind ; i++)
      {
         short posaux, coloraux;
         posaux          = (short) LC.Pos[i].pos;
         coloraux        = (short) LC.Pos[i].color;
         LR.Pos[i].pos   = (short) posaux;
         LR.Pos[i].color = (byte)  coloraux;
      }
      mmrtmp.LC = LC;
      mmrtmp.LR = LR;    
      ++mmrtmp.nlin;
      int pos = (int) (1078 + ((mmrtmp.nlin - 1) * mmrtmp.sizelin));
      for(i=0; i<mmrtmp.sizelin; i++)
         if (mmrtmp.LC.ptr[i] != 0)
            mmrtmp.img.ptrBmp[pos+i] = (byte) 0 ;  /*1*/
         else
            mmrtmp.img.ptrBmp[pos+i] = (byte) 255; /*0*/
      LC.ind  = 0;
      LC.ptr  = LC.linea;
      LC.indptr = 0;
      mmrtmp.LC = LC;
      mmrtmp.LR = LR;
      return mmrtmp;
   }
}
