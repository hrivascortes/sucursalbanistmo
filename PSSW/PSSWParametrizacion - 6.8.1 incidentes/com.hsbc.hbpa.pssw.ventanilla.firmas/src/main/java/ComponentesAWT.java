import java.awt.*;
import java.awt.datatransfer.*;
import java.awt.event.KeyEvent;

public class ComponentesAWT extends Frame
{
   Clipboard clipbd = getToolkit().getSystemClipboard();
   TextArea text = new TextArea(1, 1);
   TextField tfKey;
   TextField tfWhen;
   TextField tfX;
   TextField tfY;
   Button boton;

   public ComponentesAWT(Image logo, int indice, int ancho, int largo)
   {
      Panel centerPanel = new Panel();
      Panel bottomPanel = new Panel();

      tfKey = new TextField();
      tfKey.setEditable( false );
      tfKey.setVisible( false );
      tfWhen = new TextField();
      tfWhen.setEditable( false );
      tfWhen.setVisible( false );
      // Coordenada X relativa al componente, donde se ha producido el evento
      tfX = new TextField();
      tfX.setEditable( false );
      tfX.setVisible( false );
      // Coordenada Y relativa al componente, donde se ha producido el evento
      tfY = new TextField();
      tfY.setEditable( false );
		tfY.setVisible( false );
      // En la zona central colocamos los elementos mayores
      centerPanel.setLayout( new GridLayout( 1,3 ) );
      // Incoporamos los elementos peque�os en la zona inferior
      bottomPanel.add( tfKey );
      bottomPanel.add( tfWhen );
      bottomPanel.add( tfX );
      bottomPanel.add( tfY );
      // En la columna izquierda posicionamos un canvas
      centerPanel.add( new MiCanvas(logo, indice , ancho, largo) );
      // Incoporamos los elementos peque�os en la zona inferior
      setLayout( new BorderLayout() );
      add( "Center",centerPanel );
      add( "South",bottomPanel );
   }
   
	protected void processKeyEvent(KeyEvent e)
   {
      tfKey.setText( String.valueOf( e.getKeyCode() ));
      tfWhen.setText( String.valueOf( e.getWhen() ) );
      /** OPCION DE IMPRIMIR LA PANTALLA **/
      if (tfKey.getText().equals("1020") == true)
      {
         String selection = text.getSelectedText();
         StringSelection clipString = new StringSelection(selection);
         clipbd.setContents(clipString, clipString);
         //System.out.println("No puedes Imprimir la Firma Maximizada.");
      }
      /** OPCION DE SALIR POR TECLADO **/
      if ((tfKey.getText().equals("27") == true) || (tfKey.getText().equals("65535") == true))
      {
         //System.out.println("Salida por perder el foco por Teclado en ZOOM.");
         hide();
      }
   }
}
